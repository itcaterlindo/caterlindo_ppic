/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.37-MariaDB-1~bionic : Database - db_caterlindo_ppic
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_caterlindo_ppic` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci */;

USE `db_caterlindo_ppic`;

/*Table structure for table `td_purchase_suggestion_material` */

DROP TABLE IF EXISTS `td_purchase_suggestion_material`;

CREATE TABLE `td_purchase_suggestion_material` (
  `purchasesuggestionmaterial_kd` int(11) NOT NULL AUTO_INCREMENT,
  `purchasesuggestion_kd` int(11) DEFAULT NULL,
  `purchasesuggestionsobarang_kd` int(11) DEFAULT NULL,
  `part_kd` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `rm_kd` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `purchasesuggestionmaterial_rm_deskripsi` text COLLATE latin1_general_ci,
  `purchasesuggestionmaterial_rm_spesifikasi` text COLLATE latin1_general_ci,
  `purchasesuggestionmaterial_rm_satuan` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `purchasesuggestionmaterial_qty` float(10,2) DEFAULT NULL,
  `purchasesuggestionmaterial_leadtimesupplier_hari` float(10,2) DEFAULT NULL,
  `purchasesuggestionmaterial_leadtime_tanggal` date DEFAULT NULL,
  `purchasesuggestionmaterial_tglinput` datetime DEFAULT NULL,
  `purchasesuggestionmaterial_tgledit` datetime DEFAULT NULL,
  `admin_kd` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`purchasesuggestionmaterial_kd`),
  KEY `fk_purcahsesuggestionmat` (`purchasesuggestion_kd`),
  KEY `fk_purhcasesuggestion_sobrg` (`purchasesuggestionsobarang_kd`),
  CONSTRAINT `fk_purcahsesuggestionmat` FOREIGN KEY (`purchasesuggestion_kd`) REFERENCES `tm_purchase_suggestion` (`purchasesuggestion_kd`),
  CONSTRAINT `fk_purhcasesuggestion_sobrg` FOREIGN KEY (`purchasesuggestionsobarang_kd`) REFERENCES `td_purchase_suggestion_so_barang` (`purchasesuggestionsobarang_kd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*Table structure for table `td_purchase_suggestion_so` */

DROP TABLE IF EXISTS `td_purchase_suggestion_so`;

CREATE TABLE `td_purchase_suggestion_so` (
  `purchasesuggestionso_kd` int(11) NOT NULL,
  `purchasesuggestion_kd` int(11) DEFAULT NULL,
  `kd_msalesorder` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `purchasesuggestionso_leadtime_produksi` float(10,2) DEFAULT NULL,
  `purchasesuggestionso_tglinput` datetime DEFAULT NULL,
  `purchasesuggestionso_tgledit` datetime DEFAULT NULL,
  `admin_kd` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`purchasesuggestionso_kd`),
  KEY `fk_purchasesuggestion_kd_so` (`purchasesuggestion_kd`),
  CONSTRAINT `fk_purchasesuggestion_kd_so` FOREIGN KEY (`purchasesuggestion_kd`) REFERENCES `tm_purchase_suggestion` (`purchasesuggestion_kd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*Table structure for table `td_purchase_suggestion_so_barang` */

DROP TABLE IF EXISTS `td_purchase_suggestion_so_barang`;

CREATE TABLE `td_purchase_suggestion_so_barang` (
  `purchasesuggestionsobarang_kd` int(11) NOT NULL,
  `purchasesuggestion_kd` int(11) DEFAULT NULL,
  `purchasesuggestionso_kd` int(11) DEFAULT NULL,
  `kd_barang` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `bom_kd` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `purchasesuggestionsobarang_qty` float(10,2) DEFAULT NULL,
  `purchasesuggestionsobarang_tglinput` datetime DEFAULT NULL,
  `purchasesuggestionsobarang_tgledit` datetime DEFAULT NULL,
  `admin_kd` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`purchasesuggestionsobarang_kd`),
  KEY `fk_purhcasesuggestoiso_barang` (`purchasesuggestionso_kd`),
  KEY `fk_purchasesuggestion_sobarang` (`purchasesuggestion_kd`),
  CONSTRAINT `fk_purchasesuggestion_sobarang` FOREIGN KEY (`purchasesuggestion_kd`) REFERENCES `tm_purchase_suggestion` (`purchasesuggestion_kd`),
  CONSTRAINT `fk_purhcasesuggestoiso_barang` FOREIGN KEY (`purchasesuggestionso_kd`) REFERENCES `td_purchase_suggestion_so` (`purchasesuggestionso_kd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*Table structure for table `tm_purchase_suggestion` */

DROP TABLE IF EXISTS `tm_purchase_suggestion`;

CREATE TABLE `tm_purchase_suggestion` (
  `purchasesuggestion_kd` int(11) NOT NULL,
  `purchasesuggestion_kode` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `purchasesuggestion_leadtime_prpo` float(10,2) DEFAULT NULL,
  `purchasesuggestion_note` text COLLATE latin1_general_ci,
  `purchasesuggestion_state` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `purchasesuggestion_tglinput` datetime DEFAULT NULL,
  `purchasesuggestion_tgledit` datetime DEFAULT NULL,
  `admin_kd` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`purchasesuggestion_kd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
