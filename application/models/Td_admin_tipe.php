<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_admin_tipe extends CI_Model {
	private $tbl_name = 'td_admin_tipe';
	private $p_key = 'kd_tipe_admin';
	private $title_name = 'Tipe Admin';

	public function check_access($kd_tipe_admin = '', $access_column = '') {
		
	}

	public function get_all(){
		$act = $this->db->get($this->tbl_name);

		return $act;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}
}