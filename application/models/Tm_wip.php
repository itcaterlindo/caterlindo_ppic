<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_wip extends CI_Model
{
	private $tbl_name = 'tm_wip';
	private $p_key = 'wip_kd';

	/* --start ssp tabel untuk modul ppic-- */
	// public function ssp_table() {
	// 	$data['table'] = $this->tbl_name;

	// 	$data['primaryKey'] = $this->p_key;

	// 	$data['columns'] = array (
	// 		array( 'db' => 'a.'.$this->p_key,
	// 			'dt' => 1, 'field' => $this->p_key,
	// 			'formatter' => function($d, $row){

	// 				return $this->tbl_btn($d, $row[4]);
	// 			}),
	// 		array( 'db' => 'a.wo_tanggal',
	// 			'dt' => 2, 'field' => 'wo_tanggal',
	// 			'formatter' => function($d){
	// 				$d = format_date($d,'d-M-Y');
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'a.wo_noterbit',
	// 			'dt' => 3, 'field' => 'wo_noterbit',
	// 			'formatter' => function($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'GROUP_CONCAT( IF (c.tipe_customer = "Lokal", c.no_salesorder, c.no_po) ) AS no_salesorders',
	// 			'dt' => 4, 'field' => 'no_salesorders',
	// 			'formatter' => function($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'a.wo_status',
	// 			'dt' => 5, 'field' => 'wo_status',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);
	// 				$word = process_status($d);
	// 				$color = color_status($d);
	// 				$d = bg_label($word, $color);

	// 				return $d;
	// 			}),
	// 	);

	// 	$data['sql_details'] = sql_connect();
	// 	$data['joinQuery'] = "FROM tm_workorder AS a
	// 						LEFT JOIN tb_relasi_sowo as b ON a.wo_kd=b.wo_kd
	// 						LEFT JOIN tm_salesorder as c ON c.kd_msalesorder=b.kd_msalesorder
	// 						GROUP BY a.wo_kd";
	// 	$data['where'] = "";

	// 	return $data;
	// }
	/* --end ssp tabel untuk modul ppic-- */

	// private function tbl_btn($id, $wo_status) {
	// 	$btns = array();
	// 	$btns[] = get_btn(array('title' => 'Detail Item', 'icon' => 'list', 'onclick' => 'detail_wo_item(\''.$id.'\')'));
	// 	if ($wo_status == 'process_wo') {
	// 		$btns[] = get_btn(array('title' => 'Detail Work Order', 'icon' => 'list', 'onclick' => 'itemdetail_box(\''.$id.'\')'));
	// 		$btns[] = get_btn(array('title' => 'Cetak Work Order', 'icon' => 'print', 'onclick' => 'cetak_wo(\''.$id.'\')'));
	// 	}
	// 	if ($wo_status == 'pending') {
	// 		$btns[] = get_btn_divider();
	// 		$btns[] = get_btn(array('title' => 'Cancel Work Order', 'icon' => 'times-circle', 'onclick' => 'cancel_wo(\''.$id.'\')'));
	// 	}

	// 	$btn_group = group_btns($btns);

	// 	return $btn_group;
	// }

	public function create_code()
	{
		$query = $this->db->select('MAX(' . $this->p_key . ') as maxID')
			->get($this->tbl_name)
			->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}
	public function insert_data($data)
	{
		$query = $this->db->insert($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_data($id)
	{
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		return $query ? TRUE : FALSE;
	}

	public function get_by_param($param = [])
	{
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all()
	{
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data($aWhere = [], $data)
	{
		$query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query ? TRUE : FALSE;
	}

	public function insert_batch_data($data = [])
	{
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_by_param($param = [])
	{
		$query = $this->db->delete($this->tbl_name, $param);
		return $query ? TRUE : FALSE;
	}

	public function get_where_in($where, $in = [])
	{
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}

	public function update_data_batch($key, $dataArray = [])
	{
		$query = $this->db->update_batch($this->tbl_name, $dataArray, $key);
		return $query ? TRUE : FALSE;
	}

	// /** Fungsi change wip stok mass update (update langsung semua, tidak 1-1) */
	// public function change_wip_stock($items = [], $sts)
	// {
	// 	$wips = $this->get_all()->result_array();
	// 	$aDataUpdate = [];
	// 	$aDataBarangUpdate = [];
	// 	foreach ($items as $item) {
	// 		foreach ($wips as $wip) {
	// 			/** Update  */
	// 			if ($wip['kd_barang'] == $item['kd_barang']) {
	// 				if ($sts == 'in') {
	// 					$qty = $wip['wip_qty'] + $item['qty'];
	// 				} else {
	// 					$qty = $wip['wip_qty'] - $item['qty'];
	// 				}
	// 				$aDataUpdate[] = [
	// 					'kd_barang' => $item['kd_barang'],
	// 					'wip_qty' =>  $qty,
	// 					'wip_tgledit' => date('Y-m-d H:i:s'),
	// 					'admin_kd' => $this->session->userdata('kd_admin'),
	// 				];
	// 				$aDataBarangUpdate[] = $item['kd_barang'];
	// 			}
	// 		}
	// 	}

	// 	$aDataBarangInsert = [];
	// 	$wip_kd = $this->create_code();
	// 	foreach ($items as $item2) {
	// 		if (!in_array($item2['kd_barang'], $aDataBarangUpdate)) {
	// 			$aDataBarangInsert[] = [
	// 				'wip_kd' => $wip_kd,
	// 				'kd_barang' => $item2['kd_barang'],
	// 				'wip_qty' => $item2['qty'],
	// 				'wip_tglinput' => date('Y-m-d H:i:s'),
	// 				'wip_tgledit' => date('Y-m-d H:i:s'),
	// 				'admin_kd' => $this->session->userdata('kd_admin'),
	// 			];
	// 			$wip_kd++;
	// 		}
	// 	}
	// 	$act = true;
	// 	if (!empty($aDataBarangUpdate)) {
	// 		$result = $this->getSumQTYByKd($aDataUpdate);
	// 		$act = $this->update_data_batch('kd_barang', $result);
	// 	}
	// 	if (!empty($aDataBarangInsert)) {
	// 		$act = $this->insert_batch_data($aDataBarangInsert);
	// 	}
	// 	return $act;
	// }

	/** Fungsi lain mengurangi barang WIP satu satu (sebelumnya langsung banyak / mass update)  */
	public function change_wip_stock($items = [], $sts)
	{
		$wips = $this->get_all()->result_array();
		$aDataUpdate = [];
		$aDataBarangUpdate = [];
		foreach ($items as $item) {
			foreach ($wips as $wip) {
				/** Update  */
				if ($wip['kd_barang'] == $item['kd_barang']) {
					$aDataUpdate[] = [
						'kd_barang' => $item['kd_barang'],
						'wip_qty' =>  $item['qty'],
						'wip_tgledit' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					];
					$aDataBarangUpdate[] = $item['kd_barang'];
				}
			}
		}

		$aDataBarangInsert = [];
		/** Generate new code nya dari fungsi getSumQTYByKdInsert */
		foreach ($items as $item2) {
			if (!in_array($item2['kd_barang'], $aDataBarangUpdate)) {
				$aDataBarangInsert[] = [
					'kd_barang' => $item2['kd_barang'],
					'wip_qty' => $item2['qty'],
					'wip_tglinput' => date('Y-m-d H:i:s'),
					'wip_tgledit' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				];
			}
		}
		$act = true;
		if (!empty($aDataBarangUpdate)) {
			foreach($aDataUpdate as $key => $value){
				$wip = $this->get_by_param(['kd_barang' => $value['kd_barang']])->row_array();
				if ($sts == 'in') {
					$value['wip_qty'] =  $wip['wip_qty'] + $value['wip_qty'];
					/** Jika WIP qty hasilnya minus maka nilainya di 0 */
					if($value['wip_qty'] < 0){
						$value['wip_qty'] = 0;
					}
					$this->update_data(['kd_barang' => $value['kd_barang']], $value);
				} else {
					$value['wip_qty'] =  $wip['wip_qty'] - $value['wip_qty']; 
					/** Jika WIP qty hasilnya minus maka nilainya di 0 */
					if($value['wip_qty'] < 0){
						$value['wip_qty'] = 0;
					}
					$this->update_data(['kd_barang' => $value['kd_barang']], $value);
				}
			}
		}

		if (!empty($aDataBarangInsert)) {
			/** Summary QTY berdasarkan kode jika ada kode barang yang sama */
			$result = $this->getSumQTYByKdInsert($aDataBarangInsert);
			$act = $this->insert_batch_data($result);
		}
		return $act;
	}

	/** Fungsi untuk SUM wip qty ketika ada kode barang yang sama di satu DN (fungsi update) */
	private function getSumQTYByKdUpdate($data = []){
		$groups = array();
		foreach ($data as $item) {
			$key = $item['kd_barang'];
			$qty = 0;
			if (!array_key_exists($key, $groups)) {
				$groups[$key] = array(
					'kd_barang' => $item['kd_barang'],
					'wip_qty' =>  $item['wip_qty'],
					'wip_tgledit' => $item['wip_tgledit'],
					'admin_kd' => $item['admin_kd'],
				);
			} else {
				$qty = $item['wip_qty'] + $groups[$key]['wip_qty'];
				$groups[$key]['wip_qty'] = $qty;
			}
		}
		return $groups;
	}

	/** Fungsi untuk SUM wip qty ketika ada kode barang yang sama di satu WO (fungsi insert) */
	private function getSumQTYByKdInsert($data = []){
		$groups = array();
		$wip_kd = $this->create_code();
		foreach ($data as $item) {
			$key = $item['kd_barang'];
			$qty = 0;
			if (!array_key_exists($key, $groups)) {
				$groups[$key] = array(
					'wip_kd' 		=> $wip_kd,
					'kd_barang' 	=> $item['kd_barang'],
					'wip_qty' 		=> $item['wip_qty'],
					'wip_tglinput' 	=> $item['wip_tglinput'],
					'wip_tgledit' 	=> $item['wip_tgledit'],
					'admin_kd' 		=> $item['admin_kd'],
				);
			} else {
				$wip_kd = $wip_kd;
				$qty = $item['wip_qty'] + $groups[$key]['wip_qty'];
				$groups[$key]['wip_kd'] = $wip_kd;
				$groups[$key]['wip_qty'] = $qty;
			}
			$wip_kd++;
		}
		return $groups;
	}

}
