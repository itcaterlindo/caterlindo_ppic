<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_rak_ruang_kolom extends CI_Model {
	private $tbl_name = 'td_rak_ruang_kolom';
	private $p_key = 'rakruangkolom_kd';
	private $title_name = 'Data Ruang Rak Kolom';

	/* --start ssp tabel untuk modul data warehouse-- */
	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){

					return $this->tbl_btn($d, $row[2]);
				} ),
			array( 'db' => 'a.'.$this->p_key, 'dt' => 2, 'field' => $this->p_key ),
			array( 'db' => 'd.nm_gudang', 'dt' => 3, 'field' => 'nm_gudang' ),
			array( 'db' => 'c.nm_rak', 'dt' => 4, 'field' => 'nm_rak' ),
			array( 'db' => 'b.nm_rak_ruang', 'dt' => 5, 'field' => 'nm_rak_ruang' ),
			array( 'db' => 'a.rakruangkolom_nama', 'dt' => 6, 'field' => 'rakruangkolom_nama' ),
		);

		$data['sql_details'] = sql_connect();

		$data['joinQuery'] = "FROM ".$this->tbl_name." as a 
							LEFT JOIN td_rak_ruang as b ON a.kd_rak_ruang=b.kd_rak_ruang
							LEFT JOIN tm_rak as c ON b.rak_kd=c.kd_rak
							LEFT JOIN tm_gudang as d ON c.gudang_kd=d.kd_gudang";

		if (isset($_SESSION['modul_inventory']['kd_gudang']) && isset($_SESSION['modul_inventory']['kd_rak']) && isset($_SESSION['modul_inventory']['kd_rak_ruang']) ) :
			$data['where'] = "a.kd_rak_ruang = '".$_SESSION['modul_inventory']['kd_rak_ruang']."' ";
		else :
			$data['where'] = "";
		endif;

		return $data;
	}
	/* --end ssp tabel untuk modul data warehouse-- */

	/* --start button tabel untuk modul data warehouse-- */
	private function tbl_btn($id, $var) {
		$read_access = $this->session->read_access;
		$update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		// $btns[] = get_btn(array('access' => $read_access, 'title' => 'Data Barang', 'icon' => 'search', 'onclick' => 'data_barang(\''.$id.'\')'));
		$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah', 'icon' => 'pencil', 'onclick' => 'get_form(\''.$id.'\')'));
		$btns[] = get_btn_divider();
		$btns[] = get_btn(array('access' => $delete_access, 'title' => 'Hapus', 'icon' => 'trash',
			'onclick' => 'return confirm(\'Anda akan menghapus data ruang = '.$var.'?\')?hapus_data(\''.$id.'\'):false'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}
	/* --end button tabel untuk modul data warehouse-- */

	public function get_row($id = '') {
		$this->db->from($this->tbl_name)
			->where(array($this->p_key => $id));
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}

	public function get_data($id = '') {
		$row = $this->get_row($id);
		if (!empty($row)) :
			$data = array('rakruangkolom_kd'=> $row->rakruangkolom_kd, 'kd_rak_ruang' => $row->kd_rak_ruang, 'gudang_kd' => $row->gudang_kd, 'rak_kd' => $row->rak_kd, 'rakruangkolom_nama' => $row->rakruangkolom_nama);
		else :
			$kd_gudang = isset($_SESSION['modul_inventory']['kd_gudang'])?$_SESSION['modul_inventory']['kd_gudang']:'';
			$kd_rak = isset($_SESSION['modul_inventory']['kd_rak'])?$_SESSION['modul_inventory']['kd_rak']:'';
			$kd_rak_ruang = isset($_SESSION['modul_inventory']['kd_rak_ruang'])?$_SESSION['modul_inventory']['kd_rak_ruang']:'';
			$data = array('kd_rak_ruang' => $kd_rak_ruang, 'gudang_kd' => $kd_gudang, 'rak_kd' => $kd_rak, 'rakruangkolom_nama' => '');
		endif;
		return $data;
	}

	public function form_rules() {
		$rules = array(
			array('field' => 'txtNmRuangKolom', 'label' => 'Nama Ruang Kolom', 'rules' => 'required'),
		);
		return $rules;
	}

	public function form_warning($datas = '') {
		$forms = array('txtNmRuang');
		foreach ($datas as $key => $data) :
			$str[$data] = (!empty(form_error($forms[$key])))?buildLabel('warning', form_error($forms[$key], '"', '"')):'';
		endforeach;
		return $str;
	}

	private function create_code() {
		$this->db->select($this->p_key.' AS code')
			->from($this->tbl_name)
			->where(array('DATE(rakruangkolom_tglinput)' => date('Y-m-d')))
			->order_by('rakruangkolom_tglinput DESC, '.$this->p_key.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -3);
		endif;
		$angka = $urutan + 1;
		$primary = 'RRK'.date('dmy').str_pad($angka, 3, '000', STR_PAD_LEFT);
		return $primary;
	}

	public function submit_data($data = '') {
		if (!empty($data[$this->p_key])) :
			$label = 'Mengubah '.$this->title_name;
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'rakruangkolom_tgledit' => date('Y-m-d H:i:s')));
			$where = array($this->p_key => $data[$this->p_key]);
			$act = $this->update($submit, $where);
		else :
			$label = 'Menambahkan '.$this->title_name;
			$data[$this->p_key] = $this->create_code();
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'rakruangkolom_tglinput' => date('Y-m-d H:i:s')));
			$act = $this->create($submit);
		endif;
		$str = $this->report($act, $label, $submit);
		return $str;
	}

	private function create($data = '') {
		$act = $this->db->insert($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	private function update($data = '', $where = '') {
		$act = $this->db->update($this->tbl_name, $data, $where);
		return $act?TRUE:FALSE;
	}

	public function report($act = '', $label = '', $data = '') {
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
		endif;
		$this->m_builder->write_log($stat, $label, $data);
		return $str;
	}

	public function delete_data($id = '') {
		$act = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		$report = $this->report($act, 'Menghapus '.$this->title_name, array($this->p_key => $id));
		return $report;
	}

	public function get_byParam ($param=[]){
		$act = $this->db->get_where($this->tbl_name, $param);
		return $act;
	}

	public function get_all () {
		$act = $this->db->select($this->tbl_name.'.*, td_rak_ruang.* ,tm_rak.*, tm_gudang.*')
					->from($this->tbl_name)
					->join('td_rak_ruang', $this->tbl_name.'.kd_rak_ruang=td_rak_ruang.kd_rak_ruang', 'left')
					->join('tm_rak', $this->tbl_name.'.rak_kd=tm_rak.kd_rak', 'left')
					->join('tm_gudang', $this->tbl_name.'.gudang_kd=tm_gudang.kd_gudang', 'left')
					->get();
		return $act;
	}

	public function get_detail_by_param ($param=[]) {
		$act = $this->db->select($this->tbl_name.'.*, td_rak_ruang.* ,tm_rak.*, tm_gudang.*')
					->from($this->tbl_name)
					->join('td_rak_ruang', $this->tbl_name.'.kd_rak_ruang=td_rak_ruang.kd_rak_ruang', 'left')
					->join('tm_rak', 'td_rak_ruang.rak_kd=tm_rak.kd_rak', 'left')
					->join('tm_gudang', 'tm_rak.gudang_kd=tm_gudang.kd_gudang', 'left')
					->where($param)
					->get();
		return $act;
	}
}