<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_purchaseorder_logstatus extends CI_Model {
	private $tbl_name = 'tb_purchaseorder_logstatus';
	private $p_key = 'pologstatus_kd';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'postatus_kd', 
				'dt' => 1, 'field' => 'postatus_kd',
				'formatter' => function ($d){
					$d = $this->tbl_btn($d);
					
					return $d;
				}),
			array( 'db' => 'postatus_nama', 
				'dt' => 2, 'field' => 'postatus_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'postatus_level', 
				'dt' => 3, 'field' => 'postatus_level',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM ".$this->tbl_name;
		$data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id) {
		// $read_access = $this->session->read_access;
		// $update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		if($delete_access == 1 ){
			$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
			$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));
		}		
		$btn_group = group_btns($btns);

		return $btn_group;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }
    
    public function get_all(){
		$query = $this->db->get($this->tbl_name);
		return $query;
	}

	public function get_by_param_detail ($param=[]) {
		$query = $this->db->select($this->tbl_name.'.*, tb_purchaseorder_status.*, tb_admin.nm_admin')
					->from($this->tbl_name)
					->join('tb_admin', $this->tbl_name.'.pologstatus_user=tb_admin.kd_admin', 'left')
					->join('tb_purchaseorder_status', $this->tbl_name.'.postatus_kd=tb_purchaseorder_status.postatus_kd', 'left')
					->where($param)
					->order_by($this->tbl_name.'.pologstatus_tglinput', 'DESC')
					->get();
		return $query; 
	}

	public function get_max_log($po_kd) {
		$qmaxTrans = $this->db->select('a.postatus_kd, MAX(a.pologstatus_tglinput) AS tgl_maxtrans')
				->from($this->tbl_name.' AS a')
				->where('a.po_kd', $po_kd)
				->group_by('a.postatus_kd')
				->get_compiled_select();
				
		$query = $this->db->select('aa.*, bb.postatus_nama, cc.kd_karyawan, cc.nm_admin')
				->from($this->tbl_name.' AS aa')
				->join('tb_purchaseorder_status AS bb', 'aa.postatus_kd=bb.postatus_kd', 'left')
				->join('tb_admin AS cc', 'aa.pologstatus_user=cc.kd_admin', 'left')
				->join('('.$qmaxTrans.') as tt', 'aa.postatus_kd=tt.postatus_kd AND aa.pologstatus_tglinput=tt.tgl_maxtrans', 'right')
				->where('aa.po_kd', $po_kd)
				->get();
		return $query;
	}

	public function action_insert_log($po_kd, $postatus_level, $keterangan){
		$this->load->model(['tb_purchaseorder_status']);

		$rowStatus = $this->tb_purchaseorder_status->get_by_param (['postatus_level' => $postatus_level])->row_array();

		$dataLogStatus = [
			'po_kd' => $po_kd,
			'pologstatus_user' => $this->session->userdata('kd_admin'),
			'postatus_kd' => $postatus_level,
			'pologstatus_keterangan' => !empty($keterangan) ? $keterangan : null,
			'pologstatus_tglinput' => date('Y-m-d H:i:s'),
			'admin_kd' => $this->session->userdata('kd_admin'),
		];

		$act = $this->insert_data($dataLogStatus);

		return $act;
	}

	public function get_log_printout($po_kd = null) {
		$qmaxTrans = $this->db->select('c.wfstate_kd, MAX(a.pologstatus_tglinput) AS tgl_maxtrans')
				->from($this->tbl_name.' AS a')
				->join('td_workflow_transition as b', 'a.wftransition_kd=b.wftransition_kd', 'left')
				->join('td_workflow_state as c', 'b.wftransition_destination=c.wfstate_kd', 'left')
				->where('a.po_kd', $po_kd)
				->group_by('c.wfstate_kd')
				->get_compiled_select();
				
		$query = $this->db->select('aa.*, c.wfstate_nama, cc.kd_karyawan, cc.nm_admin')
				->from($this->tbl_name.' AS aa')
				->join('td_workflow_transition as b', 'aa.wftransition_kd=b.wftransition_kd', 'left')
				->join('td_workflow_state as c', 'b.wftransition_destination=c.wfstate_kd', 'left')
				->join('tb_admin AS cc', 'aa.admin_kd=cc.kd_admin', 'left')
				->join('('.$qmaxTrans.') as tt', 'c.wfstate_kd=tt.wfstate_kd AND aa.pologstatus_tglinput=tt.tgl_maxtrans', 'right')
				->where('aa.po_kd', $po_kd)
				->where('c.wfstate_onprintout', 1)
				->order_by('c.wfstate_onprintoutseq', 'asc')
				->get();
				
		return $query;
	}
	
}