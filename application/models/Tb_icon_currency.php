<?php
defined('BASEPATH') or exit('No direct script acccess allowed!');

class Tb_icon_currency extends CI_Model {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'tb_icon_currency';
	private $p_key = 'id';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){

					return $this->tbl_btn($d, $row[1]);
				} ),
			array( 'db' => $this->p_key,
				'dt' => 2, 'field' => $this->p_key,
				'formatter' => function($d){

					return $d;
				} ),
			array( 'db' => 'nm_currency',
				'dt' => 3, 'field' => 'nm_currency',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				} ),
			array( 'db' => 'icon_currency',
				'dt' => 4, 'field' => 'icon_currency',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				} ),
			array( 'db' => 'icon_currency',
				'dt' => 5, 'field' => 'icon_currency',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);
					$icon = '<i class="fa '.$d.'"></i>'; 

					return $icon;
				} ),
		);

		$data['sql_details'] = sql_connect();

		$data['joinQuery'] = "";
		$data['where'] = "row_flag = 'active'";
		return $data;
	}

	function tbl_btn($id, $var) {		
		$read_access = $this->session->read_access;
		$update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
		$btns[] = get_btn_divider();
		$btns[] = get_btn(array('access' => $delete_access, 'title' => 'Hapus', 'icon' => 'trash', 'onclick' => 'return confirm(\'Anda akan menghapus Currency List = '.$var.'?\')?hapus_data(\''.$id.'\'):false'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	function get_all() {
		$this->db->select('id, nm_currency, icon_currency, icon_unicode, row_flag');
		$this->db->from($this->tbl_name);
		$this->db->where(array('row_flag' => 'active'));
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	function get_data($code) {
		$this->db->select('id, nm_currency, icon_currency, icon_unicode, row_flag');
		$this->db->from($this->tbl_name);
		$this->db->where(array('id' => $code));
		$query = $this->db->get();
		$result = $query->row();
		return $result;
	}

	function send_data($data) {
		if (empty($data['id'])) :
			$label_err = 'menambahkan';
			$act = $this->input_data($data);
		else :
			$label_err = 'mengubah';
			$act = $this->update_data($data, array('id' => $data['id']));
		endif;
		
		if ($act) :
			$log_stat = 'berhasil '.$label_err.' Currency List';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$log_stat.'!');
		else :
			$log_stat = 'gagal '.$label_err.' Currency List';
			$str['confirm'] = 'error';
			$str['idErrForm'] = buildAlert('danger', 'Gagal!', 'Gagal '.$log_stat.', kesalahan sistem!');
		endif;
		$this->m_builder->write_log($log_stat, $label_err, $data);

		return $str;
	}

	function input_data($data) {
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s'), 'row_flag' => 'active');
		$submit = array_merge($data, $data_adm);
		$aksi = $this->db->insert($this->tbl_name, $submit);
		return $aksi?TRUE:FALSE;
	}

	function update_data($data, $where) {
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s'));
		$submit = array_merge($data, $data_adm);
		$aksi = $this->db->update($this->tbl_name, $submit, $where);
		return $aksi?TRUE:FALSE;
	}
}