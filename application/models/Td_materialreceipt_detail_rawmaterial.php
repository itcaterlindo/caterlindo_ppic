<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_materialreceipt_detail_rawmaterial extends CI_Model {
	private $tbl_name = 'td_materialreceipt_detail_rawmaterial';
	private $p_key = 'materialreceiptdetailrm_kd';

    public function ssp_table($materialreceipt_kd) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array (
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){

					return $this->tbl_btn($d);
				}),
			array( 'db' => 'b.rm_kode',
				'dt' => 2, 'field' => 'rm_kode',
				'formatter' => function($d, $row){
					$d = "$d | {$row[7]} {$row[8]}";
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.materialreceiptdetailrm_qty',
				'dt' => 3, 'field' => 'materialreceiptdetailrm_qty',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.rmsatuan_nama',
				'dt' => 4, 'field' => 'rmsatuan_nama',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.materialreceiptdetailrm_qtykonversi',
				'dt' => 5, 'field' => 'materialreceiptdetailrm_qtykonversi',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'd.rmsatuan_kd',
				'dt' => 6, 'field' => 'rmsatuan_kd',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.materialreceiptdetailrm_tgledit',
				'dt' => 7, 'field' => 'materialreceiptdetailrm_tgledit',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.rm_deskripsi',
				'dt' => 8, 'field' => 'rm_deskripsi',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.rm_spesifikasi',
				'dt' => 9, 'field' => 'rm_spesifikasi',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
							LEFT JOIN tm_rawmaterial as b ON a.rm_kd=b.rm_kd
							LEFT JOIN td_rawmaterial_satuan as c ON c.rmsatuan_kd=a.materialreceiptdetailrm_satuan
							LEFT JOIN td_rawmaterial_satuan as d ON d.rmsatuan_kd=a.materialreceiptdetailrm_satuankonversi";
		$data['where'] = "a.materialreceipt_kd = '".$materialreceipt_kd."'";

		return $data;
	}

	public function ssp_table2 ($materialreceipt_kd) 
    {
		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		
		$data = [];
		$qData = $this->db->select($this->tbl_name.'.*, satuan.rmsatuan_nama as satuan, satuan_konversi.rmsatuan_nama as satuan_konversi, tm_rawmaterial.rm_kode, td_workorder_item.woitem_no_wo')
				->from($this->tbl_name)
				->where($this->tbl_name.'.materialreceipt_kd', $materialreceipt_kd)
				->join('tm_rawmaterial', $this->tbl_name.'.rm_kd=tm_rawmaterial.rm_kd', 'left')
				->join('td_rawmaterial_satuan as satuan', $this->tbl_name.'.materialreceiptdetailrm_satuan=satuan.rmsatuan_kd', 'left')
				->join('td_rawmaterial_satuan as satuan_konversi', $this->tbl_name.'.materialreceiptdetailrm_satuankonversi=satuan_konversi.rmsatuan_kd', 'left')
				->join('td_materialreceipt_detail', $this->tbl_name.'.materialreceiptdetail_kd=td_materialreceipt_detail.materialreceiptdetail_kd', 'left')
				->join('td_workorder_item', 'td_workorder_item.woitem_kd=td_materialreceipt_detail.woitem_kd', 'left')
                ->order_by($this->tbl_name.'.materialreceiptdetailrm_tgledit', 'desc')
				->get();
		foreach ($qData->result_array() as $r) {
			$data[] = [
				'1' => $this->tbl_btn($r['materialreceiptdetailrm_kd']),
				'2' => $r['woitem_no_wo'],
				'3' => "{$r['rm_kode']} {$r['materialreceiptdetailrm_deskripsi']} {$r['materialreceiptdetailrm_spesifikasi']}",
				'4' => $r['materialreceiptdetailrm_qty'],
				'5' => $r['satuan'],
				'6' => $r['materialreceiptdetailrm_qtykonversi'],
				'7' => $r['satuan_konversi'],
				'8' => $r['rmgr_code_srj'],
				'9' => $r['batch'],
				'10' => $r['materialreceiptdetailrm_tgledit'],
			];
		}
		
		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

	private function tbl_btn($id) {
		$btns = array();
		$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_item(\''.$id.'\')'));
		
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
        $query = $this->db->select('MAX('.$this->p_key.') as maxID')
                ->get($this->tbl_name)
                ->row();
        $code = (int) $query->maxID + 1;
        return $code;
	}

	public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}

	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }
    
    public function insert_batch_data ($data=[]){
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
    
    }

    public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

    public function get_where_in ($where, $in = []){
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}

	public function getMaterialOutGroupByRM($materialreceipt_kd)
	{
		return $this->db->select("{$this->tbl_name}.rm_kd, SUM({$this->tbl_name}.materialreceiptdetailrm_qtykonversi) as sum_materialreceiptdetailrm_qtykonversi,
				tm_rawmaterial.rm_kode, td_rawmaterial_satuan.rmsatuan_nama, {$this->tbl_name}.materialreceiptdetailrm_deskripsi, {$this->tbl_name}.materialreceiptdetailrm_spesifikasi")
			->join('tm_rawmaterial', "tm_rawmaterial.rm_kd={$this->tbl_name}.rm_kd", 'left')
			->join('td_rawmaterial_satuan', "td_rawmaterial_satuan.rmsatuan_kd={$this->tbl_name}.materialreceiptdetailrm_satuan", 'left')
			->where("{$this->tbl_name}.materialreceipt_kd", $materialreceipt_kd)
			->group_by("{$this->tbl_name}.rm_kd")
			->get($this->tbl_name)->result_array();
	}
		
	public function getMaterialDetail($params = [])
	{
		return $this->db->select("{$this->tbl_name}.*, tm_rawmaterial.rm_kode, td_rawmaterial_satuan.rmsatuan_nama, tm_rawmaterial.rm_oldkd, satuankonversi.rmsatuan_nama as satuankonversi_nama")
			->where($params)
			->join('tm_rawmaterial', "tm_rawmaterial.rm_kd={$this->tbl_name}.rm_kd", 'left')
			->join('td_rawmaterial_satuan', "td_rawmaterial_satuan.rmsatuan_kd={$this->tbl_name}.materialreceiptdetailrm_satuan", 'left')
			->join('td_rawmaterial_satuan as satuankonversi', "satuankonversi.rmsatuan_kd={$this->tbl_name}.materialreceiptdetailrm_satuankonversi", 'left')
			->get($this->tbl_name);
	}

	public function getMaterialOutGroupByMaterialReceiptDetailRM($materialreceipt_kd)
	{
		return $this->db->select("{$this->tbl_name}.materialreceiptdetail_kd, {$this->tbl_name}.rm_kd, SUM({$this->tbl_name}.materialreceiptdetailrm_qtykonversi) as sum_materialreceiptdetailrm_qtykonversi,
				tm_rawmaterial.rm_kode, td_rawmaterial_satuan.rmsatuan_nama, {$this->tbl_name}.materialreceiptdetailrm_deskripsi, {$this->tbl_name}.materialreceiptdetailrm_spesifikasi")
			->join('tm_rawmaterial', "tm_rawmaterial.rm_kd={$this->tbl_name}.rm_kd", 'left')
			->join('td_rawmaterial_satuan', "td_rawmaterial_satuan.rmsatuan_kd={$this->tbl_name}.materialreceiptdetailrm_satuan", 'left')
			->where("{$this->tbl_name}.materialreceipt_kd", $materialreceipt_kd)
			->group_by("{$this->tbl_name}.rm_kd, {$this->tbl_name}.materialreceiptdetail_kd")
			->get($this->tbl_name)->result_array();
	}

	public function getMaterialDetailGroupByRM($params = [])
	{
		return $this->db->select("{$this->tbl_name}.materialreceiptdetailrm_deskripsi, {$this->tbl_name}.materialreceiptdetailrm_spesifikasi, SUM({$this->tbl_name}.materialreceiptdetailrm_qty) AS sum_materialreceiptdetailrm_qty, SUM({$this->tbl_name}.materialreceiptdetailrm_qtykonversi) AS sum_materialreceiptdetailrm_qtykonversi, 
			tm_rawmaterial.rm_kode, td_rawmaterial_satuan.rmsatuan_nama, tm_rawmaterial.rm_oldkd, satuankonversi.rmsatuan_nama as satuankonversi_nama")
			->where($params)
			->join('tm_rawmaterial', "tm_rawmaterial.rm_kd={$this->tbl_name}.rm_kd", 'left')
			->join('td_rawmaterial_satuan', "td_rawmaterial_satuan.rmsatuan_kd={$this->tbl_name}.materialreceiptdetailrm_satuan", 'left')
			->join('td_rawmaterial_satuan as satuankonversi', "satuankonversi.rmsatuan_kd={$this->tbl_name}.materialreceiptdetailrm_satuankonversi", 'left')
			->group_by("{$this->tbl_name}.rm_kd")
			->get($this->tbl_name);
	}

	public function getMaterialDetailGroupByRMWoitem($params = [])
	{
		return $this->db->select("{$this->tbl_name}.rm_kd, {$this->tbl_name}.materialreceiptdetailrm_deskripsi, {$this->tbl_name}.materialreceiptdetailrm_spesifikasi, SUM({$this->tbl_name}.materialreceiptdetailrm_qty) AS sum_materialreceiptdetailrm_qty, SUM({$this->tbl_name}.materialreceiptdetailrm_qtykonversi) AS sum_materialreceiptdetailrm_qtykonversi, 
			tm_rawmaterial.rm_kode, td_rawmaterial_satuan.rmsatuan_nama, tm_rawmaterial.rm_oldkd, satuankonversi.rmsatuan_nama as satuankonversi_nama, td_materialreceipt_detail.woitem_kd")
			->where($params)
			->join('tm_rawmaterial', "tm_rawmaterial.rm_kd={$this->tbl_name}.rm_kd", 'left')
			->join('td_rawmaterial_satuan', "td_rawmaterial_satuan.rmsatuan_kd={$this->tbl_name}.materialreceiptdetailrm_satuan", 'left')
			->join('td_rawmaterial_satuan as satuankonversi', "satuankonversi.rmsatuan_kd={$this->tbl_name}.materialreceiptdetailrm_satuankonversi", 'left')
			->join('td_materialreceipt_detail', "td_materialreceipt_detail.materialreceiptdetail_kd={$this->tbl_name}.materialreceiptdetail_kd", 'left')
			->group_by("{$this->tbl_name}.rm_kd, td_materialreceipt_detail.woitem_kd")
			->get($this->tbl_name);
	}

}