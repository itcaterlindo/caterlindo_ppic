<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_deliverynote extends CI_Model
{
	private $tbl_name = 'tm_deliverynote';
	private $p_key = 'dn_kd';

	public function ssp_table()
	{
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array(
				'db' => 'a.' . $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function ($d, $row) {

					return $this->tbl_btn($d, $row[9]);
				}
			),
			array(
				'db' => 'a.dn_tanggal',
				'dt' => 2, 'field' => 'dn_tanggal',
				'formatter' => function ($d) {
					$d = format_date($d, 'Y-m-d');
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'a.dn_no',
				'dt' => 3, 'field' => 'dn_no',
				'formatter' => function ($d, $row) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'b.bagian_nama as asal',
				'dt' => 4, 'field' => 'asal',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'c.bagian_nama as tujuan',
				'dt' => 5, 'field' => 'tujuan',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'a.dn_note',
				'dt' => 6, 'field' => 'dn_note',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'd.dnstate_nama',
				'dt' => 7, 'field' => 'dnstate_nama',
				'formatter' => function ($d, $row) {
					$d = build_span($row[10], $d);

					return $d;
				}
			),
			array(
				'db' => 'a.dn_tgledit',
				'dt' => 8, 'field' => 'dn_tgledit',
				'formatter' => function ($d) {
					return $d;
				}
			),
			array(
				'db' => 'e.nm_admin',
				'dt' => 9, 'field' => 'nm_admin',
				'formatter' => function ($d) {
					return $d;
				}
			),
			array(
				'db' => 'a.dnstate_kd',
				'dt' => 10, 'field' => 'dnstate_kd',
				'formatter' => function ($d) {
					return $d;
				}
			),
			array(
				'db' => 'd.dnstate_span',
				'dt' => 11, 'field' => 'dnstate_span',
				'formatter' => function ($d) {
					return $d;
				}
			),

		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM " . $this->tbl_name . " AS a
                            LEFT JOIN tb_bagian as b ON a.dn_asal=b.bagian_kd
                            LEFT JOIN tb_bagian as c ON a.dn_tujuan=c.bagian_kd
                            LEFT JOIN tb_deliverynote_state as d ON a.dnstate_kd=d.dnstate_kd
							LEFT JOIN tb_admin as e ON e.kd_admin=a.dn_originator";

		$kd_admin = $this->session->userdata('kd_admin');
		$tipe_admin_kd = $this->session->userdata('tipe_admin_kd');
		$bagians = $this->tb_deliverynote_user->get_bagianAllowed($kd_admin, $tipe_admin_kd);
		$sBagians = implode(", ", $bagians);
		$data['where'] = "a.dn_asal IN ({$sBagians}) OR a.dn_tujuan IN ({$sBagians})";

		return $data;
	}

	public function ssp_table2($aParam = [])
	{
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));

		$kd_admin = $this->session->userdata('kd_admin');
		$tipe_admin_kd = $this->session->userdata('tipe_admin_kd');
		$bagians = $this->tb_deliverynote_user->get_bagianAllowed($kd_admin, $tipe_admin_kd);
		$qData = $this->db->select($this->tbl_name . '.*, 
					bagian_asal.bagian_nama AS bagian_asal_nama, 
					bagian_tujuan.bagian_nama AS bagian_tujuan_nama, 
					tb_deliverynote_state.dnstate_nama,
					tb_deliverynote_state.dnstate_span,
					tb_admin.nm_admin')
			->from($this->tbl_name)
			->join('tb_bagian as bagian_asal', $this->tbl_name . '.dn_asal=bagian_asal.bagian_kd', 'left')
			->join('tb_bagian as bagian_tujuan', $this->tbl_name . '.dn_tujuan=bagian_tujuan.bagian_kd', 'left')
			->join('tb_deliverynote_state', $this->tbl_name . '.dnstate_kd=tb_deliverynote_state.dnstate_kd', 'left')
			->join('tb_admin', $this->tbl_name . '.dn_originator=tb_admin.kd_admin', 'left')
			->where_in($this->tbl_name . '.dn_asal', $bagians)->or_where_in($this->tbl_name . '.dn_tujuan', $bagians)
			->get()->result_array();
		$data = [];
		foreach ($qData as $r) {
			$data[] = [
				'1' => $this->tbl_btn($r['dn_kd'], $r['dnstate_kd']),
				'2' => $r['dn_tanggal'],
				'3' => $r['dn_no'],
				'4' => $r['bagian_asal_nama'],
				'5' => $r['bagian_tujuan_nama'],
				'6' => $r['dn_note'],
				'7' => build_span($r['dnstate_span'], $r['dnstate_nama']),
				'8' => $r['dn_tgledit'],
				'9' => $r['nm_admin'],
			];
		}

		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

	public function ssp_table_dnfg()
	{
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array(
				'db' => 'a.' . $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function ($d, $row) {

					return $this->tbl_btn_dnfg($d);
				}
			),
			array(
				'db' => 'a.dn_tanggal',
				'dt' => 2, 'field' => 'dn_tanggal',
				'formatter' => function ($d) {
					$d = format_date($d, 'Y-m-d');
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'a.dn_no',
				'dt' => 3, 'field' => 'dn_no',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'b.bagian_nama as asal',
				'dt' => 4, 'field' => 'asal',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'c.bagian_nama as tujuan',
				'dt' => 5, 'field' => 'tujuan',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'd.dnstate_nama',
				'dt' => 6, 'field' => 'dnstate_nama',
				'formatter' => function ($d) {
					$d = $this->dn_status($d);

					return $d;
				}
			),
			array(
				'db' => 'a.dn_tglinput',
				'dt' => 7, 'field' => 'dn_tglinput',
				'formatter' => function ($d) {
					return $d;
				}
			),

		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM " . $this->tbl_name . " AS a
                            LEFT JOIN tb_bagian as b ON a.dn_asal=b.bagian_kd
                            LEFT JOIN tb_bagian as c ON a.dn_tujuan=c.bagian_kd
                            LEFT JOIN tb_deliverynote_state as d ON a.dnstate_kd=d.dnstate_kd";
		$data['where'] = "a.dn_tujuan = '70' AND a.dnstate_kd = 2";

		return $data;
	}

	private function tbl_btn($id, $dnstate_kd)
	{
		$dn = $this->get_by_param(['dn_kd' => $id])->row_array();
		$kd_admin = $this->session->userdata('kd_admin');
		$tipe_admin_kd = $this->session->userdata('tipe_admin_kd');
		$cekPerminssionCreated = $this->tb_deliverynote_user->cek_permission($dn['dn_asal'], $kd_admin, $tipe_admin_kd);

		$btns = array();
		$btns[] = get_btn(array('title' => 'Lihat Item', 'icon' => 'search', 'onclick' => 'lihat_data(\'' . $id . '\')'));
		if ($dnstate_kd == 1 && $cekPerminssionCreated == true) {
			$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_master(\'' . $id . '\')'));
		}
		if ($dnstate_kd == 3) {
			$btns[] = get_btn(array('title' => 'Cetak Item', 'icon' => 'print', 'onclick' => 'cetak_data(\'' . $id . '\')'));
		}
		if ($dnstate_kd == 1  && $cekPerminssionCreated == true) {
			$btns[] = get_btn_divider();
			$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\'' . $id . '\')'));
		}

		$btn_group = group_btns($btns);

		return $btn_group;
	}

	private function tbl_btn_dnfg($id)
	{
		$btns = array();
		$btns[] = get_btn(array('title' => 'Lihat Item', 'icon' => 'search', 'onclick' => 'lihat_data(\'' . $id . '\')'));

		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function dn_status($materialreq_status)
	{
		$label = 'warning';
		if ($materialreq_status == 'finish') {
			$label = 'success';
		}
		$status = '<span class="label label-' . $label . '">' . ucwords($materialreq_status) . '</span>';
		return $status;
	}

	public function create_code()
	{
		$query = $this->db->select('MAX(' . $this->p_key . ') as maxID')
			->get($this->tbl_name)
			->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

	public function create_no($bagian_kd, $dn_tanggal)
	{
		$this->load->model(['tb_bagian']);
		$bagian = $this->tb_bagian->get_by_param(['bagian_kd' => $bagian_kd])->row_array();

		$dn_year = format_date($dn_tanggal, 'Y');
		$query = $this->db->select('MAX(dn_no) as max_no')
			->where('YEAR(dn_tanggal)', $dn_year)
			->where('dn_asal', $bagian_kd)
			->get($this->tbl_name)->row();
		if (empty($query->max_no)) :
			$no = 0;
		else :
			$no = substr($query->max_no, -4);
		endif;
		$no = (int)$no + 1;
		$dn_no = 'DN' . '-' . strtoupper($bagian['bagian_nama']) . '-' . $dn_year . '-' . str_pad($no, '4', '0', STR_PAD_LEFT);

		return $dn_no;
	}

	public function insert_data($data)
	{
		$query = $this->db->insert($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_data($id)
	{
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		return $query ? TRUE : FALSE;
	}

	public function get_by_param($param = [])
	{
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all()
	{
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data($aWhere = [], $data)
	{
		$query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query ? TRUE : FALSE;
	}

	public function insert_batch_data($data = [])
	{
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_by_param($param = [])
	{
		$query = $this->db->delete($this->tbl_name, $param);
		return $query ? TRUE : FALSE;
	}

	public function get_where_in($where, $in = [])
	{
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}

	public function get_by_param_detail($param = [])
	{
		$query = $this->db->select($this->tbl_name . '.*, tb_deliverynote_state.*, bagian_asal.bagian_nama as bagian_asal, bagian_tujuan.bagian_nama as bagian_tujuan, tb_admin.nm_admin')
			->from($this->tbl_name)
			->join('tb_bagian as bagian_asal', $this->tbl_name . '.dn_asal=bagian_asal.bagian_kd', 'left')
			->join('tb_bagian as bagian_tujuan', $this->tbl_name . '.dn_tujuan=bagian_tujuan.bagian_kd', 'left')
			->join('tb_deliverynote_state', $this->tbl_name . '.dnstate_kd=tb_deliverynote_state.dnstate_kd', 'left')
			->join('tb_admin', $this->tbl_name . '.dn_originator=tb_admin.kd_admin', 'left')
			->where($param)
			->get();
		return $query;
	}
	public function all_received($dn_kd, $dn_asal = null, $dn_tujuan = null)
	{
		$resp = false;
		$qDn = $this->db->select('td_deliverynote_detail.dndetail_kd, td_deliverynote_detail.dndetail_qty, SUM(td_deliverynote_received.dndetailreceived_qty) as sumQtyReceived')
			->from('td_deliverynote_detail')
			->join('td_deliverynote_received', 'td_deliverynote_detail.dndetail_kd=td_deliverynote_received.dndetail_kd', 'left')
			->where('td_deliverynote_detail.dn_kd', $dn_kd)
			->group_by('td_deliverynote_detail.dndetail_kd')
			->get()->result();
		$arrCek = [];
		foreach ($qDn as $eDn) {
			$arrCek[] = $eDn->dndetail_qty - $eDn->sumQtyReceived;
		}
		if (!empty($arrCek) && max($arrCek) <= 0) {
			$resp = true;
		}
		return $resp;
	}

	public function update_woitem_prosesstatus($dn_kd)
	{
		$this->load->model(['td_workorder_item']);

		$qWoitemRcvs = $this->db->select('woitem_kd')
			->from('td_deliverynote_received')
			->where('dn_kd', $dn_kd)
			->group_by('woitem_kd')
			->get()->result_array();
		$qDnrcv = $this->db->select('td_workorder_item.woitem_kd, td_workorder_item.woitem_qty, SUM(td_deliverynote_received.dndetailreceived_qty) as SumDndetailreceived_kd')
			->from('td_workorder_item')
			->join('td_deliverynote_received', 'td_deliverynote_received.woitem_kd=td_workorder_item.woitem_kd', 'left')
			->join('tm_deliverynote', 'tm_deliverynote.dn_kd=td_deliverynote_received.dn_kd', 'left')
			->where('tm_deliverynote.dn_tujuan', '70')
			->where_in('td_workorder_item.woitem_kd', array_column($qWoitemRcvs, 'woitem_kd'))
			->group_by('td_workorder_item.woitem_kd')
			->get()->result_array();
		$woitem_prosestatus = 'finish';
		$data = [];
		foreach ($qDnrcv as $eDnrcv) {
			$diff = (int) $eDnrcv['woitem_qty'] - $eDnrcv['SumDndetailreceived_kd'];
			if ($diff == 0) {
				$data[] = $eDnrcv['woitem_kd'];
			}
		}
		$act = true;
		if (!empty($data)) {
			$act = $this->td_workorder_item->update_prosesstatus($data, $woitem_prosestatus);
		}
		return $act;
	}
}
