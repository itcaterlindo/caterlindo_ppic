<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_purchaseorder extends CI_Model {
	private $tbl_name = 'tm_purchaseorder';
	private $p_key = 'po_kd';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tb_purchaseorder_status', 'tb_purchaseorder_user', 'db_hrm/tb_hari_efektif', '_mail_configuration', 'tb_admin', 'tb_purchaseorder_user', 'td_rawmaterial_goodsreceive', 'tb_purchaseorder_logstatus']);	
    }

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){
					
					return $this->tbl_btn($d, $row[6]);
				}),
			array( 'db' => 'a.po_tglinput', 
				'dt' => 2, 'field' => 'po_tglinput',
				'formatter' => function ($d){
					$d = format_date($d, 'd-m-Y H:i:s');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.po_no', 
				'dt' => 3, 'field' => 'po_no',
				'formatter' => function ($d , $row){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'GROUP_CONCAT(f.pr_no) as pr_no', 
				'dt' => 4, 'field' => 'pr_no',
				'formatter' => function ($d , $row){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'b.suplier_nama', 
				'dt' => 5, 'field' => 'suplier_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'd.nm_admin', 
				'dt' => 6, 'field' => 'nm_admin',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.postatus_nama', 
				'dt' => 7, 'field' => 'postatus_nama',
				'formatter' => function ($d, $row){
					$d = $this->security->xss_clean($d);
					$d = $this->labelStatus($d, $row[7]);

					return $d;
				}),
			array( 'db' => 'c.postatus_label', 
				'dt' => 8, 'field' => 'postatus_label',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." as a
							LEFT JOIN tm_suplier b ON a.suplier_kd=b.suplier_kd
							LEFT JOIN tb_purchaseorder_status c ON a.postatus_kd=c.postatus_kd
							LEFT JOIN tb_admin d ON a.po_originator=d.kd_admin
							LEFT JOIN tb_relasi_popr as e ON a.po_kd=e.po_kd
							LEFT JOIN tm_purchaserequisition as f ON e.pr_kd=f.pr_kd
							GROUP BY a.po_kd";
		$data['where'] = "";
		
		return $data;
	}

	public function ssp_table2 ($aParam = []) {
		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		
		$data = [];
		$qData = $this->db->select($this->tbl_name.'.*, tm_suplier.suplier_nama, tb_admin.nm_admin, (SELECT rmgr_kd FROM td_rawmaterial_goodsreceive WHERE po_kd = tm_purchaseorder.po_kd GROUP BY po_kd) AS rmgr_kd, td_workflow_state.wfstate_nama,  td_workflow_state.wfstate_badgecolor, td_workflow_state.wfstate_action,
					GROUP_CONCAT(tm_purchaserequisition.pr_no SEPARATOR ", ") as pr_no')
				->from($this->tbl_name)
				->join('tm_suplier', $this->tbl_name.'.suplier_kd=tm_suplier.suplier_kd', 'left')
				->join('td_workflow_state', $this->tbl_name.'.wfstate_kd=td_workflow_state.wfstate_kd', 'left')
				->join('tb_admin', $this->tbl_name.'.po_originator=tb_admin.kd_admin', 'left')
				->join('tb_relasi_popr', $this->tbl_name.'.po_kd=tb_relasi_popr.po_kd', 'left')
				->join('tm_purchaserequisition', 'tb_relasi_popr.pr_kd=tm_purchaserequisition.pr_kd', 'left');
				//->join('td_rawmaterial_goodsreceive', $this->tbl_name.'.po_kd=td_rawmaterial_goodsreceive.po_kd', 'left');
		if (!empty($aParam['pr_no'])) {
			$qData = $qData->like('tm_purchaserequisition.pr_no', $aParam['pr_no'], 'match');
		}else{
			if (!empty($aParam['tahun'])){
				$qData = $qData->where('YEAR(tm_purchaseorder.po_tanggal)', $aParam['tahun']);
			}
			if ($aParam['bulan'] != 'ALL'){
				$qData = $qData->where('MONTH(tm_purchaseorder.po_tanggal)', $aParam['bulan']);
			}
			if ($aParam['wfstate_kd'] != 'ALL'){
				$qData = $qData->where('tm_purchaseorder.wfstate_kd', $aParam['wfstate_kd']);
			}
			if ($aParam['suplier_kd'] != 'ALL'){
				$qData = $qData->where('tm_suplier.suplier_kd', $aParam['suplier_kd']);
			}
		}
		
		$qData = $qData->group_by($this->tbl_name.'.po_kd')->order_by('td_workflow_state.wfstate_kd', 'asc')->order_by($this->tbl_name.'.po_tgledit', 'desc')
				->get();
		foreach ($qData->result_array() as $r) {
			$data[] = [
				'1' => $this->tbl_btn($r['po_kd'], $r['wfstate_action'], $r['rmgr_kd']),
				'2' => $this->tbl_btn_SAP($r['po_kd'], $r['status_sap']),
				'3' => $r['po_tanggal'],
				'4' => $r['po_no'],
				'5' => $r['pr_no'],
				'6' => $r['suplier_nama'],
				'7' => $r['nm_admin'],
				'8' => build_badgecolor($r['wfstate_badgecolor'], $r['wfstate_nama']),
				'9' => choice_enum($r['po_closeorder'])
			];
		}
		
		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

	private function tbl_btn_SAP($id, $status) {
		if (cek_permission('PURCHASEORDER_PUSH_TO_SAP')) {
			if($status == '0'){
				$btn_group ='<button type="button" onclick="push_to_sap(\''.$id.'\')" class="btn btn-p"><i class="fa fa-arrow-up"></i>&nbsp;Push To SAP</button>';
			}else{
				$btn_group ='<button type="button" onclick="push_to_sap(\''.$id.'\')" class="btn btn-success" disabled><i class="fa fa-check"></i>&nbsp;Done To SAP</button>';
			}
		}else{
			$btn_group = "";
		}
		
		return $btn_group;
	}

	private function tbl_btn($id, $wfstate_actions = null, $rmgr) {
		$aWfstate_action = explode(';', $wfstate_actions);
		$btns = array();
		if (cek_permission('PURCHASEORDER_VIEW')) {
			$btns[] = get_btn(array('title' => 'Lihat data', 'icon' => 'search', 'onclick' => 'lihat_data(\''.url_encrypt($id).'\')'));
		}
		if (in_array('edit', $aWfstate_action)){
			if (cek_permission('PURCHASEORDER_UPDATE')) {
				if(strlen($rmgr) == 0){
					$btns[] = get_btn(array('title' => 'Edit data', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
				}
			}
		}
		if(in_array('print', $aWfstate_action)){
			if (cek_permission('PURCHASEORDER_VIEW')) {
				$btns[] = get_btn(array('title' => 'Cetak Item', 'icon' => 'print', 'onclick' => 'cetak_data(\''.url_encrypt($id).'\')'));
				$btns[] = get_btn(array('title' => 'Update Note', 'icon' => 'file', 'onclick' => 'update_note(\''.$id.'\')'));
			}
		}
		// if ($state != 'Approved') {
		// 	if (cek_permission('PURCHASEORDER_VIEW')) {
		// 		$btns[] = get_btn(array('title' => 'Preview PO', 'icon' => 'paper-plane', 'onclick' => 'view_detail_box(\''.url_encrypt($id).'\')'));
		// 	}
		// }
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function labelStatus($label, $colour){
		$span = '<span class="label label-'.$colour.'">'.$label.'</span>';

		return $span;
	}

	public function create_code() {
		$this->db->select($this->p_key.' AS code')
			->from($this->tbl_name)
			->where(array('DATE(po_tglinput)' => date('Y-m-d')))
			->order_by($this->p_key.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -6);
		endif;
		$angka = $urutan + 1;
		$primary = 'MPO'.date('ymd').str_pad($angka, 6, '0', STR_PAD_LEFT);
		return $primary;
	}

	public function create_no() {
		$this->db->select('MAX(po_no) AS no')
			->from($this->tbl_name);
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$no = $row->no;
			$urutan =$no;
		endif;
		$angka = $urutan + 1;
		return $angka;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

	public function get_by_id_detail($id){
		$query = $this->db->select($this->tbl_name.'.*, tm_suplier.suplier_nama, tm_suplier.suplier_termpayment, tm_suplier.suplier_kode, tm_suplier.suplier_includeppn, tm_suplier.suplier_alamat, tm_suplier.suplier_telpon1, tm_suplier.kd_currency,tb_purchaseorder_status.*, tb_set_dropdown.nm_select')
					->from($this->tbl_name)
					->join('tm_suplier', $this->tbl_name.'.suplier_kd=tm_suplier.suplier_kd', 'left')
					->join('tb_set_dropdown', 'tm_suplier.badanusaha_kd = tb_set_dropdown.id', 'left')
					->join('tb_purchaseorder_status', $this->tbl_name.'.postatus_kd=tb_purchaseorder_status.postatus_kd', 'left')
					->join('tb_admin', $this->tbl_name.'.po_originator=tb_admin.kd_admin', 'left')
					->where($this->tbl_name.'.'.$this->p_key, $id)
					->get();
		return $query;
	}

	public function generateButtonAction($po_kd){
		$rowData = $this->get_by_param(array('po_kd' => $po_kd))->row_array();
		$rowLevel = $this->tb_purchaseorder_status->get_by_param(array('postatus_kd' => $rowData['postatus_kd']))->row_array();
		$nextLevel = $this->tb_purchaseorder_status->get_by_param(array('postatus_level' => $rowLevel['postatus_level'] + 1))->row_array();
		$backLevel = $this->tb_purchaseorder_status->get_by_param(array('postatus_level' => $rowLevel['postatus_level'] - 1))->row_array();
		$editingLevel = 1;

		/** Cek user */
		$admin_kd = $this->session->userdata('kd_admin');
		$isAdmin = $this->tb_admin->isAdmin($admin_kd);
		$isApprover = $this->tb_purchaseorder_user->isApprover($admin_kd);
		$isCurrentState = $this->tb_purchaseorder_user->isCurrentState($admin_kd, $rowLevel['postatus_kd']);
		$isEditingState = $this->tb_purchaseorder_status->isEditingState($rowLevel['postatus_kd']);
		$isMaxState = $this->tb_purchaseorder_status->isMaxState($rowLevel['postatus_kd']);
		$isFinishState = $this->tb_purchaseorder_status->isFinishState($rowLevel['postatus_kd']);
		
		$btn = '<div class="btn-group">';
		if ($isAdmin || $isApprover){
			$btn .= '<button class="btn btn-danger btn-sm"  onclick=ubah_status("'.$po_kd.'","'.$editingLevel.'","full")><i class="fa fa-random"></i> Ubah Status </button>';
		}

		if ($isCurrentState || $isAdmin || $isApprover || $isEditingState){
			$btn .= '<button class="btn btn-warning btn-sm"  onclick=ubah_status("'.$po_kd.'","'.$backLevel['postatus_level'].'","")><i class="fa fa-hand-o-left"></i> Ubah Status '.$backLevel['postatus_nama'].'</button>';
			if (!($isMaxState || $isFinishState)){
				$btn .= '<button class="btn btn-warning btn-sm"  onclick=ubah_status("'.$po_kd.'","'.$nextLevel['postatus_level'].'","")><i class="fa fa-hand-o-right"></i> Ubah Status '.$nextLevel['postatus_nama'].'</button>';
			}
		}
		/** Button Finish */
		if ($isMaxState) {
			$qGrdetail = $this->td_rawmaterial_goodsreceive->get_by_param(['po_kd' => $po_kd])->num_rows();
			if (!empty($qGrdetail)) {
				$btnFinish = '<button class="btn btn-primary btn-sm"  onclick=ubah_status("'.$po_kd.'","'.$nextLevel['postatus_level'].'","")><i class="fa fa-hand-o-right"></i> Ubah Status '.$nextLevel['postatus_nama'].'</button>';
				$btn .= $btnFinish;
			}
		}
		$btn .= '</div>';

		return $btn;
	}

	private function buildMessageEmail($po_kd, $postatus_level){
		return $this->load->view('templates/email/email_notification');
	}

	public function action_ubah_status($po_kd, $postatus_level, $keterangan){ 
		$this->load->helper(['my_helper']);       
		/** get id postatus */
		$rowStatus = $this->tb_purchaseorder_status->get_by_param(array('postatus_level' => $postatus_level))->row_array();

		$data = array(
			'postatus_kd' => $rowStatus['postatus_kd'],
			'po_tgledit' => date('Y-m-d H:i:s'),
			'admin_kd' => $this->session->userdata('kd_admin'),
		);

		$act = $this->update_data(array('po_kd' => $po_kd), $data);

		/** send email if true */
		$actEmail = false;
		$to = array();
		if ($rowStatus['postatus_sendemail'] == 'true'){
			/** get untuk data email */
			$rowPO = $this->get_by_id_detail($po_kd)->row_array();
			$subject = 'Purchase Order-'.$rowPO['po_no'].'-'.$rowPO['nm_select'].' '.$rowPO['suplier_nama'];
			$dataMessage['text'] = 'Terdapat Purchase Order yang perlu ditindak lanjuti :';
			$dataMessage['url'] = my_baseurl().'purchasing/purchase_order/purchase_order_detail?id='.url_encrypt($po_kd);
			$dataMessage['urltext'] = 'Detail Purchase Order';
			$dataMessage['keterangan'] = !empty($keterangan) ? $keterangan : '-';
			$message = $this->load->view('templates/email/email_notification', $dataMessage, true);
			
			$resultUser = $this->tb_purchaseorder_user->get_by_param (array('postatus_kd' => $rowStatus['postatus_kd']))->result_array();
			foreach ($resultUser as $each){
				$to[] = $each['pouser_email'];
			}
			$actEmail = $this->_mail_configuration->sendEmail($to, $subject, $message);
		}
		$actLog = $this->tb_purchaseorder_logstatus->action_insert_log($po_kd, $postatus_level, $keterangan);

		$resp = array('actUpdate'=> $act, 'email' => $actEmail, 'to' => $to);
		return $resp;        
	}
	
	public function get_by_param_detail($param=[]){
		$query = $this->db->select($this->tbl_name.'.*, tm_suplier.suplier_nama, tb_purchaseorder_status.*, tb_set_dropdown.nm_select')
					->from($this->tbl_name)
					->join('tm_suplier', $this->tbl_name.'.suplier_kd=tm_suplier.suplier_kd')
					->join('tb_set_dropdown', 'tm_suplier.badanusaha_kd = tb_set_dropdown.id', 'left')
					->join('tb_purchaseorder_status', $this->tbl_name.'.postatus_kd=tb_purchaseorder_status.postatus_kd', 'left')
					->where($param)
					->get();
		return $query;
	}

	/** Auto update PO jadi finish apabila sudah di goods receive semua */
	private function group_by_podetail ($data){
		$groups = array();
		foreach ($data as $item) {
			$key = $item['podetail_kd'];
			/** jika tidak ada key */
			if (!array_key_exists($key, $groups)) {
				$groups[$key] = array(
					'podetail_kd' => $key,
					'rekap_gr_qty' => (float) $item['rmgr_qty'],
				);
			}else{
				$groups[$key]['rekap_gr_qty'] = (float) $groups[$key]['rekap_gr_qty'] + $item['rmgr_qty'];
			}
		}
		
		foreach ($groups as $each){
			$groupResult[] = $each;
		}
		return $groupResult;
	}

	public function autoUpdate_status_po($po_kd){
		$rowPO = $this->get_by_param(array('po_kd' => $po_kd))->row_array();
		/** Cek status apakah sudah ACK atau sudah Finish */
		$act = true;
		$resp = array();
		$resultGroupGR = array(); 
		$isACKState = $this->tb_purchaseorder_status->isMaxState($rowPO['postatus_kd']);
		$isFinishState = $this->tb_purchaseorder_status->isFinishState($rowPO['postatus_kd']);

		$resultDetailPO = $this->td_purchaseorder_detail->get_by_param(array('po_kd' => $po_kd))->result_array();
		$resultGR = $this->td_rawmaterial_goodsreceive->get_by_param(array('po_kd' => $po_kd));
		
		if ($resultGR->num_rows() != 0){
			$resultGroupGR = $this->group_by_podetail ($resultGR->result_array());
		}
		/** Proses bandingkan data qty PR dan PO */
		$cekSelisih = array();
		foreach ($resultDetailPO as $eachDetailPO):
			$podetail_qty = (float) $eachDetailPO['podetail_qty'];
			foreach ($resultGroupGR as $eachGroupGR):
				if ($eachGroupGR['podetail_kd'] == $eachDetailPO['podetail_kd']){
					$podetail_qty = (float) $podetail_qty - $eachGroupGR['rekap_gr_qty'];
				}
			endforeach;
			$cekSelisih[] = (float) $podetail_qty;
		endforeach;
		/** Cek apabila nilai terbesarnya dari sisa antaran PR dikurangi dengan total qty PO lebih kecil dari nol
		 * maka lakukan update ke finish
		 */
		$rowFinishStatus = $this->tb_purchaseorder_status->finishStatus();
		if (max($cekSelisih) <= 0 && $isACKState){
			$act = $this->action_ubah_status($po_kd, $rowFinishStatus['postatus_level'], 'Update Otomatis');
			$resp = array('status' => $act['actUpdate']);
		}elseif(max($cekSelisih) > 0 && $isFinishState){
			/** Kembalikan menjadi ack state */
			$befFinish = (int) $rowFinishStatus['postatus_level'] - 1;
			$act = $this->action_ubah_status($po_kd, $befFinish, 'Update Otomatis');
			$resp = array('status' => $act['actUpdate']);
		}

		return $resp;
	}

	public function update_close_order($po_kd = null) {
		if (!empty($po_kd)) {
			$po = $this->get_by_param(['po_kd' => $po_kd])->row_array();
			$podetails = $this->td_purchaseorder_detail->get_by_param(array('po_kd' => $po_kd))->result_array();
			#group by podetail kd yang sudah di po kan
			$gr_groups = $this->db->select('podetail_kd, SUM(rmgr_qty) as sum_gr')
				->where('po_kd', $po_kd)
				->group_by('podetail_kd')
				->get('td_rawmaterial_goodsreceive')->result_array();
			$aSelisih = [];

			foreach ($podetails as $podetail):
				$resPo_qty = (float) $podetail['podetail_qty'];
				foreach ($gr_groups as $gr_group):
					if ($gr_group['podetail_kd'] == $podetail['podetail_kd']){
						$resPo_qty = (float) $resPo_qty - $gr_group['sum_gr'];
					}
				endforeach;
				$aSelisih[] = $resPo_qty;
			endforeach;
		}

		if (!empty($aSelisih)){
			# close order
			if (max($aSelisih) <= 0 && $po['po_closeorder'] == '0'){
				$act = $this->update_data(['po_kd' => $po_kd], ['po_closeorder' => '1', 'po_tgledit' => date('Y-m-d H:i:s')]);
			}elseif(max($aSelisih) > 0 && $po['po_closeorder'] == '1'){
				# reopen order
				$act = $this->update_data(['po_kd' => $po_kd], ['po_closeorder' => '0', 'po_tgledit' => date('Y-m-d H:i:s')]);
			}
		}

		return true;
	}

	public function getLeadtime($sup = [], $filter, $bulan, $tahun, $po_no)
	{
		$new_sup = array();
        for($i=0; $i<count($sup); $i++){
            array_push($new_sup, '"'.$sup[$i].'"');
        }
        $new_sup = implode(", ", $new_sup);

        $items = array();

        if($filter == 'po'){
            $query = $this->db->query('SELECT * FROM (
                            SELECT 
                                sp.suplier_kd, 
                                sp.suplier_nama, 
                                po.po_tanggal, 
                                po.po_no,
                                pr.pr_no,
                                pr.pr_kd,
                                pr.pr_tglinput as req_date,
                                (SELECT MIN(prdetail_duedate) FROM td_purchaserequisition_detail INNER JOIN td_purchaseorder_detail ON td_purchaserequisition_detail.prdetail_kd = td_purchaseorder_detail.prdetail_kd WHERE td_purchaseorder_detail.po_kd = po.po_kd) AS ppic_plan,
                                rmgr.rmgr_tgldatang,
                                CONCAT(rm.rm_nama , "/" , rm.rm_deskripsi) AS rm_nama,
                                rm.rm_kd,
                                rmk.rmkategori_nama as material,
								rmgs.rmgroupsup_name,
								sp.suplier_leadtime
                                FROM (SELECT rmgr_sive.* FROM td_rawmaterial_goodsreceive as rmgr_sive
										LEFT JOIN tm_purchaseorder po ON po.po_kd = rmgr_sive.po_kd
										LEFT JOIN tm_suplier sup ON sup.suplier_kd = po.suplier_kd
										GROUP BY po_kd) rmgr

                                LEFT JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd 
                                LEFT JOIN tm_suplier sp ON sp.suplier_kd = po.suplier_kd
                                LEFT JOIN td_purchaseorder_detail pod ON rmgr.podetail_kd = pod.podetail_kd
                                LEFT JOIN td_purchaserequisition_detail prd ON prd.prdetail_kd = pod.prdetail_kd
                                LEFT JOIN tm_purchaserequisition pr ON pr.pr_kd = prd.pr_kd
                                LEFT JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
								LEFT JOIN td_rawmaterial_group_suplier rmgs ON rm.rmgroupsup_kd = rmgs.rmgroupsup_kd
                                LEFT JOIN td_rawmaterial_kategori rmk ON rm.rmkategori_kd = rmk.rmkategori_kd WHERE rm.rmgroupsup_kd IS NOT NULL) AS Lead_time
                            
                            WHERE suplier_kd IN ('. $new_sup .') AND MONTH(rmgr_tgldatang) = "'. $bulan .'" AND YEAR(rmgr_tgldatang) = "'. $tahun .'" GROUP BY po_no ORDER BY rmgr_tgldatang ASC');

                            $data = $query->result();

                            foreach($data as $datas){
                                if(!in_array($datas->rmgroupsup_name, array_column($items, 'rmgroupsup_name'), false) && !in_array($datas->suplier_kd, array_column($items, 'material'), false)){
									// if(!in_array($datas->suplier_kd, array_column($items, 'suplier_kd'), false)){
										array_push($items, array(
											"suplier_kd" => $datas->suplier_kd,
											"suplier_nama" => $datas->suplier_nama,
											"material" => $datas->rmgroupsup_name,
											"leadtime" => $datas->suplier_leadtime,
											"real_leadtime" => "",
											"leadtime_ppic" => "",
											"po_no" => "",
											"pr_no" => "",
											"pr_date" => "",
											"po_date" => "",
											"gr_plan" => "",
											"ppic_plan" => "",
											"recive" => "",
											"real_leadtime" => "",
											"note" => '',
											"__children" => array()
										));
									// }
                                }
                            }
                    
                           
                            for ($i = 0; $i < count($items); $i++) {
                                foreach($data as $itm){
									if(!empty($itm->ppic_plan)){
										if($itm->rmgroupsup_name == $items[$i]['material'] && $itm->suplier_kd == $items[$i]['suplier_kd']){
											// if($itm->suplier_kd == $items[$i]['suplier_kd']){
												$leadtime_ppic = $this->tb_hari_efektif->get_lead_time($itm->po_tanggal,  $itm->ppic_plan) + 1;
												$real_leadtime = $this->tb_hari_efektif->get_lead_time($itm->po_tanggal, format_date($itm->rmgr_tgldatang, 'Y-m-d')) + 1;
												array_push($items[$i]['__children'], array(
													"material" => "",
													"leadtime" => "",
													"real_leadtime" => "",
													"leadtime_ppic" => $leadtime_ppic,
													"po_no" => $itm->po_no,
													"pr_no" => $itm->pr_no,
													"po_date" => $itm->po_tanggal,
													"ppic_plan" => $itm->ppic_plan,
													"pr_date" => format_date($itm->req_date, 'Y-m-d'),
													"gr_plan" => $itm->ppic_plan,
													"recive" => format_date($itm->rmgr_tgldatang, 'Y-m-d'),
													"real_leadtime" => $real_leadtime,
													"note" => ($real_leadtime > $leadtime_ppic && $itm->suplier_leadtime > $leadtime_ppic ? "PPIC Plan tdk terpenuhi, tp Leadtime terpenuhi" : ""),
												));
											// }
										}
									}
                                }
                            }
							$items = $this->my_array_unique($items, '');
                            return $items;
        }else{
                $new_po = array();

                for($i=0; $i<count($po_no); $i++){
                    array_push($new_po, '"'.$po_no[$i].'"');
                }
                $new_po = implode(", ", $new_po);

                $query = $this->db->query('SELECT suplier_kd, 
												suplier_nama, 
												po_tanggal, 
												po_no,
												pr_no,
												pr_kd,
												req_date,
												ppic_plan,
												SUM(podetail_qty) as podetail_qty,
												rm_nama,
												rm_kd,
												material
				FROM (
                    SELECT 
                        sp.suplier_kd, 
                        sp.suplier_nama, 
                        po.po_tanggal, 
                        po.po_no,
                        pr.pr_no,
                        pr.pr_kd,
                        pr.pr_tglinput as req_date,
                        prd.prdetail_duedate AS ppic_plan,
                        FLOOR(pod.podetail_qty) as podetail_qty,
                        rm.rm_deskripsi AS rm_nama,
                        rm.rm_kd,
                        rmk.rmkategori_nama as material
                    FROM tm_purchaseorder po
                    LEFT JOIN tm_suplier sp ON sp.suplier_kd = po.suplier_kd
                    LEFT JOIN td_purchaseorder_detail pod ON pod.po_kd = po.po_kd
                    LEFT JOIN td_purchaserequisition_detail prd ON prd.prdetail_kd = pod.prdetail_kd
                    LEFT JOIN tm_purchaserequisition pr ON pr.pr_kd = prd.pr_kd
                    LEFT JOIN tm_rawmaterial rm ON pod.rm_kd = rm.rm_kd
                    LEFT JOIN td_rawmaterial_kategori rmk ON rm.rmkategori_kd = rmk.rmkategori_kd WHERE rm.rmgroupsup_kd IS NOT NULL) AS Lead_time
                    
                    WHERE po_no IN ('. $new_po .') GROUP BY po_no ORDER BY ppic_plan ASC');

                    $data = $query->result();

                    foreach($data as $datas){
                        if(!in_array($datas->suplier_kd, array_column($items, 'suplier_kd'), false)){
                            array_push($items, array(
                                "suplier_kd" => $datas->suplier_kd,
                                "suplier_nama" => $datas->suplier_nama,
                                "po_no" => "",
                                "pr_no" => "",
                                "no_seri" => "",
                                "po_date" => "",
                                "gr_plan" => "",
                                "ppic_plan" => "",
                                "material" => "",
                                "qty_order" => "",
                                "item" => "",
                                "__children" => array()
                            ));
                        }
                    }
            
                   
                    for ($i = 0; $i < count($items); $i++) {
                        $ser = 1;
                        foreach($data as $itm){
                            if($itm->suplier_kd == $items[$i]['suplier_kd']){
                                array_push($items[$i]['__children'], array(
                                    "po_no" => $itm->po_no,
                                    "pr_no" => $itm->pr_no,
                                    "no_seri" => "$ser",
                                    "po_date" => $itm->po_tanggal,
                                    "gr_plan" => $itm->ppic_plan,
                                    "ppic_plan" => $itm->ppic_plan,
                                    "material" => $itm->rm_nama,
                                    "qty_order" => $itm->podetail_qty,
                                    "item" => $itm->material,
                                    
                                ));
                                $ser++;   
                            }
                        }
                    }
            
                    return $items;
            }

    
	}
	function filter_array($array,$term){
        $matches = array();
        foreach($array as $a){
            if($a['LTtoDuedate'] !== $term)
                $matches[]=$a;
        }
        return $matches;
    }


	public function getLeadtimePPIC($sup = [], $filter, $bulan, $tahun, $po_no)
	{
		$new_sup = array();
        for($i=0; $i<count($sup); $i++){
            array_push($new_sup, '"'.$sup[$i].'"');
        }
        $new_sup = implode(", ", $new_sup);

        $items = array();

        if($filter == 'po'){
			if($bulan == "ALL"){
				$query = $this->db->query('SELECT * FROM (
					SELECT 
						sp.suplier_kd, 
						sp.suplier_nama, 
						po.po_tanggal, 
						po.po_no,
						pr.pr_no,
						pr.pr_kd,
						pr.pr_tglinput as req_date,
						pod.podetail_kd,
						prd.prdetail_kd,
						(SELECT prdetail_duedate FROM td_purchaserequisition_detail WHERE prdetail_kd = prd.prdetail_kd) AS ppic_plan,
						(SELECT podetail_tgldelivery FROM td_purchaseorder_detail WHERE podetail_kd = pod.podetail_kd) AS ppic_plan_po,
						rmgr.rmgr_tgldatang,
						CONCAT(IFNULL(rm.rm_deskripsi, "") , " || ", IFNULL(rm.rm_spesifikasi, "")) AS rm_nama,
						rm.rm_kd,
						rm.rm_kode,
						rmk.rmkategori_nama as material,
						sp.suplier_leadtime
						FROM (SELECT rmgr_sive.* FROM td_rawmaterial_goodsreceive as rmgr_sive
															INNER JOIN tm_purchaseorder po ON po.po_kd = rmgr_sive.po_kd
															INNER JOIN tm_suplier sup ON sup.suplier_kd = po.suplier_kd) rmgr
						INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd 
						LEFT JOIN tm_suplier sp ON sp.suplier_kd = po.suplier_kd
						INNER JOIN td_purchaseorder_detail pod ON rmgr.podetail_kd = pod.podetail_kd
						INNER JOIN td_purchaserequisition_detail prd ON prd.prdetail_kd = pod.prdetail_kd
						INNER JOIN tm_purchaserequisition pr ON pr.pr_kd = prd.pr_kd
						INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
						INNER JOIN (SELECT * FROM td_rawmaterial_kategori WHERE rmkategori_kd NOT IN ("13", "14", "21","22","23","24","25","26")) rmk ON rm.rmkategori_kd = rmk.rmkategori_kd) AS Lead_time
					
					WHERE YEAR(rmgr_tgldatang) = "'. $tahun .'" GROUP BY rm_kode, po_no, rmgr_tgldatang ORDER BY rmgr_tgldatang ASC');
			}else{
				$query = $this->db->query('SELECT * FROM (
								SELECT 
									sp.suplier_kd, 
									sp.suplier_nama, 
									po.po_tanggal, 
									po.po_no,
									pr.pr_no,
									pr.pr_kd,
									pr.pr_tglinput as req_date,
									pod.podetail_kd,
									prd.prdetail_kd,
									(SELECT prdetail_duedate FROM td_purchaserequisition_detail WHERE prdetail_kd = prd.prdetail_kd) AS ppic_plan,
									(SELECT podetail_tgldelivery FROM td_purchaseorder_detail WHERE podetail_kd = pod.podetail_kd) AS ppic_plan_po,
									rmgr.rmgr_tgldatang,
									CONCAT(IFNULL(rm.rm_deskripsi, "") , " || ", IFNULL(rm.rm_spesifikasi, "")) AS rm_nama,
									rm.rm_kd,
									rm.rm_kode,
									rmk.rmkategori_nama as material,
									sp.suplier_leadtime
									FROM (SELECT rmgr_sive.* FROM td_rawmaterial_goodsreceive as rmgr_sive
																		INNER JOIN tm_purchaseorder po ON po.po_kd = rmgr_sive.po_kd
																		INNER JOIN tm_suplier sup ON sup.suplier_kd = po.suplier_kd) rmgr
									INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd 
									LEFT JOIN tm_suplier sp ON sp.suplier_kd = po.suplier_kd
									INNER JOIN td_purchaseorder_detail pod ON rmgr.podetail_kd = pod.podetail_kd
									INNER JOIN td_purchaserequisition_detail prd ON prd.prdetail_kd = pod.prdetail_kd
									INNER JOIN tm_purchaserequisition pr ON pr.pr_kd = prd.pr_kd
									INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
									INNER JOIN (SELECT * FROM td_rawmaterial_kategori WHERE rmkategori_kd NOT IN ("13", "14", "21","22","23","24","25","26")) rmk ON rm.rmkategori_kd = rmk.rmkategori_kd) AS Lead_time
								
								WHERE MONTH(rmgr_tgldatang) = "'. $bulan .'" AND YEAR(rmgr_tgldatang) = "'. $tahun .'" GROUP BY rm_kode, po_no, rmgr_tgldatang ORDER BY rmgr_tgldatang ASC');
			}

                            $data = $query->result();

                            foreach($data as $datas){
                                if(!in_array($datas->suplier_kd, array_column($items, 'material'), false)){
									// if(!in_array($datas->suplier_kd, array_column($items, 'suplier_kd'), false)){
										array_push($items, array(
											"suplier_kd" => $datas->rm_kode,
											"suplier_nama" => $datas->rm_nama,
											"material" => '',
											"leadtime" => '',
											"po_no" => '',
											"pr_no" => '',
											"pr_date" => '',
											"ppic_plan" => '',
											"po_date" => '',
											"gr_plan" => '',
											"recive" => '',
											"leadtime_ppic" => '',
											"real_leadtime" => '',
											"av" => '',
											"LTtoDuedate" => "",
											"range_duedate_pr_po"=>"",
											"range_duedate_pr_recipt"=>"",
											"range_duedate_po_recipt"=>"",
											"note" => '',
											"__children" => array()
										));
									// }
                                }
                            }

							$rata = 0;
							$ddpmls = [];
							$avgddpmls = 0;
							$rmdp =[];
							$avgrmdp = 0;
							//for ($i = 0; $i < count($items); $i++) {
                                 foreach($data as $itm){
                                //     if($itm->rm_kode == $items[$i]['suplier_kd']){
										// if($itm->suplier_kd == $items[$i]['suplier_kd']){
											$leadtime_ppic = $this->tb_hari_efektif->get_lead_time(format_date($itm->req_date, 'Y-m-d'), $itm->po_tanggal);
											$real_leadtime = $this->tb_hari_efektif->get_lead_time($itm->po_tanggal, format_date($itm->rmgr_tgldatang, 'Y-m-d'));

											//Average LT PR PO :
											$rata += $leadtime_ppic;

											//Duedate PR Melebihi LT Sup
											//ltduedatre range lt supplier
											$lt_duedate_pr = $this->tb_hari_efektif->get_lead_time(format_date($itm->req_date, 'Y-m-d'), $itm->ppic_plan);
											$av = $lt_duedate_pr - ROUND($rata / COUNT($data));
											$xc = $itm->suplier_leadtime - $av;
											if($xc > 0){
												// array_push($ddpmls, $xc);
												$avgddpmls += $xc;
											}
											// $lt_range_duedate_pr_po = $this->tb_hari_efektif->($itm->ppic_plan, $itm->ppic_plan_po);s
											// if($lt_range_duedate_pr_po != 0){
											// 	array_push($ddpmls, $lt_range_duedate_pr_po);
											// 	$avgddpmls += $lt_range_duedate_pr_po;

											// }
											

											//recipt melebihi duedate pr
											$lt_range_duedate_pr_recipt = $this->tb_hari_efektif->get_lead_time($itm->ppic_plan, format_date($itm->rmgr_tgldatang, 'Y-m-d'));
											if($lt_range_duedate_pr_recipt != 0){
												array_push($rmdp, $lt_range_duedate_pr_recipt);
												$avgrmdp += $lt_range_duedate_pr_recipt;

											}
										// }
                                //     }
                                // }
                            }
                    
                           
                            for ($i = 0; $i < count($items); $i++) {
                                foreach($data as $itm){
                                    if($itm->rm_kode == $items[$i]['suplier_kd']){
										// if($itm->suplier_kd == $items[$i]['suplier_kd']){
											//kolom LT PR PO
											$leadtime_ppic = $this->tb_hari_efektif->get_lead_time(format_date($itm->req_date, 'Y-m-d'), $itm->po_tanggal);

											//Kolom LT ACTUAL
											$real_leadtime = $this->tb_hari_efektif->get_lead_time($itm->po_tanggal, format_date($itm->rmgr_tgldatang, 'Y-m-d'));

											//KOLOM LT duedate PR
											$lt_duedate_pr = $this->tb_hari_efektif->get_lead_time(format_date($itm->req_date, 'Y-m-d'), $itm->ppic_plan);

											//Kolom jarak duedate pr ke duedate po
											$lt_range_duedate_pr_po = $this->tb_hari_efektif->get_lead_time($itm->ppic_plan, $itm->ppic_plan_po);

											//Kolom jarak duedate pr ke receipt
											$lt_range_duedate_pr_recipt = $this->tb_hari_efektif->get_lead_time($itm->ppic_plan, format_date($itm->rmgr_tgldatang, 'Y-m-d'));

											//Kolom jarak duedate po ke receipt
											$lt_range_duedate_po_recipt = $this->tb_hari_efektif->get_lead_time($itm->ppic_plan_po, format_date($itm->rmgr_tgldatang, 'Y-m-d'));

											//ltduedatre range lt supplier
											$av = $lt_duedate_pr - ROUND($rata / COUNT($data));

											array_push($items[$i]['__children'], array(
												"material" => $itm->suplier_nama,
												"leadtime" => $itm->suplier_leadtime,
												"po_no" => $itm->po_no,
												"pr_no" => $itm->pr_no,
												"pr_date" => format_date($itm->req_date, 'Y-m-d'),
												"ppic_plan" => $itm->ppic_plan,
												"po_date" => $itm->po_tanggal,
												"gr_plan" => $itm->ppic_plan_po,
												"recive" => format_date($itm->rmgr_tgldatang, 'Y-m-d'),
												"leadtime_ppic" => $leadtime_ppic,
												"real_leadtime" => $real_leadtime,
												"av" => $av,
												"LTtoDuedate" =>  ($itm->suplier_leadtime - $av <= 0) ? "" : $itm->suplier_leadtime - $av,
												"range_duedate_pr_po"=>$lt_range_duedate_pr_po,
												"range_duedate_pr_recipt"=>$lt_range_duedate_pr_recipt,
												"range_duedate_po_recipt"=>$lt_range_duedate_po_recipt,
												"note" => empty($itm->suplier_leadtime) ? 'Suplier Leadtime Null / 0.':null,
												
											));
											// $xc = $itm->suplier_leadtime - $av;
											// 	if($xc > 0){
											// 	array_push($ddpmls, $xc);
											// 	$avgddpmls += $xc;
											// }
										// }
                                    }
                                }
                            }

							$liste = [];

							foreach($data as $itm){

										if (empty($itm->suplier_leadtime)) {

											if(!in_array($itm->suplier_nama, $liste, true)){
												array_push($liste, $itm->suplier_nama);
											}
										}
							}
							$avgzz = 0;
							$items = $this->my_array_unique($items, '');
							$xarray = array_merge(array_column($items, '__children'));
							$result = array();
							foreach ($xarray as $nested) {
								$result = array_merge($result, $nested);
							}
							$ddpmls =  $this->filter_array($result, "");
							foreach ($ddpmls as $xuc) {
								$avgzz += (int) $xuc['LTtoDuedate'];
							}
							$bluk['vdata'] = $result;
							$bluk['vdataori'] = $ddpmls;

							$bluk['avgz'] = $avgzz;


							
                        	$bluk['items'] = $items;
							$bluk['avg_lt_pr_po'] =  ($rata != 0) ? ROUND($rata / COUNT($data)) : 0;
							$bluk['dd_pr_melebihi_lt_sup'] = COUNT($ddpmls);
							$bluk['avg_dd_pr_melebihi_lt_sup'] = ($avgzz != 0) ? ROUND($avgzz / COUNT($ddpmls)) : 0;
							$bluk['recipt_melebihi_dd_pr'] = COUNT($rmdp);
							$bluk['recipt_melebihi_dd_prxx'] = $rmdp;
							$bluk['avg_recipt_melebihi_dd_pr'] = ($avgrmdp != 0) ? ROUND($avgrmdp / COUNT($rmdp)) : 0;
							$bluk['sl0'] = $liste;
							$bluk['total_kejadian'] = COUNT($data);

							return $bluk;
        }

    
	}

	public function outstanding_po($rm_kd = [])
	{
		 $this->db->select('a.*, b.*, c.rm_kode, c.rm_oldkd, d.rmsatuan_nama, e.suplier_nama')
                    ->from('td_purchaseorder_detail as a')
                    ->join('tm_purchaseorder as b', 'a.po_kd=b.po_kd', 'left')
                    ->join('tm_rawmaterial as c', 'a.rm_kd=c.rm_kd', 'left')
                    ->join('td_rawmaterial_satuan as d', 'c.rmsatuan_kd=d.rmsatuan_kd', 'left')
                    ->join('tm_suplier as e', 'b.suplier_kd=e.suplier_kd', 'left')
					->join('td_workflow_state f', 'b.wfstate_kd=f.wfstate_kd', 'left')
					->join('tb_purchaseorder_status g', 'b.postatus_kd=g.postatus_kd', 'left')
					->group_start()
					->where('g.postatus_nama !=', 'Cancel')
					->where('g.postatus_nama !=', 'Finish')
					->group_end()
					->group_start()
					->where('g.postatus_nama !=', 'Approve')
					->where('b.po_closeorder !=', '1')
					->group_end();
					
		if( !empty($rm_kd) ){
			$this->db->where_in('a.rm_kd', $rm_kd);
		}
		$podetails = $this->db->get()->result_array();
        $arrayResult = [];
        if (!empty($podetails)) {
            foreach ($podetails as $podetail) {
                $podetailkds[] = $podetail['podetail_kd']; 
            }

            $grdetails = $this->db->select()
                        ->from('td_rawmaterial_goodsreceive as a')
                        ->where_in('podetail_kd', $podetailkds)
                        ->get()->result_array();

            if (!empty($grdetails)) {
                /** GR Group By podetail_kd */
                $groupsGR = []; $groupResult = [];
                foreach ($grdetails as $grdetail0) {
                    $key = $grdetail0['podetail_kd'];
                    if (!array_key_exists($key, $groupsGR)) {
                        $groupsGR[$key] = array(
                            'podetail_kd' => $key,
                            'rmgr_qty' => $grdetail0['rmgr_qtykonversi'],
                        );
                    } else {
                        $groupsGR[$key]['rmgr_qty'] = (float) $groupsGR[$key]['rmgr_qty'] + $grdetail0['rmgr_qtykonversi'];
                    }
                }
                foreach ($groupsGR as $eachGroup){
                    $groupResult[] = $eachGroup;
                }
                /**  GR Group By prdetail_kd */

                $arrayResult = [];
                foreach ($podetails as $podetail) {
                    $sisaPO = $podetail['podetail_qty'] * $podetail['podetail_konversi']; 
                    $rmgr_qty = 0;
                    foreach ($groupResult as $eachGroupResult) {
                        if ($eachGroupResult['podetail_kd'] == $podetail['podetail_kd']) {
                            $rmgr_qty = (float) $eachGroupResult['rmgr_qty'];
                            $sisaPO = $sisaPO - $rmgr_qty;
                        }
                    }
                    if ($sisaPO > 0) {
                        $arrayResult [] = [
                            'podetail_kd' => $podetail['podetail_kd'],
                            'po_tglinput' => $podetail['po_tglinput'],
                            'rm_kd' => $podetail['rm_kd'],
                            'rm_kode' => $podetail['rm_kode'],
                            'rm_oldkd' => $podetail['rm_oldkd'],
                            'podetail_nama' => $podetail['podetail_nama'],
                            'podetail_deskripsi' => $podetail['podetail_deskripsi'],
                            'podetail_spesifikasi' => $podetail['podetail_spesifikasi'],
                            'podetail_qty' => $podetail['podetail_qty'] * $podetail['podetail_konversi'],
                            'rmsatuan_nama' => $podetail['rmsatuan_nama'],
                            'podetail_tgldelivery' => $podetail['podetail_tgldelivery'],
                            'podetail_remark' => $podetail['podetail_remark'],
                            'po_no' => $podetail['po_no'],
                            'suplier_nama' => $podetail['suplier_nama'],
                            'sum_podetailqty' => $rmgr_qty.'/'.$sisaPO,
							'sisa_po' => $sisaPO
                        ];
                    }
                }
                // echo json_encode($arrayResult);die();
                /** Cari kembali detail Qty yang sudah di PO kan */
                // $arrayFinalResult = [];
                // if (!empty($arrayResult)) {
                //     foreach ($arrayResult as $eachResult) {
                //         foreach ($podetails as $podetail2) {
                //             if ($podetail2['podetail_kd'] == $eachResult['podetail_kd']) {
                //                 $arrayFinalResult[] = [
                //                     'podetail_kd' => $eachResult['podetail_kd'],
                //                     'po_tglinput' => $eachResult['po_tglinput'],
                //                     'rm_kd' => $eachResult['rm_kd'],
                //                     'rm_kode' => $eachResult['rm_kode'],
                //                     'rm_oldkd' => $eachResult['rm_oldkd'],
                //                     'podetail_nama' => $eachResult['podetail_nama'],
                //                     'podetail_deskripsi' => $eachResult['podetail_deskripsi'],
                //                     'podetail_spesifikasi' => $eachResult['podetail_spesifikasi'],
                //                     'podetail_qty' => $eachResult['podetail_qty'],
                //                     'rmsatuan_nama' => $eachResult['rmsatuan_nama'],
                //                     'podetail_tgldelivery' => $eachResult['podetail_tgldelivery'],
                //                     'podetail_remark' => $eachResult['podetail_remark'],
                //                     'po_no' => $eachResult['po_no'],
                //                     'podetail_qty' => $podetail2['podetail_qty'],
                //                     'no_po' => $podetail2['po_no'],
                //                 ];
                //             }
                //         }
                //     }
                // }

            }
        }

        $data['resultPO'] = $arrayResult;
        $data['resultGR'] = $grdetails;

		return $data;
	}

	/** Function for purchase suggest */
	public function outstanding_po_purchasesuggest($rm_kd = []){
		$outstandingPO = $this->outstanding_po($rm_kd); 
        /** SUM sisa PO berdasarkan rm_kd */
        $groupBy = $this->group_by("rm_kd", $outstandingPO['resultPO']);
        $arrFinish = [];
        $i = 0;
        foreach($groupBy as $groupKey => $groupVal){
            $arrFinish[$i]['rm_kd'] = $groupKey;
            $arrFinish[$i]['sisa_po'] = array_sum( array_column($groupVal, 'sisa_po') );
            $i++;
        }
        $data['resultPO'] = $arrFinish;
		return $data;
	}

	public function po_approved_by_itemgroup($kd_itemgroup, $tahun)
	{
		/** wf_kd 3 adalah kode workflow dari purchasing, wfstate_kd 15 adalah kode workflow status Approve */
		$data = $this->db->select('t1.po_tanggal, t1.po_no, t5.suplier_nama, t2.rm_kode, t0.podetail_deskripsi, t0.podetail_harga, t0.podetail_spesifikasi, (t0.podetail_qty) as podetail_qty, t6.rmsatuan_nama, t0.podetail_tgldelivery, GROUP_CONCAT( DISTINCT CONCAT(t7.rmgr_tglsrj, " | ", t7.rmgr_nosrj) ) as surat_jalan')
			->from('td_purchaseorder_detail t0')
			->join('tm_purchaseorder t1', 't0.po_kd = t1.po_kd', 'left')
			->join('tm_rawmaterial t2', 't0.rm_kd = t2.rm_kd', 'left')
			->join('tb_purchaseorder_status t3', 't1.postatus_kd = t3.postatus_kd', 'left')
			->join('td_rawmaterial_goodsreceive t4', 't0.podetail_kd = t4.podetail_kd', 'left')
			->join('tm_suplier t5', 't1.suplier_kd = t5.suplier_kd', 'left')
			->join('td_rawmaterial_satuan t6', 't0.rmsatuan_kd = t6.rmsatuan_kd', 'left')
			->join('td_rawmaterial_goodsreceive t7', 't0.podetail_kd = t7.podetail_kd', 'left')
			->where('t2.itemgroup_kd', $kd_itemgroup)
			->where('YEAR(t1.po_tanggal)', $tahun)
			->group_start()
			->where('t1.wf_kd', '3')
			->where('t1.wfstate_kd', '15')
			->group_end()
			->group_by('t0.podetail_kd')
			->order_by('t1.po_tanggal', 'DESC')
			->get()->result_array();
		return $data;
	}

	function my_array_unique($array, $keep_key_assoc = false){
		$duplicate_keys = array();
		$tmp = array();       
	
		foreach ($array as $key => $val){
			// convert objects to arrays, in_array() does not support objects
			if (is_object($val))
				$val = (array)$val;
	
			if (!in_array($val, $tmp))
				$tmp[] = $val;
			else
				$duplicate_keys[] = $key;
		}
	
		foreach ($duplicate_keys as $key)
			unset($array[$key]);
	
		return $keep_key_assoc ? $array : array_values($array);
	}

    /** Fungsi group by */
    private function group_by($key, $data) {
		$result = array();
		foreach($data as $val) {
			if(array_key_exists($key, $val)){
				$result[$val[$key]][] = $val;
			}else{
				$result[""][] = $val;
			}
		}
		return $result;
	}

}