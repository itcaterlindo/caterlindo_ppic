<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_inspectionproduct_userstate extends CI_Model
{
	private $tbl_name = 'td_inspectionproduct_userstate';

	public function ssp_table2()
	{
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		$userdataAdmin = $this->session->userdata('kd_admin');

		$query = $this->db->select('td_inspectionproduct_userstate.admin_kd, tb_admin.nm_admin, td_admin_tipe.nm_tipe_admin, GROUP_CONCAT(inspectionproductstate_nama SEPARATOR "; ") inspectionproductstate_nama, '.$this->tbl_name.'.inspectionproductuserstate_tglinput')
				->from($this->tbl_name)
				->join('tb_inspectionproduct_state', 'tb_inspectionproduct_state.inspectionproductstate_kd = '.$this->tbl_name.'.inspectionproductstate_kd', 'left')
				->join('tb_admin', 'tb_admin.kd_admin='.$this->tbl_name.'.admin_kd', 'left')
				->join('td_admin_tipe', 'td_admin_tipe.kd_tipe_admin=tb_admin.tipe_admin_kd', 'left')
				->group_by('td_inspectionproduct_userstate.admin_kd')
				->order_by('td_inspectionproduct_userstate.admin_kd, tb_inspectionproduct_state.inspectionproductstate_nama')->get();
		$qData = $query->result_array();
		$data = [];
		foreach ($qData as $r) {
			
			$data[] = [
				'1' => $this->tbl_btn($r['admin_kd']),
				'2' => $r['nm_admin'],
				'3' => $r['nm_tipe_admin'],
				'4' => $r['inspectionproductstate_nama'],
			];
		}

		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

	private function tbl_btn($admin_kd)
	{
		$btns = array();
		$btns[] = get_btn(array('title' => 'Edit state', 'icon' => 'edit', 'onclick' => 'form_state(\'' . $admin_kd . '\')'));
		$btns[] = get_btn(array('title' => 'Hapus state', 'icon' => 'trash', 'onclick' => 'hapus_data_user_state(\'' . $admin_kd . '\')'));

		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function insert_data($data)
	{
		$query = $this->db->insert($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_data($array = [])
	{
		$query = $this->db->delete($this->tbl_name, $array);
		return $query ? TRUE : FALSE;
	}

	public function get_by_param($param = [])
	{
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all()
	{
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data($aWhere = [], $data)
	{
		$query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query ? TRUE : FALSE;
	}

	public function insert_batch_data($data = [])
	{
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_by_param($param = [])
	{
		$query = $this->db->delete($this->tbl_name, $param);
		return $query ? TRUE : FALSE;
	}

	public function get_where_in($where, $in = [])
	{
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}

	public function cek_permission($inspectionproductstate_kd, $kd_admin = null) :bool
	{
		$qCountUser = 0;
		$resp = false;
		$qCountUser += $this->get_by_param(['admin_kd' => $kd_admin, 'inspectionproductstate_kd' => $inspectionproductstate_kd])->num_rows();

		if (!empty($qCountUser)) {
			$resp = true;
		}
		return $resp;
	}


}
