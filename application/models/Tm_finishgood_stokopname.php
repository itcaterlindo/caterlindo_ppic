<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_finishgood_stokopname extends CI_Model {
	private $tbl_name = 'tm_finishgood_stokopname';
	private $p_key = 'fgstopname_kd';

	public function __construct(){
		parent::__construct();

		// $this->load->model(['']);
	}

	// public function ssp_table() {
	// 	$data['table'] = $this->tbl_name;

	// 	$data['primaryKey'] = $this->p_key;

	// 	$data['columns'] = array(
	// 		array( 'db' => $this->p_key,
	// 			'dt' => 1, 'field' => $this->p_key,
	// 			'formatter' => function($d){
					
	// 				return $this->tbl_btn($d);
	// 			}),
	// 		array( 'db' => 'fgstopname_periode', 
	// 			'dt' => 2, 'field' => 'fgstopname_periode',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'fgstopname_tglperiode', 
	// 			'dt' => 3, 'field' => 'fgstopname_tglperiode',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'fgstopname_status', 
	// 			'dt' => 4, 'field' => 'fgstopname_status',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 	);

	// 	$data['sql_details'] = sql_connect();
	// 	$data['joinQuery'] = "FROM tm_finishgood_stokopname";

	// 	$fgstopname_tglperiode = date('Y');
	// 	$data['where'] = "YEARS(fgstopname_tglperiode) = '".$fgstopname_tglperiode."'";
		
	// 	return $data;
	// }

	// private function tbl_btn($id) {
	// 	// $read_access = $this->session->read_access;
	// 	// $update_access = $this->session->update_access;
	// 	// $delete_access = $this->session->delete_access;
	// 	$btns = array();
	// 	$btns[] = get_btn(array('title' => 'Cetak Barcode', 'icon' => 'print', 'onclick' => 'cetak_barcode(\''.$id.'\')'));
	// 	$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'del_data(\''.$id.'\')'));
		
	// 	$btn_group = group_btns($btns);

	// 	return $btn_group;
	// }


    public function buat_kode () {
		// FST190507000001
		$ident = 'FSO';
		$identtgl = substr(date('Y'), -2).date('m').date('d');

		$this->db->select($this->p_key);
		$this->db->where('DATE(fgstopname_tglinput)', date('Y-m-d'));
		$this->db->order_by('fgstopname_tglinput', 'DESC');
		$query = $this->db->get($this->tbl_name);
		if ($query->num_rows() == 0){
			$data = $ident.$identtgl.'000001';
		}else {
			$lastkode = $query->result_array();
			$lastkode = max($lastkode);
			$subs_laskode = substr($lastkode[$this->p_key], -6);
			$nextnumber = $subs_laskode + 1;
			$data = $ident.$identtgl.str_pad($nextnumber,6,"0",STR_PAD_LEFT);
		}
		return $data;
	}

    public function insert_data ($data){
        $query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
    }
    
    public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_byId($id){
		$this->db->where($this->p_key, $id);
		$act = $this->db->get($this->tbl_name);
		return $act;
    }
    	
	public function get_byParam ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all_by_year(){
		$act = $this->db->select('a.*, b.nm_gudang')
				->from('tm_finishgood_stokopname as a')
				->join('tm_gudang as b', 'a.kd_gudang=b.kd_gudang', 'left')
				->where('YEAR(a.fgstopname_tglperiode)', date('Y'))->order_by('a.fgstopname_tglinput', 'desc')->get();
		return $act;
	}

	public function get_all_detail(){
		return $this->db->join('tm_gudang', $this->tbl_name.'.kd_gudang=tm_gudang.kd_gudang', 'left')
			->order_by($this->p_key,'desc')->get($this->tbl_name);
	}

    
}