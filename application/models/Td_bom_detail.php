<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_bom_detail extends CI_Model
{
	private $tbl_name = 'td_bom_detail';
	private $p_key = 'bomdetail_kd';

	public function ssp_table($bom_kd)
	{
		$data['table'] = $this->tbl_name;
		$data['primaryKey'] = $this->p_key;
		$data['columns'] = array(
			array(
				'db' => 'a.' . $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function ($d) {

					return $this->tbl_btn($d);
				}
			),
			array(
				'db' => 'b.partmain_nama',
				'dt' => 2, 'field' => 'partmain_nama',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'c.partjenis_nama',
				'dt' => 3, 'field' => 'partjenis_nama',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'e.part_versi',
				'dt' => 4, 'field' => 'part_versi',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'a.bomdetail_generatewo',
				'dt' => 5, 'field' => 'bomdetail_generatewo',
				'formatter' => function ($d) {
					$d = choice_enum($d);

					return $d;
				}
			),
			array(
				'db' => 'a.bomdetail_wogrouping',
				'dt' => 6, 'field' => 'bomdetail_wogrouping',
				'formatter' => function ($d) {
					$d = choice_enum($d);

					return $d;
				}
			),
			array(
				'db' => 'a.bomdetail_tgledit',
				'dt' => 7, 'field' => 'bomdetail_tgledit',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);
					return $d;
				}
			),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM " . $this->tbl_name . " as a
							LEFT JOIN tm_part_main as b ON a.partmain_kd=b.partmain_kd
							LEFT JOIN td_part_jenis as c ON b.partjenis_kd=c.partjenis_kd
							LEFT JOIN tb_part_lastversion as d ON b.partmain_kd=d.partmain_kd
							LEFT JOIN td_part as e ON d.part_kd=e.part_kd";
		$data['where'] = "a.bom_kd= '" . $bom_kd . "'";

		return $data;
	}

	private function tbl_btn($id)
	{
		$btns = array();
		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data_detail(\'' . $id . '\')'));
		$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data_detail(\'' . $id . '\')'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code()
	{
		$this->db->select('MAX(' . $this->p_key . ') AS code')
			->from($this->tbl_name);
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = (int) substr($code, -10);
		endif;
		$angka = $urutan + 1;
		$primary = 'DBOM' . str_pad($angka, 10, '0', STR_PAD_LEFT);
		return $primary;
	}

	public function insert_data($data)
	{
		$query = $this->db->insert($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_data($id)
	{
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		return $query ? TRUE : FALSE;
	}

	public function get_by_param($param = [])
	{
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all()
	{
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data($aWhere = [], $data)
	{
		$query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query ? TRUE : FALSE;
	}

	public function delete_by_param($param = [])
	{
		$query = $this->db->delete($this->tbl_name, $param);
		return $query ? TRUE : FALSE;
	}

	public function get_by_param_detail($param = [])
	{
		$query = $this->db->select($this->tbl_name . '.*, td_part.*, td_part_jenis.*, tm_bom.*, tm_barang.item_code, tm_part_main.*')
			->from($this->tbl_name)
			->join('tm_part_main', $this->tbl_name . '.partmain_kd=tm_part_main.partmain_kd', 'left')
			->join('tb_part_lastversion', 'tm_part_main.partmain_kd=tb_part_lastversion.partmain_kd', 'left')
			->join('td_part', 'tb_part_lastversion.part_kd=td_part.part_kd', 'left')
			->join('td_part_jenis', 'td_part_jenis.partjenis_kd=tm_part_main.partjenis_kd', 'left')
			->join('tm_bom', 'tm_bom.bom_kd=' . $this->tbl_name . '.bom_kd', 'left')
			->join('tm_barang', 'tm_bom.kd_barang=tm_barang.kd_barang', 'left')
			->where($param)
			->get();
		return $query;
	}

	public function insert_batch_data($data = [])
	{
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}
}
