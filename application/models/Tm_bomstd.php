<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_bomstd extends CI_Model {
	private $tbl_name = 'tm_bomstd';
	private $p_key = 'bomstd_kd';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'b.item_code', 
				'dt' => 2, 'field' => 'item_code',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.deskripsi_barang', 
				'dt' => 3, 'field' => 'deskripsi_barang',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'b.dimensi_barang', 
				'dt' => 4, 'field' => 'dimensi_barang',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'a.bomstd_ket', 
				'dt' => 5, 'field' => 'bomstd_ket',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.bomstd_tglinput', 
				'dt' => 6, 'field' => 'bomstd_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] = "FROM ".$this->tbl_name." as a
                            LEFT JOIN tm_barang as b ON a.kd_barang=b.kd_barang";
		$data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btns = array();
		$btns[] = get_btn(array('title' => 'Detail Item', 'icon' => 'search', 'onclick' => 'detail_data(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Duplicate Item', 'icon' => 'clone', 'onclick' => 'duplicate_data(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));	
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
		$this->db->select('MAX('.$this->p_key.') AS code')
			->from($this->tbl_name);
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = (int) substr($code, -8);
		endif;
		$angka = $urutan + 1;
		$primary = 'MBOM'.str_pad($angka, 8, '0', STR_PAD_LEFT);
		return $primary;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}
	
	public function get_by_param_detail ($param=[]){
		$query = $this->db->select($this->tbl_name.'.*, tm_barang.*, tm_group_barang.nm_group')
					->from($this->tbl_name)
					->join('tm_barang', $this->tbl_name.'.kd_barang=tm_barang.kd_barang', 'left')
					->join('tm_group_barang', 'tm_barang.group_barang_kd = tm_group_barang.kd_group_barang', 'left')
					->where($param)
					->get();
		return $query;
	}

}