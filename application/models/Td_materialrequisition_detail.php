<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_materialrequisition_detail extends CI_Model {
	private $tbl_name = 'td_materialrequisition_detail';
	private $p_key = 'materialreqdetail_kd';

    public function ssp_table($materialreq_kd) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array (
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){

					return $this->tbl_btn($d);
				}),
			array( 'db' => 'b.woitem_no_wo',
				'dt' => 2, 'field' => 'woitem_no_wo',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.woitem_itemcode',
				'dt' => 3, 'field' => 'woitem_itemcode',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.materialreqdetail_qty',
				'dt' => 4, 'field' => 'materialreqdetail_qty',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.materialreqdetail_remark',
				'dt' => 5, 'field' => 'materialreqdetail_remark',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.materialreqdetail_tgledit',
				'dt' => 6, 'field' => 'materialreqdetail_tgledit',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
							LEFT JOIN td_workorder_item as b ON a.woitem_kd=b.woitem_kd";
		$data['where'] = "a.materialreq_kd = '".$materialreq_kd."'";

		return $data;
	}

	private function tbl_btn($id) {
		$btns = array();
		$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_item(\''.$id.'\')'));
		
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
        $query = $this->db->select('MAX('.$this->p_key.') as maxID')
                ->get($this->tbl_name)
                ->row();
        $code = (int) $query->maxID + 1;
        return $code;
	}

	public function create_no () {
		$query = $this->db->select('MAX(materialreq_no) as max_no')
				->get($this->tbl_name)->row();
		if (empty($query->max_no)) :
			$no = 0;
		else:
			$no = $query->max_no;
		endif;
		$no = $no + 1;

		return $no;
	}

	public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}

	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }
    
    public function insert_batch_data ($data=[]){
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
    
    }

    public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

    public function get_where_in ($where, $in = []){
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}
	
	public function get_by_param_detail ($param=[]) {
		$query = $this->db->select($this->tbl_name.'.*, td_workorder_item_detail.*')
		->from($this->tbl_name)
		->join('td_workorder_item_detail', $this->tbl_name.'.woitemdetail_kd=td_workorder_item_detail.woitemdetail_kd', 'left')
		->where($param)
		->get();
		return $query;
	}

	public function get_where_in_detail ($where, $in = []){
		$query = $this->db->select($this->tbl_name.'.*, tm_materialrequisition.*, td_workorder_item.*')
				->join('tm_materialrequisition', $this->tbl_name.'.materialreq_kd=tm_materialrequisition.materialreq_kd', 'left')
				->join('td_workorder_item', $this->tbl_name.'.woitem_kd=td_workorder_item.woitem_kd', 'left')
				->where_in($where, $in)
				->get($this->tbl_name);
		return $query;
	}

	public function getDetailMaterialReqWoitem ($materialreq_kd) 
    {
        $qResult = $this->db->join('td_workorder_item', "td_workorder_item.woitem_kd={$this->tbl_name}.woitem_kd",'left')
            ->where("{$this->tbl_name}.materialreq_kd", $materialreq_kd)
            ->get($this->tbl_name)->result_array();
        return $qResult;
    }
}