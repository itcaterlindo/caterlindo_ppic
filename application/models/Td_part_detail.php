<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_part_detail extends CI_Model {
	private $tbl_name = 'td_part_detail';
	private $p_key = 'partdetail_kd';

	public function ssp_table($part_kd) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'b.rm_kode',
				'dt' => 2, 'field' => 'rm_kode',
				'formatter' => function($d, $row){

					return $this->tbl_plate($d, $row[0], $row[7]);
				}),
			array( 'db' => 'CONCAT(COALESCE(a.partdetail_deskripsi,""), "/" , COALESCE(a.partdetail_spesifikasi,"") ) as concat_deskripsi', 
				'dt' => 3, 'field' => 'concat_deskripsi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.partdetail_qty', 
				'dt' => 4, 'field' => 'partdetail_qty',
				'formatter' => function ($d , $row){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'c.rmsatuan_nama', 
				'dt' => 5, 'field' => 'rmsatuan_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'd.bagian_nama', 
				'dt' => 6, 'field' => 'bagian_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.partdetail_tglinput', 
				'dt' => 7, 'field' => 'partdetail_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'b.rmkategori_kd', 
				'dt' => 8, 'field' => 'rmkategori_kd',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),

		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] = "FROM ".$this->tbl_name." as a
                            LEFT JOIN tm_rawmaterial as b ON a.rm_kd=b.rm_kd
                            LEFT JOIN td_rawmaterial_satuan as c ON a.rmsatuan_kd=c.rmsatuan_kd
                            LEFT JOIN tb_bagian as d ON a.bagian_kd=d.bagian_kd";
		$data['where'] = "a.part_kd= '".$part_kd."'";
		
		return $data;
	}

	public function ssp_table_costing($part_kd) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn_costing($d);
				}),
			array( 'db' => 'b.rm_kode',
				'dt' => 2, 'field' => 'rm_kode',
				'formatter' => function($d, $row){

					return $this->tbl_platecosting($d, $row[0], $row[10], $row[11]);
				}),
			array( 'db' => 'CONCAT(COALESCE(a.partdetail_deskripsi,""), "/" , COALESCE(a.partdetail_spesifikasi,"") ) as concat_deskripsi', 
				'dt' => 3, 'field' => 'concat_deskripsi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.partdetail_qty', 
				'dt' => 4, 'field' => 'partdetail_qty',
				'formatter' => function ($d , $row){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'c.rmsatuan_nama', 
				'dt' => 5, 'field' => 'rmsatuan_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'd.bagian_nama', 
				'dt' => 6, 'field' => 'bagian_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.partdetail_hargaunit', 
				'dt' => 7, 'field' => 'partdetail_hargaunit',
				'formatter' => function ($d){
					$d = custom_numberformat($d, 2, 'IDR');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.partdetail_hargatotal', 
				'dt' => 8, 'field' => 'partdetail_hargatotal',
				'formatter' => function ($d){
					$d = custom_numberformat($d, 2, 'IDR');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.partdetail_hargaupdate', 
				'dt' => 9, 'field' => 'partdetail_hargaupdate',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.partdetail_tglinput', 
				'dt' => 10, 'field' => 'partdetail_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'b.rmkategori_kd', 
				'dt' => 11, 'field' => 'rmkategori_kd',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.rm_oldkd', 
				'dt' => 12, 'field' => 'rm_oldkd',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),


		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] = "FROM ".$this->tbl_name." as a
                            LEFT JOIN tm_rawmaterial as b ON a.rm_kd=b.rm_kd
                            LEFT JOIN td_rawmaterial_satuan as c ON a.rmsatuan_kd=c.rmsatuan_kd
                            LEFT JOIN tb_bagian as d ON a.bagian_kd=d.bagian_kd";
		$data['where'] = "a.part_kd= '".$part_kd."'";
		
		return $data;
	}

	private function tbl_btn($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btns = array();
		$btn_group = '';
		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data_detail(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data_detail(\''.$id.'\')'));	
		$btn_group .= group_btns($btns);

		return $btn_group;
	}

	private function tbl_btn_costing($id) {
		$btns = array();
		$btn_group = '';
		$btns[] = get_btn(array('title' => 'Edit Harga', 'icon' => 'pencil', 'onclick' => 'edit_data_detailharga(\''.$id.'\')'));
		$btn_group .= group_btns($btns);

		return $btn_group;
	}

	private function tbl_plate($rm_kd, $id, $row){
		$rows = '';
		$rows = $rm_kd;
		/** Jika Plate */
		if ($row == '01'){
			$rows .= 
				'<a href="javascript:void(0);" id="" title="Detail Plate" onclick="open_detail_plate(\''.'add'.'\',\''.$id.'\')" class="btn btn-sm btn-primary pull-right">
					<i class="fa fa-plus"></i>
				</a>';
		}elseif ($row == '02') {
			$rows .= 
				'<a href="javascript:void(0);" id="" title="Detail Pipe" onclick="open_form_detailpipe (\''.''.'\',\''.$id.'\')" class="btn btn-sm btn-warning pull-right">
					<i class="fa fa-plus"></i>
				</a>';
		}
		return $rows;
	}

	private function tbl_platecosting($rm_kd, $id, $row, $rm_oldkd){
		$rows = '';
		$rows = $rm_kd.'/'.$rm_oldkd;
		/** Jika Plate */
		if ($row == '01'){
			$rows .= 
				'<a href="javascript:void(0);" id="" title="Detail Plate" onclick="open_detail_platecosting(\''.$id.'\')" class="btn btn-sm btn-primary pull-right">
					<i class="fa fa-plus"></i>
				</a>';
		}
		return $rows;
	}

	public function create_code() {
		$this->db->select('MAX('.$this->p_key.') AS code')
			->from($this->tbl_name);
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = (int) substr($code, -10);
		endif;
		$angka = $urutan + 1;
		$primary = 'DPRT'.str_pad($angka, 10, '0', STR_PAD_LEFT);
		return $primary;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }
    
    public function get_by_id_detail($id){
        $query = $this->db->select('a.*, f.partmain_nama, c.rm_kode, d.rmsatuan_nama')
                ->from($this->tbl_name.' as a')
				->join('td_part as b', 'a.part_kd=b.part_kd', 'left')
                ->join('tm_rawmaterial as c', 'a.rm_kd=c.rm_kd', 'left')
                ->join('td_rawmaterial_satuan as d', 'a.rmsatuan_kd=d.rmsatuan_kd', 'left')
				->join('tb_bagian as e', 'a.bagian_kd=e.bagian_kd', 'left')
				->join('tm_part_main as f', 'b.partmain_kd=f.partmain_kd', 'left')
                ->where('a.partdetail_kd', $id)
                ->get();
        return $query;
	}

    public function get_by_param_detail($param=[]){
        $query = $this->db->select($this->tbl_name.'.*, tm_part_main.partmain_nama, tm_rawmaterial.*, td_rawmaterial_satuan.rmsatuan_nama, tb_bagian.bagian_nama ')
                ->from($this->tbl_name)
                ->join('td_part', $this->tbl_name.'.part_kd=td_part.part_kd', 'left')
                ->join('tm_rawmaterial', $this->tbl_name.'.rm_kd=tm_rawmaterial.rm_kd', 'left')
                ->join('td_rawmaterial_satuan', 'tm_rawmaterial.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
				->join('tb_bagian', $this->tbl_name.'.bagian_kd=tb_bagian.bagian_kd', 'left')
				->join('tm_part_main', 'td_part.partmain_kd=tm_part_main.partmain_kd', 'left')
				->where($param)
				->order_by('tb_bagian.bagian_kd')
                ->get();
        return $query;
	}

    public function get_detail_in_part($key, $param=[]){
		$query = $this->db->select($this->tbl_name.'.*, tm_part_main.partmain_kd, tm_part_main.partmain_nama, tm_rawmaterial.rm_kode, tm_rawmaterial.rmkategori_kd, tm_rawmaterial.rm_platepanjang, tm_rawmaterial.rm_platelebar, tm_rawmaterial.rm_platetebal, tm_rawmaterial.rm_platemassajenis,
					td_rawmaterial_satuan.rmsatuan_nama, tb_bagian.bagian_nama ')
                ->from($this->tbl_name)
                ->join('td_part', $this->tbl_name.'.part_kd=td_part.part_kd', 'left')
                ->join('tm_rawmaterial', $this->tbl_name.'.rm_kd=tm_rawmaterial.rm_kd', 'left')
                ->join('td_rawmaterial_satuan', $this->tbl_name.'.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
				->join('tb_bagian', $this->tbl_name.'.bagian_kd=tb_bagian.bagian_kd', 'left')
				->join('tm_part_main', 'td_part.partmain_kd=tm_part_main.partmain_kd', 'left')
				->where_in($key,$param)
				->order_by('tb_bagian.bagian_kd')
                ->get();
        return $query;
	}

	public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

	public function insert_batch_data ($data=[]){
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}

	public function update_harga ($part_kd) {
		$this->load->model(['td_rawmaterial_harga']);
		$resp = false;
		if (!empty($part_kd)) {
			$resultPartdetail = $this->get_by_param(['part_kd' => $part_kd])->result_array();
			$arrRmkd = [];
			$arrayUpdate = [];
			foreach ($resultPartdetail as $r) {
				if (!in_array($r['rm_kd'], $arrRmkd)) {
					$arrRmkd [] = $r['rm_kd'];
				}
			}
			$resultRmharga = $this->td_rawmaterial_harga->get_where_in ('rm_kd', $arrRmkd)->result_array();
			foreach ($resultPartdetail as $r2) {
				$partdetail_hargaunit = null;
				$partdetail_hargatotal = null;
				foreach ($resultRmharga as $rHarga){
					if ($rHarga['rm_kd'] == $r2['rm_kd']) {
						$partdetail_hargaunit = $rHarga['rmharga_hargainput'];
						$partdetail_hargatotal = (float) $partdetail_hargaunit * $r2['partdetail_qty'];
					}
				}
				if ($r2['rm_kd'] != 'SPECIAL') {
					$arrayUpdate[] = [
						'partdetail_kd' => $r2['partdetail_kd'],
						'partdetail_hargaunit' => $partdetail_hargaunit,
						'partdetail_hargatotal' => $partdetail_hargatotal,
						'partdetail_hargaupdate' => date('Y-m-d H:i:s')
					];
				}
			}
			if (!empty($arrayUpdate)) {
				$resp = $this->db->update_batch($this->tbl_name, $arrayUpdate, 'partdetail_kd');
			}
		}else{
			$resp = false;
		}
		
		return $resp;
	}

}