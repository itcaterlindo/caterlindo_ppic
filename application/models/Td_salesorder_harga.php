<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_salesorder_harga extends CI_Model {
	private $tbl_name = 'td_salesorder_harga';
	private $p_key = 'kd_dharga_so';

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

}