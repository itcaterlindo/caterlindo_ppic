<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_rawmaterial_group_suplier extends CI_Model {
	private $tbl_name = 'td_rawmaterial_group_suplier';
	private $p_key = 'rmgroupsup_kd';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'rmgroupsup_kode', 
				'dt' => 2, 'field' => 'rmgroupsup_kode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'rmgroupsup_name', 
				'dt' => 3, 'field' => 'rmgroupsup_name',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM ".$this->tbl_name;
		$data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id) {
		$update_access = cek_permission('MATERIALGROUP_UPDATE');
		$btns = array();
		if ($update_access) {
			$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
			$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));
		}
		$btn_group = group_btns($btns);

		return $btn_group;
	}


    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	

    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function delete_by_param($param, $kode) {
		$query = $this->db->delete($this->tbl_name, array($param => $kode)); 
		return $query?TRUE:FALSE;
	}

}