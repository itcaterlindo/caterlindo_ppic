<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_finishgood_repacking extends CI_Model {
	private $tbl_name = 'td_finishgood_repacking';
	private $p_key = 'fgrepack_kd';

	public function ssp_table($fgrepack_tglinput) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $this->tbl_btn($d);
				}),
			array( 'db' => 'a.fgrepack_tglinput', 
				'dt' => 2, 'field' => 'fgrepack_tglinput',
				'formatter' => function ($d){
                    $d = format_date($d, 'd-m-Y H:i:s');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.item_code', 
				'dt' => 3, 'field' => 'item_code',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.fgbarcode_barcode', 
				'dt' => 4, 'field' => 'fgbarcode_barcode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.fgbarcode_desc', 
				'dt' => 5, 'field' => 'fgbarcode_desc',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.fgbarcode_dimensi', 
				'dt' => 6, 'field' => 'fgbarcode_dimensi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.fgrepack_status', 
				'dt' => 7, 'field' => 'fgrepack_status',
				'formatter' => function ($d){
					if ($d == 'IN'){
						$d = 'Repack Barang Masuk';
					}else{
						$d = 'Repack Barang Keluar';
					}
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array( 'db' => 'd.fgdetail_qty_awal', 
				'dt' => 8, 'field' => 'fgdetail_qty_awal',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array( 'db' => 'a.fgrepack_qty', 
				'dt' => 9, 'field' => 'fgrepack_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array( 'db' => 'a.fgrepack_qty', 
				'dt' => 10, 'field' => 'fgrepack_qty',
				'formatter' => function ($d){
					$d = !empty($d) ? $d:0;
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM ".$this->tbl_name." as a 
								LEFT JOIN tm_finishgood as b ON a.fg_kd=b.fg_kd
								LEFT JOIN td_finishgood_barcode as c ON a.fgbarcode_kd=c.fgbarcode_kd
								LEFT JOIN td_finishgood_detail as d ON a.fgrepack_kd=d.fgrepack_kd";
		$data['where'] = "DATE(a.fgrepack_tglinput) = '".$fgrepack_tglinput."'";
		
		return $data;
	}

	private function tbl_btn($id) {
		// $read_access = $this->session->read_access;
		// $update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		if($delete_access == 1 ){
			// $btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));
			$btns[] = get_btn(array('title' => '-', 'icon' => '-'));
		}		
		$btn_group = group_btns($btns);

		return $btn_group;
	}


    public function buat_kode () {
		// RPK190507000001
		$ident = 'RPK';
		$identtgl = substr(date('Y'), -2).date('m').date('d');

		$this->db->select($this->p_key);
		$this->db->where('DATE(fgrepack_tglinput)', date('Y-m-d'));
		$this->db->order_by('fgrepack_tglinput', 'DESC');
		$query = $this->db->get($this->tbl_name);
		if ($query->num_rows() == 0){
			$data = $ident.$identtgl.'000001';
		}else {
			$lastkode = $query->result_array();
			$lastkode = max($lastkode);
			$subs_laskode = substr($lastkode[$this->p_key], -6);
			$nextnumber = $subs_laskode + 1;
			$data = $ident.$identtgl.str_pad($nextnumber,6,"0",STR_PAD_LEFT);
		}
		return $data;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_byId($id){
		$this->db->where($this->p_key, $id);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_barcode_by_param($param=[]){
		$act = $this->db->select($this->tbl_name.'.*, td_finishgood_barcode.*, tm_finishgood.*')
						->from($this->tbl_name)
						->join('td_finishgood_barcode', 'td_finishgood_repacking.fgbarcode_kd=td_finishgood_barcode.fgbarcode_kd', 'left')
						->join('tm_finishgood', 'td_finishgood_barcode.fg_kd=tm_finishgood.fg_kd', 'left')
						->where($param)
						->get();
		return $act;
	}

	public function get_repack_by_date($date){
		$act = $this->db->select('a.*, d.item_code, bb.fgbarcode_barcode AS barcode_in, ccc.fgbarcode_barcode AS barcode_out, bb.fgbarcode_desc AS desc_in, ccc.fgbarcode_desc AS desc_out,
								bb.fgbarcode_dimensi AS dimensi_in, ccc.fgbarcode_dimensi AS dimensi_out, b.fgin_qty_awal AS awal_in, c.fgout_qty_awal AS awal_out')
						->from('td_finishgood_repacking AS a')
						->join('td_finishgood_in AS b', 'a.fgrepack_kd=b.fgrepack_kd', 'left')
						->join('td_finishgood_barcode AS bb', 'b.fgbarcode_kd=bb.fgbarcode_kd', 'left')
						->join('td_finishgood_out AS c', 'a.fgrepack_kd=c.fgrepack_kd', 'left')
						->join('td_finishgood_in AS cc', 'c.fgin_kd=cc.fgin_kd', 'left')
						->join('td_finishgood_barcode AS ccc', 'cc.fgbarcode_kd=ccc.fgbarcode_kd', 'left')
						->join('tm_finishgood AS d', 'a.fg_kd=d.fg_kd', 'left')
						->where('DATE(a.fgrepack_tglinput)', $date)
						->order_by('a.fgrepack_tglinput', 'desc')
						->get();
		return $act;
	}

	public function get_by_date_item($startdate, $enddate, $barang_kd, $kd_gudang){
		$this->load->model(['tm_barang']);
		$barang = $this->tm_barang->get_item(['kd_barang' => $barang_kd]);
		$itembarcode_custom = '899000';
		$this->db->select('a.*, d.item_barcode, d.item_code, bb.fgbarcode_barcode as barcode_in, ccc.fgbarcode_barcode as barcode_out, b.fgin_qty_awal as awal_in, c.fgout_qty_awal as awal_out, d.barang_kd, bb.fgbarcode_kd, e.item_group_kd')
						->from('td_finishgood_repacking AS a')
						// adj in
						->join('td_finishgood_in as b', 'a.fgrepack_kd=b.fgrepack_kd','left')
						->join('td_finishgood_barcode AS bb', 'b.fgbarcode_kd=bb.fgbarcode_kd', 'left')
						// adj out
						->join('td_finishgood_out as c', 'a.fgrepack_kd=c.fgrepack_kd','left')
						->join('td_finishgood_in as cc', 'c.fgin_kd=cc.fgin_kd','left')
						->join('td_finishgood_barcode AS ccc', 'cc.fgbarcode_kd=ccc.fgbarcode_kd', 'left')
	
						->join('tm_finishgood AS d', 'a.fg_kd=d.fg_kd', 'left')
						->join('tm_barang AS e', 'e.kd_barang=d.barang_kd', 'left');

		/** Jika barang PJ (item group custom) tampilkan semua item code yang itemgroupnya custom */
		if($barang->item_group_kd == '2'){
			$this->db->where('d.item_barcode', $itembarcode_custom);
		}else{
			$this->db->where('d.barang_kd', $barang_kd);
		}
		$this->db->where('DATE(a.fgrepack_tglinput) >=', $startdate)
						->where('DATE(a.fgrepack_tglinput) <=', $enddate)
						->where('d.kd_gudang', $kd_gudang)
						->order_by('a.fgrepack_tglinput','desc');
		$act = $this->db->get();
		return $act;
	}

	
}