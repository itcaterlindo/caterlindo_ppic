<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_files extends CI_Model {
    private $tbl_name = 'tb_files';
	private $p_key = 'file_kd';

	// public function ssp_table() {
	// 	$data['table'] = $this->tbl_name;

	// 	$data['primaryKey'] = $this->p_key;

	// 	$data['columns'] = array(
	// 		array( 'db' => $this->p_key, 
	// 			'dt' => 1, 'field' => $this->p_key,
	// 			'formatter' => function ($d){
	// 				$d = $this->tbl_btn($d);
					
	// 				return $d;
	// 			}),
	// 		array( 'db' => 'pph_nama', 
	// 			'dt' => 2, 'field' => 'pph_nama',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'pph_persen', 
	// 			'dt' => 3, 'field' => 'pph_persen',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 	);

	// 	$data['sql_details'] = sql_connect();
    //     $data['joinQuery'] =    "FROM ".$this->tbl_name;
	// 	$data['where'] = "";
		
	// 	return $data;
	// }

	// private function tbl_btn($id) {
	// 	$btns = array();
	// 	if (cek_permission('PPH_UPDATE')) {
	// 		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
	// 	}
	// 	if (cek_permission('PPH_DELETE')) {
	// 		$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));
	// 	}

	// 	$btn_group = group_btns($btns);

	// 	return $btn_group;
	// }

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by('pph_nama')->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }

    public function create_code() {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_by_param_in ($param, $params=[]) {
		$this->db->where_in($param, $params);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

}