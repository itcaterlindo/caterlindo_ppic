<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_rawmaterial_in extends CI_Model {
	private $tbl_name = 'td_rawmaterial_in';
	private $p_key = 'rmin_kd';

	public function ssp_table($date) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'a.rmin_tglinput', 
				'dt' => 2, 'field' => 'rmin_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.po_no', 
				'dt' => 3, 'field' => 'po_no',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.rm_kd', 
				'dt' => 4, 'field' => 'rm_kd',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'c.podetail_nama', 
				'dt' => 5, 'field' => 'podetail_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'c.podetail_deskripsi', 
				'dt' => 6, 'field' => 'podetail_deskripsi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.rmin_qty', 
				'dt' => 7, 'field' => 'rmin_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.rmin_konversiqty', 
				'dt' => 8, 'field' => 'rmin_konversiqty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
							LEFT JOIN tm_purchaseorder as b ON a.po_kd=b.po_kd
							LEFT JOIN td_purchaseorder_detail as c ON a.podetail_kd=c.podetail_kd";
        $data['where'] = "DATE(a.rmin_tglinput)='".$date."'";
		
		return $data;
	}

	private function tbl_btn($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btns = array();
		$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));	
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

	public function create_no() { 
		$max = $this->db->select('MAX(rmin_no) as maxno')
				->from($this->tbl_name)
				->get();
		$no = 0;
		if ($max->num_rows() != 0){
			$max = $max->row_array();
			$max = $max['maxno'];
			$no = $max + 1;
		}else{
			$no = 1;
		}
		return $no;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	

	public function get_by_param_detail($param = []){
		$query = $this->db->select($this->tbl_name.'.*, tm_rawmaterial.*, td_rawmaterial_satuan.rmsatuan_nama')
					->from($this->tbl_name)
					->join('tm_rawmaterial', $this->tbl_name.'.rm_kd=tm_rawmaterial.rm_kd')
					->join('td_rawmaterial_satuan', 'tm_rawmaterial.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd')
					->where($param)
					->get();
		return $query;
    }
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_where_in ($where, $in = []){
		$query = $this->db->where_in($where, $in)
				->join('tm_purchaseorder', $this->tbl_name.'.po_kd=tm_purchaseorder.po_kd', 'left')
				->select($this->tbl_name.'.*, tm_purchaseorder.*')
				->get($this->tbl_name);
		return $query;
	}

	public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

	public function get_kartustock($rm_kd, $startdate, $enddate){
		$result = $this->db->select($this->tbl_name.'.*, tm_rawmaterial.*, td_rawmaterial_goodsreceive.rmgr_nosrj, td_rawmaterial_goodsreceive.rmgr_type, td_rawmaterial_goodsreceive.rmgr_ket, td_rawmaterial_satuan.rmsatuan_nama, satuan_secondary.rmsatuan_nama as satuan_secondary_nama, td_rawmaterial_stok_adjustment.rmstokadj_type, td_rawmaterial_stok_adjustment.rmstokadj_remark, tm_purchaseorder.po_no')
					->from($this->tbl_name)
					->join('tm_rawmaterial', $this->tbl_name.'.rm_kd=tm_rawmaterial.rm_kd', 'left')
					->join('td_rawmaterial_satuan', 'tm_rawmaterial.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
					->join('td_rawmaterial_satuan as satuan_secondary', 'tm_rawmaterial.rmsatuansecondary_kd=satuan_secondary.rmsatuan_kd', 'left')
					->join('td_rawmaterial_goodsreceive', $this->tbl_name.'.rmgr_kd=td_rawmaterial_goodsreceive.rmgr_kd', 'left')
					->join('tm_purchaseorder', 'tm_purchaseorder.po_kd=td_rawmaterial_goodsreceive.po_kd', 'left')
					->join('td_rawmaterial_stok_adjustment', $this->tbl_name.'.rmstokadj_kd=td_rawmaterial_stok_adjustment.rmstokadj_kd', 'left')
					->where($this->tbl_name.'.rm_kd', $rm_kd)
					->where($this->tbl_name.'.rmin_tglinput >=', $startdate)
					->where($this->tbl_name.'.rmin_tglinput <=', $enddate)
					->order_by($this->tbl_name.'.rmin_tglinput')
					->get()->result_array();
		$arrResult = [];
		foreach ($result as $r) {
			$po_no = !empty($r['po_no']) ? "PO: {$r['po_no']}" : '';
			$trans_bukti = "GR-{$r['rmgr_nosrj']}"." (".$r['rmgr_type'].")";
			$trans_bukti_remark = $r['rmgr_ket'].' '.$po_no;
			if (!empty($r['rmstokadj_type'])){
				$trans_bukti = "ADJUSTMENT-{$r['rmstokadj_type']}";
				$trans_bukti_remark = $r['rmstokadj_remark'];
			}

			/** Jika terjadi good return / retur barang maka ubah nilai menjadi plus taruh ke trans qty out (rmgr_type RETURN) */
			$trans_jenis = ''; $trans_qty_in = 0; $trans_qty_out = 0;
			if($r['rmgr_type'] == 'RETURN'){
				$trans_jenis = 'OUT';
				$trans_qty_in = 0;
				$trans_qty_out = abs($r['rmin_qty']);
			}else{
				$trans_jenis = 'IN';
				$trans_qty_in = $r['rmin_qty'];
				$trans_qty_out = 0;
			}

			$arrResult[] = [
				'trans_tgl' => format_date($r['rmin_tglinput'], 'Y-m-d H:i:s'),
				'trans_jenis' => $trans_jenis,
				'rm_kd' => $r['rm_kd'],
				'rm_kode' => $r['rm_kode'],
				'rm_deskripsi' => $r['rm_deskripsi'],
				'rm_spesifikasi' => $r['rm_spesifikasi'],
				'rm_konversiqty' => $r['rm_konversiqty'],
				'trans_bukti' => $trans_bukti,
				'trans_bukti_remark' => $trans_bukti_remark,
				'trans_bukti_uraian' => $trans_bukti.' | '.$trans_bukti_remark,
				'trans_qty_in' => $trans_qty_in,
				'trans_qty_out' => $trans_qty_out,
				'trans_satuan' => $r['rmsatuan_nama'],
				'trans_qty_secondary' => $trans_qty_in / $r['rm_konversiqty'],
				'trans_satuan_secondary' => $r['satuan_secondary_nama'],
			];
		}
		return $arrResult;
	}
}