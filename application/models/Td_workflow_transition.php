<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_workflow_transition extends CI_Model {
	private $tbl_name = 'td_workflow_transition';
	private $p_key = 'wftransition_kd';

	public function ssp_table($wf_kd) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){
					
					return $this->tbl_btn($row[10], $d);
				}),
			array( 'db' => 'a.wftransition_nama', 
				'dt' => 2, 'field' => 'wftransition_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.wfstate_nama', 
				'dt' => 3, 'field' => 'wfstate_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'd.wfstate_nama as wfstate_dst', 
				'dt' => 4, 'field' => 'wfstate_dst',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.wftransition_email', 
				'dt' => 5, 'field' => 'wftransition_email',
				'formatter' => function ($d){
					$d = choice_enum($d);
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'b.wftransitionnotification_to', 
				'dt' => 6, 'field' => 'wftransitionnotification_to',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'b.wftransitionnotification_cc', 
				'dt' => 7, 'field' => 'wftransitionnotification_cc',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.wftransition_action', 
				'dt' => 8, 'field' => 'wftransition_action',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.wftransition_active', 
				'dt' => 9, 'field' => 'wftransition_active',
				'formatter' => function ($d){
					$d = choice_enum($d);
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.wftransition_tgledit', 
				'dt' => 10, 'field' => 'wftransition_tgledit',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.wf_kd', 
				'dt' => 11, 'field' => 'wf_kd',
				'formatter' => function ($d){
					$d = choice_enum($d);
					$d = $this->security->xss_clean($d);

					return $d;
                }),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
					LEFT JOIN td_workflow_transition_notification as b ON a.wftransition_kd=b.wftransition_kd
					LEFT JOIN td_workflow_state as c ON a.wftransition_source=c.wfstate_kd
					LEFT JOIN td_workflow_state as d ON a.wftransition_destination=d.wfstate_kd";
        $data['where'] = "a.wf_kd = $wf_kd";
		
		return $data;
	}

	private function tbl_btn($wf_kd, $id) {
		$btns = array();
		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$wf_kd.'\',\''.$id.'\')'));	
		$btns[] = get_btn(array('title' => 'Edit Permission', 'icon' => 'pencil', 'onclick' => 'edit_permission(\''.$wf_kd.'\',\''.$id.'\')'));	
		$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'delete_data(\''.$id.'\')'));	
		$btn_group = group_btns($btns);

		return $btn_group;
	}

    public function create_code () {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_where_in ($where, $in = []) {
		$query = $this->db->where_in($where, $in)
				->select($this->tbl_name.'.*')
				->get($this->tbl_name);
		return $query;
	}

	public function update_batch ($idColumn, $data = []) {
		$act = $this->db->update_batch($this->tbl_name, $data, $idColumn);
		return $act;
	}
	
}