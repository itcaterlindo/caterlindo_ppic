<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_finishgood_stokopname_detail extends CI_Model {
	private $tbl_name = 'td_finishgood_stokopname_detail';
	private $p_key = 'fgstopnamedetail_kd';

	public function __construct(){
		parent::__construct();
		$this->load->model(['td_finishgood_in']);
	}

	// public function ssp_table($fgstopname_kd, $kd_gudang) {
	// 	$data['table'] = $this->tbl_name;

	// 	$data['primaryKey'] = $this->p_key;

	// 	$data['columns'] = array(
	// 		array( 'db' => $this->p_key,
	// 			'dt' => 1, 'field' => $this->p_key,
	// 			'formatter' => function($d){
					
	// 				return $this->tbl_btn($d);
	// 			}),
	// 		array( 'db' => 'c.fgbarcode_barcode', 
	// 			'dt' => 2, 'field' => 'fgbarcode_barcode',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'd.item_code', 
	// 			'dt' => 3, 'field' => 'item_code',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'a.fgstopnamedetail_qty', 
	// 			'dt' => 4, 'field' => 'fgstopnamedetail_qty',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'c.fgbarcode_desc', 
	// 			'dt' => 5, 'field' => 'fgbarcode_desc',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'c.fgbarcode_dimensi', 
	// 			'dt' => 6, 'field' => 'fgbarcode_dimensi',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'a.rakruangkolom_kd', 
	// 			'dt' => 7, 'field' => 'rakruangkolom_kd',
	// 			'formatter' => function ($d){
	// 				$d = $this->td_finishgood_in->get_lokasi($d);
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'a.fgstopnamedetail_ket', 
	// 			'dt' => 8, 'field' => 'fgstopnamedetail_ket',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'a.fgstopnamedetail_tglinput', 
	// 			'dt' => 9, 'field' => 'fgstopnamedetail_tglinput',
	// 			'formatter' => function ($d){
	// 				$d = format_date($d, 'd-m-Y H:i:s');
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 	);

	// 	$data['sql_details'] = sql_connect();
	// 	$data['joinQuery'] = "FROM ".$this->tbl_name." as a
	// 							LEFT JOIN td_finishgood_in as b ON a.fgin_kd=b.fgin_kd
	// 							LEFT JOIN td_finishgood_barcode as c ON b.fgbarcode_kd=c.fgbarcode_kd
	// 							LEFT JOIN tm_barang as d ON c.barang_kd=d.kd_barang
	// 							LEFT JOIN tm_finishgood_stokopname as e ON a.fgstopname_kd=e.fgstopname_kd";
	// 	$data['where'] = "a.fgstopname_kd = '".$fgstopname_kd."' AND e.kd_gudang = '".$kd_gudang."'";
		
	// 	return $data;
	// }

	public function ssp_table($fgstopname_kd, $kd_gudang) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'b.fgbarcode_barcode', 
				'dt' => 2, 'field' => 'fgbarcode_barcode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'd.item_code', 
				'dt' => 3, 'field' => 'item_code',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.fgstopnamedetail_qty', 
				'dt' => 4, 'field' => 'fgstopnamedetail_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.fgbarcode_desc', 
				'dt' => 5, 'field' => 'fgbarcode_desc',
				'formatter' => function ($d, $row){
					$d = "$d $row[8]";
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.rakruangkolom_kd', 
				'dt' => 6, 'field' => 'rakruangkolom_kd',
				'formatter' => function ($d){
					$d = $this->td_finishgood_in->get_lokasi($d);
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.fgstopnamedetail_ket', 
				'dt' => 7, 'field' => 'fgstopnamedetail_ket',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.fgstopnamedetail_tglinput', 
				'dt' => 8, 'field' => 'fgstopnamedetail_tglinput',
				'formatter' => function ($d){
					$d = format_date($d, 'Y-m-d H:i:s');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.fgbarcode_dimensi', 
				'dt' => 9, 'field' => 'fgbarcode_dimensi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." as a
								LEFT JOIN td_finishgood_barcode as b ON b.fgbarcode_kd=a.fgbarcode_kd
								LEFT JOIN tm_finishgood_stokopname as c ON c.fgstopname_kd=a.fgstopname_kd
								LEFT JOIN tm_barang as d ON d.kd_barang=b.barang_kd";
		$data['where'] = "a.fgstopname_kd = '".$fgstopname_kd."' AND c.kd_gudang = '".$kd_gudang."'";
		
		return $data;
	}

	private function tbl_btn($id) {
		$btns = array();
		$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));
		
		$btn_group = group_btns($btns);

		return $btn_group;
	}


    public function buat_kode () {
		// FST190507000001
		$ident = 'DST';
		$identtgl = substr(date('Y'), -2).date('m').date('d');

		$this->db->select($this->p_key);
		$this->db->where('DATE(fgstopnamedetail_tglinput)', date('Y-m-d'));
		$this->db->order_by('fgstopnamedetail_tglinput', 'DESC');
		$query = $this->db->get($this->tbl_name);
		if ($query->num_rows() == 0){
			$data = $ident.$identtgl.'000001';
		}else {
			$lastkode = $query->result_array();
			$lastkode = max($lastkode);
			$subs_laskode = substr($lastkode[$this->p_key], -6);
			$nextnumber = $subs_laskode + 1;
			$data = $ident.$identtgl.str_pad($nextnumber,6,"0",STR_PAD_LEFT);
		}
		return $data;
	}

    public function insert_data ($data){
        $query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
    }
    
    public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_byId($id){
		$this->db->where($this->p_key, $id);
		$act = $this->db->get($this->tbl_name);
		return $act;
    }
    	
	public function get_byParam ($param=[]) {
		$act = $this->db->where($param)
					->get($this->tbl_name);
		return $act;
	}

	public function get_by_param_fgin ($param=[]) {
		$act = $this->db->select($this->tbl_name.'.*, td_finishgood_in.rakruangkolom_kd AS rakruangkolom_stok, td_finishgood_in.fgin_qty, td_finishgood_barcode.*, tm_barang.item_code')
					->from($this->tbl_name)
					->join('td_finishgood_in', $this->tbl_name.'.fgin_kd = td_finishgood_in.fgin_kd', 'left')
					->join('td_finishgood_barcode', 'td_finishgood_in.fgbarcode_kd = td_finishgood_barcode.fgbarcode_kd', 'left')
					->join('tm_barang', 'td_finishgood_barcode.barang_kd=tm_barang.kd_barang', 'left')
					->where($param)
					->get();
		return $act;
	}

	public function get_by_groupby_fg ($fgstopname_kd){
		$act = $this->db->select('fg_kd, SUM(fgstopnamedetail_qty) as sum_opname')
					->from($this->tbl_name)
					->where('fgstopname_kd', $fgstopname_kd)
					->group_by('fg_kd')
					->get();
		return $act;
	}

	public function get_fginkd_with_stockopnamekd($param_fgin=[], $param_fgopname=[] ){
		$act = $this->db->select('fgin_kd')
						->from($this->tbl_name)
						->where($param_fgin)
						->where($param_fgopname)
						->get();
		return $act;
	}
    
}