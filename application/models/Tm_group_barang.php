<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_group_barang extends CI_Model {
	private $tbl_name = 'tm_group_barang';
	private $p_key = 'kd_group_barang';

	public function get_dropdown() {
		$result = $this->get_all();
		$data[''] = '-- Pilih Product Group --';
		if (!empty($result)) :
			foreach ($result as $row) :
				$data[$row->kd_group_barang] = $row->nm_group;
			endforeach;
		endif;
		return $data;
	}

	public function get_all() {
		$this->db->from($this->tbl_name);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function get_name($kd_group_barang = '') {
		$this->db->select('nm_group')
			->from($this->tbl_name)
			->where($this->p_key, $kd_group_barang);
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			return $row->nm_group;
		else :
			return '';
		endif;
	}
}