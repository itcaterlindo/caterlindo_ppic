<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_purchase_suggestion extends CI_Model
{
    private $tbl_name = 'tm_purchase_suggestion';
    private $p_key = 'purchasesuggestion_kd';

    public function __construct()
    {
        parent::__construct();
    }

    public function ssp_table()
    {
        $data['table'] = $this->tbl_name;

        $data['primaryKey'] = $this->p_key;

        $data['columns'] = array(
            array(
                'db' => 'a.' . $this->p_key,
                'dt' => 1, 'field' => $this->p_key,
                'formatter' => function ($d) {

                    return $this->tbl_btn($d);
                }
            ),
            array(
                'db' => 'a.purchasesuggestion_kode',
                'dt' => 2, 'field' => 'purchasesuggestion_kode',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.purchasesuggestion_leadtime_prpo',
                'dt' => 3, 'field' => 'purchasesuggestion_leadtime_prpo',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.purchasesuggestion_note',
                'dt' => 4, 'field' => 'purchasesuggestion_note',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.purchasesuggestion_state',
                'dt' => 5, 'field' => 'purchasesuggestion_state',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean(ucwords($d));

                    return $d;
                }
            ),
            array(
                'db' => 'a.purchasesuggestion_tgledit',
                'dt' => 6, 'field' => 'purchasesuggestion_tgledit',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            // array(
            //     'db' => "GROUP_CONCAT(COALESCE(IF(c.tipe_customer='Ekspor',c.no_po, c.no_salesorder), '') SEPARATOR ', ') as sos",
            //     'dt' => 7, 'field' => 'sos',
            //     'formatter' => function ($d) {
            //         $d = $this->security->xss_clean($d);

            //         return $d;
            //     }
            // )
        );

        $data['sql_details'] = sql_connect();
        $data['joinQuery'] = "FROM " . $this->tbl_name . " as a";
        $data['where'] = "";

        return $data;
    }

    private function tbl_btn($id)
    {
        $delete_access = $this->session->delete_access;
        $btns = array();
        if ($delete_access == 1) {
            $btns[] = get_btn(array('title' => 'Lihat Item', 'icon' => 'search', 'onclick' => 'view_data(\'' . $id . '\')'));
            $btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\'' . $id . '\')'));
            $btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\'' . $id . '\')'));
        }
        $btn_group = group_btns($btns);

        return $btn_group;
    }

    public function create_code() {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

    public function generate_kode()
    {
        if (empty($code)) {
            $query = $this->db->select('MAX(purchasesuggestion_kode) as code')
                        ->from ($this->tbl_name)
                        ->get()->row();
            $code = $query->code;
        }
        $num = (int) substr($code, -6);
        $num = $num + 1;
        $code = 'PRS-'.str_pad($num, 6, '0', STR_PAD_LEFT);
        return $code;
    }

    public function insert_data($data)
    {
        $query = $this->db->insert($this->tbl_name, $data);
        return $query ? TRUE : FALSE;
    }

    public function delete_data($id)
    {
        $query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
        return $query ? TRUE : FALSE;
    }

    public function get_by_param($param = [])
    {
        $this->db->where($param);
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    public function update_data($aWhere = [], $data)
    {
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
        return $query ? TRUE : FALSE;
    }

    public function update_data_batch($key, $dataArray = [])
    {
        $query = $this->db->update_batch($this->tbl_name, $dataArray, $key);
        return $query ? TRUE : FALSE;
    }

}
