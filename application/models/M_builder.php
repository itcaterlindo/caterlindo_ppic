<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_builder extends CI_Model {
	public function getRow($table, $data = array(), $return) {
		$this->db->where($data);
		$this->db->from($table);
		$q_get = $this->db->get();
		$r_get = $q_get->{$return}();

		return ($r_get)?$r_get:FALSE;
	}

	public function buat_kode($nm_tabel, $conditions = array(), $panjang, $uuid) {
		$this->db->from($nm_tabel);
		foreach ($conditions as $key => $value) :
			if ($key != 'id_tgl' && $key != 'select' && $key != 'order_by' && $key != 'id_unik') :
				foreach ($value as $column => $param) :
					$this->db->{$key}($column, $param);
				endforeach;
			elseif ($key == 'select') :
				$this->db->{$key}($value.' AS IDMax');
			elseif ($key == 'order_by') :
				$this->db->{$key}($value);
			endif;
		endforeach;
		$q_kode = $this->db->get();
		$j_kode = $q_kode->num_rows();
		$r_kode = $q_kode->row_array();

		if (array_key_exists('id_tgl', $conditions)) :
			$hari_akhir = date('d');
			$bln_akhir = date('m');
			$thn_akhir = date('y');
			foreach($conditions['id_tgl'] as $key => $f_key) {
				if ($key == 'hari') {
					$hari_akhir = substr($r_kode['IDMax'], $f_key[0], $f_key[1]);
				} elseif ($key == 'bulan') {
					$bln_akhir = substr($r_kode['IDMax'], $f_key[0], $f_key[1]);
				} elseif ($key == 'tahun') {
					$thn_akhir = substr($r_kode['IDMax'], $f_key[0], $f_key[1]);
				}
			}
			$hari = date('d');
			$bln = date('m');
			$thn = date('y');
			if ($j_kode > 0 AND $hari_akhir == $hari AND $bln_akhir == $bln AND $thn_akhir == $thn) {
				$IdMax = $r_kode['IDMax'];
				$urutan = (int) substr($IdMax, -$panjang);
			} else {
				$urutan = 0;
			}
			
			$angka = $urutan + 1;
			
			$id_otomatis = $uuid.$hari.$bln.$thn.str_pad($angka, $panjang, '000', STR_PAD_LEFT);
		elseif (array_key_exists('id_unik', $conditions)) :
			$unik = substr($r_kode['IDMax'], $conditions['id_unik'][0], $conditions['id_unik'][1]);

			if ($j_kode > 0 AND $uuid == $unik) :
				$IdMax = $r_kode['IDMax'];
				$urutan = (int) substr($IdMax, -$panjang);
			else :
				$urutan = 0;
			endif;
			
			$angka = $urutan + 1;
			
			$id_otomatis = $uuid.str_pad($angka, $panjang, '000', STR_PAD_LEFT);
		else :			
			if ($j_kode > 0) {
				$IdMax = $r_kode['IDMax'];
				$urutan = (int) substr($IdMax, -$panjang);
			} else {
				$urutan = 0;
			}
			
			$angka = $urutan + 1;
			
			$id_otomatis = $uuid.str_pad($angka, $panjang, '000', STR_PAD_LEFT);
		endif;
		
		return $id_otomatis;
	}

	function create_pkey($tbl_name = '', $key_column = '', $uuid = '', $date_format = 'ymd', $inc_length = 4, $order_by = 'tgl_input DESC', $key_before = '', $inc = 0) {
	    $urutan = 0;
	    if ($inc < 1) :
	        $this->db->select('tgl_input, '.$key_column)
	            ->from($tbl_name)
	            ->where(['DATE(tgl_input)' => date('Y-m-d')])
	            ->order_by($order_by);
	        $query = $this->db->get();
	        $num_row = $query->num_rows();
	        if ($num_row > 0) :
	            $row = $query->row();
	            if (isset($row)) :
	                $code = $row->{$key_column};
	                $urutan = substr($code, -$inc_length);
	            endif;
	        endif;
	    else :
	        $urutan = substr($key_before, -$inc_length);
	    endif;
	    $angka = $urutan + 1;
	    $date = !empty($date_format)?date($date_format):'';
	    $pkey = $uuid.$date.str_pad($angka, $inc_length, '000', STR_PAD_LEFT);
	    return $pkey;
	}

	public function check_barcode($barcode) {
		$this->db->where(array('item_barcode' => $barcode));
		$this->db->from('tm_barang');
		$q_barang = $this->db->get();
		$j_barang = $q_barang->num_rows();
		
		return $j_barang > 0?FALSE:TRUE;
	}

	public function check_code($code, $table, $column) {
		$table = empty($table)?'tm_barang':$table;
		$column = empty($column)?array('item_code' => $code):$column;
		$this->db->where($column);
		$this->db->from($table);
		$q_barang = $this->db->get();
		$j_barang = $q_barang->num_rows();
		
		return $j_barang > 0?FALSE:TRUE;
	}

	public function insert_log($user, $activity_log) {
		$conds = array(
			'select' => 'activity_time, kd_log_activity',
			'order_by' => 'activity_time DESC, kd_log_activity DESC',
			'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
		);
		$kd_log_activity = $this->buat_kode('tb_log_activity', $conds, 11, 'LOG');

		$d_log = array(
			'kd_log_activity' => $kd_log_activity,
			'user' => $user,
			'activity_log' => $activity_log,
			'activity_time' => date('Y-m-d H:i:s'),
		);
		$input_log = $this->db->insert('tb_log_activity', $d_log);

		return $input_log?TRUE:FALSE;
	}

	public function create_product_folder($type){
		$type = isset($type)?$type:'all';
		$path = 'assets/admin_assets/dist/img/product_media/';
		if ($type == 'all') :
			$this->db->from('tm_barang');
			$q_barang = $this->db->get();
			$r_barang = $q_barang->result();
			foreach ($r_barang as $d_barang) :
				$kd_barang = $d_barang->kd_barang;
			endforeach;
		endif;
	}

	public function detail_customer($kd_customer){
		$this->db->select('a.*');
		$this->db->from('tm_customer a');
		$this->db->where(array('a.kd_customer' => $kd_customer));
		$query = $this->db->get();
		$num_rows = $query->num_rows();
		$result = $query->row();

		return $num_rows > 0?$result:FALSE;
	}

	public function detail_alamat($kd_customer, $method){
		$this->db->select('a.*');
		$this->db->from('td_customer_alamat a');
		$this->db->where(array('a.customer_kd' => $kd_customer))
			->order_by('tgl_input, kd_alamat_kirim');
		$query = $this->db->get();
		$result = $query->{$method}();

		return $result;
	}

	public function simpan_lokasi($nm_lokasi) {
		$this->db->like(array($column => $val));
		$this->db->where(array($column => $val));
	}

	function create_code_format($tbl_name, $code_for) {
		// Dapatkan format no quotation
		$this->db->from('tb_code_format');
		$this->db->where(array('code_for' => $code_for));
		$query = $this->db->get();
		$result = $query->row();
		$code_length = $result->code_length;
		$code_format = $result->code_format;
		$code_for = $result->code_for;
		$code_separator = $result->code_separator;
		$on_reset = $result->on_reset;
		$increment = $this->get_increment($tbl_name, 'tgl_input DESC', $on_reset);
		$reset = $this->get_reset($tbl_name, $on_reset);
		$no_quotation = code_maker($code_format, $code_length, '-', $code_separator, $increment, $reset);

		return $no_quotation;
	}

	public function get_increment($table, $order_by, $resetter) {
		$where = '';
		if ($resetter == 'year') :
			$select = 'YEAR(tgl_input) as thn_input';
			$where = array('YEAR(tgl_input)' => date('Y'));
		elseif ($resetter == 'month') :
			$select = 'YEAR(tgl_input) as thn_input, MONTH(tgl_input) as bln_input';
			$where = array('YEAR(tgl_input)' => date('Y'), 'MONTH(tgl_input)' => date('m'));
		elseif ($resetter == 'day') :
			$select = 'DATE(tgl_input) as hari_input';
			$where = array('DATE(tgl_input)' => date('Y-m-d'));
		endif;

		$this->db->from($table);
		$this->db->where($where);
		$this->db->order_by($order_by);
		$query = $this->db->get();
		$num = $query->num_rows();

		return $num;
	}

	public function get_increment_so($table, $order_by, $group_by, $resetter, $tipe = '') {
		$where = '';
		if ($resetter == 'year') :
			$select = 'YEAR(tgl_input) as thn_input';
			$where = array('YEAR(tgl_input)' => date('Y'), 'tipe_customer' => $tipe);
		elseif ($resetter == 'month') :
			$select = 'YEAR(tgl_input) as thn_input, MONTH(tgl_input) as bln_input';
			$where = array('YEAR(tgl_input)' => date('Y'), 'MONTH(tgl_input)' => date('m'), 'tipe_customer' => $tipe);
		elseif ($resetter == 'day') :
			$select = 'DATE(tgl_input) as hari_input';
			$where = array('DATE(tgl_input)' => date('Y-m-d'), 'tipe_customer' => $tipe);
		endif;

		$this->db->from($table);
		$this->db->where($where);
		$this->db->order_by($order_by);
		if (!empty($group_by)) :
			$this->db->group_by($group_by);
		endif;
		$query = $this->db->get();
		$num = $query->num_rows();

		return $num;
	}

	public function get_reset($table, $resetter){
		$where = '';
		if ($resetter == 'year') :
			$select = 'YEAR(tgl_input) as thn_input';
			$where = array('YEAR(tgl_input)' => date('Y'));
		elseif ($resetter == 'month') :
			$select = 'YEAR(tgl_input) as thn_input, MONTH(tgl_input) as bln_input';
			$where = array('YEAR(tgl_input)' => date('Y'), 'MONTH(tgl_input)' => date('m'));
		elseif ($resetter == 'day') :
			$select = 'DATE(tgl_input) as hari_input';
			$where = array('DATE(tgl_input)' => date('Y-m-d'));
		endif;

		// Cek dari variabel tabel, jika tidak ada data maka gunakan tabel code format untuk parameter reset
		$this->db->select($select);
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		$num = $query->num_rows();
		$result = $query->row();
		if ($num < 1) :
			$this->db->select($select);
			$this->db->from('tb_code_format');
			$query = $this->db->get();
			$result = $query->row();
		endif;
		if ($resetter == 'year') :
			$thn = $result->thn_input;
			$tgl = $thn;
			$compare = date('Y');
		elseif ($resetter == 'month') :
			$thn = $result->thn_input;
			$bln = $result->bln_input;
			$tgl = $thn.'-'.$bln;
			$compare = date('Y-m');
		elseif ($resetter == 'day') :
			$tgl = $result->hari_input;
			$compare = date('Y-m-d');
		endif;

		return strtotime($tgl) == strtotime($compare)?'0':'1';
	}

	function write_log($stat, $var, $data = array()){
		$nm_kolom = '';
		$no = 0;
		$jml = count($data);
		foreach ($data as $key => $val) :
			$no++;
			$koma = $no == $jml?'':',';
			$nm_kolom .= ' '.$key.' = '.$val.$koma;
		endforeach;
		$this->insert_log($this->session->kd_admin, $this->session->username.' '.$stat.' '.$var.' dengan'.$nm_kolom);
	}

	public function create_batch_log_code($tambah = '1') {
		$this->db->select('kd_log_activity AS p_key')
			->from('tb_log_activity')
			->where(array('DATE(activity_time)' => date('Y-m-d')))
			->order_by('activity_time DESC, kd_log_activity DESC');
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			$primary_key = $row->p_key;
			$urutan = substr($primary_key, -11);
		else :
			$urutan = 0;
		endif;
		$angka = $urutan + $tambah;
		$code = 'LOG'.date('dmy').str_pad($angka, 11, '000', STR_PAD_LEFT);
		return $code;
	}

	function write_batch_log($stat = '', $var = '', $data = '') {
		if (is_array($data)) :
			$jml_baris = count($data);
			for ($i = 0; $i < $jml_baris; $i++) :
				$no_baris = 1 + $i;
				$nm_kolom = '';
				$no = 0;
				$jml = count($data[$i]);
				foreach ($data[$i] as $key => $val) :
					$no++;
					$koma = $no == $jml?'':',';
					$nm_kolom .= ' '.$key.' = '.$val.$koma;
				endforeach;
				$log_batch[] = array('kd_log_activity' => $this->create_batch_log_code($no_baris), 'user' => $this->session->kd_admin, 'activity_log' => $this->session->username.' '.$stat.' '.$var.' dengan'.$nm_kolom, 'activity_time' => date('Y-m-d H:i:s'));
			endfor;
			$act = $this->db->insert_batch('tb_log_activity', $log_batch);
		endif;
	}
}