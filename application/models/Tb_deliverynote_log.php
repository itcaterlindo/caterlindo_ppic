<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_deliverynote_log extends CI_Model {
	private $tbl_name = 'tb_deliverynote_log';
	private $p_key = 'dnlog_kd';

    // public function ssp_table($dn_kd) {
	// 	$data['table'] = $this->tbl_name;

	// 	$data['primaryKey'] = $this->p_key;

	// 	$data['columns'] = array (
	// 		array( 'db' => 'a.'.$this->p_key,
	// 			'dt' => 1, 'field' => $this->p_key,
	// 			'formatter' => function($d, $row){

	// 				return $this->tbl_btn($d);
	// 			}),
	// 		array( 'db' => 'b.woitem_no_wo',
	// 			'dt' => 2, 'field' => 'woitem_no_wo',
	// 			'formatter' => function($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'b.woitem_itemcode',
	// 			'dt' => 3, 'field' => 'woitem_itemcode',
	// 			'formatter' => function($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'a.dndetail_qty',
	// 			'dt' => 4, 'field' => 'dndetail_qty',
	// 			'formatter' => function($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'a.dndetail_qtyreceived',
	// 			'dt' => 5, 'field' => 'dndetail_qtyreceived',
	// 			'formatter' => function($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'a.dndetail_remark',
	// 			'dt' => 6, 'field' => 'dndetail_remark',
	// 			'formatter' => function($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
    //             }),
	// 		array( 'db' => 'a.dndetail_tglinput',
	// 			'dt' => 7, 'field' => 'dndetail_tglinput',
	// 			'formatter' => function($d){
	// 				return $d;
    //             }),
            
	// 	);

	// 	$data['sql_details'] = sql_connect();
	// 	$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
    //                         LEFT JOIN td_workorder_item as b ON a.woitem_kd=b.woitem_kd";
	// 	$data['where'] = "";

	// 	return $data;
	// }

	// private function tbl_btn($id) {
	// 	$btns = array();
	// 	$btns[] = get_btn(array('title' => 'Lihat Item', 'icon' => 'search', 'onclick' => 'edit_data(\''.$id.'\')'));
	// 	$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
	// 	$btns[] = get_btn(array('title' => 'Cetak Item', 'icon' => 'print', 'onclick' => 'cetak_data(\''.$id.'\')'));
	// 	$btns[] = get_btn_divider();
	// 	$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));
		
	// 	$btn_group = group_btns($btns);

	// 	return $btn_group;
    // }
    
	public function create_code() {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

	public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}

	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }
    
    public function insert_batch_data ($data=[]){
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
    
    }

    public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

    public function get_where_in ($where, $in = []){
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}

	public function get_by_param_detail ($param=[]) {
		$query = $this->db->select($this->tbl_name.'.*, tb_deliverynote_state.dnstate_nama, tb_admin.nm_admin')
                    ->from($this->tbl_name)
                    ->join('tb_deliverynote_state', $this->tbl_name.'.dnstate_kd=tb_deliverynote_state.dnstate_kd', 'left')
                    ->join('tb_admin', $this->tbl_name.'.admin_kd=tb_admin.kd_admin', 'left')
					->where($param)
					->order_by($this->tbl_name.'.dnlog_tglinput', 'desc')
					->get();
		return $query;
	}
}