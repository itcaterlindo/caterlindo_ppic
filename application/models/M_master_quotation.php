<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class M_master_quotation extends CI_Model {
	private $tbl_name = 'tm_quotation';

	public function update_price($var, $master, $val) {
		if ($var == 'ongkir') :
			$data = array('jml_ongkir' => $val);
		elseif ($var == 'install') :
			$data = array('jml_install' => $val);
		endif;
		$where = array('kd_mquotation' => $master);
		$query = $this->db->update($this->tbl_name, $data, $where);

		$value = $this->detail_price($var, $master);

		return $value;
	}

	public function detail_price($var, $master) {
		if ($var == 'ongkir') :
			$this->db->select('jml_ongkir AS jml_biaya');
		elseif ($var == 'install') :
			$this->db->select('jml_install AS jml_biaya');
		endif;
		$this->db->from($this->tbl_name);
		$this->db->where(array('kd_mquotation' => $master));
		$query = $this->db->get();
		$num = $query->num_rows();
		$result = $query->row();
		$value = $result->jml_biaya;

		return $value;
	}

	public function sum_detail($master) {
		$this->db->select('SUM(total_nilai) AS tot');
		$this->db->from('td_quotation_harga');
		$this->db->where(array('mquotation_kd' => $master));
		$query = $this->db->get();
		$result = $query->row();
		$sum = $result->tot;
		return $sum;
	}

	public function sum_item($master) {
		$this->db->select('SUM(total_harga) AS tot');
		$this->db->from('td_quotation_item');
		$this->db->where(array('mquotation_kd' => $master));
		$query = $this->db->get();
		$result = $query->row();
		$sum = $result->tot;
		return $sum;
	}

	public function total_potongan($master) {
		$this->db->from('td_quotation_harga');
		$this->db->where(array('mquotation_kd' => $master));
		$query = $this->db->get();
		$result = $query->result();

		return $result;
	}

	public function check_val($act, $value, $master) {
		$this->db->from($this->tbl_name);
		if ($act == 'submit_ongkir') :
			$this->db->like(array('jml_ongkir' => $value));
		elseif ($act == 'submit_install') :
			$this->db->like(array('jml_install' => $value));
		endif;
		$this->db->where(array('kd_mquotation' => $master));
		$query = $this->db->get();
		$num = $query->num_rows();
		return $num < 1?TRUE:FALSE;
	}

	public function insert_ppn($data = array(), $where = array()) {
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s'));
		$submit = array_merge($data, $data_adm);
		$aksi = $this->db->update($this->tbl_name, $submit, $where);
		return $aksi?TRUE:FALSE;
	}

	public function get_row($kd_mquotation) {
		$this->db->select('kd_mquotation, salesperson_kd, email_sales, mobile_sales, no_quotation, tgl_quotation, customer_kd, tipe_customer, tipe_harga, alamat_kirim_kd, status_quotation, nm_kolom_ppn, jml_ppn, jml_ongkir, jml_install');
		$this->db->from($this->tbl_name);
		$this->db->where(array('kd_mquotation' => $kd_mquotation));
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}

	public function ubah_stat($status, $kd_mquotation) {
		$data = array(
			'status_quotation' => $status,
		);
		$where = array('kd_mquotation' => $kd_mquotation);
		$aksi = $this->db->update($this->tbl_name, $data, $where);
		return $aksi?TRUE:FALSE;
	}

	public function get_quo_bycustomer($kd_customer) {
		$this->db->select('kd_mquotation, no_quotation');
		$this->db->from($this->tbl_name);
		$this->db->where(array('customer_kd' => $kd_customer, 'status_quotation' => 'pending'));
		$query = $this->db->get();
		$num = $query->num_rows();
		$dropdown = '';
		if ($num > 0) :
			$result = $query->result();
			foreach ($result as $row) :
				$kd_mquotation = $row->kd_mquotation;
				$no_quotation = $row->no_quotation;
				$dropdown .= '<option value=\''.$kd_mquotation.'\'>'.$no_quotation.'</option>';
			endforeach;
		endif;
		return $dropdown;
	}
}