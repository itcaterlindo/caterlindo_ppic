<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_bagian extends CI_Model
{
	private $tbl_name = 'tb_bagian';
	private $p_key = 'bagian_kd';

	public function ssp_table()
	{
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array(
				'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function ($d) {

					return $this->tbl_btn($d);
				}
			),
			array(
				'db' => $this->p_key,
				'dt' => 2, 'field' => $this->p_key,
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'bagian_nama',
				'dt' => 3, 'field' => 'bagian_nama',
				'formatter' => function ($d, $row) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'bagian_lokasi',
				'dt' => 4, 'field' => 'bagian_lokasi',
				'formatter' => function ($d, $row) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'pic',
				'dt' => 5, 'field' => 'pic',
				'formatter' => function ($d, $row) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'bagian_tglinput',
				'dt' => 6, 'field' => 'bagian_tglinput',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM " . $this->tbl_name;
		$data['where'] = "";

		return $data;
	}

	private function tbl_btn($id)
	{
		$btns = array();
		if (cek_permission('PARTBAGIAN_UPDATE')) {
			$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\'' . $id . '\')'));
		}
		if (cek_permission('PARTBAGIAN_DELETE')) {
			$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\'' . $id . '\')'));
		}
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function insert_data($data)
	{
		$query = $this->db->insert($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_data($id)
	{
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		return $query ? TRUE : FALSE;
	}

	public function get_by_param($param = [])
	{
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all()
	{
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data($aWhere = [], $data)
	{
		$query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query ? TRUE : FALSE;
	}

	public function get_by_param_in ($param, $params=[]) {
		$this->db->where_in($param, $params)->order_by('bagian_lokasi, bagian_kd');
		$act = $this->db->get($this->tbl_name);
		return $act;
	}
}
