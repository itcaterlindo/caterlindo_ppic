<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_workorder_item_so extends CI_Model
{
	private $tbl_name = 'td_workorder_item_so';
	private $p_key = 'woitemso_kd';

	public function buat_kode()
	{
		$query = $this->db->select('MAX(' . $this->p_key . ') as maxID')
			->get($this->tbl_name)
			->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

	public function insert_data($data)
	{
		$query = $this->db->insert($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_data($id)
	{
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		return $query ? TRUE : FALSE;
	}

	public function get_by_param($param = [])
	{
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all()
	{
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data($aWhere = [], $data)
	{
		$query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query ? TRUE : FALSE;
	}

	public function delete_by_param($param = [])
	{
		$query = $this->db->delete($this->tbl_name, $param);
		return $query ? TRUE : FALSE;
	}

	public function get_where_in($where, $in = [])
	{
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}

	public function insert_batch_data($data)
	{
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function get_detail_part_in($param, $params = [])
	{
		$query = $this->db->select()
			->from($this->tbl_name)
			->join('tm_bom', $this->tbl_name . '.bom_kd=tm_bom.bom_kd', 'left')
			->join('td_bom_detail', 'tm_bom.bom_kd=td_bom_detail.bom_kd', 'left')
			->join('tm_part_main', 'td_bom_detail.partmain_kd=tm_part_main.partmain_kd', 'left')
			->join('tb_part_lastversion', 'tm_part_main.partmain_kd=tb_part_lastversion.partmain_kd', 'left')
			->join('td_part_jenis', 'tm_part_main.partjenis_kd=td_part_jenis.partjenis_kd', 'left')
			->where_in($param, $params)
			->get();
		return $query;
	}

	public function get_detail_part($param)
	{
		$query = $this->db->select("*, td_workorder_item_so.kd_barang AS kd_barang")
			->from($this->tbl_name)
			->join('tm_bom', $this->tbl_name . '.bom_kd=tm_bom.bom_kd', 'left')
			->join('td_bom_detail', 'tm_bom.bom_kd=td_bom_detail.bom_kd', 'left')
			->join('tm_part_main', 'td_bom_detail.partmain_kd=tm_part_main.partmain_kd', 'left')
			->join('tb_part_lastversion', 'tm_part_main.partmain_kd=tb_part_lastversion.partmain_kd', 'left')
			->join('td_part_jenis', 'tm_part_main.partjenis_kd=td_part_jenis.partjenis_kd', 'left')
			->where($param)
			->get();
		return $query;
	}

	public function get_detail_bom_project($param)
	{
		$query = $this->db->select($this->tbl_name . '.*, tm_bom.bom_jenis, tm_project.project_kd, tm_barang.item_code')
			->from($this->tbl_name)
			->join('tm_bom', $this->tbl_name . '.bom_kd=tm_bom.bom_kd', 'left')
			->join('tm_project', 'tm_bom.project_kd=tm_project.project_kd', 'left')
			->join('tm_barang', 'tm_bom.kd_barang=tm_barang.kd_barang', 'left')
			->where($param)
			->get();
		return $query;
	}

	public function insertBatch_GroupByItemSO($wo_kd): bool
	{
		$this->load->model(['tm_workorder', 'td_workorder_item_so_detail']);

		$resp = true;
		$isWithSO = $this->td_workorder_item_so_detail->get_by_param(['wo_kd' => $wo_kd])->num_rows();
		/** Jika dengan SO */
		if (!empty($isWithSO)) {
			$sodetailSTDs = $this->db->query(
				"SELECT td_workorder_item_so_detail.woitemsodetail_kd, tm_barang.kd_barang, td_workorder_item_so_detail.woitemsodetail_itemcode,
				td_workorder_item_so_detail.woitemsodetail_itemdeskripsi, td_workorder_item_so_detail.woitemsodetail_itemdimensi,
				td_workorder_item_so_detail.woitemsodetail_qty,
				td_workorder_item_so_detail.woitemsodetail_status, td_workorder_item_so_detail.woitemsodetail_tipe,
				tm_bom.bom_kd
			  FROM td_workorder_item_so_detail
			  LEFT JOIN tm_barang ON tm_barang.item_code = td_workorder_item_so_detail.woitemsodetail_itemcode
				LEFT JOIN tm_bom ON tm_bom.kd_barang = tm_barang.kd_barang
			  WHERE td_workorder_item_so_detail.woitemsodetail_status = 'std'
				  AND td_workorder_item_so_detail.wo_kd = '$wo_kd'"
			)->result_array();

			$dataWoitemsos = [];
			$arrayStds = [];
			// STD
			foreach ($sodetailSTDs as $sodetailSTD) {
				$arrayStds[$sodetailSTD['kd_barang']][] = $sodetailSTD;
			}

			$woitemso_kd = $this->buat_kode();
			foreach ($arrayStds as $kd_barang => $elements) {
				$sum_woitemsodetail_qty = 0;
				foreach ($elements as $element) {
					$sum_woitemsodetail_qty += $element['woitemsodetail_qty'];
				}
				$woitemso_prosesstatus = 'editing';
				if (empty($elements[0]['bom_kd'])) {
					$woitemso_prosesstatus = 'null_bom';
				}
				$dataWoitemsos[] = [
					'woitemso_kd' => $woitemso_kd,
					'wo_kd' => $wo_kd,
					'kd_barang' => $kd_barang,
					'woitemso_itemcode' => $elements[0]['woitemsodetail_itemcode'],
					'woitemso_itemdeskripsi' => $elements[0]['woitemsodetail_itemdeskripsi'],
					'woitemso_itemdimensi' => $elements[0]['woitemsodetail_itemdimensi'],
					'woitemso_qtyso' => $sum_woitemsodetail_qty,
					'woitemso_qty' => 0,
					'woitemso_status' => $elements[0]['woitemsodetail_status'],
					'bom_kd' => $elements[0]['bom_kd'],
					'woitemso_nopj' => null,
					'woitemso_tipe' => 'so',
					'woitemso_prosesstatus' => $woitemso_prosesstatus,
					'woitemso_tglselesai' => null,
					'woitemso_tglinput' => date('Y-m-d H:i:s'),
					'woitemso_tgledit' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				];

				// Update batch detail woitemsodetail
				foreach ($elements as $element) {
					$arrayUpdateWoitemsoDetails[] = [
						'woitemsodetail_kd' => $element['woitemsodetail_kd'],
						'woitemso_kd' => $woitemso_kd,
					];
				}
				$woitemso_kd++;
			}

			// CUSTOM
			$sodetailCUSTOMs = $this->db->query(
				"SELECT td_workorder_item_so_detail.woitemsodetail_kd, td_workorder_item_so_detail.kd_barang, td_workorder_item_so_detail.woitemsodetail_itemcode,
					td_workorder_item_so_detail.woitemsodetail_itemdeskripsi, td_workorder_item_so_detail.woitemsodetail_itemdimensi,
					td_workorder_item_so_detail.woitemsodetail_qty,
					td_workorder_item_so_detail.woitemsodetail_status, td_workorder_item_so_detail.woitemsodetail_tipe,
					tm_bom.bom_kd
				  FROM td_workorder_item_so_detail
				  LEFT JOIN tm_bom ON tm_bom.kd_barang = td_workorder_item_so_detail.kd_barang AND tm_bom.bom_jenis = 'custom'
				  WHERE td_workorder_item_so_detail.woitemsodetail_status = 'custom'
					  AND td_workorder_item_so_detail.wo_kd = '$wo_kd'
				  GROUP BY td_workorder_item_so_detail.kd_barang"
			)->result_array();

			foreach ($sodetailCUSTOMs as $sodetailCUSTOM) {
				$woitemso_prosesstatus = 'editing';
				if (empty($sodetailCUSTOM['bom_kd'])) {
					$woitemso_prosesstatus = 'null_bom';
				}
				$dataWoitemsos[] = [
					'woitemso_kd' => $woitemso_kd,
					'wo_kd' => $wo_kd,
					'kd_barang' => $sodetailCUSTOM['kd_barang'],
					'woitemso_itemcode' => $sodetailCUSTOM['woitemsodetail_itemcode'],
					'woitemso_itemdeskripsi' => $sodetailCUSTOM['woitemsodetail_itemdeskripsi'],
					'woitemso_itemdimensi' => $sodetailCUSTOM['woitemsodetail_itemdimensi'],
					'woitemso_qtyso' => $sodetailCUSTOM['woitemsodetail_qty'],
					'woitemso_qty' => 0,
					'woitemso_status' => $sodetailCUSTOM['woitemsodetail_status'],
					'bom_kd' => $sodetailCUSTOM['bom_kd'],
					'woitemso_nopj' => null,
					'woitemso_tipe' => 'so',
					'woitemso_prosesstatus' => $woitemso_prosesstatus,
					'woitemso_tglselesai' => null,
					'woitemso_tglinput' => date('Y-m-d H:i:s'),
					'woitemso_tgledit' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				];

				// Update batch detail woitemsodetail
				$arrayUpdateWoitemsoDetails[] = [
					'woitemsodetail_kd' => $sodetailCUSTOM['woitemsodetail_kd'],
					'woitemso_kd' => $woitemso_kd
				];
				$woitemso_kd++;
			}

			$this->db->trans_start();
			/** Update woitemsodetail prosesstatus */
			$act = $this->insert_batch_data($dataWoitemsos);
			if ($act) {
				$this->db->update_batch('td_workorder_item_so_detail', $arrayUpdateWoitemsoDetails, 'woitemsodetail_kd');
				$this->tm_workorder->update_data(['wo_kd' => $wo_kd], ['wo_tglstep0' => date('Y-m-d H:i:s')]);
			}
			$this->db->trans_complete();

			$resp = $this->db->trans_status();
		}

		return $resp;
	}

	/** Action yang dilakukan :
	 * 1. proses td_workorder_item_so_detail menjadi workorder dengan nomor wo ini
	 * 2. proses td_workorder_item_so menjadi workorder dengan ketentuan 
	 * 		a. item dengan status editing dan memiliki baik nilai 0 atau > 0
	 * 		b. item customyang statusnya editing 
	 */
	public function updateBatch_prosesStatus($wo_kd, $woitemso_prosestatus = 'editing'): bool
	{
		$woitemsoProcesseds = $this->td_workorder_item_so->get_by_param(['wo_kd' => $wo_kd, 'woitemso_prosesstatus' => 'editing'])->result_array();
		$arrayWoitemsos = [];
		$arrayWoitemsoDetails = [];
		foreach ($woitemsoProcesseds as $data) {
			$arrayWoitemsos[] = [
				'woitemso_kd' => $data['woitemso_kd'],
				'woitemso_prosesstatus' => $woitemso_prosestatus,
				'woitemso_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			];
			$arrayWoitemsoDetails[] = [
				'woitemso_kd' => $data['woitemso_kd'],
				'woitemsodetail_prosesstatus' => $woitemso_prosestatus,
				'woitemsodetail_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			];
		}
		$this->db->trans_start();
		if (!empty($arrayWoitemsos)) {
			$this->db->update_batch($this->tbl_name, $arrayWoitemsos, 'woitemso_kd');
		}
		if (!empty($arrayWoitemsoDetails)) {
			$this->db->update_batch('td_workorder_item_so_detail', $arrayWoitemsoDetails, 'woitemso_kd');
		}
		$this->db->trans_complete();
		return $this->db->trans_status();
	}

	public function updateBatch_generateQtyWO($wo_kd): bool
	{
		define("DEFAULT_FG_GD", "MGD071218001");
		$this->load->model(['tb_finishgood_user']);

		/** Cek Lokasi User */
		$userfg = $this->tb_finishgood_user->get_by_param(array('kd_tipe_admin' => $this->session->userdata('tipe_admin_kd')))->row_array();
		$set_gudang = $userfg['kd_gudang'];
		$set_gudang = !empty($set_gudang) ? $set_gudang : DEFAULT_FG_GD;

		/** Get data barang per wo:
		 * - stok FG
		 * - stok WIP
		 * - stok max
		 * - stok min
		 */
		$itemQtys = $this->db->select('td_workorder_item_so.woitemso_kd, td_workorder_item_so.kd_barang, 
                tm_wip.wip_qty, tm_barang.barang_stock_max, tm_barang.barang_stock_min, tm_finishgood.fg_qty,
                SUM(td_workorder_item_so.woitemso_qty) as sum_woitemso_qty, SUM(td_workorder_item_so.woitemso_qtyso) as sum_woitemso_qtyso')
			->join('tm_wip', 'tm_wip.kd_barang=td_workorder_item_so.kd_barang', 'left')
			->join('tm_barang', 'tm_barang.kd_barang=td_workorder_item_so.kd_barang', 'left')
			->join('tm_finishgood', 'tm_finishgood.barang_kd=td_workorder_item_so.kd_barang', 'left')
			->where('td_workorder_item_so.wo_kd', $wo_kd)
			->where('td_workorder_item_so.woitemso_status', 'std')
			->where('tm_finishgood.kd_gudang', $set_gudang)
			->group_by('td_workorder_item_so.kd_barang')
			->get('td_workorder_item_so')->result_array();

		$kd_barangs = array_column($itemQtys, 'kd_barang');

		/** Untuk get data barang yang sudah masuk planning */
		$itemPlanneds = $this->db->select('td_workorder_item_so.kd_barang, SUM(td_workorder_item_so.woitemso_qtyso) as sum_woitemso_qtyso_planned')
			->join('tm_workorder', 'tm_workorder.wo_kd=tb_relasi_sowo.wo_kd', 'left')
			->join('td_workorder_item_so_detail', 'td_workorder_item_so_detail.kd_msalesorder=tb_relasi_sowo.kd_msalesorder', 'left')
			->join('td_workorder_item_so', 'td_workorder_item_so.woitemso_kd=td_workorder_item_so_detail.woitemso_kd', 'left')
			->where_in('td_workorder_item_so.kd_barang', $kd_barangs)
			->where('tm_workorder.wo_status', 'process_wo')
			->where('td_workorder_item_so.woitemso_prosesstatus', 'workorder')
			->group_by('td_workorder_item_so.kd_barang')
			->get('tb_relasi_sowo')->result_array();

		$arrayPlanneds = [];
		foreach ($itemPlanneds as $itemPlanned) {
			$arrayPlanneds[$itemPlanned['kd_barang']] = (int)$itemPlanned['sum_woitemso_qtyso_planned'];
		}

		$arrayItemQtys = [];
		$qtyRecomendation = 0;
		foreach ($itemQtys as $itemQty) {
			$sum_woitemso_qtyso_planned = isset($arrayPlanneds[$itemQty['kd_barang']]) ? $arrayPlanneds[$itemQty['kd_barang']] : 0;
			/** Hitung qty rekomenadi dari :
			 * (stok_fg + stok_wip) - qty_planned - qty_planning + x = qty_max
			 *   	qty_planned = Stok barang yang woitemso_prosesstatus = workorder
			 * 
			 * fg_qty => (int)$itemQty[fg_qty],
			 *	wip_qty => (int)$itemQty[wip_qty],
			 *	sum_woitemso_qtyso_planned => (int) $sum_woitemso_qtyso_planned,
			 *	sum_woitemso_qtyso_planning => (int) $itemQty[sum_woitemso_qtyso],
			 *	barang_stock_max => (int) $itemQty[barang_stock_max],
			 *	barang_stock_min => (int) $itemQty[barang_stock_min],
			 */
			$qtyRecomendation = (int) $itemQty['barang_stock_max'] - ($itemQty['fg_qty'] + $itemQty['wip_qty'] - $sum_woitemso_qtyso_planned - $itemQty['sum_woitemso_qtyso']);
			if ($qtyRecomendation < 0) {
				$qtyRecomendation = 0;
			}
			$arrayItemQtys[$itemQty['kd_barang']] = [
				'woitemso_kd' => (int)$itemQty['woitemso_kd'],
				'woitemso_qty' => $qtyRecomendation,
			];
		}
		$this->db->trans_start();
		if (!empty($arrayItemQtys)) {
			$this->db->update_batch($this->tbl_name, $arrayItemQtys, 'woitemso_kd');
		}
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	public function updateBatch_generateQtyWO_2($wo_kd, $stts)
	{
		define("DEFAULT_FG_GD", "MGD071218001");
		$this->load->model(['tb_finishgood_user']);

		/** Cek Lokasi User */
		$userfg = $this->tb_finishgood_user->get_by_param(array('kd_tipe_admin' => $this->session->userdata('tipe_admin_kd')))->row_array();
		$set_gudang = $userfg['kd_gudang'];
		$set_gudang = !empty($set_gudang) ? $set_gudang : DEFAULT_FG_GD;

		/** Get data barang per wo:
		 * - stok FG
		 * - stok WIP
		 * - stok max
		 * - stok min
		 */
		$itemQtys = $this->db->select('td_workorder_item_so.woitemso_kd, td_workorder_item_so.kd_barang, 
                tm_wip.wip_qty, tm_barang.barang_stock_max, tm_barang.barang_stock_min, tm_finishgood.fg_qty,
                SUM(td_workorder_item_so.woitemso_qty) as sum_woitemso_qty, SUM(td_workorder_item_so.woitemso_qtyso) as sum_woitemso_qtyso')
			->join('tm_wip', 'tm_wip.kd_barang=td_workorder_item_so.kd_barang', 'left')
			->join('tm_barang', 'tm_barang.kd_barang=td_workorder_item_so.kd_barang', 'left')
			->join('tm_finishgood', 'tm_finishgood.barang_kd=td_workorder_item_so.kd_barang', 'left')
			->where('td_workorder_item_so.wo_kd', $wo_kd)
			->where('td_workorder_item_so.woitemso_status', 'std')
			->where('tm_finishgood.kd_gudang', $set_gudang)
			->group_by('td_workorder_item_so.kd_barang')
			->get('td_workorder_item_so')->result_array();

		$kd_barangs = array_column($itemQtys, 'kd_barang');



		// hanya percobaan sih gais
		$qStokPlanned = $this->db->select('tm_salesorder.kd_msalesorder, tm_salesorder.no_salesorder, tm_salesorder.tipe_customer, tm_salesorder.no_po, 
				td_workorder_item_so_detail.kd_barang, SUM(td_workorder_item_so_detail.woitemsodetail_qty) as qtyplanned, tm_workorder.wo_noterbit')
			->join('tm_salesorder', 'tm_salesorder.kd_msalesorder=tb_relasi_sowo.kd_msalesorder', 'left')
			->join('td_workorder_item_so_detail', 'tb_relasi_sowo.relasisowo_kd = td_workorder_item_so_detail.relasisowo_kd', 'left')
			->join('tm_workorder', 'td_workorder_item_so_detail.wo_kd=tm_workorder.wo_kd', 'left')
			->where([
				'tm_salesorder.status_so' => 'process_wo',
				'td_workorder_item_so_detail.woitemsodetail_prosesstatus' => 'workorder',
				'tm_workorder.wo_status' => 'process_wo',
				'td_workorder_item_so_detail.woitemsodetail_status' => 'std'
			])
			->group_by('td_workorder_item_so_detail.kd_msalesorder, 
				td_workorder_item_so_detail.kd_barang')
			->order_by('tm_salesorder.tgl_kirim')
			->get('tb_relasi_sowo')
			->result_array();
		/** Qty barang keluar dari yg planned */
	$qStokPlannedOut = $this->db->select('tb_relasi_sowo.kd_msalesorder, tm_finishgood.barang_kd, SUM(td_finishgood_out.fgout_qty) as sum_out')
		->join('tm_salesorder', 'tb_relasi_sowo.kd_msalesorder=tm_salesorder.kd_msalesorder', 'left')
		->join('td_finishgood_out', 'td_finishgood_out.kd_msalesorder=tm_salesorder.kd_msalesorder', 'left')
		->join('td_finishgood_in', 'td_finishgood_in.fgin_kd=td_finishgood_out.fgin_kd', 'left')
		->join('tm_finishgood', 'tm_finishgood.fg_kd=td_finishgood_in.fg_kd', 'left')
		->group_by('tb_relasi_sowo.kd_msalesorder, tm_finishgood.barang_kd')
		->get('tb_relasi_sowo')
		->result_array();
	$stokPlanned = [];
	foreach ($qStokPlanned as $ePlanned) {
		$qtyplanned = $ePlanned['qtyplanned'];
		foreach ($qStokPlannedOut as $eStokPlannedOut) {
			if ($eStokPlannedOut['kd_msalesorder'] == $ePlanned['kd_msalesorder'] && $eStokPlannedOut['barang_kd'] == $ePlanned['kd_barang']) {
				$qtyplanned = $qtyplanned - $eStokPlannedOut['sum_out'];
				if ($qtyplanned < 0) {
					$qtyplanned = 0;
				}
			}
		}
		$stokPlanned[] = [
			'kd_msalesorder' => $ePlanned['kd_msalesorder'],
			'no_salesorder' => $ePlanned['no_salesorder'],
			'tipe_customer' => $ePlanned['tipe_customer'],
			'no_po' => $ePlanned['no_po'],
			'kd_barang' => $ePlanned['kd_barang'],
			'wo_noterbit' => $ePlanned['wo_noterbit'],
			'qtyplanned' => $qtyplanned,
		];
	}

	$groupedAndSummed = array_reduce($stokPlanned, function($result, $object) {
		$category = $object['kd_barang'];
		if (!isset($result[$category])) {
			$result[$category] = [$category => 0];
		}
		$result[$category][$category] += $object['qtyplanned'];
		return $result;
	}, []);




		/** Untuk get data barang yang sudah masuk planning */
		// $itemPlanneds = $this->db->select('td_workorder_item_so.kd_barang, SUM(td_workorder_item_so.woitemso_qtyso) as sum_woitemso_qtyso_planned')
		// 	->join('tm_workorder', 'tm_workorder.wo_kd=tb_relasi_sowo.wo_kd', 'left')
		// 	->join('td_workorder_item_so_detail', 'td_workorder_item_so_detail.kd_msalesorder=tb_relasi_sowo.kd_msalesorder', 'left')
		// 	->join('td_workorder_item_so', 'td_workorder_item_so.woitemso_kd=td_workorder_item_so_detail.woitemso_kd', 'left')
		// 	->where_in('td_workorder_item_so.kd_barang', $kd_barangs)
		// 	->where('tm_workorder.wo_status', 'process_wo')
		// 	->where('td_workorder_item_so.woitemso_prosesstatus', 'workorder')
		// 	->group_by('td_workorder_item_so.kd_barang')
		// 	->get('tb_relasi_sowo')->result_array();

		// $arrayPlanneds = [];
		// foreach ($itemPlanneds as $itemPlanned) { 
		// 	$arrayPlanneds[$itemPlanned['kd_barang']] = (int)$itemPlanned['sum_woitemso_qtyso_planned'];
		// }

		$arrayItemQtys = [];
		$qtyRecomendation = 0;
		foreach ($itemQtys as $itemQty) {
			$sum_woitemso_qtyso_planned = isset($groupedAndSummed[$itemQty['kd_barang']][$itemQty['kd_barang']]) ? $groupedAndSummed[$itemQty['kd_barang']][$itemQty['kd_barang']] : 0;

			if($stts == "add"){
				$qtyRecomendation = ($itemQty['fg_qty'] + $itemQty['wip_qty'] - $sum_woitemso_qtyso_planned);
					if ($qtyRecomendation == 0 || $qtyRecomendation <= 0) {
						$qtyRecomendation = 0 + $itemQty['sum_woitemso_qtyso'];
					}else {
						$p = $qtyRecomendation - $itemQty['sum_woitemso_qtyso'];
						if ($p <= 0) {
							$qtyRecomendation = $p * -1;
						}else{
							$qtyRecomendation = 0;
						}
					} 
					//$qtyRecomendation = $sum_woitemso_qtyso_planned;
			}else{
				$qtyRecomendation = 0;
			}
			
			$arrayItemQtys[$itemQty['kd_barang']] = [
				'woitemso_kd' => (int)$itemQty['woitemso_kd'],
				'woitemso_qty' => $qtyRecomendation,
			];
		}
		$this->db->trans_start();
		if (!empty($arrayItemQtys)) {
			$this->db->update_batch($this->tbl_name, $arrayItemQtys, 'woitemso_kd');
		}
		$this->db->trans_complete();

		return $this->db->trans_status();
		//return $arrayItemQtys;
	}
}
