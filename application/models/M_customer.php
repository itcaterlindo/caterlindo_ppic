<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class M_customer extends CI_Model {
	private $tbl_name = 'tm_customer';

	public function alamat_customer($kd_customer) {
		$this->db->select('a.kd_alamat_kirim, a.kd_badan_usaha, a.nm_customer, a.alamat, b.nm_negara, c.nm_provinsi, d.nm_kota, e.nm_kecamatan, f.nm_select');
		$this->db->from('td_customer_alamat a');
		$this->db->join('tb_negara b', 'a.negara_kd = b.kd_negara', 'left');
		$this->db->join('tb_provinsi c', 'a.provinsi_kd = c.kd_provinsi AND a.negara_kd = c.negara_kd', 'left');
		$this->db->join('tb_kota d', 'a.kota_kd = d.kd_kota AND a.provinsi_kd = d.provinsi_kd AND a.negara_kd = d.negara_kd', 'left');
		$this->db->join('tb_kecamatan e', 'a.kecamatan_kd = e.kd_kecamatan AND a.kota_kd = e.kota_kd AND a.provinsi_kd = e.provinsi_kd AND a.negara_kd = e.negara_kd', 'left');
		$this->db->join('tb_set_dropdown f', 'f.id = a.kd_badan_usaha', 'left');
		$this->db->where(array('a.customer_kd' => $kd_customer));
		$query = $this->db->get();
		$num = $query->num_rows();
		$result = $query->result();

		return $num > 0?$result:'';
	}

	public function detail_customer($kd_customer) {
		$this->db->select('a.nm_customer, a.alamat, a.code_customer, a.contact_person, a.no_telp_utama, a.no_telp_lain, a.email, b.nm_negara, c.nm_provinsi, d.nm_kota, e.nm_kecamatan, f.nm_select, h.nm_select AS price_category');
		$this->db->from('tm_customer a');
		$this->db->join('tb_negara b', 'a.negara_kd = b.kd_negara', 'left');
		$this->db->join('tb_provinsi c', 'a.provinsi_kd = c.kd_provinsi AND a.negara_kd = c.negara_kd', 'left');
		$this->db->join('tb_kota d', 'a.kota_kd = d.kd_kota AND a.provinsi_kd = d.provinsi_kd AND a.negara_kd = d.negara_kd', 'left');
		$this->db->join('tb_kecamatan e', 'a.kecamatan_kd = e.kd_kecamatan AND a.kota_kd = e.kota_kd AND a.provinsi_kd = e.provinsi_kd AND a.negara_kd = e.negara_kd', 'left');
		$this->db->join('tb_set_dropdown f', 'f.id = a.kd_badan_usaha', 'left');
		$this->db->join('tb_jenis_customer g', 'g.kd_jenis_customer = a.jenis_customer_kd', 'left');
		$this->db->join('tb_set_dropdown h', 'h.id = g.kd_price_category', 'left');
		$this->db->where(array('a.kd_customer' => $kd_customer));
		$query = $this->db->get();
		$num = $query->num_rows();
		$result = $query->row();

		return $result;
	}

	public function get_byname($nm_customer) {
		$this->db->select('kd_customer, nm_customer');
		$this->db->from($this->tbl_name);
		$this->db->like(array('nm_customer' => $nm_customer));
		$this->db->limit(10);
		$query = $this->db->get();
		$num = $query->num_rows();
		$result = $query->result();
		return $num > 0?$result:FALSE;
	}
}