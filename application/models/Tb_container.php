<?php
defined('BASEPATH') or exit('No direct script acccess allowed!');

class Tb_container extends CI_Model {
	private $tbl_name = 'tb_container';
	private $p_key = 'id';
	private $title_name = 'Data Container';

	/* --start ssp tabel untuk modul data warehouse-- */
	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){
					return $this->tbl_btn($d, $row[1]);
				} ),
			array( 'db' => 'a.nm_container', 'dt' => 2, 'field' => 'nm_container' ),
			array( 'db' => 'a.deskripsi', 'dt' => 3, 'field' => 'deskripsi' ),
			array( 'db' => 'a.konversi_20ft', 'dt' => 4, 'field' => 'konversi_20ft' ),
		);

		$data['sql_details'] = sql_connect();

		$data['joinQuery'] = "FROM ".$this->tbl_name." a";

		$data['where'] = "";

		return $data;
	}
	/* --end ssp tabel untuk modul data warehouse-- */

	/* --start button tabel untuk modul data warehouse-- */
	private function tbl_btn($id, $var) {
		$read_access = $this->session->read_access;
		$update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah', 'icon' => 'pencil', 'onclick' => 'get_form(\''.$id.'\')'));
		$btns[] = get_btn_divider();
		$btns[] = get_btn(array('access' => $delete_access, 'title' => 'Hapus', 'icon' => 'trash',
			'onclick' => 'return confirm(\'Anda akan menghapus data container = '.$var.'?\')?hapus_data(\''.$id.'\'):false'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function get_all() {
		$this->db->from($this->tbl_name);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function get_id() {
		$this->db->from($this->tbl_name);
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			return $row->{$this->p_key};
		else :
			return '';
		endif;
	}

	public function get_row($id = '') {
		$this->db->from($this->tbl_name)
			->where(array($this->p_key => $id));
		$query = $this->db->get();
		$num = $query->num_rows();
		if ($num > 0) :
			$row = $query->row();
			$data = array('id' => $row->id, 'nm_container' => $row->nm_container, 'deskripsi' => $row->deskripsi, 'konversi_20ft' => $row->konversi_20ft);
		else :
            $data = array('id' => "", 'nm_container' => "", 'deskripsi' => "", 'konversi_20ft' => "");		
        endif;
		return $data;
	}

	public function form_rules($opt) {
		if($opt == "new"):
			// Insert new
			$rules = array(
				array('field' => 'txtNama', 'label' => 'Nama Container', 'rules' => 'required'),
				array('field' => 'txtDeskripsi', 'label' => 'Deskripsi', 'rules' => 'required'),
				array('field' => 'txt20Ft', 'label' => '20Ft', 'rules' => 'required'),
			);
		else:
			// Insert edit
			$rules = array(
				array('field' => 'txtNama', 'label' => 'Nama Container', 'rules' => 'required'),
				array('field' => 'txtDeskripsi', 'label' => 'Deskripsi', 'rules' => 'required'),
				array('field' => 'txt20Ft', 'label' => '20Ft', 'rules' => 'required'),
			);
		endif;

		return $rules;
	}

	public function chk_container($nm_container = '') {
		$this->db->from($this->tbl_name)
			->where(array('nm_container' => $nm_container));
		$query = $this->db->get();
		$num = $query->num_rows();

		return $num > 0?FALSE:TRUE;
	}

	public function build_warning($datas = '') {
		$forms = array('txtNama', 'txtDeskripsi', 'txt20Ft');
		foreach ($datas as $key => $data) :
			$str[$data] = (!empty(form_error($forms[$key])))?buildLabel('warning', form_error($forms[$key], '"', '"')):'';
		endforeach;
		return $str;
	}

	public function submit_data($data = '', $tipe = '') {
		if ($tipe == "edit") :
			// Proses jika edit data
			$label = 'Mengubah '.$this->title_name;
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s')));
			$where[$this->p_key] = $data[$this->p_key];
			$act = $this->update($submit, $where);
		else :
			// Proses jika new data
			$chk_container = $this->chk_container($data['nm_container']);
			if (!$chk_container) :
				$str = $this->report(0, 'Menambahkan '.$this->title_name.' Nama container \''.$data['nm_container'].'\' sudah digunakan!', $data);
				return $str;
				exit();
			endif;
			$label = 'Menambahkan '.$this->title_name;
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s')));
			$act = $this->create($submit);
		endif;
		$str = $this->report($act, $label, $submit);
		return $str;
	}

	private function create($data = '') {
		$act = $this->db->insert($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	private function update($data = '', $where = '') {
		$act = $this->db->update($this->tbl_name, $data, $where);
		return $act?TRUE:FALSE;
	}

	public function report($act = '', $label = '', $data = '') {
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
		endif;
		$this->m_builder->write_log($stat, $label, $data);
		return $str;
	}

	public function delete_data($id = '') {
		$act = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		$report = $this->report($act, 'Menghapus '.$this->title_name, array($this->p_key => $id));
		return $report;
	}
}