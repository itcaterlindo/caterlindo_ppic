<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_tipe_harga extends CI_Model {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'tb_tipe_harga';
	private $p_key = 'nm_harga';
	private $title = 'Tipe Harga';

	public function get_table() {
		$this->db->select('a.nm_harga, b.currency_nm, a.row_flag');
		$this->db->from($this->tbl_name.' a');
		$this->db->join('tm_currency b', 'b.kd_currency = a.currency_kd', 'left');
		$query = $this->db->get();
		$result = $query->result();

		$no = 0;
		foreach ($result as $row) :
			$no++;
			$tbl_row[] = '<tr><td style=\'text-align:center;\'>'.$no.'</td><td style=\'text-align:center;\'>'.$this->tbl_btn($row->nm_harga, $row->row_flag).'</td><td>'.$row->nm_harga.'</td><td>'.$row->currency_nm.'</td><td style=\'text-align:center;\'>'.ucfirst($row->row_flag).'</td></tr>';
		endforeach;
		return $tbl_row;
	}

	private function tbl_btn($id, $flag) {
		$status = set_flag($flag);
		$update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
		$btns[] = get_btn_divider();
		$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah Status', 'icon' => 'exchange', 'onclick' => 'return confirm(\'Anda akan mengubah status '.$this->title.' = '.$id.'?\')?hapus_data(\''.$id.'\', \''.$status.'\'):false'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function get_data($kode) {
		$this->db->from($this->tbl_name);
		$this->db->where(array('nm_harga' => $kode));
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}

	public function send_data($data) {
		if (empty($data[$this->p_key])) :
			$label_err = 'menambahkan '.$this->title;
			$data[$this->p_key] = $this->create_pkey();
			$act = $this->input_data($data);
		else :
			$label_err = 'mengubah '.$this->title;
			$act = $this->update_data($data, array($this->p_key => $data[$this->p_key]));
		endif;
		
		if ($act) :
			$log_stat = 'berhasil';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.'!');
		else :
			$log_stat = 'gagal';
			$str['confirm'] = 'error';
			$str['idErrForm'] = buildAlert('danger', 'Gagal!', 'Gagal '.$label_err.', kesalahan sistem!');
		endif;
		$this->m_builder->write_log($log_stat, $label_err, $data);

		return $str;
	}

	private function input_data($data) {
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s'));
		$submit = array_merge($data, $data_adm);
		$aksi = $this->db->insert($this->tbl_name, $submit);
		return $aksi?TRUE:FALSE;
	}

	private function update_data($data, $where) {
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s'));
		$submit = array_merge($data, $data_adm);
		$aksi = $this->db->update($this->tbl_name, $submit, $where);
		return $aksi?TRUE:FALSE;
	}
}