<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_part_overhead extends CI_Model {
	private $tbl_name = 'td_part_overhead';
	private $p_key = 'partoverhead_kd';


	public function ssp_table($part_kd) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'b.overhead_nama', 
				'dt' => 2, 'field' => 'overhead_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.partoverhead_qty', 
				'dt' => 3, 'field' => 'partoverhead_qty',
				'formatter' => function ($d , $row){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'c.rmsatuan_nama', 
				'dt' => 4, 'field' => 'rmsatuan_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'a.partoverhead_hargaunit', 
				'dt' => 5, 'field' => 'partoverhead_hargaunit',
				'formatter' => function ($d){
					$d = custom_numberformat($d, 2, 'IDR');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.partoverhead_hargatotal', 
				'dt' => 6, 'field' => 'partoverhead_hargatotal',
				'formatter' => function ($d){
					$d = custom_numberformat($d, 2, 'IDR');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.partoverhead_tglinput', 
				'dt' => 7, 'field' => 'partoverhead_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] = "FROM ".$this->tbl_name." as a
							LEFT JOIN tb_part_overhead as b ON a.overhead_kd=b.overhead_kd
							LEFT JOIN td_rawmaterial_satuan as c ON b.overhead_satuankd=c.rmsatuan_kd";
		$data['where'] = "a.part_kd= '".$part_kd."'";
		
		return $data;
	}

	private function tbl_btn($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btns = array();
		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data_overhead(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data_overhead(\''.$id.'\')'));	
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
        $query = $this->db->select('MAX('.$this->p_key.') as maxID')
                ->get($this->tbl_name)
                ->row();
        $code = (int) $query->maxID + 1;
        return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }

    public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

    public function get_all () {
        return $this->db->get($this->tbl_name);
    }

    public function get_by_param_detail ($param = []) {
        $qResult = $this->db->select()
                    ->from($this->tbl_name)
					->join('tb_part_overhead', $this->tbl_name.'.overhead_kd=tb_part_overhead.overhead_kd', 'left')
					->where($param)
                    ->order_by($this->tbl_name.'.'.$this->p_key, 'asc')
                    ->get();
        return $qResult;
    }
	
	public function get_ovh_by_partmain($part_main_kd){
		$sql="SELECT tmp.partmain_kd, tbpo.sap_kd, tdpo.partoverhead_qty FROM tm_part_main AS tmp 
					INNER JOIN tb_part_lastversion ON tmp.partmain_kd = tb_part_lastversion.partmain_kd
					INNER JOIN td_part AS tdp ON tdp.part_kd = tb_part_lastversion.part_kd
					INNER JOIN td_part_overhead as tdpo ON tdpo.part_kd = tdp.part_kd
					INNER JOIN tb_part_overhead as tbpo ON tbpo.overhead_kd = tdpo.overhead_kd
			WHERE tmp.partmain_kd = '$part_main_kd'";
					
			$query = $this->db->query($sql);
			
			return $query;
	}

	public function get_ovh_by_partmain_in($part_main_kd){
		$param = implode("', '",$part_main_kd);
		$sql="SELECT tmp.partmain_kd, tbpo.sap_kd, tdpo.partoverhead_qty FROM tm_part_main AS tmp 
					INNER JOIN tb_part_lastversion ON tmp.partmain_kd = tb_part_lastversion.partmain_kd
					INNER JOIN td_part AS tdp ON tdp.part_kd = tb_part_lastversion.part_kd
					INNER JOIN td_part_overhead as tdpo ON tdpo.part_kd = tdp.part_kd
					INNER JOIN tb_part_overhead as tbpo ON tbpo.overhead_kd = tdpo.overhead_kd
			WHERE tmp.partmain_kd IN ('$param')";
					

			$query = $this->db->query($sql);
			
			return $query;
	}

	public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

}