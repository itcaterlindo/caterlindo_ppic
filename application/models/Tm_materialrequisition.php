<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_materialrequisition extends CI_Model {
	private $tbl_name = 'tm_materialrequisition';
	private $p_key = 'materialreq_kd';

    public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array (
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){

					return $this->tbl_btn($d, $row[7]);
				}),
			array( 'db' => 'a.materialreq_tanggal',
				'dt' => 2, 'field' => 'materialreq_tanggal',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.materialreq_no',
				'dt' => 3, 'field' => 'materialreq_no',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.bagian_nama',
				'dt' => 4, 'field' => 'bagian_nama',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.wfstate_nama',
				'dt' => 5, 'field' => 'wfstate_nama',
				'formatter' => function($d, $row){
					$d = build_badgecolor($row[6], $d);

					return $d;
				}),
			array( 'db' => 'a.materialreq_tgledit',
				'dt' => 6, 'field' => 'materialreq_tgledit',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array(
				'db' => 'c.wfstate_badgecolor',
				'dt' => 7, 'field' => 'wfstate_badgecolor',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'c.wfstate_action',
				'dt' => 8, 'field' => 'wfstate_action',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
							LEFT JOIN tb_bagian as b ON a.bagian_kd=b.bagian_kd
							LEFT JOIN td_workflow_state as c ON c.wfstate_kd=a.wfstate_kd";
		$kd_admin = $this->session->userdata('kd_admin');
		$tipe_admin_kd = $this->session->userdata('tipe_admin_kd');
		$bagians = $this->tb_deliverynote_user->get_bagianAllowed($kd_admin, $tipe_admin_kd);
		$sBagians = implode(", ", $bagians);
		$data['where'] = "a.bagian_kd IN ({$sBagians})";

		return $data;
	}

	public function ssp_table2 ($aParam = []) 
    {
		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		
		$data = [];
		/** Access for user bagian */
		$kd_admin = $this->session->userdata('kd_admin');
		$tipe_admin_kd = $this->session->userdata('tipe_admin_kd');
		$bagians = $this->tb_deliverynote_user->get_bagianAllowed($kd_admin, $tipe_admin_kd);

		$qData = $this->db->select($this->tbl_name.'.*, GROUP_CONCAT(COALESCE(tm_planningweekly.planningweekly_no) SEPARATOR ", ") as planningweekly_nos, 
            td_workflow_state.wfstate_action, td_workflow_state.wfstate_badgecolor, td_workflow_state.wfstate_nama, tb_bagian.bagian_nama, div.bagian_nama as divisi_nama')
				->from($this->tbl_name)
				->join('tb_materialrequisition_detail_planningweekly', $this->tbl_name.'.materialreq_kd=tb_materialrequisition_detail_planningweekly.materialreq_kd', 'left')
				->join('td_workflow_state', $this->tbl_name.'.wfstate_kd=td_workflow_state.wfstate_kd', 'left')
				->join('tm_planningweekly', 'tb_materialrequisition_detail_planningweekly.planningweekly_kd=tm_planningweekly.planningweekly_kd', 'left')
				->join('tb_bagian', $this->tbl_name.'.bagian_kd=tb_bagian.bagian_kd', 'left')
				->join('tb_bagian as div', $this->tbl_name.'.divisi_kd=div.bagian_kd', 'left');

		$qData = $qData->where_in($this->tbl_name.'.divisi_kd', $bagians)
				->group_by($this->tbl_name.'.materialreq_kd')
				->order_by($this->tbl_name.'.materialreq_tgledit', 'desc')
				->get();
		foreach ($qData->result_array() as $r) {
			$data[] = [
				'1' => $this->tbl_btn($r['materialreq_kd'], $r['wfstate_action']),
				'2' => $r['materialreq_tanggal'],
				'3' => $r['materialreq_no'],
				'4' => $r['divisi_nama'],
				'5' => $r['bagian_nama'],
				'6' => $r['planningweekly_nos'],
				'7' => build_badgecolor($r['wfstate_badgecolor'], $r['wfstate_nama']),
				'8' => choice_enum($r['materialreq_receiptclosed']),
				'9' => $r['materialreq_tgledit'],
			];
		}
		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

	private function tbl_btn($id, $wfstate_action) {
		$btns = array();
		if (!empty($wfstate_action)){
			$exp = explode(';', $wfstate_action);
			if (in_array('view', $exp)){ 
					$btns[] = get_btn(array('title' => 'Lihat Item', 'icon' => 'list', 'onclick' => 'view_data(\''.$id.'\')'));
			}
			if (in_array('edit', $exp)){ 
				if (cek_permission('MATERIALREQ_UPDATE')){
					$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
				}
			}
			if (in_array('print', $exp)){ 
				$btns[] = get_btn(array('title' => 'Cetak Item', 'icon' => 'print', 'onclick' => 'cetak_data(\''.$id.'\')'));
			}
			if (in_array('delete', $exp)){ 
				if (cek_permission('MATERIALREQ_DELETE')){
					$btns[] = get_btn_divider();
					$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));
				}
			}
		}
		
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	private function materailreq_status ($materialreq_status) {
		$label = 'warning'; 
		if ($materialreq_status == 'finish') {
			$label = 'success';
		}
		$status = '<span class="label label-'.$label.'">'.ucwords($materialreq_status).'</span>';
		return $status;
	} 

	// public function create_no($date) {
	// 	$this->db->select('materialreq_no AS code')
	// 		->from($this->tbl_name)
	// 		->where(array('YEAR(materialreq_tanggal)' => format_date($date, 'Y')))
	// 		->where(array('MONTH(materialreq_tanggal)' => format_date($date, 'm')))
	// 		->order_by('materialreq_no DESC');
	// 	$query = $this->db->get();
	// 	$num = $query->num_rows();
	// 	$urutan = 0;
	// 	if ($num > 0) :
	// 		$row = $query->row();
	// 		$code = $row->code;
	// 		$urutan = substr($code, -4);
	// 	endif;
	// 	$angka = $urutan + 1;
	// 	$primary = 'MRQ-'.date('ym').'-'.str_pad($angka, 4, '0', STR_PAD_LEFT);
	// 	return $primary;
	// }

	public function create_no($divisi_kd, $dn_tanggal)
	{
		$this->load->model(['tb_bagian']);
		$bagian = $this->tb_bagian->get_by_param(['bagian_kd' => $divisi_kd])->row_array();

		$dn_year = format_date($dn_tanggal, 'Y');
		$dn_month = format_date($dn_tanggal, 'm');
		$query = $this->db->select('materialreq_no as max_no')
			->where('YEAR(materialreq_tanggal)', $dn_year)
			->where('divisi_kd', $divisi_kd)
			->order_by('materialreq_kd', 'desc')
			->get($this->tbl_name)->row();
		if (empty($query->max_no)) :
			$no = 0;
		else :
			$no = (int) substr($query->max_no, -4);
		endif;
		$no = (int)$no + 1;
		$dn_no = 'MRQ' . '-' . strtoupper($bagian['bagian_nama']) . '-' . format_date($dn_year, 'Y') . '-' . str_pad($no, '4', '0', STR_PAD_LEFT);

		return $dn_no;
	}

	public function create_code () {
		$query = $this->db->select('MAX(materialreq_kd) as max_kd')
				->get($this->tbl_name)->row();
		if (empty($query->max_kd)) :
			$no = 0;
		else:
			$no = $query->max_kd;
		endif;
		$no = $no + 1;

		return $no;
	}

	public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}

	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }
    
    public function insert_batch_data ($data=[]){
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
    
    }

    public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

    public function get_where_in ($where, $in = []){
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}

	public function get_by_param_detail ($param=[]) {
		$query = $this->db->select($this->tbl_name.'.*, tb_bagian.*')
					->from($this->tbl_name)
					->join('tb_bagian', $this->tbl_name.'.bagian_kd=tb_bagian.bagian_kd', 'left')
					->where($param)
					->get();
		return $query;
	}

	public function getRowMaterialReqBagianState($id)
    {
        $qRow = $this->db->select('tm_materialrequisition.*, tb_bagian.bagian_nama, div.bagian_nama as divisi_nama, td_workflow_state.*, tb_admin.nm_admin')
			->where('tm_materialrequisition.materialreq_kd', $id)
            ->join('tb_bagian', 'tb_bagian.bagian_kd=tm_materialrequisition.bagian_kd', 'left')
            ->join('tb_bagian as div', 'div.bagian_kd=tm_materialrequisition.divisi_kd', 'left')
            ->join('td_workflow_state', 'td_workflow_state.wfstate_kd=tm_materialrequisition.wfstate_kd', 'left')
            ->join('tb_admin', 'tb_admin.kd_admin=tm_materialrequisition.materialreq_originator', 'left')
            ->get('tm_materialrequisition')->row_array();
        return $qRow;
    }

	public function generate_button ($id, $wf_kd, $wfstate_kd) 
	{
		$admin_kd = $this->session->userdata('kd_admin');
		$admin = $this->db->where('kd_admin', $admin_kd)->get('tb_admin')->row_array();
		$kd_tipe_admin = $admin['tipe_admin_kd'];

		$transitions = $this->db->select('td_workflow_transition.*, td_workflow_transition_permission.wftransitionpermission_adminkd, td_workflow_transition_permission.kd_tipe_admin')
					->from('td_workflow_transition')
					->join('td_workflow_transition_permission', 'td_workflow_transition.wftransition_kd=td_workflow_transition_permission.wftransition_kd','left')
					->where('td_workflow_transition.wf_kd', $wf_kd)
					->where('td_workflow_transition.wftransition_source', $wfstate_kd)
					->where('td_workflow_transition.wftransition_active', '1')
					->get()->result_array();
	
		$html = '<div class="btn-group">
		<button type="button" class="btn btn-warning btn-flat dropdown-toggle" data-toggle="dropdown">
			<span class="fa fa fa-hand-o-right"></span>
			Ubah State
		  	<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">';
		$allowTransitions = [];
		foreach ($transitions as $transition) {
			if ($transition['wftransitionpermission_adminkd'] == $admin_kd || $transition['kd_tipe_admin'] == $kd_tipe_admin){
				$allowTransitions[] = [
					'wftransition_kd' => $transition['wftransition_kd'],
					'wf_kd' => $transition['wf_kd'],
					'wftransition_nama' => $transition['wftransition_nama'],
					'wftransition_email' => $transition['wftransition_email'],
				];
			}
		}
		#Group by wftransition_kd
		$tempArrayExist = []; $arrayGroups = [];
		foreach ($allowTransitions as $allowTransition) {
			if (!in_array($allowTransition['wftransition_kd'], $tempArrayExist)){
				$arrayGroups[] = $allowTransition;
				$tempArrayExist[] = $allowTransition['wftransition_kd'];
			}
		}
		#after group
		foreach ($arrayGroups as $arrayGroup) {
			$html .= '<li><a href="javascript:void(0)" onclick=ubah_state("'.$id.'",'.$arrayGroup['wftransition_kd'].')> <i class="fa fa-hand-o-right"></i> '.$arrayGroup['wftransition_nama'].'</a></li>';
		}
		
		$html .= '
		</ul>
	  </div>';

		return $html;
	}

	public function checkMaterialReqReceipt($materialreq_kds = []) {
		$materialReqdetails = $this->db->select('td_materialrequisition_detail.materialreq_kd, td_materialrequisition_detail.materialreqdetail_kd, td_materialrequisition_detail.materialreqdetail_qty')
			->where_in('td_materialrequisition_detail.materialreq_kd', $materialreq_kds)
			->get('td_materialrequisition_detail')->result_array();
		$materialReceptdetails = $this->db->select('td_materialreceipt_detail.materialreqdetail_kd, SUM(td_materialreceipt_detail.materialreceiptdetail_qty) AS sum_materialreceiptdetail_qty')
			->where_in('tb_materialreceipt_detail_requisition.materialreq_kd', $materialreq_kds) 
			->join('tb_materialreceipt_detail_requisition', 'tb_materialreceipt_detail_requisition.materialreceiptreq_kd=td_materialreceipt_detail.materialreceiptreq_kd', 'left')
			->group_by('td_materialreceipt_detail.materialreqdetail_kd')
			->get('td_materialreceipt_detail')->result_array();
		$arrayBalances = [];
		$qtyBalance = 0;
		foreach ($materialReqdetails as $materialReqdetail){
			$qtyBalance = (float)$materialReqdetail['materialreqdetail_qty'];
			foreach ($materialReceptdetails as $materialReceptdetail) {
				if ($materialReceptdetail['materialreqdetail_kd'] == $materialReqdetail['materialreqdetail_kd']){
					$qtyBalance = (float)$qtyBalance - $materialReceptdetail['sum_materialreceiptdetail_qty'];
				}
			}
			$arrayBalances[$materialReqdetail['materialreq_kd']][] = (float)$qtyBalance;
		}
		$dataUpdateBatch = [];
		foreach ($arrayBalances as $eMaterialreq_kd => $elementBalance){
			/** Set to not closed */
			$materialreq_receiptclosed = 0;
			if (max($elementBalance) == 0) {
				/** Set to closed */
				$materialreq_receiptclosed = 1;
			}
			$dataUpdateBatch[] = [
				'materialreq_kd' => $eMaterialreq_kd,
				'materialreq_receiptclosed' => $materialreq_receiptclosed,
			];
		}
		if (!empty($dataUpdateBatch)){
			$this->db->update_batch($this->tbl_name, $dataUpdateBatch, 'materialreq_kd');
		}
		
		return;
	}
}