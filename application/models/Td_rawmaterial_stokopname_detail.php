<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_rawmaterial_stokopname_detail extends CI_Model {
	private $tbl_name = 'td_rawmaterial_stokopname_detail';
	private $p_key = 'rmstokopnamedetail_kd';

	public function create_code() {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

    public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
    }

	public function update_batch ($idColumn, $data = []) {
		$act = $this->db->update_batch($this->tbl_name, $data, $idColumn);
		return $act;
	}

	public function generateDetailStokOpname($rmstokopname_kd) 
	{	
		$this->load->model(['td_rawmaterial_stok_master', 'tm_rawmaterial_stokopname']);
		$rmstokopname = $this->tm_rawmaterial_stokopname->get_by_param(['rmstokopname_kd' => $rmstokopname_kd])->row_array();
		$rmstokopnameDetails = $this->get_by_param(['rmstokopname_kd' => $rmstokopname_kd])->result_array();
		$rm_kds = array_column($rmstokopnameDetails, 'rm_kd');
		$stokMasters = $this->td_rawmaterial_stok_master->get_by_param_in_detail('td_rawmaterial_stok_master.rm_kd', $rm_kds)->result_array();
		$mutasiStokPeriodes = $this->td_rawmaterial_stok_master->generateMutasiStokResult($rm_kds, $rmstokopname['rmstokopname_periode']);

		$dataUpdateBatchs = [];
		foreach ($rmstokopnameDetails as $rmstokopnameDetail){
			$rmstokopnamedetail_qtykonversi = 0;
			$rmstokopnamedetail_konversi = 1;
			$rmstokopnamedetail_qty = 0;
			$rmstokopnamedetail_satuan = null;
			if (isset($mutasiStokPeriodes[$rmstokopnameDetail['rm_kd']])){
				$rmstokopnamedetail_qtykonversi = $mutasiStokPeriodes[$rmstokopnameDetail['rm_kd']]['qty_aft_mutasi'];
				$rmstokopnamedetail_konversi = $mutasiStokPeriodes[$rmstokopnameDetail['rm_kd']]['rm_konversiqty'];
				$rmstokopnamedetail_qty = $rmstokopnamedetail_qtykonversi / $rmstokopnamedetail_konversi;
				$rmstokopnamedetail_satuan = $mutasiStokPeriodes[$rmstokopnameDetail['rm_kd']]['rmsatuansecondary_kd'];
			}

			$dataUpdateBatchs[] = [
				'rmstokopnamedetail_kd' => $rmstokopnameDetail['rmstokopnamedetail_kd'],
				'rmstokopnamedetail_konversi' => !empty($rmstokopnameDetail['rmstokopnamedetail_konversi']) ? $rmstokopnameDetail['rmstokopnamedetail_konversi'] : $rmstokopnamedetail_konversi,
				'rmstokopnamedetail_qtykonversi' => !empty($rmstokopnameDetail['rmstokopnamedetail_qtykonversi']) ? $rmstokopnameDetail['rmstokopnamedetail_qtykonversi']: $rmstokopnamedetail_qtykonversi,
				'rmstokopnamedetail_qty' => !empty($rmstokopnameDetail['rmstokopnamedetail_qty']) ? $rmstokopnameDetail['rmstokopnamedetail_qty'] : $rmstokopnamedetail_qty,
				'rmstokopnamedetail_satuan' => !empty($rmstokopnameDetail['rmstokopnamedetail_satuan']) ? $rmstokopnameDetail['rmstokopnamedetail_satuan'] :$rmstokopnamedetail_satuan,
				'admin_kd' => $this->session->userdata('kd_admin')
			];
		}
		if (!empty($dataUpdateBatchs)){
			$this->update_batch('rmstokopnamedetail_kd', $dataUpdateBatchs);
		}

		return true;
	}

	public function stokOpnameDetailRM($rmstokopname_kd)
	{
		$query = $this->db->select("{$this->tbl_name}.*, tm_rawmaterial.*, td_rawmaterial_satuan.rmsatuan_nama, 
				satuan_secondary.rmsatuan_kd as satuan_secondary_kd, satuan_secondary.rmsatuan_nama as satuan_secondary_nama, td_rawmaterial_stok_master.rmstokmaster_qty,
				satuan_opname.rmsatuan_nama as satuan_opname_nama")
			->where("{$this->tbl_name}.rmstokopname_kd", $rmstokopname_kd)
			->join('tm_rawmaterial', "tm_rawmaterial.rm_kd={$this->tbl_name}.rm_kd", 'left')
			->join('td_rawmaterial_satuan', "td_rawmaterial_satuan.rmsatuan_kd=tm_rawmaterial.rmsatuan_kd", 'left')
			->join('td_rawmaterial_satuan as satuan_secondary', "satuan_secondary.rmsatuan_kd=tm_rawmaterial.rmsatuan_kd", 'left')
			->join('td_rawmaterial_satuan as satuan_opname', "satuan_opname.rmsatuan_kd={$this->tbl_name}.rmstokopnamedetail_satuan", 'left')
			->join('td_rawmaterial_stok_master', "td_rawmaterial_stok_master.rm_kd={$this->tbl_name}.rm_kd" ,'left')
			->order_by("tm_rawmaterial.rm_kode", 'asc')
			->get($this->tbl_name);
		return $query;
	}

	public function get_by_param_detail($param = [])
	{
		$query = $this->db->select("{$this->tbl_name}.*, tm_rawmaterial.*, td_rawmaterial_satuan.rmsatuan_nama, 
				satuan_secondary.rmsatuan_kd as satuan_secondary_kd, satuan_secondary.rmsatuan_nama as satuan_secondary_nama, td_rawmaterial_stok_master.rmstokmaster_qty,
				satuan_opname.rmsatuan_kd as satuan_opname_kd, satuan_opname.rmsatuan_nama as satuan_opname_nama")
			->where($param)
			->join('tm_rawmaterial', "tm_rawmaterial.rm_kd={$this->tbl_name}.rm_kd", 'left')
			->join('td_rawmaterial_satuan', "td_rawmaterial_satuan.rmsatuan_kd=tm_rawmaterial.rmsatuan_kd", 'left')
			->join('td_rawmaterial_satuan as satuan_secondary', "satuan_secondary.rmsatuan_kd=tm_rawmaterial.rmsatuansecondary_kd", 'left')
			->join('td_rawmaterial_satuan as satuan_opname', "satuan_opname.rmsatuan_kd={$this->tbl_name}.rmstokopnamedetail_satuan", 'left')
			->join('td_rawmaterial_stok_master', "td_rawmaterial_stok_master.rm_kd={$this->tbl_name}.rm_kd" ,'left')
			->get($this->tbl_name);
		return $query;
	}

	public function generateFinishOpnameResult($rmstokopname_kd)
	{
		$this->load->model(['td_rawmaterial_stok_master', 'tm_rawmaterial_stokopname']);

		$rowRmstokmaster = $this->tm_rawmaterial_stokopname->get_by_param(['rmstokopname_kd' => $rmstokopname_kd])->row_array();
		$rmstokopnamedetails = $this->get_by_param(['rmstokopname_kd' => $rmstokopname_kd])->result_array();
		$rm_kds = array_column($rmstokopnamedetails, 'rm_kd');
		$rmstokmasters = $this->td_rawmaterial_stok_master->get_by_param_in('rm_kd', $rm_kds)->result_array();

		$dataBatch = array();
		foreach ($rmstokmasters as $rmstokmaster) {
			foreach ($rmstokopnamedetails as $rmstokopnamedetail){
				if ($rmstokopnamedetail['rm_kd'] == $rmstokmaster['rm_kd']){
					$dataBatch[] = [
						'rmstokmaster_kd' => $rmstokmaster['rmstokmaster_kd'],
						'rmstokmaster_stokopname_tgl' => $rowRmstokmaster['rmstokopname_periode'],
						'rmstokmaster_stokopname_qty' => $rmstokopnamedetail['rmstokopnamedetail_qtykonversi'],
						'rmstokmaster_tgledit' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin')
					];
				}
			}
		}

		if (!empty($dataBatch)){
			$this->td_rawmaterial_stok_master->update_batch ('rmstokmaster_kd', $dataBatch);
		}
		return true;
	}

	public function getOpnameDetailByStokopnameID($rmstokopname_kd)
	{
		$this->load->model(array('td_rawmaterial_stok_master', 'tm_rawmaterial_stokopname'));
        $opnameMaster = $this->tm_rawmaterial_stokopname->get_by_param(['rmstokopname_kd' => $rmstokopname_kd])->row_array();
		$opnameDetails = $this->stokOpnameDetailRM($rmstokopname_kd)->result_array();
        $rm_kds = array_column($opnameDetails, 'rm_kd');
        $mutasiStok = $this->td_rawmaterial_stok_master->generateMutasiStokResult($rm_kds, $opnameMaster['rmstokopname_periode']);
        $arrDetails = array();
        foreach ($opnameDetails as $opnameDetail) {
            $arrDetails[] = [
                'rmstokopnamedetail_kd' => $opnameDetail['rmstokopnamedetail_kd'],
                'rm_kd' => $opnameDetail['rm_kd'],
                'rm_oldkd' => $opnameDetail['rm_oldkd'],
                'rm_kode' => $opnameDetail['rm_kode'],
                'rm_deskripsi' => $opnameDetail['rm_deskripsi'],
                'rm_spesifikasi' => $opnameDetail['rm_spesifikasi'],
                'rmsatuan_nama' => $opnameDetail['rmsatuan_nama'],
                'satuan_secondary_nama' => $opnameDetail['satuan_secondary_nama'],
                'rmstokopnamedetail_qty' => $opnameDetail['rmstokopnamedetail_qty'],
                'satuan_opname_nama' => $opnameDetail['satuan_opname_nama'],
                'rmstokopnamedetail_konversi' => $opnameDetail['rmstokopnamedetail_konversi'],
                'rmstokopnamedetail_qtykonversi' => $opnameDetail['rmstokopnamedetail_qtykonversi'],
                'rmstokopnamedetail_remark' => $opnameDetail['rmstokopnamedetail_remark'],
                'rmstokopnamedetail_tgledit' => $opnameDetail['rmstokopnamedetail_tgledit'],
                'qty_aft_mutasi' => isset($mutasiStok[$opnameDetail['rm_kd']]['qty_aft_mutasi']) ? $mutasiStok[$opnameDetail['rm_kd']]['qty_aft_mutasi'] : 0
            ];
        }
        return $arrDetails;

	}

	public function get_priodeopname($rm_kd, $startdate, $enddate)
	{
		$qResults = $this->db->select("{$this->tbl_name}.*, tm_rawmaterial.*, tm_rawmaterial_stokopname.rmstokopname_periode, tm_rawmaterial_stokopname.rmstokopname_kode, tm_rawmaterial_stokopname.rmstokopname_note,
				td_rawmaterial_satuan.rmsatuan_nama, satuan_secondary.rmsatuan_nama as satuan_secondary_nama")
			->join('tm_rawmaterial_stokopname', "tm_rawmaterial_stokopname.rmstokopname_kd={$this->tbl_name}.rmstokopname_kd" ,'left')
			->join('tm_rawmaterial', "tm_rawmaterial.rm_kd={$this->tbl_name}.rm_kd" ,'left')
			->join('td_rawmaterial_satuan', "td_rawmaterial_satuan.rmsatuan_kd=tm_rawmaterial.rmsatuan_kd" ,'left')
			->join('td_rawmaterial_satuan as satuan_secondary', "satuan_secondary.rmsatuan_kd=tm_rawmaterial.rmsatuansecondary_kd" ,'left')
			->where("{$this->tbl_name}.rm_kd", $rm_kd)
			->where('tm_rawmaterial_stokopname.rmstokopname_status', 'finish')
			->where('tm_rawmaterial_stokopname.rmstokopname_periode >=', $startdate)
			->where('tm_rawmaterial_stokopname.rmstokopname_periode <=', $enddate)
			->get($this->tbl_name)
			->result_array();

			$arrResult = array();
		foreach ($qResults as $r) {
			$trans_qty_secondary = $r['rmstokopnamedetail_qty'] / $r['rm_konversiqty'];
			$arrResult[] = [
				'trans_tgl' => format_date($r['rmstokopname_periode'], 'Y-m-d H:i:s'),
				'trans_jenis' => 'OPNAME',
				'rm_kd' => $r['rm_kd'],
				'rm_kode' => $r['rm_kode'],
				'rm_deskripsi' => $r['rm_deskripsi'],
				'rm_spesifikasi' => $r['rm_spesifikasi'],
				'rm_konversiqty' => $r['rm_konversiqty'],
				'trans_bukti' => $r['rmstokopname_kode'],
				'trans_bukti_remark' => $r['rmstokopname_note'],
				'trans_bukti_uraian' => "STOCKOPNAME-{$r['rmstokopname_kode']}/{$r['rmstokopname_note']} | QTY = {$r['rmstokopnamedetail_qty']} {$r['rmsatuan_nama']} ($trans_qty_secondary {$r['satuan_secondary_nama']})",
				'trans_qty_in' => 0,
				'trans_qty_out' => 0,
				'trans_satuan' => $r['rmsatuan_nama'],
				'trans_qty_secondary' => $trans_qty_secondary,
				'trans_satuan_secondary' => $r['satuan_secondary_nama'],
			];
		}
		
		return $arrResult;		
	}
	
}