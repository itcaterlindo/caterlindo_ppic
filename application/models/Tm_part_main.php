<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_part_main extends CI_Model {
	private $tbl_name = 'tm_part_main';
	private $p_key = 'partmain_kd';
	private $r_detailpartversi;

	public function ssp_table($partjenis_kd, $partstate_kd) {
		$this->r_detailpartversi = $this->db->select()
						->from('td_part')
						->join('tb_part_state', 'td_part.partstate_kd=tb_part_state.partstate_kd', 'left')
						->join('tb_admin', 'td_part.admin_kd=tb_admin.kd_admin', 'left')
						->order_by('td_part.part_versi')
						->get()->result_array();

		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){
					
					return $this->tbl_btn($d, $row[6], $row[7]);
				}),
				array( 'db' => 'a.'.$this->p_key,
				'dt' => 2, 'field' => $this->p_key,
				'formatter' => function($d, $row){
					
					return $this->push_sap($d);
				}),
				array( 'db' => 'a.'.$this->p_key,
				'dt' => 3, 'field' => $this->p_key,
				'formatter' => function($d, $row){
					
					return $this->push_sap2($d);
				}),
			array( 'db' => 'a.partmain_nama', 
				'dt' => 4, 'field' => 'partmain_nama',
				'formatter' => function ($d , $row){
					$d = $d;
					$d .= '<ul>';
					foreach ($this->r_detailpartversi as $eachDetailversi) {
						if ($eachDetailversi['partmain_kd'] == $row[0]){
							$d .= '<li> Versi '.$eachDetailversi['part_versi'].'/'.$eachDetailversi['nm_admin'].'/ '.build_span ($eachDetailversi['partstate_label'], $eachDetailversi['partstate_nama']).'</li>';
						}
					}
					
					$d .= '</ul>';
					$d = $this->security->xss_clean($d);
					return $d;
                }),
			array( 'db' => 'b.partjenis_nama', 
				'dt' => 5, 'field' => 'partjenis_nama',
				'formatter' => function ($d , $row){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.partmain_note', 
				'dt' => 6, 'field' => 'partmain_note',
				'formatter' => function ($d , $row){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'd.part_versi', 
				'dt' => 7, 'field' => 'part_versi',
				'formatter' => function ($d , $row){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.partmain_status', 
				'dt' => 8, 'field' => 'partmain_status',
				'formatter' => function ($d){
					$d = $this->label_status($d);

					return $d;
                }),
			array( 'db' => 'a.partmain_tgledit', 
				'dt' => 9, 'field' => 'partmain_tgledit',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name.' AS a 
						LEFT JOIN td_part_jenis as b ON a.partjenis_kd=b.partjenis_kd
						LEFT JOIN tb_part_lastversion as c on a.partmain_kd=c.partmain_kd
						LEFT JOIN td_part as d ON c.part_kd=d.part_kd
						LEFT JOIN td_part as e ON a.partmain_kd=e.partmain_kd';
		$where = "";
		if (!empty($partjenis_kd) && !empty($partstate_kd)) {
			$where .= "a.partjenis_kd = '$partjenis_kd' AND e.partstate_kd = '$partstate_kd'";
		}else if(!empty($partjenis_kd)){
			$where .= "a.partjenis_kd = '$partjenis_kd'";
		}else if(!empty($partstate_kd)){
			$where .= "e.partstate_kd = '$partstate_kd'";
		}else{
			$where .= "a.partmain_kd LIKE '%'";
		}
		
		$where .= " GROUP BY a.partmain_kd";
		$data['where'] = $where;
		
		return $data;
	}

	private function tbl_btn($id, $versi, $sts) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btns = array();
		$btns[] = '<li><a href="'. base_url().'manage_items/manage_part/part_versi?partmain_kd='.$id.'" title="Detail Versi" ><i class="fa fa-list"></i> Detail Versi</a></li>';
		if ($sts == 1) {
			if (cek_permission('PARTDATA_UPDATE')) {
				$btns[] = get_btn(array('title' => 'Edit Nama Part', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
			}
			if (cek_permission('PARTDATA_DUPLICATE')) {
				$btns[] = get_btn(array('title' => 'Duplikasi Part', 'icon' => 'clone', 'onclick' => 'duplicate_data(\''.$id.'\')'));
			}
			$btns[] = get_btn(array('title' => 'Lihat Default Part', 'icon' => 'search', 'onclick' => 'detail_data(\''.$id.'\',\''.$versi.'\')'));
			$btns[] = get_btn_divider();
			$btns[] = get_btn(array('title' => 'Ubah Tidak Aktif', 'icon' => 'times', 'onclick' => 'ubah_status(\''.$id.'\', \''.'0'.'\')'));	
		}else{
			$btns[] = get_btn(array('title' => 'Lihat Default Part', 'icon' => 'search', 'onclick' => 'detail_data(\''.$id.'\',\''.$versi.'\')'));
			$btns[] = get_btn_divider();
			$btns[] = get_btn(array('title' => 'Ubah Aktif', 'icon' => 'check', 'onclick' => 'ubah_status(\''.$id.'\',  \''.'1'.'\')'));	
		}
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	
	private function push_sap($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btn_group = '';

		$this->db->where(['partmain_kd' => $id]);
		$act = $this->db->get($this->tbl_name)->row_array();
		$param = 'bom';

		if (cek_permission('BOM_PUSH_SAP')) {
			// if($act['status_sap'] == '0'){
					$btn_group ='<button type="button" onclick="push_to_sap(\''.$id.'\', \''.$param.'\')" class="btn btn-p"><i class="fa fa-arrow-up"></i>&nbsp;Push To SAP</button>';
				// }else{
				// 	$btn_group ='<button type="button" onclick="push_to_sap(\''.$id.'\')" class="btn btn-success" disabled><i class="fa fa-check"></i>&nbsp;Done To SAP</button>';
				// }
		}

		//$btns = get_btn(array('title' => 'Push SAP', 'icon' => ' fa-arrow-up', 'onclick' => 'detail_data(\''.$id.'\')'));
	
		return $btn_group;
	}

	private function push_sap2($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btn_group = '';

		$this->db->where(['partmain_kd' => $id]);
		$act = $this->db->get($this->tbl_name)->row_array();
		$param = 'item';

		if (cek_permission('BOM_PUSH_SAP')) {
			if($act['status_sap_add_item'] == '0'){
					$btn_group ='<button type="button" onclick="push_to_sap(\''.$id.'\', \''.$param.'\')" class="btn btn-p"><i class="fa fa-arrow-up"></i>&nbsp;Push To SAP</button>';
				}else{
					$btn_group ='<button type="button" onclick="push_to_sap(\''.$id.'\')" class="btn btn-success" disabled><i class="fa fa-check"></i>&nbsp;Done To SAP</button>';
				}
		}

		//$btns = get_btn(array('title' => 'Push SAP', 'icon' => ' fa-arrow-up', 'onclick' => 'detail_data(\''.$id.'\')'));
	
		return $btn_group;
	}

	private function label_status ($partmain_status) {
		$status = 'Tidak Aktif';
		$type = 'danger';
		if ($partmain_status == 1) {
			$status = 'Aktif';
			$type = 'success';
		}
		return buildLabel($type, $status);
	}

	public function create_code() {
		$this->db->select('MAX('.$this->p_key.') AS code')
			->from($this->tbl_name);
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = (int) substr($code, -10);
		endif;
		$angka = $urutan + 1;
		$primary = 'MPRT'.str_pad($angka, 10, '0', STR_PAD_LEFT);
		return $primary;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->select($this->tbl_name.'.*, td_part_jenis.partjenis_nama')
				->from($this->tbl_name)
				->join('td_part_jenis', $this->tbl_name.'.partjenis_kd=td_part_jenis.partjenis_kd', 'left')
				->order_by($this->tbl_name.'.partmain_nama', 'asc')
				->get();
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

	public function get_by_param_detail ($param=[]) {
		$query = $this->db->select($this->tbl_name.'.*, td_part_jenis.*')
					->where($param)
					->from($this->tbl_name)
					->join('td_part_jenis', $this->tbl_name.'.partjenis_kd=td_part_jenis.partjenis_kd', 'left')
					->get();
		return $query;
	}

}