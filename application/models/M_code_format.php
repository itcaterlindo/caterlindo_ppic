<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class M_code_format extends CI_Model {
	public function read_data($var) {
		$this->db->select('code_length, code_format, code_for, code_separator, on_reset');
		$this->db->from('tb_code_format');
		$this->db->where(array('code_for' => $var));
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}
}