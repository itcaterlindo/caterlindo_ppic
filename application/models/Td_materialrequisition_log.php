<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_materialrequisition_log extends CI_Model {
	private $tbl_name = 'td_materialrequisition_log';
	private $p_key = 'materialreqlog_kd';

    public function create_code () {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_where_in ($where, $in = []) {
		$query = $this->db->where_in($where, $in)
				->select($this->tbl_name.'.*')
				->get($this->tbl_name);
		return $query;
	}

	public function update_batch ($idColumn, $data = []) {
		$act = $this->db->update_batch($this->tbl_name, $data, $idColumn);
		return $act;
	}

	public function get_log_printout($materialreq_kd) {
		$qmaxTrans = $this->db->select('c.wfstate_kd, MAX(a.materialreqlog_tglinput) AS tgl_maxtrans')
				->from($this->tbl_name.' AS a')
				->join('td_workflow_transition as b', 'a.wftransition_kd=b.wftransition_kd', 'left')
				->join('td_workflow_state as c', 'b.wftransition_destination=c.wfstate_kd', 'left')
				->where('a.materialreq_kd', $materialreq_kd)
				->group_by('c.wfstate_kd')
				->get_compiled_select();
				
		$query = $this->db->select('aa.*, c.wfstate_nama, cc.kd_karyawan, cc.nm_admin')
				->from($this->tbl_name.' AS aa')
				->join('td_workflow_transition as b', 'aa.wftransition_kd=b.wftransition_kd', 'left')
				->join('td_workflow_state as c', 'b.wftransition_destination=c.wfstate_kd', 'left')
				->join('tb_admin AS cc', 'aa.admin_kd=cc.kd_admin', 'left')
				->join('('.$qmaxTrans.') as tt', 'c.wfstate_kd=tt.wfstate_kd AND aa.materialreqlog_tglinput=tt.tgl_maxtrans', 'right')
				->where('aa.materialreq_kd', $materialreq_kd)
				->where('c.wfstate_onprintout', 1)
				->order_by('c.wfstate_onprintoutseq', 'asc')
				->get();
				
		return $query;
	}
	
}