<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_materialreceipt extends CI_Model
{
    private $tbl_name = 'tm_materialreceipt';
    private $p_key = 'materialreceipt_kd';

    public function ssp_table()
    {
        $data['table'] = $this->tbl_name;

        $data['primaryKey'] = $this->p_key;

        $data['columns'] = array(
            array(
                'db' => $this->p_key,
                'dt' => 1, 'field' => $this->p_key,
                'formatter' => function ($d, $row) {

                    return $this->tbl_btn($d, $row[8]);
                }
            ),
            array(
                'db' => 'a.materialreceipt_no',
                'dt' => 2, 'field' => 'materialreceipt_no',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'b.bagian_nama',
                'dt' => 3, 'field' => 'bagian_nama',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.materialreceipt_start',
                'dt' => 4, 'field' => 'materialreceipt_start',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.materialreceipt_end',
                'dt' => 5, 'field' => 'materialreceipt_end',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'c.wfstate_nama',
                'dt' => 6, 'field' => 'wfstate_nama',
                'formatter' => function ($d, $row) {
                    $d = build_badgecolor($row[7], $d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.materialreceipt_tgledit',
                'dt' => 7, 'field' => 'materialreceipt_tgledit',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'c.wfstate_badgecolor',
                'dt' => 8, 'field' => 'wfstate_badgecolor',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'c.wfstate_action',
                'dt' => 9, 'field' => 'wfstate_action',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
        );

        $data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM " . $this->tbl_name . " as a
								LEFT JOIN tb_bagian as b ON a.bagian_kd=b.bagian_kd
                                LEFT JOIN td_workflow_state as c ON c.wfstate_kd=a.wfstate_kd";
        // $kd_admin = $this->session->userdata('kd_admin');
        // $tipe_admin_kd = $this->session->userdata('tipe_admin_kd');
        // $bagians = $this->tb_deliverynote_user->get_bagianAllowed($kd_admin, $tipe_admin_kd);
        // $sBagians = implode(", ", $bagians);
        // $data['where'] = "a.bagian_kd IN ({$sBagians})";
        $data['where'] = "";

        return $data;
    }

    public function ssp_table2 ($aParam = []) 
    {
		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
        $xparam = $this->session->userdata('kd_admin');
        $admin = $this->db->where('kd_admin', $xparam)->get('tb_admin')->row_array();
		
		$data = [];
		$qData = $this->db->select($this->tbl_name.'.*, GROUP_CONCAT(COALESCE(tm_materialrequisition.materialreq_no) SEPARATOR ", ") as materialreq_nos, 
            td_workflow_state.wfstate_action, td_workflow_state.wfstate_badgecolor, td_workflow_state.wfstate_nama, tb_bagian.bagian_nama, td_materialreceipt_detail_rawmaterial.rmgr_code_srj')
				->from($this->tbl_name)
				->join('tb_materialreceipt_detail_requisition', $this->tbl_name.'.materialreceipt_kd=tb_materialreceipt_detail_requisition.materialreceipt_kd', 'left')
				->join('td_workflow_state', $this->tbl_name.'.wfstate_kd=td_workflow_state.wfstate_kd', 'left')
				->join('tm_materialrequisition', 'tb_materialreceipt_detail_requisition.materialreq_kd=tm_materialrequisition.materialreq_kd', 'left')
				->join('tb_bagian', $this->tbl_name.'.bagian_kd=tb_bagian.bagian_kd', 'left')
                ->join(' (SELECT materialreceiptdetailrm_kd, materialreceipt_kd, GROUP_CONCAT( DISTINCT rmgr_code_srj SEPARATOR ", ") as rmgr_code_srj FROM td_materialreceipt_detail_rawmaterial WHERE rmgr_code_srj IS NOT NULL AND rmgr_code_srj != "" GROUP BY materialreceipt_kd) td_materialreceipt_detail_rawmaterial', 'td_materialreceipt_detail_rawmaterial.materialreceipt_kd='.$this->tbl_name.'.materialreceipt_kd', 'left');
                
               
                if($admin['tipe_admin_kd'] == "TPA271021003"){
                    $qData = $qData->where('tb_bagian.bagian_kd', '10');
                }else if($admin['tipe_admin_kd'] == "TPA271021002"){
                    $qData = $qData->where('tb_bagian.bagian_kd', '20');
                }else if($admin['tipe_admin_kd'] == "TPA271021001"){
                    $qData = $qData->where('tb_bagian.bagian_kd', '40');
                }
                else if($admin['tipe_admin_kd'] == "TPA081021001"){
                    $qData = $qData->where('tb_bagian.bagian_kd', '50');
                }
                else if($admin['tipe_admin_kd'] == "TPA071021001"){
                    $qData = $qData->where('tb_bagian.bagian_kd', '60');
                }

		$qData = $qData->group_by($this->tbl_name.'.materialreceipt_kd')->order_by($this->tbl_name.'.materialreceipt_tgledit', 'DESC')->get();
		foreach ($qData->result_array() as $r) {
			$data[] = [
				'1' => $this->tbl_btn($r['materialreceipt_kd'], $r['wfstate_action']),
				'2' => $r['materialreceipt_tanggal'],
				'3' => $r['bagian_nama'],
				'4' => $r['materialreceipt_no'],
				'5' => $r['materialreq_nos'],
                '6' => $r['rmgr_code_srj'],
				'7' => build_badgecolor($r['wfstate_badgecolor'], $r['wfstate_nama']),
				'8' => $r['materialreceipt_tgledit'],
			];
		}
		
		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

    private function tbl_btn($id, $wfstate_action)
    {
        $btns = array();
        if (!empty($wfstate_action)){
			$exp = explode(';', $wfstate_action);
            if (in_array('view', $exp)){    
                $btns[] = get_btn(array('title' => 'Lihat Item', 'icon' => 'list', 'onclick' => 'lihat_data(\'' . $id . '\')'));
            }
            if (in_array('edit', $exp)){
                if (cek_permission('MATERIALRECEIPT_UPDATE')) {
                    $btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\'' . $id . '\')'));
                }
            }
            if (in_array('print', $exp)){    
                $btns[] = get_btn(array('title' => 'Cetak Item', 'icon' => 'print', 'onclick' => 'cetak_data(\'' . $id . '\')'));
                $btns[] = get_btn(array('title' => 'Cetak Item Detail by WO', 'icon' => 'print', 'onclick' => 'cetak_item_detail_by_wo(\'' . $id . '\')'));
            }
            if (in_array('delete', $exp)){    
                if (cek_permission('MATERIALRECEIPT_DELETE')) {
                    $btns[] = get_btn_divider();
                    $btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\'' . $id . '\')'));
                }
            }
        }

        $btn_group = group_btns($btns);

        return $btn_group;
    }

    public function insert_data($data)
    {
        $query = $this->db->insert($this->tbl_name, $data);
        return $query ? TRUE : FALSE;
    }

    public function delete_data($id)
    {
        $query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
        return $query ? TRUE : FALSE;
    }

    public function get_by_param($param = [])
    {
        $this->db->where($param);
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    public function get_all()
    {
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    public function update_data($aWhere = [], $data)
    {
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
        return $query ? TRUE : FALSE;
    }

    public function create_code()
    {
        $query = $this->db->select('MAX(' . $this->p_key . ') as maxID')
            ->get($this->tbl_name)
            ->row();
        $code = (int) $query->maxID + 1;
        return $code;
    }

    public function generate_no()
    {
        $num = 1;
        $qSeq = $this->db->where('YEAR(materialreceipt_tglinput)', date('Y'))
            ->order_by('materialreceipt_kd', 'desc')
            ->get($this->tbl_name)->row_array();
        if (!empty($qSeq)) {
            $explodeNum = explode('-', $qSeq['materialreceipt_no']);
            $num = (int) $explodeNum[2];
            $num++;
        }
        $woplanningNo = "MRC-" . date('Y') . '-' . str_pad($num, 4, '0', STR_PAD_LEFT);
        return $woplanningNo;
    }

    public function insert_batch($data)
    {
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
    }

    public function get_by_param_in($param, $params = [])
    {
        $this->db->where_in($param, $params);
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    public function get_by_param_bom_in($param, $params)
    {
        $query = $this->db->select($this->tbl_name . '.*, tm_bom.bom_kd')
            ->where_in($param, $params)
            ->join('tm_bom', $this->tbl_name . '.project_kd=tm_bom.project_kd', 'left')
            ->get($this->tbl_name);
        return $query;
    }

    public function get_by_param_reqdetail($param = []) 
    {
        $query = $this->db->where($param)
            ->join('tb_materialreceipt_detail_requisition', $this->tbl_name . '.materialreceipt_kd=tb_materialreceipt_detail_requisition.materialreceipt_kd', 'left')
            ->join('tm_materialrequisition', 'tb_materialreceipt_detail_requisition.materialreq_kd=tm_materialrequisition.materialreq_kd', 'left')
            ->join('td_workflow_state', $this->tbl_name . '.wfstate_kd=td_workflow_state.wfstate_kd', 'left')
            ->get($this->tbl_name);
        return $query;
    }

    public function getRowmaterialreceiptBagianState($id)
    {
        $qRow = $this->db->where('tm_materialreceipt.materialreceipt_kd', $id)
            ->join('td_workflow_state', 'td_workflow_state.wfstate_kd=tm_materialreceipt.wfstate_kd', 'left')
            ->join('tb_admin', 'tb_admin.kd_admin=tm_materialreceipt.materialreceipt_originator', 'left')
            ->join('tb_bagian', 'tb_bagian.bagian_kd=tm_materialreceipt.bagian_kd', 'left')
            ->get('tm_materialreceipt')->row_array();
        return $qRow;
    }

    public function generate_button ($id, $wf_kd, $wfstate_kd) {
		$admin_kd = $this->session->userdata('kd_admin');
		$admin = $this->db->where('kd_admin', $admin_kd)->get('tb_admin')->row_array();
		$kd_tipe_admin = $admin['tipe_admin_kd'];

		$transitions = $this->db->select('td_workflow_transition.*, td_workflow_transition_permission.wftransitionpermission_adminkd, td_workflow_transition_permission.kd_tipe_admin')
					->from('td_workflow_transition')
					->join('td_workflow_transition_permission', 'td_workflow_transition.wftransition_kd=td_workflow_transition_permission.wftransition_kd','left')
					->where('td_workflow_transition.wf_kd', $wf_kd)
					->where('td_workflow_transition.wftransition_source', $wfstate_kd)
					->where('td_workflow_transition.wftransition_active', '1')
					->get()->result_array();
	
		$html = '<div class="btn-group">
		<button type="button" class="btn btn-warning btn-flat dropdown-toggle" data-toggle="dropdown">
			<span class="fa fa fa-hand-o-right"></span>
			Ubah State
		  	<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">';
		$allowTransitions = [];
		foreach ($transitions as $transition) {
			if ($transition['wftransitionpermission_adminkd'] == $admin_kd || $transition['kd_tipe_admin'] == $kd_tipe_admin){
				$allowTransitions[] = [
					'wftransition_kd' => $transition['wftransition_kd'],
					'wf_kd' => $transition['wf_kd'],
					'wftransition_nama' => $transition['wftransition_nama'],
					'wftransition_email' => $transition['wftransition_email'],
				];
			}
		}
		#Group by wftransition_kd
		$tempArrayExist = []; $arrayGroups = [];
		foreach ($allowTransitions as $allowTransition) {
			if (!in_array($allowTransition['wftransition_kd'], $tempArrayExist)){
				$arrayGroups[] = $allowTransition;
				$tempArrayExist[] = $allowTransition['wftransition_kd'];
			}
		}
		#after group
		foreach ($arrayGroups as $arrayGroup) {
			$html .= '<li><a href="javascript:void(0)" onclick=ubah_state("'.$id.'",'.$arrayGroup['wftransition_kd'].')> <i class="fa fa-hand-o-right"></i> '.$arrayGroup['wftransition_nama'].'</a></li>';
		}
		
		$html .= '
		</ul>
	  </div>';

		return $html;

	}
}
