<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Quotation extends CI_Model {
	function quotatationItems($kd_mquotatation) {
		$this->db->select('a.kd_ditem_quotation, a.mquotation_kd, a.barang_kd, a.item_status, a.item_desc, a.item_dimension, a.item_qty, a.harga_barang, a.harga_retail, a.disc_type, a.item_disc, a.total_harga, a.item_note, a.item_code');
		$this->db->from('td_quotation_item a');
		$this->db->join('tm_barang b', 'b.kd_barang = a.barang_kd', 'left');
		$this->db->where(array('a.mquotation_kd' => $kd_mquotatation));
		$this->db->order_by('a.kd_ditem_quotation ASC');
		$query = $this->db->get();
		$result = $query->result();
		$num = $query->num_rows();

		return $num > 0?$result:$num;
	}

	function item_child($ditem_quotation_kd) {
		$this->db->select('a.kd_citem_quotation, a.ditem_quotation_kd, a.kd_parent, a.kd_child, a.item_status, a.item_desc, a.item_dimension, a.item_qty, a.harga_barang, a.harga_retail, a.disc_type, a.item_disc, a.total_harga, a.item_note, a.item_code');
		$this->db->from('td_quotation_item_detail a');
		$this->db->join('tm_barang b', 'b.kd_barang = a.kd_parent', 'left');
		$this->db->join('tm_barang c', 'c.kd_barang = a.kd_child', 'left');
		$this->db->where_in('a.ditem_quotation_kd', $ditem_quotation_kd);
		$this->db->order_by('a.kd_citem_quotation ASC');
		$query = $this->db->get();
		$result = $query->result_array();
		$num = $query->num_rows();

		return $num > 0?$result:$num;
	}

	function quotation_master_detail($kd_quotation) {
		$this->db->select('a.no_quotation, a.email_sales, a.mobile_sales, a.tgl_quotation, a.status_quotation, a.nm_kolom_ppn, a.jml_ppn, b.nm_salesperson, c.nm_customer, c.no_telp_utama, c.no_telp_lain, c.email, c.alamat, c.kode_pos, e.nm_negara, f.nm_provinsi, g.nm_kota, h.nm_kecamatan, i.nm_select, j.kd_jenis_customer, j.term_payment_format, j.set_ppn, j.custom_barcode, k.nm_select AS tipe_customer, m.currency_kd, n.currency_type, n.icon_type, n.currency_icon, n.pemisah_angka, n.decimal, n.format_akhir, o.nm_select as header_customer, c.code_customer');
		$this->db->from('tm_quotation a');
		$this->db->join('tb_salesperson b', 'b.kd_salesperson = a.salesperson_kd', 'left');
		$this->db->join('tm_customer c', 'c.kd_customer = a.customer_kd', 'left');
		$this->db->join('td_customer_alamat d', 'd.kd_alamat_kirim = a.alamat_kirim_kd', 'left');
		$this->db->join('tb_negara e', 'e.kd_negara = d.negara_kd', 'left');
		$this->db->join('tb_provinsi f', 'f.kd_provinsi = d.provinsi_kd AND f.negara_kd = d.negara_kd', 'left');
		$this->db->join('tb_kota g', 'g.kd_kota = d.kota_kd AND g.provinsi_kd = d.provinsi_kd AND g.negara_kd = d.negara_kd', 'left');
		$this->db->join('tb_kecamatan h', 'h.kd_kecamatan = d.kecamatan_kd AND h.kota_kd = d.kota_kd AND h.provinsi_kd = d.provinsi_kd AND h.negara_kd = d.negara_kd', 'left');
		$this->db->join('tb_set_dropdown i', 'i.id = d.kd_badan_usaha', 'left');
		$this->db->join('tb_jenis_customer j', 'j.kd_jenis_customer = c.jenis_customer_kd', 'left');
		$this->db->join('tb_set_dropdown k', 'k.id = j.kd_manage_items', 'left');
		$this->db->join('tb_set_dropdown l', 'l.id = j.kd_price_category', 'left');
		$this->db->join('tb_tipe_harga m', 'm.nm_harga = l.nm_select', 'left');
		$this->db->join('tm_currency n', 'n.kd_currency = m.currency_kd', 'left');
		$this->db->join('tb_set_dropdown o', 'o.id = c.kd_badan_usaha', 'left');
		$this->db->where(array('a.kd_mquotation' => $kd_quotation));
		$query = $this->db->get();
		$result = $query->row();
		$num = $query->num_rows();

		return $num > 0?$result:FALSE;
	}

	function get_price($kd_mquotation = '') {
		$this->db->from('td_quotation_harga');
		if (is_array($kd_mquotation)) :
			$this->db->where_in('mquotation_kd', $kd_mquotation);
		else :
			$this->db->where(array('mquotation_kd' => $kd_mquotation));
		endif;
		$query = $this->db->get();
		$num = $query->num_rows();
		$result = $query->result();

		return $result;
	}

	function get_item($kd_mquotation) {
		$this->db->select('kd_ditem_quotation, mquotation_kd, barang_kd, item_code, item_barcode, item_status, item_desc, item_dimension, item_qty, harga_barang, harga_retail, disc_type, item_disc, total_harga, item_note');
		$this->db->from('td_quotation_item');
		$this->db->where(array('mquotation_kd' => $kd_mquotation));
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	function get_item_detail($kd_mquotation) {
		$this->db->select('kd_citem_quotation, ditem_quotation_kd, mquotation_kd, kd_parent, kd_child, item_code, item_barcode, item_status, item_desc, item_dimension, item_qty, harga_barang, harga_retail, disc_type, item_disc, total_harga, item_note');
		$this->db->from('td_quotation_item_detail');
		$this->db->where(array('mquotation_kd' => $kd_mquotation));
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	function get_harga($kd_mquotation) {
		$this->db->select('kd_dharga_quotation, mquotation_kd, nm_kolom, type_kolom, total_nilai');
		$this->db->from('td_quotation_harga');
		$this->db->where(array('mquotation_kd' => $kd_mquotation));
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	function quotation_item_list($kd_mquotation) {
		$this->db->select('a.kd_ditem_quotation, a.mquotation_kd, a.barang_kd, a.item_status, a.item_desc, a.item_dimension, a.item_qty, a.harga_barang, a.harga_retail, a.disc_type, a.item_disc, a.total_harga, a.item_note, a.item_code');
		$this->db->from('td_quotation_item a');
		$this->db->join('tm_barang b', 'b.kd_barang = a.barang_kd', 'left');
		if (is_array($kd_mquotation)) :
			$this->db->where_in('a.mquotation_kd', $kd_mquotation);
		else :
			$this->db->where(array('a.mquotation_kd' => $kd_mquotation));
		endif;
		$this->db->order_by('a.kd_ditem_quotation ASC');
		$query = $this->db->get();
		$result = $query->result();
		$num = $query->num_rows();

		return $result;
	}

	function get_parentitem_byitemkd($item_kd = '') {
		$this->db->select('a.kd_ditem_quotation, a.mquotation_kd, a.barang_kd, a.item_code, a.item_barcode, a.item_status, a.item_desc, a.item_dimension, a.netweight, a.grossweight, a.boxweight, a.length_cm, a.width_cm, a.height_cm, a.item_cbm, a.item_qty, a.harga_barang, a.harga_retail, a.disc_type, a.item_disc, a.total_harga, a.item_note, a.item_code')
			->from('td_quotation_item a')
			->join('tm_barang b', 'b.kd_barang = a.barang_kd', 'left')
			->order_by('a.kd_ditem_quotation ASC');
		if (is_array($item_kd)) :
			$this->db->where_in('a.kd_ditem_quotation', $item_kd);
		else :
			$this->db->where(array('a.kd_ditem_quotation' => $item_kd));
		endif;
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	function get_childitem_byitemkd($item_kd = '') {
		$this->db->select('a.kd_citem_quotation, a.ditem_quotation_kd, a.mquotation_kd, a.kd_parent, a.kd_child, a.item_code, a.item_barcode, a.item_status, a.item_desc, a.item_dimension, a.netweight, a.grossweight, a.boxweight, a.length_cm, a.width_cm, a.height_cm, a.item_cbm, a.item_qty, a.harga_barang, a.harga_retail, a.disc_type, a.item_disc, a.total_harga, a.item_note, a.item_code')
			->from('td_quotation_item_detail a')
			->join('tm_barang b', 'b.kd_barang = a.kd_parent', 'left')
			->join('tm_barang c', 'c.kd_barang = a.kd_child', 'left')
			->order_by('a.kd_citem_quotation ASC');
		if (is_array($item_kd)) :
			$this->db->where_in('a.kd_citem_quotation', $item_kd);
		else :
			$this->db->where(array('a.kd_citem_quotation' => $item_kd));
		endif;
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	function trans_quo_so($kd_mquotation = '') {
		$this->load->model(array('tm_salesorder'));
		$this->db->from('tm_quotation')->where(array('kd_mquotation' => $kd_mquotation));
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			$master['kd_msalesorder'] = $row->kd_mquotation;
			$master['mquotation_kd'] = $row->kd_mquotation;
			$master['salesperson_kd'] = $row->salesperson_kd;
			$master['email_sales'] = $row->email_sales;
			$master['mobile_sales'] = $row->mobile_sales;
			$master['no_salesorder'] = $this->tm_salesorder->create_nosalesorder('salesorderlocal', 'Lokal');
			$master['tgl_so'] = date('Y-m-d');
			$master['tipe_so'] = 'lokal';
			$master['customer_kd'] = $row->customer_kd;
			$master['tipe_customer'] = $row->tipe_customer;
			$master['tipe_harga'] = $row->tipe_harga;
			$master['alamat_kirim_kd'] = $row->alamat_kirim_kd;
			$master['status_so'] = 'pending';
			$master['nm_kolom_ppn'] = $row->nm_kolom_ppn;
			$master['jml_ppn'] = $row->jml_ppn;
			$master['jml_ongkir'] = $row->jml_ongkir;
			$master['jml_install'] = $row->jml_install;
			$master['order_tipe'] = 'new';
			$master['tgl_input'] = date('Y-m-d');
			$master['admin_kd'] = $this->session->userdata('kd_admin');
		endif;
	}
}