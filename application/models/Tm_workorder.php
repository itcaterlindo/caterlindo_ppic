<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_workorder extends CI_Model
{
	private $tbl_name = 'tm_workorder';
	private $p_key = 'wo_kd';

	/* --start ssp tabel untuk modul ppic-- */
	public function ssp_table()
	{
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array(
				'db' => 'a.' . $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function ($d, $row) {

					return $this->tbl_btn($d, $row[4]);
				}
			),
			array(
				'db' => 'a.wo_tanggal',
				'dt' => 2, 'field' => 'wo_tanggal',
				'formatter' => function ($d) {
					$d = format_date($d, 'Y-m-d');
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'a.wo_noterbit',
				'dt' => 3, 'field' => 'wo_noterbit',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'GROUP_CONCAT( IF (c.tipe_customer = "Lokal", c.no_salesorder, c.no_po) SEPARATOR ", ") AS no_salesorders',
				'dt' => 4, 'field' => 'no_salesorders',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'a.wo_status',
				'dt' => 5, 'field' => 'wo_status',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);
					$word = process_status($d);
					$color = color_status($d);
					$d = bg_label($word, $color);

					return $d;
				}
			),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM tm_workorder AS a
							LEFT JOIN tb_relasi_sowo as b ON a.wo_kd=b.wo_kd
							LEFT JOIN tm_salesorder as c ON c.kd_msalesorder=b.kd_msalesorder
							GROUP BY a.wo_kd";
		$data['where'] = "";

		return $data;
	}
	/* --end ssp tabel untuk modul ppic-- */

	private function tbl_btn($id, $wo_status)
	{
		$btns = array();
		$btns[] = get_btn(array('title' => 'Detail Item', 'icon' => 'list', 'onclick' => 'detail_wo_item(\'' . $id . '\')'));
		if ($wo_status == 'process_wo') {
			$btns[] = get_btn(array('title' => 'Detail Work Order', 'icon' => 'list', 'onclick' => 'itemdetail_box(\'' . $id . '\')'));
			// $btns[] = get_btn(array('title' => 'Cetak Work Order', 'icon' => 'print', 'onclick' => 'cetak_wo(\''.$id.'\')'));
		}
		if ($wo_status == 'pending') {
			$btns[] = get_btn_divider();
			$btns[] = get_btn(array('title' => 'Cancel Work Order', 'icon' => 'times-circle', 'onclick' => 'cancel_wo(\'' . $id . '\')'));
		}

		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code()
	{
		$this->db->select($this->p_key . ' AS code')
			->from($this->tbl_name)
			->where(array('DATE(wo_tglinput)' => date('Y-m-d')))
			->order_by('wo_tglinput DESC, ' . $this->p_key . ' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -6);
		endif;
		$angka = $urutan + 1;
		$primary = 'MWO' . date('ymd') . str_pad($angka, 6, '0', STR_PAD_LEFT);
		return $primary;
	}

	public function insert_data($data)
	{
		$query = $this->db->insert($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_data($id)
	{
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		return $query ? TRUE : FALSE;
	}

	public function get_by_param($param = [])
	{
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all()
	{
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data($aWhere = [], $data)
	{
		$query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query ? TRUE : FALSE;
	}

	public function insert_batch_data($data = [])
	{
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_by_param($param = [])
	{
		$query = $this->db->delete($this->tbl_name, $param);
		return $query ? TRUE : FALSE;
	}

	public function get_where_in($where, $in = [])
	{
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}

	public function get_by_param_detail($param = [])
	{
		$query = $this->db->select($this->tbl_name . '.*, tm_salesorder.no_salesorder, tm_salesorder.no_po, tm_salesorder.tipe_customer')
			->from($this->tbl_name)
			->join('tb_relasi_sowo', $this->tbl_name . '.wo_kd=tb_relasi_sowo.wo_kd', 'left')
			->join('tm_salesorder', 'tm_salesorder.kd_msalesorder=tb_relasi_sowo.kd_msalesorder', 'left')
			->where($param)
			->get();
		return $query;
	}

	public function create_noterbit()
	{
		$query = $this->db->select('COUNT(wo_kd) as countWO')
			->where(['DATE(wo_tglinput)' => date('Y-m-d')])
			->get($this->tbl_name)->row();
		$countWO = (int) $query->countWO;
		if (empty($countWO)) {
			$num = 1;
		} else {
			$num = $countWO + 1;
		}
		$wo_noterbit = 'WO-' . date('ymd') . '-' . str_pad($num, 2, '0', STR_PAD_LEFT);
		return $wo_noterbit;
	}

	public function getOutstandingWO($data)
	{
		$items = $this->db->select('net_needed')
			->from('(SELECT tdw.woitem_kd,
		tmr.rm_kode,
		tdw.woitem_qty, 
		tdpd.partdetail_qty, 
		tmr.rm_nama,
		tmw.wo_tglinput,
		tmr.rm_deskripsi, 
		ROUND((tdw.woitem_qty * tdpd.partdetail_qty), 1) AS many_needed,
		SUM(ROUND((tdw.woitem_qty * tdpd.partdetail_qty), 1)) AS net_needed
		FROM tm_workorder tmw
		INNER JOIN (SELECT * FROM td_workorder_item WHERE woitem_itemcode != \'CUSTOM ITEM\') AS tdw ON tdw.wo_kd = tmw.wo_kd
		INNER JOIN tm_barang AS tmb ON tmb.item_code = tdw.woitem_itemcode
		INNER JOIN tm_bom AS tmbo ON tmbo.kd_barang = tmb.kd_barang
		INNER JOIN td_bom_detail AS tdbo ON tdbo.bom_kd = tmbo.bom_kd
		INNER JOIN tm_part_main AS tmp ON tdbo.partmain_kd = tmp.partmain_kd
		INNER JOIN td_part AS tdp ON tmp.partmain_kd = tdp.partmain_kd
		INNER JOIN td_part_detail AS tdpd ON tdp.part_kd = tdpd.part_kd
		INNER JOIN tm_rawmaterial AS tmr ON tmr.rm_kd = tdpd.rm_kd
		WHERE tmw.wo_tglinput >= now()-interval 1 month GROUP BY tmr.rm_kode) outstand_wo')
			->where(array('rm_kode' => $data))
			->get()->row_array();
		return $items['net_needed'];
	}

	public function outstanding_wo_suggest($rm_kd)
	{	
		/** get data di WO dengan status workorder */
		$itemwo = $this->db->query("SELECT td_workorder_item.woitem_itemcode, 
		sum(td_workorder_item.woitem_qty) as qty_wo, 
		td_workorder_item.woitem_prosesstatus
		FROM td_workorder_item 
		left join tm_rawmaterial on td_workorder_item.woitem_itemcode = tm_rawmaterial.rm_kode 
		where td_workorder_item.woitem_prosesstatus = 'workorder' 
		group by td_workorder_item.woitem_itemcode ")
		->result();

		/** get data di DN sebagai pengurang qty dari WO */
		$itemdn = $this->db->query("SELECT td_workorder_item.woitem_kd,
		td_workorder_item.woitem_itemcode, 
		td_workorder_item.woitem_qty as qty_wo, 
		td_deliverynote_detail.dndetail_qty as qty_dn, 
		td_workorder_item.woitem_prosesstatus,
		tm_deliverynote.dn_tujuan 
		FROM td_workorder_item 
		LEFT JOIN td_deliverynote_detail on td_workorder_item.woitem_kd = td_deliverynote_detail.woitem_kd 
		left JOIN tm_deliverynote ON td_deliverynote_detail.dn_kd = tm_deliverynote.dn_kd 
		left join tm_rawmaterial on td_workorder_item.woitem_itemcode = tm_rawmaterial.rm_kode 
		where td_workorder_item.woitem_prosesstatus = 'workorder' 
		ORDER BY `td_workorder_item`.`woitem_itemcode` DESC")
		->result();

		$result = [];
		foreach($itemwo as $w){
			$sisa_wo = $w->qty_wo;
			foreach($itemdn as $d){
				if(($w->woitem_itemcode == $d->woitem_itemcode) and ($d->dn_tujuan == '80')){
					$sisa_wo -= $d->qty_dn;
				}
			}
			array_push($result, array('woitem_itemcode' => $w->woitem_itemcode, 'sisa_wo' => $sisa_wo));
		}

		// echo json_encode($result);exit();

		return $result;
	}
}
