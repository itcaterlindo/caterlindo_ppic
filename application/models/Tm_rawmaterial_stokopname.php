<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_rawmaterial_stokopname extends CI_Model {
	private $tbl_name = 'tm_rawmaterial_stokopname';
	private $p_key = 'rmstokopname_kd';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){
					
					return $this->tbl_btn($d, $row[7]);
				}),
			array( 'db' => 'a.rmstokopname_kode', 
				'dt' => 2, 'field' => 'rmstokopname_kode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.rmstokopnamejenis_nama', 
				'dt' => 3, 'field' => 'rmstokopnamejenis_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.rmstokopname_periode', 
				'dt' => 4, 'field' => 'rmstokopname_periode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'a.rmstokopname_status', 
				'dt' => 5, 'field' => 'rmstokopname_status',
				'formatter' => function ($d){
					$d = $this->generateSpanStatus($d);
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.rmstokopname_note', 
				'dt' => 6, 'field' => 'rmstokopname_note',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.rmstokopname_tgledit', 
				'dt' => 7, 'field' => 'rmstokopname_tgledit',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.rmstokopname_status', 
				'dt' => 8, 'field' => 'rmstokopname_status',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
							LEFT JOIN td_rawmaterial_stokopname_jenis as b ON b.rmstokopnamejenis_kd=a.rmstokopnamejenis_kd";
        $data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id, $rmstokopname_status) {
		$btns = array();
		$btns[] = get_btn(array('title' => 'Lihat', 'icon' => 'list', 'onclick' => 'view_opname(\''.$id.'\')'));	
		if ($rmstokopname_status == 'editing') {
			$btns[] = get_btn(array('title' => 'Set Active', 'icon' => 'check', 'onclick' => 'action_set_status(\''.$id.'\', \''.'active'.'\')'));	
			$btns[] = get_btn(array('title' => 'Set Cancel', 'icon' => 'times', 'onclick' => 'action_set_status(\''.$id.'\', \''.'cancel'.'\')'));	
			$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));	
			$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));	
		}
		if ($rmstokopname_status == 'active') {
			$btns[] = get_btn(array('title' => 'Set Editing', 'icon' => 'bookmark', 'onclick' => 'action_set_status(\''.$id.'\', \''.'editing'.'\')'));	
		}
		
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function generateSpanStatus($rmstokopname_status){
		switch ($rmstokopname_status) {
			case 'editing':
				$build_badge = build_span('warning', $rmstokopname_status);
				break;
			case 'active':
				$build_badge = build_span('success', $rmstokopname_status);
				break;
			case 'finish':
				$build_badge = build_span('primary', $rmstokopname_status);
				break;
			case 'cancel':
				$build_badge = build_span('danger', $rmstokopname_status);
				break;
			default:
				$build_badge = build_span('warning', $rmstokopname_status);
		}
		return $build_badge;
	}

	public function create_code() {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

	public function create_no($rmstokopname_periode) { 
		$no = "RMOPNAME-".date('YmdHm', strtotime($rmstokopname_periode));
		return $no;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	

	public function get_by_param_detail($param = []){
		$query = $this->db->select($this->tbl_name.'.*, td_rawmaterial_stokopname_jenis.*')
					->from($this->tbl_name)
					->join('td_rawmaterial_stokopname_jenis', $this->tbl_name.'.rmstokopnamejenis_kd=td_rawmaterial_stokopname_jenis.rmstokopnamejenis_kd', 'left')
					->where($param)
					->get();
		return $query;
    }
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	// public function get_where_in ($where, $in = []){
	// 	$query = $this->db->where_in($where, $in)
	// 			->join('tm_purchaseorder', $this->tbl_name.'.po_kd=tm_purchaseorder.po_kd', 'left')
	// 			->select($this->tbl_name.'.*, tm_purchaseorder.*')
	// 			->get($this->tbl_name);
	// 	return $query;
	// }

	
}