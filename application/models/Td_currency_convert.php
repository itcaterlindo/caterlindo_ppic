<?php
defined('BASEPATH') or exit('No direct script acccess allowed!');

class Td_currency_convert extends CI_Model {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'td_currency_convert';
	private $p_key = 'kd_currency_convert';
	private $title = 'Currency Convert';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){

					return $this->tbl_btn($d, $row[1]);
				} ),
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 2, 'field' => $this->p_key,
				'formatter' => function($d){

					return $d;
				} ),
			array( 'db' => 'b.currency_nm AS nm_primary',
				'dt' => 3, 'field' => 'nm_primary',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				} ),
			array( 'db' => 'c.currency_nm AS nm_secondary',
				'dt' => 4, 'field' => 'nm_secondary',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				} ),
			array( 'db' => 'a.tgl_berlaku',
				'dt' => 5, 'field' => 'tgl_berlaku',
				'formatter' => function($d){
					$d = format_date($d, 'd-m-Y H:i:s');

					return $d;
				} ),
			array( 'db' => 'a.convert_value',
				'dt' => 6, 'field' => 'convert_value',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				} ),
		);

		$data['sql_details'] = sql_connect();

		$data['joinQuery'] = "
			FROM ".$this->tbl_name." a
			LEFT JOIN tm_currency b ON b.kd_currency = a.primary_kd
			LEFT JOIN tm_currency c ON c.kd_currency = a.secondary_kd
		";
		$data['where'] = "row_flag = 'active'";
		return $data;
	}

	function tbl_btn($id, $var) {
		$read_access = $this->session->read_access;
		$update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
		$btns[] = get_btn_divider();
		$btns[] = get_btn(array('access' => $delete_access, 'title' => 'Hapus', 'icon' => 'trash', 'onclick' => 'return confirm(\'Anda akan menghapus Currency Convert = '.$var.'?\')?hapus_data(\''.$id.'\'):false'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	function get_all() {
		$this->db->select('kd_currency_convert, primary_kd, secondary_kd, tgl_berlaku, convert_value');
		$this->db->from($this->tbl_name);
		$this->db->where(array('row_flag' => 'active'));
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	function get_data($code) {
		$this->db->select('kd_currency_convert, primary_kd, secondary_kd, tgl_berlaku, convert_value');
		$this->db->from($this->tbl_name);
		$this->db->where(array($this->p_key => $code));
		$query = $this->db->get();
		$result = $query->row();
		return $result;
	}

	function send_data($data) {
		if (empty($data[$this->p_key])) :
			$label_err = 'menambahkan';
			$data[$this->p_key] = $this->create_pkey();
			$act = $this->input_data($data);
		else :
			$label_err = 'mengubah';
			$act = $this->update_data($data, array($this->p_key => $data[$this->p_key]));
		endif;
		
		if ($act) :
			$log_stat = 'berhasil '.$label_err.' '.$this->title;
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$log_stat.'!');
		else :
			$log_stat = 'gagal '.$label_err.' '.$this->title;
			$str['confirm'] = 'error';
			$str['idErrForm'] = buildAlert('danger', 'Gagal!', 'Gagal '.$log_stat.', kesalahan sistem!');
		endif;
		$this->m_builder->write_log($log_stat, $label_err, $data);

		return $str;
	}

	function create_pkey() {
		$conds = array(
			'select' => 'tgl_input, '.$this->p_key,
			'order_by' => 'tgl_input DESC',
			'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
		);
		$pkey = $this->m_builder->buat_kode($this->tbl_name, $conds, 3, 'DCC');
		return $pkey;
	}

	function input_data($data) {
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s'), 'row_flag' => 'active');
		$submit = array_merge($data, $data_adm);
		$aksi = $this->db->insert($this->tbl_name, $submit);
		return $aksi?TRUE:FALSE;
	}

	function update_data($data, $where) {
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s'));
		$submit = array_merge($data, $data_adm);
		$aksi = $this->db->update($this->tbl_name, $submit, $where);
		return $aksi?TRUE:FALSE;
	}

	function get_value($kd_currency, $tgl) {
		$this->db->select('convert_value');
		$this->db->from($this->tbl_name);
		$this->db->where(array('tgl_berlaku <=' => $tgl, 'secondary_kd' => $kd_currency, 'row_flag' => 'active'));
		$this->db->order_by('tgl_input DESC, kd_currency_convert DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$row = $query->row();
		$val = !empty($row->convert_value)?$row->convert_value:'1';
		return $val;
	}
}