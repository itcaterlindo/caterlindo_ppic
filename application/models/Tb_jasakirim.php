<?php
defined('BASEPATH') or exit('No direct script acccess allowed!');

class Tb_jasakirim extends CI_Model {
	private $tbl_name = 'tb_jasakirim';
	private $p_key = 'kd_jasakirim';
	private $title_name = 'Data Jasa Kirim';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;
		$data['primaryKey'] = $this->p_key;
		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key, 'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){
					return $this->tbl_btn($d, $row[1]);
				}
			),
			array( 'db' => 'a.nm_jasakirim', 'dt' => 2, 'field' => 'nm_jasakirim'),
			array( 'db' => 'a.alamat', 'dt' => 3, 'field' => 'alamat', 'formatter' => function ($d, $row) {
				$kecamatan = $row[3].after_before_char($row[3], array($row[4], $row[5], $row[6]), ', ', '.');
				$kota = $row[4].after_before_char($row[4], array($row[5], $row[6]), ', ', '.');
				$provinsi = $row[5].after_before_char($row[5], $row[6], ', ', '.');
				$negara = $row[6].after_before_char($row[6], $row[6], '.', '.');
				$alamat = $d.'<br>'.$kecamatan.$kota.$provinsi.$negara.'<br>Kode Pos : '.$row[7];
				return $alamat;
			}),
			array( 'db' => 'e.nm_kecamatan', 'dt' => 4, 'field' => 'nm_kecamatan'),
			array( 'db' => 'd.nm_kota', 'dt' => 5, 'field' => 'nm_kota'),
			array( 'db' => 'c.nm_provinsi', 'dt' => 6, 'field' => 'nm_provinsi'),
			array( 'db' => 'b.nm_negara', 'dt' => 7, 'field' => 'nm_negara'),
			array( 'db' => 'a.kode_pos', 'dt' => 8, 'field' => 'kode_pos'),
			array( 'db' => 'a.tgl_input', 'dt' => 9, 'field' => 'tgl_input', 'formatter' => function ($d){return format_date($d, 'd-m-Y H:i:s');}),
			array( 'db' => 'a.tgl_edit', 'dt' => 10, 'field' => 'tgl_edit', 'formatter' => function ($d){return format_date($d, 'd-m-Y H:i:s');}),
		);
		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "
			FROM ".$this->tbl_name." a
			LEFT JOIN tb_negara b ON b.kd_negara = a.negara_kd
			LEFT JOIN tb_provinsi c ON c.kd_provinsi = a.provinsi_kd AND c.negara_kd = b.kd_negara
			LEFT JOIN tb_kota d ON d.kd_kota = a.kota_kd AND d.provinsi_kd = c.kd_provinsi AND d.negara_kd = b.kd_negara
			LEFT JOIN tb_kecamatan e ON e.kd_kecamatan = a.kecamatan_kd AND e.kota_kd = d.kd_kota AND e.provinsi_kd = c.kd_provinsi AND e.negara_kd = b.kd_negara
		";
		$data['where'] = "";
		return $data;
	}

	public function tbl_btn($id = '', $var = '') {		
		$read_access = $this->session->read_access;
		$update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		$btns[] = get_btn(array('access' => $update_access, 'title' => 'Edit Data', 'icon' => 'pencil', 'onclick' => 'get_form(\''.$id.'\')'));
		$btns[] = get_btn_divider();
		$btns[] = get_btn(array('access' => $delete_access, 'title' => 'Hapus', 'icon' => 'trash', 'onclick' => 'return confirm(\'Anda akan menghapus Jasa Kirim = '.$var.'?\')?delete_data(\''.$id.'\'):false'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function get_row($id = '') {
		$this->db->from($this->tbl_name)
			->where(array($this->p_key => $id));
		$query = $this->db->get();
		$num = $query->num_rows();
		if ($num > 0) :
			$row = $query->row();
			$data = array('kd_jasakirim' => $row->kd_jasakirim, 'nm_jasakirim' => $row->nm_jasakirim, 'alamat' => $row->alamat, 'kecamatan_kd' => $row->kecamatan_kd, 'kota_kd' => $row->kota_kd, 'provinsi_kd' => $row->provinsi_kd, 'negara_kd' => $row->negara_kd, 'kode_pos' => $row->kode_pos);
		else :
			$data = array('kd_jasakirim' => '', 'nm_jasakirim' => '', 'alamat' => '', 'kecamatan_kd' => '', 'kota_kd' => '', 'provinsi_kd' => '', 'negara_kd' => '', 'kode_pos' => '');
		endif;
		return $data;
	}

	public function form_rules() {
		$rules = array(
			array('field' => 'txtNama', 'label' => 'Nama Jasa Kirim', 'rules' => 'required'),
			array('field' => 'txtAlamat', 'label' => 'Alamat', 'rules' => 'required'),
		);
		return $rules;
	}

	public function build_warning($datas = '') {
		$forms = array('txtNama', 'txtAlamat');
		foreach ($datas as $key => $data) :
			$str[$data] = (!empty(form_error($forms[$key])))?buildLabel('warning', form_error($forms[$key], '"', '"')):'';
		endforeach;
		return $str;
	}

	private function create_code() {
		$this->db->select($this->p_key.' AS code')
			->from($this->tbl_name)
			->where(array('DATE(tgl_input)' => date('Y-m-d')))
			->order_by('tgl_input DESC, '.$this->p_key.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -6);
		endif;
		$angka = $urutan + 1;
		$primary = 'MJK'.date('dmy').str_pad($angka, 3, '000', STR_PAD_LEFT);
		return $primary;
	}

	public function submit_data($data = '', $tipe = '') {
		if (!empty($data[$this->p_key])) :
			$label = 'Mengubah '.$this->title_name;
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s')));
			$where[$this->p_key] = $data[$this->p_key];
			$act = $this->update($submit, $where);
		else :
			$label = 'Menambahkan '.$this->title_name;
			$data[$this->p_key] = $this->create_code();
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s')));
			$act = $this->create($submit);
		endif;
		$str = $this->report($act, $label, $submit);
		$str[$this->p_key] = $data[$this->p_key];
		return $str;
	}

	private function create($data = '') {
		$act = $this->db->insert($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	private function update($data = '', $where = '') {
		$act = $this->db->update($this->tbl_name, $data, $where);
		return $act?TRUE:FALSE;
	}

	public function delete_data($id = '') {
		$data = $this->get_row($id);
		$act = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		$report = $this->report($act, 'Menghapus', array('kd_mdo' => $id, 'Kode Master Delivery Order' => $id));
		return $report;
	}

	public function report($act = '', $label = '', $data = '') {
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['kd_mdo'] = $data[$this->p_key];
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
		endif;
		$this->m_builder->write_log($stat, $label, $data);
		return $str;
	}
}