<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_planningweekly_detail extends CI_Model
{
    private $tbl_name = 'td_planningweekly_detail';
    private $p_key = 'planningweeklydetail_kd';

    public function ssp_table($id)
    {
        $data['table'] = $this->tbl_name;

        $data['primaryKey'] = $this->p_key;

        $data['columns'] = array(
            array(
                'db' => $this->p_key,
                'dt' => 1, 'field' => $this->p_key,
                'formatter' => function ($d) {

                    return $this->tbl_btn($d);
                }
            ),
            array(
                'db' => 'b.woitem_no_wo',
                'dt' => 2, 'field' => 'woitem_no_wo',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'b.woitem_itemcode',
                'dt' => 3, 'field' => 'woitem_itemcode',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'b.woitem_deskripsi',
                'dt' => 4, 'field' => 'woitem_deskripsi',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'b.woitem_jenis',
                'dt' => 5, 'field' => 'woitem_jenis',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.planningweeklydetail_qty',
                'dt' => 6, 'field' => 'planningweeklydetail_qty',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.planningweeklydetail_note',
                'dt' => 7, 'field' => 'planningweeklydetail_note',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.planningweeklydetail_tgledit',
                'dt' => 8, 'field' => 'planningweeklydetail_tgledit',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),

        );

        $data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM " . $this->tbl_name . " as a
								LEFT JOIN td_workorder_item as b ON a.woitem_kd=b.woitem_kd";
        $data['where'] = "a.planningweekly_kd = '" . $id . "'";

        return $data;
    }

    private function tbl_btn($id)
    {
        $btns = array();

        // $btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'form_box(\'edit\',\'' . $id . '\')'));
        // todoaction on edit
        // $btns[] = get_btn_divider();
        $btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data_detail(\'' . $id . '\')'));

        $btn_group = group_btns($btns);

        return $btn_group;
    }

    public function insert_data($data)
    {
        $query = $this->db->insert($this->tbl_name, $data);
        return $query ? TRUE : FALSE;
    }

    public function delete_data($id)
    {
        $query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
        return $query ? TRUE : FALSE;
    }

    public function get_by_param($param = [])
    {
        $this->db->where($param);
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    public function get_all()
    {
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    public function update_data($aWhere = [], $data)
    {
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
        return $query ? TRUE : FALSE;
    }

    public function create_code()
    {
        $query = $this->db->select('MAX(' . $this->p_key . ') as maxID')
            ->get($this->tbl_name)
            ->row();
        $code = (int) $query->maxID + 1;
        return $code;
    }

    public function generate_no()
    {
        $num = 1;
        $qSeq = $this->db->where('YEAR(planningweekly_tglinput)', date('Y'))
            ->where('MONTH(planningweekly_tglinput)', date('m'))
            ->order_by('planningweekly_no', 'desc')
            ->get($this->tbl_name)->row_array();
        $woplanningNo = "WOPLANNING-" . date('ym') . '-' . str_pad($num, 3, '0', STR_PAD_LEFT);

        return $woplanningNo;
    }

    public function insert_batch($data)
    {
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
    }

    public function get_by_param_in($param, $params = [])
    {
        $this->db->where_in($param, $params);
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    public function get_by_param_bom_in($param, $params)
    {
        $query = $this->db->select($this->tbl_name . '.*, tm_bom.bom_kd')
            ->where_in($param, $params)
            ->join('tm_bom', $this->tbl_name . '.project_kd=tm_bom.project_kd', 'left')
            ->get($this->tbl_name);
        return $query;
    }

    public function getItemInsertedGroupByWoitem($divisi_kd, $woitem_kds = [])
    {
        $planningDetails = $this->db->select('td_planningweekly_detail.woitem_kd, SUM(td_planningweekly_detail.planningweeklydetail_qty) as sum_planningweeklydetail_qty')
        // $planningDetails = $this->db->select('tm_planningweekly.bagian_kd,td_planningweekly_detail.woitem_kd, td_planningweekly_detail.planningweeklydetail_qty')
            ->join('tm_planningweekly', 'tm_planningweekly.planningweekly_kd=td_planningweekly_detail.planningweekly_kd', 'left')
            ->where('tm_planningweekly.divisi_kd', $divisi_kd)  //revisi bagian->divisi
            ->where_in('td_planningweekly_detail.woitem_kd', $woitem_kds)
            ->group_by('td_planningweekly_detail.woitem_kd')
            ->get('td_planningweekly_detail')->result_array();
        return $planningDetails;
    }

    public function getDetailPlanningWoitem ($planningweekly_kd) 
    {
        $qResult = $this->db->join('td_workorder_item', "td_workorder_item.woitem_kd={$this->tbl_name}.woitem_kd",'left')
            ->where("{$this->tbl_name}.planningweekly_kd", $planningweekly_kd)
            ->get($this->tbl_name)->result_array();
        return $qResult;
    }

    public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}
}
