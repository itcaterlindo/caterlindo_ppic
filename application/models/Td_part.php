<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_part extends CI_Model {
	private $tbl_name = 'td_part';
	private $p_key = 'part_kd';

	public function create_code() {
		$this->db->select('MAX('.$this->p_key.') AS code')
			->from($this->tbl_name);
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = (int) substr($code, -10);
		endif;
		$angka = $urutan + 1;
		$primary = 'PART'.str_pad($angka, 10, '0', STR_PAD_LEFT);
		return $primary;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

	public function get_by_param_detail ($param=[]) {
		$query = $this->db->select($this->tbl_name.'.*, td_part_jenis.*, tm_part_main.partmain_nama, tm_part_main.partmain_status, tb_admin.nm_admin, tb_part_state.*')
					->where($param)
					->from($this->tbl_name)
					->join('tm_part_main', $this->tbl_name.'.partmain_kd=tm_part_main.partmain_kd', 'left')
					->join('td_part_jenis', 'tm_part_main.partjenis_kd=td_part_jenis.partjenis_kd', 'left')
					->join('tb_admin', $this->tbl_name.'.admin_kd=tb_admin.kd_admin', 'left')
					->join('tb_part_state', $this->tbl_name.'.partstate_kd=tb_part_state.partstate_kd', 'left')
					->get();
		return $query;
	}

	public function get_max_versi ($partmain_kd) {
		$query = $this->db->select('MAX(part_versi) as maxVersi')
					->where(['partmain_kd' => $partmain_kd])
					->get($this->tbl_name)->row();
		$rowData = $this->db->select('*')
					->where(['partmain_kd' => $partmain_kd, 'part_versi' => $query->maxVersi])
					->get($this->tbl_name)->row();
		return $rowData;
	}

	public function generate_versi ($partmain_kd) {
		$query = $this->db->select('MAX(part_versi) as maxVersi')
					->where(['partmain_kd' => $partmain_kd])
					->get($this->tbl_name)->row();
		$versi = $query->maxVersi;
		$nextVersi = (int) $versi + 1;
		return $nextVersi;
	}

	public function get_by_param_in ($param, $data = []) {
		$query = $this->db->select($this->tbl_name.'.*, tm_part_main.*')
				->where_in($param, $data)
				->from($this->tbl_name)
				->join('tm_part_main', $this->tbl_name.'.partmain_kd=tm_part_main.partmain_kd', 'left')
				->get();
		return $query;
	}

	/** Untuk part costing */
	public function partcosting_data ($valPart_kd) {
		$this->load->model(['td_part_labourcost', 'tb_part_labourcost_harga', 'td_part_overhead']);
        /** Labourcost */
        $qLabourcost = $this->td_part_labourcost->get_detail_in_part('td_part_labourcost.part_kd', $valPart_kd);
        $qHargaLabourcost = $this->tb_part_labourcost_harga->get_all();

        /** Overhead */
        $qOverhead = $this->td_part_overhead->get_by_param_detail (['td_part_overhead.part_kd' => $valPart_kd]);

        /** per part */
        $qResultPart = $this->db->select('td_part_detail.*, td_rawmaterial_harga.*, tm_rawmaterial.rm_kode, td_rawmaterial_satuan.rmsatuan_nama, tb_bagian.bagian_nama, 
                        tm_rawmaterial.rm_platepanjang, tm_rawmaterial.rm_platelebar, tm_rawmaterial.rm_platetebal, tm_rawmaterial.rm_platemassajenis, tm_rawmaterial.rmkategori_kd, tm_rawmaterial.rm_oldkd')
                    ->from('td_part_detail')
                    ->join('td_rawmaterial_harga', 'td_part_detail.rm_kd=td_rawmaterial_harga.rm_kd', 'left')
                    ->join('tm_rawmaterial', 'td_part_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
                    ->join('td_rawmaterial_satuan', 'td_part_detail.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
                    ->join('tb_bagian', 'td_part_detail.bagian_kd=tb_bagian.bagian_kd', 'left')
                    ->where('td_part_detail.part_kd', $valPart_kd)
                    ->order_by('td_part_detail.bagian_kd', 'asc')
                    ->get();
        /** Detail Plate */
		$qResultPlate = $this->db->select('td_part_detailplate.*, tm_rawmaterial.rm_platepanjang, tm_rawmaterial.rm_platelebar, tm_rawmaterial.rm_platetebal, tm_rawmaterial.rm_platemassajenis, td_part_detail.partdetail_qty,
						td_rawmaterial_harga.rmharga_hargagr, td_rawmaterial_harga.rmharga_hargainput, tm_rawmaterial.rm_kode, tm_rawmaterial.rm_oldkd')
                    ->from('td_part_detailplate')
                    ->join('tm_rawmaterial', 'td_part_detailplate.rm_kd=tm_rawmaterial.rm_kd', 'left')
                    ->join('td_part_detail', 'td_part_detailplate.partdetail_kd=td_part_detail.partdetail_kd', 'left')
                    ->join('td_rawmaterial_harga', 'td_part_detailplate.rm_kd=td_rawmaterial_harga.rm_kd', 'left')
                    ->where('td_part_detailplate.part_kd', $valPart_kd)
                    ->get();
        $arrCutsizePartdetail_kd = [];
        $arrMainPartdetail_kd = [];
        $arrSumPlatePartdetail_kd = [];
        $resultCutsize = []; 
        foreach ($qResultPlate->result_array() as $rPlate) {
            /** Cutsize */
            if (!in_array($rPlate['partdetail_kd'], $arrCutsizePartdetail_kd)) {
				if ($rPlate['partdetailplate_jenis'] == 'cut_size') {
					$cutSizeKonversi = round(generate_massa_plate($rPlate['partdetailplate_panjang'], $rPlate['partdetailplate_lebar'], $rPlate['partdetailplate_tebal'], $rPlate['partdetailplate_massajenis']) * $rPlate['partdetailplate_qty'], 3);
					$cutSizeKonversiPlate = generate_massa_plate($rPlate['rm_platepanjang'], $rPlate['rm_platelebar'], $rPlate['rm_platetebal'], $rPlate['rm_platemassajenis']);
					$hargaPerKonversiGR = round($rPlate['rmharga_hargagr'] / $cutSizeKonversiPlate, 2);
					$hargaPerKonversiInput = $rPlate['partdetailplate_hargaunit'];
					$resultCutsize[] = [
						'partdetail_kd' => $rPlate['partdetail_kd'],
						'rm_kd' => $rPlate['rm_kd'],
						'cutSize' => $cutSizeKonversi,
						'hargaKonversiGR' => $hargaPerKonversiGR,
						'hargaKonversiInput' => $hargaPerKonversiInput,
					];
					$arrCutsizePartdetail_kd[] = $rPlate['partdetail_kd'];
				}
            }
            /** Sum Main */
            if ($rPlate['partdetailplate_jenis'] == 'main') {
                $konversiMain = generate_massa_plate($rPlate['partdetailplate_panjang'], $rPlate['partdetailplate_lebar'], $rPlate['partdetailplate_tebal'], $rPlate['partdetailplate_massajenis']) * $rPlate['partdetailplate_qty'];
                $arrMainPartdetail_kd[$rPlate['partdetail_kd'].'/'.$rPlate['rm_kd']][] = round($konversiMain, 3);
            }

            /** Sum koversi plate */
            if (!in_array($rPlate['partdetail_kd'], $arrSumPlatePartdetail_kd)) {
				if ($rPlate['partdetailplate_jenis'] != 'cut_size') {
					$konversiPlate = generate_massa_plate($rPlate['partdetailplate_panjang'], $rPlate['partdetailplate_lebar'], $rPlate['partdetailplate_tebal'], $rPlate['partdetailplate_massajenis']) * $rPlate['partdetailplate_qty'];
					$arrSumPlatePartdetail_kd[$rPlate['partdetail_kd'].'/'.$rPlate['rm_kd']][] = round($konversiPlate, 3);
				}
            }
        }
        /** Sum Main */
        $resultSumMain = [];
        foreach ($arrMainPartdetail_kd as $partdetailkd => $element) {
            $expPartDetail = explode('/', $partdetailkd);
            $resultSumMain[] = [
                'partdetail_kd' => $expPartDetail[0],
                'rm_kd' => $expPartDetail[1],
                'sumMain' => array_sum($element),
            ];
        }
        /** Sum koversi plate */
        $resultSumPlate = [];
        foreach ($arrSumPlatePartdetail_kd as $SumPlatepartdetailkd => $elementPlate) {
            $expPlatePartDetail = explode('/', $SumPlatepartdetailkd);
            $resultSumPlate[] = [
                'partdetail_kd' => $expPlatePartDetail[0],
                'rm_kd' => $expPlatePartDetail[1],
                'sumPlate' => array_sum($elementPlate),
            ];
        }
        /** Waste */
		$resultWaste = [];
		$sumPlate = 0;
		$waste = 0;
        foreach ($resultCutsize as $rCutsize) {
            foreach ($resultSumPlate as $rSumPlate) {
                if ($rSumPlate['partdetail_kd'] ==  $rCutsize['partdetail_kd']) {
                    $sumPlate = $rSumPlate['sumPlate'];
                    $waste = $rCutsize['cutSize'] - $sumPlate;
                }
            }
            $resultWaste [] = [
                'partdetail_kd' => $rCutsize['partdetail_kd'],
                'rm_kd' => $rCutsize['rm_kd'],
                'cutSize' => $rCutsize['cutSize'],
                'sumPlate' => $sumPlate,
                'waste' => $waste,
            ];
        }
        /** Plate Used (cutsize - ) */
        $resultUsed = [];
        $wasteUsed = 0;
        foreach ($resultCutsize as $rCutsizeUsed) {
            /** waste */
            $wasteUsed = 0;
            foreach ($resultWaste as $rWasteUsed) {
                if ($rWasteUsed['partdetail_kd'] ==  $rCutsizeUsed['partdetail_kd']) {
                    $wasteUsed = $rWasteUsed['waste'];
                }
            }
			/** main */
			$sumMain = 0;
			$plateUsed = 0;
			$priceplateUsedGR = 0;
			$priceplateUsedInput = 0;
            foreach ($resultSumMain as $rSumMain) {
                if ($rSumMain['partdetail_kd'] ==  $rCutsizeUsed['partdetail_kd']) {
                    $sumMain = $rSumMain['sumMain'];
                    $plateUsed = round($sumMain + $wasteUsed, 3);
                    $priceplateUsedGR = round($plateUsed * $rCutsizeUsed['hargaKonversiGR'], 2);
                    $priceplateUsedInput = round($plateUsed * $rCutsizeUsed['hargaKonversiInput'], 2);
                }
            }
            $resultUsed [] = [
                'partdetail_kd' => $rCutsizeUsed['partdetail_kd'],
                'rm_kd' => $rCutsizeUsed['rm_kd'],
                'cutSize' => $rCutsizeUsed['cutSize'],
                'sumMain' => $sumMain,
                'waste' => $wasteUsed,
                'plateUsed' => $plateUsed,
                'hargaKonversiGR' => $rCutsizeUsed['hargaKonversiGR'],
                'hargaKonversiInput' => $rCutsizeUsed['hargaKonversiInput'],
                'pricePlateUsedGR' => $priceplateUsedGR,
                'pricePlateUsedInput' => $priceplateUsedInput,
            ];
        }

        /** LabourCost */
        $resultLabourcost = [];
		$labourcostPriceSatuan = null;
		$labourcostPriceTotal = null;
        foreach ($qLabourcost->result_array() as $rLabourcost) {
            $labourcostPriceSatuan = $rLabourcost['partlabourcost_hargaunit'];
            $labourcostPriceTotal = $rLabourcost['partlabourcost_hargatotal'];
            $resultLabourcost[] = [
                'bagian_kd' => $rLabourcost['bagian_kd'],
                'bagian_nama' => $rLabourcost['bagian_nama'],
                'partlabourcost_qty' => $rLabourcost['partlabourcost_qty'],
                'partlabourcost_satuan' => $rLabourcost['partlabourcost_satuan'],
                'partlabourcost_durasi' => $rLabourcost['partlabourcost_durasi'],
                'labourcostPriceSatuan' => $labourcostPriceSatuan,
                'labourcostPriceTotal' => $labourcostPriceTotal,
            ];
		}
		
		/** Detiailpipe */
		$qDetailpipe = $this->db->select()
						->from('td_part_detailpipe')
						->join('tm_rawmaterial','td_part_detailpipe.rm_kd=tm_rawmaterial.rm_kd','left')
						->join('td_rawmaterial_satuan', 'tm_rawmaterial.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
						->get();

		$data['resultPart'] = $qResultPart->result_array();
        $data['resultPlate'] = $qResultPlate->result_array();
        $data['resultPlateWaste'] = $resultWaste;
		$data['resultPlateUsed'] = $resultUsed;
		$data['resultPipe'] = $qDetailpipe->result_array();
        $data['resultLabourcost'] = $resultLabourcost;
        $data['resultOverhead'] = $qOverhead->result_array();
        
        return $data;
	}
	
	public function generateButtonState ($part_kd) {
		$this->load->model(['tb_part_state', 'tb_admin', 'td_part_state_user']);
		$qPart = $this->get_by_param (['part_kd' => $part_kd])->row_array();
		$qPartState = $this->tb_part_state->get_all()->result_array();
		$admin_kd = $this->session->userdata('kd_admin');

		$curState_kd = $qPart['partstate_kd']; 
		$nextPartstate_kd = (int)$curState_kd + 1;
		$backPartstate_kd = (int)$curState_kd - 1;
		$nextPartstate_nama = null;
		$backPartstate_nama = null;
		$nextAllowState = 0;
		$backAllowState = 0;
		$curPartstate_filteruser = 0;
		foreach ($qPartState as $rPartstate) {
			/** Curstate */
			if ($rPartstate['partstate_kd'] == $curState_kd) {
				$curPartstate_filteruser = $rPartstate['partstate_filteruser'];
			}
			/** Next */
			if ($rPartstate['partstate_kd'] == $nextPartstate_kd) {
				$nextPartstate_nama = $rPartstate['partstate_nama'];					
			}
			/** Back */
			if ($rPartstate['partstate_kd'] == $backPartstate_kd) {
				$backPartstate_nama = $rPartstate['partstate_nama'];
			}
		}
		/** Cek currentState */
		if ($curPartstate_filteruser == 0) {
			$nextAllowState = 1;
			$backAllowState = 1;
		}else {
			$cekState = $this->td_part_state_user->get_by_param(['partstate_kd' => $curState_kd, 'partstateuser_admin' => $admin_kd]);
			if ($cekState->num_rows() > 0) {
				$nextAllowState = 1;
				$backAllowState = 1;
			}
		}
		/** Is Admin or is approved*/
		if ($this->tb_admin->isAdmin($admin_kd)) {
			$nextAllowState = 1;
			$backAllowState = 1;
		}
		
		$btn = '';
		if (!empty($backPartstate_nama) && $backAllowState) {
			$btn .= '<button class="btn btn-sm btn-warning" onclick=ubah_state("'.$part_kd.'","'.$backPartstate_kd.'")> <i class="fa fa-hand-o-left"> </i> Ubah ke '.ucwords(str_replace('_', ' ', $backPartstate_nama)).'</button>';
		}
		if (!empty($nextPartstate_nama) && $nextAllowState) {
			$btn .= '<button class="btn btn-sm btn-warning" onclick=ubah_state("'.$part_kd.'","'.$nextPartstate_kd.'")> Ubah ke '.ucwords(str_replace('_', ' ', $nextPartstate_nama)).' <i class="fa fa-hand-o-right"></i> </button>';
		}
		
		return $btn;
	}

	private function ubah_state_email ($part_kd, $partstate_kd, $keterangan) {
		$this->load->model(['_mail_configuration', 'td_part_state_user']);

		$stateUser = $this->td_part_state_user->get_by_param(['partstate_kd' => $partstate_kd])->result_array();
		$to = [];
		$cc = [];
		$txtcc = '';
		foreach ($stateUser as $rStateUser) {
			$to [] = $rStateUser['partstateuser_email'];
			$txtcc .= ';'.$rStateUser['partstateuser_emailcc'];
		}
		$txtcc = substr($txtcc, 1, strlen($txtcc));
		$cc = explode(';', str_replace(' ', '', $txtcc));

		$rowPart = $this->get_by_param_detail(['td_part.part_kd' => $part_kd])->row_array();
		$subject = 'Bill Of Material (Part)-'.$rowPart['partmain_nama'];

		$dtMessage['text'] = 'Terdapat Bill of Material (Part) yang perlu ditindak lanjuti :';
		$dtMessage['url'] = my_baseurl().'manage_items/manage_part/part_data/view_detailtable_box?part_kd='.$part_kd;
		$dtMessage['urltext'] = 'Detail Bill of Material (Part)';
		$dtMessage['keterangan'] = !empty($keterangan) ? $keterangan : '-';
		$message = $this->load->view('templates/email/email_notification', $dtMessage, true);

		$actEmail = $this->_mail_configuration->sendEmailCc($to, $cc, $subject, $message);
		return $actEmail;
	}

	private function ubah_state_email_tojson ($part_kd, $partstate_kd, $keterangan) {
		$this->load->model(['_mail_configuration', 'td_part_state_user']);

		$stateUser = $this->td_part_state_user->get_by_param(['partstate_kd' => $partstate_kd])->result_array();
		$to = [];
		$cc = [];
		$txtcc = '';
		foreach ($stateUser as $rStateUser) {
			$to [] = $rStateUser['partstateuser_email'];
			$txtcc .= ';'.$rStateUser['partstateuser_emailcc'];
		}
		$txtcc = substr($txtcc, 1, strlen($txtcc));
		$cc = explode(';', str_replace(' ', '', $txtcc));

		$rowPart = $this->get_by_param_detail(['td_part.part_kd' => $part_kd])->row_array();
		$subject = 'Bill Of Material (Part)-'.$rowPart['partmain_nama'];

		$dtMessage['to'] = implode(";",$to);
		$dtMessage['cc'] = implode(";",$cc);
		$dtMessage['subject'] = $subject;
		$dtMessage['text'] = 'Terdapat Bill of Material (Part) yang perlu ditindak lanjuti :';
		$dtMessage['url'] = my_baseurl().'manage_items/manage_part/part_data/view_detailtable_box?part_kd='.$part_kd;
		$dtMessage['urltext'] = 'Detail Bill of Material (Part)';
		$dtMessage['keterangan'] = !empty($keterangan) ? $keterangan : '-';
		//$message = $this->load->view('templates/email/email_notification', $dtMessage, true);

		//$actEmail = $this->_mail_configuration->sendEmailCc($to, $cc, $subject, $message);
		return $dtMessage;
	}

	public function ubah_state ($part_kd, $partstate_kd, $keterangan) {
		$this->load->model(['td_part_state_log', 'tb_part_state']);
		$dataUpdate = [
			'partstate_kd' => $partstate_kd,
			'part_tgledit' => date('Y-m-d H:i:s'),
		];
		$dataLog = [
			'part_kd' => $part_kd,
			'partstate_kd' => $partstate_kd,
			'partstatelog_note' => $keterangan,
			'admin_kd' => $this->session->userdata('kd_admin'),
		];
		$actEmail = [];
		$send_email = false;
		$this->db->trans_start();
		$rowPartstate = $this->tb_part_state->get_by_param(['partstate_kd' => $partstate_kd])->row_array();
		if ($rowPartstate['partstate_email'] == 1) {
			//$actEmail = $this->ubah_state_email ($part_kd, $partstate_kd, $keterangan);
			$actEmail = $this->ubah_state_email_tojson ($part_kd, $partstate_kd, $keterangan);
			$send_email = true;
		}
		
		$actUpdate = $this->update_data (['part_kd' => $part_kd], $dataUpdate);
		$actLog = $this->td_part_state_log->insert_data($dataLog);
		$this->db->trans_complete();
		$data['data_message_json'] = $actEmail;
		$data['send_email'] = $send_email;
		if ($this->db->trans_status() === FALSE) {
			$data['status'] = false;
			return $data;
		}else {
			$data['status'] = true;
			return $data;
		}
	}

	public function duplicate_part ($partmain_kd, $part_kd, $versi) {
		$this->load->model(['td_part_detail', 'td_part_labourcost', 'td_part_detailplate', 'td_part_detailpipe']);
		$duplicatePart_kd = $this->td_part->create_code(); 
		$dataMaster = array(
			'part_kd' => $duplicatePart_kd,
			'partmain_kd' => $partmain_kd,
			'part_versi' => $versi,
			'part_tglinput' => date('Y-m-d H:i:s'),
			'part_tgledit' => date('Y-m-d H:i:s'),
			'admin_kd' => $this->session->userdata('kd_admin'),
		);

		/** Detail part */
		$dataDetail = $this->td_part_detail->get_by_param(['part_kd' => $part_kd])->result_array();
		$partdetail_kd = $this->td_part_detail->create_code();
		if (!empty($dataDetail)):
			foreach ($dataDetail as $each):
				$batchInsert [] = array(
					'partdetail_kd' => $partdetail_kd,
					'part_kd' => $duplicatePart_kd,
					'rm_kd' => $each['rm_kd'],
					'partdetail_nama' => $each['partdetail_nama'],
					'partdetail_deskripsi' => $each['partdetail_deskripsi'],
					'partdetail_spesifikasi' => $each['partdetail_spesifikasi'],
					'bagian_kd' => $each['bagian_kd'],
					'partdetail_qty' => $each['partdetail_qty'],
					'rmsatuan_kd' => $each['rmsatuan_kd'],
					'partdetail_tglinput' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				);
				/** Array untuk detail plate */
				$arrDetailPartCode[$each['partdetail_kd']] = $partdetail_kd;

				$urutan = (int) substr($partdetail_kd, -10);
				$angka = $urutan + 1;
				$partdetail_kd = 'DPRT'.str_pad($angka, 10, '0', STR_PAD_LEFT);
			endforeach;
		endif;

		/** Detail Labour */
		$dataLabour = $this->td_part_labourcost->get_by_param(['part_kd' => $part_kd])->result_array();
		$partlabourcost_kd = $this->td_part_labourcost->create_code();
		if (!empty($dataLabour)):
			foreach ($dataLabour as $eachLabour):
				$batchInsertLabourcost [] = array(
					'partlabourcost_kd' => $partlabourcost_kd,
					'part_kd' => $duplicatePart_kd,
					'bagian_kd' => $eachLabour['bagian_kd'],
					'partlabourcost_qty' => $eachLabour['partlabourcost_qty'],
					'partlabourcost_satuan' => $eachLabour['partlabourcost_satuan'],
					'partlabourcost_durasi' => $eachLabour['partlabourcost_durasi'],
					'partlabourcost_tglinput' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				);
				$urutanLabourcost = (int) substr($partlabourcost_kd, -10);
				$angkaLabourcost = $urutanLabourcost + 1;
				$partlabourcost_kd = 'LPRT'.str_pad($angkaLabourcost, 10, '0', STR_PAD_LEFT);
			endforeach;
		endif;

		/** Detail Plate */
		$dataPlateDetail = $this->td_part_detailplate->get_by_param(['part_kd' => $part_kd])->result_array();
		$partdetailplate_kd = $this->td_part_detailplate->create_code();
		if (!empty($dataPlateDetail)):
			foreach ($dataPlateDetail as $eachDetailPlate):
				$batchInsertDetailPlate [] = array(
					'partdetailplate_kd' => $partdetailplate_kd,
					'part_kd' => $duplicatePart_kd,
					'partdetail_kd' => $arrDetailPartCode[$eachDetailPlate['partdetail_kd']],
					'rm_kd' => $eachDetailPlate['rm_kd'],
					'partdetailplate_panjang' => $eachDetailPlate['partdetailplate_panjang'],
					'partdetailplate_lebar' => $eachDetailPlate['partdetailplate_lebar'],
					'partdetailplate_tebal' => $eachDetailPlate['partdetailplate_tebal'],
					'partdetailplate_massajenis' => $eachDetailPlate['partdetailplate_massajenis'],
					'partdetailplate_qty' => $eachDetailPlate['partdetailplate_qty'],
					'partdetailplate_jenis' => $eachDetailPlate['partdetailplate_jenis'],
					'partdetailplate_keterangan' => $eachDetailPlate['partdetailplate_keterangan'],
					'partdetailplate_tglinput' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				);
				$urutanDetailPlate = (int) substr($partdetailplate_kd, -10);
				$angkaDetailPlate = $urutanDetailPlate + 1;
				$partdetailplate_kd = 'PPRT'.str_pad($angkaDetailPlate, 10, '0', STR_PAD_LEFT);
			endforeach;
		endif;

		/** Detail Plate */
		$dataPipeDetail = $this->td_part_detailpipe->get_by_param(['part_kd' => $part_kd])->result_array();
		$partdetailpipe_kd = $this->td_part_detailpipe->create_code();
		if (!empty($dataPipeDetail)):
			foreach ($dataPipeDetail as $eachDetailPipe):
				$batchInsertDetailPipe [] = array(
					'partdetailpipe_kd' => $partdetailpipe_kd,
					'part_kd' => $duplicatePart_kd,
					'partdetail_kd' => $arrDetailPartCode[$eachDetailPipe['partdetail_kd']],
					'rm_kd' => $eachDetailPipe['rm_kd'],
					'partdetailpipe_panjang' => $eachDetailPipe['partdetailpipe_panjang'],
					'partdetailpipe_qty' => $eachDetailPipe['partdetailpipe_qty'],
					'partdetailpipe_keterangan' => $eachDetailPipe['partdetailpipe_keterangan'],
					'partdetailpipe_tglinput' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				);
				$partdetailpipe_kd++;
			endforeach;
		endif;

		/** Action */
		$resp = [];
		$actMaster = true;
		$actDetail = true;
		$actDetailLabourcost = true;
		$actDetailPlate = true;

		$actMaster = $this->td_part->insert_data($dataMaster);
		if (!empty($dataDetail)){
			$actDetail = $this->td_part_detail->insert_batch_data($batchInsert);
		}
		if (!empty($dataLabour)){
			$actDetailLabourcost = $this->td_part_labourcost->insert_batch_data($batchInsertLabourcost);
		}
		if (!empty($dataPlateDetail)){
			$actDetailPlate = $this->td_part_detailplate->insert_batch_data($batchInsertDetailPlate);
		}
		if (!empty($dataPipeDetail)){
			$actDetailPipe = $this->td_part_detailpipe->insert_batch_data($batchInsertDetailPipe);
		}

		if ($actMaster){
			$resp = true;
		}else{
			$resp = false;
		}
		return $resp;
	}

	public function get_totalharga_plate_costing($part_kd) {
		$this->load->model(['td_part_labourcost', 'td_part_overhead', 'td_part_detailplate']);
		$qPartdetail = $this->db->select()
						->from('td_part_detail')
						->join('tm_rawmaterial', 'td_part_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
						->where(['td_part_detail.part_kd' => $part_kd])
						->like('tm_rawmaterial.rm_kode', 'SSPL', 'both')
						->get()
						->result_array();
		
		$qLabourcost = $this->td_part_labourcost->get_by_param(['part_kd' => $part_kd])->result_array();
		$qOverhead = $this->td_part_overhead->get_by_param(['part_kd' => $part_kd])->result_array();
		$qDetailplate = $this->td_part_detailplate->get_by_param(['part_kd' => $part_kd])->result_array();		

		/** Total harga Non Plate*/
		$arrayNonplate = [];
		$arrayPartdetailkd_plate = [];
		$arrayPlate = [];
		$harga_kg = 0;
		foreach ($qPartdetail as $rPartdetail){
			if ($rPartdetail['rmkategori_kd'] != '01'){
				$arrayNonplate[] = $rPartdetail['partdetail_hargatotal'];
			}else{
				/** detail item Plate */
				$arrayPlate[] = [
					'partdetail_kd' => $rPartdetail['partdetail_kd'],
					'rm_kd' => $rPartdetail['rm_kd'],
				];
			}
		}
		$subNonplate = round(array_sum($arrayNonplate), 2);

		/** Total harga Plate*/
		$arrayUsed = [];
		$arrayCutsize = [];
		$arraySumAdditionlMain = [];
		$arrayMain = [];
		foreach ($arrayPlate as $rPlate) {
			foreach($qDetailplate as $rDetailplate){
				/** Cut Size */
				if ($rDetailplate['partdetail_kd'] == $rPlate['partdetail_kd'] && $rDetailplate['partdetailplate_jenis'] == 'cut_size') {
					$massaPlate_cutsize = round(generate_massa_plate($rDetailplate['partdetailplate_panjang'], $rDetailplate['partdetailplate_lebar'], $rDetailplate['partdetailplate_tebal'], $rDetailplate['partdetailplate_massajenis']) * $rDetailplate['partdetailplate_qty'], 3);
					$arrayCutsize[] = [
						'partdetail_kd' => $rPlate['partdetail_kd'],
						'cutsize_qty' => $massaPlate_cutsize,
						'cutsize_hargaunit' => $rDetailplate['partdetailplate_hargaunit'],
					];
				}

				/** Main plate*/
				if ($rDetailplate['partdetail_kd'] == $rPlate['partdetail_kd'] && $rDetailplate['partdetailplate_jenis'] == 'main') {
					$massaMain = round(generate_massa_plate($rDetailplate['partdetailplate_panjang'], $rDetailplate['partdetailplate_lebar'], $rDetailplate['partdetailplate_tebal'], $rDetailplate['partdetailplate_massajenis']) * $rDetailplate['partdetailplate_qty'], 3);
					$arrayMain[$rDetailplate['partdetail_kd']][] = $massaMain;
				}

				/** Sum qty additional + main */
				if ($rDetailplate['partdetail_kd'] == $rPlate['partdetail_kd'] && $rDetailplate['partdetailplate_jenis'] != 'cut_size') {
					$massaPlate = round(generate_massa_plate($rDetailplate['partdetailplate_panjang'], $rDetailplate['partdetailplate_lebar'], $rDetailplate['partdetailplate_tebal'], $rDetailplate['partdetailplate_massajenis']) * $rDetailplate['partdetailplate_qty'], 3);
					$arrayAllplate_qty[$rPlate['partdetail_kd']][] = $massaPlate;
				}
			}
			if(isset($arrayAllplate_qty[$rPlate['partdetail_kd']])){
				$arraySumAdditionlMain[$rPlate['partdetail_kd']] = array_sum($arrayAllplate_qty[$rPlate['partdetail_kd']]);
			}
		 
		}
		$arrayUsed = [];
		$waste_qty = 0;
		$main_qty = 0;
		$sumUsed_qty = 0;
		foreach ($arrayCutsize as $rCutsize){
			$waste_qty = $rCutsize['cutsize_qty'] - $arraySumAdditionlMain[$rCutsize['partdetail_kd']];
			$main_qty = array_sum($arrayMain[$rCutsize['partdetail_kd']]);
			$sumUsed_qty = $waste_qty + $main_qty;
			$arrayUsed[] = [
				'partdetail_kd' => $rCutsize['partdetail_kd'],
				'used_qty' => $sumUsed_qty,
				'used_hargaunit' => $rCutsize['cutsize_hargaunit'],
				'used_hargatotal' => $sumUsed_qty * $rCutsize['cutsize_hargaunit'],
			];
		}
		$arrPlateTotal = [];
		foreach ($arrayUsed as $rUserhargatotal) {
			$arrPlateTotal[] = $rUserhargatotal['used_hargatotal'];
		}

		$subPlate = round(array_sum($arrPlateTotal), 2);

		/** Total Labourcost */
		$arrayLabourcost = [];
		foreach ($qLabourcost as $rLabourcost) {
			$arrayLabourcost[] = $rLabourcost['partlabourcost_hargatotal'];
		}
		$subLabourcost = round(array_sum($arrayLabourcost), 2);

		/** Total Overhead */
		$arrayOverhead = [];
		foreach ($qOverhead as $rOverhead) {
			$arrayOverhead[] = $rOverhead['partoverhead_hargatotal'];
		}
		$subOverhead = round(array_sum($arrayOverhead), 2);

		$sumTotal = $subPlate;
		return $sumTotal;
	}

	public function part_costing_plate ($valPart_kd) {
		$this->load->model(['td_part_labourcost', 'tb_part_labourcost_harga', 'td_part_overhead']);
        /** Labourcost */
        $qLabourcost = $this->td_part_labourcost->get_detail_in_part('td_part_labourcost.part_kd', $valPart_kd);
        $qHargaLabourcost = $this->tb_part_labourcost_harga->get_all();

        /** Overhead */
        $qOverhead = $this->td_part_overhead->get_by_param_detail (['td_part_overhead.part_kd' => $valPart_kd]);

        /** per part */
        $qResultPart = $this->db->select('td_part_detail.*, td_rawmaterial_harga.*, tm_rawmaterial.rm_kode, td_rawmaterial_satuan.rmsatuan_nama, tb_bagian.bagian_nama, 
                        tm_rawmaterial.rm_platepanjang, tm_rawmaterial.rm_platelebar, tm_rawmaterial.rm_platetebal, tm_rawmaterial.rm_platemassajenis, tm_rawmaterial.rmkategori_kd, tm_rawmaterial.rm_oldkd')
                    ->from('td_part_detail')
                    ->join('td_rawmaterial_harga', 'td_part_detail.rm_kd=td_rawmaterial_harga.rm_kd', 'left')
                    ->join('tm_rawmaterial', 'td_part_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
                    ->join('td_rawmaterial_satuan', 'td_part_detail.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
                    ->join('tb_bagian', 'td_part_detail.bagian_kd=tb_bagian.bagian_kd', 'left')
                    ->where('td_part_detail.part_kd', $valPart_kd)
					->like('tm_rawmaterial.rm_kode', 'SSPL', 'both')
                    ->order_by('td_part_detail.bagian_kd', 'asc')
                    ->get();
        /** Detail Plate */
		$qResultPlate = $this->db->select('td_part_detailplate.*, tm_rawmaterial.rm_platepanjang, tm_rawmaterial.rm_platelebar, tm_rawmaterial.rm_platetebal, tm_rawmaterial.rm_platemassajenis, td_part_detail.partdetail_qty,
						td_rawmaterial_harga.rmharga_hargagr, td_rawmaterial_harga.rmharga_hargainput, tm_rawmaterial.rm_kode, tm_rawmaterial.rm_oldkd')
                    ->from('td_part_detailplate')
                    ->join('tm_rawmaterial', 'td_part_detailplate.rm_kd=tm_rawmaterial.rm_kd', 'left')
                    ->join('td_part_detail', 'td_part_detailplate.partdetail_kd=td_part_detail.partdetail_kd', 'left')
                    ->join('td_rawmaterial_harga', 'td_part_detailplate.rm_kd=td_rawmaterial_harga.rm_kd', 'left')
                    ->where('td_part_detailplate.part_kd', $valPart_kd)
					->like('tm_rawmaterial.rm_kode', 'SSPL', 'both')
                    ->get();
        $arrCutsizePartdetail_kd = [];
        $arrMainPartdetail_kd = [];
        $arrSumPlatePartdetail_kd = [];
        $resultCutsize = []; 
		try {

				
				foreach ($qResultPlate->result_array() as $rPlate) {
					/** Cutsize */
					if (!in_array($rPlate['partdetail_kd'], $arrCutsizePartdetail_kd)) {
						if ($rPlate['partdetailplate_jenis'] == 'cut_size') {
							$cutSizeKonversi = round(generate_massa_plate($rPlate['partdetailplate_panjang'], $rPlate['partdetailplate_lebar'], $rPlate['partdetailplate_tebal'], $rPlate['partdetailplate_massajenis']) * $rPlate['partdetailplate_qty'], 3);
							$cutSizeKonversiPlate = generate_massa_plate($rPlate['rm_platepanjang'], $rPlate['rm_platelebar'], $rPlate['rm_platetebal'], $rPlate['rm_platemassajenis']);
							if($cutSizeKonversiPlate >= 0 && $rPlate['rmharga_hargagr'] >= 0){
								$hargaPerKonversiGR = 0;
							}else{
								$hargaPerKonversiGR = 0;
							}
							$hargaPerKonversiInput = $rPlate['partdetailplate_hargaunit'];
							$resultCutsize[] = [
								'partdetail_kd' => $rPlate['partdetail_kd'],
								'rm_kd' => $rPlate['rm_kd'],
								'cutSize' => $cutSizeKonversi,
								'hargaKonversiGR' => $hargaPerKonversiGR,
								'hargaKonversiInput' => $hargaPerKonversiInput,
							];
							$arrCutsizePartdetail_kd[] = $rPlate['partdetail_kd'];
						}
					}
					/** Sum Main */
					if ($rPlate['partdetailplate_jenis'] == 'main') {
						$konversiMain = generate_massa_plate($rPlate['partdetailplate_panjang'], $rPlate['partdetailplate_lebar'], $rPlate['partdetailplate_tebal'], $rPlate['partdetailplate_massajenis']) * $rPlate['partdetailplate_qty'];
						$arrMainPartdetail_kd[$rPlate['partdetail_kd'].'/'.$rPlate['rm_kd']][] = round($konversiMain, 3);
					}

					/** Sum koversi plate */
					if (!in_array($rPlate['partdetail_kd'], $arrSumPlatePartdetail_kd)) {
						if ($rPlate['partdetailplate_jenis'] != 'cut_size') {
							$konversiPlate = generate_massa_plate($rPlate['partdetailplate_panjang'], $rPlate['partdetailplate_lebar'], $rPlate['partdetailplate_tebal'], $rPlate['partdetailplate_massajenis']) * $rPlate['partdetailplate_qty'];
							$arrSumPlatePartdetail_kd[$rPlate['partdetail_kd'].'/'.$rPlate['rm_kd']][] = round($konversiPlate, 3);
						}
					}
				}
				/** Sum Main */
				$resultSumMain = [];
				foreach ($arrMainPartdetail_kd as $partdetailkd => $element) {
					$expPartDetail = explode('/', $partdetailkd);
					$resultSumMain[] = [
						'partdetail_kd' => $expPartDetail[0],
						'rm_kd' => $expPartDetail[1],
						'sumMain' => array_sum($element),
					];
				}
				/** Sum koversi plate */
				$resultSumPlate = [];
				foreach ($arrSumPlatePartdetail_kd as $SumPlatepartdetailkd => $elementPlate) {
					$expPlatePartDetail = explode('/', $SumPlatepartdetailkd);
					$resultSumPlate[] = [
						'partdetail_kd' => $expPlatePartDetail[0],
						'rm_kd' => $expPlatePartDetail[1],
						'sumPlate' => array_sum($elementPlate),
					];
				}
				/** Waste */
				$resultWaste = [];
				$sumPlate = 0;
				$waste = 0;
				foreach ($resultCutsize as $rCutsize) {
					foreach ($resultSumPlate as $rSumPlate) {
						if ($rSumPlate['partdetail_kd'] ==  $rCutsize['partdetail_kd']) {
							$sumPlate = $rSumPlate['sumPlate'];
							$waste = $rCutsize['cutSize'] - $sumPlate;
						}
					}
					$resultWaste [] = [
						'partdetail_kd' => $rCutsize['partdetail_kd'],
						'rm_kd' => $rCutsize['rm_kd'],
						'cutSize' => $rCutsize['cutSize'],
						'sumPlate' => $sumPlate,
						'waste' => $waste,
					];
				}
				/** Plate Used (cutsize - ) */
				$resultUsed = 0;
				$wasteUsed = 0;

				$pembagi = count($resultCutsize);
				if($pembagi != 0){
					foreach ($resultCutsize as $rCutsizeUsed) {
						/** waste */
						$wasteUsed = 0;
						foreach ($resultWaste as $rWasteUsed) {
							if ($rWasteUsed['partdetail_kd'] ==  $rCutsizeUsed['partdetail_kd']) {
								$wasteUsed = $rWasteUsed['waste'];
							}
						}
						/** main */
						$sumMain = 0;
						$plateUsed = 0;
						$priceplateUsedGR = 0;
						$priceplateUsedInput = 0;
						foreach ($resultSumMain as $rSumMain) {
							if ($rSumMain['partdetail_kd'] ==  $rCutsizeUsed['partdetail_kd']) {
								$sumMain = $rSumMain['sumMain'];
								$plateUsed = round($sumMain + $wasteUsed, 3);
								$priceplateUsedGR = round($plateUsed * $rCutsizeUsed['hargaKonversiGR'], 2);
								$priceplateUsedInput = round($plateUsed * $rCutsizeUsed['hargaKonversiInput'], 2);
							}
						}
						// $resultUsed [] = [
						//     'partdetail_kd' => $rCutsizeUsed['partdetail_kd'],
						//     'rm_kd' => $rCutsizeUsed['rm_kd'],
						//     'cutSize' => $rCutsizeUsed['cutSize'],
						//     'sumMain' => $sumMain,
						//     'waste' => $wasteUsed,
						//     'plateUsed' => $plateUsed,
						//     'hargaKonversiGR' => $rCutsizeUsed['hargaKonversiGR'],
						//     'hargaKonversiInput' => $rCutsizeUsed['hargaKonversiInput'],
						//     'pricePlateUsedGR' => $priceplateUsedGR,
						//     'pricePlateUsedInput' => $priceplateUsedInput,
						// ];
						$resultUsed += $rCutsizeUsed['hargaKonversiInput'];
					}
					$resultUsed = $resultUsed / $pembagi;
				}

				$data = $resultUsed;
				return $data;
		}catch(Exception $e) {
				$data = 0;
				return $data;
		}
	}

	public function get_totalharga_costing_cust($part_kd) {
		$this->load->model(['td_part_labourcost', 'td_part_overhead', 'td_part_detailplate']);
		$qPartdetail = $this->db->select()
						->from('td_part_detail')
						->join('tm_rawmaterial', 'td_part_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
						->where(['td_part_detail.part_kd' => $part_kd])
						->get()
						->result_array();
		
		$qLabourcost = $this->td_part_labourcost->get_by_param(['part_kd' => $part_kd])->result_array();
		$qOverhead = $this->td_part_overhead->get_by_param(['part_kd' => $part_kd])->result_array();
		$qDetailplate = $this->td_part_detailplate->get_by_param(['part_kd' => $part_kd])->result_array();		

		/** Total harga Non Plate*/
		$arrayNonplate = [];
		$arrayPartdetailkd_plate = [];
		$arrayPlate = [];
		$harga_kg = 0;
		foreach ($qPartdetail as $rPartdetail){
			if ($rPartdetail['rmkategori_kd'] != '01'){
				$arrayNonplate[] = $rPartdetail['partdetail_hargatotal'];
			}else{
				/** detail item Plate */
				$arrayPlate[] = [
					'partdetail_kd' => $rPartdetail['partdetail_kd'],
					'rm_kd' => $rPartdetail['rm_kd'],
				];
			}
		}
		$subNonplate = round(array_sum($arrayNonplate), 2);

		/** Total harga Plate*/
		$arrayUsed = [];
		$arrayCutsize = [];
		$arraySumAdditionlMain = [];
		$arrayMain = [];
		foreach ($arrayPlate as $rPlate) {
			foreach($qDetailplate as $rDetailplate){
				/** Cut Size */
				if ($rDetailplate['partdetail_kd'] == $rPlate['partdetail_kd'] && $rDetailplate['partdetailplate_jenis'] == 'cut_size') {
					$massaPlate_cutsize = round(generate_massa_plate($rDetailplate['partdetailplate_panjang'], $rDetailplate['partdetailplate_lebar'], $rDetailplate['partdetailplate_tebal'], $rDetailplate['partdetailplate_massajenis']) * $rDetailplate['partdetailplate_qty'], 3);
					$arrayCutsize[] = [
						'partdetail_kd' => $rPlate['partdetail_kd'],
						'cutsize_qty' => $massaPlate_cutsize,
						'cutsize_hargaunit' => $rDetailplate['partdetailplate_hargaunit'],
					];
				}

				/** Main plate*/
				if ($rDetailplate['partdetail_kd'] == $rPlate['partdetail_kd'] && $rDetailplate['partdetailplate_jenis'] == 'main') {
					$massaMain = round(generate_massa_plate($rDetailplate['partdetailplate_panjang'], $rDetailplate['partdetailplate_lebar'], $rDetailplate['partdetailplate_tebal'], $rDetailplate['partdetailplate_massajenis']) * $rDetailplate['partdetailplate_qty'], 3);
					$arrayMain[$rDetailplate['partdetail_kd']][] = $massaMain;
				}

				/** Sum qty additional + main */
				if ($rDetailplate['partdetail_kd'] == $rPlate['partdetail_kd'] && $rDetailplate['partdetailplate_jenis'] != 'cut_size') {
					$massaPlate = round(generate_massa_plate($rDetailplate['partdetailplate_panjang'], $rDetailplate['partdetailplate_lebar'], $rDetailplate['partdetailplate_tebal'], $rDetailplate['partdetailplate_massajenis']) * $rDetailplate['partdetailplate_qty'], 3);
					$arrayAllplate_qty[$rPlate['partdetail_kd']][] = $massaPlate;
				}
			}
			if(isset($arrayAllplate_qty[$rPlate['partdetail_kd']])){
				$arraySumAdditionlMain[$rPlate['partdetail_kd']] = array_sum($arrayAllplate_qty[$rPlate['partdetail_kd']]);
			}
		 
		}
		$arrayUsed = [];
		$waste_qty = 0;
		$main_qty = 0;
		$sumUsed_qty = 0;
		foreach ($arrayCutsize as $rCutsize){
			$waste_qty = $rCutsize['cutsize_qty'] - $arraySumAdditionlMain[$rCutsize['partdetail_kd']];
			$main_qty = array_sum($arrayMain[$rCutsize['partdetail_kd']]);
			$sumUsed_qty = $waste_qty + $main_qty;
			$arrayUsed[] = [
				'partdetail_kd' => $rCutsize['partdetail_kd'],
				'used_qty' => $sumUsed_qty,
				'used_hargaunit' => $rCutsize['cutsize_hargaunit'],
				'used_hargatotal' => $sumUsed_qty * $rCutsize['cutsize_hargaunit'],
			];
		}
		$arrPlateTotal = [];
		foreach ($arrayUsed as $rUserhargatotal) {
			$arrPlateTotal[] = $rUserhargatotal['used_hargatotal'];
		}

		$subPlate = round(array_sum($arrPlateTotal), 2);

		/** Total Labourcost */
		$arrayLabourcost = [];
		foreach ($qLabourcost as $rLabourcost) {
			$arrayLabourcost[] = $rLabourcost['partlabourcost_hargatotal'];
		}
		$subLabourcost = round(array_sum($arrayLabourcost), 2);

		/** Total Overhead */
		$arrayOverhead = [];
		foreach ($qOverhead as $rOverhead) {
			$arrayOverhead[] = $rOverhead['partoverhead_hargatotal'];
		}
		$subOverhead = round(array_sum($arrayOverhead), 2);

		$sumTotal = $subNonplate + $subPlate + $subLabourcost + $subOverhead;
		return $sumTotal;
	}


	public function get_totalharga_costing ($part_kd) {
		$this->load->model(['td_part_labourcost', 'td_part_overhead', 'td_part_detailplate']);
		$qPartdetail = $this->db->select()
						->from('td_part_detail')
						->join('tm_rawmaterial', 'td_part_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
						->where(['td_part_detail.part_kd' => $part_kd])
						->get()
						->result_array();
		
		$qLabourcost = $this->td_part_labourcost->get_by_param(['part_kd' => $part_kd])->result_array();
		$qOverhead = $this->td_part_overhead->get_by_param(['part_kd' => $part_kd])->result_array();
		$qDetailplate = $this->td_part_detailplate->get_by_param(['part_kd' => $part_kd])->result_array();		

		/** Total harga Non Plate*/
		$arrayNonplate = [];
		$arrayPartdetailkd_plate = [];
		$arrayPlate = [];
		$harga_kg = 0;
		foreach ($qPartdetail as $rPartdetail){
			if ($rPartdetail['rmkategori_kd'] != '01'){
				$arrayNonplate[] = $rPartdetail['partdetail_hargatotal'];
			}else{
				/** detail item Plate */
				$arrayPlate[] = [
					'partdetail_kd' => $rPartdetail['partdetail_kd'],
					'rm_kd' => $rPartdetail['rm_kd'],
				];
			}
		}
		$subNonplate = round(array_sum($arrayNonplate), 2);

		/** Total harga Plate*/
		$arrayUsed = [];
		$arrayCutsize = [];
		$arraySumAdditionlMain = [];
		$arrayMain = [];
		foreach ($arrayPlate as $rPlate) {
			foreach($qDetailplate as $rDetailplate){
				/** Cut Size */
				if ($rDetailplate['partdetail_kd'] == $rPlate['partdetail_kd'] && $rDetailplate['partdetailplate_jenis'] == 'cut_size') {
					$massaPlate_cutsize = round(generate_massa_plate($rDetailplate['partdetailplate_panjang'], $rDetailplate['partdetailplate_lebar'], $rDetailplate['partdetailplate_tebal'], $rDetailplate['partdetailplate_massajenis']) * $rDetailplate['partdetailplate_qty'], 3);
					$arrayCutsize[] = [
						'partdetail_kd' => $rPlate['partdetail_kd'],
						'cutsize_qty' => $massaPlate_cutsize,
						'cutsize_hargaunit' => $rDetailplate['partdetailplate_hargaunit'],
					];
				}

				/** Main plate*/
				if ($rDetailplate['partdetail_kd'] == $rPlate['partdetail_kd'] && $rDetailplate['partdetailplate_jenis'] == 'main') {
					$massaMain = round(generate_massa_plate($rDetailplate['partdetailplate_panjang'], $rDetailplate['partdetailplate_lebar'], $rDetailplate['partdetailplate_tebal'], $rDetailplate['partdetailplate_massajenis']) * $rDetailplate['partdetailplate_qty'], 3);
					$arrayMain[$rDetailplate['partdetail_kd']][] = $massaMain;
				}

				/** Sum qty additional + main */
				if ($rDetailplate['partdetail_kd'] == $rPlate['partdetail_kd'] && $rDetailplate['partdetailplate_jenis'] != 'cut_size') {
					$massaPlate = round(generate_massa_plate($rDetailplate['partdetailplate_panjang'], $rDetailplate['partdetailplate_lebar'], $rDetailplate['partdetailplate_tebal'], $rDetailplate['partdetailplate_massajenis']) * $rDetailplate['partdetailplate_qty'], 3);
					$arrayAllplate_qty[$rPlate['partdetail_kd']][] = $massaPlate;
				}
			}
			$arraySumAdditionlMain[$rPlate['partdetail_kd']] = array_sum($arrayAllplate_qty[$rPlate['partdetail_kd']]); 
		}
		$arrayUsed = [];
		$waste_qty = 0;
		$main_qty = 0;
		$sumUsed_qty = 0;
		foreach ($arrayCutsize as $rCutsize){
			$waste_qty = $rCutsize['cutsize_qty'] - $arraySumAdditionlMain[$rCutsize['partdetail_kd']];
			$main_qty = array_sum($arrayMain[$rCutsize['partdetail_kd']]);
			$sumUsed_qty = $waste_qty + $main_qty;
			$arrayUsed[] = [
				'partdetail_kd' => $rCutsize['partdetail_kd'],
				'used_qty' => $sumUsed_qty,
				'used_hargaunit' => $rCutsize['cutsize_hargaunit'],
				'used_hargatotal' => $sumUsed_qty * $rCutsize['cutsize_hargaunit'],
			];
		}
		$arrPlateTotal = [];
		foreach ($arrayUsed as $rUserhargatotal) {
			$arrPlateTotal[] = $rUserhargatotal['used_hargatotal'];
		}

		$subPlate = round(array_sum($arrPlateTotal), 2);

		/** Total Labourcost */
		$arrayLabourcost = [];
		foreach ($qLabourcost as $rLabourcost) {
			$arrayLabourcost[] = $rLabourcost['partlabourcost_hargatotal'];
		}
		$subLabourcost = round(array_sum($arrayLabourcost), 2);

		/** Total Overhead */
		$arrayOverhead = [];
		foreach ($qOverhead as $rOverhead) {
			$arrayOverhead[] = $rOverhead['partoverhead_hargatotal'];
		}
		$subOverhead = round(array_sum($arrayOverhead), 2);

		$sumTotal = $subNonplate + $subPlate + $subLabourcost + $subOverhead;
		return $sumTotal;
	}

}