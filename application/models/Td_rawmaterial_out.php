<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_rawmaterial_out extends CI_Model {
	private $tbl_name = 'td_rawmaterial_out';
	private $p_key = 'rmout_kd';

	// public function ssp_table($date) {
	// 	$data['table'] = $this->tbl_name;

	// 	$data['primaryKey'] = $this->p_key;

	// 	$data['columns'] = array(
	// 		array( 'db' => $this->p_key,
	// 			'dt' => 1, 'field' => $this->p_key,
	// 			'formatter' => function($d){
					
	// 				return $this->tbl_btn($d);
	// 			}),
	// 		array( 'db' => 'a.rmin_tglinput', 
	// 			'dt' => 2, 'field' => 'rmin_tglinput',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'b.po_no', 
	// 			'dt' => 3, 'field' => 'po_no',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
    //             }),
	// 		array( 'db' => 'a.rm_kd', 
	// 			'dt' => 4, 'field' => 'rm_kd',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
    //             }),
    //         array( 'db' => 'c.podetail_nama', 
	// 			'dt' => 5, 'field' => 'podetail_nama',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
    //         array( 'db' => 'c.podetail_deskripsi', 
	// 			'dt' => 6, 'field' => 'podetail_deskripsi',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
    //         array( 'db' => 'a.rmin_qty', 
	// 			'dt' => 7, 'field' => 'rmin_qty',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
    //         array( 'db' => 'a.rmin_konversiqty', 
	// 			'dt' => 8, 'field' => 'rmin_konversiqty',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 	);

	// 	$data['sql_details'] = sql_connect();
	// 	$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
	// 						LEFT JOIN tm_purchaseorder as b ON a.po_kd=b.po_kd
	// 						LEFT JOIN td_purchaseorder_detail as c ON a.podetail_kd=c.podetail_kd";
    //     $data['where'] = "DATE(a.rmin_tglinput)='".$date."'";
		
	// 	return $data;
	// }

	// private function tbl_btn($id) {
	// 	$read_access = 1;
	// 	$update_access = 1;
	// 	$delete_access = 1;
	// 	$btns = array();
	// 	$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));	
	// 	$btn_group = group_btns($btns);

	// 	return $btn_group;
	// }

	public function create_code() {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	

    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function delete_by_param($param, $kode) {
		$query = $this->db->delete($this->tbl_name, array($param => $kode)); 
		return $query?TRUE:FALSE;
	}

	public function get_kartustock($rm_kd, $startdate, $enddate){
		$result = $this->db->select($this->tbl_name.'.*, tm_rawmaterial.*, 
			td_rawmaterial_satuan.rmsatuan_nama, satuan_secondary.rmsatuan_nama as satuan_secondary_nama,
			tm_whdeliveryorder.whdeliveryorder_no, tm_whdeliveryorder.whdeliveryorder_note,
			td_materialrequisition_general.materialreqgeneral_tanggal, td_materialrequisition_general.materialreqgeneral_remark,
			tm_materialreceipt.materialreceipt_no, td_workorder_item.woitem_no_wo,
			td_rawmaterial_stok_adjustment.rmstokadj_type, td_rawmaterial_stok_adjustment.rmstokadj_remark
			')
					->from($this->tbl_name)
					->join('tm_rawmaterial', $this->tbl_name.'.rm_kd=tm_rawmaterial.rm_kd', 'left')
					->join('td_rawmaterial_satuan', 'tm_rawmaterial.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
					->join('td_rawmaterial_satuan as satuan_secondary', 'tm_rawmaterial.rmsatuansecondary_kd=satuan_secondary.rmsatuan_kd', 'left')
					
					->join('td_whdeliveryorder_detail', $this->tbl_name.'.whdeliveryorderdetail_kd=td_whdeliveryorder_detail.whdeliveryorderdetail_kd', 'left')
					->join('tm_whdeliveryorder', 'tm_whdeliveryorder.whdeliveryorder_kd=td_whdeliveryorder_detail.whdeliveryorder_kd', 'left')
					
					->join('td_materialrequisition_general', 'td_materialrequisition_general.materialreqgeneral_kd='.$this->tbl_name.'.materialreqgeneral_kd', 'left')

					->join('td_materialreceipt_detail_rawmaterial', 'td_materialreceipt_detail_rawmaterial.materialreceiptdetailrm_kd='.$this->tbl_name.'.materialreceiptdetailrm_kd', 'left')
					->join('td_materialreceipt_detail', 'td_materialreceipt_detail.materialreceiptdetail_kd=td_materialreceipt_detail_rawmaterial.materialreceiptdetail_kd', 'left')
					->join('td_workorder_item', 'td_workorder_item.woitem_kd=td_materialreceipt_detail.woitem_kd', 'left')
					->join('tm_materialreceipt', 'tm_materialreceipt.materialreceipt_kd=td_materialreceipt_detail_rawmaterial.materialreceipt_kd', 'left')

					->join('td_rawmaterial_stok_adjustment', 'td_rawmaterial_stok_adjustment.rmstokadj_kd='.$this->tbl_name.'.rmstokadj_kd', 'left')


					->where($this->tbl_name.'.rm_kd', $rm_kd)
					->where($this->tbl_name.'.rmout_tglinput >=', $startdate)
					->where($this->tbl_name.'.rmout_tglinput <=', $enddate)
					->order_by($this->tbl_name.'.rmout_tglinput')
					->get()->result_array();
		$arrResult = [];
		foreach ($result as $r) {
			$whdo = !empty($r['whdeliveryorder_no']) ? 'WHDO-'.$r['whdeliveryorder_no'] : null;
			$whdoRemark = !empty($r['whdeliveryorder_note']) ? $r['whdeliveryorder_note'] : null;
			$reqGeneralRemark = !empty($r['materialreqgeneral_remark']) ? $r['materialreqgeneral_remark'] : null;
			$reqGeneral = !empty($r['materialreqgeneral_tanggal']) ? 'GENERALREQ-'.$r['materialreqgeneral_tanggal'] : null;
			$receipt = !empty($r['materialreceipt_no']) ? 'MATRECEIPT-'.$r['materialreceipt_no'] : null;
			$receiptRemark = !empty($r['woitem_no_wo']) ? 'WO: ' .$r['woitem_no_wo'] : null;
			$adj = !empty($r['rmstokadj_type']) ? 'ADJUSTMENT-'.$r['rmstokadj_type'] : null;
			$adjRemark = !empty($r['rmstokadj_remark']) ? $r['rmstokadj_remark'] : null;
			$trans_bukti = $whdo.$reqGeneral.$receipt.$adj;
			$trans_bukti_remark = $whdoRemark.$reqGeneralRemark.$receiptRemark.$adjRemark;

			$arrResult[] = [
				'trans_tgl' => format_date($r['rmout_tglinput'], 'Y-m-d H:i:s'),
				'trans_jenis' => 'OUT',
				'rm_kd' => $r['rm_kd'],
				'rm_kode' => $r['rm_kode'],
				'rm_deskripsi' => $r['rm_deskripsi'],
				'rm_spesifikasi' => $r['rm_spesifikasi'],
				'rm_konversiqty' => $r['rm_konversiqty'],
				'trans_bukti' => $trans_bukti,
				'trans_bukti_remark' => $trans_bukti_remark,
				'trans_bukti_uraian' => $trans_bukti.' | '.$trans_bukti_remark,
				'trans_qty_in' => 0,
				'trans_qty_out' => $r['rmout_qty'],
				'trans_satuan' => $r['rmsatuan_nama'],
				'trans_qty_secondary' => $r['rmout_qty'] / $r['rm_konversiqty'],
				'trans_satuan_secondary' => $r['satuan_secondary_nama'],
			];
		}
		return $arrResult;
	}
	
}