<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_rawmaterial_satuan_konversi extends CI_Model
{
    private $tbl_name = 'td_rawmaterial_satuan_konversi';
    private $p_key = 'rmsatuankonversi_kd';

    public function ssp_table()
    {
        $data['table'] = $this->tbl_name;

        $data['primaryKey'] = $this->p_key;

        $data['columns'] = array(
            array(
                'db' => $this->p_key,
                'dt' => 1, 'field' => $this->p_key,
                'formatter' => function ($d) {

                    return $this->tbl_btn($d);
                }
            ),
            array(
                'db' => 'b.rmsatuan_nama',
                'dt' => 2, 'field' => 'rmsatuan_nama',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.rmsatuankonversi_konversi',
                'dt' => 3, 'field' => 'rmsatuankonversi_konversi',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'c.rmsatuan_nama as rmsatuan_to_nama',
                'dt' => 4, 'field' => 'rmsatuan_to_nama',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.rmsatuankonversi_tgledit',
                'dt' => 5, 'field' => 'rmsatuankonversi_tgledit',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
        );

        $data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM " . $this->tbl_name . " as a
								LEFT JOIN td_rawmaterial_satuan as b ON b.rmsatuan_kd=a.rmsatuankonversi_from
								LEFT JOIN td_rawmaterial_satuan as c ON c.rmsatuan_kd=a.rmsatuankonversi_to";
        $data['where'] = "";

        return $data;
    }

    public function ssp_table2 () 
    {
		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		
		$data = [];
		$qData = $this->db->select($this->tbl_name.'.*, fr_satuan.rmsatuan_nama as fr_satuan, to_satuan.rmsatuan_nama as to_satuan')
				->from($this->tbl_name)
				->join('td_rawmaterial_satuan as fr_satuan', $this->tbl_name.'.rmsatuankonversi_from=fr_satuan.rmsatuan_kd', 'left')
				->join('td_rawmaterial_satuan as to_satuan', $this->tbl_name.'.rmsatuankonversi_to=to_satuan.rmsatuan_kd', 'left')
                ->order_by($this->tbl_name.'.rmsatuankonversi_tgledit', 'desc')
				->get();
		foreach ($qData->result_array() as $r) {
			$data[] = [
				'1' => $this->tbl_btn($r['rmsatuankonversi_kd']),
				'2' => $r['fr_satuan'],
				'3' => $r['rmsatuankonversi_konversi'],
				'4' => $r['to_satuan'],
				'5' => $r['rmsatuankonversi_tgledit'],
			];
		}
		
		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

    private function tbl_btn($id)
    {
        $btns = array();
        $btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\'' . $id . '\')'));
        $btns[] = get_btn_divider();
        $btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\'' . $id . '\')'));

        $btn_group = group_btns($btns);

        return $btn_group;
    }

    public function insert_data($data)
    {
        $query = $this->db->insert($this->tbl_name, $data);
        return $query ? TRUE : FALSE;
    }

    public function delete_data($id)
    {
        $query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
        return $query ? TRUE : FALSE;
    }

    public function get_by_param($param = [])
    {
        $this->db->where($param);
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    public function get_all()
    {
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    public function update_data($aWhere = [], $data)
    {
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
        return $query ? TRUE : FALSE;
    }

    public function create_code()
    {
        $query = $this->db->select('MAX(' . $this->p_key . ') as maxID')
            ->get($this->tbl_name)
            ->row();
        $code = (int) $query->maxID + 1;
        return $code;
    }

    public function insert_batch($data)
    {
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
    }

    public function get_by_param_in($param, $params = [])
    {
        $this->db->where_in($param, $params);
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    // public function get_by_param_bom_in($param, $params)
    // {
    //     $query = $this->db->select($this->tbl_name . '.*, tm_bom.bom_kd')
    //         ->where_in($param, $params)
    //         ->join('tm_bom', $this->tbl_name . '.project_kd=tm_bom.project_kd', 'left')
    //         ->get($this->tbl_name);
    //     return $query;
    // }

    // public function get_by_param_reqdetail($param = []) 
    // {
    //     $query = $this->db->where($param)
    //         ->join('tb_materialreceipt_detail_requisition', $this->tbl_name . '.materialreceipt_kd=tb_materialreceipt_detail_requisition.materialreceipt_kd', 'left')
    //         ->join('tm_materialrequisition', 'tb_materialreceipt_detail_requisition.materialreq_kd=tm_materialrequisition.materialreq_kd', 'left')
    //         ->join('td_workflow_state', $this->tbl_name . '.wfstate_kd=td_workflow_state.wfstate_kd', 'left')
    //         ->get($this->tbl_name);
    //     return $query;
    // }

    // public function getRowmaterialreceiptBagianState($id)
    // {
    //     $qRow = $this->db->where('tm_materialreceipt.materialreceipt_kd', $id)
    //         ->join('td_workflow_state', 'td_workflow_state.wfstate_kd=tm_materialreceipt.wfstate_kd', 'left')
    //         ->join('tb_admin', 'tb_admin.kd_admin=tm_materialreceipt.materialreceipt_originator', 'left')
    //         ->join('tb_bagian', 'tb_bagian.bagian_kd=tm_materialreceipt.bagian_kd', 'left')
    //         ->get('tm_materialreceipt')->row_array();
    //     return $qRow;
    // }

    // public function generate_button ($id, $wf_kd, $wfstate_kd) {
	// 	$admin_kd = $this->session->userdata('kd_admin');
	// 	$admin = $this->db->where('kd_admin', $admin_kd)->get('tb_admin')->row_array();
	// 	$kd_tipe_admin = $admin['tipe_admin_kd'];

	// 	$transitions = $this->db->select('td_workflow_transition.*, td_workflow_transition_permission.wftransitionpermission_adminkd, td_workflow_transition_permission.kd_tipe_admin')
	// 				->from('td_workflow_transition')
	// 				->join('td_workflow_transition_permission', 'td_workflow_transition.wftransition_kd=td_workflow_transition_permission.wftransition_kd','left')
	// 				->where('td_workflow_transition.wf_kd', $wf_kd)
	// 				->where('td_workflow_transition.wftransition_source', $wfstate_kd)
	// 				->where('td_workflow_transition.wftransition_active', '1')
	// 				->get()->result_array();
	
	// 	$html = '<div class="btn-group">
	// 	<button type="button" class="btn btn-warning btn-flat dropdown-toggle" data-toggle="dropdown">
	// 		<span class="fa fa fa-hand-o-right"></span>
	// 		Ubah State
	// 	  	<span class="caret"></span>
	// 	</button>
	// 	<ul class="dropdown-menu">';
	// 	$allowTransitions = [];
	// 	foreach ($transitions as $transition) {
	// 		if ($transition['wftransitionpermission_adminkd'] == $admin_kd || $transition['kd_tipe_admin'] == $kd_tipe_admin){
	// 			$allowTransitions[] = [
	// 				'wftransition_kd' => $transition['wftransition_kd'],
	// 				'wf_kd' => $transition['wf_kd'],
	// 				'wftransition_nama' => $transition['wftransition_nama'],
	// 				'wftransition_email' => $transition['wftransition_email'],
	// 			];
	// 		}
	// 	}
	// 	#Group by wftransition_kd
	// 	$tempArrayExist = []; $arrayGroups = [];
	// 	foreach ($allowTransitions as $allowTransition) {
	// 		if (!in_array($allowTransition['wftransition_kd'], $tempArrayExist)){
	// 			$arrayGroups[] = $allowTransition;
	// 			$tempArrayExist[] = $allowTransition['wftransition_kd'];
	// 		}
	// 	}
	// 	#after group
	// 	foreach ($arrayGroups as $arrayGroup) {
	// 		$html .= '<li><a href="javascript:void(0)" onclick=ubah_state("'.$id.'",'.$arrayGroup['wftransition_kd'].')> <i class="fa fa-hand-o-right"></i> '.$arrayGroup['wftransition_nama'].'</a></li>';
	// 	}
		
	// 	$html .= '
	// 	</ul>
	//   </div>';

	// 	return $html;

	// }
}
