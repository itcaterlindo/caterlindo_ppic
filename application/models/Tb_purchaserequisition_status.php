<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_purchaserequisition_status extends CI_Model {
	private $tbl_name = 'tb_purchaserequisition_status';
	private $p_key = 'prstatus_kd';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'prstatus_kd', 
				'dt' => 1, 'field' => 'prstatus_kd',
				'formatter' => function ($d){
					$d = $this->tbl_btn($d);
					
					return $d;
				}),
			array( 'db' => 'prstatus_nama', 
				'dt' => 2, 'field' => 'prstatus_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'prstatus_level', 
				'dt' => 3, 'field' => 'prstatus_level',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM ".$this->tbl_name;
		$data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id) {
		$btns = array();
		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));
		
		$btn_group = group_btns($btns);

		return $btn_group;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }
    
    public function get_all(){
		$query = $this->db->get($this->tbl_name);
		return $query;
	}

	public function isMaxState($prstatus_kd){
		$maxLevel = $this->db->select_max('prstatus_level')->get($this->tbl_name)->row_array();
		$befFinishState = $maxLevel['prstatus_level'] - 1;
		$maxLevelRow = $this->get_by_param(['prstatus_level' => $befFinishState])->row_array();
		if ($prstatus_kd == $maxLevelRow['prstatus_kd']){
			$resp = true;
		}else{
			$resp = false;
		}

		return $resp;
	}

	public function isEditingState($prstatus_kd){
		$maxLevelRow = $this->get_by_param(['prstatus_level' => 1])->row_array();
		if ($prstatus_kd == $maxLevelRow['prstatus_kd']){
			$resp = true;
		}else{
			$resp = false;
		}

		return $resp;
    }
    
    public function maxStatus () {
		$maxLevel = $this->db->select_max('prstatus_level')->get($this->tbl_name)->row_array();
		$befFinishState = $maxLevel['prstatus_level'] - 1;
        $maxLevelRow = $this->get_by_param(['prstatus_level' => $befFinishState])->row_array();
        return $maxLevelRow;
	}
	
	public function isFinishState($prstatus_kd){
		$maxLevel = $this->db->select_max('prstatus_level')->get($this->tbl_name)->row_array();
		$maxLevelRow = $this->get_by_param(['prstatus_level' => $maxLevel['prstatus_level']])->row_array();
		if ($prstatus_kd == $maxLevelRow['prstatus_kd']){
			$resp = true;
		}else{
			$resp = false;
		}

		return $resp;
	}

	public function finishStatus () {
		$maxLevel = $this->db->select_max('prstatus_level')->get($this->tbl_name)->row_array();
        $maxLevelRow = $this->get_by_param(['prstatus_level' => $maxLevel['prstatus_level']])->row_array();
        return $maxLevelRow;
	}
	
}