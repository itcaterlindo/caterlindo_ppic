<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_deliverynote_detail extends CI_Model {
	private $tbl_name = 'td_deliverynote_detail';
	private $p_key = 'dndetail_kd';

    public function ssp_table($dn_kd) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array (
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){

					return $this->tbl_btn($d);
				}),
			array( 'db' => 'b.woitem_no_wo',
				'dt' => 2, 'field' => 'woitem_no_wo',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.woitem_itemcode',
				'dt' => 3, 'field' => 'woitem_itemcode',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.dndetail_qty',
				'dt' => 4, 'field' => 'dndetail_qty',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.dndetail_qtyreceived',
				'dt' => 5, 'field' => 'dndetail_qtyreceived',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.dndetail_remark',
				'dt' => 6, 'field' => 'dndetail_remark',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.dndetail_tglinput',
				'dt' => 7, 'field' => 'dndetail_tglinput',
				'formatter' => function($d){
					return $d;
                }),
            
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
                            LEFT JOIN td_workorder_item as b ON a.woitem_kd=b.woitem_kd";
		$data['where'] = "";

		return $data;
	}

	private function tbl_btn($id) {
		$btns = array();
		$btns[] = get_btn(array('title' => 'Lihat Item', 'icon' => 'search', 'onclick' => 'edit_data(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Cetak Item', 'icon' => 'print', 'onclick' => 'cetak_data(\''.$id.'\')'));
		$btns[] = get_btn_divider();
		$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));
		
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	private function materailreq_status ($materialreq_status) {
		$label = 'warning'; 
		if ($materialreq_status == 'finish') {
			$label = 'success';
		}
		$status = '<span class="label label-'.$label.'">'.ucwords($materialreq_status).'</span>';
		return $status;
	} 

	public function create_code() {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

	public function create_no ($bagian_kd, $dn_tanggal) {
        $this->load->model(['tb_bagian']);
        $bagian = $this->tb_bagian->get_by_param (['bagian_kd' => $bagian_kd])->row_array();

		$dn_year = format_date($dn_tanggal, 'Y');
        $query = $this->db->select('MAX(dn_no) as max_no')
                ->where('YEAR(dn_tanggal)', $dn_year)
                ->where('woitem_kd', $bagian_kd)
				->get($this->tbl_name)->row();
		if (empty($query->max_no)) :
			$no = 0;
		else:
			$no = substr($query->max_no, -4);
        endif;
        $no = (int)$no + 1;
        $dn_no = 'DN'.'-'.strtoupper($bagian['bagian_nama']).'-'.$dn_year.'-'.str_pad($no, '4', '0', STR_PAD_LEFT);

		return $dn_no;
	}

	public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}

	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }
    
    public function insert_batch_data ($data=[]){
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
    
    }

    public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

    public function get_where_in ($where, $in = []){
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}

	public function get_by_param_detail ($param=[]) {
		$query = $this->db->select($this->tbl_name.'.*, bagian_asal.bagian_nama as bagian_asal, bagian_tujuan.bagian_nama as bagian_tujuan')
					->from($this->tbl_name)
					->join('tb_bagian as bagian_asal', $this->tbl_name.'.dn_asal=bagian_asal.bagian_kd', 'left')
					->join('tb_bagian as bagian_tujuan', $this->tbl_name.'.dn_tujuan=bagian_tujuan.bagian_kd', 'left')
					->where($param)
					->get();
		return $query;
	}
}