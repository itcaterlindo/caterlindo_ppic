<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class M_salesorder extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->model(['tm_project']);
	}

	function salesOrderItems($kd_msalesorder) {
		$this->db->select('a.kd_ditem_so, a.msalesorder_kd, a.barang_kd, a.item_barcode, a.item_status, a.item_desc, a.item_dimension, a.item_qty, a.harga_barang, a.harga_retail, a.disc_type, a.item_disc, a.total_harga, a.item_note, a.item_code, b.hs_code, a.netweight, a.grossweight, a.item_cbm, a.order_tipe')
			->from('td_salesorder_item a')
			->join('tm_barang b', 'b.kd_barang = a.barang_kd', 'left')
			->where(array('a.msalesorder_kd' => $kd_msalesorder))
			->order_by('a.kd_ditem_so ASC');
		$query = $this->db->get();
		$result = $query->result();

		return $result;
	}

	function item_child($ditem_so_kd) {
		$result = '';
		if (!empty($ditem_so_kd)) :
			$this->db->select('a.kd_citem_so, a.ditem_so_kd, a.kd_parent, a.kd_child, a.item_barcode, a.item_status, a.item_desc, a.item_dimension, a.item_qty, a.harga_barang, a.harga_retail, a.disc_type, a.item_disc, a.total_harga, a.item_note, a.item_code, c.hs_code, a.netweight, a.grossweight, a.item_cbm, a.order_tipe')
				->from('td_salesorder_item_detail a')
				->join('tm_barang b', 'b.kd_barang = a.kd_parent', 'left')
				->join('tm_barang c', 'c.kd_barang = a.kd_child', 'left')
				->where_in('a.ditem_so_kd', $ditem_so_kd)
				->order_by('a.ditem_so_kd ASC, a.kd_citem_so ASC');
			$query = $this->db->get();
			$result = $query->result_array();
		endif;

		return $result;
	}

	function insert_data_parent($data = array()) {
		/** add no Porject untuk item custom */
		$data_pj = array();
		$arrProject = array();
		if ($data['item_status'] == 'custom'):
			$arrProject = array(
				'project_kd' => $this->tm_project->create_code (0, ''),
				'kd_msalesorder' => $data['msalesorder_kd'],
				'kd_ditem_so' => $data['kd_ditem_so'],	
				'project_nopj' => $this->tm_project->generate_nopj('', ''),
				'project_itemcode' => $data['item_code'],
				'project_itemdesc' => $data['item_desc'],
				'project_itemdimension' => $data['item_dimension'],
				'project_hargabarang' => $data['harga_barang'],
				'project_status' => 'pending',
				'project_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin')
			);
		endif;
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s'));
		$submit = array_merge($data, $data_adm);
		$aksi['soItem'] = $this->db->insert('td_salesorder_item', $submit);
		if(!empty($arrProject)):
			$aksi['project'] = $this->tm_project->insert_data($arrProject);
		endif;
		return $aksi?TRUE:FALSE;
	}

	function insert_data_child($data = array()) {
		/** add no Porject untuk item custom */
		$data_pj = array();
		$arrProject = array();
		if ($data['item_status'] == 'custom'):
			$arrProject = array(
				'project_kd' => $this->tm_project->create_code(0, ''),
				'kd_msalesorder' => $data['msalesorder_kd'],
				'kd_citem_so' => $data['kd_citem_so'],	
				'project_nopj' => $this->tm_project->generate_nopj('', ''),
				'project_itemcode' => $data['item_code'],
				'project_itemdesc' => $data['item_desc'],
				'project_itemdimension' => $data['item_dimension'],
				'project_hargabarang' => $data['harga_barang'],
				'project_status' => 'pending',
				'project_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin')
			);
		endif;
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s'));
		$submit = array_merge($data, $data_adm);
		$aksi['soItemDetail'] = $this->db->insert('td_salesorder_item_detail', $submit);
		if(!empty($arrProject)):
			$aksi['project'] = $this->tm_project->insert_data($arrProject);
		endif;
		return $aksi?TRUE:FALSE;
	}

	function update_data_parent($data = array(), $where = array()) {
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s'));
		$submit = array_merge($data, $data_adm);
		$aksi = $this->db->update('td_salesorder_item', $submit, $where);
		return $aksi?TRUE:FALSE;
	}

	function update_data_child($data = array(), $where = array()) {
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s'));
		$submit = array_merge($data, $data_adm);
		$aksi = $this->db->update('td_salesorder_item_detail', $submit, $where);
		return $aksi?TRUE:FALSE;
	}

	function get_price($kd_msalesorder = '') {
		$this->db->from('td_salesorder_harga');
		if (is_array($kd_msalesorder)) :
			$this->db->where_in('msalesorder_kd', $kd_msalesorder);
		else :
			$this->db->where(array('msalesorder_kd' => $kd_msalesorder));
		endif;
		$query = $this->db->get();
		$num = $query->num_rows();
		$result = $query->result();

		return $result;
	}

	function get_parent($kd_item) {
		$this->db->select('a.kd_ditem_so, a.barang_kd, a.item_status, a.item_code, a.item_barcode, a.item_desc, a.item_dimension, a.item_qty, a.harga_barang, a.harga_retail, a.disc_type, a.item_disc, a.disc_customer, a.disc_distributor, a.total_harga, a.item_note, a.netweight, a.grossweight, a.boxweight, a.length_cm, a.width_cm, a.height_cm, a.item_cbm, b.item_code AS code_ori, b.item_barcode AS barcode_ori, b.deskripsi_barang AS desc_ori, b.dimensi_barang AS dimensi_ori, b.netweight AS netweight_ori, b.grossweight AS grossweight_ori, b.boxweight AS boxweight_ori, b.length_cm AS length_cm_ori, b.width_cm AS width_cm_ori, b.height_cm AS height_cm_ori, b.item_cbm AS item_cbm_ori');
		$this->db->from('td_salesorder_item a');
		$this->db->join('tm_barang b', 'b.kd_barang = a.barang_kd', 'left');
		$this->db->where(array('a.kd_ditem_so' => $kd_item));
		$query = $this->db->get();
		$result = $query->row();
		return $result;
	}

	function get_child($kd_item) {
		$this->db->select('a.kd_citem_so, a.ditem_so_kd, a.kd_parent, a.kd_child, a.item_barcode, a.item_code AS child_code, a.item_status, a.item_desc, a.item_dimension, a.item_qty, a.harga_barang, a.harga_retail, a.disc_type, a.item_disc, a.disc_customer, a.disc_distributor, a.total_harga, a.item_note, a.netweight, a.grossweight, a.boxweight, a.length_cm, a.width_cm, a.height_cm, a.item_cbm, b.item_code AS parent_code, b.deskripsi_barang AS parent_desc, c.item_barcode AS barcode_ori, c.item_code AS code_ori, c.deskripsi_barang AS desc_ori, c.dimensi_barang AS dimensi_ori, c.netweight AS netweight_ori, c.grossweight AS grossweight_ori, c.boxweight AS boxweight_ori, c.length_cm AS length_cm_ori, c.width_cm AS width_cm_ori, c.height_cm AS height_cm_ori, c.item_cbm AS item_cbm_ori');
		$this->db->from('td_salesorder_item_detail a');
		$this->db->join('tm_barang b', 'b.kd_barang = a.kd_parent', 'left');
		$this->db->join('tm_barang c', 'c.kd_barang = a.kd_child', 'left');
		$this->db->where(array('a.kd_citem_so' => $kd_item));
		$query = $this->db->get();
		$result = $query->row();
		return $result;
	}

	function insert_data_harga($data = array()) {
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s'));
		$submit = array_merge($data, $data_adm);
		$aksi = $this->db->insert('td_salesorder_harga', $submit);
		return $aksi?TRUE:FALSE;
	}

	function last_id() {
		$this->db->select('kd_ditem_so');
		$this->db->from('td_salesorder_item');
		$this->db->where(array('DATE(tgl_input)' => date('Y-m-d')));
		$this->db->order_by('tgl_input DESC, kd_ditem_so DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$num = $query->num_rows();
		$row = $query->row();
		if ($num > 0) :
			$id = $row->kd_ditem_so;
			$last_id = substr($id, 9);
		endif;
		return isset($last_id)?$last_id:'0';
	}

	function trans_to_salesitem($datas) {
		$no = 0;
		if (!empty($datas)) :
			foreach ($datas as $data) :
				$no++;
				$item[$no]['kd_ditem_so'] = $data->kd_ditem_quotation;
				$item[$no]['msalesorder_kd'] = $data->mquotation_kd;
				$item[$no]['barang_kd'] = $data->barang_kd;
				$item[$no]['item_code'] = $data->item_code;
				$item[$no]['item_barcode'] = $data->item_barcode;
				$item[$no]['item_status'] = $data->item_status;
				$item[$no]['item_desc'] = $data->item_desc;
				$item[$no]['item_dimension'] = $data->item_dimension;
				$item[$no]['netweight'] = $data->netweight;
				$item[$no]['grossweight'] = $data->grossweight;
				$item[$no]['boxweight'] = $data->boxweight;
				$item[$no]['length_cm'] = $data->length_cm;
				$item[$no]['width_cm'] = $data->width_cm;
				$item[$no]['height_cm'] = $data->height_cm;
				$item[$no]['item_cbm'] = $data->item_cbm;
				$item[$no]['item_qty'] = $data->item_qty;
				$item[$no]['harga_barang'] = $data->harga_barang;
				$item[$no]['harga_retail'] = $data->harga_retail;
				$item[$no]['disc_type'] = $data->disc_type;
				$item[$no]['item_disc'] = $data->item_disc;
				$item[$no]['total_harga'] = $data->total_harga;
				$item[$no]['item_note'] = $data->item_note;
				$item[$no]['admin_kd'] = $this->session->kd_admin;
				$item[$no]['tgl_input'] = date('Y-m-d H:i:s');
			endforeach;
			return $this->db->insert_batch('td_salesorder_item', $item);
		else :
			return TRUE;
		endif;
	}

	function trans_to_salesitemdetail($datas) {
		$no = 0;
		if (!empty($datas)) :
			foreach ($datas as $data) :
				$no++;
				$item[$no]['kd_citem_so'] = $data->kd_citem_quotation;
				$item[$no]['ditem_so_kd'] = $data->ditem_quotation_kd;
				$item[$no]['msalesorder_kd'] = $data->mquotation_kd;
				$item[$no]['kd_parent'] = $data->kd_parent;
				$item[$no]['kd_child'] = $data->kd_child;
				$item[$no]['item_code'] = $data->item_code;
				$item[$no]['item_barcode'] = $data->item_barcode;
				$item[$no]['item_status'] = $data->item_status;
				$item[$no]['item_desc'] = $data->item_desc;
				$item[$no]['item_dimension'] = $data->item_dimension;
				$item[$no]['netweight'] = $data->netweight;
				$item[$no]['grossweight'] = $data->grossweight;
				$item[$no]['boxweight'] = $data->boxweight;
				$item[$no]['length_cm'] = $data->length_cm;
				$item[$no]['width_cm'] = $data->width_cm;
				$item[$no]['height_cm'] = $data->height_cm;
				$item[$no]['item_cbm'] = $data->item_cbm;
				$item[$no]['item_qty'] = $data->item_qty;
				$item[$no]['harga_barang'] = $data->harga_barang;
				$item[$no]['harga_retail'] = $data->harga_retail;
				$item[$no]['disc_type'] = $data->disc_type;
				$item[$no]['item_disc'] = $data->item_disc;
				$item[$no]['total_harga'] = $data->total_harga;
				$item[$no]['item_note'] = $data->item_note;
				$item[$no]['admin_kd'] = $this->session->kd_admin;
				$item[$no]['tgl_input'] = date('Y-m-d H:i:s');
			endforeach;
			return $this->db->insert_batch('td_salesorder_item_detail', $item);
		else :
			return TRUE;
		endif;
	}

    function trans_to_salesharga($datas) {
		$no = 0;
		if (!empty($datas)) :
			foreach ($datas as $data) :
				$no++;
				$item[$no]['kd_dharga_so'] = $data->kd_dharga_quotation;
				$item[$no]['msalesorder_kd'] = $data->mquotation_kd;
				$item[$no]['nm_kolom'] = $data->nm_kolom;
				$item[$no]['type_kolom'] = $data->type_kolom;
				$item[$no]['total_nilai'] = $data->total_nilai;
				$item[$no]['admin_kd'] = $this->session->kd_admin;
				$item[$no]['tgl_input'] = date('Y-m-d H:i:s');
			endforeach;
			return $this->db->insert_batch('td_salesorder_harga', $item);
		else :
			return TRUE;
		endif;
	}

	public function count_item_so($kd_msalesorder = '') {
		$this->db->select('SUM(b.item_qty) + SUM(c.item_qty) AS jml_item')
			->from('tm_salesorder a')
			->join('td_salesorder_item b', 'b.msalesorder_kd = a.kd_msalesorder', 'left')
			->join('td_salesorder_item_detail c', 'c.msalesorder_kd = a.kd_msalesorder', 'left')
			->where(array('a.kd_msalesorder' => $kd_msalesorder));
		$query = $this->db->get();
		$row = $query->row();
		$jml_item = $row->jml_item;
		return $jml_item;
	}

	public function count_so_master_items($kd_msalesorder = '') {
		$this->db->select('SUM(item_qty) as tot_master')
			->from('td_salesorder_item')
			->where(array('msalesorder_kd' => $kd_msalesorder));
		$query = $this->db->get();
		$row = $query->row();
		$tot_master = $row->tot_master;
		return $tot_master;
	}

	public function count_so_detail_items($kd_msalesorder = '') {
		$this->db->select('SUM(item_qty) as tot_detail')
			->from('td_salesorder_item_detail')
			->where(array('msalesorder_kd' => $kd_msalesorder));
		$query = $this->db->get();
		$row = $query->row();
		$tot_detail = $row->tot_detail;
		return $tot_detail;
	}

	public function sum_totalharga_exclude($kdmso=[]){
		$this->load->model(['td_salesorder_item','td_salesorder_item_detail']);
		$parent = $this->td_salesorder_item->get_item_sumtotal_by_mso($kdmso)->result();
		$child = $this->td_salesorder_item_detail->get_item_sumtotal_by_mso($kdmso)->result();
		$diskon = $this->db->select('msalesorder_kd, SUM(total_nilai) as sum_total_nilai')
					->from('td_salesorder_harga')
					->where_in('msalesorder_kd', $kdmso)
					->get()
					->result();
		foreach ($kdmso as $kdmso):
			$total = 0;
			foreach ($parent as $r):
				if ($r->msalesorder_kd == $kdmso):
					$total += $r->sum_total_harga;
				endif;
			endforeach;
			
			foreach ($child as $c):
				if ($c->msalesorder_kd == $kdmso):
					$total += $c->sum_total_harga;
				endif;
			endforeach;

			foreach ($diskon as $d):
				if ($d->msalesorder_kd == $kdmso):
					$total = $total - $d->sum_total_nilai;
				endif;
			endforeach;

			$arr_total[] = array(
				'mso_kd' => $kdmso,
				'total' => $total,
			);
		endforeach;
		
		return $arr_total;
	}

	/** Untuk rekap berdasarkan std/custom jumlah item untuk rekap finishgood out */
	public function get_rekapharga_itemstatus_by_so($kd_msalesorder) {
		/** Item*/
		$itemStd = $this->db->select('a.kd_msalesorder, a.no_salesorder, b.barang_kd, b.item_barcode, b.item_code, b.item_status, SUM(b.item_qty) AS item_qty, b.harga_barang, b.harga_retail, b.disc_type, b.item_disc, b.total_harga, b.item_note, b.netweight, b.grossweight, b.item_cbm, b.order_tipe')
					->from('tm_salesorder AS a ')
					->join('td_salesorder_item AS b', 'a.kd_msalesorder=b.msalesorder_kd', 'left')
					->where('a.kd_msalesorder', $kd_msalesorder)
					->where('b.item_status', 'std')
					->group_by('b.barang_kd')
					->get_compiled_select();
		$itemCustom = $this->db->select('a.kd_msalesorder, a.no_salesorder, "PRD020817000573" AS barang_kd, "899000" AS item_barcode, "CUSTOM ITEM" AS item_code, b.item_status, SUM(b.item_qty) AS item_qty, b.harga_barang, b.harga_retail, b.disc_type, b.item_disc, b.total_harga, b.item_note, b.netweight, b.grossweight, b.item_cbm, b.order_tipe')
					->from('tm_salesorder AS a ')
					->join('td_salesorder_item AS b', 'a.kd_msalesorder=b.msalesorder_kd', 'left')
					->where('a.kd_msalesorder', $kd_msalesorder)
					->where('b.item_status', 'custom')
					->group_by('b.item_status')
					->get_compiled_select();
		/** Item Detail*/
		$itemDetailStd = $this->db->select('a.kd_msalesorder, a.no_salesorder, b.kd_child AS barang_kd, b.item_barcode, b.item_code, b.item_status, SUM(b.item_qty) AS item_qty, b.harga_barang, b.harga_retail, b.disc_type, b.item_disc, b.total_harga, b.item_note, b.netweight, b.grossweight, b.item_cbm, b.order_tipe')
					->from('tm_salesorder AS a ')
					->join('td_salesorder_item_detail AS b', 'a.kd_msalesorder=b.msalesorder_kd', 'left')
					->where('a.kd_msalesorder', $kd_msalesorder)
					->where('b.item_status', 'std')
					->group_by('b.kd_child')
					->get_compiled_select();
		$itemDetailCustom = $this->db->select('a.kd_msalesorder, a.no_salesorder, "PRD020817000573" AS barang_kd, "899000" AS item_barcode, "CUSTOM ITEM" AS item_code, b.item_status, SUM(b.item_qty) AS item_qty, b.harga_barang, b.harga_retail, b.disc_type, b.item_disc, b.total_harga, b.item_note, b.netweight, b.grossweight, b.item_cbm, b.order_tipe')
					->from('tm_salesorder AS a ')
					->join('td_salesorder_item_detail AS b', 'a.kd_msalesorder=b.msalesorder_kd', 'left')
					->where('a.kd_msalesorder', $kd_msalesorder)
					->where('b.item_status', 'custom')
					->group_by('b.item_status')
					->get_compiled_select();
		$query = ($itemStd . ' UNION ALL ' . $itemCustom. ' UNION ALL ' .$itemDetailStd. ' UNION ALL ' .$itemDetailCustom);
		$queryRekap = $this->db->select('t.kd_msalesorder, t.no_salesorder, t.barang_kd, t.item_barcode, t.item_code, t.item_status, SUM(qty) AS item_qty, b.deskripsi_barang, b.dimensi_barang, c.nm_group, t.harga_barang, t.harga_retail, t.disc_type, t.item_disc, t.total_harga, t.item_note, t.netweight, t.grossweight, t.item_cbm, t.order_tipe')
					->from('('.$query.') as t')
					->join('tm_barang AS b', 't.barang_kd = b.kd_barang', 'left')
					->join('tm_group_barang AS c', 'c.kd_group_barang = b.group_barang_kd', 'left')
					->group_by('t.barang_kd')
					->get_compiled_select();
		$queryJoinGrouping = $this->db->select('tr.*, pg.kd_mgrouping')
					->from('('.$queryRekap. ') as tr')
					->join('tm_product_grouping AS pg', 'tr.barang_kd=pg.barang_kd', 'left')
					->get_compiled_select();
		$act = $this->db->query($queryJoinGrouping)->result();

		return $act;
	}

}