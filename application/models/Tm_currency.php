<?php
defined('BASEPATH') or exit('No direct script acccess allowed!');

class Tm_currency extends CI_Model {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'tm_currency';
	private $p_key = 'kd_currency';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){

					return $this->tbl_btn($d, $row[1]);
				} ),
			array( 'db' => $this->p_key,
				'dt' => 2, 'field' => $this->p_key,
				'formatter' => function($d){

					return $d;
				} ),
			array( 'db' => 'currency_nm',
				'dt' => 3, 'field' => 'currency_nm',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				} ),
			array( 'db' => 'currency_type',
				'dt' => 4, 'field' => 'currency_type',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				} ),
			array( 'db' => 'icon_type',
				'dt' => 5, 'field' => 'icon_type',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				} ),
			array( 'db' => 'currency_icon',
				'dt' => 6, 'field' => 'currency_icon',
				'formatter' => function($d, $row){
					$d = $this->security->xss_clean($d);
					$data = currency_icon($row[4], $d);

					return $data;
				} ),
			array( 'db' => 'decimal', 'dt' => 7, 'field' => 'decimal', 'formatter' => function($d) {
				return choice_enum($d);
			} ),
		);

		$data['sql_details'] = sql_connect();

		$data['joinQuery'] = "";
		$data['where'] = "currency_flag = 'active'";
		return $data;
	}

	function tbl_btn($id, $var) {		
		$read_access = $this->session->read_access;
		$update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
		$btns[] = get_btn_divider();
		$btns[] = get_btn(array('access' => $delete_access, 'title' => 'Hapus', 'icon' => 'trash', 'onclick' => 'return confirm(\'Anda akan menghapus Set Currency = '.$var.'?\')?hapus_data(\''.$id.'\'):false'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	function get_all() {
		$this->db->from($this->tbl_name);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	function get_primary() {
		$this->db->from($this->tbl_name);
		$this->db->where(array('currency_type' => 'primary'));
		$query = $this->db->get();
		$result = $query->row();
		return $result;
	}

	function get_secondary() {
		$this->db->from($this->tbl_name);
		$this->db->where(array('currency_type' => 'secondary'));
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	function get_data($code) {
		$this->db->from($this->tbl_name);
		$this->db->where(array('kd_currency' => $code));
		$query = $this->db->get();
		$result = $query->row();
		return $result;
	}

	function form_simbol($tipe, $simbol) {
		$form = '';
		$list = $this->tb_icon_currency->get_all();
		if (!empty($tipe)) :
			$form .= '<div class="form-group">';
			$form .= '<label for="idSimbolCurrency" class="col-md-2 control-label">Simbol Currency</label>';
			$form .= '<div class="col-md-3">';
			$form .= '<div id="idErrSimbol"></div>';
			if ($tipe == 'icon') :
				$pil = array('' => '-- Pilih Simbol Currency --');
				foreach ($list as $icon) :
					$pil[$icon->icon_currency] = $icon->icon_unicode.' - '.$icon->nm_currency;
				endforeach;
				$sel = $simbol;
				$attr = array('id' => 'idSimbolCurrency', 'class' => 'form-control', 'style' => 'font-family: \'FontAwesome\';');
				$form .= form_dropdown('txtSimbol', $pil, $sel, $attr);
			elseif ($tipe == 'text') :
				$form .= form_input(array('name' => 'txtSimbol', 'id' => 'idSimbolCurrency', 'class' => 'form-control', 'placeholder' => 'Simbol Currency', 'value' => $simbol));
			endif;
			$form .= '</div>';
			$form .= '</div>';
		endif;

		return $form;
	}

	function check_type($type) {
		$str['confirm'] = 'success';
		$str['alert'] = '';
		if ($type == 'primary') :
			$this->db->from($this->tbl_name);
			$this->db->where(array('currency_type' => 'primary'));
			$query = $this->db->get();
			$num = $query->num_rows();
			if ($num > 0) :
				$str['confirm'] = 'error';
				$str['alert'] = buildLabel('warning', 'Hanya ada satu tipe primary untuk set currency');
			endif;
		endif;
		return $str;
	}

	function create_pkey() {
		$conds = array(
			'select' => 'tgl_input, '.$this->p_key,
			'order_by' => 'tgl_input DESC',
			'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
		);
		$pkey = $this->m_builder->buat_kode($this->tbl_name, $conds, 3, 'MCN');
		return $pkey;
	}

	function send_data($data) {
		if (empty($data['kd_currency'])) :
			$label_err = 'menambahkan';
			$data['kd_currency'] = $this->create_pkey();
			$act = $this->input_data($data);
		else :
			$label_err = 'mengubah';
			$act = $this->update_data($data, array('kd_currency' => $data['kd_currency']));
		endif;
		
		if ($act) :
			$log_stat = 'berhasil '.$label_err.' Set Currency';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$log_stat.'!');
		else :
			$log_stat = 'gagal '.$label_err.' Set Currency';
			$str['confirm'] = 'error';
			$str['idErrForm'] = buildAlert('danger', 'Gagal!', 'Gagal '.$log_stat.', kesalahan sistem!');
		endif;
		$this->m_builder->write_log($log_stat, $label_err, $data);

		return $str;
	}

	function input_data($data) {
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s'), 'currency_flag' => 'active');
		$submit = array_merge($data, $data_adm);
		$aksi = $this->db->insert($this->tbl_name, $submit);
		return $aksi?TRUE:FALSE;
	}

	function update_data($data, $where) {
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s'));
		$submit = array_merge($data, $data_adm);
		$aksi = $this->db->update($this->tbl_name, $submit, $where);
		return $aksi?TRUE:FALSE;
	}

	function format_helper_currency ($kd_currency) {
		$row = $this->get_data($kd_currency);
		$format_helper = $row->icon_type.' '.$row->currency_icon.' '.$row->pemisah_angka.' '.$row->format_akhir;
		return $format_helper;
	}

	function format_helper_currency_all () {
		$rows = $this->get_all();
		$format_helper = '';
		$results = [];
		foreach($rows as $row){
			$format_helper = $row->icon_type.' '.$row->currency_icon.' '.$row->pemisah_angka.' '.$row->format_akhir;
			$results[$row->kd_currency] = $format_helper;
		}
		return $results;
	}
}