<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_inspection_product extends CI_Model {
	private $tbl_name = 'tm_inspection_product';
	private $p_key = 'kd_inspectionproduct';
	private $title_name = 'Data inspection Product';

	/* --start ssp tabel untuk modul data inspection product-- */
	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){

					return $this->tbl_btn($d, $row[2], $row[12]);
				}),
			array( 'db' => 'inspectionproduct_tanggal', 'dt' => 2, 'field' => 'inspectionproduct_tanggal'),
			array( 'db' => 'inspectionproduct_no', 'dt' => 3, 'field' => 'inspectionproduct_no'),
			array( 'db' => 'inspectionproduct_item_code', 'dt' => 4, 'field' => 'inspectionproduct_item_code'),
			array( 'db' => 'woitem_no_wo', 'dt' => 5, 'field' => 'woitem_no_wo'),
			array( 'db' => 'bagian_nama', 'dt' => 6, 'field' => 'bagian_nama'),
			array( 'db' => 'inspectionproduct_tindakan', 'dt' => 7, 'field' => 'inspectionproduct_tindakan'),
			array( 'db' => 'inspectionproduct_qty', 'dt' => 8, 'field' => 'inspectionproduct_qty'),
			array( 'db' => 'bagian_nama2', 'dt' => 9, 'field' => 'bagian_nama2'),
			array( 'db' => 'inspectionproduct_penyebab', 'dt' => 10, 'field' => 'inspectionproduct_penyebab'),
			array( 'db' => 'inspectionproduct_keterangan', 'dt' => 11, 'field' => 'inspectionproduct_keterangan'),
			array( 'db' => 'inspectionproduct_tgledit', 'dt' => 12, 'field' => 'inspectionproduct_tgledit'),
			array( 'db' => 'flag_close', 'dt' => 13, 'field' => 'flag_close', 'formatter' => function($d, $row){
				if($d == 0):
					$word = "Open";
					$color = "green";
				else:
					$word = "Close";
					$color = "red";
				endif;
				$label = build_span($word, $color);
				return $label;
			}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." AS a 
							  LEFT JOIN td_workorder_item AS b ON b.woitem_kd = a.inspectionproduct_woitem_kd
							  LEFT JOIN tb_bagian AS c ON c.bagian_kd = a.inspectionproduct_bagian_kd
							  LEFT JOIN (SELECT bagian_kd AS bagian_kd2, bagian_nama AS bagian_nama2 FROM tb_bagian) AS d ON d.bagian_kd2=a.inspectionproduct_penyebab_bagian_kd
							  LEFT JOIN tb_inspectionproduct_state AS e ON e.inspectionproductstate_kd = a.inspectionproductstate_kd";
		$data['where'] = "";

		return $data;
	}

	public function ssp_table2()
	{
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));

		$viewPermission = cek_permission('INSPECTIONPRODUCT_VIEW');
		if($viewPermission){
			$qData = $this->db->query("SELECT * FROM ".$this->tbl_name." AS a 
				LEFT JOIN td_workorder_item AS b ON b.woitem_kd = a.inspectionproduct_woitem_kd
				LEFT JOIN tb_bagian AS c ON c.bagian_kd = a.inspectionproduct_bagian_kd
				LEFT JOIN (SELECT bagian_kd AS bagian_kd2, bagian_nama AS bagian_nama2 FROM tb_bagian) AS d ON d.bagian_kd2=a.inspectionproduct_penyebab_bagian_kd
				LEFT JOIN tb_inspectionproduct_state AS e ON e.inspectionproductstate_kd = a.inspectionproductstate_kd
				ORDER BY a.inspectionproductstate_kd ASC, a.inspectionproduct_no DESC")
				->result_array();
		}else{
			$qData = [];
		}
		
		$data = [];
		foreach ($qData as $r) {
			$data[] = [
				'1' => $this->tbl_btn($r['kd_inspectionproduct'], $r['inspectionproduct_no'], $r['inspectionproductstate_kd']),
				'2' => $r['inspectionproduct_tanggal'],
				'3' => $r['inspectionproduct_no'],
				'4' => $r['inspectionproduct_item_code'],
				'5' => $r['woitem_no_wo'],
				'6' => $r['bagian_nama'],
				'7' => $r['inspectionproduct_tindakan'],
				'8' => $r['inspectionproduct_qty'],
				'9' => $r['bagian_nama2'],
				'10' => $r['inspectionproduct_penyebab'],
				'11' => $r['inspectionproduct_keterangan'],
				'12' => $r['inspectionproduct_tgledit'],
				'13' => build_span($r['inspectionproductstate_span'], $r['inspectionproductstate_nama']),
			];
		}

		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

	public function ssp_bagian_table2()
	{
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		$userdataAdmin = $this->session->userdata('kd_admin');
		$whereBagian = $this->get_allowed_bagian($userdataAdmin, 'bagian');

		if( !empty($whereBagian) ){
			$qData = $this->db->select('*')
				->from($this->tbl_name.' AS a')
				->join('td_workorder_item AS b', 'b.woitem_kd = a.inspectionproduct_woitem_kd', 'left')
				->join('tb_bagian AS c', 'c.bagian_kd = a.inspectionproduct_bagian_kd', 'left')
				->join('tb_inspectionproduct_state AS d', 'd.inspectionproductstate_kd = a.inspectionproductstate_kd', 'left')
				->where_not_in('a.inspectionproductstate_kd', ['0', '1'])
				->where_in('a.inspectionproduct_bagian_kd', $whereBagian)->get()->result_array();
		}else{
			$qData = [];
		}

		$data = [];
		foreach ($qData as $r) {
			$span = "";
			$btn = "";
			if($r['flag_approved_bagian'] == 1){
				$span = build_span('primary', 'Approved');
			}else{
				$span = build_span('danger', 'Belum');
			}

			if($r['flag_approved_bagian'] == 0){
				$btn ='<button type="button" onclick="approved_bagian(\''.$r['kd_inspectionproduct'].'\')" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Approved</button>';
			}

			$data[] = [
				'1' => $btn,
				'2' => $r['inspectionproduct_tanggal'],
				'3' => $r['inspectionproduct_no'],
				'4' => $r['inspectionproduct_item_code'],
				'5' => $r['woitem_no_wo'],
				'6' => $r['inspectionproduct_tindakan'],
				'7' => $r['inspectionproduct_qty'],
				'8' => $r['bagian_nama'],
				'9' => $r['inspectionproduct_keterangan'],
				'10' => $r['approved_bagian_tgl'],
				'11' => $span
			];
		}

		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

	public function ssp_penyebab_table2()
	{
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		$userdataAdmin = $this->session->userdata('kd_admin');
		$whereBagian = $this->get_allowed_bagian($userdataAdmin, 'penyebab');

		if( !empty($whereBagian) ){
			$qData = $this->db->select('*')
				->from($this->tbl_name.' AS a')
				->join('td_workorder_item AS b', 'b.woitem_kd = a.inspectionproduct_woitem_kd', 'left')
				->join('tb_bagian AS c', 'c.bagian_kd = a.inspectionproduct_bagian_kd', 'left')
				->join('(SELECT bagian_kd AS bagian_kd2, bagian_nama AS bagian_nama2 FROM tb_bagian) AS d', 'd.bagian_kd2=a.inspectionproduct_penyebab_bagian_kd', 'left')
				->join('tb_inspectionproduct_state AS e', 'e.inspectionproductstate_kd = a.inspectionproductstate_kd', 'left')
				->where_not_in('a.inspectionproductstate_kd', ['0', '1'])
				->where_in('a.inspectionproduct_penyebab_bagian_kd', $whereBagian)->get()->result_array();
		}else{
			$qData = [];
		}

		$data = [];
		foreach ($qData as $r) {
			$span = "";
			$btn = "";
			if($r['flag_approved_penyebab'] == 1){
				$span = build_span('primary', 'Approved');
			}else{
				$span = build_span('danger', 'Belum');
			}

			if($r['flag_approved_penyebab'] == 0){
				$btn ='<button type="button" onclick="approved_penyebab(\''.$r['kd_inspectionproduct'].'\')" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Approved</button>';
			}

			$data[] = [
				'1' => $btn,
				'2' => $r['inspectionproduct_tanggal'],
				'3' => $r['inspectionproduct_no'],
				'4' => $r['inspectionproduct_item_code'],
				'5' => $r['woitem_no_wo'],
				'6' => $r['inspectionproduct_tindakan'],
				'7' => $r['inspectionproduct_qty'],
				'8' => $r['bagian_nama2'],
				'9' => $r['inspectionproduct_penyebab'],
				'10' => $r['inspectionproduct_keterangan'],
				'11' => $r['approved_penyebab_tgl'],
				'12' => $span
			];
		}

		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

	public function ssp_repair_table2()
	{
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		$userdataAdmin = $this->session->userdata('kd_admin');
		$whereBagian = $this->get_allowed_bagian($userdataAdmin, 'repair');

		if( !empty($whereBagian) ){
			$qData = $this->db->select('a.kd_inspectionproduct, a.inspectionproduct_tanggal, a.inspectionproduct_no, a.inspectionproduct_item_code, a.inspectionproduct_tindakan, a.inspectionproduct_qty, b.woitem_no_wo, a.inspectionproduct_keterangan, f.bagian_nama, d.flag_approved, d.tgl_approved, g.nm_admin, d.bagian_kd')
				->from($this->tbl_name . ' AS a')
				->join('td_workorder_item AS b', 'b.woitem_kd = a.inspectionproduct_woitem_kd', 'left')
				->join('tb_inspectionproduct_state AS c', 'c.inspectionproductstate_kd = a.inspectionproductstate_kd', 'left')
				->join('td_inspectionproduct_repair AS d', 'd.inspectionproduct_kd = a.kd_inspectionproduct', 'left')
				->join('tb_bagian AS f', 'f.bagian_kd = d.bagian_kd', 'left')
				->join('tb_admin AS g', 'g.kd_admin = d.approved_admin_kd', 'left')
				->where_not_in('a.inspectionproductstate_kd', ['0', '1'])
				->where_in('d.bagian_kd', $whereBagian)
				->get()->result_array();
		}else{
			$qData = [];
		}
			
		$data = [];
		foreach ($qData as $r) {
			$span = "";
			$btn = "";
			if($r['flag_approved'] == 1){
				$span = build_span('primary', 'Approved');
			}else{
				$span = build_span('danger', 'Belum');
			}

			if($r['flag_approved'] == 0){
				$btn ='<button type="button" onclick="approved_repair(\''.$r['kd_inspectionproduct'].'\', \''.$r['bagian_kd'].'\')" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Approved</button>';
			}
			$data[] = [
				'1' => $btn,
				'2' => $r['bagian_nama'],
				'3' => $r['inspectionproduct_tanggal'],
				'4' => $r['inspectionproduct_no'],
				'5' => $r['inspectionproduct_item_code'],
				'6' => $r['woitem_no_wo'],
				'7' => $r['inspectionproduct_tindakan'],
				'8' => $r['inspectionproduct_qty'],
				'9' => $r['inspectionproduct_keterangan'],
				'10' => $r['tgl_approved'],
				'11' => $span
			];
		}

		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

	/* --end ssp tabel untuk modul data inspection product-- */

	/* --start button tabel untuk modul data inspection product-- */
	private function tbl_btn($id, $var, $state) {
		$this->load->model('td_inspectionproduct_userstate');
		$read_access = $this->session->read_access;
		$update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		/** 0 = cancel, 1 = editing (insert update delete) */
		$kd_admin = $this->session->userdata('kd_admin');
		$cekPermissionEditing = $this->td_inspectionproduct_userstate->cek_permission(1, $kd_admin);
		$cekPermissionApprove = $this->td_inspectionproduct_userstate->cek_permission(2, $kd_admin);
		$cekPermissionDiketahui = $this->td_inspectionproduct_userstate->cek_permission(3, $kd_admin);
		$cekPermissionClose = $this->td_inspectionproduct_userstate->cek_permission(4, $kd_admin);
		$cekPermissionCreated = $this->td_inspectionproduct_userstate->cek_permission(5, $kd_admin);
		/** Alurnya 1 editing -> 5 created -> 2 approved -> 3 diketahui -> 4 close */
		/** 0 cancel, 4 close */
		if($state !== 0 || $state !== 4){
			switch ($state) {
				case 1:
					/** Editing Permission Non Approved */
					if($cekPermissionEditing){
						$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah', 'icon' => 'pencil', 'onclick' => 'get_form(\''.$id.'\')'));
						$btns[] = get_btn(array('access' => $delete_access, 'title' => 'Hapus', 'icon' => 'trash', 'onclick' => 'return confirm(\'Anda akan menghapus data inspection product = '.$var.'?\')?hapus_data(\''.$id.'\'):false'));
					}
					/** Editing Permission Created */
					if($cekPermissionCreated){
						$btns[] = get_btn(array('access' => $update_access, 'title' => 'Created', 'icon' => 'file', 'onclick' => 'action_state(\''.$id.'\', 5)'));
					}
					break;
				case 2:
					/** Editing Permission Diketahui */
					if($cekPermissionDiketahui){
						$btns[] = get_btn(array('access' => $update_access, 'title' => 'Diketahui', 'icon' => 'check', 'onclick' => 'action_state(\''.$id.'\', 3)'));
					}
					break;
				case 3:
					/** Editing Permission Close */
					if($cekPermissionClose){
						$btns[] = get_btn(array('access' => $update_access, 'title' => 'Close', 'icon' => 'close', 'onclick' => 'action_state(\''.$id.'\', 4)'));
					}
					break;
				case 5:
					/** Editing Permission Approved */
					if($cekPermissionApprove){
						$btns[] = get_btn(array('access' => $update_access, 'title' => 'Approved', 'icon' => 'check', 'onclick' => 'action_state(\''.$id.'\', 2)'));
					}
					break;
			}
		}

		$btns[] = get_btn(array('access' => $read_access, 'title' => 'Cetak', 'icon' => 'print', 'onclick' => 'cetak(\''.$id.'\')'));
		$btn_group = group_btns($btns);
		return $btn_group;
	}
	/* --end button tabel untuk modul data inspection-- */

	/** Menampilkan bagian berdasarkan user */
	private function get_allowed_bagian($admin_kd = null, $allowedModul = '')
	{
		$this->load->model(['tb_inspectionproduct_user', 'tb_bagian']);
		/** allowedModul = bagian, penyebab, repair */
		if($allowedModul == 'bagian'){
			$arrData = $this->tb_inspectionproduct_user->get_where(['admin_kd' => $admin_kd, 'approved_bagian' => '1'])->result_array();
		}else if($allowedModul == 'penyebab'){
			$arrData = $this->tb_inspectionproduct_user->get_where(['admin_kd' => $admin_kd, 'approved_penyebab' => '1'])->result_array();
		}else if($allowedModul == 'repair'){
			$arrData = $this->tb_inspectionproduct_user->get_where(['admin_kd' => $admin_kd, 'approved_repair' => '1'])->result_array();
		}else{
			$arrData = [];
		}
		$bagians = [];
		if(!empty($arrData)){
			// $bagians = "'".implode("','", array_column($arrData, 'kd_bagian'))."'";
			$bagians = array_column($arrData, 'kd_bagian');
		}

		/** Jika tipe role admin maka bagian tampilkan semua */
		if( $this->session->userdata('tipe_admin') == 'Admin' ){
			$arrBagian = $this->tb_bagian->get_all()->result_array();
			$bagians = array_column($arrBagian, 'bagian_kd');
		}

		return $bagians;
	}

	public function get_row($id = '') {
		$this->db->from($this->tbl_name)
			->where(array($this->p_key => $id));
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}

	public function create_code() {
		$this->db->select($this->p_key.' AS code')
			->from($this->tbl_name)
			->where(array('DATE(inspectionproduct_tglinput)' => date('Y-m-d')))
			->order_by('inspectionproduct_tglinput DESC, '.$this->p_key.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0):
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -3);
		endif;
		$angka = $urutan + 1;
		$primary = 'INP'.date('dmy').str_pad($angka, 3, '000', STR_PAD_LEFT);
		return $primary;
	}

	public function create_no($tgl)
	{
		$inp_year = format_date($tgl, 'Y');
		$query = $this->db->select('MAX(inspectionproduct_no) as max_no')
			->where('YEAR(inspectionproduct_tanggal)', $inp_year)
			->get($this->tbl_name)->row();
		if (empty($query->max_no)) :
			$no = 0;
		else :
			$no = substr($query->max_no, -3);
		endif;
		$no = (int)$no + 1;
		$inp_no = 'IP-'.$inp_year . '.' . str_pad($no, '3', '000', STR_PAD_LEFT);
		return $inp_no;
	}

	public function create($data = '') {
		$act = $this->db->insert($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	public function update($data = '', $id = '') {
		$this->db->where($this->p_key, $id);
		$act = $this->db->update($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	public function delete_data($id = '') {
		$act = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		return $act;
	}

	public function get_all(){
		return $this->db->get($this->tbl_name);
	}

	public function get_by_param ($param=[]) {
		$this->db->from($this->tbl_name);
		$this->db->join('td_workorder_item', 'td_workorder_item.woitem_kd='.$this->tbl_name.'.inspectionproduct_woitem_kd', 'left');
		$this->db->join('tb_bagian', 'tb_bagian.bagian_kd='.$this->tbl_name.'.inspectionproduct_bagian_kd', 'left');
		$this->db->join('(SELECT bagian_kd AS bagian_kd3, bagian_nama AS bagian_nama3 FROM tb_bagian) tb_bagian3', 'tb_bagian3.bagian_kd3='.$this->tbl_name.'.inspectionproduct_penyebab_bagian_kd', 'left');
		$this->db->join('tb_admin', 'tb_admin.kd_admin='.$this->tbl_name.'.admin_kd', 'left');
		$this->db->join('(SELECT kd_admin AS kd_admin2, nm_admin AS nm_admin2 FROM tb_admin) AS tb_admin2', 'tb_admin2.kd_admin2='.$this->tbl_name.'.approved_bagian_admin_kd', 'left');
		$this->db->join('(SELECT kd_admin AS kd_admin3, nm_admin AS nm_admin3 FROM tb_admin) AS tb_admin3', 'tb_admin3.kd_admin3='.$this->tbl_name.'.approved_penyebab_admin_kd', 'left');
		$this->db->where($param);
		$act = $this->db->get();
		return $act;
	}

	public function sendMail($inspection_kd, $state_kd)
	{
		$this->load->model(['_mail_configuration', 'tm_inspection_product', 'tb_inspectionproduct_state']);
		$state = $this->tb_inspectionproduct_state->get_by_param(['inspectionproductstate_kd'=>$state_kd])->row();
		$inspection = $this->get_row($inspection_kd);
		/** Kirim notif email hanya untuk yang reject */
		if($inspection->inspectionproduct_tindakan == 'reject'){
			/** State notif ke email */
			if($state->inspectionproductstate_notifemail == '1'){
				$subject = 'Inspection Product - ( '.$inspection->inspectionproduct_no.' )';
				$dtMessage['text'] = 'Terdapat barang reject yang perlu anda proses :';
				$dtMessage['url'] = 'http://'.$_SERVER['HTTP_HOST'].base_url().'/quality_control/inspection_product/report_pdf?id='.$inspection_kd;
				$dtMessage['urltext'] = 'Inspection Product ( '.$inspection->inspectionproduct_no.' ) '.$inspection->inspectionproduct_item_code;
				$dtMessage['keterangan'] = $inspection->inspectionproduct_keterangan;
				$message = $this->load->view('templates/email/email_notification', $dtMessage, true);

				$emailTo = explode(';', $state->inspectionproductstate_emailto);
				$emailCc = explode(';', $state->inspectionproductstate_emailcc);

				$act = $this->_mail_configuration->sendEmailCc($emailTo, $emailCc, $subject, $message);
				return $act;
			}
		}
		
		return true;
	}
}