<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_payment extends CI_Model {
	private $tbl_name = 'tm_payment';
	private $p_key = 'payment_kd';

	public function ssp_table2 ($bulan, $tahun) {
		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		
		$data = [];
		if($bulan != 'ALL'){
			$qData = $this->db->select('tm_payment.*,tm_m_payment.*,tm_suplier.suplier_nama,tm_suplier.suplier_kode')
			->from('tm_payment')
			->join('tm_suplier', 'tm_payment.suplier_kd = tm_suplier.suplier_kd', 'LEFT')
			->join('tm_m_payment', 'tm_payment.m_payment_kd = tm_m_payment.m_payment_kd', 'LEFT')
			->where('MONTH(tm_payment.tanggal_input)', $bulan)
			->where('YEAR(tm_payment.tanggal_input)', $tahun)
			->get();
		}else{
			$qData = $this->db->select('tm_payment.*,tm_m_payment.*,tm_suplier.suplier_nama,tm_suplier.suplier_kode')
			->from('tm_payment')
			->join('tm_suplier', 'tm_payment.suplier_kd = tm_suplier.suplier_kd', 'LEFT')
			->join('tm_m_payment', 'tm_payment.m_payment_kd = tm_m_payment.m_payment_kd', 'LEFT')
			->where('YEAR(tm_payment.tanggal_input)', $tahun)
			->get();
		}

		foreach ($qData->result_array() as $r) {
			$no_inv = $this->get_invoice_no($r['payment_kd']);

			$data[] = [
				'1' => $this->tbl_btn($r['payment_kd']),
				'2' => $r['tanggal_input'],
				'3' => $r['no_check'],
				'4' => $no_inv,
				'5' => $r['nm_bank'],
				'6' => $r['suplier_kode'].' | '.$r['suplier_nama'],
				'7' => $r['grand_total'],
			];
		}
		
		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

	public function get_invoice_no($no_pay)
	{
		$items = '';
		$database = $this->db->select('tmp.purchaseinvoice_no')
						->from('td_payment tdp')
						->join('tm_purchaseinvoice tmp', 'tmp.purchaseinvoice_kd = tdp.invoice_kd', 'LEFT')
						->where(array('tdp.payment_kd' => $no_pay))
						->group_by('tmp.purchaseinvoice_no')
						->get()->result_array();

		foreach ($database as $r) {
			$items .= $r['purchaseinvoice_no'] . '; ';
			
		}
		return $items;
	}

	private function tbl_btn($id) {
		$btns = array();
		if (cek_permission('PURCHASEINVOICE_VIEW')) {
			$btns[] = get_btn(array('title' => 'Lihat Item', 'icon' => 'search', 'onclick' => 'view_box(\''.$id.'\')'));
		}
        if (cek_permission('PURCHASEINVOICE_UPDATE')) {
            $btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
        }
		$btn_group = group_btns($btns);

		return $btn_group;
	}


    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

}