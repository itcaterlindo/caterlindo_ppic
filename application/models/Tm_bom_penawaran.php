<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_bom_penawaran extends CI_Model {
	private $tbl_name = 'tm_bom_penawaran';
	private $p_key = 'bompenawaran_kd';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){
					
					return $this->tbl_btn($d, $row[4]);
				}),
			array( 'db' => 'b.nm_customer',
				'dt' => 2, 'field' => 'nm_customer',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'd.nm_jenis_customer',
				'dt' => 3, 'field' => 'nm_jenis_customer',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.bompenawaran_note', 
				'dt' => 4, 'field' => 'bompenawaran_note',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'c.bomstate_nama', 
				'dt' => 5, 'field' => 'bomstate_nama',
				'formatter' => function ($d, $row){
                    $d = build_span ($row[6], $d);
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'a.bompenawaran_tglinput', 
				'dt' => 6, 'field' => 'bompenawaran_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'c.bomstate_label', 
				'dt' => 7, 'field' => 'bomstate_label',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] = "FROM ".$this->tbl_name." as a
                            LEFT JOIN tm_customer as b ON a.kd_customer=b.kd_customer
							LEFT JOIN tb_bom_state as c ON a.bomstate_kd=c.bomstate_kd
							LEFT JOIN tb_jenis_customer as d ON a.kd_jenis_customer=d.kd_jenis_customer";
		if (cek_permission('BOMPENAWARAN_VIEWEXPORT')) {
			$data['where'] = "";
		}else {
			$data['where'] = "d.kd_jenis_customer <> '004'";
		}
		
		return $data;
	}

	private function tbl_btn($id, $state) {
		$view_bom = cek_permission('BOMPENAWARAN_VIEW_BOM');
		$update_bom = cek_permission('BOMPENAWARAN_UPDATE');
		$delete_bom = cek_permission('BOMPENAWARAN_DELETE');
		$btns = array();
		if ($view_bom) {
			$btns[] = get_btn(array('title' => 'Lihat BoM', 'icon' => 'search', 'onclick' => 'detail_data(\''.url_encrypt($id).'\')'));
		}
		if ($state == 'editing') {
			if ($update_bom) {
				$btns[] = get_btn(array('title' => 'Edit BoM', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
			}
			if ($delete_bom) {
				$btns[] = get_btn(array('title' => 'Delete BoM', 'icon' => 'trash', 'onclick' => 'delete_data(\''.$id.'\')'));
			}
		}
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
		$this->db->select('MAX('.$this->p_key.') AS code')
			->from($this->tbl_name);
		$query = $this->db->get();
		$query = $query->row();
		$urutan = $query->code;
		$angka = $urutan + 1;
		return $angka;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

	public function get_by_param_in ($param, $params=[]) {
		$this->db->where_in($param, $params);
		$act = $this->db->get($this->tbl_name);
		return $act;
    }
    
	public function get_by_param_detail ($param = []) {
        $result = $this->db->select()
                    ->from ($this->tbl_name)
                    ->join('tm_customer', $this->tbl_name.'.kd_customer=tm_customer.kd_customer', 'left')
					->join('tb_bom_state', $this->tbl_name.'.bomstate_kd=tb_bom_state.bomstate_kd', 'left')
					->join('tb_jenis_customer', $this->tbl_name.'.kd_jenis_customer=tb_jenis_customer.kd_jenis_customer', 'left')
                    ->where($param)
                    ->get();
        return $result;
	}
	
	public function generateButtonState ($bompenawaran_kd) {
		$this->load->model(['tb_admin', 'tb_bom_state', 'td_bom_state_user']);
		$qBom = $this->get_by_param (['bompenawaran_kd' => $bompenawaran_kd])->row_array();
		$qBomState = $this->tb_bom_state->get_all()->result_array();
		$admin_kd = $this->session->userdata('kd_admin');

		$curState_kd = $qBom['bomstate_kd']; 
		$nextBomstate_kd = (int)$curState_kd + 1;
		$backBomstate_kd = (int)$curState_kd - 1;
		$nextBomstate_nama = null;
		$backBomstate_nama = null;
		$nextAllowState = 0;
		$backAllowState = 0;
		$curBomstate_filteruser = 0;
		foreach ($qBomState as $rBomstate) {
			/** Curstate */
			if ($rBomstate['bomstate_kd'] == $curState_kd) {
				$curBomstate_filteruser = $rBomstate['bomstate_filteruser'];
			}
			/** Next */
			if ($rBomstate['bomstate_kd'] == $nextBomstate_kd) {
				$nextBomstate_nama = $rBomstate['bomstate_nama'];					
			}
			/** Back */
			if ($rBomstate['bomstate_kd'] == $backBomstate_kd) {
				$backBomstate_nama = $rBomstate['bomstate_nama'];
			}
		}
		/** Cek currentState */
		if ($curBomstate_filteruser == 0) {
			$nextAllowState = 1;
			$backAllowState = 1;
		}else {
			$cekState = $this->td_bom_state_user->get_by_param(['bomstate_kd' => $curState_kd, 'bomstateuser_admin' => $admin_kd]);
			if ($cekState->num_rows() > 0) {
				$nextAllowState = 1;
				$backAllowState = 1;
			}
		}
		/** Is Admin or is approved*/
		if ($this->tb_admin->isAdmin($admin_kd)) {
			$nextAllowState = 1;
			$backAllowState = 1;
		}
		
		$btn = '';
		if (!empty($backBomstate_nama) && $backAllowState) {
			$btn .= '<button class="btn btn-sm btn-warning" onclick=ubah_state("'.$bompenawaran_kd.'","'.$backBomstate_kd.'")> <i class="fa fa-hand-o-left"> </i> Ubah ke '.ucwords(str_replace('_', ' ', $backBomstate_nama)).'</button>';
		}
		if (!empty($nextBomstate_nama) && $nextAllowState) {
			$btn .= '<button class="btn btn-sm btn-warning" onclick=ubah_state("'.$bompenawaran_kd.'","'.$nextBomstate_kd.'")> Ubah ke '.ucwords(str_replace('_', ' ', $nextBomstate_nama)).' <i class="fa fa-hand-o-right"></i> </button>';
		}
		
		return $btn;
	}

	private function ubah_state_email ($bompenawaran_kd, $bomstate_kd, $keterangan) {
		$this->load->model(['_mail_configuration', 'td_bom_state_user']);

		$actEmail = true;
		$stateUser = $this->td_bom_state_user->get_by_param(['bomstate_kd' => $bomstate_kd])->result_array();
		$to = [];
		$cc = [];
		$txtcc = '';
		foreach ($stateUser as $rStateUser) {
			$to [] = $rStateUser['bomstateuser_email'];
			$txtcc .= ';'.$rStateUser['bomstateuser_emailcc'];
		}
		$txtcc = substr($txtcc, 1, strlen($txtcc));
		$cc = explode(';', str_replace(' ', '', $txtcc));

		$rowBom = $this->get_by_param_detail(['tm_bom_penawaran.bompenawaran_kd' => $bompenawaran_kd])->row_array();
		$subject = 'Bill Of Material (Costing)-'.$rowBom['nm_customer'].'/'.$rowBom['bompenawaran_note'];

		$dtMessage['text'] = 'Terdapat Bill of Material (Costing) yang perlu ditindak lanjuti :';
		$dtMessage['url'] = my_baseurl().'manage_items/bom/bom_penawaran/view_bom?id='.url_encrypt($bompenawaran_kd);
		$dtMessage['urltext'] = 'Detail Bill of Material (Costing)';
		$dtMessage['keterangan'] = !empty($keterangan) ? $keterangan : '-';
		$message = $this->load->view('templates/email/email_notification', $dtMessage, true);

		if (!empty($to)) {
			$actEmail = $this->_mail_configuration->sendEmailCc($to, $cc, $subject, $message);
		}
		return $actEmail;
	}

	private function ubah_state_email_js ($bompenawaran_kd, $bomstate_kd, $keterangan) {
		$this->load->model(['_mail_configuration', 'td_bom_state_user']);

		$actEmail = true;
		$stateUser = $this->td_bom_state_user->get_by_param(['bomstate_kd' => $bomstate_kd])->result_array();
		$to = [];
		$cc = [];
		$txtcc = '';
		foreach ($stateUser as $rStateUser) {
			$to [] = $rStateUser['bomstateuser_email'];
			$txtcc .= ';'.$rStateUser['bomstateuser_emailcc'];
		}
		$txtcc = substr($txtcc, 1, strlen($txtcc));
		$cc = explode(';', str_replace(' ', '', $txtcc));

		$rowBom = $this->get_by_param_detail(['tm_bom_penawaran.bompenawaran_kd' => $bompenawaran_kd])->row_array();
		$subject = 'Bill Of Material (Costing)-'.$rowBom['nm_customer'].'/'.$rowBom['bompenawaran_note'];
		$dtMessage['to'] = $to;
		$dtMessage['cc'] = $cc;
		$dtMessage['subject'] = $subject;
		$dtMessage['text'] = 'Terdapat Bill of Material (Costing) yang perlu ditindak lanjuti :';
		$dtMessage['url'] = my_baseurl().'manage_items/bom/bom_penawaran/view_bom?id='.url_encrypt($bompenawaran_kd);
		$dtMessage['urltext'] = 'Detail Bill of Material (Costing)';
		$dtMessage['keterangan'] = !empty($keterangan) ? $keterangan : '-';
		//$message = $this->load->view('templates/email/email_notification', $dtMessage, true);

		// if (!empty($to)) {
		// 	$actEmail = $this->_mail_configuration->sendEmailCc($to, $cc, $subject, $message);
		// }
		return $dtMessage;
	}


	private function ubah_state_part($bompenawaran_kd, $bomstate_kd, $keterangan) {
		$this->load->model(['td_part', 'td_bom_penawaran_detail', 'tb_bom_state']);

		$bompenarawaranDetail = $this->td_bom_penawaran_detail->get_by_param(['bompenawaran_kd' => $bompenawaran_kd])->result_array();
		$partstate = $this->tb_bom_state->get_by_param(['bomstate_kd' => $bomstate_kd])->row_array();
		foreach ($bompenarawaranDetail as $rPenawarandetail) {
			$act[] = $this->td_part->ubah_state ($rPenawarandetail['part_kd'], $partstate['partstate_kd'], $keterangan);
		}
		return true;
	}

	public function ubah_state ($bompenawaran_kd, $bomstate_kd, $keterangan) {
		$this->load->model(['td_bom_state_log', 'tb_bom_state']);
		$actEmail = [];
		$send_email = false;
		$dataUpdate = [
			'bomstate_kd' => $bomstate_kd,
			'bompenawaran_tgledit' => date('Y-m-d H:i:s'),
		];
		$dataLog = [
			'bompenawaran_kd' => $bompenawaran_kd,
			'bomstate_kd' => $bomstate_kd,
			'bomstatelog_note' => $keterangan,
			'admin_kd' => $this->session->userdata('kd_admin'),
		];
		$this->db->trans_start();
		$rowBomstate = $this->tb_bom_state->get_by_param(['bomstate_kd' => $bomstate_kd])->row_array();
		$rowBom = $this->get_by_param(['bompenawaran_kd'=>$bompenawaran_kd])->row_array();
		if ($rowBomstate['bomstate_email'] == 1 && $rowBom['kd_jenis_customer'] != '004') { // Jika bukan jenis export
			//$actEmail = $this->ubah_state_email ($bompenawaran_kd, $bomstate_kd, $keterangan);
			$actEmail = $this->ubah_state_email_js ($bompenawaran_kd, $bomstate_kd, $keterangan);
			$send_email = true;
		}
		$actUpdatePart = $this->ubah_state_part($bompenawaran_kd, $bomstate_kd, $keterangan);
		$actUpdate = $this->update_data (['bompenawaran_kd' => $bompenawaran_kd], $dataUpdate);
		$actLog = $this->td_bom_state_log->insert_data($dataLog);
		$this->db->trans_complete();

		$data['data_message_json'] = $actEmail;
		$data['send_email'] = $send_email;
		if ($this->db->trans_status() === FALSE) {
			$data['status'] = false;
			return $data;
		}else {
			$data['status'] = true;
			return $data;
		}
	}

}