<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_deliverynote_user extends CI_Model
{
	private $tbl_name = 'tb_deliverynote_user';
	private $p_key = 'dnuser_kd';

	public function ssp_table()
	{
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array(
				'db' => 'a.' . $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function ($d, $row) {

					return $this->tbl_btn($d);
				}
			),
			array(
				'db' => 'b.nm_admin',
				'dt' => 2, 'field' => 'nm_admin',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'c.nm_tipe_admin',
				'dt' => 3, 'field' => 'nm_tipe_admin',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'd.bagian_nama',
				'dt' => 4, 'field' => 'bagian_nama',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'a.dnuser_tgledit',
				'dt' => 5, 'field' => 'dnuser_tgledit',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),

		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM " . $this->tbl_name . " AS a
                            LEFT JOIN tb_admin as b ON b.kd_admin=a.admin_kd
							LEFT JOIN td_admin_tipe as c on c.kd_tipe_admin=a.kd_tipe_admin
							LEFT JOIN tb_bagian as d on d.bagian_kd=a.kd_bagian";
		$data['where'] = "";

		return $data;
	}

	private function tbl_btn($id)
	{
		$btns = array();
		$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\'' . $id . '\')'));

		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code()
	{
		$query = $this->db->select('MAX(' . $this->p_key . ') as maxID')
			->get($this->tbl_name)
			->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

	public function insert_data($data)
	{
		$query = $this->db->insert($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_data($id)
	{
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		return $query ? TRUE : FALSE;
	}

	public function get_by_param($param = [])
	{
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all()
	{
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data($aWhere = [], $data)
	{
		$query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query ? TRUE : FALSE;
	}

	public function insert_batch_data($data = [])
	{
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_by_param($param = [])
	{
		$query = $this->db->delete($this->tbl_name, $param);
		return $query ? TRUE : FALSE;
	}

	public function get_where_in($where, $in = [])
	{
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}

	public function cek_permission($kd_bagian, $kd_admin = null, $kd_tipe_admin = null) :bool
	{
		$qCountUserBagian = 0;
		$resp = false;
		if (!empty($kd_admin)) {
			$qCountUserBagian += $this->get_by_param(['admin_kd' => $kd_admin, 'kd_bagian' => $kd_bagian])->num_rows();
		}
		if (!empty($kd_tipe_admin)) {
			$qCountUserBagian += $this->get_by_param(['kd_tipe_admin' => $kd_tipe_admin, 'kd_bagian' => $kd_bagian])->num_rows();
		}

		if (!empty($qCountUserBagian)) {
			$resp = true;
		}
		return $resp;
	}

	public function get_bagianAllowed($kd_admin, $kd_tipe_admin){
		$bagians = [];
		if (!empty($kd_admin)) {
			$qBagianUser = $this->get_by_param(['admin_kd' => $kd_admin])->result_array();
			if (!empty($qBagianUser)){
				$bagians = array_column($qBagianUser, 'kd_bagian');
			}
		}
		if (!empty($kd_tipe_admin)) {
			$qBagianTipeUser = $this->get_by_param(['kd_tipe_admin' => $kd_tipe_admin])->result_array();
			if (!empty($qBagianTipeUser)){
				$bagians = array_merge($bagians, array_column($qBagianTipeUser, 'kd_bagian'));
			}
		}
		return $bagians;
	}
}
