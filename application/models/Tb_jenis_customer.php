<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_jenis_customer extends CI_Model {
	private $tbl_name = 'tb_jenis_customer';
	private $p_key = 'kd_jenis_customer';
	private $title_name = 'Data Jenis Customer';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){

					return $this->tbl_btn($d, $row[1]);
				} ),
			array( 'db' => 'a.nm_jenis_customer', 'dt' => 2, 'field' => 'nm_jenis_customer' ),
			array( 'db' => 'b.nm_select AS kat_harga', 'dt' => 3, 'field' => 'kat_harga' ),
			array( 'db' => 'a.term_payment_waktu', 'dt' => 4, 'field' => 'term_payment_waktu' ),
			array( 'db' => 'c.nm_select AS tipe_customer', 'dt' => 5, 'field' => 'tipe_customer' ),
		);

		$data['sql_details'] = sql_connect();

		$data['joinQuery'] = "
			FROM
				".$this->tbl_name." a
			LEFT JOIN tb_set_dropdown b ON b.id = a.kd_price_category
			LEFT JOIN tb_set_dropdown c ON c.id = a.kd_manage_items
		";

		$data['where'] = "";

		return $data;
	}

	private function tbl_btn($id, $var) {
		$read_access = $this->session->read_access;
		$update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		if (cek_permission('JENISCUSTOMER_UPDATE')) {
			$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah', 'icon' => 'pencil', 'onclick' => 'get_form(\''.$id.'\')'));
			$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah Master BoM', 'icon' => 'pencil', 'onclick' => 'ubah_master_bom(\''.$id.'\')'));
		}
		$btns[] = get_btn_divider();
		if (cek_permission('JENISCUSTOMER_DELETE')) {
			$btns[] = get_btn(array('access' => $delete_access, 'title' => 'Hapus', 'icon' => 'trash',
				'onclick' => 'return confirm(\'Anda akan menghapus tipe customer = '.$var.'?\')?delete_data(\''.$id.'\'):false'));
		}
		
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function get_row($id = '') {
		$this->db->from($this->tbl_name)->where(array($this->p_key => $id));
		$query = $this->db->get();
		$row = $query->row();

		return $row;
	}

	public function get_data($id = '') {
		$row = $this->get_row($id);
		if (!empty($row)) :
			$data = array('kd_jenis_customer' => $row->kd_jenis_customer, 'kd_sap' => $row->kd_sap,'nm_jenis_customer' => $row->nm_jenis_customer, 'kd_price_category' => $row->kd_price_category, 'kd_manage_items' => $row->kd_manage_items, 'tipe_penjualan' => $row->tipe_penjualan, 'term_payment_format' => $row->term_payment_format, 'term_payment_waktu' => $row->term_payment_waktu, 'kd_term_payment_sap' => $row->kd_term_payment_sap,
			'set_ppn' => $row->set_ppn, 'kd_ppn_sap' => $row->kd_ppn_sap, 'custom_barcode' => $row->custom_barcode);
		else :
			$data = array('kd_jenis_customer' => '', 'nm_jenis_customer' => '', 'kd_price_category' => '', 'kd_manage_items' => '', 'tipe_penjualan' => '', 'term_payment_format' => '', 'term_payment_waktu' => '', 'kd_term_payment_sap' => '', 'set_ppn' => '', 'kd_ppn_sap' => '', 'custom_barcode' => '');
		endif;
		return $data;
	}

	public function form_rules() {
		$rules = array(
			array('field' => 'txtNm', 'label' => 'Nama Jenis Customer', 'rules' => 'required'),
			array('field' => 'selPrice', 'label' => 'Price Category', 'rules' => 'required'),
			array('field' => 'selItem', 'label' => 'Tipe Customer', 'rules' => 'required'),
			array('field' => 'txtSetPpn', 'label' => 'Set Default PPN (%)', 'rules' => 'required'),
			array('field' => 'txtBarcode', 'label' => 'Custom Barcode', 'rules' => 'required'),
			array('field' => 'txtTermFormat', 'label' => 'Format Term Payment', 'rules' => 'required'),
			array('field' => 'txtTermWaktu', 'label' => 'Masa Tenggang Term Payment (Hari)', 'rules' => 'required'),
		);
		return $rules;
	}

	public function form_warning($datas = '') {
		$forms = array('txtNm', 'selPrice', 'selItem', 'txtSetPpn', 'txtBarcode', 'txtTermFormat', 'txtTermWaktu');
		foreach ($datas as $key => $data) :
			$str[$data] = (!empty(form_error($forms[$key])))?buildLabel('warning', form_error($forms[$key], '"', '"')):'';
		endforeach;
		return $str;
	}

	private function create_code() {
		$this->db->select($this->p_key.' AS code')
			->from($this->tbl_name)
			->order_by($this->p_key.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -3);
		endif;
		$angka = $urutan + 1;
		$primary = str_pad($angka, 3, '000', STR_PAD_LEFT);
		return $primary;
	}

	public function submit_data($data = '') {
		if (empty($data[$this->p_key])) :
			$label = 'Menambahkan '.$this->title_name;
			$data[$this->p_key] = $this->create_code();
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s')));
			$act = $this->create($submit);
		else :
			$label = 'Mengubah '.$this->title_name;
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s')));
			$where = array($this->p_key => $data[$this->p_key]);
			$act = $this->update($submit, $where);
		endif;
		$str = $this->report($act, $label, $submit);
		return $str;
	}

	private function create($data = '') {
		$act = $this->db->insert($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	private function update($data = '', $where = '') {
		$act = $this->db->update($this->tbl_name, $data, $where);
		return $act?TRUE:FALSE;
	}

	public function report($act = '', $label = '', $data = '') {
		$this->load->model(array('m_builder'));
		if ($act) :
			$stat = 'Berhasil';
			$str[$this->p_key] = $data[$this->p_key];
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
			$str['data'] = $data;
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
			$str['data'] = $data;
		endif;
		$this->m_builder->write_log($stat, $label, $data);
		return $str;
	}

	public function delete_data($id = '') {
		$act['master'] = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		$act['detail'] = $this->db->delete('tb_default_disc', array('jenis_customer_kd' => $id));
		$report = $this->report($act, 'Menghapus '.$this->title_name, array($this->p_key => $id));
		return $report;
	}

	public function sel_tipe_penjualan($tipe_penjualan = '') {
		$this->db->from($this->tbl_name)
			->where(array('tipe_penjualan' => $tipe_penjualan));
		$query = $this->db->get();
		$result = $query->result();
		$num = $query->num_rows();
		$data = '<option value="">-- Pilih Tipe Customer --</option>';
		if (!empty($result)) :
			if ($num > 1) :
				$data .= '<option value="semua">Pilih Semua</option>';
			endif;
			foreach ($result as $row) :
				$data .= '<option value="'.$row->kd_jenis_customer.'">'.$row->nm_jenis_customer.'</option>';
			endforeach;
		endif;
		return $data;
	}

	public function get_all () {
        return $this->db->get($this->tbl_name);
    }
}