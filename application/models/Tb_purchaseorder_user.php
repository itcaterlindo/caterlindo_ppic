<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_purchaseorder_user extends CI_Model {
	private $tbl_name = 'tb_purchaseorder_user';
	private $p_key = 'pouser_kd';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'b.nm_admin', 
				'dt' => 2, 'field' => 'nm_admin',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.pouser_email', 
				'dt' => 3, 'field' => 'pouser_email',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.postatus_nama', 
				'dt' => 4, 'field' => 'postatus_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM ".$this->tbl_name." as a 
								LEFT JOIN tb_admin as b ON a.pouser_user = b.kd_admin
								LEFT JOIN tb_purchaseorder_status as c ON a.postatus_kd=c.postatus_kd";
		$data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id) {
		// $read_access = $this->session->read_access;
		// $update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		if($delete_access == 1 ){
			$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
			$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));
		}		
		$btn_group = group_btns($btns);

		return $btn_group;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

	public function get_by_param_detail($param = []){
		$query = $this->db->select($this->tbl_name.'.*, tb_admin.kd_admin, tb_admin.nm_admin, tb_purchaseorder_status.*')
					->from ($this->tbl_name)
					->join('tb_admin', $this->tbl_name.'.pouser_user=tb_admin.kd_admin', 'left')
					->join('tb_purchaseorder_status', $this->tbl_name.'.postatus_kd=tb_purchaseorder_status.postatus_kd', 'left')
					->where($param)
					->get();
		return $query;
	}

	public function isApprover($admin_kd){
		$query = $this->get_by_param (array('pouser_user' => $admin_kd, 'postatus_kd' => 4));
		if ($query->num_rows() != 0){
			$resp = true;
		}else{
			$resp = false;
		}

		return $resp;
	}

	public function isCurrentState($admin_kd, $postatus_kd){
		$query = $this->get_by_param(['pouser_user' => $admin_kd, 'postatus_kd' => $postatus_kd]);
		if ($query->num_rows() == 0 ){
			$resp = false;
		}else{
			$resp = true;
		}

		return $resp;

	}


}