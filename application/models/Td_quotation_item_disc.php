<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_quotation_item_disc extends CI_Model {
	private $tbl_name = 'td_quotation_item_disc';
	private $p_key = 'kd_item_disc';
	private $title_name = 'Data Quotation Default Discount';

	public function get_all_where($conds = '') {
		$this->db->from($this->tbl_name)
			->where($conds)
			->order_by('jml_disc DESC');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	/*
	** variabel $cols harus berbentuk array
	** dengan contoh $cols = array('nama_kolom' => 'value_kolom')
	** untuk dilanjutkan dalam filter tabel
	*/
	public function default_disc_list($cols = '') {
		$this->db->select('DISTINCT(nm_pihak)', FALSE)
			->from($this->tbl_name);
		if (!empty($cols)) :
			$this->db->where($cols);
		endif;
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	/*
	** variabel $cols harus berbentuk array
	** dengan contoh $cols = array('nama_kolom' => 'value_kolom')
	** untuk dilanjutkan dalam filter tabel
	*/
	public function get_default_disc($cols = '') {
		$this->db->from($this->tbl_name);
		if (!empty($cols)) :
			$this->db->where($cols);
		endif;
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
}