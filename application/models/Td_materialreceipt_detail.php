<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_materialreceipt_detail extends CI_Model
{
	private $tbl_name = 'td_materialreceipt_detail';
	private $p_key = 'materialreceiptdetail_kd';

	public function ssp_table($materialreceipt_kd)
	{
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array(
				'db' => 'a.' . $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function ($d, $row) {

					return $this->tbl_btn($d);
				}
			),
			array(
				'db' => 'b.woitem_no_wo',
				'dt' => 2, 'field' => 'woitem_no_wo',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'b.woitem_itemcode',
				'dt' => 3, 'field' => 'woitem_itemcode',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'b.woitem_deskripsi',
				'dt' => 4, 'field' => 'woitem_deskripsi',
				'formatter' => function ($d, $row) {
					$d = "$d {$row[8]}";
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'b.woitem_jenis',
				'dt' => 5, 'field' => 'woitem_jenis',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'a.materialreceiptdetail_qty',
				'dt' => 6, 'field' => 'materialreceiptdetail_qty',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'a.materialreceiptdetail_remark',
				'dt' => 7, 'field' => 'materialreceiptdetail_remark',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'a.materialreceiptdetail_tgledit',
				'dt' => 8, 'field' => 'materialreceiptdetail_tgledit',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'b.woitem_dimensi',
				'dt' => 9, 'field' => 'woitem_dimensi',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM " . $this->tbl_name . " AS a
							LEFT JOIN td_workorder_item as b ON a.woitem_kd=b.woitem_kd";
		$data['where'] = "a.materialreceipt_kd = '" . $materialreceipt_kd . "'";

		return $data;
	}

	private function tbl_btn($id)
	{
		$btns = array();
		$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_item(\'' . $id . '\')'));

		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code()
	{
		$query = $this->db->select('MAX(' . $this->p_key . ') as maxID')
			->get($this->tbl_name)
			->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

	public function insert_data($data)
	{
		$query = $this->db->insert($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_data($id)
	{
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		return $query ? TRUE : FALSE;
	}

	public function get_by_param($param = [])
	{
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all()
	{
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data($aWhere = [], $data)
	{
		$query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query ? TRUE : FALSE;
	}

	public function insert_batch_data($data = [])
	{
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_by_param($param = [])
	{
		$query = $this->db->delete($this->tbl_name, $param);
		return $query ? TRUE : FALSE;
	}

	public function get_where_in($where, $in = [])
	{
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}

	public function getItemInsertedGroupByReqdetail($materialreq_kds = [])
	{
		return  $this->db->select('td_materialreceipt_detail.materialreqdetail_kd, td_materialreceipt_detail.woitem_kd, SUM(td_materialreceipt_detail.materialreceiptdetail_qty) as sum_materialreceiptdetail_qty')
			->join('tb_materialreceipt_detail_requisition', 'tb_materialreceipt_detail_requisition.materialreceiptreq_kd=td_materialreceipt_detail.materialreceiptreq_kd', 'left')
			->where_in('tb_materialreceipt_detail_requisition.materialreq_kd', $materialreq_kds)
			->group_by('td_materialreceipt_detail.materialreqdetail_kd')
			->get($this->tbl_name)->result_array();
	}

	//fungsi lama getMaterialPartReceipt by: bayu / faisal
	public function getMaterialPartReceipt($materialreceipt_kd)
	{
		// Data by WO Part
		$woParts = $this->db->select("td_workorder_item_detail.part_kd, tm_materialrequisition.bagian_kd, {$this->tbl_name}.materialreceiptdetail_qty")
			->where("{$this->tbl_name}.materialreceipt_kd", $materialreceipt_kd)
			->where('td_workorder_item_detail.part_kd IS NOT NULL')
			->join('td_materialrequisition_detail', "td_materialrequisition_detail.materialreqdetail_kd={$this->tbl_name}.materialreqdetail_kd", 'left')
			->join('tm_materialrequisition', "tm_materialrequisition.materialreq_kd=td_materialrequisition_detail.materialreq_kd", 'left')
			->join('td_workorder_item_detail', "td_workorder_item_detail.woitem_kd={$this->tbl_name}.woitem_kd", 'left')
			->group_by("{$this->tbl_name}.woitem_kd")
			->get($this->tbl_name)->result_array();

		/** Merge detail part */
		$mergeDetailParts = [];
		foreach ($woParts as $woPart) {
			$mergeDetailParts[] = [
				'part_kd' => $woPart['part_kd'],
				'bagian_kd' => $woPart['bagian_kd'],
				'materialreceiptdetail_qty' => $woPart['materialreceiptdetail_qty']
			];
		}

		//Data by WO packing
		$woPackings = $this->db->select("{$this->tbl_name}.woitem_kd, td_workorder_item_detail.woitemdetail_kd, tm_materialrequisition.bagian_kd, {$this->tbl_name}.materialreceiptdetail_qty")
			->where("{$this->tbl_name}.materialreceipt_kd", $materialreceipt_kd)
			->where("td_workorder_item_detail.bom_kd IS NOT NULL")
			->join('td_materialrequisition_detail', "td_materialrequisition_detail.materialreqdetail_kd={$this->tbl_name}.materialreqdetail_kd", 'left')
			->join('tm_materialrequisition', "tm_materialrequisition.materialreq_kd=td_materialrequisition_detail.materialreq_kd", 'left')
			->join('td_workorder_item_detail', "td_workorder_item_detail.woitem_kd={$this->tbl_name}.woitem_kd", 'left')
			->get($this->tbl_name)->result_array();
		if (!empty($woPackings)) {
			$woitemdetail_kds = array_unique(array_column($woPackings, 'woitemdetail_kd'));
			$wopackingParts = $this->db->select('td_workorder_item_detail.woitemdetail_kd	, td_workorder_item_detail.woitemdetail_parent, td_workorder_item_detail.part_kd	')
				->where_in("td_workorder_item_detail.woitemdetail_parent", $woitemdetail_kds)
				->where("td_workorder_item_detail.woitemdetail_generatewo", 'F')
				->get('td_workorder_item_detail')->result_array();


			/** Generate part berdasarkan atas :
			 * - wo packing
			 * - wo yang tidak digenerate no wo atas wo packing itu
			 */
			foreach ($woPackings as $woPacking) {
				foreach ($wopackingParts as $wopackingPart) {
					if ($wopackingPart['woitemdetail_parent'] == $woPacking['woitemdetail_kd']) {
						$mergeDetailParts[] = [
							'part_kd' => $wopackingPart['part_kd'],
							'bagian_kd' => $woPacking['bagian_kd'],
							'materialreceiptdetail_qty' => $woPacking['materialreceiptdetail_qty']
						];
					}
				}
			}
		}

		$groupMaterialNeeds = [];
		if (!empty($mergeDetailParts)) {
			/** Merge part_kd and sum qty */
			$groupDetailParts = [];
			$groupDetailPartQtys = [];
			foreach ($mergeDetailParts as $mergeDetailPart) {
				$groupDetailPartQtys[$mergeDetailPart['part_kd']][] = $mergeDetailPart['materialreceiptdetail_qty'];
			}
			foreach ($groupDetailPartQtys as $ePart_kd => $qtys) {
				foreach ($mergeDetailParts as $mergeDetailPart0) {
					if ($mergeDetailPart0['part_kd'] == $ePart_kd) {
						$bagian_kd = $mergeDetailPart0['bagian_kd'];
					}
				}
				$groupDetailParts[] = [
					'part_kd' => $ePart_kd,
					'bagian_kd' => $bagian_kd,
					'materialreceiptdetail_qty' => array_sum($qtys)
				];
			}

			/** Get ALL material detail by part */
			$part_kds = array_column($groupDetailParts, 'part_kd');
			$materialParts = $this->db->select('td_part_detail.*, tm_rawmaterial.rm_kode, td_rawmaterial_satuan.rmsatuan_nama')
				->where_in('td_part_detail.part_kd', $part_kds)
				->join('td_rawmaterial_satuan', "td_rawmaterial_satuan.rmsatuan_kd=td_part_detail.rmsatuan_kd", 'left')
				->join('tm_rawmaterial', "tm_rawmaterial.rm_kd=td_part_detail.rm_kd", 'left')
				->get('td_part_detail')->result_array();

			$materialNeeds = [];
			foreach ($materialParts as $materialPart) {
				foreach ($groupDetailParts as $groupDetailPart) {
					if ($groupDetailPart['part_kd'] == $materialPart['part_kd'] && $groupDetailPart['bagian_kd'] == $materialPart['bagian_kd']) {
						$materialNeeds[] = [
							'part_kd' => $groupDetailPart['part_kd'],
							'rm_kd' => $materialPart['rm_kd'],
							'rm_kode' => $materialPart['rm_kode'],
							'bagian_kd' => $materialPart['bagian_kd'],
							'partdetail_nama' => $materialPart['partdetail_nama'],
							'partdetail_deskripsi' => $materialPart['partdetail_deskripsi'],
							'partdetail_nama' => $materialPart['partdetail_nama'],
							'partdetail_spesifikasi' => $materialPart['partdetail_spesifikasi'],
							'rmsatuan_kd' => $materialPart['rmsatuan_kd'],
							'rmsatuan_nama' => $materialPart['rmsatuan_nama'],
							'sum_partdetail_qty' => $materialPart['partdetail_qty'] * $groupDetailPart['materialreceiptdetail_qty']
						];
					}
				}
			}
			/** Material Need Group by material_kd & SUM qty  */

			$groupMaterialNeedQtys = [];
			foreach ($materialNeeds as $materialNeed) {
				$groupMaterialNeedQtys[$materialNeed['rm_kd']][] = $materialNeed;
			}
			foreach ($groupMaterialNeedQtys as $eRm_kd => $qtyElements) {
				$groupMaterialNeeds[] = [
					'part_kd' => $qtyElements[0]['part_kd'],
					'rm_kd' => $eRm_kd,
					'rm_kode' => $qtyElements[0]['rm_kode'],
					'bagian_kd' => $qtyElements[0]['bagian_kd'],
					'partdetail_nama' => $qtyElements[0]['partdetail_nama'],
					'partdetail_deskripsi' => $qtyElements[0]['partdetail_deskripsi'],
					'partdetail_nama' => $qtyElements[0]['partdetail_nama'],
					'partdetail_spesifikasi' => $qtyElements[0]['partdetail_spesifikasi'],
					'rmsatuan_kd' => $qtyElements[0]['rmsatuan_kd'],
					'rmsatuan_nama' => $qtyElements[0]['rmsatuan_nama'],
					'sum_partdetail_qty' => array_sum(array_column($qtyElements, 'sum_partdetail_qty'))
				];
			}
		}

		return $groupMaterialNeeds;
	}



	//update terbaru fungsi getMaterialPartReceipt by: rizqi
	public function getMaterialPartReceiptxy($materialreceipt_kd)
	{
		$groupMaterialNeeds = $this->db->query("
		SELECT
				woitem_kd ,
				bagian_kd,
				bagian_nama,
				rm_kd,
				partdetail_nama,
				partdetail_deskripsi,
				partdetail_spesifikasi,
				rm_kode,
				rmsatuan_nama,
				SUM(qty_bom) as sum_partdetail_qty

 FROM (SELECT * FROM (SELECT
			tmd.materialreceipt_kd ,
			tmd.materialreceiptdetail_kd ,
			tmd.materialreceiptdetail_qty,
			tmd.woitem_kd ,
			tmd.rmgr_code_srj,
			tmd.batch,
			tm.materialreceipt_no ,
			tm.bagian_kd,
			tb.bagian_nama,
			tpd.rm_kd,
			tpd.partdetail_nama,
			tpd.partdetail_deskripsi,
			tpd.partdetail_spesifikasi,
			tr.rm_kode,
			tr.rm_oldkd,
			trs.rmsatuan_kd,
			trs.rmsatuan_nama,
			sum(tpd.partdetail_qty) as sum_partdetail_qty,
			(tmd.materialreceiptdetail_qty * sum(tpd.partdetail_qty)) as qty_bom
		from
			td_materialreceipt_detail tmd
		left join tm_materialreceipt tm on
			tm.materialreceipt_kd = tmd.materialreceipt_kd
		left JOIN tb_bagian tb on
			tb.bagian_kd = tm.bagian_kd
		left join 
		(
			SELECT a.woitem_kd, b.woitem_no_wo, c.part_kd 
			FROM td_materialreceipt_detail AS a
			LEFT JOIN td_workorder_item AS b ON a.woitem_kd=b.woitem_kd
			LEFT JOIN td_workorder_item_detail AS c ON c.woitem_kd=b.woitem_kd 
			WHERE a.materialreceipt_kd = $materialreceipt_kd AND c.part_kd IS NOT NULL 
			GROUP BY a.woitem_kd 
		) AS twid ON twid.woitem_kd = tmd.woitem_kd
		left JOIN td_part_detail tpd on
			tpd.part_kd = twid.part_kd
			and tpd.bagian_kd = tm.bagian_kd
		left join tm_rawmaterial as tr on
			tr.rm_kd = tpd.rm_kd
		left join td_rawmaterial_satuan trs on
			trs.rmsatuan_kd=tpd.rmsatuan_kd
		where
			tmd.materialreceipt_kd = $materialreceipt_kd
			and twid.part_kd is not null
		GROUP by
			tpd.rm_kd,
			tmd.woitem_kd,
			tmd.materialreceiptdetail_kd
		
		UNION ALL

		select
			tmd.materialreceipt_kd ,
			tmd.materialreceiptdetail_kd ,
			tmd.materialreceiptdetail_qty,
			tmd.woitem_kd ,
			tmd.rmgr_code_srj,
			tmd.batch,
			tm.materialreceipt_no ,
			tm.bagian_kd,
			tb.bagian_nama,
			tpd.rm_kd,
			tpd.partdetail_nama,
			tpd.partdetail_deskripsi,
			tpd.partdetail_spesifikasi,
			tr.rm_kode,
			tr.rm_oldkd,
			trs.rmsatuan_kd,
			trs.rmsatuan_nama,
			sum(tpd.partdetail_qty) as sum_partdetail_qty,
			(tmd.materialreceiptdetail_qty * sum(tpd.partdetail_qty)) as qty_bom
		from
			td_materialreceipt_detail tmd
		left join tm_materialreceipt tm on
			tm.materialreceipt_kd = tmd.materialreceipt_kd
		left JOIN tb_bagian tb on
			tb.bagian_kd = tm.bagian_kd
		left join td_workorder_item_detail twid on
			twid.woitem_kd = tmd.woitem_kd
		left join td_workorder_item_detail twid2 on
			twid2.woitemdetail_parent = twid.woitemdetail_kd
		inner join td_part_detail tpd on
			tpd.part_kd = twid2.part_kd
			and tpd.bagian_kd = tm.bagian_kd
		left join tm_rawmaterial as tr on
			tr.rm_kd = tpd.rm_kd
		left join td_rawmaterial_satuan trs on
			trs.rmsatuan_kd=tpd.rmsatuan_kd
		where
			tmd.materialreceipt_kd = $materialreceipt_kd
			and twid.bom_kd is not null
		group by
			tpd.rm_kd,
			tmd.woitem_kd,
			tmd.materialreceiptdetail_kd) AS WB GROUP by
			rm_kd,
			woitem_kd,
			materialreceiptdetail_kd) AS FINAL GROUP BY rm_kd")->result_array();

		return $groupMaterialNeeds;
	}

	public function getReceiptDetailWoitem($params = [])
	{
		return $this->db->where($params)
			->join('td_workorder_item', "td_workorder_item.woitem_kd={$this->tbl_name}.woitem_kd", 'left')
			->get($this->tbl_name);
	}

	public function generateDetailMaterialByMaterailReceipt($materialreceipt_kd)
	{
		$materialReceipts = $this->db->query("
		SELECT
			*
		from
			td_materialreceipt_detail tmd
		left join tm_materialreceipt tm on
			tm.materialreceipt_kd = tmd.materialreceipt_kd
		left join td_workorder_item twi on
			twi.woitem_kd = tmd.woitem_kd
		left join tb_bagian tb on
			tb.bagian_kd = tm.bagian_kd
		where
			tmd.materialreceipt_kd = $materialreceipt_kd;
		")->result_array();

		/** Part */
		$qParts = $this->db->query("
		SELECT
			tmd.materialreceipt_kd ,
			tmd.materialreceiptdetail_kd ,
			tmd.materialreceiptdetail_qty,
			tmd.woitem_kd ,
			tm.materialreceipt_no ,
			tm.bagian_kd,
			tb.bagian_nama,
			tpd.rm_kd,
			tpd.partdetail_deskripsi,
			tpd.partdetail_spesifikasi,
			tr.rm_kode,
			tr.rm_oldkd,
			trs.rmsatuan_kd,
			trs.rmsatuan_nama,
			sum(tpd.partdetail_qty) as sum_partdetail_qty
		from
			td_materialreceipt_detail tmd
		left join tm_materialreceipt tm on
			tm.materialreceipt_kd = tmd.materialreceipt_kd
		left JOIN tb_bagian tb on
			tb.bagian_kd = tm.bagian_kd
		left join 
		(
			SELECT a.woitem_kd, b.woitem_no_wo, c.part_kd 
			FROM td_materialreceipt_detail AS a
			LEFT JOIN td_workorder_item AS b ON a.woitem_kd=b.woitem_kd
			LEFT JOIN td_workorder_item_detail AS c ON c.woitem_kd=b.woitem_kd 
			WHERE a.materialreceipt_kd = $materialreceipt_kd AND c.part_kd IS NOT NULL 
			GROUP BY a.woitem_kd 
		) AS twid ON twid.woitem_kd = tmd.woitem_kd
		left JOIN td_part_detail tpd on
			tpd.part_kd = twid.part_kd
			and tpd.bagian_kd = tm.bagian_kd
		left join tm_rawmaterial as tr on
			tr.rm_kd = tpd.rm_kd
		left join td_rawmaterial_satuan trs on
			trs.rmsatuan_kd=tpd.rmsatuan_kd
		where
			tmd.materialreceipt_kd = $materialreceipt_kd
			and twid.part_kd is not null
		GROUP by
			tpd.rm_kd,
			tmd.woitem_kd
		
		UNION ALL

		select
			tmd.materialreceipt_kd ,
			tmd.materialreceiptdetail_kd ,
			tmd.materialreceiptdetail_qty,
			tmd.woitem_kd ,
			tm.materialreceipt_no ,
			tm.bagian_kd,
			tb.bagian_nama,
			tpd.rm_kd,
			tpd.partdetail_deskripsi,
			tpd.partdetail_spesifikasi,
			tr.rm_kode,
			tr.rm_oldkd,
			trs.rmsatuan_kd,
			trs.rmsatuan_nama,
			sum(tpd.partdetail_qty) as sum_partdetail_qty
		from
			td_materialreceipt_detail tmd
		left join tm_materialreceipt tm on
			tm.materialreceipt_kd = tmd.materialreceipt_kd
		left JOIN tb_bagian tb on
			tb.bagian_kd = tm.bagian_kd
		left join td_workorder_item_detail twid on
			twid.woitem_kd = tmd.woitem_kd
		left join td_workorder_item_detail twid2 on
			twid2.woitemdetail_parent = twid.woitemdetail_kd
		inner join td_part_detail tpd on
			tpd.part_kd = twid2.part_kd
			and tpd.bagian_kd = tm.bagian_kd
		left join tm_rawmaterial as tr on
			tr.rm_kd = tpd.rm_kd
		left join td_rawmaterial_satuan trs on
			trs.rmsatuan_kd=tpd.rmsatuan_kd
		where
			tmd.materialreceipt_kd = $materialreceipt_kd
			and twid.bom_kd is not null
		group by
			tpd.rm_kd,
			tmd.woitem_kd;
		")->result_array();

		$resultMaterialDetails = [];
		foreach ($materialReceipts as $materialReceipt) {
			$arrMaterialDetails = [];
			foreach ($qParts as $qPart) {
				if ($qPart['materialreceiptdetail_kd'] == $materialReceipt['materialreceiptdetail_kd']) {
					$arrMaterialDetails[] = $qPart;
				}
			}
			$materialReceipt['detailParts'] = $arrMaterialDetails;
			$resultMaterialDetails[] = $materialReceipt;
		}

		return ($resultMaterialDetails);
	}

	public function generateDetailMaterialByMaterailReceiptDetailKd($materialreceipt_kd)
	{
		$materialReceipts = $this->db->query("
		SELECT
			*
		from
			td_materialreceipt_detail tmd
		left join tm_materialreceipt tm on
			tm.materialreceipt_kd = tmd.materialreceipt_kd
		left join td_workorder_item twi on
			twi.woitem_kd = tmd.woitem_kd
		left join tb_bagian tb on
			tb.bagian_kd = tm.bagian_kd
		where
			tmd.materialreceipt_kd = $materialreceipt_kd;
		")->result_array();

		/** Part */
		$qParts = $this->db->query("
		SELECT
			tmd.materialreceipt_kd ,
			tmd.materialreceiptdetail_kd ,
			tmd.materialreceiptdetail_qty,
			tmd.woitem_kd ,
			tmd.rmgr_code_srj,
			tmd.batch,
			tm.materialreceipt_no ,
			tm.bagian_kd,
			tb.bagian_nama,
			tpd.rm_kd,
			tpd.partdetail_nama,
			tpd.partdetail_deskripsi,
			tpd.partdetail_spesifikasi,
			tr.rm_kode,
			tr.rm_oldkd,
			trs.rmsatuan_kd,
			trs.rmsatuan_nama,
			sum(tpd.partdetail_qty) as sum_partdetail_qty
		from
			td_materialreceipt_detail tmd
		left join tm_materialreceipt tm on
			tm.materialreceipt_kd = tmd.materialreceipt_kd
		left JOIN tb_bagian tb on
			tb.bagian_kd = tm.bagian_kd
		left join 
		(
			SELECT a.woitem_kd, b.woitem_no_wo, c.part_kd 
			FROM td_materialreceipt_detail AS a
			LEFT JOIN td_workorder_item AS b ON a.woitem_kd=b.woitem_kd
			LEFT JOIN td_workorder_item_detail AS c ON c.woitem_kd=b.woitem_kd 
			WHERE a.materialreceipt_kd = $materialreceipt_kd AND c.part_kd IS NOT NULL 
			GROUP BY a.woitem_kd 
		) AS twid ON twid.woitem_kd = tmd.woitem_kd
		left JOIN td_part_detail tpd on
			tpd.part_kd = twid.part_kd
			and tpd.bagian_kd = tm.bagian_kd
		left join tm_rawmaterial as tr on
			tr.rm_kd = tpd.rm_kd
		left join td_rawmaterial_satuan trs on
			trs.rmsatuan_kd=tpd.rmsatuan_kd
		where
			tmd.materialreceipt_kd = $materialreceipt_kd
			and twid.part_kd is not null
		GROUP by
			tpd.rm_kd,
			tmd.woitem_kd,
			tmd.materialreceiptdetail_kd
		
		UNION ALL

		select
			tmd.materialreceipt_kd ,
			tmd.materialreceiptdetail_kd ,
			tmd.materialreceiptdetail_qty,
			tmd.woitem_kd ,
			tmd.rmgr_code_srj,
			tmd.batch,
			tm.materialreceipt_no ,
			tm.bagian_kd,
			tb.bagian_nama,
			tpd.rm_kd,
			tpd.partdetail_nama,
			tpd.partdetail_deskripsi,
			tpd.partdetail_spesifikasi,
			tr.rm_kode,
			tr.rm_oldkd,
			trs.rmsatuan_kd,
			trs.rmsatuan_nama,
			sum(tpd.partdetail_qty) as sum_partdetail_qty
		from
			td_materialreceipt_detail tmd
		left join tm_materialreceipt tm on
			tm.materialreceipt_kd = tmd.materialreceipt_kd
		left JOIN tb_bagian tb on
			tb.bagian_kd = tm.bagian_kd
		left join td_workorder_item_detail twid on
			twid.woitem_kd = tmd.woitem_kd
		left join td_workorder_item_detail twid2 on
			twid2.woitemdetail_parent = twid.woitemdetail_kd
			AND twid2.woitemdetail_generatewo <> 'T'
		inner join td_part_detail tpd on
			tpd.part_kd = twid2.part_kd
			and tpd.bagian_kd = tm.bagian_kd
		left join tm_rawmaterial as tr on
			tr.rm_kd = tpd.rm_kd
		left join td_rawmaterial_satuan trs on
			trs.rmsatuan_kd=tpd.rmsatuan_kd
		where
			tmd.materialreceipt_kd = $materialreceipt_kd
			and twid.bom_kd is not null
		group by
			tpd.rm_kd,
			tmd.woitem_kd,
			tmd.materialreceiptdetail_kd;
		")->result_array();

		$resultMaterialDetails = [];
		foreach ($materialReceipts as $materialReceipt) {
			$arrMaterialDetails = [];
			foreach ($qParts as $qPart) {
				if ($qPart['materialreceiptdetail_kd'] == $materialReceipt['materialreceiptdetail_kd']) {
					$arrMaterialDetails[] = $qPart;
				}
			}
			$materialReceipt['detailParts'] = $arrMaterialDetails;
			$resultMaterialDetails[] = $materialReceipt;
		}

		return ($resultMaterialDetails);
	}

	public function generateDetailMaterialByMaterailReceiptDetailKdPT2($materialreceipt_kd)
	{
		$materialReceipts = $this->db->query("
		SELECT
			*
		from
			td_materialreceipt_detail tmd
		left join tm_materialreceipt tm on
			tm.materialreceipt_kd = tmd.materialreceipt_kd
		left join td_workorder_item twi on
			twi.woitem_kd = tmd.woitem_kd
		left join tb_bagian tb on
			tb.bagian_kd = tm.bagian_kd
		where
			tmd.materialreceipt_kd = $materialreceipt_kd;
		")->result_array();

		/** Part */
		$qParts = $this->db->query("SELECT * FROM (
		SELECT
			tmd.materialreceipt_kd ,
			tmd.materialreceiptdetail_kd ,
			tmd.materialreceiptdetail_qty,
			tmd.woitem_kd ,
			tmd.rmgr_code_srj,
			tmd.batch,
			tm.materialreceipt_no ,
			tm.bagian_kd,
			tb.bagian_nama,
			tpd.rm_kd,
			tpd.partdetail_nama,
			tpd.partdetail_deskripsi,
			tpd.partdetail_spesifikasi,
			tr.rm_kode,
			tr.rm_oldkd,
			trs.rmsatuan_kd,
			trs.rmsatuan_nama,
			sum(tpd.partdetail_qty) as sum_partdetail_qty
		from
			td_materialreceipt_detail tmd
		left join tm_materialreceipt tm on
			tm.materialreceipt_kd = tmd.materialreceipt_kd
		left JOIN tb_bagian tb on
			tb.bagian_kd = tm.bagian_kd
		left join 
		(
			SELECT a.woitem_kd, b.woitem_no_wo, c.part_kd 
			FROM td_materialreceipt_detail AS a
			LEFT JOIN td_workorder_item AS b ON a.woitem_kd=b.woitem_kd
			LEFT JOIN td_workorder_item_detail AS c ON c.woitem_kd=b.woitem_kd 
			WHERE a.materialreceipt_kd = $materialreceipt_kd AND c.part_kd IS NOT NULL 
			GROUP BY a.woitem_kd 
		) AS twid ON twid.woitem_kd = tmd.woitem_kd
		left JOIN td_part_detail tpd on
			tpd.part_kd = twid.part_kd
			and tpd.bagian_kd = tm.bagian_kd
		left join tm_rawmaterial as tr on
			tr.rm_kd = tpd.rm_kd
		left join td_rawmaterial_satuan trs on
			trs.rmsatuan_kd=tpd.rmsatuan_kd
		where
			tmd.materialreceipt_kd = $materialreceipt_kd
			and twid.part_kd is not null
		GROUP by
			tpd.rm_kd,
			tmd.woitem_kd,
			tmd.materialreceiptdetail_kd
		
		UNION ALL

		select
			tmd.materialreceipt_kd ,
			tmd.materialreceiptdetail_kd ,
			tmd.materialreceiptdetail_qty,
			tmd.woitem_kd ,
			tmd.rmgr_code_srj,
			tmd.batch,
			tm.materialreceipt_no ,
			tm.bagian_kd,
			tb.bagian_nama,
			tpd.rm_kd,
			tpd.partdetail_nama,
			tpd.partdetail_deskripsi,
			tpd.partdetail_spesifikasi,
			tr.rm_kode,
			tr.rm_oldkd,
			trs.rmsatuan_kd,
			trs.rmsatuan_nama,
			sum(tpd.partdetail_qty) as sum_partdetail_qty
		from
			td_materialreceipt_detail tmd
		left join tm_materialreceipt tm on
			tm.materialreceipt_kd = tmd.materialreceipt_kd
		left JOIN tb_bagian tb on
			tb.bagian_kd = tm.bagian_kd
		left join td_workorder_item_detail twid on
			twid.woitem_kd = tmd.woitem_kd
		left join td_workorder_item_detail twid2 on
			twid2.woitemdetail_parent = twid.woitemdetail_kd
		inner join td_part_detail tpd on
			tpd.part_kd = twid2.part_kd
			and tpd.bagian_kd = tm.bagian_kd
		left join tm_rawmaterial as tr on
			tr.rm_kd = tpd.rm_kd
		left join td_rawmaterial_satuan trs on
			trs.rmsatuan_kd=tpd.rmsatuan_kd
		where
			tmd.materialreceipt_kd = $materialreceipt_kd
			and twid.bom_kd is not null
		group by
			tpd.rm_kd,
			tmd.woitem_kd,
			tmd.materialreceiptdetail_kd) AS WB GROUP by
			rm_kd,
			woitem_kd,
			materialreceiptdetail_kd
		")->result_array();

		$resultMaterialDetails = [];
		foreach ($materialReceipts as $materialReceipt) {
			$arrMaterialDetails = [];
			foreach ($qParts as $qPart) {
				if ($qPart['materialreceiptdetail_kd'] == $materialReceipt['materialreceiptdetail_kd']) {
					$arrMaterialDetails[] = $qPart;
				}
			}
			$materialReceipt['detailParts'] = $arrMaterialDetails;
			$resultMaterialDetails[] = $materialReceipt;
		}

		return ($resultMaterialDetails);
	}
}
