<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_salesorder extends CI_Model {
	private $tbl_name = 'tm_salesorder';
	private $p_key = 'kd_msalesorder';

	
	public function __construct() {
		parent::__construct();

		$this->load->model(array('td_salesorder_item'));
	}

	/* --start ssp tabel untuk modul ppic-- */
	public function ssp_table($where = '') {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){

					return $this->tbl_btn($d, $row[1], $row[7]);
				} ),
			array( 'db' => 'a.no_salesorder',
				'dt' => 2, 'field' => 'no_salesorder',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				} ),
			array( 'db' => 'a.no_po',
				'dt' => 3, 'field' => 'no_po',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				} ),
			array( 'db' => 'b.nm_customer',
				'dt' => 4, 'field' => 'nm_customer',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				} ),
			array( 'db' => 'c.nm_salesperson',
				'dt' => 5, 'field' => 'nm_salesperson',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				} ),
			array( 'db' => 'a.tgl_so',
				'dt' => 6, 'field' => 'tgl_so',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);
					$d = !empty($d) && $d != '0000-00-00'?format_date($d, 'd-m-Y'):'-';

					return $d;
				} ),
			array( 'db' => 'a.tgl_kirim',
				'dt' => 7, 'field' => 'tgl_kirim',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);
					$d = !empty($d) && $d != '0000-00-00'?format_date($d, 'd-m-Y'):'-';

					return $d;
				} ),
			array( 'db' => 'a.status_so',
				'dt' => 8, 'field' => 'status_so',
				'formatter' => function($d, $row){
					$d = $this->security->xss_clean($d);
					$word = process_status($d);
					$color = color_status($d);
					$d = bg_label($word, $color);

					return $d;
				} ),
		);

		$data['sql_details'] = sql_connect();

		$data['joinQuery'] = "
			FROM
				".$this->tbl_name." a
			LEFT JOIN tm_customer b ON a.customer_kd = b.kd_customer
			LEFT JOIN tb_salesperson c ON a.salesperson_kd = c.kd_salesperson
		";

		if (!empty($where)) :
			if (is_array($where)) :
				$no = 0;
				$jml = count($where);
				$list_where = '';
				foreach ($where as $key) :
					$no++;
					$penghubung = $no == $jml?'':', ';
					$list_where .= '\''.$key.'\''.$penghubung;
				endforeach;
				$data['where'] = "a.status_so IN (".$list_where.")";
			else :
				$data['where'] = "a.status_so = '".$where."'";
			endif;
		else :
			$data['where'] = "";
		endif;

		return $data;
	}
	/* --end ssp tabel untuk modul ppic-- */

	/* --start ssp tabel untuk modul sales order-- */
	public function ssp_table_so($where = '') {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){

					return $this->tbl_btn_so($d, $row[1], $row[8], strtolower($row[4]), $row[9]);
				} ),
			array( 'db' => 'a.no_salesorder',
				'dt' => 2, 'field' => 'no_salesorder',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				} ),
			array( 'db' => 'a.no_po',
				'dt' => 3, 'field' => 'no_po',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);
					$d = empty($d)?'-':$d;

					return $d;
				} ),
			array( 'db' => 'b.nm_customer',
				'dt' => 4, 'field' => 'nm_customer',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				} ),
			array( 'db' => 'a.tipe_customer',
				'dt' => 5, 'field' => 'tipe_customer',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				} ),
			array( 'db' => 'c.nm_salesperson',
				'dt' => 6, 'field' => 'nm_salesperson',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				} ),
			array( 'db' => 'a.tgl_so',
				'dt' => 7, 'field' => 'tgl_so',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);
					// $d = format_date($d, 'd-m-Y');

					return $d;
				} ),
			array( 'db' => 'a.tgl_kirim',
				'dt' => 8, 'field' => 'tgl_kirim',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);
					// $d = format_date($d, 'd-m-Y');

					return $d;
				} ),
			array( 'db' => 'a.status_so',
				'dt' => 9, 'field' => 'status_so',
				'formatter' => function($d, $row){
					$d = $this->security->xss_clean($d);
					$word = process_status($d);
					$color = color_status($d);
					$d = bg_label($word, $color);

					return $d;
				} ),
			array( 'db' => 'a.order_tipe',
				'dt' => 10, 'field' => 'order_tipe',
				'formatter' => function($d){
					$d = ucwords($this->security->xss_clean($d));

					return $d;
				} ),
			array( 'db' => 'a.push_sap',
			'dt' => 11, 'field' => 'push_sap',
			'formatter' => function($d, $row){
				$change_state = cek_permission('SALESORDER_UPDATE_STATE');
				$btn = "";
				if($change_state):
					$data = '"' . $row[0] . '"';
					$status_so = $row[8];
					$push_sap = $row[10];
					$btn = "";

					if($status_so == "process_lpo" || $status_so == "process_wo" || $status_so == "finish"){
						if($push_sap == "T"){
							$btn = "<button type='button' class='btn btn-primary btn-flat btn-push-sap' onclick='push_sap(" . $data .")' ><i class='fa fa-arrow-up'></i> Push to SAP </button>";
						}
					}

					// if($status_so != "pending" || $status_so != "cancel" || $status_so != "finish"):
					// 	if($push_sap == "T"):
					// 		$btn = "<button type='button' class='btn btn-primary btn-flat btn-push-sap' onclick='push_sap(" . $data .")' ><i class='fa fa-arrow-up'></i> Push to SAP </button>";
					// 	else:
					// 		$btn = "";
					// 	endif;
					// else:
					// 	$btn = "";
					// endif;
				endif;
				return $btn;
			} ),
		);

		$data['sql_details'] = sql_connect();

		$data['joinQuery'] = "
			FROM
				".$this->tbl_name." a
			LEFT JOIN tm_customer b ON a.customer_kd = b.kd_customer
			LEFT JOIN tb_salesperson c ON a.salesperson_kd = c.kd_salesperson
		";

		$kd_items = $this->session->kd_manage_items;
		if ($kd_items == '1') :
			$data['where'] = "";
		elseif ($kd_items == '2') :
			$data['where'] = "a.tipe_customer = 'Ekspor'";
		elseif ($kd_items == '3') :
			$data['where'] = "a.tipe_customer = 'Lokal'";
		elseif ($kd_items == '10') :
			$data['where'] = "a.tipe_customer = 'Tidak Ada'";
		endif;

		return $data;
	}
	/* --end ssp tabel untuk modul sales order-- */

	/* --start button tabel untuk modul ppic-- */
	private function tbl_btn($id, $var, $status) {
		$read_access = $this->session->read_access;
		$update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		$btns[] = get_btn(array('access' => $read_access, 'title' => 'Detail Production Order', 'icon' => 'list', 'onclick' => 'detail_data(\''.$id.'\')'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}
	/* --end button tabel untuk modul ppic-- */

	/* --start button tabel untuk modul sales order-- */
	private function tbl_btn_so($id, $var, $status, $tipe, $order_tipe) {
		$read_access = cek_permission('SALESORDER_VIEW');
		$update_access = cek_permission('SALESORDER_UPDATE');
		$delete_access = cek_permission('SALESORDER_DELETE');
		$create_lpo = cek_permission('SALESORDER_CREATE_PRODUCTIONORDER');
		$create_do = cek_permission('SALESORDER_CREATE_DELIVERYORDER');
		$change_state = cek_permission('SALESORDER_UPDATE_STATE');
		$SO = $this->get_row($id);
		
		$btns = array();
		if ($read_access) {
			$btns[] = get_btn(array('access' => $read_access, 'title' => 'Detail SO', 'icon' => 'search', 'onclick' => 'bacaSales(\''.$id.'\', \'so_detail\', \''.$tipe.'\')'));
		}
		if ($status == 'pending') {
			if ($update_access) {
				$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah SO', 'icon' => 'pencil', 'onclick' => 'editSales(\''.$id.'\', \''.$order_tipe.'\')'));
				$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah Item', 'icon' => 'list', 'onclick' => 'editItems(\''.$id.'\')'));
				// $btns[] = get_btn(array('access' => $update_access, 'title' => 'Additional Order', 'icon' => 'plus', 'onclick' => 'additionalOrder(\''.$id.'\')'));
				$btns[] = get_btn_divider();
			}
			if ($change_state) {
				$btns[] = get_btn(array('access' => $update_access, 'title' => 'Cancel SO', 'icon' => 'ban',
					'onclick' => 'return confirm(\'Anda akan membatalkan Sales Order = '.$var.'?\')?ubahStatus(\''.$id.'\', \'cancel\'):false'));
			}
			if ($tipe == 'lokal') {
				if ($create_lpo) {
					$btns[] = get_btn(array('access' => $update_access, 'title' => 'Process Production Order', 'icon' => 'check',
					'onclick' => 'return confirm(\'Sales Order = '.$var.', akan diproses Production Order?\')?check_items(\''.$id.'\'):false'));
				}
				if ($read_access) {
					$btns[] = get_btn(array('access' => $read_access, 'title' => 'Cetak Proforma Invoice', 'icon' => 'file-text',
						'onclick' => 'window.open(\''.base_url().'sales/invoice/invoice_data/cetak/'.$tipe.'/final/'.$id.'/proforma\')'));
				}
			}else{
				if ($create_lpo) {
					$btns[] = get_btn(array('access' => $update_access, 'title' => 'Process Production Order', 'icon' => 'check',
					'onclick' => 'return confirm(\'Sales Order = '.$var.', akan diproses Production Order?\')?productionProcess(\''.$id.'\', \''.$tipe.'\'):false'));
				}
			}
			$btns[] = get_btn_divider();
			if ($delete_access) {
				$btns[] = get_btn(array('access' => $delete_access, 'title' => 'Hapus SO', 'icon' => 'trash', 'onclick' => 'return confirm(\'Anda akan menghapus Sales Order = '.$var.'?\')?hapusSO(\''.$id.'\'):false'));
			}
		}elseif ($status == 'process_lpo') {
			if ($update_access) {
				$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah SO', 'icon' => 'pencil', 'onclick' => 'editSales(\''.$id.'\', \''.$order_tipe.'\')'));
				$btns[] = get_btn(array('access' => $update_access, 'title' => 'Additional Order', 'icon' => 'plus', 'onclick' => 'additionalOrder(\''.$id.'\')'));
			}
			if ($tipe == 'lokal') {
				if ($update_access) {
					$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah DP', 'icon' => 'dollar', 'onclick' => 'bukaDp(\''.$id.'\')'));
					$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah DP Termin', 'icon' => 'dollar', 'onclick' => 'bukaDpTermin(\''.$id.'\')'));
					$btns[] = get_btn(array('access' => $update_access, 'title' => 'List DP', 'icon' => 'dollar', 'onclick' => 'bukaListDp(\''.$id.'\')'));

				}
				if ($create_do) {
					$btns[] = get_btn(array('access' => $update_access, 'title' => 'Buat DO', 'icon' => 'truck',
						'onclick' => 'return confirm(\'Sales Order = '.$var.', akan diproses DO?\')?prosesDo(\'\', \''.$id.'\', \''.ucwords($tipe).'\'):false'));
				}
			}else{
				if ($create_do) {
					$btns[] = get_btn(array('access' => $update_access, 'title' => 'Buat DO', 'icon' => 'truck',
						'onclick' => 'return confirm(\'Sales Order = '.$var.', akan diproses DO?\')?prosesDo(\'\', \''.$id.'\', \''.ucwords($tipe).'\'):false'));
				}
			}
			$btns[] = get_btn_divider();
			if ($tipe == 'lokal') {
				if ($read_access) {
					$btns[] = get_btn(array('access' => $read_access, 'title' => 'Cetak Invoice DP', 'icon' => 'search', 'onclick' => 'bacaSales(\''.$id.'\', \'inv_dp_detail\', \''.$tipe.'\')'));
					/* $btns[] = get_btn(array('access' => $update_access, 'title' => 'Cetak Invoice DP', 'icon' => 'file-text-o',
						'onclick' => 'window.open(\''.base_url().'sales/invoice/invoice_data/cetak/'.$tipe.'/dp/'.$id.'\')')); */
					$btns[] = get_btn(array('access' => $read_access, 'title' => 'Cetak Proforma Invoice', 'icon' => 'file-text',
						'onclick' => 'window.open(\''.base_url().'sales/invoice/invoice_data/cetak/'.$tipe.'/final/'.$id.'/proforma\')'));
				}
			}else{
				if ($read_access) {
					$btns[] = get_btn(array('access' => $read_access, 'title' => 'Cetak Proforma Invoice', 'icon' => 'file-text',
						'onclick' => 'window.open(\''.base_url().'sales/invoice/invoice_data/cetak/'.$tipe.'/final/'.$id.'/proforma\')'));
				}
			}
			if ($read_access) {
				$btns[] = get_btn(array('access' => $read_access, 'title' => 'Cetak Production Order', 'icon' => 'file-excel-o',
					'onclick' => 'window.open(\''.base_url().'menu_ppic/data_lpo/cetak/'.$id.'\')'));
			}
			// if ($create_do) {
			// 	$btns[] = get_btn(array('access' => $update_access, 'title' => 'Buat DO', 'icon' => 'truck',
			// 		'onclick' => 'return confirm(\'Sales Order = '.$var.', akan diproses DO?\')?prosesDo(\'\', \''.$id.'\', \''.ucwords($tipe).'\'):false'));
			// }
			if ($change_state) {
				if ($change_state) {
					$btns[] = get_btn(array('access' => $update_access, 'title' => 'Finish SO', 'icon' => 'check',
						'onclick' => 'return confirm(\'Sales Order = '.$var.', dalam status finish?\')?ubahStatus(\''.$id.'\', \'finish\'):false'));
					$btns[] = get_btn(array('access' => $update_access, 'title' => 'Cancel SO', 'icon' => 'ban', 'onclick' => 'cancel_so(\''.$id.'\')'));
				}
			}
		}elseif ($status == 'process_wo') {
			if ($update_access) {
				$btns[] = get_btn(array('access' => $update_access, 'title' => 'Additional Order', 'icon' => 'plus', 'onclick' => 'additionalOrder(\''.$id.'\')'));
			}
			if ($tipe == 'lokal') :
				if ($update_access) {
					$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah DP', 'icon' => 'dollar', 'onclick' => 'bukaDp(\''.$id.'\')'));
					$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah DP Termin', 'icon' => 'dollar', 'onclick' => 'bukaDpTermin(\''.$id.'\')'));
					$btns[] = get_btn(array('access' => $update_access, 'title' => 'List DP', 'icon' => 'dollar', 'onclick' => 'bukaListDp(\''.$id.'\')'));
				}
			endif;
			$btns[] = get_btn_divider();
			if ($tipe == 'lokal') {
				if ($read_access) {
					$btns[] = get_btn(array('access' => $read_access, 'title' => 'Cetak Invoice DP', 'icon' => 'search', 'onclick' => 'bacaSales(\''.$id.'\', \'inv_dp_detail\', \''.$tipe.'\')'));
					/* $btns[] = get_btn(array('access' => $update_access, 'title' => 'Cetak Invoice DP', 'icon' => 'file-text-o',
						'onclick' => 'window.open(\''.base_url().'sales/invoice/invoice_data/cetak/'.$tipe.'/dp/'.$id.'\')')); */
					$btns[] = get_btn(array('access' => $read_access, 'title' => 'Cetak Proforma Invoice', 'icon' => 'file-text',
						'onclick' => 'window.open(\''.base_url().'sales/invoice/invoice_data/cetak/'.$tipe.'/final/'.$id.'/proforma\')'));
				}
			}else{
				if ($read_access) {
					$btns[] = get_btn(array('access' => $read_access, 'title' => 'Cetak Proforma Invoice', 'icon' => 'file-text',
						'onclick' => 'window.open(\''.base_url().'sales/invoice/invoice_data/cetak/'.$tipe.'/final/'.$id.'/proforma\')'));
				}
			}
			if ($read_access) {
				$btns[] = get_btn(array('access' => $read_access, 'title' => 'Cetak Production Order', 'icon' => 'file-excel-o',
					'onclick' => 'window.open(\''.base_url().'menu_ppic/data_lpo/cetak/'.$id.'\')'));
			}
			if ($create_do) {
				$btns[] = get_btn(array('access' => $update_access, 'title' => 'Buat DO', 'icon' => 'truck',
					'onclick' => 'return confirm(\'Sales Order = '.$var.', akan diproses DO?\')?prosesDo(\'\', \''.$id.'\', \''.ucwords($tipe).'\'):false'));
			}
			if ($change_state) {
				$btns[] = get_btn(array('access' => $update_access, 'title' => 'Finish SO', 'icon' => 'check',
					'onclick' => 'return confirm(\'Sales Order = '.$var.', dalam status finish?\')?ubahStatus(\''.$id.'\', \'finish\'):false'));
				$btns[] = get_btn(array('access' => $update_access, 'title' => 'Cancel SO', 'icon' => 'ban', 'onclick' => 'cancel_so(\''.$id.'\')'));
			}
		}elseif ($status == 'finish') {
			// if ($update_access) {
			// 	$btns[] = get_btn(array('access' => $update_access, 'title' => 'Additional Order', 'icon' => 'plus', 'onclick' => 'additionalOrder(\''.$id.'\')'));
			// }
			if ($tipe == 'lokal') {
				if ($read_access){
					$btns[] = get_btn(array('access' => $read_access, 'title' => 'Cetak Invoice DP', 'icon' => 'search', 'onclick' => 'bacaSales(\''.$id.'\', \'inv_dp_detail\', \''.$tipe.'\')'));
					/* $btns[] = get_btn(array('access' => $update_access, 'title' => 'Cetak Invoice DP', 'icon' => 'file-text-o',
						'onclick' => 'window.open(\''.base_url().'sales/invoice/invoice_data/cetak/'.$tipe.'/dp/'.$id.'\')')); */
					$btns[] = get_btn(array('access' => $read_access, 'title' => 'Cetak Proforma Invoice', 'icon' => 'file-text',
						'onclick' => 'window.open(\''.base_url().'sales/invoice/invoice_data/cetak/'.$tipe.'/final/'.$id.'/proforma\')'));
				}
			}else{
				if ($read_access) {
					$btns[] = get_btn(array('access' => $read_access, 'title' => 'Cetak Proforma Invoice', 'icon' => 'file-text',
						'onclick' => 'window.open(\''.base_url().'sales/invoice/invoice_data/cetak/'.$tipe.'/final/'.$id.'/proforma\')'));
				}
			}
		}
		$btn_group = group_btns($btns);

		return $btn_group;
	}
	/* --end button tabel untuk modul sales order-- */

	/*
	** Fungsi ini digunakan untuk mendapatkan data berdasarkan kd_msalesorder
	*/
	public function get_row($kd_msalesorder = '') {
		$this->db->from($this->tbl_name)			
			->where(array($this->p_key => $kd_msalesorder));
		$query = $this->db->get();
		$row = $query->row();
		$data = [];
		if (!empty($row)) :
			$data = $row;
		endif;
		return $data;
	}

	public function create_nosalesorder($code_for = '', $tipe_customer = '') {
		$this->load->model(array('tb_code_format', 'm_builder'));
		$this->load->helper(array('my_helper'));
		$row = $this->tb_code_format->get_code_for($code_for);
		if (!empty($row)) :
			$row_no = $this->get_last_nosalesorder($row->on_reset, $tipe_customer, $row->code_for);
			$reset = $this->check_reset_nosales($row->code_for, $row->on_reset);
			// Cari persamaan antara urutan angka dengan unformatted dan formatted
			$code = code_maker_new($row->code_format, $row_no, $row->code_separator, $reset);
		else :
			$code = '';
		endif;
		return $code;
	}

	public function get_last_nosalesorder($on_reset = '', $tipe_customer = '', $code_for = '') {
		if ($on_reset == 'year') :
			$where = "YEAR(a.tgl_input) = '".date('Y')."' AND a.tipe_customer = '".$tipe_customer."' AND b.tgl_edit < a.tgl_input AND b.code_for = '".$code_for."'";
		elseif ($on_reset == 'month') :
			$where = "YEAR(a.tgl_input) = '".date('Y')."' AND MONTH(a.tgl_input) = '".date('m')."' AND a.tipe_customer = '".$tipe_customer."' AND b.tgl_edit < a.tgl_input AND b.code_for = '".$code_for."'";
		elseif($on_reset == 'day') :
			$where = "DATE(a.tgl_input) = '".date('Y-m-d')."' AND a.tipe_customer = '".$tipe_customer."' AND b.tgl_edit < a.tgl_input AND b.code_for = '".$code_for."'";
		endif;
		$this->db->select('a.no_salesorder')
			->from($this->tbl_name.' AS a, tb_code_format AS b')
			->where($where)
			->group_by('a.no_salesorder')
			->order_by('a.no_salesorder DESC')
			->limit(1);
		$query = $this->db->get();
		$row = $query->row();
		$no_salesorder = !empty($row)?$row->no_salesorder:'';
		return $no_salesorder;
	}

	public function check_reset_nosales($tipe_format = '', $resetter = '') {
		$where = '';
		if ($resetter == 'year') :
			$select = 'YEAR(tgl_input) as thn_input';
			$where = array('YEAR(tgl_input)' => date('Y'));
		elseif ($resetter == 'month') :
			$select = 'YEAR(tgl_input) as thn_input, MONTH(tgl_input) as bln_input';
			$where = array('YEAR(tgl_input)' => date('Y'), 'MONTH(tgl_input)' => date('m'));
		elseif ($resetter == 'day') :
			$select = 'DATE(tgl_input) as hari_input';
			$where = array('DATE(tgl_input)' => date('Y-m-d'));
		endif;

		// Cek dari variabel tabel, jika tidak ada data maka gunakan tabel code format untuk parameter reset
		$this->db->select($select)->from($this->tbl_name)->where($where);
		$query = $this->db->get();
		$num = $query->num_rows();
		$result = $query->row();
		if ($num < 1) :
			$this->db->select($select)->from('tb_code_format')->where(array('code_for' => $tipe_format));
			$query = $this->db->get();
			$result = $query->row();
		endif;
		if ($resetter == 'year') :
			$thn = $result->thn_input;
			$tgl = $thn;
			$compare = date('Y');
		elseif ($resetter == 'month') :
			$thn = $result->thn_input;
			$bln = $result->bln_input;
			$tgl = $thn.'-'.$bln;
			$compare = date('Y-m');
		elseif ($resetter == 'day') :
			$tgl = $result->hari_input;
			$compare = date('Y-m-d');
		endif;

		return strtotime($tgl) == strtotime($compare)?'0':'1';
	}

	public function create_noinvoicedp($code_for = '', $tipe_customer = '') {
		$this->load->model(array('tb_code_format', 'm_builder'));
		$this->load->helper(array('my_helper'));
		$row = $this->tb_code_format->get_code_for($code_for);
		if (!empty($row)) :
			$row_no = $this->get_last_noinvoicedp($row->on_reset, $tipe_customer, $row->code_for);
			$reset = $this->check_reset_noinvoice($row->code_for, $row->on_reset);
			// Cari persamaan antara urutan angka dengan unformatted dan formatted
			$code = code_maker_new($row->code_format, $row_no, $row->code_separator, $reset);
		else :
			$code = '';
		endif;
		return $code;
	}

	public function get_last_noinvoicedp($on_reset = '', $tipe_customer = '', $code_for = '') {
		if ($on_reset == 'year') :
			$where = "YEAR(tt.tgl_input) = '".date('Y')."' AND tt.tipe_customer = '".$tipe_customer."' AND b.tgl_edit < tt.tgl_input AND b.code_for = '".$code_for."'";
		elseif ($on_reset == 'month') :
			$where = "YEAR(tt.tgl_input) = '".date('Y')."' AND MONTH(tt.tgl_input) = '".date('m')."' AND tt.tipe_customer = '".$tipe_customer."' AND b.tgl_edit < tt.tgl_input AND b.code_for = '".$code_for."'";
		elseif($on_reset == 'day') :
			$where = "DATE(tt.tgl_input) = '".date('Y-m-d')."' AND tt.tipe_customer = '".$tipe_customer."' AND b.tgl_edit < tt.tgl_input AND b.code_for = '".$code_for."'";
		endif;
		$query = $this->db->query("
			SELECT tt.no_invoice
			FROM
				(
					SELECT no_invoice_dp AS no_invoice, tgl_input, tipe_customer FROM tm_salesorder WHERE no_invoice_dp IS NOT NULL OR no_invoice_dp != '' UNION ALL
					SELECT no_invoice, tgl_input, tipe_do AS tipe_customer FROM tm_deliveryorder UNION ALL
					SELECT a.no_invoice, a.tgl_input, b.tipe_customer FROM td_so_termin a LEFT JOIN tm_salesorder b ON a.msalesorder_kd = b.kd_msalesorder
				) AS tt,
				tb_code_format AS b
			WHERE
				".$where."
			GROUP BY
				tt.no_invoice
			ORDER BY
				tt.no_invoice DESC
			LIMIT 1
		");
		$row = $query->row();
		$no_invoice = !empty($row)?$row->no_invoice:'';
		return $no_invoice;
	}

	public function check_reset_noinvoice($tipe_format = '', $resetter = '') {
		$where = '';
		if ($resetter == 'year') :
			$select = "YEAR(tt.tgl_input) AS thn_input";
			$sel_format = "YEAR(tgl_input) AS thn_input";
			$where = "YEAR(tt.tgl_input) = '".date('Y')."'";
		elseif ($resetter == 'month') :
			$select = "YEAR(tt.tgl_input) AS thn_input, MONTH(tt.tgl_input) AS bln_input";
			$sel_format = "YEAR(tgl_input) AS thn_input, MONTH(tgl_input) AS bln_input";
			$where = "YEAR(tt.tgl_input) = '".date('Y')."' AND MONTH(tt.tgl_input) = '".date('m')."'";
		elseif ($resetter == 'day') :
			$select = "DATE(tt.tgl_input) AS hari_input";
			$sel_format = "DATE(tgl_input) AS hari_input";
			$where = "DATE(tt.tgl_input) = '".date('Y-m-d')."'";
		endif;

		// Cek dari variabel tabel, jika tidak ada data maka gunakan tabel code format untuk parameter reset
		$query = $this->db->query("
			SELECT ".$select."
			FROM
				(
					SELECT tgl_input, no_invoice_dp AS no_invoice FROM tm_salesorder WHERE no_invoice_dp IS NOT NULL OR no_invoice_dp != '' UNION ALL
					SELECT tgl_input, no_invoice FROM tm_deliveryorder UNION ALL
					SELECT no_invoice, tgl_input FROM td_so_termin
				) AS tt
			WHERE
				".$where."
			GROUP BY
				tt.tgl_input
			ORDER BY
				tt.tgl_input
			LIMIT 1
		");
		$num = $query->num_rows();
		$result = $query->row();
		if ($num < 1) :
			$this->db->select($sel_format)
				->from('tb_code_format')
				->where(array('code_for' => $tipe_format));
			$query = $this->db->get();
			$result = $query->row();
		endif;
		if ($resetter == 'year') :
			$thn = $result->thn_input;
			$tgl = $thn;
			$compare = date('Y');
		elseif ($resetter == 'month') :
			$thn = $result->thn_input;
			$bln = $result->bln_input;
			$tgl = $thn.'-'.$bln;
			$compare = date('Y-m');
		elseif ($resetter == 'day') :
			$tgl = $result->hari_input;
			$compare = date('Y-m-d');
		endif;

		return strtotime($tgl) == strtotime($compare)?'0':'1';
	}

	public function ubah_status($kode, $status, $var) {
		$act = $this->update_data($status, $kode);
		if ($act) :
			return buildAlert('success', 'Berhasil!', 'Berhasil mengubah status No Salesorder = '.$var.'!');
			$this->m_builder->write_log('Berhasil!', 'mengubah status', array('No Salesorder' => $var));
		else :
			return buildAlert('danger', 'Gagal!', 'Gagal mengubah status No Salesorder = '.$var.'!');
			$this->m_builder->write_log('Gagal!', 'mengubah status', array('No Salesorder' => $var));
		endif;
	}

	public function insert_dp($data, $set) {
		$log = array_merge($data, $set);
		$status = array('status_so' => 'process_lpo');
		$act['dp'] = $this->update_data($data, $set);
		// $act['status_so'] = $this->update_data($status, $set);
		$act['status_quo'] = $this->m_master_quotation->ubah_stat('process_lpo', $set['kd_msalesorder']);
		if ($act) :
			$log_stat = 'berhasil menambahkan DP';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$log_stat.'!');
		else :
			$log_stat = 'gagal menambahkan DP';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', 'Gagal '.$log_stat.'!');
		endif;
		$this->m_builder->write_log($log_stat, 'menambahkan', $log);

		return $str;
	}

	public function update_data($data = '', $where = '') {
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s'));
		$submit = array_merge($data, $data_adm);
		$aksi = $this->db->update($this->tbl_name, $submit, $where);
		return $aksi;
	}

	public function hapus_dp($kd_msalesorder = '') {
		$act = $this->db->update($this->tbl_name, array('jml_dp' => '0', 'jml_ppn_dp' => '0', 'no_invoice_dp' => ''), array($this->p_key => $kd_msalesorder));
		if ($act) :
			$log_stat = 'Berhasil hapus DP!';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', $log_stat);
		else :
			$log_stat = 'Gagal hapus DP!';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Berhasil!', $log_stat);
		endif;
		$this->m_builder->write_log($log_stat, 'menghapus', array('jml_dp' => '0', 'jml_ppn_dp' => '0', 'no_invoice_dp' => '', $this->p_key => $kd_msalesorder));
		return $str;
	}

	public function get_nosalesorder($kd_msalesorder = '') {
		$this->db->select('no_salesorder')
			->from($this->tbl_name)
			->where(array($this->p_key => $kd_msalesorder));
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			return $row->no_salesorder;
		else :
			return '';
		endif;
	}

	/*
	** Fungsi ini digunakan untuk mendapatkan data kode salesorder yang bukan additional,
	** data ini digunakan untuk mengupdate data pada db_caterlindo pada tabel tm_po
	*/
	public function get_master_so($no_salesorder = '') {
		$this->db->select($this->p_key)
			->from($this->tbl_name)
			->where(array('no_salesorder' => $no_salesorder, 'order_tipe' => 'new'));
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			return $row->{$this->p_key};
		else :
			return '';
		endif;
	}

	/*
	** Fungsi ini digunakan untuk cek status so
	*/
	public function chk_stat_so($kd_msalesorder = '', $status_so = '') {
		$this->db->select('status_so')
			->from($this->tbl_name)
			->where(array('kd_msalesorder' => $kd_msalesorder, 'status_so' => $status_so));
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			return TRUE;
		else :
			return FALSE;
		endif;
	}

	public function form_rules_tgl_kirim() {
		$rules = array(
			array('field' => 'txtKdMso', 'label' => 'Kode Master SO', 'rules' => 'required'),
			array('field' => 'txtTglKirim', 'label' => 'Tgl Kirim', 'rules' => 'required|alpha_dash'),
			array('field' => 'txtTglSelesai', 'label' => 'Tgl Selesai', 'rules' => 'required|alpha_dash'),
		);
		return $rules;
	}

	public function build_warning_tgl_kirim($datas = '') {
		$forms = array('txtKdMso', 'txtTglKirim', 'txtTglStuff');
		foreach ($datas as $key => $data) :
			$str[$data] = (!empty(form_error($forms[$key])))?buildLabel('warning', form_error($forms[$key], '"', '"')):'';
		endforeach;
		return $str;
	}

	public function update_tgl_kirim($datas = '') {
		$act = $this->update_data(array('tgl_kirim' => $datas['tgl_kirim'], 'tgl_selesai' => $datas['tgl_selesai']), array('kd_msalesorder' => $datas['kd_msalesorder']));
		if ($act) :
			$log_stat = 'berhasil mengubah tanggal kirim';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$log_stat.'!');
		else :
			$log_stat = 'gagal mengubah tanggal kirim';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', 'Gagal '.$log_stat.'!');
		endif;
		$this->m_builder->write_log($log_stat, 'mengubah', $datas);

		return $str;
	}

	public function get_unfinish_mso($kd_mso = '') {
		$this->db->from($this->tbl_name.' a')
			->select('a.kd_msalesorder, a.tgl_stuffing, a.tgl_kirim, a.tipe_customer, a.no_salesorder, a.no_po')
			->join('tm_deliveryorder b', 'b.msalesorder_kd = a.kd_msalesorder', 'left')
			->order_by('IF(a.tgl_stuffing IS NOT NULL, a.tgl_stuffing, a.tgl_kirim) ASC');
		if (!empty($kd_mso)) :
			$this->db->where_in('a.kd_msalesorder', $kd_mso);
		else :
			$this->db->where_not_in('b.status_do', array('finish', 'cancel'))
				->or_where_not_in('a.status_so', array('finish', 'cancel'));
		endif;
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	// select dengan return result sesuai kebutuhan 1 table
	public function getResult($select, $where=array(), $order_by, $group_by, $join=array()) {
		$this->db->select($select);
		$this->db->from($this->tbl_name.' as a');
		if(!empty($where)):
			$this->db->where($where);
		endif;
		if(!empty($join)):
			foreach ($join as $eachJoin):
				$this->db->join($eachJoin['joinTable'], $eachJoin['joinOn'], $eachJoin['joinStatus']);
			endforeach;
		endif;
		if (!empty($order_by)):
			$this->db->order_by($order_by);
		endif;
		if (!empty($group_by)):
			$this->db->group_by($grup_by);
		endif;
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	/** Update sales order where in */
	public function update_wherein($where, $data){
		$this->db->where_in('kd_msalesorder',$where);
		$aksi = $this->db->update($this->tbl_name, $data);
		return $aksi;
	}

	/** Get mquotation_kd */
	public function get_wherein ($where) {
		$this->db->where_in('kd_msalesorder', $where);
		$aksi = $this->db->get($this->tbl_name);
		return $aksi;
	}

	/** Untuk menampilakn sales order yang akan dikirim 
	 * di finishgood barang keluar; dengan kondisi 
	 * (1)status SO; (2)deliveryorder pending */
	public function get_where_param($param=[], $param2=[], $term) {
		$this->db->select('a.*, b.nm_customer');
		$this->db->from($this->tbl_name.' as a');
		$this->db->join('tm_customer as b', 'a.customer_kd=b.kd_customer', 'left');
		$this->db->join('tm_deliveryorder as c', 'a.kd_msalesorder=c.msalesorder_kd', 'left');
		$this->db->where($param);
		$this->db->or_where($param2);
		$this->db->or_where('c.status_do', 'pending');
		$act = $this->db->get();
		return $act;
	}

	/** Untuk rekap berdasarkan std/custom jumlah item untuk rekap finishgood out */
	public function get_rekap_itemstatus_by_nosalesorder($kd_msalesorder) {
		/** Item*/
		$itemStd = $this->db->select('a.kd_msalesorder, a.no_salesorder, b.barang_kd, b.item_barcode, b.item_code, b.item_status, SUM(b.item_qty) AS qty')
					->from('tm_salesorder AS a ')
					->join('td_salesorder_item AS b', 'a.kd_msalesorder=b.msalesorder_kd', 'left')
					->where('a.kd_msalesorder', $kd_msalesorder)
					->where('b.item_status', 'std')
					->group_by('b.barang_kd')
					->get_compiled_select();
		$itemCustom = $this->db->select('a.kd_msalesorder, a.no_salesorder, b.barang_kd, "899000" AS item_barcode, b.item_code, b.item_status, SUM(b.item_qty) AS qty')
					->from('tm_salesorder AS a ')
					->join('td_salesorder_item AS b', 'a.kd_msalesorder=b.msalesorder_kd', 'left')
					->where('a.kd_msalesorder', $kd_msalesorder)
					->where('b.item_status', 'custom')
					->group_by('b.barang_kd')
					->get_compiled_select();
		/** Item Detail*/
		$itemDetailStd = $this->db->select('a.kd_msalesorder, a.no_salesorder, b.kd_child AS barang_kd, b.item_barcode, b.item_code, b.item_status, SUM(b.item_qty) AS qty')
					->from('tm_salesorder AS a ')
					->join('td_salesorder_item_detail AS b', 'a.kd_msalesorder=b.msalesorder_kd', 'left')
					->where('a.kd_msalesorder', $kd_msalesorder)
					->where('b.item_status', 'std')
					->group_by('b.kd_child')
					->get_compiled_select();
		$itemDetailCustom = $this->db->select('a.kd_msalesorder, a.no_salesorder,  b.kd_child AS barang_kd, "899000" AS item_barcode, b.item_code, b.item_status, SUM(b.item_qty) AS qty')
					->from('tm_salesorder AS a ')
					->join('td_salesorder_item_detail AS b', 'a.kd_msalesorder=b.msalesorder_kd', 'left')
					->where('a.kd_msalesorder', $kd_msalesorder)
					->where('b.item_status', 'custom')
					->group_by('b.kd_child')
					->get_compiled_select();
		$query = ($itemStd . ' UNION ALL ' . $itemCustom. ' UNION ALL ' .$itemDetailStd. ' UNION ALL ' .$itemDetailCustom);
		$queryRekap = $this->db->select('t.kd_msalesorder, t.no_salesorder, t.barang_kd, t.item_barcode, t.item_code, t.item_status, SUM(qty) AS qty, b.deskripsi_barang, b.dimensi_barang, c.nm_group')
					->from('('.$query.') as t')
					->join('tm_barang AS b', 't.barang_kd = b.kd_barang', 'left')
					->join('tm_group_barang AS c', 'c.kd_group_barang = b.group_barang_kd', 'left')
					->group_by('t.barang_kd')
					->get_compiled_select();
		$queryJoinGrouping = $this->db->select('tr.*, pg.kd_mgrouping')
					->from('('.$queryRekap. ') as tr')
					->join('tm_product_grouping AS pg', 'tr.barang_kd=pg.barang_kd', 'left')
					->get_compiled_select();
		$act = $this->db->query($queryJoinGrouping)->result_array();

		return $act;
	}

	public function get_by_id($kd_msalesorder){
		$act = $this->db->select($this->tbl_name.'.*, tm_customer.*')
						->from($this->tbl_name)
						->join('tm_customer', $this->tbl_name.'.customer_kd=tm_customer.kd_customer')
						->where($this->tbl_name.'.kd_msalesorder', $kd_msalesorder)
						->get();
		return $act;
	}

	public function get_by_status($param=[]){
		$query = $this->db->select('a.*, b.nm_customer')
					->from($this->tbl_name.' as a')
					->join('tm_customer as b', 'a.customer_kd=b.kd_customer', 'left')
					->where_in('a.status_so', $param)
					->order_by('IF(a.tgl_stuffing IS NOT NULL, a.tgl_stuffing, a.tgl_kirim) ASC')
					->get();
		return $query;
	}

	public function get_by_param_master ($param=[]) {
		$query = $this->db->select($this->tbl_name.'.*, tm_customer.*')
					->from($this->tbl_name)
					->join('tm_customer', $this->tbl_name.'.customer_kd=tm_customer.kd_customer', 'left')
					->where($param)
					->get();
		return $query;
	}

	public function ubah_status_so ($kd_mso, $status) {

		/** Transaction dipindah di controllernya */

		// $this->load->model(array('m_msalesorder', 'model_salesorder'));
		// if ($status == 'pending') :
		// 	$stat_quo = 'process_so';
		// else :
		// 	$stat_quo = $status;
		// endif;
		
		// $master_det = $this->m_msalesorder->read_masterdata($kd_mso);
		// $kd_mquo = $master_det->mquotation_kd;
		
		
		// $this->db->trans_start();
		// $actQuo = $this->model_salesorder->ubah_status_quo($kd_mquo, $stat_quo);
		// $actSO = $this->db->update($this->tbl_name, ['status_so' => $status], ['kd_msalesorder' => $kd_mso]);
		// $this->db->trans_complete();
		// $resp = true;
		// if ($this->db->trans_status() === FALSE) {
		// 	$resp = false;
		// }
		// return $resp;


		$this->load->model(array('m_msalesorder', 'model_salesorder'));
		if ($status == 'pending') :
			$stat_quo = 'process_so';
		else :
			$stat_quo = $status;
		endif;
		
		$master_det = $this->m_msalesorder->read_masterdata($kd_mso);
		$kd_mquo = $master_det->mquotation_kd;

		$actQuo = $this->model_salesorder->ubah_status_quo($kd_mquo, $stat_quo);
		$actSO = $this->db->update($this->tbl_name, ['status_so' => $status], ['kd_msalesorder' => $kd_mso]);
		if($actSO):
			$resp = true;
		else:
			$resp = false;
		endif;
		return $resp;
	}

	public function get_all_where($conds = '') {
		$this->db->from($this->tbl_name)
			->where($conds)
			->order_by('no_salesorder DESC');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

}