<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class pr_has_ticket extends CI_Model
{
	private $tbl_name = 'pr_has_ticket';
	private $p_key = 'pht_kd';

	public function insert_data($data)
	{
		$query = $this->db->insert($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_data($id)
	{
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		return $query ? TRUE : FALSE;
	}

	public function get_by_param($param = [])
	{
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all()
	{
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data($aWhere = [], $data)
	{
		$query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query ? TRUE : FALSE;
	}

	public function insert_batch_data($data = [])
	{
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_by_param($param = [])
	{
		$query = $this->db->delete($this->tbl_name, $param);
		return $query ? TRUE : FALSE;
	}

	public function get_where_in($where, $in = [])
	{
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}

	public function get_by_param_detail($param = [])
	{
		$query = $this->db->select()
			->from($this->tbl_name)
			->join('tm_salesorder', $this->tbl_name . '.kd_msalesorder=tm_salesorder.kd_msalesorder', 'left')
			->join('tm_workorder', $this->tbl_name . '.wo_kd=tm_workorder.wo_kd', 'left')
			->where($param)
			->get();
		return $query;
	}

}
