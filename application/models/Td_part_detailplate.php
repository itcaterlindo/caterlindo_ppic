<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_part_detailplate extends CI_Model {
	private $tbl_name = 'td_part_detailplate';
	private $p_key = 'partdetailplate_kd';

	public function ssp_table($partdetail_kd) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'a.partdetailplate_jenis', 
				'dt' => 2, 'field' => 'partdetailplate_jenis',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.partdetailplate_panjang', 
				'dt' => 3, 'field' => 'partdetailplate_panjang',
				'formatter' => function ($d , $row){
					$d = round($d).' x '.round($row[9]);
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.partdetailplate_qty', 
				'dt' => 4, 'field' => 'partdetailplate_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.partdetailplate_keterangan', 
				'dt' => 5, 'field' => 'partdetailplate_keterangan',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'a.partdetailplate_tebal', 
				'dt' => 6, 'field' => 'partdetailplate_tebal',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.partdetailplate_massajenis', 
				'dt' => 7, 'field' => 'partdetailplate_massajenis',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.partdetailplate_qty', 
				'dt' => 8, 'field' => 'partdetailplate_qty',
				'formatter' => function ($d, $row){
					$d = generate_massa_plate($row[2], $row[9], $row[5], $row[6]) * $d;
					$d = round($d, 3);
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.partdetailplate_tglinput', 
				'dt' => 9, 'field' => 'partdetailplate_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.partdetailplate_lebar', 
				'dt' => 10, 'field' => 'partdetailplate_lebar',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." as a";
		$data['where'] = "a.partdetail_kd= '".$partdetail_kd."'";
		
		return $data;
	}

	public function ssp_tablecosting($partdetail_kd) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btncosting($d);
				}),
			array( 'db' => 'a.partdetailplate_jenis', 
				'dt' => 2, 'field' => 'partdetailplate_jenis',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.partdetailplate_panjang', 
				'dt' => 3, 'field' => 'partdetailplate_panjang',
				'formatter' => function ($d , $row){
					$d = $d.' x '.$row[10];
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.partdetailplate_keterangan', 
				'dt' => 4, 'field' => 'partdetailplate_keterangan',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'a.partdetailplate_qty', 
				'dt' => 5, 'field' => 'partdetailplate_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.partdetailplate_qty', 
				'dt' => 6, 'field' => 'partdetailplate_qty',
				'formatter' => function ($d, $row){
					$d = generate_massa_plate($row[2], $row[10], $row[11], $row[12]);
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.partdetailplate_hargaunit', 
				'dt' => 7, 'field' => 'partdetailplate_hargaunit',
				'formatter' => function ($d){
					$d = custom_numberformat($d, 2, 'IDR');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.partdetailplate_hargatotal', 
				'dt' => 8, 'field' => 'partdetailplate_hargatotal',
				'formatter' => function ($d){
					$d = custom_numberformat($d, 2, 'IDR');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.partdetailplate_hargaupdate', 
				'dt' => 9, 'field' => 'partdetailplate_hargaupdate',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.partdetailplate_tglinput', 
				'dt' => 10, 'field' => 'partdetailplate_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.partdetailplate_lebar', 
				'dt' => 11, 'field' => 'partdetailplate_lebar',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.partdetailplate_tebal', 
				'dt' => 12, 'field' => 'partdetailplate_tebal',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.partdetailplate_massajenis', 
				'dt' => 13, 'field' => 'partdetailplate_massajenis',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." as a";
		$data['where'] = "a.partdetail_kd= '".$partdetail_kd."'";
		
		return $data;
	}

	private function tbl_btn($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btns = array();
		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data_detailplate(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data_detailplate(\''.$id.'\')'));	
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	private function tbl_btncosting($id) {
		$btns = array();
		// $btns[] = get_btn(array('title' => 'Edit Harga', 'icon' => 'pencil', 'onclick' => 'edit_harga_detailplate(\''.$id.'\')'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
		$this->db->select('MAX('.$this->p_key.') AS code')
			->from($this->tbl_name);
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = (int) substr($code, -10);
		endif;
		$angka = $urutan + 1;
		$primary = 'PPRT'.str_pad($angka, 10, '0', STR_PAD_LEFT);
		return $primary;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }

	public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

	public function insert_batch_data ($data=[]){
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}

	public function get_by_in ($key, $param=[]) {
		$query = $this->db->select($this->tbl_name.'.*, tm_rawmaterial.*')
				->from($this->tbl_name)
				->join('tm_rawmaterial', $this->tbl_name.'.rm_kd=tm_rawmaterial.rm_kd', 'left')
				->where_in($key,$param)
				->get();
		return $query;
	}

	public function get_by_param_detail ($param=[]) {
		$query = $this->db->select($this->tbl_name.'.*, tm_rawmaterial.*')
				->from($this->tbl_name)
				->join('tm_rawmaterial', $this->tbl_name.'.rm_kd=tm_rawmaterial.rm_kd', 'left')
				->where($param)
				->get();
		return $query;
	}

	public function update_harga ($partdetail_kd) {
		$resp = false;
		$hargakg = 0;
		$partdetailplate_hargatotal = 0;
		$arrayUpdate = [];
		$massaPlate = 0;
		$partdetail = $this->db->select()
						->from('td_part_detail')
						->join('tm_rawmaterial', 'td_part_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
						->join('td_rawmaterial_harga', 'td_part_detail.rm_kd=td_rawmaterial_harga.rm_kd', 'left')
						->where('td_part_detail.partdetail_kd', $partdetail_kd)
						->get()
						->row_array();
		$partdetailplates = $this->get_by_param(['partdetail_kd' => $partdetail_kd])->result_array();

		/** Hitung harga per Kg */
		$cutSize = generate_massa_plate($partdetail['rm_platepanjang'], $partdetail['rm_platelebar'], $partdetail['rm_platetebal'], $partdetail['rm_platemassajenis']);
		if (!empty($cutSize)) {
			$hargakg = round($partdetail['partdetail_hargaunit'] / $cutSize, 2);
		}

		if (!empty($partdetailplates)){
			foreach ($partdetailplates as $rPlate) {
				$massaPlate = generate_massa_plate($rPlate['partdetailplate_panjang'], $rPlate['partdetailplate_lebar'], $rPlate['partdetailplate_tebal'], $rPlate['partdetailplate_massajenis']);
				$partdetailplate_hargatotal = $hargakg * $massaPlate * $rPlate['partdetailplate_qty'];
				$partdetailplate_hargatotal = round($partdetailplate_hargatotal, 3);
				$arrayUpdate[] = [
					'partdetailplate_kd' => $rPlate['partdetailplate_kd'],
					'partdetailplate_hargaunit' => $hargakg,
					'partdetailplate_hargatotal' => $partdetailplate_hargatotal,
					'partdetailplate_hargaupdate' => date('Y-m-d H:i:s'),
				];
			}
			$resp = $this->db->update_batch($this->tbl_name, $arrayUpdate, 'partdetailplate_kd');
		}else{
			$resp = false;
		}

		return $resp;
	}

}