<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_setting extends CI_Model {
	private $tbl_name = 'tb_setting';
	private $p_key = 'id';
	private $title_name = 'Data App Setting';

	public function get_all() {
		$this->db->from($this->tbl_name);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function submit_data($data = '') {
		$label = 'Mengubah '.$this->title_name;
		if (!empty($data)) :
			$jml_data = count($data['id']);
			for ($i = 0; $i < $jml_data; $i++) :
				$id = $data['id'][$i];
				$submit[] = array('id' => $id, 'val_setting' => $data['val_setting'][$id]);
			endfor;
			$act = $this->update_batch($submit, $this->p_key);
			$str = $this->report_batch($act, $label, $submit);
		else :
			$str = $this->report(0, $label, array('id' => 'null', 'nm_setting' => 'null', 'val_setting' => 'null'));
		endif;
		return $str;
	}

	private function create($data = '') {
		$act = $this->db->insert($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	private function update($data = '', $where = '') {
		$act = $this->db->update($this->tbl_name, $data, $where);
		return $act?TRUE:FALSE;
	}

	private function create_batch($data = '') {
		$act = $this->db->insert_batch($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	private function update_batch($data = '', $where = '') {
		$act = $this->db->update_batch($this->tbl_name, $data, $where);
		return $act?TRUE:FALSE;
	}

	public function report($act = '', $label = '', $data = '') {
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
		endif;
		$this->m_builder->write_log($stat, $label, $data);
		return $str;
	}

	public function report_batch($act = '', $label = '', $data = '') {
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
		endif;
		$this->m_builder->write_batch_log($stat, $label, $data);
		return $str;
	}
}