<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_permission extends CI_Model {
	private $tbl_name = 'tb_permission';
	private $p_key = 'permission_kd';

	public function create_code() {
        $query = $this->db->select('MAX('.$this->p_key.') as maxID')
                ->get($this->tbl_name)
                ->row();
        $code = (int) $query->maxID + 1;
        return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }

    public function get_all () {
        return $this->db->get($this->tbl_name);
	}
	
	public function cek_permission($kd_admin, $permission_nama) {
		$tipeAdmin = $this->db->where(['kd_admin' => $kd_admin])->get('tb_admin')->row_array();

		$cekAdmin = $this->db->join('tb_permission', 'td_admin_permission.permission_kd=tb_permission.permission_kd', 'left')
					->where(['td_admin_permission.kd_admin' => $kd_admin, 'tb_permission.permission_nama' => $permission_nama])
					->get('td_admin_permission')
					->num_rows();
		$cekTipeAdmin = $this->db->join('tb_permission', 'td_tipeadmin_permission.permission_kd=tb_permission.permission_kd', 'left')
					->where(['td_tipeadmin_permission.kd_tipe_admin' => $tipeAdmin['tipe_admin_kd'], 'tb_permission.permission_nama' => $permission_nama])
					->get('td_tipeadmin_permission')
					->num_rows();
					
		if (!empty($cekAdmin) || !empty($cekTipeAdmin)) {
			return true;	
		}else{
			return false;
		}
	}
}