<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_perbandinganharga_detail_katalog extends CI_Model {
	private $tbl_name = 'td_perbandinganharga_detail_katalog';
	private $p_key = 'perbandinganhargadetailkatalog_kd';

	public function create_code() {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_by_param_in($column, $data=[]) {
		$query = $this->db->where_in($column, $data)->get($this->tbl_name);
		return $query;
	}

	public function delete_by_param($param, $kode) {
		$query = $this->db->delete($this->tbl_name, array($param => $kode)); 
		return $query?TRUE:FALSE;
    }

}