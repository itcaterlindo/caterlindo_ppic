<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_part_lastversion extends CI_Model {
	private $tbl_name = 'tb_part_lastversion';
	private $p_key = 'partlastver_kd';

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }
    
    public function get_all_detail () {
        $query = $this->db->select('tm_part_main.*, td_part.*')
                ->from($this->tbl_name)
                ->join('tm_part_main', $this->tbl_name.'.partmain_kd=tm_part_main.partmain_kd', 'left ')
                ->join('td_part', $this->tbl_name.'.part_kd=td_part.part_kd', 'left ')
                ->get();
        return $query;
    }

}