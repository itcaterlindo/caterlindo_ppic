<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_finishgood_detail extends CI_Model {
	private $tbl_name = 'td_finishgood_detail';
	private $p_key = 'fgdetail_kd';

	public function __construct(){
		parent::__construct();
		$this->load->model(['tm_gudang', 'tm_rak' ,'td_rak_ruang']);
	}

	public function ssp_table($jnsFilter, $detilVal) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),

			array( 'db' => 'a.fgdetail_tglinput', 
				'dt' => 2, 'field' => 'fgdetail_tglinput',
				'formatter' => function ($d){
					$d = format_date($d, 'd-m-Y H:i:s');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'd.item_code', 
				'dt' => 3, 'field' => 'item_code',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.fgbarcode_barcode', 
				'dt' => 4, 'field' => 'fgbarcode_barcode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.fgbarcode_desc', 
				'dt' => 5, 'field' => 'fgbarcode_desc',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.fgdetail_qty', 
				'dt' => 6, 'field' => 'fgdetail_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.kd_rak_ruang', 
				'dt' => 7, 'field' => 'kd_rak_ruang',
				'formatter' => function ($d){
					$d = $this->get_lokasi($d);
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
			LEFT JOIN td_finishgood_in AS b ON a.fgin_kd=b.fgin_kd
			LEFT JOIN td_finishgood_barcode AS c ON b.fgbarcode_kd=c.fgbarcode_kd
			LEFT JOIN tm_finishgood AS d ON c.fg_kd=d.fg_kd";

		switch ($jnsFilter) {
			case '1':
				$data['where'] = "a.fgout_kd IS NULL GROUP BY c.fgbarcode_kd";
				break;
			case '2':
				$data['where'] = "a.kd_rak_ruang IS NULL AND a.fgout_kd IS NULL GROUP BY c.fgbarcode_kd";
				break;
			case '3':
				$data['where'] = "c.fgbarcode_barcode = '".$detilVal['barcode']."' AND a.fgout_kd IS NULL GROUP BY c.fgbarcode_kd";
				break;
			case '4':
				$data['where'] = "d.item_code = '".$detilVal['itemCode']."' AND a.fgout_kd IS NULL GROUP BY c.fgbarcode_kd";
				break;
			case '5':
				$data['where'] = "a.kd_rak_ruang = '".$detilVal['ruang']."' AND a.fgout_kd IS NULL GROUP BY c.fgbarcode_kd";
				break;
		}
		
		return $data;
	}

	private function tbl_btn($id) {
		// $read_access = $this->session->read_access;
		// $update_access = $this->session->update_access;
		// $delete_access = $this->session->delete_access;
		$btns = array();
		$btns[] = get_btn(array('title' => 'Edit Lokasi', 'icon' => 'pencil', 'onclick' => 'edit_lokasi(\''.$id.'\')'));
		// $btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'del_data(\''.$id.'\')'));
		
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function get_lokasi($kd_rak_ruang){
		if (!empty($kd_rak_ruang)){
			$act = $this->td_rak_ruang->get_byParam (array('kd_rak_ruang'=> $kd_rak_ruang))->row();
			if($act){
				$gdg = $act->gudang_kd;
				$actGdg = $this->tm_gudang->get_row($gdg);
				$gdg = $actGdg->nm_gudang;
				$rak = $act->rak_kd;
				$actRak = $this->tm_rak->get_row($rak);
				$rak = $actRak->nm_rak;
				$ruang = $act->nm_rak_ruang;
				$lokasi = $gdg.'/'.$rak.'/'.$ruang;
			}elseif($kd_rak_ruang == '-'){
				$lokasi = 'Kosong';
			}else{
				$lokasi = 'Tidak ditemukan';
			}
		}else{
			$lokasi = 'Kosong';
		}
		return $lokasi;
	}

    public function buat_kode () {
		// FDT190507000001
		$ident = 'FDT';
		$identtgl = substr(date('Y'), -2).date('m').date('d');

		$this->db->select($this->p_key);
		$this->db->where('DATE(fgdetail_tglinput)', date('Y-m-d'));
		$this->db->order_by('fgdetail_tglinput', 'DESC');
		$query = $this->db->get($this->tbl_name);
		if ($query->num_rows() == 0){
			$data = $ident.$identtgl.'000001';
		}else {
			$lastkode = $query->result_array();
			$lastkode = max($lastkode);
			$subs_laskode = substr($lastkode[$this->p_key], -6);
			$nextnumber = $subs_laskode + 1;
			$data = $ident.$identtgl.str_pad($nextnumber,6,"0",STR_PAD_LEFT);
		}
		return $data;
	}

    public function insert_data ($data){
        $query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
    }
    
    public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_byId($id){
		$this->db->where($this->p_key, $id);
		$act = $this->db->get($this->tbl_name);
		return $act;
    }
  	
	public function get_byParam ($param=[]) {
		$act = $this->db->select('a.*, b.*, c.*, d.*')
						->from($this->tbl_name. ' as a')
						->join('td_finishgood_in as b', 'a.fgin_kd=b.fgin_kd', 'left')
						->join('td_finishgood_barcode as c', 'b.fgbarcode_kd=c.fgbarcode_kd', 'left')
						->join('tm_finishgood as d', 'c.fg_kd=d.fg_kd', 'left')
						->where($param)
						->get();
		return $act;
	}

	public function delete_by_param($param = []) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

	public function get_lokasi_by_id ($id) {
		$this->db->select('a.*, b.*, c.*, d.*, e.*');
		$this->db->from($this->tbl_name.' as a');
		$this->db->join('td_rak_ruang as b', 'a.kd_rak_ruang=b.kd_rak_ruang', 'left');
		$this->db->join('td_finishgood_in as c', 'a.fgin_kd=c.fgin_kd', 'left');
		$this->db->join('td_finishgood_barcode as d', 'c.fgbarcode_kd=d.fgbarcode_kd', 'left');
		$this->db->join('tm_finishgood as e', 'd.fg_kd=d.fg_kd', 'left');
		$this->db->where('a.'.$this->p_key, $id);
		$this->db->group_by('d.fgbarcode_kd');
		$act = $this->db->get();
		return $act;
	}

	public function get_max_trans_by_fgkd($fg_kd){
		$act = $this->db->select('*')
						->from($this->tbl_name)
						->where('fg_kd', $fg_kd)
						->order_by('fgdetail_tglinput', 'desc')
						->order_by('fgdetail_kd', 'desc')
						->limit(1)
						->get();
		return $act;
	}
    
}