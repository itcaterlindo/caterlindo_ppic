<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_rawmaterial_stokopname_jenis extends CI_Model {
	private $tbl_name = 'td_rawmaterial_stokopname_jenis';
	private $p_key = 'rmstokopnamejenis_kd';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'a.rmstokopnamejenis_nama', 
				'dt' => 2, 'field' => 'rmstokopnamejenis_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.rmstokopnamejenis_tgledit', 
				'dt' => 3, 'field' => 'rmstokopnamejenis_tgledit',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." AS a";
        $data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btns = array();
		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));	
		$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));	
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

	public function create_no() { 
		$max = $this->db->select('MAX(rmin_no) as maxno')
				->from($this->tbl_name)
				->get();
		$no = 0;
		if ($max->num_rows() != 0){
			$max = $max->row_array();
			$max = $max['maxno'];
			$no = $max + 1;
		}else{
			$no = 1;
		}
		return $no;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	

	// public function get_by_param_detail($param = []){
	// 	$query = $this->db->select($this->tbl_name.'.*, tm_rawmaterial.*, td_rawmaterial_satuan.rmsatuan_nama')
	// 				->from($this->tbl_name)
	// 				->join('tm_rawmaterial', $this->tbl_name.'.rm_kd=tm_rawmaterial.rm_kd')
	// 				->join('td_rawmaterial_satuan', 'tm_rawmaterial.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd')
	// 				->where($param)
	// 				->get();
	// 	return $query;
    // }
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	// public function get_where_in ($where, $in = []){
	// 	$query = $this->db->where_in($where, $in)
	// 			->join('tm_purchaseorder', $this->tbl_name.'.po_kd=tm_purchaseorder.po_kd', 'left')
	// 			->select($this->tbl_name.'.*, tm_purchaseorder.*')
	// 			->get($this->tbl_name);
	// 	return $query;
	// }

	
}