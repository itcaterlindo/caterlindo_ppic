<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_workorder_item_detail extends CI_Model
{
	private $tbl_name = 'td_workorder_item_detail';
	private $p_key = 'woitemdetail_kd';

	public function insert_data($data)
	{
		$query = $this->db->insert($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_data($id)
	{
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		return $query ? TRUE : FALSE;
	}

	public function get_by_param($param = [])
	{
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all()
	{
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data($aWhere = [], $data)
	{
		$query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query ? TRUE : FALSE;
	}

	public function delete_by_param($param = [])
	{
		$query = $this->db->delete($this->tbl_name, $param);
		return $query ? TRUE : FALSE;
	}

	public function get_where_in($where, $in = [])
	{
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}

	public function insert_batch_data($data)
	{
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function buat_kode($code)
	{
		// CWO190507000001
		$ident = 'CWO';
		$identtgl = date('ymd');

		if (empty($code)) {
			$this->db->select($this->p_key);
			$this->db->where('DATE(woitemdetail_tglinput)', date('Y-m-d'));
			$this->db->order_by('woitemdetail_tglinput', 'DESC');
			$query = $this->db->get($this->tbl_name);
			if ($query->num_rows() == 0) {
				$code = $ident . $identtgl . '000000';
			} else {
				$code = $query->result_array();
				$code = max($code);
				$code = $code[$this->p_key];
			}
		}
		$subs_laskode = substr($code, -6);
		$nextnumber = (int) $subs_laskode + 1;
		$data = $ident . $identtgl . str_pad($nextnumber, 6, "0", STR_PAD_LEFT);

		return $data;
	}

	public function get_by_param_detail($param = [])
	{
		$query = $this->db->select($this->tbl_name . '.*, td_part.*, td_part_jenis.*')
			->from($this->tbl_name)
			->join('td_part', 'td_part.part_kd=' . $this->tbl_name . '.part_kd', 'left')
			->join('tm_part_main', 'tm_part_main.partmain_kd=td_part.partmain_kd', 'left')
			->join('td_part_jenis', 'td_part_jenis.partjenis_kd = tm_part_main.partjenis_kd', 'left')
			->where($param)
			->get();
		return $query;
	}

	public function get_wocustom_by_bagian($bagian_kd)
	{
		$query = $this->db->select($this->tbl_name . '.*')
			->from($this->tbl_name)
			->join('td_bom_detail', $this->tbl_name . '.bom_kd=td_bom_detail.bom_kd', 'left')
			->join('tm_part_main', 'td_bom_detail.partmain_kd=tm_part_main.partmain_kd', 'left')
			->join('tb_part_lastversion', 'tb_part_lastversion.partmain_kd=tm_part_main.partmain_kd', 'left')
			->join('td_part_detail', 'td_part_detail.part_kd = tb_part_lastversion.part_kd', 'left')
			->where([$this->tbl_name . '.woitemdetail_itemstatus' => 'custom', 'td_part_detail.bagian_kd' => $bagian_kd])
			->where($this->tbl_name . '.bom_kd IS NOT NULL')
			->group_by($this->tbl_name . '.' . $this->p_key)
			->get();
		return $query;
	}

	public function get_by_param_groupby_woitemso($param = [])
	{
		$query = $this->db->where($param)
			->select('woitemso_kd')
			->group_by('woitemso_kd')
			->get($this->tbl_name);
		return $query;
	}

	public function get_by_param_woitem($param = [])
	{
		$query = $this->db->select()
			->from($this->tbl_name)
			->join('td_workorder_item', $this->tbl_name . '.woitem_kd=td_workorder_item.woitem_kd', 'left')
			->where($param)
			->get();
		return $query;
	}

	/** Untuk generate part sebelum proses WO 
	 * step 1 yaitu mencari bom dan part dari woitem_so
	 * jika woitemso > 0
	 */
	public function insertBatch_woPart_step1($wo_kd)
	{
		$resp = false;
		$woitemparts = $this->db->join('td_workorder_item_detail', 'td_workorder_item_detail.woitemso_kd=td_workorder_item_so.woitemso_kd', 'left')
			->join('tm_bom', 'td_workorder_item_detail.bom_kd=tm_bom.bom_kd', 'left')
			->join('td_bom_detail', 'tm_bom.bom_kd=td_bom_detail.bom_kd', 'left')
			->join('tm_part_main', 'td_bom_detail.partmain_kd=tm_part_main.partmain_kd', 'left')
			->join('tb_part_lastversion', 'tm_part_main.partmain_kd=tb_part_lastversion.partmain_kd', 'left')
			->join('td_part_jenis', 'tm_part_main.partjenis_kd=td_part_jenis.partjenis_kd', 'left')
			->where('td_workorder_item_so.wo_kd', $wo_kd)
			->where('td_workorder_item_so.woitemso_status', 'std')
			->where('td_workorder_item_so.woitemso_prosesstatus', 'editing')
			->where('td_workorder_item_so.woitemso_qty >', 0)
			->get('td_workorder_item_so')
			->result();

		if (!empty($woitemparts)) {
			/** WO item detail */
			$arrayWorkorderitemdetail = [];
			$woitemdetail_kd = '';
			foreach ($woitemparts as $woitempart) {
				$woitemdetail_kd = $this->td_workorder_item_detail->buat_kode($woitemdetail_kd);
				$arrayWorkorderitemdetail[] = [
					'woitemdetail_kd' => $woitemdetail_kd,
					'wo_kd' => $wo_kd,
					'kd_barang' => $woitempart->kd_barang,
					'woitem_kd' => null,
					'woitemso_kd' => $woitempart->woitemso_kd,
					'part_kd' => $woitempart->part_kd,
					'partjenis_kd' => $woitempart->partjenis_kd,
					'bom_kd' => null,
					'woitemdetail_qty' => $woitempart->woitemso_qty,
					'woitemdetail_generatewo' => $woitempart->bomdetail_generatewo,
					'woitemdetail_wogrouping' => $woitempart->bomdetail_wogrouping,
					'woitemdetail_parent' => $woitempart->woitemdetail_kd,
					'woitemdetail_tglinput' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				];
			}


			$this->db->trans_begin();
			$actWOitemdetail = $this->td_workorder_item_detail->insert_batch_data($arrayWorkorderitemdetail);
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				$resp = false;
			} else {
				$this->db->trans_commit();
				$resp = true;
			}
		} else {
			/** Tidak diproses */
			$resp = true;
		}

		return $resp;
	}

	/** Detail Action :
	 * -> group by part, sum qty wo
	 * -> yg wogrouping F (false) akan dibuatkan no wo sendiri
	 */
	public function insertBatch_woPart_step2($wo_kd): bool
	{
		$woitempartdetails = $this->db->join('td_workorder_item_so', 'td_workorder_item_so.woitemso_kd=td_workorder_item_detail.woitemso_kd', 'left')
			->join('td_part', 'td_part.part_kd=td_workorder_item_detail.part_kd', 'left')
			->join('tm_part_main', 'tm_part_main.partmain_kd=td_part.partmain_kd', 'left')
			->where('td_workorder_item_detail.wo_kd', $wo_kd)
			->where('td_workorder_item_so.woitemso_status', 'std')
			->where('td_workorder_item_detail.woitem_kd IS NULL')
			->get('td_workorder_item_detail')->result_array();

		$arrayPartGroups = [];
		$arrayPartUngroups = [];
		$arrayPartUngenerateWOs = [];
		foreach ($woitempartdetails as $woitempartdetail) {
			if ($woitempartdetail['woitemdetail_generatewo'] == 'T') {
				if ($woitempartdetail['woitemdetail_wogrouping'] == 'T') {
					$arrayPartGroups[$woitempartdetail['part_kd']][] = $woitempartdetail;
				} else {
					$arrayPartUngroups[] = $woitempartdetail;
				}
			} else {
				// No genereate WO
				$arrayPartUngenerateWOs[] = $woitempartdetail;
			}
		}

		/** Group By part_kd */
		$woitem_kd = '';
		$arrayWorkorderitem = [];
		$arrayBatchUpdateDetail = [];
		foreach ($arrayPartGroups as $part_kds => $elements) {
			$woitemso_qty = 0;
			foreach ($elements as $element) {
				$woitemso_qty += $element['woitemso_qty'];
			}
			$woitem_kd = $this->td_workorder_item->buat_kode($woitem_kd);
			$arrayWorkorderitem[] = [
				'woitem_kd' => $woitem_kd,
				'wo_kd' => $wo_kd,
				'woitem_itemcode' => $elements[0]['partmain_nama'],
				'woitem_no_wo' => null,
				'woitem_qty' => $woitemso_qty,
				'woitem_jenis' => 'part',
				'woitem_note' => null,
				'woitem_satuan' => 'pcs',
				'woitem_status' => 'std',
				'woitem_tipe' => 'so',
				'woitem_tglselesai' => null,
				'woitem_prosesstatus' => 'workorder',
				'woitem_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			];

			/** Untuk update woitem_kd pada td_workorder_item_detail */
			foreach ($elements as $element2) {
				$arrayBatchUpdateDetail[] = [
					'woitemdetail_kd' => $element2['woitemdetail_kd'],
					'woitem_kd' => $woitem_kd,
				];
			}
		}

		/** UnGroup part_kd */
		foreach ($arrayPartUngroups as $arrayPartUngroup) {
			$woitem_kd = $this->td_workorder_item->buat_kode($woitem_kd);
			$arrayWorkorderitem[] = [
				'woitem_kd' => $woitem_kd,
				'wo_kd' => $wo_kd,
				'woitem_itemcode' => $arrayPartUngroup['partmain_nama'],
				'woitem_no_wo' => null,
				'woitem_qty' => $arrayPartUngroup['woitemso_qty'],
				'woitem_jenis' => 'part',
				'woitem_note' => null,
				'woitem_satuan' => 'pcs',
				'woitem_status' => 'std',
				'woitem_tipe' => 'so',
				'woitem_tglselesai' => null,
				'woitem_prosesstatus' => 'workorder',
				'woitem_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			];

			/** Untuk update woitem_kd pada td_workorder_item_detail */
			$arrayBatchUpdateDetail[] = [
				'woitemdetail_kd' => $arrayPartUngroup['woitemdetail_kd'],
				'woitem_kd' => $woitem_kd,
			];
		}

		if (!empty($arrayWorkorderitem)) {
			$actInsertBatch = $this->td_workorder_item->insert_batch_data($arrayWorkorderitem);
			if ($actInsertBatch) {
				if (!empty($arrayBatchUpdateDetail)) {
					$this->db->update_batch('td_workorder_item_detail', $arrayBatchUpdateDetail, 'woitemdetail_kd');
				}
			}
		}

		return true;
	}

	public function get_itemchild_by_parent($woitem_kd){
		$qRow = $this->db->where('woitem_kd', $woitem_kd)->get($this->tbl_name)->row_array();
		$qParts = $this->db->select("tm_part_main.partmain_nama, td_part.part_versi, {$this->tbl_name}.*")
			->join('td_part', "td_part.part_kd={$this->tbl_name}.part_kd",'left')
			->join('tm_part_main', "tm_part_main.partmain_kd=td_part.partmain_kd",'left')
			->where("{$this->tbl_name}.woitemdetail_parent", $qRow['woitemdetail_kd'])
			->where("{$this->tbl_name}.woitemdetail_generatewo <>", 'T')
			->get($this->tbl_name)->result_array();
		return $qParts;
	}

	public function get_by_param_detail_in($where, $in = [])
	{
		$query = $this->db->select($this->tbl_name . '.*, td_part.*, td_part_jenis.*, tm_part_main.partmain_nama')
			->from($this->tbl_name)
			->join('td_part', 'td_part.part_kd=' . $this->tbl_name . '.part_kd', 'left')
			->join('tm_part_main', 'tm_part_main.partmain_kd=td_part.partmain_kd', 'left')
			->join('td_part_jenis', 'td_part_jenis.partjenis_kd = tm_part_main.partjenis_kd', 'left')
			->where_in($where, $in)
			->get();
		return $query;
	}
}
