<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_salesorder_item_disc extends CI_Model {
	private $tbl_name = 'td_salesorder_item_disc';
	private $p_key = 'kd_default_disc';
	private $title_name = 'Data SO Default Disc';

	public function get_all_where($conds = '') {
		$this->db->from($this->tbl_name)
			->where($conds)
			->order_by('jml_disc DESC');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function get_where($conds = '') {
		$this->db->from($this->tbl_name)
			->where($conds);
		$query = $this->db->get();
		$result = $query->row();
		return $result;
	}

	public function get_where_array_so($mso_kd = '', $conds = '') {
		$this->db->from($this->tbl_name)
			->where_in('mso_kd', $mso_kd)
			->where($conds);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function select_distinct($kd_mso = '', $kd_jenis_customer = '') {
		$this->db->select('b.*')
			->from($this->tbl_name.' a')
			->join('tb_default_disc b', 'b.nm_pihak = a.nm_pihak', 'left')
			->where(array('a.mso_kd' => $kd_mso, 'b.jenis_customer_kd' => $kd_jenis_customer))
			->group_by('b.kd_default_disc');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function data_form_default_disc($so_item_kd = '', $kd_mso = '', $kd_jenis_customer = '') {
		$result = $this->get_all_where(array('so_item_kd' => $so_item_kd, 'mso_kd' => $kd_mso));
		$data = [];
		if (!empty($result)) :
			foreach ($result as $row) :
				$data[] = (object) array('kd_item_disc' => $row->kd_item_disc, 'item_kd' => $row->so_item_kd, 'sales_kd' => $row->mso_kd, 'item_relation' => $row->item_relation, 'nm_pihak' => $row->nm_pihak, 'jml_disc' => $row->jml_disc, 'type_individual_disc' => $row->type_individual_disc, 'jml_individual_disc' => $row->jml_individual_disc, 'flag_individual_disc' => $row->flag_individual_disc, 'view_access' => $row->view_access);
			endforeach;
		else :
			$result = $this->select_distinct($kd_mso, $kd_jenis_customer);
			if (!empty($result)) :
				foreach ($result as $row) :
					$data[] = (object) array('kd_item_disc' => '', 'item_kd' => $so_item_kd, 'sales_kd' => $kd_mso, 'item_relation' => '', 'nm_pihak' => $row->nm_pihak, 'jml_disc' => $row->jml_disc, 'type_individual_disc' => 'decimal', 'jml_individual_disc' => '0', 'flag_individual_disc' => $row->individual_disc, 'view_access' => $row->view_access);
				endforeach;
			endif;
		endif;
		return $data;
	}

	public function render_form_default_disc($so_item_kd = '', $kd_mso = '', $kd_jenis_customer = '') {
		$data = $this->data_form_default_disc($so_item_kd, $kd_mso, $kd_jenis_customer);
		if (empty($data)) :
			$this->load->model(array('tb_default_disc'));
			$result = $this->tb_default_disc->get_all($kd_jenis_customer);
			if (!empty($result)) :
				foreach ($result as $row) :
					if ($row->status == '1') :
						$data[] = (object) array('kd_item_disc' => '', 'item_kd' => '', 'sales_kd' => '', 'item_relation' => '', 'nm_pihak' => $row->nm_pihak, 'jml_disc' => $row->jml_disc, 'type_individual_disc' => 'decimal', 'jml_individual_disc' => '0', 'flag_individual_disc' => $row->individual_disc, 'view_access' => $row->view_access);
					endif;
				endforeach;
			endif;
		endif;

		$form = '';
		foreach ($data as $row) :
			$form .= '<hr class="col-xs-8">';
			$form .= '<div class="form-group">';
				$form .= '<label for="idTxtDisc'.ucwords($row->nm_pihak).'" class="col-md-4 control-label">Override Disc '.ucwords($row->nm_pihak).'</label>';
				$form .= '<div class="col-md-4">';
					$form .= '<div id="idErrDisc'.ucwords($row->nm_pihak).'"></div>';
					$form .= form_input(array('type' => 'hidden', 'name' => 'txtNmPihakDisc[]', 'value' => $row->nm_pihak));
					$form .= form_input(array('type' => 'hidden', 'name' => 'txtDiscViewAccess[]', 'value' => $row->view_access));
					$form .= form_input(array('name' => 'txtDefaultDisc[]', 'id' => 'idTxtDisc'.ucwords($row->nm_pihak), 'class' => 'form-control', 'value' => $row->jml_disc, 'placeholder' => 'Disc '.ucwords($row->nm_pihak)));
				$form .= '</div>';
			$form .= '</div>';
			if ($row->flag_individual_disc == '1') :
				$form .= '<div class="form-group">';
					$form .= '<label for="idSelIndividualDisc'.ucwords($row->nm_pihak).'" class="col-md-4 control-label">Individual Disc Type</label>';
					$form .= '<div class="col-md-4">';
						$form .= '<div id="idErrSelIndividualDisc'.ucwords($row->nm_pihak).'"></div>';
						$form .= form_dropdown('selIndividualDisc[]', array('percent' => 'Persen', 'decimal' => 'Harga'), $row->type_individual_disc, array('id' => 'idSelIndividualDisc'.ucwords($row->nm_pihak), 'class' => 'form-control'));
					$form .= '</div>';
				$form .= '</div>';
				$form .= '<div class="form-group">';
					$form .= '<label for="idTxtIndividualDiscVal'.ucwords($row->nm_pihak).'" class="col-md-4 control-label">Disc Value</label>';
					$form .= '<div class="col-md-4">';
						$form .= '<div id="idErrIndividualDiscVal'.ucwords($row->nm_pihak).'"></div>';
						$form .= form_input(array('name' => 'txtIndividualDiscVal[]', 'id' => 'idTxtIndividualDiscVal'.ucwords($row->nm_pihak), 'class' => 'form-control', 'value' => $row->jml_individual_disc, 'placeholder' => 'Disc Value'));
					$form .= '</div>';
				$form .= '</div>';
			else :
				echo form_input(array('type' => 'hidden', 'name' => 'selIndividualDisc[]', 'value' => $row->type_individual_disc));
				echo form_input(array('type' => 'hidden', 'name' => 'txtIndividualDiscVal[]', 'value' => $row->jml_individual_disc));
			endif;
			$form .= form_input(array('type' => 'hidden', 'name' => 'txtFlagIndividualDisc[]', 'value' => $row->flag_individual_disc));
		endforeach;
		return $form;
	}
}