<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_purchaseorder_detail extends CI_Model {
	private $tbl_name = 'td_purchaseorder_detail';
	private $p_key = 'podetail_kd';

	public function ssp_table($po_kd) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'b.rm_kode', 
				'dt' => 2, 'field' => 'rm_kode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
				array( 'db' => 'CONCAT (COALESCE(a.podetail_deskripsi,""), "/", COALESCE(a.podetail_spesifikasi,"")) as podetail_deskripsi', 
				'dt' => 3, 'field' => 'podetail_deskripsi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'a.podetail_tgldelivery', 
				'dt' => 4, 'field' => 'podetail_tgldelivery',
				'formatter' => function ($d){
					$d = format_date($d, 'd-m-Y');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.podetail_qty', 
				'dt' => 5, 'field' => 'podetail_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.rmsatuan_nama', 
				'dt' => 6, 'field' => 'rmsatuan_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.podetail_konversi', 
				'dt' => 7, 'field' => 'podetail_konversi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.podetail_harga', 
				'dt' => 8, 'field' => 'podetail_harga',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.podetail_konversicurrency', 
				'dt' => 9, 'field' => 'podetail_konversicurrency',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.podetail_tglinput', 
				'dt' => 10, 'field' => 'podetail_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." as a
							LEFT JOIN tm_rawmaterial as b ON a.rm_kd=b.rm_kd
							LEFT JOIN td_rawmaterial_satuan as c ON a.rmsatuan_kd=c.rmsatuan_kd";
		$data['where'] = "po_kd = '".$po_kd."'";
		
		return $data;
	}

	private function tbl_btn($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btns = array();
		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data_detail(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data_detail(\''.$id.'\')'));	
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
		$this->db->select($this->p_key.' AS code')
			->from($this->tbl_name)
			->where(array('DATE(podetail_tglinput)' => date('Y-m-d')))
			->order_by('podetail_tglinput DESC, '.$this->p_key.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -6);
		endif;
		$angka = $urutan + 1;
		$primary = 'DPO'.date('ymd').str_pad($angka, 6, '0', STR_PAD_LEFT);
		return $primary;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	

	public function get_by_param_detail($param = []){
		$query = $this->db->select($this->tbl_name.'.*, tm_rawmaterial.*, td_rawmaterial_satuan.rmsatuan_nama, td_purchaserequisition_detail.prdetail_kd, td_purchaserequisition_detail.prdetail_nama, td_purchaserequisition_detail.prdetail_deskripsi, td_purchaserequisition_detail.prdetail_spesifikasi, td_purchaserequisition_detail.prdetail_qty, td_purchaserequisition_detail.prdetail_duedate, td_purchaserequisition_detail.prdetail_remarks,
						tm_purchaserequisition.*, '.$this->tbl_name.'.rmsatuan_kd as podetail_rmsatuan_kd')
					->from($this->tbl_name)
					->join('tm_rawmaterial', $this->tbl_name.'.rm_kd=tm_rawmaterial.rm_kd', 'left')
					->join('td_rawmaterial_satuan', $this->tbl_name.'.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
					->join('td_purchaserequisition_detail', $this->tbl_name.'.prdetail_kd=td_purchaserequisition_detail.prdetail_kd', 'left')
					->join('tm_purchaserequisition', 'td_purchaserequisition_detail.pr_kd=tm_purchaserequisition.pr_kd', 'left')
					->where($param)
					->get();
		return $query;
    }
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_where_in ($where, $in = []) {
		$query = $this->db->where_in($where, $in)
				->join('tm_purchaseorder', $this->tbl_name.'.po_kd=tm_purchaseorder.po_kd', 'left')
				->join('tb_purchaseorder_status', 'tm_purchaseorder.postatus_kd=tb_purchaseorder_status.postatus_kd', 'left')
				->select($this->tbl_name.'.*, tm_purchaseorder.*, tb_purchaseorder_status.*')
				->get($this->tbl_name);
		return $query;
	}

	public function summaryOutstandingDetailByRM($rm_kd)
	{
		$query = $this->db->select('a.rm_kd,a.podetail_nama,a.podetail_deskripsi,SUM(a.podetail_qty) as podetail_qty')
				->from('td_purchaseorder_detail as a')
				->join('tm_purchaseorder as b', 'a.po_kd = b.po_kd', 'LEFT')
				->where(array('b.po_closeorder' => '0','a.rm_kd' => $rm_kd))
				->get()->row_array();
		return $query;
	}
	
}