<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_perbandinganharga extends CI_Model {
	private $tbl_name = 'tm_perbandinganharga';
	private $p_key = 'perbandinganharga_kd';
	private $r_detailPR;

	public function ssp_table() {
		$this->r_detailPR = $this->db->select('td_perbandinganharga_pr.perbandinganharga_kd, GROUP_CONCAT(tm_purchaserequisition.pr_no SEPARATOR ", ") as pr_no')
						->from('td_perbandinganharga_pr')
						->join('tm_purchaserequisition', 'td_perbandinganharga_pr.pr_kd=tm_purchaserequisition.pr_kd', 'left')
						->group_by('td_perbandinganharga_pr.perbandinganharga_kd')
						->get()->result_array();

		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'perbandinganharga_no', 
				'dt' => 2, 'field' => 'perbandinganharga_no',
				'formatter' => function ($d){

					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'perbandinganharga_no', 
				'dt' => 3, 'field' => 'perbandinganharga_no',
				'formatter' => function ($d, $row){
					foreach ($this->r_detailPR as $eachDetailPR) {
						if ($eachDetailPR['perbandinganharga_kd'] == $row[0]){
							$d = $eachDetailPR['pr_no'];
						}
					}
					$d = $this->security->xss_clean($d);
					return $d;
				}),
			array( 'db' => 'perbandinganharga_tglinput', 
				'dt' => 4, 'field' => 'perbandinganharga_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name;
        $data['where'] = "";
		
		return $data;
	}

	public function ssp_table2 ($aParam = []) {
		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		
		$data = [];
		$qData = $this->db->select($this->tbl_name.'.*, GROUP_CONCAT(tm_purchaserequisition.pr_no SEPARATOR ", ") as pr_no')
				->from($this->tbl_name)
				->join('td_perbandinganharga_pr', $this->tbl_name.'.perbandinganharga_kd=td_perbandinganharga_pr.perbandinganharga_kd', 'left')
				->join('tm_purchaserequisition', 'td_perbandinganharga_pr.pr_kd=tm_purchaserequisition.pr_kd', 'left');
		if (!empty($aParam['pr_no'])) {
			$qData = $qData->like('tm_purchaserequisition.pr_no', $aParam['pr_no'], 'match');
		}else{
			if (!empty($aParam['tahun'])){
				$qData = $qData->where('YEAR(tm_perbandinganharga.perbandinganharga_tglinput)', $aParam['tahun']);
			}
			if ($aParam['bulan'] != 'ALL'){
				$qData = $qData->where('MONTH(tm_perbandinganharga.perbandinganharga_tglinput)', $aParam['bulan']);
			}
		}
		$qData = $qData->group_by($this->tbl_name.'.'.$this->p_key)
				->get();
		foreach ($qData->result_array() as $r) {
			$data[] = [
				'1' => $this->tbl_btn($r['perbandinganharga_kd']),
				'2' => $r['perbandinganharga_no'],
				'3' => $r['pr_no'],
				'4' => $r['perbandinganharga_tglinput'],
				'5' => $r['perbandinganharga_tgledit'],
			];
		}
		
		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

	private function tbl_btn($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btns = array();
		if (cek_permission('PURCHASEPERBANDINGANHARGA_CETAK')) {
			$btns[] = get_btn(array('title' => 'Cetak Item', 'icon' => 'print', 'onclick' => 'cetak_data(\''.url_encrypt($id).'\')'));	
		}
		if (cek_permission('PURCHASEPERBANDINGANHARGA_CETAKBYSUPLIER')) {
			$btns[] = get_btn(array('title' => 'Cetak Item By Suplier', 'icon' => 'print', 'onclick' => 'cetak_data_bysuplier(\''.url_encrypt($id).'\')'));	
		}
		if (cek_permission('PURCHASEPERBANDINGANHARGA_UPDATE')) {
			$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));	
		}
		if (cek_permission('PURCHASEPERBANDINGANHARGA_DELETE')) {
			$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));	
		}

		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_no() {
		$ident = 'PHRG-';
		$identtgl = date('ymd').'-';

		$query =  $this->db->select('perbandinganharga_no')
				->where('DATE(perbandinganharga_tglinput)', date('Y-m-d'))
				->order_by('perbandinganharga_no', 'DESC')
				->get($this->tbl_name);
		if ($query->num_rows() == 0){
			$data = $ident.$identtgl.'001';
		}else {
			$lastkode = $query->row_array();
			$lastkode = $lastkode['perbandinganharga_no'];
			$subs_laskode = substr($lastkode, -3);
			$nextnumber = $subs_laskode + 1;
			$data = $ident.$identtgl.str_pad($nextnumber,3,"0",STR_PAD_LEFT);
		}
		return $data;
	}

	public function create_code() {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_by_param_in($column, $data=[]) {
		$query = $this->db->where_in($column, $data)->get($this->tbl_name);
		return $query;
	}

	public function delete_by_param($param, $kode) {
		$query = $this->db->delete($this->tbl_name, array($param => $kode)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param_detail ($param=[]) {
		$result = $this->db->select()
					->from($this->tbl_name)
					->join('td_perbandinganharga_pr', $this->tbl_name.'.perbandinganharga_kd=td_perbandinganharga_pr.perbandinganharga_kd', 'left')
					->join('tm_purchaserequisition', 'td_perbandinganharga_pr.pr_kd=tm_purchaserequisition.pr_kd', 'left')
					->join('td_perbandinganharga_detail', $this->tbl_name.'.perbandinganharga_kd=td_perbandinganharga_detail.perbandinganharga_kd', 'left')
					->where($param)
					->get();
		return $result;
	}
}