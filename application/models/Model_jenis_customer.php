<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Model_jenis_customer extends CI_Model {
	public function get_tipe($kd_customer = '', $var = '') {
		$column = $this->prep_column($var);
		$this->db->select('c.nm_select')
			->from('tm_customer a')
			->join('tb_jenis_customer b', 'b.kd_jenis_customer = a.jenis_customer_kd', 'left')
			->join('tb_set_dropdown c', 'c.id = b.'.$column, 'left')
			->where(array('a.kd_customer' => $kd_customer));
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			$tipe = $row->nm_select;
			return $tipe;
		else :
			return 'Lokal';
		endif;
	}

	public function get_col($kd_customer = '', $var = '') {
		$this->db->select($var)
			->from('tm_customer a')
			->join('tb_jenis_customer b', 'b.kd_jenis_customer = a.jenis_customer_kd', 'left')
			->where(array('a.kd_customer' => $kd_customer));
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			$tipe = $row->{$var};
			return $tipe;
		else :
			return '';
		endif;
	}

	private function prep_column($var = '') {
		if ($var == 'tipe_customer') :
			$col = 'kd_manage_items';
		elseif ($var == 'tipe_harga') :
			$col = 'kd_price_category';
		endif;
		return $col;
	}
}