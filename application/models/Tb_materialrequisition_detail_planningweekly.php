<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_materialrequisition_detail_planningweekly extends CI_Model
{
    private $tbl_name = 'tb_materialrequisition_detail_planningweekly';
    private $p_key = 'materialreqdetailwekekly_kd';

    public function insert_data($data)
    {
        $query = $this->db->insert($this->tbl_name, $data);
        return $query ? TRUE : FALSE;
    }

    public function delete_data($id)
    {
        $query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
        return $query ? TRUE : FALSE;
    }

    public function get_by_param($param = [])
    {
        $this->db->where($param);
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    public function get_all()
    {
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    public function update_data($aWhere = [], $data)
    {
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
        return $query ? TRUE : FALSE;
    }

    public function create_code()
    {
        $query = $this->db->select('MAX(' . $this->p_key . ') as maxID')
            ->get($this->tbl_name)
            ->row();
        $code = (int) $query->maxID + 1;
        return $code;
    }

    public function insert_batch($data)
    {
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
    }

    public function get_by_param_in($param, $params = [])
    {
        $this->db->where_in($param, $params);
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    public function get_by_param_detail($param)
    {
        return $this->db->where($param)
            ->join('tm_materialrequisition', "tm_materialrequisition.materialreq_kd={$this->tbl_name}.materialreq_kd", 'left')
            ->join('tm_planningweekly', "tm_planningweekly.planningweekly_kd={$this->tbl_name}.planningweekly_kd", 'left')
            ->join('tb_bagian', "tb_bagian.bagian_kd=tm_materialrequisition.bagian_kd", 'left')
            ->get($this->tbl_name);
    }

    /** For dropdown Get wo */
    public function getWObyMaterialReqDetail($materialreceipt_kd, $paramLike)
    {
        $qWos =  $this->db->select("{$this->tbl_name}.*, td_workorder_item.woitem_kd, td_workorder_item.woitem_no_wo, 
                td_workorder_item.woitem_itemcode, 
                td_materialrequisition_detail.materialreqdetail_qty")
            ->where("{$this->tbl_name}.materialreceipt_kd", $materialreceipt_kd);

        if (!empty($paramLike)) {
            $qWos = $qWos->like("td_workorder_item.woitem_no_wo", $paramLike)
                    ->or_like('td_workorder_item.woitem_itemcode');
        }
        $qWos = $qWos->join('tm_materialrequisition', "tm_materialrequisition.materialreq_kd={$this->tbl_name}.materialreq_kd", 'left')
            ->join('td_materialrequisition_detail', 'td_materialrequisition_detail.materialreq_kd=tm_materialrequisition.materialreq_kd', 'left')
            ->join('td_workorder_item', 'td_workorder_item.woitem_kd=td_materialrequisition_detail.woitem_kd', 'left')
            ->group_by('td_materialrequisition_detail.materialreqdetail_kd')
            ->get($this->tbl_name);
        
        return $qWos;
    }
}
