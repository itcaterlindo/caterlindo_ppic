<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Model_salesorder extends CI_Model {
	public function read_masterdata($kd_msalesorder) {
		$this->load->model(array('tb_tipe_harga', 'tm_currency', 'td_currency_convert'));

		$this->db->select('b.nm_salesperson, a.email_sales, a.mobile_sales, p.no_quotation, p.tgl_quotation, a.no_salesorder, a.tgl_so, a.tgl_kirim, a.tgl_stuffing, a.note_so, a.no_po, a.tgl_po, a.status_so, a.nm_kolom_ppn, a.jml_ppn, a.jml_ongkir, a.jml_install, a.jml_dp, a.jml_ppn_dp, a.no_invoice_dp, a.status_dp, a.order_tipe, c.jenis_customer_kd, c.nm_customer AS nm_customer, c.contact_person AS contact_customer, c.alamat AS alamat_customer, c.no_telp_utama AS telp_utama_customer, c.no_telp_lain AS telp_lain_customer, c.email AS email_customer, c.npwp_customer, d.nm_negara AS negara_customer, e.nm_provinsi AS provinsi_customer, f.nm_kota AS kota_customer, g.nm_kecamatan AS kecamatan_customer, h.nm_customer AS nm_kirim, h.contact_person AS contact_kirim, h.alamat AS alamat_kirim, h.no_telp_utama AS telp_utama_kirim, h.no_telp_lain AS telp_lain_kirim, h.email AS email_kirim, i.nm_negara AS negara_kirim, j.nm_provinsi AS provinsi_kirim, k.nm_kota AS kota_kirim, l.nm_kecamatan AS kecamatan_kirim, m.nm_select AS badan_usaha_customer, n.nm_select AS badan_usaha_kirim, o.kd_jenis_customer, o.term_payment_format, q.nm_select AS price_category, r.currency_kd, s.currency_type, s.icon_type, s.currency_icon, s.pemisah_angka, s.format_akhir, s.decimal, a.tipe_customer, a.tipe_container, o.custom_barcode, a.tgl_dp, c.kode_pos AS kode_pos_customer, h.kode_pos AS kode_pos_kirim, a.status_dp, c.code_customer');
		$this->db->from('tm_salesorder a');
		$this->db->join('tb_salesperson b', 'b.kd_salesperson = a.salesperson_kd', 'left');
		$this->db->join('tm_customer c', 'c.kd_customer = a.customer_kd', 'left');
		$this->db->join('tb_negara d', 'd.kd_negara = c.negara_kd', 'left');
		$this->db->join('tb_provinsi e', 'e.kd_provinsi = c.provinsi_kd AND e.negara_kd = c.negara_kd', 'left');
		$this->db->join('tb_kota f', 'f.kd_kota = c.kota_kd AND f.provinsi_kd = c.provinsi_kd AND f.negara_kd = c.negara_kd', 'left');
		$this->db->join('tb_kecamatan g', 'g.kd_kecamatan = c.kecamatan_kd AND g.kota_kd = c.kota_kd AND g.provinsi_kd = c.provinsi_kd AND g.negara_kd = c.negara_kd', 'left');
		$this->db->join('td_customer_alamat h', 'h.kd_alamat_kirim = a.alamat_kirim_kd', 'left');
		$this->db->join('tb_negara i', 'i.kd_negara = h.negara_kd', 'left');
		$this->db->join('tb_provinsi j', 'j.kd_provinsi = h.provinsi_kd AND j.negara_kd = h.negara_kd', 'left');
		$this->db->join('tb_kota k', 'k.kd_kota = h.kota_kd AND k.provinsi_kd = h.provinsi_kd AND k.negara_kd = h.negara_kd', 'left');
		$this->db->join('tb_kecamatan l', 'l.kd_kecamatan = h.kecamatan_kd AND l.kota_kd = h.kota_kd AND l.provinsi_kd = h.provinsi_kd AND l.negara_kd = h.negara_kd', 'left');
		$this->db->join('tb_set_dropdown m', 'm.id = c.kd_badan_usaha', 'left');
		$this->db->join('tb_set_dropdown n', 'n.id = h.kd_badan_usaha', 'left');
		$this->db->join('tb_jenis_customer o', 'o.kd_jenis_customer = c.jenis_customer_kd', 'left');
		$this->db->join('tm_quotation p', 'p.kd_mquotation = a.mquotation_kd', 'left');
		$this->db->join('tb_set_dropdown q', 'q.id = o.kd_price_category', 'left');
		$this->db->join('tb_tipe_harga r', 'r.nm_harga = q.nm_select', 'left');
		$this->db->join('tm_currency s', 's.kd_currency = r.currency_kd', 'left');
		$this->db->where(array('a.kd_msalesorder' => $kd_msalesorder));
		$this->db->limit(1);
		$query = $this->db->get();
		$row = $query->row();


		$retail_curr = $this->tb_tipe_harga->get_data('Harga Retail');
		$detail_retail = $this->tm_currency->get_data($retail_curr->currency_kd);

		if (!empty($row)) :
			/* --start model salesorder properties-- */
			$val_retail = '';
			$val_detail = '';
			if ($detail_retail->currency_type == 'secondary') :
				$val_retail = $this->td_currency_convert->get_value($retail_curr->currency_kd, $row->tgl_so);
			endif;
			if ($row->currency_type == 'secondary') :
				$val_detail = $this->td_currency_convert->get_value($row->currency_kd, $row->tgl_so);
			endif;

			$nm_customer = $row->badan_usaha_customer == 'Lainnya'?$row->nm_customer:$row->badan_usaha_customer.' '.$row->nm_customer;
			$negara_customer = $row->negara_customer.after_before_char($row->negara_customer, $row->negara_customer, '.', '');
			$provinsi_customer = $row->provinsi_customer.after_before_char($row->provinsi_customer, $row->negara_customer, ', ', '.');
			$kode_pos_customer = $row->kode_pos_customer.after_before_char($row->kode_pos_customer, $row->kode_pos_customer, ', ', '.');
			$kota_customer = $row->kota_customer.after_before_char($row->kota_customer, array($row->provinsi_customer, $row->negara_customer), ', ', '.');
			$kecamatan_customer = $row->kecamatan_customer.after_before_char($row->kecamatan_customer, array($row->kota_customer, $row->provinsi_customer, $row->negara_customer), ', ', '.');
			$alamat_dua_customer = $kecamatan_customer.$kota_customer.$provinsi_customer.$kode_pos_customer.$negara_customer;
			if (!empty($row->telp_utama_customer)) :
				$telp_customer = getrid_char(preg_replace('~[-_]~', '', $row->telp_utama_customer), '-', '4');
			elseif (!empty($row->telp_lain_customer)) :
				$telp_customer = getrid_char(preg_replace('~[-_]~', '', $row->telp_lain_customer), '-', '4');
			else :
				$telp_customer = '-';
			endif;
			$email_customer = !empty($row->email_customer)?$row->email_customer:'-';

			$nm_kirim = $row->badan_usaha_kirim == 'Lainnya'?$row->nm_kirim:$row->badan_usaha_kirim.' '.$row->nm_kirim;
			$negara_kirim = $row->negara_kirim.after_before_char($row->negara_kirim, $row->negara_kirim, '.', '');
			$provinsi_kirim = $row->provinsi_kirim.after_before_char($row->provinsi_kirim, $row->negara_kirim, ', ', '.');
			$kode_pos_kirim = $row->kode_pos_kirim.after_before_char($row->kode_pos_kirim, $row->kode_pos_kirim, ', ', '.');
			$kota_kirim = $row->kota_kirim.after_before_char($row->kota_kirim, array($row->provinsi_kirim, $row->negara_kirim), ', ', '.');
			$kecamatan_kirim = after_before_char($row->kecamatan_kirim, array($row->negara_kirim, $row->provinsi_kirim, $row->negara_kirim), ', ', '.');
			$alamat_dua_kirim = $kecamatan_kirim.$kota_kirim.$provinsi_kirim.$kode_pos_kirim.$negara_kirim;
			if (!empty($row->telp_utama_kirim)) :
				$telp_kirim = getrid_char(preg_replace('~[-_]~', '', $row->telp_utama_kirim), '-', '4');
			elseif (!empty($row->telp_lain_kirim)) :
				$telp_kirim = getrid_char(preg_replace('~[-_]~', '', $row->telp_lain_kirim), '-', '4');
			else :
				$telp_kirim = '-';
			endif;
			$email_kirim = !empty($row->email_kirim)?$row->email_kirim:'-';

			$ket_note_so = empty($row->note_so)?'empty':'available';
			$note_so = empty($row->note_so)?'-':'<p>'.$row->note_so.'</p>';

			$currency_icon = $row->icon_type.' '.$row->currency_icon.' '.$row->pemisah_angka.' '.$row->format_akhir;
			$decimal = !empty($row->decimal)?$row->decimal:'0';
			if ($row->tipe_customer == 'Ekspor') :
				$no_master_so = $row->no_po;
			elseif ($row->tipe_customer == 'Lokal') :
				$no_master_so = $row->no_salesorder;
			endif;
			$tgl_kirim = empty($row->tgl_kirim) || $row->tgl_kirim == '0000-00-00'?'0000-00-00':format_date($row->tgl_kirim, 'd M, Y');
			$tgl_stuffing = empty($row->tgl_stuffing) || $row->tgl_stuffing == '0000-00-00'?'0000-00-00':$row->tgl_stuffing;
			$tipe_container = !empty($row->tipe_container)?$row->tipe_container:'-';
			$order_tipe = !empty($row->order_tipe)?$row->order_tipe:'new';
			/* --end model salesorder properties-- */

			$data = array('kd_msalesorder' => $kd_msalesorder, 'no_invoice_dp' => $row->no_invoice_dp, 'jml_dp' => $row->jml_dp, 'jml_ppn_dp' => $row->jml_ppn_dp, 'status_dp' => $row->status_dp, 'custom_barcode' => $row->custom_barcode, 'type_retail' => $detail_retail->currency_type, 'val_retail' => $val_retail, 'val_detail' => $val_detail, 'nm_salesperson' => $row->nm_salesperson, 'telp_sales' => getrid_char($row->mobile_sales, '-', '3'), 'email_sales' => $row->email_sales, 'nm_customer' => $nm_customer, 'contact_customer' => $row->contact_customer, 'alamat_satu_customer' => $row->alamat_customer, 'alamat_dua_customer' => $alamat_dua_customer, 'telp_customer' => $telp_customer, 'email_customer' => $email_customer, 'nm_kirim' => $nm_kirim, 'contact_kirim' => $row->contact_kirim, 'alamat_satu_kirim' => ucwords(strtolower($row->alamat_kirim)), 'alamat_dua_kirim' => $alamat_dua_kirim, 'telp_kirim' => $telp_kirim, 'email_kirim' => $email_kirim, 'no_salesorder' => $row->no_salesorder, 'tgl_so' => format_date($row->tgl_so, 'd-m-Y'), 'tgl_kirim' => $tgl_kirim, 'tgl_stuffing' => $tgl_stuffing, 'note_so' => $note_so, 'ket_note_so' => $ket_note_so, 'no_po' => $row->no_po, 'tgl_po' => format_date($row->tgl_po, 'd-m-Y'), 'status_so' => $row->status_so, 'nm_kolom_ppn' => $row->nm_kolom_ppn, 'jml_ppn' => $row->jml_ppn, 'jml_ongkir' => $row->jml_ongkir, 'jml_install' => $row->jml_install, 'jenis_customer_kd' => $row->jenis_customer_kd, 'kd_jenis_customer' => $row->kd_jenis_customer, 'term_payment_format' => $row->term_payment_format, 'no_quotation' => $row->no_quotation, 'tgl_quotation' => format_date($row->tgl_quotation, 'd-m-Y'), 'tipe_customer' => $row->tipe_customer, 'price_category' => $row->price_category, 'currency_type' => $row->currency_type, 'currency_icon' => $currency_icon, 'decimal' => $decimal, 'no_master_so' => $no_master_so, 'tipe_container' => $tipe_container, 'order_tipe' => $order_tipe, 'npwp_customer' => $row->npwp_customer, 'tgl_dp' => $row->tgl_dp, 'code_customer' => $row->code_customer);
		else :
			$data = array('kd_msalesorder' => '', 'no_invoice_dp' => 'XX/TRIAL/INV', 'jml_dp' => '0', 'jml_ppn_dp' => '0', 'custom_barcode' => '890XXXX', 'type_retail' => '', 'val_retail' => '', 'val_detail' => '', 'nm_salesperson' => 'Trial Sales', 'telp_sales' => '-', 'email_sales' => '-', 'nm_customer' => 'Trial Customer', 'contact_customer' => 'Pak Dhe', 'alamat_satu_customer' => 'Nun Jauh Disana', 'alamat_dua_customer' => 'Kec. Tutut, Kota Gede, Jawawa, Endonesah', 'telp_customer' => '-', 'email_customer' => '-', 'nm_kirim' => 'Trial Nama Kirim', 'contact_kirim' => 'Pak Puh', 'alamat_satu_kirim' => 'Ada Disini', 'alamat_dua_kirim' => 'Distrik Listrik, Kota C, Ferhound, Asgard', 'telp_kirim' => 'Belum Ada Kabel Telpom', 'email_kirim' => 'Apalagi Email xD', 'no_salesorder' => 'XX-SO-ABCDEF', 'tgl_so' => '19-12-2017', 'tgl_kirim' => '18-01-2150', 'tgl_stuffing' => '18-01-2150', 'note_so' => 'Ini adalah trial report yang dihasilkan otomatis dari sistem.', 'ket_note_so' => 'Kasih keterangan apa ya..?', 'no_po' => '00-PO-QWERTY', 'tgl_po' => '15-12-1962', 'status_so' => 'trial', 'nm_kolom_ppn' => 'Bebas PPN', 'jml_ppn' => '0', 'jml_ongkir' => '0', 'jml_install' => '0', 'jenis_customer_kd' => '', 'kd_jenis_customer' => '', 'term_payment_format' => 'Tidak usah dibayar bossque :D', 'no_quotation' => '11-QUO-LIFFEA', 'tgl_quotation' => '04-07-1985', 'tipe_customer' => '', 'price_category' => '', 'currency_type' => '', 'currency_icon' => '', 'decimal' => '', 'no_master_so' => '', 'tipe_container' => '-', 'order_tipe' => 'new', 'npwp_customer' => '-', 'code_customer' => '');
		endif;
		return $data;
	}

	public function read_no_dps($kd_msalesorder = '') {
		$this->db->select('a.kd_so_termin, a.jml_termin, a.jml_termin_ppn, a.no_invoice, a.tgl_termin, a.status_dp')
			->from('td_so_termin a')
			->where(array('msalesorder_kd' => $kd_msalesorder));
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	private function create_sokey() {
		$this->db->select('kd_msalesorder AS code')
			->from('tm_salesorder')
			->where(array('DATE(tgl_input)' => date('Y-m-d')))
			->order_by('tgl_input DESC, kd_msalesorder DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -6);
		endif;
		$angka = $urutan + 1;
		$primary = 'MSO'.date('dmy').str_pad($angka, 6, '000', STR_PAD_LEFT);
		return $primary;
	}

	function create_no_so($tipe_customer = '') {
		// Dapatkan format no quotation
		if ($tipe_customer == 'Ekspor') :
			$code = 'salesorderekspor';
		elseif ($tipe_customer == 'Lokal' || empty($tipe_customer)) :
			$code = 'salesorderlocal';
			$tipe_customer = 'Lokal';
		endif;
		$no_salesorder = $this->tm_salesorder->create_nosalesorder($code, $tipe_customer);

		return $no_salesorder;
	}

	function create_kdparent($no = 0, $code = '') {
		if ($no == 0 && empty($code)) :
			$this->db->select('kd_ditem_so AS code')
				->from('td_salesorder_item')
				->where(array('DATE(tgl_input)' => date('Y-m-d')))
				->order_by('tgl_input DESC, kd_ditem_so DESC');
			$query = $this->db->get();
			$num = $query->num_rows();
			$urutan = 0;
			$row = $query->row();
			$code = empty($row)?'':$row->code;
		else :
			$num = 1;
			$code = $code;
		endif;
		if ($num > 0) :
			$urutan = substr($code, -3);
		endif;
		$angka = (int) $urutan + 1 ;
		// $angka = $urutan + 1 + $no;
		$primary = 'DSI'.date('dmy').str_pad($angka, 3, '000', STR_PAD_LEFT);
		return $primary;
	}

	function create_kdchild($no = 0, $code = '') {
		if ($no == 0 && empty($code)) :
	 		$this->db->select('kd_citem_so AS code')
				->from('td_salesorder_item_detail')
				->where(array('DATE(tgl_input)' => date('Y-m-d')))
				->order_by('tgl_input DESC, kd_citem_so DESC');
			$query = $this->db->get();
			$num = $query->num_rows();
			$urutan = 0;
			$row = $query->row();
			$code = empty($row)?'':$row->code;
		else :
			$num = 1;
			$code = $code;
		endif;
		if ($num > 0) :
			$urutan = substr($code, -3);
		endif;
		$angka = (int) $urutan + 1;
		// $angka = $urutan + 1 + $no;
		$primary = 'DSC'.date('dmy').str_pad($angka, 3, '000', STR_PAD_LEFT);
		return $primary;
	}

	function create_price_kd($no = 0, $code = '') {
		if ($no == 0 && empty($code)) :
	 		$this->db->select('kd_dharga_so AS code')
				->from('td_salesorder_harga')
				->where(array('DATE(tgl_input)' => date('Y-m-d')))
				->order_by('tgl_input DESC, kd_dharga_so DESC');
			$query = $this->db->get();
			$num = $query->num_rows();
			$urutan = 0;
			$row = $query->row();
			$code = empty($row)?'':$row->code;
		else :
			$num = 1;
			$code = $code;
		endif;
		if ($num > 0) :
			$urutan = substr($code, -3);
		endif;
		$angka = $urutan + 1 + $no;
		$primary = 'DSH'.date('dmy').str_pad($angka, 3, '000', STR_PAD_LEFT);
		return $primary;
	}

	public function send_item_data($data = '') {
		$this->load->model(array('m_quotation', 'tb_default_disc', 'm_salesorder', 'tm_project'));
		$data_customer = $this->tb_default_disc->get_all_bycustomer($_SESSION['sales_order']['customer_kd']);
		$item_parent = $this->m_quotation->get_parentitem_byitemkd($data['kd_parent']);
		$item_child = $this->m_quotation->get_childitem_byitemkd($data['kd_child']);
		/* ==setelah mendapatkan data parent item dan child item, maka tinggal kita fetch dan sesuaikan dengan kolom pada item so== */
		$data_master = array('kd_msalesorder' => '', 'mquotation_kd' => $_SESSION['sales_order']['kd_mquotation'], 'salesperson_kd' => $_SESSION['sales_order']['salesperson_kd'], 'email_sales' => $_SESSION['sales_order']['email_sales'], 'mobile_sales' => $_SESSION['sales_order']['mobile_sales'], 'no_salesorder' => '', 'tipe_so' => strtolower($_SESSION['sales_order']['tipe_customer']), 'customer_kd' => $_SESSION['sales_order']['customer_kd'], 'tipe_customer' => $_SESSION['sales_order']['tipe_customer'], 'tipe_harga' => $_SESSION['sales_order']['tipe_harga'], 'alamat_kirim_kd' => $_SESSION['sales_order']['alamat_kirim_kd'], 'status_so' => 'pending', 'nm_kolom_ppn' => $_SESSION['sales_order']['nm_kolom_ppn'], 'jml_ppn' => $_SESSION['sales_order']['jml_ppn'], 'jml_ongkir' => $_SESSION['sales_order']['jml_ongkir'], 'jml_install' => $_SESSION['sales_order']['jml_install'], 'order_tipe' => 'new', 'tgl_so' => date('Y-m-d'), 'tgl_input' => date('Y-m-d H:i:s'), 'admin_kd' => $this->session->kd_admin);
		$act_master = $this->send_data($data_master);

		if ($act_master != FALSE) :
			$disc_customer = 0;
			$disc_distributor = 0;
			if (!empty($data_customer)) :
				foreach ($data_customer as $customer) :
					if ($customer->nm_pihak == 'Customer' && $customer->status == '1') :
						$disc_customer = $customer->jml_disc;
					elseif ($customer->nm_pihak == 'Distributor' && $customer->status == '1') :
						$disc_distributor = $customer->jml_disc;
					endif;
				endforeach;
			endif;
			$insert_quo_disc = $this->insert_quo_discprices($_SESSION['sales_order']['kd_mquotation'], $act_master['kd_msalesorder']);
			if (!$insert_quo_disc) :
				$del_all = $this->delete_from_all($act_master['kd_msalesorder']);
				$str['confirm'] = 'error';
				$str['alert'] = buildAlert('danger', 'Gagal!', 'Gagal transfer data diskon harga quotation, kesalahan sistem!');
				return $str;
				exit();
			endif;
			$no_parent = 0;
			$no_child = 0;
			$temp_nopj = 0;
			if (!empty($item_parent)) :
				$kd_parent = '';
				$kd_child = '';
				$no_pj = '';
				$code = '';
				foreach ($item_parent as $parent) :
					$kd_parent = $this->create_kdparent($no_parent, $kd_parent);
					$no_parent++;
					/** Generate untuk item custom */
					if ($parent->item_status == 'custom'):
						$code = $this->tm_project->create_code($temp_nopj, $code);
						$no_pj = $this->tm_project->generate_nopj ($temp_nopj, $no_pj);
						$arrProject[] = array(
							'project_kd' => $code,
							'kd_msalesorder' => $act_master['kd_msalesorder'],
							'kd_ditem_so' => $kd_parent,	
							'kd_citem_so' => null,	
							'project_nopj' => $no_pj,
							'project_itemcode' => $parent->item_code,
							'project_itemdesc' => $parent->item_desc,
							'project_itemdimension' => $parent->item_dimension,
							'project_hargabarang' => $parent->harga_barang,
							'project_status' => 'pending',
							'project_tglinput' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin')
						);						
						$temp_nopj++;
					endif;
					$data_parent[] = array('msalesorder_kd' => $act_master['kd_msalesorder'], 'kd_ditem_so' => $kd_parent, 'barang_kd' => $parent->barang_kd, 'item_code' => $parent->item_code, 'item_barcode' => $parent->item_barcode, 'item_status' => $parent->item_status, 'item_desc' => $parent->item_desc, 'item_dimension' => $parent->item_dimension, 'netweight' => $parent->netweight, 'grossweight' => $parent->grossweight, 'boxweight' => $parent->boxweight, 'length_cm' => $parent->length_cm, 'width_cm' => $parent->width_cm, 'height_cm' => $parent->height_cm, 'item_cbm' => $parent->item_cbm, 'item_qty' => $parent->item_qty, 'harga_barang' => $parent->harga_barang, 'harga_retail' => $parent->harga_retail, 'disc_type' => $parent->disc_type, 'item_disc' => $parent->item_disc, 'disc_customer' => $disc_customer, 'disc_distributor' => $disc_distributor, 'total_harga' => $parent->total_harga, 'item_note' => $parent->item_note, 'admin_kd' => $this->session->kd_admin, 'tgl_input' => date('Y-m-d H:i:s'));

					if (!empty($item_child)) :
						foreach ($item_child as $child) :
							if ($child->ditem_quotation_kd == $parent->kd_ditem_quotation) :
								$kd_child = $this->create_kdchild($no_child, $kd_child);
								$no_child++;
								/** Generate untuk item custom */
								if ($child->item_status == 'custom'):
									$code = $this->tm_project->create_code($temp_nopj, $code);
									$no_pj = $this->tm_project->generate_nopj ($temp_nopj, $no_pj);
									$arrProject[] = array(
										'project_kd' => $code,
										'kd_msalesorder' => $act_master['kd_msalesorder'],
										'kd_ditem_so' => null,	
										'kd_citem_so' => $kd_child,	
										'project_nopj' => $no_pj,
										'project_itemcode' => $child->item_code,
										'project_itemdesc' => $child->item_desc,
										'project_itemdimension' => $child->item_dimension,
										'project_hargabarang' => $child->harga_barang,
										'project_status' => 'pending',
										'project_tglinput' => date('Y-m-d H:i:s'),
										'admin_kd' => $this->session->userdata('kd_admin')
									);						
									$temp_nopj++;
								endif;

								$data_child[] = array('msalesorder_kd' => $act_master['kd_msalesorder'], 'kd_citem_so' => $kd_child, 'ditem_so_kd' => $kd_parent, 'kd_parent' => $child->kd_parent, 'kd_child' => $child->kd_child, 'item_code' => $child->item_code, 'item_barcode' => $child->item_barcode, 'item_status' => $child->item_status, 'item_desc' => $child->item_desc, 'item_dimension' => $child->item_dimension, 'netweight' => $child->netweight, 'grossweight' => $child->grossweight, 'boxweight' => $child->boxweight, 'length_cm' => $child->length_cm, 'width_cm' => $child->width_cm, 'height_cm' => $child->height_cm, 'item_cbm' => $child->item_cbm, 'item_qty' => $child->item_qty, 'harga_barang' => $child->harga_barang, 'harga_retail' => $child->harga_retail, 'disc_type' => $child->disc_type, 'item_disc' => $child->item_disc, 'disc_customer' => $disc_customer, 'disc_distributor' => $disc_distributor, 'total_harga' => $child->total_harga, 'item_note' => $child->item_note, 'admin_kd' => $this->session->kd_admin, 'tgl_input' => date('Y-m-d H:i:s'));
							endif;
						endforeach;
					endif;
				endforeach;
				$act['parent'] = $this->insert_parent($data_parent);
				if (isset($data_child)) :
					$act['child'] = $this->insert_child($data_child);
				endif;
				if (isset($arrProject)) :
					$act['project'] = $this->tm_project->insert_batch($arrProject);
				endif;
			endif;
			if ($act) :
				$act['ubah_status'] = $this->ubah_status_quo($act_master['mquotation_kd'], 'process_so');
				if ($act) :
					$str['confirm'] = 'success';
					$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil transfer data quotation kedalam salesorder!');
				else :
					$del_all = $this->delete_from_all($act_master['kd_msalesorder']);
					$str['confirm'] = 'error';
					$str['alert'] = buildAlert('danger', 'Gagal!', 'Gagal mengubah status data quotation, kesalahan sistem!');
				endif;
			else :
				$del_all = $this->delete_from_all($act_master['kd_msalesorder']);
				$str['confirm'] = 'error';
				$str['alert'] = buildAlert('danger', 'Gagal!', 'Gagal menambahkan data item kesalesorder, kesalahan sistem!');
			endif;
		else :
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', 'Gagal menambahkan master salesorder, kesalahan sistem!');
		endif;
		return $str;
	}

	public function insert_parent($data = '') {
		$act = $this->db->insert_batch('td_salesorder_item', $data);
		return $act?TRUE:FALSE;
	}

	public function insert_child($data = '') {
		$act = $this->db->insert_batch('td_salesorder_item_detail', $data);
		return $act?TRUE:FALSE;
	}

	public function insert_master($data = '') {
		$act = $this->db->insert('tm_salesorder', $data);
		return $act?TRUE:FALSE;
	}

	public function update_master($data = '') {
		$act = $this->db->insert('tm_salesorder', $data);
		return $act?TRUE:FALSE;
	}

	public function send_data($data = '') {
		if (empty($data['kd_msalesorder'])) :
			$data['kd_msalesorder'] = $this->create_sokey();
			$data['no_salesorder'] = $this->create_no_so($data['tipe_customer']);
			$act_master = $this->insert_master($data);
		endif;
		return $act_master?$data:FALSE;
	}

	public function ubah_status_quo($kd_mquo = '', $status = '') {
		if (stripos($kd_mquo, '-') !== FALSE) :
			$kd_masters = explode('-', $kd_mquo);
			for ($i = 0; $i < count($kd_masters); $i++) :
				$kd_mquotation[] = $kd_masters[$i];
				$data[] = array('kd_mquotation' => $kd_masters[$i], 'status_quotation' => $status);
			endfor;
		else :
			$kd_mquotation = $kd_mquo;
			$data[] = array('kd_mquotation' => $kd_mquotation, 'status_quotation' => $status);
		endif;
		$act = $this->update_batch_quo($data, 'kd_mquotation');
		return $act?TRUE:FALSE;
	}

	public function insert_quo_discprices($kd_mquo = '', $kd_mso = '') {
		if (stripos($kd_mquo, '-') !== FALSE) :
			$kd_masters = explode('-', $kd_mquo);
			for ($i = 0; $i < count($kd_masters); $i++) :
				$kd_mquotation[] = $kd_masters[$i];
				$data[] = $kd_masters[$i];
			endfor;
		else :
			$kd_mquotation = $kd_mquo;
			$data = $kd_mquotation;
		endif;
		$data = $this->m_quotation->get_price($data);
		$price_no = 0;
		$kd_price = '';
		foreach ($data as $row) :
			$kd_price = $this->create_price_kd($price_no, $kd_price);
			$price_no++;
			$data_prices[] = array('kd_dharga_so' => $kd_price, 'msalesorder_kd' => $kd_mso, 'nm_kolom' => $row->nm_kolom, 'type_kolom' => $row->type_kolom, 'total_nilai' => $row->total_nilai, 'tgl_input' => date('Y-m-d'), 'admin_kd' => $_SESSION['kd_admin']);
		endforeach;
		if (isset($data_prices)) :
			$act = $this->db->insert_batch('td_salesorder_harga', $data_prices);
			return $act?TRUE:FALSE;
		else :
			return TRUE;
		endif;
	}

	public function update_batch_quo($data = '', $where = '') {
		$act = $this->db->update_batch('tm_quotation', $data, $where);
		return $act?TRUE:FALSE;
	}

	public function delete_from_all() {
		$act['master'] = $this->db->delete('tm_salesorder', array('kd_msalesorder' => $kd_mso));
		$act['item_parent'] = $this->db->delete('td_salesorder_item', array('msalesorder_kd' => $kd_mso));
		$act['item_child'] = $this->db->delete('td_salesorder_item_detail', array('msalesorder_kd' => $kd_mso));
		$act['harga'] = $this->db->delete('td_salesorder_harga', array('msalesorder_kd' => $kd_mso));
		return $act?TRUE:FALSE;
	}

	public function report_sales($tahun = null, $bulan = null)
	{
		$data = $this->db->select('a.no_salesorder,b.nm_salesperson,a.tgl_so,a.tgl_kirim,c.nm_customer,d.nm_jenis_customer,CONCAT_WS(";", a.no_invoice_dp,f.no_invoice) no_invoice,a.jml_ppn,COALESCE(a.jml_ongkir,0) jml_ongkir,COALESCE(a.jml_install,0) jml_install,
			e.item_standart_harga, e.item_custom_harga, e.total_nilai_disc,
			e.item_standart_qty,e.item_custom_qty')
		->from('tm_salesorder a')
		->join('tb_salesperson b', 'a.salesperson_kd = b.kd_salesperson', 'LEFT')
		->join('tm_customer c', 'a.customer_kd = c.kd_customer', 'LEFT')
		->join('tb_jenis_customer d', 'c.jenis_customer_kd = d.kd_jenis_customer', 'LEFT')
		->join('(SELECT a.msalesorder_kd, SUM(a.item_standart_qty) item_standart_qty, SUM(a.item_custom_qty) item_custom_qty, 
					SUM(a.item_standart_harga) item_standart_harga, SUM(a.item_custom_harga) item_custom_harga, 
					a.item_qty,
					COALESCE(b.total_nilai,0) total_nilai_disc, b.nm_kolom nm_kolom_disc
					FROM ( SELECT a.msalesorder_kd, SUM(a.total_harga) total_harga, SUM(a.item_qty) item_qty,
							SUM( CASE WHEN a.item_status = "std" THEN a.total_harga ELSE 0 END ) item_standart_harga,
							SUM( CASE WHEN a.item_status = "custom" THEN a.total_harga ELSE 0 END ) item_custom_harga,
							SUM( CASE WHEN a.item_status = "std" THEN a.item_qty ELSE 0 END ) item_standart_qty,
							SUM( CASE WHEN a.item_status = "custom" THEN a.item_qty ELSE 0 END ) item_custom_qty
							FROM td_salesorder_item a 
							LEFT JOIN tm_barang b ON b.kd_barang = a.barang_kd 
							GROUP BY a.msalesorder_kd
							UNION ALL
							SELECT a.msalesorder_kd, SUM(a.total_harga) total_harga, SUM(a.item_qty) total_qty,
							SUM( CASE WHEN a.item_status = "std" THEN a.total_harga ELSE 0 END ) item_standart_harga,
							SUM( CASE WHEN a.item_status = "custom" THEN a.total_harga ELSE 0 END ) item_custom_harga,
							SUM( CASE WHEN a.item_status = "std" THEN a.item_qty ELSE 0 END ) item_standart_qty,
							SUM( CASE WHEN a.item_status = "custom" THEN a.item_qty ELSE 0 END ) item_custom_qty
							FROM td_salesorder_item_detail a 
							GROUP BY a.msalesorder_kd ) a
					LEFT JOIN td_salesorder_harga b ON a.msalesorder_kd = b.msalesorder_kd
					GROUP BY a.msalesorder_kd ) e', 'a.kd_msalesorder = e.msalesorder_kd', 'LEFT')
		->join('tm_deliveryorder f', 'a.kd_msalesorder = f.msalesorder_kd', 'LEFT')
		->where_in('YEAR(a.tgl_so)', $tahun)
		->where_in('MONTH(a.tgl_so)', $bulan)
		->where_not_in('a.status_so', array('cancel', 'pending'))
		->where('a.tipe_customer', 'Lokal')
		->where('b.nm_salesperson !=', 'Kantor')
		->order_by('a.no_salesorder ASC, b.nm_salesperson ASC')
		->get()->result_array();
		return $data;
	}

	public function report_perbandingan_pertahun($tahunAwal, $tahunAkhir)
	{
		$data = $this->db->select('YEAR(tgl_so) tahun,
			SUM(e.item_standart_harga + e.item_custom_harga - e.total_nilai_disc + COALESCE(a.jml_ongkir, 0) + COALESCE(a.jml_install, 0)) as total_ammount_exc,
			SUM(CASE WHEN d.nm_jenis_customer = "Retail" THEN e.item_standart_harga + e.item_custom_harga - e.total_nilai_disc + COALESCE(a.jml_ongkir, 0) + COALESCE(a.jml_install, 0) ELSE 0 END) retail,
			SUM(CASE WHEN d.nm_jenis_customer = "Distributor" THEN e.item_standart_harga + e.item_custom_harga - e.total_nilai_disc + COALESCE(a.jml_ongkir, 0) + COALESCE(a.jml_install, 0) ELSE 0 END) distributor,
			SUM(CASE WHEN d.nm_jenis_customer = "Reseller" THEN e.item_standart_harga + e.item_custom_harga - e.total_nilai_disc + COALESCE(a.jml_ongkir, 0) + COALESCE(a.jml_install, 0) ELSE 0 END) reseller')
		->from('tm_salesorder a')
		->join('tb_salesperson b', 'a.salesperson_kd = b.kd_salesperson', 'LEFT')
		->join('tm_customer c', 'a.customer_kd = c.kd_customer', 'LEFT')
		->join('tb_jenis_customer d', 'c.jenis_customer_kd = d.kd_jenis_customer', 'LEFT')
		->join('(SELECT a.msalesorder_kd, 
				SUM(a.item_standart_qty) item_standart_qty, 
				SUM(a.item_custom_qty) item_custom_qty, 
				SUM(a.item_standart_harga) item_standart_harga, 
				SUM(a.item_custom_harga) item_custom_harga, 
				a.item_qty, COALESCE(b.total_nilai,0) total_nilai_disc, b.nm_kolom nm_kolom_disc 
				FROM ( SELECT a.msalesorder_kd, SUM(a.total_harga) total_harga, SUM(a.item_qty) item_qty,
						SUM( CASE WHEN a.item_status = "std" THEN a.total_harga ELSE 0 END ) item_standart_harga,
						SUM( CASE WHEN a.item_status = "custom" THEN a.total_harga ELSE 0 END ) item_custom_harga,
						SUM( CASE WHEN a.item_status = "std" THEN a.item_qty ELSE 0 END ) item_standart_qty,
						SUM( CASE WHEN a.item_status = "custom" THEN a.item_qty ELSE 0 END ) item_custom_qty
						FROM td_salesorder_item a 
						LEFT JOIN tm_barang b ON b.kd_barang = a.barang_kd 
						GROUP BY a.msalesorder_kd
						UNION ALL
						SELECT a.msalesorder_kd, SUM(a.total_harga) total_harga, SUM(a.item_qty) total_qty,
						SUM( CASE WHEN a.item_status = "std" THEN a.total_harga ELSE 0 END ) item_standart_harga,
						SUM( CASE WHEN a.item_status = "custom" THEN a.total_harga ELSE 0 END ) item_custom_harga,
						SUM( CASE WHEN a.item_status = "std" THEN a.item_qty ELSE 0 END ) item_standart_qty,
						SUM( CASE WHEN a.item_status = "custom" THEN a.item_qty ELSE 0 END ) item_custom_qty
						FROM td_salesorder_item_detail a 
						GROUP BY a.msalesorder_kd ) a 
				LEFT JOIN td_salesorder_harga b ON a.msalesorder_kd = b.msalesorder_kd 
				GROUP BY a.msalesorder_kd) e', 'a.kd_msalesorder = e.msalesorder_kd', 'LEFT')
		->join('tm_deliveryorder f', 'a.kd_msalesorder = f.msalesorder_kd', 'LEFT')
		->where('YEAR(a.tgl_so) >=', $tahunAwal)
		->where('YEAR(a.tgl_so) <=', $tahunAkhir)
		->where_not_in('a.status_so', array('cancel', 'pending'))
		->where('a.tipe_customer', 'Lokal')
		->where('b.nm_salesperson !=', 'Kantor')
		->group_by('YEAR(a.tgl_so)')
		->order_by('a.tgl_so ASC')
		->get()->result_array();
		return $data;
	}

	public function report_exclude_by_tahun_jeniscust($tahun, $jenis_customer)
	{
		$data = $this->db->select('b.nm_salesperson,
		SUM(CASE WHEN MONTH(a.tgl_so) = 1 THEN e.item_standart_harga + e.item_custom_harga - e.total_nilai_disc + COALESCE(a.jml_ongkir, 0) + COALESCE(a.jml_install, 0) ELSE 0 END) "1",
		SUM(CASE WHEN MONTH(a.tgl_so) = 2 THEN e.item_standart_harga + e.item_custom_harga - e.total_nilai_disc + COALESCE(a.jml_ongkir, 0) + COALESCE(a.jml_install, 0) ELSE 0 END) "2",
		SUM(CASE WHEN MONTH(a.tgl_so) = 3 THEN e.item_standart_harga + e.item_custom_harga - e.total_nilai_disc + COALESCE(a.jml_ongkir, 0) + COALESCE(a.jml_install, 0) ELSE 0 END) "3",
		SUM(CASE WHEN MONTH(a.tgl_so) = 4 THEN e.item_standart_harga + e.item_custom_harga - e.total_nilai_disc + COALESCE(a.jml_ongkir, 0) + COALESCE(a.jml_install, 0) ELSE 0 END) "4",
		SUM(CASE WHEN MONTH(a.tgl_so) = 5 THEN e.item_standart_harga + e.item_custom_harga - e.total_nilai_disc + COALESCE(a.jml_ongkir, 0) + COALESCE(a.jml_install, 0) ELSE 0 END) "5",
		SUM(CASE WHEN MONTH(a.tgl_so) = 6 THEN e.item_standart_harga + e.item_custom_harga - e.total_nilai_disc + COALESCE(a.jml_ongkir, 0) + COALESCE(a.jml_install, 0) ELSE 0 END) "6",
		SUM(CASE WHEN MONTH(a.tgl_so) = 7 THEN e.item_standart_harga + e.item_custom_harga - e.total_nilai_disc + COALESCE(a.jml_ongkir, 0) + COALESCE(a.jml_install, 0) ELSE 0 END) "7",
		SUM(CASE WHEN MONTH(a.tgl_so) = 8 THEN e.item_standart_harga + e.item_custom_harga - e.total_nilai_disc + COALESCE(a.jml_ongkir, 0) + COALESCE(a.jml_install, 0) ELSE 0 END) "8",
		SUM(CASE WHEN MONTH(a.tgl_so) = 9 THEN e.item_standart_harga + e.item_custom_harga - e.total_nilai_disc + COALESCE(a.jml_ongkir, 0) + COALESCE(a.jml_install, 0) ELSE 0 END) "9",
		SUM(CASE WHEN MONTH(a.tgl_so) = 10 THEN e.item_standart_harga + e.item_custom_harga - e.total_nilai_disc + COALESCE(a.jml_ongkir, 0) + COALESCE(a.jml_install, 0) ELSE 0 END) "10",
		SUM(CASE WHEN MONTH(a.tgl_so) = 11 THEN e.item_standart_harga + e.item_custom_harga - e.total_nilai_disc + COALESCE(a.jml_ongkir, 0) + COALESCE(a.jml_install, 0) ELSE 0 END) "11",
		SUM(CASE WHEN MONTH(a.tgl_so) = 12 THEN e.item_standart_harga + e.item_custom_harga - e.total_nilai_disc + COALESCE(a.jml_ongkir, 0) + COALESCE(a.jml_install, 0) ELSE 0 END) "12"')
		->from('tm_salesorder a')
		->join('tb_salesperson b', 'a.salesperson_kd = b.kd_salesperson', 'LEFT')
		->join('tm_customer c', 'a.customer_kd = c.kd_customer', 'LEFT')
		->join('tb_jenis_customer d', 'c.jenis_customer_kd = d.kd_jenis_customer', 'LEFT')
		->join('(SELECT a.msalesorder_kd, 
					SUM(a.item_standart_qty) item_standart_qty,
					SUM(a.item_custom_qty) item_custom_qty,
					SUM(a.item_standart_harga) item_standart_harga ,
					SUM(a.item_custom_harga) item_custom_harga, 
					a.item_qty,
					COALESCE(b.total_nilai,0) total_nilai_disc, b.nm_kolom nm_kolom_disc
					FROM ( SELECT a.msalesorder_kd, SUM(a.total_harga) total_harga, SUM(a.item_qty) item_qty,
							SUM( CASE WHEN a.item_status = "std" THEN a.total_harga ELSE 0 END ) item_standart_harga,
							SUM( CASE WHEN a.item_status = "custom" THEN a.total_harga ELSE 0 END ) item_custom_harga,
							SUM( CASE WHEN a.item_status = "std" THEN a.item_qty ELSE 0 END ) item_standart_qty,
							SUM( CASE WHEN a.item_status = "custom" THEN a.item_qty ELSE 0 END ) item_custom_qty
							FROM td_salesorder_item a 
							LEFT JOIN tm_barang b ON b.kd_barang = a.barang_kd 
							GROUP BY a.msalesorder_kd
							UNION ALL
							SELECT a.msalesorder_kd, SUM(a.total_harga) total_harga, SUM(a.item_qty) total_qty,
							SUM( CASE WHEN a.item_status = "std" THEN a.total_harga ELSE 0 END ) item_standart_harga,
							SUM( CASE WHEN a.item_status = "custom" THEN a.total_harga ELSE 0 END ) item_custom_harga,
							SUM( CASE WHEN a.item_status = "std" THEN a.item_qty ELSE 0 END ) item_standart_qty,
							SUM( CASE WHEN a.item_status = "custom" THEN a.item_qty ELSE 0 END ) item_custom_qty
							FROM td_salesorder_item_detail a 
							GROUP BY a.msalesorder_kd ) a
					LEFT JOIN td_salesorder_harga b ON a.msalesorder_kd = b.msalesorder_kd
					GROUP BY a.msalesorder_kd ) e', 'a.kd_msalesorder = e.msalesorder_kd', 'LEFT')
		->join('tm_deliveryorder f', 'a.kd_msalesorder = f.msalesorder_kd', 'LEFT')
		->where_in('YEAR(a.tgl_so)', $tahun)
		->where_not_in('a.status_so', array('cancel', 'pending'))
		->where('a.tipe_customer', 'Lokal')
		->where('b.nm_salesperson !=', 'Kantor')
		->where('d.nm_jenis_customer', $jenis_customer)
		->group_by('a.salesperson_kd')
		->order_by('b.nm_salesperson ASC')
		->get()->result_array();
		return $data;
		// Return bulan (number)
	}

	public function report_sales_invoice($tahun, $bulan, $sales_person, $tipe_customer)
	{
		$this->db->select('a.no_salesorder, b.nm_salesperson, a.tgl_so, a.tgl_kirim, x.tgl_invoice, h.tgl_termin, a.tgl_dp, c.code_customer, c.nm_customer, x.no_invoice, 
							COALESCE(a.jml_ppn,0) jml_ppn, COALESCE(a.jml_ongkir,0) jml_ongkir,COALESCE(a.jml_install,0) jml_install, COALESCE(a.jml_dp,0) jml_dp, COALESCE(a.jml_ppn_dp,0) jml_ppn_dp, 
							COALESCE(e.item_standart_harga,0) item_standart_harga, COALESCE(e.item_custom_harga,0) item_custom_harga, COALESCE(e.total_nilai_disc,0) total_nilai_disc, x.status_invoice, f.do_ket, g.nm_negara, g.nm_provinsi, g.nm_kota, g.nm_kecamatan, COALESCE(h.jml_termin,0) jml_termin, COALESCE(h.jml_termin_ppn,0) jml_termin_ppn')
				->from('( SELECT x.*, CASE 
						  WHEN x.status = "DP"  THEN "DP" 
					   	  WHEN x.status = "LUNAS" THEN "Lunas" 
						  WHEN x.status = "TERMIN" THEN "Termin" 
						  ELSE NULL END status_invoice
						  FROM (SELECT a.kd_msalesorder kd_msalesorder, 
								a.no_invoice_dp no_invoice, 
								a.tgl_so tgl_invoice, 
								a.tipe_customer tipe_customer, 
								IF(a.no_invoice_dp IS NOT NULL, "DP", NULL) status
								FROM tm_salesorder a
								WHERE no_invoice_dp IS NOT NULL AND no_invoice_dp != ""
								UNION ALL
								SELECT aa.msalesorder_kd, aa.no_invoice, aa.tgl_termin, bb.tipe_customer , "TERMIN"
								FROM td_so_termin aa 
								LEFT JOIN tm_salesorder bb ON aa.msalesorder_kd = bb.kd_msalesorder
								UNION ALL	
								SELECT b.msalesorder_kd kd_msalesorder, 
								b.no_invoice no_invoice, 
								b.tgl_do tgl_invoice, 
								b.tipe_do tipe_customer,
								IF(b.no_invoice IS NOT NULL, "LUNAS", NULL) status
								FROM tm_deliveryorder b
								WHERE no_invoice IS NOT NULL AND no_invoice != "") x
								ORDER BY x.no_invoice ASC) x')
				->join('tm_salesorder a', 'a.kd_msalesorder = x.kd_msalesorder')
				->join('tb_salesperson b', 'a.salesperson_kd = b.kd_salesperson', 'LEFT')
				->join('tm_customer c', 'a.customer_kd = c.kd_customer', 'LEFT')
				->join('tb_jenis_customer d', 'c.jenis_customer_kd = d.kd_jenis_customer', 'LEFT')
				->join('(SELECT a.msalesorder_kd, 
							SUM(a.item_standart_qty) item_standart_qty, 
							SUM(a.item_custom_qty) item_custom_qty, 
							SUM(a.item_standart_harga) item_standart_harga, 
							SUM(a.item_custom_harga) item_custom_harga, 
							a.item_qty, COALESCE(b.total_nilai,0) total_nilai_disc, b.nm_kolom nm_kolom_disc 
							FROM ( SELECT a.msalesorder_kd, SUM(a.total_harga) total_harga, SUM(a.item_qty) item_qty,
									SUM( CASE WHEN a.item_status = "std" THEN a.total_harga ELSE 0 END ) item_standart_harga,
									SUM( CASE WHEN a.item_status = "custom" THEN a.total_harga ELSE 0 END ) item_custom_harga,
									SUM( CASE WHEN a.item_status = "std" THEN a.item_qty ELSE 0 END ) item_standart_qty,
									SUM( CASE WHEN a.item_status = "custom" THEN a.item_qty ELSE 0 END ) item_custom_qty
									FROM td_salesorder_item a 
									LEFT JOIN tm_barang b ON b.kd_barang = a.barang_kd 
									GROUP BY a.msalesorder_kd
									UNION ALL
									SELECT a.msalesorder_kd, SUM(a.total_harga) total_harga, SUM(a.item_qty) total_qty,
									SUM( CASE WHEN a.item_status = "std" THEN a.total_harga ELSE 0 END ) item_standart_harga,
									SUM( CASE WHEN a.item_status = "custom" THEN a.total_harga ELSE 0 END ) item_custom_harga,
									SUM( CASE WHEN a.item_status = "std" THEN a.item_qty ELSE 0 END ) item_standart_qty,
									SUM( CASE WHEN a.item_status = "custom" THEN a.item_qty ELSE 0 END ) item_custom_qty
									FROM td_salesorder_item_detail a 
									GROUP BY a.msalesorder_kd ) a 
							LEFT JOIN td_salesorder_harga b ON a.msalesorder_kd = b.msalesorder_kd
							GROUP BY a.msalesorder_kd) e', 'x.kd_msalesorder = e.msalesorder_kd', 'LEFT')
				->join('tm_deliveryorder f', 'f.msalesorder_kd = x.kd_msalesorder', 'LEFT')
				->join('(SELECT a.kd_customer, a.nm_customer, b.nm_negara, c.nm_provinsi, d.nm_kota, e.nm_kecamatan
							FROM tm_customer a
							LEFT JOIN tb_negara b ON b.kd_negara = a.negara_kd
							LEFT JOIN tb_provinsi c ON c.kd_provinsi = a.provinsi_kd AND c.negara_kd = b.kd_negara
							LEFT JOIN tb_kota d ON d.kd_kota = a.kota_kd AND d.provinsi_kd = c.kd_provinsi AND d.negara_kd = b.kd_negara
							LEFT JOIN tb_kecamatan e ON e.kd_kecamatan = a.kecamatan_kd AND e.kota_kd = d.kd_kota AND e.provinsi_kd = c.kd_provinsi AND e.negara_kd = b.kd_negara) g', 'a.customer_kd = g.kd_customer','LEFT')
				->join('td_so_termin h', 'h.msalesorder_kd = f.msalesorder_kd OR h.no_invoice = x.no_invoice', 'LEFT');
			$this->db->where('if(status_invoice = "Lunas" ,year(x.tgl_invoice), year(a.tgl_dp)) like "%' . $tahun . '%"');
			if($bulan != ""):
			// $this->db->where('MONTH(select if(x.status_invoice == "Lunas" ,x.tgl_invoice, a.tgl_dp))', $bulan);
			$this->db->where('if(status_invoice = "Lunas" ,month(x.tgl_invoice), month(a.tgl_dp)) like "%' . $bulan . '%"');
			endif;
			if($sales_person != ""):
			$this->db->where('a.salesperson_kd', $sales_person);
			endif;
			if($tipe_customer != ""):
			$this->db->where('x.tipe_customer', $tipe_customer);
			endif;
			$this->db->where_not_in('a.status_so', array('cancel', 'pending'));
			$this->db->order_by('x.no_invoice ASC');
			$data = $this->db->get()->result_array();
			return $data;
	}

	public function report_sales_outstanding_lokal(){
		$this->db->select('a.kd_msalesorder,a.no_po,a.tgl_so,a.tgl_kirim,a.tgl_selesai,c.item_qty,b.kd_mdo,a.note_so,a.no_salesorder,d.nm_customer')
			->from('tm_salesorder a')
			->join('tm_deliveryorder b', 'a.kd_msalesorder = b.msalesorder_kd', 'LEFT')
			->join('( SELECT tb.msalesorder_kd, SUM(tb.item_qty) AS item_qty FROM
						(SELECT msalesorder_kd, SUM(item_qty) AS item_qty
						FROM td_salesorder_item
						GROUP BY msalesorder_kd
						UNION ALL
						SELECT msalesorder_kd, SUM(item_qty) AS item_qty
						FROM td_salesorder_item_detail
						GROUP BY msalesorder_kd) tb
					GROUP BY tb.msalesorder_kd ) c', 'a.kd_msalesorder = c.msalesorder_kd', 'LEFT')
			->join('tm_customer d', 'a.customer_kd = d.kd_customer', 'LEFT')
			->where('b.kd_mdo', null)
			->where('a.tipe_customer', 'Lokal')
			->where('a.status_so !=', 'cancel')
			->order_by('a.tgl_kirim ASC')
			->order_by('IFNULL(a.tgl_kirim, a.tgl_so) ASC');
			$data = $this->db->get()->result_array();
		return $data;

	}

	// --- FOR REPORT EKSPOR --- //

	// REPORT LAMA TIDAK SESUAI ROUND KETIKA DISCOUNT DISTRIBUTOR (GET FROM DATABASE)
	// public function report_sales_ekspor_invoice($tahun, $bulan, $tipe_customer)
	// {
	// 	// Barang dihitung berdasarkan DO Item (Bukan SO item) referrence by SO ITEM
	// 	$this->db->select('x.tgl_stuffing, a.no_po, a.tgl_so, a.tgl_kirim, x.tgl_do, c.code_customer, c.nm_customer, x.no_invoice, 
	// 						a.tipe_container, x.nm_jasakirim, x.seal_number, x.container_number, x.vessel, x.etd_sub, x.etd_sin, x.eta, x.container_arrived, x.no_bl,
	// 						COALESCE(a.jml_ongkir,0) jml_ongkir,COALESCE(a.jml_install,0) jml_install, COALESCE(a.jml_dp,0) jml_dp, COALESCE(e.total_item_cbm_do,0) total_item_cbm,
	// 						COALESCE(e.item_standart_harga_do,0) item_standart_harga, COALESCE(e.item_custom_harga_do,0) item_custom_harga, 
	// 						COALESCE(e.item_standart_disc_distributor_do,0) item_standart_disc_distributor, COALESCE(e.item_custom_disc_distributor_do,0) item_custom_disc_distributor,
	// 						COALESCE(e.item_standart_disc_customer_do,0) item_standart_disc_customer, COALESCE(e.item_custom_disc_customer_do,0) item_custom_disc_customer,
	// 						COALESCE(e.item_standart_qty_do,0) item_standart_qty, COALESCE(e.item_custom_qty_do,0) item_custom_qty, COALESCE(e.item_accessories_qty_do,0) item_accessories_qty,
	// 						x.do_ket, g.nm_negara, g.nm_provinsi, g.nm_kota, g.nm_kecamatan,
	// 						(SELECT ABS(DATEDIFF(a.tgl_kirim,x.tgl_stuffing)) ) as count_day')
	// 			->from('tm_deliveryorder x')
	// 			->join('tm_salesorder a', 'a.kd_msalesorder = x.msalesorder_kd')
	// 			->join('tb_salesperson b', 'a.salesperson_kd = b.kd_salesperson', 'LEFT')
	// 			->join('tm_customer c', 'a.customer_kd = c.kd_customer', 'LEFT')
	// 			->join('tb_jenis_customer d', 'c.jenis_customer_kd = d.kd_jenis_customer', 'LEFT')
	// 			->join('( SELECT a.mdo_kd, a.msalesorder_kd, 
	// 						SUM(CASE WHEN item_status = "std" THEN a.stuffing_item_qty ELSE 0 END) item_standart_qty_do, 
	// 						SUM(CASE WHEN item_status = "custom" THEN a.stuffing_item_qty ELSE 0 END) item_custom_qty_do, 
	// 						SUM(CASE WHEN item_status = "std" THEN a.total_harga_do ELSE 0 END) item_standart_harga_do, 
	// 						SUM(CASE WHEN item_status = "custom" THEN a.total_harga_do ELSE 0 END) item_custom_harga_do,
	// 						SUM(CASE WHEN item_status = "std" THEN a.total_disc_distributor_do ELSE 0 END) item_standart_disc_distributor_do, 
	// 						SUM(CASE WHEN item_status = "custom" THEN a.total_disc_distributor_do ELSE 0 END) item_custom_disc_distributor_do,
	// 						SUM(CASE WHEN item_status = "std" THEN a.total_disc_customer_do ELSE 0 END) item_standart_disc_customer_do, 
	// 						SUM(CASE WHEN item_status = "custom" THEN a.total_disc_customer_do ELSE 0 END) item_custom_disc_customer_do,
	// 						SUM(CASE WHEN nm_kat_barang = "Accessories" THEN a.stuffing_item_qty ELSE 0 END) item_accessories_qty_do,
	// 						SUM(a.stuffing_item_qty) item_qty_do,
	// 						SUM(a.total_item_cbm_do) total_item_cbm_do
	// 						FROM ( SELECT c.barang_kd, d.deskripsi_barang, e.nm_kat_barang ,a.mdo_kd, c.msalesorder_kd, c.kd_ditem_so, c.item_status, c.harga_barang, c.item_qty, c.total_harga total_harga, a.stuffing_item_qty, c.harga_barang * a.stuffing_item_qty total_harga_do, c.item_cbm,
	// 								c.item_cbm * a.stuffing_item_qty total_item_cbm_do,
	// 								c.harga_barang * (SELECT a.jml_disc FROM td_salesorder_item_disc a WHERE a.mso_kd = c.msalesorder_kd AND a.nm_pihak = "Distributor" AND a.so_item_kd = kd_ditem_so) / 100 * a.stuffing_item_qty total_disc_distributor_do,
	// 								c.harga_barang * (SELECT a.jml_disc FROM td_salesorder_item_disc a WHERE a.mso_kd = c.msalesorder_kd AND a.nm_pihak = "Customer" AND a.so_item_kd = kd_ditem_so) / 100 * a.stuffing_item_qty total_disc_customer_do									
	// 								FROM td_do_item a
	// 								LEFT JOIN tm_deliveryorder b ON a.mdo_kd = b.kd_mdo
	// 								LEFT JOIN td_salesorder_item c ON a.parent_kd = c.kd_ditem_so
	// 								LEFT JOIN tm_barang d ON c.barang_kd = d.kd_barang
	// 								LEFT JOIN tm_kat_barang e ON d.kat_barang_kd = e.kd_kat_barang ) a 
	// 						LEFT JOIN td_salesorder_harga b ON a.msalesorder_kd = b.msalesorder_kd
	// 						GROUP BY a.mdo_kd ) e', 'x.kd_mdo = e.mdo_kd', 'LEFT')
	// 			->join('(SELECT a.kd_customer, a.nm_customer, b.nm_negara, c.nm_provinsi, d.nm_kota, e.nm_kecamatan
	// 						FROM tm_customer a
	// 						LEFT JOIN tb_negara b ON b.kd_negara = a.negara_kd
	// 						LEFT JOIN tb_provinsi c ON c.kd_provinsi = a.provinsi_kd AND c.negara_kd = b.kd_negara
	// 						LEFT JOIN tb_kota d ON d.kd_kota = a.kota_kd AND d.provinsi_kd = c.kd_provinsi AND d.negara_kd = b.kd_negara
	// 						LEFT JOIN tb_kecamatan e ON e.kd_kecamatan = a.kecamatan_kd AND e.kota_kd = d.kd_kota AND e.provinsi_kd = c.kd_provinsi AND e.negara_kd = b.kd_negara) g', 'a.customer_kd = g.kd_customer','LEFT');
	// 		$this->db->where('YEAR(x.tgl_do)', $tahun);
	// 		if($bulan != ""):
	// 		$this->db->where('MONTH(x.tgl_do)', $bulan);
	// 		endif;
	// 		if($tipe_customer != ""):
	// 		$this->db->where('x.tipe_do', $tipe_customer);
	// 		endif;
	// 		$this->db->where_not_in('a.status_so', array('cancel', 'pending'));
	// 		$this->db->order_by('x.no_invoice ASC');
	// 		$data = $this->db->get()->result_array();
	// 		return $data;
	// 		// QUERY GET LEAD TIME FROM DB HRM
	// 		// SELECT a.all_day - a.off_day lead_time
	// 		// FROM ( SELECT ABS(DATEDIFF("2017-01-01","2017-02-01")) as all_day, 
	// 		// ( SELECT count(*) as day_count 
	// 		//   FROM tb_hari_libur
	// 		//   WHERE tgl_libur 
	// 		// 	BETWEEN "2017-01-01" AND "2017-02-01" ) off_day ) a
	// 		// FUNGSI ROUND UP KETIKA 0.005 
	// 		// SELECT CEIL(187.325 * 100) / 100
	// 	}

	public function report_sales_ekspor_invoice($tahun, $bulan, $tipe_customer)
	{	
		$this->load->model(array('tb_default_disc', 'db_hrm/tb_hari_efektif', 'model_do', 'tm_deliveryorder', 'td_salesorder_item_disc', 'tm_salesorder', 'm_customer', 'tb_teritory', 'tb_tipe_teritory'));
		// Item qty dihitung berdasarkan DO Item (Bukan SO item) referrence value by SO ITEM
		if($bulan != null ):
		$param = [
			'YEAR(tgl_do)' => $tahun,
			'MONTH(tgl_do)' => $bulan,
			'tipe_do' => $tipe_customer
		];
		else:
		$param = [
			'YEAR(tgl_do)' => $tahun,
			'tipe_do' => $tipe_customer
		];
		endif;
		$deliveryOrder = $this->tm_deliveryorder->get_by_param($param)->result_array();
		$listkdDO = [];
		$listkdSO = [];
		foreach($deliveryOrder as $do):
			$listkdDO[] = $do['kd_mdo'];
			$listkdSO[] = $do['msalesorder_kd'];
		endforeach;
		$parentItem = $this->model_do->list_parent_items($listkdDO, '', "Ekspor");
		// HITUNG JUMLAH HARGA BARANG
		$inv = [];
		$i = 0;
		foreach($deliveryOrder as $do):
			// Hitung parent item
			$qty = 0;
			$cbm = 0;
			$ammountPo = 0;
			$ammountCustomer = 0;
			$ammountFinance = 0;
			$ammountDistributor = 0;
			foreach($parentItem as $parent):
				// Summary Parent
				if($parent->mdo_kd == $do['kd_mdo']):
					$qty += $parent->stuffing_item_qty;
					$cbm += $parent->item_cbm * $parent->stuffing_item_qty;
					$ammountPo += $parent->harga_barang * $parent->stuffing_item_qty; 
					// Discount Customer
					$disc = $this->td_salesorder_item_disc->get_where([
						'so_item_kd' => $parent->parent_kd,
						'mso_kd' => $do['msalesorder_kd'],
						'nm_pihak' => "Customer"
					])->jml_disc;
					$ammountCustomer += ROUND($parent->harga_barang - $parent->harga_barang * $disc/100, 2) * $parent->stuffing_item_qty;
					// Discount Finance
					$disc = 25;
					$ammountFinance += ROUND($parent->harga_barang - $parent->harga_barang * $disc/100, 2) * $parent->stuffing_item_qty;
					// Discount Distributor
					$disc = $this->td_salesorder_item_disc->get_where([
						'so_item_kd' => $parent->parent_kd,
						'mso_kd' => $do['msalesorder_kd'],
						'nm_pihak' => "Distributor"
					])->jml_disc;
					$ammountDistributor += ROUND($parent->harga_barang - $parent->harga_barang * $disc/100, 2) * $parent->stuffing_item_qty;
				endif; 
			endforeach;
			$inv[$i]['tgl_do'] = $do['tgl_do'];
			$inv[$i]['month'] = format_date($do['tgl_do'], 'M');
			$inv[$i]['po_date'] = $this->tm_salesorder->get_row($do['msalesorder_kd'])->tgl_so;
			$inv[$i]['stuffing_plan'] = $this->tm_salesorder->get_row($do['msalesorder_kd'])->tgl_kirim;
			$inv[$i]['actual_shipment'] = $do['tgl_stuffing'];
			$inv[$i]['code_po'] = $this->tm_salesorder->get_row($do['msalesorder_kd'])->no_po;
			$inv[$i]['no_invoice'] = $do['no_invoice'];
			$inv[$i]['qty'] = $qty;
			$inv[$i]['ammount_po'] = $ammountPo;
			$inv[$i]['ammount_customer'] = $ammountCustomer;
			$inv[$i]['ammount_finance'] = $ammountFinance;
			$inv[$i]['ammount_distributor'] = $ammountDistributor;
			$inv[$i]['container_20ft'] = $this->tm_deliveryorder->get_by_param([
						'msalesorder_kd' => $do['msalesorder_kd'], 
						'kd_mdo' => $do['kd_mdo']])->row()->container_20ft;
			
			// tgl kirim atau so masih ada yang kosong atau null 
			if($this->tm_salesorder->get_row($do['msalesorder_kd'])->tgl_so == NULL || $this->tm_salesorder->get_row($do['msalesorder_kd'])->tgl_kirim == NULL):
				$inv[$i]['lead_time'] = 0;
			else:
				$inv[$i]['lead_time'] = $this->tb_hari_efektif->get_lead_time($this->tm_salesorder->get_row($do['msalesorder_kd'])->tgl_so, $this->tm_salesorder->get_row($do['msalesorder_kd'])->tgl_kirim);
			endif;

			$inv[$i]['remarks'] = $this->tm_salesorder->get_row($do['msalesorder_kd'])->tipe_container;
			$inv[$i]['cbm'] = $cbm;
			$inv[$i]['shipping'] = $do['nm_jasakirim'];
			$inv[$i]['no_container'] = $do['container_number'];
			$inv[$i]['no_seal'] = $do['seal_number'];
			$inv[$i]['vessel'] = $do['vessel'];
			$inv[$i]['etd_sub'] = $do['etd_sub'];
			$inv[$i]['etd_sin'] = $do['etd_sin'];
			$inv[$i]['eta'] = $do['eta'];
			$inv[$i]['container_arrived'] = $do['container_arrived'];
			$inv[$i]['no_bl'] = $do['no_bl'];
			$inv[$i]['nm_negara'] = $this->m_customer->detail_customer($this->tm_salesorder->get_row($do['msalesorder_kd'])->customer_kd)->nm_negara;
			$inv[$i]['nm_provinsi'] = $this->m_customer->detail_customer($this->tm_salesorder->get_row($do['msalesorder_kd'])->customer_kd)->nm_provinsi;
			$inv[$i]['do_ket'] = $do['do_ket'];
			$teritory_id = $this->tm_salesorder->get_row($do['msalesorder_kd'])->teritory_id;
			$inv[$i]['teritory'] = $this->tb_teritory->get_row($teritory_id)['nm_teritory'];
			$tipe_teritory_id = $this->tb_teritory->get_row($teritory_id)['tipe_teritory_id'];
			$inv[$i]['tipe_teritory'] = $this->tb_tipe_teritory->get_row($tipe_teritory_id)['kd_tipe_teritory'];
			$i++;
		endforeach;
		return !empty($inv) ? $inv : null;
	}

	public function report_sales_ekspor_additionalitem($tahun, $bulan, $tipe_customer){
		$this->load->model(array('tb_default_disc', 'db_hrm/tb_hari_efektif', 'model_do', 'tm_deliveryorder', 'td_salesorder_item_disc', 'tm_salesorder', 'm_customer', 'tb_teritory'));
		// Item qty dihitung berdasarkan DO Item (Bukan SO item) referrence value by SO ITEM
		if($bulan != null ):
		$param = [
			'YEAR(tgl_do)' => $tahun,
			'MONTH(tgl_do)' => $bulan,
			'tipe_do' => $tipe_customer
		];
		else:
		$param = [
			'YEAR(tgl_do)' => $tahun,
			'tipe_do' => $tipe_customer
		];
		endif;
		$deliveryOrder = $this->tm_deliveryorder->get_by_param($param)->result_array();
		$listkdDO = [];
		$listkdSO = [];
		foreach($deliveryOrder as $do):
			$listkdDO[] = $do['kd_mdo'];
			$listkdSO[] = $do['msalesorder_kd'];
		endforeach;
		$parentItem = $this->model_do->list_parent_items($listkdDO, '', "Ekspor");

		$arr = [];
		$i = 0;
		foreach($parentItem as $parent):
			if($parent->order_tipe == "additional"):
				$arr[$i]['no_invoice'] = $this->model_do->get_row($parent->mdo_kd)->no_invoice;
				$arr[$i]['no_po'] = $this->tm_salesorder->get_row($parent->msalesorder_kd)->no_po;
				$arr[$i]['item_code'] = $parent->item_code;
				$arr[$i]['item_desc'] = $parent->item_desc;
				$arr[$i]['delivery_date'] = $this->model_do->get_row($parent->mdo_kd)->tgl_stuffing;
				$arr[$i]['qty'] = $parent->stuffing_item_qty;
				$arr[$i]['item_note_do'] = $parent->item_note_do == "" ? "Tidak ada note" : $parent->item_note_do;
				$i++;
			endif;
		endforeach;

		return $arr;
	}

	public function report_sales_outstanding_ekspor(){
		$this->db->select('a.kd_msalesorder,a.no_po,a.tgl_so,a.tgl_po,a.tgl_kirim,a.tgl_selesai,c.item_qty,a.tipe_container,b.tgl_stuffing,b.kd_mdo,a.note_so,a.container_20ft,d.nm_customer')
			->from('tm_salesorder a')
			->join('tm_deliveryorder b', 'a.kd_msalesorder = b.msalesorder_kd', 'LEFT')
			->join('( SELECT msalesorder_kd, SUM(item_qty) AS item_qty
					  FROM td_salesorder_item
					  GROUP BY msalesorder_kd) c', 'a.kd_msalesorder = c.msalesorder_kd', 'LEFT')
			->join('tm_customer d', 'a.customer_kd = d.kd_customer', 'LEFT')
			->where('b.kd_mdo', null)
			->where('a.tipe_customer', 'Ekspor')
			->where('a.status_so !=', 'cancel')
			->where('a.status_so !=', 'finish')
			->order_by('a.tgl_kirim ASC')
			->order_by('IFNULL(a.tgl_kirim, a.tgl_so) ASC');
		$data = $this->db->get()->result_array();
		return $data;

	}

	public function report_sales_leave_item_ekspor($tahun)
	{
		// Status SO hanya finish 
		// Barang SO yang tidak ada di DO dan jumlah barang tidak sama dengan 0
		$data = $this->db->select('a.kd_ditem_so,a.msalesorder_kd,c.no_po,c.tgl_so,c.tgl_kirim,b.no_invoice,a.item_desc,a.item_code,a.item_qty,b.stuffing_item_qty,(a.item_qty - b.stuffing_item_qty) actual_qty')
		->from('td_salesorder_item a')
		->join('(SELECT a.parent_kd, SUM(a.stuffing_item_qty) stuffing_item_qty, b.no_invoice
								FROM td_do_item a
								LEFT JOIN tm_deliveryorder b ON a.mdo_kd = b.kd_mdo
								GROUP BY b.msalesorder_kd, a.parent_kd) b', 'a.kd_ditem_so = b.parent_kd', 'LEFT')
		->join('tm_salesorder c', 'a.msalesorder_kd = c.kd_msalesorder', 'LEFT')
		->where('YEAR(c.tgl_kirim)', $tahun)
		->where('c.status_so', 'finish')
		->where('c.tipe_customer', 'Ekspor')
		->where('(a.item_qty - b.stuffing_item_qty) !=', 0)
		->where('b.no_invoice !=', 0)
		->order_by('c.tgl_kirim', 'ASC')
		->get()->result_array();
		return $data;
		
		// QUERY RAW
		// SELECT a.kd_ditem_so, a.msalesorder_kd, c.no_po, c.tgl_so, c.tgl_kirim, b.no_invoice, a.item_desc, a.item_code, a.item_qty, b.stuffing_item_qty, (a.item_qty - b.stuffing_item_qty) actual_qty 
		// FROM td_salesorder_item a 
		// LEFT JOIN (SELECT a.parent_kd, SUM(a.stuffing_item_qty) stuffing_item_qty, b.no_invoice FROM td_do_item a 
		// LEFT JOIN tm_deliveryorder b ON a.mdo_kd = b.kd_mdo 
		// GROUP BY b.msalesorder_kd, a.parent_kd) b ON a.kd_ditem_so = b.parent_kd LEFT JOIN tm_salesorder c ON a.msalesorder_kd = c.kd_msalesorder 
		// WHERE YEAR(c.tgl_kirim) = '2021' AND c.status_so = 'finish' AND c.tipe_customer = 'Ekspor' AND (a.item_qty - b.stuffing_item_qty) !=0 AND b.no_invoice != "" ORDER BY c.tgl_kirim ASC


		// QUERY GET VALUE BARANG
		// SELECT a.kd_ditem_so, a.msalesorder_kd, a.item_code, a.item_qty, b.stuffing_item_qty, a.item_qty - b.stuffing_item_qty actual_qty 
		// FROM td_salesorder_item a
		// LEFT JOIN ( SELECT a.parent_kd, SUM(a.stuffing_item_qty) stuffing_item_qty
		// 						FROM td_do_item a
		// 						LEFT JOIN tm_deliveryorder b ON a.mdo_kd = b.kd_mdo
		// 						GROUP BY b.msalesorder_kd, a.parent_kd) b ON a.kd_ditem_so = b.parent_kd
		// LEFT JOIN tm_salesorder c ON a.msalesorder_kd = c.kd_msalesorder
		// WHERE a.msalesorder_kd = "MSO231221000002"

	}

}