<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_workorder_item_so_detail extends CI_Model
{
    private $tbl_name = 'td_workorder_item_so_detail';
    private $p_key = 'woitemsodetail_kd';

    public function generate_kd()
    {
        $query = $this->db->select('MAX(' . $this->p_key . ') as maxID')
            ->get($this->tbl_name)
            ->row();
        $code = (int) $query->maxID + 1;
        return $code;
    }

    public function insert_data($data)
    {
        $query = $this->db->insert($this->tbl_name, $data);
        return $query ? TRUE : FALSE;
    }

    public function delete_data($id)
    {
        $query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
        return $query ? TRUE : FALSE;
    }

    public function get_by_param($param = [])
    {
        $this->db->where($param);
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    public function get_all()
    {
        $act = $this->db->order_by($this->p_key)->get($this->tbl_name);
        return $act;
    }

    public function update_data($aWhere = [], $data)
    {
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
        return $query ? TRUE : FALSE;
    }

    public function insert_batch_data($data = [])
    {
        $query = $this->db->insert_batch($this->tbl_name, $data);
        return $query ? TRUE : FALSE;
    }

    public function delete_by_param($param = [])
    {
        $query = $this->db->delete($this->tbl_name, $param);
        return $query ? TRUE : FALSE;
    }

    public function get_where_in($where, $in = [])
    {
        $query = $this->db->where_in($where, $in)->get($this->tbl_name);
        return $query;
    }

    public function excuteQuery(string $query)
    {
        return $this->db->query($query);
    }

    public function insertBatch_detailSalesOrderByWo($wo_kd): bool
    {
        $this->load->model(['tb_relasi_sowo', 'td_salesorder_item', 'td_salesorder_item_detail']);
        $sowos = $this->tb_relasi_sowo->get_by_param(['wo_kd' => $wo_kd])->result_array();
        $kd_msalesorders = array_column($sowos, 'kd_msalesorder');

        $parents = $this->td_salesorder_item->get_items($kd_msalesorders);
        $childs = $this->td_salesorder_item_detail->get_items($kd_msalesorders);

        $arrayBatchData = [];
        $woitemsodetail_kd = $this->generate_kd();
        foreach ($parents as $r_parent) {
            foreach ($sowos as $sowoParent) {
                if ($sowoParent['kd_msalesorder'] == $r_parent->msalesorder_kd) {
                    $relasisowo_kd = $sowoParent['relasisowo_kd'];
                }
            }
            $arrayBatchData[] = [
                'woitemsodetail_kd' => $woitemsodetail_kd,
                'wo_kd' => $wo_kd,
                'relasisowo_kd' => $relasisowo_kd,
                'kd_msalesorder' => $r_parent->msalesorder_kd,
                'kd_barang' => $r_parent->barang_kd,
                'parent_kd' => $r_parent->kd_ditem_so,
                'child_kd' => null,
                'woitemso_kd' => null,
                'woitemsodetail_itemcode' => $r_parent->item_code,
                'woitemsodetail_itemdeskripsi' => $r_parent->item_desc,
                'woitemsodetail_itemdimensi' => $r_parent->item_dimension,
                'woitemsodetail_qty' => $r_parent->item_qty,
                'woitemsodetail_status' => $r_parent->item_status,
                'woitemsodetail_tipe' => 'so',
                'woitemsodetail_prosesstatus' => 'editing',
                'woitemsodetail_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin')
            ];
            $woitemsodetail_kd++;
        }

        foreach ($childs as $r_child) {
            foreach ($sowos as $sowoChild) {
                if ($sowoChild['kd_msalesorder'] == $r_child->msalesorder_kd) {
                    $relasisowo_kd = $sowoChild['relasisowo_kd'];
                }
            }
            $arrayBatchData[] = [
                'woitemsodetail_kd' => $woitemsodetail_kd,
                'wo_kd' => $wo_kd,
                'relasisowo_kd' => $relasisowo_kd,
                'kd_msalesorder' => $r_child->msalesorder_kd,
                'kd_barang' => $r_child->barang_kd,
                'parent_kd' => null,
                'child_kd' => $r_child->kd_citem_so,
                'woitemso_kd' => null,
                'woitemsodetail_itemcode' => $r_child->item_code,
                'woitemsodetail_itemdeskripsi' => $r_child->item_desc,
                'woitemsodetail_itemdimensi' => $r_child->item_dimension,
                'woitemsodetail_qty' => $r_child->item_qty,
                'woitemsodetail_status' => $r_child->item_status,
                'woitemsodetail_tipe' => 'so',
                'woitemsodetail_prosesstatus' => 'editing',
                'woitemsodetail_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin')
            ];
            $woitemsodetail_kd++;
        }

        $act = $this->insert_batch_data($arrayBatchData);
        return $act;
    }

    public function insertBatch_additionalSalesOrder($wo_kd, $soitems): void
    {
        $this->load->model(array('td_salesorder_item', 'td_salesorder_item_detail'));

        $items = [];
        $itemdetails = [];
        if (!empty($soitems)) {
            $arrayParentkd = [];
            $arrayChildkd = [];
            $kd_msalesorders = [];
            foreach ($soitems as $r_soitem) {
                $id = explode('#', $r_soitem);
                if (!empty($id[1])) {
                    $arrayParentkd[] = $id[1];
                } else {
                    $arrayChildkd[] = $id[2];
                }
                if (!in_array($id[0], $kd_msalesorders)) {
                    $kd_msalesorders[] = $id[0];
                }
            }
            if (!empty($arrayParentkd)) {
                $items = $this->td_salesorder_item->get_where_in_project($arrayParentkd)->result_array();
            }
            if (!empty($arrayChildkd)) {
                $itemdetails = $this->td_salesorder_item_detail->get_where_in_project($arrayChildkd)->result_array();
            }
            $arrayWoitemsodetails = [];
            $woitemsodetail_kd = $this->generate_kd();
            foreach ($items as $item) {
                $arrayWoitemsodetails[] = [
                    'woitemsodetail_kd' => $woitemsodetail_kd,
                    'wo_kd' => $wo_kd,
                    'relasisowo_kd' => null,
                    'kd_msalesorder' => $item['msalesorder_kd'],
                    'kd_barang' => $item['barang_kd'],
                    'parent_kd' => $item['kd_ditem_so'],
                    'child_kd' => null,
                    'woitemso_kd' => null,
                    'woitemsodetail_itemcode' => $item['item_code'],
                    'woitemsodetail_itemdeskripsi' => $item['item_desc'],
                    'woitemsodetail_itemdimensi' => $item['item_dimension'],
                    'woitemsodetail_qty' => $item['item_qty'],
                    'woitemsodetail_status' => $item['item_status'],
                    'woitemsodetail_tipe' => 'so',
                    'woitemsodetail_prosesstatus' => 'editing',
                    'woitemsodetail_tglinput' => date('Y-m-d H:i:s'),
                    'woitemsodetail_tgledit' => date('Y-m-d H:i:s'),
                    'admin_kd' => $this->session->userdata('kd_admin'),
                ];
                $woitemsodetail_kd++;
            }
            foreach ($itemdetails as $item) {
                $arrayWoitemsodetails[] = [
                    'woitemsodetail_kd' => $woitemsodetail_kd,
                    'wo_kd' => $wo_kd,
                    'relasisowo_kd' => null,
                    'kd_msalesorder' => $itemdetails['msalesorder_kd'],
                    'kd_barang' => $itemdetails['barang_kd'],
                    'parent_kd' => null,
                    'child_kd' => $itemdetails['kd_citem_so'],
                    'woitemso_kd' => null,
                    'woitemsodetail_itemcode' => $itemdetails['item_code'],
                    'woitemsodetail_itemdeskripsi' => $itemdetails['item_desc'],
                    'woitemsodetail_itemdimensi' => $itemdetails['item_dimension'],
                    'woitemsodetail_qty' => $itemdetails['item_qty'],
                    'woitemsodetail_status' => $itemdetails['item_status'],
                    'woitemsodetail_tipe' => 'so',
                    'woitemsodetail_prosesstatus' => 'editing',
                    'woitemsodetail_tglinput' => date('Y-m-d H:i:s'),
                    'woitemsodetail_tgledit' => date('Y-m-d H:i:s'),
                    'admin_kd' => $this->session->userdata('kd_admin'),
                ];
                $woitemsodetail_kd++;
            }

            $this->insert_batch_data($arrayWoitemsodetails);
            return;
        }
    }

    // public function get_by_param_relassowo_wo($param = [])
    // {
    //     $query = $this->db->select()
    //         ->from($this->tbl_name)
    //         ->join('tb_relasi_sowo', $this->tbl_name . '.relasisowo_kd=tb_relasi_sowo.relasisowo_kd', 'left')
    //         ->join('tm_workorder', 'tb_relasi_sowo.wo_kd=tm_workorder.wo_kd', 'left')
    //         ->where($param)
    //         ->get();
    //     return $query;
    // }
}
