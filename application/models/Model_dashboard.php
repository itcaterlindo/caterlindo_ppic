<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Model_dashboard extends CI_Model {
	private $tbl_name = '';
	private $p_key = '';
	private $title_name = 'Data Report Dashboard';

	public function get_count() {
		$stmt = "SELECT (SELECT COUNT(a.kd_kat_barang) FROM tm_kat_barang a) AS jml_tipe_product, (SELECT COUNT(b.kd_barang) FROM tm_barang b) AS jml_product, (SELECT COUNT(c.kd_msalesorder) FROM tm_salesorder c) AS jml_so, (SELECT COUNT(d.kd_customer) FROM tm_customer d) AS jml_customer";
		$query = $this->db->query($stmt);
		$row = $query->row();
		if (!empty($row)) :
			$data = array('jml_tipe_product' => $row->jml_tipe_product, 'jml_product' => $row->jml_product, 'jml_so' => $row->jml_so, 'jml_customer' => $row->jml_customer);
		else :
			$data = array('jml_tipe_product' => '0', 'jml_product' => '0', 'jml_so' => '0', 'jml_customer' => '0');
		endif;
		return $data;
	}

	public function report_rules() {
		$rules = array(
			array('field' => 'selTipePenjualan', 'label' => 'Tipe Penjualan', 'rules' => 'required'),
			array('field' => 'selTipeCustomer', 'label' => 'Tipe Customer', 'rules' => 'required'),
			array('field' => 'selTipeDiscount', 'label' => 'Tipe Discount', 'rules' => 'required'),
			array('field' => 'selSales', 'label' => 'Salesperson', 'rules' => 'required'),
			array('field' => 'selStatusItem', 'label' => 'Status Item', 'rules' => 'required'),
			array('field' => 'txtTglAwal', 'label' => 'Tanggal Mulai', 'rules' => 'required'),
			array('field' => 'txtTglAkhir', 'label' => 'Tanggal Akhir', 'rules' => 'required'),
		);
		return $rules;
	}

	public function report_warning($datas = '') {
		$forms = array('selTipePenjualan', 'selTipeCustomer', 'selTipeDiscount', 'selSales', 'selStatusItem', 'txtTglAwal', 'txtTglAkhir');
		foreach ($datas as $key => $data) :
			$str[$data] = (!empty(form_error($forms[$key])))?buildLabel('warning', form_error($forms[$key], '"', '"')):'';
		endforeach;
		return $str;
	}
}