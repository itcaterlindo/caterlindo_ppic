<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_perbandinganharga_pr extends CI_Model {
	private $tbl_name = 'td_perbandinganharga_pr';
	private $p_key = 'perbandinganhargapr_kd';

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_by_param_in($column, $data=[]) {
		$query = $this->db->where_in($column, $data)->get($this->tbl_name);
		return $query;
	}

	public function delete_by_param($param, $kode) {
		$query = $this->db->delete($this->tbl_name, array($param => $kode)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param_detail ($param=[]) {
		$result = $this->db->select()
					->from($this->tbl_name)
					->join('tm_purchaserequisition', $this->tbl_name.'.pr_kd=tm_purchaserequisition.pr_kd', 'left')
					->join('td_perbandinganharga_detail', $this->tbl_name.'.perbandinganharga_kd=td_perbandinganharga_detail.perbandinganharga_kd', 'left')
					->where($param)
					->get();
		return $result;
	}
}