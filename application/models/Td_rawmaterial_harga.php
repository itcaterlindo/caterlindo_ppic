<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_rawmaterial_harga extends CI_Model {
	private $tbl_name = 'td_rawmaterial_harga';
	private $p_key = 'rmharga_kd';

    public function ssp_table () {
        $data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'b.rm_kode',
				'dt' => 2, 'field' => 'rm_kode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.rm_deskripsi', 
				'dt' => 3, 'field' => 'rm_deskripsi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'b.rm_spesifikasi', 
				'dt' => 4, 'field' => 'rm_spesifikasi',
				'formatter' => function ($d, $row){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'c.rmsatuan_nama', 
				'dt' => 5, 'field' => 'rmsatuan_nama',
				'formatter' => function ($d, $row){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'a.rmharga_hargagr', 
				'dt' => 6, 'field' => 'rmharga_hargagr',
				'formatter' => function ($d){
                    $d = custom_numberformat($d, 2, 'IDR');
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'a.rmharga_hargainput', 
				'dt' => 7, 'field' => 'rmharga_hargainput',
				'formatter' => function ($d, $row){
                    $d = custom_numberformat($d, 2, 'IDR');
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'a.rmharga_tgledit_hargainput', 
				'dt' => 8, 'field' => 'rmharga_tgledit_hargainput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),            
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] = "FROM ".$this->tbl_name." as a
                            LEFT JOIN tm_rawmaterial as b ON a.rm_kd=b.rm_kd
                            LEFT JOIN td_rawmaterial_satuan as c ON b.rmsatuan_kd=c.rmsatuan_kd";
		$data['where'] = "";
		
		return $data;
    }

    private function tbl_btn($id) {
        $btns = array();
        if (cek_permission('MSTHARGAMATERIAL_UPDATE')) {
            $btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_item(\''.$id.'\')'));
        }
        if (cek_permission('MSTHARGAMATERIAL_DELETE')) { 
            $btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'delete_item(\''.$id.'\')'));
        }
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
        $query = $this->db->select('MAX('.$this->p_key.') as maxID')
                ->get($this->tbl_name)
                ->row();
        $code = (int) $query->maxID + 1;
        return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }

    public function update_batch ($idColumn, $data = []) {
		$act = $this->db->update_batch($this->tbl_name, $data, $idColumn);
		return $act;
    }
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
    }
    
    public function get_where_in ($where, $in = []) {
        $query = $this->db->where_in($where, $in)
                ->select($this->tbl_name.'.*')
                ->get($this->tbl_name);
        return $query;
    }

    public function get_by_param_detail ($param=[]) {
		$result = $this->db->select()
                    ->from($this->tbl_name)
                    ->join('tm_rawmaterial', $this->tbl_name.'.rm_kd=tm_rawmaterial.rm_kd', 'left')
                    ->where($param)
                    ->get();
		return $result;
	}
    
    public function update_harga_gr ($data = []) {
        /** FORMAT */
        // $dataRM = [
        //     'rm_kd' => $rm_kd,
        //     'rm_hargagr' => $hargakonversi,
        //     'rmharga_tgledit_hargagr' => $now,
        // ];
        $act = false;
        $cekRM = $this->get_by_param(['rm_kd' => $data['rm_kd']])->num_rows();
        if (empty($cekRM)) {
            /** Belum ada data RM */
            $rmharga_kd = $this->create_code();
            $dataHarga = [
                'rmharga_kd' => $rmharga_kd,
                'rm_kd' => $data['rm_kd'],
                'rmharga_hargagr' => $data['rmharga_hargagr'],
                'rmharga_hargainput' => $data['rmharga_hargagr'],
                'rmharga_tgledit_hargagr' => $data['rmharga_tgledit_hargagr'],
            ];
            $act = $this->insert_data($dataHarga);
        }else {
            /** Update Harga GR */
            $dataUpdate = [
                'rmharga_hargagr' => $data['rmharga_hargagr'],
                'rmharga_tgledit_hargagr' => $data['rmharga_tgledit_hargagr'],
            ];
            $act = $this->update_data (['rm_kd' => $data['rm_kd']], $dataUpdate);
        }
        return $act;

    }

    public function update_harga_gr_batch ($data = []) {
        $arrRmkd = [];
        $arrExist = [];
        $arrExistkd = [];
        $actExist = true;
        $actNotExist = true;
        $resp = true;
        $dataNoSpecial = [];
        foreach ($data as $eData) {
            if ($eData['rm_kd'] == 'SPECIAL'){
                continue;
            }
            $dataNoSpecial[] = $eData;
        }
        /** Exist rm_kd */
        foreach ($dataNoSpecial as $r) {
            if (!in_array($r['rm_kd'], $arrRmkd)) {
                $arrRmkd[] = $r['rm_kd'];
            }
        }
        if (!empty($arrRmkd)) {
            $rmhargaExist = $this->get_where_in('rm_kd', $arrRmkd)->result_array();
            if (!empty($rmhargaExist)) {
                foreach ($dataNoSpecial as $r2) {
                    foreach ($rmhargaExist as $rExist) {
                        if ($rExist['rm_kd'] == $r2['rm_kd']) {
                            $arrExist[] = [
                                'rm_kd' => $r2['rm_kd'],
                                'rmharga_hargagr' => $r2['rmharga_hargagr'],
                                'rmharga_tgledit_hargagr' => $r2['rmharga_tgledit_hargagr'],
                            ];
                            if (!in_array($rExist['rm_kd'], $arrExistkd)) {
                                $arrExistkd[] = $rExist['rm_kd'];
                            }
                        }
                    }
                }
                $actExist = $this->update_batch('rm_kd', $arrExist);
            }
        }

        /** Not exist */
        $arrNotExistkd = [];
        $arrNotExist = [];
        $cekArrNotExsitBefIns = [];
        foreach ($dataNoSpecial as $r3) {
            if (!in_array($r3['rm_kd'], $arrExistkd)) {
                if (!in_array($r3['rm_kd'], $arrNotExistkd)) {
                    $arrNotExistkd[] = $r3['rm_kd'];
                }
            }
        }
        if (!empty($arrNotExistkd)) {
            $rmharga_kd = $this->create_code();
            foreach ($dataNoSpecial as $r4) {
                if (in_array($r4['rm_kd'], $arrNotExistkd)) {
                    if (!in_array($r4['rm_kd'], $cekArrNotExsitBefIns)) {
                        $arrNotExist[] = [
                            'rmharga_kd' => $rmharga_kd,
                            'rm_kd' => $r4['rm_kd'],
                            'rmharga_hargagr' => $r4['rmharga_hargagr'],
                            'rmharga_hargainput' => $r4['rmharga_hargagr'],
                            'rmharga_tglinput' => $r4['rmharga_tgledit_hargagr'],
                            'rmharga_tgledit_hargagr' => $r4['rmharga_tgledit_hargagr'],
                        ];
                        $cekArrNotExsitBefIns[] = $r4['rm_kd'];
                        $rmharga_kd++;
                    }
                }
            }
            $actNotExist = $this->insert_batch($arrNotExist);
        }

        if ($actExist && $actNotExist) {
            $resp = true;
        }else {
            $resp = false;
        }

        return $resp;
    }

}