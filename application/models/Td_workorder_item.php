<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_workorder_item extends CI_Model
{
	private $tbl_name = 'td_workorder_item';
	private $p_key = 'woitem_kd';

	public function __construct()
	{
		$this->load->model(['td_workorder_item_so', 'td_workorder_item_detail']);
	}

	public function ssp_table()
	{
	}

	public function insert_data($data)
	{
		$query = $this->db->insert($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_data($id)
	{
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		return $query ? TRUE : FALSE;
	}

	public function get_by_param($param = [])
	{
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all()
	{
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data($aWhere = [], $data)
	{
		$query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query ? TRUE : FALSE;
	}

	public function delete_by_param($param = [])
	{
		$query = $this->db->delete($this->tbl_name, $param);
		return $query ? TRUE : FALSE;
	}

	public function get_where_in($where, $in = [])
	{
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}

	public function insert_batch_data($data)
	{
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function update_batch($data = [])
	{
		$query = $this->db->update_batch($this->tbl_name, $data, $this->p_key);
		return $query ? TRUE : FALSE;
	}

	public function buat_kode($code)
	{
		// DWO190507000001
		$ident = 'DWO';
		$identtgl = date('ymd');
		if (empty($code)) {
			$this->db->select($this->p_key);
			$this->db->where('DATE(woitem_tglinput)', date('Y-m-d'));
			$this->db->order_by('woitem_tglinput', 'DESC');
			$query = $this->db->get($this->tbl_name);
			if ($query->num_rows() == 0) {
				$num = 0;
			} else {
				$lastkode = $query->result_array();
				$lastkode = max($lastkode);
				$num = (int) substr($lastkode[$this->p_key], -6);
			}
		} else {
			$num = (int) substr($code, -6);
		}
		$nextnumber = $num + 1;
		$data = $ident . $identtgl . str_pad($nextnumber, 6, "0", STR_PAD_LEFT);

		return $data;
	}

	public function buat_no_wo($jns_wo, $code)
	{
		if ($jns_wo == 'std') {
			if (empty($code)) {
				$act = $this->db->select('woitem_no_wo')
					->where('woitem_status', $jns_wo)
					->get($this->tbl_name);
				if ($act->num_rows() == 0) {
					$code = 0;
				} else {
					$act = $act->result();
					foreach ($act as $each) {
						$arrCode[] = (int) $each->woitem_no_wo;
					}
					$code = max($arrCode);
				}
			}
			$code = (int) $code + 1;
		} else if ($jns_wo == 'custom') {
			$curYear = date('y');
			if (empty($code)) {
				$this->db->select('MAX(woitem_no_wo) as last_custom');
				$this->db->where('woitem_status', $jns_wo);
				$act = $this->db->get($this->tbl_name);
				if ($act->num_rows() == 0) {
					$code = 'PJ' . $curYear . str_pad(0, 4, '0', STR_PAD_LEFT);
				} else {
					$act = $act->row();
					$code = $act->last_custom;
				}
			}
			$code = (int) substr($code, -4) + 1;
			$code = 'PJ' . $curYear . str_pad($code, 4, '0', STR_PAD_LEFT);
		}
		return $code;
	}

	public function get_by_detail_part_woitem($param)
	{
		$query = $this->db->select($this->tbl_name . '.*, tm_bom.*, tm_part_main.*, td_part_jenis.*, tm_barang.item_code, td_part.*')
			->from($this->tbl_name)
			->join('tm_bom', $this->tbl_name . '.bom_kd=tm_bom.bom_kd', 'left')
			->join('td_bom_detail', 'td_bom_detail.bom_kd=tm_bom.bom_kd', 'left')
			->join('tm_barang', 'tm_bom.kd_barang=tm_barang.kd_barang', 'left')
			->join('tm_part_main', 'tm_part_main.partmain_kd=td_bom_detail.partmain_kd', 'left')
			->join('tb_part_lastversion', 'tb_part_lastversion.partmain_kd=tm_part_main.partmain_kd', 'left')
			->join('td_part', 'td_part.part_kd=tb_part_lastversion.part_kd', 'left')
			->join('td_part_jenis', 'td_part_jenis.partjenis_kd=tm_part_main.partjenis_kd', 'left')
			->where($param)
			->get();
		return $query;
	}

	public function update_where_in($param, $arrWhere_in = [], $data = [])
	{
		$act = $this->db->where_in($param, $arrWhere_in)
			->update($this->tbl_name, $data);
		return $act;
	}

	// Untuk partjenis WO
	public function woitem_detail_partjenis($wo_kd)
	{
		$query = $this->db->select()
			->from($this->tbl_name)
			->join('td_workorder_item_detail', $this->tbl_name . '.woitem_kd=td_workorder_item_detail.woitem_kd', 'left')
			->join('td_part_jenis', 'td_workorder_item_detail.partjenis_kd=td_part_jenis.partjenis_kd', 'left')
			->where([$this->tbl_name . '.wo_kd' => $wo_kd, 'td_workorder_item_detail.woitemdetail_generatewo' => 'T'])
			->where($this->tbl_name . '.woitem_jenis <>', 'packing')
			->get();
		return $query;
	}

	public function update_prosesstatus($woitem_kds = [], $woitem_prosestatus)
	{
		foreach ($woitem_kds as $woitem_kd) {
			$data[] = [
				'woitem_kd' => $woitem_kd,
				'woitem_prosesstatus' => $woitem_prosestatus,
				'woitem_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			];
		}
		$act = $this->update_batch($data);
		return $act;
	}

	/** Untuk generate wo complete dengan perubahan :
	 * > generate wo diambil dari bomdetail
	 * > grouping wo diambil dari bomdetail
	 * > item custom item tidak di grouping
	 * > untuk itemqty = 0 tidak di proses wo
	 */
	public function insertBatch_woComplete($wo_kd): bool
	{
		define("CUSTOM_ITEM", "PRD020817000573");
		define("JNS_WO", "std");

		$resp = false;
		$woitemparts = $this->db->select()
			->from('td_workorder_item_so')
			->join('tm_bom', 'td_workorder_item_so' . '.bom_kd=tm_bom.bom_kd', 'left')
			->join('td_bom_detail', 'tm_bom.bom_kd=td_bom_detail.bom_kd', 'left')
			->join('tm_part_main', 'td_bom_detail.partmain_kd=tm_part_main.partmain_kd', 'left')
			->join('tb_part_lastversion', 'tm_part_main.partmain_kd=tb_part_lastversion.partmain_kd', 'left')
			->join('td_part_jenis', 'tm_part_main.partjenis_kd=td_part_jenis.partjenis_kd', 'left')
			->where('td_workorder_item_so.wo_kd', $wo_kd)
			->where('td_workorder_item_so.woitemso_qty >', 0)
			->get()->result();
		$woitemsos = $this->td_workorder_item_so->get_by_param(['wo_kd' => $wo_kd])->result();

		/** Group by kdbarang dan item std */
		$arrayGroupStds = [];
		foreach ($woitemparts as $woitempart) {
			if ($woitempart->woitemso_prosesstatus == 'editing' && $woitempart->woitemso_status == JNS_WO) {
				$arrayGroupStds[$woitempart->kd_barang][] = $woitempart;
			}
		}

		/** Break action  */
		if (empty($woitemparts) || empty($arrayGroupStds)) {
			return false;
			die;
		}

		$dataWoitems = [];
		$dataWoitemDetails = [];
		$woitem_kd = '';
		$woitemdetail_kd = '';
		foreach ($arrayGroupStds as $kd_barang => $elements) {
			// Bukan custom item
			if ($kd_barang != CUSTOM_ITEM) {
				$woitemso_qty = 0;
				foreach ($woitemsos as $woitemso) {
					if ($woitemso->kd_barang == $kd_barang) {
						$woitem_itemcode = $woitemso->woitemso_itemcode;
						$woitem_deskripsi = $woitemso->woitemso_itemdeskripsi;
						$woitem_dimensi = $woitemso->woitemso_itemdimensi;
						$woitemso_qty += $woitemso->woitemso_qty;
					}
				}

				$woitem_kd = $this->buat_kode($woitem_kd);
				$dataWoitems[] = [
					'woitem_kd' => $woitem_kd,
					'wo_kd' => $wo_kd,
					'woitem_itemcode' => $woitem_itemcode,
					'woitem_no_wo' => null,
					'woitem_qty' => $woitemso_qty,
					'woitem_jenis' => 'packing',
					'woitem_deskripsi' => $woitem_deskripsi,
					'woitem_dimensi' => $woitem_dimensi,
					'woitem_note' => 'WO Packing',
					'woitem_satuan' => 'unit',
					'woitem_status' => JNS_WO,
					'woitem_tipe' => 'so',
					'woitem_tglselesai' => null,
					'woitem_prosesstatus' => 'workorder',
					'woitem_tglinput' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				];


				foreach ($woitemsos as $woitemsoNotCustom) {
					if ($woitemsoNotCustom->kd_barang == $kd_barang && $woitemsoNotCustom->woitemso_prosesstatus == 'editing' && $woitemsoNotCustom->woitemso_status == JNS_WO) {
						$woitemdetail_kd = $this->td_workorder_item_detail->buat_kode($woitemdetail_kd);
						$dataWoitemDetails[] = [
							'woitemdetail_kd' => $woitemdetail_kd,
							'wo_kd' => $wo_kd,
							'kd_barang' => $kd_barang,
							'woitem_kd' => $woitem_kd,
							'woitemso_kd' => $woitemsoNotCustom->woitemso_kd,
							'part_kd' => null,
							'partjenis_kd' => null,
							'bom_kd' => $woitemsoNotCustom->bom_kd,
							'woitemdetail_qty' => $woitemsoNotCustom->woitemso_qty,
							'woitemdetail_generatewo' => 'T',
							'woitemdetail_wogrouping' => 'F',
							'woitemdetail_parent' => '',
							'woitemdetail_tglinput' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin'),
						];
					}
				}
			} else {
				// Item CUSTOM std
				foreach ($woitemsos as $woitemso) {
					if ($woitemso->kd_barang == CUSTOM_ITEM) {
						$woitem_itemcodeCustom = $woitemso->woitemso_itemcode;
						$woitem_deskripsi = $woitemso->woitemso_itemdeskripsi;
						$woitem_dimensi = $woitemso->woitemso_itemdimensi;
					}
				}
				foreach ($elements as $element) {
					$woitem_kd = $this->buat_kode($woitem_kd);
					$dataWoitems[] = [
						'woitem_kd' => $woitem_kd,
						'wo_kd' => $wo_kd,
						'woitem_itemcode' => $woitem_itemcodeCustom,
						'woitem_no_wo' => null,
						'woitem_qty' => $element->woitemso_qty,
						'woitem_jenis' => 'packing',
						'woitem_deskripsi' => $woitem_deskripsi,
						'woitem_dimensi' => $woitem_dimensi,
						'woitem_note' => 'WO Packing',
						'woitem_satuan' => 'unit',
						'woitem_status' => JNS_WO,
						'woitem_tipe' => 'so',
						'woitem_tglselesai' => null,
						'woitem_prosesstatus' => 'workorder',
						'woitem_tglinput' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					];

					$woitemdetail_kd = $this->td_workorder_item_detail->buat_kode($woitemdetail_kd);
					$dataWoitemDetails[] = [
						'woitemdetail_kd' => $woitemdetail_kd,
						'wo_kd' => $wo_kd,
						'kd_barang' => $kd_barang,
						'woitem_kd' => $woitem_kd,
						'woitemso_kd' => $element->woitemso_kd,
						'part_kd' => null,
						'partjenis_kd' => null,
						'bom_kd' => $element->bom_kd,
						'woitemdetail_qty' => $element->woitemso_qty,
						'woitemdetail_generatewo' => 'T',
						'woitemdetail_wogrouping' => 'F',
						'woitemdetail_tglinput' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					];
				}
			}
		}

		$this->db->trans_begin();
		$this->insert_batch_data($dataWoitems);
		$this->td_workorder_item_detail->insert_batch_data($dataWoitemDetails);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$resp = false;
		} else {
			$this->db->trans_commit();
			$resp = true;
		}

		return $resp;
	}

	/** Generate WO custom yaitu :
	 * - Dengan penambahan bisa ambil beberapa item project dalam BoM Custom 
	 * - Contoh penomoran project : PJ210001;  PJ210001-1/2; PJ210001-2/2
	 * */
	public function insertBatch_woCustom($wo_kd)
	{
		// $resp = false;
		$woitemso = $this->td_workorder_item_so->get_detail_bom_project(['td_workorder_item_so.wo_kd' => $wo_kd, 'td_workorder_item_so.woitemso_status' => 'custom', 'td_workorder_item_so.woitemso_prosesstatus' => 'editing'])->result_array();
		$woitemparts = $this->td_workorder_item_so->get_detail_part(['td_workorder_item_so.wo_kd' => $wo_kd, 'td_workorder_item_so.woitemso_status' => 'custom', 'td_workorder_item_so.woitemso_prosesstatus' => 'editing'])->result_array();

		if (!empty($woitemso)) {
			/** u/ hitung jumlah part per nom, unutk support generate no wo pj */
			$arrBomCount = [];
			foreach ($woitemparts as $ePartTrue) {
				if ($ePartTrue['partjenis_generatewo'] == 'F') {
					continue;
				}
				$arrBomCount[$ePartTrue['bom_kd']][] = $ePartTrue['part_kd'];
			}

			$jns_wo = 'custom';
			$woitem_kd = '';
			$woitemdetail_kd = '';
			foreach ($woitemso as $eWoitemso) {
				$woitem_kd = $this->buat_kode($woitem_kd);
				/** wopacking item */
				$arrayWorkorderitem[] = [
					'woitem_kd' => $woitem_kd,
					'wo_kd' => $wo_kd,
					'woitem_itemcode' => $eWoitemso['woitemso_itemcode'],
					'woitem_no_wo' => $eWoitemso['woitemso_nopj'],
					'woitem_qty' => $eWoitemso['woitemso_qty'],
					'woitem_jenis' => 'packing',
					'woitem_deskripsi' => $eWoitemso['woitemso_itemdeskripsi'],
					'woitem_dimensi' => $eWoitemso['woitemso_itemdimensi'],
					'woitem_note' => 'WO Packing',
					'woitem_satuan' => 'unit',
					'woitem_status' => $jns_wo,
					'woitem_tipe' => 'so',
					'woitem_tglselesai' => $eWoitemso['woitemso_tglselesai'],
					'woitem_prosesstatus' => 'workorder',
					'woitem_tglinput' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				];
				/** wopacking item detail*/
				$woitemdetail_kd = $this->td_workorder_item_detail->buat_kode($woitemdetail_kd);
				$arrayWorkorderitemdetail[] = [
					'woitemdetail_kd' => $woitemdetail_kd,
					'wo_kd' => $wo_kd,
					'kd_barang' => $eWoitemso['kd_barang'],
					'woitem_kd' => $woitem_kd,
					'woitemso_kd' => $eWoitemso['woitemso_kd'],
					'part_kd' => null,
					'partjenis_kd' => null,
					'bom_kd' => $eWoitemso['bom_kd'],
					'woitemdetail_qty' => $eWoitemso['woitemso_qty'],
					'woitemdetail_generatewo' => 'T',
					'woitemdetail_tglinput' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				];

				/** custom Part Detail */
				$inc = 1;
				foreach ($woitemparts as $eWoitempart) {
					if ($eWoitempart['woitemso_kd'] == $eWoitemso['woitemso_kd']) {
						$woitem_kd = $this->buat_kode($woitem_kd);

						if ($eWoitempart['partjenis_generatewo'] == 'T') {
							$sumItemPJ = count($arrBomCount[$eWoitempart['bom_kd']]);
							$inc = array_search($eWoitempart['part_kd'], $arrBomCount[$eWoitempart['bom_kd']]) + 1;
							$woitem_no_wo = $eWoitempart['woitemso_nopj'] . '-' . $inc . '/' . $sumItemPJ;

							$arrayWorkorderitem[] = [
								'woitem_kd' => $woitem_kd,
								'wo_kd' => $wo_kd,
								'woitem_itemcode' => $eWoitempart['woitemso_itemcode'],
								'woitem_no_wo' => $woitem_no_wo,
								'woitem_qty' => $eWoitempart['woitemso_qty'],
								'woitem_jenis' => 'part',
								'woitem_deskripsi' => null,
								'woitem_dimensi' => null,
								'woitem_note' => null,
								'woitem_satuan' => 'pcs',
								'woitem_status' => $eWoitempart['woitemso_status'],
								'woitem_tipe' => 'so',
								'woitem_tglselesai' => $eWoitempart['woitemso_tglselesai'],
								'woitem_prosesstatus' => 'workorder',
								'woitem_tglinput' => date('Y-m-d H:i:s'),
								'admin_kd' => $this->session->userdata('kd_admin'),
							];

							$woitemdetail_kd = $this->td_workorder_item_detail->buat_kode($woitemdetail_kd);
							$arrayWorkorderitemdetail[] = [
								'woitemdetail_kd' => $woitemdetail_kd,
								'wo_kd' => $wo_kd,
								'kd_barang' => $eWoitempart['kd_barang'],
								'woitem_kd' => $woitem_kd,
								'woitemso_kd' => $eWoitempart['woitemso_kd'],
								'part_kd' => $eWoitempart['part_kd'],
								'partjenis_kd' => $eWoitempart['partjenis_kd'],
								'bom_kd' => null,
								'woitemdetail_qty' => $eWoitempart['woitemso_qty'],
								'woitemdetail_generatewo' => $eWoitempart['partjenis_generatewo'],
								'woitemdetail_tglinput' => date('Y-m-d H:i:s'),
								'admin_kd' => $this->session->userdata('kd_admin'),
							];
						} else {
							$woitemdetail_kd = $this->td_workorder_item_detail->buat_kode($woitemdetail_kd);
							$arrayWorkorderitemdetail[] = [
								'woitemdetail_kd' => $woitemdetail_kd,
								'wo_kd' => $wo_kd,
								'kd_barang' => $eWoitempart['kd_barang'],
								'woitem_kd' => null,
								'woitemso_kd' => $eWoitempart['woitemso_kd'],
								'part_kd' => $eWoitempart['part_kd'],
								'partjenis_kd' => $eWoitempart['partjenis_kd'],
								'bom_kd' => null,
								'woitemdetail_qty' => $eWoitempart['woitemso_qty'],
								'woitemdetail_generatewo' => $eWoitempart['partjenis_generatewo'],
								'woitemdetail_tglinput' => date('Y-m-d H:i:s'),
								'admin_kd' => $this->session->userdata('kd_admin'),
							];
						}
					}
				}
			}

			
			$withkd_barang = [];
			$withoutkd_barang = [];

			// Divide the objects based on the 'category' value
			foreach ($arrayWorkorderitemdetail as $object) {
				$kd_barang = $object['part_kd'];

				if ($kd_barang !== null) {
					$withoutkd_barang[] = $object;
					
				} else {
					$withkd_barang[] = $object;
				}
			}

			for ($i=0; $i < COUNT($withkd_barang); $i++) { 
				for ($j=0; $j < COUNT($withoutkd_barang); $j++) { 
					if($withkd_barang[$i]['woitemso_kd'] === $withoutkd_barang[$j]['woitemso_kd']){
						$withoutkd_barang[$j]['woitemdetail_parent'] = $withkd_barang[$i]['woitemdetail_kd'];
					}
				}
				$withkd_barang[$i]['woitemdetail_parent'] = "";
			}

			$data['withkd_barang'] = $withkd_barang;
			$data['withoutkd_barang'] = $withoutkd_barang;
			$data_final = array_merge($withkd_barang, $withoutkd_barang);



			$this->db->trans_begin();
			$actWOitem = $this->insert_batch_data($arrayWorkorderitem);
			$actWOitemdetail = $this->td_workorder_item_detail->insert_batch_data($data_final);
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				$resp = false;
			} else {
				$this->db->trans_commit();
				$resp = true;
			 }
		} else {
			/** Tidak diproses */
			$resp = true;
		}
		return $resp;
	}

	/**
	 * Generate No WO
	 * 
	 */
	public function updateBatch_generateNoWO($wo_kd): bool
	{
		$qOrderWoitems = $this->db->select('a.woitem_kd, b.woitem_itemcode, b.woitem_jenis, b.woitem_qty')
			->from('td_workorder_item_detail as a')
			->join('td_workorder_item as b', 'b.woitem_kd=a.woitem_kd', 'left')
			->join('tm_barang as c', 'c.kd_barang=a.kd_barang', 'left')
			->where('a.wo_kd', $wo_kd)
			->where('a.woitemdetail_generatewo', 'T')
			->where('b.woitem_status', 'std')
			->group_by('b.woitem_kd')
			->order_by('c.item_code asc, a.partjenis_kd asc')
			->get()
			->result_array();

		$woitem_no_wo = '';
		$dataUpdate = [];
		foreach ($qOrderWoitems as $qOrderWoitem) {
			$woitem_no_wo = $this->td_workorder_item->buat_no_wo('std', $woitem_no_wo);
			$dataUpdate[] = [
				'woitem_kd' => $qOrderWoitem['woitem_kd'],
				'woitem_no_wo' => $woitem_no_wo,
			];
		}
		$act = true;
		if (!empty($dataUpdate)) {
			$act = $this->db->update_batch('td_workorder_item', $dataUpdate, 'woitem_kd');
		}
		return $act;
	}
}
