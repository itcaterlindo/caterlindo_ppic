<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_perbandinganharga_detail extends CI_Model {
	private $tbl_name = 'td_perbandinganharga_detail';
	private $p_key = 'perbandinganhargadetail_kd';

	// public function ssp_table($date) {
	// 	$data['table'] = $this->tbl_name;

	// 	$data['primaryKey'] = $this->p_key;

	// 	$data['columns'] = array(
	// 		array( 'db' => $this->p_key,
	// 			'dt' => 1, 'field' => $this->p_key,
	// 			'formatter' => function($d){
					
	// 				return $this->tbl_btn($d);
	// 			}),
	// 		array( 'db' => 'a.rmstok_tglinput', 
	// 			'dt' => 2, 'field' => 'rmstok_tglinput',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'b.po_no', 
	// 			'dt' => 3, 'field' => 'po_no',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
    //             }),
	// 		array( 'db' => 'a.rm_kd', 
	// 			'dt' => 4, 'field' => 'rm_kd',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
    //             }),
    //         array( 'db' => 'c.podetail_nama', 
	// 			'dt' => 5, 'field' => 'podetail_nama',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
    //         array( 'db' => 'c.podetail_deskripsi', 
	// 			'dt' => 6, 'field' => 'podetail_deskripsi',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
    //         array( 'db' => 'a.rmstok_qty', 
	// 			'dt' => 7, 'field' => 'rmstok_qty',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
    //         array( 'db' => 'a.rmstok_konversiqty', 
	// 			'dt' => 8, 'field' => 'rmstok_konversiqty',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 	);

	// 	$data['sql_details'] = sql_connect();
	// 	$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
	// 						LEFT JOIN tm_purchaseorder as b ON a.po_kd=b.po_kd
	// 						LEFT JOIN td_purchaseorder_detail as c ON a.podetail_kd=c.podetail_kd";
    //     $data['where'] = "DATE(a.rmstok_tglinput)='".$date."'";
		
	// 	return $data;
	// }

	// private function tbl_btn($id) {
	// 	$read_access = 1;
	// 	$update_access = 1;
	// 	$delete_access = 1;
	// 	$btns = array();
	// 	$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));	
	// 	$btn_group = group_btns($btns);

	// 	return $btn_group;
	// }

	public function create_code() {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_by_param_in($column, $data=[]) {
		$query = $this->db->where_in($column, $data)->get($this->tbl_name);
		return $query;
	}

	public function delete_by_param($param, $kode) {
		$query = $this->db->delete($this->tbl_name, array($param => $kode)); 
		return $query?TRUE:FALSE;
    }

}