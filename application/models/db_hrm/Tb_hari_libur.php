<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_hari_libur extends CI_Model {
	private $db_name = 'sim_hrm';
	private $tbl_name = 'tb_hari_libur';
	private $p_key = 'id';

	public function __construct() {
		$this->db = $this->load->database($this->db_name, TRUE);
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function getByDateBetween($startdate, $enddate){
		return $this->db->where('DATE(tgl_libur) >=', $startdate)
				->where('DATE(tgl_libur) <=', $enddate)
				->get($this->tbl_name);

		$act = $this->db->order_by($this->p_key)->get($this->tbl_name)->result_array();
		return $act;
	}

    public function get_all_where_range($tglAwal, $tglAkhir){
		$act = $this->db->order_by($this->p_key)
                ->where('tgl_libur >=', $tglAwal)
                ->where('tgl_libur <=', $tglAkhir)
                ->get($this->tbl_name)->result_array();
		return $act;
	}

}