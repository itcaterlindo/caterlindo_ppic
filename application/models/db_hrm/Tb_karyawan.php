<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_karyawan extends CI_Model {
	private $db_name = 'sim_hrm';
	private $tbl_name = 'tb_karyawan';
	private $p_key = 'id';

	public function __construct() {
		$this->db = $this->load->database($this->db_name, TRUE);
	}

	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function get_where_in ($param, $data=[]) {
		$query = $this->db->where_in($param, $data)
				->get($this->tbl_name);
		return $query;
	}

}