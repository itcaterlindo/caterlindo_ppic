<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_hari_efektif extends CI_Model {
	private $db_name = 'sim_hrm';
	private $tbl_name = 'tb_hari_efektif';
	private $p_key = 'id';

	public function __construct() {
		$this->db = $this->load->database($this->db_name, TRUE);
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

    public function get_all_where_range($tglAwal, $tglAkhir){
		$act = $this->db->order_by($this->p_key)
                ->where('tgl_hari_efektif >=', $tglAwal)
                ->where('tgl_hari_efektif <=', $tglAkhir)
                ->get($this->tbl_name)->result_array();
		return $act;
	}

    public function get_all_where($conds = '') {
		$this->db->from($this->tbl_name)
			->where($conds)
			->order_by('id DESC');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

    // Lead time jarak antara tgl A ke tgl B selain hari libur
    public function get_lead_time($tglAwal, $tglAkhir)
    {
        $param = [
			"tgl_hari_efektif >" => $tglAwal,
			"tgl_hari_efektif <=" => $tglAkhir,
			"status" => "Efektif"
		];
        $result = $this->tb_hari_efektif->get_all_where($param);
        return $result != null ? count($result) : 0;
    }

	public function getTglStartByLimit($enddate, $limit)
	{
		$result = $this->db->where('tgl_hari_efektif < ', $enddate)
			->where('status', 'Efektif')
			->order_by('tgl_hari_efektif', 'desc')
			->limit($limit)
			->get($this->tbl_name)
			->result_array();
		return !empty($result[$limit-1]['tgl_hari_efektif']) ? $result[$limit-1]['tgl_hari_efektif'] : '0000-00-00';
	}

}