<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class M_icon_currency extends CI_Model {
	private $nm_tabel = 'tb_icon_currency';

	public function get_icon() {
		$this->db->select('id, nm_currency, icon_currency, icon_unicode');
		$this->db->from($this->nm_tabel);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function get_currency($tipe, $icon) {
		if ($tipe == 'icon') :
			$this->db->select('icon_currency');
			$this->db->from($this->nm_tabel);
			$this->db->where(array('id' => $tipe));
			$query = $this->db->get();
			$row = $query->row();
			$icon = '<i class="fa '.$row->icon_currency.'"></i>';
		elseif ($tipe == 'text') :
			$icon = $icon;
		endif;
		return $icon;
	}
}