<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_tipeadmin_permission extends CI_Model {
	private $tbl_name = 'td_tipeadmin_permission';
	private $p_key = 'tipeadminpermission_kd';

    public function create_code() {
		$this->db->select('MAX('.$this->p_key.') AS code')
			->from($this->tbl_name);
		$query = $this->db->get();
		$query = $query->row();
		$urutan = $query->code;
		$angka = $urutan + 1;
		return $angka;
    }
    
	public function get_all(){
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
    }
    
    public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
    }
    
    public function insert_batch_data($data) {
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
    }
}