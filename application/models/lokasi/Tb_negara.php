<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_negara extends CI_Model {
	private $tbl_name = 'tb_negara';
	private $p_key = 'kd_negara';
	private $title_name = 'Data Negara';

	public function get_all_dropdown() {
		$this->db->from($this->tbl_name)
			->order_by($this->p_key.' ASC');
		$query = $this->db->get();
		$result = $query->result();
		$data[''] = '-- Pilih Negara --';
		if (!empty($result)) :
			foreach ($result as $row) :
				$data[$row->kd_negara] = $row->nm_negara;
			endforeach;
		endif;
		return $data;
	}

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){

					return $this->tbl_btn($d, $row[2]);
				} ),
			array( 'db' => $this->p_key, 'dt' => 2, 'field' => $this->p_key ),
			array( 'db' => 'nm_negara', 'dt' => 3, 'field' => 'nm_negara' ),
		);

		$data['sql_details'] = sql_connect();

		$data['joinQuery'] = "";

		$data['where'] = "";

		return $data;
	}

	private function tbl_btn($id, $var) {
		$read_access = $this->session->read_access;
		$update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah', 'icon' => 'pencil', 'onclick' => 'form_main(\''.$id.'\')'));
		$btns[] = get_btn_divider();
		$btn_group = group_btns($btns);

		return $btn_group;
	}
	public function insert_data($data)
	{
		$query = $this->db->insert($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function get_by_param($param = [])
	{
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all()
	{
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data($aWhere = [], $data)
	{
		$query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query ? TRUE : FALSE;
	}

	public function get_row($id = '') {
		$this->db->from($this->tbl_name)
			->where(array($this->p_key => $id));
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}

	public function get_data($id = '') {
		$row = $this->get_row($id);
		if (!empty($row)) :
			$data = array('kd_negara' => $row->kd_negara, 'nm_negara' => $row->nm_negara);
		else :
			$data = array('kd_negara' => '', 'nm_negara' => '');
		endif;
		return $data;
	}

	public function form_rules() {
		$rules = array(
			array('field' => 'txtNm', 'label' => 'Nama Negara', 'rules' => 'required'),
		);
		return $rules;
	}

	public function form_warning($datas = '') {
		$forms = array('txtNm');
		foreach ($datas as $key => $data) :
			$str[$data] = (!empty(form_error($forms[$key])))?buildLabel('warning', form_error($forms[$key], '"', '"')):'';
		endforeach;
		return $str;
	}

	private function create_code() {
		$this->db->select($this->p_key.' AS code')
			->from($this->tbl_name)
			->order_by($this->p_key.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = $code;
		endif;
		$angka = $urutan + 1;
		$primary = str_pad($angka, 3, '000', STR_PAD_LEFT);
		return $primary;
	}

	public function submit_data($data = '') {
		if (!empty($data[$this->p_key])) :
			$label = 'Mengubah '.$this->title_name;
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s')));
			$where = array($this->p_key => $data[$this->p_key]);
			$act = $this->update($submit, $where);
		else :
			$label = 'Menambahkan '.$this->title_name;
			$data[$this->p_key] = $this->create_code();
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s')));
			$act = $this->create($submit);
		endif;
		$str = $this->report($act, $label, $submit);
		return $str;
	}

	private function create($data = '') {
		$act = $this->db->insert($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	private function update($data = '', $where = '') {
		$act = $this->db->update($this->tbl_name, $data, $where);
		return $act?TRUE:FALSE;
	}

	public function report($act = '', $label = '', $data = '') {
		$this->load->model(array('m_builder'));
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
		endif;
		$this->m_builder->write_log($stat, $label, $data);
		return $str;
	}

	public function delete_data($id = '') {
		$act = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		$report = $this->report($act, 'Menghapus '.$this->title_name, array($this->p_key => $id));
		return $report;
	}
}