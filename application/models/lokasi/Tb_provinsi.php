<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_provinsi extends CI_Model {
	private $tbl_name = 'tb_provinsi';
	private $p_key = 'kd_provinsi';
	private $p_key_id= 'id';
	private $title_name = 'Data Provinsi';

	public function get_all_dropdown($negara_kd = '') {
		$this->db->from($this->tbl_name)
			->where(array('negara_kd' => $negara_kd))
			->order_by($this->p_key.' ASC');
		$query = $this->db->get();
		$result = $query->result();
		$data[''] = '-- Pilih Provinsi --';
		if (!empty($result)) :
			foreach ($result as $row) :
				$data[$row->kd_provinsi] = $row->nm_provinsi;
			endforeach;
		endif;
		return $data;
	}

	public function get_where_negara($negara_kd = '') {
		$this->db->from($this->tbl_name)
			->where(array('negara_kd' => $negara_kd));
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function dropdown_on_negara($negara_kd = '') {
		$result = $this->get_where_negara($negara_kd);
		$data = '<option value=\'\'>-- Pilih Provinsi --</option>';
		if (!empty($result)) :
			foreach ($result as $row) :
				$data .= '<option value=\''.$row->kd_provinsi.'\'>'.$row->nm_provinsi.'</option>';
			endforeach;
		endif;
		return $data;
	}

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key_id,
				'dt' => 1, 'field' => $this->p_key_id,
				'formatter' => function($d, $row){

					return $this->tbl_btn($d, $row[3]);
				} ),
			array( 'db' => 'a.'.$this->p_key, 'dt' => 2, 'field' => $this->p_key ),
			array( 'db' => 'b.nm_negara', 'dt' => 3, 'field' => 'nm_negara' ),
			array( 'db' => 'a.nm_provinsi', 'dt' => 4, 'field' => 'nm_provinsi' ),
		);

		$data['sql_details'] = sql_connect();

		$data['joinQuery'] = "FROM ".$this->tbl_name." a LEFT JOIN tb_negara b ON b.kd_negara = a.negara_kd";

		$data['where'] = "a.negara_kd = ".$_SESSION['modul_lokasi']['kd_negara'];

		return $data;
	}

	public function ssp_table_p() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key_id,
				'dt' => 1, 'field' => $this->p_key_id,
				'formatter' => function($d, $row){

					return $this->tbl_btn($d, $row[3]);
				} ),
			array( 'db' => 'a.'.$this->p_key, 'dt' => 2, 'field' => $this->p_key ),
			array( 'db' => 'b.nm_negara', 'dt' => 3, 'field' => 'nm_negara' ),
			array( 'db' => 'a.nm_provinsi', 'dt' => 4, 'field' => 'nm_provinsi' ),
		);

		$data['sql_details'] = sql_connect();

		$data['joinQuery'] = "FROM ".$this->tbl_name." a LEFT JOIN tb_negara b ON b.kd_negara = a.negara_kd";
		$data['where'] = "";

		return $data;
	}

	private function tbl_btn($id, $var) {
		$read_access = $this->session->read_access;
		$update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah', 'icon' => 'pencil', 'onclick' => 'form_mainp(\''.$id.'\')'));
		$btns[] = get_btn_divider();
		$btn_group = group_btns($btns);

		return $btn_group;
	}
	public function insert_data($data)
	{
		$query = $this->db->insert($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}
	public function get_by_param($param = [])
	{
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all()
	{
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		
		}	
	public function get_row($id = '') 
	{
		$this->db->from($this->tbl_name)
			->where(array($this->p_key => $id, 'negara_kd' => $_SESSION['modul_lokasi']['kd_negara']));
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}

	public function get_data($id = '') {
		$row = $this->get_row($id);
		if (!empty($row)) :
			$data = array('kd_provinsi' => $row->kd_provinsi, 'negara_kd' => $row->negara_kd, 'nm_provinsi' => $row->nm_provinsi);
		else :
			$data = array('kd_provinsi' => '', 'negara_kd' => '', 'nm_provinsi' => '');
		endif;
		return $data;
	}

	public function form_rules() {
		$rules = array(
			array('field' => 'txtNm', 'label' => 'Nama Provinsi', 'rules' => 'required'),
		);
		return $rules;
	}

	public function form_warning($datas = '') {
		$forms = array('txtNm');
		foreach ($datas as $key => $data) :
			$str[$data] = (!empty(form_error($forms[$key])))?buildLabel('warning', form_error($forms[$key], '"', '"')):'';
		endforeach;
		return $str;
	}

	private function create_code() {
		$this->db->select($this->p_key.' AS code')
			->from($this->tbl_name)
			->where(array('negara_kd' => $_SESSION['modul_lokasi']['kd_negara']))
			->order_by($this->p_key.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = $code;
		endif;
		$angka = $urutan + 1;
		$primary = str_pad($angka, 3, '000', STR_PAD_LEFT);
		return $primary;
	}

	public function submit_data($data = '') {
		if (!empty($data[$this->p_key])) :
			$label = 'Mengubah '.$this->title_name;
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s')));
			$where = array($this->p_key => $data[$this->p_key], 'negara_kd' => $data['negara_kd']);
			$act = $this->update($submit, $where);
		else :
			$label = 'Menambahkan '.$this->title_name;
			$data[$this->p_key] = $this->create_code();
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s')));
			$act = $this->create($submit);
		endif;
		$str = $this->report($act, $label, $submit);
		return $str;
	}

	private function create($data = '') {
		$act = $this->db->insert($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	private function update($data = '', $where = '') {
		$act = $this->db->update($this->tbl_name, $data, $where);
		return $act?TRUE:FALSE;
	}
	public function update_data($aWhere = [], $data)
	{
		$query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query ? TRUE : FALSE;
	}

	public function report($act = '', $label = '', $data = '') {
		$this->load->model(array('m_builder'));
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
		endif;
		$this->m_builder->write_log($stat, $label, $data);
		return $str;
	}

	public function delete_data($id = '') {
		$act = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		$report = $this->report($act, 'Menghapus '.$this->title_name, array($this->p_key => $id));
		return $report;
	}
}