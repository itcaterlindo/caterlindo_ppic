<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_bom_state_log extends CI_Model {
	private $tbl_name = 'td_bom_state_log';
	private $p_key = 'bomstatelog_kd';

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }

    public function get_all () {
        return $this->db->get($this->tbl_name);
	}

	public function get_byparam_detail ($param = []) {
		$result = $this->db->select()
					->from($this->tbl_name)
					->join('tb_bom_state', $this->tbl_name.'.bomstate_kd=tb_bom_state.bomstate_kd', 'left')
					->join('tb_admin', $this->tbl_name.'.admin_kd=tb_admin.kd_admin', 'left')
					->where($param)
					->order_by($this->tbl_name.'.bomstatelog_tglinput', 'desc')
					->get();
		return $result;
	}

	// public function get_last_log($part_kd) {
	// 	$qmaxTrans = $this->db->select('a.part_kd, MAX(a.partstatelog_tglinput) AS tgl_maxtrans')
	// 			->from($this->tbl_name.' AS a')
	// 			->where('a.part_kd', $part_kd)
	// 			->group_by('a.partstate_kd')
	// 			->get_compiled_select();
				
	// 	$query = $this->db->select('aa.*, bb.partstate_nama, cc.kd_karyawan, cc.nm_admin')
	// 			->from($this->tbl_name.' AS aa')
	// 			->join('tb_part_state AS bb', 'aa.partstate_kd=bb.partstate_kd', 'left')
	// 			->join('tb_admin AS cc', 'aa.admin_kd=cc.kd_admin', 'left')
	// 			->join('('.$qmaxTrans.') as tt', 'aa.part_kd=tt.part_kd AND aa.partstatelog_tglinput=tt.tgl_maxtrans', 'right')
	// 			->where('aa.part_kd', $part_kd)
	// 			->get();
	// 	return $query;
	// }
	
}