<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_part_detailpipe extends CI_Model {
	private $tbl_name = 'td_part_detailpipe';
	private $p_key = 'partdetailpipe_kd';

	public function create_code() {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }

	public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

	public function insert_batch_data ($data=[]){
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}

	public function get_by_param_detail ($param=[]) {
		$query = $this->db->select($this->tbl_name.'.*, tm_rawmaterial.*')
				->from($this->tbl_name)
				->join('tm_rawmaterial', $this->tbl_name.'.rm_kd=tm_rawmaterial.rm_kd', 'left')
				->where($param)
				->get();
		return $query;
	}

	public function update_qty_detail($partdetail_kd) {
		$this->load->model(['td_part_detail']);
		$detailpipe = $this->get_by_param(['partdetail_kd' => $partdetail_kd]);
		$qty = 0;
		if ($detailpipe->num_rows() != 0){
			$rDetailpipe = $detailpipe->result_array();
			foreach ($rDetailpipe as $eDetailpipe) {
				$total = $eDetailpipe['partdetailpipe_panjang'] * $eDetailpipe['partdetailpipe_qty'];
				$qty += $total;
			}
		}
		$aData = [
			'partdetail_qty' => $qty,
			'partdetail_tgledit' => date('Y-m-d H:i:s')
		];
		$actUpdate = $this->td_part_detail->update_data(['partdetail_kd'=>$partdetail_kd ], $aData);
		return $actUpdate;
	}

}