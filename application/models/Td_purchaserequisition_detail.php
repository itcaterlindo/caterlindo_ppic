<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_purchaserequisition_detail extends CI_Model {
	private $tbl_name = 'td_purchaserequisition_detail';
	private $p_key = 'prdetail_kd';

	public function ssp_table($pr_kd) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'b.rm_kode', 
				'dt' => 2, 'field' => 'rm_kode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'CONCAT(COALESCE(a.prdetail_deskripsi,""), "/" , COALESCE(a.prdetail_spesifikasi,"") ) as concat_deskripsi', 
				'dt' => 3, 'field' => 'concat_deskripsi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.prdetail_qty', 
				'dt' => 4, 'field' => 'prdetail_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'c.rmsatuan_nama', 
				'dt' => 5, 'field' => 'rmsatuan_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.prdetail_konversi', 
				'dt' => 6, 'field' => 'prdetail_konversi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.prdetail_qtykonversi', 
				'dt' => 7, 'field' => 'prdetail_qtykonversi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.prdetail_duedate', 
				'dt' => 8, 'field' => 'prdetail_duedate',
				'formatter' => function ($d){
					$d = format_date($d, 'Y-m-d');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.prdetail_remarks', 
				'dt' => 9, 'field' => 'prdetail_remarks',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.prdetail_tglinput', 
				'dt' => 10, 'field' => 'prdetail_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." as a
                            LEFT JOIN tm_rawmaterial AS b ON a.rm_kd=b.rm_kd
							LEFT JOIN td_rawmaterial_satuan AS c ON a.rmsatuan_kd=c.rmsatuan_kd";
		$data['where'] = "a.pr_kd = '".$pr_kd."'";
		
		return $data;
	}

	private function tbl_btn($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btns = array();
		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data_detail(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data_detail(\''.$id.'\')'));	
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code($urutan = null) {
		if (empty($urutan)){
			$this->db->select($this->p_key.' AS code')
				->from($this->tbl_name)
				->where(array('DATE(prdetail_tglinput)' => date('Y-m-d')))
				->order_by('prdetail_tglinput DESC, '.$this->p_key.' DESC');
			$query = $this->db->get();
			$num = $query->num_rows();
			$urutan = 0;
			if ($num > 0) :
				$row = $query->row();
				$code = $row->code;
				$urutan = substr($code, -6);
			endif;
		}else{
			$urutan = substr($urutan, -6);
		}
		$angka = (int) $urutan + 1;
		$primary = 'DPR'.date('ymd').str_pad($angka, 6, '0', STR_PAD_LEFT);
		return $primary;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

	public function get_by_id_detail($id){
		$query = $this->db->select($this->tbl_name.'.*, td_rawmaterial_satuan.rmsatuan_nama as default_rmsatunnama')		
					->from($this->tbl_name)
					->join('tm_rawmaterial', $this->tbl_name.'.rm_kd=tm_rawmaterial.rm_kd', 'left')
					->join('td_rawmaterial_satuan', 'tm_rawmaterial.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
					->where($this->tbl_name.'.'.$this->p_key, $id)
					->get();
		return $query;
	}

	public function get_by_param_detail($param = []){
		$query = $this->db->select($this->tbl_name.'.*, td_rawmaterial_satuan.rmsatuan_nama, tm_rawmaterial.*')
					->from($this->tbl_name)
					->join('tm_rawmaterial', $this->tbl_name.'.rm_kd=tm_rawmaterial.rm_kd', 'left')
					->join('td_rawmaterial_satuan', $this->tbl_name.'.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
					->where($param)
					->get();
		return $query;
	}

	public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

	public function get_where_in_detail ($where, $in = []){
		$query = $this->db->select($this->tbl_name.'.*, td_rawmaterial_satuan.rmsatuan_nama, tm_purchaserequisition.*, tm_rawmaterial.*')
				->from($this->tbl_name)
				->join('td_rawmaterial_satuan', $this->tbl_name.'.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
				->join('tm_purchaserequisition', $this->tbl_name.'.pr_kd=tm_purchaserequisition.pr_kd', 'left')
				->join('tm_rawmaterial', $this->tbl_name.'.rm_kd=tm_rawmaterial.rm_kd', 'left')
				->where_in($where, $in)
				->get();
		return $query;
	}

	public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function summaryOutstandingDetailByRM($rm_kd)
	{
		$query = $this->db->select('a.rm_kd,a.prdetail_nama,a.prdetail_deskripsi,SUM(a.prdetail_qty) as prdetail_qty')
				->from('td_purchaserequisition_detail as a')
				->join('tm_purchaserequisition as b', 'a.pr_kd = b.pr_kd', 'LEFT')
				->where(array('b.pr_closeorder' => '0','a.rm_kd' => $rm_kd))
				->get()->row_array();
		return $query;
	}

}