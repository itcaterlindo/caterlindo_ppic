<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_finishgood_out extends CI_Model {
	private $tbl_name = 'td_finishgood_out';
	private $p_key = 'fgout_kd';

	public function ssp_table($kd_msalesorder, $kd_gudang) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'e.item_code', 
				'dt' => 2, 'field' => 'item_code',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.fgbarcode_barcode', 
				'dt' => 3, 'field' => 'fgbarcode_barcode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.fgout_qty_awal', 
				'dt' => 4, 'field' => 'fgout_qty_awal',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array( 'db' => 'a.fgout_qty', 
				'dt' => 5, 'field' => 'fgout_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array( 'db' => 'a.fgout_qty', 
				'dt' => 6, 'field' => 'fgout_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array( 'db' => 'a.fgout_tglinput', 
				'dt' => 7, 'field' => 'fgout_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM ".$this->tbl_name." as a 
								LEFT JOIN td_finishgood_in as b ON a.fgin_kd = b.fgin_kd
								LEFT JOIN td_finishgood_barcode as c ON b.fgbarcode_kd = c.fgbarcode_kd
								LEFT JOIN tm_finishgood as e ON b.fg_kd=e.fg_kd
								";
		$data['where'] = "a.kd_msalesorder = '".$kd_msalesorder."' AND e.kd_gudang = '".$kd_gudang."'";
		
		return $data;
	}

	public function ssp_table2 ($kd_msalesorder, $kd_gudang)
    {
		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		
		$data = [];
		$qData = $this->db->select($this->tbl_name.'.*, td_finishgood_barcode.fgbarcode_barcode, tm_finishgood.item_code')
				->from($this->tbl_name)
				->where('tm_finishgood.kd_gudang', $kd_gudang)
				->where($this->tbl_name.'.kd_msalesorder', $kd_msalesorder)
				->join('td_finishgood_in', 'td_finishgood_in.fgin_kd='.$this->tbl_name.'.fgin_kd', 'left')
				->join('td_finishgood_barcode', 'td_finishgood_barcode.fgbarcode_kd=td_finishgood_in.fgbarcode_kd', 'left')
				->join('tm_finishgood', 'tm_finishgood.fg_kd=td_finishgood_in.fg_kd', 'left')
				->get();
		foreach ($qData->result_array() as $r) {
			$data[] = [
				'1' => $this->tbl_btn($r[$this->p_key]),
				'2' => $r['item_code'],
				'3' => $r['fgbarcode_barcode'],
				'4' => $r['fgout_qty_awal'],
				'5' => $r['fgout_qty'],
				'6' => $r['fgout_qty_awal'] - $r['fgout_qty'],
				'7' => $r['fgout_tglinput'],
			];
		}
		
		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

	private function tbl_btn($id) {
		$delete_access = $this->session->delete_access;
		$btns = array();
		if($delete_access == 1 ){
			$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));
		}		
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function ssp_table_history($kd_msalesorder, $kd_gudang) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'd.item_code', 
				'dt' => 1, 'field' => 'item_code',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.fgbarcode_batch', 
				'dt' => 2, 'field' => 'fgbarcode_batch',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.fgbarcode_barcode', 
				'dt' => 3, 'field' => 'fgbarcode_barcode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.fgout_qty_awal', 
				'dt' => 4, 'field' => 'fgout_qty_awal',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array( 'db' => 'a.fgout_qty', 
				'dt' => 5, 'field' => 'fgout_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array( 'db' => 'a.fgout_qty', 
				'dt' => 6, 'field' => 'fgout_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM ".$this->tbl_name." as a 
								LEFT JOIN td_finishgood_in as b ON a.fgin_kd = b.fgin_kd
								LEFT JOIN td_finishgood_barcode as c ON b.fgbarcode_kd = c.fgbarcode_kd
								LEFT JOIN tm_barang as d ON c.barang_kd = d.kd_barang
								LEFT JOIN tm_finishgood as e ON b.fg_kd=e.fg_kd
								";
		$data['where'] = "a.kd_msalesorder = '".$kd_msalesorder."' AND e.kd_gudang = '".$kd_gudang."'";
		
		return $data;
	}


    public function buat_kode () {
		// KST190507000001
		$ident = 'OUT';
		$identtgl = substr(date('Y'), -2).date('m').date('d');

		$this->db->select($this->p_key);
		$this->db->where('DATE(fgout_tglinput)', date('Y-m-d'));
		$this->db->order_by('fgout_tglinput', 'DESC');
		$query = $this->db->get($this->tbl_name);
		if ($query->num_rows() == 0){
			$data = $ident.$identtgl.'000001';
		}else {
			$lastkode = $query->result_array();
			$lastkode = max($lastkode);
			$subs_laskode = substr($lastkode[$this->p_key], -6);
			$nextnumber = $subs_laskode + 1;
			$data = $ident.$identtgl.str_pad($nextnumber,6,"0",STR_PAD_LEFT);
		}
		return $data;
	}

    public function insert_data ($data){
        $query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_byId($id){
		$act = $this->db->select('a.*, b.*, c.*, d.*, e.*')
							->from($this->tbl_name.' as a')
							->join('td_finishgood_detail as b', 'a.fgout_kd=b.fgout_kd')
							->join('td_finishgood_in as c', 'b.fgin_kd=c.fgin_kd', 'left')
							->join('td_finishgood_barcode as d', 'c.fgbarcode_kd=d.fgbarcode_kd', 'left')
							->join('tm_finishgood as e', 'd.fg_kd=e.fg_kd', 'left')
							->where('a.'.$this->p_key, $id)
							->group_by('d.fgbarcode_kd')
							->get();
		return $act;
	}

	public function get_byBarcode($id){
		$act = $this->db->get_where($this->tbl_name, array('barcodefg_barcode'=>$id));
		return $act;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_rekap_by_kd_msalesorder ($kd_msalesorder) {
		$act = $this->db->select('d.kd_barang as barang_kd, d.item_code, SUM(a.fgout_qty) AS qtyout')
					->from($this->tbl_name.' as a')
					->join('td_finishgood_in as b', 'a.fgin_kd=b.fgin_kd')
					->join('td_finishgood_barcode as c', 'b.fgbarcode_kd=c.fgbarcode_kd')
					->join('tm_barang as d', 'c.barang_kd=d.kd_barang', 'left')
					->where('a.kd_msalesorder', $kd_msalesorder)
					->group_by('d.kd_barang')
					->get();
		return $act;
	}

	public function get_barcode_by_param($param=[]){
		$act = $this->db->select($this->tbl_name.'.*, td_finishgood_barcode.*, td_finishgood_in.*')
						->from($this->tbl_name)
						->join('td_finishgood_in', 'td_finishgood_out.fgin_kd=td_finishgood_in.fgin_kd', 'left')
						->join('td_finishgood_barcode', 'td_finishgood_in.fgbarcode_kd=td_finishgood_barcode.fgbarcode_kd', 'left')
						->where($param)
						->get();
		return $act;
	}

	public function get_by_date_item($startdate, $enddate, $barang_kd, $kd_gudang) {
		$this->load->model(['tm_barang']);
		$barang = $this->tm_barang->get_item(['kd_barang' => $barang_kd]);
		$itembarcode_custom = '899000';
		$this->db->select('a.*, b.fgbarcode_barcode, c.item_barcode, c.item_code, b.fgbarcode_batch, a.kd_msalesorder, f.nm_customer, aa.rakruangkolom_kd, e.no_salesorder, e.no_po, e.tipe_customer, c.barang_kd, b.fgbarcode_kd, g.item_group_kd')
					->from($this->tbl_name.' as a')
					->join('td_finishgood_in as aa', 'a.fgin_kd=aa.fgin_kd', 'left')
					->join('td_finishgood_barcode AS b', 'aa.fgbarcode_kd=b.fgbarcode_kd', 'left')
					->join('tm_finishgood AS c', 'aa.fg_kd=c.fg_kd', 'left')
					->join('tm_salesorder as e', 'a.kd_msalesorder=e.kd_msalesorder', 'left')
					->join('tm_customer as f', 'e.customer_kd=f.kd_customer', 'left')
					->join('tm_barang as g', 'g.kd_barang=c.barang_kd', 'left');
		/** Jika barang PJ (item group custom) tampilkan semua item code yang itemgroupnya custom */
		if($barang->item_group_kd == '2'){
			$this->db->where('c.item_barcode', $itembarcode_custom);
		}else{
			$this->db->where('c.barang_kd', $barang_kd);
		}
		$this->db->where('DATE(a.fgout_tglinput) >=', $startdate)
					->where('DATE(a.fgout_tglinput) <=', $enddate)
					->where('c.kd_gudang', $kd_gudang)
					->where('a.fgadj_kd IS NULL')
					->where('a.fgrepack_kd IS NULL')
					->where('a.fgdlv_kd IS NULL')
					->order_by('a.fgout_tglinput', 'DESC');
		$act = $this->db->get();
		return $act;
	}

	public function get_by_mso_gdg($kd_msalesorder, $gudang_kd){
		$query = $this->db->select('a.kd_msalesorder, a.fgout_tglinput ,a.fgout_qty_awal, a.fgout_qty, c.fgbarcode_batch, c.fgbarcode_barcode, d.item_code')
					->from($this->tbl_name . ' as a')
					->join('td_finishgood_in as b', 'a.fgin_kd = b.fgin_kd', 'left')
					->join('td_finishgood_barcode as c', 'b.fgbarcode_kd = c.fgbarcode_kd', 'left')
					->join('tm_barang as d', 'c.barang_kd = d.kd_barang', 'left')
					->join('tm_finishgood as e', 'b.fg_kd=e.fg_kd', 'left')
					->where('a.kd_msalesorder', $kd_msalesorder)
					->where('e.kd_gudang', $gudang_kd)
					->order_by('a.fgout_tglinput', 'desc')
					->get();
		return $query;
	}
}