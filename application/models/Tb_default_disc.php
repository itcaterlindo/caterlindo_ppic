<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_default_disc extends CI_Model {
	private $tbl_name = 'tb_default_disc';
	private $p_key = 'kd_default_disc';
	private $title_name = 'Data Default Discount';

	public function select_where($conds = '') {
		$this->db->from($this->tbl_name)
			->order_by('tgl_input ASC, '.$this->p_key.' ASC');
		if (!empty($conds)) :
			$this->db->where($conds);
		endif;
		$query = $this->db->get();
		$result = $query->row();
		return $result;
	}

	public function get_all($kd_jenis_customer = '') {
		$this->db->from($this->tbl_name)
			->where(array('jenis_customer_kd' => $kd_jenis_customer))
			->order_by('tgl_input ASC, '.$this->p_key.' ASC');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function get_all_bycustomer($kd_customer = '') {
		$this->db->from($this->tbl_name.' a')
			->join('tm_customer b', 'b.jenis_customer_kd = a.jenis_customer_kd', 'left')
			->where(array('b.kd_customer' => $kd_customer));
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function get_by_jeniscustomer($kd_jenis_customer = '') {
		$this->db->from($this->tbl_name)
			->where(array('jenis_customer_kd' => $kd_jenis_customer, 'status' => '1'))
			->order_by('tgl_input ASC, '.$this->p_key.' ASC');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function last_code_today() {
		$this->db->select($this->p_key.' AS last_code')
			->from($this->tbl_name)
			->where(array('DATE(tgl_input)' => date('Y-m-d')))
			->order_by('tgl_input DESC, '.$this->p_key.' DESC');
		$query = $this->db->get();
		$row = $query->row();
		$last_code = '';
		if (!empty($row)) :
			$last_code = $row->last_code;
		endif;
		return $last_code;
	}

	private function create_code($last_code = '') {
		$inc = 0;
		if (empty($last_code)) :
			$last_code = $this->last_code_today();
		endif;
		$inc = (int) substr($last_code, -3);
		$num = $inc + 1;
		$code = 'DSC'.date('dmy').str_pad($num, 3, '000', STR_PAD_LEFT);
		return $code;
	}

	public function submit_data($data = '') {
		$this->delete(array('jenis_customer_kd' => $data['jenis_customer_kd']));
		$kd_default_disc = $this->create_code();
		$no = 0;
		for ($i = 0; $i < count($data['nm_pihak']); $i++) :
			$jenis_customer_kd = $data['jenis_customer_kd'];
			$kd_default_disc = $no == 0?$kd_default_disc:$this->create_code($kd_default_disc);
			$nm_pihak = $data['nm_pihak'][$i];
			$status = empty($data['status'][$i])?'0':$data['status'][$i];
			$jml_disc = empty($data['jml_disc'][$i])?'0':$data['jml_disc'][$i];
			$individual_disc = empty($data['individual_disc'][$i])?'0':$data['individual_disc'][$i];
			$view_access = $data['view_access'][$i];

			if (!empty($nm_pihak)) :
				$data_submit[] = array('kd_default_disc' => $kd_default_disc, 'jenis_customer_kd' => $jenis_customer_kd, 'nm_pihak' => $nm_pihak, 'status' => $status, 'jml_disc' => $jml_disc, 'individual_disc' => $individual_disc, 'view_access' => $view_access, 'tgl_input' => date('Y-m-d H:i:s'), 'admin_kd' => $this->session->kd_admin);
				$no++;
			endif;
		endfor;
		if (isset($data_submit)) :
			$act = $this->insert_batch($data_submit);
		else :
			$act = 0;
			$data_submit = array('' => '');
		endif;
		$str = $this->report_batch($act, 'Menambahkan '.$this->title_name, $data_submit);
		return $str;
	}

	private function insert_batch($data = '') {
		$act = $this->db->insert_batch($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	public function delete($data = '') {
		$act = $this->db->delete($this->tbl_name, $data);
		$report = $this->report_single($act, 'Menghapus '.$this->title_name, $data);
		return $report;
	}

	public function report_single($act = '', $label = '', $data = '') {
		$this->load->model(array('m_builder'));
		$str = $this->report($act, $label);
		$this->m_builder->write_log($str['stat'], $label, $data);
		return $str;
	}

	public function report_batch($act = '', $label = '', $data = '') {
		$this->load->model(array('m_builder'));
		$str = $this->report($act, $label);
		$this->m_builder->write_batch_log($str['stat'], $label, $data);
		return $str;
	}

	public function report($act = '', $label = '') {
		if ($act) :
			$str['stat'] = 'Berhasil';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$str['stat'] = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
		endif;
		return $str;
	}

	public function create_form_diskon($id = '') {
		$this->load->helper(array('form'));
		$result = $this->get_all($id);

		$no = 0;
		$form = '';
		if (!empty($result)) :
			foreach ($result as $row) :
				$no++;
				$form .= $this->get_form_diskon($no, $row->view_access, $row->nm_pihak, $row->status, $row->jml_disc, $row->individual_disc);
			endforeach;
		else :
			$form .= $this->get_form_diskon(1, 'nothing', '', '', '', '');
		endif;
		
		return $form;
	}

	public function get_form_diskon($no = '', $view_access = 'nothing', $nm_pihak = '', $status = '', $jml_disc = '', $individual = '') {
		if ($no == 1) :
			$btn = '<btn type="button" id="idBtnTambahDiskon" name="btnTambahDiskon" class="btn btn-success btn-flat" title="Tambah Setting Default Diskon">
				<i class="fa fa-plus"></i>
			</btn>';
			$buka_form = '';
			$tutup_form = '';
		else :
			$btn = '<btn type="button" name="btnHapusDiskon" class="btn btn-warning btn-flat btn-hapus-diskon" title="Hapus Setting Default Diskon">
				<i class="fa fa-trash"></i>
			</btn>';
			$buka_form = '<div class="class-form-diskon" style="display: none;">';
			$tutup_form = '</div>';
		endif;

		$access_opt = array('nothing' => 'Tidak Ada', 'view_diskon_distributor' => 'Access Distributor', 'view_diskon_customer' => 'Access Customer');
		$access_attr = array('id' => 'idSelItem', 'class' => 'form-control');

		$form = $buka_form.'<div class="box-header" style="margin-top:-15px;">
			<h3 class="box-title">Setting Default Discount</h3>
			<div class="pull-right">
				'.$btn.'
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">View Access</label>
			<div class="col-sm-3 col-xs-12">
				'.form_dropdown('selDiscAccess[]', $access_opt, $view_access, $access_attr).'
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">Nama Pihak</label>
			<div class="col-sm-3 col-xs-12">
				<div id="idErrNamaPihakDua"></div>
				'.form_input(array('name' => 'txtNmPihak[]', 'class' => 'form-control', 'placeholder' => 'Nama Pihak', 'value' => $nm_pihak)).'
			</div>
			<label class="col-md-2 control-label">Status Diskon</label>
			<div class="col-sm-3 col-xs-12">
				<div id="idErrStatus"></div>
				'.form_dropdown('selStatus[]', array('' => '-- Pilih Status --', '0' => 'Tidak Aktif', '1' => 'Aktif'), $status, array('class' => 'form-control')).'
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">Jumlah Diskon (%)</label>
			<div class="col-sm-3 col-xs-12">
				<div id="idErrDiskon"></div>
				'.form_input(array('name' => 'txtJmlDiskon[]', 'class' => 'form-control', 'placeholder' => 'Jumlah', 'value' => $jml_disc)).'
			</div>
			<label class="col-md-2 control-label">Individual Discount</label>
			<div class="col-sm-3 col-xs-12">
				<div id="idErrIndividual"></div>
				'.form_dropdown('selIndividual[]', array('' => '-- Pilih Status --', '0' => 'Tidak Aktif', '1' => 'Aktif'), $individual, array('class' => 'form-control')).'
			</div>
		</div>'.$tutup_form;

		return $form;
	}
}