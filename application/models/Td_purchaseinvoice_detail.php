<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_purchaseinvoice_detail extends CI_Model {
	private $tbl_name = 'td_purchaseinvoice_detail';
	private $p_key = 'purchaseinvoicedetail_kd';

	public function __construct() {
		parent::__construct();

    }

	public function create_code() {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_by_param1($param=[]) {
		$this->db->join('td_purchaseorder_detail', 'td_purchaseorder_detail.podetail_kd=td_purchaseinvoice_detail.podetail_kd','left');
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}
	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

	public function insert_batch($data) {
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

}