<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_rawmaterial_goodsreceive extends CI_Model {
	private $tbl_name = 'td_rawmaterial_goodsreceive';
	private $p_key = 'rmgr_kd';

	public function ssp_table($date, $rmgr_type=[]) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'a.rmgr_code_srj', 
				'dt' => 2, 'field' => 'rmgr_code_srj',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.batch', 
				'dt' => 3, 'field' => 'batch',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.rmgr_tgldatang', 
				'dt' => 4, 'field' => 'rmgr_tgldatang',
				'formatter' => function ($d){
                    $d = format_date($d, 'd-m-Y');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.po_no', 
				'dt' => 5, 'field' => 'po_no',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'd.rm_kode', 
				'dt' => 6, 'field' => 'rm_kode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'c.podetail_deskripsi', 
				'dt' => 7, 'field' => 'podetail_deskripsi',
				'formatter' => function ($d, $row){
					$d = "$d / $row[10]";
					if ($row[9] == 'BACK') {
						$d = "$row[10] / $row[11]";
					}elseif($row[9] == 'DN'){
						$d = "$row[13] / $row[14]";
					}
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.rmgr_ket', 
				'dt' => 8, 'field' => 'rmgr_ket',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.rmgr_nosrj', 
				'dt' => 9, 'field' => 'rmgr_nosrj',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.rmgr_qty', 
				'dt' => 10, 'field' => 'rmgr_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.rmgr_qtykonversi', 
				'dt' => 11, 'field' => 'rmgr_qtykonversi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.rmgr_type', 
				'dt' => 12, 'field' => 'rmgr_type',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'c.podetail_spesifikasi', 
				'dt' => 13, 'field' => 'podetail_spesifikasi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'e.whdeliveryorderdetail_deskripsi', 
				'dt' => 14, 'field' => 'whdeliveryorderdetail_deskripsi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'e.whdeliveryorderdetail_spesifikasi', 
				'dt' => 15, 'field' => 'whdeliveryorderdetail_spesifikasi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'd.rm_deskripsi', 
				'dt' => 16, 'field' => 'rm_deskripsi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'd.rm_spesifikasi', 
				'dt' => 17, 'field' => 'rm_spesifikasi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$impl = implode("', '", $rmgr_type);
		$sRmgr_type = "'".$impl."'";

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
							LEFT JOIN tm_purchaseorder as b ON a.po_kd=b.po_kd
							LEFT JOIN td_purchaseorder_detail as c ON a.podetail_kd=c.podetail_kd
							LEFT JOIN tm_rawmaterial as d ON a.rm_kd=d.rm_kd
							LEFT JOIN td_whdeliveryorder_detail as e ON a.whdeliveryorderdetail_kd=e.whdeliveryorderdetail_kd";
        $data['where'] = "DATE(a.rmgr_tgldatang)='".$date."' and a.rmgr_type IN (".$sRmgr_type.")";
		
		return $data;
	}

	public function ssp_table2($date, $rmgr_type=[]) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'a.rmgr_tgldatang', 
				'dt' => 2, 'field' => 'rmgr_tgldatang',
				'formatter' => function ($d){
                    $d = format_date($d, 'd-m-Y');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.po_no', 
				'dt' => 3, 'field' => 'po_no',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'd.rm_kode', 
				'dt' => 4, 'field' => 'rm_kode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'c.podetail_deskripsi', 
				'dt' => 5, 'field' => 'podetail_deskripsi',
				'formatter' => function ($d, $row){
					$d = "$d / $row[10]";
					if ($row[9] == 'BACK') {
						$d = "$row[10] / $row[11]";
					}elseif($row[9] == 'DN'){
						$d = "$row[13] / $row[14]";
					}
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.rmgr_ket', 
				'dt' => 6, 'field' => 'rmgr_ket',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.rmgr_nosrj', 
				'dt' => 7, 'field' => 'rmgr_nosrj',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.rmgr_qty', 
				'dt' => 8, 'field' => 'rmgr_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.rmgr_qtykonversi', 
				'dt' => 9, 'field' => 'rmgr_qtykonversi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.rmgr_type', 
				'dt' => 10, 'field' => 'rmgr_type',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'c.podetail_spesifikasi', 
				'dt' => 11, 'field' => 'podetail_spesifikasi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'e.whdeliveryorderdetail_deskripsi', 
				'dt' => 11, 'field' => 'whdeliveryorderdetail_deskripsi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'e.whdeliveryorderdetail_spesifikasi', 
				'dt' => 12, 'field' => 'whdeliveryorderdetail_spesifikasi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'd.rm_deskripsi', 
				'dt' => 13, 'field' => 'rm_deskripsi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'd.rm_spesifikasi', 
				'dt' => 14, 'field' => 'rm_spesifikasi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$impl = implode("', '", $rmgr_type);
		$sRmgr_type = "'".$impl."'";

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
							LEFT JOIN tm_purchaseorder as b ON a.po_kd=b.po_kd
							LEFT JOIN td_purchaseorder_detail as c ON a.podetail_kd=c.podetail_kd
							LEFT JOIN tm_rawmaterial as d ON a.rm_kd=d.rm_kd
							LEFT JOIN td_purchaseinvoice_detail as f ON a.rmgr_kd=f.rmgr_kd
							LEFT JOIN td_whdeliveryorder_detail as e ON a.whdeliveryorderdetail_kd=e.whdeliveryorderdetail_kd";
        $data['where'] = "ISNULL(f.rmgr_kd) AND a.rmgr_type = 'RECEIVE'";
		
		return $data;
	}

	private function tbl_btn($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		

		$this->db->where(['rmgr_kd' => $id]);
		$act = $this->db->get($this->tbl_name)->row_array();

		if($act['status_sap'] == '0'){
			$btns = array();
			$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));	
			$btn_group = group_btns($btns);
		}else{
			$btn_group ='';
		}
		return $btn_group;
	}
	public function tbl_btn_pushSAP($id) {
	
		$this->db->where(['rmgr_kd' => $id]);
		$act = $this->db->get($this->tbl_name)->row_array();

		if (cek_permission('GR_TO_SAP')) {
			if($act['status_sap'] == '0'){
				$btn_group ='<button type="button" onclick="push_to_sap(\''.$id.'\')" class="btn btn-p" disabled><i class="fa fa-arrow-up"></i>&nbsp;Push To SAP</button>';
			}else{
				$btn_group ='<button type="button" onclick="push_to_sap(\''.$id.'\')" class="btn btn-success" disabled><i class="fa fa-check"></i>&nbsp;Done To SAP</button>';
			}
		// $btns[] = get_btn(array('title' => 'Push SAP.', 'icon' => 'arrow-up', 'onclick' => 'push_to_sap(\''.$id.'\')'));
		}else{
			return '';
		}
		

		return $btn_group;
	}

	public function ssp_table_grpurchase() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn_grpurchase($d);
				}),
			array( 'db' => 'a.rmgr_tgldatang', 
				'dt' => 2, 'field' => 'rmgr_tgldatang',
				'formatter' => function ($d){
                    $d = format_date($d, 'd-m-Y');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.po_no', 
				'dt' => 3, 'field' => 'po_no',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'd.rm_kode', 
				'dt' => 4, 'field' => 'rm_kode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'c.podetail_deskripsi', 
				'dt' => 5, 'field' => 'podetail_deskripsi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'k.suplier_nama', 
				'dt' => 6, 'field' => 'suplier_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.rmgr_nosrj', 
				'dt' => 7, 'field' => 'rmgr_nosrj',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.rmgr_qty', 
				'dt' => 8, 'field' => 'rmgr_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.rmgr_hargaunit', 
				'dt' => 9, 'field' => 'rmgr_hargaunit',
				'formatter' => function ($d){
					$d = formatRupiah($d);
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
							LEFT JOIN tm_purchaseorder as b ON a.po_kd=b.po_kd
							LEFT JOIN tm_suplier as k ON b.suplier_kd=k.suplier_kd
							LEFT JOIN td_purchaseorder_detail as c ON a.podetail_kd=c.podetail_kd
							LEFT JOIN tm_rawmaterial as d ON a.rm_kd=d.rm_kd";
        $data['where'] = "a.status_sap='1'";
		
		return $data;
	}

	private function tbl_btn_grpurchase($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btns = array();
		$btns[] = get_btn(array('title' => 'Edit Harga', 'icon' => 'pencil', 'onclick' => 'edit_data_grpurchase(\''.$id.'\')'));	
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
		$this->db->select($this->p_key.' AS code')
			->from($this->tbl_name)
			->where(array('DATE(rmgr_tglinput)' => date('Y-m-d')))
			->order_by('rmgr_tglinput DESC, '.$this->p_key.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -6);
		endif;
		$angka = $urutan + 1;
		$primary = 'MGR'.date('ymd').str_pad($angka, 6, '0', STR_PAD_LEFT);
		return $primary;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_by_param_join($param=[]) {
		$this->db->join('td_purchaseorder_detail', 'td_purchaseorder_detail.podetail_kd = td_rawmaterial_goodsreceive.podetail_kd', 'left');
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_where_in ($where, $in = []) {
		$query = $this->db->where_in($where, $in)
				->select($this->tbl_name.'.*')
				->get($this->tbl_name);
		return $query;
	}

	public function update_batch ($idColumn, $data = []) {
		$act = $this->db->update_batch($this->tbl_name, $data, $idColumn);
		return $act;
	}

	/** Report cek goods receive apakah sudah push SAP atau belum */
	public function get_rmgr_status_push_sap($tahun, $status)
	{
		$query = "SELECT a.rmgr_nosrj, b.po_no, a.rmgr_ket, GROUP_CONCAT( DISTINCT DATE_FORMAT(a.rmgr_tgldatang,'%Y-%m-%d') SEPARATOR ' | ' ) as rmgr_tgldatang, a.status_sap, e.suplier_kode, e.suplier_nama
				FROM td_rawmaterial_goodsreceive AS a
				LEFT JOIN tm_purchaseorder as b ON a.po_kd=b.po_kd
				LEFT JOIN td_purchaseorder_detail as c ON a.podetail_kd = c.podetail_kd
				LEFT JOIN tm_rawmaterial as d ON a.rm_kd=d.rm_kd
				LEFT JOIN tm_suplier as e ON b.suplier_kd = e.suplier_kd
				WHERE YEAR(a.rmgr_tgldatang) = ".$tahun." AND a.status_sap = ".$status." AND b.po_no IS NOT NULL
				GROUP by a.rmgr_nosrj
				ORDER by a.rmgr_tgldatang DESC";
		$data = $this->db->query($query)->result_array();
		return $data;
	}


	public function get_code_srj_valid($param)
	{
		$data = $this->db->select('*')
		->from('(SELECT * FROM td_rawmaterial_goodsreceive WHERE DATE(rmgr_tglinput) = DATE(NOW()) GROUP BY rmgr_nosrj ORDER BY rmgr_kd DESC LIMIT 1) AS B')
		->get()->result_array();


		$dataxx = $this->db->select('*')
		->from('td_rawmaterial_goodsreceive WHERE DATE(rmgr_tglinput) = DATE(NOW()) AND rmgr_nosrj ="'. $param .'" GROUP BY rmgr_nosrj')
		->get()->result_array();
		
		// $pp = str_replace('', '', $data[0]['A']);
		header('Content-Type: application/json');
		if(COUNT($data) != 0){
			$param = intval(substr($data[0]['rmgr_code_srj'], -2));
		}else{
			$param = '0';
		}
		$valid = $param + 1;
		$numPadded = sprintf("%02d", $valid);
		
		if($dataxx){
			return $dataxx[0]['rmgr_code_srj'];
		}else{
			return date("Ymd"). '-' .$numPadded;
		}
		
	}
	
}