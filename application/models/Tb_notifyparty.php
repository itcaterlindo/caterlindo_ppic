<?php
defined('BASEPATH') or exit('No direct script acccess allowed!');

class Tb_notifyparty extends CI_Model {
	private $tbl_name = 'tb_notifyparty';
	private $p_key = 'kd_notifyparty';
	private $title_name = 'Data Notify Party';

	/* --start ssp tabel untuk modul data warehouse-- */
	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){

					return $this->tbl_btn($d, $row[2]);
				} ),
			array( 'db' => 'a.'.$this->p_key, 'dt' => 2, 'field' => $this->p_key ),
			array( 'db' => 'a.nm_notifyparty', 'dt' => 3, 'field' => 'nm_notifyparty' ),
			array( 'db' => 'a.alamat', 'dt' => 4, 'field' => 'alamat',
				'formatter' => function($d, $row) {
					$nm_negara = empty($row[4])?'':$row[4].', ';
					$nm_provinsi = empty($row[5])?'':$row[5].', ';
					$nm_kota = empty($row[6])?'':$row[6].', ';
					$nm_kecamatan = empty($row[7])?'':$row[7].', ';
					$alamat = $nm_negara.$nm_provinsi.$nm_kota.$nm_kecamatan.$d;
					return $alamat;
				} ),
			array( 'db' => 'b.nm_negara', 'dt' => 5, 'field' => 'nm_negara' ),
			array( 'db' => 'c.nm_provinsi', 'dt' => 6, 'field' => 'nm_provinsi' ),
			array( 'db' => 'd.nm_kota', 'dt' => 7, 'field' => 'nm_kota' ),
			array( 'db' => 'e.nm_kecamatan', 'dt' => 8, 'field' => 'nm_kecamatan' ),
			array( 'db' => 'a.kode_pos', 'dt' => 9, 'field' => 'kode_pos' ),
		);

		$data['sql_details'] = sql_connect();

		$data['joinQuery'] = "
			FROM ".$this->tbl_name." a LEFT JOIN tb_negara b ON b.kd_negara = a.negara_kd
			LEFT JOIN tb_provinsi c ON c.negara_kd = b.kd_negara AND c.kd_provinsi = a.provinsi_kd 
			LEFT JOIN tb_kota d ON d.provinsi_kd = c.kd_provinsi AND d.negara_kd = b.kd_negara AND d.kd_kota = a.kota_kd 
			LEFT JOIN tb_kecamatan e ON e.kota_kd = d.kd_kota AND e.provinsi_kd = c.kd_provinsi AND e.negara_kd = b.kd_negara AND e.kd_kecamatan = a.kecamatan_kd
		";

		$data['where'] = "";

		return $data;
	}
	/* --end ssp tabel untuk modul data warehouse-- */

	/* --start button tabel untuk modul data warehouse-- */
	private function tbl_btn($id, $var) {
		$read_access = $this->session->read_access;
		$update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah', 'icon' => 'pencil', 'onclick' => 'get_form(\''.$id.'\')'));
		$btns[] = get_btn_divider();
		$btns[] = get_btn(array('access' => $delete_access, 'title' => 'Hapus', 'icon' => 'trash',
			'onclick' => 'return confirm(\'Anda akan menghapus data Notify Party = '.$var.'?\')?hapus_data(\''.$id.'\'):false'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}
	/* --end button tabel untuk modul data warehouse-- */

	/*public function notify_address() {
		$id = $this->get_id();
		$this->db->select('a.nm_notifyparty, a.alamat, a.kode_pos, b.nm_negara, c.nm_provinsi, d.nm_kota, e.nm_kecamatan')
			->from($this->tbl_name.' a')
			->join('tb_negara b', 'b.kd_negara = a.negara_kd', 'left')
			->join('tb_provinsi c', 'c.kd_provinsi = a.provinsi_kd AND b.kd_negara = c.negara_kd', 'left')
			->join('tb_kota d', 'd.kd_kota = a.kota_kd AND c.kd_provinsi = d.provinsi_kd AND b.kd_negara = d.negara_kd', 'left')
			->join('tb_kecamatan e', 'e.kd_kecamatan = a.kecamatan_kd AND d.kd_kota = e.kota_kd AND c.kd_provinsi = e.provinsi_kd AND b.kd_negara = e.negara_kd', 'left')
			->where(array('a.'.$this->p_key => $id));
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			$negara = $row->nm_negara.after_before_char($row->nm_negara, $row->nm_negara, '.', '');
			$provinsi = $row->nm_provinsi.after_before_char($row->nm_provinsi, $row->nm_negara, ', ', '.');
			$kota = $row->nm_kota.after_before_char($row->nm_kota, array($row->nm_provinsi, $row->nm_negara), ', ', '.');
			$kecamatan = $row->nm_kecamatan.after_before_char($row->nm_kecamatan, array($row->nm_kota, $row->nm_provinsi, $row->nm_negara), ', ', '.');
			$alamat = $row->alamat.after_before_char($row->alamat, $row->kode_pos, ', ', '.');
			$kode_pos = $row->kode_pos.after_before_char($row->kode_pos, $row->kode_pos, '.', '');
		else :
			$negara = 'Australia.';
			$provinsi = 'West Australia, ';
			$kota = 'Perth, ';
			$kecamatan = '';
			$alamat = '26 Howe Street Osborne Park, ';
			$kode_pos = '6017.';
		endif;
		return $alamat.$kode_pos.'<br>'.$kecamatan.$kota.$provinsi.$negara;
	}*/

	public function get_all() {
		$this->db->from($this->tbl_name);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function get_notify_address($notify_name = '') {
		if (empty($notify_name)) :
			$negara = '';
			$provinsi = '';
			$kota = '';
			$kecamatan = '';
			$alamat = '';
			$kode_pos = '-';
		else :
			$this->db->select('a.nm_notifyparty, a.alamat, a.kode_pos, b.nm_negara, c.nm_provinsi, d.nm_kota, e.nm_kecamatan')
				->from($this->tbl_name.' a')
				->join('tb_negara b', 'b.kd_negara = a.negara_kd', 'left')
				->join('tb_provinsi c', 'c.kd_provinsi = a.provinsi_kd AND b.kd_negara = c.negara_kd', 'left')
				->join('tb_kota d', 'd.kd_kota = a.kota_kd AND c.kd_provinsi = d.provinsi_kd AND b.kd_negara = d.negara_kd', 'left')
				->join('tb_kecamatan e', 'e.kd_kecamatan = a.kecamatan_kd AND d.kd_kota = e.kota_kd AND c.kd_provinsi = e.provinsi_kd AND b.kd_negara = e.negara_kd', 'left')
				->where(array('a.nm_notifyparty' => $notify_name));
			$query = $this->db->get();
			$row = $query->row();
			if (!empty($row)) :
				$negara = empty($row->nm_negara)?'':$row->nm_negara.', ';
				$provinsi = empty($row->nm_provinsi)?'':$row->nm_provinsi.', ';
				$kota = empty($row->nm_kota)?'':$row->nm_kota.', ';
				$kecamatan = empty($row->nm_kecamatan)?'':$row->nm_kecamatan.', ';
				$alamat = empty($row->alamat)?'':$row->alamat;
				$kode_pos = empty($row->kode_pos)?'.':' - '.$row->kode_pos.'.';
			else :
				$negara = 'Australia.';
				$provinsi = 'West Australia, ';
				$kota = 'Perth, ';
				$kecamatan = '';
				$alamat = '26 Howe Street Osborne Park, ';
				$kode_pos = '6017.';
			endif;
		endif;
		$alamat_full = $negara.$provinsi.$kota.$kecamatan.$alamat.$kode_pos;
		return $alamat_full;
	}

	public function get_id() {
		$this->db->from($this->tbl_name);
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			return $row->{$this->p_key};
		else :
			return '';
		endif;
	}

	public function get_row($id = '') {
		$this->db->from($this->tbl_name)
			->where(array($this->p_key => $id));
		$query = $this->db->get();
		$num = $query->num_rows();
		if ($num > 0) :
			$row = $query->row();
			$data = array('kd_notifyparty' => $row->kd_notifyparty, 'nm_notifyparty' => $row->nm_notifyparty, 'alamat' => $row->alamat, 'kecamatan_kd' => $row->kecamatan_kd, 'kota_kd' => $row->kota_kd, 'provinsi_kd' => $row->provinsi_kd, 'negara_kd' => $row->negara_kd, 'kode_pos' => $row->kode_pos);
		else :
			$data = array('kd_notifyparty' => '', 'nm_notifyparty' => '', 'alamat' => '', 'kecamatan_kd' => '', 'kota_kd' => '', 'provinsi_kd' => '', 'negara_kd' => '', 'kode_pos' => '');
		endif;
		return $data;
	}

	public function form_rules() {
		$rules = array(
			array('field' => 'txtNama', 'label' => 'Nama Notify Party', 'rules' => 'required'),
			array('field' => 'txtAlamat', 'label' => 'Alamat', 'rules' => 'required'),
			array('field' => 'txtKodePos', 'label' => 'Kode Pos', 'rules' => 'required|numeric'),
		);
		return $rules;
	}

	public function chk_notifyparty($nm_notifyparty = '', $kd_notifyparty = '') {
		$this->db->from($this->tbl_name)
			->where(array('nm_notifyparty' => $nm_notifyparty, 'kd_notifyparty !=' => $kd_notifyparty));
		$query = $this->db->get();
		$num = $query->num_rows();

		return $num > 0?FALSE:TRUE;
	}

	public function build_warning($datas = '') {
		$forms = array('txtNama', 'txtAlamat', 'txtKodePos');
		foreach ($datas as $key => $data) :
			$str[$data] = (!empty(form_error($forms[$key])))?buildLabel('warning', form_error($forms[$key], '"', '"')):'';
		endforeach;
		return $str;
	}

	private function create_code() {
		$this->db->select($this->p_key.' AS code')
			->from($this->tbl_name)
			->where(array('DATE(tgl_input)' => date('Y-m-d')))
			->order_by('tgl_input DESC, '.$this->p_key.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -6);
		endif;
		$angka = $urutan + 1;
		$primary = 'NTP'.date('dmy').str_pad($angka, 3, '000', STR_PAD_LEFT);
		return $primary;
	}

	public function submit_data($data = '', $tipe = '') {
		$chk_notifyparty = $this->chk_notifyparty($data['nm_notifyparty'], $data[$this->p_key]);
		if (!$chk_notifyparty) :
			$str = $this->report(0, 'Mengubah '.$this->title_name.' Nama Notify Party \''.$data['nm_notifyparty'].'\' sudah digunakan!', $data);
			return $str;
			exit();
		endif;
		if (!empty($data[$this->p_key])) :
			$label = 'Mengubah '.$this->title_name;
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s')));
			$where[$this->p_key] = $data[$this->p_key];
			$act = $this->update($submit, $where);
		else :
			$label = 'Menambahkan '.$this->title_name;
			$data[$this->p_key] = $this->create_code();
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s')));
			$act = $this->create($submit);
		endif;
		$str = $this->report($act, $label, $submit);
		$str[$this->p_key] = $data[$this->p_key];
		return $str;
	}

	private function create($data = '') {
		$act = $this->db->insert($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	private function update($data = '', $where = '') {
		$act = $this->db->update($this->tbl_name, $data, $where);
		return $act?TRUE:FALSE;
	}

	public function report($act = '', $label = '', $data = '') {
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['kd_mdo'] = $data[$this->p_key];
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
		endif;
		$this->m_builder->write_log($stat, $label, $data);
		return $str;
	}

	public function delete_data($id = '') {
		$act = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		$report = $this->report($act, 'Menghapus '.$this->title_name, array($this->p_key => $id));
		return $report;
	}
}