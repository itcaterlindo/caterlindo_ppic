<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class M_format_laporan extends CI_model{
	private $tbl_name = 'tb_format_laporan';

	public function check_laporan($var) {
		$this->db->from($this->tbl_name);
		$this->db->where(array('laporan_for' => $var));
		$query = $this->db->get();
		$num = $query->num_rows();

		return $num > 0?TRUE:FALSE;
	}

	public function read_data($var) {
		$this->db->from($this->tbl_name);
		$this->db->where(array('laporan_for' => $var));
		$query = $this->db->get();
		$num = $query->num_rows();
		$row = $query->row();

		return $row;
	}

	public function submit_laporan($var, $data = array()) {
		if ($var == 'mengubah') :
			$act = $this->update_data($data, array('laporan_for' => $data['laporan_for']));
		elseif ($var == 'menambahkan') :
			$act = $this->insert_data($data);
		endif;

		return $act;
	}

	public function insert_data($data) {
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s'));
		$submit = array_merge($data, $data_adm);
		$aksi = $this->db->insert($this->tbl_name, $submit);
		return $aksi?TRUE:FALSE;
	}

	public function update_data($data, $where) {
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s'));
		$submit = array_merge($data, $data_adm);
		$aksi = $this->db->update($this->tbl_name, $submit, $where);
		return $aksi?TRUE:FALSE;
	}

	public function view_laporan($var, $format) {
		$data = $this->read_data($var);
		$str['no_form'] = $data->no_form;
		$str['laporan_title'] = $data->laporan_title;
		$str['laporan_footer'] = $data->laporan_footer;
		if (!empty($format)) :
			if (strpos($str['laporan_footer'], '{term_payment}') !== FALSE) :
				$rep_footer = str_replace('{term_payment}', $format, $str['laporan_footer']);
				$str['laporan_footer'] = $rep_footer;
				$str['format'] = '';
			else :
				$str['laporan_footer'] = $str['laporan_footer'];
				$str['format'] = $format;
			endif; 
		endif;
		return $str;
	}
}