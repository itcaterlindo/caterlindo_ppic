<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_planningweekly extends CI_Model
{
    private $tbl_name = 'tm_planningweekly';
    private $p_key = 'planningweekly_kd';

    public function ssp_table()
    {
        $data['table'] = $this->tbl_name;

        $data['primaryKey'] = $this->p_key;

        $data['columns'] = array(
            array(
                'db' => $this->p_key,
                'dt' => 1, 'field' => $this->p_key,
                'formatter' => function ($d, $row) {

                    return $this->tbl_btn($d, $row[9]);
                }
            ),
            array(
                'db' => 'a.planningweekly_no',
                'dt' => 2, 'field' => 'planningweekly_no',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'd.bagian_nama as divisi_nama',
                'dt' => 3, 'field' => 'divisi_nama',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'b.bagian_nama',
                'dt' => 4, 'field' => 'bagian_nama',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.planningweekly_start',
                'dt' => 5, 'field' => 'planningweekly_start',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.planningweekly_end',
                'dt' => 6, 'field' => 'planningweekly_end',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'c.wfstate_nama',
                'dt' => 7, 'field' => 'wfstate_nama',
                'formatter' => function ($d, $row) {
                    $d = build_badgecolor($row[8], $d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.planningweekly_tgledit',
                'dt' => 8, 'field' => 'planningweekly_tgledit',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'c.wfstate_badgecolor',
                'dt' => 9, 'field' => 'wfstate_badgecolor',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'c.wfstate_action',
                'dt' => 10, 'field' => 'wfstate_action',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
        );

        $data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM " . $this->tbl_name . " as a
								LEFT JOIN tb_bagian as b ON a.bagian_kd=b.bagian_kd
                                LEFT JOIN td_workflow_state as c ON c.wfstate_kd=a.wfstate_kd
                                LEFT JOIN tb_bagian as d ON d.bagian_kd=a.divisi_kd";
        $kd_admin = $this->session->userdata('kd_admin');
        $tipe_admin_kd = $this->session->userdata('tipe_admin_kd');
        $bagians = $this->tb_deliverynote_user->get_bagianAllowed($kd_admin, $tipe_admin_kd);
        $sBagians = implode(", ", $bagians);
        $data['where'] = "a.bagian_kd IN ({$sBagians})";

        return $data;
    }

    public function ssp_table2()
    {
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $data = [];
        $kd_admin = $this->session->userdata('kd_admin');
        $tipe_admin_kd = $this->session->userdata('tipe_admin_kd');
        $bagians = $this->tb_deliverynote_user->get_bagianAllowed($kd_admin, $tipe_admin_kd);

        $qData = $this->db->select($this->tbl_name . '.*, tb_bagian.bagian_nama, div.bagian_nama as div_nama, td_workflow_state.*')
            ->from($this->tbl_name);
        if (!empty($bagians)) {
            $qData = $qData->where_in($this->tbl_name . '.divisi_kd', $bagians);
        }
        $qData = $qData->join('tb_bagian', $this->tbl_name . '.bagian_kd=tb_bagian.bagian_kd', 'left')
            ->join('td_workflow_state', $this->tbl_name . '.wfstate_kd=td_workflow_state.wfstate_kd', 'left')
            ->join('tb_bagian as div', $this->tbl_name . '.divisi_kd=div.bagian_kd', 'left')
            ->get();

        foreach ($qData->result_array() as $r) {
            $data[] = [
                '1' => $this->tbl_btn($r['planningweekly_kd'], $r['wfstate_action']),
                '2' => $r['planningweekly_no'],
                '3' => $r['div_nama'],
                '4' => $r['bagian_nama'],
                '5' => $r['planningweekly_start'],
                '6' => $r['planningweekly_end'],
                '7' => build_badgecolor($r['wfstate_badgecolor'], $r['wfstate_nama']),
                '8' => choice_enum($r['planningweekly_closed']),
                '9' => $r['planningweekly_tgledit'],
            ];
        }

        $output = array(
            'draw' => $draw,
            'recordsTotal' => count($data),
            'recordsFiltered' => count($data),
            'data' => $data
        );
        return $output;
    }

    private function tbl_btn($id, $wfstate_action)
    {
        $btns = array();
        if (!empty($wfstate_action)) {
            $exp = explode(';', $wfstate_action);
            if (in_array('view', $exp)) {
                $btns[] = get_btn(array('title' => 'Lihat Item', 'icon' => 'list', 'onclick' => 'lihat_data(\'' . $id . '\')'));
            }
            if (in_array('edit', $exp)) {
                if (cek_permission('WEEKLYPLANNING_UPDATE')) {
                    $btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\'' . $id . '\')'));
                }
            }
            if (in_array('print', $exp)) {
                $btns[] = get_btn(array('title' => 'Cetak Item', 'icon' => 'print', 'onclick' => 'cetak_data(\'' . $id . '\')'));
            }
            if (in_array('delete', $exp)) {
                if (cek_permission('WEEKLYPLANNING_DELETE')) {
                    $btns[] = get_btn_divider();
                    $btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\'' . $id . '\')'));
                }
            }
        }

        $btn_group = group_btns($btns);

        return $btn_group;
    }

    public function insert_data($data)
    {
        $query = $this->db->insert($this->tbl_name, $data);
        return $query ? TRUE : FALSE;
    }

    public function delete_data($id)
    {
        $query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
        return $query ? TRUE : FALSE;
    }

    public function get_by_param($param = [])
    {
        $this->db->where($param);
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    public function get_all()
    {
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    public function update_data($aWhere = [], $data)
    {
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
        return $query ? TRUE : FALSE;
    }

    public function create_code()
    {
        $query = $this->db->select('MAX(' . $this->p_key . ') as maxID')
            ->get($this->tbl_name)
            ->row();
        $code = (int) $query->maxID + 1;
        return $code;
    }

    public function generate_no()
    {
        $num = 1;
        $qSeq = $this->db->where('YEAR(planningweekly_tglinput)', date('Y'))
            ->order_by('planningweekly_kd', 'desc')
            ->get($this->tbl_name)->row_array();
        if (!empty($qSeq)) {
            $num = (int) substr($qSeq['planningweekly_no'], -3);
            $num++;
        }
        $woplanningNo = "WOPLANNING-" . date('Y') . '-' . str_pad($num, 3, '0', STR_PAD_LEFT);

        return $woplanningNo;
    }

    public function insert_batch($data)
    {
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
    }

    public function get_by_param_in($param, $params = [])
    {
        $this->db->where_in($param, $params);
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    public function get_by_param_bom_in($param, $params)
    {
        $query = $this->db->select($this->tbl_name . '.*, tm_bom.bom_kd')
            ->where_in($param, $params)
            ->join('tm_bom', $this->tbl_name . '.project_kd=tm_bom.project_kd', 'left')
            ->get($this->tbl_name);
        return $query;
    }

    public function getRowPlanningWeeklyBagianState($id)
    {
        $qRow = $this->db->select('tm_planningweekly.*, tb_bagian.bagian_nama, div.bagian_nama as divisi_nama, td_workflow_state.*, tb_admin.nm_admin')
            ->where('tm_planningweekly.planningweekly_kd', $id)
            ->join('tb_bagian', 'tb_bagian.bagian_kd=tm_planningweekly.bagian_kd', 'left')
            ->join('tb_bagian as div', 'div.bagian_kd=tm_planningweekly.divisi_kd', 'left')
            ->join('td_workflow_state', 'td_workflow_state.wfstate_kd=tm_planningweekly.wfstate_kd', 'left')
            ->join('tb_admin', 'tb_admin.kd_admin=tm_planningweekly.planningweekly_originator', 'left')
            ->get('tm_planningweekly')->row_array();
        return $qRow;
    }

    public function getPlanningweeklyBagian($param = [])
    {
        return $this->db->where($param)
            ->join('tb_bagian', 'tb_bagian.bagian_kd=tm_planningweekly.bagian_kd', 'left')
            ->get($this->tbl_name);
    }

    public function generate_button($id, $wf_kd, $wfstate_kd)
    {
        $admin_kd = $this->session->userdata('kd_admin');
        $admin = $this->db->where('kd_admin', $admin_kd)->get('tb_admin')->row_array();
        $kd_tipe_admin = $admin['tipe_admin_kd'];

        $transitions = $this->db->select('td_workflow_transition.*, td_workflow_transition_permission.wftransitionpermission_adminkd, td_workflow_transition_permission.kd_tipe_admin')
            ->from('td_workflow_transition')
            ->join('td_workflow_transition_permission', 'td_workflow_transition.wftransition_kd=td_workflow_transition_permission.wftransition_kd', 'left')
            ->where('td_workflow_transition.wf_kd', $wf_kd)
            ->where('td_workflow_transition.wftransition_source', $wfstate_kd)
            ->where('td_workflow_transition.wftransition_active', '1')
            ->get()->result_array();

        $html = '<div class="btn-group">
		<button type="button" class="btn btn-warning btn-flat dropdown-toggle" data-toggle="dropdown">
			<span class="fa fa fa-hand-o-right"></span>
			Ubah State
		  	<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">';
        $allowTransitions = [];
        foreach ($transitions as $transition) {
            if ($transition['wftransitionpermission_adminkd'] == $admin_kd || $transition['kd_tipe_admin'] == $kd_tipe_admin) {
                $allowTransitions[] = [
                    'wftransition_kd' => $transition['wftransition_kd'],
                    'wf_kd' => $transition['wf_kd'],
                    'wftransition_nama' => $transition['wftransition_nama'],
                    'wftransition_email' => $transition['wftransition_email'],
                ];
            }
        }
        #Group by wftransition_kd
        $tempArrayExist = [];
        $arrayGroups = [];
        foreach ($allowTransitions as $allowTransition) {
            if (!in_array($allowTransition['wftransition_kd'], $tempArrayExist)) {
                $arrayGroups[] = $allowTransition;
                $tempArrayExist[] = $allowTransition['wftransition_kd'];
            }
        }
        #after group
        foreach ($arrayGroups as $arrayGroup) {
            $html .= '<li><a href="javascript:void(0)" onclick=ubah_state("' . $id . '",' . $arrayGroup['wftransition_kd'] . ')> <i class="fa fa-hand-o-right"></i> ' . $arrayGroup['wftransition_nama'] . '</a></li>';
        }

        $html .= '
		</ul>
	  </div>';

        return $html;
    }

    public function checkRequisitionPlanning($planningweekly_kds = [])
    {
        if (empty($planningweekly_kds)) {
            return;
            die();
        }

        $planningweeklydetails = $this->db->select('tb_materialrequisition_detail_planningweekly.planningweekly_kd, td_materialrequisition_detail.planningweeklydetail_kd, 
                SUM(td_materialrequisition_detail.materialreqdetail_qty) as sum_materialreqdetail_qty, SUM(td_planningweekly_detail.planningweeklydetail_qty) as sum_planningweeklydetail_qty')
            ->join('td_materialrequisition_detail', 'td_materialrequisition_detail.materialreqdetailweekly_kd=tb_materialrequisition_detail_planningweekly.materialreqdetailweekly_kd', 'left')
            ->join('td_planningweekly_detail', 'td_planningweekly_detail.planningweeklydetail_kd=td_materialrequisition_detail.planningweeklydetail_kd', 'left')
            ->where_in('tb_materialrequisition_detail_planningweekly.planningweekly_kd', $planningweekly_kds)
            ->group_by('td_materialrequisition_detail.planningweeklydetail_kd, tb_materialrequisition_detail_planningweekly.planningweekly_kd')
            ->get('tb_materialrequisition_detail_planningweekly')->result_array();
        $arrayBalances = [];
        $qtyBalance = 0;
        foreach ($planningweeklydetails as $planningweeklydetail) {
            $qtyBalance = (float)$planningweeklydetail['sum_planningweeklydetail_qty'] - $planningweeklydetail['sum_materialreqdetail_qty'];
            $arrayBalances[$planningweeklydetail['planningweekly_kd']][] = (float)$qtyBalance;
        }
        $dataUpdateBatch = [];
        foreach ($arrayBalances as $ePlanningweekly_kd => $elementBalance) {
            /** Set to not closed */
            $planningweekly_closed = 0;
            if (max($elementBalance) == 0) {
                /** Set to closed */
                $planningweekly_closed = 1;
            }
            $dataUpdateBatch[] = [
                'planningweekly_kd' => $ePlanningweekly_kd,
                'planningweekly_closed' => $planningweekly_closed,
            ];
        }
        if (!empty($dataUpdateBatch)) {
            $this->db->update_batch($this->tbl_name, $dataUpdateBatch, 'planningweekly_kd');
        }

        return;
    }
}
