<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Model_product extends CI_Model {
	public function product_grid_data($group = '', $kat = '') {
		$this->db->select('a.kd_barang, a.item_barcode, a.item_code, a.hs_code, a.deskripsi_barang, a.dimensi_barang, b.harga_lokal_distributor, b.harga_lokal_retail, b.harga_lokal_reseller, b.harga_ekspor, a.netweight, a.grossweight, a.boxweight, a.length_cm, a.width_cm, a.height_cm, a.item_cbm')
			->from('tm_barang a')
			->join('td_barang_harga b', 'b.kd_harga_barang = a.harga_barang_kd AND a.kd_barang = b.barang_kd', "left")
			->where(array('group_barang_kd' => $group, 'kat_barang_kd' => $kat));
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function editable_grid($type = '', $group = '', $kat = '') {
		$table_row = '';
		$result = $this->product_grid_data($group, $kat);
		if (!empty($result)) :
			$no = 0;
			foreach ($result as $row) :
				$no++;
				$table_row .= '<tr>';
				$table_row .= '<td>'.$no.'</td>';
				$table_row .= '<td>'.$row->item_code.form_input(array('type' => 'hidden', 'name' => 'txtFormType', 'value' => $type)).form_input(array('type' => 'hidden', 'name' => 'txtKd[]', 'class' => 'item-code', 'value' => $row->kd_barang)).'</td>';
				if ($type == 'product_desc') :
					/* Jika type adalah product_desc maka kolom yang digunakan adalah kd_barang, item_code, item_barcode, hs_code, deskripsi_barang, dimensi_barang */
					$table_row .= '<td><div class="err-barcode"></div>'.form_input(array('name' => 'txtBarcode['.$row->kd_barang.']', 'class' => 'form-control item-barcode', 'value' => $row->item_barcode, 'readonly' => 'readonly')).form_input(array('type' => 'hidden', 'class' => 'val-jml-err-barcode', 'value' => '0')).'</td>';
					$table_row .= '<td>'.form_input(array('name' => 'txtHsCode['.$row->kd_barang.']', 'class' => 'form-control item-hscode', 'value' => $row->hs_code)).'</td>';
					$table_row .= '<td>'.form_input(array('name' => 'txtDesc['.$row->kd_barang.']', 'class' => 'form-control item-desc', 'value' => $row->deskripsi_barang)).'</td>';
					$table_row .= '<td>'.form_input(array('name' => 'txtDimension['.$row->kd_barang.']', 'class' => 'form-control item-dimension', 'value' => $row->dimensi_barang)).'</td>';
				elseif ($type == 'product_dimension') :
					/* Jika type adalah product_dimension maka kolom yang digunakan adalah kd_barang, item_code, grossweight, boxweight, netweight, length_cm, width_cm, height_cm, item_cbm */
					$table_row .= '<td>'.form_input(array('name' => 'txtGrossWeight['.$row->kd_barang.']', 'class' => 'form-control item-grossweight hitung_netweight', 'value' => $row->grossweight)).'</td>';
					$table_row .= '<td>'.form_input(array('name' => 'txtBoxWeight['.$row->kd_barang.']', 'class' => 'form-control item-boxweight hitung_netweight', 'value' => $row->boxweight)).'</td>';
					$table_row .= '<td>'.form_input(array('name' => 'txtNetWeight['.$row->kd_barang.']', 'class' => 'form-control item-netweight', 'value' => $row->netweight, 'readonly' => 'readonly')).'</td>';
					$table_row .= '<td>'.form_input(array('name' => 'txtLength['.$row->kd_barang.']', 'class' => 'form-control item-length hitung_cbm', 'value' => $row->length_cm)).'</td>';
					$table_row .= '<td>'.form_input(array('name' => 'txtWidth['.$row->kd_barang.']', 'class' => 'form-control item-width hitung_cbm', 'value' => $row->width_cm)).'</td>';
					$table_row .= '<td>'.form_input(array('name' => 'txtHeight['.$row->kd_barang.']', 'class' => 'form-control item-height hitung_cbm', 'value' => $row->height_cm)).'</td>';
					$table_row .= '<td>'.form_input(array('name' => 'txtItemCbm['.$row->kd_barang.']', 'class' => 'form-control item-cbm', 'value' => $row->item_cbm, 'readonly' => 'readonly')).'</td>';
				elseif ($type == 'product_price') :
					$this->db->where(array('id' => $this->session->kd_manage_items));
					$this->db->from('tb_set_dropdown');
					$q_set = $this->db->get();
					$r_set = $q_set->row();
					$manage_items = $r_set->nm_select;
					if ($manage_items == 'Lokal') :
						$harga_lokal = 'text';
						$kolom_lokal = '';
						$harga_ekspor = 'hidden';
						$kolom_ekspor = 'style="display: none;"';
					elseif ($manage_items == 'Ekspor') :
						$harga_lokal = 'hidden';
						$kolom_lokal = 'style="display: none;"';
						$harga_ekspor = 'text';
						$kolom_ekspor = '';
					elseif ($manage_items == 'Semua') :
						$harga_lokal = 'text';
						$kolom_lokal = '';
						$harga_ekspor = 'text';
						$kolom_ekspor = '';
					elseif ($manage_items == 'Tidak Ada') :
						$harga_lokal = 'hidden';
						$kolom_lokal = 'style="display: none;"';
						$harga_ekspor = 'hidden';
						$kolom_ekspor = 'style="display: none;"';
					endif;
					/* Jika type adalah product_dimension maka kolom yang digunakan adalah kd_barang, item_code, harga_lokal_distributor, harga_lokal_retail, harga_lokal_reseller, harga_ekspor */
					$table_row .= '<td '.$kolom_lokal.'>'.form_input(array('type' => $harga_lokal, 'name' => 'txtHargaRetail['.$row->kd_barang.']', 'class' => 'form-control item-retail-price', 'value' => $row->harga_lokal_retail)).'</td>';
					$table_row .= '<td '.$kolom_lokal.'>'.form_input(array('type' => $harga_lokal, 'name' => 'txtHargaReseller['.$row->kd_barang.']', 'class' => 'form-control item-reseller-price', 'value' => $row->harga_lokal_reseller)).'</td>';
					$table_row .= '<td '.$kolom_lokal.'>'.form_input(array('type' => $harga_lokal, 'name' => 'txtHargaDistributor['.$row->kd_barang.']', 'class' => 'form-control item-distributor-price', 'value' => $row->harga_lokal_distributor)).'</td>';
					$table_row .= '<td '.$kolom_ekspor.'>'.form_input(array('type' => $harga_ekspor, 'name' => 'txtHargaEkspor['.$row->kd_barang.']', 'class' => 'form-control item-ekspor-price', 'value' => $row->harga_ekspor)).'</td>';
				endif;
				$table_row .= '</tr>';
			endforeach;
		endif;
		return $table_row;
	}
}