<?php
defined('BASEPATH') or exit('No direct script acccess allowed!');

class Tb_tipe_teritory extends CI_Model {
	private $tbl_name = 'tb_tipe_teritory';
	private $p_key = 'id';
	private $title_name = 'Data Teritory';

	public function get_all() {
		$this->db->from($this->tbl_name);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function get_id() {
		$this->db->from($this->tbl_name);
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			return $row->{$this->p_key};
		else :
			return '';
		endif;
	}

	public function get_row($id = '') {
		$this->db->from($this->tbl_name)
			->where(array($this->p_key => $id));
		$query = $this->db->get();
		$num = $query->num_rows();
		if ($num > 0) :
			$row = $query->row();
			$data = array('id' => $row->id, 'kd_tipe_teritory' => $row->kd_tipe_teritory , 'nm_tipe_teritory' => $row->nm_tipe_teritory);
		else :
            $data = array('id' => "", 'kd_tipe_teritory' => "", 'nm_tipe_teritory' => "");	
        endif;
		return $data;
	}

}