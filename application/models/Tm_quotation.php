<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_quotation extends CI_Model {
	private $tbl_name = 'tm_quotation';
	private $p_key = 'kd_mquotation';
	private $title_name = 'Data Master Quotation';

	public function create_noquotation() {
		$this->load->model(array('tb_code_format', 'm_builder'));
		$this->load->helper(array('my_helper'));
		$row = $this->tb_code_format->get_code_for('quotation');
		if (!empty($row)) :
			$row_no = $this->get_last_noquotation($row->on_reset);
			$reset = $this->check_reset_noquotation($row->code_for, $row->on_reset);
			// Cari persamaan antara urutan angka dengan unformatted dan formatted
			$code = code_maker_new($row->code_format, $row_no, $row->code_separator, $reset);
		else :
			$code = '';
		endif;
		return $code;
	}

	public function get_last_noquotation($on_reset = '') {
		if ($on_reset == 'year') :
			$where = "YEAR(a.tgl_input) = '".date('Y')."' AND b.tgl_edit < a.tgl_input AND b.code_for = 'quotation'";
		elseif ($on_reset == 'month') :
			$where = "YEAR(a.tgl_input) = '".date('Y')."' AND MONTH(a.tgl_input) = '".date('m')."' AND b.tgl_edit < a.tgl_input AND b.code_for = 'quotation'";
		elseif($on_reset == 'day') :
			$where = "DATE(a.tgl_input) = '".date('Y-m-d')."' AND b.tgl_edit < a.tgl_input AND b.code_for = 'quotation'";
		endif;
		$this->db->select('a.no_quotation')
			->from($this->tbl_name.' AS a, tb_code_format AS b')
			->where($where)
			->group_by('a.no_quotation, a.tgl_input')
			->order_by('a.no_quotation DESC, a.tgl_input DESC')
			->limit(1);
		$query = $this->db->get();
		$row = $query->row();
		$no_quotation = !empty($row)?$row->no_quotation:'';
		return $no_quotation;
	}

	public function check_reset_noquotation($tipe_format = '', $resetter = '') {
		$where = '';
		if ($resetter == 'year') :
			$select = 'YEAR(tgl_input) as thn_input';
			$where = array('YEAR(tgl_input)' => date('Y'));
		elseif ($resetter == 'month') :
			$select = 'YEAR(tgl_input) as thn_input, MONTH(tgl_input) as bln_input';
			$where = array('YEAR(tgl_input)' => date('Y'), 'MONTH(tgl_input)' => date('m'));
		elseif ($resetter == 'day') :
			$select = 'DATE(tgl_input) as hari_input';
			$where = array('DATE(tgl_input)' => date('Y-m-d'));
		endif;

		// Cek dari variabel tabel, jika tidak ada data maka gunakan tabel code format untuk parameter reset
		$this->db->select($select)->from($this->tbl_name)->where($where);
		$query = $this->db->get();
		$num = $query->num_rows();
		$result = $query->row();
		if ($num < 1) :
			$this->db->select($select)->from('tb_code_format')->where(array('code_for' => $tipe_format));
			$query = $this->db->get();
			$result = $query->row();
		endif;
		if ($resetter == 'year') :
			$thn = $result->thn_input;
			$tgl = $thn;
			$compare = date('Y');
		elseif ($resetter == 'month') :
			$thn = $result->thn_input;
			$bln = $result->bln_input;
			$tgl = $thn.'-'.$bln;
			$compare = date('Y-m');
		elseif ($resetter == 'day') :
			$tgl = $result->hari_input;
			$compare = date('Y-m-d');
		endif;

		return strtotime($tgl) == strtotime($compare)?'0':'1';
	}

	public function get_header($kd_mquotation = '') {
		$this->db->select('a.kd_mquotation, a.no_quotation, a.email_sales, a.mobile_sales, a.tgl_quotation, a.status_quotation, a.nm_kolom_ppn, a.jml_ppn, b.nm_salesperson, c.nm_customer, c.no_telp_utama, c.no_telp_lain, c.email, c.alamat, e.nm_negara, f.nm_provinsi, g.nm_kota, h.nm_kecamatan, i.nm_select, j.term_payment_format, j.set_ppn, j.custom_barcode, k.nm_select AS tipe_customer, m.currency_kd, n.currency_type, n.icon_type, n.currency_icon, n.pemisah_angka, n.decimal, n.format_akhir')
			->from('tm_quotation a')
			->join('tb_salesperson b', 'b.kd_salesperson = a.salesperson_kd', 'left')
			->join('tm_customer c', 'c.kd_customer = a.customer_kd', 'left')
			->join('td_customer_alamat d', 'd.kd_alamat_kirim = a.alamat_kirim_kd', 'left')
			->join('tb_negara e', 'e.kd_negara = d.negara_kd', 'left')
			->join('tb_provinsi f', 'f.kd_provinsi = d.provinsi_kd AND f.negara_kd = d.negara_kd', 'left')
			->join('tb_kota g', 'g.kd_kota = d.kota_kd AND g.provinsi_kd = d.provinsi_kd AND g.negara_kd = d.negara_kd', 'left')
			->join('tb_kecamatan h', 'h.kd_kecamatan = d.kecamatan_kd AND h.kota_kd = d.kota_kd AND h.provinsi_kd = d.provinsi_kd AND h.negara_kd = d.negara_kd', 'left')
			->join('tb_set_dropdown i', 'i.id = d.kd_badan_usaha', 'left')
			->join('tb_jenis_customer j', 'j.kd_jenis_customer = c.jenis_customer_kd', 'left')
			->join('tb_set_dropdown k', 'k.id = j.kd_manage_items', 'left')
			->join('tb_set_dropdown l', 'l.id = j.kd_price_category', 'left')
			->join('tb_tipe_harga m', 'm.nm_harga = l.nm_select', 'left')
			->join('tm_currency n', 'n.kd_currency = m.currency_kd', 'left');
		if (is_array($kd_mquotation)) :
			$this->db->where_in('a.kd_mquotation', $kd_mquotation);
			$query = $this->db->get();
			$result = $query->result();
		else :
			$this->db->where(array('a.kd_mquotation' => $kd_quotation));
			$query = $this->db->get();
			$result = $query->row();
		endif;
		return $result;
	}
}