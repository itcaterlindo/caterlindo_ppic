<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_finishgood extends CI_Model {
	private $tbl_name = 'tm_finishgood';
	private $p_key = 'fg_kd';

	public function __construct(){
		parent::__construct();
	}

	public function ssp_table($tglprod) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'item_code', 
				'dt' => 2, 'field' => 'item code',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'barcodefg_batch', 
				'dt' => 3, 'field' => 'batch',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'barcodefg_barcode', 
				'dt' => 4, 'field' => 'barcode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'barcodefg_shift', 
				'dt' => 5, 'field' => 'shift',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'barcodefg_desc', 
				'dt' => 6, 'field' => 'barcodefg_desc',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'barcodefg_dimensi', 
				'dt' => 7, 'field' => 'barcodefg_dimensi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "";
		$data['where'] = "barcodefg_tglbatch = '".$tglprod."'";
		
		return $data;
	}

	private function tbl_btn($id) {
		// $read_access = $this->session->read_access;
		// $update_access = $this->session->update_access;
		// $delete_access = $this->session->delete_access;
		$btns = array();
		$btns[] = get_btn(array('title' => 'Cetak Barcode', 'icon' => 'print', 'onclick' => 'cetak_barcode(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'del_data(\''.$id.'\')'));
		
		$btn_group = group_btns($btns);

		return $btn_group;
	}


    public function buat_kode () {
		// MFG190507000001
		$ident = 'MFG';
		$identtgl = substr(date('Y'), -2).date('m').date('d');

		$this->db->select($this->p_key);
		$this->db->where('DATE(fg_tglinput)', date('Y-m-d'));
		$this->db->order_by('fg_tglinput', 'DESC');
		$query = $this->db->get($this->tbl_name);
		if ($query->num_rows() == 0){
			$data = $ident.$identtgl.'000001';
		}else {
			$lastkode = $query->result_array();
			$lastkode = max($lastkode);
			$subs_laskode = substr($lastkode[$this->p_key], -6);
			$nextnumber = $subs_laskode + 1;
			$data = $ident.$identtgl.str_pad($nextnumber,6,"0",STR_PAD_LEFT);
		}
		return $data;
	}

    public function insert_data ($data){
        $query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
    }
    
    public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_byId($id){
		$this->db->where($this->p_key, $id);
		$act = $this->db->get($this->tbl_name);
		return $act;
    }
    	
	public function get_byParam ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_stock($fg_kd, $qty, $status){
		$stockAkhir = $this->tm_finishgood->get_byId($fg_kd)->row_array();
		$stockAkhir = $stockAkhir['fg_qty'];
		if ($status == 'IN'){
			$stockUpdate = $stockAkhir + $qty;
		}elseif ($status == 'OUT'){
			$stockUpdate = $stockAkhir - $qty;
		}
		
		$data = array(
			'fg_qty' => $stockUpdate,
			'fg_tgledit' => date('Y-m-d H:i:s'),
			'admin_kd' => $this->session->userdata('kd_admin'),
		);
		$actStock = $this->update_data (['fg_kd'=>$fg_kd], $data);
		
		return $actStock;
	}

	public function getrow_by_barang_gdg($barang_kd, $kd_gudang){
		$act = $this->db->select('*')
					->from($this->tbl_name)
					->where('kd_gudang', $kd_gudang)
					->where('barang_kd', $barang_kd)
					->get();
		
		return $act;
	}

	public function get_where_kd_gudang($kd_gudang){
		$act = $this->db->select($this->tbl_name.'.*, tm_barang.deskripsi_barang, tm_barang.dimensi_barang, tm_barang.item_code')
					->from($this->tbl_name)
					->join('tm_barang', $this->tbl_name.'.barang_kd=tm_barang.kd_barang', 'left')
					->where('tm_finishgood.kd_gudang', $kd_gudang)
					->order_by($this->tbl_name.'.item_barcode')
					->get();
		return $act;
	}

	public function create_fg_kd($barang_kd){
		$this->load->model(['tm_barang']);

		$actBarang = $this->tm_barang->get_item(array('kd_barang'=> $barang_kd));
		$fg_kd = $this->buat_kode();
		$dataFG = array(
			'fg_kd' => $fg_kd,
			'kd_gudang' => $this->set_gudang,
			'barang_kd' => $barang_kd,
			'item_code' => $actBarang->item_code,
			'item_barcode' => $actBarang->item_group_kd == '2' ? '899000' : $actBarang->item_barcode, /** Jika barang custom atau PJ barcodenya '899000' */
			'fg_qty' => 0,
			'fg_tglinput' => date('Y-m-d H:i:s'),
			'fg_tgledit' => null,
			'admin_kd' => $this->session->userdata('kd_admin'),
		);
		$actInsertFG = $this->insert_data ($dataFG);
				
		return $fg_kd;
	}

    
}