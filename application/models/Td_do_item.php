<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_do_item extends CI_Model {
	private $tbl_name = 'td_do_item';
	private $p_key = 'kd_ditem_do';
	private $title_name = 'Data Item DO';

	public function get_from_so($mso_kd = '') {
		$this->db->from($this->tbl_name.' a')
			->join('tm_deliveryorder b', 'b.kd_mdo = a.mdo_kd', 'left')
			->where(array('b.msalesorder_kd' => $mso_kd));
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	/** Outputnya berupa array */
	public function get_from_so_array($mso_kd = '') {
		$this->db->from($this->tbl_name.' a')
			->join('tm_deliveryorder b', 'b.kd_mdo = a.mdo_kd', 'left')
			->where(array('b.msalesorder_kd' => $mso_kd));
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	public function count_from_so($mso_kd = '') {
		$this->db->select('SUM(stuffing_item_qty) as tot_qty')
			->from($this->tbl_name.' a')
			->join('tm_deliveryorder b', 'b.kd_mdo = a.mdo_kd', 'left')
			->where(array('b.msalesorder_kd' => $mso_kd));
		$query = $this->db->get();
		$row = $query->row();
		$qty = $row->tot_qty;
		return $qty;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function count_from_do($mdo_kd = '') {
		$this->db->select('SUM(stuffing_item_qty) as tot_qty')
			->from($this->tbl_name.' a')
			->join('tm_deliveryorder b', 'b.kd_mdo = a.mdo_kd', 'left')
			->where(array('b.kd_mdo' => $mdo_kd));
		$query = $this->db->get();
		$row = $query->row();
		$qty = $row->tot_qty;
		return $qty;
	}

	public function form_rules() {
		$rules = array(
			array('field' => 'txtKdMSo', 'label' => 'Kode Master SO', 'rules' => 'required'),
		);
		return $rules;
	}

	public function build_warning($datas = '') {
		$forms = array('txtKdMDo', 'txtKdMSo');
		foreach ($datas as $key => $data) :
			$str[$data] = (!empty(form_error($forms[$key])))?buildLabel('warning', form_error($forms[$key], '"', '"')):'';
		endforeach;
		return $str;
	}

	public function create_code($tambah = '') {
		$this->db->select($this->p_key.' AS p_key')
			->from($this->tbl_name)
			->where(array('DATE(tgl_input)' => date('Y-m-d')))
			->order_by('tgl_input DESC, '.$this->p_key.' DESC');
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			$primary_key = $row->p_key;
			$urutan = substr($primary_key, -6);
		else :
			$urutan = 0;
		endif;
		$angka = $urutan + $tambah;
		$code = 'DDI'.date('dmy').str_pad($angka, 6, '000', STR_PAD_LEFT);
		return $code;
	}

	public function submit_data($data = '') {
		if (empty($data['kd_mdo'])) :
			return $this->report(0, 'Menambahkan '.$this->title_name, $data, $data['kd_mdo'], $data['msalesorder_kd'], $data['so_count']);
		else :
			// $this->db->delete($this->tbl_name, array('mdo_kd' => $data['kd_mdo']));
			$jml_parent = is_array($data['parent_kd'])?count($data['parent_kd']):0;
			$jml_child = is_array($data['child_kd'])?count($data['child_kd']):0;
			$no = 0;
			if ($jml_parent > 0) :
				for($i = 0; $i < $jml_parent; $i++) :
					$no++;
					$kd_parent = $data['parent_kd'][$i];
					$submit_parent[] = array('kd_ditem_do' => $this->create_code($no), 'mdo_kd' => $data['kd_mdo'], 'parent_kd' => $kd_parent, 'item_barcode' => $data['item_barcode'][$kd_parent], 'stuffing_item_qty' => $data['stuffing_item_qty'][$kd_parent], 'item_note' => $data['item_note'][$kd_parent], 'admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s'));
				endfor;
				$act['parent'] = $this->create($submit_parent);
			endif;
			if ($jml_child > 0) :
				for($i = 0; $i < $jml_child; $i++) :
					$no++;
					$kd_child = $data['child_kd'][$i];
					$submit_child[] = array('kd_ditem_do' => $this->create_code($no), 'mdo_kd' => $data['kd_mdo'], 'parent_kd' => $data['child_parent'][$kd_child], 'child_kd' => $kd_child, 'item_barcode' => $data['item_barcode'][$kd_child], 'stuffing_item_qty' => $data['stuffing_item_qty'][$kd_child], 'item_note' => $data['item_note'][$kd_child], 'admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s'));
				endfor;
				$act['child'] = $this->create($submit_child);
			endif;
			if ($jml_parent > 0 || $jml_child > 0) :
				if ($jml_parent > 0 && $jml_child > 0) :
					$data_submit = array_merge($submit_parent, $submit_child);
				elseif (empty($submit_child)) :
					$data_submit = $submit_parent;
				elseif (empty($submit_parent)) :
					$data_submit = $submit_child;
				endif;
				$str = $this->report($act, 'Menambahkan '.$this->title_name, $data_submit, $data['kd_mdo'], $data['msalesorder_kd'], $data['so_count']);
			else :
				$str = $this->tm_deliveryorder->delete_data($data['kd_mdo']);
			endif;
			return $str;
		endif;
	}

	private function create($data = '') {
		$act = $this->db->insert_batch($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	public function report($act = '', $label = '', $data = '', $kd_mdo = '', $kd_mso = '', $so_count = '') {
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['kd_mdo'] = $kd_mdo;
			$str['kd_mso'] = $kd_mso;
			$str['so_count'] = $so_count;
			$str['do_count'] = $this->count_from_so($kd_mso);
			$str['status_akhir'] = 'finish';
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
		endif;
		return $str;
	}

	public function get_item_do ($kd_mdo) {
		$qParent  = $this->db->select('tm_deliveryorder.msalesorder_kd, tm_deliveryorder.no_invoice, tm_deliveryorder.gudang_kd,'.$this->tbl_name.'.*, tm_barang.hs_code, td_salesorder_item.item_code, td_salesorder_item.item_status, td_salesorder_item.item_desc, td_salesorder_item.item_dimension, 
					td_salesorder_item.netweight, td_salesorder_item.grossweight, td_salesorder_item.boxweight, td_salesorder_item.length_cm, td_salesorder_item.width_cm, td_salesorder_item.height_cm, td_salesorder_item.item_cbm, td_salesorder_item.item_qty, td_salesorder_item.item_note, '.$this->tbl_name.'.item_note item_note_do')
				->from($this->tbl_name)
				->join('td_salesorder_item', $this->tbl_name.'.parent_kd=td_salesorder_item.kd_ditem_so', 'left')
				->join('tm_deliveryorder', $this->tbl_name.'.mdo_kd=tm_deliveryorder.kd_mdo', 'left')
				->join('tm_barang', 'td_salesorder_item.barang_kd=tm_barang.kd_barang', 'left')
				->where($this->tbl_name.'.child_kd IS NULL')
				->where($this->tbl_name.'.mdo_kd', $kd_mdo)
				->get_compiled_select();

		$qChild  = $this->db->select('tm_deliveryorder.msalesorder_kd, tm_deliveryorder.no_invoice, tm_deliveryorder.gudang_kd,'.$this->tbl_name.'.*, tm_barang.hs_code, td_salesorder_item_detail.item_code, td_salesorder_item_detail.item_status, td_salesorder_item_detail.item_desc, td_salesorder_item_detail.item_dimension, 
					td_salesorder_item_detail.netweight, td_salesorder_item_detail.grossweight, td_salesorder_item_detail.boxweight, td_salesorder_item_detail.length_cm, td_salesorder_item_detail.width_cm, td_salesorder_item_detail.height_cm, td_salesorder_item_detail.item_cbm, td_salesorder_item_detail.item_qty, td_salesorder_item.item_note, '.$this->tbl_name.'.item_note item_note_do')
				->from($this->tbl_name)
				->join('td_salesorder_item_detail', $this->tbl_name.'.child_kd=td_salesorder_item_detail.kd_citem_so', 'left')
				->join('tm_deliveryorder', $this->tbl_name.'.mdo_kd=tm_deliveryorder.kd_mdo', 'left')
				->join('td_salesorder_item', $this->tbl_name.'.parent_kd=td_salesorder_item.kd_ditem_so', 'left')
				->join('tm_barang', 'td_salesorder_item.barang_kd=tm_barang.kd_barang', 'left')
				->where($this->tbl_name.'.child_kd IS NOT NULL')
				->where($this->tbl_name.'.mdo_kd', $kd_mdo)
				->get_compiled_select();
		$queryJoin = $qParent.' UNION ALL '.$qChild;
		$qResult = $this->db->query($queryJoin)->result_array();
		return $qResult;
	}

	/** Untuk update otomatis so yang sudah di DO */
	public function check_itemdoso_complete ($kd_mso) {
		$rParents = $this->db->select('td_salesorder_item.msalesorder_kd, td_salesorder_item.kd_ditem_so, td_salesorder_item.item_qty, SUM('.$this->tbl_name.'.stuffing_item_qty) as stuffing_item_qty')
				->from('td_salesorder_item')
				->join($this->tbl_name, $this->tbl_name.'.parent_kd=td_salesorder_item.kd_ditem_so', 'left')
				->where($this->tbl_name.'.child_kd IS NULL')
				->where('td_salesorder_item.msalesorder_kd', $kd_mso)
				->group_by('td_salesorder_item.kd_ditem_so')
				->get()->result();

		$rChilds = $this->db->select('td_salesorder_item_detail.msalesorder_kd, td_salesorder_item_detail.kd_citem_so, td_salesorder_item_detail.item_qty, SUM('.$this->tbl_name.'.stuffing_item_qty) as stuffing_item_qty')
				->from('td_salesorder_item_detail')
				->join($this->tbl_name, $this->tbl_name.'.child_kd=td_salesorder_item_detail.kd_citem_so', 'left')
				->where('td_salesorder_item_detail.msalesorder_kd', $kd_mso)
				->group_by('td_salesorder_item_detail.kd_citem_so')
				->get()->result();
		$aParent = [];
		foreach ($rParents as $eParent) {
			$aParent [] = $eParent->item_qty - $eParent->stuffing_item_qty;
		}
		$aChild = [];
		foreach ($rChilds as $eChild) {
			$aChild [] = $eChild->item_qty - $eChild->stuffing_item_qty;
		}
		$mResult = array_merge($aParent, $aChild);
		$maxResult = max($mResult);
		$resp = true;
		if ($maxResult > 0) {
			$resp = false;
		}
		return $resp;
	}
}