<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_suplier extends CI_Model {
	private $tbl_name = 'tm_suplier';
	private $p_key = 'suplier_kd';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'suplier_kode', 
				'dt' => 2, 'field' => 'suplier_kode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'suplier_nama', 
				'dt' => 3, 'field' => 'suplier_nama',
				'formatter' => function ($d , $row){
					$d = $this->badanusahaSuplier($row[4], $d);
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'suplier_alamat', 
				'dt' => 4, 'field' => 'suplier_alamat',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'ppn_sap', 
				'dt' => 5, 'field' => 'ppn_sap',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);
					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM ".$this->tbl_name;
		$data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id) {
		$btns = array();
		if (cek_permission('SUPPLIERDATA_UPDATE')) {
			$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
		}
		if (cek_permission('SUPPLIERDATA_DELETE')) {
			$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));	
		}
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function badanusahaSuplier($badanusaha_kd, $suplier_nama){
		$act = $this->tb_set_dropdown->get_by_param (array('id' => $badanusaha_kd));
		if ($act->num_rows() > 0){
			$act = $act->row_array();
			$badanusaha = $act['nm_select'];
			$badanusahaSuplier = $badanusaha.' '.$suplier_nama;
		}else{
			$badanusahaSuplier = $suplier_nama;
		}
		
		return $badanusahaSuplier;
	}

	public function generate_kd ($code) {
		if (empty($code)) {
			$query = $this->db->select('MAX('.$this->p_key.') as code')
						->from ($this->tbl_name)
						->get()->row();
			$code = $query->code;
		}
		$num = (int) substr($code, -6);
		$num = $num + 1;
		$code = 'SUP'.str_pad($num, 6, '0', STR_PAD_LEFT);
		return $code;
	}

	public function create_code($supliernama) {
		$supliernama = strtoupper(str_replace(' ', '', $supliernama));
		$suplier_kd = substr($supliernama, 0,3);

		$query = $this->db->select('MAX(suplier_kode) AS code')
			->from($this->tbl_name)
			->like('suplier_kode', $suplier_kd, 'after')
			->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -3);
		endif;
		$urutan++;
		$primary = $suplier_kd.str_pad($urutan, 3, '0', STR_PAD_LEFT);
		return $primary;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

	public function getLeadtime($sup = [], $filter, $bulan, $tahun, $po_no, $smt)
	{
		$new_sup = array();
        for($i=0; $i<count($sup); $i++){
            array_push($new_sup, '"'.$sup[$i].'"');
        }
        $new_sup = implode(", ", $new_sup);

		$real_output = array();
        $hitung__real_leadtime = array();
		$count_long = array();
		$count_on_late = [];
		$count_avg = array();
		$groups_count_avg = array();

		if($smt == '0'){
            $query  =   "SET @bulan1 := '". $bulan ."', @tahun := '". $tahun ."', @bulan2 := '". $bulan ."'";
            $this->db->query($query);
        }else{
            if($smt == '01'){
                $query  =   "SET @bulan1 := '01', @tahun := '". $tahun ."', @bulan2 := '06'";
                $this->db->query($query);
            }else{
                $query  =   "SET @bulan1 := '07', @tahun := '". $tahun ."',@bulan2 := '12'";
                $this->db->query($query);
            }
        }

        if($filter == 'po'){
            $query = $this->db->query("SELECT * FROM ( SELECT 
                                sp.suplier_kd, 
                                sp.suplier_nama,
								sp.suplier_leadtime,
                                po.po_tanggal, 
                                po.po_no,
                                pr.pr_no,
                                pr.pr_kd,
                                pr.pr_tglinput as req_date,
                                (SELECT MIN(prdetail_duedate) FROM td_purchaserequisition_detail INNER JOIN td_purchaseorder_detail ON td_purchaserequisition_detail.prdetail_kd = td_purchaseorder_detail.prdetail_kd WHERE td_purchaseorder_detail.po_kd = po.po_kd) AS ppic_plan,
                                rmgr.rmgr_tgldatang,
                                CONCAT(rm.rm_nama , '/' , rm.rm_deskripsi) AS rm_nama,
                                rm.rm_kd,
								rm.rmgroupsup_kd,
                                rmk.rmkategori_nama as material
                                FROM (SELECT rmgr_sive.* FROM td_rawmaterial_goodsreceive as rmgr_sive
										LEFT JOIN tm_purchaseorder po ON po.po_kd = rmgr_sive.po_kd
										LEFT JOIN tm_suplier sup ON sup.suplier_kd = po.suplier_kd
										GROUP BY po_kd) rmgr
 
                                INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd 
                                INNER JOIN tm_suplier sp ON sp.suplier_kd = po.suplier_kd
                                INNER JOIN td_purchaseorder_detail pod ON rmgr.podetail_kd = pod.podetail_kd
                                INNER JOIN td_purchaserequisition_detail prd ON prd.prdetail_kd = pod.prdetail_kd
                                INNER JOIN tm_purchaserequisition pr ON pr.pr_kd = prd.pr_kd
                                INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                                INNER JOIN td_rawmaterial_kategori rmk ON rm.rmkategori_kd = rmk.rmkategori_kd) AS Lead_time
                            
                            WHERE suplier_kd IN ($new_sup) AND 
									po_no IN (SELECT
										po.po_no
											FROM (SELECT rmgr_sive.* FROM td_rawmaterial_goodsreceive as rmgr_sive
											LEFT JOIN tm_purchaseorder po ON po.po_kd = rmgr_sive.po_kd
											LEFT JOIN tm_suplier sup ON sup.suplier_kd = po.suplier_kd
											GROUP BY po_kd) rmgr
											INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
											INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
											INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
											INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
										INNER JOIN td_purchaseorder_detail pod on rmgr.podetail_kd = pod.podetail_kd
										WHERE sup.suplier_kd IN ($new_sup) AND rm.rmgroupsup_kd IS NOT NULL AND MONTH(rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2 AND YEAR(rmgr_tgldatang) = @tahun
										GROUP BY po_no) 
							AND MONTH(rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2 AND YEAR(rmgr_tgldatang) = @tahun GROUP BY po_no ORDER BY rmgr_tgldatang ASC");

                            $data = $query->result();
							
                            foreach($data as $datas){
                                    array_push($hitung__real_leadtime, array(
                                        "suplier_kd" => $datas->suplier_kd,
                                        "rmgroupsup_kd" => $datas->rmgroupsup_kd,
                                        // // "material" => $datas->material,
                                        // // "leadtime" => $datas->suplier_leadtime,
                                        // "po_no" => $datas->po_no,
										"on_lt" =>  ($datas->suplier_leadtime > $this->tb_hari_efektif->get_lead_time($datas->po_tanggal, format_date($datas->rmgr_tgldatang, 'Y-m-d')) + 1) ? '1' : '0',
										// "pr_no" => $datas->pr_no,
										// "po_date" => $datas->po_tanggal,
										// "ppic_plan" => $datas->ppic_plan,
										// "pr_date" => format_date($datas->req_date, 'Y-m-d'),
										// "gr_plan" => $datas->ppic_plan,
										// "recive" => format_date($datas->rmgr_tgldatang, 'Y-m-d'),
										"real_leadtime" => $this->tb_hari_efektif->get_lead_time($datas->po_tanggal, format_date($datas->rmgr_tgldatang, 'Y-m-d')) + 1,
                                    ));
                            }

							unset($sup[0]);

							$filters = $sup;

							//digunakan untuk ambuk late_lt & on_lt
							function has_id($array, $id, $rmgroup) {
								foreach ($array as $index => $item) {
									if($item['suplier_kd'] == $id && $item['rmgroupsup_kd'] == $rmgroup) {
										return true;
									}
								}
								return false;
							}
							function get_array_index($array, $id, $rmgroup) {
								foreach ($array as $index => $item) {
									if($item['suplier_kd'] == $id && $item['rmgroupsup_kd'] == $rmgroup) {
										return $index;
									}
								}
							}
						
							foreach ($hitung__real_leadtime as $item) {
								if( in_array($item['suplier_kd'], $filters) ) {
									if( !has_id($count_on_late, $item['suplier_kd'],  $item['rmgroupsup_kd']) ) {
										array_push($count_on_late, array(
											"suplier_kd" => $item['suplier_kd'],
											"rmgroupsup_kd" => $item['rmgroupsup_kd'],
											'return_percent' => '',
											'Return' => '',
											'on_lt' => $item['on_lt'] == 1 ? 1 : 0,
											'late_lt' => $item['on_lt'] == 0 ? 1 : 0,
											'Lt' => '',
											'Lt_score' => '',
										));    
									}
									else {
										$index = get_array_index($count_on_late, $item['suplier_kd'], $item['rmgroupsup_kd']);
										if($item['on_lt'] == 1) {
											$count_on_late[$index]['on_lt'] += 1;
										}
										else {
											$count_on_late[$index]['late_lt'] += 1;
										}
									}
								}
							}


							// $group = array();
							//DI gunakan untuk ambil nilai long & short time

							$result = array();
							foreach ($hitung__real_leadtime as $element) {
								$result[$element['suplier_kd']][$element['rmgroupsup_kd']][] = $element['real_leadtime'];
							}
							foreach ($result as $key=>$value) {
								foreach($value as $kuy=>$val){
									array_push($count_long, array(
										"suplier_kd" =>$key,
										"rmgroupsup_kd" => strval($kuy),
										"short_lt" => min($val), 
										"long_lt" => max($val),
								));
								}
								// $group[] = $subarr;
								
								// echo $subarr[package]." minimum = ";
								// echo min($subarr[value])." and maximum = ";
								// echo max($subarr[value])."<br>";
							 }
							// foreach(  $hitung__real_leadtime as $product ) {
							// 	$comapy[] = $product['suplier_kd'];
							// 	$price[] = $product['real_leadtime'];
							// 	$rmgroup[] = $product['rmgroupsup_kd'];
							// }
							
							
							// $group = array();
							// foreach($comapy as $key=>$val){
							// 	// foreach($rmgroup as $kay=>$value){
							// 	 $group[$val][] = $price[$key];
							// 	//  $group[$val][$value][] = $price[$key];
							// 	// }
							// }
								
							// foreach($group as $key=>$val){
							// 	array_push($count_long, array(
							// 					   "suplier_kd"=>$key,
							// 					   "short_lt" => min($val), 
							// 					   "long_lt" => max($val),
							// 	));
							// }

							//Digunakan hitung Rata2
							// $output = Array();
							
							// $i = 1;
							// foreach($hitung__real_leadtime as $value) {
							//   $output_element = &$output[$value['suplier_kd'] . "_" . $value['rmgroupsup_kd']];
							//   $output_element['suplier_kd'] = $value['suplier_kd'];
							//   $output_element['rmgroupsup_kd'] = $value['rmgroupsup_kd'];
							//   $output_element['count'] = $i;
							//   if(!isset($output_element['rmgroupsup_kd']) && $value['rmgroupsup_kd'] = 0){
							// 	$output_element['count'] += $i;
							//   }
							  
							//   !isset($output_element['real_leadtime']) && $output_element['real_leadtime'] = 0;
							//   $output_element['real_leadtime'] += $value['real_leadtime'];
							  
							// }
						  
							$groups = array();
							$i = 1;
							foreach ($hitung__real_leadtime as $item) {
								$key = $item['suplier_kd'];
								$group = $item['rmgroupsup_kd'];
								$real_key = $key . $group;
								if (!array_key_exists($real_key, $count_avg)) {
									$count_avg[$real_key] = array(
										'suplier_kd' => $item['suplier_kd'],
										'rmgroupsup_kd' => $item['rmgroupsup_kd'],
										'real_leadtime' => $item['real_leadtime'],
										'count' => $i,
									);
								} else {
									$count_avg[$real_key]['count'] = $count_avg[$real_key]['count'] + 1;
									$count_avg[$real_key]['real_leadtime'] = ($count_avg[$real_key]['real_leadtime'] + $item['real_leadtime']) ;
									
								}
							}

							foreach ($count_avg as $item) {
								$key = $item['suplier_kd'];
								$group = $item['rmgroupsup_kd'];
								$real_key = $key . $group;

								if (!array_key_exists($real_key, $groups_count_avg)) {
									array_push($groups_count_avg, array(
										'suplier_kd' => $item['suplier_kd'],
										'rmgroupsup_kd' => $group,
										'AVG' =>  round($item['real_leadtime'] / $item['count'])
									));
								}
							}


							$real_output['count_late'] = $count_on_late;
							$real_output['count_long_shrt'] = $count_long;
							$real_output['count_avg'] = $groups_count_avg;




							return $real_output;
        }

    
	}

}