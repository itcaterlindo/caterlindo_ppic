<?php
defined('BASEPATH') or exit('No direct script acccess allowed!');

class Tm_deliveryorder extends CI_Model {
	private $tbl_name = 'tm_deliveryorder';
	private $p_key = 'kd_mdo';
	private $title_name = 'Data Delivery Orders';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;
		$data['primaryKey'] = $this->p_key;
		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key, 'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){
					return $this->tbl_btn($d, $row[1], $row[10], $row[8], $row[9]);
				}
			),
			array( 'db' => 'b.no_salesorder', 'dt' => 2, 'field' => 'no_salesorder', ),
			array( 'db' => 'a.no_invoice', 'dt' => 3, 'field' => 'no_invoice', ),
			array( 'db' => 'b.no_po', 'dt' => 4, 'field' => 'no_po', ),
			array( 'db' => 'a.do_trucknumber', 'dt' => 5, 'field' => 'do_trucknumber', ),
			array( 'db' => 'a.do_ket', 'dt' => 6, 'field' => 'do_ket', ),
			array( 'db' => 'a.tgl_do', 'dt' => 7, 'field' => 'tgl_do', ),
			array( 'db' => 'a.tgl_stuffing', 'dt' => 8, 'field' => 'tgl_stuffing',
				'formatter' => function($d, $row){
					return format_date($row[6], 'd-m-Y').' - '.format_date($d, 'd-m-Y');
				}
			),
			array( 'db' => 'a.tipe_do', 'dt' => 9, 'field' => 'tipe_do' ),
			array( 'db' => 'a.status_do', 'dt' => 10, 'field' => 'status_do',
				'formatter' => function($d, $row){
					$word = process_status($d);
					$color = color_status($d);
					$d = bg_label($word, $color);
					return $d;
				}
			),
			array( 'db' => 'a.msalesorder_kd', 'dt' => 11, 'field' => 'msalesorder_kd' ),
			array( 'db' => 'a.push_sap', 'dt' => 12, 'field' => 'push_sap', 
				'formatter' => function($d, $row){
					if($d == 'T'):
						$word = "Belum";
						$color = "red";
					else:
						$word = "Sudah";
						$color = "green";
					endif;
					$label = bg_label($word, $color);
					return $label;
				}
			),
		);
		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "
			FROM ".$this->tbl_name." a
		LEFT JOIN tm_salesorder b ON b.kd_msalesorder = a.msalesorder_kd
		";
		$data['where'] = "a.tipe_do != ''";

		$kd_items = $this->session->kd_manage_items;
		if ($kd_items == '1') :
			$data['where'] .= "";
		elseif ($kd_items == '2') :
			$data['where'] .= "AND a.tipe_do = 'Ekspor'";
		elseif ($kd_items == '3') :
			$data['where'] .= "AND a.tipe_do = 'Lokal'";
		elseif ($kd_items == '10') :
			$data['where'] .= "AND a.tipe_do = 'Tidak Ada'";
		endif;

		return $data;
	}

	public function tbl_btn($id = '', $var = '', $kd_mso = '', $tipe_do = '', $status = '') {		
		$read_access = cek_permission('DELIVERYORDER_VIEW');
		$update_access = cek_permission('DELIVERYORDER_UPDATE');
		$delete_access = cek_permission('DELIVERYORDER_DELETE');
		$btns = array();
		if ($read_access) {
			$btns[] = get_btn(array('access' => $read_access, 'title' => 'Detail', 'icon' => 'list', 'onclick' => 'detail_data(\''.$id.'\')'));
		}
		if ($status == 'pending' || $status == 'stuffing') :
			if ($update_access) {
				$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah Nomer Invoice', 'icon' => 'pencil', 'onclick' => 'edit_noinvoice(\''.$id.'\')'));
				$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah DO', 'icon' => 'truck', 'onclick' => 'prosesDo(\''.$id.'\', \''.$kd_mso.'\', \''.$tipe_do.'\')'));
				$btns[] = get_btn(array('access' => $update_access, 'title' => 'DO Stuffing', 'icon' => 'qrcode', 'onclick' => 'stuffing(\''.$id.'\')'));
				$btns[] = get_btn(array('access' => $update_access, 'title' => 'Finish DO', 'icon' => 'check', 'onclick' => 'return confirm(\'DO No = '.$var.' akan diubah status menjadi finish?\')?ubah_status(\''.$id.'\', \'finish\'):false'));
			}
		endif;
		if ($delete_access) {
			$btns[] = get_btn_divider();
			$btns[] = get_btn(array('access' => $delete_access, 'title' => 'Hapus', 'icon' => 'trash', 'onclick' => 'return confirm(\'Anda akan menghapus DO No = '.$var.'?\')?hapus_data(\''.$id.'\'):false'));
		}
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function get_row($id = '') {
		$this->db->from($this->tbl_name)
			->where(array($this->p_key => $id));
		$query = $this->db->get();
		$num = $query->num_rows();
		if ($num > 0) :
			$row = $query->row();
			$data = array('kd_mdo' => $row->kd_mdo, 'msalesorder_kd' => $row->msalesorder_kd, 'no_invoice' => $row->no_invoice, 'tipe_do' => $row->tipe_do, 'notify_name' => $row->notify_name, 'notify_address' => $row->notify_address, 'nm_jasakirim' => $row->nm_jasakirim, 'alamat_jasakirim' => $row->alamat_jasakirim, 'do_trucknumber' => $row->do_trucknumber, 'do_ket' => $row->do_ket, 'do_attachment' => $row->do_attachment, 'tgl_do' => format_date($row->tgl_do, 'd-m-Y'), 'tgl_stuffing' => format_date($row->tgl_stuffing, 'd-m-Y'), 'seal_number' => $row->seal_number, 'container_number' => $row->container_number, 'goods_desc' => $row->goods_desc, 'jml_koli' => $row->jml_koli, 'vessel' => $row->vessel, 'etd_sub' => format_date($row->etd_sub, 'd-m-Y'), 'etd_sin' => format_date($row->etd_sin, 'd-m-Y'), 'eta' => format_date($row->eta, 'd-m-Y'), 'container_arrived' => format_date($row->container_arrived, 'H:i:s'), 'no_bl' => $row->no_bl, 'tipe_container' => $row->tipe_container, 'container_20ft' => $row->container_20ft, 'invoicearsap_kd' => $row->invoicearsap_kd, 'push_sap' => $row->push_sap, 'gudang_kd' => $row->gudang_kd );
		else :
			$data = array('kd_mdo' => '', 'msalesorder_kd' => '', 'no_invoice' => '', 'tipe_do' => '', 'notify_name' => '', 'notify_address' => '', 'nm_jasakirim' => '', 'alamat_jasakirim' => '', 'do_trucknumber' => '', 'do_ket' => '', 'do_attachment' => '', 'tgl_do' => '', 'tgl_stuffing' => '', 'seal_number' => '', 'container_number' => '', 'goods_desc' => '', 'jml_koli' => '', 'vessel' => '', 'etd_sub' => '', 'etd_sin' => '', 'eta' => '', 'container_arrived' => '', 'no_bl' => '', 'tipe_container' => '', 'container_20ft' => '', 'invoicearsap_kd' => '', 'push_sap' => '', 'gudang_kd' => '');
		endif;
		return $data;
	}

	public function form_rules() {
		$rules = array(
			array('field' => 'txtTglDo', 'label' => 'Tgl DO', 'rules' => 'required'),
			array('field' => 'txtJmlKoli', 'label' => 'Jml Koli', 'rules' => 'required|numeric'),
		);
		return $rules;
	}

	public function invoice_form_rules() {
		$rules = array(
			array('field' => 'txtInvoice', 'label' => 'Nomer Invoice', 'rules' => 'required')
		);
		return $rules;
	}

	public function build_warning($datas = '') {
		$forms = array('txtTglDo', 'txtJmlKoli');
		foreach ($datas as $key => $data) :
			$str[$data] = (!empty(form_error($forms[$key])))?buildLabel('warning', form_error($forms[$key], '"', '"')):'';
		endforeach;
		return $str;
	}

	public function invoice_build_warning($datas = '') {
		$forms = array('txtInvoice');
		foreach ($datas as $key => $data) :
			$str[$data] = (!empty(form_error($forms[$key])))?buildLabel('warning', form_error($forms[$key], '"', '"')):'';
		endforeach;
		return $str;
	}

	private function create_code() {
		$this->db->select($this->p_key.' AS code')
			->from($this->tbl_name)
			->where(array('DATE(tgl_input)' => date('Y-m-d')))
			->order_by('tgl_input DESC, '.$this->p_key.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -6);
		endif;
		$angka = $urutan + 1;
		$primary = 'MDO'.date('dmy').str_pad($angka, 6, '000', STR_PAD_LEFT);
		return $primary;
	}

	public function create_code_ar_sap() {
		$this->db->select('invoicearsap_kd AS code')
			->from($this->tbl_name)
			->where(array('DATE(tgl_input)' => date('Y-m-d')))
			->order_by('tgl_input DESC, '.$this->p_key.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -6);
		endif;
		$angka = $urutan + 1;
		$primary = 'MAR'.date('dmy').str_pad($angka, 6, '000', STR_PAD_LEFT);
		return $primary;
	}

	public function submit_data($data = '', $tipe = '') {
		$this->load->model(array('tm_salesorder'));
		if ($tipe == 'Ekspor') :
			$code_format = 'invoiceekspor';
		elseif ($tipe == 'Lokal') :
			$code_format = 'invoicelocal';
		endif;
		if (!empty($data[$this->p_key])) :
			$label = 'Mengubah '.$this->title_name;
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s')));
			$where[$this->p_key] = $data[$this->p_key];
			$act = $this->update($submit, $where);
		else :
			$label = 'Menambahkan '.$this->title_name;
			$data[$this->p_key] = $this->create_code();
			/* --Start check no_salesorder, jika no_salesorder sudah ada dan data diinputkan pada bulan yang sama maka no invoice tidak akan dirubah-- */
			$data['no_invoice'] = $this->tm_salesorder->create_noinvoicedp($code_format, $tipe);
			$data['invoicearsap_kd'] = $this->create_code_ar_sap();
			$row_so = $this->check_from_so($data['msalesorder_kd']);
			if (!empty($row_so)) :
				$no_invoice = $row_so->no_invoice;
				$kd_invoice_sap = $row_so->invoicearsap_kd;
				$bln_input = format_date($row_so->tgl_input, 'm');
				if (format_date($data['tgl_do'], 'm') == $bln_input) :
					$data['no_invoice'] = $no_invoice;
					$data['invoicearsap_kd'] = $kd_invoice_sap;
				endif;
			endif;
			/* --END check no_salesorder, jika no_salesorder sudah ada dan data diinputkan pada bulan yang sama maka no invoice tidak akan dirubah-- */
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s')));
			$act = $this->create($submit);
		endif;
		$str = $this->report($act, $label, $submit);
		$str[$this->p_key] = $data[$this->p_key];
		return $str;
	}

	public function submit_invoice_data($data = '') {
		if (!empty($data[$this->p_key])) :
			$label = 'Mengubah '.$this->title_name;
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s')));
			$where[$this->p_key] = $data[$this->p_key];
			$act = $this->update($submit, $where);
		else :
			$label = 'Menambahkan '.$this->title_name;
			$data[$this->p_key] = $this->create_code();
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s')));
			$act = $this->create($submit);
		endif;
		$str = $this->report($act, $label, $submit);
		$str[$this->p_key] = $data[$this->p_key];
		return $str;
	}

	private function create($data = '') {
		$act = $this->db->insert($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	private function update($data = '', $where = '') {
		$act = $this->db->update($this->tbl_name, $data, $where);
		return $act?TRUE:FALSE;
	}

	public function delete_data($id = '') {
		$data = $this->get_row($id);
		/* --Jika status pending jangan ubah status ke process_wo-- */
		$this->load->model(array('tm_salesorder'));
		$r_so = $this->tm_salesorder->get_row($data['msalesorder_kd']);
		if ($r_so->status_so != 'pending' || 'cancel') :
			$act['mso'] = $this->db->update('tm_salesorder', array('status_so' => 'process_wo'), array('kd_msalesorder' => $data['msalesorder_kd']));
		endif;
		$act['master'] = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		$act['items'] = $this->db->delete('td_do_item', array('mdo_kd' => $id));
		$report = $this->report($act, 'Menghapus', array('kd_mdo' => $id, 'Kode Master Delivery Order' => $id));
		return $report;
	}

	public function report($act = '', $label = '', $data = '') {
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['kd_mdo'] = $data[$this->p_key];
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
		endif;
		$this->m_builder->write_log($stat, $label, $data);
		return $str;
	}

	public function store_in_session($data = '') {
		foreach ($data as $col => $val) :
			$_SESSION['master_do'][$col] = $val;
		endforeach;
		$str = $this->report(TRUE, 'Menambahkan '.$this->title_name, $data);
		return $str;
	}

	public function check_from_so($kd_mso = '') {
		$this->db->from($this->tbl_name)
			->where(array('msalesorder_kd' => $kd_mso));
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}

	public function get_no_invoice($kd_mdo = '') {
		$this->db->select('no_invoice')
			->from($this->tbl_name)
			->where(array($this->p_key => $kd_mdo));
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			return $row->no_invoice;
		else :
			return '';
		endif;
	}

	public function get_same_kd($no_invoice = '') {
		$this->db->select('a.kd_mdo, a.msalesorder_kd, b.no_po')
			->from($this->tbl_name.' a')
			->join('tm_salesorder b', 'b.kd_msalesorder = a.msalesorder_kd', 'left')
			->where(array('a.no_invoice' => $no_invoice))
			->order_by('a.tgl_input ASC');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	/*
	** Fungsi ini digunakan untuk cek status do menurut kode sales order
	*/
	public function chk_stat_so($kd_mso = '', $status = '') {
		$this->db->select('status_do')
			->from($this->tbl_name)
			->where(array('msalesorder_kd' => $kd_mso, 'status_do' => $status));
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			return TRUE;
		else :
			return FALSE;
		endif;
	}

	/*
	** Fungsi ini digunakan untuk menghapus data do berdasarkan kode salesorder
	** untuk menghapus data dari td_do_item maka diperlukan kode mdo
	*/
	public function del_do_so($kd_mso = '') {
		$kd_mdo = '';
		$no_invoice = '';
		$this->db->select('kd_mdo, no_invoice')
			->from($this->tbl_name)
			->where(array('msalesorder_kd' => $kd_mso));
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			$kd_mdo = $row->kd_mdo;
			$no_invoice = $row->no_invoice;
			$act['master'] = $this->db->delete($this->tbl_name, array($this->p_key => $kd_mdo));
			$act['items'] = $this->db->delete('td_do_item', array('mdo_kd' => $kd_mdo));
		else :
			$act = 1;
		endif;
		$report = $this->report($act, 'Menghapus', array('kd_mdo' => $kd_mdo, 'No Invoice' => $no_invoice));
		return $report;
	}

	public function ubah_status($kd_mdo = '', $status = '') {
		$label = 'Merubah status '.$this->title_name;
		if ($status == 'finish') :
			$this->db->select('b.no_salesorder, b.no_po, b.tipe_customer')
				->from($this->tbl_name.' a')
				->join('tm_salesorder b', 'b.kd_msalesorder = a.msalesorder_kd', 'left')
				->where(array('a.kd_mdo' => $kd_mdo));
			$query = $this->db->get();
			$row = $query->row();
			if (!empty($row)) :
				if ($row->tipe_customer == 'Lokal') :
					$no_po = $row->no_salesorder;
				elseif ($row->tipe_customer == 'Ekspor') :
					$no_po = $row->no_po;
				endif;
			endif;
			$this->load->model(array('db_caterlindo/tm_po'));
			$data['fc_nopo'] = $no_po;
			$data['fc_status'] = $status;
			$act = $this->tm_po->update_data($data);
			if (!$act) :
				$stat = 'Gagal';
				$str['confirm'] = 'error';
				$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
				return $str;
				exit();
			endif;
		endif;
		$data = array('kd_mdo' => $kd_mdo, 'status_do' => $status, 'tgl_edit' => date('Y-m-d H:i:s'), 'admin_kd' => $this->session->userdata('kd_admin'));
		$act = $this->update($data, array('kd_mdo' => $kd_mdo));
		$str = $this->report($act, $label, $data);
		return $str;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

}