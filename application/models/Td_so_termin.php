<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_so_termin extends CI_Model {
    private $tbl_name = 'td_so_termin';
    private $p_key = 'kd_so_termin';
    private $title = 'Data Termin Sales Order';

    public function read_data($kd_so_termin = '') {
        $this->db->from($this->tbl_name)->where(array('kd_so_termin' => $kd_so_termin));
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function read_all($msalesorder_kds = '') {
        $this->db->from($this->tbl_name)->where_in('msalesorder_kd', $msalesorder_kds);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function get_by_param($param){
        $this->db->from($this->tbl_name)->where($param);
        $result = $this->db->get();
        return $result;
    }

	public function create_code($code_inc = 0, $code_before = '') {
        $length = 4;
        $urutan = 0;
        if ($code_inc < 1) :
            $this->db->select($this->p_key.' AS code')
                ->from($this->tbl_name)
                ->where(array('DATE(tgl_input)' => date('Y-m-d')))
                ->order_by('tgl_input DESC, '.$this->p_key.' DESC');
            $query = $this->db->get();
            $num = $query->num_rows();
            if ($num > 0) :
                $row = $query->row();
                $code = $row->code;
                $urutan = substr($code, -$length);
            endif;
        else :
            $urutan = substr($code_before, -$length);
        endif;
		$angka = $urutan + 1;
		$primary = date('ymd').str_pad($angka, $length, '000', STR_PAD_LEFT);
		return $primary;
	}

    public function update_data($aWhere = [], $data)
	{
		$query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query ? TRUE : FALSE;
	}

}