<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Model_do extends CI_Model {
	public function get_header_detail($kd_mdo = '') {
		$this->load->helper(array('my_helper'));
		$this->load->model(array('tb_tipe_harga', 'tm_currency', 'td_currency_convert'));
		$this->db->select('a.kd_mdo, a.msalesorder_kd, a.no_invoice, a.tipe_do, a.notify_name, a.notify_address, a.nm_jasakirim, a.alamat_jasakirim, a.do_trucknumber, a.do_ket, a.do_attachment, a.status_do, a.tgl_do, a.seal_number, a.container_number, a.goods_desc, a.jml_koli, b.no_salesorder, b.no_po, b.email_sales, b.mobile_sales, b.jml_dp, b.jml_ongkir, b.jml_install, b.jml_ppn, b.nm_kolom_ppn, b.note_so, b.tipe_harga AS price_category, b.no_invoice_dp, c.nm_customer AS nm_cust, c.contact_person AS nm_contact_cust, c.alamat AS alamat_cust, c.no_telp_utama AS telp_utama_customer, c.no_telp_lain AS telp_lain_customer, c.email AS email_customer, c.npwp_customer AS npwp_customer, d.nm_customer AS nm_penerima, d.contact_person AS nm_contact_penerima, d.alamat AS alamat_penerima, d.no_telp_utama AS telp_utama_penerima, d.no_telp_lain AS telp_lain_penerima, d.email AS email_penerima, d.npwp_customer AS npwp_penerima, e.nm_select AS badan_usaha_cust, f.nm_select AS badan_usaha_penerima, g.nm_negara AS negara_cust, h.nm_negara AS negara_penerima, i.nm_provinsi AS provinsi_cust, j.nm_provinsi AS provinsi_penerima, k.nm_kota AS kota_cust, l.nm_kota AS kota_penerima, m.nm_kecamatan AS kecamatan_cust, n.nm_kecamatan AS kecamatan_penerima, o.nm_salesperson, p.term_payment_waktu, p.term_payment_format, p.custom_barcode, p.kd_jenis_customer, q.currency_kd, r.currency_type, r.icon_type, r.currency_icon, r.pemisah_angka, r.format_akhir, r.decimal, (SELECT COUNT(s.msalesorder_kd) FROM tm_deliveryorder s WHERE s.msalesorder_kd = a.msalesorder_kd) AS jml_so, c.kode_pos AS kode_pos_cust, d.kode_pos AS kode_pos_penerima, c.code_customer')
			->from('tm_deliveryorder a')
			->join('tm_salesorder b', 'b.kd_msalesorder = a.msalesorder_kd', 'left')
			->join('tm_customer c', 'c.kd_customer = b.customer_kd', 'left')
			->join('td_customer_alamat d', 'd.kd_alamat_kirim = b.alamat_kirim_kd', 'left')
			->join('tb_set_dropdown e', 'e.id = c.kd_badan_usaha', 'left')
			->join('tb_set_dropdown f', 'f.id = d.kd_badan_usaha', 'left')
			->join('tb_negara g', 'g.kd_negara = c.negara_kd', 'left')
			->join('tb_negara h', 'h.kd_negara = d.negara_kd', 'left')
			->join('tb_provinsi i', 'i.kd_provinsi = c.provinsi_kd AND i.negara_kd = g.kd_negara', 'left')
			->join('tb_provinsi j', 'j.kd_provinsi = d.provinsi_kd AND j.negara_kd = h.kd_negara', 'left')
			->join('tb_kota k', 'k.kd_kota = c.kota_kd AND i.kd_provinsi = k.provinsi_kd AND k.negara_kd = g.kd_negara', 'left')
			->join('tb_kota l', 'l.kd_kota = d.kota_kd AND j.kd_provinsi = l.provinsi_kd AND l.negara_kd = h.kd_negara', 'left')
			->join('tb_kecamatan m', 'm.kd_kecamatan = c.kecamatan_kd AND k.kd_kota = m.kota_kd AND i.kd_provinsi = m.provinsi_kd AND m.negara_kd = g.kd_negara', 'left')
			->join('tb_kecamatan n', 'n.kd_kecamatan = d.kecamatan_kd AND l.kd_kota = n.kota_kd AND j.kd_provinsi = n.provinsi_kd AND n.negara_kd = h.kd_negara', 'left')
			->join('tb_salesperson o', 'o.kd_salesperson = b.salesperson_kd', 'left')
			->join('tb_jenis_customer p', 'p.kd_jenis_customer = c.jenis_customer_kd', 'left')
			->join('tb_tipe_harga q', 'q.nm_harga = b.tipe_harga', 'left')
			->join('tm_currency r', 'r.kd_currency = q.currency_kd', 'left')
			->where(array('a.kd_mdo' => $kd_mdo))
			->limit('1');
		$query = $this->db->get();
		$row = $query->row();

		$retail_curr = $this->tb_tipe_harga->get_data('Harga Retail');
		$detail_retail = $this->tm_currency->get_data($retail_curr->currency_kd);
		if (!empty($row)) :
			/* --start properti untuk customer-- */
			$val_retail = '';
			$val_detail = '';
			$tgl_so = empty($row->tgl_so)?date('Y-m-d'):$row->tgl_so;
			if ($detail_retail->currency_type == 'secondary') :
				$val_retail = $this->td_currency_convert->get_value($retail_curr->currency_kd, $tgl_so);
			endif;
			if ($row->currency_type == 'secondary') :
				$val_detail = $this->td_currency_convert->get_value($row->currency_kd, $tgl_so);
			endif;

			$negara_cust = $row->negara_cust.after_before_char($row->negara_cust, $row->negara_cust, '.', '');
			$provinsi_cust = $row->provinsi_cust.after_before_char($row->provinsi_cust, $row->negara_cust, ', ', '.');
			$kode_pos_cust = $row->kode_pos_cust.after_before_char($row->kode_pos_cust, $row->kode_pos_cust, ', ', '.');
			$kota_cust = $row->kota_cust.after_before_char($row->kota_cust, array($row->provinsi_cust, $row->negara_cust), ', ', '.');
			$kecamatan_cust = $row->kecamatan_cust.after_before_char($row->kecamatan_cust, array($row->kota_cust, $row->provinsi_cust, $row->negara_cust), ', ', '.');
			$alamat_cust = $row->alamat_cust.'.<br>'.$kecamatan_cust.$kota_cust.$provinsi_cust.$kode_pos_cust.$negara_cust;
			$badan_usaha_cust = $row->badan_usaha_cust == 'Lainnya'?'':$row->badan_usaha_cust;
			$nm_cust = $badan_usaha_cust.' '.$row->nm_cust;
			if (!empty($row->telp_utama_customer)) :
				$telp_customer = getrid_char(preg_replace('~[-_]~', '', $row->telp_utama_customer), '-', '4');
			elseif (!empty($row->telp_lain_customer)) :
				$telp_customer = getrid_char(preg_replace('~[-_]~', '', $row->telp_lain_customer), '-', '4');
			else :
				$telp_customer = '-';
			endif;
			$email_customer = !empty($row->email_customer)?$row->email_customer:'-';
			$npwp_customer = !empty($row->npwp_customer)?$row->npwp_customer:'00.000.000.0-000.000';
			/* --end properti untuk customer-- */

			/* --start properti untuk penerima-- */
			$negara_penerima = $row->negara_penerima.after_before_char($row->negara_penerima, $row->negara_penerima, '.', '');
			$provinsi_penerima = $row->provinsi_penerima.after_before_char($row->provinsi_penerima, $row->negara_penerima, ', ', '.');
			$kode_pos_penerima = $row->kode_pos_penerima.after_before_char($row->kode_pos_penerima, $row->kode_pos_penerima, ', ', '.');
			$kota_penerima = $row->kota_penerima.after_before_char($row->kota_penerima, array($row->provinsi_penerima, $row->negara_penerima), ', ', '.');
			$kecamatan_penerima = $row->kecamatan_penerima.after_before_char($row->kecamatan_penerima, array($row->kota_penerima, $row->provinsi_penerima, $row->negara_penerima), ', ', '.');
			$alamat_penerima = $row->alamat_penerima.'.<br>'.$kecamatan_penerima.$kota_penerima.$provinsi_penerima.$kode_pos_penerima.$negara_penerima;
			$badan_usaha_penerima = $row->badan_usaha_penerima == 'Lainnya'?'':$row->badan_usaha_penerima;
			$nm_penerima = $badan_usaha_penerima.' '.$row->nm_penerima;
			if (!empty($row->telp_utama_penerima)) :
				$telp_penerima = getrid_char(preg_replace('~[-_]~', '', $row->telp_utama_penerima), '-', '4');
			elseif (!empty($row->telp_lain_customer)) :
				$telp_penerima = getrid_char(preg_replace('~[-_]~', '', $row->telp_lain_penerima), '-', '4');
			else :
				$telp_penerima = '-';
			endif;
			$email_penerima = !empty($row->email_penerima)?$row->email_penerima:'-';
			$npwp_penerima = !empty($row->npwp_penerima)?$row->npwp_penerima:'00.000.000.0-000.000';
			/* --end properti untuk penerima-- */

			$currency_icon = $row->icon_type.' '.$row->currency_icon.' '.$row->pemisah_angka.' '.$row->format_akhir;
			$decimal = !empty($row->decimal)?$row->decimal:'0';
			$no_po = !empty($row->no_po)?$row->no_po:'-';

			$ket_note_so = empty($row->note_so)?'empty':'available';
			$jml_koli = empty($row->jml_koli)?'-':$row->jml_koli;
			$is_dp = !empty($row->no_invoice_dp)?TRUE:FALSE;
			$jml_so = $row->jml_so;

			/* --start masukkan data kedalam array, untuk mempermudah akses-- */
			$data = array('kd_mdo' => $row->kd_mdo, 'msalesorder_kd' => $row->msalesorder_kd, 'no_invoice' => $row->no_invoice, 'no_salesorder' => $row->no_salesorder, 'tipe_do' => $row->tipe_do, 'notify_name' => $row->notify_name, 'notify_address' => $row->notify_address, 'nm_jasakirim' => $row->nm_jasakirim, 'alamat_jasakirim' => $row->alamat_jasakirim, 'do_trucknumber' => $row->do_trucknumber, 'do_ket' => $row->do_ket, 'do_attachment' => $row->do_attachment, 'status_do' => $row->status_do, 'no_po' => $no_po, 'nm_cust' => $nm_cust, 'nm_contact_cust' => $row->nm_contact_cust, 'alamat_cust' => $alamat_cust, 'telp_customer' => $telp_customer, 'email_customer' => $email_customer, 'npwp_customer' => $npwp_customer, 'nm_penerima' => $nm_penerima, 'nm_contact_penerima' => $row->nm_contact_penerima, 'alamat_penerima' => $alamat_penerima, 'telp_penerima' => $telp_penerima, 'email_penerima' => $email_penerima, 'npwp_penerima' => $npwp_penerima, 'nm_salesperson' => $row->nm_salesperson, 'email_sales' => $row->email_sales, 'telp_sales' => getrid_char($row->mobile_sales, '-', '3'), 'price_category' => $row->price_category, 'val_retail' => $val_retail, 'val_detail' => $val_detail, 'currency_type' => $row->currency_type, 'currency_icon' => $currency_icon, 'decimal' => $decimal, 'type_retail' => $detail_retail->currency_type, 'jml_dp' => $row->jml_dp, 'jml_ongkir' => $row->jml_ongkir, 'jml_install' => $row->jml_install, 'jml_ppn' => $row->jml_ppn, 'nm_kolom_ppn' => $row->nm_kolom_ppn, 'ket_note_so' => $ket_note_so, 'tgl_do' => format_date($row->tgl_do, 'd M Y'), 'note_so' => $row->note_so, 'kd_jenis_customer' => $row->kd_jenis_customer, 'seal_number' => $row->seal_number, 'container_number' => $row->container_number, 'goods_desc' => $row->goods_desc, 'jml_koli' => $jml_koli, 'term_payment_waktu' => $row->term_payment_waktu, 'no_invoice_dp' => $row->no_invoice_dp, 'is_dp' => $is_dp, 'jml_so' => $jml_so, 'code_customer' => $row->code_customer);
			/* --end masukkan data kedalam array, untuk mempermudah akses-- */
		else :
			/* --start masukkan data kedalam array, untuk mempermudah akses-- */
			$data = array('kd_mdo' => '', 'msalesorder_kd' => '', 'no_invoice' => '00-XX-9898', 'no_salesorder' => '', 'tipe_do' => 'Lokal', 'notify_name' => 'Notify Name', 'notify_address' => 'Notify Address', 'nm_jasakirim' => 'Jasa Pengiriman', 'alamat_jasakirim' => 'Alamat Jasa Kirim', 'do_trucknumber' => 'XX 9999 QWE', 'do_ket' => 'Keterangan DO', 'do_attachment' => '-', 'status_do' => 'Trial', 'no_po' => 'PO00XX', 'nm_cust' => 'PT Customer Name', 'nm_contact_cust' => 'Mr. X', 'alamat_cust' => 'Alamat Customer', 'telp_customer' => '', 'email_customer' => '', 'npwp_customer' => '00.000.000.0-000.000', 'nm_penerima' => 'PT Receiver Name', 'nm_contact_penerima' => 'Mrs. X', 'alamat_penerima' => 'Alamat Penerima', 'telp_penerima' => '', 'email_penerima' => '', 'npwp_penerima' => '00.000.000.0-000.000', 'nm_salesperson' => 'Trial Bossque', 'email_sales' => 'trial@sales.co.id', 'telp_sales' => '+6243512399', 'price_category' => 'Harga Retail', 'val_retail' => '0', 'val_detail' => '0', 'currency_type' => '-', 'currency_icon' => '', 'decimal' => '0', 'type_retail' => '-', 'jml_dp' => '', 'jml_ongkir' => '', 'jml_install' => '', 'jml_ppn' => '', 'nm_kolom_ppn' => '', 'ket_note_so' => '', 'tgl_do' => '', 'note_so' => '', 'kd_jenis_customer' => '', 'seal_number' => '', 'container_number' => '', 'goods_desc' => '', 'jml_koli' => '-', 'term_payment_waktu' => '', 'no_invoice_dp' => '-', 'is_dp' => $is_dp, 'jml_so' => '0', 'code_customer' => '');
			/* --end masukkan data kedalam array, untuk mempermudah akses-- */
		endif;
		return $data;
	}

	/**
	 * 
	 * Fungsi get row untuk mengambil data master DO berdasarkan kode master DO
	 * 
	 */
	public function get_row($kd_mdo = ''){
		$this->db->from('tm_deliveryorder')			
			->where(array('kd_mdo' => $kd_mdo));
		$query = $this->db->get();
		$row = $query->row();
		$data = [];
		if (!empty($row)) :
			$data = $row;
		endif;
		return $data;
	}

	public function parent_items($kd_mdo = '') {
		$this->db->select('a.stuffing_item_qty, b.item_code, b.item_desc, b.item_dimension, b.harga_barang, b.harga_retail, b.item_disc, b.total_harga, b.item_note, b.disc_type, b.netweight, b.grossweight, b.item_status, c.hs_code, b.item_cbm, a.item_note item_note_do')
			->from('td_do_item a')
			->join('td_salesorder_item b', 'b.kd_ditem_so = a.parent_kd', 'left')
			->join('tm_barang c', 'c.kd_barang = b.barang_kd', 'left')
			->where(array('a.mdo_kd' => $kd_mdo, 'a.parent_kd !=' => ''))
			->where('a.child_kd is NULL', NULL, FALSE);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function child_items($kd_mdo = '') {
		$this->db->select('a.stuffing_item_qty, b.item_code, b.item_desc, b.item_dimension, b.harga_barang, b.harga_retail, b.item_disc, b.total_harga, b.item_note, b.disc_type, b.netweight, b.grossweight, b.item_status, c.hs_code, b.item_cbm, a.item_note item_note_do')
			->from('td_do_item a')
			->join('td_salesorder_item_detail b', 'b.kd_citem_so = a.child_kd', 'left')
			->join('tm_barang c', 'c.kd_barang = b.kd_child', 'left')
			->where(array('a.mdo_kd' => $kd_mdo, 'a.child_kd !=' => ''));
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function list_parent_items($kd_mdo = array(''), $item_status = '', $tipe_do = '') {
		if (!empty($kd_mdo)) :
			$this->db->select('a.mdo_kd, a.stuffing_item_qty, a.parent_kd, b.msalesorder_kd, b.item_code, c.item_cbm, b.item_status, b.item_desc, b.item_dimension, b.harga_barang, b.harga_retail, b.item_disc, b.total_harga, b.item_note, b.disc_type, b.netweight, b.grossweight, b.item_status, IFNULL(b.disc_customer, \'kosong\') AS disc_customer, IFNULL(b.disc_distributor, \'kosong\') AS disc_distributor, c.hs_code, b.item_cbm, a.item_note item_note_do, b.order_tipe')
				->from('td_do_item a')
				->join('td_salesorder_item b', 'b.kd_ditem_so = a.parent_kd', 'left')
				->join('tm_barang c', 'c.kd_barang = b.barang_kd', 'left')
				->where_in('a.mdo_kd', $kd_mdo)
				->where(array('a.parent_kd !=' => ''))
				->where('a.child_kd is NULL', NULL, FALSE);
			if (!empty($tipe_do)) :
				if ($tipe_do == 'Ekspor') :
					$this->db->order_by('b.item_code ASC, b.kd_ditem_so ASC');
				elseif ($tipe_do == 'Lokal') :
					// $this->db->order_by('b.kd_ditem_so ASC');
					$this->db->order_by('b.tgl_input ASC');
				endif;
			endif;
			if (!empty($item_status)) :
				$this->db->where(array('b.item_status' => $item_status));
			endif;
			$query = $this->db->get();
			$result = $query->result();
			return $result;
		endif;
	}

	public function list_child_items($kd_mdo = array(''), $item_status = '', $tipe_do = '') {
		if (!empty($kd_mdo)) :
			$this->db->select('a.mdo_kd, a.stuffing_item_qty, a.parent_kd, a.child_kd, b.msalesorder_kd, b.item_code, b.item_status, b.item_desc, b.item_dimension, b.harga_barang, b.harga_retail, b.item_disc, b.total_harga, b.item_note, b.disc_type, b.netweight, b.grossweight, b.item_status, IFNULL(b.disc_customer, \'kosong\') AS disc_customer, IFNULL(b.disc_distributor, \'kosong\') AS disc_distributor, c.hs_code, b.item_cbm, a.item_note item_note_do')
				->from('td_do_item a')
				->join('td_salesorder_item_detail b', 'b.kd_citem_so = a.child_kd', 'left')
				->join('tm_barang c', 'c.kd_barang = b.kd_child', 'left')
				->where_in('a.mdo_kd', $kd_mdo)
				->where(array('a.child_kd !=' => ''));
			if (!empty($tipe_do)) :
				if ($tipe_do == 'Ekspor') :
					$this->db->order_by('b.item_code ASC, b.kd_citem_so ASC');
				elseif ($tipe_do == 'Lokal') :
					$this->db->order_by('b.kd_citem_so ASC');
				endif;
			endif;
			if (!empty($item_status)) :
				$this->db->where(array('b.item_status' => $item_status));
			endif;
			$query = $this->db->get();
			$result = $query->result();
			return $result;
		endif;
	}

	public function parent_order_items($kd_mdo = '') {
		$this->db->select('a.stuffing_item_qty, b.item_code, b.item_desc, b.item_dimension, b.harga_barang, b.harga_retail, b.item_disc, b.total_harga, b.item_note, b.disc_type, b.netweight, b.grossweight, b.item_status, c.hs_code, b.item_cbm')
			->from('td_do_item a')
			->join('td_salesorder_item b', 'b.kd_ditem_so = a.parent_kd', 'left')
			->join('tm_barang c', 'c.kd_barang = b.barang_kd', 'left')
			->where(array('a.mdo_kd' => $kd_mdo, 'a.parent_kd !=' => ''))
			->order_by('a.stuffing_item_qty ASC, b.item_desc ASC');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function child_order_items($kd_mdo = '') {
		$this->db->select('a.stuffing_item_qty, b.item_code, b.item_desc, b.item_dimension, b.harga_barang, b.harga_retail, b.item_disc, b.total_harga, b.item_note, b.disc_type, b.netweight, b.grossweight, b.item_status, c.hs_code, b.item_cbm')
			->from('td_do_item a')
			->join('td_salesorder_item_detail b', 'b.kd_citem_so = a.child_kd', 'left')
			->join('tm_barang c', 'c.kd_barang = b.kd_child', 'left')
			->where(array('a.mdo_kd' => $kd_mdo, 'a.child_kd !=' => ''))
			->order_by('a.stuffing_item_qty ASC, b.item_desc ASC');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}


	/*
	** function ini digunakan untuk mendapatkan kode master yang nantinya bisa digunakan untuk mengenerate report untuk sales
	** tipe_report berisi data ('quotation', 'salesorder', 'invoice')
	*/
	public function get_invoice_item($tipe_report = '', $tipe_do = '', $kd_jenis_customer = '', $nm_disc = '', $kd_salesperson = '', $tgl_mulai = '', $tgl_akhir = '') {
		$this->load->helper(array('my_helper'));
		$tgl_mulai = format_date($tgl_mulai, 'Y-m-d');
		$tgl_akhir = format_date($tgl_akhir, 'Y-m-d');
		$this->db->select('a.kd_mdo, a.tgl_do, a.msalesorder_kd, a.no_invoice, b.no_po, b.tipe_harga, c.nm_customer, c.npwp_customer, c.alamat , e.nm_pihak, e.jml_disc, (SELECT a.currency_type FROM tm_currency a LEFT JOIN tb_tipe_harga b ON b.currency_kd = a.kd_currency WHERE b.nm_harga = \'Harga Retail\') AS currency_type_retail, (SELECT a.convert_value FROM td_currency_convert a LEFT JOIN tb_tipe_harga b ON b.currency_kd = a.secondary_kd WHERE a.tgl_berlaku <= tgl_do AND a.row_flag <= \'active\' AND b.nm_harga = \'Harga Retail\' ORDER BY a.tgl_input DESC LIMIT 1) AS convert_value_retail, g.currency_type, g.icon_type, g.currency_icon, g.pemisah_angka, g.format_akhir, g.decimal, (SELECT a.convert_value FROM td_currency_convert a LEFT JOIN tb_tipe_harga b ON b.currency_kd = a.secondary_kd WHERE a.tgl_berlaku <= tgl_do AND a.row_flag <= \'active\' AND b.nm_harga = tipe_harga ORDER BY a.tgl_input DESC LIMIT 1) AS convert_value, h.nm_salesperson')
			->from('tm_deliveryorder a')
			->join('tm_salesorder b', 'a.msalesorder_kd = b.kd_msalesorder', 'left')
			->join('tm_customer c', 'c.kd_customer = b.customer_kd', 'left')
			->join('tb_jenis_customer d', 'd.kd_jenis_customer = c.jenis_customer_kd', 'left')
			->join('tb_default_disc e', 'e.jenis_customer_kd = d.kd_jenis_customer', 'left')
			->join('tb_tipe_harga f', 'f.nm_harga = b.tipe_harga', 'left')
			->join('tm_currency g', 'g.kd_currency = f.currency_kd', 'left')
			->join('tb_salesperson h', 'h.kd_salesperson = b.salesperson_kd', 'left')
			->where(array('d.tipe_penjualan' => $tipe_do, 'e.nm_pihak' => $nm_disc))
			->group_start()
				->where(array('a.tgl_do >=' => $tgl_mulai, 'a.tgl_do <=' => $tgl_akhir))
			->group_end()
			->order_by('a.no_invoice ASC');
		if (!empty($kd_salesperson) && $kd_salesperson != 'semua') :
			$this->db->where(array('b.salesperson_kd' => $kd_salesperson));
		endif;
		if (!empty($kd_jenis_customer) && $kd_jenis_customer != 'semua') :
			$this->db->where(array('d.kd_jenis_customer' => $kd_jenis_customer));
		endif;
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function get_sales_report($data = '') {
		$this->load->model(array('m_salesorder'));
		
		$tipe_report = $data['tipe_report'];
		$tipe_penjualan = $data['tipe_penjualan'];
		$kd_jenis_customer = $data['kd_jenis_customer'];
		$nm_disc = $data['nm_disc'];
		$kd_salesperson = $data['kd_salesperson'];
		$status_item = $data['status_item'] == 'semua'?'':$data['status_item'];
		$table['tgl_awal'] = $data['tgl_awal'];
		$table['tgl_akhir'] = $data['tgl_akhir'];

		$r_masters = $this->get_invoice_item($tipe_report, $tipe_penjualan, $kd_jenis_customer, $nm_disc, $kd_salesperson, $table['tgl_awal'], $table['tgl_akhir']);
		$kd_mdos = array('');
		$kd_sos = array('');
		if (!empty($r_masters)) :
			foreach ($r_masters as $r_master) :
				$kd_mdos[] = $r_master->kd_mdo;
				$kd_sos[] = $r_master->msalesorder_kd;
			endforeach;
		endif;
		$table['tipe_report'] = $tipe_report;
		$table['tipe_penjualan'] = $tipe_penjualan;
		$table['nm_disc'] = $nm_disc;
		$table['master'] = $r_masters;
		$table['disc_lists'] = $this->m_salesorder->get_price($kd_sos);
		$table['parent_items'] = $this->model_do->list_parent_items($kd_mdos, $status_item);
		$table['child_items'] = $this->model_do->list_child_items($kd_mdos, $status_item);
		return $table;
	}
}