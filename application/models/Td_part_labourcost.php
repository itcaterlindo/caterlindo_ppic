<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_part_labourcost extends CI_Model {
	private $tbl_name = 'td_part_labourcost';
	private $p_key = 'partlabourcost_kd';

	public function ssp_table($part_kd) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'b.bagian_nama', 
				'dt' => 2, 'field' => 'bagian_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'CONCAT (a.partlabourcost_qty, " ", a.partlabourcost_satuan) as jml_pekerja', 
				'dt' => 3, 'field' => 'jml_pekerja',
				'formatter' => function ($d , $row){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.partlabourcost_durasi', 
				'dt' => 4, 'field' => 'partlabourcost_durasi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'a.partlabourcost_tglinput', 
				'dt' => 5, 'field' => 'partlabourcost_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] = "FROM ".$this->tbl_name." as a
                            LEFT JOIN tb_bagian as b ON a.bagian_kd=b.bagian_kd";
		$data['where'] = "a.part_kd= '".$part_kd."'";
		
		return $data;
	}

	public function ssp_tablecosting($part_kd) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btncosting($d);
				}),
			array( 'db' => 'b.bagian_nama', 
				'dt' => 2, 'field' => 'bagian_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'CONCAT (a.partlabourcost_qty, " ", a.partlabourcost_satuan) as jml_pekerja', 
				'dt' => 3, 'field' => 'jml_pekerja',
				'formatter' => function ($d , $row){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.partlabourcost_durasi', 
				'dt' => 4, 'field' => 'partlabourcost_durasi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.partlabourcost_hargaunit', 
				'dt' => 5, 'field' => 'partlabourcost_hargaunit',
				'formatter' => function ($d){
					$d = custom_numberformat($d, 2, 'IDR');
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.partlabourcost_hargatotal', 
				'dt' => 6, 'field' => 'partlabourcost_hargatotal',
				'formatter' => function ($d){
					$d = custom_numberformat($d, 2, 'IDR');
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'a.partlabourcost_hargaupdate', 
				'dt' => 7, 'field' => 'partlabourcost_hargaupdate',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.partlabourcost_tglinput', 
				'dt' => 8, 'field' => 'partlabourcost_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] = "FROM ".$this->tbl_name." as a
                            LEFT JOIN tb_bagian as b ON a.bagian_kd=b.bagian_kd";
		$data['where'] = "a.part_kd= '".$part_kd."'";
		
		return $data;
	}

	private function tbl_btn($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btns = array();
		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data_labourcost(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data_labourcost(\''.$id.'\')'));	
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	private function tbl_btncosting($id) {
		$btns = array();
		$btns[] = get_btn(array('title' => 'Edit Harga', 'icon' => 'pencil', 'onclick' => 'edit_data_labourcostharga(\''.$id.'\')'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
		$this->db->select('MAX('.$this->p_key.') AS code')
			->from($this->tbl_name);
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = (int) substr($code, -10);
		endif;
		$angka = $urutan + 1;
		$primary = 'LPRT'.str_pad($angka, 10, '0', STR_PAD_LEFT);
		return $primary;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }
    
    public function get_by_id_detail($id){
        $query = $this->db->select('a.*, b.* ')
                ->from($this->tbl_name.' as a')
                ->join('tb_bagian as b', 'a.bagian_kd=b.bagian_kd', 'left')
                ->where('a.partlabourcost_kd', $id)
                ->get();
        return $query;
	}
	
    public function get_by_param_detail($param=[]){
        $query = $this->db->select($this->tbl_name.'.*, tb_bagian.* ')
                ->from($this->tbl_name)
                ->join('tb_bagian', $this->tbl_name.'.bagian_kd=tb_bagian.bagian_kd', 'left')
                ->where($param)
                ->get();
        return $query;
	}

    public function get_detail_in_part($key, $param=[]){
        $query = $this->db->select($this->tbl_name.'.*, tb_bagian.*, tm_part_main.partmain_nama, tm_part_main.partmain_kd ')
                ->from($this->tbl_name)
                ->join('td_part', $this->tbl_name.'.part_kd=td_part.part_kd', 'left')
				->join('tb_bagian', $this->tbl_name.'.bagian_kd=tb_bagian.bagian_kd', 'left')
				->join('tm_part_main', 'td_part.partmain_kd=tm_part_main.partmain_kd', 'left')
                ->where_in($key, $param)
                ->get();
        return $query;
	}

	public function get_lc_by_partmain($key, $param=[]){
        $query = $this->db->select($this->tbl_name.'.partlabourcost_durasi, td_part_labourcost.partlabourcost_qty, tb_bagian.*, tm_part_main.partmain_nama, tm_part_main.partmain_kd ')
                ->from($this->tbl_name)
				->join('tb_part_lastversion', $this->tbl_name.'.part_kd=tb_part_lastversion.part_kd', 'left')
                ->join('td_part', 'tb_part_lastversion.part_kd=td_part.part_kd', 'left')
				->join('tb_bagian', $this->tbl_name.'.bagian_kd=tb_bagian.bagian_kd', 'left')
				->join('tm_part_main', 'tb_part_lastversion.partmain_kd=tm_part_main.partmain_kd', 'left')
                ->where_in($key, $param)
                ->get();
        return $query;
	}
	
	
	public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

	public function insert_batch_data ($data=[]){
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}

	public function update_harga ($part_kd) {
		$resp = false;
		if (!empty($part_kd)) {
			$resultPartlabourcost = $this->get_by_param(['part_kd' => $part_kd])->result_array();
			$resultHargaLabourcost = $this->db->get('tb_part_labourcost_harga')->result_array();
			
			$arrayUpdate = [];
			foreach ($resultPartlabourcost as $r) {
				$partlabourcost_hargaunit = null;
				$partlabourcost_hargatotal = null;
				foreach ($resultHargaLabourcost as $rHarga){
					if ($rHarga['bagian_kd'] == $r['bagian_kd'] && $rHarga['labourcostharga_satuan'] == $r['partlabourcost_satuan']) {
						$partlabourcost_hargaunit = $rHarga['labourcostharga_harga'];
						$partlabourcost_hargatotal = (float) $partlabourcost_hargaunit * $r['partlabourcost_qty'] * $r['partlabourcost_durasi'];
					}
				}
				$arrayUpdate[] = [
					'partlabourcost_kd' => $r['partlabourcost_kd'],
					'partlabourcost_hargaunit' => $partlabourcost_hargaunit,
					'partlabourcost_hargatotal' => $partlabourcost_hargatotal,
					'partlabourcost_hargaupdate' => date('Y-m-d H:i:s')
				];
			}
			if (!empty($arrayUpdate)) {
				$resp = $this->db->update_batch($this->tbl_name, $arrayUpdate, 'partlabourcost_kd');
			}
		}else{
			$resp = false;
		}
		
		return $resp;
	}

}