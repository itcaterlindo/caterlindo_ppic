<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_purchase_suggestion_so extends CI_Model
{
	private $tbl_name = 'td_purchase_suggestion_so';
	private $p_key = 'purchasesuggestionso_kd';

	public function ssp_table($purchasesuggestion_kd)
	{
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array(
				'db' => 'a.' . $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function ($d) {

					return $this->tbl_btn($d);
				}
			),
			array(
				'db' => 'b.no_salesorder',
				'dt' => 2, 'field' => 'no_salesorder',
				'formatter' => function ($d, $row) {
					if ($row[7] == 'Ekspor'){
						$d = $row[6];
					}
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'a.purchasesuggestionso_tglselesai_produksi',
				'dt' => 3, 'field' => 'purchasesuggestionso_tglselesai_produksi',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'a.purchasesuggestionso_leadtime_produksi',
				'dt' => 4, 'field' => 'purchasesuggestionso_leadtime_produksi',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'a.purchasesuggestionso_tglmulai_produksi',
				'dt' => 5, 'field' => 'purchasesuggestionso_tglmulai_produksi',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'a.purchasesuggestionso_tgledit',
				'dt' => 6, 'field' => 'purchasesuggestionso_tgledit',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'b.no_po',
				'dt' => 7, 'field' => 'no_po',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'b.tipe_customer',
				'dt' => 8, 'field' => 'tipe_customer',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] =    "FROM " . $this->tbl_name . " as a 
								LEFT JOIN tm_salesorder as b ON a.kd_msalesorder = b.kd_msalesorder";
		$data['where'] = "a.purchasesuggestion_kd = '" . $purchasesuggestion_kd . "'";

		return $data;
	}

	private function tbl_btn($id)
	{
		$delete_access = $this->session->delete_access;
		$btns = array();
		if ($delete_access == 1) {
			$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\'' . $id . '\')'));
			$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'delete_data(\'' . $id . '\')'));
		}
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

	public function insert_data($data)
	{
		$query = $this->db->insert($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_data($id)
	{
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		return $query ? TRUE : FALSE;
	}

	public function get_by_param($param = [])
	{
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_data($aWhere = [], $data)
	{
		$query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query ? TRUE : FALSE;
	}

	public function update_data_batch($key, $dataArray = [])
	{
		$query = $this->db->update_batch($this->tbl_name, $dataArray, $key);
		return $query ? TRUE : FALSE;
	}

	public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
    }

	public function getTglKirimStuffing($purchasesuggestion_kd)
	{
		return $this->db->join('tm_salesorder', "tm_salesorder.kd_msalesorder={$this->tbl_name}.kd_msalesorder", 'left')
			->where($this->tbl_name . '.purchasesuggestion_kd', $purchasesuggestion_kd)
			->get($this->tbl_name);
	}

	public function getTglSelesaiPeriode($start, $end)
	{
		return $this->db->join('tm_salesorder', "tm_salesorder.kd_msalesorder={$this->tbl_name}.kd_msalesorder", 'left')
			->group_start()
			->where($this->tbl_name . '.purchasesuggestionso_tglmulai_produksi >=', $start)
			->where($this->tbl_name . '.purchasesuggestionso_tglmulai_produksi <=', $end)
			->group_end()
			->or_group_start()
			->where($this->tbl_name . '.purchasesuggestionso_tglselesai_produksi >=', $start)
			->where($this->tbl_name . '.purchasesuggestionso_tglselesai_produksi <=', $end)
			->group_end()
			->get($this->tbl_name);
	}

	public function getSOProcessed()
	{
		return $this->db->join('tm_salesorder', "tm_salesorder.kd_msalesorder={$this->tbl_name}.kd_msalesorder", 'left')
			->join('tm_customer', 'tm_customer.kd_customer=tm_salesorder.customer_kd', 'left')
			->where_in('tm_salesorder.status_so', ['process_lpo', 'process_wo'])
			->order_by("{$this->tbl_name}.purchasesuggestionso_tglmulai_produksi", 'asc')
			->get($this->tbl_name);
	}

	public function generateTglMulaiProduksi($purchasesuggestion_kd)
	{
		$this->load->model(['db_hrm/tb_hari_efektif']);
		$suggestSos = $this->get_by_param(['purchasesuggestion_kd' => $purchasesuggestion_kd])->result_array();

		$tglstart = '';
		$arrayTglStartResult = array();
		foreach ($suggestSos as $suggestSo) {
			$leadtimeprodhari = $suggestSo['purchasesuggestionso_leadtime_produksi'];
			$tglselesai = $suggestSo['purchasesuggestionso_tglselesai_produksi'];
			$tglstart = $this->tb_hari_efektif->getTglStartByLimit($tglselesai, $leadtimeprodhari);
			
			$arrayTglStartResult[] = [
				'purchasesuggestionso_kd' => $suggestSo['purchasesuggestionso_kd'],
				'purchasesuggestionso_tglmulai_produksi' => $tglstart,
				'purchasesuggestionso_tgledit' => date('Y-m-d H:i:s'),
			];
		}

		try {
            $act = !empty($arrayTglStartResult) ? $this->update_data_batch($this->p_key, $arrayTglStartResult) : TRUE;
            $resp['code'] = 200;
            $resp['status'] = 'Sukses'; 
            $resp['pesan'] = 'Tersimpan';
        } catch (Exception $e) {
            $resp['code'] = 400;
			$resp['status'] = 'Sukses'; 
            $resp['pesan'] = $e->getMessage();
        }

        return $resp;
	}

	public function generateTglMulaiProduksi_old($purchasesuggestion_kd)
	{
		$this->load->model(['db_hrm/tb_hari_libur']);
		$suggestSos = $this->get_by_param(['purchasesuggestion_kd' => $purchasesuggestion_kd])->result_array();

		$tglstart = '';
		$arrayTglStartResult = array();
		foreach ($suggestSos as $suggestSo) {
			$actualLeadtimeProdHari = 0;
			$leadtimeprodhari = $suggestSo['purchasesuggestionso_leadtime_produksi'];
			$tglselesai = $suggestSo['purchasesuggestionso_tglselesai_produksi'];

			$tglstart = date('Y-m-d', strtotime("-$leadtimeprodhari days", strtotime($tglselesai)));
			while ($actualLeadtimeProdHari <= $suggestSo['purchasesuggestionso_leadtime_produksi']) {
				$begin = new DateTime($tglstart);
				$end = new DateTime($tglselesai);
				$interval = DateInterval::createFromDateString('1 day');
				$period = new DatePeriod($begin, $interval, $end);

				$countLibur = 0;
				foreach ($period as $dt) {
					// Weekend check
					if ($dt->format("N") == 6 || $dt->format("N") == 7) {
						$countLibur++;
					}
				}
				// Add libur nasional
				$countLibur += $this->tb_hari_libur->getByDateBetween($tglstart, $tglselesai)->num_rows();
				
				$leadtimeprodhari = $leadtimeprodhari + $countLibur;
				$actualLeadtimeProdHari = $leadtimeprodhari - $countLibur;
				$tglstart = date('Y-m-d', strtotime("-$leadtimeprodhari days", strtotime($tglselesai)));
			}
			if (date('N', strtotime($tglstart)) == 6){
                $tglstart = date('Y-m-d', strtotime("-1 days", strtotime($tglstart)));
            }elseif(date('N', strtotime($tglstart)) == 7){
                $tglstart = date('Y-m-d', strtotime("-2 days", strtotime($tglstart)));
            }
			$arrayTglStartResult[] = [
				'purchasesuggestionso_kd' => $suggestSo['purchasesuggestionso_kd'],
				'purchasesuggestionso_tglmulai_produksi' => $tglstart,
				'purchasesuggestionso_tgledit' => date('Y-m-d H:i:s'),
			];
		}

		try {
            $act = !empty($arrayTglStartResult) ? $this->update_data_batch($this->p_key, $arrayTglStartResult) : TRUE;
            $resp['code'] = 200;
            $resp['status'] = 'Sukses'; 
            $resp['pesan'] = 'Tersimpan';
        } catch (Exception $e) {
            $resp['code'] = 400;
			$resp['status'] = 'Sukses'; 
            $resp['pesan'] = $e->getMessage();
        }

        return $resp;
	}
	
}
