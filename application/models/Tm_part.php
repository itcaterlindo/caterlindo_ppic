<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_part extends CI_Model {
	private $tbl_name = 'tm_part';
	private $p_key = 'part_kd';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => $this->p_key, 
				'dt' => 2, 'field' => $this->p_key,
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'part_nama', 
				'dt' => 3, 'field' => 'part_nama',
				'formatter' => function ($d , $row){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'part_tglinput', 
				'dt' => 4, 'field' => 'part_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name;
		$data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btns = array();
		$btns[] = get_btn(array('title' => 'Detail Item', 'icon' => 'search', 'onclick' => 'detail_data(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Duplicate Item', 'icon' => 'clone', 'onclick' => 'duplicate_data(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));	
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
		$this->db->select('MAX('.$this->p_key.') AS code')
			->from($this->tbl_name);
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = (int) substr($code, -6);
		endif;
		$angka = $urutan + 1;
		$primary = 'PART'.str_pad($angka, 6, '0', STR_PAD_LEFT);
		return $primary;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

}