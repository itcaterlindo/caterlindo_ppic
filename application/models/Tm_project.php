<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_project extends CI_Model {
    private $tbl_name = 'tm_project';
	private $p_key = 'project_kd';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){
					
					return $this->tbl_btn($d, $row[10]);
				}),
			array( 'db' => 'a.project_nopj', 
				'dt' => 2, 'field' => 'project_nopj',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.no_salesorder', 
				'dt' => 3, 'field' => 'no_salesorder',
				'formatter' => function ($d, $row){
					if ($row[8] == 'Ekspor') {
						$d = $row[7];
					}
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.project_itemcode', 
				'dt' => 4, 'field' => 'project_itemcode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.project_itemdesc', 
				'dt' => 5, 'field' => 'project_itemdesc',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.project_itemdimension', 
				'dt' => 6, 'field' => 'project_itemdimension',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.project_status', 
				'dt' => 7, 'field' => 'project_status',
				'formatter' => function ($d){
					$label = 'success';
					if ($d == 'pending') {
						$label = 'warning';
					}elseif ($d == 'cancel'){
						$label = 'danger';
					}
					$d = build_span ($label, $d);

					return $d;
				}),
			array( 'db' => 'a.project_tglinput', 
				'dt' => 8, 'field' => 'project_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.no_po', 
				'dt' => 9, 'field' => 'no_po',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.tipe_customer', 
				'dt' => 10, 'field' => 'tipe_customer',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.project_status', 
				'dt' => 11, 'field' => 'project_status',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] =    "FROM ".$this->tbl_name." as a
								LEFT JOIN tm_salesorder as b ON a.kd_msalesorder=b.kd_msalesorder";
		$data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id, $status) {
		$btns = array();
		if ($status != 'approved') {
			$btns[] = get_btn(array('title' => 'Approve Item', 'icon' => 'check', 'onclick' => 'ubah_status(\''.$id.'\', \''.'approved'.'\')'));
		}else{
			$btns[] = get_btn(array('title' => 'Cancel Item', 'icon' => 'check', 'onclick' => 'ubah_status(\''.$id.'\', \''.'cancel'.'\')'));
		}
		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'form_box(\'edit\',\''.$id.'\')'));
		// $btns[] = get_btn(array('title' => 'Cetak Item', 'icon' => 'print', 'onclick' => 'cetak_data(\''.$id.'\')'));
		$btns[] = get_btn_divider();
		$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));
		
		$btn_group = group_btns($btns);

		return $btn_group;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by('project_nopj')->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }

    public function create_code ($no, $code) {
		$ident = 'MPJ';
        $identtgl = date('y').date('m').date('d');
        
        if (empty($no) && empty($code)):
            $query = $this->db->select($this->p_key)
                    ->where('DATE(project_tglinput)', date('Y-m-d'))
                    ->order_by('project_tglinput', 'DESC')
                    ->get($this->tbl_name);
            if ($query->num_rows() == 0):
                $code = $ident.$identtgl.'000001';
            else:
                $code = $query->result();
                $code = max($code);
                $code = $code->{$this->p_key};
            endif;
        endif;
        $subsLaskode = substr($code, -6);
        $nextnumber = (int) $subsLaskode + 1;
        $code = $ident.$identtgl.str_pad($nextnumber,6,"0",STR_PAD_LEFT);
	
		return $code;
	}
    
	public function generate_nopj ($no, $no_pj) {
		if (empty($no) && empty($no_pj)):
			$no_pj = '';
			$qProject = $this->db->select('MAX(project_nopj) as lastPJ')
					->like('project_nopj', 'PJ'.date('y'), 'after')
					->get($this->tbl_name);
			if ($qProject->num_rows() == 0):
				$no_pj = 'PJ'.date('y').'00000';
			else:
                $no_pj = $qProject->row()->lastPJ;
			endif;

		endif;
		$num = substr($no_pj, -4);
		$num = (int) $num + 1;
		$no_pj = 'PJ'.date('y').str_pad($num, 4, '0', STR_PAD_LEFT);

		return $no_pj;
    }
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_by_param_in ($param, $params=[]) {
		$this->db->where_in($param, $params);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_by_param_bom_in ($param, $params) {
		$query = $this->db->select($this->tbl_name.'.*, tm_bom.bom_kd')
				->where_in($param, $params)
				->join('tm_bom', $this->tbl_name.'.project_kd=tm_bom.project_kd', 'left')
				->get($this->tbl_name);
		return $query;
	}

}