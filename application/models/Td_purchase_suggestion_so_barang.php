<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_purchase_suggestion_so_barang extends CI_Model
{
    private $tbl_name = 'td_purchase_suggestion_so_barang';
    private $p_key = 'purchasesuggestionsobarang_kd';

    public function ssp_table($purchasesuggestion_kd)
    {
        $data['table'] = $this->tbl_name;

        $data['primaryKey'] = $this->p_key;

        $data['columns'] = array(
            array(
                'db' => 'a.' . $this->p_key,
                'dt' => 1, 'field' => $this->p_key,
                'formatter' => function ($d) {

                    return $this->tbl_btn($d);
                }
            ),
            array(
                'db' => 'b.no_salesorder',
                'dt' => 2, 'field' => 'no_salesorder',
                'formatter' => function ($d, $row) {
                    /** Jika barang yang di add tidak ada SOnya (purchasesuggestionso_kd) maka jangan tampilkan SO */
                    if ($row[10] == null || $row[10] == "" ){
						$d = "";
					}else{
                        if ($row[9] == 'Ekspor'){
                            $d = $row[8];
                        }
                    }
                    $d = $this->security->xss_clean($d);
                    return $d;
                }
            ),
            array(
                'db' => 'c.item_code',
                'dt' => 3, 'field' => 'item_code',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.purchasesuggestionsobarang_deskripsi',
                'dt' => 4, 'field' => 'purchasesuggestionsobarang_deskripsi',
                'formatter' => function ($d, $row) {
                    $d = "$d ($row[7]}";
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.purchasesuggestionsobarang_qty',
                'dt' => 5, 'field' => 'purchasesuggestionsobarang_qty',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'e.item_code as itemcode_bom',
                'dt' => 6, 'field' => 'itemcode_bom',
                'formatter' => function ($d, $row) {
                    // $d = !empty($d) ? $row[10] : null;
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.purchasesuggestionsobarang_tgledit',
                'dt' => 7, 'field' => 'purchasesuggestionsobarang_tgledit',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.purchasesuggestionsobarang_dimensi',
                'dt' => 8, 'field' => 'purchasesuggestionsobarang_dimensi',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
				'db' => 'b.no_po',
				'dt' => 9, 'field' => 'no_po',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
			array(
				'db' => 'b.tipe_customer',
				'dt' => 10, 'field' => 'tipe_customer',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
            array(
				'db' => 'a.purchasesuggestionso_kd',
				'dt' => 11, 'field' => 'purchasesuggestionso_kd',
				'formatter' => function ($d) {
					$d = $this->security->xss_clean($d);

					return $d;
				}
			),
        );

        $data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM " . $this->tbl_name . " as a 
                                LEFT JOIN td_purchase_suggestion_so as ab ON ab.purchasesuggestion_kd=a.purchasesuggestion_kd
                                LEFT JOIN tm_salesorder as b ON b.kd_msalesorder = ab.kd_msalesorder
								LEFT JOIN tm_barang as c ON a.kd_barang=c.kd_barang
								LEFT JOIN tm_bom as d ON a.kd_barang=d.kd_barang
                                LEFT JOIN tm_barang as e ON e.kd_barang=d.kd_barang";
        $data['where'] = "a.purchasesuggestion_kd = '" . $purchasesuggestion_kd . "'";

        return $data;
    }

    private function tbl_btn($id)
    {
        $delete_access = $this->session->delete_access;
        $btns = array();
        if ($delete_access == 1) {
            $btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\'' . $id . '\')'));
            $btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'delete_data(\'' . $id . '\')'));
        }
        $btn_group = group_btns($btns);

        return $btn_group;
    }

    public function create_code()
    {
        $query = $this->db->select('MAX(' . $this->p_key . ') as maxID')
            ->get($this->tbl_name)
            ->row();
        $code = (int) $query->maxID + 1;
        return $code;
    }

    public function insert_data($data)
    {
        $query = $this->db->insert($this->tbl_name, $data);
        return $query ? TRUE : FALSE;
    }

    public function delete_data($id)
    {
        $query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
        return $query ? TRUE : FALSE;
    }

    public function delete_by_param($param=[]) 
    {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

    public function get_by_param($param = [])
    {
        $this->db->where($param);
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    public function update_data($aWhere = [], $data)
    {
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
        return $query ? TRUE : FALSE;
    }

    public function update_data_batch($key, $dataArray = [])
    {
        $query = $this->db->update_batch($this->tbl_name, $dataArray, $key);
        return $query ? TRUE : FALSE;
    }

    public function insert_batch($data)
    {
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
    }

    public function get_relate_part($arrayWhere = [])
    {
        $result = $this->db->where($arrayWhere)
            ->join('td_bom_detail', "td_bom_detail.bom_kd={$this->tbl_name}.bom_kd", 'left')
            ->join('tb_part_lastversion', "tb_part_lastversion.partmain_kd=td_bom_detail.partmain_kd", 'left')
            ->join('td_part_detail', "td_part_detail.part_kd=tb_part_lastversion.part_kd", 'left')
            ->get($this->tbl_name);
        
        return $result;
    }

    public function generate_so_barangs($purchasesuggestion_kd)
    {
        $this->load->model(['td_purchase_suggestion_so', 'td_salesorder_item', 'td_purchase_suggestion_so_material', 'td_salesorder_item_detail', 'tm_bom']);
        /** Delete Item so barang */
        $this->td_purchase_suggestion_so_material->delete_by_param(['purchasesuggestion_kd' => $purchasesuggestion_kd]); 
        $this->delete_by_param(['purchasesuggestion_kd' => $purchasesuggestion_kd]); 

        $suggestionsos = $this->td_purchase_suggestion_so->get_by_param(['purchasesuggestion_kd' => $purchasesuggestion_kd])->result_array();

        /** Item */
        $soitems = array();
        $soitemResults = array();
        foreach ($suggestionsos as $suggestionso) {
            $suggestionSOResults['purchasesuggestionso_kd'] = $suggestionso['purchasesuggestionso_kd'];
            $soitems = $this->td_salesorder_item->get_by_param_suggestion_so_barang(['msalesorder_kd' =>$suggestionso['kd_msalesorder']])->result_array();
            $soitemDetails =  $this->td_salesorder_item_detail->get_by_param_suggestion_so_barang(['msalesorder_kd' =>$suggestionso['kd_msalesorder']])->result_array();
            $soitems = array_merge($soitems, $soitemDetails);
            $suggestionSOResults['items'] = $soitems;
            
            $soitemResults[] = $suggestionSOResults;
            
        }
        
        $arrayData = array();
        $arrayKdBarang = array();
        $purcahasesuggestionsobarang_kd = $this->create_code();
        foreach ($soitemResults as $eachResult) {
            $purchasesuggestionso_kd = $eachResult['purchasesuggestionso_kd'];
            foreach ($eachResult['items'] as $eachItem) {
                $arrayData[] = [
                    'purchasesuggestionsobarang_kd' => $purcahasesuggestionsobarang_kd,
                    'purchasesuggestion_kd' => $purchasesuggestion_kd,
                    'purchasesuggestionso_kd' => (int)$purchasesuggestionso_kd,
                    'kd_barang' => $eachItem['barang_kd'],
                    'bom_kd' => null,
                    'purchasesuggestionsobarang_itemstatus' => $eachItem['item_status'],
                    'purchasesuggestionsobarang_deskripsi' => $eachItem['item_desc'],
                    'purchasesuggestionsobarang_dimensi' => $eachItem['item_dimension'],
                    'purchasesuggestionsobarang_qty' => (int)$eachItem['item_qty'],
                    'purchasesuggestionsobarang_tglinput' => date('Y-m-d H:i:s'),
                    'purchasesuggestionsobarang_tgledit' => date('Y-m-d H:i:s'),
                    'admin_kd' => $this->session->userdata('kd_admin')
                ];
                $purcahasesuggestionsobarang_kd++;

                /** Generate array kd_barang */
                if (!in_array($eachItem['barang_kd'], $arrayKdBarang)){
                    $arrayKdBarang[] = $eachItem['barang_kd'];
                }
            }
        }

        /** BOM*/
        $boms = $this->tm_bom->get_by_param_in ('kd_barang', $arrayKdBarang)->result_array();
        $arrayBom = array();
        foreach ($boms as $bom){
            $arrayBom[$bom['kd_barang']] = $bom['bom_kd'];
        }
        $arrayDataBom = array();
        foreach ($arrayData as $eachDataitem){
            $eachDataitem['bom_kd'] = isset( $arrayBom[$eachDataitem['kd_barang']] ) ? $arrayBom[$eachDataitem['kd_barang']] : null;
            $arrayDataBom[] = $eachDataitem;
        }
        
        try {
            $act = $this->insert_batch($arrayDataBom);
            $resp['code'] = 200;
            $resp['status'] = 'Sukses'; 
            $resp['pesan'] = 'Tersimpan';
        } catch (Exception $e) {
            $resp['code'] = 200;
            $resp['status'] = 'Sukses'; 
            $resp['pesan'] = $e->getMessage();
        }

        return $resp;
    }
}
