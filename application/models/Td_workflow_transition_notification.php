<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_workflow_transition_notification extends CI_Model {
	private $tbl_name = 'td_workflow_transition_notification';
	private $p_key = 'wftransitionnotification_kd';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'whdeliveryorder_tanggal', 
				'dt' => 2, 'field' => 'whdeliveryorder_tanggal',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'whdeliveryorder_no', 
				'dt' => 3, 'field' => 'whdeliveryorder_no',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'whdeliveryorder_to', 
				'dt' => 4, 'field' => 'whdeliveryorder_to',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'whdeliveryorder_note', 
				'dt' => 5, 'field' => 'whdeliveryorder_note',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'whdeliveryorder_status', 
				'dt' => 6, 'field' => 'whdeliveryorder_status',
				'formatter' => function ($d){
					$label = 'warning';
					if ($d == 'approved'){
						$label = 'success';
					}
					$d = build_span ($label, $d);
					$d = $this->security->xss_clean($d);

					return $d;
                }),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name;
        $data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id) {
		$btns = array();
		$btns[] = get_btn(array('title' => 'Lihat Item', 'icon' => 'search', 'onclick' => 'view_box(\''.$id.'\')'));	
		$btns[] = get_btn(array('title' => 'Cetak Item', 'icon' => 'print', 'onclick' => 'cetak_data(\''.$id.'\')'));	
		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));	
		$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));	
		$btn_group = group_btns($btns);

		return $btn_group;
	}

    public function create_code () {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_where_in ($where, $in = []) {
		$query = $this->db->where_in($where, $in)
				->select($this->tbl_name.'.*')
				->get($this->tbl_name);
		return $query;
	}

	public function update_batch ($idColumn, $data = []) {
		$act = $this->db->update_batch($this->tbl_name, $data, $idColumn);
		return $act;
	}

    public function generate_notification($wftransition_kd, $subject, $message) {
        $this->load->model(['_mail_configuration']);

        $wftransition = $this->db->where('wftransition_kd', $wftransition_kd)
                ->get('td_workflow_transition')->row_array();
        if ($wftransition['wftransition_email'] == '1') {
            $wftrannotif = $this->get_by_param(['wftransition_kd' => $wftransition_kd])->row_array();
            $to = explode(';', $wftrannotif['wftransitionnotification_to']);
            $cc = explode(';', $wftrannotif['wftransitionnotification_cc']);
            $act = $this->_mail_configuration->sendEmailCc($to, $cc, $subject, $message);
        }else{
            $act = true;
        }
        return $act;
    }

	public function getStatus_generate_notification($wftransition_kd) {
        $this->load->model(['_mail_configuration']);

        $wftransition = $this->db->where('wftransition_kd', $wftransition_kd)
                ->get('td_workflow_transition')->row_array();
        if ($wftransition['wftransition_email'] == '1') {
			$wftrannotif = $this->get_by_param(['wftransition_kd' => $wftransition_kd])->row_array();
            $act['to'] = $wftrannotif['wftransitionnotification_to'];
            $act['cc'] = $wftrannotif['wftransitionnotification_cc'];
			$act['status'] = $wftrannotif['wftransitionnotification_to'] ? 1 : 0;
        }else{	
            $act['status'] = 0;
        }
        return $act;
    }
	
}