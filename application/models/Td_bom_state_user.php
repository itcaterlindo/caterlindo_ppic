<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_bom_state_user extends CI_Model {
	private $tbl_name = 'td_bom_state_user';
	private $p_key = 'bomstateuser_kd';

	public function ssp_table () {
        $data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'b.nm_admin',
				'dt' => 2, 'field' => 'nm_admin',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.bomstate_nama', 
				'dt' => 3, 'field' => 'bomstate_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.bomstateuser_email', 
				'dt' => 4, 'field' => 'bomstateuser_email',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.bomstateuser_emailcc', 
				'dt' => 5, 'field' => 'bomstateuser_emailcc',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.bomstateuser_tgledit', 
				'dt' => 6, 'field' => 'bomstateuser_tgledit',
				'formatter' => function ($d, $row){
					$d = $this->security->xss_clean($d);

					return $d;
                }),       
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] = "FROM ".$this->tbl_name." as a
							LEFT JOIN tb_admin as b ON a.bomstateuser_admin=b.kd_admin
							LEFT JOIN tb_bom_state as c ON a.bomstate_kd=c.bomstate_kd";
		$data['where'] = "";
		
		return $data;
	}
	
	private function tbl_btn($id) {
		$btns = array();
		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_item(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'delete_item(\''.$id.'\')'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }

    public function get_all () {
        return $this->db->get($this->tbl_name);
	}
	
	public function isApprover ($admin_kd) {
		$bomstateApproved = 4;
		$cekAdmin = $this->get_by_param(['bomstateuser_admin' => $admin_kd, 'bomstate_kd' => $bomstateApproved]);
		$resp = false;
		if ($cekAdmin->num_rows() > 0) {
			$resp = true;
		}
		return $resp;
	}

	public function get_by_param_detail ($param = []) {
		$result = $this->db->select()
                    ->from($this->tbl_name)
                    ->join('tb_admin', $this->tbl_name.'.bomstateuser_admin=tb_admin.kd_admin', 'left')
                    ->join('tb_bom_state', $this->tbl_name.'.bomstate_kd=tb_bom_state.bomstate_kd', 'left')
                    ->where($param)
                    ->get();
		return $result;
	}

	public function create_code() {
        $query = $this->db->select('MAX('.$this->p_key.') as maxID')
                ->get($this->tbl_name)
                ->row();
        $code = (int) $query->maxID + 1;
        return $code;
	}

}