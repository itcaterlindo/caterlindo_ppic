<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class _mail_configuration extends CI_Model {
    private $email_name = 'noreply@caterlindo.co.id';
    private $email_password = 'Caterlindo@2019';

	public function __construct() {
		parent::__construct();
        $this->load->library('email');
    }

    private function emailConfig (){
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "mail.caterlindo.co.id";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = $this->email_name;
        $config['smtp_pass'] = $this->email_password;
        $config['smtp_crypto'] = 'ssl';
        $config['charset'] = "utf-8";
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        return $config;
    }

    public function sendEmail($to = [], $subject, $message){
        $this->email->initialize($this->emailConfig());
        $this->email->from($this->email_name, 'Caterlindo PPIC System');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        $sendEmail = $this->email->send();

        return $sendEmail ? $sendEmail : false;

    }

    public function sendEmailCc($to = [], $cc = [], $subject, $message){
        $this->email->initialize($this->emailConfig());
        $this->email->from($this->email_name, 'Caterlindo PPIC System');
        $this->email->to($to);
        $this->email->cc($cc);
        $this->email->subject($subject);
        $this->email->message($message);
        $sendEmail = $this->email->send();

        return $sendEmail ? $sendEmail : false;

    }

	
}