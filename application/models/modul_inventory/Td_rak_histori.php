<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_rak_histori extends CI_Model {
	private $tbl_name = 'td_rak_histori';
	private $p_key = 'kd_rak_histori';
	private $title_name = 'Data Histori Barang';

	/* --start ssp tabel untuk modul data warehouse-- */
	public function ssp_table() {
		$data['table'] = 'tm_barang';

		$data['primaryKey'] = 'kd_barang';

		$q_in = '(SELECT IFNULL(SUM(c.qty), \'0\') FROM td_rak_histori c WHERE c.rak_ruang_kd = b.rak_ruang_kd AND c.rak_kd = b.rak_kd AND c.gudang_kd = b.gudang_kd AND c.barang_kd = b.barang_kd AND c.mutasi = \'0\' AND c.status = \'in\')';
		$q_out = '(SELECT IFNULL(SUM(c.qty), \'0\') FROM td_rak_histori c WHERE c.rak_ruang_kd = b.rak_ruang_kd AND c.rak_kd = b.rak_kd AND c.gudang_kd = b.gudang_kd AND c.barang_kd = b.barang_kd AND c.mutasi = \'0\' AND c.status = \'out\')';
		$data['columns'] = array(
			array( 'db' => 'a.kd_barang',
				'dt' => 1, 'field' => 'kd_barang',
				'formatter' => function($d, $row){

					return $this->tbl_btn($d, $row[3]);
				} ),
			array( 'db' => 'a.kd_barang', 'dt' => 2, 'field' => 'kd_barang' ),
			array( 'db' => 'a.item_barcode', 'dt' => 3, 'field' => 'item_barcode' ),
			array( 'db' => 'a.item_code', 'dt' => 4, 'field' => 'item_code' ),
			array( 'db' => 'a.deskripsi_barang', 'dt' => 5, 'field' => 'deskripsi_barang' ),
			array( 'db' => 'a.dimensi_barang', 'dt' => 6, 'field' => 'dimensi_barang' ),
			array( 'db' => $q_in, 'dt' => 7, 'field' => $q_in ),
			array( 'db' => $q_out, 'dt' => 8, 'field' => $q_out ),
			array( 'db' => 'a.total_stok', 'dt' => 9, 'field' => 'total_stok' ),
		);

		$data['sql_details'] = sql_connect();

		$data['joinQuery'] = "FROM tm_barang a RIGHT JOIN td_rak_histori b ON b.barang_kd = a.kd_barang";

		if (isset($_SESSION['modul_inventory']['kd_gudang']) && isset($_SESSION['modul_inventory']['kd_rak'])) :
			$data['where'] = "b.gudang_kd = '".$_SESSION['modul_inventory']['kd_gudang']."' AND b.rak_kd = '".$_SESSION['modul_inventory']['kd_rak']."' AND b.rak_ruang_kd = '".$_SESSION['modul_inventory']['kd_rak_ruang']."'";
		else :
			$data['where'] = "";
		endif;

		$data['groupBy'] = 'b.barang_kd';

		return $data;
	}
	/* --end ssp tabel untuk modul data warehouse-- */

	/* --start button tabel untuk modul data warehouse-- */
	private function tbl_btn($id = '', $var = '') {
		$read_access = $this->session->read_access;
		$update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		$btns[] = get_btn(array('access' => $read_access, 'title' => 'Lihat Histori', 'icon' => 'search', 'onclick' => 'get_histori({\'kd_barang\': \''.$id.'\', \'kd_gudang\': \''.$_SESSION['modul_inventory']['kd_gudang'].'\', \'kd_rak\': \''.$_SESSION['modul_inventory']['kd_rak'].'\', \'kd_rak_ruang\': \''.$_SESSION['modul_inventory']['kd_rak_ruang'].'\'})'));
		$btns[] = get_btn_divider();
		$btns[] = get_btn(array('access' => $delete_access, 'title' => 'Hapus', 'icon' => 'trash',
			'onclick' => 'return confirm(\'Anda akan menghapus data Barang = '.$var.'?\')?hapus_data(\''.$id.'\'):false'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}
	/* --end button tabel untuk modul data warehouse-- */

	public function tbl_histori_btn($id = '', $var = '') {
		$read_access = $this->session->read_access;
		$update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah Data', 'icon' => 'pencil', 'onclick' => 'ubah_histori({\'kd_rak_histori\': \''.$id.'\'})'));
		$btns[] = get_btn_divider();
		$btns[] = get_btn(array('access' => $delete_access, 'title' => 'Hapus', 'icon' => 'trash',
			'onclick' => 'return confirm(\'Anda akan menghapus histori data Barang = '.$var.'?\')?hapus_histori_data(\''.$id.'\'):false'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function get_row($id = '') {
		$this->db->select('a.*, b.item_code, b.item_barcode, b.deskripsi_barang, b.dimensi_barang')
			->from($this->tbl_name.' a')
			->join('tm_barang b', 'b.kd_barang = a.barang_kd', 'left')
			->where(array('a.'.$this->p_key => $id));
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}

	public function get_data($id = '') {
		$row = $this->get_row($id);
		if (!empty($row)) :
			$data = array('kd_rak_histori' => $row->kd_rak_histori, 'rak_ruang_kd' => $row->rak_ruang_kd, 'gudang_kd' => $row->gudang_kd, 'rak_kd' => $row->rak_kd, 'barang_kd' => $row->barang_kd, 'tgl_transaksi' => format_date($row->tgl_transaksi, 'd-m-Y'), 'qty' => $row->qty, 'status' => $row->status, 'item_code' => $row->item_code, 'item_barcode' => $row->item_barcode, 'deskripsi_barang' => $row->deskripsi_barang, 'dimensi_barang' => $row->dimensi_barang);
		else :
			$kd_gudang = isset($_SESSION['modul_inventory']['kd_gudang'])?$_SESSION['modul_inventory']['kd_gudang']:'';
			$kd_rak = isset($_SESSION['modul_inventory']['kd_rak'])?$_SESSION['modul_inventory']['kd_rak']:'';
			$kd_rak_ruang = isset($_SESSION['modul_inventory']['kd_rak_ruang'])?$_SESSION['modul_inventory']['kd_rak_ruang']:'';
			$data = array('kd_rak_histori' => '', 'rak_ruang_kd' => $kd_rak_ruang, 'gudang_kd' => $kd_gudang, 'rak_kd' => $kd_rak, 'barang_kd' => '', 'tgl_transaksi' => date('d-m-Y'), 'qty' => '', 'status' => 'in');
		endif;
		return $data;
	}

	public function form_rules() {
		$rules = [];
		if ($this->input->post('selInputOption') == 'item_code') :
			$rules = array(
				array('field' => 'txtItemCode', 'label' => 'Item Code', 'rules' => 'required'),
				array('field' => 'txtJmlItem', 'label' => 'Jumlah Item', 'rules' => 'required'),
			);
		elseif ($this->input->post('selInputOption') == 'barcode') :
			$rules = [
				['field' => 'txtItemBarcode', 'label' => 'Item Barcode', 'rules' => 'required'],
			];
		endif;
		return $rules;
	}

	public function form_warning($datas = '') {
		if ($this->input->post('selInputOption') == 'item_code') :
			$forms = array('txtItemCode', 'txtJmlItem');
			foreach ($datas as $key => $data) :
				$str[$data] = (!empty(form_error($forms[$key])))?buildLabel('warning', form_error($forms[$key], '"', '"')):'';
			endforeach;
		elseif ($this->input->post('selInputOption') == 'barcode') :
			$forms = array('txtItemBarcode');
			foreach ($datas as $key => $data) :
				$str[$data] = (!empty(form_error($forms[$key])))?buildLabel('warning', form_error($forms[$key], '"', '"')):'';
			endforeach;
		endif;
		return $str;
	}

	private function create_code() {
		$this->db->select($this->p_key.' AS code')
			->from($this->tbl_name)
			->where(array('DATE(tgl_input)' => date('Y-m-d')))
			->order_by('tgl_input DESC, '.$this->p_key.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -3);
		endif;
		$angka = $urutan + 1;
		$primary = 'RRG'.date('dmy').str_pad($angka, 3, '000', STR_PAD_LEFT);
		return $primary;
	}

	public function submit_data($data = '') {
		if (!empty($data[$this->p_key])) :
			$label = 'Mengubah '.$this->title_name;
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s')));
			$where = array($this->p_key => $data[$this->p_key]);
			$act = $this->update($submit, $where);
		else :
			$label = 'Menambahkan '.$this->title_name;
			$data[$this->p_key] = $this->create_code();
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s')));
			$act = $this->create($submit);
		endif;
		$str = $this->report($act, $label, $submit);
		return $str;
	}

	private function create($data = '') {
		$act = $this->db->insert($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	private function update($data = '', $where = '') {
		$act = $this->db->update($this->tbl_name, $data, $where);
		return $act?TRUE:FALSE;
	}

	public function report($act = '', $label = '', $data = '') {
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
		endif;
		$this->m_builder->write_log($stat, $label, $data);
		return $str;
	}

	public function delete_data($id = '') {
		$act = $this->db->delete($this->tbl_name, array('barang_kd' => $id, 'gudang_kd' => $_SESSION['modul_inventory']['kd_gudang'], 'rak_kd' => $_SESSION['modul_inventory']['kd_rak'], 'rak_ruang_kd' => $_SESSION['modul_inventory']['kd_rak_ruang']));
		$report = $this->report($act, 'Menghapus '.$this->title_name, array($this->p_key => $id));
		return $report;
	}

	public function get_all($params = [], $orders = '') {
		$this->db->select('a.*, b.item_code, b.deskripsi_barang, b.dimensi_barang')
			->from($this->tbl_name.' a')
			->join('tm_barang b', 'b.kd_barang = a.barang_kd', 'right')
			->where($params);
		if (!empty($orders)) :
			$this->db->order_by($orders);
		endif;
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
}