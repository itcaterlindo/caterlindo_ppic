<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class M_barang_masuk extends CI_Model {
	private $tbl_name = '';
	private $p_key = '';
	private $title_name = 'Data Barang Masuk';

	/* --start ssp tabel untuk modul data warehouse-- */
	public function ssp_table() {
		$data['table'] = 'tm_gudang';

		$data['primaryKey'] = 'kd_gudang';

		$data['columns'] = array(
			array( 'db' => 'kd_gudang',
				'dt' => 1, 'field' => 'kd_gudang',
				'formatter' => function($d, $row){

					return $this->tbl_btn($d, $row[2]);
				} ),
			array( 'db' => 'kd_gudang', 'dt' => 2, 'field' => 'kd_gudang' ),
			array( 'db' => 'nm_gudang', 'dt' => 3, 'field' => 'nm_gudang' ),
		);

		$data['sql_details'] = sql_connect();

		$data['joinQuery'] = '';

		$data['where'] = '';

		return $data;
	}
	/* --end ssp tabel untuk modul data warehouse-- */

	/* --start button tabel untuk modul data warehouse-- */
	private function tbl_btn($id, $var) {
		$read_access = $this->session->read_access;
		$update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		$btns[] = get_btn(array('access' => $read_access, 'title' => 'Data Rak', 'icon' => 'search', 'onclick' => 'data_rak(\''.$id.'\')'));
		$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah', 'icon' => 'pencil', 'onclick' => 'get_form(\''.$id.'\')'));
		$btns[] = get_btn_divider();
		$btns[] = get_btn(array('access' => $delete_access, 'title' => 'Hapus', 'icon' => 'trash',
			'onclick' => 'return confirm(\'Anda akan menghapus data warehouse = '.$var.'?\')?hapus_data(\''.$id.'\'):false'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}
	/* --end button tabel untuk modul data warehouse-- */

	public function form_select_rules() {
		$rules = array(
			array('field' => 'selTipeBarang', 'label' => 'Tipe Barang', 'rules' => 'required'),
		);
		return $rules;
	}

	public function form_select_warning($datas = '') {
		$forms = array('selTipeBarang');
		foreach ($datas as $key => $data) :
			$str[$data] = (!empty(form_error($forms[$key])))?buildLabel('warning', form_error($forms[$key], '"', '"')):'';
		endforeach;
		return $str;
	}
}