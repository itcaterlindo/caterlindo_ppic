<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_purchaserequisition_logstatus extends CI_Model {
	private $tbl_name = 'tb_purchaserequisition_logstatus';
    private $p_key = 'prlogstatus_kd';
    
    public function __construct() {
		parent::__construct();

		$this->load->helper(['my_helper']);
		$this->load->model(['db_hrm/tb_karyawan']);
    }    

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'prstatus_kd', 
				'dt' => 1, 'field' => 'prstatus_kd',
				'formatter' => function ($d){
					$d = $this->tbl_btn($d);
					
					return $d;
				}),
			array( 'db' => 'prstatus_nama', 
				'dt' => 2, 'field' => 'prstatus_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'prstatus_level', 
				'dt' => 3, 'field' => 'prstatus_level',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM ".$this->tbl_name;
		$data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id) {
		$btns = array();
		if($delete_access == 1 ){
			$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
			$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));
		}		
		$btn_group = group_btns($btns);

		return $btn_group;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }
    
    public function get_all(){
		$query = $this->db->get($this->tbl_name);
		return $query;
	}

	public function get_by_param_detail ($param=[]) {
		$query = $this->db->select($this->tbl_name.'.*, tb_purchaserequisition_status.*, tb_admin.nm_admin')
					->from($this->tbl_name)
					->join('tb_admin', $this->tbl_name.'.prlogstatus_user=tb_admin.kd_admin', 'left')
					->join('tb_purchaserequisition_status', $this->tbl_name.'.prstatus_kd=tb_purchaserequisition_status.prstatus_kd', 'left')
					->where($param)
					->order_by($this->tbl_name.'.prlogstatus_tglinput', 'DESC')
					->get();
		return $query; 
    }
    
    public function statusBy($pr_kd, $prstatus_kd){
		$merge = [];
        $query = $this->db->select($this->tbl_name.'.*, tb_admin.*')
                    ->from ($this->tbl_name)
                    ->join('tb_admin', $this->tbl_name.'.prlogstatus_user=tb_admin.kd_admin', 'left')
                    ->where(array($this->tbl_name.'.pr_kd' => $pr_kd, $this->tbl_name.'.prstatus_kd' => $prstatus_kd))
                    ->order_by($this->tbl_name.'.prlogstatus_tglinput', 'desc')
                    ->get();
        $query = $query->row_array();

        $nm_karyawan = '';
        if (!empty($query)){
            $rowKaryawan = $this->tb_karyawan->get_by_param(array('kd_karyawan' => $query['kd_karyawan']))->row_array();
            $nm_karyawan = $rowKaryawan['nm_karyawan'];
			$merge = array_merge($query, array('nm_karyawan' => !empty($nm_karyawan) ? $nm_karyawan : ''));
		}
		
        return $merge;
	}
	
	public function action_insert_log($pr_kd, $prstatus_level, $keterangan){
		$this->load->model(['tb_purchaserequisition_status']);

		$rowStatus = $this->tb_purchaserequisition_status->get_by_param (['prstatus_level' => $prstatus_level])->row_array();

		$dataLogStatus = [
			'pr_kd' => $pr_kd,
			'prlogstatus_user' => $this->session->userdata('kd_admin'),
			'prstatus_kd' => $rowStatus['prstatus_kd'],
			'prlogstatus_keterangan' => !empty($keterangan) ? $keterangan : null,
			'prlogstatus_tglinput' => date('Y-m-d H:i:s'),
			'admin_kd' => $this->session->userdata('kd_admin'),
		];

		$act = $this->insert_data($dataLogStatus);

		return $act;
	}

	public function get_log_printout($pr_kd = null) {
		$qmaxTrans = $this->db->select('c.wfstate_kd, MAX(a.prlogstatus_tglinput) AS tgl_maxtrans')
				->from($this->tbl_name.' AS a')
				->join('td_workflow_transition as b', 'a.wftransition_kd=b.wftransition_kd', 'left')
				->join('td_workflow_state as c', 'b.wftransition_destination=c.wfstate_kd', 'left')
				->where('a.pr_kd', $pr_kd)
				->group_by('c.wfstate_kd')
				->get_compiled_select();
				
		$query = $this->db->select('aa.*, c.wfstate_nama, cc.kd_karyawan, cc.nm_admin')
				->from($this->tbl_name.' AS aa')
				->join('td_workflow_transition as b', 'aa.wftransition_kd=b.wftransition_kd', 'left')
				->join('td_workflow_state as c', 'b.wftransition_destination=c.wfstate_kd', 'left')
				->join('tb_admin AS cc', 'aa.admin_kd=cc.kd_admin', 'left')
				->join('('.$qmaxTrans.') as tt', 'c.wfstate_kd=tt.wfstate_kd AND aa.prlogstatus_tglinput=tt.tgl_maxtrans', 'right')
				->where('aa.pr_kd', $pr_kd)
				->where('c.wfstate_onprintout', 1)
				->order_by('c.wfstate_onprintoutseq', 'asc')
				->get();
				
		return $query;
	}
	
}