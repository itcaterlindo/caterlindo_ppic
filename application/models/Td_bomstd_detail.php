<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_bomstd_detail extends CI_Model {
	private $tbl_name = 'td_bomstd_detail';
	private $p_key = 'bomstddetail_kd';

	public function ssp_table($bomstd_kd) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'b.part_nama', 
				'dt' => 2, 'field' => 'part_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.bomstddetail_tglinput', 
				'dt' => 3, 'field' => 'bomstddetail_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] = "FROM ".$this->tbl_name." as a
                            LEFT JOIN tm_part as b ON a.part_kd=b.part_kd";
		$data['where'] = "a.bomstd_kd= '".$bomstd_kd."'";
		
		return $data;
	}

	private function tbl_btn($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btns = array();
		$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data_detail(\''.$id.'\')'));	
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
		$this->db->select('MAX('.$this->p_key.') AS code')
			->from($this->tbl_name);
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = (int) substr($code, -8);
		endif;
		$angka = $urutan + 1;
		$primary = 'DBOM'.str_pad($angka, 8, '0', STR_PAD_LEFT);
		return $primary;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }
    
	public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param_detail($param=[]){
		$query = $this->db->select($this->tbl_name.'.*, tm_part.*')
					->from($this->tbl_name)
					->join('tm_part', $this->tbl_name.'.part_kd=tm_part.part_kd', 'left')
					->where($param)
					->get();
		return $query;
	}

	public function insert_batch_data ($data=[]){
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}

}