<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_code_format extends CI_Model {
	private $tbl_name = 'tb_code_format';
	private $p_key = 'id';
	private $title_name = 'Code Format';

	public function get_code_for($code_for = '') {
		$this->db->select('code_for, code_format, code_separator, on_reset')
			->from('tb_code_format')
			->where(array('code_for' => $code_for));
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}
}