<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_admin extends CI_Model {
	private $tbl_name = 'tb_admin';
	private $p_key = 'kd_admin';
	private $title_name = 'Data Admin';

	public function get_row($kd_admin = '') {
		$this->db->from($this->tbl_name)
			->where(array('kd_admin' => $kd_admin));
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}

	public function form_rules() {
		$rules = array(
			array('field' => 'txtNm', 'label' => 'Nama', 'rules' => 'required'),
			array('field' => 'txtUsername', 'label' => 'Username', 'rules' => 'required'),
		);
		if (!empty($this->input->post('txtPassword')) || !empty($this->input->post('txtPassConf'))) :
			$pass = array(
				array('field' => 'txtPassword', 'label' => 'Password', 'rules' => 'required'),
				array('field' => 'txtPassConf', 'label' => 'Konfirmasi Password', 'rules' => 'required|matches[txtPassword]'),
			);
			$rules = array_merge($rules, $pass);
		endif;
		return $rules;
	}

	public function chk_username($username = '', $kd_admin = '') {
		$this->db->from($this->tbl_name)
			->where(array('username' => $username, 'kd_admin !=' => $kd_admin));
		$query = $this->db->get();
		$num = $query->num_rows();

		return $num > 0?FALSE:TRUE;
	}

	public function form_warning($datas = '') {
		$forms = array('txtNm', 'txtUsername');
		foreach ($datas as $key => $data) :
			$str[$data] = (!empty(form_error($forms[$key])))?buildLabel('warning', form_error($forms[$key], '"', '"')):'';
		endforeach;
		if (!empty($this->input->post('txtPassword')) || !empty($this->input->post('txtPassConf'))) :
			$err_pass = array('idErrPass', 'idErrPassConf');
			$pass = array('txtPassword', 'txtPassConf');
			foreach ($err_pass as $key => $data) :
				$str[$data] = (!empty(form_error($pass[$key])))?buildLabel('warning', form_error($pass[$key], '"', '"')):'';
			endforeach;
		endif;
		return $str;
	}

	public function update_profile($data = '') {
		$title = 'Mengubah '.$this->title_name;
		if (!empty($data['kd_admin'])) :
			$chk_username = $this->chk_username($data['username'], $data['kd_admin']);
			if (!$chk_username) :
				$str = $this->report(0, 'Mengubah '.$this->title_name.' Username \''.$data['username'].'\' sudah digunakan!', $data);
				return $str;
				exit();
			endif;
			$act = $this->update($data, array('kd_admin' => $data['kd_admin']));
			if ($act) :
				$_SESSION['username'] = $data['username'];
				$_SESSION['nm_admin'] = $data['nm_admin'];
			endif;
			$str = $this->report($act, $title, $data);
		else :
			$str = $this->report(0, $title.' \'kd_admin\' tidak ada!', $data);
		endif;
		return $str;
	}

	private function create($data = '') {
		$act = $this->db->insert($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	private function update($data = '', $where = '') {
		$act = $this->db->update($this->tbl_name, $data, $where);
		return $act?TRUE:FALSE;
	}

	public function report($act = '', $label = '', $data = '') {
		$this->load->model(array('m_builder'));
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label);
		endif;
		$this->m_builder->write_log($stat, $label, $data);
		return $str;
	}

	public function get_all(){
		$query = $this->db->get($this->tbl_name);
		return $query;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function isAdmin ($kd_admin) {
		$query = $this->get_by_param(array('kd_admin' => $kd_admin));
		if ($query->num_rows() != 0){
			$query = $query->row_array();
			if ($query['tipe_admin_kd'] == 'TPA190617001'){
				$resp = true;
			}else{
				$resp = false;
			}
		}else{
			$resp = false;
		}
		
		return $resp;
	}
	
	public function isCosting ($kd_admin) {
		$query = $this->get_by_param(array('kd_admin' => $kd_admin));
		if ($query->num_rows() != 0){
			$query = $query->row_array();
			if ($query['tipe_admin_kd'] == 'TPA080720001'){
				$resp = true;
			}else{
				$resp = false;
			}
		}else{
			$resp = false;
		}
		
		return $resp;
	}

	public function get_by_param_detail ($param=[]) {
		$this->db->where($param);
		$this->db->join('td_admin_tipe', $this->tbl_name.'.tipe_admin_kd=td_admin_tipe.kd_tipe_admin', 'left');
		$act = $this->db->get($this->tbl_name);
		return $act;
	}
}