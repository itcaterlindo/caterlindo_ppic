<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_m_payment extends CI_Model {
	private $tbl_name = 'Tm_m_payment';
	private $p_key = 'm_payment_kd';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key, 
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function ($d){
					$d = $this->tbl_btn($d);
					
					return $d;
				}),
			array( 'db' => 'nm_bank', 
				'dt' => 2, 'field' => 'nm_bank',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'no_rek', 
				'dt' => 3, 'field' => 'no_rek',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM ".$this->tbl_name;
		$data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id) {
		$btns = array();
		if (cek_permission('PPH_DELETE')) {
			$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));
		}

		$btn_group = group_btns($btns);

		return $btn_group;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

}