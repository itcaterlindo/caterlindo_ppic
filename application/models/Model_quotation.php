<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Model_quotation extends CI_Model {

    public function report_quotation($tahun, $bulan)
    {
        $data = $this->db->select('a.no_quotation,a.tgl_quotation,c.nm_salesperson,d.nm_customer,e.nm_jenis_customer,b.tgl_so,b.no_salesorder,(COALESCE(f.total_harga,0) + COALESCE(g.total_harga,0)) total_harga_exc,(COALESCE(f.item_qty,0) + COALESCE(g.item_qty,0)) total_qty')
						->from('tm_quotation a')
						->join('tm_salesorder b', 'a.kd_mquotation = b.mquotation_kd', 'LEFT')
						->join('tb_salesperson c', 'a.salesperson_kd = c.kd_salesperson', 'LEFT')
						->join('tm_customer d', 'a.customer_kd = d.kd_customer', 'LEFT')
						->join('tb_jenis_customer e', 'd.jenis_customer_kd = e.kd_jenis_customer', 'LEFT')
						->join('(SELECT a.mquotation_kd, SUM(a.total_harga) total_harga, SUM(a.item_qty) item_qty
												FROM td_quotation_item a 
												LEFT JOIN tm_barang b ON b.kd_barang = a.barang_kd 
												GROUP BY a.mquotation_kd) f', 'a.kd_mquotation = f.mquotation_kd', 'LEFT')
						->join('(SELECT a.mquotation_kd, SUM(a.total_harga) total_harga, SUM(a.item_qty) item_qty
												FROM td_quotation_item_detail a 
												LEFT JOIN tm_barang b ON b.kd_barang = a.kd_parent 
												LEFT JOIN tm_barang c ON c.kd_barang = a.kd_child 
												GROUP BY a.mquotation_kd) g', 'a.kd_mquotation = g.mquotation_kd', 'LEFT')
						->where('YEAR(a.tgl_quotation)', $tahun)
						->where('MONTH(a.tgl_quotation)', $bulan)
						->where('a.status_quotation !=', 'cancel')
						->where('a.tipe_customer', 'Lokal')
						->where('c.nm_salesperson !=', 'Kantor')
						->order_by('a.no_quotation ASC, c.nm_salesperson ASC')
						->get()->result_array();
        return $data;
    }

    public function report_quotation_pertahun($tahunAwal, $tahunAkhir)
    {
        $data = $this->db->select('YEAR(a.tgl_quotation) tahun,COUNT(a.tgl_quotation) quotation,SUM(CASE WHEN b.tgl_so IS NOT NULL THEN 1 ELSE 0 END) so')
                ->from('tm_quotation a')
                ->join('tm_salesorder b', 'a.kd_mquotation = b.mquotation_kd', 'LEFT')
                ->join('tb_salesperson c', 'a.salesperson_kd = c.kd_salesperson', 'LEFT')
                ->join('tm_customer d', 'a.customer_kd = d.kd_customer', 'LEFT')
                ->join('tb_jenis_customer e', 'd.jenis_customer_kd = e.kd_jenis_customer', 'LEFT')
                ->where('YEAR(a.tgl_quotation) >=', $tahunAwal)
                ->where('YEAR(a.tgl_quotation) <=', $tahunAkhir)
                ->where('a.status_quotation !=', 'cancel')
                ->where('a.tipe_customer', 'Lokal')
                ->where('c.nm_salesperson !=', 'Kantor')
                ->group_by('YEAR(a.tgl_quotation)')
                ->order_by('a.tgl_quotation ASC')
                ->get()->result_array();
        return $data;
    }

} 
