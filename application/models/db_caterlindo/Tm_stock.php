<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_stock extends CI_Model {
	private $db_name = 'db_caterlindo';
	private $tbl_name = 'tm_stock';
	private $p_key = 'fc_kdstock';
	private $title_name = 'Data Detail Stock Barang';

	public function __construct() {
		$this->db_caterlindo = $this->load->database($this->db_name, TRUE);
	}

	public function get_report_opname($fc_periode = '') {
		$this->db_caterlindo->select('MIN(a.fd_tglinput) AS min_tgl_input, MAX(a.fd_tglinput) AS max_tgl_input')
			->from('td_opname a')
			->where(array('a.fc_periode' => $fc_periode))
			->group_by('DATE(a.fd_tglinput)');
		$query = $this->db_caterlindo->get();
		$row = $query->row();
		if (!empty($row)) :
			$tgl_min = $row->min_tgl_input;
			$tgl_max = $row->max_tgl_input;

			$this->db_caterlindo->select('fc_kdstock AS kd_stock, fd_tgl, fn_sisa')
				->from('td_kartustock')
				->where(array('DATE(fd_tgl) <=' => $tgl_max))
				->order_by('id ASC, fd_tgl ASC, ABS(DATEDIFF(DATE(fd_tgl), \''.$tgl_max.'\')) DESC');
			$query = $this->db_caterlindo->get();
			$result = $query->result();
			foreach ($result as $row) :
				$kd_stock = strlen($row->kd_stock) > 6?substr($row->kd_stock, 0, 6):$row->kd_stock;
				$stocks[$kd_stock] = array($kd_stock, $row->fd_tgl, $row->fn_sisa);
			endforeach;
			foreach ($stocks as $kd_stocks => $result) :
				$kd_barcodes[] = $result[0];
			endforeach;

			$this->db_caterlindo->select('fc_kdstock, fn_qtyopname')
				->from('td_opname')
				->where(array('fc_periode' => $fc_periode))
				->order_by('fc_kdstock');
			$query = $this->db_caterlindo->get();
			$r_tdopname = $query->result();
			foreach ($r_tdopname as $row) :
				$kd_stock = strlen($row->fc_kdstock) > 6?substr($row->fc_kdstock, 0, 6):$row->fc_kdstock;
				$sum[$kd_stock][] = $row->fn_qtyopname;
			endforeach;

			$this->db_caterlindo->select('fc_kdstock, fc_barcode, fv_namastock, fn_qty')
				->from('tm_stock')
				->where_in('fc_barcode', $kd_barcodes)
				->order_by('fc_kdstock');
			$query = $this->db_caterlindo->get();
			$result = $query->result();
			foreach ($result as $row) :
				$qty = $stocks[$row->fc_barcode][2];
				$total = array_key_exists($row->fc_barcode, $sum)?array_sum($sum[$row->fc_barcode]):'0';
				if ($qty > 0 && $total > 0) :
					$data[] = (object) array('fc_kdstock' => $row->fc_kdstock, 'fv_namastock' => $row->fv_namastock, 'fc_barcode' => $row->fc_barcode, 'fn_qty' => $qty, 'qty_opname' => $total);
				endif;
			endforeach;
		else :
			$data[] = (object) array('fc_kdstock' => '-', 'fv_namastock' => '-', 'fc_barcode' => '-', 'fn_qty' => '-', 'qty_opname' => '-');
		endif;

		return $data;
	}

	public function get_report_stock() {
		$tgl_max = date('Y-m-d');

		$this->db_caterlindo->select('fc_kdstock AS kd_stock, fd_tgl, fn_sisa')
			->from('td_kartustock')
			->where(array('DATE(fd_tgl) <=' => $tgl_max))
			->order_by('id ASC, fd_tgl ASC, ABS(DATEDIFF(DATE(fd_tgl), \''.$tgl_max.'\')) DESC');
		$query = $this->db_caterlindo->get();
		$result = $query->result();
		foreach ($result as $row) :
			$kd_stock = strlen($row->kd_stock) > 6?substr($row->kd_stock, 0, 6):$row->kd_stock;
			$stocks[$kd_stock] = array($kd_stock, $row->fd_tgl, $row->fn_sisa);
		endforeach;
		foreach ($stocks as $kd_stocks => $result) :
			$kd_barcodes[] = $result[0];
		endforeach;

		$this->db_caterlindo->select('fc_kdstock, fc_barcode, fv_namastock, fn_qty')
			->from('tm_stock')
			->where_in('fc_barcode', $kd_barcodes)
			->order_by('fc_barcode');
		$query = $this->db_caterlindo->get();
		$result = $query->result();
		foreach ($result as $row) :
			$data[] = (object) array('fc_kdstock' => $row->fc_kdstock, 'fv_namastock' => $row->fv_namastock, 'fc_barcode' => $row->fc_barcode, 'fn_qty' => $row->fn_qty, 'fn_sisa' => $stocks[$row->fc_barcode][2]);
		endforeach;

		return $data;
	}
}