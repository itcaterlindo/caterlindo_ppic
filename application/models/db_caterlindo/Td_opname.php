<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_opname extends CI_Model {
	private $db_name = 'db_caterlindo';
	private $tbl_name = 'td_opname';
	private $p_key = 'id';
	private $title_name = 'Data Detail Opname Barang';

	public function __construct() {
		$this->db = $this->load->database($this->db_name, TRUE);
	}

	public function form_rules() {
		$rules = array(
			array('field' => 'txtTglOpname', 'label' => 'Tgl Opname', 'rules' => 'required'),
		);
		return $rules;
	}

	public function build_warning($datas = '') {
		$forms = array('txtTglOpname');
		foreach ($datas as $key => $data) :
			$str[$data] = (!empty(form_error($forms[$key])))?buildLabel('warning', form_error($forms[$key], '"', '"')):'';
		endforeach;
		return $str;
	}
}