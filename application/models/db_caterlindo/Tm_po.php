<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_po extends CI_Model {
	private $db_name = 'db_caterlindo';
	private $tbl_name = 'tm_po';
	private $p_key = 'fc_nopo';
	private $title_name = 'Data Master Purchase Order';

	public function __construct() {
		$this->db = $this->load->database($this->db_name, TRUE);
	}

	public function get_row($key = '') {
		$this->db->from($this->tbl_name)
			->where(array($this->p_key => $key));
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}

	public function count_row($key = '') {
		$this->db->from($this->tbl_name)
			->where(array($this->p_key => $key));
		$query = $this->db->get();
		$num = $query->num_rows();
		return $num;
	}

	public function submit_data($data = '') {
		// Check no purchase order sudah ada atau belum
		if (!empty($data[$this->p_key])) :
			$row = $this->get_row(substr($data[$this->p_key], 0, 20));
			if (!empty($row)) :
				$label = 'Mengubah ';
				$act = $this->update_data($data);
			else :
				$label = 'Menambahkan ';
				$act = $this->insert_data($data);
			endif;
			$str = $this->report($act, $label.$this->title_name, $data);
			return $str;
		else :
			$str = $this->report(0, 'Submit '.$this->title_name.'<br>No PO pada SO kosong bossque!', $data);
			return $str;
		endif;
	}

	public function insert_data($data = '') {
		$submit = array_merge($data, array('fc_userid' => $this->session->username, 'fc_status' => 'I'));
		$act = $this->db->insert($this->tbl_name, $submit);
		return $act?TRUE:FALSE;
	}

	public function update_data($data = '') {
		$submit = array_merge($data, array('fc_userid' => $this->session->username));
		$act = $this->db->update($this->tbl_name, $submit, array($this->p_key => $data[$this->p_key]));
		return $act?TRUE:FALSE;
	}

	public function delete_data($id = '') {
		$act['master'] = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		$act['items'] = $this->db->delete('td_po', array($this->p_key => $id));
		$report = $this->report($act, 'Menghapus', array($this->p_key => $id, 'Kode Master Delivery Order' => $id));
		return $report;
	}

	public function report($act = 0, $label = '', $data = array('')) {
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label);
		endif;
		$this->m_builder->write_log($stat, $label, $data);
		return $str;
	}
}