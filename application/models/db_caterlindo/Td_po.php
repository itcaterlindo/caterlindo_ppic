<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_po extends CI_Model {
	private $db_name = 'db_caterlindo';
	private $tbl_name = 'td_po';
	private $p_key = 'id';
	private $title_name = 'Data Detail Purchase Order';

	public function __construct() {
		$this->db = $this->load->database($this->db_name, TRUE);
	}

	public function select_from_po($no_po = '') {
		$this->db->from($this->tbl_name)
			->where(array('fc_nopo' => $no_po));
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function submit_data($data = '', $no_po = '') {
		$this->load->model(array('tm_po'));

		if (empty($no_po)) :
			return $this->report(0, 'Menambahkan '.$this->title_name);
		else :
			if (!empty($data)) :
				foreach ($data['item_barcode'] as $key => $barcode) :
					$stuff_qty = $data['stuffing_item_qty'][$barcode];
					$parent_data[$barcode] = array('fc_nopo' => $no_po, 'fc_barcode' => $barcode, 'fn_qtyout' => $stuff_qty, 'fc_status' => 'I');
				endforeach;
				$this->db->delete($this->tbl_name, array('fc_nopo' => $no_po));
				$act = $this->create($parent_data);
				$str = $this->report($act, 'Menambahkan '.$this->title_name);
			endif;
			if (empty($po_items) && empty($data)) :
				$str = $this->tm_po->delete_data($no_po);
			endif;
			return $str;
		endif;
	}

	private function create($data = '') {
		$act = $this->db->insert_batch($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	public function report($act = '', $label = '') {
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
		endif;
		return $str;
	}
}