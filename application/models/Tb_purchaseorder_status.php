<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_purchaseorder_status extends CI_Model {
	private $tbl_name = 'tb_purchaseorder_status';
	private $p_key = 'postatus_kd';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'postatus_kd', 
				'dt' => 1, 'field' => 'postatus_kd',
				'formatter' => function ($d){
					$d = $this->tbl_btn($d);
					
					return $d;
				}),
			array( 'db' => 'postatus_nama', 
				'dt' => 2, 'field' => 'postatus_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'postatus_level', 
				'dt' => 3, 'field' => 'postatus_level',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM ".$this->tbl_name;
		$data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id) {
		// $read_access = $this->session->read_access;
		// $update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		if($delete_access == 1 ){
			$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
			$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));
		}		
		$btn_group = group_btns($btns);

		return $btn_group;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }
    
    public function get_all(){
		$query = $this->db->get($this->tbl_name);
		return $query;
	}

	public function isMaxState($postatus_kd){
		$maxLevel = $this->db->select_max('postatus_level')->get($this->tbl_name)->row_array();
		$befMax = $maxLevel['postatus_level'] - 1;
		$maxLevelRow = $this->get_by_param(['postatus_level' => $befMax])->row_array();
		if ($postatus_kd == $maxLevelRow['postatus_kd']){
			$resp = true;
		}else{
			$resp = false;
		}

		return $resp;
	}

	public function isEditingState($postatus_kd){
		$maxLevelRow = $this->get_by_param(['postatus_level' => 1])->row_array();
		if ($postatus_kd == $maxLevelRow['postatus_kd']){
			$resp = true;
		}else{
			$resp = false;
		}

		return $resp;
	}

	public function isFinishState($postatus_kd){
		$maxLevel = $this->db->select_max('postatus_level')->get($this->tbl_name)->row_array();
		$maxLevelRow = $this->get_by_param(['postatus_level' => $maxLevel['postatus_level']])->row_array();
		if ($postatus_kd == $maxLevelRow['postatus_kd']){
			$resp = true;
		}else{
			$resp = false;
		}

		return $resp;
	}

	public function finishStatus () {
		$maxLevel = $this->db->select_max('postatus_level')->get($this->tbl_name)->row_array();
        $maxLevelRow = $this->get_by_param(['postatus_level' => $maxLevel['postatus_level']])->row_array();
        return $maxLevelRow;
	}
	
}