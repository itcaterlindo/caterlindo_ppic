<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_workflow extends CI_Model {
	private $tbl_name = 'tm_workflow';
	private $p_key = 'wf_kd';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'wf_nama', 
				'dt' => 2, 'field' => 'wf_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'wf_tglinput', 
				'dt' => 3, 'field' => 'wf_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name;
        $data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id) {
		$btns = array();
		$btns[] = get_btn(array('title' => 'Lihat State', 'icon' => 'list', 'onclick' => 'open_state(\''.$id.'\')'));	
		$btns[] = get_btn(array('title' => 'Lihat Transisi', 'icon' => 'list', 'onclick' => 'open_transition(\''.$id.'\')'));	
		$btn_group = group_btns($btns);

		return $btn_group;
	}

    public function create_code () {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_where_in ($where, $in = []) {
		$query = $this->db->where_in($where, $in)
				->select($this->tbl_name.'.*')
				->get($this->tbl_name);
		return $query;
	}

	public function update_batch ($idColumn, $data = []) {
		$act = $this->db->update_batch($this->tbl_name, $data, $idColumn);
		return $act;
	}

	public function generate_button ($id, $wf_kd, $wfstate_kd) {
		$admin_kd = $this->session->userdata('kd_admin');
		$admin = $this->db->where('kd_admin', $admin_kd)->get('tb_admin')->row_array();
		$kd_tipe_admin = $admin['tipe_admin_kd'];

		$transitions = $this->db->select('td_workflow_transition.*, td_workflow_transition_permission.wftransitionpermission_adminkd, td_workflow_transition_permission.kd_tipe_admin')
					->from('td_workflow_transition')
					->join('td_workflow_transition_permission', 'td_workflow_transition.wftransition_kd=td_workflow_transition_permission.wftransition_kd','left')
					->where('td_workflow_transition.wf_kd', $wf_kd)
					->where('td_workflow_transition.wftransition_source', $wfstate_kd)
					->where('td_workflow_transition.wftransition_active', '1')
					->get()->result_array();
	
		$html = '<div class="btn-group">
		<button type="button" class="btn btn-warning btn-flat dropdown-toggle" data-toggle="dropdown">
			<span class="fa fa fa-hand-o-right"></span>
			Ubah State
		  	<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">';
		$allowTransitions = [];
		foreach ($transitions as $transition) {
			if ($transition['wftransitionpermission_adminkd'] == $admin_kd || $transition['kd_tipe_admin'] == $kd_tipe_admin){
				$allowTransitions[] = [
					'wftransition_kd' => $transition['wftransition_kd'],
					'wf_kd' => $transition['wf_kd'],
					'wftransition_nama' => $transition['wftransition_nama'],
					'wftransition_email' => $transition['wftransition_email'],
				];
			}
		}
		#Group by wftransition_kd
		$tempArrayExist = []; $arrayGroups = [];
		foreach ($allowTransitions as $allowTransition) {
			if (!in_array($allowTransition['wftransition_kd'], $tempArrayExist)){
				$arrayGroups[] = $allowTransition;
				$tempArrayExist[] = $allowTransition['wftransition_kd'];
			}
		}
		#after group
		foreach ($arrayGroups as $arrayGroup) {
			$html .= '<li><a href="javascript:void(0)" onclick=ubah_state("'.$id.'",'.$arrayGroup['wftransition_kd'].')> <i class="fa fa-hand-o-right"></i> '.$arrayGroup['wftransition_nama'].'</a></li>';
		}
		
		$html .= '
		</ul>
	  </div>';

		return $html;

	}
	
}