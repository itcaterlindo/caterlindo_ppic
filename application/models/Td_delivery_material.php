<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_delivery_material extends CI_Model {
	private $tbl_name = 'td_delivery_material';
	private $p_key = 'deliverymaterial_kd';

	public function ssp_table2($year, $month) {

		$draw = intval($this->input->post("draw"));
		$start = intval($this->input->post("start"));
		$length = intval($this->input->post("length"));

		$viewPermission = cek_permission("DELIVERYMATERIAL_VIEW");
		if($viewPermission){
			$qData = $this->db->query("SELECT x.deliverymaterial_kd, x.deliverymaterial_no, a.rm_kode, a.rm_nama, a.rm_deskripsi, a.rm_spesifikasi, x.deliverymaterial_qty, c.rmsatuan_nama, x.status, too.nm_gudang, fr.nm_gudang as p, x.deliverymaterial_remark, x.ke_gudang
				FROM ".$this->tbl_name." as x
				LEFT JOIN tm_rawmaterial as a ON a.rm_kd=x.rm_kd
				LEFT JOIN td_rawmaterial_kategori as b ON a.rmkategori_kd=b.rmkategori_kd
				LEFT JOIN td_rawmaterial_satuan as c ON a.rmsatuan_kd=c.rmsatuan_kd
				INNER JOIN tm_gudang as too ON too.kd_gudang=x.ke_gudang
				INNER JOIN tm_gudang as fr ON fr.kd_gudang=x.dari_gudang
				WHERE YEAR(x.deliverymaterial_tanggal) = '".$year."' AND MONTH(x.deliverymaterial_tanggal) = '".$month."'
				ORDER BY x.deliverymaterial_tgledit DESC")
				->result_array();
		}else{
			$qData = [];
		}

		$data = [];
		foreach ($qData as $r) {
			$data[] = [
				'1' => $this->tbl_btn($r['deliverymaterial_kd'], $r['status'], $r['ke_gudang']),
				'2' => $r['deliverymaterial_no'],
				'3' => $r['rm_kode'],
				'4' => $r['rm_nama'],
				'5' => $r['rm_deskripsi'],
				'6' => $r['rm_spesifikasi'],
				'7' => $r['deliverymaterial_qty'],
				'8' => $r['rmsatuan_nama'],
				'9' => $r['status'] == 'terkirim' ? bg_label($r['status'], 'yellow') : bg_label($r['status'], 'green'),
				'10' => $r['nm_gudang'],
				'11' => $r['p'],
				'12' => $r['deliverymaterial_remark'],
			];
		}

		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;

	}

	private function tbl_btn($id, $status, $gudang) {
		
		$this->load->model('tb_deliverymaterial_user');
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$kd_tipe_admin = $this->session->userdata('tipe_admin_kd');
		$userDRM = $this->tb_deliverymaterial_user->get_by_param(['kd_tipe_admin' => $kd_tipe_admin, 'kd_gudang' => $gudang, 'flag_terima' => '1'])->row_array();
		$btns = array();
		if($status == 'terkirim'){
			if(!empty($userDRM)){
				$btns[] = get_btn(array('title' => 'Terima Item', 'icon' => 'check', 'onclick' => 'terima(\''.$id.'\')'));	
			}
			if(cek_permission("DELIVERYMATERIAL_DELETE")){
				$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus(\''.$id.'\')'));
			}
		}else{
			$btns[] = get_btn(array('title' => 'History Inspection', 'icon' => 'history', 'onclick' => 'history_inspection(\''.$id.'\')'));	
		}
		
		$btn_group = group_btns($btns);
		return $btn_group;
	}

    public function create_code () {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

	public function create_no ($tgl) {
		$drm_year = format_date($tgl, 'Y');
		$query = $this->db->select('MAX(deliverymaterial_no) as max_no')
			->where('YEAR(deliverymaterial_tanggal)', $drm_year)
			->get($this->tbl_name)->row();
		if (empty($query->max_no)) :
			$no = 0;
		else :
			$no = substr($query->max_no, -3);
		endif;
		$no = (int)$no + 1;
		$drm_no = $drm_year . '.' . str_pad($no, '3', '000', STR_PAD_LEFT);
		return $drm_no;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_where_in ($where, $in = []) {
		$query = $this->db->where_in($where, $in)
				->select($this->tbl_name.'.*')
				->get($this->tbl_name);
		return $query;
	}

	public function update_batch ($idColumn, $data = []) {
		$act = $this->db->update_batch($this->tbl_name, $data, $idColumn);
		return $act;
	}
	
}