<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class M_app_setting extends CI_Model {
	private $tbl_name = 'tb_app_setting';

	public function get_data($var) {
		$this->db->from($this->tbl_name);
		$this->db->where(array('nm_setting' => $var));
		$query = $this->db->get();
		$row = $query->row();

		return $row;
	}

	public function get_all() {
		$this->db->from($this->tbl_name);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function submit_data($data) {
		$this->db->trans_start();
		$this->db->update_batch($this->tbl_name, $data, 'id');
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) :
			$str['confirm'] = 'error';
			$str['idErrForm'] = buildAlert('danger', 'Gagal!', 'Mengubah data setting sistem!');
		else :
			$str['confirm'] = 'success';
			$str['idErrForm'] = buildAlert('success', 'Berhasil!', 'Mengubah data setting sistem!');
		endif;
		return $str;
	}

	public function update_data($data, $where) {
		$column = $data;
		$bind = $where;
		$query = $this->db->update($this->tbl_name, $column, $bind);
		
		return $query?TRUE:FALSE;
	}

	public function default_disc($tipe_harga_customer = '') {
		$tipe_harga = strtolower(substr($tipe_harga_customer, 6));
		$this->db->select('nilai_setting');
		$this->db->from($this->tbl_name);
		$this->db->where(array('nm_setting' => 'default_diskon'));
		$this->db->group_start();
		$this->db->where(array('tipe_harga' => $tipe_harga));
		$this->db->or_where(array( 'tipe_harga' => 'semua'));
		$this->db->group_end();
		$query = $this->db->get();
		$row = $query->row();
		if ($row) :
			$disc = $row->nilai_setting;
		else :
			$disc = '0';
		endif;
		return $disc;
	}
}