<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_rawmaterial_stok_master extends CI_Model {
	private $tbl_name = 'td_rawmaterial_stok_master';
	private $p_key = 'rmstokmaster_kd';

	// public function ssp_table($date) {
	// 	$data['table'] = $this->tbl_name;

	// 	$data['primaryKey'] = $this->p_key;

	// 	$data['columns'] = array(
	// 		array( 'db' => $this->p_key,
	// 			'dt' => 1, 'field' => $this->p_key,
	// 			'formatter' => function($d){
					
	// 				return $this->tbl_btn($d);
	// 			}),
	// 		array( 'db' => 'a.rmstok_tglinput', 
	// 			'dt' => 2, 'field' => 'rmstok_tglinput',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'b.po_no', 
	// 			'dt' => 3, 'field' => 'po_no',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
    //             }),
	// 		array( 'db' => 'a.rm_kd', 
	// 			'dt' => 4, 'field' => 'rm_kd',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
    //             }),
    //         array( 'db' => 'c.podetail_nama', 
	// 			'dt' => 5, 'field' => 'podetail_nama',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
    //         array( 'db' => 'c.podetail_deskripsi', 
	// 			'dt' => 6, 'field' => 'podetail_deskripsi',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
    //         array( 'db' => 'a.rmstok_qty', 
	// 			'dt' => 7, 'field' => 'rmstok_qty',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
    //         array( 'db' => 'a.rmstok_konversiqty', 
	// 			'dt' => 8, 'field' => 'rmstok_konversiqty',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 	);

	// 	$data['sql_details'] = sql_connect();
	// 	$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
	// 						LEFT JOIN tm_purchaseorder as b ON a.po_kd=b.po_kd
	// 						LEFT JOIN td_purchaseorder_detail as c ON a.podetail_kd=c.podetail_kd";
    //     $data['where'] = "DATE(a.rmstok_tglinput)='".$date."'";
		
	// 	return $data;
	// }

	// private function tbl_btn($id) {
	// 	$read_access = 1;
	// 	$update_access = 1;
	// 	$delete_access = 1;
	// 	$btns = array();
	// 	$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));	
	// 	$btn_group = group_btns($btns);

	// 	return $btn_group;
	// }

	public function create_code($code) {
		$urutan = 0;
		if (empty($code)) {
			$this->db->select($this->p_key.' AS code')
				->from($this->tbl_name)
				->where(array('DATE(rmstokmaster_tglinput)' => date('Y-m-d')))
				->order_by('rmstokmaster_tglinput DESC, '.$this->p_key.' DESC');
			$query = $this->db->get();
			$num = $query->num_rows();
			if ($num > 0) :
				$row = $query->row();
				$code = $row->code;
				$urutan = (int) substr($code, -6);
			endif;
		}else{
			$urutan = (int) substr($code, -6);
		}
		$angka = $urutan + 1;
		$primary = 'RSM'.date('ymd').str_pad($angka, 6, '0', STR_PAD_LEFT);
		return $primary;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function update_stok($rmstokmaster_kd, $jnsTrans, $qty){
        $rowStok = $this->get_by_param(['rmstokmaster_kd' => $rmstokmaster_kd])->row_array();
        if ($jnsTrans == 'IN'){
            $newQty = $rowStok['rmstokmaster_qty'] + $qty;
        }elseif ($jnsTrans == 'OUT'){
            $newQty = $rowStok['rmstokmaster_qty'] - $qty;
        }

        $data = array(
            'rmstokmaster_qty' => $newQty,
            'rmstokmaster_tgledit' => now_with_ms(),
        );

		$act = $this->update_data(['rmstokmaster_kd' => $rmstokmaster_kd], $data);
		return $act;
	}
	    
	public function get_rmstokmasterkd_batch ($arrayRmkds = []) {
		foreach($arrayRmkds as $rmkd) {
            $arrayRmkd[] = $rmkd['rm_kd'];
        }
        $rowRMstok = $this->get_by_param_in('rm_kd', $arrayRmkd)->result_array();
        
        $code = null;
        $arrayRespExist = [];
        $arrayRespNull = [];
        foreach ($arrayRmkds as $eachRmkd) {
            $rmstokmaster_kd = null;
            $rmstokmaster_qty = 0;
            foreach ($rowRMstok as $eachRMstok) {
                if ($eachRMstok['rm_kd'] == $eachRmkd['rm_kd']) {
                    /** Exist on table */
                    $rmstokmaster_kd = $eachRMstok['rmstokmaster_kd'];
                    $rmstokmaster_qty = $eachRMstok['rmstokmaster_qty'];
                }
            }
            /** Not Exist */
            if (empty($rmstokmaster_kd)) {
                $code = $this->create_code($code);
                $arrayRespNull[] = [
                    'rmstokmaster_kd' => $code,
                    'rm_kd' => $eachRmkd['rm_kd'],
					'kd_gudang' => 'MGD260719003',
                    'rmstokmaster_qty' => $rmstokmaster_qty,
                    'rmstokmaster_tglinput' => now_with_ms(),
                    'admin_kd' => $this->session->userdata('kd_admin'),
                ];
            }else{
                /** Exist */
                $arrayRespExist[] = [
                    'rmstokmaster_kd' => $rmstokmaster_kd,
                    'rm_kd' => $eachRmkd['rm_kd'],
					'kd_gudang' =>'MGD260719003',
                    'rmstokmaster_qty' => $rmstokmaster_qty,
                    'rmstokmaster_tglinput' => null,
                    'admin_kd' => null,
                ];
            }
        }
        if (!empty($arrayRespNull)) {
            $actInsertMater = $this->insert_batch($arrayRespNull);
        }
        $response = array_merge($arrayRespNull, $arrayRespExist);
        
		return $response;
	}

	public function get_by_param_in($column, $data=[]) {
		$query = $this->db->where_in($column, $data)->get($this->tbl_name);
		return $query;
    }
    	
	public function update_stock_batch ($jnsTrans, $data = []){
		$arrayRmstok = array();
		$arrUpdateBatch = array();
		$groups = array();
		$groupResult = array();

		/** Group by rmstokmaster_kd */
		foreach ($data as $item){
			$key = $item['rmstokmaster_kd'];
			if (empty($item['rmstokmaster_kd'])) {
				continue;
			}
			if (!array_key_exists($key, $groups)) {
				$groups[$key] = array(
					'rmstokmaster_kd' => $key,
					'qty' => $item['qty'],
				);
			}else {
				$groups[$key]['qty'] = $groups[$key]['qty'] + $item['qty'];
			}
		}
		foreach ($groups as $each){
			$groupResult[] = $each;
		}
		/** Group by rmstok_kd */
		foreach ($groupResult as $d){
			$arrayRmstok[] = $d['rmstokmaster_kd'];
		}

		$rmstokmasters = $this->get_by_param_in($this->p_key, $arrayRmstok)->result_array();
		foreach ($rmstokmasters as $each){
			$kal = 0;
			foreach ($groupResult as $eachData){
				if ($eachData['rmstokmaster_kd'] == $each['rmstokmaster_kd'] ){
					if ($jnsTrans == 'IN'){
						$kal = (float) $each['rmstokmaster_qty'] + $eachData['qty'];
					}elseif($jnsTrans == 'OUT'){
						$kal = (float) $each['rmstokmaster_qty'] - $eachData['qty'];
					}
				}
			}
			$arrUpdateBatch [] = array (
				'rmstokmaster_kd' => $each['rmstokmaster_kd'],
				'rmstokmaster_qty' => $kal,
				'rmstokmaster_tgledit' => now_with_ms(),
			);
		}
		$act = $this->db->update_batch($this->tbl_name, $arrUpdateBatch, $this->p_key);
		return $act;
	}

	public function delete_by_param($param, $kode) {
		$query = $this->db->delete($this->tbl_name, array($param => $kode)); 
		return $query?TRUE:FALSE;
	}

	public function getLastStokopnameDate($rm_kd) {
		$row = $this->db->where('rm_kd', $rm_kd)
			->get($this->tbl_name)->row_array();
		return $row['rmstokmaster_stokopname_tgl'];
	}

	public function get_by_param_in_detail($column, $data=[]) {
		$query = $this->db->select()
			->where_in($column, $data)
			->join('tm_rawmaterial', "tm_rawmaterial.rm_kd={$this->tbl_name}.rm_kd", 'left')
			->get($this->tbl_name);
		return $query;
    }

	public function update_batch ($idColumn, $data = []) {
		$act = $this->db->update_batch($this->tbl_name, $data, $idColumn);
		return $act;
	}

	public function generateMutasiStokResult($rm_kds = array(), $endTime)
	{
		$this->load->model(array('td_rawmaterial_in', 'td_rawmaterial_out'));
		/** Get last Opname */
		$lastOpnames = $this->db->select("{$this->tbl_name}.*, tm_rawmaterial.rm_konversiqty, tm_rawmaterial.rmsatuansecondary_kd")
			->join('tm_rawmaterial', "tm_rawmaterial.rm_kd={$this->tbl_name}.rm_kd",'left')
			->where_in("{$this->tbl_name}.rm_kd", $rm_kds)
			->get($this->tbl_name)->result_array();
		
		$arrMutasi = array();
		foreach ($lastOpnames as $lastOpname){
			$mutasiIn = $this->td_rawmaterial_in->get_kartustock($lastOpname['rm_kd'], $lastOpname['rmstokmaster_stokopname_tgl'], $endTime);
			$mutasiOut = $this->td_rawmaterial_out->get_kartustock($lastOpname['rm_kd'], $lastOpname['rmstokmaster_stokopname_tgl'], $endTime);
			$sum_qty_in = 0;
			$sum_qty_out = 0;
			$qty_aft_mutasi = $lastOpname['rmstokmaster_stokopname_qty'];
			if (!empty($mutasiIn)){
				$arrQtyIn = array_column($mutasiIn, 'trans_qty_in');
				$sum_qty_in = array_sum($arrQtyIn);
				$qty_aft_mutasi += $sum_qty_in;
			}
			if (!empty($mutasiOut)){
				$arrQtyOut = array_column($mutasiOut, 'trans_qty_out');
				$sum_qty_out = array_sum($arrQtyOut);
				$qty_aft_mutasi -= $sum_qty_out;
			}
			$arrMutasi[$lastOpname['rm_kd']] = [
				'rm_kd' => $lastOpname['rm_kd'],
				'rm_konversiqty' => $lastOpname['rm_konversiqty'],
				'rmsatuansecondary_kd' => $lastOpname['rmsatuansecondary_kd'],
				'rmstokmaster_stokopname_tgl' => $lastOpname['rmstokmaster_stokopname_tgl'],
				'rmstokmaster_stokopname_qty' => $lastOpname['rmstokmaster_stokopname_qty'],
				'end_time' => $endTime,
				'mutasi_in' => $mutasiIn,
				'mutasi_out' => $mutasiOut,
				'sum_qty_in' => $sum_qty_in,
				'sum_qty_out' => $sum_qty_out,
				'qty_aft_mutasi' => $qty_aft_mutasi
			];
		}
		return $arrMutasi;		
	}

	/** Function for purchase suggest summary material */
	/** Parameter raw material kode = array */
	public function getLastStokResult($rm_kd = [], $endTime)
	{
		$this->load->model(array('td_rawmaterial_in', 'td_rawmaterial_out'));
		/** Get last Opname */
		$lastOpnames = $this->db->select("{$this->tbl_name}.*, tm_rawmaterial.rm_konversiqty, tm_rawmaterial.rmsatuansecondary_kd")
			->join('tm_rawmaterial', "tm_rawmaterial.rm_kd={$this->tbl_name}.rm_kd",'left')
			->where_in("{$this->tbl_name}.rm_kd", $rm_kd)
			->get($this->tbl_name)->result_array();
		$mutasiResult = [];
		$i = 0;
		foreach ($lastOpnames as $lastOpname){
			$mutasiIn = $this->td_rawmaterial_in->get_kartustock($lastOpname['rm_kd'], $lastOpname['rmstokmaster_stokopname_tgl'], $endTime);
			$mutasiOut = $this->td_rawmaterial_out->get_kartustock($lastOpname['rm_kd'], $lastOpname['rmstokmaster_stokopname_tgl'], $endTime);
			$sum_qty_in = 0;
			$sum_qty_out = 0;
			$qty_aft_mutasi = $lastOpname['rmstokmaster_stokopname_qty'];
			if (!empty($mutasiIn)){
				$arrQtyIn = array_column($mutasiIn, 'trans_qty_in');
				$sum_qty_in = array_sum($arrQtyIn);
				$qty_aft_mutasi += $sum_qty_in;
			}
			if (!empty($mutasiOut)){
				$arrQtyOut = array_column($mutasiOut, 'trans_qty_out');
				$sum_qty_out = array_sum($arrQtyOut);
				$qty_aft_mutasi -= $sum_qty_out;
			}
			$mutasiResult[$i] = [
				'rm_kd' => $lastOpname['rm_kd'],
				'qty_aft_mutasi' => !isset($qty_aft_mutasi) ? 0 : $qty_aft_mutasi
			];
			$i++;
		}
		return $mutasiResult;		
	}

}