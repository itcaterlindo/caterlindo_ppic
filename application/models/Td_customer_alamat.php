<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_customer_alamat extends CI_Model {
	private $tbl_name = 'td_customer_alamat';
	private $p_key = 'kd_alamat_kirim';

	public function create_code() {
		$query = $this->db->select('MAX(kd_alamat_kirim) as maxCode')
				->where(array('DATE(tgl_input)' => date('Y-m-d') ))
                ->get($this->tbl_name);
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row_array();
			$urutan = substr($row['maxCode'], -6);
		endif;
		$angka = $urutan + 1;
		$primary = 'ALC'.date('dmy').str_pad($angka, 6, '0', STR_PAD_LEFT);
		return $primary;
	}
	
	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}
    
}