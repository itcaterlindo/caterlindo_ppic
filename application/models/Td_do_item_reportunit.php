<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_do_item_reportunit extends CI_Model {
	private $tbl_name = 'td_do_item_reportunit';
	private $p_key = 'doitemrepunit_kd';

    public function create_code () {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_where_in ($where, $in = []) {
		$query = $this->db->where_in($where, $in)
				->select($this->tbl_name.'.*')
				->get($this->tbl_name);
		return $query;
	}

	public function update_batch ($idColumn, $data = []) {
		$act = $this->db->update_batch($this->tbl_name, $data, $idColumn);
		return $act;
	}

    public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

    public function generatereport_strukturbiaya($kd_mdo = null, $kat_barang_kd=null) {
        $resp = $this->get_data_report($kd_mdo, $kat_barang_kd);
        return $resp;
    }

    public function generatereport_strukturbiaya_group_hscode($kd_mdo = null) {
        $resp = $this->get_data_report_group_hscode($kd_mdo);
        return $resp;
    }

    public function get_data_report($kd_mdo = null, $kat_barang_kd=null)
    {
        $this->load->model(['tm_deliveryorder']);
        $data = [];
        if (!empty($kd_mdo)){
            /** Check DO banyak SO */
            $DOkode = $this->tm_deliveryorder->get_by_param(['kd_mdo' => $kd_mdo])->row_array();
            $DOinv = $this->tm_deliveryorder->get_by_param(['no_invoice' => $DOkode['no_invoice']])->result_array();
            /** array untuk where in di raw query */
            $implodeKdMdo = "'".implode("','", array_column($DOinv, 'kd_mdo'))."'"; 

            # ----- do -----
            $data['do'] = $this->db->select()
                    ->from('tm_deliveryorder as a')
                    ->where('kd_mdo', $kd_mdo)
                    ->get()->row_array();

            # ----- header -----
            $data['headers'] = $this->db->select('b.kat_barang_kd, c.nm_kat_barang, b.hs_code')
                    ->from('td_do_item_reportunit as a')
                    ->join('tm_barang as b', 'a.kd_barang=b.kd_barang', 'left')
                    ->join('tm_kat_barang as c', 'c.kd_kat_barang=b.kat_barang_kd', 'left')
                    ->where_in('a.kd_mdo', array_column($DOinv, 'kd_mdo'));
            if (!empty($kat_barang_kd)){
                $data['headers'] = $data['headers']->where('b.kat_barang_kd', $kat_barang_kd);
            }
            $data['headers'] = $data['headers']->group_by('b.kat_barang_kd')
                    ->order_by('b.hs_code', 'asc')
                    ->get()->result_array();
            
            # ----- partdetails (sekalian generate sum plate main) -----
            $partdetails = $this->db->select('b.*, c.rm_kode, c.rm_hs_code, c.rm_jenisperolehan, c.negara_id, c.rm_kode, d.nm_negara, e.negaragroup_kd, e.negaragroup_nama, 
                f.kd_barang, f.item_code, g.kd_kat_barang, i.partmain_nama, b.partdetail_hargatotal')
                    ->from('td_do_item_reportunit as a')
                    ->join('td_part_detail as b', 'b.part_kd=a.part_kd', 'left')
                    ->join('tm_rawmaterial as c', 'c.rm_kd=b.rm_kd', 'left')
                    ->join('tb_negara as d', 'd.id=c.negara_id', 'left')
                    ->join('tb_negara_group as e', 'e.negaragroup_kd=d.negaragroup_kd', 'left')
                    ->join('tm_barang as f', 'f.kd_barang=a.kd_barang', 'left')
                    ->join('tm_kat_barang as g', 'g.kd_kat_barang=f.kat_barang_kd', 'left')
                    ->join('td_part as h', 'h.part_kd=b.part_kd', 'left')
                    ->join('tm_part_main as i', 'i.partmain_kd=h.partmain_kd', 'left')
                    ->where_in('a.kd_mdo', array_column($DOinv, 'kd_mdo'));
            if (!empty($kat_barang_kd)){
                $partdetails = $partdetails->where('g.kd_kat_barang', $kat_barang_kd);
            }
            $partdetails = $partdetails->get()->result_array();
            
                # partdetailplate
            $part_kds =  $this->db->distinct()->select('a.part_kd')
                    ->from('td_do_item_reportunit as a')
                    ->from('tm_barang as b', 'b.kd_barang=a.kd_barang', 'left')
                    ->where_in('a.kd_mdo', array_column($DOinv, 'kd_mdo'))
                    ->where('a.part_kd IS NOT NULL');
                if (!empty($kat_barang_kd)){
                    $part_kds = $part_kds->where('b.kat_barang_kd', $kat_barang_kd);
                }
            $part_kds = $part_kds->get()->result_array();
            $arrayPart_kds = array_column($part_kds, 'part_kd');
            
            $partdetailplates = $this->db->select()
                ->from('td_part_detailplate as a')
                ->where_in('a.part_kd', $arrayPart_kds)
                ->where('a.partdetailplate_jenis', 'main')
                ->get()->result_array();
                # Jumlahkan plate main harga total
            $arrayPlatedetailplates = []; $sumMain = []; $arraysumPlatedetailplates = [];
            foreach ($partdetailplates as $partdetailplate) {
                $arrayPlatedetailplates[$partdetailplate['partdetail_kd']][] =  $partdetailplate['partdetailplate_hargatotal'];
            }
            foreach ($arrayPlatedetailplates as $arrayPlatedetailplate => $hrgtotal) {
                $arraysumPlatedetailplates[$arrayPlatedetailplate] = array_sum($hrgtotal);
            }
                # proses include dengan part detail
            $arrayResultdetailplates = [];
            foreach($partdetails as $partdetail){
                $partdetail_hargatotal = $partdetail['partdetail_hargatotal'];
                if (isset($arraysumPlatedetailplates[$partdetail['partdetail_kd']])){
                    $partdetail_hargatotal = $arraysumPlatedetailplates[$partdetail['partdetail_kd']];
                }
                
                $arrayResultdetailplates[] = [
                    'partdetail_kd' => $partdetail['partdetail_kd'],
                    'part_kd' => $partdetail['part_kd'],
                    'kd_kat_barang' => $partdetail['kd_kat_barang'],
                    'kd_barang' => $partdetail['kd_barang'],
                    'rm_kd' => $partdetail['rm_kd'],
                    'rm_kode' => $partdetail['rm_kode'],
                    'rm_hs_code' => $partdetail['rm_hs_code'],
                    'rm_jenisperolehan' => $partdetail['rm_jenisperolehan'],
                    'nm_negara' => $partdetail['nm_negara'],
                    'negaragroup_kd' => $partdetail['negaragroup_kd'],
                    'negaragroup_nama' => $partdetail['negaragroup_nama'],
                    'partdetail_nama' => $partdetail['partdetail_nama'],
                    'partdetail_deskripsi' => $partdetail['partdetail_deskripsi'],
                    'partdetail_spesifikasi' => $partdetail['partdetail_spesifikasi'],
                    'partdetail_hargatotal' => $partdetail_hargatotal,
                ];
            }
                #group by kat_barang, rm_kd
            $arrayResultGroupdetailplates = [];
            foreach ($arrayResultdetailplates as $arrayResultdetailplate) {
                $arrayResultGroupdetailplates[$arrayResultdetailplate['kd_kat_barang']]
                    [$arrayResultdetailplate['rm_kd'].'|'.
                    $arrayResultdetailplate['rm_kode'].'|'.
                    $arrayResultdetailplate['rm_hs_code'].'|'.
                    $arrayResultdetailplate['rm_jenisperolehan'].'|'.
                    $arrayResultdetailplate['nm_negara'].'|'.
                    $arrayResultdetailplate['negaragroup_kd'].'|'.
                    $arrayResultdetailplate['partdetail_nama'].'|'.
                    $arrayResultdetailplate['partdetail_deskripsi'].'|'.
                    $arrayResultdetailplate['partdetail_spesifikasi']
                    ][] = $arrayResultdetailplate;
            }
                #sum harga totoal per kat barang per rm_kd
            $arrayGroupSumhrgtotaldetails = [];
            foreach ($arrayResultGroupdetailplates as $kd_kat_barangs => $elementRm_kds) {
                foreach ($elementRm_kds as $rm_kds => $elementDetails){
                    $sumHrgtotal = 0;
                    foreach ($elementDetails as $elementDetail) {
                        $sumHrgtotal += $elementDetail['partdetail_hargatotal'];
                    }
                    $expValRm_kds = explode('|',$rm_kds);
                    $arrayGroupSumhrgtotaldetails[] = [
                        'kd_kat_barang' => $kd_kat_barangs,
                        'rm_kd' => $expValRm_kds[0],
                        'rm_kode' => $expValRm_kds[1],
                        'rm_hs_code' => $expValRm_kds[2],
                        'rm_jenisperolehan' => $expValRm_kds[3],
                        'nm_negara' => $expValRm_kds[4],
                        'negaragroup_kd' => $expValRm_kds[5],
                        'partdetail_nama' => $expValRm_kds[6],
                        'partdetail_deskripsi' => $expValRm_kds[7],
                        'partdetail_spesifikasi' => $expValRm_kds[8],
                        'sum_partdetail_total' => $sumHrgtotal,
                    ]; 
                }
            }
            $data['partdetails'] = $arrayGroupSumhrgtotaldetails;            
            
            # ----- partlabourcosts -----
            $data['partlabourcosts'] = $this->db->select('a.*, b.*, d.kd_kat_barang ,e.bagian_nama, SUM(b.partlabourcost_hargatotal) as sum_partlabourcost_total')
                    ->from('td_do_item_reportunit as a')
                    ->join('td_part_labourcost as b', 'b.part_kd=a.part_kd', 'left')
                    ->join('tm_barang as c', 'c.kd_barang=a.kd_barang', 'left')
                    ->join('tm_kat_barang as d', 'd.kd_kat_barang=c.kat_barang_kd', 'left')
                    ->join('tb_bagian as e', 'e.bagian_kd=b.bagian_kd', 'left')
                    ->where_in('a.kd_mdo', array_column($DOinv, 'kd_mdo'));
            if (!empty($kat_barang_kd)){
                $data['partlabourcosts'] = $data['partlabourcosts']->where('d.kd_kat_barang', $kat_barang_kd);
            }
            $data['partlabourcosts'] = $data['partlabourcosts']->group_by('d.kd_kat_barang, b.bagian_kd')
                    ->get()->result_array();

            # ----- partoverheads -----
            $data['partoverheads'] = $this->db->select('a.*, b.*, d.kd_kat_barang, e.overhead_nama, SUM(b.partoverhead_hargatotal) as sum_partoverhead_total')
                    ->from('td_do_item_reportunit as a')
                    ->join('td_part_overhead as b', 'b.part_kd=a.part_kd', 'left')
                    ->join('tm_barang as c', 'c.kd_barang=a.kd_barang', 'left')
                    ->join('tm_kat_barang as d', 'd.kd_kat_barang=c.kat_barang_kd', 'left')
                    ->join('tb_part_overhead as e', 'e.overhead_kd=b.overhead_kd', 'left')
                    ->where_in('a.kd_mdo', array_column($DOinv, 'kd_mdo'));
            if (!empty($kat_barang_kd)){
                $data['partoverheads'] = $data['partoverheads']->where('d.kd_kat_barang', $kat_barang_kd);
            }
            $data['partoverheads'] = $data['partoverheads']->group_by('d.kd_kat_barang, b.overhead_kd')
                    ->get()->result_array();
            
            # ----- invoice distributors -----
            $qInvoiceDistributors = 'SELECT a.kd_ditem_do, d.kat_barang_kd, b.barang_kd, b.item_status, b.item_code, b.item_desc, b.item_dimension, b.harga_barang, b.total_harga, c.jml_disc, a.stuffing_item_qty, "parent" AS sts
                FROM td_do_item AS a
                LEFT JOIN td_salesorder_item AS b ON b.kd_ditem_so=a.parent_kd
                LEFT JOIN td_salesorder_item_disc AS c ON c.so_item_kd=b.kd_ditem_so AND c.item_relation = "parent" AND c.nm_pihak = "Distributor"
                LEFT JOIN tm_barang AS d ON d.kd_barang=b.barang_kd

                WHERE a.mdo_kd IN ('.$implodeKdMdo.')';
            if (!empty($kat_barang_kd)){
                $qInvoiceDistributors .= 'AND d.kat_barang_kd = "'.$kat_barang_kd.'"';
            }
            $qInvoiceDistributors .= 'AND a.parent_kd IS NOT NULL AND a.child_kd IS NULL

                UNION ALL
                
                SELECT a.kd_ditem_do, d.kat_barang_kd, b.kd_child AS barang_kd, b.item_status, b.item_code, b.item_desc, b.item_dimension, b.harga_barang, b.total_harga, c.jml_disc, a.stuffing_item_qty, "child" AS sts
                FROM td_do_item AS a
                LEFT JOIN td_salesorder_item_detail AS b ON b.kd_citem_so=a.child_kd
                LEFT JOIN td_salesorder_item_disc AS c ON c.so_item_kd=b.kd_citem_so AND c.item_relation = "child" AND c.nm_pihak = "Distributor"
                LEFT JOIN tm_barang AS d ON d.kd_barang=b.kd_child

                WHERE a.mdo_kd IN ('.$implodeKdMdo.')';
            if (!empty($kat_barang_kd)){
                $qInvoiceDistributors .= 'AND d.kat_barang_kd = "'.$kat_barang_kd.'"';
            }
            $qInvoiceDistributors .= 'AND a.child_kd IS NOT NULL';

            $invoiceDistributors = $this->db->query($qInvoiceDistributors)->result_array();

            $arrayInvoiceDistributors = []; $sumInvoiceDsitributors = 0;
            foreach ($invoiceDistributors as $invoiceDistributor) {
                $arrayInvoiceDistributors[$invoiceDistributor['kat_barang_kd']][] = $invoiceDistributor;
            }
            $arrayResultInvoiceDistributors = [];
            foreach($arrayInvoiceDistributors as $kat_barang_kds => $elements) {
                $sum_harga_item = 0;
                $sum_totalharga_item = 0;
                $totalUnitDO = 0;
                foreach ($elements as $element) {
                    #harga per item
                    $actualPercents = (100 - $element['jml_disc']) /100;
                    $totalPeritems = $actualPercents * $element['harga_barang'];
                    $totalPeritems = round($totalPeritems, 2);
                    $sum_harga_item += $totalPeritems;

                    #harga total
                    $totalhargaPeritems = $actualPercents * $element['harga_barang'] * $element['stuffing_item_qty'];
                    $totalhargaPeritems = round($totalhargaPeritems, 2);
                    $sum_totalharga_item += $totalhargaPeritems;

                    # total unit DO
                    $totalUnitDO += $element['stuffing_item_qty'];
                }
                $sum_harga_item = round($sum_harga_item, 2);
                $sum_totalharga_item = round($sum_totalharga_item, 2);
                $arrayResultInvoiceDistributors[$kat_barang_kds] = [
                    'sum_harga_item' => $sum_harga_item,
                    'sum_totalharga_item' => $sum_totalharga_item,
                    'sum_totalunit_item' => $totalUnitDO,
                ];
            }
            $data['invoiceDistributors'] = $arrayResultInvoiceDistributors;

            # ----- biaya angkut -----
                # hitung sum invoice distributor total harga setelah diskon
            $qTotalInvoiceDistributors = 'SELECT a.kd_ditem_do, d.kat_barang_kd, b.barang_kd, b.item_status, b.item_code, b.item_desc, b.item_dimension, b.harga_barang, b.total_harga, c.jml_disc, a.stuffing_item_qty, "parent" AS sts
                FROM td_do_item AS a
                LEFT JOIN td_salesorder_item AS b ON b.kd_ditem_so=a.parent_kd
                LEFT JOIN td_salesorder_item_disc AS c ON c.so_item_kd=b.kd_ditem_so AND c.item_relation = "parent" AND c.nm_pihak = "Distributor"
                LEFT JOIN tm_barang AS d ON d.kd_barang=b.barang_kd

                WHERE a.mdo_kd IN ('.$implodeKdMdo.') AND a.parent_kd IS NOT NULL AND a.child_kd IS NULL

                UNION ALL
                
                SELECT a.kd_ditem_do, d.kat_barang_kd, b.kd_child AS barang_kd, b.item_status, b.item_code, b.item_desc, b.item_dimension, b.harga_barang, b.total_harga, c.jml_disc, a.stuffing_item_qty, "child" AS sts
                FROM td_do_item AS a
                LEFT JOIN td_salesorder_item_detail AS b ON b.kd_citem_so=a.child_kd
                LEFT JOIN td_salesorder_item_disc AS c ON c.so_item_kd=b.kd_citem_so AND c.item_relation = "child" AND c.nm_pihak = "Distributor"
                LEFT JOIN tm_barang AS d ON d.kd_barang=b.kd_child

                WHERE a.mdo_kd IN ('.$implodeKdMdo.') AND a.child_kd IS NOT NULL';

            $totalInvoiceDistributors = $this->db->query($qTotalInvoiceDistributors)->result_array();

            $arraySumTotalhargaDistributor = [];
            foreach ($totalInvoiceDistributors as $totalInvoiceDistributor) {
                $total_hargaDistributor = (100 - $totalInvoiceDistributor['jml_disc']) / 100 * $totalInvoiceDistributor['harga_barang'] * $totalInvoiceDistributor['stuffing_item_qty'];
                $total_hargaDistributor = round($total_hargaDistributor, 2);
                $arraySumTotalhargaDistributor[] = $total_hargaDistributor;
            }

            $currency_biayaAngkut = $data['do']['biaya_angkut'] / $data['do']['currency_rp'];
            $sum_totalharga_invoice = array_sum($arraySumTotalhargaDistributor);
            $sum_totalharga_invoice = round($sum_totalharga_invoice, 2);
            $arrayBiayaAngkuts = [];
            foreach ($arrayResultInvoiceDistributors as $kd_kat_barangs => $elBiayas) {
                $biayaAngkutKat = 0;
                $biayaAngkutKat = $elBiayas['sum_totalharga_item'] / $sum_totalharga_invoice * $currency_biayaAngkut / $elBiayas['sum_totalunit_item'];
                $biayaAngkutKat = round ($biayaAngkutKat, 2);
                $arrayBiayaAngkuts[$kd_kat_barangs] = $biayaAngkutKat;
            }
            $data['biayaAngkuts'] = $arrayBiayaAngkuts;

        }
        return $data;
    }

    /** Group by hs code */
    public function get_data_report_group_hscode($kd_mdo = null, $hscode=null)
    {
        $this->load->model(['tm_deliveryorder']);
        $data = [];
        if (!empty($kd_mdo)){
            /** Check DO banyak SO */
            $DOkode = $this->tm_deliveryorder->get_by_param(['kd_mdo' => $kd_mdo])->row_array();
            $DOinv = $this->tm_deliveryorder->get_by_param(['no_invoice' => $DOkode['no_invoice']])->result_array();
            /** array untuk where in di raw query */
            $implodeKdMdo = "'".implode("','", array_column($DOinv, 'kd_mdo'))."'"; 

            # ----- do -----
            $data['do'] = $this->db->select()
                    ->from('tm_deliveryorder as a')
                    ->where('kd_mdo', $kd_mdo)
                    ->get()->row_array();

            # ----- header -----
            $data['headers'] = $this->db->select('b.hs_code, GROUP_CONCAT(DISTINCT c.nm_kat_barang SEPARATOR ",") AS nm_kat_barang')
                    ->from('td_do_item_reportunit as a')
                    ->join('tm_barang as b', 'a.kd_barang=b.kd_barang', 'left')
                    ->join('tm_kat_barang as c', 'c.kd_kat_barang=b.kat_barang_kd', 'left')
                    ->where_in('a.kd_mdo', array_column($DOinv, 'kd_mdo'));
            if (!empty($hscode)){
                $data['headers'] = $data['headers']->where('b.hs_code', $hscode);
            }
            $data['headers'] = $data['headers']->group_by('b.hs_code')
                    ->order_by('b.hs_code', 'asc')
                    ->get()->result_array();
            
            # ----- partdetails (sekalian generate sum plate main) -----
            $partdetails = $this->db->select('f.hs_code, b.*, c.rm_kode, c.rm_hs_code, c.rm_jenisperolehan, c.negara_id, c.rm_kode, d.nm_negara, e.negaragroup_kd, e.negaragroup_nama, 
                f.kd_barang, f.item_code, g.kd_kat_barang, i.partmain_nama, b.partdetail_hargatotal')
                    ->from('td_do_item_reportunit as a')
                    ->join('td_part_detail as b', 'b.part_kd=a.part_kd', 'left')
                    ->join('tm_rawmaterial as c', 'c.rm_kd=b.rm_kd', 'left')
                    ->join('tb_negara as d', 'd.id=c.negara_id', 'left')
                    ->join('tb_negara_group as e', 'e.negaragroup_kd=d.negaragroup_kd', 'left')
                    ->join('tm_barang as f', 'f.kd_barang=a.kd_barang', 'left')
                    ->join('tm_kat_barang as g', 'g.kd_kat_barang=f.kat_barang_kd', 'left')
                    ->join('td_part as h', 'h.part_kd=b.part_kd', 'left')
                    ->join('tm_part_main as i', 'i.partmain_kd=h.partmain_kd', 'left')
                    ->where_in('a.kd_mdo', array_column($DOinv, 'kd_mdo'));
            if (!empty($hscode)){
                $partdetails = $partdetails->where('f.hs_code', $hscode);
            }
            $partdetails = $partdetails->get()->result_array();
            
                # partdetailplate
            $part_kds =  $this->db->distinct()->select('a.part_kd')
                    ->from('td_do_item_reportunit as a')
                    ->from('tm_barang as b', 'b.kd_barang=a.kd_barang', 'left')
                    ->where_in('a.kd_mdo', array_column($DOinv, 'kd_mdo'))
                    ->where('a.part_kd IS NOT NULL');
                if (!empty($hscode)){
                    $part_kds = $part_kds->where('b.hs_code', $hscode);
                }
            $part_kds = $part_kds->get()->result_array();
            $arrayPart_kds = array_column($part_kds, 'part_kd');
            
            $partdetailplates = $this->db->select()
                ->from('td_part_detailplate as a')
                ->where_in('a.part_kd', $arrayPart_kds)
                ->where('a.partdetailplate_jenis', 'main')
                ->get()->result_array();
                # Jumlahkan plate main harga total
            $arrayPlatedetailplates = []; $sumMain = []; $arraysumPlatedetailplates = [];
            foreach ($partdetailplates as $partdetailplate) {
                $arrayPlatedetailplates[$partdetailplate['partdetail_kd']][] =  $partdetailplate['partdetailplate_hargatotal'];
            }
            foreach ($arrayPlatedetailplates as $arrayPlatedetailplate => $hrgtotal) {
                $arraysumPlatedetailplates[$arrayPlatedetailplate] = array_sum($hrgtotal);
            }
                # proses include dengan part detail
            $arrayResultdetailplates = [];
            foreach($partdetails as $partdetail){
                $partdetail_hargatotal = $partdetail['partdetail_hargatotal'];
                if (isset($arraysumPlatedetailplates[$partdetail['partdetail_kd']])){
                    $partdetail_hargatotal = $arraysumPlatedetailplates[$partdetail['partdetail_kd']];
                }
                
                $arrayResultdetailplates[] = [
                    'hs_code' => $partdetail['hs_code'],
                    'partdetail_kd' => $partdetail['partdetail_kd'],
                    'part_kd' => $partdetail['part_kd'],
                    'kd_kat_barang' => $partdetail['kd_kat_barang'],
                    'kd_barang' => $partdetail['kd_barang'],
                    'rm_kd' => $partdetail['rm_kd'],
                    'rm_kode' => $partdetail['rm_kode'],
                    'rm_hs_code' => $partdetail['rm_hs_code'],
                    'rm_jenisperolehan' => $partdetail['rm_jenisperolehan'],
                    'nm_negara' => $partdetail['nm_negara'],
                    'negaragroup_kd' => $partdetail['negaragroup_kd'],
                    'negaragroup_nama' => $partdetail['negaragroup_nama'],
                    'partdetail_nama' => $partdetail['partdetail_nama'],
                    'partdetail_deskripsi' => $partdetail['partdetail_deskripsi'],
                    'partdetail_spesifikasi' => $partdetail['partdetail_spesifikasi'],
                    'partdetail_hargatotal' => $partdetail_hargatotal,
                ];
            }
                #group by hs code, rm_kd
            $arrayResultGroupdetailplates = [];
            foreach ($arrayResultdetailplates as $arrayResultdetailplate) {
                $arrayResultGroupdetailplates[$arrayResultdetailplate['hs_code']]
                    [$arrayResultdetailplate['rm_kd'].'|'.
                    $arrayResultdetailplate['rm_kode'].'|'.
                    $arrayResultdetailplate['rm_hs_code'].'|'.
                    $arrayResultdetailplate['rm_jenisperolehan'].'|'.
                    $arrayResultdetailplate['nm_negara'].'|'.
                    $arrayResultdetailplate['negaragroup_kd'].'|'.
                    $arrayResultdetailplate['partdetail_nama'].'|'.
                    $arrayResultdetailplate['partdetail_deskripsi'].'|'.
                    $arrayResultdetailplate['partdetail_spesifikasi']
                    ][] = $arrayResultdetailplate;
            }
                #sum harga totoal per kat barang per rm_kd
            $arrayGroupSumhrgtotaldetails = [];
            foreach ($arrayResultGroupdetailplates as $hs_codes => $elementRm_kds) {
                foreach ($elementRm_kds as $rm_kds => $elementDetails){
                    $sumHrgtotal = 0;
                    foreach ($elementDetails as $elementDetail) {
                        $sumHrgtotal += $elementDetail['partdetail_hargatotal'];
                    }
                    $expValRm_kds = explode('|',$rm_kds);
                    $arrayGroupSumhrgtotaldetails[] = [
                        'hs_code' => $hs_codes,
                        'rm_kd' => $expValRm_kds[0],
                        'rm_kode' => $expValRm_kds[1],
                        'rm_hs_code' => $expValRm_kds[2],
                        'rm_jenisperolehan' => $expValRm_kds[3],
                        'nm_negara' => $expValRm_kds[4],
                        'negaragroup_kd' => $expValRm_kds[5],
                        'partdetail_nama' => $expValRm_kds[6],
                        'partdetail_deskripsi' => $expValRm_kds[7],
                        'partdetail_spesifikasi' => $expValRm_kds[8],
                        'sum_partdetail_total' => $sumHrgtotal,
                    ]; 
                }
            }
            $data['partdetails'] = $arrayGroupSumhrgtotaldetails;            
            
            # ----- partlabourcosts -----
            $data['partlabourcosts'] = $this->db->select('a.*, b.*, d.kd_kat_barang, c.hs_code ,e.bagian_nama, SUM(b.partlabourcost_hargatotal) as sum_partlabourcost_total')
                    ->from('td_do_item_reportunit as a')
                    ->join('td_part_labourcost as b', 'b.part_kd=a.part_kd', 'left')
                    ->join('tm_barang as c', 'c.kd_barang=a.kd_barang', 'left')
                    ->join('tm_kat_barang as d', 'd.kd_kat_barang=c.kat_barang_kd', 'left')
                    ->join('tb_bagian as e', 'e.bagian_kd=b.bagian_kd', 'left')
                    ->where_in('a.kd_mdo', array_column($DOinv, 'kd_mdo'));
            if (!empty($hscode)){
                $data['partlabourcosts'] = $data['partlabourcosts']->where('c.hs_code', $hscode);
            }
            $data['partlabourcosts'] = $data['partlabourcosts']->group_by('c.hs_code, b.bagian_kd')
                    ->get()->result_array();

            # ----- partoverheads -----
            $data['partoverheads'] = $this->db->select('a.*, b.*, d.kd_kat_barang, c.hs_code, e.overhead_nama, SUM(b.partoverhead_hargatotal) as sum_partoverhead_total')
                    ->from('td_do_item_reportunit as a')
                    ->join('td_part_overhead as b', 'b.part_kd=a.part_kd', 'left')
                    ->join('tm_barang as c', 'c.kd_barang=a.kd_barang', 'left')
                    ->join('tm_kat_barang as d', 'd.kd_kat_barang=c.kat_barang_kd', 'left')
                    ->join('tb_part_overhead as e', 'e.overhead_kd=b.overhead_kd', 'left')
                    ->where_in('a.kd_mdo', array_column($DOinv, 'kd_mdo'));
            if (!empty($hscode)){
                $data['partoverheads'] = $data['partoverheads']->where('c.hs_code', $hscode);
            }
            $data['partoverheads'] = $data['partoverheads']->group_by('c.hs_code, b.overhead_kd')
                    ->get()->result_array();
            
            # ----- invoice distributors -----
            $qInvoiceDistributors = 'SELECT a.kd_ditem_do, d.kat_barang_kd, d.hs_code, b.barang_kd, b.item_status, b.item_code, b.item_desc, b.item_dimension, b.harga_barang, b.total_harga, c.jml_disc, a.stuffing_item_qty, "parent" AS sts
                FROM td_do_item AS a
                LEFT JOIN td_salesorder_item AS b ON b.kd_ditem_so=a.parent_kd
                LEFT JOIN td_salesorder_item_disc AS c ON c.so_item_kd=b.kd_ditem_so AND c.item_relation = "parent" AND c.nm_pihak = "Distributor"
                LEFT JOIN tm_barang AS d ON d.kd_barang=b.barang_kd

                WHERE a.mdo_kd IN ('.$implodeKdMdo.')';
            if (!empty($hscode)){
                $qInvoiceDistributors .= 'AND d.hs_code = "'.$hscode.'"';
            }
            $qInvoiceDistributors .= 'AND a.parent_kd IS NOT NULL AND a.child_kd IS NULL

                UNION ALL
                
                SELECT a.kd_ditem_do, d.kat_barang_kd, d.hs_code, b.kd_child AS barang_kd, b.item_status, b.item_code, b.item_desc, b.item_dimension, b.harga_barang, b.total_harga, c.jml_disc, a.stuffing_item_qty, "child" AS sts
                FROM td_do_item AS a
                LEFT JOIN td_salesorder_item_detail AS b ON b.kd_citem_so=a.child_kd
                LEFT JOIN td_salesorder_item_disc AS c ON c.so_item_kd=b.kd_citem_so AND c.item_relation = "child" AND c.nm_pihak = "Distributor"
                LEFT JOIN tm_barang AS d ON d.kd_barang=b.kd_child

                WHERE a.mdo_kd IN ('.$implodeKdMdo.')';
            if (!empty($hscode)){
                $qInvoiceDistributors .= 'AND d.hs_code = "'.$hscode.'"';
            }
            $qInvoiceDistributors .= 'AND a.child_kd IS NOT NULL';

            $invoiceDistributors = $this->db->query($qInvoiceDistributors)->result_array();

            $arrayInvoiceDistributors = []; $sumInvoiceDsitributors = 0;
            foreach ($invoiceDistributors as $invoiceDistributor) {
                $arrayInvoiceDistributors[$invoiceDistributor['hs_code']][] = $invoiceDistributor;
            }
            $arrayResultInvoiceDistributors = [];
            foreach($arrayInvoiceDistributors as $hs_codes => $elements) {
                $sum_harga_item = 0;
                $sum_totalharga_item = 0;
                $totalUnitDO = 0;
                foreach ($elements as $element) {
                    #harga per item
                    $actualPercents = (100 - $element['jml_disc']) /100;
                    $totalPeritems = $actualPercents * $element['harga_barang'];
                    $totalPeritems = round($totalPeritems, 2);
                    $sum_harga_item += $totalPeritems;

                    #harga total
                    $totalhargaPeritems = $actualPercents * $element['harga_barang'] * $element['stuffing_item_qty'];
                    $totalhargaPeritems = round($totalhargaPeritems, 2);
                    $sum_totalharga_item += $totalhargaPeritems;

                    # total unit DO
                    $totalUnitDO += $element['stuffing_item_qty'];
                }
                $sum_harga_item = round($sum_harga_item, 2);
                $sum_totalharga_item = round($sum_totalharga_item, 2);
                $arrayResultInvoiceDistributors[$hs_codes] = [
                    'sum_harga_item' => $sum_harga_item,
                    'sum_totalharga_item' => $sum_totalharga_item,
                    'sum_totalunit_item' => $totalUnitDO,
                ];
            }
            $data['invoiceDistributors'] = $arrayResultInvoiceDistributors;

            # ----- biaya angkut -----
                # hitung sum invoice distributor total harga setelah diskon
            $qTotalInvoiceDistributors = 'SELECT a.kd_ditem_do, d.kat_barang_kd, b.barang_kd, b.item_status, b.item_code, b.item_desc, b.item_dimension, b.harga_barang, b.total_harga, c.jml_disc, a.stuffing_item_qty, "parent" AS sts
                FROM td_do_item AS a
                LEFT JOIN td_salesorder_item AS b ON b.kd_ditem_so=a.parent_kd
                LEFT JOIN td_salesorder_item_disc AS c ON c.so_item_kd=b.kd_ditem_so AND c.item_relation = "parent" AND c.nm_pihak = "Distributor"
                LEFT JOIN tm_barang AS d ON d.kd_barang=b.barang_kd

                WHERE a.mdo_kd IN ('.$implodeKdMdo.') AND a.parent_kd IS NOT NULL AND a.child_kd IS NULL

                UNION ALL
                
                SELECT a.kd_ditem_do, d.kat_barang_kd, b.kd_child AS barang_kd, b.item_status, b.item_code, b.item_desc, b.item_dimension, b.harga_barang, b.total_harga, c.jml_disc, a.stuffing_item_qty, "child" AS sts
                FROM td_do_item AS a
                LEFT JOIN td_salesorder_item_detail AS b ON b.kd_citem_so=a.child_kd
                LEFT JOIN td_salesorder_item_disc AS c ON c.so_item_kd=b.kd_citem_so AND c.item_relation = "child" AND c.nm_pihak = "Distributor"
                LEFT JOIN tm_barang AS d ON d.kd_barang=b.kd_child

                WHERE a.mdo_kd IN ('.$implodeKdMdo.') AND a.child_kd IS NOT NULL';

            $totalInvoiceDistributors = $this->db->query($qTotalInvoiceDistributors)->result_array();

            $arraySumTotalhargaDistributor = [];
            foreach ($totalInvoiceDistributors as $totalInvoiceDistributor) {
                $total_hargaDistributor = (100 - $totalInvoiceDistributor['jml_disc']) / 100 * $totalInvoiceDistributor['harga_barang'] * $totalInvoiceDistributor['stuffing_item_qty'];
                $total_hargaDistributor = round($total_hargaDistributor, 2);
                $arraySumTotalhargaDistributor[] = $total_hargaDistributor;
            }

            $currency_biayaAngkut = $data['do']['biaya_angkut'] / $data['do']['currency_rp'];
            $sum_totalharga_invoice = array_sum($arraySumTotalhargaDistributor);
            $sum_totalharga_invoice = round($sum_totalharga_invoice, 2);
            $arrayBiayaAngkuts = [];
            foreach ($arrayResultInvoiceDistributors as $hs_codes => $elBiayas) {
                $biayaAngkutKat = 0;
                $biayaAngkutKat = $elBiayas['sum_totalharga_item'] / $sum_totalharga_invoice * $currency_biayaAngkut / $elBiayas['sum_totalunit_item'];
                $biayaAngkutKat = round ($biayaAngkutKat, 2);
                $arrayBiayaAngkuts[$hs_codes] = $biayaAngkutKat;
            }
            $data['biayaAngkuts'] = $arrayBiayaAngkuts;

        }
        return $data;
    }

	
}