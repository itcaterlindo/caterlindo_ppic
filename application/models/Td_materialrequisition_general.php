<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_materialrequisition_general extends CI_Model {
	private $tbl_name = 'td_materialrequisition_general';
	private $p_key = 'materialreqgeneral_kd';

	public function ssp_table($date) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'a.materialreqgeneral_tanggal', 
				'dt' => 2, 'field' => 'materialreqgeneral_tanggal',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'e.bagian_nama', 
				'dt' => 3, 'field' => 'bagian_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'c.rm_kode', 
				'dt' => 4, 'field' => 'rm_kode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'c.rm_kd', // DEKSRIPSI
				'dt' => 5, 'field' => 'rm_kd',
				'formatter' => function ($d, $row){
					$desc = "{$row[9]} {$row[10]}";
					if ($d == 'SPECIAL') {
						$desc = "{$row[11]} {$row[12]}";
					}

					return $desc;
                }),
			array( 'db' => 'a.materialreqgeneral_qty',
				'dt' => 6, 'field' => 'materialreqgeneral_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'b.rmsatuan_nama',
				'dt' => 7, 'field' => 'rmsatuan_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.materialreqgeneral_remark',
				'dt' => 8, 'field' => 'materialreqgeneral_remark',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.materialreqgeneral_tgledit',
				'dt' => 9, 'field' => 'materialreqgeneral_tgledit',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'c.rm_deskripsi',
				'dt' => 10, 'field' => 'rm_deskripsi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'c.rm_spesifikasi',
				'dt' => 11, 'field' => 'rm_spesifikasi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'd.podetail_deskripsi',
				'dt' => 12, 'field' => 'podetail_deskripsi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'd.podetail_spesifikasi',
				'dt' => 13, 'field' => 'podetail_spesifikasi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM {$this->tbl_name} as a 
							LEFT JOIN td_rawmaterial_satuan as b ON a.materialreqgeneral_satuan_kd=b.rmsatuan_kd
							LEFT JOIN tm_rawmaterial as c ON c.rm_kd=a.rm_kd
							LEFT JOIN td_purchaseorder_detail as d ON d.podetail_kd=a.podetail_kd
							LEFT JOIN tb_bagian as e on e.bagian_kd=a.bagian_kd";
		if($this->session->tipe_admin == 'Admin'){
       		 $data['where'] = "a.materialreqgeneral_tanggal = '$date'";
		}else{
			$param = $this->session->kd_admin;
			$data['where'] = "a.materialreqgeneral_tanggal = '$date' AND a.admin_kd = '$param'";
		}
		
		return $data;
	}

	private function tbl_btn($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btns = array();
		$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));	
		$btn_group = group_btns($btns);

		return $btn_group;
	}

    public function create_code () {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_where_in ($where, $in = []) {
		$query = $this->db->where_in($where, $in)
				->select($this->tbl_name.'.*')
				->get($this->tbl_name);
		return $query;
	}

	public function update_batch ($idColumn, $data = []) {
		$act = $this->db->update_batch($this->tbl_name, $data, $idColumn);
		return $act;
	}
	
}