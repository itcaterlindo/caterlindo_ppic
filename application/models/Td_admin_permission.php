<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_admin_permission extends CI_Model {
	private $tbl_name = 'td_admin_permission';
	private $p_key = 'adminpermission_kd';

    public function ssp_table () {
        $data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'b.nm_admin',
				'dt' => 2, 'field' => 'nm_admin',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.permission_nama', 
				'dt' => 3, 'field' => 'permission_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'd.nm_menu', 
				'dt' => 4, 'field' => 'nm_menu',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.adminpermission_tgledit', 
				'dt' => 5, 'field' => 'adminpermission_tgledit',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),    
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] = "FROM ".$this->tbl_name." as a
                            LEFT JOIN tb_admin as b ON a.kd_admin=b.kd_admin
                            LEFT JOIN tb_permission as c ON a.permission_kd=c.permission_kd
                            LEFT JOIN tb_menu as d ON c.menu_kd=d.id";
		$data['where'] = "";
		
		return $data;
    }

    private function tbl_btn($id) {
		$btns = array();
		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_item(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'delete_item(\''.$id.'\')'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}

    
	public function create_code() {
        $query = $this->db->select('MAX('.$this->p_key.') as maxID')
                ->get($this->tbl_name)
                ->row();
        $code = (int) $query->maxID + 1;
        return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }

    public function get_all () {
        return $this->db->get($this->tbl_name);
    }

    public function cek_adminpermission ($kd_admin, $permission_nama) {
        $adminpermission = $this->db->select()
                            ->from($this->tbl_name)
                            ->join('tb_permission', $this->tbl_name.'.permission_kd=tb_permission.permission_kd', 'left')
                            ->where([$this->tbl_name.'.kd_admin' => $kd_admin, 'tb_permission.permission_nama' => $permission_nama])
                            ->get();
        if(!empty($adminpermission->num_rows())){
            return true;
        }else{
            return false;
        }
    }

    public function get_by_param_detail ($param = []) {
		$result = $this->db->select()
                    ->from($this->tbl_name)
                    ->join('tb_admin', $this->tbl_name.'.kd_admin=tb_admin.kd_admin', 'left')
                    ->join('tb_permission', $this->tbl_name.'.permission_kd=tb_permission.permission_kd', 'left')
                    ->where($param)
                    ->get();
		return $result;
	}

	public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}
	
	public function insert_batch_data($data) {
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
    }

}