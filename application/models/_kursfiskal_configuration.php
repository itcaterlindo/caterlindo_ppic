<?php
defined('BASEPATH') or exit('No direct script access allowed!');
define('URL', 'https://fiskal.kemenkeu.go.id/informasi-publik/kurs-pajak');

class _kursfiskal_configuration extends CI_Model {

	public function __construct() {
		parent::__construct();
        $this->load->library(['simple_html_dom']);
		$this->load->helper(['my_helper']);

    }

    function getby_kdcurrency ($kd_currency, $tgl_currency) {
		$this->load->model(['tm_currency']);

        $currency = $this->tm_currency->get_data($kd_currency);

		$arrContextOptions=array(
			"ssl"=>array(
				"verify_peer"=>false,
				"verify_peer_name"=>false,
			),
		);  
		$url = URL."?date=".format_date($tgl_currency, 'Y-m-d');
		$html = file_get_html($url, false, stream_context_create($arrContextOptions));
		$tb = $html->find('table', 0);
		$tbo = $tb->find('tbody', 0);

		$tbtr = 'not_null';
		$i = 0;
		while (!empty($tbtr)){
			$tbtr = $tbo->find('tr', $i);
			$aHtmlTr[] = $tbtr;
			$i++;
		}

		$z = 0;
		while ($z < (count($aHtmlTr)-1)){
			$nilai = str_replace('.', '', trim($aHtmlTr[$z]->find('td', 2)->plaintext));
			$nilai = str_replace(',', '.', $nilai);

			$aTd[] = [
				'tanggal' => $tgl_currency,
				'no' => (int)trim($aHtmlTr[$z]->find('td', 0)->plaintext),
				'matauang' => trim($aHtmlTr[$z]->find('td', 1)->plaintext),
				'nilai' => (float)$nilai,
				'perubahan' => trim($aHtmlTr[$z]->find('td', 3)->plaintext),
			];
			$z++;
		}
		$result = [];
		foreach($aTd as $eTd){
			if($eTd['no'] == $currency->fiskal_no) {
				$result = $eTd;
			}
		}
		return $result;
    }

   

	
}