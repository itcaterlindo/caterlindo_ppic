<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_inspection_rawmaterial extends CI_Model {
	private $tbl_name = 'tm_inspection_rawmaterial';
	private $p_key = 'kd_inspectionrawmaterial';
	private $title_name = 'Data inspection rawmaterial';

	/* --start ssp tabel untuk modul data inspection rawmaterial-- */
	
	public function ssp_table2()
	{
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		$state_kd = $this->input->get('state_kd');
		$whereState = "";
		if($state_kd !== 'ALL'){
			$whereState = "WHERE ".$this->tbl_name.".inspectionrawmaterialstate_kd = '".$state_kd."'";
		}else{
			$whereState = "";
		}

		$viewPermission = cek_permission('INSPECTIONRAWMATERIAL_VIEW');
		if($viewPermission){
			$qData = $this->db->query("SELECT * FROM ".$this->tbl_name."
				LEFT JOIN tb_bagian ON tb_bagian.bagian_kd = ".$this->tbl_name.".inspectionrawmaterial_bagian_kd
				LEFT JOIN (SELECT bagian_kd AS bagian_kd2, bagian_nama AS bagian_nama2 FROM tb_bagian) AS tb_bagian2 ON tb_bagian2.bagian_kd2 = ".$this->tbl_name.".inspectionrawmaterial_penyebab_bagian_kd
				LEFT JOIN tb_inspectionrawmaterial_state ON tb_inspectionrawmaterial_state.inspectionrawmaterialstate_kd = ".$this->tbl_name.".inspectionrawmaterialstate_kd
				LEFT JOIN td_delivery_material ON td_delivery_material.deliverymaterial_kd = tm_inspection_rawmaterial.deliverymaterial_kd
				LEFT JOIN tm_rawmaterial ON tm_rawmaterial.rm_kd = td_delivery_material.rm_kd
				".$whereState."
				ORDER BY ".$this->tbl_name.".inspectionrawmaterial_tgledit DESC")
				->result_array();
		}else{
			$qData = [];
		}
		
		$data = [];
		foreach ($qData as $r) {
			$data[] = [
				'1' => $this->tbl_btn($r['kd_inspectionrawmaterial'], $r['inspectionrawmaterial_no'], $r['inspectionrawmaterialstate_kd']),
				'2' => $r['inspectionrawmaterial_tanggal'],
				'3' => $r['inspectionrawmaterial_no'],
				'4' => $r['deliverymaterial_no'],
				'5' => $r['inspectionrawmaterial_rm_kode'],
				'6' => $r['rm_deskripsi'],
				'7' => $r['rm_spesifikasi'],
				'8' => $r['bagian_nama'],
				'9' => $r['inspectionrawmaterial_tindakan'],
				'10' => $r['inspectionrawmaterial_qty'],
				'11' => $r['inspectionrawmaterial_qty_reject'],
				'12' => $r['bagian_nama2'],
				'13' => $r['inspectionrawmaterial_penyebab'],
				'14' => $r['inspectionrawmaterial_keterangan'],
				'15' => $r['inspectionrawmaterial_tgledit'],
				'16' => build_span($r['inspectionrawmaterialstate_span'], $r['inspectionrawmaterialstate_nama']),
			];
		}

		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

	public function ssp_bagian_table2()
	{
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		$userdataAdmin = $this->session->userdata('kd_admin');
		$whereBagian = $this->get_allowed_bagian($userdataAdmin, 'bagian');

		if( !empty($whereBagian) ){
			$qData = $this->db->select('*')
				->from($this->tbl_name)
				->join('tb_bagian', 'tb_bagian.bagian_kd = '.$this->tbl_name.'.inspectionrawmaterial_bagian_kd', 'left')
				->join('tb_inspectionrawmaterial_state', 'tb_inspectionrawmaterial_state.inspectionrawmaterialstate_kd = '.$this->tbl_name.'.inspectionrawmaterialstate_kd', 'left')
				->join('td_delivery_material', 'td_delivery_material.deliverymaterial_kd = tm_inspection_rawmaterial.deliverymaterial_kd', 'left')
				->join('tm_rawmaterial', 'tm_rawmaterial.rm_kd = td_delivery_material.rm_kd', 'left')
				->where_not_in(''.$this->tbl_name.'.inspectionrawmaterialstate_kd', ['0', '1'])
				->where_in(''.$this->tbl_name.'.inspectionrawmaterial_bagian_kd', $whereBagian)->get()->result_array();
		}else{
			$qData = [];
		}

		$data = [];
		foreach ($qData as $r) {
			$span = "";
			$btn = "";
			if($r['flag_approved_bagian'] == 1){
				$span = build_span('primary', 'Approved');
			}else{
				$span = build_span('danger', 'Belum');
			}

			if($r['flag_approved_bagian'] == 0){
				$btn ='<button type="button" onclick="approved_bagian(\''.$r['kd_inspectionrawmaterial'].'\')" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Approved</button>';
			}

			$data[] = [
				'1' => $btn,
				'2' => $r['inspectionrawmaterial_tanggal'],
				'3' => $r['inspectionrawmaterial_no'],
				'4' => $r['rm_deskripsi'],
				'5' => $r['rm_spesifikasi'],
				'6' => $r['inspectionrawmaterial_tindakan'],
				'7' => $r['inspectionrawmaterial_qty_reject'],
				'8' => $r['bagian_nama'],
				'9' => $r['inspectionrawmaterial_keterangan'],
				'10' => $r['approved_bagian_tgl'],
				'11' => $span
			];
		}

		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

	public function ssp_penyebab_table2()
	{
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		$userdataAdmin = $this->session->userdata('kd_admin');
		$whereBagian = $this->get_allowed_bagian($userdataAdmin, 'penyebab');

		if( !empty($whereBagian) ){
			$qData = $this->db->select('*')
				->from($this->tbl_name)
				->join('tb_bagian', 'tb_bagian.bagian_kd = '.$this->tbl_name.'.inspectionrawmaterial_penyebab_bagian_kd', 'left')
				->join('tb_inspectionrawmaterial_state', 'tb_inspectionrawmaterial_state.inspectionrawmaterialstate_kd = '.$this->tbl_name.'.inspectionrawmaterialstate_kd', 'left')
				->join('td_delivery_material', 'td_delivery_material.deliverymaterial_kd = tm_inspection_rawmaterial.deliverymaterial_kd', 'left')
				->join('tm_rawmaterial', 'tm_rawmaterial.rm_kd = td_delivery_material.rm_kd', 'left')
				->where_not_in(''.$this->tbl_name.'.inspectionrawmaterialstate_kd', ['0', '1'])
				->where_in(''.$this->tbl_name.'.inspectionrawmaterial_bagian_kd', $whereBagian)->get()->result_array();
		}else{
			$qData = [];
		}

		$data = [];
		foreach ($qData as $r) {
			$span = "";
			$btn = "";
			if($r['flag_approved_penyebab'] == 1){
				$span = build_span('primary', 'Approved');
			}else{
				$span = build_span('danger', 'Belum');
			}

			if($r['flag_approved_penyebab'] == 0){
				$btn ='<button type="button" onclick="approved_penyebab(\''.$r['kd_inspectionrawmaterial'].'\')" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Approved</button>';
			}

			$data[] = [
				'1' => $btn,
				'2' => $r['inspectionrawmaterial_tanggal'],
				'3' => $r['inspectionrawmaterial_no'],
				'4' => $r['rm_deskripsi'],
				'5' => $r['rm_spesifikasi'],
				'6' => $r['inspectionrawmaterial_tindakan'],
				'7' => $r['inspectionrawmaterial_qty_reject'],
				'8' => $r['bagian_nama'],
				'9' => $r['inspectionrawmaterial_penyebab'],
				'10' => $r['inspectionrawmaterial_keterangan'],
				'11' => $r['approved_penyebab_tgl'],
				'12' => $span
			];
		}

		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

	public function ssp_kirim_table2()
	{
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));

		$this->load->model('td_relasi_deliverymaterialinspectionrawmaterial');
		/** Tampilkan data yang sudah di ketahui saja */
		$viewPermission = cek_permission('INSPECTIONRAWMATERIAL_VIEW');
		if($viewPermission){
			/** TAMPILKAN INSPECTION RAWMATERIAL DENGAN STATE DIKETAHUI DAN CLOSE */
			$qData = $this->db->query("SELECT * FROM ".$this->tbl_name."
				LEFT JOIN tb_bagian ON tb_bagian.bagian_kd = ".$this->tbl_name.".inspectionrawmaterial_bagian_kd
				LEFT JOIN (SELECT bagian_kd AS bagian_kd2, bagian_nama AS bagian_nama2 FROM tb_bagian) AS tb_bagian2 ON tb_bagian2.bagian_kd2 = ".$this->tbl_name.".inspectionrawmaterial_penyebab_bagian_kd
				LEFT JOIN tb_inspectionrawmaterial_state ON tb_inspectionrawmaterial_state.inspectionrawmaterialstate_kd = ".$this->tbl_name.".inspectionrawmaterialstate_kd
				LEFT JOIN td_delivery_material ON td_delivery_material.deliverymaterial_kd = tm_inspection_rawmaterial.deliverymaterial_kd
				LEFT JOIN tm_rawmaterial ON tm_rawmaterial.rm_kd = td_delivery_material.rm_kd
				WHERE ".$this->tbl_name.".inspectionrawmaterialstate_kd IN ('2', '3', '4')
				ORDER BY ".$this->tbl_name.".inspectionrawmaterial_tgledit DESC") 
				->result_array();
		}else{
			$qData = [];
		}
		
		$data = [];
		foreach ($qData as $r) {
			$qtyBaik = $r['inspectionrawmaterial_qty'] - $r['inspectionrawmaterial_qty_reject'];
			/** Check button barang baik */
			$relasiDrmInrm = $this->td_relasi_deliverymaterialinspectionrawmaterial->get_by_param(['inspectionrawmaterial_kd' => $r['kd_inspectionrawmaterial']])->result_array();
			$checkBtnBaik = in_array('baik', array_column($relasiDrmInrm, 'keterangan'));
			$checkBtnReject = in_array('reject', array_column($relasiDrmInrm, 'keterangan'));
			$btns = [];
			/** Jika barang baik sudah ada di tabel relasi drm x inspection rawmaterial berarti sudah terkirim dan tidak perlu ditampilkan */
			if(!$checkBtnBaik && $qtyBaik > 0){
				$btns[] = get_btn(array('access' => true, 'title' => 'Kirim Qty Baik', 'icon' => 'check', 'onclick' => 'action_kirim(\''.$r['deliverymaterial_kd'].'\',\''.$r['kd_inspectionrawmaterial'].'\',\'baik\')'));
			}
			/** Jika barang reject sudah ada di tabel relasi drm x inspection rawmaterial berarti sudah terkirim dan tidak perlu ditampilkan */
			if(!$checkBtnReject && $r['inspectionrawmaterial_qty_reject'] > 0){
				$btns[] = get_btn(array('access' => true, 'title' => 'Kirim Qty Reject', 'icon' => 'close', 'onclick' => 'action_kirim(\''.$r['deliverymaterial_kd'].'\',\''.$r['kd_inspectionrawmaterial'].'\',\'reject\')'));
			}
			$btns[] = get_btn(array('access' => true, 'title' => 'History Item', 'icon' => 'book', 'onclick' => 'history_item(\''.$r['kd_inspectionrawmaterial'].'\')'));
			$btn_group = group_btns($btns);
			$data[] = [
				'1' => $btn_group,
				'2' => $r['inspectionrawmaterial_tanggal'],
				'3' => $r['deliverymaterial_no'],
				'4' => $r['inspectionrawmaterial_no'],
				'5' => $r['inspectionrawmaterial_rm_kode'],
				'6' => $r['rm_deskripsi'],
				'7' => $r['rm_spesifikasi'],
				'8' => $r['inspectionrawmaterial_qty'],
				'9' => $r['inspectionrawmaterial_qty_reject'],
				'10' => $qtyBaik,
				'11' => $r['inspectionrawmaterial_keterangan'],
				'12' => $r['inspectionrawmaterial_tgledit'],
				'13' => build_span($r['inspectionrawmaterialstate_span'], $r['inspectionrawmaterialstate_nama']),
			];
		}

		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}
	
	/* --end ssp tabel untuk modul data inspection rawmaterial-- */

	/* --start button tabel untuk modul data inspection rawmaterial-- */
	private function tbl_btn($id, $var, $state) {
		$this->load->model('td_inspectionrawmaterial_userstate');
		$read_access = $this->session->read_access;
		$update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		/** 0 = cancel, 1 = editing (insert update delete) */
		$kd_admin = $this->session->userdata('kd_admin');
		$cekPermissionEditing = $this->td_inspectionrawmaterial_userstate->cek_permission(1, $kd_admin);
		$cekPermissionApprove = $this->td_inspectionrawmaterial_userstate->cek_permission(2, $kd_admin);
		$cekPermissionDiketahui = $this->td_inspectionrawmaterial_userstate->cek_permission(3, $kd_admin);
		$cekPermissionClose = $this->td_inspectionrawmaterial_userstate->cek_permission(4, $kd_admin);
		$cekPermissionCreated = $this->td_inspectionrawmaterial_userstate->cek_permission(5, $kd_admin);

		if($state !== 0 || $state !== 4){
			switch ($state) {
				case 1:
					/** Editing Permission Non Approved */
					if($cekPermissionEditing){
						$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah', 'icon' => 'pencil', 'onclick' => 'get_form(\''.$id.'\')'));
						$btns[] = get_btn(array('access' => $delete_access, 'title' => 'Hapus', 'icon' => 'trash', 'onclick' => 'return confirm(\'Anda akan menghapus data inspection rawmaterial = '.$var.'?\')?hapus_data(\''.$id.'\'):false'));
					}
					/** Editing Permission Created */
					if($cekPermissionCreated){
						$btns[] = get_btn(array('access' => $update_access, 'title' => 'Created', 'icon' => 'file', 'onclick' => 'action_state(\''.$id.'\', 5)'));
					}
					break;
				case 2:
					/** Editing Permission Diketahui */
					if($cekPermissionDiketahui){
						$btns[] = get_btn(array('access' => $update_access, 'title' => 'Diketahui', 'icon' => 'check', 'onclick' => 'action_state(\''.$id.'\', 3)'));
					}
					break;
				case 3:
					/** Editing Permission Close */
					if($cekPermissionClose){
						$btns[] = get_btn(array('access' => $update_access, 'title' => 'Close', 'icon' => 'close', 'onclick' => 'action_state(\''.$id.'\', 4)'));
					}
					break;
				case 5:
					/** Editing Permission Approved */
					if($cekPermissionApprove){
						$btns[] = get_btn(array('access' => $update_access, 'title' => 'Approved', 'icon' => 'check', 'onclick' => 'action_state(\''.$id.'\', 2)'));
					}
					break;
			}
		}

		$btns[] = get_btn(array('access' => $read_access, 'title' => 'Cetak', 'icon' => 'print', 'onclick' => 'cetak(\''.$id.'\')'));
		$btn_group = group_btns($btns);
		return $btn_group;
	}
	/* --end button tabel untuk modul data inspection-- */

	/** Menampilkan bagian berdasarkan user */
	private function get_allowed_bagian($admin_kd = null, $allowedModul = '')
	{
		$this->load->model(['tb_inspectionrawmaterial_user', 'tb_bagian']);
		/** allowedModul = bagian, penyebab, repair */
		if($allowedModul == 'bagian'){
			$arrData = $this->tb_inspectionrawmaterial_user->get_where(['admin_kd' => $admin_kd, 'approved_bagian' => '1'])->result_array();
		}else if($allowedModul == 'penyebab'){
			$arrData = $this->tb_inspectionrawmaterial_user->get_where(['admin_kd' => $admin_kd, 'approved_penyebab' => '1'])->result_array();
		}else if($allowedModul == 'repair'){
			$arrData = $this->tb_inspectionrawmaterial_user->get_where(['admin_kd' => $admin_kd, 'approved_repair' => '1'])->result_array();
		}else{
			$arrData = [];
		}
		$bagians = [];
		if(!empty($arrData)){
			// $bagians = "'".implode("','", array_column($arrData, 'kd_bagian'))."'";
			$bagians = array_column($arrData, 'kd_bagian');
		}

		/** Jika tipe role admin maka bagian tampilkan semua */
		if( $this->session->userdata('tipe_admin') == 'Admin' ){
			$arrBagian = $this->tb_bagian->get_all()->result_array();
			$bagians = array_column($arrBagian, 'bagian_kd');
		}

		return $bagians;
	}

	public function get_row($id = '') {
		$this->db->from($this->tbl_name)
			->where(array($this->p_key => $id));
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}

	public function create_code() {
		$this->db->select($this->p_key.' AS code')
			->from($this->tbl_name)
			->where(array('DATE(inspectionrawmaterial_tglinput)' => date('Y-m-d')))
			->order_by('inspectionrawmaterial_tglinput DESC, '.$this->p_key.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0):
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -3);
		endif;
		$angka = $urutan + 1;
		$primary = 'INM'.date('dmy').str_pad($angka, 3, '000', STR_PAD_LEFT);
		return $primary;
	}

	public function create_no($tgl)
	{
		$inp_year = format_date($tgl, 'Y');
		$query = $this->db->select('MAX(inspectionrawmaterial_no) as max_no')
			->where('YEAR(inspectionrawmaterial_tanggal)', $inp_year)
			->get($this->tbl_name)->row();
		if (empty($query->max_no)) :
			$no = 0;
		else :
			$no = substr($query->max_no, -3);
		endif;
		$no = (int)$no + 1;
		$inp_no = 'IM-'.$inp_year . '.' . str_pad($no, '3', '000', STR_PAD_LEFT);
		return $inp_no;
	}

	public function create($data = '') {
		$act = $this->db->insert($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	public function update($data = '', $id = '') {
		$this->db->where($this->p_key, $id);
		$act = $this->db->update($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	public function delete_data($id = '') {
		$act = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		return $act;
	}

	public function get_all(){
		return $this->db->get($this->tbl_name);
	}

	public function get_by_param ($param=[]) {
		$this->db->from($this->tbl_name);
		$this->db->join('tb_inspectionrawmaterial_state', 'tb_inspectionrawmaterial_state.inspectionrawmaterialstate_kd='.$this->tbl_name.'.inspectionrawmaterialstate_kd', 'left');
		$this->db->join('tb_bagian', 'tb_bagian.bagian_kd='.$this->tbl_name.'.inspectionrawmaterial_bagian_kd', 'left');
		$this->db->join('(SELECT bagian_kd AS bagian_kd3, bagian_nama AS bagian_nama3 FROM tb_bagian) tb_bagian3', 'tb_bagian3.bagian_kd3='.$this->tbl_name.'.inspectionrawmaterial_penyebab_bagian_kd', 'left');
		$this->db->join('tb_admin', 'tb_admin.kd_admin='.$this->tbl_name.'.admin_kd', 'left');
		$this->db->join('(SELECT kd_admin AS kd_admin2, nm_admin AS nm_admin2 FROM tb_admin) AS tb_admin2', 'tb_admin2.kd_admin2='.$this->tbl_name.'.approved_bagian_admin_kd', 'left');
		$this->db->join('(SELECT kd_admin AS kd_admin3, nm_admin AS nm_admin3 FROM tb_admin) AS tb_admin3', 'tb_admin3.kd_admin3='.$this->tbl_name.'.approved_penyebab_admin_kd', 'left');
		$this->db->join('td_delivery_material', 'td_delivery_material.deliverymaterial_kd=tm_inspection_rawmaterial.deliverymaterial_kd', 'left');
		$this->db->join('tm_rawmaterial', 'tm_rawmaterial.rm_kd=td_delivery_material.rm_kd', 'left');
		$this->db->join('td_rawmaterial_goodsreceive', 'td_rawmaterial_goodsreceive.rmgr_code_srj=td_delivery_material.rmgr_code_srj AND td_rawmaterial_goodsreceive.rm_kd=td_delivery_material.rm_kd', 'left');
		$this->db->join('tm_purchaseorder', 'tm_purchaseorder.po_kd=td_rawmaterial_goodsreceive.po_kd', 'left');
		$this->db->join('tm_suplier', 'tm_suplier.suplier_kd=tm_purchaseorder.suplier_kd', 'left');
		$this->db->where($param);
		$act = $this->db->get();
		return $act;
	}

	public function get_reject_material_by_month($year, $month)
	{
		$reportData = $this->db->query("
			SELECT 
				tm_inspection_rawmaterial.inspectionrawmaterial_rm_kode, 
				tm_rawmaterial.rm_deskripsi, 
				GROUP_CONCAT(tm_inspection_rawmaterial.inspectionrawmaterial_no) kd_inspectionrawmaterial, 
				SUM(tm_inspection_rawmaterial.inspectionrawmaterial_qty) sum_inspectionrawmaterial_qty, 
				COALESCE(SUM(tm_inspection_rawmaterial.inspectionrawmaterial_qty_reject),0) sum_inspectionrawmaterial_qty_reject 
			FROM tm_inspection_rawmaterial 
			LEFT JOIN td_delivery_material ON td_delivery_material.deliverymaterial_kd = tm_inspection_rawmaterial.deliverymaterial_kd
			LEFT JOIN tm_rawmaterial ON tm_rawmaterial.rm_kd = td_delivery_material.rm_kd
			WHERE tm_inspection_rawmaterial.inspectionrawmaterial_tindakan IS NOT NULL AND 
			YEAR(tm_inspection_rawmaterial.inspectionrawmaterial_tanggal) = '".$year."' AND 
			MONTH(tm_inspection_rawmaterial.inspectionrawmaterial_tanggal) = '".$month."' 
			GROUP BY tm_inspection_rawmaterial.inspectionrawmaterial_rm_kode
		")->result_array();
		return $reportData;
	}

	public function sendMail($inspection_kd, $state_kd)
	{
		$this->load->model(['_mail_configuration', 'tm_inspection_rawmaterial', 'tb_inspectionrawmaterial_state']);
		$state = $this->tb_inspectionrawmaterial_state->get_by_param(['inspectionrawmaterialstate_kd'=>$state_kd])->row();
		$inspection = $this->get_row($inspection_kd);
		/** Kirim notif email hanya untuk yang reject */
		if($inspection->inspectionrawmaterial_tindakan == 'reject'){
			/** State notif ke email */
			if($state->inspectionrawmaterialstate_notifemail == '1'){
				$subject = 'Inspection Rawmaterial - ( '.$inspection->inspectionrawmaterial_no.' )';
				$dtMessage['text'] = 'Terdapat barang reject yang perlu anda proses :';
				$dtMessage['url'] = 'http://'.$_SERVER['HTTP_HOST'].base_url().'/quality_control/inspection_rawmaterial/report_pdf?id='.$inspection_kd;
				$dtMessage['urltext'] = 'Inspection Rawmaterial ( '.$inspection->inspectionrawmaterial_no.' ) '.$inspection->inspectionrawmaterial_rm_kode;
				$dtMessage['keterangan'] = $inspection->inspectionrawmaterial_keterangan;
				$message = $this->load->view('templates/email/email_notification', $dtMessage, true);

				$emailTo = explode(';', $state->inspectionrawmaterialstate_emailto);
				$emailCc = explode(';', $state->inspectionrawmaterialstate_emailcc);

				$act = $this->_mail_configuration->sendEmailCc($emailTo, $emailCc, $subject, $message);
				return $act;
			}
		}
		return true;
	}
	
}