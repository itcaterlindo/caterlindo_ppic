<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_purchaseinvoice extends CI_Model {
	private $tbl_name = 'tm_purchaseinvoice';
	private $p_key = 'purchaseinvoice_kd';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
	}
	
	// public function ssp_table() {
	// 	$data['table'] = $this->tbl_name;

	// 	$data['primaryKey'] = $this->p_key;

	// 	$data['columns'] = array(
	// 		array( 'db' => $this->p_key,
	// 			'dt' => 1, 'field' => $this->p_key,
	// 			'formatter' => function($d){
					
	// 				return $this->tbl_btn($d);
	// 			}),
			
	// 		array( 'db' => 'a.purchaseinvoice_tanggaltermpayment', 
	// 			'dt' => 2, 'field' => 'purchaseinvoice_tanggaltermpayment',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'a.purchaseinvoice_tanggal', 
	// 			'dt' => 3, 'field' => 'purchaseinvoice_tanggal',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'a.purchaseinvoice_no', 
	// 			'dt' => 4, 'field' => 'purchaseinvoice_no',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'a.purchaseinvoice_faktur', 
	// 			'dt' => 5, 'field' => 'purchaseinvoice_faktur',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
	// 		array( 'db' => 'b.suplier_nama', 
	// 			'dt' => 6, 'field' => 'suplier_nama',
	// 			'formatter' => function ($d){
	// 				$d = $this->security->xss_clean($d);

	// 				return $d;
	// 			}),
			
	// 	);

	// 	$data['sql_details'] = sql_connect();
	// 	$data['joinQuery'] =    "FROM ".$this->tbl_name." as a
	// 							LEFT JOIN tm_suplier as b ON a.suplier_kd=b.suplier_kd";
	// 	$data['where'] = "";
		
	// 	return $data;
	// }

	public function ssp_table2 ($bulan, $tahun) {
		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		
		$data = [];
		$qData = $this->db->select($this->tbl_name.'.*, tm_suplier.suplier_nama, tm_suplier.suplier_kode')
				->from($this->tbl_name)
				->join('tm_suplier', $this->tbl_name.'.suplier_kd=tm_suplier.suplier_kd', 'left')
				->get();

				if($bulan != 'ALL'){
					$qData = $this->db->select($this->tbl_name.'.*, tm_suplier.suplier_nama, tm_suplier.suplier_kode')
					->from($this->tbl_name)
					->join('tm_suplier', $this->tbl_name.'.suplier_kd=tm_suplier.suplier_kd', 'left')
					->where('MONTH(tm_purchaseinvoice.purchaseinvoice_tanggal)', $bulan)
					->where('YEAR(tm_purchaseinvoice.purchaseinvoice_tanggal)', $tahun)
					->get();
				}else{
					$qData = $this->db->select($this->tbl_name.'.*, tm_suplier.suplier_nama, tm_suplier.suplier_kode')
					->from($this->tbl_name)
					->join('tm_suplier', $this->tbl_name.'.suplier_kd=tm_suplier.suplier_kd', 'left')
					->where('YEAR(tm_purchaseinvoice.purchaseinvoice_tanggal)', $tahun)
					->get();
				}



		foreach ($qData->result_array() as $r) {
			$data[] = [
				'1' => $r['status_sap'] == '0' ? $this->tbl_btn($r['purchaseinvoice_kd']) : $this->tbl_btn_add($r['purchaseinvoice_kd']),
				'2' => $r['purchaseinvoice_tanggal'],
				'3' => $r['purchaseinvoice_tanggaltermpayment'],
				'4' => $r['purchaseinvoice_no'],
				'5' => $r['purchaseinvoice_faktur'],
				'6' => $r['suplier_kode'].' | '.$r['suplier_nama'],
				'7' => custom_numberformat_round($r['purchaseinvoice_grandtotal'], 2, 'IDR'),
				'8' => $r['purchaseinvoice_note'],
				'9' => $r['purchaseinvoice_tgledit'],
			];
		}
		
		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

	private function tbl_btn($id) {
		$btns = array();
		if (cek_permission('PURCHASEINVOICE_VIEW')) {
			$btns[] = get_btn(array('title' => 'Lihat Item', 'icon' => 'search', 'onclick' => 'view_box(\''.$id.'\')'));
		}
        if (cek_permission('PURCHASEINVOICE_UPDATE')) {
            $btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
        }		
		if (cek_permission('PURCHASEINVOICE_UPDATE')) {
            $btns[] = get_btn(array('title' => 'Push ke SAP', 'icon' => 'arrow-up', 'onclick' => 'push_sap(\''.$id.'\')'));
        }
        if (cek_permission('PURCHASEINVOICE_DELETE')) {
			$btns[] = get_btn_divider();
            $btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'action_hapus_master(\''.$id.'\')'));
        }
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	private function tbl_btn_add($id) {
		$btns = array();
		if (cek_permission('PURCHASEINVOICE_VIEW')) {
			$btns[] = get_btn(array('title' => 'Lihat Item', 'icon' => 'search', 'onclick' => 'view_box(\''.$id.'\')'));
		}
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function labelStatus($label, $colour){
		$span = '<span class="label label-'.$colour.'">'.$label.'</span>';

		return $span;
	}

	public function create_code() {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}
	public function get_by_param1 ($param=[]) {
		
		$this->db->join('tm_suplier', 'tm_suplier.suplier_kd=tm_purchaseinvoice.suplier_kd','inner');
		$this->db->join('tb_admin', 'tb_admin.kd_admin=tm_purchaseinvoice.admin_kd','inner');
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

	public function insert_batch_data($data) {
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function calc_grandtotal_dp_batch($purchaseinvoice_kds = []) {
		$data = [];
		if (!empty($purchaseinvoice_kds)) {
			$resultInv = $this->db->select()
				->from('tm_purchaseinvoice')
				->where_in('purchaseinvoice_kd', $purchaseinvoice_kds)
				->get()->result_array();
			$resultInvdetail = $this->db->select()
						->from('td_purchaseinvoice_detail')
						->where_in('purchaseinvoice_kd', $purchaseinvoice_kds)
						->get()->result_array();
	
			foreach ($resultInv as $eInv) {
				$totalInv = 0; $ppnRp = 0;
				$arrSubtotal = [];
				foreach ($resultInvdetail as $eInvdetail) {
					$subTotal = 0;
					if ($eInvdetail['purchaseinvoice_kd'] == $eInv['purchaseinvoice_kd']){
						$subTotal = $eInvdetail['purchaseinvoicedetail_qty'] * $eInvdetail['purchaseinvoicedetail_hargaunit'];
						$arrSubtotal[] = $subTotal;
					}
				}
				$totalInv = array_sum($arrSubtotal);
				if (!empty($eInv['purchaseinvoice_ppn'])) {
					$ppnRp = $eInv['purchaseinvoice_ppn'] / 100 * $totalInv;
				}
				$fix_total = $totalInv + $ppnRp;
				$grandTotal = $fix_total ? $fix_total : $eInv['purchaseinvoice_grandtotal'];
				$data[] = [
					'purchaseinvoice_kd' => $eInv['purchaseinvoice_kd'],
					'purchaseinvoice_no' => $eInv['purchaseinvoice_no'],
					'purchaseinvoice_faktur' => $eInv['purchaseinvoice_faktur'],
					'grandTotal' => $grandTotal,
				];
			}
		}

		return $data;
		
	}

	public function calc_grandtotal_dp_dpp_batch($purchaseinvoice_kds = []) {
		$data = [];
		if (!empty($purchaseinvoice_kds)) {
			$resultInv = $this->db->select()
				->from('tm_purchaseinvoice')
				->where_in('purchaseinvoice_kd', $purchaseinvoice_kds)
				->get()->result_array();
			$resultInvdetail = $this->db->select()
						->from('td_purchaseinvoice_detail')
						->where_in('purchaseinvoice_kd', $purchaseinvoice_kds)
						->get()->result_array();
	
			foreach ($resultInv as $eInv) {
				$totalInv = 0;
				$arrSubtotal = [];
				foreach ($resultInvdetail as $eInvdetail) {
					$subTotal = 0;
					if ($eInvdetail['purchaseinvoice_kd'] == $eInv['purchaseinvoice_kd']){
						$subTotal = $eInvdetail['purchaseinvoicedetail_qty'] * $eInvdetail['purchaseinvoicedetail_hargaunit'];
						$arrSubtotal[] = $subTotal;
					}
				}
				$totalInv = array_sum($arrSubtotal);
				
				$data[] = [
					'purchaseinvoice_kd' => $eInv['purchaseinvoice_kd'],
					'purchaseinvoice_no' => $eInv['purchaseinvoice_no'],
					'purchaseinvoice_faktur' => $eInv['purchaseinvoice_faktur'],
					'grandTotal' => $totalInv,
				];
			}
		}

		return $data;
		
	}

}