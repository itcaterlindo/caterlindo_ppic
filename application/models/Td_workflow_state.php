<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_workflow_state extends CI_Model {
	private $tbl_name = 'td_workflow_state';
	private $p_key = 'wfstate_kd';

	public function ssp_table($wf_kd) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){
					
					return $this->tbl_btn($row[9], $d);
				}),
			array( 'db' => 'wfstate_nama', 
				'dt' => 2, 'field' => 'wfstate_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'wfstate_initial', 
				'dt' => 3, 'field' => 'wfstate_initial',
				'formatter' => function ($d){
					$d = choice_enum($d);
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'wfstate_badgecolor', 
				'dt' => 4, 'field' => 'wfstate_badgecolor',
				'formatter' => function ($d){
					$d = build_badgecolor ($d, $d);

					return $d;
                }),
			array( 'db' => 'wfstate_onprintout', 
				'dt' => 5, 'field' => 'wfstate_onprintout',
				'formatter' => function ($d){
					$d = choice_enum($d);
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'wfstate_onprintoutseq', 
				'dt' => 6, 'field' => 'wfstate_onprintoutseq',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'wfstate_action', 
				'dt' => 7, 'field' => 'wfstate_action',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'wfstate_active', 
				'dt' => 8, 'field' => 'wfstate_active',
				'formatter' => function ($d){
					$d = choice_enum($d);
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'wfstate_tgledit', 
				'dt' => 9, 'field' => 'wfstate_tgledit',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'wf_kd', 
				'dt' => 10, 'field' => 'wf_kd',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name;
        $data['where'] = "wf_kd = $wf_kd";
		
		return $data;
	}

	private function tbl_btn($wf_kd, $id) {
		$btns = array();
		$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$wf_kd.'\',\''.$id.'\')'));	
		$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'delete_data(\''.$id.'\')'));	
		$btn_group = group_btns($btns);

		return $btn_group;
	}

    public function create_code () {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_where_in ($where, $in = []) {
		$query = $this->db->where_in($where, $in)
				->select($this->tbl_name.'.*')
				->get($this->tbl_name);
		return $query;
	}

	public function update_batch ($idColumn, $data = []) {
		$act = $this->db->update_batch($this->tbl_name, $data, $idColumn);
		return $act;
	}

	public function getInitialState($wf_kd)
	{
		$qRow = $this->get_by_param(['wf_kd' => $wf_kd, 'wfstate_initial' => '1'])->row_array();
		return $qRow['wfstate_kd'];
	}
	
}