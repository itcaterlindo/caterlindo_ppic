<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_salesorder_item extends CI_Model {
	private $tbl_name = 'td_salesorder_item';
	private $p_key = 'kd_ditem_so';

	/*
	** Fungsi ini digunakan untuk menghitung jumlah item
	** berdasarkan no_salesorder, dengan begitu jumlah data akan sama jika order_tipe new dan additional ada.
	*/
	public function count_from_noso($no_salesorder = '') {
		$this->db->select('SUM(a.item_qty) AS total')
			->from($this->tbl_name.' a')
			->join('tm_salesorder b', 'b.kd_msalesorder = a.msalesorder_kd', 'left')
			->where(array('b.no_salesorder' => $no_salesorder));
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			return $row->total;
		else :
			return '0';
		endif;
	}

	/*
	** Fungsi ini digunakan untuk mengambil data item menurut no_salesorder pada tm_salesorder
	*/
	public function get_item($no_salesorder = '') {
		$this->db->select('a.kd_ditem_so, a.msalesorder_kd, a.barang_kd, a.item_barcode, a.item_status, a.item_desc, a.item_dimension, a.item_qty, a.harga_barang, a.harga_retail, a.disc_type, a.item_disc, a.total_harga, a.item_note, a.item_code, a.netweight, a.grossweight, c.hs_code, d.kd_mgrouping')
			->from($this->tbl_name.' a')
			->join('tm_salesorder b', 'b.kd_msalesorder = a.msalesorder_kd', 'left')
			->join('tm_barang c', 'c.kd_barang = a.barang_kd', 'left')
			->join('tm_product_grouping d', 'c.kd_barang = d.barang_kd', 'left')
			->where(array('b.no_salesorder' => $no_salesorder))
			->order_by('a.kd_ditem_so ASC');
		$query = $this->db->get();
		$result = $query->result();

		return $result;
	}

	public function get_items($kd_mso = '') {
		$this->db->from($this->tbl_name)
			->where_in('msalesorder_kd', $kd_mso)
			->order_by('tgl_input');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	
	// Untuk get item sekaligus menghitung jumlah qty per item
	public function get_items_rekap ($kd_mso) {
		$this->db->select('a.msalesorder_kd, a.kd_ditem_so as parent_kd, "-" as child_kd , a.barang_kd, a.item_code, a.item_barcode, a.item_status, a.item_desc, a.item_dimension, a.item_note, SUM(a.item_qty) AS item_qty, b.kd_mgrouping');
		$this->db->from($this->tbl_name.' as a');
		$this->db->join('tm_product_grouping as b', 'a.barang_kd=b.barang_kd', 'left');
		$this->db->where_in('msalesorder_kd', $kd_mso);
		$this->db->group_by('a.barang_kd, a.item_status, a.item_dimension, a.item_note');
		$query = $this->db->get();
		return $query;
	}

	public function get_item_sumtotal_by_mso($kd_mso){
		$this->db->select('msalesorder_kd, SUM(total_harga) AS sum_total_harga');
		$this->db->from($this->tbl_name);
		$this->db->where_in('msalesorder_kd', $kd_mso);
		$this->db->group_by('msalesorder_kd');
		$query = $this->db->get();
		return $query;
	}

	public function get_item_where_condition($where = []) {
		$query = $this->db->select($this->tbl_name.'.*')
					->from($this->tbl_name)
					->where($where)
					->order_by($this->tbl_name.'.no_pj')
					->get();
		return $query;
	}

	public function get_by_param_barcode_eksport ($param=[]) {
		$query = $this->db->select('td_salesorder_item.item_status, td_salesorder_item.item_qty, tm_barang.*, td_barang_barcode_eksport.*')
				->from($this->tbl_name)
				->join('tm_barang', $this->tbl_name.'.barang_kd=tm_barang.kd_barang', 'left')
				->join('td_barang_barcode_eksport', $this->tbl_name.'.barang_kd=td_barang_barcode_eksport.kd_barang', 'inner')
				->where($param)
				->get();
		return $query;
	}

	// Untuk get item sekaligus menghitung jumlah qty per item per so
	public function get_items_rekap_by_so ($kd_mso) {
		$this->db->select('a.msalesorder_kd, a.kd_ditem_so as parent_kd, "-" as child_kd , a.barang_kd, a.item_code, a.item_barcode, a.item_status, a.item_desc, a.item_dimension, a.item_note, SUM(a.item_qty) AS item_qty, b.kd_mgrouping, c.tgl_selesai');
		$this->db->from($this->tbl_name.' as a');
		$this->db->join('tm_product_grouping as b', 'a.barang_kd=b.barang_kd', 'left');
		$this->db->join('tm_salesorder as c', 'a.msalesorder_kd=c.kd_msalesorder', 'left');
		$this->db->where_in('msalesorder_kd', $kd_mso);
		$this->db->group_by('a.barang_kd, a.item_status, a.item_dimension, a.item_note, a.msalesorder_kd');
		$query = $this->db->get();
		return $query;
	}

	public function get_where_in($kd_ditem_so = []) {
		$this->db->from($this->tbl_name)
			->where_in('kd_ditem_so', $kd_ditem_so);
		$query = $this->db->get();
		return $query;
	}

	public function get_where_in_project ($kd_ditem_so = []) {
		$this->db->select($this->tbl_name.'.*, tm_project.project_kd, tm_project.project_nopj, tm_project.project_itemcode, tm_project.project_itemdesc, tm_project.project_itemdimension')
			->from($this->tbl_name)
			->join('tm_project', $this->tbl_name.'.kd_ditem_so=tm_project.kd_ditem_so', 'left')
			->where_in($this->tbl_name.'.kd_ditem_so', $kd_ditem_so);
		$query = $this->db->get();
		return $query;
	}

	public function get_by_param_suggestion_so_barang($param = [])
    {
        $this->db->where($param)->select('barang_kd, item_code, item_barcode, item_status, item_desc, item_dimension, item_qty');
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

	public function get_where_in_po($po = '')
	{
		$this->db->select('a.*, b.no_po');
		$this->db->from($this->tbl_name.' as a');
		$this->db->join('tm_salesorder as b', 'a.msalesorder_kd=b.kd_msalesorder', 'inner');
		$this->db->where_in('b.no_po', $po);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function update_data($id, $data = [])
	{
		$this->db->where($this->p_key, $id);
		$act = $this->db->update($this->tbl_name, $data);
		return $act;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

}