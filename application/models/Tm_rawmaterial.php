<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_rawmaterial extends CI_Model {
	private $tbl_name = 'tm_rawmaterial';
	private $p_key = 'rm_kd';

	public function __construct() {
		parent::__construct();

		$this->load->model(['td_rawmaterial_group']);
	}

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'a.rm_kode', 
				'dt' => 2, 'field' => 'rm_kode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.rm_oldkd', 
				'dt' => 3, 'field' => 'rm_oldkd',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.rm_nama', 
				'dt' => 4, 'field' => 'rm_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.rm_deskripsi', 
				'dt' => 5, 'field' => 'rm_deskripsi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.rm_spesifikasi', 
				'dt' => 6, 'field' => 'rm_spesifikasi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.rmkategori_nama', 
				'dt' => 7, 'field' => 'rmkategori_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.rmsatuan_nama', 
				'dt' => 8, 'field' => 'rmsatuan_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.flag_active', 
				'dt' => 9, 'field' => 'flag_active',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);
					if($d == '1'){
						$d = bg_label('Active', 'green');
					}else{
						$d = bg_label('Inactive', 'red');
					}
					return $d;
				}),
			array( 'db' => 'a.rm_tgledit', 
				'dt' => 10, 'field' => 'rm_tgledit',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] =    "FROM ".$this->tbl_name." as a
								LEFT JOIN td_rawmaterial_kategori as b ON a.rmkategori_kd=b.rmkategori_kd
								LEFT JOIN td_rawmaterial_satuan as c ON a.rmsatuan_kd=c.rmsatuan_kd";
		$data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id) {
		$view_access = cek_permission('MATERIALDATA_VIEW');
		$update_access = cek_permission('MATERIALDATA_UPDATE');
		$delete_access = cek_permission('MATERIALDATA_DELETE');
		$btns = array();
		if ($view_access) {
			$btns[] = get_btn(array('title' => 'Lihat Item', 'icon' => 'search', 'onclick' => 'view_data(\''.$id.'\')'));
		}
		if ($update_access) {
			$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
		}
		if ($delete_access) {
			$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));	
		}
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_rm_kd ($code) {
		if (empty($code)) {
			$query = $this->db->select('MAX('.$this->p_key.') as code')
						->from ($this->tbl_name)
						->where('rm_kd <> "SPECIAL"')
						->get()->row();
			$code = $query->code;
		}
		$num = (int) substr($code, -6);
		$num = $num + 1;
		$code = 'RM'.str_pad($num, 6, '0', STR_PAD_LEFT);
		return $code;
	}

	public function create_code() {
		$this->db->select('MAX('.$this->p_key.') AS code')
			->from($this->tbl_name);
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -6);
		endif;
		$angka = $urutan + 1;
		$primary = 'MRM'.str_pad($angka, 6, '0', STR_PAD_LEFT);
		return $primary;
	}

	public function create_code_new($rm_nama){
		$subsRm_nama = strtoupper(substr(str_replace(' ', '', $rm_nama), 0, 4));
		$query = $this->db->select('MAX(rm_kode) AS code')
					->from($this->tbl_name)
					->like('rm_kode', $subsRm_nama, 'after')
					->get();
		$num = 0;
		if ($query->num_rows() != 0 ){
			$row = $query->row_array();
			$row = $row['code'];
			$num = (int)substr($row, -3);
		}
		$nextNum = $num + 1;
		$nextNum = $subsRm_nama.str_pad($nextNum, 3, '0', STR_PAD_LEFT);
		return $nextNum;
	}

	public function create_code_custom($rm_nama){
		$subsRm_nama = strtoupper(substr(str_replace(' ', '', $rm_nama), 0, 3));
		$custom_nama = 'S'.$subsRm_nama;
		$rmgroup_custom = $this->td_rawmaterial_group->customCode();
		$query = $this->db->select('MAX(rm_kode) AS code')
					->from($this->tbl_name)
					->where('rmgroup_kd', $rmgroup_custom)
					->like('rm_kode', $custom_nama, 'after')
					->get();
		$num = 0;
		if ($query->num_rows() != 0 ){
			$row = $query->row_array();
			$row = $row['code'];
			$num = (int)substr($row, -4);
		}
		$nextNum = $num + 1;
		$nextNum = $custom_nama.str_pad($nextNum, 4, '0', STR_PAD_LEFT);
		return $nextNum;
	}

	public function get_row($kd) {
		$this->db->from($this->tbl_name)
			->where(['flag_active'=>'1'])
			->where(array($this->p_key => $kd));
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where(['flag_active'=>'1']);
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->where('flag_active', '1')
			->get($this->tbl_name);
		return $act;
	}

	public function get_rekomendasi_pr(){
		$query = $this->db->where('rm_stock <= rm_stock_safety')
					->get($this->tbl_name);
		return $query;
	}

	public function get_detail_material ($param = []){
		$act = $this->db->select($this->tbl_name.'.*, td_rawmaterial_satuan.rmsatuan_nama, td_rawmaterial_kategori.rmkategori_nama, td_rawmaterial_group.rmgroup_nama,
					satuan_secondary.rmsatuan_nama as satuan_secondary_nama')
				->from($this->tbl_name)
				->join('td_rawmaterial_satuan', $this->tbl_name.'.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
				->join('td_rawmaterial_kategori', $this->tbl_name.'.rmkategori_kd=td_rawmaterial_kategori.rmkategori_kd', 'left')
				->join('td_rawmaterial_group', $this->tbl_name.'.rmgroup_kd=td_rawmaterial_group.rmgroup_kd', 'left')
				->join('td_rawmaterial_satuan as satuan_secondary', 'satuan_secondary.rmsatuan_kd='.$this->tbl_name.'.rmsatuansecondary_kd', 'left')
				->where([$this->tbl_name.'.flag_active'=>'1'])
				->where($param)
				->get();
		return $act;
	}

	public function get_all_detail_material (){
		$act = $this->db->select($this->tbl_name.'.*, td_rawmaterial_satuan.rmsatuan_nama, td_rawmaterial_kategori.rmkategori_nama, td_rawmaterial_group.rmgroup_nama')
				->from($this->tbl_name)
				->join('td_rawmaterial_satuan', $this->tbl_name.'.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
				->join('td_rawmaterial_kategori', $this->tbl_name.'.rmkategori_kd=td_rawmaterial_kategori.rmkategori_kd', 'left')
				->join('td_rawmaterial_group', $this->tbl_name.'.rmgroup_kd=td_rawmaterial_group.rmgroup_kd', 'left')
				->where($this->tbl_name.'.flag_active', '1')
				->get();
		return $act;
	}

	public function get_by_param_detail_satuan($param = []){
		$act = $this->db->select($this->tbl_name.'.*, td_rawmaterial_satuan.rmsatuan_nama, td_rawmaterial_kategori.rmkategori_nama, td_rawmaterial_group.rmgroup_nama, satuan_secondary.rmsatuan_nama as satuan_secondary_nama')
				->from($this->tbl_name)
				->join('td_rawmaterial_satuan', $this->tbl_name.'.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
				->join('td_rawmaterial_satuan as satuan_secondary', $this->tbl_name.'.rmsatuansecondary_kd=satuan_secondary.rmsatuan_kd', 'left')
				->join('td_rawmaterial_kategori', $this->tbl_name.'.rmkategori_kd=td_rawmaterial_kategori.rmkategori_kd', 'left')
				->join('td_rawmaterial_group', $this->tbl_name.'.rmgroup_kd=td_rawmaterial_group.rmgroup_kd', 'left')
				->where($param)
				->get();
		return $act;
	}

	public function is_plate($rm_kd){
		$query = $this->get_by_param (['rm_kd' => $rm_kd])->row_array();
		if ($query['rmkategori_kd'] == '01'){
			return true;
		}else{
			return false;
		}
	}

}