<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_suplier_katalog extends CI_Model {
	private $tbl_name = 'td_suplier_katalog';
	private $p_key = 'katalog_kd';
	private $currencys;

	public function ssp_table() {
		$this->load->model(['tm_currency']);

		$this->currencys = $this->tm_currency->format_helper_currency_all();

		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'c.rm_kode', 
				'dt' => 2, 'field' => 'rm_kode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.rm_nama', 
				'dt' => 3, 'field' => 'rm_nama',
				'formatter' => function ($d , $row){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'b.suplier_nama', 
				'dt' => 4, 'field' => 'suplier_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.katalog_harga', 
				'dt' => 5, 'field' => 'katalog_harga',
				'formatter' => function ($d, $row){
					$d = format_currency($d, $this->currencys[$row[9]]);

					return $d;
				}),
			array( 'db' => 'd.rmsatuan_nama', 
				'dt' => 6, 'field' => 'rmsatuan_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.katalog_duedate', 
				'dt' => 7, 'field' => 'katalog_duedate',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.katalog_aktif', 
				'dt' => 8, 'field' => 'katalog_aktif',
				'formatter' => function ($d){
					$d = choice_enum($d);

					return $d;
				}),
			array( 'db' => 'a.katalog_tgledit', 
				'dt' => 9, 'field' => 'katalog_tgledit',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.kd_currency', 
				'dt' => 10, 'field' => 'kd_currency',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." as a
							LEFT JOIN tm_suplier b ON a.suplier_kd=b.suplier_kd
							LEFT JOIN tm_rawmaterial as c ON a.rm_kd=c.rm_kd
							LEFT JOIN td_rawmaterial_satuan as d ON a.katalog_satuankd=d.rmsatuan_kd";
		$data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id) {
		$btns = array();
		if (cek_permission('SUPPLIERKATALOG_UPDATE')) {
			$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
		}
		if (cek_permission('SUPPLIERKATALOG_DELETE')) {
			$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));	
		}
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
		$this->db->select($this->p_key.' AS code')
			->from($this->tbl_name)
			->where(array('DATE(katalog_tglinput)' => date('Y-m-d')))
			->order_by($this->p_key.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -6);
		endif;
		$angka = $urutan + 1;
		$primary = 'MRK'.date('ymd').str_pad($angka, 6, '0', STR_PAD_LEFT);
		return $primary;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_by_param_in ($param, $params=[]) {
		$this->db->where_in($param, $params);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_detail_by_param($param = []){
		$act = $this->db->select($this->tbl_name.'.*, tm_suplier.suplier_nama, tm_rawmaterial.*')
				->from($this->tbl_name)
				->join('tm_suplier', $this->tbl_name.'.suplier_kd=tm_suplier.suplier_kd', 'left')
				->join('tm_rawmaterial', $this->tbl_name.'.rm_kd=tm_rawmaterial.rm_kd', 'left')
				->where($param)
				->get();
		return $act;
	}

	public function get_all_detail(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

	public function update_harga_batch($data){
		if (!empty($data)){
			foreach ($data as $eachData){
				$dataUpdate[] = array(
					'katalog_kd' => $eachData['katalog_kd'],
					'katalog_harga' => $eachData['podetail_harga'],
					'katalog_konversi' => $eachData['podetail_konversi'],
					'katalog_satuankd' => $eachData['katalog_satuankd'],
					'katalog_tgledit' => format_date($eachData['podetail_tglinput'], 'Y-m-d H:i:s'),
				);	
			}
			$act = $this->db->update_batch($this->tbl_name, $dataUpdate, 'katalog_kd');
		}else{
			$act = false;
		}

		return $act;
	}

	public function update_harga($data){
		if (!empty($data)){
			$dataUpdate = array(
				'katalog_harga' => $data['katalog_harga'],
				'katalog_konversi' => $data['katalog_konversi'],
				'kd_currency' => $data['kd_currency'],
				'katalog_satuankd' => $data['katalog_satuankd'],
				'katalog_tgledit' => $data['katalog_tgledit'],
			);
			$act = $this->update_data(array('katalog_kd' => $data['katalog_kd']), $dataUpdate);
		}else{
			$act = false;
		}
		return $act;
	}

	public function create_katalog($suplier_kd, $rm_kd, $katalog_harga, $katalog_konversi, $rmsatuan_kd, $kd_currency){
		/** Cek apakah rm_kd sudah ada di katalog supliernya */
		$queryKatalog = $this->get_by_param(array('suplier_kd'=> $suplier_kd, 'rm_kd' => $rm_kd));
		$cekKatalog = $queryKatalog->num_rows();
		if ($cekKatalog == 0){
			$this->load->model(['tm_suplier']);
			$suplier = $this->tm_suplier->get_by_param(['suplier_kd' => $suplier_kd])->row_array();
			
			$katalog_kd = $this->create_code();
			$data = array(
				'katalog_kd' => $katalog_kd,
				'suplier_kd' => $suplier_kd,
				'rm_kd' => $rm_kd,
				'katalog_harga' => $katalog_harga,
				'katalog_konversi' => $katalog_konversi,
				'katalog_duedate' => $suplier['suplier_leadtime'],
				'kd_currency' => $kd_currency,
				'katalog_satuankd' => $rmsatuan_kd,
				'katalog_aktif' => '1',
				'katalog_tglinput' => date('Y-m-d H:i:s'),
				'katalog_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);
	
			$act = $this->insert_data($data);
			if ($act){
				$resp = $katalog_kd;
			}else{
				$resp = false;
			}
		}else{
			/** Jika sudah ada katalog */
			$rowKatalog = $queryKatalog->row_array();
			$dataUpdate = array(
				'katalog_kd' => $rowKatalog['katalog_kd'],
				'katalog_harga' => $katalog_harga,
				'kd_currency' => $kd_currency,
				'katalog_konversi' => $katalog_konversi,
				'katalog_satuankd' => $rmsatuan_kd,
				'katalog_tgledit' => date('Y-m-d H:i:s'),
			);
			$actUpdate = $this->update_harga($dataUpdate);
			if ($actUpdate){
				$resp = $rowKatalog['katalog_kd'];
			}else{
				$resp = false;
			}
		}

		return $resp;
	}

}