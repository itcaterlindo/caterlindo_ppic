<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Model_kartustokfg extends CI_Model {

    public function __construct()
    {
        $this->load->model(['td_finishgood_in', 'td_finishgood_out', 'td_finishgood_adjustment', 'td_finishgood_repacking', 'td_finishgood_delivery']);
    }

    public function kartu_stok_all($startdate, $enddate, $item_code, $kd_gudang){
        $act_in = $this->kartu_stok_in($startdate, $enddate, $item_code, $kd_gudang);
		$act_out = $this->kartu_stok_out($startdate, $enddate, $item_code, $kd_gudang);
		$act_adj = $this->kartu_stok_adjustment($startdate, $enddate, $item_code, $kd_gudang);
		$act_repack = $this->kartu_stok_repacking($startdate, $enddate, $item_code, $kd_gudang);
		$act_delivery = $this->kartu_stok_delivery($startdate, $enddate, $item_code, $kd_gudang);

		$merge[] = $act_in;
		$merge[] = $act_out;
		$merge[] = $act_adj;
		$merge[] = $act_repack;
		$merge[] = $act_delivery;

		$mergeAll = array();
		foreach($merge as $each_merge) {
			if(is_array($each_merge)) {
				$mergeAll = array_merge($mergeAll, $each_merge);
			}
		}
		asort($mergeAll);

		return $mergeAll;
    }

    public function kartu_stok_in ($startdate, $enddate, $barang_kd, $kd_gudang){
		$act = $this->td_finishgood_in->get_by_date_item($startdate, $enddate, $barang_kd, $kd_gudang);
		if($act->num_rows() != 0) {
			$act = $act->result_array();
			foreach ($act as $each_act){
				$aAct[] = array(
					'tgl_trans' => $each_act['fgin_tglinput'],
					'item_code' => $each_act['item_code'],
					'barang_kd' => $each_act['barang_kd'],
					'uraian' => 'Produksi '.$each_act['dn_no'],
					'barcode' => $each_act['fgbarcode_barcode'],
					'awal' => $each_act['fgin_qty_awal'],
					'masuk' => $each_act['fgin_qty'],
					'keluar' => 0,
					'sisa' => $each_act['fgin_qty_awal'] + $each_act['fgin_qty'],
				);
			}
		}else{
			$aAct=array();
		}
		krsort($aAct);

		return $aAct;
	}

    public function kartu_stok_out ($startdate, $enddate, $barang_kd, $kd_gudang){
        $act = $this->td_finishgood_out->get_by_date_item($startdate, $enddate, $barang_kd, $kd_gudang);

		if ($act->num_rows() !=0 ){
			$act = $act->result_array();
			foreach ($act as $each_act){
				$no_salesorder = '';
				if ($each_act['tipe_customer'] == 'Lokal'){
					$no_salesorder = $each_act['no_salesorder'];
				}else{
					$no_salesorder = $each_act['no_po'];
				}
				$aAct[] = array(
					'tgl_trans' => $each_act['fgout_tglinput'],
					'item_code' => $each_act['item_code'],
					'barang_kd' => $each_act['barang_kd'],
					'uraian' => $no_salesorder.'/'.$each_act['nm_customer'],
					'barcode' => $each_act['fgbarcode_barcode'],
					'awal' => $each_act['fgout_qty_awal'],
					'masuk' => 0,
					'keluar' => $each_act['fgout_qty'],
					'sisa' => $each_act['fgout_qty_awal'] - $each_act['fgout_qty'],
				);
			}
		}else{
			$aAct = array();
		}
		krsort($aAct);

		return $aAct;
    }

    public function kartu_stok_adjustment($startdate, $enddate, $barang_kd, $kd_gudang){

        $act = $this->td_finishgood_adjustment->get_by_date_item($startdate, $enddate, $barang_kd, $kd_gudang);
		
		if ($act->num_rows() != 0){
			$act = $act->result_array();
			foreach($act as $each_act){
				if ($each_act['fgadj_status'] == 'IN'){
					$uraian = 'AdjustmentMasuk-'.$each_act['fgadj_keterangan'];
					$awal = $each_act['awal_in'];
					$masuk = $each_act['fgadj_qty'];
					$keluar = 0;
					$sisa = $awal + $masuk;
					$barcode = $each_act['barcode_in'];
				}else{
					// OUT
					$uraian = 'AdjustmentKeluar-'.$each_act['fgadj_keterangan'];
					$awal = $each_act['awal_out'];
					$masuk = 0;
					$keluar = $each_act['fgadj_qty'];
					$sisa = $awal - $keluar;
					$barcode = $each_act['barcode_out'];
				}
				$aData [] = array(
					'tgl_trans' => $each_act['fgadj_tglinput'],
					'item_code' => $each_act['item_code'],
					'barang_kd' => $each_act['barang_kd'],
					'uraian' => $uraian,
					'barcode' => $barcode,
					'awal' => $awal,
					'masuk' => $masuk,
					'keluar' => $keluar,
					'sisa' => $sisa,
				);
			}
		}else{
			$aData = array();
		}
		krsort($aData);

		return $aData;

    }

    public function kartu_stok_repacking($startdate, $enddate, $barang_kd, $kd_gudang){
        $act = $this->td_finishgood_repacking->get_by_date_item($startdate, $enddate, $barang_kd, $kd_gudang);
		
		if ($act->num_rows() != 0){
			$act = $act->result_array();
			foreach($act as $each_act){
				if ($each_act['fgrepack_status'] == 'IN'){
					$uraian = 'Repacking-Barang Masuk';
					$awal = $each_act['awal_in'];
					$masuk = $each_act['fgrepack_qty'];
					$keluar = 0;
					$sisa = $awal + $masuk;
					$barcode = $each_act['barcode_in'];
				}else{
					// OUT
					$uraian = 'Repacking-Barang Keluar';
					$awal = $each_act['awal_out'];
					$masuk = 0;
					$keluar = $each_act['fgrepack_qty'];
					$sisa = $awal - $keluar;
					$barcode = $each_act['barcode_out'];
				}
				$aData [] = array(
					'tgl_trans' => $each_act['fgrepack_tglinput'],
					'item_code' => $each_act['item_code'],
					'barang_kd' => $each_act['barang_kd'],
					'uraian' => $uraian,
					'barcode' => $barcode,
					'awal' => $awal,
					'masuk' => $masuk,
					'keluar' => $keluar,
					'sisa' => $sisa,
				);
			}
		}else{
			$aData = array();
		}
		krsort($aData);

		return $aData;
    }

	public function kartu_stok_delivery($startdate, $enddate, $barang_kd, $kd_gudang){

        $act = $this->td_finishgood_delivery->get_by_date_item($startdate, $enddate, $barang_kd, $kd_gudang);
		if ($act->num_rows() != 0){
			$act = $act->result_array();
			foreach($act as $each_act){

				/** Fungsi ini untuk akomodir jika ada data awal stok in dan out kosong semua */
				if($each_act['awal_in'] == null && $each_act['awal_out'] == null){
					continue;
				}
				/** End */

				if ($each_act['fgdlv_status'] == 'IN'){
					$uraian = 'DeliveryMasuk-'.$each_act['fgdlv_keterangan'];
					$awal = $each_act['awal_in'];
					$masuk = $each_act['fgdlv_qty'];
					$keluar = 0;
					$sisa = $awal + $masuk;
					$barcode = $each_act['barcode_in'];
				}else if($each_act['fgdlv_status'] == 'OUT'){
					// OUT
					$uraian = 'DeliveryKeluar-'.$each_act['fgdlv_keterangan'];
					$awal = $each_act['awal_out'];
					$masuk = 0;
					$keluar = $each_act['fgdlv_qty'];
					$sisa = $awal - $keluar;
					$barcode = $each_act['barcode_out'];
				}
				$aData [] = array(
					'tgl_trans' => $each_act['fgdlv_tglinput'],
					'item_code' => $each_act['item_code'],
					'barang_kd' => $each_act['barang_kd'],
					'uraian' => $uraian,
					'barcode' => $barcode,
					'awal' => $awal,
					'masuk' => $masuk,
					'keluar' => $keluar,
					'sisa' => $sisa,
				);
			}
		}else{
			$aData = array();
		}
		krsort($aData);

		return $aData;

    }

}