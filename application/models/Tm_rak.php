<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_rak extends CI_Model {
	private $tbl_name = 'tm_rak';
	private $p_key = 'kd_rak';
	private $title_name = 'Data Rak';

	/* --start ssp tabel untuk modul data warehouse-- */
	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){

					return $this->tbl_btn($d, $row[2]);
				} ),
			array( 'db' => 'a.'.$this->p_key, 'dt' => 2, 'field' => $this->p_key ),
			array( 'db' => 'b.nm_gudang', 'dt' => 3, 'field' => 'nm_gudang' ),
			array( 'db' => 'a.nm_rak', 'dt' => 4, 'field' => 'nm_rak' ),
		);

		$data['sql_details'] = sql_connect();

		$data['joinQuery'] = "FROM ".$this->tbl_name." a LEFT JOIN tm_gudang b ON b.kd_gudang = a.gudang_kd";

		if (isset($_SESSION['modul_inventory']['kd_gudang'])) :
			$data['where'] = "a.gudang_kd = '".$_SESSION['modul_inventory']['kd_gudang']."'";
		else :
			$data['where'] = "";
		endif;

		return $data;
	}
	/* --end ssp tabel untuk modul data warehouse-- */

	/* --start button tabel untuk modul data warehouse-- */
	private function tbl_btn($id, $var) {
		$read_access = $this->session->read_access;
		$update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		$btns[] = get_btn(array('access' => $read_access, 'title' => 'Data Ruang', 'icon' => 'search', 'onclick' => 'data_ruang(\''.$id.'\')'));
		$btns[] = get_btn(array('access' => $update_access, 'title' => 'Ubah', 'icon' => 'pencil', 'onclick' => 'get_form(\''.$id.'\')'));
		$btns[] = get_btn_divider();
		$btns[] = get_btn(array('access' => $delete_access, 'title' => 'Hapus', 'icon' => 'trash',
			'onclick' => 'return confirm(\'Anda akan menghapus data rak = '.$var.'?\')?hapus_data(\''.$id.'\'):false'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}
	/* --end button tabel untuk modul data warehouse-- */

	public function get_row($id = '') {
		$this->db->from($this->tbl_name)
			->where(array($this->p_key => $id));
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}

	public function get_data($id = '') {
		$row = $this->get_row($id);
		if (!empty($row)) :
			$data = array('kd_rak' => $row->kd_rak, 'gudang_kd' => $row->gudang_kd, 'nm_rak' => $row->nm_rak);
		else :
			$kd_gudang = isset($_SESSION['modul_inventory']['kd_gudang'])?$_SESSION['modul_inventory']['kd_gudang']:'';
			$data = array('kd_rak' => '', 'gudang_kd' => $kd_gudang, 'nm_rak' => '');
		endif;
		return $data;
	}

	public function form_rules() {
		$rules = array(
			array('field' => 'txtNmRak', 'label' => 'Nama Rak', 'rules' => 'required'),
		);
		return $rules;
	}

	public function form_warning($datas = '') {
		$forms = array('txtNmRak');
		foreach ($datas as $key => $data) :
			$str[$data] = (!empty(form_error($forms[$key])))?buildLabel('warning', form_error($forms[$key], '"', '"')):'';
		endforeach;
		return $str;
	}

	private function create_code() {
		$this->db->select($this->p_key.' AS code')
			->from($this->tbl_name)
			->where(array('DATE(tgl_input)' => date('Y-m-d')))
			->order_by('tgl_input DESC, '.$this->p_key.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -3);
		endif;
		$angka = $urutan + 1;
		$primary = 'MRK'.date('dmy').str_pad($angka, 3, '000', STR_PAD_LEFT);
		return $primary;
	}

	public function submit_data($data = '') {
		if (!empty($data[$this->p_key])) :
			$label = 'Mengubah '.$this->title_name;
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s')));
			$where = array($this->p_key => $data[$this->p_key]);
			$act = $this->update($submit, $where);
		else :
			$label = 'Menambahkan '.$this->title_name;
			$data[$this->p_key] = $this->create_code();
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s')));
			$act = $this->create($submit);
		endif;
		$str = $this->report($act, $label, $submit);
		return $str;
	}

	private function create($data = '') {
		$act = $this->db->insert($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	private function update($data = '', $where = '') {
		$act = $this->db->update($this->tbl_name, $data, $where);
		return $act?TRUE:FALSE;
	}

	public function report($act = '', $label = '', $data = '') {
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
		endif;
		$this->m_builder->write_log($stat, $label, $data);
		return $str;
	}

	public function delete_data($id = '') {
		$act = $this->db->delete($this->tbl_name, array($this->p_key => $id));
		$report = $this->report($act, 'Menghapus '.$this->title_name, array($this->p_key => $id));
		return $report;
	}
	
	public function get_byParam ($param=[]){
		$act = $this->db->get_where($this->tbl_name, $param);
		return $act;
	}
}