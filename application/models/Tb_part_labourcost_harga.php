<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_part_labourcost_harga extends CI_Model {
	private $tbl_name = 'tb_part_labourcost_harga';
	private $p_key = 'labourcostharga_kd';

	public function ssp_table () {
        $data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'b.bagian_nama',
				'dt' => 2, 'field' => 'bagian_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.labourcostharga_satuan', 
				'dt' => 3, 'field' => 'labourcostharga_satuan',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.labourcostharga_harga', 
				'dt' => 4, 'field' => 'labourcostharga_harga',
				'formatter' => function ($d){
					$d = custom_numberformat($d, 2, 'IDR');
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.labourcostharga_tgledit', 
				'dt' => 5, 'field' => 'labourcostharga_tgledit',
				'formatter' => function ($d, $row){
					$d = $this->security->xss_clean($d);

					return $d;
                }),       
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] = "FROM ".$this->tbl_name." as a
                            LEFT JOIN tb_bagian as b ON a.bagian_kd=b.bagian_kd";
		$data['where'] = "";
		
		return $data;
	}
	
	private function tbl_btn($id) {
		$btns = array();
		if (cek_permission('MSTHARGALABOURCOST_UPDATE')) {
			$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_item(\''.$id.'\')'));
		}
		if (cek_permission('MSTHARGALABOURCOST_DELETE')) {
			$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'delete_item(\''.$id.'\')'));
		}
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
        $query = $this->db->select('MAX('.$this->p_key.') as maxID')
                ->get($this->tbl_name)
                ->row();
        $code = (int) $query->maxID + 1;
        return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }

    public function get_all () {
        return $this->db->get($this->tbl_name);
	}
	
	public function get_by_param_detail ($param = []) {
		$result = $this->db->select()
                    ->from($this->tbl_name)
                    ->join('tb_bagian', $this->tbl_name.'.bagian_kd=tb_bagian.bagian_kd', 'left')
                    ->where($param)
                    ->get();
		return $result;
	}

}