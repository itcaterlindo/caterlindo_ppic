<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_workorder_item_pyr extends CI_Model {
	private $tbl_name = 'td_workorder_item_pyr';
	private $p_key = 'woitempyr_kd';

	public function ssp_table2()
	{
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		
		$viewPermission = true;
		if($viewPermission){
			$qData = $this->db->query("SELECT td_workorder_item_pyr.woitempyr_kd, td_workorder_item_pyr.woitem_kd, td_workorder_item.woitem_itemcode, td_workorder_item.woitem_status, td_workorder_item.woitem_deskripsi, td_workorder_item.woitem_qty, td_workorder_item.woitem_satuan, td_workorder_item.woitem_note, td_workorder_item.woitem_tglselesai, td_workorder_item.woitem_prosesstatus, td_workorder_item.woitem_no_wo, tm_inspection_product.inspectionproduct_no, tm_inspection_rawmaterial.inspectionrawmaterial_no, wo_reject.woitem_no_wo AS woitem_no_wo_reject, tm_workorder.wo_noterbit
			FROM td_workorder_item_pyr
			LEFT JOIN td_workorder_item ON td_workorder_item.woitem_kd = td_workorder_item_pyr.woitem_kd
			LEFT JOIN td_workorder_item AS wo_reject ON wo_reject.woitem_kd = td_workorder_item_pyr.woitemreject_kd
			LEFT JOIN tm_inspection_product ON tm_inspection_product.kd_inspectionproduct = td_workorder_item_pyr.inspectionproduct_kd
			LEFT JOIN tm_inspection_rawmaterial ON tm_inspection_rawmaterial.kd_inspectionrawmaterial = td_workorder_item_pyr.inspectionproduct_kd
			LEFT JOIN tm_workorder ON tm_workorder.wo_kd = td_workorder_item.wo_kd
			ORDER BY td_workorder_item_pyr.woitempyr_tglinput DESC")->result_array();
		}else{
			$qData = [];
		}
		
		$data = [];
		foreach ($qData as $r) {
			/** WO PROSES LABEL */
			$woitemrd = process_status($r['woitem_prosesstatus']);
			$color = 'yellow';
			switch ($r['woitem_prosesstatus']) {
				case 'workorder':
					$color = 'yellow';
					break;
				case 'finish':
					$color = 'green';
					break;
				default:
					$color = 'red';
			}
			$woitem_prosesstatus = bg_label($woitemrd, $color);
			/** END WO PROSES LABEL */

			/** NOMOR REJECT INSPECTION RAWMATERIAL DISPLAY */
			if( !empty($r['inspectionproduct_no']) ){
				$no_inspection_reject = $r['woitem_no_wo_reject']." | ".$r['inspectionproduct_no'];
			}elseif( !empty($r['inspectionrawmaterial_no']) ){
				$no_inspection_reject = $r['woitem_no_wo_reject']." | ".$r['inspectionrawmaterial_no'];
			}else{
				$no_inspection_reject = $r['woitem_no_wo_reject'];
			}

			$data[] = [
				'1' => $r['woitem_prosesstatus'] == 'workorder' ? $this->tbl_btn($r['woitem_kd']) : '',
				'2' => $r['woitem_itemcode'],
				'3' => $r['woitem_status'],
				'4' => $r['woitem_deskripsi'],
				'5' => $r['woitem_qty'],
				'6' => $r['woitem_satuan'],
				'7' => $r['woitem_note'],
				'8' => $r['woitem_tglselesai'],
				'9' => $woitem_prosesstatus,
				'10' => $r['woitem_no_wo'], 
				'11' => $no_inspection_reject,
				'12' => $r['wo_noterbit']
			];
		}

		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

	/* --start button tabel untuk modul data inspection rawmaterial-- */
	private function tbl_btn($id) {
		$btns[] = get_btn(array('access' => true, 'title' => 'Cancel WO', 'icon' => 'close', 'onclick' => 'cancel_wo(\''.$id.'\')'));
		$btn_group = group_btns($btns);
		return $btn_group;
	}
	/* --end button tabel untuk modul data inspection-- */

	public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }

    public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

    public function get_where_in ($where, $in = []){
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}

	public function insert_batch_data($data) {
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}

	public function create_code () {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

	public function update_where_in ($param, $arrWhere_in = [], $data= []) {
		$act = $this->db->where_in($param, $arrWhere_in)
				->update($this->tbl_name, $data);
		return $act;
	}

	public function get_woreject_bywoitem($woitem_kd)
	{
		$this->load->model(['td_workorder_item_detail']);

		$qResult = $this->db->join('td_workorder_item', "td_workorder_item.woitem_kd={$this->tbl_name}.woitem_kd")
			->join('td_workorder_item_detail', "td_workorder_item_detail.woitem_kd=td_workorder_item.woitem_kd" ,'left')
			->where("{$this->tbl_name}.woitemreject_kd", $woitem_kd)
			->get($this->tbl_name)->result_array();
		$woitemdetail_kds = array_column($qResult, 'woitemdetail_kd');
		$qChild = [];
		if (!empty($woitemdetail_kds)){
			$qChild = $this->td_workorder_item_detail->get_by_param_detail_in('td_workorder_item_detail.woitemdetail_parent', $woitemdetail_kds)->result_array();
		}

		$resp['woitem'] = $qResult;
		$resp['child'] = $qChild;
		return $resp;
	}

}