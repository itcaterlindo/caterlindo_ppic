<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_barang_harga extends CI_Model {
	private $tbl_name = 'td_barang_harga';
	private $p_key = 'kd_harga_barang';
	private $title_name = 'Data Harga Barang';

	public function create_code($tambah = '1') {
		$this->db->select($this->p_key.' AS p_key')
			->from($this->tbl_name)
			->where(array('DATE(tgl_input)' => date('Y-m-d')))
			->order_by('tgl_input DESC, '.$this->p_key.' DESC');
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			$primary_key = $row->p_key;
			$urutan = substr($primary_key, -3);
		else :
			$urutan = 0;
		endif;
		$angka = $urutan + $tambah;
		$code = 'PPR'.date('dmy').str_pad($angka, 3, '000', STR_PAD_LEFT);
		return $code;
	}

	public function submit_batch($data = '') {
		$jml_data = count($data);
		$no = 0;
		for ($i = 0; $i < $jml_data; $i++) :
			$no++;
			$barang_kd = $data[$i]['kd_barang'];
			$keys[] = array('harga_barang_kd' => $this->create_code($no), 'kd_barang' => $barang_kd, 'admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s'));
			$batch_data[] = array($this->p_key => $this->create_code($no), 'barang_kd' => $barang_kd, 'harga_lokal_retail' => $data[$i]['harga_lokal_retail'], 'harga_lokal_reseller' => $data[$i]['harga_lokal_reseller'], 'harga_lokal_distributor' => $data[$i]['harga_lokal_distributor'], 'harga_ekspor' => $data[$i]['harga_ekspor'], 'admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s'));
		endfor;
		if (isset($batch_data)) :
			$act = $this->create_batch($batch_data);
			$str = $this->report_batch($act, 'Menambahkan '.$this->title_name, $batch_data);
			if ($act) :
				return $keys;
			else :
				return 'error';
			endif;
		endif;
	}

	public function create_batch($data = '') {
		$act = $this->db->insert_batch($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	public function report($act = '', $label = '', $data = '') {
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
		endif;
		$this->m_builder->write_log($stat, $label, $data);
		return $str;
	}

	public function report_batch($act = '', $label = '', $data = '') {
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
		endif;
		$this->m_builder->write_batch_log($stat, $label, $data);
		return $str;
	}
}