<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_relasi_popr extends CI_Model {
	private $tbl_name = 'tb_relasi_popr';
	private $p_key = 'relasipopr_kd';

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }
    
    public function insert_batch_data ($data=[]){
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
    
    }

    public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

    public function clear_insert_batch_data ($po_kd, $data=[]){
        $this->delete_by_param(array('po_kd' => $po_kd));
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
    }
    
    public function get_by_param_detail ($param=[]) {
        $act = $this->db->select($this->tbl_name.'.*, tm_purchaseorder.*, tm_purchaserequisition.*, tb_purchaseorder_status.postatus_nama, tb_purchaseorder_status.postatus_label, tb_purchaserequisition_status.prstatus_nama, tb_purchaserequisition_status.prstatus_label')
                    ->join('tm_purchaseorder', $this->tbl_name.'.po_kd=tm_purchaseorder.po_kd', 'left')
                    ->join('tm_purchaserequisition', $this->tbl_name.'.pr_kd=tm_purchaserequisition.pr_kd', 'left')
                    ->join('tb_purchaseorder_status', 'tm_purchaseorder.postatus_kd=tb_purchaseorder_status.postatus_kd', 'left')
                    ->join('tb_purchaserequisition_status', 'tm_purchaserequisition.prstatus_kd=tb_purchaserequisition_status.prstatus_kd', 'left')
                    ->from($this->tbl_name)
                    ->where($param)
		            ->get();
		return $act;
    }
    
    public function get_where_in ($where, $in = []){
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}

}