<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_product_grouping extends CI_Model {
	private $tbl_name = 'td_product_grouping';
	private $p_key = 'kd_dgrouping';

	public function get_item($mgrouping_kd = '') {
		$this->db->select('a.mgrouping_kd, a.barang_kd, b.item_barcode, b.deskripsi_barang, b.dimensi_barang, b.item_code, c.nm_group')
			->from($this->tbl_name.' a')
			->join('tm_barang b', 'a.barang_kd = b.kd_barang', 'left')
			->join('tm_group_barang c', 'c.kd_group_barang = b.group_barang_kd', 'left');
		if (is_array($mgrouping_kd)) :
			$this->db->where_in('mgrouping_kd', $mgrouping_kd);
		else :
			$this->db->where(array('mgrouping_kd' => $mgrouping_kd));
		endif;
		$this->db->where(array('status' => 'body'));
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

}