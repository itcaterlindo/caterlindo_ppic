<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_inspectionrawmaterial_log extends CI_Model {
	private $tbl_name = 'tb_inspectionrawmaterial_log';
	private $p_key = 'inspectionrawmateriallog_kd';

	public function create_code() {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function delete_data_by_param($arr) {
		$query = $this->db->delete($this->tbl_name, $arr); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
    }

    public function get_all () {
        return $this->db->get($this->tbl_name);
	}

	public function get_byparam_detail ($param = []) {
		$result = $this->db->select()
					->from($this->tbl_name)
					->join('tb_inspectionrawmaterial_state', $this->tbl_name.'.inspectionrawmateriallogstate_kd=tb_inspectionrawmaterial_state.inspectionrawmaterialstate_kd', 'left')
					->join('tb_admin', $this->tbl_name.'.admin_kd=tb_admin.kd_admin', 'left')
					->where($param)
					->order_by($this->tbl_name.'.inspectionrawmateriallog_tglinput', 'desc')
					->get();
		return $result;
	}
	
	
}