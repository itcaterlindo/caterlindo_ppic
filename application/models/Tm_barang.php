<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_barang extends CI_Model {
	private $tbl_name = 'tm_barang';
	private $p_key = 'kd_barang';
	private $title_name = 'Data Master Barang';

	public function get_barcode($code = '') {
		$this->db->select('item_barcode');
		$this->db->from($this->tbl_name);
		$this->db->where(array('kd_barang' => $code));
		$query = $this->db->get();
		$row = $query->row();
		$item_barcode = $row->item_barcode;
		return $item_barcode;
	}

	public function submit_batch($data = '') {
		$db_caterlindo = $this->load->database('db_caterlindo', TRUE);
		if (empty($data['kd_barang'])) :
			return $this->report(0, 'Mengubah '.$this->title_name, $data);
		else :
			$label = 'Mengubah '.$this->title_name;
			$jml_data = count($data['kd_barang']);
			if ($jml_data > 0) :
				for ($i = 0; $i < $jml_data; $i++) :
					$kd_barang = $data['kd_barang'][$i];
					if ($data['form_type'] == 'product_desc') :
						$batch_data[] = array('kd_barang' => $kd_barang, 'item_barcode' => $data['item_barcode'][$kd_barang], 'hs_code' => $data['hs_code'][$kd_barang], 'deskripsi_barang' => $data['deskripsi_barang'][$kd_barang], 'dimensi_barang' => $data['dimensi_barang'][$kd_barang], 'admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s'));
						$cat_batch_data[] = array('fc_barcode' => $data['item_barcode'][$kd_barang], 'fv_namastock' => $data['deskripsi_barang'][$kd_barang].' '.$data['dimensi_barang'][$kd_barang], 'fd_update' => date('Y-m-d H:i:s'), 'fc_userinput' => $this->session->userdata('username'));
					elseif ($data['form_type'] == 'product_dimension') :
						$batch_data[] = array('kd_barang' => $kd_barang, 'grossweight' => $data['grossweight'][$kd_barang], 'boxweight' => $data['boxweight'][$kd_barang], 'netweight' => $data['netweight'][$kd_barang], 'length_cm' => $data['length_cm'][$kd_barang], 'width_cm' => $data['width_cm'][$kd_barang], 'height_cm' => $data['height_cm'][$kd_barang], 'item_cbm' => $data['item_cbm'][$kd_barang], 'admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s'));
					elseif ($data['form_type'] == 'product_price') :
						$batch_data[] = array('kd_barang' => $kd_barang, 'harga_lokal_retail' => $data['harga_lokal_retail'][$kd_barang], 'harga_lokal_reseller' => $data['harga_lokal_reseller'][$kd_barang], 'harga_lokal_distributor' => $data['harga_lokal_distributor'][$kd_barang], 'harga_ekspor' => $data['harga_ekspor'][$kd_barang]);
					endif;
				endfor;
				if ($data['form_type'] == 'product_desc' || $data['form_type'] == 'product_dimension') :
					$act = $this->update_batch($batch_data, $this->p_key);
					$str = $this->report_batch($act, $label, $batch_data);
					if (isset($cat_batch_data)) :
						$db_caterlindo->update_batch($cat_batch_data, 'fc_barcode');
					endif;
				elseif ($data['form_type'] == 'product_price') :
					/*
					** algoritma untuk insert batch product price adalah
					** ketika user submit batch data maka sistem pertama akan menginsertkan datanya
					** setelah itu sistem akan memberikan kd_harga_barang sebagai outputnya
					** yang akan digunakan untuk update harga yang digunakan oleh barang tsb.
					** jadi data2 yang sudah diolah oleh looping diatas akan dikirim kemodel td_barang_harga
					*/
					$this->load->model(array('td_barang_harga'));
					$insert_prices = $this->td_barang_harga->submit_batch($batch_data);
					if ($insert_prices != 'error') :
						$act = $this->update_batch($insert_prices, $this->p_key);
						$str = $this->report_batch($act, $label, $insert_prices);
					endif;
				endif;
				return $str;
			endif;
		endif;
	}

	public function submit_data($data = '') {
		if (!empty($data[$this->p_key])) :
			$label = 'Mengubah '.$this->title_name;
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s')));
			$where[$this->p_key] = $data[$this->p_key];
			$act = $this->update($submit, $where);
		else :
			$label = 'Menambahkan '.$this->title_name;
			$data[$this->p_key] = $this->create_code();
			$submit = array_merge($data, array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s')));
			$act = $this->create($submit);
		endif;
		$str = $this->report($act, $label, $submit);
		$str[$this->p_key] = $data[$this->p_key];
		return $str;
	}

	private function create($data = '') {
		$act = $this->db->insert($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	private function update($data = '', $where = '') {
		$act = $this->db->update($this->tbl_name, $data, $where);
		return $act?TRUE:FALSE;
	}

	private function create_batch($data = '') {
		$act = $this->db->insert_batch($this->tbl_name, $data);
		return $act?TRUE:FALSE;
	}

	private function update_batch($data = '', $where = '') {
		$act = $this->db->update_batch($this->tbl_name, $data, $where);
		return $act?TRUE:FALSE;
	}

	public function report($act = '', $label = '', $data = '') {
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
		endif;
		$this->m_builder->write_log($stat, $label, $data);
		return $str;
	}

	public function report_batch($act = '', $label = '', $data = '') {
		if ($act) :
			$stat = 'Berhasil';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', $label.'!');
		else :
			$stat = 'Gagal';
			$str['confirm'] = 'error';
			$str['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem!');
		endif;
		$this->m_builder->write_batch_log($stat, $label, $data);
		return $str;
	}

	public function get_item($params = []) {
		$this->db->from($this->tbl_name)->where($params);
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}
}