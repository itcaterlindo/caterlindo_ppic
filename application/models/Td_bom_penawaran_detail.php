<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_bom_penawaran_detail extends CI_Model {
	private $tbl_name = 'td_bom_penawaran_detail';
	private $p_key = 'bompenawarandetail_kd';

	public function ssp_table($bompenawaran_kd) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'c.partmain_nama',
				'dt' => 2, 'field' => 'partmain_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'd.partjenis_nama', 
				'dt' => 3, 'field' => 'partjenis_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'c.partmain_note', 
				'dt' => 4, 'field' => 'partmain_note',
				'formatter' => function ($d, $row){
                    $d = build_span ($row[5], $d);
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'b.part_versi', 
				'dt' => 5, 'field' => 'part_versi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'e.partstate_nama', 
				'dt' => 6, 'field' => 'partstate_nama',
				'formatter' => function ($d, $row){
                    $d = build_span($row[6], $d);
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'e.partstate_label', 
				'dt' => 7, 'field' => 'partstate_label',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'a.bompenawarandetail_tglinput', 
				'dt' => 8, 'field' => 'bompenawarandetail_tglinput',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] = "FROM ".$this->tbl_name." as a
                            LEFT JOIN td_part as b ON a.part_kd=b.part_kd
                            LEFT JOIN tm_part_main as c ON b.partmain_kd=c.partmain_kd
                            LEFT JOIN td_part_jenis as d ON c.partjenis_kd=d.partjenis_kd
                            LEFT JOIN tb_part_state as e ON b.partstate_kd=e.partstate_kd";
		$data['where'] = "a.bompenawaran_kd= '".$bompenawaran_kd."'";
		
		return $data;
	}

	private function tbl_btn($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btns = array();
		$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'delete_item_detail(\''.$id.'\')'));
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
		$this->db->select('MAX('.$this->p_key.') AS code')
			->from($this->tbl_name);
		$query = $this->db->get();
		$query = $query->row();
		$urutan = $query->code;
		$angka = $urutan + 1;
		return $angka;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

	public function get_by_param_in ($param, $params=[]) {
		$this->db->where_in($param, $params);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}
	
	public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}
    
    public function get_by_param_detail ($param = []) {
        $result = $this->db->select()
                    ->from ($this->tbl_name)
                    ->join('tm_bom_penawaran', $this->tbl_name.'.bompenawaran_kd=tm_bom_penawaran.bompenawaran_kd', 'left')
                    ->join('tm_customer', 'tm_bom_penawaran.kd_customer=tm_customer.kd_customer', 'left')
                    ->join('tb_bom_state', 'tm_bom_penawaran.bomstate_kd=tb_bom_state.bomstate_kd', 'left')
                    ->where($param)
                    ->get();
        return $result;
	}
	
	public function get_by_param_detail_part ($param = []) {
		$result = $this->db->select()
				->from ($this->tbl_name)
				->join('td_part', $this->tbl_name.'.part_kd=td_part.part_kd', 'left')
				->join('tm_part_main', 'td_part.partmain_kd=tm_part_main.partmain_kd', 'left')
				->join('tb_part_state', 'td_part.partstate_kd=tb_part_state.partstate_kd', 'left')
				->where($param)
				->get();
		return $result;
	}

}