<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_finishgood_in extends CI_Model {
	private $tbl_name = 'td_finishgood_in';
	private $p_key = 'fgin_kd';

	public function __construct() {
		parent::__construct();
		$this->load->model(['tb_finishgood_user', 'tm_rak', 'td_rak_ruang_kolom', 'tm_barang']);
		/** Cek Lokasi User */
		$userfg = $this->tb_finishgood_user->get_by_param (array('kd_tipe_admin' => $this->session->userdata('tipe_admin_kd')))->row_array();
		$this->set_gudang = $userfg['kd_gudang'];
	}

	public function ssp_table($fgin_tglinput) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'a.fgin_tglinput', 
				'dt' => 2, 'field' => 'fgin_tglinput',
				'formatter' => function ($d){
                    $d = format_date($d, 'd-m-Y H:i:s');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.item_code', 
				'dt' => 3, 'field' => 'item_code',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.fgbarcode_barcode', 
				'dt' => 4, 'field' => 'fgbarcode_barcode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'CONCAT(COALESCE(b.fgbarcode_desc,""), "/" , COALESCE(b.fgbarcode_dimensi,"") ) as concat_deskripsi', 
				'dt' => 5, 'field' => 'concat_deskripsi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.fgin_qty_awal', 
				'dt' => 6, 'field' => 'fgin_qty_awal',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.fgin_qty', 
				'dt' => 7, 'field' => 'fgin_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.fgin_qty', 
				'dt' => 8, 'field' => 'fgin_qty',
				'formatter' => function ($d){
					$d = !empty($d) ? $d:0;
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM ".$this->tbl_name." as a 
								LEFT JOIN td_finishgood_barcode as b ON a.fgbarcode_kd = b.fgbarcode_kd
								LEFT JOIN tm_barang as c ON b.barang_kd=c.kd_barang
								LEFT JOIN tm_finishgood as d ON a.fg_kd=d.fg_kd";
		$data['where'] = "DATE(a.fgin_tglinput) = '".$fgin_tglinput."' AND a.fgadj_kd IS NULL AND a.fgrepack_kd IS NULL
							AND d.kd_gudang = '".$this->set_gudang."' ";			
		
		return $data;
	}

	private function tbl_btn($id) {
		$delete_access = $this->session->delete_access;
		$btns = array();
		if($delete_access == 1 ){
			$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));
		}		
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function ssp_table_barang_rak($jnsFilter, $detilVal){
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn_rak($d);
				}),

			array( 'db' => 'a.fgin_tglinput', 
				'dt' => 2, 'field' => 'fgin_tglinput',
				'formatter' => function ($d){
					$d = format_date($d, 'd-m-Y H:i:s');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.item_code', 
				'dt' => 3, 'field' => 'item_code',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.fgbarcode_barcode', 
				'dt' => 4, 'field' => 'fgbarcode_barcode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.fgbarcode_desc', 
				'dt' => 5, 'field' => 'fgbarcode_desc',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.fgin_qty', 
				'dt' => 6, 'field' => 'fgin_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.rakruangkolom_kd', 
				'dt' => 7, 'field' => 'rakruangkolom_kd',
				'formatter' => function ($d){
					$d = $this->get_lokasi($d);
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
			INNER JOIN td_finishgood_barcode AS b ON a.fgbarcode_kd=b.fgbarcode_kd
			LEFT JOIN tm_finishgood AS c ON a.fg_kd=c.fg_kd
			LEFT JOIN td_finishgood_out AS d ON a.fgin_kd=d.fgin_kd";

		/** Jika admin bisa melihat semua; jika user lain cek dulu lokasi setting usernya dimana */
		if ( $this->session->userdata('tipe_admin_kd') == 'TPA190617001' ){
			// administrator
			$data['where'] = "";
		}else{
			$data['where'] = "c.kd_gudang= '".$this->set_gudang."' AND ";
		}

		switch ($jnsFilter) {
			case '1':
				$data['where'] .= "d.fgin_kd IS NULL";
				break;
			case '2':
				$data['where'] .= "a.rakruangkolom_kd IS NULL AND d.fgin_kd IS NULL";
				break;
			case '3':
				$data['where'] .= "b.fgbarcode_barcode = '".$detilVal['barcode']."' AND d.fgin_kd IS NULL";
				break;
			case '4':
				$data['where'] .= "c.barang_kd = '".$detilVal['barang_kd']."' AND d.fgin_kd IS NULL";
				break;
			case '5':
				$data['where'] .= "a.rakruangkolom_kd = '".$detilVal['ruangKolom']."'  AND d.fgin_kd IS NULL";
				break;
		}
		
		return $data;
	}

	private function tbl_btn_rak($id) {
		$btns = array();
		$btns[] = get_btn(array('title' => 'Edit Lokasi', 'icon' => 'pencil', 'onclick' => 'edit_lokasi(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Cetak Barcode', 'icon' => 'print', 'onclick' => 'cetak_barcode(\''.$id.'\')'));
		
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function get_lokasi($rakruangkolom_kd){
		if (!empty($rakruangkolom_kd)){
			$act = $this->td_rak_ruang_kolom->get_detail_by_param (array('td_rak_ruang_kolom.rakruangkolom_kd'=> $rakruangkolom_kd))->row();
			if($act){
				$lokasi = $act->nm_gudang.'/'.$act->nm_rak.'/'.$act->nm_rak_ruang.'/'.$act->rakruangkolom_nama;
			}elseif($rakruangkolom_kd == '-'){
				$lokasi = 'Kosong';
			}else{
				$lokasi = 'Tidak ditemukan';
			}
		}else{
			$lokasi = 'Kosong';
		}
		return $lokasi;
	}


    public function buat_kode () {
		// FIN190507000001
		$ident = 'FIN';
		$identtgl = substr(date('Y'), -2).date('m').date('d');

		$this->db->select('MAX('.$this->p_key.') as maxCode');
		$this->db->where(['DATE(fgin_tglinput)' => date('Y-m-d')]);
		$query = $this->db->get($this->tbl_name);
		if ($query->num_rows() == 0){
			$data = $ident.$identtgl.'000001';
		}else {
			$lastkode = $query->row_array();
			$lastkode = $lastkode['maxCode'];
			$subs_laskode = substr($lastkode, -6);
			$nextnumber = (int) $subs_laskode + 1;
			$data = $ident.$identtgl.str_pad($nextnumber,6,"0",STR_PAD_LEFT);
		}
		return $data;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_byId($id){
		$act = $this->db->select('a.*, b.*, c.*')
							->from($this->tbl_name.' as a')
							->join('td_finishgood_barcode as b', 'a.fgbarcode_kd=b.fgbarcode_kd')
							->join('tm_finishgood as c', 'b.fg_kd=c.fg_kd', 'left')
							->where('a.'.$this->p_key, $id)
							->get();
		return $act;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

	public function update_data_batch($key, $dataArray=[]){
		$query = $this->db->update_batch($this->tbl_name, $dataArray, $key);
		return $query?TRUE:FALSE;
	}

	public function get_byBarcode($id){
		$act = $this->db->get_where($this->tbl_name, array('barcodefg_barcode'=>$id));
		return $act;
	}

	public function get_barcode_by_param($param=[]){
		$act = $this->db->select($this->tbl_name.'.*, td_finishgood_barcode.*')
						->from($this->tbl_name)
						->join('td_finishgood_barcode', 'td_finishgood_in.fgbarcode_kd=td_finishgood_barcode.fgbarcode_kd', 'left')
						// ->join('tm_finishgood', 'td_finishgood_barcode.fg_kd=tm_finishgood.fg_kd', 'left')
						->where($param)
						->get();
		return $act;
	}

	/** get id dari barcode dengan catatan barang belum keluar */
	// public function get_id_by_barcode($barcode){
	// 	$act = $this->db->select($this->tbl_name.'.'.$this->p_key.', td_finishgood_barcode.barang_kd, td_finishgood_in.rakruangkolom_kd, td_finishgood_in.fgin_qty, td_finishgood_in.fg_kd')
	// 					->from($this->tbl_name)
	// 					->join('td_finishgood_barcode', 'td_finishgood_in.fgbarcode_kd=td_finishgood_barcode.fgbarcode_kd', 'left')
	// 					->join('td_finishgood_out', 'td_finishgood_in.fgin_kd=td_finishgood_out.fgin_kd', 'left')
	// 					->where('td_finishgood_barcode.fgbarcode_barcode', $barcode)
	// 					->where('td_finishgood_out.fgin_kd IS NULL')
	// 					->get();
	// 	return $act;	
	// }
	public function get_id_by_barcode($barcode){
		$act = $this->db->select($this->tbl_name.'.'.$this->p_key.', td_finishgood_barcode.barang_kd, td_finishgood_in.rakruangkolom_kd, td_finishgood_in.fgin_qty, td_finishgood_in.fg_kd')
						->from('td_finishgood_barcode')
						->join($this->tbl_name, 'td_finishgood_in.fgbarcode_kd=td_finishgood_barcode.fgbarcode_kd', 'left')
						->join('td_finishgood_out', 'td_finishgood_in.fgin_kd=td_finishgood_out.fgin_kd', 'left')
						->where('td_finishgood_barcode.fgbarcode_barcode', $barcode)
						->where('td_finishgood_out.fgin_kd IS NULL')
						->get();
		return $act;	
	}

	/** untuk cek id terakhir transaksi antara fgin dan fgout */
	public function get_kd_max_trans($fg_kd){
		$actCekIn = $this->db->select('a.fgin_kd, a.fgin_tglinput')
						->from('td_finishgood_in AS a')
						// ->join('td_finishgood_barcode AS b', 'a.fgbarcode_kd=b.fgbarcode_kd', 'left')
						->where('a.fg_kd', $fg_kd)
						->order_by('a.fgin_tglinput', 'desc')
						->limit(1)
						->get()->row_array();
		$fgin_tglinput = $actCekIn['fgin_tglinput'];
		$actCekOut = $this->db->select('a.fgin_kd, a.fgout_kd, a.fgout_tglinput')
						->from('td_finishgood_out AS a')
						->join('td_finishgood_in AS b', 'a.fgin_kd=b.fgin_kd', 'left')
						// ->join('td_finishgood_barcode AS c', 'b.fgbarcode_kd=c.fgbarcode_kd', 'left')
						->where('b.fg_kd', $fg_kd)
						->order_by('a.fgout_tglinput', 'desc')
						->limit(1)
						->get()->row_array();
		$fgout_tglinput = $actCekOut['fgout_tglinput'];
		if ($fgin_tglinput > $fgout_tglinput){
			/** kd fgin yg  terakhir */
			$kdMaxTrans = $actCekIn['fgin_kd'];
		}else{
			$kdMaxTrans = $actCekOut['fgout_kd'];
		}

		return $kdMaxTrans;
	}

	public function get_lokasi_by_id($id){
		$act = $this->db->select($this->tbl_name.'.*, td_finishgood_barcode.*, tm_barang.item_code, td_rak_ruang_kolom.*')
						->from($this->tbl_name)
						->join('td_finishgood_barcode', 'td_finishgood_in.fgbarcode_kd=td_finishgood_barcode.fgbarcode_kd', 'left')
						->join('tm_barang', 'td_finishgood_barcode.barang_kd=tm_barang.kd_barang', 'left')
						->join('td_rak_ruang_kolom', $this->tbl_name.'.rakruangkolom_kd=td_rak_ruang_kolom.rakruangkolom_kd', 'left')
						->where($this->p_key, $id)
						->get();
		return $act;
	}

	public function get_by_date_item($startdate, $enddate, $barang_kd, $kd_gudang) {
		$barang = $this->tm_barang->get_item(['kd_barang' => $barang_kd]);
		$itembarcode_custom = '899000';
		$this->db->select('a.*, b.fgbarcode_barcode, c.item_barcode, c.item_code, b.fgbarcode_batch, c.barang_kd, b.fgbarcode_kd, e.dn_no')
						->from('td_finishgood_in AS a')
						->join('td_finishgood_barcode AS b', 'a.fgbarcode_kd=b.fgbarcode_kd', 'left')
						->join('tm_finishgood AS c', 'a.fg_kd=c.fg_kd', 'left')
						->join('td_deliverynote_received AS d', 'd.dndetailreceived_kd=b.dndetailreceived_kd', 'left')
						->join('tm_deliverynote AS e', 'd.dn_kd=e.dn_kd', 'left')
						->join('tm_barang AS f', 'f.kd_barang=c.barang_kd', 'left');
		/** Jika barang PJ (item group custom) tampilkan semua item code yang itemgroupnya custom */
		if($barang->item_group_kd == '2'){
			$this->db->where('c.item_barcode', $itembarcode_custom);
		}else{
			$this->db->where('c.barang_kd', $barang_kd);
		}
		$this->db->where('DATE(a.fgin_tglinput) >=', $startdate)
						->where('DATE(a.fgin_tglinput) <=', $enddate)
						->where('c.kd_gudang', $kd_gudang)
						->where('a.fgadj_kd IS NULL')
						->where('a.fgrepack_kd IS NULL')
						->where('a.fgdlv_kd IS NULL')
						->order_by('a.fgin_tglinput', 'desc');
		$act = $this->db->get();
		return $act;
	}

	// detail fgin belum out
	public function get_fgin_blm_out ($kd_gudang){
		$act = $this->db->select('a.*, c.fgbarcode_barcode, c.fgbarcode_desc, c.fgbarcode_dimensi, d.item_code')
						->from($this->tbl_name.' as a')
						->join('td_finishgood_out as b', 'a.fgin_kd=b.fgin_kd', 'left')
						->join('td_finishgood_barcode as c', 'a.fgbarcode_kd=c.fgbarcode_kd', 'left')
						->join('tm_finishgood as d', 'a.fg_kd=d.fg_kd', 'left')
						->where('d.kd_gudang', $kd_gudang)
						->where('b.fgin_kd IS NULL')
						->where('a.rakruangkolom_kd IS NULL')
						->get();
		return $act;						
	}

	// rekap jml blm out
	public function get_rekap_fgin_blm_out($kd_gudang){
		$act = $this->db->select('a.fg_kd, SUM(a.fgin_qty) as qtyStock')
						->from($this->tbl_name.' as a')
						->join('td_finishgood_out as b', 'a.fgin_kd=b.fgin_kd', 'left')
						->join('tm_finishgood as c', 'a.fg_kd=c.fg_kd', 'left')
						->where('c.kd_gudang', $kd_gudang)
						->where('b.fgin_kd IS NULL')
						->group_by('c.fg_kd')
						->get();
		return $act;
	}

}