<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class M_salesperson extends CI_Model {
	private $tbl_name = 'tb_salesperson';

	// Fungsi untuk cek apakah kd_karyawan tsb ada pada tb_salesperson
	function check_sales($kd_karyawan) {
		$this->db->select('kd_salesperson');
		$this->db->from($this->tbl_name);
		$this->db->where(array('karyawan_kd' => $kd_karyawan));
		$query = $this->db->get();
		$row = $query->row();
		if (!empty($row)) :
			$row_kd_sales = $row->kd_salesperson;
		else :
			$row_kd_sales = '';
		endif;
		return $row_kd_sales;
	}

	function get_row($kd_salesperson) {
		$this->db->from($this->tbl_name)
			->where(array('kd_salesperson' => $kd_salesperson));
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}

	function get_detail($kd_salesperson) {
		$this->db->select('nm_salesperson, karyawan_kd');
		$this->db->from($this->tbl_name);
		$this->db->where(array('kd_salesperson' => $kd_salesperson));
		$query = $this->db->get();
		$num = $query->num_rows();
		$row = $query->row();
		return $num > 0?$row:FALSE;
	}

	function get_all() {
		$this->db->from($this->tbl_name);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	function generate_dropdown() {
		$result = $this->get_all();
		$data = array('' => '-- Pilih Salesperson --', 'semua' => 'Pilih Semua');
		if (!empty($result)) :
			foreach ($result as $row) :
				$data[$row->kd_salesperson] = $row->nm_salesperson;
			endforeach;
		endif;
		return $data;
	}
}