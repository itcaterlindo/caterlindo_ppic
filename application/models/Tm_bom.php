<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_bom extends CI_Model {
	private $tbl_name = 'tm_bom';
	private $p_key = 'bom_kd';

	public function ssp_table_std() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 2, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->btn_sap($d);
				}),
			array( 'db' => 'ifnull(b.item_code, c.rm_kode)', 
				'dt' => 3, 'field' => 'ifnull(b.item_code, c.rm_kode)',
				'formatter' => function ($d, $row){
					if(empty($d)){
						$d = $row[7];
					}
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'ifnull(b.deskripsi_barang,c.rm_deskripsi)', 
				'dt' => 4, 'field' => 'ifnull(b.deskripsi_barang,c.rm_deskripsi)',
				'formatter' => function ($d, $row){
					if(empty($d)){
						$d = $row[8];
					}
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'ifnull(b.dimensi_barang,c.rm_spesifikasi)', 
				'dt' => 5, 'field' => 'ifnull(b.dimensi_barang,c.rm_spesifikasi)',
				'formatter' => function ($d, $row){
					if(empty($d)){
						$d = $row[9];
					}
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'a.bom_ket', 
				'dt' => 6, 'field' => 'bom_ket',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.bom_tgledit', 
				'dt' => 7, 'field' => 'bom_tgledit',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.rm_kode', 
				'dt' => 8, 'field' => 'rm_kode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.rm_deskripsi', 
				'dt' => 9, 'field' => 'rm_deskripsi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.rm_spesifikasi', 
				'dt' => 10, 'field' => 'rm_spesifikasi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] = "FROM ".$this->tbl_name." as a
							LEFT JOIN tm_barang as b ON a.kd_barang=b.kd_barang
							LEFT JOIN tm_rawmaterial as c ON a.kd_barang=c.rm_kd";
		$data['where'] = "a.bom_jenis = 'std'";
		
		return $data;
	}

	public function ssp_table_pj() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn_pj($d);
				}),
				array( 'db' => 'a.'.$this->p_key,
				'dt' => 2, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->btn_sap($d);
				}),
			array( 'db' => 'b.project_itemcode', 
				'dt' => 3, 'field' => 'project_itemcode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.no_salesorder', 
				'dt' => 4, 'field' => 'no_salesorder',
				'formatter' => function ($d, $row){
					$no_salesorder = $this->generate_so ($row[8], $d, $row[9], $row[10]);
					return $no_salesorder;
				}),
			array( 'db' => 'b.project_nopj', 
				'dt' => 5, 'field' => 'project_nopj',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.project_itemdesc', 
				'dt' => 6, 'field' => 'project_itemdesc',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'b.project_itemdimension', 
				'dt' => 7, 'field' => 'project_itemdimension',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'a.bom_ket', 
				'dt' => 8, 'field' => 'bom_ket',
				'formatter' => function ($d, $row){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.bom_tgledit', 
				'dt' => 9, 'field' => 'bom_tgledit',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'c.tipe_customer', 
				'dt' => 10, 'field' => 'tipe_customer',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'c.no_po', 
				'dt' => 11, 'field' => 'no_po',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'd.nm_customer', 
				'dt' => 12, 'field' => 'nm_customer',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] = "FROM ".$this->tbl_name." as a
							LEFT JOIN tm_project as b ON a.project_kd=b.project_kd
							LEFT JOIN tm_salesorder as c ON b.kd_msalesorder=c.kd_msalesorder
							LEFT JOIN tm_customer as d ON d.kd_customer=c.customer_kd";
		$data['where'] = "a.bom_jenis = 'custom'";
		
		return $data;
	}



	public function ssp_report_bwp($p) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'tmr.rm_kode',
			'dt' => 1, 'field' => 'rm_kode',
			'formatter' => function($d){
				$d = $this->security->xss_clean($d);

				return $d;
			}),
			array( 'db' => 'tmr.rm_nama',
				'dt' => 2, 'field' => 'rm_nama',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'tmp.partmain_nama',
				'dt' => 3, 'field' => 'partmain_nama',
				'formatter' => function($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'tmb.item_code', 
				'dt' => 4, 'field' => 'item_code',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'tmb.deskripsi_barang', 
				'dt' => 5, 'field' => 'deskripsi_barang',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'tmb.dimensi_barang', 
				'dt' => 6, 'field' => 'dimensi_barang',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] = "FROM tm_barang AS tmb 
								LEFT JOIN tm_bom AS tmbo ON tmbo.kd_barang = tmb.kd_barang
								LEFT JOIN td_bom_detail AS tdbo ON tdbo.bom_kd = tmbo.bom_kd
								LEFT JOIN tm_part_main AS tmp ON tdbo.partmain_kd = tmp.partmain_kd
												LEFT JOIN tb_part_lastversion AS tplv ON tmp.partmain_kd = tplv.partmain_kd
												LEFT JOIN td_part AS tdp ON tdp.part_kd = tplv.part_kd
												LEFT JOIN td_part_detail AS tdpd ON tdp.part_kd = tdpd.part_kd
												LEFT JOIN tm_rawmaterial AS tmr ON tmr.rm_kd = tdpd.rm_kd WHERE tmr.rm_kd = '$p'";
		$data['where'] = "";
		
		return $data;
	}




	private function tbl_btn($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btns = array();
		$btns[] = get_btn(array('title' => 'Lihat BoM', 'icon' => 'search', 'onclick' => 'detail_data(\''.$id.'\')'));
		if (cek_permission('BOMSTD_FILES_VIEW')) {
			$btns[] = get_btn(array('title' => 'Lihat Drawing', 'icon' => 'search', 'onclick' => 'view_files(\''.url_encrypt($id).'\')'));
		}
		if (cek_permission('BOMSTD_COSTING')) { 
			$btns[] = get_btn(array('title' => 'BoM Costing', 'icon' => 'dollar', 'onclick' => 'bomstdcosting_box(\''.$id.'\')'));
			$btns[] = get_btn(array('title' => 'Print BoM Costing', 'icon' => 'print', 'onclick' => 'bomstdcosting_print(\''.$id.'\')'));
		}
		if (cek_permission('BOMSTD_UPDATE')) { 
			$btns[] = get_btn(array('title' => 'Edit BoM', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
		}
		if (cek_permission('BOMSTD_DUPLICATE')) { 
			$btns[] = get_btn(array('title' => 'Duplicate BoM', 'icon' => 'clone', 'onclick' => 'duplicate_data(\''.$id.'\')'));
		}
		$btns[] = get_btn_divider();
		if (cek_permission('BOMSTD_DELETE')) { 
			$btns[] = get_btn(array('title' => 'Hapus BoM', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));	
		}
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	private function btn_sap($id) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btn_group = '';

		$this->db->where(['bom_kd' => $id]);
		$act = $this->db->get($this->tbl_name)->row_array();

		if (cek_permission('BOM_PUSH_SAP')) {
			// if($act['status_sap'] == '0'){
					$btn_group ='<button type="button" onclick="push_to_sap(\''.$id.'\')" class="btn btn-p"><i class="fa fa-arrow-up"></i>&nbsp;Push To SAP</button>';
				// }else{
				// 	$btn_group ='<button type="button" onclick="push_to_sap(\''.$id.'\')" class="btn btn-success" disabled><i class="fa fa-check"></i>&nbsp;Done To SAP</button>';
				// }
		}

		//$btns = get_btn(array('title' => 'Push SAP', 'icon' => ' fa-arrow-up', 'onclick' => 'detail_data(\''.$id.'\')'));
	
		return $btn_group;
	}

	private function tbl_btn_pj($id) {
		$btns = array();
		$btns[] = get_btn(array('title' => 'Lihat BoM', 'icon' => 'search', 'onclick' => 'detail_data(\''.$id.'\')'));
		if (cek_permission('BOMPENAWARAN_VIEW_HARGACOSTING')) { 
			$btns[] = get_btn(array('title' => 'Print BoM Costing', 'icon' => 'print', 'onclick' => 'bomcustomcosting_print(\''.$id.'\')'));
		}
		if (cek_permission('BOMCUSTOM_FILES_VIEW')) {
			$btns[] = get_btn(array('title' => 'Lihat Drawing', 'icon' => 'search', 'onclick' => 'view_files(\''.url_encrypt($id).'\')'));
		}
		if (cek_permission('BOMCUSTOM_UPDATE')) {
			$btns[] = get_btn(array('title' => 'Edit BoM', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
		}
		if (cek_permission('BOMCUSTOM_DELETE')) {
			$btns[] = get_btn_divider();
			$btns[] = get_btn(array('title' => 'Hapus BoM', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));	
		}
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	private function generate_jenis_bom ($jns) {
		$rJenis = '';
		if ($jns == 'std'){
			$rJenis = 'Standart';
		}else{
			$rJenis = 'Custom';
		}
		return $rJenis;
	}

	private function generate_so ($tipe_customer, $no_salesorder, $no_po, $nm_customer) {
		$keterangan = '';
		if (!empty($no_salesorder)) {
			if ($tipe_customer == 'Ekspor') {
				$no_salesorder = $no_po;
			}
			$keterangan = $no_salesorder.'|'.$nm_customer;
		}

		return $keterangan;
	}

	public function create_code() {
		$this->db->select('MAX('.$this->p_key.') AS code')
			->from($this->tbl_name);
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = (int) substr($code, -10);
		endif;
		$angka = $urutan + 1;
		$primary = 'MBOM'.str_pad($angka, 10, '0', STR_PAD_LEFT);
		return $primary;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}
	
	public function get_by_param_detail ($param=[]){
		$query = $this->db->select($this->tbl_name.'.*, tm_barang.*, tm_rawmaterial.*, tm_group_barang.nm_group, tm_project.project_kd, tm_project.kd_msalesorder, tm_project.project_nopj, tm_project.project_itemcode, tm_project.project_itemdesc, tm_project.project_itemdimension')
					->from($this->tbl_name)
					->join('tm_barang', 'tm_barang.kd_barang=tm_bom.kd_barang', 'left')
					->join('tm_group_barang', 'tm_barang.group_barang_kd = tm_group_barang.kd_group_barang', 'left')
					->join('tm_project', 'tm_bom.project_kd=tm_project.project_kd', 'left')
					->join('tm_rawmaterial', 'tm_rawmaterial.rm_kd=tm_bom.kd_barang', 'left')
					->where($param)
					->get();
		return $query;
	}

	public function get_by_param_in ($param, $params=[]) {
		$this->db->where_in($param, $params);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_by_param_detail_in ($column, $param=[]){
		$query = $this->db->select($this->tbl_name.'.*, tm_barang.*, tm_rawmaterial.*, tm_group_barang.nm_group, tm_project.project_kd, tm_project.*')
					->from($this->tbl_name)
					->join('tm_barang', $this->tbl_name.'.kd_barang=tm_barang.kd_barang', 'left')
					->join('tm_group_barang', 'tm_barang.group_barang_kd = tm_group_barang.kd_group_barang', 'left')
					->join('tm_project', $this->tbl_name.'.project_kd=tm_project.project_kd', 'left')
					->join('tm_rawmaterial', 'tm_rawmaterial.rm_kd=tm_bom.kd_barang', 'left')
					->where_in($column, $param)
					->get();
		return $query;
	}

	public function generate_versi ($bommain_kd) {
		$query = $this->db->select('MAX(bom_versi) as maxVersi')
					->where(['bommain_kd' => $bommain_kd])
					->get($this->tbl_name)->row();
		$versi = $query->maxVersi;
		$nextVersi = (int) $versi + 1;
		return $nextVersi;
	}

}