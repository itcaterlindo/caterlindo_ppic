<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_finishgood_barcode extends CI_Model {
	private $tbl_name = 'td_finishgood_barcode';
	private $p_key = 'fgbarcode_kd';

	public function __construct() {
		parent::__construct();

		$this->load->model(['tb_finishgood_user']);
		/** Cek Lokasi User */
		$userfg = $this->tb_finishgood_user->get_by_param (array('kd_tipe_admin' => $this->session->userdata('tipe_admin_kd')))->row_array();
		$this->set_gudang = $userfg['kd_gudang'];
	}

	public function ssp_table($tglprod) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'd.dn_no', 
				'dt' => 2, 'field' => 'dn_no',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'b.item_code', 
				'dt' => 3, 'field' => 'item_code',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.fgbarcode_batch', 
				'dt' => 4, 'field' => 'fgbarcode_batch',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.fgbarcode_barcode', 
				'dt' => 5, 'field' => 'fgbarcode_barcode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.fgbarcode_shift', 
				'dt' => 6, 'field' => 'fgbarcode_shift',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.fgbarcode_desc', 
				'dt' => 7, 'field' => 'fgbarcode_desc',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.fgbarcode_dimensi', 
				'dt' => 8, 'field' => 'fgbarcode_dimensi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.fgbarcode_tglinput', 
				'dt' => 9, 'field' => 'fgbarcode_tglinput',
				'formatter' => function ($d){
					$d = format_date($d, 'd-m-Y H:i:s');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
								LEFT JOIN tm_barang AS b ON a.barang_kd=b.kd_barang
								LEFT JOIN td_deliverynote_received as c ON a.dndetailreceived_kd=c.dndetailreceived_kd
								LEFT JOIN tm_deliverynote as d ON a.kd_dn=d.dn_kd";
		$data['where'] = "a.fgbarcode_tglbatch = '".$tglprod."'";
		
		return $data;
	}

	private function tbl_btn($id) {
		// $read_access = $this->session->read_access;
		// $update_access = $this->session->update_access;
		// $delete_access = $this->session->delete_access;
		$btns = array();
		$btns[] = get_btn(array('title' => 'Cetak Barcode', 'icon' => 'print', 'onclick' => 'cetak_barcode(\''.$id.'\')'));
		$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'del_data(\''.$id.'\')'));
		
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function urut_barcode($barang_kd, $batch, $woitem_status = ""){
		$this->db->select('SUBSTR(a.fgbarcode_barcode,8,3) as maxQty')
				->from($this->tbl_name.' as a')
				->join('tm_barang as b', 'a.barang_kd=b.kd_barang', 'left');
		if($woitem_status == "custom"){
			$this->db->where('a.fgbarcode_batch', $batch)
				->where('b.item_group_kd', '2');
		}else{
			$this->db->where('b.kd_barang', $barang_kd)
				->where('a.fgbarcode_batch', $batch);
		}
		$this->db->order_by('maxQty', 'DESC');
		$act = $this->db->get();
		if ($act->num_rows() != 0){
			$act = $act->result_array();
			$maxQty = max($act);
			$maxQty = $maxQty['maxQty'] + 1;
			$maxQty = str_pad($maxQty,3,"0",STR_PAD_LEFT);
		}else{
			$maxQty = '001';
		}
		return $maxQty;
	}

    public function buat_kode () {
		// FBC190507000001
		$ident = 'FBC';
		$identtgl = substr(date('Y'), -2).date('m').date('d');

		$this->db->select($this->p_key);
		$this->db->where('DATE(fgbarcode_tglinput)', date('Y-m-d'));
		$this->db->order_by('fgbarcode_tglinput', 'DESC');
		$query = $this->db->get($this->tbl_name);
		if ($query->num_rows() == 0){
			$data = $ident.$identtgl.'000001';
		}else {
			$lastkode = $query->result_array();
			$lastkode = max($lastkode);
			$subs_laskode = substr($lastkode[$this->p_key], -6);
			$nextnumber = $subs_laskode + 1;
			$data = $ident.$identtgl.str_pad($nextnumber,6,"0",STR_PAD_LEFT);
		}
		return $data;
	}

    public function buat_kode_urut ($fgbarcode_kd = null) {
		$ident = 'FBC';
		$identtgl = date('ymd');

		$lastkode = $ident.$identtgl.'000000';
		if (empty($fgbarcode_kd)) {
			$this->db->select($this->p_key);
			$this->db->where('DATE(fgbarcode_tglinput)', date('Y-m-d'));
			$this->db->order_by($this->p_key, 'DESC');
			$query = $this->db->get($this->tbl_name)->row_array();
			if (!empty($query)) {
				$lastkode = $query[$this->p_key];
			}
		}else{
			$lastkode = $fgbarcode_kd;
		}
		$subs_laskode = substr($lastkode, -6);
		$nextnumber = $subs_laskode + 1;
		$lastkode = $ident.$identtgl.str_pad($nextnumber,6,"0",STR_PAD_LEFT);

		return $lastkode;
	}

    public function insert_data ($data){
        $query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_id($id){
		$act = $this->db->select('a.*, b.item_code')
					->from($this->tbl_name.' AS a')
					->join('tm_barang AS b', 'a.barang_kd=b.kd_barang', 'left')
					->where($this->p_key, $id)
					->get();
		return $act;
	}

	public function select_by_barcode($id){
		$act = $this->db->select('a.*, b.*, c.fgin_qty, c.fgin_kd, c.rakruangkolom_kd, c.fg_kd')
					->from($this->tbl_name.' AS a')
					->join('tm_barang AS b', 'a.barang_kd=b.kd_barang', 'left')
					->join('td_finishgood_in as c', 'a.fgbarcode_kd=c.fgbarcode_kd', 'left')
					->join('td_finishgood_out as d', 'c.fgin_kd=d.fgin_kd', 'left')
					->where('d.fgin_kd IS NULL')
					->where('a.fgbarcode_barcode', $id)
					->get();
		return $act;
	}

	public function get_byParam ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_id_by_param ($param=[]) {
		$this->db->select('fgbarcode_kd, barang_kd');
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_by_dateBatch ($date) {
		$act = $this->db->select('a.*, b.*')
					->from($this->tbl_name.' AS a')
					->join('tm_barang AS b', 'a.barang_kd=b.kd_barang', 'left')
					->where('fgbarcode_tglbatch',$date)
					->order_by('a.fgbarcode_tglinput', 'desc')
					->get();
		return $act;
	}

	public function insert_batch($data) {
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
    
}