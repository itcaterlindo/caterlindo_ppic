<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_deliverymaterial_user extends CI_Model {
	private $tbl_name = 'tb_deliverymaterial_user';
	private $p_key = 'deliverymaterialuser_kd';

	public function ssp_table() {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'b.nm_tipe_admin', 
				'dt' => 2, 'field' => 'nm_tipe_admin',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.nm_gudang', 
				'dt' => 3, 'field' => 'nm_gudang',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.flag_terima', 
				'dt' => 4, 'field' => 'flag_terima',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d == '1' ? 'Ya' : 'Tidak';
				}),
		);

		$data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM ".$this->tbl_name." as a 
								LEFT JOIN td_admin_tipe as b ON a.kd_tipe_admin = b.kd_tipe_admin
								LEFT JOIN tm_gudang as c ON a.kd_gudang=c.kd_gudang";
		$data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id) {
		// $read_access = $this->session->read_access;
		// $update_access = $this->session->update_access;
		$delete_access = $this->session->delete_access;
		$btns = array();
		if($delete_access == 1 ){
			$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
			$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));
		}		
		$btn_group = group_btns($btns);

		return $btn_group;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	function get_detail_by_param($param=[]){
		$act = $this->db->select('tb_deliverymaterial_user.*, tm_gudang.nm_gudang, td_admin_tipe.nm_tipe_admin')
				->from($this->tbl_name)
				->join('tm_gudang', 'tb_deliverymaterial_user.kd_gudang=tm_gudang.kd_gudang', 'left')
				->join('td_admin_tipe', 'tb_deliverymaterial_user.kd_tipe_admin=td_admin_tipe.kd_tipe_admin', 'left')
				->where($param)
				->get();
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

}