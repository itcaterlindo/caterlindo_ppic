<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class M_karyawan extends CI_Model {
	private $db_name = 'sim_hrm';
	private $tbl_name = 'tb_karyawan';

	function get_contact($kd_karyawan) {
		$my_db = $this->load->database($this->db_name, TRUE);
		$my_db->select('telp_rumah, telp_mobile, telp_kantor, email_utama, email_lain');
		$my_db->from($this->tbl_name);
		$my_db->where(array('kd_karyawan' => $kd_karyawan));
		$query = $my_db->get();
		$num = $query->num_rows();
		$row = $query->row();
		return $num > 0?$row:'';
	}
}