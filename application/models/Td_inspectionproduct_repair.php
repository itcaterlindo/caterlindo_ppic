<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_inspectionproduct_repair extends CI_Model {
	private $tbl_name = 'td_inspectionproduct_repair';
    
	public function get_all(){
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_by_param ($param=[]) {
		$this->db->join('tb_bagian', 'tb_bagian.bagian_kd='.$this->tbl_name.'.bagian_kd', 'left');
		$this->db->join('tb_admin', 'tb_admin.kd_admin='.$this->tbl_name.'.approved_admin_kd', 'left');
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
    }

	public function get_by_param_in ($key, $param=[]) {
		$this->db->where_in($key, $param);
		$act = $this->db->get($this->tbl_name);
		return $act;
    }
    
    public function delete_by_param($param=[]) {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
    }
    
    public function insert_batch_data($data) {
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query?TRUE:FALSE;
    }

	public function update_data($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}
}