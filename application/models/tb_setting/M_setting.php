<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_setting extends CI_Model {
	public function getSetting($val){
		$this->db->where('nm_setting', $val);
		$this->db->from('tb_setting');
		$q_setting = $this->db->get();
		$j_setting = $q_setting->num_rows();
		$r_setting = $q_setting->row();

		return ($j_setting > 0)?$r_setting->val_setting:'Tidak ada data!';
	}

	function delUnusedImg($path_to_img, $img_tbl, $img_col) {
		$db_hrm = $this->load->database('sim_hrm', TRUE);
		$img_on_dir = glob(FCPATH . $path_to_img . '*.*');
		$path = strlen(FCPATH . $path_to_img);

		$this->db->trans_start();
		foreach ($img_on_dir as $img) :
			$foto = substr($img, $path);
			
			$db_hrm->from($img_tbl);
			$db_hrm->select($img_col);
			$db_hrm->where($img_col, $foto);
			$q_img = $db_hrm->get();
			$j_img = $q_img->num_rows();

			if ($j_img < 1) :
				unlink($img);
			endif;
		endforeach;
		$this->db->trans_complete();
	}

	function delUnusedLocalImg($path_to_img, $img_tbl, $img_col) {
		$img_on_dir = glob(FCPATH . $path_to_img . '*.*');
		$path = strlen(FCPATH . $path_to_img);

		$this->db->trans_start();
		foreach ($img_on_dir as $img) :
			$foto = substr($img, $path);
			
			$this->db->from($img_tbl);
			$this->db->select($img_col);
			$this->db->where($img_col, $foto);
			$q_img = $this->db->get();
			$j_img = $q_img->num_rows();

			if ($j_img < 1) :
				unlink($img);
			endif;
		endforeach;
		$this->db->trans_complete();
	}
}