<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class M_msalesorder extends CI_Model {
	private $tbl_name = 'tm_salesorder';

	public function read_masterdata($kd_msalesorder) {
		$this->db->select('kd_msalesorder, mquotation_kd, salesperson_kd, email_sales, mobile_sales, tgl_so, tgl_kirim, note_so, no_po, tgl_po, file_attach_po, tipe_container, customer_kd, no_salesorder, alamat_kirim_kd, status_so, jml_ppn, teritory_id, container_20ft');
		$this->db->from($this->tbl_name);
		$this->db->where(array('kd_msalesorder' => $kd_msalesorder));
		$query = $this->db->get();
		$num = $query->num_rows();
		$row = $query->row();
		return $row;
	}

	public function insert_data($data = array()) {
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_input' => date('Y-m-d H:i:s'));
		$submit = array_merge($data, $data_adm);
		$aksi = $this->db->insert($this->tbl_name, $submit);
		return $aksi?TRUE:FALSE;
	}

	public function update_data($data = array(), $where = array()) {
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s'));
		$submit = array_merge($data, $data_adm);
		$aksi = $this->db->update($this->tbl_name, $submit, $where);
		return $aksi?TRUE:FALSE;
	}

	public function detail_price($var, $master) {
		if ($var == 'ongkir') :
			$this->db->select('jml_ongkir AS jml_biaya');
		elseif ($var == 'install') :
			$this->db->select('jml_install AS jml_biaya');
		endif;
		$this->db->from($this->tbl_name);
		$this->db->where(array('kd_msalesorder' => $master));
		$query = $this->db->get();
		$num = $query->num_rows();
		$result = $query->row();
		$value = $result->jml_biaya;

		return $value;
	}

	public function total_potongan($master) {
		$this->db->from('td_salesorder_harga');
		$this->db->where(array('msalesorder_kd' => $master));
		$query = $this->db->get();
		$result = $query->result();

		return $result;
	}

	public function check_val($act, $value, $master) {
		$this->db->from($this->tbl_name);
		if ($act == 'submit_ongkir') :
			$this->db->like(array('jml_ongkir' => $value));
		elseif ($act == 'submit_install') :
			$this->db->like(array('jml_install' => $value));
		endif;
		$this->db->where(array('kd_msalesorder' => $master));
		$query = $this->db->get();
		$num = $query->num_rows();
		return $num < 1?TRUE:FALSE;
	}

	public function update_price($var, $master, $val) {
		if ($var == 'ongkir') :
			$data = array('jml_ongkir' => $val);
		elseif ($var == 'install') :
			$data = array('jml_install' => $val);
		endif;
		$where = array('kd_msalesorder' => $master);
		$query = $this->db->update($this->tbl_name, $data, $where);

		$value = $this->detail_price($var, $master);

		return $value;
	}

	public function insert_ppn($data = array(), $where = array()) {
		$data_adm = array('admin_kd' => $this->session->userdata('kd_admin'), 'tgl_edit' => date('Y-m-d H:i:s'));
		$submit = array_merge($data, $data_adm);
		$aksi = $this->db->update($this->tbl_name, $submit, $where);
		return $aksi?TRUE:FALSE;
	}

	public function get_master($kd_mquotation) {
		$this->db->select('kd_msalesorder');
		$this->db->from('tm_salesorder');
		$this->db->where(array('mquotation_kd' => $kd_mquotation));
		$query = $this->db->get();
		$num = $query->num_rows();
		$row = $query->row();
		if ($num > 0) :
			$kd_msalesorder = $row->kd_msalesorder;
		endif;
		return $num > 0?$kd_msalesorder:'';
	}

	public function trans_to_msales($data) {
		$msales['kd_msalesorder'] = $data->kd_mquotation;
		$msales['mquotation_kd'] = $data->kd_mquotation;
		$msales['salesperson_kd'] = $data->salesperson_kd;
		$msales['email_sales'] = $data->email_sales;
		$msales['mobile_sales'] = $data->mobile_sales;
		$msales['no_salesorder'] = $data->no_salesorder;
		$msales['tgl_so'] = date('Y-m-d');
		$msales['customer_kd'] = $data->customer_kd;
		$msales['tipe_customer'] = $data->tipe_customer;
		$msales['tipe_harga'] = $data->tipe_harga;
		$msales['alamat_kirim_kd'] = $data->alamat_kirim_kd;
		$msales['status_so'] = 'pending';
		$msales['nm_kolom_ppn'] = $data->nm_kolom_ppn;
		$msales['jml_ppn'] = $data->jml_ppn;
		$msales['jml_ongkir'] = $data->jml_ongkir;
		$msales['jml_install'] = $data->jml_install;
		return $this->insert_data($msales);
	}
}