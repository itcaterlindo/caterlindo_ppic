<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_rawmaterial_stok_adjustment extends CI_Model {
	private $tbl_name = 'td_rawmaterial_stok_adjustment';
	private $p_key = 'rmstokadj_kd';

	public function ssp_table($date) {
		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d){
					
					return $this->tbl_btn($d);
				}),
			array( 'db' => 'a.rmstokadj_waktu', 
				'dt' => 2, 'field' => 'rmstokadj_waktu',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.rmstokadj_type', 
				'dt' => 3, 'field' => 'rmstokadj_type',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'b.rm_kode', 
				'dt' => 4, 'field' => 'rm_kode',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'b.rm_deskripsi', 
				'dt' => 5, 'field' => 'rm_deskripsi',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.rmstokadj_qty', 
				'dt' => 6, 'field' => 'rmstokadj_qty',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
            array( 'db' => 'a.rmstokadj_remark', 
				'dt' => 7, 'field' => 'rmstokadj_remark',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
							LEFT JOIN tm_rawmaterial as b ON b.rm_kd=a.rm_kd";
        $data['where'] = "DATE(a.rmstokadj_waktu)='".$date."'";
		
		return $data;
	}

	private function tbl_btn($id) {
		$btns = array();
		$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$id.'\')'));	
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code() {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

	public function create_no() { 
		$max = $this->db->select('MAX(rmin_no) as maxno')
				->from($this->tbl_name)
				->get();
		$no = 0;
		if ($max->num_rows() != 0){
			$max = $max->row_array();
			$max = $max['maxno'];
			$no = $max + 1;
		}else{
			$no = 1;
		}
		return $no;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	

	public function get_by_param_detail($param = []){
		$query = $this->db->select($this->tbl_name.'.*, tm_rawmaterial.*, td_rawmaterial_satuan.rmsatuan_nama')
					->from($this->tbl_name)
					->join('tm_rawmaterial', $this->tbl_name.'.rm_kd=tm_rawmaterial.rm_kd')
					->join('td_rawmaterial_satuan', 'tm_rawmaterial.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd')
					->where($param)
					->get();
		return $query;
    }
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_where_in ($where, $in = []){
		$query = $this->db->where_in($where, $in)
				->join('tm_purchaseorder', $this->tbl_name.'.po_kd=tm_purchaseorder.po_kd', 'left')
				->select($this->tbl_name.'.*, tm_purchaseorder.*')
				->get($this->tbl_name);
		return $query;
	}
	
}