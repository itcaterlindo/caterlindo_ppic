<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Td_purchase_suggestion_so_material extends CI_Model
{
    private $tbl_name = 'td_purchase_suggestion_so_material';
    private $p_key = 'purchasesuggestionmaterial_kd';
    private $p_foreign_key = 'purchasesuggestionsobarang_kd';

    public function ssp_table($purchasesuggestion_kd)
    {
        $data['table'] = $this->tbl_name;

        $data['primaryKey'] = $this->p_key;

        $data['columns'] = array(
            array(
                'db' => 'a.' . $this->p_key,
                'dt' => 1, 'field' => $this->p_key,
                'formatter' => function ($d) {

                    return $this->tbl_btn($d);
                }
            ),
            array(
                'db' => 'd.no_salesorder',
                'dt' => 2, 'field' => 'no_salesorder',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'e.rm_kode',
                'dt' => 3, 'field' => 'rm_kode',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.purchasesuggestionmaterial_rm_deskripsi',
                'dt' => 4, 'field' => 'purchasesuggestionmaterial_rm_deskripsi',
                'formatter' => function ($d, $row) {
                    $d = "$d | {$row[9]}";
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.purchasesuggestionmaterial_qty',
                'dt' => 5, 'field' => 'purchasesuggestionmaterial_qty',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'f.rmsatuan_nama',
                'dt' => 6, 'field' => 'rmsatuan_nama',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.purchasesuggestionmaterial_leadtimesupplier_hari',
                'dt' => 7, 'field' => 'purchasesuggestionmaterial_leadtimesupplier_hari',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.purchasesuggestionmaterial_suggestpr_tanggal',
                'dt' => 8, 'field' => 'purchasesuggestionmaterial_suggestpr_tanggal',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.purchasesuggestionmaterial_tgledit',
                'dt' => 9, 'field' => 'purchasesuggestionmaterial_tgledit',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
            array(
                'db' => 'a.purchasesuggestionmaterial_rm_spesifikasi',
                'dt' => 10, 'field' => 'purchasesuggestionmaterial_rm_spesifikasi',
                'formatter' => function ($d) {
                    $d = $this->security->xss_clean($d);

                    return $d;
                }
            ),
        );

        $data['sql_details'] = sql_connect();
        $data['joinQuery'] =    "FROM " . $this->tbl_name . " as a 
								LEFT JOIN td_purchase_suggestion_so_barang as b ON b.purchasesuggestionsobarang_kd = a.purchasesuggestionsobarang_kd
								LEFT JOIN td_purchase_suggestion_so as c ON c.purchasesuggestionso_kd = b.purchasesuggestionso_kd
								LEFT JOIN tm_salesorder as d ON d.kd_msalesorder=c.kd_msalesorder
                                LEFT JOIN tm_rawmaterial as e ON e.rm_kd=a.rm_kd
                                LEFT JOIN td_rawmaterial_satuan as f ON f.rmsatuan_kd=a.purchasesuggestionmaterial_rm_satuan";
        $data['where'] = "a.purchasesuggestion_kd = '" . $purchasesuggestion_kd . "'";

        return $data;
    }

    private function tbl_btn($id)
    {
        $delete_access = $this->session->delete_access;
        $btns = array();
        if ($delete_access == 1) {
            $btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\'' . $id . '\')'));
        }
        $btn_group = group_btns($btns);

        return $btn_group;
    }

    public function create_code()
    {
        $query = $this->db->select('MAX(' . $this->p_key . ') as maxID')
            ->get($this->tbl_name)
            ->row();
        $code = (int) $query->maxID + 1;
        return $code;
    }

    public function insert_data($data)
    {
        $query = $this->db->insert($this->tbl_name, $data);
        return $query ? TRUE : FALSE;
    }

    public function delete_data($id)
    {
        $query = $this->db->delete($this->tbl_name, array($this->p_key => $id));
        return $query ? TRUE : FALSE;
    }

    public function get_by_param($param = [])
    {
        $this->db->where($param);
        $act = $this->db->get($this->tbl_name);
        return $act;
    }

    public function update_data($aWhere = [], $data)
    {
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
        return $query ? TRUE : FALSE;
    }

    public function update_data_batch($key, $dataArray = [])
    {
        $query = $this->db->update_batch($this->tbl_name, $dataArray, $key);
        return $query ? TRUE : FALSE;
    }

    public function insert_batch($data)
    {
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
    }

    public function delete_by_param($param=[]) 
    {
		$query = $this->db->delete($this->tbl_name, $param); 
		return $query?TRUE:FALSE;
	}

    public function generate_so_materials($purchasesuggestion_kd)
    {
        $this->load->model(['td_purchase_suggestion_so_barang']);
        $sobarangs = $this->td_purchase_suggestion_so_barang->get_relate_part(['purchasesuggestion_kd' => $purchasesuggestion_kd])->result_array();

        $arrayBatch = array();
        $purchasesuggestionmaterial_kd = $this->create_code();
        foreach ($sobarangs as $eachBarang) {
            $arrayBatch[] = [
                'purchasesuggestionmaterial_kd' => $purchasesuggestionmaterial_kd,
                'purchasesuggestion_kd' => $purchasesuggestion_kd,
                'purchasesuggestionso_kd' => $eachBarang['purchasesuggestionso_kd'],
                'purchasesuggestionsobarang_kd' => $eachBarang['purchasesuggestionsobarang_kd'],
                'part_kd' => $eachBarang['part_kd'],
                'rm_kd' => $eachBarang['rm_kd'],
                'purchasesuggestionmaterial_rm_deskripsi' => $eachBarang['partdetail_deskripsi'],
                'purchasesuggestionmaterial_rm_spesifikasi' => $eachBarang['partdetail_spesifikasi'],
                'purchasesuggestionmaterial_rm_satuan' => $eachBarang['rmsatuan_kd'],
                'purchasesuggestionmaterial_rm_spesifikasi' => $eachBarang['partdetail_spesifikasi'],
                'purchasesuggestionmaterial_rm_satuan' => $eachBarang['rmsatuan_kd'],
                'purchasesuggestionmaterial_qty' => $eachBarang['purchasesuggestionsobarang_qty'] * $eachBarang['partdetail_qty'],
                'purchasesuggestionmaterial_leadtimesupplier_hari' => null,
                'purchasesuggestionmaterial_suggestpr_tanggal' => null,
                'purchasesuggestionmaterial_tglinput' => date('Y-m-d H:i:s'),
                'purchasesuggestionmaterial_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => date('Y-m-d H:i:s'),
            ];
            $purchasesuggestionmaterial_kd++;
        }
        try {
            $actDel = $this->delete_by_param(['purchasesuggestion_kd' => $purchasesuggestion_kd]);
            $act = $this->insert_batch($arrayBatch);
            $resp['code'] = 200;
            $resp['status'] = 'Sukses';
            $resp['pesan'] = 'Tersimpan';
        } catch (Exception $e) {
            $resp['code'] = 400;
            $resp['status'] = 'Gagal';
            $resp['pesan'] = $e->getMessage();
        }

        return $resp;
    }

    public function generate_rm_leadtimesupplier($purchasesuggestion_kd)
    {
        $rmkds = $this->db->select("{$this->tbl_name}.rm_kd")
            ->where("{$this->tbl_name}.purchasesuggestion_kd", $purchasesuggestion_kd)
            ->group_by("{$this->tbl_name}.rm_kd")
            ->get($this->tbl_name)
            ->result_array();
        $arrayRmkds = array_column($rmkds, 'rm_kd');

        $rmDuedates = $this->db->select('td_suplier_katalog.rm_kd, IFNULL(MAX(td_suplier_katalog.katalog_duedate), 0) as max_katalog_duedate')
            ->where_in('td_suplier_katalog.rm_kd', $arrayRmkds)
            ->group_by('td_suplier_katalog.rm_kd')
            ->get('td_suplier_katalog')->result_array();
        $arrayDuedates = array();
        foreach ($rmDuedates as $rmDuedate) {
            $arrayDuedates[$rmDuedate['rm_kd']] = $rmDuedate['max_katalog_duedate'];
        }

        $materials = $this->get_by_param(['purchasesuggestion_kd' => $purchasesuggestion_kd])->result_array();
        $arrayUpdateBatch = array();
        foreach ($materials as $material) {
            $arrayUpdateBatch[] = [
                'purchasesuggestionmaterial_kd' => $material['purchasesuggestionmaterial_kd'],
                'purchasesuggestionmaterial_leadtimesupplier_hari' => isset($arrayDuedates[$material['rm_kd']]) ? $arrayDuedates[$material['rm_kd']] : 0
            ];
        }

        try {
            $act = $this->update_data_batch($this->p_key, $arrayUpdateBatch);
            $resp['code'] = 200;
            $resp['status'] = 'Sukses';
            $resp['pesan'] = 'Tersimpan';
        } catch (Exception $e) {
            $resp['code'] = 400;
            $resp['status'] = 'Gagal';
            $resp['messages'] = $e->getMessage();
        }

        return $resp;
    }

    public function generate_suggest_pr_material($purchasesuggestion_kd)
    {
        $this->load->model(['db_hrm/tb_hari_efektif']);

        $materials = $this->db->select()
            ->where("{$this->tbl_name}.purchasesuggestion_kd", $purchasesuggestion_kd)
            ->join('td_purchase_suggestion_so', "td_purchase_suggestion_so.purchasesuggestionso_kd={$this->tbl_name}.purchasesuggestionso_kd", 'left')
            ->join('tm_purchase_suggestion', "tm_purchase_suggestion.purchasesuggestion_kd={$this->tbl_name}.purchasesuggestion_kd", 'left')
            ->get($this->tbl_name)->result_array();

        $arrayMaterial = array();
        foreach ($materials as $material) {
            $leadtimehari = $material['purchasesuggestionmaterial_leadtimesupplier_hari'] + $material['purchasesuggestion_leadtime_prpo'];
            $tglselesai = $material['purchasesuggestionso_tglmulai_produksi'];

            $tglstart = $this->tb_hari_efektif->getTglStartByLimit($tglselesai, $leadtimehari);
            $arrayMaterial[] = [
                'purchasesuggestionmaterial_kd' => $material['purchasesuggestionmaterial_kd'],
                'purchasesuggestionmaterial_suggestpr_tanggal' => $tglstart
            ];
        }

        try {
            $act = $this->update_data_batch($this->p_key, $arrayMaterial);
            $resp['code'] = 200;
            $resp['status'] = 'Sukses';
            $resp['pesan'] = 'Tersimpan';
        } catch (Exception $e) {
            $resp['code'] = 400;
            $resp['status'] = 'Gagal';
            $resp['messages'] = $e->getMessage();
        }

        return $resp;
    }

    public function generate_suggest_pr_material_old($purchasesuggestion_kd)
    {
        $this->load->model(['db_hrm/tb_hari_libur']);

        $materials = $this->db->select()
            ->where("{$this->tbl_name}.purchasesuggestion_kd", $purchasesuggestion_kd)
            ->join('td_purchase_suggestion_so', "td_purchase_suggestion_so.purchasesuggestionso_kd={$this->tbl_name}.purchasesuggestionso_kd", 'left')
            ->join('tm_purchase_suggestion', "tm_purchase_suggestion.purchasesuggestion_kd={$this->tbl_name}.purchasesuggestion_kd", 'left')
            ->get($this->tbl_name)->result_array();

        $arrayMaterial = array();
        foreach ($materials as $material) {
            $actualLeadtimeHari = 0;
            $leadtimehari = $material['purchasesuggestionmaterial_leadtimesupplier_hari'] + $material['purchasesuggestion_leadtime_prpo'];
            $tglselesai = $material['purchasesuggestionso_tglmulai_produksi'];

            $tglstart = date('Y-m-d', strtotime("-$leadtimehari days", strtotime($tglselesai)));
            while ($actualLeadtimeHari < ($material['purchasesuggestionmaterial_leadtimesupplier_hari'] + $material['purchasesuggestion_leadtime_prpo'])) {
                $begin = new DateTime($tglstart);
                $end = new DateTime($tglselesai);
                $interval = DateInterval::createFromDateString('1 day');
                $period = new DatePeriod($begin, $interval, $end);

                $countLibur = 0;
                foreach ($period as $dt) {
                    // Weekend check
                    if ($dt->format("N") == 6 || $dt->format("N") == 7) {
                        $countLibur++;
                    }
                }
                // Add libur nasional
                $countLibur += $this->tb_hari_libur->getByDateBetween($tglstart, $tglselesai)->num_rows();

                $leadtimehari = $leadtimehari + $countLibur;
                $actualLeadtimeHari = $leadtimehari - $countLibur;
                $tglstart = date('Y-m-d', strtotime("-$leadtimehari days", strtotime($tglselesai)));
            }
            if (date('N', strtotime($tglstart)) == 6) {
                $tglstart = date('Y-m-d', strtotime("-1 days", strtotime($tglstart)));
            } elseif (date('N', strtotime($tglstart)) == 7) {
                $tglstart = date('Y-m-d', strtotime("-2 days", strtotime($tglstart)));
            }
            $arrayMaterial[] = [
                'purchasesuggestionmaterial_kd' => $material['purchasesuggestionmaterial_kd'],
                'purchasesuggestionmaterial_suggestpr_tanggal' => $tglstart
            ];
        }

        try {
            $act = $this->update_data_batch($this->p_key, $arrayMaterial);
            $resp['code'] = 200;
            $resp['status'] = 'Sukses';
            $resp['pesan'] = 'Tersimpan';
        } catch (Exception $e) {
            $resp['code'] = 400;
            $resp['status'] = 'Gagal';
            $resp['messages'] = $e->getMessage();
        }

        return $resp;
    }

    public function get_suggestpr_periode($startdate, $enddate)
    {
        // material limit stok
        $rmUnderLimits = $this->db->select('tm_rawmaterial.rm_kd, td_rawmaterial_stok_master.rmstokmaster_qty, tm_rawmaterial.rm_stock_min')
            ->where('td_rawmaterial_stok_master.rmstokmaster_qty <', 'tm_rawmaterial.rm_stock_min')
            ->join('tm_rawmaterial', 'tm_rawmaterial.rm_kd=td_rawmaterial_stok_master.rm_kd', 'right')
            ->get('td_rawmaterial_stok_master')->result_array();
        $filterRMUnderLimits = array_column($rmUnderLimits, 'rmstokmaster_qty', 'rm_kd');

        // material demand periode
        $materialDemands = $this->db->select("{$this->tbl_name}.rm_kd, SUM({$this->tbl_name}.purchasesuggestionmaterial_qty) as sum_purchasesuggestionmaterial_qty")
            ->where("{$this->tbl_name}.purchasesuggestionmaterial_suggestpr_tanggal >=", $startdate)
            ->where("{$this->tbl_name}.purchasesuggestionmaterial_suggestpr_tanggal <=", $enddate)
            ->group_by("{$this->tbl_name}.rm_kd")
            ->get($this->tbl_name)->result_array();
        $filterRMDemand = array_column($materialDemands, 'sum_purchasesuggestionmaterial_qty', 'rm_kd');

        $rmScopes = array_values(array_unique(array_merge(
            array_column($rmUnderLimits, 'rm_kd'), 
            array_column($materialDemands, 'rm_kd')
        )));
        
        // Get rm stok
        $rmStoks = $this->db->select('td_rawmaterial_stok_master.rm_kd, td_rawmaterial_stok_master.rmstokmaster_qty, tm_rawmaterial.rm_stock_min, tm_rawmaterial.rm_stock_max')
            ->where_in('td_rawmaterial_stok_master.rm_kd', $rmScopes)
            ->join('tm_rawmaterial', 'tm_rawmaterial.rm_kd=td_rawmaterial_stok_master.rm_kd','left')
            ->get('td_rawmaterial_stok_master')->result_array();

        // Get pr outstanding
        $prOpens = $this->db->select('td_purchaserequisition_detail.prdetail_kd, td_purchaserequisition_detail.rm_kd, td_purchaserequisition_detail.prdetail_qtykonversi, 
                IFNULL(SUM(td_purchaseorder_detail.podetail_qty * td_purchaseorder_detail.podetail_konversi), 0) as sum_podetail_qty')
            ->from('td_purchaserequisition_detail')
            ->where('tm_purchaserequisition.pr_closeorder', '0')
            ->where('td_purchaserequisition_detail.rm_kd !=', 'SPECIAL')
            ->join('tm_purchaserequisition', 'tm_purchaserequisition.pr_kd=td_purchaserequisition_detail.pr_kd', 'left')
            ->join('td_purchaseorder_detail', 'td_purchaseorder_detail.prdetail_kd=td_purchaserequisition_detail.prdetail_kd', 'left')
            ->group_by('td_purchaserequisition_detail.prdetail_kd')
            ->get_compiled_select();
        $prOpensResult = $this->db->query("
                SELECT t.rm_kd, ABS(IFNULL(SUM(sum_podetail_qty - prdetail_qtykonversi), 0)) as sum_propen_qty 
                FROM ($prOpens) as t
                GROUP BY t.rm_kd
            ")->result_array();
        $filterRMpropen = array_column($prOpensResult, 'sum_propen_qty', 'rm_kd');

        // Get po outstanding
        $poOpens = $this->db->select('td_purchaseorder_detail.podetail_kd, td_purchaseorder_detail.rm_kd, SUM(td_purchaseorder_detail.podetail_qty * td_purchaseorder_detail.podetail_konversi) as sum_qtypo_konversi,
                IFNULL(SUM(td_rawmaterial_goodsreceive.rmgr_qtykonversi),0) as sum_rmgr_qty')
            ->from('td_purchaseorder_detail')
            ->where('tm_purchaseorder.po_closeorder', '0')
            ->where('td_purchaseorder_detail.rm_kd !=', 'SPECIAL')
            ->join('tm_purchaseorder', 'tm_purchaseorder.po_kd=td_purchaseorder_detail.po_kd', 'left')
            ->join('td_rawmaterial_goodsreceive', 'td_rawmaterial_goodsreceive.podetail_kd=td_purchaseorder_detail.podetail_kd', 'left')
            ->group_by('td_purchaseorder_detail.podetail_kd')
            // ->get()->result_array();
            ->get_compiled_select();
        
        $poOpensResult = $this->db->query("
            SELECT t.rm_kd, ABS(IFNULL(SUM(sum_rmgr_qty - sum_qtypo_konversi), 0)) as sum_poopen_qty 
            FROM ($poOpens) as t
            GROUP BY t.rm_kd
        ")->result_array();
        $filterRMpoopen = array_column($poOpensResult, 'sum_poopen_qty', 'rm_kd');

        /** Suggest PR
         * + Array scopes (rm < min stok) 
         * + array scopes (material demand periode)
         * - PR outstanding
         * - PO outstanding
         */

        $materials = $this->db->select('tm_rawmaterial.*, td_rawmaterial_satuan.rmsatuan_nama, satuan_secondary.rmsatuan_kd as satuan_secondary_kd, satuan_secondary.rmsatuan_nama as satuan_secondary_nama')
            ->where_in('tm_rawmaterial.rm_kd', $rmScopes)
            ->join('td_rawmaterial_satuan', 'td_rawmaterial_satuan.rmsatuan_kd=tm_rawmaterial.rmsatuan_kd', 'left')
            ->join('td_rawmaterial_satuan as satuan_secondary', 'satuan_secondary.rmsatuan_kd=tm_rawmaterial.rmsatuansecondary_kd', 'left')
            ->get('tm_rawmaterial')->result_array();
        $filterMaterials = array();
        foreach ($materials as $m){
            $filterMaterials[$m['rm_kd']] = $m;
        }
        
        //TODO Generate Duedate from leadtime suppliereach rm_kd
        $rmDuedates = $this->db->select('td_suplier_katalog.rm_kd, IFNULL(MAX(td_suplier_katalog.katalog_duedate), 0) as max_katalog_duedate')
            ->where_in('td_suplier_katalog.rm_kd', $rmScopes)
            ->group_by('td_suplier_katalog.rm_kd')
            ->get('td_suplier_katalog')->result_array();
        $filterRMduedate = array_column($rmDuedates, 'max_katalog_duedate', 'rm_kd');

        $arraySuggestPR = array();
        $sumRMminRMdemand = 0;
        for ($i = 0; $i < count($rmScopes); $i++) {
            if ($rmScopes[$i] != 'SPECIAL' || $rmScopes[$i] != null) {
                $rmDemand = isset($filterRMDemand[$rmScopes[$i]]) ? $filterRMDemand[$rmScopes[$i]] : 0;

                /** SUM processed purchase */
                $rmOpenPR = isset($filterRMpropen[$rmScopes[$i]]) ? $filterRMpropen[$rmScopes[$i]] : 0;
                $rmOpenPO = isset($filterRMpoopen[$rmScopes[$i]]) ? $filterRMpoopen[$rmScopes[$i]] : 0;
                $sumRMopenPRPO = $rmOpenPR + $rmOpenPO;

                // Bandingkan dengan stok skarang
                $rm_stock_min = 0;
                foreach ($rmStoks as $eachRMStock){
                    if ($eachRMStock['rm_kd'] == $rmScopes[$i]){
                        $rm_stock_min = $eachRMStock['rm_stock_min'];
                        $rmstokmaster_qty = $eachRMStock['rmstokmaster_qty'];
                        $rm_suggest_qty = $rmstokmaster_qty + $sumRMopenPRPO - $rmDemand;
                        if ($rm_suggest_qty < $eachRMStock['rm_stock_min']){
                            $arraySuggestPR[] = [
                                'rm_kd' => $rmScopes[$i],
                                'rmstokmaster_qty' => $rmstokmaster_qty,
                                'rmDemand' => $rmDemand,
                                'rmOpenPR' => $rmOpenPR,
                                'rmOpenPO' => $rmOpenPO,
                                'rm_suggest_qty' => abs($rm_suggest_qty),
                                'sumRMminRMdemand' => $sumRMminRMdemand,
                                'sumRMopenPRPO' => $sumRMopenPRPO,

                                'rm_stock_min' => $rm_stock_min,
                                'rm_nama' => $filterMaterials[$rmScopes[$i]]['rm_nama'],
                                'rm_deskripsi' => $filterMaterials[$rmScopes[$i]]['rm_deskripsi'],
                                'rm_spesifikasi' => $filterMaterials[$rmScopes[$i]]['rm_spesifikasi'],
                                'rm_kode' => $filterMaterials[$rmScopes[$i]]['rm_kode'],
                                'rmsatuan_kd' => $filterMaterials[$rmScopes[$i]]['rmsatuan_kd'],
                                'rmsatuan_nama' => $filterMaterials[$rmScopes[$i]]['rmsatuan_nama'],
                                'satuan_secondary_kd' => $filterMaterials[$rmScopes[$i]]['satuan_secondary_kd'],
                                'satuan_secondary_nama' => $filterMaterials[$rmScopes[$i]]['satuan_secondary_nama'],
                                'rm_konversiqty' => $filterMaterials[$rmScopes[$i]]['rm_konversiqty'],

                                'rm_duedatesupplier' => isset($filterRMduedate[$rmScopes[$i]]) ? $filterRMduedate[$rmScopes[$i]] : 0
                            ];
                        }
                    }
                }
            }
        }

        return $arraySuggestPR;
    }

    public function getMaterialGroupBySO($purcahsesuggestion_kd)
    {
        return $this->db->select("{$this->tbl_name}.purchasesuggestionso_kd, {$this->tbl_name}.rm_kd, {$this->tbl_name}.purchasesuggestionmaterial_rm_deskripsi, {$this->tbl_name}.purchasesuggestionmaterial_rm_spesifikasi,
                {$this->tbl_name}.purchasesuggestionmaterial_leadtimesupplier_hari, {$this->tbl_name}.purchasesuggestionmaterial_suggestpr_tanggal, 
                tm_salesorder.kd_msalesorder, tm_salesorder.no_salesorder, tm_salesorder.no_po, tm_salesorder.tipe_customer , SUM({$this->tbl_name}.purchasesuggestionmaterial_qty) as sum_purchasesuggestionmaterial_qty,
                tm_rawmaterial.rm_kode, td_rawmaterial_satuan.rmsatuan_nama")
            ->join('td_purchase_suggestion_so', "td_purchase_suggestion_so.purchasesuggestionso_kd={$this->tbl_name}.purchasesuggestionso_kd", 'left')
            ->join('tm_salesorder', "tm_salesorder.kd_msalesorder=td_purchase_suggestion_so.kd_msalesorder", 'left')
            ->join('tm_rawmaterial', "tm_rawmaterial.rm_kd={$this->tbl_name}.rm_kd", 'left')
            ->join('td_rawmaterial_satuan', "td_rawmaterial_satuan.rmsatuan_kd={$this->tbl_name}.purchasesuggestionmaterial_rm_satuan", 'left')
            ->where("{$this->tbl_name}.purchasesuggestion_kd", $purcahsesuggestion_kd)
            ->group_by("{$this->tbl_name}.purchasesuggestionso_kd, {$this->tbl_name}.rm_kd")
            ->get($this->tbl_name);
    }

    public function getMaterialGroupByTglSuggest($purcahsesuggestion_kd)
    {
        return $this->db->select("{$this->tbl_name}.purchasesuggestionso_kd, {$this->tbl_name}.rm_kd, {$this->tbl_name}.purchasesuggestionmaterial_rm_deskripsi, {$this->tbl_name}.purchasesuggestionmaterial_rm_spesifikasi,
                {$this->tbl_name}.purchasesuggestionmaterial_leadtimesupplier_hari, {$this->tbl_name}.purchasesuggestionmaterial_suggestpr_tanggal, 
                tm_salesorder.kd_msalesorder, tm_salesorder.no_salesorder, tm_salesorder.no_po, tm_salesorder.tipe_customer , SUM({$this->tbl_name}.purchasesuggestionmaterial_qty) as sum_purchasesuggestionmaterial_qty,
                tm_rawmaterial.rm_kode, td_rawmaterial_satuan.rmsatuan_nama")
            ->join('td_purchase_suggestion_so', "td_purchase_suggestion_so.purchasesuggestionso_kd={$this->tbl_name}.purchasesuggestionso_kd", 'left')
            ->join('tm_salesorder', "tm_salesorder.kd_msalesorder=td_purchase_suggestion_so.kd_msalesorder", 'left')
            ->join('tm_rawmaterial', "tm_rawmaterial.rm_kd={$this->tbl_name}.rm_kd", 'left')
            ->join('td_rawmaterial_satuan', "td_rawmaterial_satuan.rmsatuan_kd={$this->tbl_name}.purchasesuggestionmaterial_rm_satuan", 'left')
            ->where("{$this->tbl_name}.purchasesuggestion_kd", $purcahsesuggestion_kd)
            ->group_by("{$this->tbl_name}.purchasesuggestionmaterial_suggestpr_tanggal, {$this->tbl_name}.rm_kd")
            ->order_by("{$this->tbl_name}.purchasesuggestionmaterial_suggestpr_tanggal", 'asc')
            ->get($this->tbl_name);
    }

    public function summaryMaterialGroupByTglSuggest($purcahsesuggestion_kd)
    {
        return $this->db->select("{$this->tbl_name}.purchasesuggestionso_kd, {$this->tbl_name}.rm_kd, {$this->tbl_name}.purchasesuggestionmaterial_rm_deskripsi, {$this->tbl_name}.purchasesuggestionmaterial_rm_spesifikasi,
                {$this->tbl_name}.purchasesuggestionmaterial_leadtimesupplier_hari, {$this->tbl_name}.purchasesuggestionmaterial_suggestpr_tanggal, 
                tm_salesorder.kd_msalesorder, tm_salesorder.no_salesorder, tm_salesorder.no_po, tm_salesorder.tipe_customer , SUM({$this->tbl_name}.purchasesuggestionmaterial_qty) as sum_purchasesuggestionmaterial_qty,
                tm_rawmaterial.rm_kode, td_rawmaterial_satuan.rmsatuan_nama, tm_rawmaterial.rm_stock_min")
            ->join('td_purchase_suggestion_so', "td_purchase_suggestion_so.purchasesuggestionso_kd={$this->tbl_name}.purchasesuggestionso_kd", 'left')
            ->join('tm_salesorder', "tm_salesorder.kd_msalesorder=td_purchase_suggestion_so.kd_msalesorder", 'left')
            ->join('tm_rawmaterial', "tm_rawmaterial.rm_kd={$this->tbl_name}.rm_kd", 'left')
            ->join('td_rawmaterial_satuan', "td_rawmaterial_satuan.rmsatuan_kd={$this->tbl_name}.purchasesuggestionmaterial_rm_satuan", 'left')
            ->where("{$this->tbl_name}.purchasesuggestion_kd", $purcahsesuggestion_kd)
            ->group_by("{$this->tbl_name}.purchasesuggestionmaterial_suggestpr_tanggal, {$this->tbl_name}.rm_kd")
            ->order_by("{$this->tbl_name}.purchasesuggestionmaterial_suggestpr_tanggal", 'asc')
            ->get($this->tbl_name);
    }

}
