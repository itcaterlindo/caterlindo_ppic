<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_purchaserequisition extends CI_Model {
	private $tbl_name = 'tm_purchaserequisition';
	private $p_key = 'pr_kd';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tb_purchaserequisition_status', 'tb_purchaserequisition_user', '_mail_configuration', 'tb_admin', 'tb_purchaserequisition_user', 'tb_relasi_popr', 'td_purchaseorder_detail', 'tb_purchaserequisition_logstatus']);	
    }

	public function ssp_table($aParam = []) {
		/** additional settting other db */
		$this->dbHrm = $this->load->database('sim_hrm', TRUE);
		$dbSimHrm = $this->dbHrm->database;


		$this->dbCs = $this->load->database('db_caterlindo_support', TRUE);
		$dbCss = $this->dbCs->database;

		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => 'a.'.$this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){
					
					return $this->tbl_btn($d, $row[6], $row[8]);
				}),
			array( 'db' => 'a.pr_tanggal', 
				'dt' => 2, 'field' => 'pr_tanggal',
				'formatter' => function ($d){
					$d = format_date($d, 'Y-m-d');
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.pr_no', 
				'dt' => 3, 'field' => 'pr_no',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
            array( 'db' => 'b.nm_admin', 
				'dt' => 4, 'field' => 'nm_admin',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'c.nm_bagian', 
				'dt' => 5, 'field' => 'nm_bagian',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.pr_note', 
				'dt' => 6, 'field' => 'pr_note',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'd.wfstate_nama', 
				'dt' => 7, 'field' => 'wfstate_nama',
				'formatter' => function ($d, $row){
					$d = build_badgecolor($row[10], $d);

					return $d;
				}),
			array( 'db' => 'GROUP_CONCAT(xc.kode_budgeting SEPARATOR "; &nbsp") as cnc_bgd', 
				'dt' => 8, 'field' => 'cnc_bgd',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => '(SELECT GROUP_CONCAT(ticket_kode SEPARATOR ";") as cnc_bgd FROM '.$dbCss.'.ticket as vx
								LEFT JOIN pr_has_ticket as xnx ON vx.ticket_id = xnx.ticket_kd
							WHERE pr_kd = a.pr_kd) as tx_code',
				'dt' => 9, 'field' => 'tx_code',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.pr_closeorder', 
				'dt' => 10, 'field' => 'pr_closeorder',
				'formatter' => function ($d){
					$d = choice_enum($d);
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'd.wfstate_badgecolor',
				'dt' => 11, 'field' => 'wfstate_badgecolor',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.pr_user', 
				'dt' => 12, 'field' => 'pr_user',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." as a
							LEFT JOIN tb_admin b ON a.pr_user=b.kd_admin
							LEFT JOIN ".$dbSimHrm.".tb_bagian as c ON a.pr_bagian=c.kd_bagian
							LEFT JOIN td_workflow_state as d ON a.wfstate_kd=d.wfstate_kd
							LEFT JOIN pr_has_ticket as xnx ON a.pr_kd = xnx.pr_kd
							LEFT JOIN td_purchaserequisition_detail as xa ON a.pr_kd=xa.pr_kd
							LEFT JOIN ".$dbCss.".td_barang_budgeting as xb ON xa.budgeting_detail_kd = xb.id_barang
							LEFT JOIN ".$dbCss.".tm_budgeting as xc ON xc.kode_budgeting = xb.id_budgeting
							LEFT JOIN ".$dbCss.".ticket as xn ON xn.ticket_id = xnx.ticket_kd
						";
		$condition = "YEAR(a.pr_tanggal)=".$aParam['tahun'];
		#bulan
		if ($aParam['bulan'] != 'ALL') {
			$condition .= " AND MONTH(a.pr_tanggal)=".(int)$aParam['bulan'];
		}
		if ($aParam['wfstate_kd'] != 'ALL') {
			$condition .= " AND d.wfstate_kd='".$aParam['wfstate_kd']."'";
		}
		#wfstate_kd
		if (!cek_permission('PURCHASEREQUISITION_VIEW_ALLPR')) {
			$admin_kd = $this->session->userdata('kd_admin');
			$condition .= " AND a.pr_user='".$admin_kd."'";
		}
		$data['where'] = $condition;

		$data['groupBy'] = "a.pr_kd";
		
		return $data;
	}

	private function tbl_btn($id, $status, $admin_kd) {
		$read_access = 1;
		$update_access = 1;
		$delete_access = 1;
		$btns = array();
		$btns[] = get_btn(array('title' => 'Lihat Item', 'icon' => 'search', 'onclick' => 'lihat_data(\''.url_encrypt($id).'\')'));
		if ($status == 'Editing'){
			$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
		}elseif($status == 'Acknowledged' || $status == 'Finish'){
			$btns[] = get_btn(array('title' => 'Cetak Item', 'icon' => 'print', 'onclick' => 'cetak_data(\''.url_encrypt($id).'\')'));
		}
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function labelStatus($label, $colour){
		$span = '<span class="label label-'.$colour.'">'.$label.'</span>';

		return $span;
	}

	public function create_code() {
		$this->db->select($this->p_key.' AS code')
			->from($this->tbl_name)
			->where(array('DATE(pr_tglinput)' => date('Y-m-d')))
			->order_by('pr_tglinput DESC, '.$this->p_key.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -6);
		endif;
		$angka = $urutan + 1;
		$primary = 'MPR'.date('ymd').str_pad($angka, 6, '0', STR_PAD_LEFT);
		return $primary;
	}

	public function create_no() {
		$this->db->select('MAX(pr_no) AS no')
			->from($this->tbl_name);
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$no = $row->no;
			$urutan =$no;
		endif;
		$angka = $urutan + 1;
		$primary = str_pad($angka, 6, '0', STR_PAD_LEFT);
		return $primary;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}

	public function get_by_id_detail($id){
		$query = $this->db->select($this->tbl_name.'.*, tb_admin.kd_karyawan, tb_admin.nm_admin, tb_purchaserequisition_status.*')
					->from($this->tbl_name)
					->join('tb_admin', $this->tbl_name.'.pr_user=tb_admin.kd_admin')
					->join('tb_purchaserequisition_status', $this->tbl_name.'.prstatus_kd=tb_purchaserequisition_status.prstatus_kd', 'left')
					->where($this->tbl_name.'.'.$this->p_key, $id)
					->get();
		return $query;
	}

	public function get_where_in ($where, $in = []){
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}

	public function generateButtonAction($pr_kd){
		$rowData = $this->get_by_param(array('pr_kd' => $pr_kd))->row_array();
		$rowLevel = $this->tb_purchaserequisition_status->get_by_param(array('prstatus_kd' => $rowData['prstatus_kd']))->row_array();
		$nextLevel = $this->tb_purchaserequisition_status->get_by_param(array('prstatus_level' => $rowLevel['prstatus_level'] + 1))->row_array();
		$backLevel = $this->tb_purchaserequisition_status->get_by_param(array('prstatus_level' => $rowLevel['prstatus_level'] - 1))->row_array();
		$editingLevel = 1;

		/** Cek user */
		$admin_kd = $this->session->userdata('kd_admin');
		$isAdmin = $this->tb_admin->isAdmin($admin_kd);
		$isApprover = $this->tb_purchaserequisition_user->isApprover($admin_kd);
		$isCurrentState = $this->tb_purchaserequisition_user->isCurrentState($admin_kd, $rowLevel['prstatus_kd']);
		$isEditingState = $this->tb_purchaserequisition_status->isEditingState($rowLevel['prstatus_kd']);
		$isMaxState = $this->tb_purchaserequisition_status->isMaxState($rowLevel['prstatus_kd']);
		$isFinishState = $this->tb_purchaserequisition_status->isFinishState($rowLevel['prstatus_kd']);
		
		$btn = '<div class="btn-group">';
		if ($isAdmin || $isApprover){
			$btn .= '<button class="btn btn-danger btn-sm"  onclick=ubah_status("'.$pr_kd.'","'.$editingLevel.'","full")><i class="fa fa-close"></i> Spesial Ubah Status </button>';
		}

		if ($isCurrentState || $isAdmin || $isApprover || $isEditingState){
			if ($isEditingState) {
				if ($rowData['pr_user'] == $admin_kd || $isAdmin) {
					$btn .= '<button class="btn btn-warning btn-sm"  onclick=ubah_status("'.$pr_kd.'","'.$backLevel['prstatus_level'].'","")><i class="fa fa-hand-o-left"></i> Ubah Status '.$backLevel['prstatus_nama'].'</button>';
					$btn .= '<button class="btn btn-warning btn-sm"  onclick=ubah_status("'.$pr_kd.'","'.$nextLevel['prstatus_level'].'","")><i class="fa fa-hand-o-right"></i> Ubah Status '.$nextLevel['prstatus_nama'].'</button>';
				}
			}else{
				$btn .= '<button class="btn btn-warning btn-sm"  onclick=ubah_status("'.$pr_kd.'","'.$backLevel['prstatus_level'].'","")><i class="fa fa-hand-o-left"></i> Ubah Status '.$backLevel['prstatus_nama'].'</button>';
				if (!($isMaxState || $isFinishState)){
					$btn .= '<button class="btn btn-warning btn-sm"  onclick=ubah_status("'.$pr_kd.'","'.$nextLevel['prstatus_level'].'","")><i class="fa fa-hand-o-right"></i> Ubah Status '.$nextLevel['prstatus_nama'].'</button>';
				}
			}
		}
		/** Button Finish */
		if ($isMaxState) {
			$qPodetail = $this->db->select()
						->from('tb_relasi_popr')
						->join('td_purchaseorder_detail', 'tb_relasi_popr.po_kd=td_purchaseorder_detail.po_kd' , 'right')
						->where(['tb_relasi_popr.pr_kd' => $pr_kd])
						->get()->num_rows();
			if (!empty($qPodetail)) {
				$btnFinish = '<button class="btn btn-primary btn-sm"  onclick=ubah_status("'.$pr_kd.'","'.$nextLevel['prstatus_level'].'","")><i class="fa fa-hand-o-right"></i> Ubah Status '.$nextLevel['prstatus_nama'].'</button>';
				$btn .= $btnFinish;
			}
		}
		$btn .= '</div>';

		return $btn;
	}

	public function action_ubah_status($pr_kd, $prstatus_level, $keterangan){ 
		$this->load->helper(['my_helper']);       
		/** Get id prstatus */
		$rowStatus = $this->tb_purchaserequisition_status->get_by_param(array('prstatus_level' => $prstatus_level))->row_array();
		/** Send email if true */
		$actEmail = false;
		$to = array();
		if ($rowStatus['prstatus_sendemail'] == 'true'){
			$rowPR = $this->get_by_id_detail($pr_kd)->row_array();

			$subject = 'Purchase Requisition-'.$rowPR['pr_no'];
			$dataMessage['text'] = 'Terdapat Purchase Requisition yang perlu ditindak lanjuti :';
			$dataMessage['url'] = my_baseurl().'purchasing/purchase_requisition/purchase_requisition_detail?id='.url_encrypt($pr_kd);
			$dataMessage['urltext'] = 'Detail Purchase requisition';
			$dataMessage['keterangan'] = !empty($keterangan) ? $keterangan : '-';
			$message = $this->load->view('templates/email/email_notification', $dataMessage, true);
			
			$resultUserEmail = $this->tb_purchaserequisition_user->get_by_param (array('prstatus_kd' => $rowStatus['prstatus_kd']))->result_array();
			foreach ($resultUserEmail as $each){
				$to[] = $each['pruser_email'];
			}
			$actEmail = $this->_mail_configuration->sendEmail($to, $subject, $message);
		}
		/** Data Update PR */
		$data = array(
			'prstatus_kd' => $rowStatus['prstatus_kd'],
			'pr_tgledit' => date('Y-m-d H:i:s'),
			'admin_kd' => $this->session->userdata('kd_admin'),
		);

		$act = $this->update_data(array('pr_kd' => $pr_kd), $data);
		$actLog = $this->tb_purchaserequisition_logstatus->action_insert_log($pr_kd, $prstatus_level, $keterangan);

		$resp = array('actUpdate'=> $act, 'email' => $actEmail, 'to' => $to);
		return $resp;        
	}

	/** Untuk rubah status PR yang sudah ACK menjadi finish apabila
	 * komposisi dari masing2 material yang di PR kan sudah masuk ke PO
	 */
	private function group_by_prdetail ($data){
		$groups = array();
		foreach ($data as $item) {
			$key = $item['prdetail_kd'];
			/** jika tidak ada key */
			if (!array_key_exists($key, $groups)) {
				$groups[$key] = array(
					'prdetail_kd' => $key,
					'rekap_podetail_qty' => (float) $item['podetail_qty'],
				);
			}else{
				$groups[$key]['rekap_podetail_qty'] = (float) $groups[$key]['rekap_podetail_qty'] + $item['podetail_qty'];
			}
		}
		
		foreach ($groups as $each){
			$groupResult[] = $each;
		}
		return $groupResult;
	}
	
	public function autoUpdate_status_pr($pr_kd) {
		$rowPR = $this->get_by_param(array('pr_kd' => $pr_kd))->row_array();
		/** Cek status apakah sudah ACK atau sudah Finish */
		$act = true;
		$resp = false;
		$resultGroupPOdetail = array(); 
		$isACKState = $this->tb_purchaserequisition_status->isMaxState($rowPR['prstatus_kd']);
		$isFinishState = $this->tb_purchaserequisition_status->isFinishState($rowPR['prstatus_kd']);

		$resultDetailPR = $this->td_purchaserequisition_detail->get_by_param(array('pr_kd' => $pr_kd))->result_array();
		/** get PO from dari kode PR */
		$resultPOPR = $this->tb_relasi_popr->get_by_param (array('pr_kd' => $pr_kd));
		$rowFinishStatus = $this->tb_purchaserequisition_status->finishStatus();
		if ($resultPOPR->num_rows() != 0){
			$resultPOPR = $resultPOPR->result_array();
			$batchPO = array();
			foreach ($resultPOPR as $eachPOPR){
				$keyPOPR = $eachPOPR['po_kd'];
				if (!in_array($keyPOPR, $batchPO)){
					$batchPO[] = $eachPOPR['po_kd'];
				}
			}
			$resultPOdetail = $this->td_purchaseorder_detail->get_where_in ('td_purchaseorder_detail.po_kd', $batchPO);
			if ($resultPOdetail->num_rows() != 0){
				$resultGroupPOdetail = $this->group_by_prdetail ($resultPOdetail->result_array());
				/** Proses bandingkan data qty PR dan PO */
				$cekSelisih = array();
				foreach ($resultDetailPR as $eachDetailPR):
					$calcPRPO = (float) $eachDetailPR['prdetail_qty'];
					foreach ($resultGroupPOdetail as $eachDetailPO):
						if ($eachDetailPO['prdetail_kd'] == $eachDetailPR['prdetail_kd']){
							$calcPRPO = (float) $eachDetailPR['prdetail_qty'] - $eachDetailPO['rekap_podetail_qty'];
						}
					endforeach;
					$cekSelisih[] = $calcPRPO;
				endforeach;
				/** Cek apabila nilai terbesarnya dari sisa antaran PR dikurangi dengan total qty PO lebih kecil dari nol
				 * maka lakukan update ke finish
				 */
				if (!empty($cekSelisih)) {
					if (max($cekSelisih) <= 0 && $isACKState){
						$act = $this->action_ubah_status($pr_kd, $rowFinishStatus['prstatus_level'], 'Update Otomatis');
						$resp = $act['actUpdate'];
					}elseif(max($cekSelisih) > 0 && $isFinishState){
						/** Kembalikan menjadi ack state */
						$befFinish = (int) $rowFinishStatus['prstatus_level'] - 1;
						$act = $this->action_ubah_status($pr_kd, $befFinish, 'Update Otomatis');
						$resp = $act['actUpdate'];
					}
				}else{
					$resp = true; // tidak ada prdetail
				}
			}else {
				/** Kembalikan menjadi ack state */
				$befFinish = (int) $rowFinishStatus['prstatus_level'] - 1;
				$act = $this->action_ubah_status($pr_kd, $befFinish, 'Update Otomatis');
				$resp = $act['actUpdate'];
			}
		}else{
			$resp = false;
		}

		return $resp;
	}

	public function update_close_order($pr_kd = null) {
		if (!empty($pr_kd)) {
			$pr = $this->get_by_param(['pr_kd' => $pr_kd])->row_array();
			$prdetails = $this->td_purchaserequisition_detail->get_by_param(array('pr_kd' => $pr_kd))->result_array();
			$poprs = $this->tb_relasi_popr->get_by_param (array('pr_kd' => $pr_kd));
			$aSelisih = [];
			if ($poprs->num_rows() != 0){
				$aPoprs = $poprs->result_array();
				$po_kds = array_column($aPoprs, 'po_kd');
				if (!empty($po_kds)) {
					#group by prdetail kd yang sudah di po kan
					$podetail_groups = $this->db->select('prdetail_kd, SUM(podetail_qty) as sum_po')
						->where_in('po_kd', $po_kds)
						->group_by('prdetail_kd')
						->get('td_purchaseorder_detail')->result_array();

					foreach ($prdetails as $prdetail):
						$resPr_qty = (float) $prdetail['prdetail_qty'];
						foreach ($podetail_groups as $podetail_group):
							if ($podetail_group['prdetail_kd'] == $prdetail['prdetail_kd']){
								$resPr_qty = (float) $prdetail['prdetail_qty'] - $podetail_group['sum_po'];
							}
						endforeach;
						$aSelisih[] = $resPr_qty;
					endforeach;
				}

			}
		}

		if (!empty($aSelisih)){
			# close order
			if (max($aSelisih) <= 0 && $pr['pr_closeorder'] == '0'){
				$act = $this->update_data(['pr_kd' => $pr_kd], ['pr_closeorder' => '1', 'pr_tgledit' => date('Y-m-d H:i:s')]);
			}elseif(max($aSelisih) > 0 && $pr['pr_closeorder'] == '1'){
				# reopen order
				$act = $this->update_data(['pr_kd' => $pr_kd], ['pr_closeorder' => '0', 'pr_tgledit' => date('Y-m-d H:i:s')]);
			}
		}

		return true;
	}

	public function update_batch($array=[], $key){
		$act = $this->db->update_batch($this->tbl_name,$array, $key);
		return $act;
	}

	public function outstanding_pr($rm_kd = [])
	{
        $this->db->select('a.*, b.*, k.*, c.rm_kode, mj.wfstate_kd, mjk.wfstate_kd as pr_wfstate_kd, c.rm_oldkd, d.rmsatuan_nama, e.nm_admin')
                    ->from('td_purchaserequisition_detail as a')
                    ->join('tm_purchaserequisition as b', 'a.pr_kd=b.pr_kd', 'left')
					->join('td_purchaseorder_detail as j', 'a.prdetail_kd=j.prdetail_kd', 'left')
					->join('tm_purchaseorder as k', 'j.po_kd=k.po_kd', 'left')
					->join('td_workflow_state as mj', 'mj.wfstate_kd=k.wfstate_kd', 'left')
					->join('td_workflow_state as mjk', 'mjk.wfstate_kd=b.wfstate_kd', 'left')
                    ->join('tm_rawmaterial as c', 'a.rm_kd=c.rm_kd', 'left')
                    ->join('td_rawmaterial_satuan as d', 'c.rmsatuan_kd=d.rmsatuan_kd', 'left')
                    ->join('tb_admin as e', 'b.pr_user=e.kd_admin')
					->join('td_workflow_state f', 'b.wfstate_kd=f.wfstate_kd', 'left')
                    ->where('f.wfstate_nama !=', 'Cancel')
					->where('b.pr_closeorder', '0');
		if( !empty($rm_kd) ){
			$this->db->where_in('a.rm_kd', $rm_kd);
		}
		$prdetails = $this->db->get()->result_array();
        if (!empty($prdetails)) {
            foreach ($prdetails as $prdetail) {
                $prdetailkds[] = $prdetail['prdetail_kd']; 
            }

            $podetails = $this->db->select()
                        ->from('td_purchaseorder_detail as a')
                        ->join('tm_purchaseorder as b', 'a.po_kd=b.po_kd', 'left')
                        ->where_in('prdetail_kd', $prdetailkds)
                        ->get()->result_array();

            if (!empty($podetails)) {
                /** Group By prdetail_kd */
                $groupsPO = []; $groupResult = [];
                foreach ($podetails as $podetail0) {
                    $key = $podetail0['prdetail_kd'];
                    if (!array_key_exists($key, $groupsPO)) {
                        $groupsPO[$key] = array(
                            'prdetail_kd' => $key,
                            'podetail_qty' => $podetail0['podetail_qty'] * $podetail0['podetail_konversi'] ,
                        );
                    } else {
                        $groupsPO[$key]['podetail_qty'] = (float) $groupsPO[$key]['podetail_qty'] + ($podetail0['podetail_qty'] * $podetail0['podetail_konversi']);
                    }
                }
                foreach ($groupsPO as $eachGroup){
                    $groupResult[] = $eachGroup;
                }
                /** Group By prdetail_kd */

                $arrayResult = [];
                foreach ($prdetails as $prdetail) {
                    $sisaPR = (float) $prdetail['prdetail_qty'] * (float) $prdetail['prdetail_konversi']; 
					$xprdetail_qty = (float) $prdetail['prdetail_qty'] * (float) $prdetail['prdetail_konversi']; 
                    $podetail_qty = 0;
                    foreach ($groupResult as $eachGroupResult) {
                        if ($eachGroupResult['prdetail_kd'] == $prdetail['prdetail_kd']) {
                            $podetail_qty = (float) $eachGroupResult['podetail_qty'];
                            $sisaPR = (float) $xprdetail_qty  - $podetail_qty;
                        }
                    }
                     if ($sisaPR > 0 || $prdetail['wfstate_kd'] != '16' && $prdetail['wfstate_kd'] != '15') {
                        $arrayResult [] = [
                            'prdetail_kd' => $prdetail['prdetail_kd'],
                            'pr_tglinput' => $prdetail['pr_tglinput'],
                            'rm_kd' => $prdetail['rm_kd'],
                            'rm_kode' => $prdetail['rm_kode'],
                            'rm_oldkd' => $prdetail['rm_oldkd'],
                            'prdetail_nama' => $prdetail['prdetail_nama'],
                            'prdetail_deskripsi' => $prdetail['prdetail_deskripsi'],
                            'prdetail_spesifikasi' => $prdetail['prdetail_spesifikasi'],
                            'prdetail_qty' => $prdetail['prdetail_qty'] * $prdetail['prdetail_konversi'],
                            'rmsatuan_nama' => $prdetail['rmsatuan_nama'],
                            'prdetail_duedate' => $prdetail['prdetail_duedate'],
                            'prdetail_remarks' => $prdetail['prdetail_remarks'],
                            'pr_no' => $prdetail['pr_no'],
                            'nm_admin' => $prdetail['nm_admin'],
                            'sum_podetailqty' => $podetail_qty.'/'.$sisaPR,
							'sisa_pr' => $sisaPR,
							'sisa_pr_suggest' => $sisaPR == 0 ? $prdetail['prdetail_qty'] * $prdetail['prdetail_konversi'] : $sisaPR,
                        ];
                     }
                }
                /** Cari kembali detail Qty yang sudah di PO kan */
                $arrayFinalResult = [];
                if (!empty($arrayResult)) {
                    foreach ($arrayResult as $eachResult) {
                        foreach ($podetails as $podetail2) {
                            if ($podetail2['prdetail_kd'] == $eachResult['prdetail_kd']) {
                                $arrayFinalResult[] = [
                                    'prdetail_kd' => $eachResult['prdetail_kd'],
                                    'pr_tglinput' => $eachResult['pr_tglinput'],
                                    'rm_kd' => $eachResult['rm_kd'],
                                    'rm_kode' => $eachResult['rm_kode'],
                                    'rm_oldkd' => $eachResult['rm_oldkd'],
                                    'prdetail_nama' => $eachResult['prdetail_nama'],
                                    'prdetail_deskripsi' => $eachResult['prdetail_deskripsi'],
                                    'prdetail_spesifikasi' => $eachResult['prdetail_spesifikasi'],
                                    'prdetail_qty' => $eachResult['prdetail_qty'],
                                    'rmsatuan_nama' => $eachResult['rmsatuan_nama'],
                                    'prdetail_duedate' => $eachResult['prdetail_duedate'],
                                    'prdetail_remarks' => $eachResult['prdetail_remarks'],
                                    'pr_no' => $eachResult['pr_no'],
                                    'nm_admin' => $eachResult['nm_admin'],
                                    'podetail_qty' => $podetail2['podetail_qty'],
                                    'no_po' => $podetail2['po_no'],
									'sisa_pr' => $sisaPR,
									'sisa_pr_suggest' => $sisaPR,
                                ];
                            }
                        }
                    }
                }

            }
        }

        $data['resultPR'] = $arrayResult;
		$data['resultPO'] = $podetails;
		return $data;
	}

	/** Function for purchase suggest */
	public function outstanding_pr_purchasesuggest($rm_kd = []){
		$outstandingPR = $this->outstanding_pr($rm_kd);
		/** SUM sisa PR berdasarkan rm_kd */
		$groupBy = $this->group_by("rm_kd", $outstandingPR['resultPR']);
		$arrFinish = [];
		$i = 0;
		foreach($groupBy as $groupKey => $groupVal){
			$arrFinish[$i]['rm_kd'] = $groupKey;
			$arrFinish[$i]['sisa_pr'] = array_sum( array_column($groupVal, 'sisa_pr_suggest') );
			$i++;

		}
		$data['resultPR'] = $arrFinish;
		return $data;
	}
	
    /** Fungsi group by */
    private function group_by($key, $data) {
		$result = array();
		foreach($data as $val) {
			if(array_key_exists($key, $val)){
				$result[$val[$key]][] = $val;
			}else{
				$result[""][] = $val;
			}
		}
		return $result;
	}

}