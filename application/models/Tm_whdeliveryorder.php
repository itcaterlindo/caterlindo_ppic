<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_whdeliveryorder extends CI_Model {
	private $tbl_name = 'tm_whdeliveryorder';
	private $p_key = 'whdeliveryorder_kd';
	private $qKaryawans;

	public function ssp_table() {
		$this->load->model(['db_hrm/Tb_karyawan']);
		$this->qKaryawans = $this->Tb_karyawan->get_all()->result_array();

		$data['table'] = $this->tbl_name;

		$data['primaryKey'] = $this->p_key;

		$data['columns'] = array(
			array( 'db' => $this->p_key,
				'dt' => 1, 'field' => $this->p_key,
				'formatter' => function($d, $row){
					
					return $this->tbl_btn($d, $row[9]);
				}),
			array( 'db' => 'a.whdeliveryorder_tanggal', 
				'dt' => 2, 'field' => 'whdeliveryorder_tanggal',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
				}),
			array( 'db' => 'a.whdeliveryorder_no', 
				'dt' => 3, 'field' => 'whdeliveryorder_no',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
				array( 'db' => 'a.set_name', 
				'dt' => 4, 'field' => 'set_name',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			// array( 'db' => 'a.kd_karyawan', 
			// 	'dt' => 4, 'field' => 'kd_karyawan',
			// 	'formatter' => function ($d, $row){
			// 		if (!empty($d)){
			// 			foreach ($this->qKaryawans as $karyawan) {
			// 				if ($karyawan['kd_karyawan'] == $d){
			// 					$d = $karyawan['nm_karyawan'];
			// 				}
			// 			}
			// 		}else{
			// 			if (!empty($row[7])){
			// 				$d = $row[7].', '.$row[8];
			// 			}else{
			// 				$d = $row[7];
			// 			}
			// 		}
			// 		$d = "$d $row[10]";
			// 		$d = $this->security->xss_clean($d);

			// 		return $d;
            //     }),
			array( 'db' => 'a.whdeliveryorder_sendback', 
				'dt' => 5, 'field' => 'whdeliveryorder_sendback',
				'formatter' => function ($d){
					if (!empty($d)){
						$d = 'Ya';
					}else{
						$d = 'Tidak';
					}
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'b.wfstate_nama', 
				'dt' => 6, 'field' => 'wfstate_nama',
				'formatter' => function ($d, $row){
					$d = build_span ($row[6], $d);
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'b.wfstate_spancolor', 
				'dt' => 7, 'field' => 'wfstate_spancolor',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'c.suplier_nama', 
				'dt' => 8, 'field' => 'suplier_nama',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'd.nm_select', 
				'dt' => 9, 'field' => 'nm_select',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'b.wfstate_action', 
				'dt' => 10, 'field' => 'wfstate_action',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
			array( 'db' => 'a.whdeliveryorder_note', 
				'dt' => 11, 'field' => 'whdeliveryorder_note',
				'formatter' => function ($d){
					$d = $this->security->xss_clean($d);

					return $d;
                }),
		);

		$data['sql_details'] = sql_connect();
		$data['joinQuery'] = "FROM ".$this->tbl_name." AS a
								LEFT JOIN td_workflow_state as b ON a.wfstate_kd=b.wfstate_kd
								LEFT JOIN tm_suplier as c ON a.suplier_kd = c.suplier_kd
								LEFT JOIN tb_set_dropdown as d ON c.badanusaha_kd=d.id";
        $data['where'] = "";
		
		return $data;
	}

	private function tbl_btn($id, $wfstate_action) {
		$btns = array();
		if (!empty($wfstate_action)){
			$exp = explode(';', $wfstate_action);
			if (in_array('view', $exp)){
				$btns[] = get_btn(array('title' => 'Lihat Item', 'icon' => 'search', 'onclick' => 'view_box(\''.$id.'\')'));	
			}
			if (in_array('edit', $exp)){
				$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));	
			}
			if (in_array('print', $exp)){
				$btns[] = get_btn(array('title' => 'Cetak Item', 'icon' => 'print', 'onclick' => 'view_pdf(\''.$id.'\')'));	
			}
		}
		$btn_group = group_btns($btns);

		return $btn_group;
	}

    public function create_code () {
		$query = $this->db->select('MAX('.$this->p_key.') as maxID')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

    public function create_no () {
		$query = $this->db->select('MAX(whdeliveryorder_no) as maxNO')
				->get($this->tbl_name)
				->row();
		$code = (int) $query->maxNO + 1;
		return $code;
	}

    public function insert_data ($data){
		$query = $this->db->insert($this->tbl_name, $data);
		return $query?TRUE:FALSE;
	}
	
	public function delete_data($id) {
		$query = $this->db->delete($this->tbl_name, array($this->p_key => $id)); 
		return $query?TRUE:FALSE;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all(){
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data ($aWhere=[], $data){
        $query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query?TRUE:FALSE;
	}	
    
    public function insert_batch($data){
        $act = $this->db->insert_batch($this->tbl_name, $data);
        return $act;
	}

	public function get_where_in ($where, $in = []) {
		$query = $this->db->where_in($where, $in)
				->select($this->tbl_name.'.*')
				->get($this->tbl_name);
		return $query;
	}

	public function update_batch ($idColumn, $data = []) {
		$act = $this->db->update_batch($this->tbl_name, $data, $idColumn);
		return $act;
	}
	
}