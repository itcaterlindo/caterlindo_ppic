<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tb_inspectionproduct_user extends CI_Model
{
	private $tbl_name = 'tb_inspectionproduct_user';
	private $p_key = 'inspectionproductuser_kd';

	public function ssp_table2()
	{
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		$userdataAdmin = $this->session->userdata('kd_admin');

		$query = $this->db->from($this->tbl_name)
				->join('tb_admin', 'tb_admin.kd_admin='.$this->tbl_name.'.admin_kd', 'left')
				->join('td_admin_tipe', 'td_admin_tipe.kd_tipe_admin=tb_admin.tipe_admin_kd', 'left')
				->join('tb_bagian', 'tb_bagian.bagian_kd='.$this->tbl_name.'.kd_bagian', 'left')->get();

		$qData = $query->result_array();
		$data = [];
		foreach ($qData as $r) {

			$spanBagian = ''; $spanPenyebab = ''; $spanRepair = '';
			if($r['approved_bagian'] == 1){
				$spanBagian = build_span('primary', 'Ya');
			}else{
				$spanBagian = build_span('danger', 'Tidak');
			}

			if($r['approved_penyebab'] == 1){
				$spanPenyebab = build_span('primary', 'Ya');
			}else{
				$spanPenyebab = build_span('danger', 'Tidak');
			}
			
			if($r['approved_repair'] == 1){
				$spanRepair = build_span('primary', 'Ya');
			}else{
				$spanRepair = build_span('danger', 'Tidak');
			}
			
			$data[] = [
				'1' => $this->tbl_btn($r['inspectionproductuser_kd'], $r['admin_kd']),
				'2' => $r['nm_admin'],
				'3' => $r['nm_tipe_admin'],
				'4' => $r['bagian_nama'],
				'5' => $spanBagian,
				'6' => $spanPenyebab,
				'7' => $spanRepair,
				'8' => $r['inspectionproductuser_tgledit'],
			];
		}

		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

	private function tbl_btn($id, $admin_kd)
	{
		$btns = array();
		$btns[] = get_btn(array('title' => 'Hapus Data', 'icon' => 'trash', 'onclick' => 'hapus_data(\'' . $id . '\' , \'' . $admin_kd . '\')'));

		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function create_code()
	{
		$query = $this->db->select('MAX(' . $this->p_key . ') as maxID')
			->get($this->tbl_name)
			->row();
		$code = (int) $query->maxID + 1;
		return $code;
	}

	public function insert_data($data)
	{
		$query = $this->db->insert($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_data($array = [])
	{
		$query = $this->db->delete($this->tbl_name, $array);
		return $query ? TRUE : FALSE;
	}

	public function get_by_param($param = [])
	{
		$this->db->join('td_inspectionproduct_userstate', 'td_inspectionproduct_userstate.inspectionproductuser_kd = '.$this->tbl_name.'.inspectionproductuser_kd');
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}

	public function get_all()
	{
		$act = $this->db->order_by($this->p_key)->get($this->tbl_name);
		return $act;
	}

	public function update_data($aWhere = [], $data)
	{
		$query = $this->db->update($this->tbl_name, $data, $aWhere);
		return $query ? TRUE : FALSE;
	}

	public function insert_batch_data($data = [])
	{
		$query = $this->db->insert_batch($this->tbl_name, $data);
		return $query ? TRUE : FALSE;
	}

	public function delete_by_param($param = [])
	{
		$query = $this->db->delete($this->tbl_name, $param);
		return $query ? TRUE : FALSE;
	}

	public function get_where($where = [])
	{
		$query = $this->db->where($where)->get($this->tbl_name);
		return $query;
	}

	public function get_where_in($where, $in = [])
	{
		$query = $this->db->where_in($where, $in)->get($this->tbl_name);
		return $query;
	}
}
