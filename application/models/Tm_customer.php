<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tm_customer extends CI_Model {
	private $tbl_name = 'tm_customer';
	private $p_key = 'kd_customer';

	/*
	** Fungsi ini digunakan untuk membuat code_customer
	** Jika ada customer dengan nama hampir sama, maka increment akan digunakan
	*/
	public function create_customer_code($name = '') {
		/** OLD FUNCTION */
		// $this->db->select('code_customer')
		// 	->from($this->tbl_name)
		// 	->like(array('code_customer' => $name))
		// 	->order_by('tgl_input DESC, code_customer DESC');
		// $query = $this->db->get();
		// $row = $query->row();
		// if (!empty($row)) :
		// 	$urutan = substr($row->code_customer, 4, 2);
		// else :
		// 	$urutan = '0';
		// endif;
		
		// $angka = $urutan + 1;
		
		// $code_customer = $name.str_pad($angka, '2', '000', STR_PAD_LEFT);
		// return $code_customer;

		/** NEW FUNCTION REFFERENCE BY CREATE CODE TM_SUPLIER */
		$customernama = strtoupper(str_replace(' ', '', $name));
		$customer_code = substr($customernama, 0,4);

		$query = $this->db->select('MAX(code_customer) AS code')
			->from($this->tbl_name)
			->like('code_customer', $customer_code, 'after')
			->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, 4, 2);
		endif;
		$angka = $urutan + 1;
		$primary = $customer_code.str_pad($angka, 2, '000', STR_PAD_LEFT);
		return $primary;
	}

	public function get_row($kd) {
		$this->db->from($this->tbl_name)
			->where(array($this->p_key => $kd));
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}

	public function get_all() {
		$result = $this->db->order_by('tgl_input', 'desc')->get($this->tbl_name);
		return $result;
	}

	public function get_by_param ($param=[]) {
		$this->db->where($param);
		$act = $this->db->get($this->tbl_name);
		return $act;
	}
}