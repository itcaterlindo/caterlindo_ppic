<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$box_id = (isset($box_id) and !empty($box_id))?$box_id:'idBoxDefault';
$box_status = (isset($box_status) and !empty($box_status))?$box_status:'default';
$box_title = (isset($box_title) and !empty($box_title))?$box_title:'Box Default Title';
$form_data = (isset($form_data) and !empty($form_data))?$form_data:'';
$idbox_overlay = (isset($idbox_overlay) and !empty($idbox_overlay))?$idbox_overlay:'';
$idbox_loader = (isset($idbox_loader) and !empty($idbox_loader))?$idbox_loader:'idBoxLoaderDefault';
$idbox_content = (isset($idbox_content) and !empty($idbox_content))?$idbox_content:'idBoxContentDefault';
?>

<div class="box box-<?php echo $box_status; ?>" id="<?php echo $box_id; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_title; ?></h3>
		<div class="box-tools pull-right">
			<?php
			if (isset($btns) and !empty($btns)) :
				foreach ($btns as $btn_name => $btn_id) :
					$btn_id = !empty($btn_id)?$btn_id:'idBtnDefault';
					if ($this->session->create_access == '1' && $btn_name == 'btn_add') :
						?>
						<button class="btn btn-box-tool" id="<?php echo $btn_id; ?>" data-toggle="tooltip" title="Tambah">
							<i class="fa fa-plus"></i>
						</button>
						<?php
					elseif ($btn_name == 'btn_hide') :
						?>
						<button class="btn btn-box-tool btn-collapse" id="<?php echo $btn_id; ?>" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
							<i class="fa fa-minus"></i>
						</button>
						<?php
					elseif ($btn_name == 'btn_close') :
						?>
						<button class="btn btn-box-tool btn-remove" id="<?php echo $btn_id; ?>" data-widget="remove" data-toggle="tooltip" title="Close">
							<i class="fa fa-times"></i>
						</button>
						<?php
					endif;
				endforeach;
			else :
				?>
				<button class="btn btn-box-tool btn-collapse" id="idBtnCollapseDefault" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
					<i class="fa fa-minus"></i>
				</button>
				<button class="btn btn-box-tool btn-remove" id="idBtnRemoveDefault" data-widget="remove" data-toggle="tooltip" title="Close">
					<i class="fa fa-times"></i>
				</button>
				<?php
			endif;
			?>
		</div>
	</div>

	<div class="box-body">
		<div id="<?php echo $idbox_loader; ?>" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="<?php echo $tbl_alertid; ?>"></div>
		<div id="<?php echo $idbox_content; ?>" style="display: none;">
			<?php
			if (isset($box_body) and !empty($box_body)) :
				$this->load->view($box_body, $form_data);
			endif;
			?>
		</div>
	</div>
	<?php
	if (!empty($idbox_overlay)) :
		?>
		<div class="overlay" id="<?php echo $idbox_overlay; ?>" style="display: none;">
			<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
		</div>
		<?php
	endif;
	?>
</div>