<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$tbl_id = isset($tbl_id)?$tbl_id:'idTableDataTables';
$tbl_alertid = isset($tbl_alertid)?$tbl_alertid:'idTableAlertDefault';
$tbl_opsi = isset($tbl_opsi)?$tbl_opsi:TRUE;
$tbl_uri = isset($tbl_uri)?$tbl_uri:'';
$tbl_order = isset($tbl_order)?$tbl_order:'';
$tbl_responsive = isset($tbl_responsive)?$tbl_responsive:FALSE;
?>
<table id="<?php echo $tbl_id; ?>" class="table table-bordered table-striped table-hover display nowrap" style="width:100%;">
    <thead>
        <tr>
            <th style="width:1%; text-align:center;" class="all">No.</th>
            <?php
            if ($tbl_opsi == TRUE) :
                ?>
                <th style="width:1%; text-align:center;" class="all">Opsi</th>
                <?php
            endif;
            
            if (isset($tbl_header) && is_array($tbl_header)) :
                foreach ($tbl_header as $value) :
                    echo $value;
                endforeach;
            endif;
            ?>
        </tr>
    </thead>
</table>

<script type="text/javascript">
    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };
    var table = $('#<?php echo $tbl_id; ?>').DataTable({
        "scrollX": true,
        "scrollY": "600px",
        "processing": true,
        "serverSide": true,
        "ordering" : true,
        <?php
        if (isset($dt_var) && is_array($dt_var) && !empty($dt_var)) :
            foreach ($dt_var as $var => $val) :
                echo '\''.$var.'\': '.$value;
            endforeach;
        endif;
        ?>
        "ajax": "<?php echo base_url().$tbl_uri; ?>",
        "language" : {
            "lengthMenu" : "Tampilkan _MENU_ data",
            "zeroRecords" : "Maaf tidak ada data yang ditampilkan",
            "info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
            "infoFiltered": "(difilter dari _MAX_ total data)",
            "infoEmpty" : "Tidak ada data yang ditampilkan",
            "search" : "Cari :",
            "loadingRecords": "Memuat Data...",
            "processing":     "Sedang Memproses...",
            "paginate": {
                "first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
                "last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
                "next":       '<span class="glyphicon glyphicon-forward"></span>',
                "previous":   '<span class="glyphicon glyphicon-backward"></span>'
            }
        },
        "columnDefs": [
            {"data": null, "searchable": false, "orderable": false, "targets": 0},
            <?php
            if ($tbl_opsi == TRUE) :
                ?>
                {"searchable": false, "orderable": false, "targets": 1},
                <?php
            endif;
            if (isset($col_def) && is_array($col_def) && !empty($col_def)) :
                $no = 0;
                foreach ($col_def as $column => $properties) :
                    $column = '{\'data\': \''.$column.'\'';
                    foreach ($properties as $property => $val) :
                        $column .= ', \''.$property.'\': '.$val;
                    endforeach;
                    $column .= '},';
                endforeach;
            endif;
            ?>
        ],
        "order": <?php echo $tbl_order; ?>,
        "rowCallback": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        }
    });
</script>