<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataProfile';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>
<div class="form-group">
	<label for="idTxtNm" class="col-md-2 control-label">Nama</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrNm"></div>
		<?php echo form_input(array('name' => 'txtNm', 'id' => 'idTxtNm', 'class' => 'form-control', 'value' => $row->nm_admin, 'placeholder' => 'Nama')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtUsername" class="col-md-2 control-label">Username</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrUsername"></div>
		<?php echo form_input(array('name' => 'txtUsername', 'id' => 'idTxtUsername', 'class' => 'form-control', 'value' => $row->username, 'placeholder' => 'Username')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtPass" class="col-md-2 control-label">Password</label>
	<div class="col-sm-3 col-xs-12">
		<?php echo buildLabel('warning', 'Kosongkan jika password tidak dirubah!'); ?>
		<div id="idErrPass"></div>
		<?php echo form_password(array('name' => 'txtPassword', 'id' => 'idTxtPass', 'class' => 'form-control', 'placeholder' => 'Password')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtPassConf" class="col-md-2 control-label">Konfirmasi Password</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrPassConf"></div>
		<?php echo form_password(array('name' => 'txtPassConf', 'id' => 'idTxtPassConf', 'class' => 'form-control', 'placeholder' => 'Konfirmasi Password')); ?>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" class="btn btn-primary btn-flat">
			<i class="fa fa-shopping-cart"></i> Submit
		</button>
	</div>
</div>
<?php echo form_close(); ?>