<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="error-container">
	<div class="well">
		<h1 class="grey lighter smaller">
			<span class="blue bigger-125">
				<i class="ace-icon fa fa-exclamation-triangle"></i>
			</span>
			<span class="red">PAGE Under Construction!</span>
		</h1>

		<hr />
		<h3 class="lighter smaller">This page is under construction. Please come back soon!</h3>

		<div>
			<img src="<?php echo base_url('assets/admin_assets/dist/img/under_construction.png'); ?>" width="100%" height="25%">
		</div>

		<hr />
		<div class="space"></div>

		<div class="center">
			<a href="javascript:history.back()" class="btn btn-grey">
				<i class="ace-icon fa fa-arrow-left"></i>
				Go Back
			</a>

			<a href="<?php echo site_url('home'); ?>" class="btn btn-primary">
				<i class="ace-icon fa fa-tachometer"></i>
				Dashboard
			</a>
		</div>
	</div>
</div>