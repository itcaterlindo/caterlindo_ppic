<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');
	var id = 0; // Untuk identitas PO agar bisa terhapus

	// Initiate select2
	$(document).ready(function(e){
		$('#idSelQuo').select2({
			theme: 'bootstrap',
			width: '100%',
			minimumInputLength: 2,
			allowClear: true,
			placeholder: 'Masukkan No. PO',
			ajax: {
				type: 'POST',
				dataType: 'JSON',
				url: '<?= base_url().$class_link."/get_value_PO" ?>',
				delay: 800,
				data: function(params) {
					return {
						search: params.term
					}
				},
				processResults: function (data, page) {
				return {
					results: data
				};
				},
			}
		});
	});
	

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table() {
		var bulan = $('#idtxtbulan').val();
		var tahun = $('#idtxttahun').val();
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_main'; ?>',
				data: {bulan:bulan, tahun:tahun},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function addPO() {
		event.preventDefault();
		var NoPO = $('#idSelQuo').val();
		if(NoPO == null || NoPO == ""){
			alert('No. PO tidak boleh kosong');
		}else{
			// $('#listPilihPO').append('<tr id="PO_'+ NoPO +'" ">'+ NoPO +' </li>'); //onClick="deletePO(PO_'+ NoPO +')"
			var PO = '<td>'+NoPO+'</td>';
			var button = '<td><button class="btn btn-sm btn-danger" onclick="deletePO(PO_'+id+')" >Hapus</button></td>';
			$('#tableListPO > tbody').append('<tr id="PO_'+id+'">'+ PO + button +'<tr>');
			$('#idSelQuo').val(null).trigger('change');
			id++;
		}
	}

	function deletePO(li_ID){
		event.preventDefault();
		$(li_ID).remove();
	}

	function exportExcel(){
		event.preventDefault();

		var rowDataArray = [];
		$("#tableListPO tbody tr").each(function() {
			var actualData = $(this).find('td:eq(0)');
			if (actualData.length > 0) {
				actualData.each(function() {
					rowDataArray.push($(this).text());
				});
			}
		});

		$.ajax({
			url: '<?= base_url().$class_link."/generateExcel" ?>',
			type: 'POST',
			dataType: "JSON",
			data: {no_po:rowDataArray},
			success: function(res){
				if(res.status == "OK"){
					window.location.href = res.url;
				}else if(res.status == "Error"){
					alert('Tidak bisa export excel karena data kosong !!');
				}
			}
		});

	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
		box_overlay('out');
    }

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

</script>