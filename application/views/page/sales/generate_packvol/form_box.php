<?php
defined('BASEPATH') or exit('No direct script access allowed!');

/* --SET DEFAULT PROPERTY UNTUK BOX-- */
$js_file = 'form_js';
$box_type = 'Form';
$box_title = 'Form Generate PackVol';
$data['master_var'] = 'GeneratePackVol';
$data['class_link'] = $class_link;
$data['box_id'] = 'id'.$box_type.'Box'.$data['master_var'];
$data['btn_hide_id'] = 'id'.$box_type.'BtnBoxHide'.$data['master_var'];
$data['btn_add_id'] = 'id'.$box_type.'BtnBoxAdd'.$data['master_var'];
$data['btn_remove_id'] = 'id'.$box_type.'BtnBoxRemove'.$data['master_var'];
$data['box_alert_id'] = 'id'.$box_type.'BoxAlert'.$data['master_var'];
$data['box_loader_id'] = 'id'.$box_type.'BoxLoader'.$data['master_var'];
$data['box_content_id'] = 'id'.$box_type.'BoxContent'.$data['master_var'];
$data['box_overlay_id'] = 'id'.$box_type.'BoxOverlay'.$data['master_var'];
/* --END OF BOX DEFAULT PROPERTY-- */

/* --SET VARIABEL TAMBAHAN DISINI-- */
/* --END OF CUSTOM PROPERTY-- */

/* --SET BUTTONS DISINI, NILAI "FALSE" UNTUK TIDAK TAMPIL, NILAI "TRUE" UNTUK MENAMPILKAN BUTTON-- */
$btn_hide = TRUE;
$btn_add = FALSE;
$btn_remove = FALSE;
/* --END OF BUTTONS SETUP-- */
?>

<div class="box box-primary" id="<?php echo $data['box_id']; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_title; ?></h3>
		<div class="box-tools pull-right">
			<?php
			if ($btn_add) :
				?>
				<button class="btn btn-box-tool" id="<?php echo $data['btn_add_id']; ?>" data-toggle="tooltip" title="Tambah Data">
					<i class="fa fa-plus"></i>
				</button>
				<?php
			endif;
			if ($btn_hide) :
				?>
				<button class="btn btn-box-tool btn-collapse" id="<?php echo $data['btn_hide_id']; ?>" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
					<i class="fa fa-minus"></i>
				</button>
				<?php
			endif;
			if ($btn_remove) :
				?>
				<button class="btn btn-box-tool btn-remove" id="<?php echo $data['btn_remove_id']; ?>" data-widget="remove" data-toggle="tooltip" title="Close">
					<i class="fa fa-times"></i>
				</button>
				<?php
			endif;
			?>
		</div>
	</div>
	<div class="box-body">
		<div id="<?php echo $data['box_alert_id']; ?>"></div>
		<div id="<?php echo $data['box_loader_id']; ?>" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>

		<div class="row">
			<form action="/caterlindo_ppic/sales/sales_orders/sales_orders_form/form_select" id="idForm" class="form-horizontal" style="width:100%;" method="post" accept-charset="utf-8">
				<div class="form-group">
					<label for="idSelQuo" class="col-md-2 control-label">No. PO</label>
					<div class="col-md-3 col-xs-11">
						<div id="idErrNoPO"></div>
						<select name="selQuo[]" id="idSelQuo" class="form-control select2" tabindex="-1" aria-hidden="true">
							<option value="">-- Pilih No. PO --</option>
							<option value="NZ001">NZ001</option>
							<option value="NZ002">NZ002</option>
							<option value="NZ003">NZ003</option>
							<option value="NZ004">NZ004</option>
							<option value="NZO05">NZ005</option>
							<option value="NZ006">NZ006</option>
						</select>
					</div>
					<div class="col-md-1 col-xs-1">
						<button type="button" id="btnTambahQuo" onclick="addPO()" class="btn btn-primary btn-flat" title="Tambahkan PO">Tambah</button>
					</div>
					<div class="col-md-1 col-xs-1">
						<button type="button" id="btnExportExcel" onclick="exportExcel()" class="btn btn-success btn-flat" title="Export Excel">Export Excel</button>
					</div>
				</div>
			</form>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-6">
				<table class="table" id="tableListPO">
					<thead>
						<tr>
							<th style="width: 20%">No. PO</th>
							<th style="width: 10%">#</th>
						</tr>
					</thead>
					<tbody>
						<!-- Untuk list PO yang dipilih -->
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="box-footer">
	</div>
	<div class="overlay" id="<?php echo $data['box_overlay_id']; ?>" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
	<?php $this->load->view('page/'.$class_link.'/'.$js_file, $data); ?>
</div>