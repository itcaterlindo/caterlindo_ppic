<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form('<?php echo $id; ?>', '<?php echo $msalesorder_kd; ?>', '<?php echo $tipe_do; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#idTableBoxLoader<?php echo $master_var; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
		open_table();
		first_load('<?php echo 'idTableBoxLoader'.$master_var; ?>', '<?php echo 'idTableBoxContent'.$master_var; ?>');
	});

	$(document).off('change', '#idSelNotify').on('change', '#idSelNotify', function() {
		var notify_name = $(this).val();
		$('.field-notify-address').slideUp(function() {
			$(this).html('');
		});
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/get_notify_address'; ?>',
			data: 'notify_name='+notify_name,
			success: function(data) {
				$('.field-notify-address').slideDown(function() {
					$(this).html(data.notify_address);
				});
				$('#idTxtNotifyAddress').val(data.notify_address);
				$('#idTxtFreight').focus();
			}
		});
	});

	/* --start jasakirim typeahead.js-- */
	var Jasakirim = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		url: '<?php echo base_url(); ?>auto_complete/jasakirim_detail',
		remote: {
			url: '<?php echo base_url(); ?>auto_complete/jasakirim_detail',
			prepare: function (query, settings) {
				settings.type = 'GET';
				settings.contentType = 'application/json; charset=UTF-8';
				settings.data = 'nm_jasakirim='+query;

				return settings;
			},
			wildcard: '%QUERY'
		}
	});
	/* --end of jasakirim typeahead.js-- */

	$(document).off('submit', '#idForm<?php echo $master_var; ?>').on('submit', '#idForm<?php echo $master_var; ?>', function(e) {
		e.preventDefault();
		submit_form(this);
	});

	function open_form(id, kd_mso, tipe) {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/open_form'; ?>',
				data: 'id='+id+'&kd_msalesorder='+kd_mso+'&tipe='+tipe,
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					$('.select2').select2({
						theme: 'bootstrap',
						width: '100%'
					});
					$('.datepicker').datetimepicker({
						format: 'DD-MM-YYYY',
					});
					$('.timepicker').timepicker({
						showInputs: false,
						showMeridian: false,
						defaultTime: false,
					});
					/* --start properties for jasakirim typeahead.js-- */
					$('#idTxtFreight').typeahead(null, {
						limit: 10,
						minLength: 1,
						name: 'jasakirim_search',
						display: 'nm_jasakirim',
						valueKey: 'get_jasakirim',
						source: Jasakirim.ttAdapter()
					});

					$('#idTxtFreight').bind('typeahead:select', function(obj, selected) {
						$('#idFormAlamatJasaKirim').slideDown(function() {
							$('#idLabelAlamatJasaKirim').html(selected.detail_alamat);
						});
						$('#idTxtAlamatJasaKirim').val(selected.detail_alamat);
					});
					/* --end properties for jasakirim typeahead.js-- */
					moveTo('idMainContent');
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function close_form() {
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$('#<?php echo $box_id; ?>').fadeOut(function() {
				$('#<?php echo $box_id; ?>').remove();
			});
		});
	}

	function submit_form(form_id) {
		$('#<?php echo $box_overlay_id; ?>').show();
		$('#<?php echo $box_alert_id; ?>').html('');
		$('#idErrAttachment').html('');
		<?php
		foreach ($form_errs as $form_err) {
			echo '$(\'#'.$form_err.'\').html(\'\');';
		}
		?>
		$.ajax({
			url: "<?php echo base_url().$class_link.'/send_data'; ?>",
			type: "POST",
			data:  new FormData(form_id),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				if (data.confirm == 'success') {
					close_form();
					$('#idTableBtnBoxAdd<?php echo $master_var; ?>').show();
					$('#idTableBoxAlert<?php echo $master_var; ?>').html(data.alert).fadeIn();
					open_table();
					if (data.open_item == 'yes') {
						show_items(data.kd_mdo);
					}
				}
				if (data.confirm == 'error') {
					$('#idTxtFreight').focus();
					$('#<?php echo $box_alert_id; ?>').html(data.alert);
					$('#idErrAttachment').html(data.idErrAttachment);
					<?php
					foreach ($form_errs as $form_err) {
						echo '$(\'#'.$form_err.'\').html(data.'.$form_err.');';
					}
					?>
				}
				$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
				$('#<?php echo $box_overlay_id; ?>').hide();
			}
		});
	}

	function show_items(id) {
		// $('#idTableSalesAlert').fadeOut('slow', function() {
		// 	$(this).html('');
		// });
		// $('#idFormItemBoxDataDO').remove();
		// $('#idFormBoxDataDO').remove();
		// $('#idFormBoxDataSO').remove();
		// $('#idFormBoxDataSOItem').remove();
		// $('#idBoxFormSalesOrder').remove();
		// $('#idBoxForm').remove();
		// $('#idBtnAddData').slideDown();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'sales/delivery_orders/items_do/get_form'; ?>',
			data: 'id='+id,
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function get_container_data()
	{
		var id = $('#idTxtContainer').val();
		$.ajax({
			url: '<?php echo base_url().'sales/set_container/get_data_byid'; ?>',
			type: 'GET',
			dataType: 'JSON',
			data: 'id='+id,
			success: function(res) {
				var nama = res.container.nm_container;
				var konversi_20ft = res.container.konversi_20ft;							
				$('#idTxtTipeContainer').val(nama);
				$('#idTxtContainer20ft').val(konversi_20ft);
			}
		});
	}


</script>