<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataDO';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtKdMDo', 'value' => $kd_mdo));
echo form_input(array('type' => 'hidden', 'name' => 'txtKdMSo', 'value' => $msalesorder_kd));
echo form_input(array('type' => 'hidden', 'name' => 'txtTipeDo', 'value' => $tipe_do));
if ($tipe_do == 'Ekspor') :
	?>
	<div class="form-group">
		<label for="idTxtNotifyName" class="col-md-2 control-label">Notify Name</label>
		<div class="col-sm-4 col-xs-12" style="margin-top: 8px">
			<div id="idErrNotifyName"></div>
			<?php echo form_dropdown('selNotify', $notify_opts, $notify_name, ['id' => 'idSelNotify', 'class' => 'form-control']); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="idTxtNotifyAddress" class="col-md-2 control-label">Notify Address</label>
		<div class="col-sm-6 col-xs-12">
			<div id="idErrNotifyAddress"></div>
			<div class="field-notify-address" style="padding: 6px 12px; display: block;"><?php echo $notify_address; ?></div>
			<?php echo form_input(array('type' => 'hidden', 'name' => 'txtNotifyAddress', 'id' => 'idTxtNotifyAddress', 'value' => $notify_address)); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="idTxtFreight" class="col-md-2 control-label">Dikirim Oleh</label>
		<div class="col-sm-3 col-xs-12">
			<div id="idErrFreight"></div>
			<div id="scrollable-dropdown-menu">
				<?php echo form_input(array('name' => 'txtFreight', 'id' => 'idTxtFreight', 'class' => 'form-control', 'value' => $nm_jasakirim, 'placeholder' => 'Dikirim Oleh', 'autofocus' => 'autofocus')); ?>
			</div>
			<?php echo form_input(array('type' => 'hidden', 'name' => 'txtAlamatJasaKirim', 'id' => 'idTxtAlamatJasaKirim', 'value' => $alamat_jasakirim)); ?>
		</div>
	</div>
	<div id="idFormAlamatJasaKirim" class="form-group" <?php echo empty($alamat_jasakirim)?'style="display: none;"':''; ?>>
		<label for="idLabelAlamatJasaKirim" class="col-md-2 control-label">Alamat Jasa Pengiriman</label>
		<div class="col-sm-6 col-xs-12">
			<div id="idLabelAlamatJasaKirim"><?php echo $alamat_jasakirim; ?></div>
		</div>
	</div>
	<div class="form-group">
		<label for="idTxtNoKendaraan" class="col-md-2 control-label">No Kendaraan</label>
		<div class="col-sm-3 col-xs-12">
			<div id="idErrNoKendaraan"></div>
			<?php echo form_input(array('name' => 'txtNoKendaraan', 'id' => 'idTxtNoKendaraan', 'class' => 'form-control', 'value' => $do_trucknumber, 'placeholder' => 'No Kendaraan')); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="idTxtSealNumber" class="col-md-2 control-label">Seal Number</label>
		<div class="col-sm-3 col-xs-12">
			<div id="idErrSealNumber"></div>
			<?php echo form_input(array('name' => 'txtSealNumber', 'id' => 'idTxtSealNumber', 'class' => 'form-control', 'value' => $seal_number, 'placeholder' => 'Seal Number')); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="idTxtContainerNumber" class="col-md-2 control-label">Container Number</label>
		<div class="col-sm-3 col-xs-12">
			<div id="idErrContainerNumber"></div>
			<?php echo form_input(array('name' => 'txtContainerNumber', 'id' => 'idTxtContainerNumber', 'class' => 'form-control', 'value' => $container_number, 'placeholder' => 'Container Number')); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="idTxtGoodsDesc" class="col-md-2 control-label">Desc of Goods</label>
		<div class="col-sm-3 col-xs-12">
			<div id="idErrContainerNumber"></div>
			<?php echo form_input(array('name' => 'txtGoodDesc', 'id' => 'idTxtGoodsDesc', 'class' => 'form-control', 'value' => $goods_desc, 'placeholder' => 'Desc of Goods')); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="idTxtGoodsDesc" class="col-md-2 control-label">Vessel</label>
		<div class="col-sm-3 col-xs-12">
			<div id=""></div>
			<?php echo form_input(array('name' => 'txtVessel', 'id' => 'txtVessel', 'class' => 'form-control', 'value' => $vessel, 'placeholder' => 'Vessel')); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="idTxtGoodsDesc" class="col-md-2 control-label">Etd Sub</label>
		<div class="col-sm-3 col-xs-12">
			<div id=""></div>
			<?php echo form_input(array('name' => 'txtEtdSub', 'id' => 'txtEtdSub', 'class' => 'form-control datepicker', 'value' => $etd_sub, 'placeholder' => 'Etd Sub')); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="idTxtGoodsDesc" class="col-md-2 control-label">Etd Sin</label>
		<div class="col-sm-3 col-xs-12">
			<div id=""></div>
			<?php echo form_input(array('name' => 'txtEtdSin', 'id' => 'txtEtdSin', 'class' => 'form-control datepicker', 'value' => $etd_sin, 'placeholder' => 'Etd Sin')); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="idTxtGoodsDesc" class="col-md-2 control-label">Eta</label>
		<div class="col-sm-3 col-xs-12">
			<div id=""></div>
			<?php echo form_input(array('name' => 'txtEta', 'id' => 'txtEta', 'class' => 'form-control datepicker', 'value' => $eta, 'placeholder' => 'Eta')); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="idTxtGoodsDesc" class="col-md-2 control-label">Container Arrived</label>
		<div class="col-sm-3 col-xs-12">
			<div id=""></div>
			<div class="bootstrap-timepicker">
				<?php echo form_input(array('name' => 'txtContainerArrived', 'id' => 'idContainerArrived', 'class' => 'form-control timepicker', 'value' => $container_arrived, 'placeholder' => 'Container Arrived')); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="idTxtGoodsDesc" class="col-md-2 control-label">No. B/L</label>
		<div class="col-sm-3 col-xs-12">
			<div id=""></div>
			<?php echo form_input(array('name' => 'txtNoBl', 'id' => 'txtNoBl', 'class' => 'form-control', 'value' => $no_bl, 'placeholder' => 'No. B/L')); ?>
		</div>
	</div>

	<div class="form-group">
		<label for="idTxtContainer" class="col-md-2 control-label">Container</label>
		<div class="col-md-3">
			<div id="idErrContainer"></div>
			<div id=""><?php echo form_dropdown('txtContainer', $container, '', array('name' => 'txtContainer', 'id' => 'idTxtContainer', 'class' => 'form-control select2', 'onchange' => 'get_container_data()')); ?></div>			
		</div>
	</div>

	<div class="form-group">
		<label for="idTxtTipeContainer" class="col-md-2 control-label">Tipe Container</label>
		<div class="col-md-2">
			<div id="idErrTipeContainer"></div>
			<div id=""><?php echo form_input(array('name' => 'txtTipeContainer', 'id' => 'idTxtTipeContainer', 'class' => 'form-control', 'placeholder' => 'Tipe Container', 'value' => $tipe_container, 'readonly' => true)); ?></div>
		</div>
		<div class="col-md-1">
			<div id="idErrContainer20ft"></div>
			<div id=""><?php echo form_input(array('name' => 'txtContainer20ft', 'id' => 'idTxtContainer20ft', 'class' => 'form-control', 'placeholder' => '20ft', 'value' => $container_20ft, 'readonly' => true)); ?></div>
		</div>
	</div>

	<?php
endif;
?>
<div class="form-group">
	<label for="idTxtTglDo" class="col-md-2 control-label">Tgl DO</label>
	<div class="col-sm-2 col-xs-12">
		<div id="idErrTglDo"></div>
		<?php echo form_input(array('name' => 'txtTglDo', 'id' => 'idTxtTglDo', 'class' => 'form-control datepicker', 'value' => $tgl_do, 'placeholder' => 'Tgl DO')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtTglStuffing" class="col-md-2 control-label">Tgl Stuffing</label>
	<div class="col-sm-2 col-xs-12">
		<div id="idErrTglStuffing"></div>
		<?php echo form_input(array('name' => 'txtTglStuffing', 'id' => 'idTxtTglStuffing', 'class' => 'form-control datepicker', 'value' => $tgl_stuffing, 'placeholder' => 'Tgl Stuffing')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtJmlKoli" class="col-md-2 control-label">Jml Koli</label>
	<div class="col-sm-2 col-xs-12">
		<div id="idErrJmlKoli"></div>
		<?php echo form_input(array('name' => 'txtJmlKoli', 'id' => 'idTxtJmlKoli', 'class' => 'form-control', 'value' => $jml_koli, 'placeholder' => 'Jml Koli')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtKeterangan" class="col-md-2 control-label">Keterangan</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrKeterangan"></div>
		<?php echo form_textarea(array('rows' => 5, 'name' => 'txtKeterangan', 'id' => 'idTxtKeterangan', 'class' => 'form-control', 'value' => $do_ket, 'placeholder' => 'Keterangan')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtGudang" class="col-md-2 control-label">Gudang (Warehouse)</label>
	<div class="col-sm-4 col-xs-12" style="margin-top: 8px">
		<div id="idErrGudang"></div>
		<?php echo form_dropdown('selGudang', $gudang, $gudang_kd !== null ? $gudang_kd : null, ['id' => 'idGudang', 'class' => 'form-control']); ?>
	</div>
</div>
<?php
if (!empty($do_attachment)) :
	?>
	<div class="form-group">
		<label for="idPreviewAttachment" class="col-md-2 control-label">Preview Attachment</label>
		<div class="col-sm-3 col-xs-12">
			<?php anchor_popup('assets/admin_assets/dist/img/attachment_do/'.$do_attachment.'', 'File Attachment', array('title' => 'File Attachment '.$do_attachment)); ?>
		</div>
	</div>
	<?php
endif;
?>
<div class="form-group">
	<label for="idFileAttachment" class="col-md-2 control-label">Attachment</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrAttachment"></div>
		<?php echo form_input(array('type' => 'hidden', 'name' => 'txtFileAttachment', 'value' => $do_attachment)); ?>
		<?php echo form_upload(array('name' => 'fileAttachment', 'id' => 'idFileAttachment', 'class' => 'form-control')); ?>
	</div>
</div>
<div class="form-group">
		<label for="idTxtAlamatkirim" class="col-md-2 control-label">Alamat Kirim</label>
		<div class="col-sm-4 col-xs-12" style="margin-top: 8px">
			<div id="idErrAlamatkirim"></div>
			<?php echo form_dropdown('selAlamatKirim', $opsiAlamatKirim, isset($so->alamat_kirim_kd) ? $so->alamat_kirim_kd : null, ['id' => 'idSelNotify', 'class' => 'form-control']); ?>
		</div>
	</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" class="btn btn-primary btn-flat">
			<i class="fa fa-shopping-cart"></i> Submit
		</button>
	</div>
</div>
<?php echo form_close(); ?>