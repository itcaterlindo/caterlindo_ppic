<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<table id="idTable" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:1%; text-align:center;" class="all">Opsi</th>
		<th style="width:3%; text-align:center;">No. Sales Order</th>
		<th style="width:3%; text-align:center;">No. Invoice</th>
		<th style="width:3%; text-align:center;">No. PO</th>
		<th style="width:5%; text-align:center;" class="never">No. Kendaraan</th>
		<th style="width:5%; text-align:center;">Keterangan DO</th>
		<th style="width:3%; text-align:center;" class="never">Tgl Delivery Order</th>
		<th style="width:3%; text-align:center;" class="all">Tgl DO & Stuffing</th>
		<th style="width:1%; text-align:center;" class="all">Tipe DO</th>
		<th style="width:1%; text-align:center;">Status</th>
		<th style="width:1%; text-align:center;" class="never">Kode Salesorder</th>
		<th style="width:1%; text-align:center;">Push SAP</th>
	</tr>
	</thead>
</table>

<script type="text/javascript">
	$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};
	var table = $('#idTable').DataTable({
		"processing": true,
		"serverSide": true,
		"ordering" : true,
		"ajax": "<?php echo base_url().$class_link.'/table_data'; ?>",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"columnDefs": [
			{"data": null, "searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
			{"searchable": false, "orderable": false, "targets": 1},
			{"className": "dt-right", "targets": 2},
			{"className": "dt-right", "targets": 3},
			{"className": "dt-right", "targets": 4},
			{"className": "dt-center", "targets": 5},
			{"className": "dt-right", "targets": 6},
			{"className": "dt-right", "targets": 7},
			{"className": "dt-right", "targets": 8},
			{"className": "dt-right", "targets": 9},
			{"className": "dt-right", "targets": 12},
		],
		"order":[8, 'desc'],
		"rowCallback": function (row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			var index = page * length + (iDisplayIndex + 1);
			$('td:eq(0)', row).html(index);
		}
	});
</script>