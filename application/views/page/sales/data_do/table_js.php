<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table();
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table() {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/open_table'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function remove_box() {
		$('#idFormBox<?php echo $master_var; ?>').remove();
		$('#idBoxItems<?php echo $master_var; ?>').remove();
		$('#idDetailBox<?php echo $master_var; ?>').remove();
		$('#idInvoiceBox<?php echo $master_var; ?>').remove();
	}

	function show_form(id) {
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		remove_box();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/show_form'; ?>',
			data: 'id='+id,
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function hapus_data(id) {
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		$('#<?php echo $box_overlay_id; ?>').show();
		remove_box();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/delete_data'; ?>',
			data: 'id='+id,
			success: function(data) {
				$('#<?php echo $box_alert_id; ?>').html(data.alert).fadeIn();
				$('#<?php echo $box_overlay_id; ?>').hide();
				open_table();
			}
		});
	}

	function prosesDo(id, kd_mso, tipe) {
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		remove_box();
		$.ajax({
			url: '<?php echo base_url(); ?>/sales/delivery_orders/data_do/get_form',
			type: 'GET',
			data: 'id='+id+'&kd_salesorder='+kd_mso+'&tipe='+tipe,
			success:function(box) {
				$('#idMainContent').prepend(box);
			}
		});
	}

	function detail_data(id) {
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		remove_box();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/get_detail'; ?>',
			data: 'id='+id,
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function edit_noinvoice(id) {
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		remove_box();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/get_invoice_form'; ?>',
			data: 'id='+id,
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function ubah_status(id, status) {
		$('#<?php echo $box_overlay_id; ?>').show();
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		remove_box();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/ubah_status'; ?>',
			data: 'id='+id+'&status='+status,
			success: function(data) {
				$('#<?php echo $box_overlay_id; ?>').hide();
				$('#<?php echo $box_alert_id; ?>').fadeIn().html(data.alert);
				moveTo('idMainContent');
				open_table();
			}
		});
	}
</script>