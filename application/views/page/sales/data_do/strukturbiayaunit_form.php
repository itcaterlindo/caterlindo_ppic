<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataBUform';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtkd_mdo', 'value' => $kd_mdo));
?>
<div class="row invoice-info">
	<div class="col-sm-6 invoice-col">
		<b>No PO :</b> <?php echo isset($do['no_po']) ? $do['no_po'] : '-'; ?><br>
		<b>Tgl DO :</b> <?php echo isset($do['tgl_do']) ? $do['tgl_do'] : '-'; ?><br>
	</div>
	<div class="col-sm-6 invoice-col">
		<b>No Invoice :</b> <?php echo isset($do['no_invoice']) ? $do['no_invoice'] : '-'; ?><br>
		<b>Notify Address :</b> <?php echo isset($do['notify_address']) ? $do['notify_address'] : '-' ?><br>
	</div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">

    <div class="form-group">
		<label for='idtxtpodetail_tgldelivery' class="col-md-2 control-label">Tanggal</label>
		<div class="col-sm-6 col-xs-12">
			<div class="errInput" id="idErrpodetail_tgldelivery"></div>
			<div class="input-group date">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				<?php echo form_input(array('type'=>'text', 'class'=> 'form-control datetimepicker', 'placeholder' =>'Last Generate' ,'value'=> isset($do['tgl_do']) ? $do['tgl_do']: null ));?>
			</div>
		</div>
        <div class="col-sm-1 col-xs-12">
            <button class="btn btn-sm btn-default" id="idbtngetCurrency" onclick="get_currency('<?php echo $do['tgl_do']; ?>')"></i> Currency Fiskal</button>
	    </div>
	</div>

    <div class="form-group">
		<label for='idtxtcurrency_rp' class="col-md-2 control-label">Currency (Rp)</label>
		<div class="col-sm-6 col-xs-12">
            <div class="errInput" id="idErrcurrency_rp"></div>
			<?php echo form_input(array('type'=>'number', 'name' => 'txtcurrency_rp', 'id' => 'idtxtcurrency_rp', 'class'=> 'form-control', 'placeholder' =>'Currency' ,'value'=> isset($do['currency_rp']) ? $do['currency_rp']: null ));?>
		</div>	
	</div>

    <div class="form-group">
        <label for='idtxtbiaya_angkut' class="col-md-2 control-label">Biaya Angkut (Rp)</label>
		<div class="col-sm-6 col-xs-12">
            <div class="errInput" id="idErrbiaya_angkut"></div>
			<?php echo form_input(array('type'=>'number', 'name' => 'txtbiaya_angkut', 'class'=> 'form-control', 'placeholder' =>'Biaya Angkut' ,'value'=> isset($do['biaya_angkut']) ? $do['biaya_angkut']: null ));?>
		</div>
	</div>
    
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2 col-xs-12">
            <button class="btn btn-sm btn-primary" onclick="submitFormCurrency('<?php echo $form_id; ?>')"> <i class="fa fa-save"></i> Simpan </button>
        </div>
        
    </div>

    </div>
</div>
<?php echo form_close(); ?>