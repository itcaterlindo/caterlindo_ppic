<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataDO';
$form_id = 'idDetail'.$master_var;
$color = color_status($status_do);
$word = process_status($status_do);
?>
<!-- START BUTTON FOR PRINT -->
<?php
$disc_custs = $this->td_salesorder_item_disc->select_distinct($msalesorder_kd, $kd_jenis_customer);
if (empty($disc_custs)) :
	$disc_custs = $this->tb_default_disc->get_all($kd_jenis_customer);
endif;
foreach ($disc_custs as $disc_cust) :
	if ($disc_cust->status == '1') :
		$nm_pihak = strtolower($disc_cust->nm_pihak);
		?>
		<?php /* Cek login account eksport2 for privilage not showing distributor */ if( $this->session->tipe_admin == 'Eksport2' && in_array("Customer", array_column($disc_custs, "nm_pihak")) ):  ?>
			<a href="<?php echo base_url().'sales/invoice/invoice_data/cetak_inv_do/0/customer/'.$kd_mdo; ?>" class="btn btn-success btn-flat" title="Cetak Invoice" target="_blank" style="margin-top: 2px;">
				<i class="fa fa-print"></i> Cetak Invoice
			</a>
		<?php /* Ketika invoice customer sudah tampil perulangan invoice berhenti */ break; ?>
		<?php /* Cek login account eksport for privilage not showing customer */ elseif( $this->session->tipe_admin == 'Eksport' && in_array("Distributor", array_column($disc_custs, "nm_pihak")) ):  ?>
			<a href="<?php echo base_url().'sales/invoice/invoice_data/cetak_inv_do/0/distributor/'.$kd_mdo; ?>" class="btn btn-success btn-flat" title="Cetak Invoice" target="_blank" style="margin-top: 2px;">
				<i class="fa fa-print"></i> Cetak Invoice
			</a>
		<?php /* Ketika invoice distributor sudah tampil perulangan invoice berhenti */ break; ?>
		<?php /* Cek login account selain eksport2 */ else: ?>
			<a href="<?php echo base_url().'sales/invoice/invoice_data/cetak_inv_do/0/'.$nm_pihak.'/'.$kd_mdo; ?>" class="btn btn-success btn-flat" title="Cetak Invoice" target="_blank" style="margin-top: 2px;">
				<i class="fa fa-print"></i> Cetak Invoice <?php echo $disc_cust->nm_pihak; ?>
			</a>
		<?php endif; ?>
		<?php
		if ($is_dp) :
			?>
			<a href="<?php echo base_url().'sales/invoice/invoice_data/cetak_inv_do/1/'.$nm_pihak.'/'.$kd_mdo; ?>" class="btn btn-info btn-flat" title="Cetak Invoice beserta DP" target="_blank" style="margin-top: 2px;">
				<i class="fa fa-print"></i> Cetak Invoice <?php echo $disc_cust->nm_pihak; ?> + DP
			</a>
			<?php
		endif;
	endif;
endforeach;
if ($tipe_do == 'Ekspor') :
	?>
	<a href="<?php echo base_url().'sales/delivery_orders/data_do/cetak_shipping_instruction/'.$kd_mdo; ?>" class="btn btn-primary btn-flat" title="Cetak Shipping Instruction" target="_blank" style="margin-top: 2px;">
		<i class="fa fa-print"></i> Cetak Shipping Instruction
	</a>
	<?php
	foreach ($disc_custs as $disc_cust) :
		if ($disc_cust->status == '1') :
			$nm_pihak = strtolower($disc_cust->nm_pihak);
			?>
				<?php /* Cek login account eksport2 for privilage not showing distributor */ if( $this->session->tipe_admin == 'Eksport2' && in_array("Customer", array_column($disc_custs, "nm_pihak")) ):  ?>
					<a href="<?php echo base_url().'sales/delivery_orders/data_do/cetak_packing_list/customer/'.$kd_mdo; ?>" class="btn btn-warning btn-flat" title="Cetak Packing List" target="_blank">
						<i class="fa fa-print"></i> Cetak Packing List
					</a>
				<?php /* Ketika invoice customer sudah tampil perulangan invoice berhenti */ break; ?>
				<?php /* Cek login account eksport for privilage not showing customer */ elseif( $this->session->tipe_admin == 'Eksport' && in_array("Distributor", array_column($disc_custs, "nm_pihak")) ):  ?>
					<a href="<?php echo base_url().'sales/delivery_orders/data_do/cetak_packing_list/distributor/'.$kd_mdo; ?>" class="btn btn-warning btn-flat" title="Cetak Packing List" target="_blank">
						<i class="fa fa-print"></i> Cetak Packing List
					</a>
				<?php /* Ketika invoice distributor sudah tampil perulangan invoice berhenti */ break; ?>
				<?php /* Cek login account selain eksport2 */ else: ?>
				<a href="<?php echo base_url().'sales/delivery_orders/data_do/cetak_packing_list/'.$nm_pihak.'/'.$kd_mdo; ?>" class="btn btn-warning btn-flat" title="Cetak Packing List" target="_blank">
					<i class="fa fa-print"></i> Cetak Packing List <?php echo $disc_cust->nm_pihak; ?>
				</a>
				<?php endif; ?>
			<?php
		endif;
	endforeach;
	?>
	<a href="<?php echo base_url().'sales/delivery_orders/data_do/cetak_picking_slip/'.$kd_mdo; ?>" class="btn btn-default btn-flat" title="Cetak Picking Slip" target="_blank" style="margin-top: 2px;">
		<i class="fa fa-print"></i> Cetak Picking Slip
	</a>
	<a href="<?php echo base_url().'sales/delivery_orders/data_do/strukturbiayaunit_box?kd_mdo='.$kd_mdo_ori; ?>" class="btn btn-default btn-flat" title="Cetak Picking Slip" target="_blank" style="margin-top: 2px;">
		<i class="fa fa-print"></i> Generate Biaya Per-Unit
	</a>
	<?php
elseif ($tipe_do == 'Lokal') :
	?>
	<a href="<?php echo base_url().'sales/delivery_orders/data_do/cetak_do/'.$kd_mdo; ?>" class="btn btn-primary btn-flat" title="Cetak DO" target="_blank" style="margin-top: 2px;">
		<i class="fa fa-print"></i> Cetak DO
	</a>
	<a href="<?php echo base_url().'sales/delivery_orders/data_do/cetak_do_xml/'.$kd_mdo; ?>" class="btn btn-info btn-flat" title="Cetak DO" target="_blank" style="margin-top: 2px;">
		<i class="fa fa-print"></i> Cetak DO(XML)
	</a>
	<?php
endif;
?>
<hr>
<!-- END BUTTON FOR PRINT -->

<!-- START DO HEADER DETAIL -->
<table>
	<tr>
		<th style="width: 150px;vertical-align: top;">Invoice To</th>
		<td style="vertical-align: top;">:</td>
		<td style="width: 250px;">
			<strong><?php echo $nm_cust; ?></strong><br>
			<?php echo $nm_contact_cust.after_before_char($nm_contact_cust, $nm_contact_cust, '<br>', ''); ?>
			<?php echo $alamat_cust; ?>
		</td>
	</tr>
	<?php
	if ($tipe_do == 'Ekspor') :
		?>
		<tr>
			<th style="width: 150px;vertical-align: top;">Notify Party</th>
			<td style="vertical-align: top;">:</td>
			<td>
				<strong><?php echo $notify_name; ?></strong><br>
				<?php echo $notify_address; ?>
			</td>
		</tr>
		<?php
	endif;
	?>
	<tr>
		<th style="width: 150px;vertical-align: top;">Delivery Address</th>
		<td style="vertical-align: top;">:</td>
		<td>
			<strong><?php echo $nm_penerima; ?></strong><br>
			<?php echo $nm_contact_penerima.after_before_char($nm_contact_penerima, $nm_contact_penerima, '<br>', ''); ?>
			<?php echo $alamat_penerima; ?>
		</td>
		<th style="width: 150px;vertical-align: top;text-align: center;">Date</th>
		<td style="vertical-align: top;">:</td>
		<td style="vertical-align: top;"><?php echo format_date($tgl_do, 'd M Y'); ?></td>
	</tr>
	<tr>
		<?php
		if ($tipe_do == 'Ekspor') :
			?>
			<th style="width: 150px;vertical-align: top;">Freight By</th>
			<td style="vertical-align: top;">:</td>
			<td>
				<strong><?php echo $nm_jasakirim; ?></strong><br>
				<?php echo $alamat_jasakirim; ?>
			</td>
			<?php
		endif;
		if (!empty($no_po)) :
			?>
			<th style="width: 150px;vertical-align: top;text-align: center;">Order No</th>
			<td style="vertical-align: top;">:</td>
			<td style="vertical-align: top;"><?php echo $no_po; ?></td>
			<?php
		endif;
		?>
	</tr>
	<?php
	if ($tipe_do == 'Ekspor') :
		?>
		<tr>
			<th style="width: 125px;vertical-align: top;">Description of goods</th>
			<td style="vertical-align: top;">:</td>
			<td><?php echo !empty($goods_desc)?$goods_desc:'Stainless Steel Goods'; ?></td>
		</tr>
		<?php
	endif;
	?>
</table>
<!-- END DO HEADER DETAIL -->
<hr>
<!-- START DO ITEMS DETAIL -->
<table class="table table-bordered table-hover table-striped">
	<thead>
		<tr>
			<td>No</td>
			<td>Product Code</td>
			<td>Desc</td>
			<td>Size</td>
			<td>Tariff</td>
			<td>Qty</td>
			<td>Netweight</td>
			<td>Grossweight</td>
			<td>Item CBM</td>
			<td>Note</td>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($parent_items as $parent) :
			$no++;
			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $parent->item_code; ?></td>
				<td><?php echo $parent->item_desc; ?></td>
				<td><?php echo $parent->item_dimension; ?></td>
				<td><?php echo $parent->hs_code; ?></td>
				<td><?php echo $parent->stuffing_item_qty; ?></td>
				<td><?php echo $parent->netweight; ?></td>
				<td><?php echo $parent->grossweight; ?></td>
				<td><?php echo $parent->item_cbm; ?></td>
				<td><?php echo $parent->item_note_do; ?></td>
			</tr>
			<?php
		endforeach;
		foreach ($child_items as $child) :
			$no++;
			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $child->item_code; ?></td>
				<td><?php echo $child->item_desc; ?></td>
				<td><?php echo $child->item_dimension; ?></td>
				<td><?php echo $child->hs_code; ?></td>
				<td><?php echo $child->stuffing_item_qty; ?></td>
				<td><?php echo $child->netweight; ?></td>
				<td><?php echo $child->grossweight; ?></td>
				<td><?php echo $child->item_cbm; ?></td>
				<td><?php echo $child->item_note_do; ?></td>
			</tr>
			<?php
		endforeach;
		?>
	</tbody>
</table>
<!-- END DO ITEMS DETAIL -->