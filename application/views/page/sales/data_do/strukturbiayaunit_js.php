<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
    strukturbiayaunit_read ('<?= $kd_mdo ?>', '<?= isset($slug) ? $slug : null; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#<?php echo $box_loader_id; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
	});

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

    function strukturbiayaunit_read (kd_mdo = null, slug = null) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/strukturbiayaunit_read'; ?>',
			data: {kd_mdo : kd_mdo, slug : slug},
			success: function(html) {
                $('#<?php echo $box_content_id; ?>').html(html);
				moveTo('idMainContent');
			}
		});
    }

    function strukturbiayaunit_form (kd_mdo = null, slug = null) {
        event.preventDefault();
        $.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/strukturbiayaunit_form'; ?>',
			data: {kd_mdo : kd_mdo, slug : slug},
			success: function(html) {
                toggle_modal('Proses', html);
                render_datetimepicker('datetimepicker');
			}
		});
    }

    function strukturbiayaunit_view (kd_mdo = null) {
        $.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/strukturbiayaunit_view'; ?>',
			data: {kd_mdo : kd_mdo},
			success: function(html) {
                $('#idMainContent').append(html);
			}
		});
    }

	function strukturbiayaunitgrouphscode_view (kd_mdo = null) {
        $.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/strukturbiayaunit_group_hscode_view'; ?>',
			data: {kd_mdo : kd_mdo},
			success: function(html) {
                $('#idMainContent').append(html);
			}
		});
    }

    function strukturbiayaunitpartstatus_box (kd_mdo = null) {
        event.preventDefault();
        $.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/strukturbiayaunitpartstatus_box'; ?>',
			data: {kd_mdo : kd_mdo},
			success: function(html) {
                $('#idMainContent').prepend(html);
                moveTo('idMainContent');
			}
		});
    }

    function get_currency (tgl_currency = '<?php date('Y-m-d'); ?>') {
        $('#idbtngetCurrency').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtngetCurrency').attr('disabled', true);
        event.preventDefault();
        $.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/get_currency'; ?>',
			data: {tgl_currency : tgl_currency},
			success: function(resp) {
                console.log(resp);
                $('#idtxtcurrency_rp').val(resp.nilai);
                $('#idbtngetCurrency').html('Currency Fiskal');
				$('#idbtngetCurrency').attr('disabled', false);
			}
		});
    }

    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

    function submitFormCurrency (form_id) {
        event.preventDefault();
        let form = document.getElementById(form_id);
		let url = "<?php echo base_url().$class_link; ?>/action_submit_biayastrukturunit";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
                console.log(data);
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					window.location.reload();
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
    }

    function action_generatereport_strukturbiaya (kd_mdo = null) {
        event.preventDefault();
        var conf = confirm('Apakah anda yakin ?');
        if (conf) {
            if (kd_mdo) {
                $('#idbtnproses').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		        $('#idbtnproses').attr('disabled', true);
                $.ajax({
                    type: 'GET',
                    url: '<?php echo base_url().$class_link.'/action_generatereport_strukturbiaya'; ?>',
                    data: {kd_mdo : kd_mdo},
                    success: function(resp) {
                        console.log(resp);
                        $('#idbtnproses').html('<i class="fa fa-refresh"></i>Proses');
				        $('#idbtnproses').attr('disabled', false);
                        notify (resp.status, resp.pesan, 'success');
                        window.location.reload();
                    }
                });
            }else{
                notify ('Gagal', 'ID Kosong', 'error');
            }
        }
    }

    function render_datetimepicker(valClass){
		$('.'+valClass).datetimepicker({
			format: 'YYYY-MM-DD',
    	});
	}

    function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

    function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

    function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }

</script>