<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
extract($format_laporan);
$master_var = 'DataDO';
$form_id = 'idDetail'.$master_var;
$color = color_status($status_do);
$word = process_status($status_do);
$no_form = !empty($no_form)?$no_form.'a':'';
?>
<style type="text/css">
	body {
		font-size: 12px;
	}
	img {
		width: 100% !important;
	}
	thead.report-header {
		display: table-header-group;
	}

	tfoot.report-footer {
		display:table-footer-group;
	}
	table.report-container {
		page-break-after: always;
	}
	/* @media print {
		body {
			transform: scale(80%);
		}
		.print-left {
			text-align: left;
		}
		.print-center {
			text-align: center;
		}
		.print-right {
			text-align: right;
		}
	} */
</style>

<div class="row">
	<a href="javascript:void(0);" class="btn btn-danger no-print" onclick="window.close();" style="margin-left: 25px;">
		<i class="fa fa-ban"></i> Close
	</a>
	<a href="javascript:void(0);" class="btn btn-primary no-print" onclick="window.print();" style="margin-left: 25px;">
		<i class="fa fa-print"></i> Cetak Data
	</a>
	<a href="<?php echo base_url().'report/packing_list/report/'.$tipe_laporan.'/'.$kd_mdo; ?>" class="btn btn-success no-print" style="margin-left: 25px;">
		<i class="fa fa-file-excel-o"></i> Export Excel
	</a>
</div>
<div class="row invoice print">
	<table class="">
		<thead class="report-header">
			<tr>
				<td>
					<div class="header-info"></div>
				</td>
			</tr>
		</thead>
		
		<tbody class="report-content">
			<tr>
				<td>
					<!-- title row -->
					<div class="row">
						<table border="0">
							<tr>
								<td style="width: 40%;">
									<img alt="" src="<?php echo my_baseurl(); ?>assets/admin_assets/dist/img/LogoSS2021.jpg" />
								</td>
								
								<td style="width: 60%;">

									<table border="0" style="width: 100%;"> 
										<tr>
											<td colspan="3" style="text-align: right; font-size: 36px; font-weight: bold;">PACKING LIST</td>
										</tr>
										<tr>
											<td width="30%"></td>
											<td width="35%" style="font-size: 12px; border-bottom: 1px solid black;">No. Dokumen</td>
											<td width="35%" style="font-size: 12px; text-align: left; border-bottom: 1px solid black;">: CAT4-SLS-009</td>
										</tr>
										<tr>
											<td></td>
											<td style="font-size: 12px; border-bottom: 1px solid black;">Tanggal</td>
											<td style="font-size: 12px; border-bottom: 1px solid black;">: 12 Agustus 2021</td>
										</tr>
										<tr>
											<td></td>
											<td style="font-size: 12px; border-bottom: 1px solid black;">Revisi</td>
											<td style="font-size: 12px; border-bottom: 1px solid black;">: 05</td>
										</tr>
									</table>

								</td>
							</tr>
						</table>
						<hr>
						<h3>
							<div style="text-align: center;"><b><u>Packing List</u></b></div>
							<div style="text-align: center;"><?php echo $no_invoice; ?></div>
						</h3>
					</div>
					<!-- START PACKING LIST HEADER DETAIL -->
					<?php
					if ($tipe_laporan == 'distributor') :
						?>
						<table>
							<tr>
								<th style="width: 150px;vertical-align: top;">Invoice To Buyer<br>( Notify Party )</th>
								<td style="vertical-align: top; padding-left: 10px; padding-right: 10px;">:</td>
								<td style="width: 250px;">
									<strong><?php echo $notify_name; ?></strong><br>
									<?php echo $notify_address; ?>
								</td>
							</tr>
							<?php
							if ($tipe_do == 'Ekspor') :
								?>
								<tr>
									<th style="width: 150px;vertical-align: top;">Consignee</th>
									<td style="vertical-align: top;">:</td>
									<td>
										<strong><?php echo $nm_cust; ?></strong><br>
										<?php echo $nm_contact_cust.after_before_char($nm_contact_cust, $nm_contact_cust, '<br>', ''); ?>
										<?php echo $alamat_cust; ?>
									</td>
								</tr>
								<?php
							endif;
							?>
							<tr>
								<th style="width: 150px;vertical-align: top;">Delivery Address</th>
								<td style="vertical-align: top;">:</td>
								<td>
									<strong><?php echo $nm_penerima; ?></strong><br>
									<?php echo $nm_contact_penerima.after_before_char($nm_contact_penerima, $nm_contact_penerima, '<br>', ''); ?>
									<?php echo $alamat_penerima; ?>
								</td>
								<th style="width: 150px;vertical-align: top;text-align: center;">Date</th>
								<td style="vertical-align: top;">:</td>
								<td style="vertical-align: top;"><?php echo format_date($tgl_do, 'd M Y'); ?></td>
							</tr>
							<tr>
								<?php
								if ($tipe_do == 'Ekspor') :
									?>
									<th style="width: 150px;vertical-align: top;">Freight By</th>
									<td style="vertical-align: top;">:</td>
									<td>
										<strong><?php echo $nm_jasakirim; ?></strong><br>
										<?php echo $alamat_jasakirim; ?>
									</td>
									<?php
								endif;
								if (!empty($no_po)) :
									?>
									<th style="width: 150px;vertical-align: top;text-align: center;">Order No</th>
									<td style="vertical-align: top;">:</td>
									<td style="vertical-align: top;">
										<?php
										foreach ($list_nopo as $list_po) :
											echo '<li>'.$list_po.'</li>';
										endforeach;
										?>
									</td>
									<?php
								endif;
								?>
							</tr>
							<?php
							if ($tipe_do == 'Ekspor') :
								?>
								<tr>
									<th style="width: 125px;vertical-align: top;">Description of goods</th>
									<td style="vertical-align: top;">:</td>
									<td><?php echo !empty($goods_desc)?$goods_desc:'Stainless Steel Goods'; ?></td>
								</tr>
								<tr>
									<th style="width: 125px;vertical-align: top;">Number of boxs</th>
									<td style="vertical-align: top;">:</td>
									<td><?php echo $jml_koli; ?></td>
								</tr>
								<?php
							endif;
							?>
						</table>
						<?php
					elseif ($tipe_laporan == 'customer') :
						?>
						<table>
							<tr>
								<th style="width: 150px;vertical-align: top;">Invoice To</th>
								<td style="vertical-align: top;">:</td>
								<td style="width: 250px;">
									<strong><?php echo $nm_cust; ?></strong><br>
									<?php echo $nm_contact_cust.after_before_char($nm_contact_cust, $nm_contact_cust, '<br>', ''); ?>
									<?php echo $alamat_cust; ?>
								</td>
							</tr>
							<tr>
								<th style="width: 150px;vertical-align: top;">Delivery Address</th>
								<td style="vertical-align: top;">:</td>
								<td>
									<strong><?php echo $nm_penerima; ?></strong><br>
									<?php echo $nm_contact_penerima.after_before_char($nm_contact_penerima, $nm_contact_penerima, '<br>', ''); ?>
									<?php echo $alamat_penerima; ?>
								</td>
								<th style="width: 150px;vertical-align: top;text-align: center;">Date</th>
								<td style="vertical-align: top;">:</td>
								<td style="vertical-align: top;"><?php echo format_date($tgl_do, 'd M Y'); ?></td>
							</tr>
							<tr>
								<?php
								if ($tipe_do == 'Ekspor') :
									?>
									<th style="width: 150px;vertical-align: top;">Freight By</th>
									<td style="vertical-align: top;">:</td>
									<td>
										<strong><?php echo $nm_jasakirim; ?></strong><br>
										<?php echo $alamat_jasakirim; ?>
									</td>
									<?php
								endif;
								if (!empty($no_po)) :
									?>
									<th style="width: 150px;vertical-align: top;text-align: center;">Order No</th>
									<td style="vertical-align: top;">:</td>
									<td style="vertical-align: top;">
										<?php
										foreach ($list_nopo as $list_po) :
											echo '<li>'.$list_po.'</li>';
										endforeach;
										?>
									</td>
									<?php
								endif;
								?>
							</tr>
							<?php
							if ($tipe_do == 'Ekspor') :
								?>
								<tr>
									<th style="width: 125px;vertical-align: top;">Description of goods</th>
									<td style="vertical-align: top;">:</td>
									<td><?php echo !empty($goods_desc)?$goods_desc:'Stainless Steel Goods'; ?></td>
								</tr>
								<tr>
									<th style="width: 125px;vertical-align: top;">Number of boxs</th>
									<td style="vertical-align: top;">:</td>
									<td><?php echo $jml_koli; ?></td>
								</tr>
								<?php
							endif;
							?>
						</table>
						<?php
					endif;
					?>
					<!-- END PACKING LIST HEADER DETAIL -->
					<hr>
					<!-- START PACKING LIST ITEMS DETAIL -->
					<table style="width: 100%;" border="1">
						<thead>
							<tr>
								<td style="width: 1%;">No</td>
								<td style="width: 15%;">Product Code</td>
								<td style="width: 25%;">Desc</td>
								<td style="width: 39%;">Size</td>
								<td style="width: 5%;">Tariff</td>
								<td style="width: 5%;">Qty</td>
								<td style="width: 5%;">Netweight</td>
								<td style="width: 5%;">Grossweight</td>
								<td style="width: 5%;">Meas / (CBM)</td>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 0;
							$tots_stuff = array('');
							$tots_nw = array('');
							$tots_gw = array('');
							foreach ($parent_items as $parent) :
								$no++;
								?>
								<tr>
									<td style="text-align: center;"><?php echo $no; ?></td>
									<td><?php echo $parent->item_code; ?></td>
									<td><?php echo $parent->item_desc; ?></td>
									<td><?php echo $parent->item_dimension; ?></td>
									<td style="text-align: center;"><?php echo $parent->hs_code; ?></td>
									<td style="text-align: center;"><?php echo $parent->stuffing_item_qty; ?></td>
									<td style="text-align: center;"><?php echo $parent->stuffing_item_qty * $parent->netweight; ?></td>
									<td style="text-align: center;"><?php echo $parent->stuffing_item_qty * $parent->grossweight; ?></td>
									<td style="text-align: center;"><?php $bg = $parent->stuffing_item_qty * $parent->item_cbm; echo sprintf('%.4f', floor($bg*10000*($bg>0?1:-1))/10000*($bg>0?1:-1)); ?></td>
								</tr>
								<?php
								$tots_stuff[] = $parent->stuffing_item_qty;
								$tots_nw[] = $parent->stuffing_item_qty * $parent->netweight;
								$tots_gw[] = $parent->stuffing_item_qty * $parent->grossweight;
								$totalx[] = $parent->stuffing_item_qty * $parent->item_cbm;
							endforeach;
							foreach ($child_items as $child) :
								$no++;
								?>
								<tr>
									<td style="text-align: center;"><?php echo $no; ?></td>
									<td><?php echo $child->item_code; ?></td>
									<td><?php echo $child->item_desc; ?></td>
									<td><?php echo $child->item_dimension; ?></td>
									<td style="text-align: center;"><?php echo $child->hs_code; ?></td>
									<td style="text-align: center;"><?php echo $child->stuffing_item_qty; ?></td>
									<td style="text-align: center;"><?php echo $child->stuffing_item_qty * $child->netweight; ?></td>
									<td style="text-align: center;"><?php echo $child->stuffing_item_qty * $child->grossweight; ?></td>
									<td style="text-align: center;"><?php $bg = $child->stuffing_item_qty * $child->item_cbm; echo sprintf('%.4f', floor($bg*10000*($bg>0?1:-1))/10000*($bg>0?1:-1)); ?></td>
								</tr>
								<?php
								$tots_stuff[] = $child->stuffing_item_qty;
								$tots_nw[] = $child->stuffing_item_qty * $child->netweight;
								$tots_gw[] = $child->stuffing_item_qty * $child->grossweight;
								$totalx[] = $child->stuffing_item_qty * $child->item_cbm;
							endforeach;
							$tot_stuff = array_sum($tots_stuff);
							$tot_nw = array_sum($tots_nw);
							$tot_gw = array_sum($tots_gw);
							$totalnx = array_sum($totalx);
							?>
							<tr>
								<td></td>
								<td></td>
								<td><b>Grand Total</b></td>
								<td></td>
								<td></td>
								<td style="text-align: center;"><b><?php echo $tot_stuff; ?></b></td>
								<td style="text-align: center;"><b><?php echo $tot_nw; ?></b></td>
								<td style="text-align: center;"><b><?php echo $tot_gw; ?></b></td>
								<td style="text-align: center;"><b><?php echo sprintf('%.4f', floor($totalnx*10000*($totalnx>0?1:-1))/10000*($totalnx>0?1:-1));; ?></b></td>
							</tr>
						</tbody>
					</table>
					<!-- END PACKING LIST ITEMS DETAIL -->

					<!-- START PACKING LIST FOOTER -->
					<?php
					if ($tipe_laporan == 'distributor') :
						?>
						<div class="row invoice-info print-foot" style="margin-top: 15px;">
							<div class="col-sm-1 invoice-col">
								NOTE :
							</div>
						</div>
						<div class="row invoice-info print-foot" style="margin-top: 15px;">
							<div class="col-sm-3 invoice-col">
								Gross Weight exclude tare weight container
							</div>
						</div>
						<div class="row invoice-info print-foot" style="margin-top: 15px;">
							<div class="col-sm-2 invoice-col">
								Container Number :
							</div>
							<div class="col-sm-3 invoice-col">
								<?php echo $container_number; ?>
							</div>
						</div>
						<div class="row invoice-info print-foot">
							<div class="col-sm-2 invoice-col">
								Seal Number :
							</div>
							<div class="col-sm-3 invoice-col">
								<?php echo $seal_number; ?>
							</div>
						</div>
						<div class="row invoice-info print-foot" style="margin-top: 15px;">
							<div class="col-xs-9"></div>
							<div class="col-xs-3" style="text-align: center;">
								<div style="margin-bottom: 100px;">Prepared by,</div>
								<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
								<div><b>Exim</b></div>
							</div>
						</div>
						<?php
					elseif ($tipe_laporan == 'customer') :
						?>
						<h4 style="text-align: center;">PACKING LIST</h4>
						<div class="row invoice-info print-foot">
							<div class="col-sm-2 invoice-col">
								1. SHIPPER :
							</div>
							<div class="col-md-3 invoice-col">
								<?php echo $nm_jasakirim; ?>
							</div>
						</div>
						<div class="row invoice-info print-foot">
							<div class="col-sm-2 invoice-col">
								2. CONSIGNEE/NOTIFY PARTY :
							</div>
							<div class="col-md-3 invoice-col">
								<?php echo $notify_name; ?><br>
								<?php echo $notify_address; ?>
							</div>
						</div>
						<div class="row invoice-info print-foot">
							<div class="col-sm-2 invoice-col">
								3. DESCRIPTION OF GOODS :
							</div>
							<div class="col-md-3 invoice-col">
								Stainless Steel Goods
							</div>
						</div>
						<div class="row invoice-info print-foot">
							<div class="col-sm-2 invoice-col">
								4. NETT / GROSS WEIGHT :
							</div>
							<div class="col-md-3 invoice-col">
								<?php echo $tot_nw.' / '.$tot_gw.' KGS'; ?>
							</div>
						</div>
						<div class="row invoice-info print-foot">
							<div class="col-sm-2 invoice-col">
								5. MEASUREMENT :
							</div>
						</div>
						<div class="row invoice-info print-foot">
							<div class="col-sm-2 invoice-col">
								6. LC/NON LC :
							</div>
							<div class="col-md-3 invoice-col">
								NON LC
							</div>
						</div>
						<div class="row invoice-info print-foot">
							<div class="col-sm-2 invoice-col">
								7. SHIPPED ON BOARD :
							</div>
							<div class="col-md-3 invoice-col">
								<?php echo $tgl_do; ?>
							</div>
						</div>
						<?php
					endif;
					?>
					<!-- END PACKING LIST FOOTER -->
				</td>
			</tr>
		</tbody>
	</table>
	<hr>
	<table class="div-footer" style="width: 50%; " border="0">
		<tbody>
			<tr>
				<td>
					<img src="<?php echo base_url(); ?>assets/admin_assets/dist/img/LogoISO2021.jpg" alt="" >
				</td>
			</tr>
		</tbody>
	</table>
</div>