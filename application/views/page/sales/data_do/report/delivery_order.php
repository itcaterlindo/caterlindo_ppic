<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
extract($format_laporan);
$master_var = 'DataDO';
$form_id = 'idDetail'.$master_var;
$color = color_status($status_do);
$word = process_status($status_do);
$no_form = !empty($no_form)?$no_form.'d':'';
?>
<style type="text/css">
	body {
		font-size: 12px;
	}
	img {
		width: 100% !important;
	}
	.table>tbody>tr>td {
		padding:1px;
	}
	.table>tfoot>tr>td {
		padding:1px;
	}
	/* @media print {
		body {transform: scale(80%);}
		.print {
			page-break-after: always;
		}
		.print-foot {
			page-break-inside: avoid;
		}
		.div-footer {
			position: fixed;
			bottom: 0;
		}
		.print-left {
			text-align: left;
		}
		.print-center {
			text-align: center;
		}
		.print-right {
			text-align: right;
			margin-right: 25px;
		}
	} */
</style>
<div class="row">
	<div class="col-xs-12">
		<a href="javascript:void(0);" class="btn btn-danger no-print" onclick="window.close();" style="margin-left: 25px;">
			<i class="fa fa-ban"></i> Close
		</a>
		<a href="javascript:void(0);" class="btn btn-primary no-print" onclick="window.print();" style="margin-left: 25px;">
			<i class="fa fa-print"></i> Cetak Data
		</a>
	</div>
</div>
<div class="row invoice print">
	<!-- title row -->
	<div class="row">
		<div class="col-xs-12">
					
				<table border="0">
					<tr>
						<td style="width: 40%;">
							<img alt="" src="<?php echo my_baseurl(); ?>assets/admin_assets/dist/img/LogoSS2021.jpg"  />
						</td>
						
						<td style="width: 60%;">

							<table border="0" style="width: 100%;"> 
								<tr>
									<td colspan="3" style="text-align: right; font-size: 36px; font-weight: bold;">DELIVERY ORDER</td>
								</tr>
								<tr>
									<td width="30%"></td>
									<td width="35%" style="font-size: 12px; border-bottom: 1px solid black;">No. Dokumen</td>
									<td width="35%" style="font-size: 12px; text-align: left; border-bottom: 1px solid black;">: CAT4-SLS-011</td>
								</tr>
								<tr>
									<td></td>
									<td style="font-size: 12px; border-bottom: 1px solid black;">Tanggal</td>
									<td style="font-size: 12px; border-bottom: 1px solid black;">: 12 Agustus 2021</td>
								</tr>
								<tr>
									<td></td>
									<td style="font-size: 12px; border-bottom: 1px solid black;">Revisi</td>
									<td style="font-size: 12px; border-bottom: 1px solid black;">: 05</td>
								</tr>
							</table>

						</td>
					</tr>
				</table>
				<hr>
				<h3>
					<div style="text-align: center;"><b><u>Delivery Order</u></b></div>
					<div style="text-align: center;"><?php echo $no_invoice; ?></div>
				</h3>
		</div><!-- /.col -->
	</div>
	<!-- START DO HEADER DETAIL -->
	<table>
		<tr>
			<th style="width: 150px;vertical-align: top;">Invoice To</th>
			<td style="width: 50px;vertical-align: top;">:</td>
			<td style="width: 250px;">
				<strong><?php echo $nm_cust; ?></strong><br>
				<?php echo $nm_contact_cust.after_before_char($nm_contact_cust, $nm_contact_cust, '<br>', ''); ?>
				<?php echo $alamat_cust; ?>
			</td>
		</tr>
		<tr>
			<th style="width: 150px;vertical-align: top;">Customer Telp</th>
			<td style="width: 50px;vertical-align: top;">:</td>
			<td style="width: 250px;">
				<strong><?php echo $telp_customer; ?></strong>
			</td>
		</tr>
		<tr>
			<th style="width: 150px;vertical-align: top;">Delivery Address</th>
			<td style="vertical-align: top;">:</td>
			<td>
				<strong><?php echo $nm_penerima; ?></strong><br>
				<?php echo $nm_contact_penerima.after_before_char($nm_contact_penerima, $nm_contact_penerima, '<br>', ''); ?>
				<?php echo $alamat_penerima; ?>
			</td>
			<th style="width: 75px;vertical-align: top;text-align: center;">Date</th>
			<td style="width: 50px;vertical-align: top;">:</td>
			<td style="vertical-align: top;"><?php echo format_date($tgl_do, 'd M Y'); ?></td>
		</tr>
		<tr>
			<th style="width: 150px;vertical-align: top;">Receiver Telp</th>
			<td style="width: 50px;vertical-align: top;">:</td>
			<td style="width: 250px;">
				<strong><?php echo $telp_penerima; ?></strong>
			</td>
		</tr>
		<tr>
			<th style="width: 150px;vertical-align: top;">Salesorder No</th>
			<td style="vertical-align: top;">:</td>
			<td style="vertical-align: top;"><?php echo $no_salesorder; ?></td>
		</tr>
		<?php
		if (!empty($no_po) && $no_po != '-') :
			?>
			<tr>
				<th style="width: 150px;vertical-align: top;">Order No</th>
				<td style="vertical-align: top;">:</td>
				<td style="vertical-align: top;"><?php echo $no_po; ?></td>
			</tr>
			<?php
		endif;
		?>
		<tr>
			<th style="width: 150px;vertical-align: top;">Jumlah Koli</th>
			<td style="vertical-align: top;">:</td>
			<td style="vertical-align: top;"><?php echo $jml_koli; ?></td>
		</tr>
	</table>
	<!-- END DO HEADER DETAIL -->
	<hr> 
	<!-- START DO ITEMS DETAIL -->
	<table border="1" style="width: 100%; table-layout: fixed; word-wrap: break-word;">
		<thead>
			<tr>
				<th style="width: 5%;">No</th>
				<th style="width: 20%;">Product Code</th>
				<th style="width: 15%;">Status</th>
				<th style="width: 30%;">Desc</th>
				<th style="width: 30%;">Size</th>
				<th style="width: 20%;">Note</th>
				<th style="width: 10%;">Qty</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$no = 0;
			foreach($parent_items as $parent) :
				$no_child = 0;
				$rows = '';
				foreach ($child_items as $child) :
					if ($parent->parent_kd == $child->parent_kd) :
						$no_child++;
					endif;
					$rowspan = 1 + $no_child;
					$rows = 'rowspan=\''.$rowspan.'\'';
				endforeach;
				$no++;
				$tot_qty[] = $parent->stuffing_item_qty;
				?>
				<tr>
					<td <?php echo $rows; ?> align="center"><?php echo $no; ?></td>
					<td><?php echo $parent->item_code; ?></td>
					<td><?php echo item_stat($parent->item_status); ?></td>
					<td><?php echo $parent->item_desc; ?></td>
					<td><?php echo $parent->item_dimension; ?></td>
					<td><?php echo $parent->item_note_do; ?></td>
					<td align="center"><?php echo $parent->stuffing_item_qty; ?></td>
				</tr>
				<?php
				foreach ($child_items as $child) :
					if ($parent->parent_kd == $child->parent_kd) :
						$tot_qty[] = $child->stuffing_item_qty;
						?>
						<tr>
							<td><?php echo $child->item_code; ?></td>
							<td><?php echo item_stat($child->item_status); ?></td>
							<td><?php echo $child->item_desc; ?></td>
							<td><?php echo $child->item_dimension; ?></td>
							<td><?php echo $child->item_note_do; ?></td>
							<td align="center"><?php echo $child->stuffing_item_qty; ?></td>
						</tr>
						<?php
					else :
						$child_data['arr_child']['parent_kd'][] = $child->parent_kd;
						$child_data['arr_child']['item_status'][] = $child->item_status;
						$child_data['arr_child']['item_note_do'][] = $child->item_note_do;
						$child_data['arr_child']['stuffing_item_qty'][] = $child->stuffing_item_qty;
						$child_data['arr_child']['item_code'][] = $child->item_code;
						$child_data['arr_child']['item_desc'][] = $child->item_desc;
						$child_data['arr_child']['item_dimension'][] = $child->item_dimension;
					endif;
				endforeach;
			endforeach;
			if (isset($child_data)) :
				for ($i = 0; $i < count($child_data); $i++) :
					if (empty($child_data['arr_child']['parent_kd'][$i])) :
						$no++;
						$tot_qty[] = $child_data['arr_child']['stuffing_item_qty'][$i];
						?>
						<tr>
							<td align="center"><?php echo $no; ?></td>
							<td><?php echo $child_data['arr_child']['item_code'][$i]; ?></td>
							<td><?php echo item_stat($child_data['arr_child']['item_status'][$i]); ?></td>
							<td><?php echo $child_data['arr_child']['item_desc'][$i]; ?></td>
							<td><?php echo $child_data['arr_child']['item_dimension'][$i]; ?></td>
							<td><?php echo $child_data['arr_child']['item_note_do'][$i]; ?></td>
							<td align="center"><?php echo $child_data['arr_child']['stuffing_item_qty'][$i]; ?></td>
						</tr>
						<?php
					endif;
				endfor;
			endif;
			$tots_qty = array_sum($tot_qty);
			?>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td><b>Grand Total</b></td>
				<td></td>
				<td></td>
				<td style="text-align: center;"><b><?php echo $tots_qty; ?></b></td>
			</tr>
		</tbody>
	</table>
	<!-- END DO ITEMS DETAIL -->
	<div class="row invoice-info print-foot" style="margin-top: 15px;">
		<div class="col-sm-3 invoice-col">
			<?php
			if ($ket_note_so == 'available') :
				?>
				<div class="row invoice-info print-foot" style="margin-top: 15px;">
					Catatan :
					<?php echo $note_so; ?>
				</div>
				<?php
			endif;
			?>
		</div>
	</div>
	<div class="row invoice-info print-foot" style="margin-top: 15px;">
		<div class="col-xs-12">Sidoarjo, <?php echo $tgl_do; ?></div>
	</div>
	<div class="row invoice-info print-foot" style="margin-top: 15px;">
		<div class="col-xs-3" style="text-align: center;">
			<div style="margin-bottom: 100px;">
				<b>Authorized on behalf of</b><br>
				<b>PT. Caterlindo</b>
			</div>
			<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
			<b><?php echo $nm_salesperson; ?></b><br>
			Senior Sales & Marketing
		</div>
		<div class="col-xs-6"></div>
		<div class="col-xs-3">
			<div style="margin-bottom: 100px;text-align: center;">
				<b>Received by,</b>
			</div>
			<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
		</div>
	</div>
	<div class="row invoice-info print-foot" style="margin-top: 15px;">
		<div class="col-xs-4" style="text-align: center;">
			<div style="margin-bottom: 100px;text-align: center;">
				<b>Driver,</b>
			</div>
			<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
		</div>
		<div class="col-xs-4">
			<div style="margin-bottom: 100px;text-align: center;">
				<b>Security Check,</b>
			</div>
			<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
		</div>
		<div class="col-xs-4">
			<div style="margin-bottom: 100px;text-align: center;">
				<b>FG Warehouse Check,</b>
			</div>
			<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
		</div>
	</div>
	<div class="row">
		<hr>
		<table class="div-footer" style="width: 50%; " border="0">
			<tbody>
				<tr>
					<td>
						<img src="<?php echo base_url(); ?>assets/admin_assets/dist/img/LogoISO2021.jpg" alt="" >
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>