<?xml version="1.0" encoding="utf-8"?>
<TaxInvoiceBulk xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <TIN>0018245100055000</TIN>
  <ListOfTaxInvoice>
    <TaxInvoice>
      <TaxInvoiceDate><?= format_date($tgl_do, 'd M Y') ?></TaxInvoiceDate>
      <TaxInvoiceOpt>Normal</TaxInvoiceOpt>
      <TrxCode>04</TrxCode>
      <AddInfo>0</AddInfo>
      <CustomDoc/>
      <RefDesc><?= $no_invoice; ?></RefDesc>
      <FacilityStamp>0</FacilityStamp>
      <SellerIDTKU>000000</SellerIDTKU>
      <BuyerTin><?= $npwp_customer; ?></BuyerTin>
      <BuyerDocument>0</BuyerDocument>
      <BuyerCountry>IDN</BuyerCountry>
      <BuyerDocumentNumber>-</BuyerDocumentNumber>
      <BuyerName><?= $nm_cust; ?></BuyerName>
      <BuyerAdress><?= str_replace('<br>','',$alamat_cust); ?></BuyerAdress>
      <BuyerEmail><?= $email_customer; ?></BuyerEmail>
      <BuyerIDTKU>000000</BuyerIDTKU>
      <ListOfGoodService>
	  <?php foreach($parent_items as $parent) : ?>
        
		<GoodService>
          <Opt>A</Opt>
          <Code>000000</Code>
		  <Name><?= $parent->item_code; ?></Name>
          <Unit>UM.0021</Unit>
          <Price><?= $parent->harga_barang; ?></Price>
          <Qty><?= $parent->stuffing_item_qty; ?></Qty>
          <TotalDiscount><?= $parent->item_disc; ?></TotalDiscount>
          <TaxBase><?= $parent->total_harga; ?></TaxBase>
          <OtherTaxBase><?= floor(($parent->total_harga * 11) / 12); ?></OtherTaxBase>
          <VATRate>12</VATRate>
          <VAT><?= floor((($parent->total_harga * 11) / 12)* 12); ?></VAT>
          <STLGRate>0</STLGRate>
          <STLG>0</STLG>
        </GoodService>

       <?php endforeach; ?> 

		<?php foreach ($child_items as $child) : ?>

			<GoodService>
				<Opt>A</Opt>
				<Code>000000</Code>
				<Name><?= $child->item_code; ?></Name>
				<Unit>UM.0021</Unit>
				<Price><?= $child->harga_barang; ?></Price>
				<Qty><?= $child->stuffing_item_qty; ?></Qty>
				<TotalDiscount><?= $child->item_disc; ?></TotalDiscount>
				<TaxBase><?= $child->total_harga; ?></TaxBase>
				<OtherTaxBase><?= floor(($child->total_harga * 11) / 12); ?></OtherTaxBase>
				<VATRate>12</VATRate>
				<VAT><?= floor((($child->total_harga * 11) / 12)* 12); ?></VAT>
				<STLGRate>0</STLGRate>
				<STLG>0</STLG>
			</GoodService>

		<?php endforeach; ?>

      </ListOfGoodService>
    </TaxInvoice>
</ListOfTaxInvoice>
</TaxInvoiceBulk>
