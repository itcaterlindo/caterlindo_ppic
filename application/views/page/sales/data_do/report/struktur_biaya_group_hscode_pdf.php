<?php

class customPdf extends Tcpdf {

    public function Header() {
        $header = '
        <p style="margin-left:40px text-align:center"><img alt="" src="http://202.148.25.50/caterlindo_ppic/assets/admin_assets/dist/img/invoice_header.png" style="width:450px" /></p>

        <p style="margin-left:40px; text-align:center"><strong><span style="font-size:20px">FORMAT STRUKTUR BIAYA PER UNIT</span></strong></p>
        ';
        
        $this->writeHTML($header, true, false, false, false, '');
    }
}

$pdf = new customPdf('P', 'mm', 'A4', true, 'UTF-8', false);
$title = 'Struktur Biaya Group HS Code';
$pdf->SetTitle($title);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');
/** Margin */ 
// $pdf->SetHeaderMargin(50);
// $pdf->SetFooterMargin(10);
$pdf->SetMargins(5, 5, 5);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage();
$pdf->writeHTML($kontens, true, false, false, false, '');

$txtOutput = $title.'.pdf';
$pdf->Output($txtOutput, 'I');