<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

$arrayAsean = []; $arrayLokal = []; $arrayElse = [];
#kategori barang
foreach ($headers as $header): 
    #first definition
    $hargaSampaiKapal = round($invoiceDistributors[$header['kat_barang_kd']]['sum_harga_item'], 2);
    
    foreach($partdetails as $partdetail):
        if ($partdetail['kd_kat_barang'] == $header['kat_barang_kd']) :
            if ($partdetail['negaragroup_kd'] == 1) {
                # ASEAN
                $arrayAsean[$header['kat_barang_kd']][] = $partdetail;
            }elseif($partdetail['rm_jenisperolehan'] == 'lokal'){
                # LOKAL
                $arrayLokal[$header['kat_barang_kd']][] = $partdetail;
            }else{
                # ELSE
                $arrayElse[$header['kat_barang_kd']][] = $partdetail;
            }
        endif;
    endforeach;
?>

<!-- START DO HEADER DETAIL -->
<table border="0" style="font-size:75%; width: 100%;">
    <tr>
        <td style="font-weight:bolt;">NAMA BARANG : </td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $header['hs_code'].' | '.$header['nm_kat_barang']; ?></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="font-weight:bolt;">DIEKSPOR KE : </td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo isset($nm_cust) ? $nm_cust : 'Australia'; ?></td>
    </tr>
</table>

<!-- END DO HEADER DETAIL -->
<!-- START DO ITEMS DETAIL -->
<p style="font-size:75%; font-weight: bolt;">A. BAHAN KOMPONEN YANG DI IMPOR ATAU TIDAK DIKETAHUI ASALNYA</p>
<table border="1" style="font-size: 70%; width: 100%;">
    <tr style="font-weight: bolt; text-align: center;">
        <th width="10%">No</th>
        <th width="10%">HS Code</th>
        <th width="40%">Uraian Barang</th>
        <th width="10%">Jenis Perolehan</th>
        <th width="10%">Negara Asal</th>
        <th width="10%">Nilai</th>
        <th width="10%">%</th>
    </tr>
<?php 
    $subtotalCurrencyElse = 0; $totalCurrencyElse = 0; 
    if (isset($arrayElse[$header['kat_barang_kd']])) :
        $noElse = 1; 
        foreach($arrayElse[$header['kat_barang_kd']] as $eachElse): 
            # Hitung subtotal
            $totalCurrencyElse = $eachElse['sum_partdetail_total'] / $do['currency_rp'];
            $totalCurrencyElse = round($totalCurrencyElse, 2);
            $subtotalCurrencyElse += $totalCurrencyElse;
            # %
            $totalCurrencyElsePercent = $totalCurrencyElse / $hargaSampaiKapal * 100;
            $totalCurrencyElsePercent = round($totalCurrencyElsePercent, 2);
        ?>
            <tr>
                <td><?php echo $noElse;?></td>
                <td><?php echo $eachElse['rm_hs_code']?></td>
                <td><?php echo $eachElse['rm_kode'].' | '.$eachElse['partdetail_deskripsi'].' / '.$eachElse['partdetail_spesifikasi'] ;?></td>
                <td><?php echo ucwords($eachElse['rm_jenisperolehan']); ?></td>
                <td><?php echo $eachElse['nm_negara']?></td>
                <td style="text-align: right;">&#36; <?php echo number_format($totalCurrencyElse, 2); ?></td>
                <td style="text-align: center;"><?php echo number_format($totalCurrencyElsePercent, 2) ?></td>
            </tr>
        <?php 
        $noElse++;
        endforeach; 
        $subtotalCurrencyElse = round($subtotalCurrencyElse, 2);
        # %
        $subtotalCurrencyElsePercent = $subtotalCurrencyElse / $hargaSampaiKapal * 100;
        $subtotalCurrencyElsePercent = round($subtotalCurrencyElsePercent, 2);
        ?>
        <tr style="font-weight: bold;">
            <td colspan="5" style="text-align: right;">Total :</td>
            <td style="text-align: right;">&#36; <?php echo number_format($subtotalCurrencyElse, 2); ?></td>
            <td style="text-align: center;"><?php echo number_format($subtotalCurrencyElsePercent, 2); ?></td>
        </tr>
        <?php
    endif;?>
</table>

<p style="font-size:75%; font-weight: bolt;">B. BAHAN KOMPONEN YANG DI IMPOR DARI ASEAN</p>
<table border="1" style="font-size: 70%; width: 100%">
    <tr style="font-weight: bolt; text-align: center;">
        <th width="10%">No</th>
        <th width="10%">HS Code</th>
        <th width="40%">Uraian Barang</th>
        <th width="10%">Jenis Perolehan</th>
        <th width="10%">Negara Asal</th>
        <th width="10%">Nilai</th>
        <th width="10%">%</th>
    </tr>
    <?php 
    $subtotalCurrencyAsean = 0;
    if (isset($arrayAsean[$header['kat_barang_kd']])) :
        $noAsean = 1; $totalCurrencyAsean = 0; 
        foreach($arrayAsean[$header['kat_barang_kd']] as $eachAsean): 
            # Hitung subtotal
            $totalCurrencyAsean = $eachAsean['sum_partdetail_total'] / $do['currency_rp'];
            $totalCurrencyAsean = round($totalCurrencyAsean, 2);
            $subtotalCurrencyAsean += $totalCurrencyAsean;
            # %
            $totalCurrencyAseanPercent = $totalCurrencyAsean / $hargaSampaiKapal * 100;
            $totalCurrencyAseanPercent = round($totalCurrencyAseanPercent, 2);
        ?>
            <tr>
                <td><?php echo $noAsean?></td>
                <td><?php echo $eachAsean['rm_hs_code']?></td>
                <td><?php echo $eachAsean['rm_kode'].' | '.$eachAsean['partdetail_deskripsi'].' / '.$eachAsean['partdetail_spesifikasi']?></td>
                <td><?php echo ucwords($eachAsean['rm_jenisperolehan'])?></td>
                <td><?php echo $eachAsean['nm_negara']?></td>
                <td style="text-align: right;">&#36; <?php echo number_format($totalCurrencyAsean, 2)?></td>
                <td style="text-align: center;"><?php echo number_format($totalCurrencyAseanPercent, 2) ?></td>
            </tr>
        <?php 
            $noAsean++;
        endforeach; 
        $subtotalCurrencyAsean = round($subtotalCurrencyAsean, 2);
        # %
        $subtotalCurrencyAseanPercent = $subtotalCurrencyAsean / $hargaSampaiKapal * 100;
        $subtotalCurrencyAseanPercent = round($subtotalCurrencyAseanPercent, 2);
        ?> 
        <tr style="font-weight: bold;">
            <td colspan="5" style="text-align: right;">Total :</td>
            <td style="text-align: right;">&#36; <?php echo number_format($subtotalCurrencyAsean, 2); ?></td>
            <td style="text-align: center;"><?php echo number_format($subtotalCurrencyAseanPercent, 2); ?></td>
        </tr>
        <?php
    endif;?>
</table>

<p style="font-size:75%; font-weight: bolt;">C. BAHAN KOMPONEN YANG BERASAL DARI INDONESIA</p>
<table border="1" style="font-size: 70%; width: 100%;">
    <tr style="font-weight: bolt; text-align: center;">
        <th width="10%">No</th>
        <th width="10%">HS Code</th>
        <th width="50%">Uraian Barang</th>
        <th width="10%">Jenis Perolehan</th>
        <th width="10%">Nilai</th>
        <th width="10%">%</th>
    </tr>
<?php 
    $subtotalCurrencyLokal = 0;
    if (isset($arrayLokal[$header['kat_barang_kd']])) :
        $noLokal = 1; $totalCurrencyLokal = 0; 
        foreach($arrayLokal[$header['kat_barang_kd']] as $eachLokal): 
                # Hitung subtotal
                $totalCurrencyLokal = $eachLokal['sum_partdetail_total'] / $do['currency_rp'];
                $totalCurrencyLokal = round($totalCurrencyLokal, 2);
                $subtotalCurrencyLokal += $totalCurrencyLokal;
                # %
                $totalCurrencyLokalPercent = $totalCurrencyLokal / $hargaSampaiKapal * 100;
                $totalCurrencyLokalPercent = round($totalCurrencyLokalPercent, 2);
        ?>
            <tr>
                <td><?php echo $noLokal?></td>
                <td><?php echo $eachLokal['rm_hs_code']?></td>
                <td><?php echo $eachLokal['rm_kode'].' | '.$eachLokal['partdetail_deskripsi'].' / '.$eachLokal['partdetail_spesifikasi']?></td>
                <td><?php echo ucwords($eachLokal['rm_jenisperolehan'])?></td>
                <td style="text-align: right;">&#36; <?php echo number_format($totalCurrencyLokal, 2)?></td>
                <td style="text-align: center;"><?php echo number_format($totalCurrencyLokalPercent, 2) ?></td>
            </tr>
        <?php 
            $noLokal++;
        endforeach; 
        $subtotalCurrencyLokal = round($subtotalCurrencyLokal, 2);
        # %
        $subtotalCurrencyLokalPercent = (float) $subtotalCurrencyLokal / $hargaSampaiKapal * 100;
        $subtotalCurrencyLokalPercent = round($subtotalCurrencyLokalPercent, 2);
        ?>
        <tr style="font-weight: bold;">
            <td colspan="4" style="text-align: right;">Total :</td>
            <td style="text-align: right;">&#36; <?php echo number_format($subtotalCurrencyLokal, 2); ?></td>
            <td style="text-align: center;"><?php echo number_format($subtotalCurrencyLokalPercent, 2); ?></td>
        </tr>
        <?php
    endif;?>
</table>

<p style="font-size:75%; font-weight: bolt;">D. BIAYA PRODUKSI LANGSUNG</p>
    <p style="font-size:70%; font-weight: bolt;">Labourcost</p>
    <table border="1" style="font-size: 70%; width: 50%;">
        <tr style="font-weight: bolt; text-align: center;">
            <th width="15%">No</th>
            <th width="30%">Divisi</th>
            <th width="30%">Nilai</th>
            <th width="25%">%</th>
        </tr>
        <?php 
        $noLabourcost = 1; $totalCurrencyLabourcost = 0; $subtotalCurrencyLabourcost = 0;
        foreach($partlabourcosts as $eachLabourcost): 
            if ($eachLabourcost['kd_kat_barang'] == $header['kat_barang_kd']):
                # Hitung subtotal
                $totalCurrencyLabourcost = $eachLabourcost['sum_partlabourcost_total'] / $do['currency_rp'];
                $totalCurrencyLabourcost = round($totalCurrencyLabourcost, 2);
                $subtotalCurrencyLabourcost += $totalCurrencyLabourcost;
                # %
                $totalCurrencyLabourcostPercent = $totalCurrencyLabourcost / $hargaSampaiKapal * 100;
                $totalCurrencyLabourcostPercent = round($totalCurrencyLabourcostPercent, 2);
            ?>
            <tr>
                <td><?php echo $noLabourcost?></td>
                <td><?php echo $eachLabourcost['bagian_nama']?></td>
                <td style="text-align: right;">&#36; <?php echo number_format($totalCurrencyLabourcost, 2)?></td>
                <td style="text-align: center;"><?php echo number_format($totalCurrencyLabourcostPercent, 2) ?></td>
            </tr>
        <?php 
                $noLabourcost++;
            endif; 
        endforeach; 
        $subtotalCurrencyLabourcost = round($subtotalCurrencyLabourcost, 2);
        # %
        $subtotalCurrencyLabourcostPercent = $subtotalCurrencyLabourcost / $hargaSampaiKapal * 100;
        $subtotalCurrencyLabourcostPercent = round($subtotalCurrencyLabourcostPercent, 2);
        ?>
        <tr style="font-weight: bold;">
            <td colspan="2" style="text-align: right;">Total :</td>
            <td style="text-align: right;">&#36; <?php echo number_format($subtotalCurrencyLabourcost, 2); ?></td>
            <td style="text-align: center;"><?php echo number_format($subtotalCurrencyLabourcostPercent, 2); ?></td>
        </tr>
    </table>

    <p style="font-size:70%; font-weight: bolt;">Overhead</p>
    <table border="1" style="font-size: 70%; width: 50%;">
        <tr style="font-weight: bolt; text-align: center;">
            <th width="15%">No</th>
            <th width="30%">Uraian</th>
            <th width="30%">Nilai</th>
            <th width="25%">%</th>
        </tr>
        <?php 
        $noOverhead = 1; $totalCurrencyOverhead = 0; $subtotalCurrencyOverhead = 0;
        foreach($partoverheads as $eachOverhead): 
            if ($eachOverhead['kd_kat_barang'] == $header['kat_barang_kd']):
                # Hitung subtotal
                $totalCurrencyOverhead = $eachOverhead['sum_partoverhead_total'] / $do['currency_rp'];
                $totalCurrencyOverhead = round($totalCurrencyOverhead, 2);
                $subtotalCurrencyOverhead += $totalCurrencyOverhead;
                # %
                $totalCurrencyOverheadPercent = $totalCurrencyOverhead / $hargaSampaiKapal * 100;
                $totalCurrencyOverheadPercent = round($totalCurrencyOverheadPercent, 2);
            ?>
            <tr>
                <td><?php echo $noOverhead?></td>
                <td><?php echo $eachOverhead['overhead_nama']?></td>
                <td style="text-align: right;">&#36; <?php echo number_format($totalCurrencyOverhead, 2);?></td>
                <td style="text-align: center;"><?php echo number_format($totalCurrencyOverheadPercent, 2) ?></td>
            </tr>
        <?php 
                $noOverhead++;
            endif;
        endforeach;
        $subtotalCurrencyOverhead = round($subtotalCurrencyOverhead, 2); 
         # %
         $subtotalCurrencyOverheadPercent = $subtotalCurrencyOverhead / $hargaSampaiKapal * 100;
         $subtotalCurrencyOverheadPercent = round($subtotalCurrencyOverheadPercent, 2);
        ?>
        <tr style="font-weight: bold;">
            <td colspan="2" style="text-align: right;">Total :</td>
            <td style="text-align: right;">&#36; <?php echo number_format($subtotalCurrencyOverhead, 2); ?></td>
            <td style="text-align: center;"><?php echo number_format($subtotalCurrencyOverheadPercent, 2); ?></td>
        </tr>
    </table>
<!-- END DO ITEMS DETAIL -->
<p style="font-size:75%; font-weight: bolt;">E. KEUNTUNGAN</p>
<?php 
    $biayaAngkutBarang = $biayaAngkuts[$header['kat_barang_kd']];
    $totalBiayaBahan = $subtotalCurrencyElse + $subtotalCurrencyAsean + $subtotalCurrencyLokal + 
        $subtotalCurrencyLabourcost + $subtotalCurrencyOverhead + $biayaAngkutBarang;
    $keuntungan = $hargaSampaiKapal - $totalBiayaBahan;

    #%
    $totalBiayaBahanPercent = $totalBiayaBahan / $hargaSampaiKapal * 100;
    $totalBiayaBahanPercent = round($totalBiayaBahanPercent, 2);

    $keuntunganPercent = $keuntungan / $hargaSampaiKapal * 100;
    $keuntunganPercent = round($keuntunganPercent, 2);

    $biayaAngkutBarangPercent = $biayaAngkutBarang / $hargaSampaiKapal * 100;
    $biayaAngkutBarangPercent = round($biayaAngkutBarangPercent, 2);

?>
<table border="1" style="font-size: 70%; width: 50%;">
    <tr style="font-weight: bolt; text-align: center;">
        <th width="50%">Uraian</th>
        <th width="25%">Nilai</th>
        <th width="25%">%</th>
    </tr>
    <!-- <tr>
        <td style="text-align: left;">Harga sampai ke Kapal :</td>
        <td style="text-align: right;">&#36; <?php echo number_format($hargaSampaiKapal, 2); ?></td>
        <td style="text-align: center;">100</td>
    </tr>
    <tr>
        <td style="text-align: left;">Total Biaya dan Bahan (A+B+C+D+F) :</td>
        <td style="text-align: right;">&#36; <?php echo number_format($totalBiayaBahan, 2); ?></td>
        <td style="text-align: center;"><?php echo number_format($totalBiayaBahanPercent, 2); ?></td>
    </tr> -->
    <tr style="font-weight: bold;">
        <td width="50%" style="text-align: right;">Total :</td>
        <td width="25%" style="text-align: right;">&#36; <?php echo number_format($keuntungan, 2); ?></td>
        <td width="25%" style="text-align: center;"><?php echo number_format($keuntunganPercent, 2); ?></td>
    </tr>
</table>

<p style="font-size:75%; font-weight: bolt;">F. BIAYA PENGANGKUTAN BARANG SAMPAI KAPAL</p>
<table border="1" style="font-size: 70%; width: 50%;">
    <tr style="font-weight: bolt; text-align: center;">
        <th width="50%" >Uraian</th>
        <th width="25%" >Nilai</th>
        <th width="25%" >%</th>
    </tr>
    <tr style="font-weight: bold;">
        <td style="text-align: right;">Total :</td>
        <td style="text-align: right;">&#36; <?php echo number_format($biayaAngkutBarang, 2); ?></td>
        <td style="text-align: center;"><?php echo number_format($biayaAngkutBarangPercent, 2); ?></td>
    </tr>
</table>

<p style="font-size:75%; font-weight: bolt;">G. HARGA SAMPAI KE KAPAL</p>
<table border="1" style="font-size: 70%; width: 50%;">
    <tr style="font-weight: bolt; text-align: center;">
        <th width="50%" >Uraian</th>
        <th width="25%" >Nilai</th>
        <th width="25%" >%</th>
    </tr>
    <tr style="font-weight: bold;">
        <td style="text-align: right;">Total :</td>
        <td style="text-align: right;">&#36; <?php echo number_format($hargaSampaiKapal, 2); ?></td>
        <td style="text-align: center;">100</td>
    </tr>
</table>

<?php 
endforeach;
?>