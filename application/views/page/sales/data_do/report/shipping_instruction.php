<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
extract($format_laporan);
$master_var = 'DataDO';
$form_id = 'idDetail'.$master_var;
$color = color_status($status_do);
$word = process_status($status_do);
?>
<style type="text/css">
	body {
		font-size: 12px;
	}
	img {
		width: 100% !important;
	}
	.table>tbody>tr>td {
		padding:1px;
	}
	.table>tfoot>tr>td {
		padding:1px;
	}
	/* @media print {
		body {
			transform: scale(80%);
		}
		.print {
			page-break-after: always;
		}
		.print-foot {
			page-break-inside: avoid;
		}
		.div-footer {
			position: fixed;
			bottom: 0;
		}
		.print-left {
			text-align: left;
		}
		.print-center {
			text-align: center;
		}
		.print-right {
			text-align: right;
		}
	} */
</style>
<div class="row">
	<div class="col-xs-12">
		<a href="javascript:void(0);" class="btn btn-danger no-print" onclick="window.close();" style="margin-left: 25px;">
			<i class="fa fa-ban"></i> Close
		</a>
		<a href="javascript:void(0);" class="btn btn-primary no-print" onclick="window.print();" style="margin-left: 25px;">
			<i class="fa fa-print"></i> Cetak Data
		</a>
		<a href="<?php echo base_url().'report/shipping_instruction/report/'.$kd_mdo; ?>" class="btn btn-success no-print" style="margin-left: 25px;">
			<i class="fa fa-file-excel-o"></i> Export Excel
		</a>
	</div>
</div>
<div class="row invoice print">
	<!-- title row -->
	<div class="row">
		<div class="col-xs-12">
			
		<table border="0">
			<tr>
				<td style="width: 40%;">
					<img alt="" src="<?php echo my_baseurl(); ?>assets/admin_assets/dist/img/LogoSS2021.jpg" />
				</td>
				
				<td style="width: 60%;">

					<table border="0" style="width: 100%;"> 
						<tr>
							<td colspan="3" style="text-align: right; font-size: 36px; font-weight: bold;">SHIPPING INSTRUCTION</td>
						</tr>
						<tr>
							<td width="30%"></td>
							<td width="35%" style="font-size: 12px; border-bottom: 1px solid black;">No. Dokumen</td>
							<td width="35%" style="font-size: 12px; text-align: left; border-bottom: 1px solid black;">: CAT4-SLS-002</td>
						</tr>
						<tr>
							<td></td>
							<td style="font-size: 12px; border-bottom: 1px solid black;">Tanggal</td>
							<td style="font-size: 12px; border-bottom: 1px solid black;">: 12 Agustus 2021</td>
						</tr>
						<tr>
							<td></td>
							<td style="font-size: 12px; border-bottom: 1px solid black;">Revisi</td>
							<td style="font-size: 12px; border-bottom: 1px solid black;">: 04</td>
						</tr>
					</table>

				</td>
			</tr>
		</table>
		<hr>
		<h3>
			<div style="text-align: center;"><b><u>Shipping Instruction</u></b></div>
			<div style="text-align: center;"><?php echo $no_invoice; ?></div>
		</h3>

		</div><!-- /.col -->
	</div>
	<!-- START DO HEADER DETAIL -->
	<div class="row invoice-info print-foot" style="margin-top: 15px;">
		<div class="col-sm-1 invoice-col">
			1. SHIPPER :
		</div>
		<div class="col-sm-3 invoice-col">
			<strong><?php echo $nm_jasakirim; ?></strong><br>
			<?php echo $alamat_jasakirim; ?>
		</div>
	</div>
	<div class="row invoice-info print-foot" style="margin-top: 15px;">
		<div class="col-sm-1 invoice-col">
			2. CONSIGNEE :
		</div>
		<div class="col-sm-3 invoice-col">
			<strong><?php echo $nm_cust; ?></strong><br>
			<?php echo $nm_contact_cust.after_before_char($nm_contact_cust, $nm_contact_cust, '<br>', ''); ?>
			<?php echo $alamat_cust; ?>
		</div>
	</div>
	<div class="row invoice-info print-foot" style="margin-top: 15px;">
		<div class="col-sm-1 invoice-col">
			NOTIFY PARTY :
		</div>
		<div class="col-sm-3 invoice-col">
			<strong><?php echo $notify_name; ?></strong><br>
			<?php echo $notify_address; ?>
		</div>
	</div>
	<div class="row invoice-info print-foot" style="margin-top: 15px;">
		<div class="col-sm-1 invoice-col">
			NUMBER OF BOXS
		</div>
		<div class="col-sm-3 invoice-col">
			<strong><?php echo $jml_koli; ?></strong>
		</div>
	</div>
	<!-- END DO HEADER DETAIL -->
	<hr>
	<!-- START DO ITEMS DETAIL -->
	<h6>3. DESCRIPTION OF GOODS :</h6>
	<div class="row">
		<table class="table table-striped">
			<tbody>
				<?php
				$no = 0;
				$tots_stuff = array('');
				$tots_nw = array('');
				$tots_gw = array('');
				foreach ($parent_items as $parent) :
					$no++;
					$item_desc[] = $parent->item_desc;
					$stuffing_item_qty[$parent->item_desc][] = $parent->stuffing_item_qty;
					$hs_code[$parent->item_desc] = $parent->hs_code;

					$tots_stuff[] = $parent->stuffing_item_qty;
					$tots_nw[] = $parent->stuffing_item_qty * $parent->netweight;
					$tots_gw[] = $parent->stuffing_item_qty * $parent->grossweight;
				endforeach;
				foreach ($child_items as $child) :
					$no++;
					$item_desc[] = $child->item_desc;
					$stuffing_item_qty[$child->item_desc] = $child->stuffing_item_qty;
					$hs_code[$child->item_desc] = $child->hs_code;

					$tots_stuff[] = $child->stuffing_item_qty;
					$tots_nw[] = $child->stuffing_item_qty * $child->netweight;
					$tots_gw[] = $child->stuffing_item_qty * $child->grossweight;
				endforeach;
				$item_desc_unique = array_unique($item_desc);
				foreach ($item_desc_unique as $item_description) :
					$data[] = array('stuffing' => array_sum($stuffing_item_qty[$item_description]), 'hs' => $hs_code[$item_description], 'item_description' => $item_description);
				endforeach;
				asort($data);
				foreach ($data as $key => $value) :
					?>
					<tr>
						<td style="text-align: center;"><?php echo $value['stuffing']; ?></td>
						<td style="text-align: center;">Pcs</td>
						<td style="text-align: center;"><?php echo $value['hs']; ?></td>
						<td><?php echo $value['item_description']; ?></td>
					</tr>
					<?php
				endforeach;
				$tot_stuff = array_sum($tots_stuff);
				$tot_nw = array_sum($tots_nw);
				$tot_gw = array_sum($tots_gw);
				?>
			</tbody>
		</table>
	</div>
	<!-- END DO ITEMS DETAIL -->
	<div class="row print-foot" style="margin-top: 15px;">
		<div class="col-sm-6">
			4. NETT / GROSS WEIGHT : <?php echo $tot_nw.' / '.$tot_gw.' KGS'; ?>
		</div>
	</div>
	<div class="row print-foot" style="margin-top: 15px;">
		<div class="col-sm-6">
			5. MEASUREMENT :
		</div>
	</div>
	<div class="row print-foot" style="margin-top: 15px;">
		<div class="col-sm-6">
			6. LC/NON LC : NON LC
		</div>
	</div>
	<div class="row print-foot" style="margin-top: 15px;">
		<div class="col-sm-6">
			7. SHIPPED ON BOARD : <?php echo $tgl_do; ?>
		</div>
	</div>
	<hr>
	<table class="div-footer" style="width: 50%; " border="0">
		<tbody>
			<tr>
				<td>
					<img src="<?php echo base_url(); ?>assets/admin_assets/dist/img/LogoISO2021.jpg" alt="" >
				</td>
			</tr>
		</tbody>
	</table>
</div>
