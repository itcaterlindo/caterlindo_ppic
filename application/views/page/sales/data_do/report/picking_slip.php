<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
extract($format_laporan);
$master_var = 'DataDO';
$form_id = 'idDetail'.$master_var;
$color = color_status($status_do);
$word = process_status($status_do);
$no_form = !empty($no_form)?$no_form.'b':'';
?>
<style type="text/css">
	body {
		font-size: 12px;
	}
	img {
		width: 100% !important;
	}
	thead.report-header {
		display: table-header-group;
	}

	tfoot.report-footer {
		display:table-footer-group;
	}
	table.report-container {
		page-break-after: always;
	}
	/* @media print {
		body {
			transform: scale(80%);
		}
		.print-left {
			text-align: left;
		}
		.print-center {
			text-align: center;
		}
		.print-right {
			text-align: right;
		}
	} */
</style>
<div class="row">
	<a href="javascript:void(0);" class="btn btn-danger no-print" onclick="window.close();" style="margin-left: 25px;">
		<i class="fa fa-ban"></i> Close
	</a>
	<a href="javascript:void(0);" class="btn btn-primary no-print" onclick="window.print();" style="margin-left: 25px;">
		<i class="fa fa-print"></i> Cetak Data
	</a>
	<a href="<?php echo base_url().'report/picking_slip/report/'.$kd_mdo; ?>" class="btn btn-success no-print" style="margin-left: 25px;">
		<i class="fa fa-file-excel-o"></i> Export Excel
	</a>
</div>
<div class="row invoice print">
	<table class="">
		<thead class="report-header">
			<tr>
				<td>
					<div class="header-info"></div>
				</td>
			</tr>
		</thead>
		<tbody class="report-content">
			<tr>
				<td>
					<!-- title row -->
					<div class="row">
						<div class="col-xs-12">

							<table border="0">
								<tr>
									<td style="width: 40%;">
										<img alt="" src="<?php echo my_baseurl(); ?>assets/admin_assets/dist/img/LogoSS2021.jpg" />
									</td>
									
									<td style="width: 60%;">

										<table border="0" style="width: 100%;"> 
											<tr>
												<td colspan="3" style="text-align: right; font-size: 36px; font-weight: bold;">PICKING SLIP</td>
											</tr>
											<tr>
												<td width="30%"></td>
												<td width="35%" style="font-size: 12px; border-bottom: 1px solid black;">No. Dokumen</td>
												<td width="35%" style="font-size: 12px; text-align: left; border-bottom: 1px solid black;">: CAT4-SLS-010</td>
											</tr>
											<tr>
												<td></td>
												<td style="font-size: 12px; border-bottom: 1px solid black;">Tanggal</td>
												<td style="font-size: 12px; border-bottom: 1px solid black;">: 12 Agustus 2021</td>
											</tr>
											<tr>
												<td></td>
												<td style="font-size: 12px; border-bottom: 1px solid black;">Revisi</td>
												<td style="font-size: 12px; border-bottom: 1px solid black;">: 04</td>
											</tr>
										</table>

									</td>
								</tr>
							</table>
							<hr>
							<h3>
								<div style="text-align: center;"><b><u>Picking Slip</u></b></div>
								<div style="text-align: center;"><?php echo $no_invoice; ?></div>
							</h3>
						</div><!-- /.col -->
					</div>
					<!-- START DO HEADER DETAIL -->
					<table>
						<tr>
							<th style="width: 150px;vertical-align: top;">Invoice To Buyer<br>( Notify Party )</th>
							<td style="vertical-align: top; padding-left: 10px; padding-right: 10px;">:</td>
							<td style="width: 250px;">
								<strong><?php echo $notify_name; ?></strong><br>
								<?php echo $notify_address; ?>
							</td>
						</tr>
						<?php
						if ($tipe_do == 'Ekspor') :
							?>
							<tr>
								<th style="width: 150px;vertical-align: top;">Consignee</th>
								<td style="vertical-align: top; padding-left: 10px; padding-right: 10px;">:</td>
								<td>
									<strong><?php echo $nm_cust; ?></strong><br>
									<?php echo $nm_contact_cust.after_before_char($nm_contact_cust, $nm_contact_cust, '<br>', ''); ?>
									<?php echo $alamat_cust; ?>
								</td>
							</tr>
							<?php
						endif;
						?>
						<tr>
							<th style="width: 150px;vertical-align: top;">Delivery Address</th>
							<td style="vertical-align: top; padding-left: 10px; padding-right: 10px;">:</td>
							<td>
								<strong><?php echo $nm_penerima; ?></strong><br>
								<?php echo $nm_contact_penerima.after_before_char($nm_contact_penerima, $nm_contact_penerima, '<br>', ''); ?>
								<?php echo $alamat_penerima; ?>
							</td>
							<th style="width: 150px;vertical-align: top; padding-left: 15px;">Date</th>
							<td style="vertical-align: top; padding-left: 10px; padding-right: 10px;">:</td>
							<td style="vertical-align: top;"><?php echo format_date($tgl_do, 'd M Y'); ?></td>
						</tr>
						<tr>
							<?php
							if ($tipe_do == 'Ekspor') :
								?>
								<th style="width: 150px;vertical-align: top;">Freight By</th>
								<td style="vertical-align: top; padding-left: 10px; padding-right: 10px;">:</td>
								<td>
									<strong><?php echo $nm_jasakirim; ?></strong><br>
									<?php echo $alamat_jasakirim; ?>
								</td>
								<?php
							endif;
							if (!empty($no_po)) :
								?>
								<th style="width: 150px;vertical-align: top; padding-left: 15px;">Order No</th>
								<td style="vertical-align: top; padding-left: 10px; padding-right: 10px;">:</td>
								<td style="vertical-align: top;">
									<?php
									foreach ($list_nopo as $list_po) :
										echo '<li>'.$list_po.'</li>';
									endforeach;
									?>
								</td>
								<?php
							endif;
							?>
						</tr>
						<?php
						if ($tipe_do == 'Ekspor') :
							?>
							<tr>
								<th style="width: 125px;vertical-align: top;">Description of goods</th>
								<td style="vertical-align: top; padding-left: 10px; padding-right: 10px;">:</td>
								<td><?php echo !empty($goods_desc)?$goods_desc:'Stainless Steel Goods'; ?></td>
							</tr>
							<tr>
								<th style="width: 125px;vertical-align: top;">Number of boxs</th>
								<td style="vertical-align: top;">:</td>
								<td><?php echo $jml_koli; ?></td>
							</tr>
							<?php
						endif;
						?>
					</table>
					<!-- END DO HEADER DETAIL -->
					<hr style="margin-bottom: 25px;">
					<!-- START DO ITEMS DETAIL -->
					<div class="row">
						<table style="width: 100%;" border="1">
							<thead>
								<tr>
									<td>No</td>
									<td>Code</td>
									<td>Description</td>
									<td>Size</td>
									<td>Qty</td>
								</tr>
							</thead>
							<tbody>
								<?php
								$tots_stuff = array('');
								$tots_nw = array('');
								$tots_gw = array('');
								foreach ($parent_items as $parent) :
									$item_data[] = array('item_code' => $parent->item_code, 'item_desc' => $parent->item_desc, 'item_dimension' => $parent->item_dimension, 'stuffing_item_qty' => $parent->stuffing_item_qty);
									$tots_stuff[] = $parent->stuffing_item_qty;
									$tots_nw[] = $parent->netweight;
									$tots_gw[] = $parent->grossweight;
								endforeach;
								foreach ($child_items as $child) :
									$item_data[] = array('item_code' => $child->item_code, 'item_desc' => $child->item_desc, 'item_dimension' => $child->item_dimension, 'stuffing_item_qty' => $child->stuffing_item_qty);
									$tots_stuff[] = $child->stuffing_item_qty;
									$tots_nw[] = $child->netweight;
									$tots_gw[] = $child->grossweight;
								endforeach;
								$tot_stuff = array_sum($tots_stuff);
								$tot_nw = array_sum($tots_nw);
								$tot_gw = array_sum($tots_gw);

								$no = 0;
								sort($item_data);
								for ($i = 0; $i < count($item_data); $i++) :
									$no++;
									?>
									<tr>
										<td style="text-align: center;"><?php echo $no; ?></td>
										<td><?php echo $item_data[$i]['item_code']; ?></td>
										<td><?php echo $item_data[$i]['item_desc']; ?></td>
										<td><?php echo $item_data[$i]['item_dimension']; ?></td>
										<td style="text-align: center;"><?php echo $item_data[$i]['stuffing_item_qty']; ?></td>
									</tr>
									<?php
								endfor;
								?>
								<tr>
									<td></td>
									<td></td>
									<td><b>Grand Total</b></td>
									<td></td>
									<td style="text-align: center;"><b><?php echo $tot_stuff; ?></b></td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- END DO ITEMS DETAIL -->
					<div class="row invoice-info print-foot" style="margin-top: 15px;">
						<ul>
							<li>Container Number : <b><?php echo $container_number; ?></b></li>
							<li>Seal Number : <b><?php echo $seal_number; ?></b></li>
						</ul>
					</div>
					<div class="row invoice-info print-foot" style="margin-top: 15px;">
						<div class="col-xs-6" style="text-align: center;">
							<div class="col-xs-6">
								<div style="margin-bottom: 100px;"><b>Prepared by,</b></div>
								<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
								<div><b>Exim</b></div>
							</div>
						</div>
						<div class="col-xs-6" style="text-align: center;">
							<div class="col-xs-6"></div>
							<div class="col-xs-6">
								<div style="margin-bottom: 100px;"><b>Reported by,</b></div>
								<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
								<div><b>Finished Goods</b></div>
							</div>
						</div>
						<div class="col-xs-6" style="text-align: center;margin-top: 15px;">
							<div class="col-xs-6">
								<div style="margin-bottom: 100px;">Monitored by,</div>
								<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
								<div><b>Security</b></div>
							</div>
						</div>
						<div class="col-xs-6" style="text-align: center;margin-top: 15px;">
							<div class="col-xs-6"></div>
							<div class="col-xs-6">
								<hr style="margin-top: 115px;margin-bottom: 5px;border-top: 2px solid #adadad;">
								<div><b>Driver</b></div>
							</div>
						</div>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<hr>
	<table class="div-footer" style="width: 50%; " border="0">
		<tbody>
			<tr>
				<td>
					<img src="<?php echo base_url(); ?>assets/admin_assets/dist/img/LogoISO2021.jpg" alt="" >
				</td>
			</tr>
		</tbody>
	</table>
</div>