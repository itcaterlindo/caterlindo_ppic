<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataBU';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'text', 'hidden' => 'txtKdMDo', 'value' => $kd_mdo));
?>
<div class="row invoice-info">
	<div class="col-sm-6 invoice-col">
		<b>No PO :</b> <?php echo isset($do['no_po']) ? $do['no_po'] : '-'; ?><br>
		<b>Tgl DO :</b> <?php echo isset($do['tgl_do']) ? $do['tgl_do'] : '-'; ?><br>
	</div>
	<div class="col-sm-6 invoice-col">
		<b>No Invoice :</b> <?php echo isset($do['no_invoice']) ? $do['no_invoice'] : '-'; ?><br>
		<b>Notify Address :</b> <?php echo isset($do['notify_address']) ? $do['notify_address'] : '-' ?><br>
	</div>
</div>
<hr>
<div class="row">

    <div class="form-group">
		<label for='idtxtpodetail_tgldelivery' class="col-md-2 control-label">Tanggal Proses</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrpodetail_tgldelivery"></div>
			<div class="input-group date">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'readonly' => 'readonly', 'placeholder' =>'Last Generate' ,'value'=> isset($do['tgl_generatebiayaunit']) ? $do['tgl_generatebiayaunit']: null ));?>
			</div>
		</div>
		<div class="col-sm-1 col-xs-12">
			<button class="btn btn-sm btn-default" onclick="strukturbiayaunit_form('<?php echo $kd_mdo?>')"> <i class="fa fa-wrench"></i> Set Proses</button>
	    </div>
		<div class="col-sm-1 col-xs-12">
			<button class="btn btn-sm btn-danger" id="idbtnproses" onclick="action_generatereport_strukturbiaya('<?php echo $kd_mdo?>')"> <i class="fa fa-refresh"></i> Proses </button>
		</div>
	</div>

    <div class="form-group">
		<label for='idtxtpodetail_konversi' class="col-md-2 control-label">Currency (Rp)</label>
		<div class="col-sm-2 col-xs-12">
            <div class="errInput" id="idErrpodetail_konversi"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control','readonly' => 'readonly', 'placeholder' =>'Currency' ,'value'=> isset($do['currency_rp']) ? $do['currency_rp']: null ));?>
		</div>
	</div>

    <div class="form-group">
		<label for='idtxtpodetail_konversi' class="col-md-2 control-label">Biaya Angkut (Rp)</label>
		<div class="col-sm-2 col-xs-12">
            <div class="errInput" id="idErrpodetail_konversi"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control','readonly' => 'readonly', 'placeholder' =>'Currency' ,'value'=> isset($do['biaya_angkut']) ? $do['biaya_angkut']: null ));?>
		</div>
	</div>

</div>
<?php echo form_close(); ?>