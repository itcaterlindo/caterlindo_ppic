<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$form_id = 'idFormPR';

$arrayGroupBarangs = [];
foreach ($results as $result) {
    $arrayGroupBarangs[$result['kd_kat_barang'].'|'.$result['nm_kat_barang']]
        [$result['kd_barang'].'|'.
            $result['item_code'].'|'.
            $result['deskripsi_barang'].'|'.
            $result['dimensi_barang']
        ][] = $result;
}

?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<div class="col-md-12">
    <table id="idTable" class="table table-bordered table-striped table-hover" style="width:100%; font-size:90%;">
        <thead>
        <tr>
            <th style="width:5%; text-align:center;">No</th>
            <th style="width:25%; text-align:center;">Item Code</th>
            <th style="width:5%; text-align:center;">Versi Part</th>
            <th style="width:7%; text-align:center;">Status Part</th>
        </tr>
        </thead>
        <tbody>
        <?php 
       
        $no = 1;
        foreach ($arrayGroupBarangs as $kat_barangs => $elements): 
            $expKatbarangs = explode('|', $kat_barangs);
        ?>
        <tr>
            <td colspan="5" style="font-weight: bold;" ><?php echo '('.$no.') '.$expKatbarangs[1] ?></td>
        </tr>
           <?php
           foreach ($elements as $barangs => $elementDetails) :
            $expBarangs = explode('|', $barangs);
            ?>
            <tr>
                <td style="font-weight: bold;">&#160;&#160;<?php echo $expBarangs[1]; ?></td>
                <td colspan="3"><?php echo $expBarangs[2].' '.$expBarangs[3] ; ?></td>
            </tr>
            <?php
                foreach ($elementDetails as $elementDetail) :
                    ?>
                    <tr>
                        <td></td>
                        <td>&#160;&#160;&#160; - <?php echo $elementDetail['partmain_nama']; ?></td>
                        <td><?php echo $elementDetail['part_versi']; ?></td>
                        <td><?php echo build_span ($elementDetail['partstate_label'], $elementDetail['partstate_nama']); ?></td>
                    </tr>
                    <?php
                endforeach;
            endforeach;
            $no++;
        endforeach;?>
        </tbody>
    </table>
</div>

<script type="text/javascript">

	// render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"paging": true,
            "searching": true,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
            "columnDefs": [
                {"searchable": true, "orderable": true, "className": "dt-center", "targets": 5},
            ],
		});
	}

</script>