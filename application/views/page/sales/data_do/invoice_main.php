<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataDO';
$form_id = 'idInvoice'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtKdMDo', 'value' => $kd_mdo));
?>
<div class="form-group">
	<label for="idTxtInvoice" class="col-md-2 control-label">Nomer Invoice</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrInvoice"></div>
		<?php echo form_input(array('name' => 'txtInvoice', 'id' => 'idTxtInvoice', 'class' => 'form-control', 'value' => $no_invoice)); ?>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" class="btn btn-primary btn-flat">
			<i class="fa fa-shopping-cart"></i> Submit
		</button>
	</div>
</div>
<?php echo form_close(); ?>