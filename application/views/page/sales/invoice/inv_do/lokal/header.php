<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$color = color_status($status_do);
$word = process_status($status_do);
?>
<!-- START DO HEADER DETAIL -->
<table style="width: 100%;">
	<tr>
		<td style="width: 100px; font-size: 16px;"><b>No Invoice</b></td>
		<td style="width: 25px; font-size: 16px;">:</td>
		<td style="width: 100px; font-size: 16px;"><?php echo $no_invoice; ?></td>
		<td style="width: 125px;vertical-align: top;"><b>Purchase Order No.</b></td>
		<td style="width: 25px;vertical-align: top;">:</td>
		<td style="width: 125px;vertical-align: top;"><?php echo $no_po; ?></td>
		<td style="width: 100px;vertical-align: top;" rowspan="2"><b>Tgl Kirim</b></td>
		<td style="width: 25px;vertical-align: top;" rowspan="2">:</td>
		<td style="width: 100px;vertical-align: top;" rowspan="2"><?php echo format_date($tgl_do, 'd M Y'); ?></td>
	</tr>
	<tr>
		<td style="font-size: 16px;"><b>Sales Order No.</b></td>
		<td style="font-size: 16px;">:</td>
		<td style="font-size: 16px;"><?php echo $no_salesorder; ?></td>
		<td><b>Jumlah Koli</b></td>
		<td>:</td>
		<td><?php echo $jml_koli; ?></td>
	</tr>
</table>
<hr style="margin-top: 5px;margin-bottom: 5px;">
<table style="width: 100%;">
	<tr>
		<td>Sales</td>
		<td>Customer</td>
		<td>Deliver To</td>
	</tr>
	<tr>
		<td><strong>PT Caterlindo</strong></td>
		<td><strong><?php echo $code_customer." ".$nm_cust; ?></strong></td>
		<td><strong><?php echo $nm_penerima; ?></strong></td>
	</tr>
	<tr>
		<td><?php echo $nm_salesperson; ?></td>
		<td><?php echo $nm_contact_cust; ?></td>
		<td><?php echo $nm_contact_penerima; ?></td>
	</tr>
	<tr>
		<td style="width: 200px;">Jln Industri No.18, Trosobo</td>
		<td rowspan="2" style="width: 250px;"><?php echo $alamat_cust; ?></td>
		<td rowspan="2" style="width: 250px;"><?php echo $alamat_penerima; ?></td>
	</tr>
	<tr>
		<td>Taman, Sidoarjo, Jawa Timur 61262</td>
	</tr>
	<tr>
		<td>Phone : <?php echo $telp_sales; ?></td>
		<td>Phone : <?php echo $telp_customer; ?></td>
		<td>Phone : <?php echo $telp_penerima; ?></td>
	</tr>
	<tr>
		<td rowspan="2" style="vertical-align: top;">Email : <?php echo $email_sales; ?></td>
		<td>Email : <?php echo $email_customer; ?></td>
		<td rowspan="2" style="vertical-align: top;">Email : <?php echo $email_penerima; ?></td>
	</tr>
	<tr>
		<td>NPWP/NIK : <?php echo $npwp_customer; ?></td>
	</tr>
</table>
<hr style="margin-top: 5px;margin-bottom: 5px;">