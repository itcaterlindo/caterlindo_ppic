<?php
defined('BASEPATH') or exit('No direct script access allowed!');
if (!empty($format_laporan)) :
	extract($format_laporan);
endif;

if (!empty($default_disc)) :
	foreach ($default_disc as $row) :
		$item_discs[$row->so_item_kd] = array('jml_disc' => $row->jml_disc, 'type_individual_disc' => $row->type_individual_disc, 'jml_individual_disc' => $row->jml_individual_disc);	
	endforeach;
endif;

$def_disc_set = $this->tb_default_disc->select_where(array('jenis_customer_kd' => $kd_jenis_customer, 'nm_pihak' => $tipe_invoice));
?>
<table style="column-width: 10px;width: 100%;" border="1">
	<thead>
		<tr>
			<th style="width: 1%;text-align: center;">No.</th>
			<th style="width: 10%;text-align: center;">Prod Code</th>
			<th style="width: 1%;text-align: center;">Status</th>
			<th style="width: 18%;text-align: center;">Description</th>
			<th style="width: 18%;text-align: center;">Dimension</th>
			<th style="width: 1%;text-align: center;">Qty</th>
			<th style="width: 13%;text-align: center;">Price/Unit</th>
			<th style="width: 12%;text-align: center;">Disc</th>
			<th style="width: 15%;text-align: center;">Subtotal</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach($parent_items as $parent) :
			$no_child = 0;
			$rows = '';
			foreach ($child_items as $child) :
				if ($parent->parent_kd == $child->parent_kd) :
					$no_child++;
				endif;
				$rowspan = 1 + $no_child;
				$rows = 'rowspan=\''.$rowspan.'\'';
			endforeach;
			$no++;

			if ($tipe_invoice == 'customer') :
				$disc_tipe_invoice = $parent->disc_customer != 'kosong'?$parent->disc_customer:$def_disc_set->jml_disc;
			elseif ($tipe_invoice == 'distributor') :
				$disc_tipe_invoice = $parent->disc_distributor != 'kosong'?$parent->disc_distributor:$def_disc_set->jml_disc;
			endif;
			$jml_disc = isset($item_discs[$parent->parent_kd]['jml_disc'])?$item_discs[$parent->parent_kd]['jml_disc']:$disc_tipe_invoice;
			$type_individual_disc = isset($item_discs[$parent->parent_kd]['type_individual_disc'])?$item_discs[$parent->parent_kd]['type_individual_disc']:'decimal';
			$jml_individual_disc = isset($item_discs[$parent->parent_kd]['jml_individual_disc'])?$item_discs[$parent->parent_kd]['jml_individual_disc']:'0';
			$item_price_retail = $parent->harga_barang;

			$convert_retail = empty($val_retail)?'1':$val_retail;
			$convert_detail = empty($val_detail)?'1':$val_detail;
			if ($type_retail == 'primary') :
				$item_price_retail = $parent->harga_retail / $convert_detail;
			elseif ($type_retail == 'secondary') :
				$item_price_retail = $parent->harga_retail * $convert_retail;
			endif;
			if ($currency_type == 'primary') :
				$item_price = $parent->harga_barang / $convert_detail;
			elseif ($currency_type == 'secondary') :
				$item_price = $parent->harga_barang * $convert_retail;
			endif;
			$disc_type = $parent->disc_type;
			$disc_val = $parent->item_disc;
			$disc = $item_price_retail - $item_price;
			$item_price = $item_price_retail - $disc;

			$jml_default_disc = count_disc('percent', $jml_disc, $item_price);
			$item_price = $item_price - $jml_default_disc;
			$val_individual_disc = count_disc($type_individual_disc, $jml_individual_disc, $item_price);
			$item_price = $item_price - $val_individual_disc;

			$item_disc = count_disc($disc_type, $disc_val, $item_price);
			$total_disc = ($item_price_retail - $item_price) + $item_disc;
			$total_harga = ($item_price_retail - $total_disc) * $parent->stuffing_item_qty;
			
			$tot_harga[] = $total_harga;
			$tot_disc[] = $total_disc;
			$tot_price[] = $item_price;
			$tot_qty[] = $parent->stuffing_item_qty;
			?>
			<tr>
				<td <?php echo $rows; ?> align="center"><?php echo $no; ?></td>
				<td><?php echo $parent->item_code; ?></td>
				<td><?php echo item_stat($parent->item_status); ?></td>
				<td><?php echo $parent->item_desc; ?></td>
				<td><?php echo $parent->item_dimension; ?></td>
				<td align="center"><?php echo $parent->stuffing_item_qty; ?></td>
				<td align="right"><?php echo format_currency($item_price_retail, $currency_icon); ?></td>
				<td align="right"><?php echo format_currency($total_disc, $currency_icon); ?></td>
				<td align="right"><?php echo format_currency($total_harga, $currency_icon); ?></td>
			</tr>
			<?php
			foreach ($child_items as $child) :
				if (!empty($child->parent_kd) && $parent->parent_kd == $child->parent_kd) :

					if ($tipe_invoice == 'customer') :
						$disc_tipe_invoice = $child->disc_customer != 'kosong'?$child->disc_customer:$def_disc_set->jml_disc;
					elseif ($tipe_invoice == 'distributor') :
						$disc_tipe_invoice = $child->disc_distributor != 'kosong'?$child->disc_distributor:$def_disc_set->jml_disc;
					endif;
					$jml_disc = isset($item_discs[$child->child_kd]['jml_disc'])?$item_discs[$child->child_kd]['jml_disc']:$disc_tipe_invoice;
					$type_individual_disc = isset($item_discs[$child->child_kd]['type_individual_disc'])?$item_discs[$child->child_kd]['type_individual_disc']:'decimal';
					$jml_individual_disc = isset($item_discs[$child->child_kd]['jml_individual_disc'])?$item_discs[$child->child_kd]['jml_individual_disc']:'0';
					$item_price_retail = $child->harga_barang;

					$convert_retail = empty($val_retail)?'1':$val_retail;
					$convert_detail = empty($val_detail)?'1':$val_detail;
					if ($type_retail == 'primary') :
						$item_price_retail = $child->harga_retail / $convert_detail;
					elseif ($type_retail == 'secondary') :
						$item_price_retail = $child->harga_retail * $convert_retail;
					endif;
					if ($currency_type == 'primary') :
						$item_price = $child->harga_barang / $convert_detail;
					elseif ($currency_type == 'secondary') :
						$item_price = $child->harga_barang * $convert_retail;
					endif;
					$disc_type = $child->disc_type;
					$disc_val = $child->item_disc;
					$disc = $item_price_retail - $item_price;
					$item_price = $item_price_retail - $disc;

					$jml_default_disc = count_disc('percent', $jml_disc, $item_price);
					$item_price = $item_price - $jml_default_disc;
					$val_individual_disc = count_disc($type_individual_disc, $jml_individual_disc, $item_price);
					$item_price = $item_price - $val_individual_disc;

					$item_disc = count_disc($disc_type, $disc_val, $item_price);
					$total_disc = ($item_price_retail - $item_price) + $item_disc;
					$total_harga = ($item_price_retail - $total_disc) * $child->stuffing_item_qty;
					
					$tot_harga[] = $total_harga;
					$tot_disc[] = $total_disc;
					$tot_price[] = $item_price;
					$tot_qty[] = $child->stuffing_item_qty;
					?>
					<tr>
						<td><?php echo $child->item_code; ?></td>
						<td><?php echo item_stat($child->item_status); ?></td>
						<td><?php echo $child->item_desc; ?></td>
						<td><?php echo $child->item_dimension; ?></td>
						<td align="center"><?php echo $child->stuffing_item_qty; ?></td>
						<td align="right"><?php echo format_currency($item_price_retail, $currency_icon); ?></td>
						<td align="right"><?php echo format_currency($total_disc, $currency_icon); ?></td>
						<td align="right"><?php echo format_currency($total_harga, $currency_icon); ?></td>
					</tr>
					<?php
				else :
					$child_data['arr_child']['parent_kd'][] = $child->parent_kd;
					$child_data['arr_child']['child_kd'][] = $child->child_kd;
					$child_data['arr_child']['harga_retail'][] = $child->harga_retail;
					$child_data['arr_child']['harga_barang'][] = $child->harga_barang;
					$child_data['arr_child']['disc_type'][] = $child->disc_type;
					$child_data['arr_child']['item_disc'][] = $child->item_disc;
					$child_data['arr_child']['item_status'][] = $child->item_status;
					$child_data['arr_child']['stuffing_item_qty'][] = $child->stuffing_item_qty;
					$child_data['arr_child']['item_code'][] = $child->item_code;
					$child_data['arr_child']['item_desc'][] = $child->item_desc;
					$child_data['arr_child']['item_dimension'][] = $child->item_dimension;
					$child_data['arr_child']['disc_customer'][] = $child->disc_customer;
					$child_data['arr_child']['disc_distributor'][] = $child->disc_distributor;
				endif;
			endforeach;
		endforeach;
		if (isset($child_data)) :
			for ($i = 0; $i < count($child_data); $i++) :
				if (empty($child_data['arr_child']['parent_kd'][$i])) :
					$no++;

					if ($tipe_invoice == 'customer') :
						$disc_tipe_invoice = $child_data['arr_child']['disc_customer'][$i] != 'kosong'?$child_data['arr_child']['disc_customer'][$i]:$def_disc_set->jml_disc;
					elseif ($tipe_invoice == 'distributor') :
						$disc_tipe_invoice = $child_data['arr_child']['disc_distributor'][$i] != 'kosong'?$child_data['arr_child']['disc_distributor'][$i]:$def_disc_set->jml_disc;
					endif;
					$jml_disc = isset($item_discs[$child_data['arr_child']['child_kd'][$i]]['jml_disc'])?$item_discs[$child_data['arr_child']['child_kd'][$i]]['jml_disc']:$disc_tipe_invoice;
					$type_individual_disc = isset($item_discs[$child_data['arr_child']['child_kd'][$i]]['type_individual_disc'])?$item_discs[$child_data['arr_child']['child_kd'][$i]]['type_individual_disc']:'decimal';
					$jml_individual_disc = isset($item_discs[$child_data['arr_child']['child_kd'][$i]]['jml_individual_disc'])?$item_discs[$child_data['arr_child']['child_kd'][$i]]['jml_individual_disc']:'0';
					$item_price_retail = $child_data['arr_child']['harga_barang'][$i];

					$convert_retail = empty($val_retail)?'1':$val_retail;
					$convert_detail = empty($val_detail)?'1':$val_detail;
					if ($type_retail == 'primary') :
						$item_price_retail = $child_data['arr_child']['harga_retail'][$i] / $convert_detail;
					elseif ($type_retail == 'secondary') :
						$item_price_retail = $child_data['arr_child']['harga_retail'][$i] * $convert_retail;
					endif;
					if ($currency_type == 'primary') :
						$item_price = $child_data['arr_child']['harga_barang'][$i] / $convert_detail;
					elseif ($currency_type == 'secondary') :
						$item_price = $child_data['arr_child']['harga_barang'][$i] * $convert_retail;
					endif;
					$disc_type = $child_data['arr_child']['disc_type'][$i];
					$disc_val = $child_data['arr_child']['item_disc'][$i];
					$disc = $item_price_retail - $item_price;
					$item_price = $item_price_retail - $disc;

					$jml_default_disc = count_disc('percent', $jml_disc, $item_price);
					$item_price = $item_price - $jml_default_disc;
					$val_individual_disc = count_disc($type_individual_disc, $jml_individual_disc, $item_price);
					$item_price = $item_price - $val_individual_disc;

					$item_disc = count_disc($disc_type, $disc_val, $item_price);
					$total_disc = ($item_price_retail - $item_price) + $item_disc;
					$total_harga = ($item_price_retail - $total_disc) * $child_data['arr_child']['stuffing_item_qty'][$i];
					
					$tot_harga[] = $total_harga;
					$tot_disc[] = $total_disc;
					$tot_price[] = $item_price;
					$tot_qty[] = $child_data['arr_child']['stuffing_item_qty'][$i];
					?>
					<tr>
						<td align="center"><?php echo $no; ?></td>
						<td><?php echo $child_data['arr_child']['item_code'][$i]; ?></td>
						<td><?php echo item_stat($child_data['arr_child']['item_status'][$i]); ?></td>
						<td><?php echo $child_data['arr_child']['item_desc'][$i]; ?></td>
						<td><?php echo $child_data['arr_child']['item_dimension'][$i]; ?></td>
						<td align="center"><?php echo $child_data['arr_child']['stuffing_item_qty'][$i]; ?></td>
						<td align="right"><?php echo format_currency($item_price_retail, $currency_icon); ?></td>
						<td align="right"><?php echo format_currency($total_disc, $currency_icon); ?></td>
						<td align="right"><?php echo format_currency($total_harga, $currency_icon); ?></td>
					</tr>
					<?php
				endif;
			endfor;
		endif;
		$tots_qty = array_sum($tot_qty);
		$tots_harga = array_sum($tot_harga);
		$jml_pot = count($tot_potongan);
		$jml_add = $jml_pot > 0?1:0;
		$jml_rowspan = 7 + $jml_pot + $jml_add + count($so_termin);
		$jml_rowspan_det = ($jml_pot > 0)?0:8 + $jml_pot; // Rowspan di footer nomor rekening dan field sebelah kanan, default = 8
		?>
		<tr>
			<td colspan="4"></td>
			<td>Total Price</td>
			<td align="center"><?php echo $tots_qty; ?></td>
			<td colspan="2"></td>
			<td align="right"><?php echo format_currency($tots_harga, $currency_icon); ?></td>
		</tr>
		<?php
		$no = 0;
		foreach ($tot_potongan as $tot) :
			$no++;
			if ($tot->type_kolom == 'nilai') :
				$jml_potongan = $tot->total_nilai;
			elseif ($tot->type_kolom == 'persen') :
				$jml_potongan = count_disc('percent', $tot->total_nilai, $tots_harga);
			
				if (empty($decimal) || $decimal == '0') :
					$jml_potongan = pembulatan_decimal($jml_potongan);
				elseif ($decimal == '1') :
					$jml_potongan = $jml_potongan;
				endif;
			endif;
			$tots_pot[] = $jml_potongan;
			?>
			<tr>
				<?php if ($no == 1) : ?>
					<td colspan="6" rowspan="<?php echo $jml_rowspan; ?>">
						<?php
						if (empty($term_payment)) :
							echo $laporan_footer;
						else :
							echo $term_payment.'<br />'.$footer;
						endif;
						?>
					</td>
				<?php endif; ?>
				<td colspan="2"><?php echo $tot->nm_kolom; ?></td>
				<td style="text-align: right;"><?php echo format_currency($jml_potongan, $currency_icon); ?></td>
			</tr>
			<?php
			if ($no == $jml_pot) :
				$tot_pot = array_sum($tots_pot);
				$tots_harga = $tots_harga - $tot_pot;
				?>
				<tr>
					<td colspan="2"><b>Total Price</b></td>
					<td style="text-align: right;"><?php echo format_currency($tots_harga, $currency_icon); ?></td>
				</tr>
				<?php
			endif;
		endforeach;
		?>
		<tr>
			<?php
			if ($jml_rowspan_det > 0) :
				?>
				<td colspan="6" rowspan="<?php echo $jml_rowspan_det; ?>">
					<?php
					if (empty($term_payment)) :
						echo $laporan_footer;
					else :
						echo $term_payment.'<br />'.$footer;
					endif;
					?>
				</td>
				<?php
			endif;

			if (!empty($jml_dp) && $is_dp) :
				?>
				<td colspan="2">DP - <?php echo $no_invoice_dp; ?></td>
				<td style="text-align: right;">
					<div id="idTextOngkir"><?php echo format_currency($jml_dp, $currency_icon); ?></div>
				</td>
				<?php
				$tots_harga = $tots_harga - $jml_dp;
			endif;
			?>
		</tr>
		<?php
		if (!empty($so_termin)) :
			foreach ($so_termin as $termin) :
				// $jml_termin = $termin->jml_termin + $termin->jml_termin_ppn;
				$jml_termin = $termin->jml_termin;
				$jml_termins[] = $jml_termin;
				?>
				<tr>
					<td colspan="2">DP - <?php echo $termin->no_invoice; ?></td>
					<td style="text-align: right;">
						<div id="idTextOngkir"><?php echo format_currency($jml_termin, $currency_icon); ?></div>
					</td>
				</tr>
				<?php
			endforeach;
			$tots_harga = $tots_harga - array_sum($jml_termins);
		endif;
		?>
		<?php
		if (!empty($jml_dp) && $is_dp) :
			?>
			<tr>
				<td colspan="2"><b>Total Setelah DP</b></td>
				<td style="text-align: right;">
					<div id="idTextOngkir"><?php echo format_currency($tots_harga, $currency_icon); ?></div>
				</td>
			</tr>
			<?php
		endif;
		?>
		<tr>
			<td colspan="2">Ongkir</td>
			<td style="text-align: right;">
				<div id="idTextOngkir"><?php echo format_currency($jml_ongkir, $currency_icon); ?></div>
			</td>
		</tr>
		<tr>
			<td colspan="2">Installasi</td>
			<td style="text-align: right;">
				<div id="idTextInstall"><?php echo format_currency($jml_install, $currency_icon); ?></div>
			</td>
		</tr>
		<?php
		$tots_harga = ($tots_harga + $jml_ongkir + $jml_install);
		$tot_ppn = count_disc('percent', $jml_ppn, $tots_harga);
			
		if (empty($decimal) || $decimal == '0') :
			$tot_ppn_lagi = pembulatan_decimal($tot_ppn);
		elseif ($decimal == '1') :
			$tot_ppn_lagi = $tot_ppn;
		endif;
		?>
		<tr>
			<td colspan="2"><b>Total Price</b></td>
			<td style="text-align: right;"><div id="idTextPpn"><?php echo format_currency($tots_harga, $currency_icon); ?></div></td>
		</tr>
		<tr>
			<td colspan="2"><?php echo $nm_kolom_ppn; ?></td>
			<td style="text-align: right;"><div id="idTextPpn"><?php echo format_currency($tot_ppn_lagi, $currency_icon); ?></div></td>
		</tr>
		<?php $grand_tot = $tots_harga + $tot_ppn_lagi; ?>
		<tr>
			<td colspan="2"><b>Grand Total</b></td>
			<td style="text-align: right;"><div id="idTextGrandTot"><?php echo format_currency($grand_tot, $currency_icon); ?></div></td>
		</tr>
	</tbody>
</table>

<?php
if ($ket_note_so == 'available') :
	?>
	<div class="row invoice-info print-foot" style="margin-top: 15px;">
		Catatan :
		<?php echo $note_so; ?>
	</div>
	<?php
endif;
?>
<div class="row invoice-info print-foot">
	<div class="col-xs-9"></div>
	<div class="col-xs-3" style="text-align: center;">
		<div style="margin-bottom: 100px;">SIDOARJO, <?php echo $tgl_do; ?><br>Authorized on behalf of PT. Caterlindo</div>
		<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
		<div><b>Operational Manager</b></div>
	</div>
</div>