<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$data = array($format_laporan, $item_detail);
extract($format_laporan);
$no_form = !empty($no_form)?$no_form:'';
?>
<style type="text/css">
	body {
		font-size: 12px;
	}
	img {
		width: 100% !important;
	}
	thead.report-header {
		display: table-header-group;
	}

	tfoot.report-footer {
		display:table-footer-group;
	}
	table.report-container {
		page-break-after: always;
	}
	/* @media print {
		body {
			transform: scale(80%);
		}
		.print-left {
			text-align: left;
		}
		.print-center {
			text-align: center;
		}
		.print-right {
			text-align: right;
		}
	} */
</style>
<div class="row">
	<div class="col-xs-12">
		<a href="javascript:void(0);" class="btn btn-danger no-print" onclick="window.close();" style="margin-left: 25px;">
			<i class="fa fa-ban"></i> Close
		</a>
		<a href="javascript:void(0);" class="btn btn-primary no-print" onclick="window.print();" style="margin-left: 25px;">
			<i class="fa fa-print"></i> Cetak Data
		</a>
		<a href="<?php echo base_url().'report/report_sales/invoice_report/'.$tipe_invoice.'/'.$kd_mdo; ?>" class="btn btn-success no-print" style="margin-left: 25px;">
			<i class="fa fa-file-excel-o"></i> Export Excel
		</a>
	</div>
</div>
<br>
<div class="row invoice print">
	<!-- <table class="">
		<thead class="report-header">
			<tr>
				<td>
					<div class="header-info"></div>
				</td>
			</tr>
		</thead>
		<tfoot class="report-footer">
			<tr>
				<td>
					<div class="footer-info">
						<table class="div-footer" style="width: 100%;">
							<tbody>
								<tr>
									<td colspan="3"><hr></td>
								</tr>
							</tbody>
						</table>
					</div>
				</td>
			</tr>
		</tfoot>
		<tbody class="report-content">
			<tr>
				<td> -->
					<!-- title row -->
					<div class="row">
						<div class="col-xs-12">
							<h2 class="page-header" style="margin-top: -20px; margin-bottom: 5px;">
								<div style="text-align: center;margin-right: 40px;">
									<?php echo $laporan_title; ?>
								</div>
								<div style="text-align: center; margin-top: -15px;">
									<?php echo $master_head['no_invoice']; ?>
								</div>
							</h2>
						</div><!-- /.col -->
					</div>
					<?php $this->load->view('page/'.$class_link.'/inv_do/ekspor/header', $master_head); ?>
					<hr>
					<?php $this->load->view('page/'.$class_link.'/inv_do/ekspor/items', $item_detail); ?>
				<!-- </td>
			</tr>
		</tbody>
	</table> -->
	<hr>
	<table class="div-footer" style="width: 50%; " border="0">
		<tbody>
			<tr>
				<td>
					<img src="<?php echo base_url(); ?>assets/admin_assets/dist/img/LogoISO2021.jpg" alt="" >
				</td>
			</tr>
		</tbody>
	</table>
</div>