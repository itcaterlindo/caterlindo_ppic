<?php
defined('BASEPATH') or exit('No direct script access allowed!');
if (!empty($format_laporan)) :
	extract($format_laporan);
endif;

if (!empty($default_disc)) :
	foreach ($default_disc as $row) :
		$item_discs[$row->so_item_kd] = array('jml_disc' => $row->jml_disc, 'type_individual_disc' => $row->type_individual_disc, 'jml_individual_disc' => $row->jml_individual_disc);	
	endforeach;
endif;

$def_disc_set = $this->tb_default_disc->select_where(array('jenis_customer_kd' => $kd_jenis_customer, 'nm_pihak' => $tipe_invoice));
?>
<table style="width: 100%;" border="1">
	<thead>
		<tr>
			<th style="width: 1%;text-align: center;">No.</th>
			<th style="width: 10%;text-align: center;">Prod Code</th>
			<th style="width: 20%;text-align: center;">Description</th>
			<th style="width: 20%;text-align: center;">Dimension</th>
			<th style="width: 1%;text-align: center;">Qty</th>
			<th style="width: 13%;text-align: center;">Price/Unit</th>
			<th style="width: 15%;text-align: center;">Subtotal</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach($parent_items as $parent) :
			$no_child = 0;
			$rows = '';
			foreach ($child_items as $child) :
				if ($parent->parent_kd == $child->parent_kd) :
					$no_child++;
				endif;
				$rowspan = 1 + $no_child;
				$rows = 'rowspan=\''.$rowspan.'\'';
			endforeach;
			$no++;

			if ($tipe_invoice == 'customer') :
				$disc_tipe_invoice = $parent->disc_customer != 'kosong'?$parent->disc_customer:$def_disc_set->jml_disc;
			elseif ($tipe_invoice == 'distributor') :
				$disc_tipe_invoice = $parent->disc_distributor != 'kosong'?$parent->disc_distributor:$def_disc_set->jml_disc;
			endif;
			$jml_disc = isset($item_discs[$parent->parent_kd]['jml_disc'])?$item_discs[$parent->parent_kd]['jml_disc']:$disc_tipe_invoice;
			$type_individual_disc = isset($item_discs[$parent->parent_kd]['type_individual_disc'])?$item_discs[$parent->parent_kd]['type_individual_disc']:'decimal';
			$jml_individual_disc = isset($item_discs[$parent->parent_kd]['jml_individual_disc'])?$item_discs[$parent->parent_kd]['jml_individual_disc']:'0';
			$item_price_retail = $parent->harga_barang;

			$jml_default_disc = count_disc('percent', $jml_disc, $item_price_retail);
			$item_price = $item_price_retail - $jml_default_disc;
			$val_individual_disc = count_disc($type_individual_disc, $jml_individual_disc, $item_price);
			$item_price = $item_price - $val_individual_disc;
					
			$disc_type = $parent->disc_type;
			$disc_val = $parent->item_disc;
			$item_disc = count_disc($disc_type, $disc_val, $item_price);
			$item_price = $item_price - $item_disc;
			$total_disc = $item_price_retail - $item_price;

			if ($tipe_invoice == 'customer') :
				$item_disc_customer = empty($parent->disc_customer)?'0':$parent->disc_customer;
				$item_price_display = $item_price_retail;
				$total_harga = $item_price_display * $parent->stuffing_item_qty;
				$tot_harga[] = round($total_harga, 2);
			else :
				$item_price = round($item_price, 2);
				$total_harga = $item_price * $parent->stuffing_item_qty;
				$tot_harga[] = round($total_harga, 2);
			endif;
			$total_disc = $total_disc * $parent->stuffing_item_qty;
			$tot_disc[] = $total_disc;
			$tot_qty[] = $parent->stuffing_item_qty;
			?>
			<tr>
				<td <?php echo $rows; ?> align="center"><?php echo $no; ?></td>
				<td><?php echo $parent->item_code; ?></td>
				<td><?php echo $parent->item_desc; ?></td>
				<td><?php echo $parent->item_dimension; ?></td>
				<td align="center"><?php echo $parent->stuffing_item_qty; ?></td>
				<?php
				if ($tipe_invoice == 'customer') :
					?>
					<!-- <td align="right"><?php echo format_currency($item_price_display, $currency_icon).' / '.$total_disc; ?></td>
					<td align="right"><?php echo format_currency($total_harga, $currency_icon).' / '.$total_disc * $parent->stuffing_item_qty; ?></td> -->
					<td align="right"><?php echo format_currency($item_price_display, $currency_icon); ?></td>
					<td align="right"><?php echo format_currency($total_harga, $currency_icon); ?></td>
					<?php
				else :
					?>
					<td align="right"><?php echo format_currency($item_price, $currency_icon); ?></td>
					<td align="right"><?php echo format_currency($total_harga, $currency_icon); ?></td>
					<?php
				endif;
				?>
			</tr>
			<?php
			foreach ($child_items as $child) :
				if (!empty($child->parent_kd) && $parent->parent_kd == $child->parent_kd) :

					if ($tipe_invoice == 'customer') :
						$disc_tipe_invoice = $child->disc_customer != 'kosong'?$child->disc_customer:$def_disc_set->jml_disc;
					elseif ($tipe_invoice == 'distributor') :
						$disc_tipe_invoice = $child->disc_distributor != 'kosong'?$child->disc_distributor:$def_disc_set->jml_disc;
					endif;
					$jml_disc = isset($item_discs[$child->child_kd]['jml_disc'])?$item_discs[$child->child_kd]['jml_disc']:$disc_tipe_invoice;
					$type_individual_disc = isset($item_discs[$child->child_kd]['type_individual_disc'])?$item_discs[$child->child_kd]['type_individual_disc']:'decimal';
					$jml_individual_disc = isset($item_discs[$child->child_kd]['jml_individual_disc'])?$item_discs[$child->child_kd]['jml_individual_disc']:'0';
					$item_price_retail = $child->harga_barang;

					$jml_default_disc = count_disc('percent', $jml_disc, $item_price_retail);
					$item_price = $item_price_retail - $jml_default_disc;
					$val_individual_disc = count_disc($type_individual_disc, $jml_individual_disc, $item_price);
					$item_price = $item_price - $val_individual_disc;
							
					$disc_type = $child->disc_type;
					$disc_val = $child->item_disc;
					$item_disc = count_disc($disc_type, $disc_val, $item_price);
					$item_price = $item_price - $item_disc;
					$total_disc = $item_price_retail - $item_price;
			
					if ($tipe_invoice == 'customer') :
						$item_disc_customer = empty($child->disc_customer)?'0':$child->disc_customer;
						if ($jml_disc > $def_disc_set->jml_disc) :
							$item_price_display = $item_price;
							$total_disc = 0;
						else :
							$item_price_display = $item_price_retail;
						endif;
						$item_price_display = $item_price_display;
						$total_harga = $item_price_display * $child->stuffing_item_qty;
						$tot_harga[] = round($total_harga, 2);
					else :
						$item_price = round($item_price, 2);
						$total_harga = $item_price * $child->stuffing_item_qty;
						$tot_harga[] = round($total_harga, 2);
					endif;
					$tot_disc[] = $total_disc * $child->stuffing_item_qty;
					$tot_qty[] = $child->stuffing_item_qty;
					?>
					<tr>
						<td <?php echo $rows; ?> align="center"><?php echo $no; ?></td>
						<td><?php echo $child->item_code; ?></td>
						<td><?php echo $child->item_desc; ?></td>
						<td><?php echo $child->item_dimension; ?></td>
						<td align="center"><?php echo $child->stuffing_item_qty; ?></td>
						<?php
						if ($tipe_invoice == 'customer') :
							?>
							<td align="right"><?php echo format_currency($item_price_display, $currency_icon); ?></td>
							<td align="right"><?php echo format_currency($total_harga, $currency_icon); ?></td>
							<?php
						else :
							?>
							<td align="right"><?php echo format_currency($item_price, $currency_icon); ?></td>
							<td align="right"><?php echo format_currency($total_harga, $currency_icon); ?></td>
							<?php
						endif;
						?>
					</tr>
					<?php
				else :
					$child_data['arr_child']['parent_kd'][] = $child->parent_kd;
					$child_data['arr_child']['child_kd'][] = $child->child_kd;
					$child_data['arr_child']['harga_retail'][] = $child->harga_retail;
					$child_data['arr_child']['harga_barang'][] = $child->harga_barang;
					$child_data['arr_child']['disc_type'][] = $child->disc_type;
					$child_data['arr_child']['item_disc'][] = $child->item_disc;
					$child_data['arr_child']['item_status'][] = $child->item_status;
					$child_data['arr_child']['stuffing_item_qty'][] = $child->stuffing_item_qty;
					$child_data['arr_child']['item_code'][] = $child->item_code;
					$child_data['arr_child']['item_desc'][] = $child->item_desc;
					$child_data['arr_child']['item_dimension'][] = $child->item_dimension;
					$child_data['arr_child']['disc_customer'][] = $child->disc_customer;
					$child_data['arr_child']['disc_distributor'][] = $child->disc_distributor;
				endif;
			endforeach;
		endforeach;
		if (isset($child_data)) :
			for ($i = 0; $i < count($child_data); $i++) :
				if (empty($child_data['arr_child']['parent_kd'][$i])) :
					$no++;

					if ($tipe_invoice == 'customer') :
						$disc_tipe_invoice = $child_data['arr_child']['disc_customer'][$i] != 'kosong'?$child_data['arr_child']['disc_customer'][$i]:$def_disc_set->jml_disc;
					elseif ($tipe_invoice == 'distributor') :
						$disc_tipe_invoice = $child_data['arr_child']['disc_distributor'][$i] != 'kosong'?$child_data['arr_child']['disc_distributor'][$i]:$def_disc_set->jml_disc;
					endif;
					$jml_disc = isset($item_discs[$child_data['arr_child']['child_kd'][$i]]['jml_disc'])?$item_discs[$child_data['arr_child']['child_kd'][$i]]['jml_disc']:$disc_tipe_invoice;
					$type_individual_disc = isset($item_discs[$child_data['arr_child']['child_kd'][$i]]['type_individual_disc'])?$item_discs[$child_data['arr_child']['child_kd'][$i]]['type_individual_disc']:'decimal';
					$jml_individual_disc = isset($item_discs[$child_data['arr_child']['child_kd'][$i]]['jml_individual_disc'])?$item_discs[$child_data['arr_child']['child_kd'][$i]]['jml_individual_disc']:'0';
					$item_price_retail = $child_data['arr_child']['harga_barang'][$i];

					$jml_default_disc = count_disc('percent', $jml_disc, $item_price_retail);
					$item_price = $item_price_retail - $jml_default_disc;
					$val_individual_disc = count_disc($type_individual_disc, $jml_individual_disc, $item_price);
					$item_price = $item_price - $val_individual_disc;
							
					$disc_type = $child_data['arr_child']['disc_type'][$i];
					$disc_val = $child_data['arr_child']['item_disc'][$i];
					$item_disc = count_disc($disc_type, $disc_val, $item_price);
					$item_price = round($item_price - $item_disc, 2);
					$total_disc = $item_price_retail - $item_price;
			
					if ($tipe_invoice == 'customer') :
						$item_disc_customer = empty($child_data['arr_child']['disc_customer'][$i])?'0':$child_data['arr_child']['disc_customer'][$i];
						if ($jml_disc > $def_disc_set->jml_disc) :
							$item_price_display = $item_price;
							$total_disc = 0;
						else :
							$item_price_display = $item_price_retail;
						endif;
						$item_price_display = $item_price_display;
						$total_harga = $item_price_display * $parent->stuffing_item_qty;
						$tot_harga[] = round($total_harga, 2);
					else :
						$item_price = round($item_price);
						$total_harga = $item_price * $parent->stuffing_item_qty;
						$tot_harga[] = round($total_harga, 2);
					endif;
					$tot_disc[] = $total_disc * $child_data['arr_child']['stuffing_item_qty'][$i];
					$tot_qty[] = $child_data['arr_child']['stuffing_item_qty'][$i];
					?>
					<tr>
						<td <?php echo $rows; ?> align="center"><?php echo $no; ?></td>
						<td><?php echo $child_data['arr_child']['item_code'][$i]; ?></td>
						<td><?php echo $child_data['arr_child']['item_desc'][$i]; ?></td>
						<td><?php echo $child_data['arr_child']['item_dimension'][$i]; ?></td>
						<td align="center"><?php echo $child_data['arr_child']['stuffing_item_qty'][$i]; ?></td>
						<?php
						if ($tipe_invoice == 'customer') :
							?>
							<td align="right"><?php echo format_currency($item_price_display, $currency_icon); ?></td>
							<td align="right"><?php echo format_currency($total_harga, $currency_icon); ?></td>
							<?php
						else :
							?>
							<td align="right"><?php echo format_currency($item_price, $currency_icon); ?></td>
							<td align="right"><?php echo format_currency($total_harga, $currency_icon); ?></td>
							<?php
						endif;
						?>
					</tr>
					<?php
				endif;
			endfor;
		endif;
		$tot_pots = 0;
		$tots_qty = array_sum($tot_qty);
		$tots_disc = array_sum($tot_disc);
		$tots_harga = array_sum($tot_harga);
		$jml_pot = count($tot_potongan);
		$jml_add = $jml_pot > 0?1:0;
		$jml_rowspan = 7 + $jml_pot + $jml_add;
		$jml_rowspan_det = ($jml_pot > 0)?0:7 + $jml_pot;
		$no = 0;
		foreach ($tot_potongan as $tot) :
			if ($tot->type_kolom == 'nilai') :
				$jml_potongan = $tot->total_nilai;
			elseif ($tot->type_kolom == 'persen') :
				$jml_potongan = count_disc('percent', $tot->total_nilai, $tots_harga);
			
				if (empty($decimal) || $decimal == '0') :
					$jml_potongan = pembulatan_decimal($jml_potongan);
				elseif ($decimal == '1') :
					$jml_potongan = $jml_potongan;
				endif;
			endif;
			$tots_pot[] = $jml_potongan;
		endforeach;
		if (isset($tots_pot)) :
			$tot_pots = array_sum($tots_pot);
		endif;
		$total_pot = $tots_disc + $tot_pots;
		if ($tipe_invoice == 'customer') :
			$grand_tot = $tots_harga - $total_pot;
		else :
			$grand_tot = $tots_harga;
		endif;

		if ($tipe_invoice == 'customer') :
			?>
			<tr>
				<td colspan="3"></td>
				<th>Sub Total</th>
				<td align="center"></td>
				<td></td>
				<th style="text-align: right;"><?php echo format_currency($tots_harga, $currency_icon); ?></th>
			</tr>
			<tr>
				<td colspan="3"></td>
				<th>Total Discount</th>
				<td align="center"></td>
				<td></td>
				<th style="text-align: right;"><?php echo format_currency($total_pot, $currency_icon); ?></th>
			</tr>
			<?php
		endif;
		?>
		<tr>
			<td colspan="3"></td>
			<th>Total Cost (Ex Works)</th>
			<td align="center"><?php echo $tots_qty; ?></td>
			<td></td>
			<th style="text-align: right;"><?php echo format_currency($grand_tot, $currency_icon); ?></th>
		</tr>
	</tbody>
</table>


<?php
if ($ket_note_so == 'available') :
	?>
	<div class="row invoice-info" style="margin-top: 15px;">
		Catatan :
		<?php echo $note_so; ?>
		<ul>
			<?php
			foreach ($parent_items as $parent) :
				if (!empty($parent->item_note)) :
					echo '<li>'.$parent->item_code.' - '.$parent->item_note.'</li>';
				endif;
			endforeach;
			foreach ($child_items as $child) :
				if (!empty($child->item_note)) :
					echo '<li>'.$child->item_code.' - '.$child->item_note.'</li>';
				endif;
			endforeach;
			?>
		</ul>
	</div>
	<?php
endif;

if ($tipe_invoice == 'customer') :
	?>
	<div class="row" style="margin-top: 15px;">
		<div class="col-sm-12">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>For Payment made within trading terms - Total net invoice value : <?php echo format_currency($grand_tot, $currency_icon); ?></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	<div class="row invoice-info" style="margin-top: 15px;">
		<div class="col-xs-2">CONTAINER/SEAL :</div>
		<div class="col-xs-3">
			<?php echo $container_number.'/'.$seal_number; ?>
		</div>
	</div>
	<div class="row invoice-info">
		<div class="col-xs-2">MADE IN INDONESIA</div>
	</div>
	<div class="row invoice-info">
		<div class="col-xs-3">
			WE HEREBY CERTIFY THAT CARGO IS INDONESIA ORIGIN
		</div>
	</div>
	<div class="row invoice-info">
		<div class="col-xs-2">Bank Details :</div>
	</div>
	<div class="row invoice-info">
		<div class="col-xs-2">Account Name :</div>
		<div class="col-xs-2">CI Group Pty Ltd US Account</div>
	</div>
	<div class="row invoice-info">
		<div class="col-xs-2">BSB :</div>
		<div class="col-xs-2">34702</div>
	</div>
	<div class="row invoice-info">
		<div class="col-xs-2">Account No :</div>
		<div class="col-xs-2">695930</div>
	</div>
	<div class="row invoice-info">
		<div class="col-xs-2">Swift Code :</div>
		<div class="col-xs-2">WPACAU25XXX</div>
	</div>
	<div class="row invoice-info">
		<div class="col-xs-2">Payment Terms :</div>
		<div class="col-xs-2"><?php echo $term_payment_waktu; ?> Days After BL</div>
	</div>
	<?php
else :
	?>
	<div class="row invoice-info" style="margin-top: 15px;">
		<div class="col-sm-1 invoice-col">
			Container Number
		</div>
		<div class="col-sm-3 invoice-col">
			<?php echo ' : '.$container_number; ?>
		</div>
	</div>
	<div class="row invoice-info">
		<div class="col-sm-1 invoice-col">
			Seal Number
		</div>
		<div class="col-sm-3 invoice-col">
			<?php echo ' : '.$seal_number; ?>
		</div>
	</div>
	<div class="row invoice-info">
		<div class="col-sm-1">Term Payment</div>
		<div class="col-sm-3"><?php echo ' : '.$term_payment_waktu; ?> Days After BL</div>
	</div>
	<div class="row invoice-info" style="margin-top: 15px;">
		<div class="col-xs-9"></div>
		<div class="col-xs-3" style="text-align: center;">
			<div style="margin-bottom: 100px;">Prepared by,</div>
			<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
			<div><b>Exim</b></div>
		</div>
	</div>
	<?php
endif;
?>