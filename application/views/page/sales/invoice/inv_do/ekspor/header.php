<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$color = color_status($status_do);
$word = process_status($status_do);
?>
<!-- START DO HEADER DETAIL -->
<table border="0">
	<tr>
		<th style="width: 150px;vertical-align: top; height: 15px;">Invoice To Buyer</th>
		<td style="vertical-align: top; width: 15px;">:</td>
		<td style="width: 250px;" rowspan="2">
			<strong><?php echo $notify_name; ?></strong><br>
			<?php echo $notify_address; ?>
		</td>
	</tr>
	<tr>
		<th style="width: 150px;vertical-align: top;">(Notify Party)</th>
	</tr>
	<?php
	if ($tipe_do == 'Ekspor') :
		?>
		<tr>
			<th style="width: 150px; vertical-align: top;">Consignee</th>
			<td style="vertical-align: top;">:</td>
			<td>
				<strong><?php echo $code_customer." ".$nm_cust; ?></strong><br>
				<?php echo $nm_contact_cust.after_before_char($nm_contact_cust, $nm_contact_cust, '<br>', ''); ?>
				<?php echo $alamat_cust; ?>
			</td>
		</tr>
		<?php
	endif;
	?>
	<tr>
		<th style="width: 150px;vertical-align: top;">Delivery Address</th>
		<td style="vertical-align: top;">:</td>
		<td>
			<strong><?php echo $nm_penerima; ?></strong><br>
			<?php echo $nm_contact_penerima.after_before_char($nm_contact_penerima, $nm_contact_penerima, '<br>', ''); ?>
			<?php echo $alamat_penerima; ?>
		</td>
		<th style="width: 75px;vertical-align: top;">Date</th>
		<td style="vertical-align: top; width: 15px;">:</td>
		<td style="vertical-align: top;"><?php echo format_date($tgl_do, 'd M Y'); ?></td>
	</tr>
	<tr>
		<?php
		if ($tipe_do == 'Ekspor') :
			?>
			<th style="width: 150px;vertical-align: top;">Freight By</th>
			<td style="vertical-align: top;">:</td>
			<td>
				<strong><?php echo $nm_jasakirim; ?></strong><br>
				<?php echo $alamat_jasakirim; ?>
			</td>
			<?php
		endif;
		if (!empty($no_po)) :
			?>
			<th style="width: 75px;vertical-align: top;">Order No</th>
			<td style="vertical-align: top;">:</td>
			<td style="vertical-align: top;">
				<?php
				foreach ($list_nopo as $list_po) :
					echo '<li>'.$list_po.'</li>';
				endforeach;
				?>
			</td>
			<?php
		endif;
		?>
	</tr>
	<?php
	if ($tipe_do == 'Ekspor') :
		?>
		<tr>
			<th style="width: 125px;vertical-align: top;">Description of goods</th>
			<td style="vertical-align: top;">:</td>
			<td><?php echo !empty($goods_desc)?$goods_desc:'Stainless Steel Goods'; ?></td>
		</tr>
		<!-- <tr>
			<th style="width: 125px;vertical-align: top;">Number of boxs</th>
			<td style="vertical-align: top;">:</td>
			<td><?php echo $jml_koli; ?></td>
		</tr> -->
		<?php
	endif;
	?>
</table>
<!-- END DO HEADER DETAIL -->