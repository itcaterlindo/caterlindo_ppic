<?php
defined('BASEPATH') or exit('No direct script access allowed!');
if (!empty($format_laporan)) :
	extract($format_laporan);
endif;
$angka = $jml_dp;
?>
<table style="column-width: 10px;width: 100%;" border="1">
	<thead>
		<tr>
			<th style="width: 1%;text-align: center;">No.</th>
			<th style="width: 10%;text-align: center;">Prod Code</th>
			<th style="width: 1%;text-align: center;">Status</th>
			<th style="width: 18%;text-align: center;">Description</th>
			<th style="width: 18%;text-align: center;">Dimension</th>
			<th style="width: 1%;text-align: center;">Qty</th>
			<th style="width: 13%;text-align: center;">Price/Unit</th>
			<th style="width: 12%;text-align: center;">Disc</th>
			<th style="width: 15%;text-align: center;">Subtotal</th>
		</tr>
	</thead>
	<tbody>
		<?php
		if ($invoice_stat == 'dp') :
			?>
			<tr>
				<td></td>
				<td colspan="5"><b>Down Payment atas Pembelian Item yang Tersebut di Bawah ini:</b></td>
				<td style="text-align: right;"><?php echo format_currency($angka, $currency_icon); ?></td>
				<td></td>
				<td style="text-align: right;"><?php echo format_currency($angka, $currency_icon); ?></td>
			</tr>
			<?php
		endif;
		$no = 0;
		$tot_harga = array();
		$tot_qty = array();
		foreach ($items_data as $item_data) :
			$no_child = 0;
			$rows = '';
			foreach ($details_data as $detail_data) :
				if ($detail_data['ditem_so_kd'] == $item_data->kd_ditem_so) :
					$no_child++;
				endif;
				$rowspan = 1 + $no_child;
				$rows = 'rowspan=\''.$rowspan.'\'';
			endforeach;
			$no++;
			
			if ($tipe_customer == 'Lokal') :
				$convert_retail = empty($val_retail)?'1':$val_retail;
				$convert_detail = empty($val_detail)?'1':$val_detail;
				if ($type_retail == 'primary') :
					$item_price_retail = $item_data->harga_retail / $convert_detail;
				elseif ($type_retail == 'secondary') :
					$item_price_retail = $item_data->harga_retail * $convert_retail;
				endif;
				if ($currency_type == 'primary') :
					$item_price = $item_data->harga_barang / $convert_detail;
				elseif ($currency_type == 'secondary') :
					$item_price = $item_data->harga_barang * $convert_retail;
				endif;
				$disc_type = $item_data->disc_type;
				$disc_val = $item_data->item_disc;
				$disc = $item_price_retail - $item_price;
				$item_disc = count_disc($disc_type, $disc_val, $item_price);
				$tot_def_disc = count_disc('percent', $default_disc, $item_price);
				$total_disc = $disc + $item_disc + $tot_def_disc;
				$total_harga = ($item_price_retail - $total_disc) * $item_data->item_qty;
			elseif ($tipe_customer == 'Ekspor') :
				$item_price_retail = $item_data->harga_barang;
				$disc_type = $item_data->disc_type;
				$disc_val = $item_data->item_disc;
				$total_disc = count_disc($disc_type, $disc_val, $item_price_retail);
				$total_harga = ($item_price_retail - $total_disc) * $item_data->item_qty;
			endif;
			
			$tot_harga[] = $total_harga;
			$tot_disc[] = $total_disc;
			$tot_price[] = $item_price_retail;
			$tot_qty[] = $item_data->item_qty;
			?>
			<tr>
				<td <?php echo $rows; ?> style="vertical-align: middle;"><?php echo $no; ?></td>
				<td><?php echo $item_data->item_code; ?></td>
				<td><?php echo item_stat($item_data->item_status); ?></td>
				<td><?php echo $item_data->item_desc; ?></td>
				<td><?php echo $item_data->item_dimension; ?></td>
				<td style="text-align: center;"><?php echo $item_data->item_qty; ?></td>
				<?php
				if ($invoice_stat == 'final') :
					?>
					<td style="text-align: right;"><?php echo format_currency($item_price_retail, $currency_icon); ?></td>
					<td style="text-align: right;"><?php echo format_currency($total_disc, $currency_icon); ?></td>
					<td style="text-align: right;"><?php echo format_currency($total_harga, $currency_icon); ?></td>
					<?php
				elseif ($invoice_stat == 'dp') :
					?>
					<td style="text-align: right;"></td>
					<td style="text-align: right;"></td>
					<td style="text-align: right;"></td>
					<?php
				endif;
				?>
			</tr>
			<?php
			foreach ($details_data as $detail_data) :
				if ($detail_data['ditem_so_kd'] == $item_data->kd_ditem_so) :
					if ($tipe_customer == 'Lokal') :
						$convert_retail = empty($val_retail)?'1':$val_retail;
						$convert_detail = empty($val_detail)?'1':$val_detail;
						if ($type_retail == 'primary') :
							$item_price_retail = $detail_data['harga_retail'] * $convert_detail;
						elseif ($type_retail == 'secondary') :
							$item_price_retail = $detail_data['harga_retail'] / $convert_retail;
						endif;
						if ($type_retail == 'primary') :
							$item_price = $detail_data['harga_barang'] / $convert_detail;
						elseif ($type_retail == 'secondary') :
							$item_price = $detail_data['harga_barang'] * $convert_retail;
						endif;
						$disc_type = $detail_data['disc_type'];
						$disc_val = $detail_data['item_disc'];
						$disc = $item_price_retail - $item_price;
						$item_disc = count_disc($disc_type, $disc_val, $item_price);
						$tot_def_disc = count_disc('percent', $default_disc, $item_price);
						$total_disc = $disc + $item_disc + $tot_def_disc;
						$total_harga = ($item_price_retail - $total_disc) * $detail_data['item_qty'];
					elseif ($tipe_customer == 'Ekspor') :
						$item_price_retail = $detail_data['harga_barang'];
						$disc_type = $detail_data['disc_type'];
						$disc_val = $detail_data['item_disc'];
						$total_disc = count_disc($disc_type, $disc_val, $item_price_retail);
						$total_harga = ($item_price_retail - $total_disc) * $detail_data['item_qty'];
					endif;
					$tot_harga[] = $total_harga;
					$tot_disc[] = $total_disc;
					$tot_price[] = $item_price_retail;
					$tot_qty[] = $detail_data['item_qty'];
					?>
					<tr>
						<td><?php echo $detail_data['item_code']; ?></td>
						<td><?php echo item_stat($detail_data['item_status']); ?></td>
						<td><?php echo $detail_data['item_desc']; ?></td>
						<td><?php echo $detail_data['item_dimension']; ?></td>
						<td style="text-align: center;"><?php echo $detail_data['item_qty']; ?></td>
						<?php
						if ($invoice_stat == 'final') :
							?>
							<td style="text-align: right;"><?php echo format_currency($item_price_retail, $currency_icon); ?></td>
							<td style="text-align: right;"><?php echo format_currency($total_disc, $currency_icon); ?></td>
							<td style="text-align: right;"><?php echo format_currency($total_harga, $currency_icon); ?></td>
							<?php
						elseif ($invoice_stat == 'dp') :
							?>
							<td style="text-align: right;"></td>
							<td style="text-align: right;"></td>
							<td style="text-align: right;"></td>
							<?php
						endif;
						?>
					</tr>
					<?php
				endif;
			endforeach;
		endforeach;		
		$tots_pot = array();
		$tots_harga = array_sum($tot_harga);
		$tots_disc = array_sum($tot_disc);
		$tots_price = array_sum($tot_price);
		$tots_qty = array_sum($tot_qty);
		$jml_pot = count($tot_potongan);
		$jml_add = $jml_pot > 0?1:0;
		$jml_rowspan = 7 + $jml_pot + $jml_add;
		$jml_rowspan_det = ($jml_pot > 0)?0:7 + $jml_pot;
		?>
		<?php
		if ($invoice_stat == 'final') :
			?>
			<tr>
				<td colspan="5"></td>
				<td><b>Total Price</b></td>
				<td style="text-align: right;"></td>
				<td style="text-align: right;"></td>
				<td style="text-align: right;"><?php echo format_currency($tots_harga, $currency_icon); ?></td>
			</tr>
			<?php
			$no = 0;
			foreach ($tot_potongan as $tot) :
				$no++;
				if ($tot->type_kolom == 'nilai') :
					$jml_potongan = $tot->total_nilai;
				elseif ($tot->type_kolom == 'persen') :
					$jml_potongan = count_disc('percent', $tot->total_nilai, $tots_harga);
				
					if (empty($decimal) || $decimal == '0') :
						$jml_potongan = pembulatan_decimal($jml_potongan);
					elseif ($decimal == '1') :
						$jml_potongan = $jml_potongan;
					endif;
				endif;
				$tots_pot[] = $jml_potongan;
				?>
				<tr>
					<?php if ($no == 1) : ?>
						<td colspan="6" rowspan="<?php echo $jml_rowspan; ?>">
							<?php
							if (empty($term_payment)) :
								echo $laporan_footer;
							else :
								echo $term_payment.'<br />'.$footer;
							endif;
							?>
						</td>
					<?php endif; ?>
					<td colspan="2"><?php echo $tot->nm_kolom; ?></td>
					<td style="text-align: right;"><?php echo format_currency($jml_potongan, $currency_icon); ?></td>
				</tr>
				<?php
				if ($no == $jml_pot) :
					$tot_pot = array_sum($tots_pot);
					$tots_harga = $tots_harga - $tot_pot;
					?>
					<tr>
						<td colspan="2"><b>Total Price</b></td>
						<td style="text-align: right;"><?php echo format_currency($tots_harga, $currency_icon); ?></td>
					</tr>
					<?php
				endif;
			endforeach;
			$tots_harga = $tots_harga - $jml_dp;
			?>
			<tr>
				<?php
				if ($jml_rowspan_det > 0) :
					?>
					<td colspan="6" rowspan="<?php echo $jml_rowspan_det; ?>">
						<?php
						if (empty($term_payment)) :
							echo $laporan_footer;
						else :
							echo $term_payment.'<br />'.$footer;
						endif;
						?>
					</td>
					<?php
				endif;
				?>
				<td colspan="2">DP</td>
				<td style="text-align: right;">
					<div id="idTextOngkir"><?php echo format_currency($jml_dp, $currency_icon); ?></div>
				</td>
			</tr>
			<tr>
				<td colspan="2"><b>Total Setelah DP</b></td>
				<td style="text-align: right;">
					<div id="idTextOngkir"><?php echo format_currency($tots_harga, $currency_icon); ?></div>
				</td>
			</tr>
			<tr>
				<td colspan="2">Ongkir</td>
				<td style="text-align: right;">
					<div id="idTextOngkir"><?php echo format_currency($jml_ongkir, $currency_icon); ?></div>
				</td>
			</tr>
			<tr>
				<td colspan="2">Installasi</td>
				<td style="text-align: right;">
					<div id="idTextInstall"><?php echo format_currency($jml_install, $currency_icon); ?></div>
				</td>
			</tr>
			<?php
			$tots_harga = ($tots_harga + $jml_ongkir + $jml_install);
			$tot_ppn = count_disc('percent', $jml_ppn, $tots_harga);
			
			if (empty($decimal) || $decimal == '0') :
				$tot_ppn_lagi = pembulatan_decimal($tot_ppn);
			elseif ($decimal == '1') :
				$tot_ppn_lagi = $tot_ppn;
			endif;
			?>
			<tr>
				<td colspan="2"><b>Total Price</b></td>
				<td style="text-align: right;"><div id="idTextPpn"><?php echo format_currency($tots_harga, $currency_icon); ?></div></td>
			</tr>
			<tr>
				<td colspan="2"><?php echo $nm_kolom_ppn; ?></td>
				<td style="text-align: right;"><div id="idTextPpn"><?php echo format_currency($tot_ppn_lagi, $currency_icon); ?></div></td>
			</tr>
			<?php $grand_tot = $tots_harga + $tot_ppn_lagi; ?>
			<tr>
				<td colspan="2"><b>Grand Total</b></td>
				<td style="text-align: right;"><div id="idTextGrandTot"><?php echo format_currency($grand_tot, $currency_icon); ?></div></td>
			</tr>
			<?php
		elseif ($invoice_stat == 'dp') :
		 	$grand_tot = $angka + $jml_ppn_dp;
			?>
			<tr>
				<td colspan="4"></td>
				<td style="text-align: center;"><b>Total Qty</b></td>
				<td style="text-align: center;"><?php echo $tots_qty; ?></td>
				<td style="text-align: center;" colspan="2"><b>Total</b></td>
				<td style="text-align: right;"><?php echo format_currency($angka, $currency_icon); ?></td>
			</tr>
			<tr>
				<td colspan="6" rowspan="2">
					<?php
					if ($page_type == 'print') :
						if (empty($term_payment)) :
							echo $laporan_footer;
						else :
							echo $term_payment.'<br />'.$footer;
						endif;
					endif;
					?>
				</td>
				<td colspan="2" style="text-align: center;"><b><?php echo $nm_kolom_ppn; ?></b></td>
				<td style="text-align: right;"><div id="idTextGrandTot"><?php echo format_currency($jml_ppn_dp, $currency_icon); ?></div></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center;"><b>Grand Total</b></td>
				<td style="text-align: right;"><div id="idTextGrandTot"><?php echo format_currency($grand_tot, $currency_icon); ?></div></td>
			</tr>
			<?php
		endif;
		?>
	</tbody>
</table>