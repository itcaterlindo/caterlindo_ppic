<?php
defined('BASEPATH') or exit('No direct script access allowed!');
extract($format_laporan);
?>
<style type="text/css">
	body {
		font-size: 12px;
	}
	img {
		width: 100% !important;
	}
	.table>tbody>tr>td {
		padding:1px;
	}
	.table>tfoot>tr>td {
		padding:1px;
	}
	/* @media print {
		body {transform: scale(80%);}
		.print {
			page-break-after: always;
		}
		.print-foot {
			page-break-inside: avoid;
		}
	} */
</style>
<div class="row">
	<div class="col-xs-12">
		<a href="javascript:void(0);" class="btn btn-danger no-print" onclick="window.close();" style="margin-left: 25px;">
			<i class="fa fa-ban"></i> Close
		</a>
		<a href="javascript:void(0);" class="btn btn-primary no-print" onclick="window.print();" style="margin-left: 25px;">
			<i class="fa fa-print"></i> Cetak Data
		</a>
	</div>
</div>
<div class="row invoice print">
	<!-- title row -->
	<div class="row">
		<div class="col-xs-12">
			<h2 class="page-header" style="margin-top: -20px;margin-bottom: 5px;">
				<div style="text-align: center;margin-right: 35px;">
					<?php echo $laporan_title; ?>
				</div>
				<div style="text-align: center;margin-top: -15px;">
					<?php echo $master_head['no_invoice']; ?>
				</div>
			</h2>
			<hr>
		</div><!-- /.col -->
	</div>
	<?php $this->load->view('page/'.$class_link.'/local/master_head_local', $master_head); ?>
	<?php $this->load->view('page/'.$class_link.'/local/item_detail_local', $item_detail); ?>
	<?php $this->load->view('page/'.$class_link.'/local/invoice_note_local'); ?>
	<hr>
	<table class="div-footer" style="width: 50%; " border="0">
		<tbody>
			<tr>
				<td>
					<img src="<?php echo base_url(); ?>assets/admin_assets/dist/img/LogoISO2021.jpg" alt="" >
				</td>
			</tr>
		</tbody>
	</table>
</div>