<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$color = color_status($status_so);
$word = process_status($status_so);
?>
<div class="row invoice-info">
	<div class="col-sm-4 invoice-col">
		<div style="font-size: 16px;">
			<?php
			if (!empty($no_invoice)) :
				?>
				<b style="margin-right: 15px;">Invoice No.</b> : <?php echo $no_invoice; ?><br>
				<?php
			endif;
			?>
			<b style="margin-right: 25px;">Order No.</b> : <?php echo $no_salesorder; ?><br>
		</div>
		<div class="no-print">
			<b>Sales Order Status :</b> <?php echo bg_label($word, $color); ?><br>
		</div>
	</div>
	<div class="col-sm-4 invoice-col">
		<?php
		if (!empty($no_quotation)) :
			?>
			<b>Purchase Order No./Date</b> : <?php echo $no_po.'/'.$tgl_po; ?>
			<?php
		endif;
		if (!empty($no_quotation)) :
			echo '<br><b>Quotation No./Date</b> : '.$no_quotation.'/'.$tgl_quotation;
		endif;
		?>
	</div>
	<div class="col-sm-4 invoice-col">
		<b>Tanggal Kirim</b> : <?php echo $tgl_kirim; ?><br>
	</div>
</div>
<hr style="margin-top: 5px;margin-bottom: 5px;">
<div class="row invoice-info">
	<div class="col-sm-4 invoice-col">
		Sales
		<address>
			<strong><?php echo $nm_salesperson; ?></strong><br>
			PT Caterlindo<br>
			Jln Industri No.18, Trosobo<br>
			Taman, Sidoarjo, Jawa Timur 61262<br>
			Phone : <?php echo $telp_sales; ?><br>
			Email : <?php echo $email_sales; ?>
		</address>
	</div>
	<div class="col-sm-4 invoice-col">
		Customer
		<address>
			<strong><?php echo $code_customer." ".$nm_customer; ?></strong><br>
			<?php echo $contact_customer.after_before_char($contact_customer, $contact_customer, '<br>', ''); ?>
			<?php echo $alamat_satu_customer; ?><br>
			<?php echo $alamat_dua_customer; ?><br>
			Phone : <?php echo $telp_customer; ?><br>
			Email : <?php echo $email_customer; ?><br>
			NPWP/NIK : <?php echo $npwp_customer; ?>
		</address>
	</div>
	<div class="col-sm-4 invoice-col">
		Deliver To
		<address>
			<strong><?php echo $nm_kirim; ?></strong><br>
			<?php echo $contact_kirim.after_before_char($contact_kirim, $contact_kirim, '<br>', ''); ?>
			<?php echo $alamat_satu_kirim; ?><br>
			<?php echo $alamat_dua_kirim; ?><br>
			Phone : <?php echo $telp_kirim; ?><br>
			Email : <?php echo $email_kirim; ?>
		</address>
	</div>
</div>