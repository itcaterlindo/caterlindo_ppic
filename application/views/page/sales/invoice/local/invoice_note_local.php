<?php
defined('BASEPATH') or exit('No direct script access allowed!');

if ($ket_note_so == 'available') :
	?>
	<div class="row invoice-info print-foot" style="margin-top: 15px;">
		Catatan :
		<?php echo $note_so; ?>
	</div>
	<?php
endif;
?>
<div class="row invoice-info print-foot">
	<div class="col-xs-9"></div>
	<div class="col-xs-3" style="text-align: center;">
		<div style="margin-bottom: 100px;">SIDOARJO, <?php echo $tgl_so; ?><br>Authorized on behalf of PT. Caterlindo</div>
		<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
		<div><b>Operational Manager</b></div>
	</div>
</div>