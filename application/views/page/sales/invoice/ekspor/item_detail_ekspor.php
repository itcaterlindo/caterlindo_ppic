<?php
defined('BASEPATH') or exit('No direct script access allowed!');
if (!empty($format_laporan)) :
	extract($format_laporan);
endif;
$angka = $jml_dp;
?>
<table style="column-width: 10px;width: 100%;margin-top: 15px;" border="1">
	<thead>
		<tr>
			<th style="width: 1%;text-align: center;">No.</th>
			<th style="width: 10%;text-align: center;">Code</th>
			<th style="width: 18%;text-align: center;">Description</th>
			<th style="width: 18%;text-align: center;">Size</th>
			<th style="width: 1%;text-align: center;">Tariff</th>
			<th style="width: 1%;text-align: center;">Qty</th>
			<th style="width: 5%;text-align: center;">Price/Unit</th>
			<th style="width: 5%;text-align: center;">Subtotal</th>
		</tr>
	</thead>
	<tbody>
		<?php
		if ($invoice_stat == 'dp') :
			?>
			<tr>
				<td></td>
				<td colspan="5"><b>Down Payment atas Pembelian Item yang Tersebut di Bawah ini:</b></td>
				<td style="text-align: right;"><?php echo format_currency($angka, $currency_icon); ?></td>
				<td></td>
				<td style="text-align: right;"><?php echo format_currency($angka, $currency_icon); ?></td>
			</tr>
			<?php
		endif;
		$no = 0;
		$tot_harga = array();
		$tot_qty = array();
		foreach ($items_data as $item_data) :
			$no_child = 0;
			$rowspan = 0;
			$rows = '';
			foreach ($details_data as $detail_data) :
				if ($detail_data['ditem_so_kd'] == $item_data->kd_ditem_so) :
					$no_child++;
				endif;
				$rowspan = 1 + $no_child;
				$rows = 'rowspan=\''.$rowspan.'\'';
			endforeach;
			$no++;
			
			if ($tipe_customer == 'Lokal') :
				$convert_retail = empty($val_retail)?'1':$val_retail;
				$convert_detail = empty($val_detail)?'1':$val_detail;
				if ($type_retail == 'primary') :
					$item_price_retail = $item_data->harga_retail / $convert_detail;
				elseif ($type_retail == 'secondary') :
					$item_price_retail = $item_data->harga_retail * $convert_retail;
				endif;
				if ($currency_type == 'primary') :
					$item_price = $item_data->harga_barang / $convert_detail;
				elseif ($currency_type == 'secondary') :
					$item_price = $item_data->harga_barang * $convert_retail;
				endif;
				$disc_type = $item_data->disc_type;
				$disc_val = $item_data->item_disc;
				$disc = $item_price_retail - $item_price;
				$item_disc = count_disc($disc_type, $disc_val, $item_price);
				$tot_def_disc = count_disc('percent', $default_disc, $item_price);
				$total_disc = $disc + $item_disc + $tot_def_disc;
				$total_harga = ($item_price_retail - $total_disc) * $item_data->item_qty;
			elseif ($tipe_customer == 'Ekspor') :
				$item_price_retail = $item_data->harga_barang;
				$disc_type = $item_data->disc_type;
				$disc_val = $item_data->item_disc;
				$item_disc = count_disc($disc_type, $disc_val, $item_price_retail);
				$tot_def_disc = count_disc('percent', $default_disc, $item_price_retail);
				$total_disc = $item_disc + $tot_def_disc;
				$prices = $item_price_retail - $total_disc;
				$total_harga = $prices * $item_data->item_qty;
			endif;
			
			$tot_harga[] = $total_harga;
			$tot_disc[] = $total_disc;
			$tot_price[] = $item_price_retail;
			$tot_qty[] = $item_data->item_qty;
			?>
			<tr>
				<td <?php echo $rows; ?> style="vertical-align: middle;"><?php echo $no; ?></td>
				<td><?php echo $item_data->item_code; ?></td>
				<td><?php echo $item_data->item_desc; ?></td>
				<td><?php echo $item_data->item_dimension; ?></td>
				<td><?php echo $item_data->hs_code; ?></td>
				<td style="text-align: center;"><?php echo $item_data->item_qty; ?></td>
				<?php
				if ($invoice_stat == 'final') :
					?>
					<td style="text-align: right;"><?php echo format_currency($prices, $currency_icon); ?></td>
					<td style="text-align: right;"><?php echo format_currency($total_harga, $currency_icon); ?></td>
					<?php
				elseif ($invoice_stat == 'dp') :
					?>
					<td style="text-align: right;"></td>
					<td style="text-align: right;"></td>
					<?php
				endif;
				?>
			</tr>
			<?php
			foreach ($details_data as $detail_data) :
				if ($detail_data['ditem_so_kd'] == $item_data->kd_ditem_so) :
					if ($tipe_customer == 'Lokal') :
						$convert_retail = empty($val_retail)?'1':$val_retail;
						$convert_detail = empty($val_detail)?'1':$val_detail;
						if ($type_retail == 'primary') :
							$item_price_retail = $detail_data['harga_retail'] * $convert_detail;
						elseif ($type_retail == 'secondary') :
							$item_price_retail = $detail_data['harga_retail'] / $convert_retail;
						endif;
						if ($type_retail == 'primary') :
							$item_price = $detail_data['harga_barang'] / $convert_detail;
						elseif ($type_retail == 'secondary') :
							$item_price = $detail_data['harga_barang'] * $convert_retail;
						endif;
						$disc_type = $detail_data['disc_type'];
						$disc_val = $detail_data['item_disc'];
						$disc = $item_price_retail - $item_price;
						$item_disc = count_disc($disc_type, $disc_val, $item_price);
						$tot_def_disc = count_disc('percent', $default_disc, $item_price);
						$total_disc = $disc + $item_disc + $tot_def_disc;
						$total_harga = ($item_price_retail - $total_disc) * $detail_data['item_qty'];
					elseif ($tipe_customer == 'Ekspor') :
						$item_price_retail = $detail_data['harga_barang'];
						$disc_type = $detail_data['disc_type'];
						$disc_val = $detail_data['item_disc'];
						$item_disc = count_disc($disc_type, $disc_val, $item_price_retail);
						$tot_def_disc = count_disc('percent', $default_disc, $item_price_retail);
						$total_disc = $item_disc + $tot_def_disc;
						$prices = $item_price_retail - $total_disc;
						$total_harga = $prices * $detail_data['item_qty'];
					endif;
					$tot_harga[] = $total_harga;
					$tot_disc[] = $total_disc;
					$tot_price[] = $item_price_retail;
					$tot_qty[] = $detail_data['item_qty'];
					?>
					<tr>
						<td><?php echo $detail_data['item_code']; ?></td>
						<td><?php echo $detail_data['item_desc']; ?></td>
						<td><?php echo $detail_data['item_dimension']; ?></td>
						<td><?php echo $item_data->hs_code; ?></td>
						<td style="text-align: center;"><?php echo $detail_data['item_qty']; ?></td>
						<?php
						if ($invoice_stat == 'final') :
							?>
							<td style="text-align: right;"><?php echo format_currency($prices, $currency_icon); ?></td>
							<td style="text-align: right;"><?php echo format_currency($total_harga, $currency_icon); ?></td>
							<?php
						elseif ($invoice_stat == 'dp') :
							?>
							<td style="text-align: right;"></td>
							<td style="text-align: right;"></td>
							<?php
						endif;
						?>
					</tr>
					<?php
				endif;
			endforeach;
		endforeach;
		$tots_pot = array();
		$tots_harga = array_sum($tot_harga);
		$tots_disc = array_sum($tot_disc);
		$tots_price = array_sum($tot_price);
		$tots_qty = array_sum($tot_qty);
		$jml_pot = count($tot_potongan);
		$jml_add = $jml_pot > 0?1:0;
		$jml_rowspan = 7 + $jml_pot + $jml_add;
		$jml_rowspan_det = 4 + $jml_pot;
		if ($invoice_stat == 'final') :
			?>
			<tr>
				<td colspan="3"></td>
				<td><b>Grand Total</b></td>
				<td></td>
				<td style="text-align: center;"><?php echo $tots_qty; ?></td>
				<td style="text-align: right;"><?php echo format_currency($tots_price, $currency_icon); ?></td>
				<td style="text-align: right;"><?php echo format_currency($tots_harga, $currency_icon); ?></td>
			</tr>
			<?php
		endif;
		?>
	</tbody>
</table>