<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$color = color_status($status_so);
$word = process_status($status_so);
?>
<table>
	<tr>
		<th style="width: 100px;vertical-align: top;">Invoice To :</th>
		<td style="width: 175px;">
			<strong><?php echo $code_customer." ".$nm_customer; ?></strong><br>
			<?php echo $contact_customer; ?><br>
			<?php echo $alamat_satu_customer; ?><br>
			<?php echo $alamat_dua_customer; ?>
		</td>
	</tr>
	<tr>
		<th style="width: 100px;vertical-align: top;">Notify Party :</th>
		<td>
			<strong>CI Group</strong><br>
			26 Howe Street<br>
			Osborne Park<br>
			Perth WA 6017
		</td>
	</tr>
	<tr>
		<th style="width: 100px;vertical-align: top;">Delivery Address :</th>
		<td>
			<strong><?php echo $nm_kirim; ?></strong><br>
			<?php echo $contact_kirim; ?><br>
			<?php echo $alamat_satu_kirim; ?><br>
			<?php echo $alamat_dua_kirim; ?>
		</td>
		<th style="width: 100px;vertical-align: top;">Date :</th>
		<td style="vertical-align: top;"><?php echo format_date($tgl_kirim, 'd M Y'); ?></td>
	</tr>
	<tr>
		<th style="width: 100px;vertical-align: top;">Freight By :</th>
		<td>-</td>
		<?php
		if (!empty($no_po)) :
			?>
			<th style="width: 100px;vertical-align: top;">Order No :</th>
			<td style="vertical-align: top;"><?php echo $no_po; ?></td>
			<?php
		endif;
		?>
	</tr>
	<tr>
		<th style="width: 125px;vertical-align: top;">Description of goods :</th>
		<td><?php echo !empty($goods_desc)?$goods_desc:'Stainless Steel Goods'; ?></td>
	</tr>
</table>