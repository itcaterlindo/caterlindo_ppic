<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<?php
if ($format == 'final') :
	?>
	<div class="row invoice-info print-foot" style="margin-top: 15px;">
		<div class="col-sm-2 invoice-col">
			Container Number :
		</div>
		<div class="col-sm-3 invoice-col">
			-
		</div>
	</div>
	<div class="row invoice-info print-foot">
		<div class="col-sm-2 invoice-col">
			Seal Number :
		</div>
		<div class="col-sm-3 invoice-col">
			-
		</div>
	</div>
	<div class="row invoice-info print-foot" style="margin-top: 15px;">
		<div class="col-xs-9"></div>
		<div class="col-xs-3" style="text-align: center;">
			<div style="margin-bottom: 100px;">Prepared by,</div>
			<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
			<div><b>Exim</b></div>
		</div>
	</div>
	<?php
else :
	?>
	<div class="row invoice-info print-foot" style="margin-top: 15px;">
		<div class="col-sm-1 invoice-col">
			Terms :
		</div>
		<div class="col-sm-3 invoice-col">
			EX-WORK SURABAYA
		</div>
	</div>
	<div class="row invoice-info print-foot">
		<div class="col-sm-1 invoice-col">
			PAYMENT
		</div>
		<div class="col-sm-3 invoice-col">
			100 % before dispatch
		</div>
	</div>
	<div class="row invoice-info print-foot">
		<div class="col-sm-1 invoice-col">
			Bank Details :
		</div>
	</div>
	<div class="row invoice-info print-foot">
		<div class="col-sm-1 invoice-col">
			Account Name :
		</div>
		<div class="col-sm-3 invoice-col">
			CI Group Pty Ltd
		</div>
	</div>
	<div class="row invoice-info print-foot">
		<div class="col-sm-1 invoice-col"></div>
		<div class="col-sm-3 invoice-col">
			Bank: Westpac Banking Corporation
		</div>
	</div>
	<div class="row invoice-info print-foot">
		<div class="col-sm-1 invoice-col"></div>
		<div class="col-sm-3 invoice-col">
			109 St Georges Terrace
		</div>
	</div>
	<div class="row invoice-info print-foot">
		<div class="col-sm-1 invoice-col"></div>
		<div class="col-sm-3 invoice-col">
			Perth, Western Australia, 6000
		</div>
	</div>
	<div class="row invoice-info print-foot">
		<div class="col-sm-1 invoice-col">
			Branch Code (BSB)
		</div>
		<div class="col-sm-3 invoice-col">
			034702
		</div>
	</div>
	<div class="row invoice-info print-foot">
		<div class="col-sm-1 invoice-col">
			Account :
		</div>
		<div class="col-sm-3 invoice-col">
			695930
		</div>
	</div>
	<div class="row invoice-info print-foot">
		<div class="col-sm-1 invoice-col">
			Swift Code :
		</div>
		<div class="col-sm-3 invoice-col">
			WPACAU2SXXX
		</div>
	</div>
	<div class="row invoice-info print-foot" style="margin-top: 15px;">
		<div class="col-xs-9"></div>
		<div class="col-xs-3" style="text-align: center;">
			<div style="margin-bottom: 100px;">Approved by,</div>
			<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
			<div><b>Exim</b></div>
		</div>
	</div>
	<?php
endif;