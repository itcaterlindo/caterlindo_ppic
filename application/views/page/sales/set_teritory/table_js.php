<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table();
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		get_form('');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table() {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/open_table'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function remove_box(element) {
		$(element).slideUp(function() {
			$(this).remove();
		});
	}

	function get_form(id) {
		if (id != '') {
			$('#<?php echo $btn_add_id; ?>').slideDown();
		}
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		remove_box('#idFormBoxTeritory');
		if ($('#idFormBoxTeritory').length < 1) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/get_form'; ?>',
				data: 'id='+id,
				success: function(html) {
					$('#idMainContent').prepend(html);
				}
			});
		}
	}

	function hapus_data(id) {
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		$('#<?php echo $box_overlay_id; ?>').show();
		remove_box('#idFormBoxTeritory');
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/delete_data'; ?>',
			data: 'id='+id,
			success: function(data) {
				$('#<?php echo $box_alert_id; ?>').html(data.alert).fadeIn();
				$('#<?php echo $box_overlay_id; ?>').hide();
				open_table();
			}
		});
	}
</script>