<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'Teritory';
$form_id = 'idForm'.$master_var;

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtId', 'value' => $teritory['id']));
?>
<div class="form-group">
	<label for="idTxtNama" class="col-md-2 control-label">Nama Teritory</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrNama"></div>
		<?php if(empty($teritory['id'])): ?>
			<?php echo form_input(array('name' => 'txtNama', 'id' => 'idTxtNama', 'class' => 'form-control', 'value' => $teritory['nm_teritory'], 'placeholder' => 'Nama Teritory')); ?>
		<?php else: ?>
			<label for="idTxtNama" class="col-md-1 control-label"><?= $teritory['nm_teritory'] ?></label>
		<?php endif; ?>
	</div>
</div>

<div class="form-group">
	<label for="idTxtNama" class="col-md-2 control-label">Deskripsi</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrDeskripsi"></div>
		<?php echo form_input(array('name' => 'txtDeskripsi', 'id' => 'idtxtDeskripsi', 'class' => 'form-control', 'value' => $teritory['deskripsi'], 'placeholder' => 'Deskripsi')); ?>
	</div>
</div>

<div class="form-group">
	<label for="idTxtNama" class="col-md-2 control-label">Tipe</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrTipe"></div>
		<div id="idTxtTipe"><?php echo form_dropdown('txtTipe', $tipe_teritory, $teritory['tipe_teritory_id'], array('name' => 'txtTipe', 'id' => 'idTxtTipe', 'class' => 'form-control')); ?></div>
	</div>
</div>

<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" class="btn btn-primary btn-flat">
			<i class="fa fa-save"></i> Submit
		</button>
	</div>
</div>
<?php
echo form_close();