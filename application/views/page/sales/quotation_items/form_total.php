<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<h3>Special Discount</h3>
<?php echo form_open('', array('id' => 'idFormTotalPrice', 'class' => 'form-horizontal')); ?>
<div class="form-group">
	<label for="idTxtNmKolomPrice" class="col-md-2 control-label">Nama Discount</label>
	<div class="col-md-3">
		<div id="idErrNmKolom"></div>
		<?php echo form_input(array('type' => 'hidden', 'name' => 'txtKdMQuotationPrice', 'id' => 'idTxtKdMQuotationPrice', 'value' => $kd_mquotation)); ?>
		<?php echo form_input(array('name' => 'txtNmKolomPrice', 'id' => 'idTxtNmKolomPrice', 'class' => 'form-control', 'placeholder' => ' Nama Kolom')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idSelKolomType" class="col-md-2 control-label">Type Kolom</label>
	<div class="col-md-2">
		<div id="idErrSelKolomType"></div>
		<?php echo form_dropdown('selKolomType', array('nilai' => 'Nilai'), 'nilai', array('id' => 'idSelKolomType', 'class' => 'form-control')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtTotalJumlah" class="col-md-2 control-label">Total Jumlah</label>
	<div class="col-md-2">
		<div id="idErrTotalJumlah"></div>
		<?php echo form_input(array('name' => 'txtTotalJumlah', 'id' => 'idTxtTotalJumlah', 'class' => 'form-control', 'placeholder' => ' Total Jumlah')); ?>
	</div>
</div>
<div class="form-group">
	<label class="col-md-2 control-label"></label>
	<div class="col-md-2">
		<button type="submit" name="btnSubmitTotal" class="btn btn-default">
			<i class="fa fa-dollar"></i> Input Total
		</button>
	</div>
</div>
<?php echo form_close(); ?>