<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$color = '';
$word = '';
if (isset($status_quotation) && !empty($status_quotation)) :
	$color = color_status($status_quotation);
	$word = process_status($status_quotation);
endif;
$title = (isset($title) && !empty($title))?$title:'<img src="'.base_url().'assets/admin_assets/dist/img/simply_stainless_logo_compress.jpg" style="width: 250px;">';
$footer = (isset($footer) && !empty($footer))?$footer:'';
?>
<style type="text/css">
	body {
		font-size: 12px;
	}
	img {
		width: 100% !important;
	}
	.table>tbody>tr>td {
		padding:1px;
	}
	.table>tfoot>tr>td {
		padding:1px;
	}
	/* @media print {
		body {transform: scale(80%);}
		.print {
			page-break-after: always;
		}
		.print-foot {
			page-break-inside: avoid;
		}
	} */
</style>
<div class="row">
	<div class="col-xs-12">
		<a href="javascript:void(0);" class="btn btn-primary no-print" onclick="window.print();">
			<i class="fa fa-print"></i> Cetak Data
		</a>
	</div>
</div>
<div class="row invoice print">
	<!-- title row -->
	<div class="row">
		<div class="col-xs-12">
			<h2 class="page-header">
				<?php echo $title; ?>
			</h2>
		</div><!-- /.col -->
	</div>
	<div class="row invoice-info">
		<div class="row">
			<div class="col-xs-12">
				<small class="pull-right" style="margin-right: 20px;">Date: <?php echo date('d-m-Y'); ?></small>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="col-sm-4 invoice-col">
					From
					<address>
						<strong><?php echo $nm_salesperson; ?></strong><br>
						Jln Industri No.18, Trosobo<br>
						Taman, Sidoarjo Regency, East Java 61262<br>
						Phone: <?php echo $telp_salesperson; ?><br>
						Email: <?php echo $email_salesperson; ?>
					</address>
				</div>
				<div class="col-sm-4 invoice-col">
					To
					<address>
						<strong><?php echo $code_customer." ".$header_customer." ".$nm_customer; ?></strong><br>
						<?php echo $alamat_satu; ?><br>
						<?php echo $alamat_dua; ?><br>
						Phone: <?php echo $telp_customer; ?><br>
						Email: <?php echo $email_customer; ?>
					</address>
				</div>
				<div class="col-sm-4 invoice-col">
					<b>Quotation No. :</b> #<?php echo $no_quotation; ?><br>
					<b class="no-print">Quotation Status: <?php echo bg_label($word, $color); ?></b>
				</div>
			</div>
		</div>
	</div>
	<?php if (isset($items_data) && !empty($items_data)) : ?>
	<table border="1" style="width: 100%;">
		<tr style="border-top: 2px solid #000;">
			<th style="width: 25px;text-align: center;border-left: 2px solid #000;border-right: 2px solid #000; padding: 5px;">No.</th>
			<th style="width: 160px;border-right: 2px solid #000; padding: 5px;">Product Code</th>
			<th style="width: 95px;border-right: 2px solid #000; padding: 5px;">Status</th>
			<th style="width: 380px;border-right: 2px solid #000; padding: 5px;">Description</th>
			<th style="width: 380px;border-right: 2px solid #000; padding: 5px;">Dimension</th>
			<th style="width: 250px;border-right: 2px solid #000; padding: 5px;">Notes</th>
			<th style="width: 60px;text-align: center;border-right: 2px solid #000; padding: 5px;">Qty</th>
			<th style="width: 150px;text-align: center;border-right: 2px solid #000; padding: 5px;">Price/Unit</th>
			<th style="width: 150px;border-right: 2px solid #000; padding: 5px;">Disc</th>
			<th style="width: 125px;text-align: center;border-right: 2px solid #000; padding: 5px;">Subtotal</th>
			<th style="width: 125px;text-align: center;border-right: 2px solid #000; padding: 5px;">Total Item</th>
		</tr>
		<?php
		$no = 0;
		$tot_harga = array();
		$tot_disc = array();
		$tot_price = array();
		foreach ($items_data as $item_data) :
			$no_child = 0;
			$rowspan = 0;
			if (!empty($details_data)) :
				foreach ($details_data as $detail_data) :
					if ($detail_data['ditem_quotation_kd'] == $item_data->kd_ditem_quotation) :
						$no_child++;
					endif;
						$rowspan = 1 + $no_child;
				endforeach;
			endif;
			$rowspn = !empty($rowspan)?'rowspan="'.$rowspan.'"':'';
			$no++;

			if ($tipe_customer == 'Lokal' || empty($tipe_customer)) :
				$convert_retail = empty($val_retail)?'1':$val_retail;
				$convert_detail = empty($val_detail)?'1':$val_detail;
				if ($type_retail == 'primary') :
					$item_price_retail = $item_data->harga_retail / $convert_detail;
				elseif ($type_retail == 'secondary') :
					$item_price_retail = $item_data->harga_retail * $convert_retail;
				endif;
				if ($currency_type == 'primary') :
					$item_price = $item_data->harga_barang / $convert_detail;
				elseif ($currency_type == 'secondary') :
					$item_price = $item_data->harga_barang * $convert_retail;
				endif;

				$disc_type = $item_data->disc_type;
				$disc_val = $item_data->item_disc;
				$disc = $item_price_retail - $item_price;
				$item_disc = count_disc($disc_type, $disc_val, $item_price);
				$total_disc = $disc + $item_disc;
				$total_harga = ($item_price_retail - $total_disc) * $item_data->item_qty;
			elseif ($tipe_customer == 'Ekspor') :
				$item_price_retail = $item_data->harga_barang;				
				$disc_type = $item_data->disc_type;
				$disc_val = $item_data->item_disc;
				$total_disc = count_disc($disc_type, $disc_val, $item_price_retail);
				$total_harga = ($item_price_retail - $total_disc) * $item_data->item_qty;
			endif;
			$tot_harga[] = $item_data->total_harga;
			$tot_disc[] = $total_disc;
			$tot_price[] = $item_price_retail;

			if (!empty($details_data)) :
				foreach ($details_data as $detail_data) :
					if ($detail_data['ditem_quotation_kd'] == $item_data->kd_ditem_quotation) :
						$tot_harga_child[$item_data->kd_ditem_quotation][] = $detail_data['total_harga'];
					endif;
				endforeach;
			endif;
			if (isset($tot_harga_child[$item_data->kd_ditem_quotation])) :
				$total_harga_child = array_sum($tot_harga_child[$item_data->kd_ditem_quotation]);
			else :
				$total_harga_child = 0;
			endif;
			$total_harga_item = $item_data->total_harga + $total_harga_child;
			?>
			<tr style="border-top: 2px solid #000;">
				<td <?php echo $rowspn; ?> style="vertical-align: middle;text-align: center;border-left: 2px solid #000;border-right: 2px solid #000;"><?php echo $no; ?></td>
				<td style="padding: 5px;"><?php echo $item_data->item_code; ?></td>
				<td style="padding: 5px;"><?php echo item_stat($item_data->item_status); ?></td>
				<td style="padding: 5px;"><?php echo $item_data->item_desc; ?></td>
				<td style="padding: 5px;"><?php echo $item_data->item_dimension; ?></td>
				<td style="padding: 5px;"><?php echo $item_data->item_note; ?></td>
				<td style="text-align: center; padding: 5px;"><?php echo $item_data->item_qty; ?></td>
				<td style="text-align: right; padding: 5px;"><?php echo format_currency($item_price_retail, $icon_type.' '.$currency_icon.' '.$pemisah_angka.' '.$format_akhir); ?></td>
				<td style="text-align: right; padding: 5px;">
					<?php
					echo format_currency($total_disc, $icon_type.' '.$currency_icon.' '.$pemisah_angka.' '.$format_akhir).' - ';
					?>
				</td>
				<td style="text-align: right; padding: 5px;"><?php echo format_currency($total_harga, $icon_type.' '.$currency_icon.' '.$pemisah_angka.' '.$format_akhir); ?></td>
				<td <?php echo $rowspn; ?> style="vertical-align: center;text-align: right;border-left: 2px solid #000;border-right: 2px solid #000; padding: 5px;"><?php echo format_currency($total_harga_item, $icon_type.' '.$currency_icon.' '.$pemisah_angka.' '.$format_akhir); ?></td>
			</tr>
			<?php
			if (!empty($details_data)) :
				foreach ($details_data as $detail_data) :
					if ($detail_data['ditem_quotation_kd'] == $item_data->kd_ditem_quotation) :
						if ($tipe_customer == 'Lokal' || empty($tipe_customer)) :
							$convert_retail = empty($val_retail)?'1':$val_retail;
							$convert_detail = empty($val_detail)?'1':$val_detail;
							if ($type_retail == 'primary') :
								$item_price_retail = $detail_data['harga_retail'] * $convert_detail;
							elseif ($type_retail == 'secondary') :
								$item_price_retail = $detail_data['harga_retail'] / $convert_retail;
							endif;
							if ($type_retail == 'primary') :
								$item_price = $detail_data['harga_barang'] / $convert_detail;
							elseif ($type_retail == 'secondary') :
								$item_price = $detail_data['harga_barang'] * $convert_retail;
							endif;
							
							$disc_type = $detail_data['disc_type'];
							$disc_val = $detail_data['item_disc'];
							$disc = $item_price_retail - $item_price;
							$item_disc = count_disc($disc_type, $disc_val, $item_price);
							$total_disc = $disc + $item_disc;
							$total_harga = ($item_price_retail - $total_disc) * $detail_data['item_qty'];
						elseif ($tipe_customer == 'Ekspor') :
							$item_price_retail = $detail_data['harga_barang'];
							$disc_type = $detail_data['disc_type'];
							$disc_val = $detail_data['item_disc'];
							$total_disc = count_disc($disc_type, $disc_val, $item_price_retail);
							$total_harga = ($item_price_retail - $total_disc) * $detail_data['item_qty'];
						endif;
						$tot_harga[] = $detail_data['total_harga'];
						$tot_disc[] = $total_disc;
						$tot_price[] = $item_price_retail;
						?>
						<tr>
							<td style="padding: 5px;"><?php echo $detail_data['item_code']; ?></td>
							<td style="padding: 5px;"><?php echo item_stat($detail_data['item_status']); ?></td>
							<td style="padding: 5px;"><?php echo $detail_data['item_desc']; ?></td>
							<td style="padding: 5px;"><?php echo $detail_data['item_dimension']; ?></td>
							<td style="padding: 5px;"><?php echo $detail_data['item_note']; ?></td>
							<td style="text-align: center; padding: 5px;"><?php echo $detail_data['item_qty']; ?></td>
							<td style="text-align: right; padding: 5px;"><?php echo format_currency($item_price_retail, $icon_type.' '.$currency_icon.' '.$pemisah_angka.' '.$format_akhir); ?></td>
							<td style="text-align: right; padding: 5px;">
								<?php
								echo format_currency($total_disc, $icon_type.' '.$currency_icon.' '.$pemisah_angka.' '.$format_akhir).' - ';
								?>
							</td>
							<td style="text-align: right; padding: 5px;"><?php echo format_currency($total_harga, $icon_type.' '.$currency_icon.' '.$pemisah_angka.' '.$format_akhir); ?></td>
						</tr>
						<?php
					endif;
				endforeach;
			endif;
		endforeach;
		?>
		<?php
		$tots_pot = array();
		$tots_harga = array_sum($tot_harga);
		$tots_disc = array_sum($tot_disc);
		$tots_price = array_sum($tot_price);
		$jml_pot = count($tot_potongan);
		$jml_add = $jml_pot > 0?1:0;
		$jml_rowspan = 6 + $jml_pot + $jml_add;
		$jml_rowspan_det = 6 + $jml_pot;
		?>
		<tr style="border-top: 2px solid #000; border-left: 2px solid #000; border-right: 2px solid #000;">
			<td colspan="6" rowspan="<?php echo $jml_rowspan; ?>" style="border-bottom: 2px solid #000; padding: 10px;"><?php echo $footer.'<br />'.$term_payment_format; ?></td>
			<th colspan="2" style="border-left: 2px solid #000; padding: 5px;">Total Price</th>
			<td style="text-align: right;"></td>
			<td style="text-align: right;"></td>
			<td style="text-align: right; padding: 5px;"><?php echo format_currency($tots_harga, $icon_type.' '.$currency_icon.' '.$pemisah_angka.' '.$format_akhir); ?></td>
		</tr>
		<?php $no = 0; ?>
		<?php foreach ($tot_potongan as $tot) : ?>
			<?php $no++; ?>
			<?php $tots_pot[] = $tot->total_nilai; ?>
			<tr style="border-right: 2px solid #000;">
				<td colspan="2" style="padding: 5px;"><?php echo $tot->nm_kolom; ?></td>
				<?php if ($no == 1) : ?>
					<td colspan="2" rowspan="<?php echo $jml_rowspan_det; ?>"></td>
				<?php endif; ?>
				<td style="text-align: right; padding: 5px;"><?php echo format_currency($tot->total_nilai, $icon_type.' '.$currency_icon.' '.$pemisah_angka.' '.$format_akhir); ?></td>
			</tr>
			<?php if ($no == $jml_pot) : ?>
				<?php $tot_pot = array_sum($tots_pot); ?>
				<?php $tots_harga = $tots_harga - $tot_pot; ?>
				<tr style="border-right: 2px solid #000;">
					<th colspan="2" style="padding: 5px;">Total Price</th>
					<td style="text-align: right; padding: 5px;"><?php echo format_currency($tots_harga, $icon_type.' '.$currency_icon.' '.$pemisah_angka.' '.$format_akhir); ?></td>
				</tr>
			<?php endif; ?>
		<?php endforeach; ?>
		<?php $rowspan_lagi = $no < 1?'rowspan=\'6\'':'';?>
		<tr style="border-right: 2px solid #000;">
			<td colspan="2" style="padding: 5px;">Ongkir</td>
			<?php
			if ($no < 1) :
				echo '<td colspan="2" '.$rowspan_lagi.' style="border-bottom: 2px solid #000;"></td>';
			endif;
			?>
			<td style="text-align: right;">
				<div id="idTextOngkir" style="padding: 5px;"><?php echo format_currency($ongkir, $icon_type.' '.$currency_icon.' '.$pemisah_angka.' '.$format_akhir); ?></div>
			</td>
		</tr>
		<tr style="border-right: 2px solid #000;">
			<td colspan="2" style="padding: 5px;">Installasi</td>
			<td style="text-align: right; padding: 5px;">
				<div id="idTextInstall"><?php echo format_currency($install, $icon_type.' '.$currency_icon.' '.$pemisah_angka.' '.$format_akhir); ?></div>
			</td>
		</tr>
		<?php
		$tots_harga = ($tots_harga + $ongkir + $install);
		$tot_ppn = count_disc('percent', $jml_ppn, $tots_harga);
		if (empty($decimal) || $decimal == '0') :
			$tot_ppn_lagi = pembulatan_decimal($tot_ppn);
		elseif ($decimal == '1') :
			$tot_ppn_lagi = $tot_ppn;
		endif;
		?>
		<tr style="border-right: 2px solid #000;">
			<th colspan="2" style="padding: 5px;">Total Price</th>
			<td style="text-align: right; padding: 5px;"><div id="idTextPpn"><?php echo format_currency($tots_harga, $icon_type.' '.$currency_icon.' '.$pemisah_angka.' '.$format_akhir); ?></div></td>
		</tr>
		<tr style="border-right: 2px solid #000;">
			<td colspan="2" style="padding: 5px;"><?php echo $nm_kolom_ppn; ?></td>
			<td style="text-align: right; padding: 5px;"><div id="idTextPpn"><?php echo format_currency($tot_ppn_lagi, $icon_type.' '.$currency_icon.' '.$pemisah_angka.' '.$format_akhir); ?></div></td>
		</tr>
		<?php $grand_tot = $tots_harga + $tot_ppn_lagi; ?>
		<tr style="border-bottom: 2px solid #000; border-right: 2px solid #000;">
			<th colspan="2" style="padding: 5px;">Grand Total</th>
			<td style="text-align: right; padding: 5px;"><div id="idTextGrandTot"><?php echo format_currency($grand_tot, $icon_type.' '.$currency_icon.' '.$pemisah_angka.' '.$format_akhir); ?></div></td>
		</tr>
	</table>
	<?php endif; ?>
	<hr>
	<table class="div-footer" style="width: 50%; " border="0">
		<tbody>
			<tr>
				<td>
					<img src="<?php echo base_url(); ?>assets/admin_assets/dist/img/LogoISO2021.jpg" alt="" >
				</td>
			</tr>
		</tbody>
	</table>
</div>