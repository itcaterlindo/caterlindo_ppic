<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'Container';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtId', 'value' => $id));
?>
<div class="form-group">
	<label for="idTxtNama" class="col-md-2 control-label">Nama Container</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrNama"></div>
		<?php echo form_input(array('name' => 'txtNama', 'id' => 'idTxtNama', 'class' => 'form-control', 'value' => $nm_container, 'placeholder' => 'Nama Container')); ?>
	</div>
</div>

<div class="form-group">
	<label for="idTxtNama" class="col-md-2 control-label">Deskripsi</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrDeskripsi"></div>
		<?php echo form_input(array('name' => 'txtDeskripsi', 'id' => 'idtxtDeskripsi', 'class' => 'form-control', 'value' => $deskripsi, 'placeholder' => 'Deskripsi')); ?>
	</div>
</div>

<div class="form-group">
	<label for="idTxtNama" class="col-md-2 control-label">20ft</label>
	<div class="col-sm-2 col-xs-12">
		<div id="idErr20ft"></div>
		<?php echo form_input(array('type' => 'number', 'name' => 'txt20Ft', 'id' => 'idtxt20Ft', 'class' => 'form-control', 'value' => $konversi_20ft, 'placeholder' => '20Ft')); ?>
	</div>
</div>

<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" class="btn btn-primary btn-flat">
			<i class="fa fa-save"></i> Submit
		</button>
	</div>
</div>
<?php
echo form_close();