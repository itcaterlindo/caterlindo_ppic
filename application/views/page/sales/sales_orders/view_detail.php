<?php
defined('BASEPATH') or exit('No direct script access allowed!');
// print_r($_SESSION);
?>

<div class="box box-warning" id="idBoxFormSalesOrder">
	<div class="box-header with-border">
		<h3 id="idTitleTable" class="box-title">Detail Sales Order : #<?php echo $master_head['no_salesorder']; ?></h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool" id="idBtnHideBoxForm" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
				<i class="fa fa-minus"></i>
			</button>
			<button class="btn btn-box-tool" id="idBtnTutupKotak" data-widget="remove" data-toggle="tooltip" title="Tutup">
				<i class="fa fa-times"></i>
			</button>
		</div>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-xs-12">
				<?php
				$color = array('default', 'warning', 'danger', 'primary', 'success', 'info');
				if ($view_type == 'so_detail') :
					if (!empty($pihak_default_disc)) :
						foreach ($pihak_default_disc as $row) :
							shuffle($color);
							?>
							<a href="<?php echo base_url().$class_link.'/sales_order_view/cetak/'.strtolower($row->nm_pihak).'/'.$kd_msalesorder; ?>" class="btn btn-<?php echo $color[0]; ?> no-print" target="_blank">
								<i class="fa fa-print"></i> Cetak Data <?php echo $row->nm_pihak; ?>
							</a>
							<?php
						endforeach;
					else :
						?>
						<a href="<?php echo base_url().$class_link.'/sales_order_view/cetak/system_default/'.$kd_msalesorder; ?>" class="btn btn-primary no-print" target="_blank">
							<i class="fa fa-print"></i> Cetak Data
						</a>
						<?php
					endif;
				elseif ($view_type == 'inv_dp_detail') :
						/* invoice dp button goes here */
						echo !empty($master_head['no_invoice_dp'])?anchor('sales/invoice/invoice_data/cetak/'.$sales_tipe.'/dp/'.$kd_msalesorder, '<i class="fa fa-print"></i> Cetak : '.$master_head['no_invoice_dp'], array('class' => 'btn btn-'.$color[rand(0, 5)].' btn-flat', 'title' => 'Cetak Data Invoice DP '.$master_head['no_invoice_dp'], 'target' => '_blank', 'style' => 'margin-right: 5px;')):'';
						foreach ($invoice_datas as $invoice_data) :
							echo anchor('sales/invoice/invoice_data/cetak/'.$sales_tipe.'/dp/'.$kd_msalesorder.'/'.$invoice_data->kd_so_termin, '<i class="fa fa-print"></i> Cetak : '.$invoice_data->no_invoice, array('class' => 'btn btn-'.$color[rand(0, 5)].' btn-flat', 'title' => 'Cetak Data Invoice DP '.$invoice_data->no_invoice, 'target' => '_blank', 'style' => 'margin-right: 5px;'));
						endforeach;
				endif;
				?>
			</div>
		</div>
		<hr>
		<?php $this->load->view('page/'.$class_link.'/part/master_head', $master_head); ?>
		<hr>
		<?php $this->load->view('page/'.$class_link.'/part/item_detail', $item_detail); ?>
	</div>
	<div class="box-footer">
	</div>
	<div class="overlay" id="idOverlayForm" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
</div>