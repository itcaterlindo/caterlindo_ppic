<?php
defined('BASEPATH') or exit('No direct script access allowed!');

if ($ket_note_so == 'available') :
	?>
	<div class="row invoice-info print-foot" style="margin-top: 20px;">
		Catatan :
		<?php echo $note_so; ?>
	</div>
	<?php
endif;
?>
<div class="row invoice-info print-foot" style="margin-top: 20px;">
	<div class="col-xs-3" style="text-align: center;">
		<div style="margin-bottom: 100px;">Dibuat Oleh</div>
		<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
		<div><b>Sales Person</b></div>
	</div>
	<div class="col-xs-6"></div>
	<div class="col-xs-3" style="text-align: center;">
		<div style="margin-bottom: 100px;">Disetujui Oleh</div>
		<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
		<div><b>Customer</b></div>
	</div>
</div>
<hr>
<table class="div-footer" style="width: 50%; " border="0">
	<tbody>
		<tr>
			<td>
				<img src="<?php echo base_url(); ?>assets/admin_assets/dist/img/LogoISO2021.jpg" alt="" >
			</td>
		</tr>
	</tbody>
</table>