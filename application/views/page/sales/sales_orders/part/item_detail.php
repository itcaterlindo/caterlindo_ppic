<?php
defined('BASEPATH') or exit('No direct script access allowed!');
if (!empty($format_laporan)) :
	extract($format_laporan);
endif;

if (!empty($default_disc)) :
	foreach ($default_disc as $row) :
		$item_discs[$row->so_item_kd] = array('jml_disc' => $row->jml_disc, 'type_individual_disc' => $row->type_individual_disc, 'jml_individual_disc' => $row->jml_individual_disc);	
	endforeach;
endif;
?>
<table style="column-width: 10px;width: 100%;" border="1">
	<thead>
		<tr>
			<th style="width: 1%;text-align: center;">No.</th>
			<th style="width: 10%;text-align: center;">Prod Code</th>
			<th style="width: 1%;text-align: center;">Status</th>
			<th style="width: 18%;text-align: center;">Description</th>
			<th style="width: 18%;text-align: center;">Dimension</th>
			<th style="width: 1%;text-align: center;">Qty</th>
			<th style="width: 5%;text-align: center;" class="no-print">NW</th>
			<th style="width: 5%;text-align: center;" class="no-print">GW</th>
			<th style="width: 5%;text-align: center;" class="no-print">CBM</th>
			<th style="width: 13%;text-align: center;">Price/Unit</th>
			<th style="width: 12%;text-align: center;">Disc</th>
			<th style="width: 12%;text-align: center;">Subtotal</th>
		</tr>
	</thead>
	<?php
	if (!empty($items_data)) :
		?>
		<tbody>
			<?php
			$no = 0;
			$tot_harga = array();
			foreach ($items_data as $item_data) :
				$no_child = 0;
				$jml = 0;
				foreach ($details_data as $detail_data) :
					if ($detail_data['ditem_so_kd'] == $item_data->kd_ditem_so) :
						$no_child++;
					endif;
					$jml = 1 + $no_child;
				endforeach;
				if ($jml == 0) :
					$rowspan = '';
				else :
					$rowspan = 'rowspan="'.$jml.'"';
				endif;
				$no++;

				$jml_disc = isset($item_discs[$item_data->kd_ditem_so]['jml_disc'])?$item_discs[$item_data->kd_ditem_so]['jml_disc']:'0';
				$type_individual_disc = isset($item_discs[$item_data->kd_ditem_so]['type_individual_disc'])?$item_discs[$item_data->kd_ditem_so]['type_individual_disc']:'decimal';
				$jml_individual_disc = isset($item_discs[$item_data->kd_ditem_so]['jml_individual_disc'])?$item_discs[$item_data->kd_ditem_so]['jml_individual_disc']:'0';
				if ($tipe_customer == 'Lokal') :
					$convert_retail = empty($val_retail)?'1':$val_retail;
					$convert_detail = empty($val_detail)?'1':$val_detail;
					if ($type_retail == 'primary') :
						$item_price_retail = $item_data->harga_retail / $convert_detail;
					elseif ($type_retail == 'secondary') :
						$item_price_retail = $item_data->harga_retail * $convert_retail;
					endif;
					if ($currency_type == 'primary') :
						$item_price = $item_data->harga_barang / $convert_detail;
					elseif ($currency_type == 'secondary') :
						$item_price = $item_data->harga_barang * $convert_retail;
					endif;
					$jml_default_disc = count_disc('percent', $jml_disc, $item_price);
					$item_price = $item_price - $jml_default_disc;
					$val_individual_disc = count_disc($type_individual_disc, $jml_individual_disc, $item_price);
					$item_price = $item_price - $val_individual_disc;
					
					$disc_type = $item_data->disc_type;
					$disc_val = $item_data->item_disc;
					$disc = $item_price_retail - $item_price;
					$item_disc = count_disc($disc_type, $disc_val, $item_price);
					$total_disc = $disc + $item_disc;
					$total_harga = ($item_price_retail - $total_disc) * $item_data->item_qty;
				elseif ($tipe_customer == 'Ekspor') :
					$item_price_retail = $item_data->harga_barang;

					$jml_default_disc = count_disc('percent', $jml_disc, $item_price_retail);
					$item_price_retail = $item_price_retail - $jml_default_disc;
					$val_individual_disc = count_disc($type_individual_disc, $jml_individual_disc, $item_price_retail);
					$item_price_retail = $item_price_retail - $val_individual_disc;
					$item_price_retail = round($item_price_retail, 2);
					
					$disc_type = $item_data->disc_type;
					$disc_val = $item_data->item_disc;
					$item_disc = count_disc($disc_type, $disc_val, $item_price_retail);
					$total_disc = $item_disc;
					$prices = $item_price_retail - $total_disc;
					$total_harga = $prices * $item_data->item_qty;
					$total_harga = round($total_harga, 2);
				endif;
				
				$net = $item_data->item_qty * $item_data->netweight;
				$gross = $item_data->item_qty * $item_data->grossweight;
				$cbm = $item_data->item_qty * $item_data->item_cbm;
				$tot_net[] = $net;
				$tot_gross[] = $gross;
				$tot_cbm[] = $cbm;
				$tot_harga[] = $total_harga;
				$tot_disc[] = $total_disc;
				$tot_price[] = $item_price_retail;
				$tot_qty[] = $item_data->item_qty;
				?>
				<tr>
					<td <?php echo $rowspan; ?> style="vertical-align: middle;text-align: center;"><?php echo $no; ?></td>
					<td><?php echo $item_data->item_code; ?></td>
					<td><?php echo item_stat($item_data->item_status); ?></td>
					<td><?php echo $item_data->item_desc; ?></td>
					<td><?php echo $item_data->item_dimension; ?></td>
					<td style="text-align: center;"><?php echo $item_data->item_qty; ?></td>
					<td style="text-align: center;" class="no-print"><?php echo $net; ?></td>
					<td style="text-align: center;" class="no-print"><?php echo $gross; ?></td>
					<td style="text-align: center;" class="no-print"><?php echo $cbm; ?></td>
					<td style="text-align: right;"><?php echo format_currency($item_price_retail, $currency_icon); ?></td>
					<?php
					if ($disc_view) :
						?>
         		 		<td style="text-align: right;"><?php echo format_currency($total_disc, $currency_icon); ?></td>
         		 		<?php
         		 	else :
         		 		?>
						<td style="text-align: right;">
							<?php
							foreach ($data_discs as $data_disc) :
								if ($data_disc->so_item_kd == $item_data->kd_ditem_so) :
									$view_null = '<li>-</li>';
									$view_diskon = '<li>'.substr($data_disc->nm_pihak, 0, 4).' : '.$data_disc->jml_disc.'%</li>';
									if ($data_disc->view_access == 'view_diskon_distributor' && $_SESSION['view_diskon_distributor']) :
										echo $view_diskon;
									elseif ($data_disc->view_access == 'view_diskon_customer' && $_SESSION['view_diskon_customer']) :
										echo $view_diskon;
									elseif ($data_disc->view_access == 'nothing') :
										echo $view_diskon;
									else :
										echo $view_null;
									endif;
								endif;
							endforeach;
							?>
						</td>
						<?php
					endif;
					?>
					<td style="text-align: right;"><?php echo format_currency($total_harga, $currency_icon); ?></td>
				</tr>
				<?php
				foreach ($details_data as $detail_data) :
					if ($detail_data['ditem_so_kd'] == $item_data->kd_ditem_so) :
						if ($tipe_customer == 'Lokal') :
							$convert_retail = empty($val_retail)?'1':$val_retail;
							$convert_detail = empty($val_detail)?'1':$val_detail;
							if ($type_retail == 'primary') :
								$item_price_retail = $detail_data['harga_retail'] * $convert_detail;
							elseif ($type_retail == 'secondary') :
								$item_price_retail = $detail_data['harga_retail'] / $convert_retail;
							endif;
							if ($type_retail == 'primary') :
								$item_price = $detail_data['harga_barang'] / $convert_detail;
							elseif ($type_retail == 'secondary') :
								$item_price = $detail_data['harga_barang'] * $convert_retail;
							endif;
							$disc_type = $detail_data['disc_type'];
							$disc_val = $detail_data['item_disc'];
							$disc = $item_price_retail - $item_price;
							$item_disc = count_disc($disc_type, $disc_val, $item_price);
							$total_disc = $disc + $item_disc;
							$total_harga = ($item_price_retail - $total_disc) * $detail_data['item_qty'];
						elseif ($tipe_customer == 'Ekspor') :
							$item_price_retail = $detail_data['harga_barang'];
							$disc_type = $detail_data['disc_type'];
							$disc_val = $detail_data['item_disc'];
							$item_disc = count_disc($disc_type, $disc_val, $item_price_retail);
							$tot_def_disc = count_disc('percent', $disc_val, $item_price_retail);
							$total_disc = $item_disc;
							$prices = $item_price_retail - $total_disc;
							$total_harga = $prices * $detail_data['item_qty'];
						endif;
						
						$net = $detail_data['item_qty'] * $detail_data['netweight'];
						$gross = $detail_data['item_qty'] * $detail_data['grossweight'];
						$cbm = $detail_data['item_qty'] * $detail_data['item_cbm'];
						$tot_net[] = $net;
						$tot_gross[] = $gross;
						$tot_cbm[] = $cbm;
						$tot_harga[] = $total_harga;
						$tot_disc[] = $total_disc;
						$tot_price[] = $item_price_retail;
						$tot_qty[] = $detail_data['item_qty'];
						?>
						<tr>
							<td><?php echo $detail_data['item_code']; ?></td>
							<td><?php echo item_stat($detail_data['item_status']); ?></td>
							<td><?php echo $detail_data['item_desc']; ?></td>
							<td><?php echo $detail_data['item_dimension']; ?></td>
							<td style="text-align: center;"><?php echo $detail_data['item_qty']; ?></td>
							<td style="text-align: center;" class="no-print"><?php echo $net; ?></td>
							<td style="text-align: center;" class="no-print"><?php echo $gross; ?></td>
							<td style="text-align: center;" class="no-print"><?php echo $cbm; ?></td>
							<td style="text-align: right;"><?php echo format_currency($item_price_retail, $currency_icon); ?></td>
							<?php
							if ($disc_view) :
								?>
		         		 		<td style="text-align: right;"><?php echo format_currency($total_disc, $currency_icon); ?></td>
		         		 		<?php
		         		 	else :
		         		 		?>
								<td style="text-align: right;">
									<?php
									foreach ($data_discs as $data_disc) :
										if ($data_disc->so_item_kd == $item_data->kd_ditem_so) :
											$view_null = '<li>-</li>';
											$view_diskon = '<li>'.substr($data_disc->nm_pihak, 0, 4).' : '.$data_disc->jml_disc.'%</li>';
											if ($data_disc->view_access == 'view_diskon_distributor' && $_SESSION['view_diskon_distributor']) :
												echo $view_diskon;
											elseif ($data_disc->view_access == 'view_diskon_customer' && $_SESSION['view_diskon_customer']) :
												echo $view_diskon;
											elseif ($data_disc->view_access == 'nothing') :
												echo $view_diskon;
											else :
												echo $view_null;
											endif;
										endif;
									endforeach;
									?>
								</td>
								<?php
							endif;
							?>
							<td style="text-align: right;"><?php echo format_currency($total_harga, $currency_icon); ?></td>
						</tr>
						<?php
					endif;
				endforeach;
			endforeach;
			$tots_pot = array();
			$tots_net = array_sum($tot_net);
			$tots_gross = array_sum($tot_gross);
			$tots_cbm = array_sum($tot_cbm);
			$tots_harga = array_sum($tot_harga);
			$tots_disc = array_sum($tot_disc);
			$tots_price = array_sum($tot_price);
			$tots_qty = array_sum($tot_qty);
			$jml_pot = count($tot_potongan);
			$jml_add = $jml_pot > 0?1:0;
			$jml_rowspan = 5 + $jml_pot + $jml_add;
			$jml_rowspan_det = ($jml_pot > 0)?0:5 + $jml_pot;
			?>
		</tbody>
		<tbody>
			<tr>
				<td colspan="4"></td>
				<td style="text-align: center;"><b>Total Price</b></td>
				<td style="text-align: center;"><?php echo $tots_qty; ?></td>
				<td style="text-align: center;" class="no-print"><?php echo $tots_net; ?></td>
				<td style="text-align: center;" class="no-print"><?php echo $tots_gross; ?></td>
				<td style="text-align: center;" class="no-print"><?php echo $tots_cbm; ?></td>
				<td style="text-align: right;"></td>
				<td style="text-align: right;"></td>
				<?php
				if ($tipe_customer == 'Lokal') :
					?>
					<td style="text-align: right;"><?php echo format_currency($tots_harga, $currency_icon); ?></td>
					<?php
				elseif ($tipe_customer == 'Ekspor') :
					?>
					<td style="text-align: right;"><?php echo format_currency($tots_harga, $currency_icon); ?></td>
					<?php
				endif;
				?>
			</tr>
			<?php
			$no = 0;
			foreach ($tot_potongan as $tot) :
				$no++;
				$tots_pot[] = $tot->total_nilai;
				?>
				<tr>
					<?php if ($no == 1) : ?>
						<td colspan="6" rowspan="<?php echo $jml_rowspan; ?>">
							<?php
							if ($page_type == 'print') :
								if (empty($term_payment)) :
									echo $laporan_footer;
								else :
									echo $term_payment.'<br />'.$footer;
								endif;
							endif;
							?>
						</td>
					<?php endif; ?>
					<td style="text-align: right;" class="no-print"></td>
					<td style="text-align: right;" class="no-print"></td>
					<td style="text-align: right;" class="no-print"></td>
					<td colspan="2"><?php echo $tot->nm_kolom; ?></td>
					<td style="text-align: right;"><?php echo format_currency($tot->total_nilai, $currency_icon); ?></td>
				</tr>
				<?php
				if ($no == $jml_pot) :
					$tot_pot = array_sum($tots_pot);
					$tots_harga = $tots_harga - $tot_pot;
					?>
					<tr>
						<td style="text-align: right;" class="no-print"></td>
						<td style="text-align: right;" class="no-print"></td>
						<td style="text-align: right;" class="no-print"></td>
						<td colspan="2"><b>Total Price</b></td>
						<td style="text-align: right;"><?php echo format_currency($tots_harga, $currency_icon); ?></td>
					</tr>
					<?php
				endif;
			endforeach;
			$rowspan_lagi = $no > 0?'':'rowspan=\'5\'';
			?>
			<tr>
				<?php
				if ($jml_rowspan_det > 0) :
					?>
					<td colspan="6" rowspan="<?php echo $jml_rowspan_det; ?>">
						<?php
						if ($page_type == 'print') :
							if (empty($term_payment)) :
								echo $laporan_footer;
							else :
								echo $term_payment.'<br />'.$footer;
							endif;
						endif;
						?>
					</td>
					<?php
				endif;
				?>
				<td style="text-align: right;" class="no-print"></td>
				<td style="text-align: right;" class="no-print"></td>
				<td style="text-align: right;" class="no-print"></td>
				<td colspan="2">Ongkir</td>
				<td style="text-align: right;">
					<div id="idTextOngkir"><?php echo format_currency($jml_ongkir, $currency_icon); ?></div>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;" class="no-print"></td>
				<td style="text-align: right;" class="no-print"></td>
				<td style="text-align: right;" class="no-print"></td>
				<td colspan="2">Installasi</td>
				<td style="text-align: right;">
					<div id="idTextInstall"><?php echo format_currency($jml_install, $currency_icon); ?></div>
				</td>
			</tr>
			<?php
			$tots_harga = ($tots_harga + $jml_ongkir + $jml_install);
			$tot_ppn = count_disc('percent', $jml_ppn, $tots_harga);
			if (empty($decimal) || $decimal == '0') :
				$tot_ppn_lagi = pembulatan_decimal($tot_ppn);
			elseif ($decimal == '1') :
				$tot_ppn_lagi = $tot_ppn;
			endif;
			?>
			<tr>
				<td style="text-align: right;" class="no-print"></td>
				<td style="text-align: right;" class="no-print"></td>
				<td style="text-align: right;" class="no-print"></td>
				<td colspan="2"><b>Total Price</b></td>
				<td style="text-align: right;"><div id="idTextPpn"><?php echo format_currency($tots_harga, $currency_icon); ?></div></td>
			</tr>
			<tr>
				<td style="text-align: right;" class="no-print"></td>
				<td style="text-align: right;" class="no-print"></td>
				<td style="text-align: right;" class="no-print"></td>
				<td colspan="2"><?php echo $nm_kolom_ppn; ?></td>
				<td style="text-align: right;"><div id="idTextPpn"><?php echo format_currency($tot_ppn_lagi, $currency_icon); ?></div></td>
			</tr>
			<?php $grand_tot = $tots_harga + $tot_ppn_lagi; ?>
			<tr>
				<td style="text-align: right;" class="no-print"></td>
				<td style="text-align: right;" class="no-print"></td>
				<td style="text-align: right;" class="no-print"></td>
				<td colspan="2"><b>Grand Total</b></td>
				<td style="text-align: right;"><div id="idTextGrandTot"><?php echo format_currency($grand_tot, $currency_icon); ?></div></td>
			</tr>
		</tbody>
		<?php
	endif;
	?>
</table>