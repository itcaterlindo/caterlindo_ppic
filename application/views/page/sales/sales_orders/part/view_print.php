<?php
defined('BASEPATH') or exit('No direct script access allowed!');
extract($format_laporan);
?>
<style type="text/css">
	body {
		font-size: 12px;
	}
	img {
		width: 100% !important;
	}
	table>tbody>tr>td {
		padding:3px;
	}
	table>tfoot>tr>td {
		padding:3px;
	}
	
	/* @media print {
		body {transform: scale(80%);}
		.print {
			page-break-after: always;
		}
		.print-foot {
			page-break-inside: avoid;
		}
		.div-footer {
			position: fixed;
			bottom: 0;
		}
		.print-left {
			text-align: left;
		}
		.print-center {
			text-align: center;
		}
		.print-right {
			text-align: right;
		}
	} */
}
</style>
<div class="row">
	<div class="col-xs-12">
		<a href="javascript:void(0);" class="btn btn-danger no-print" onclick="window.close();" style="margin-left: 25px;">
			<i class="fa fa-ban"></i> Close
		</a>
		<a href="javascript:void(0);" class="btn btn-primary no-print" onclick="window.print();" style="margin-left: 25px;">
			<i class="fa fa-print"></i> Cetak Data
		</a>
	</div>
</div>
<div class="row invoice print">
	<!-- title row -->
	<div class="row">
		<div class="col-xs-12">
			<h2 class="page-header" style="margin-top: 5px;margin-bottom: 5px;">
				<?php echo $laporan_title; ?>
			</h2>
		</div><!-- /.col -->
	</div>
	<?php $this->load->view('page/'.$class_link.'/part/master_head', $master_head); ?>
	<?php $this->load->view('page/'.$class_link.'/part/item_detail', $item_detail); ?>
	<?php $this->load->view('page/'.$class_link.'/part/so_note', $format_laporan); ?>
</div>