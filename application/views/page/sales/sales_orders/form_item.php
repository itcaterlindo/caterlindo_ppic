<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$color = color_status($status_so);
?>

<div class="box box-warning" id="idBoxFormSalesOrder">
	<div class="box-header with-border">
		<h3 id="idTitleTable" class="box-title">Form Sales Order Items</h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool" id="idBtnHideBoxForm" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
				<i class="fa fa-minus"></i>
			</button>
			<button class="btn btn-box-tool" id="idBtnCloseBox" data-toggle="tooltip" title="Tutup">
				<i class="fa fa-times"></i>
			</button>
		</div>
	</div>
	<div class="box-body">
		<div class="row invoice-info">
			<div class="col-sm-4 invoice-col">
				From
				<address>
					<strong><?php echo $nm_salesperson; ?></strong><br>
					Jln Industri No.18, Trosobo<br>
					Taman, Sidoarjo Regency, East Java 61262<br>
					Phone: <?php echo $telp_sales; ?><br>
					Email: <?php echo $email_sales; ?>
				</address>
			</div>
			<div class="col-sm-4 invoice-col">
				To
				<address>
					<strong><?php echo $nm_customer; ?></strong><br>
					<?php echo $alamat_satu_customer; ?><br>
					<?php echo $alamat_dua_customer; ?><br>
					Phone: <?php echo $telp_customer; ?><br>
					Email: <?php echo $email_customer; ?>
				</address>
			</div>
			<div class="col-sm-4 invoice-col">
				<b>Sales Order No. #<?php echo $no_salesorder; ?></b><br>
				<b>Sales Order Status: <?php echo bg_label(format_words($status_so, '_', ' ', 'ucwords'), $color); ?></b>
			</div>
		</div>
		<hr />
		<div id="idErrFormItemSales"></div>
		<?php echo form_open('', array('id' => 'idForm', 'class' => 'form-horizontal')); ?>
			<input type="hidden" name="txtorder_tipe" id="idtxtorder_tipe" value="<?php echo isset($order_tipe) ? $order_tipe : 'so'; ?>">
			<div id="idKeterangan" class="col-md-12" style="display: none;">
				<div class="form-group">
					<label class="col-md-2 control-label">Parent Product Code : </label>
					<div id="idParentCode" class="col-md-8" style="margin-top: 7px;"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="idTxtProductCode" class="col-md-4 control-label">Product Code</label>
						<div class="col-md-6">
							<div id="idErrProductCode"></div>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtFormType', 'id' => 'idTxtFormType', 'value' => 'input')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtKdMSales', 'id' => 'idTxtKdMSales', 'value' => $kd_msalesorder)); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtNoSo', 'id' => 'idTxtNoSo', 'value' => $no_salesorder)); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtNoSalesOrder', 'id' => 'idTxtNoSalesOrder', 'value' => $no_master_so)); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtStatusSo', 'id' => 'idTxtStatusSo', 'value' => $status_so)); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtDecimal', 'id' => 'idTxtDecimal', 'value' => $decimal)); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtKdDSales', 'id' => 'idTxtKdDSales')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtKdParentSales', 'id' => 'idTxtKdParentSales')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtKdParent', 'id' => 'idTxtKdParent')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtRelation', 'id' => 'idTxtRelation', 'value' => 'parent')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtPriceCategory', 'id' => 'idTxtPriceCategory', 'value' => strtolower(str_replace(' ', '_', $price_category)))); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtKdBarang', 'id' => 'idTxtKdBarang')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtKdBarangStandart', 'id' => 'idTxtKdBarangStandart')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtKdBarangCustom', 'id' => 'idTxtKdBarangCustom')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtProductCodeStd', 'id' => 'idTxtProductCodeStd')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtProductCodeCustom', 'id' => 'idTxtProductCodeCustom')); ?>
							<div id="scrollable-dropdown-menu">
								<?php echo form_input(array('name' => 'txtProductCode', 'id' => 'idTxtProductCode', 'class' => 'form-control', 'placeholder' => 'Product Code')); ?>
							</div>
							<?php echo form_dropdown('selProductChild', array('' => '-- Pilih Child --'), '', array('id' => 'idSelProductChild', 'class' => 'form-control', 'style' => 'display:none;')); ?>
							<?php echo form_input(array('name' => 'txtProductCodeChild', 'id' => 'idTxtProductCodeChild', 'class' => 'form-control', 'placeholder' => 'Product Code', 'style' => 'display:none;margin-top:5px;')); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="idTxtBarcode" class="col-md-4 control-label">Barcode</label>
						<div class="col-md-4">
							<div id="idErrBarcode"></div>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtBarcodeStd', 'id' => 'idTxtBarcodeStd')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtBarcodeCustom', 'id' => 'idTxtBarcodeCustom', 'value' => $custom_barcode)); ?>
							<?php echo form_input(array('name' => 'txtBarcode', 'id' => 'idTxtBarcode', 'class' => 'form-control', 'placeholder' => 'Barcode', 'readonly' => 'readonly')); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="idSelItemStatus" class="col-md-4 control-label">Status</label>
						<div class="col-md-4">
							<div id="idErrItemStatus"></div>
							<?php echo form_dropdown('selItemStatus', array('std' => 'Standard', 'custom' => 'Custom'), 'std', array('id' => 'idSelItemStatus', 'class' => 'form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="idTxtItemDesc" class="col-md-4 control-label">Description</label>
						<div class="col-md-6">
							<div id="idErrItemDesc"></div>
							<div id="idTextDesc" style="padding-top:6px;">-</div>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtItemDescOri', 'id' => 'idTxtItemDescOri')); ?>
							<?php echo form_textarea(array('name' => 'txtItemDesc', 'id' => 'idTxtItemDesc', 'class' => 'form-control', 'placeholder' => 'Description', 'rows' => '3', 'style' => 'display:none;')); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="idTxtItemDimension" class="col-md-4 control-label">Dimension (LxDxH) (mm)</label>
						<div class="col-md-6">
							<div id="idErrItemDimension"></div>
							<div id="idTextDimension" style="padding-top:6px;">-</div>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtItemDimensionOri', 'id' => 'idTxtItemDimensionOri')); ?>
							<?php echo form_textarea(array('name' => 'txtItemDimension', 'id' => 'idTxtItemDimension', 'class' => 'form-control', 'placeholder' => 'Dimension', 'rows' => '3', 'style' => 'display:none;')); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="idTxtItemNotes" class="col-md-4 control-label">Notes</label>
						<div class="col-md-6">
							<div id="idErrItemNotes"></div>
							<?php echo form_textarea(array('name' => 'txtProductNote', 'id' => 'idTxtProductNote', 'class' => 'form-control', 'placeholder' => 'Notes', 'rows' => '3')); ?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="idTxtProductPrice" class="col-md-4 control-label">Price</label>
						<div class="col-md-4">
							<div id="idErrProductPrice"></div>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtProductPriceRetail', 'id' => 'idTxtProductPriceRetail')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtPriceStd', 'id' => 'idTxtPriceStd')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtPriceCustom', 'id' => 'idTxtPriceCustom')); ?>
							<?php echo form_input(array('name' => 'txtProductPrice', 'id' => 'idTxtProductPrice', 'class' => 'form-control hitung', 'placeholder' => 'Price', 'readonly' => '')); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="idSelDiscType" class="col-md-4 control-label">Disc Type</label>
						<div class="col-md-4">
							<div id="idErrDiscType"></div>
							<?php echo form_dropdown('selDiscType', array('percent' => 'Persen', 'decimal' => 'Harga'), 'decimal', array('id' => 'idSelDiscType', 'class' => 'form-control hitung')); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="idTxtProductDisc" class="col-md-4 control-label">Disc</label>
						<div class="col-md-4">
							<div id="idErrProductDisc"></div>
							<?php echo form_input(array('name' => 'txtProductDisc', 'id' => 'idTxtProductDisc', 'class' => 'form-control hitung', 'value' => '0', 'placeholder' => 'Disc')); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="idTxtProductQty" class="col-md-4 control-label">Qty</label>
						<div class="col-md-2">
							<div id="idErrProductQty"></div>
							<?php echo form_input(array('name' => 'txtProductQty', 'id' => 'idTxtProductQty', 'class' => 'form-control hitung', 'placeholder' => 'Qty')); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="idTxtProductTotPrice" class="col-md-4 control-label">Total Price</label>
						<div class="col-md-4">
							<div id="idErrProductTotPrice"></div>
							<?php echo form_input(array('name' => 'txtProductTotPrice', 'id' => 'idTxtProductTotPrice', 'class' => 'form-control', 'placeholder' => 'Total Price', 'readonly' => '')); ?>
						</div>
					</div>
					<div id="idFormDiscDefault"></div>
				</div>
			</div>
			<hr class="id-form-item-detail" style="display: none;">
			<div class="row id-form-item-detail" style="display: none;">
				<div class="col-md-3">
					<div class="form-group">
						<label for="idTxtGrossweight" class="col-md-4 control-label">Grossweight</label>
						<div class="col-md-8">
							<div id="idErrGrossweight"></div>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtGrossweightStd', 'id' => 'idTxtGrossweightStd')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtGrossweightCustom', 'id' => 'idTxtGrossweightCustom')); ?>
							<?php echo form_input(array('name' => 'txtGrossweight', 'id' => 'idTxtGrossweight', 'class' => 'form-control hitung_netweight', 'placeholder' => 'Grossweight')); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="idTxtBoxweight" class="col-md-4 control-label">Boxweight</label>
						<div class="col-md-8">
							<div id="idErrBoxweight"></div>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtBoxweightStd', 'id' => 'idTxtBoxweightStd')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtBoxweightCustom', 'id' => 'idTxtBoxweightCustom')); ?>
							<?php echo form_input(array('name' => 'txtBoxweight', 'id' => 'idTxtBoxweight', 'class' => 'form-control hitung_netweight', 'placeholder' => 'Boxweight')); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="idTxtNetweight" class="col-md-4 control-label">Netweight</label>
						<div class="col-md-8">
							<div id="idErrNetweight"></div>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtNetweightStd', 'id' => 'idTxtNetweightStd')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtNetweightCustom', 'id' => 'idTxtNetweightCustom')); ?>
							<?php echo form_input(array('name' => 'txtNetweight', 'id' => 'idTxtNetweight', 'class' => 'form-control', 'placeholder' => 'Netweight', 'readonly' => 'readonly')); ?>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="idTxtLength" class="col-md-4 control-label">Length (cm)</label>
						<div class="col-md-8">
							<div id="idErrLength"></div>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtLengthStd', 'id' => 'idTxtLengthStd')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtLengthCustom', 'id' => 'idTxtLengthCustom')); ?>
							<?php echo form_input(array('name' => 'txtLength', 'id' => 'idTxtLength', 'class' => 'form-control hitung_cbm', 'placeholder' => 'Length (cm)')); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="idTxtWidth" class="col-md-4 control-label">Width (cm)</label>
						<div class="col-md-8">
							<div id="idErrWidth"></div>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtWidthStd', 'id' => 'idTxtWidthStd')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtWidthCustom', 'id' => 'idTxtWidthCustom')); ?>
							<?php echo form_input(array('name' => 'txtWidth', 'id' => 'idTxtWidth', 'class' => 'form-control hitung_cbm', 'placeholder' => 'Width (cm)')); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="idTxtHeight" class="col-md-4 control-label">Height (cm)</label>
						<div class="col-md-8">
							<div id="idErrHeight"></div>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtHeightStd', 'id' => 'idTxtHeightStd')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtHeightCustom', 'id' => 'idTxtHeightCustom')); ?>
							<?php echo form_input(array('name' => 'txtHeight', 'id' => 'idTxtHeight', 'class' => 'form-control hitung_cbm', 'placeholder' => 'Height (cm)')); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="idTxtItemCbm" class="col-md-4 control-label">Item CBM</label>
						<div class="col-md-8">
							<div id="idErrItemCbm"></div>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtItemCbmStd', 'id' => 'idTxtItemCbmStd')); ?>
							<?php echo form_input(array('type' => 'hidden', 'name' => 'txtItemCbmCustom', 'id' => 'idTxtItemCbmCustom')); ?>
							<?php echo form_input(array('name' => 'txtItemCbm', 'id' => 'idTxtItemCbm', 'class' => 'form-control', 'placeholder' => 'Item CBM', 'readonly' => 'readonly')); ?>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="col-md-12">
				<div class="form-group">
					<button type="submit" name="btnSubmit" class="btn btn-info pull-right">
						<i class="fa fa-shopping-cart"></i> Input Barang
					</button>
					<a href="javascript:void(0);" name="btnStopChild" id="idBtnStopChild" class="btn btn-warning pull-right" onclick="stopChildData();" style="margin-right: 5px;display: none;">
						<i class="fa fa-sitemap"></i> Finish Child
					</a>
					<a href="javascript:void(0);" name="btnCancelEdit" id="idBtnCancelEdit" class="btn btn-danger pull-right" onclick="cancelEdit();" style="margin-right: 5px;display: none;">
						<i class="fa fa-ban"></i> Cancel Edit
					</a>
				</div>
			</div>
		<?php echo form_close(); ?>

		<hr />
		<div id="idDataDetail"></div>
	</div>
	<div class="box-footer">
		<a href="javascript:void(0);" id="idBtnTutupForm" class="btn btn-primary pull-right">
			<i class="fa fa-save"></i> Simpan Sales Order
		</a>
	</div>
	<div class="overlay" id="idOverlayForm" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>

	<!-- Modal Item Custom Input -->
	<div class="modal fade" id="modal-item-custom">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Input Custom Item</h4>
				</div>
				<div class="modal-body">
					<p>One fine body&hellip;</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<?php $data['class_link'] = $class_link; ?>
	<?php $data['kd_msalesorder'] = $kd_msalesorder; ?>
	<?php $data['jenis_customer_kd'] = $jenis_customer_kd; ?>
	<?php $data['form_error'] = array('idErrProductCode', 'idErrItemStatus', 'idErrItemDesc', 'idErrItemDimension', 'idErrItemNotes', 'idErrProductPrice', 'idErrDiscType', 'idErrProductDisc', 'idErrProductQty', 'idErrProductTotPrice', 'idErrBarcode'); ?>
	<?php $this->load->view('script/'.$class_link.'/form_item_js', $data); ?>
</div>