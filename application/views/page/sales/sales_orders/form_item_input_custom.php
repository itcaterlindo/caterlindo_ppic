<div class="box box-info" id="idBoxForm">
    <form action="" id="idFormInputCustomItem" class="form-horizontal FormInputCustomItem" method="post" accept-charset="utf-8">
    <input type="hidden" name="txtCustomKdsalesorder" value="<?= $data_detail->msalesorder_kd ?>" id="idTxtCustomKdsalesorder" class="form-control">
    <input type="hidden" name="txtCustomRelation" value="<?= $relation ?>" id="idTxtCustomRelation" class="form-control">
    <input type="hidden" name="txtCustomKdDetail" value="<?= $id ?>" id="idTxtCustomKdDetail" class="form-control">
        <div class="box-body">
            <div id="idBoxFormBody">
            <div id="idAlertForm"></div>
                <div class="form-group">
                    <label for="idTxtBarcode" class="col-md-2 control-label">Barcode Product</label>
                    <div class="col-md-4">
                        <div id="idErrCustomBarcode"></div>
                        <input type="text" name="txtCustomBarcode" value="<?= $data_project->project_nopj ?>" id="idTxtCustomBarcode" class="form-control" placeholder="Barcode Product" readonly>
                    </div>
                </div>

                <div class="form-group">
                    <label for="idTxtCustomCode" class="col-md-2 control-label">No Project - Code Product</label>
                    <div class="col-md-2">
                        <input type="text" name="txtCustomNoProject" value="<?= $data_project->project_nopj ?>" id="idTxtCustomNoProject" class="form-control" placeholder="No. Project" readonly>
                    </div>
                    <div class="col-md-2">
                        <div id="idErrCustomCode"></div>
                        <input type="text" name="txtCustomCode" value="<?= $data_detail->item_code ?>" id="idTxtCustomCode" class="form-control" placeholder="Code Product">
                    </div>
                </div>

                <div class="form-group">
                    <label for="idTxtBarcode" class="col-md-2 control-label">Tarif (HS Code)</label>
                    <div class="col-md-4">
                        <div id="idErrCustomTarif"></div>
                        <input type="text" name="txtCustomTarif" value="0" id="idTxtCustomTarif" class="form-control" placeholder="Tarif (HS Code)">
                    </div>
                </div>

                <div class="form-group">
                    <label for="idTxtCustomGroup" class="col-md-2 control-label">Group Product</label>
                    <div class="col-md-4">
                        <div id="idErrCustomGroup"></div>
                        <select name="selCustomGroup" id="idSelCustomGroup" class="form-control select2" tabindex="-1" aria-hidden="true">
                            <option value="" selected="selected">-- Pilih Group Product --</option>
                            <?php foreach($group_barang as $group): ?>
                            <option value="<?= $group->kd_group_barang ?>"><?= $group->nm_group ?></option>
                            <?php endforeach; ?>
                        </select>                    
                    </div>
                </div>

                <div class="form-group">
                    <label for="idTxtBarcode" class="col-md-2 control-label">Category Product</label>
                    <div class="col-md-4">
                        <div id="idErrCustomCategory"></div>
                        <select name="selCustomCategory" id="idSelCustomCategory" class="form-control select2" tabindex="-1" aria-hidden="true">
                            <option value="" selected="selected">-- Pilih Category Product --</option>
                            <?php foreach($category_barang as $category): ?>
                                <option value="<?= $category->kd_kat_barang ?>"><?= $category->nm_kat_barang ?></option>
                            <?php endforeach; ?>
                        </select>                    
                    </div>
                </div>

                <div class="form-group">
                    <label for="idTxtBarcode" class="col-md-2 control-label">Item Group (SAP)</label>
                    <div class="col-md-4">
                        <div id="idErrCustomItemGroup"></div>
                        <select name="selCustomItemGroup" id="idSelCustomItemGroup" class="form-control select2" tabindex="-1" aria-hidden="true">
                            <option value="" selected="selected">-- Pilih Item Group (SAP) --</option>
                            <?php foreach($item_group_sap as $item_group): ?>
                            <option value="<?= $item_group->item_group_kd ?>"><?= $item_group->item_group_name ?></option>
                            <?php endforeach; ?>
                        </select>                    
                    </div>
                </div>

                <div class="form-group"><label for="idTxtGrossweight" class="col-md-2 control-label">Product Grossweight</label>
                    <div class="col-md-3">
                        <div id="idErrCustomGrossweight"></div>
                        <input type="text" name="txtCustomGrossweight" value="<?= $data_detail->grossweight ?>" id="idTxtCustomGrossweight" class="form-control hitung_netweight" placeholder="Product Grossweight">
                    </div>
                    <label for="idTxtCustomBoxweight" class="col-md-2 control-label">Product Box Weight</label>
                    <div class="col-md-3">
                        <div id="idErrCustomBoxweight"></div>
                        <input type="text" name="txtCustomBoxweight" value="<?= $data_detail->boxweight ?>" id="idTxtCustomBoxweight" class="form-control hitung_netweight" placeholder="Product Box Weight">
                    </div>
                </div>

                <div class="form-group">
                    <label for="idTxtBarcode" class="col-md-2 control-label">Inventory Item</label>
                    <div class="col-md-4">
                        <div id="idErrCustomInventoryItem"></div>
                        <select name="selCustomInventItem" id="idSelCustomInventItem" class="form-control select2" tabindex="-1" aria-hidden="true">
                            <option value="Y">Ya</option>
                            <option value="N">Tidak</option>
                        </select>                    
                    </div>
                </div>

                <div class="form-group">
                    <label for="idTxtBarcode" class="col-md-2 control-label">Product Nettweight</label>
                    <div class="col-md-4">
                        <div id="idErrBarcode"></div>
                        <input type="text" name="txtCustomNettweight" value="<?= $data_detail->netweight ?>" id="idTxtCustomNettweight" class="form-control" placeholder="Nettweight Product">
                    </div>
                </div>

                <div class="form-group"><label for="idTxtGrossweight" class="col-md-2 control-label">Product Length (cm)</label>
                    <div class="col-md-3">
                        <div id="idErrGrossweight"></div>
                        <input type="text" name="txtCustomLength" value="<?= $data_detail->length_cm ?>" id="idTxtCustomLength" class="form-control hitung_netweight" placeholder="Product Grossweight">
                    </div>
                    <label for="idTxtBoxweight" class="col-md-2 control-label">Product Width (cm)</label>
                    <div class="col-md-3">
                        <div id="idErrCustomBoxweight"></div>
                        <input type="text" name="txtCustomwidth" value="<?= $data_detail->width_cm ?>" id="idTxtCustomWidth" class="form-control hitung_netweight" placeholder="Product Box Width">
                    </div>
                </div>

                <div class="form-group"><label for="idTxtGrossweight" class="col-md-2 control-label">Product Height (cm)</label>
                    <div class="col-md-3">
                        <div id="idErrCustomGrossweight"></div>
                        <input type="text" name="txtCustomHeight" value="<?= $data_detail->height_cm ?>" id="idTxtCustomHeight" class="form-control hitung_netweight" placeholder="Product Height">
                    </div>
                    <label for="idTxtBoxweight" class="col-md-2 control-label">Product CBM (Cubic Meters)</label>
                    <div class="col-md-3">
                        <div id="idErrCustomBoxweight"></div>
                        <input type="text" name="txtCustomCBM" value="<?= $data_detail->item_cbm ?>" id="idTxtCustomCBM" class="form-control hitung_netweight" placeholder="Product CBM">
                    </div>
                </div>

                <div class="form-group"><label for="idTxtGrossweight" class="col-md-2 control-label">Stock Max</label>
                    <div class="col-md-3">
                        <div id="idErrGrossweight"></div>
                        <input type="text" name="txtCustomStockMax" value="1" id="idTxtCustomStockMax" class="form-control hitung_netweight" placeholder="Stock Max">
                    </div>
                    <label for="idTxtBoxweight" class="col-md-2 control-label">Stock Min</label>
                    <div class="col-md-3">
                        <div id="idErrBoxweight"></div>
                        <input type="text" name="txtCustomStockMin" value="1" id="idTxtCustomStockMin" class="form-control hitung_netweight" placeholder="Stock Min">
                    </div>
                </div>

                <div class="form-group">
                    <label for="idTxtBarcode" class="col-md-2 control-label">Stock Safety</label>
                    <div class="col-md-4">
                        <div id="idErrBarcode"></div>
                        <input type="text" name="txtCustomStockSafety" value="1" id="idTxtCustomStockSafety" class="form-control" placeholder="Stock Safety">
                    </div>
                </div>

                <div class="form-group">
                    <label for="idTxtDesc" class="col-md-2 control-label">Product Description</label>
                    <div class="col-md-5">
                        <div id="idErrCustomDesc"></div>
                        <textarea name="txtCustomDesc" cols="40" rows="5" id="idTxtCustomDesc" class="form-control" placeholder="Product Description"><?= $data_detail->item_desc ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="idTxtDimension" class="col-md-2 control-label">Product Dimension</label>
                    <div class="col-md-5">
                        <div id="idErrDimension"></div>
                        <textarea name="txtCustomProductDimension" cols="40" rows="5" id="idTxtCustomProductDimension" class="form-control" placeholder="Product Dimension"><?= $data_detail->item_dimension ?></textarea>
                    </div>
                </div>
                                
                <div class="form-group">
                    <label for="idtxtCustomHargaDistributor" class="col-md-2 control-label">Harga Distributor</label>
                    <div class="col-md-4">
                        <div id="idErrHargaDistributor"></div>
                        <input type="text" name="txtCustomHargaDistributor" value="0" id="idtxtCustomHargaDistributor" class="form-control" placeholder="Harga Distributor">
                    </div>
                </div>

                <div class="form-group">
                    <label for="idtxtCustomHargaRetail" class="col-md-2 control-label">Harga Retail</label>
                    <div class="col-md-4">
                        <div id="idErrHargaRetail"></div>
                        <input type="text" name="txtCustomHargaRetail" value="0" id="idtxtCustomHargaRetail" class="form-control" placeholder="Harga Retail">
                    </div>
                </div>

                <div class="form-group">
                    <label for="idtxtCustomHargaReseller" class="col-md-2 control-label">Harga Reseller</label>
                    <div class="col-md-4">
                        <div id="idErrHargaReseller"></div>
                        <input type="text" name="txtCustomHargaReseller" value="0" id="idtxtCustomHargaReseller" class="form-control" placeholder="Harga Reseller">
                    </div>
                </div>

                <div class="form-group">
                    <label for="idtxtCustomHargaEkspor" class="col-md-2 control-label">Harga Ekspor</label>
                    <div class="col-md-4">
                        <div id="idErrHargaEkspor"></div>
                        <input type="text" name="txtCustomHargaEkspor" value="0" id="idtxtCustomHargaEkspor" class="form-control" placeholder="Harga Ekspor">
                    </div>
                </div>

                <button type="submit" name="btnSubmit" id="idBtnSubmit" class="btn btn-primary pull-right">Simpan <i class="fa fa-save"></i></button>
            </div>
        </div>
    </form>
</div>