<?php
defined('BASEPATH') or exit('No direct script access allowed!');
extract($master_data);
$angka = $jml_dp + $jml_ppn_dp;
$data['class_link'] = $class_link;
$data['kd_msalesorder'] = $kd_msalesorder;
$data['no_dps'] = $no_dps;
?>

<div class="box <?php echo 'box-success'; ?>" id="idBoxForm">
	<div class="box-header with-border">
		<h3 class="box-title">Form DP untuk Salesorder : <?php echo $no_salesorder; ?></h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool btn-collapse" id="idBtnBoxFormSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
				<i class="fa fa-minus"></i>
			</button>
			<button class="btn btn-box-tool btn-remove" id="idBtnBoxFormClose" data-widget="remove" data-toggle="tooltip" title="Close">
				<i class="fa fa-times"></i>
			</button>
		</div>
	</div>

	<div class="box-body">
		<div id="idBoxFormLoader" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="idBoxFormContent" style="display: none;">
			<?php
			echo form_open('', array('id' => 'idForm', 'class' => 'form-horizontal'));
			?>
			<div id="idErrForm"></div>
			<?php
			if (!empty($no_invoice_dp)) :
				?>
				<div class="form-group">
					<label for="idTxtNoInvoiceDp" class="col-md-1 control-label">No. Invoice</label>
					<div class="col-md-3">
						<div id="idErrNoInvoiceDp"></div>
						<?php
						echo form_input(array('name' => 'txtNoInvoiceDp', 'id' => 'idTxtNoInvoiceDp', 'class' => 'form-control', 'value' => $no_invoice_dp, 'placeholder' => 'No. Invoice'));
						?>
					</div>
					<div class="col-md-1">
						<button type="button" id="idBtnHapusDp" class="btn btn-danger" title="Hapus No. Invoice" onclick="return confirm('Anda akan menghapus Invoice DP, Yakin?')?hapus_dp():false">
							<i class="fa fa-trash"></i>
						</button>
					</div>
				</div>
				<?php
			else :
				echo form_input(array('type' => 'hidden', 'name' => 'txtNoInvoiceDp', 'value' => $no_invoice_dp));
			endif;
			?>
			<div class="form-group">
				<label for="idtxtTglDp" class="col-md-1 control-label">Tanggal</label>
				<div class="col-md-2">
					<?php echo form_input(array('type' => 'text', 'id' => 'idtxtTglDp' , 'name' => 'txtTglDp', 'class' => 'form-control datetimepicker', 'value' => !empty($tgl_dp) ? format_date($tgl_dp, 'd-m-Y') : date('d-m-Y'))); ?>
				</div>
				<label for="idTxtJmlDp" class="col-md-1 control-label">Jumlah DP</label>
				<div class="col-md-3">
					<div id="idErrDp"></div>
					<?php
					echo form_input(array('type' => 'hidden', 'name' => 'txtKd', 'id' => 'idTxtKd', 'value' => $kd_msalesorder));
					echo form_input(array('name' => 'txtJmlDp', 'id' => 'idTxtJmlDp', 'class' => 'form-control', 'value' => $angka, 'placeholder' => 'Jumlah DP'));
					?>
				</div>
			</div>
			<div class="box-footer">
				<div class="col-xs-4 pull-left">
					<button type="submit" name="btnSubmit" id="idBtnSubmit" class="btn btn-info pull-right" style="margin-right: 5px;"">
						<i class="fa fa-save"></i> Input DP
					</button>
				</div>
			</div>
			<?php
			echo form_close();
			?>
		</div>
	</div>
	<div class="overlay" id="idBoxFormOverlay" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
	
	<?php $this->load->view('page/'.$class_link.'/form_dp_js', $data); ?>
</div>