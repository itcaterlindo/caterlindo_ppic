<?php
defined('BASEPATH') or exit('No direct script access allowed!');
extract($master_data);
$angka = $jml_dp + $jml_ppn_dp;
$data['class_link'] = $class_link;
$data['kd_msalesorder'] = $kd_msalesorder;
$data['no_dps'] = $no_dps;
?>

<div class="box <?php echo 'box-success'; ?>" id="idBoxForm">
	<div class="box-header with-border">
		<h3 class="box-title">List DP untuk Salesorder : <?php echo $no_salesorder; ?></h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool btn-collapse" id="idBtnBoxFormSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
				<i class="fa fa-minus"></i>
			</button>
			<button class="btn btn-box-tool btn-remove" id="idBtnBoxFormClose" data-widget="remove" data-toggle="tooltip" title="Close">
				<i class="fa fa-times"></i>
			</button>
		</div>
	</div>

	<div class="box-body">
		<div id="idBoxFormLoader" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="idBoxFormContent" style="display: none;">
			<!-- Content -->
			<table class="table">
				<thead>
					<tr>
						<th>No. Invoice</th>
						<th>Tanggal DP</th>
						<th>Jumlah</th>
						<th>Push SAP</th>
						<th>#</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?= $no_invoice_dp ?></td>
						<td><?= format_date($tgl_dp, "d-m-Y") ?></td>
						<td><?= number_format($jml_dp + $jml_ppn_dp, 0, ',', '.') ?></td>
						<td><?= $status_dp == "Y" ? bg_label("Sudah", "green") : bg_label("Belum", "red")  ?></td>
						<td>
							<?php if($status_dp == "T"): ?>
								<button onClick="pushSap('utama', '<?= $kd_msalesorder ?>')" type="button" class="btn btn-primary btn-sm btn-push-sap">
									<i class="fa fa-download"></i> Push SAP
								</button>
							<?php endif; ?>
						</td>
					</tr>
					<?php foreach($data['no_dps'] as $row): ?>
						<tr>
							<td><?= $row->no_invoice ?></td>
							<td><?= format_date($row->tgl_termin, "d-m-Y") ?></td>
							<td><?= number_format($row->jml_termin + $row->jml_termin_ppn, 0, ',', '.') ?></td>
							<td><?= $row->status_dp == "Y" ? bg_label("Sudah", "green") : bg_label("Belum", "red") ?></td>
							<td>
								<?php if($row->status_dp == "T"): ?>
									<button onClick="pushSap('termin', '<?= $row->kd_so_termin ?>')" type="button" class="btn btn-primary btn-sm btn-push-sap">
										<i class="fa fa-download"></i> Push SAP
									</button>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<!-- Contennya disini -->
		</div>
	</div>
	<div class="overlay" id="idBoxFormOverlay" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
	
	<?php $this->load->view('page/'.$class_link.'/list_dp_js', $data); ?>
</div>