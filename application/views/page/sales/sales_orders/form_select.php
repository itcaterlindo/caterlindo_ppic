<?php
defined('BASEPATH') or exit('No direct script access allowed!');

echo form_open('', array('id' => 'idForm', 'class' => 'form-horizontal'));
?>
<div class="form-group">
	<label for="idSelSales" class="col-md-2 control-label">Pilih SO</label>
	<div class="col-md-3">
		<div id="idErrSales"></div>
		<?php echo form_dropdown('selSales', array('' => '-- Pilih SO --', 'po' => 'Buat Baru dari PO', 'quo' => 'Buat Baru dari Quotation'), '', array('id' => 'idSelSales', 'class' => 'form-control')); ?>
	</div>
</div>
<div class="box-footer">
	<div class="col-xs-4 pull-left">
		<button type="submit" name="btnSubmit" class="btn btn-info pull-right" style="margin-right: 5px;"">
			<i class="fa fa-arrow-right"></i> Next
		</button>
	</div>
</div>
<?php echo form_close(); ?>
<?php $this->load->view('script/'.$class_link.'/form_select_js'); ?>