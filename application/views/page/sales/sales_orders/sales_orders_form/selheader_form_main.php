<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataSO';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
$no = 0;
$colors = array('primary', 'danger', 'success', 'default', 'warning');
foreach ($headers as $row) :
	$no++;
	$kecamatan = $row->nm_kecamatan.after_before_char($row->nm_kecamatan, array($row->nm_kota, $row->nm_provinsi, $row->nm_negara), ', ', '.');
	$kota = $row->nm_kota.after_before_char($row->nm_kota, array($row->nm_provinsi, $row->nm_negara), ', ', '.');
	$provinsi = $row->nm_provinsi.after_before_char($row->nm_provinsi, $row->nm_negara, ', ', '.');
	$negara = $row->nm_negara.after_before_char($row->nm_negara, $row->nm_negara, '.', '.');
	$alamat_satu = ucwords(strtolower($row->alamat));
	$alamat_dua = $kecamatan.$kota.$provinsi.$negara;
	$telp_customer = !empty($row->no_telp_utama)?$row->no_telp_utama:$row->no_telp_lain;
	$email_customer = $row->email;
	$color = color_status($row->status_quotation);
	$word = process_status($row->status_quotation);
	$in = $no == 1?'in':'';
	$rand_colors = array_rand($colors);
	?>
	<div class="box-group">
		<div class="form-group">
			<div class="col-md-2 col-xs-12">
				<label>
					<?php echo form_input(array('type' => 'hidden', 'name' => 'txtKdMaster[]', 'value' => $row->kd_mquotation)); ?>
					<input type="radio" name="radioHeader" class="minimal icheck check-header" value="<?php echo $row->kd_mquotation; ?>">
					Pilih Sebagai Header
				</label>
			</div>
			<div class="col-sm-1 col-sm-offset-2 col-xs-12 btn-to-submit" style="display: none;">
				<button type="submit" name="btnSubmitHeader" class="btn btn-primary btn-flat">
					<i class="fa fa-save"></i> Submit
				</button>
			</div>
		</div>
		<div class="box-group" id="accordion">
			<!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
			<div class="panel box box-<?php echo $colors[$rand_colors]; ?>">
				<div class="box-header with-border">
					<h4 class="box-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $row->kd_mquotation; ?>">
						No. Quotation : <?php echo $row->no_quotation; ?>
					</a>
					</h4>
				</div>
				<div id="<?php echo $row->kd_mquotation; ?>" class="panel-collapse collapse <?php echo $in; ?>">
					<div class="box-body">
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								From
								<address>
									<strong><?php echo $row->nm_salesperson; ?></strong><br>
									Jln Industri No.18, Trosobo<br>
									Taman, Sidoarjo Regency, East Java 61262<br>
									Phone: <?php echo $row->mobile_sales; ?><br>
									Email: <?php echo $row->email_sales; ?>
								</address>
							</div>
							<div class="col-sm-4 invoice-col">
								To
								<address>
									<strong><?php echo $row->nm_customer; ?></strong><br>
									<?php echo $alamat_satu; ?><br>
									<?php echo $alamat_dua; ?><br>
									Phone: <?php echo $telp_customer; ?><br>
									Email: <?php echo $email_customer; ?>
								</address>
							</div>
							<div class="col-sm-4 invoice-col">
								<b>Quotation No. #<?php echo $row->no_quotation; ?></b><br>
								<b>Quotation Date <?php echo $row->tgl_quotation; ?></b><br>
								<b>Quotation Status: <?php echo bg_label($word, $color); ?></b>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
endforeach;
echo form_close();