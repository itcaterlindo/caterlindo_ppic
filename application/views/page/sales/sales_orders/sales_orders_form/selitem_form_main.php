<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataSO';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>
<div id="idErrForm"></div>
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th style="text-align: center;width: 1%;">No.</th>
			<th style="text-align: center;width: 1%; text-align: center;"><?php echo form_checkbox(array('name' => 'chkAllItem', 'class' => 'icheck chk-all', 'value' => '1', 'checked' => TRUE)); ?></th>
			<th style="width: 95px;">Status</th>
			<th style="text-align: center;width: 18%;">Code</th>
			<th style="text-align: center;width: 25%">Description</th>
			<th style="text-align: center;width: 25%;">Size</th>
			<th style="text-align: center;width: 15%">Note</th>
			<th style="text-align: center;width: 8%">Qty</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		if (!empty($item_parent)) :
			foreach ($item_parent as $parent) :
				$no_child = 0;
				$rowspan = 0;
				if (!empty($item_child)) :
					foreach ($item_child as $child) :
						if ($child['ditem_quotation_kd'] == $parent->kd_ditem_quotation) :
							$no_child++;
						endif;
						$rowspan = 1 + $no_child;
					endforeach;
				endif;
				$no++;
				?>
				<tr>
					<td rowspan="<?php echo $rowspan; ?>" style="vertical-align: middle;"><?php echo $no; ?></td>
					<td><?php echo form_checkbox(array('name' => 'chkItemParent[]', 'class' => 'icheck chk-item', 'value' => $parent->kd_ditem_quotation, 'checked' => TRUE)); ?></td>
					<td><?php echo item_stat($parent->item_status); ?></td>
					<td><?php echo $parent->item_code; ?></td>
					<td><?php echo $parent->item_desc; ?></td>
					<td><?php echo $parent->item_dimension; ?></td>
					<td><?php echo $parent->item_note; ?></td>
					<td style="text-align: center;"><?php echo $parent->item_qty; ?></td>
				</tr>
				<?php
				if (!empty($item_child)) :
					foreach ($item_child as $child) :
						if ($child['ditem_quotation_kd'] == $parent->kd_ditem_quotation) :
							?>
							<tr>
								<td><?php echo form_checkbox(array('name' => 'chkItemChild[]', 'class' => 'icheck chk-item', 'value' => $child['kd_citem_quotation'], 'checked' => TRUE)).form_input(array('type' => 'hidden', 'name' => 'txtKdParent['.$child['kd_citem_quotation'].']', 'value' => $child['ditem_quotation_kd'])); ?></td>
								<td><?php echo item_stat($child['item_status']); ?></td>
								<td><?php echo $child['item_code']; ?></td>
								<td><?php echo $child['item_desc']; ?></td>
								<td><?php echo $child['item_dimension']; ?></td>
								<td><?php echo $child['item_note']; ?></td>
								<td style="text-align: center;"><?php echo $child['item_qty']; ?></td>
							</tr>
							<?php
						endif;
					endforeach;
				endif;
			endforeach;
		endif;
		?>
	</tbody>
</table>
<hr>
<div id="idFormBtn" class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" class="btn btn-primary btn-flat">
			<i class="fa fa-shopping-cart"></i> Submit
		</button>
	</div>
</div>
<?php
echo form_close();