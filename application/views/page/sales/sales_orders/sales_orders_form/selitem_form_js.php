<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	var jml_click = 0;
	open_form('<?php echo $kd_mquotation; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#idTableBoxLoader<?php echo $master_var; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
		open_table();
	});

	$(document).off('submit', '#idForm<?php echo $master_var; ?>').on('submit', '#idForm<?php echo $master_var; ?>', function(e) {
		e.preventDefault();
		submit_form(this);
	});

	$(document).off('ifClicked', '.chk-all').on('ifClicked', '.chk-all', function() {
		jml_click = jml_click + 1;
		if (jml_click % 2) {
			$('.chk-item').iCheck('uncheck');
		} else {
			$('.chk-item').iCheck('check');
		}
	});

	$(document).off('ifChanged', '.chk-item').on('ifChanged', '.chk-item', function() {
		var jml_parent = $('input[name="chkItemParent[]"]').length;
		var jml_child = $('input[name="chkItemChild[]"]').length;
		var jml_checked_parent = $('input[name="chkItemParent[]"]:checked').length;
		var jml_checked_child = $('input[name="chkItemChild[]"]:checked').length;
		var jml_checkbox = parseInt(jml_parent) + parseInt(jml_child);
		var jml_checked = parseInt(jml_checked_parent) + parseInt(jml_checked_child);
		if (jml_checked < 1) {
			$('#idFormBtn').slideUp('fast');
			$('.chk-all').iCheck('uncheck');
		} else {
			$('#idFormBtn').slideDown('fast');
			$('.chk-all').iCheck('check');
		}
	});

	function open_form(kd_mquotation) {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/open_select_items_form'; ?>',
				data: 'kd_mquotation='+kd_mquotation,
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					/* --START ICHECK PROPERTIES-- */
					$('input[type="checkbox"].icheck').iCheck({
						checkboxClass: 'icheckbox_square-blue'
					});
					/* --END ICHECK PROPERTIES-- */
				}
			});
		});
	}

	function close_form() {
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$('#<?php echo $box_id; ?>').fadeOut(function() {
				$('#<?php echo $box_id; ?>').remove();
			});
		});
	}

	function submit_form(form_id) {
		$('#<?php echo $box_overlay_id; ?>').show();
		$('#<?php echo $box_alert_id; ?>').html('');
		<?php
		foreach ($form_errs as $form_err) {
			echo '$(\'#'.$form_err.'\').html(\'\');';
		}
		?>
		$.ajax({
			url: "<?php echo base_url().$class_link.'/send_item_data'; ?>",
			type: "POST",
			data:  new FormData(form_id),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					close_form();
					$('#idTableSalesAlert').html(data.alert).fadeIn();
				}
				if (data.confirm == 'error') {
					$('#<?php echo $box_alert_id; ?>').html(data.alert);
					<?php
					foreach ($form_errs as $form_err) {
						echo '$(\'#'.$form_err.'\').html(data.'.$form_err.');';
					}
					?>
				}
				$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
				$('#<?php echo $box_overlay_id; ?>').hide();
			}
		});
	}
</script>