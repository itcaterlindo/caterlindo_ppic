<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$pil_quo = array('' => '-- Pilih No Quotation --');
$sel_quo = '';
$attr_quo = array('id' => 'idSelQuo', 'class' => 'form-control select2');
echo form_open('', array('id' => 'idForm', 'class' => 'form-horizontal', 'style' => 'width:100%;'));
?>
<div class="form-group">
	<label for="idTxtNamaCustomer" class="col-md-2 control-label">Nama Customer</label>
	<div class="col-md-4">
		<div id="idErrNamaCustomer"></div>
		<?php echo form_input(array('name' => 'txtNamaCustomer', 'id' => 'idTxtNamaCustomer', 'class' => 'form-control', 'placeholder' => 'Nama Customer')); ?>
		<?php echo form_input(array('type' => 'hidden', 'name' => 'txtKdCustomer', 'id' => 'idTxtKdCustomer')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idSelQuo" class="col-md-2 control-label">No Quotation</label>
	<div class="col-md-3 col-xs-11">
		<div id="idErrNoQuo"></div>
		<?php echo form_dropdown('selQuo[]', $pil_quo, $sel_quo, $attr_quo); ?>
	</div>
	<div class="col-md-1 col-xs-1">
		<button type="button" id="btnTambahQuo" class="btn btn-success btn-flat" title="Tambahkan Quotation"><i class="fa fa-plus"></i></button>
	</div>
</div>
<div id="idFormAddQuo"></div>
<div class="box-footer">
	<div class="col-xs-4 pull-left">
		<a href="javascript:void(0);" name="btnSubmit" class="btn btn-info pull-right" style="margin-right: 5px;" onclick="transferSales($('#idForm').serializeArray());">
			<i class="fa fa-arrow-right"></i> Next
		</a>
	</div>
</div>
<?php echo form_close(); ?>
<?php $this->load->view('script/'.$class_link.'/form_pilih_js'); ?>