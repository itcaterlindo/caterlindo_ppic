<?php
defined('BASEPATH') or exit('No direct script access allowed!');

echo form_open_multipart('', array('id' => 'idForm', 'class' => 'form-horizontal'));
?>
<div class="form-group">
	<label for="idTxtNmSales" class="col-md-2 control-label">Sales Person</label>
	<div class="col-md-4">
		<div id="idErrSales"></div>
		<?php echo form_input(array('type' => 'hidden', 'name' => 'txtKdSalesOrder', 'value' => $kd_msalesorder)); ?>
		<?php echo form_input(array('type' => 'hidden', 'name' => 'txtKdQuotation', 'value' => $mquotation_kd)); ?>
		<?php echo form_input(array('type' => 'hidden', 'name' => 'txtKdSales', 'id' => 'idTxtKdSales', 'value' => $salesperson_kd)); ?>
		<?php echo form_input(array('type' => 'hidden', 'name' => 'txtFileLama', 'value' => $file_attach_po)); ?>
		<?php echo form_input(array('type' => 'hidden', 'name' => 'txtStatusSo', 'value' => $status_so)); ?>
		<?php echo form_input(array('type' => 'hidden', 'name' => 'txtOrderTipe', 'value' => $order_tipe)); ?>
		<div id="scrollable-dropdown-menu">
        	<?php echo form_input(array('name' => 'txtNmSales', 'id' => 'idTxtNmSales', 'class' => 'form-control', 'placeholder' => 'Sales Person', 'value' => $nm_salesperson, $readonly => '')); ?>
        </div>
	</div>
	<label for="idTxtNoSalesOrder" class="col-md-1" style="padding-top:7px;">No SO</label>
	<div class="col-md-2">
		<div id="idErrNoSo"></div>
		<?php echo form_input(array('name' => 'txtNoSalesOrder', 'id' => 'idTxtNoSalesOrder', 'class' => 'form-control', 'placeholder' => 'No SO', 'readonly' => '', 'value' => $no_salesorder)); ?>
	</div>
</div>

<div class="form-group">
	<label for="idTxtEmailSales" class="col-md-2 control-label">Email Address</label>
	<div class="col-md-3">
		<div id="idErrEmailSales"></div>
		<?php echo form_input(array('name' => 'txtEmailSales', 'id' => 'idTxtEmailSales', 'class' => 'form-control', 'placeholder' => 'Email Address', 'value' => $email_sales)); ?>
	</div>
	<div class="col-md-1"></div>
	<label for="idTxtDate" class="col-md-1" style="padding-top:7px;">Date</label>
	<div class="col-md-2">
		<div id="idErrDate"></div>
		<?php echo form_input(array('name' => 'txtDate', 'id' => 'idTxtDate', 'class' => 'form-control datepickerbootstrap', 'placeholder' => 'Date', 'value' => $tgl_so)); ?>
	</div>
</div>

<div class="form-group">
	<label for="idTxtMobilePhone" class="col-md-2 control-label">Mobile Phone</label>
	<div class="col-md-2">
		<div id="idErrMobilePhone"></div>
		<?php echo form_input(array('name' => 'txtMobilePhone', 'id' => 'idTxtMobilePhone', 'class' => 'form-control', 'placeholder' => 'Mobile Phone', 'value' => $mobile_sales)); ?>
	</div>
	<!-- <div class="col-md-2"></div>
	<label for="idTxtTglKirim" class="col-md-1" style="padding-top:7px;">Tgl Kirim</label>
	<div class="col-md-2">
		<div id="idErrTglKirim"></div>
		<?php //echo form_input(array('name' => 'txtTglKirim', 'id' => 'idTxtTglKirim', 'class' => 'form-control datepickerbootstrap', 'placeholder' => 'Tgl Kirim', 'value' => $tgl_kirim)); ?>
	</div> -->
</div>
<div class="form-group">
	<label for="idTxtNotes" class="col-md-2 control-label">Note SO</label>
	<div class="col-md-8">
		<div id="idErrNotes"></div>
		<?php echo form_textarea(array('name' => 'txtNotes', 'id' => 'idTxtNotes', 'class' => 'form-control', 'rows' => '4', 'placeholder' => 'Note SO', 'value' => $note_so)); ?>
	</div>
</div>
<hr>

<div class="form-group">
	<label for="idTxtNoPo" class="col-md-2 control-label">No PO Customer</label>
	<div class="col-md-3">
		<div id="idErrNoPo"></div>
		<?php
		$form_po = array('name' => 'txtNoPo', 'id' => 'idTxtNoPo', 'class' => 'form-control', 'placeholder' => 'No PO', 'value' => $no_po);
		echo form_input($form_po);
		?>
	</div>
	<div class="col-md-1"></div>
	<label for="idTxtTglPo" class="col-md-1">Tanggal PO</label>
	<div class="col-md-3">
		<div id="idErrTglPo"></div>
		<?php echo form_input(array('name' => 'txtTglPo', 'id' => 'idTxtTglPo', 'class' => 'form-control datepickerbootstrap', 'placeholder' => 'Tanggal PO', 'value' => $tgl_po)); ?>
	</div>
</div>

<div class="form-group">
	<label for="idFileAttach" class="col-md-2 control-label">PO Attachment</label>
	<div class="col-md-3">
		<div id="idErrFileAttach"></div>
		<?php echo form_upload(array('name' => 'fileAttach', 'id' => 'idFileAttach', 'class' => 'form-control')); ?>
	</div>
	<?php if (!empty($file_attach_po)) : ?>
		<div class="col-md-1"></div>
		<label for="idFilePreview" class="col-md-1 control-label">Attachment Preview</label>
		<div class="col-md-3" style="margin-top: 7px;">
			<?php echo anchor_popup('assets/admin_assets/dist/img/attachment_po/'.$file_attach_po.'', 'File Attachment PO', array('title' => $no_po)); ?>
		</div>
	<?php endif; ?>
</div>

<div class="form-group">
	<label for="idTxtContainer" class="col-md-2 control-label">Container</label>
	<div class="col-md-3">
		<div id="idErrContainer"></div>
		<div id=""><?php echo form_dropdown('txtContainer', $container, '', array('name' => 'txtContainer', 'id' => 'idTxtContainer', 'class' => 'form-control select2', 'onchange' => 'get_container_data()')); ?></div>
		<span class="label label-default">Container for Ekspor</span>
	</div>
</div>

<div class="form-group">
	<label for="idTxtTipeContainer" class="col-md-2 control-label">Tipe Container</label>
	<div class="col-md-2">
		<div id="idErrTipeContainer"></div>
		<div id=""><?php echo form_input(array('name' => 'txtTipeContainer', 'id' => 'idTxtTipeContainer', 'class' => 'form-control', 'placeholder' => 'Tipe Container', 'value' => $tipe_container, 'readonly' => true)); ?></div>
	</div>
	<div class="col-md-1">
		<div id="idErrContainer20ft"></div>
		<div id=""><?php echo form_input(array('name' => 'txtContainer20ft', 'id' => 'idTxtContainer20ft', 'class' => 'form-control', 'placeholder' => '20ft', 'value' => $container_20ft, 'readonly' => true)); ?></div>
	</div>
</div>

<div class="form-group">
	<label for="idTxtTeritory" class="col-md-2 control-label">Teritory</label>
	<div class="col-md-3">
		<div id="idErrTeritory"></div>
		<div id="idTxtTeritory"><?php echo form_dropdown('txtTeritory', $teritory, $teritory_id, array('name' => 'txtTeritory', 'id' => 'idTxtTeritory', 'class' => 'form-control')); ?></div>
		<span class="label label-default">Teritory for Ekspor</span>
	</div>
</div>

<hr>
<div class="form-group">
	<label for="idTxtCustomerId" class="col-md-2 control-label">Customer ID</label>
	<div class="col-md-2">
		<div id="idErrCustomerId"></div>
		<div id="idTxtCustomerId" style="padding-top:7px;"><?php echo $code_customer; ?></div>
	</div>
</div>
<div class="form-group">
	<label for="idTxtNmCustomer" class="col-md-2 control-label">Customer</label>
	<div class="col-md-4">
		<div id="idErrNmCustomer"></div>
		<?php echo form_input(array('type' => 'hidden', 'name' => 'txtKdCustomer', 'id' => 'idTxtKdCustomer', 'value' => $customer_kd)); ?>
		<div id="scrollable-dropdown-menu">
			<?php echo form_input(array('name' => 'txtNmCustomer', 'id' => 'idTxtNmCustomer', 'class' => 'form-control', 'placeholder' => 'Customer', 'value' => $nm_customer)); ?>
		</div>
	</div>
	<div class="col-md-2 col-md-offset-2">
		<div id="idFormBtnCustomer">
			<div id="idGroupBtnAdd">
				<a onclick="return showFormCustomer();" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Customer</a>
			</div>
			<div id="idGroupBtnEdit" style="display: none;">
				<a onclick="return clearCustomerDetail();" class="btn btn-sm btn-danger" style="margin-right:5px;"><i class="fa fa-trash"></i> Clear Values</a>
				<a onclick="return showFormCustomer();" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i> Customer</a>
			</div>
		</div>
	</div>
</div>

<div class="form-group">
	<label for="idContactPerson" class="col-md-2 control-label">Contact Person</label>
	<div class="col-md-3">
		<div id="idErrContactPerson"></div>
		<div id="idContactPerson" style="padding-top:7px;"><?php echo $contact; ?></div>
	</div>
</div>

<div class="form-group">
	<label for="idTxtAlamatInstansi" class="col-md-2 control-label">Alamat Instansi</label>
	<div class="col-md-4">
		<div id="idErrAlamatInstansi"></div>
		<div id="idTxtAlamatInstansi" style="padding-top:7px;"><?php echo $alamat_instansi; ?></div>
	</div>
</div>

<div class="form-group">
	<label for="idSelAlamatKirim" class="col-md-2 control-label">Alamat Pengiriman</label>
	<div class="col-md-4">
		<div id="idErrAlamatKirim"></div>
		<div id="idFormAlamatCustomer" style="padding-top:7px;"><?php echo $alamat_form; ?></div>
	</div>
</div>


<div class="box-footer">
	<div class="col-xs-12 pull-left">
		<button type="submit" name="btnSubmit" class="btn btn-info pull-right" style="margin-right: 5px;"">
			<i class="fa fa-save"></i> Simpan
		</button>
	</div>
</div>
<?php echo form_close(); ?>
<?php $this->load->view('script/'.$class_link.'/form_sales_js'); ?>