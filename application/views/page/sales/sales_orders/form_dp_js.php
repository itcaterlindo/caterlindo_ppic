<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	first_load('idBoxFormLoader', 'idBoxFormContent');
	<?php
	foreach ($no_dps as $no_dp) :
		$jml_dp = $no_dp->jml_termin + $no_dp->jml_termin_ppn;
		$no_invoice = str_replace('/', '-', $no_dp->no_invoice);
		echo 'add_additional_dp(\''.$no_invoice.'\', \''.$jml_dp.'\');';
	endforeach;
	?>

	$(document).off('submit', '#idForm').on('submit', '#idForm', function(e) {
		e.preventDefault();
		submit_form(this);
	});

	function hapus_dp() {
		kd_msalesorder = $('#idTxtKd').val();
		$('#idBoxFormOverlay').show();
		$.ajax({
			url: '<?php echo base_url($class_link.'/sales_order_view/hapus_dp'); ?>',
			type: 'GET',
			data: 'kd_msalesorder='+kd_msalesorder,
			success: function(data) {
				if (data.confirm == 'success') {
					$('#idTableSalesAlert').html(data.alert).fadeIn();
					close_form();
				} else if (data.confirm == 'error') {
					$('#idErrForm').html(data.alert);
				}
				$('#idBoxFormOverlay').hide();
			}
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
			render_datepicker ('datetimepicker');
		});
	}

	function close_form() {
		$('#idBoxForm').remove();
		$('#idBoxFormSalesOrder').remove();
		open_table();
	}

	function submit_form(form_id) {
		$('#idBoxFormOverlay').show();
		$.ajax({
			url: "<?php echo base_url($class_link.'/sales_order_view/submit_form/'); ?>",
			type: "POST",
			data:  new FormData(form_id),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					$('#idTableSalesAlert').html(data.alert).fadeIn();
					close_form();
					// productionProcess('<?php echo $kd_msalesorder; ?>', 'lokal');
				} else if (data.confirm == 'error') {
					$('#idErrDp').html(data.idErrDp);
				}
				$('#idBoxFormOverlay').hide();
				$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
			}
		});
	}

	$(document).off('click', '#idBtnAddDp').on('click', '#idBtnAddDp', function() {
		add_additional_dp('', '');
	});

	$(document).off('click', '.btn_remove_dp').on('click', '.btn_remove_dp', function() {
		$(this).parents('.form_additional_dp').slideUp(function() {
			$(this).remove();
		});
	});


	function render_datepicker (valClass) {
		$('.'+valClass).datetimepicker({
            format: 'DD-MM-YYYY',
        });
	}
</script>