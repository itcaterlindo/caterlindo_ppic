<?php
defined('BASEPATH') OR exit('No direct script access allowed');
extract($master);
$jml_ongkir = isset($jml_ongkir)?$jml_ongkir:'0';
$jml_install = isset($jml_install)?$jml_install:'0';
?>
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th style="width: 1%;">No.</th>
			<th style="width: 1%; text-align: center;">Opsi</th>
			<th style="width: 7%;">Item Type</th>
			<th>Product Code</th>
			<th style="width: 5%;">Status</th>
			<th>Description</th>
			<th>Dimension</th>
			<th style="width: 1%;">Qty</th>
			<th style="text-align: center;">Price/Unit</th>
			<th>Disc</th>
			<th style="text-align: center;">Subtotal</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 0; ?>
		<?php $tot_harga = array(); ?>
		<?php foreach ($items_data as $item_data) : ?>
			<?php $no++; ?>
			<?php $item_type = 'Partial'; ?>
			<?php if (!empty($details_data)) : ?>
				<?php $item_type = find_key_value($details_data, 'ditem_so_kd', $item_data->kd_ditem_so)?'Set':'Partial'; ?>
			<?php endif; ?>
			<?php $tot_harga[] = $item_data->total_harga; ?>
			<tr>
				<td><?php echo $no; ?></td>
				<td>
					<div align="center">
						<div class="btn-group">
							<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
								Opsi <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<?php //if ($order_tipe != 'additional') :?>
								<li>
									<a id="editItem" title="Edit Item" href="javascript:void(0);" onclick="editItemDetail('<?php echo $item_data->kd_ditem_so; ?>')">
										<i class="fa fa-pencil"></i> Edit
									</a>
								</li>

								<!-- Jika item custom maka tampilkan input ke master -->
								<?php if($item_data->barang_kd == "PRD020817000573"): ?>
									<li>
									<a id="editItem" title="Edit Item" href="javascript:void(0);" onclick="addItemCustomModal('<?php echo $item_data->kd_ditem_so; ?>', 'parent')">
										<i class="fa fa-save"></i> Input Master
									</a>
								</li>
								<?php endif; ?>

								<li class="divider"></li>
								<li>
									<a id="deleteItem" title="Delete Item" href="javascript:void(0);" onclick="return confirm('Item akan dihapus?')?deleteItemDetail('<?php echo $item_data->kd_ditem_so; ?>'):false;">
										<i class="fa fa-trash"></i> Delete
									</a>
								</li>
								<?php //endif;?>
							</ul>
						</div>
					</div>
				</td>
				<td>
					<label style="margin-top: 5px; font-weight: 500;"><?php echo $item_type; ?></label>
					<a href="javascript:void(0);" id="addChild" title="Add Child" onclick="addChildData('<?php echo $item_data->kd_ditem_so; ?>', '<?php echo $item_data->barang_kd; ?>', '<?php echo $item_data->item_code.'/'.$item_data->item_desc; ?>')" class="btn btn-sm btn-primary pull-right add-child-btn">
						<i class="fa fa-plus"></i>
					</a>
				</td>
				<td><?php echo $item_data->item_code; ?></td>
				<td><?php echo item_stat($item_data->item_status).'/Parent/'.$item_data->order_tipe.$item_data->item_code; ?> <?= $item_data->barang_kd == "PRD020817000573" ? '<span class="label label-danger">Belum Input Master</span>' : '' ?> </td>
				<td><?php echo $item_data->item_desc; ?></td>
				<td><?php echo $item_data->item_dimension; ?></td>
				<td style="text-align: center;"><?php echo $item_data->item_qty; ?></td>
				<td style="text-align: right;"><?php echo format_currency($item_data->harga_barang, $currency_icon); ?></td>
				<td><?php echo item_discount($item_data->disc_type, $item_data->item_disc); ?></td>
				<td style="text-align: right;"><?php echo format_currency($item_data->total_harga, $currency_icon); ?></td>
			</tr>
			<?php if (!empty($details_data)) : ?>
				<?php foreach ($details_data as $detail_data) : ?>
					<?php if ($detail_data['ditem_so_kd'] == $item_data->kd_ditem_so) : ?>
						<?php $no++; ?>
						<?php $tot_harga[] = $detail_data['total_harga']; ?>
						<tr>
							<td><?php echo $no; ?></td>
							<td>
								<div align="center">
									<div class="btn-group">
										<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
											Opsi <span class="caret"></span>
										</button>
										<ul class="dropdown-menu">
											<?php //if ($order_tipe != 'additional') :?>
											<li>
												<a id="editItem" title="Edit Item" href="javascript:void(0);" onclick="editItemDetailChild('<?php echo $detail_data['kd_citem_so']; ?>', '<?php echo $item_data->kd_ditem_so; ?>', '<?php echo $item_data->barang_kd; ?>', '<?php echo $item_data->item_code.'/'.$item_data->item_desc; ?>')">
													<i class="fa fa-pencil"></i> Edit
												</a>
											</li>

											<!-- Jika item custom maka tampilkan input ke master -->
											<?php if($detail_data['kd_child'] == "PRD020817000573"): ?>
												<li>
												<a id="editItem" title="Edit Item" href="javascript:void(0);" onclick="addItemCustomModal('<?php echo $detail_data['kd_citem_so']; ?>', 'child')">
													<i class="fa fa-save"></i> Input Master
												</a>
											</li>
											<?php endif; ?>

											<li class="divider"></li>
											<li>
												<a id="deleteItem" title="Delete Item" href="javascript:void(0);" onclick="return confirm('Item akan dihapus?')?deleteItemDetailChild('<?php echo $detail_data['kd_citem_so']; ?>'):false;">
													<i class="fa fa-trash"></i> Delete
												</a>
											</li>
											<?php //endif;?>
										</ul>
									</div>
								</div>
							</td>
							<td>
								<label style="margin-top: 5px; font-weight: 500;">Set</label>
							</td>
							<td><?php echo $detail_data['item_code']; ?></td>
							<td><?php echo item_stat($detail_data['item_status']).'/Child/'.$detail_data['order_tipe']; ?> <?= $detail_data['kd_child'] == "PRD020817000573" ? '<span class="label label-danger">Belum Input Master</span>' : '' ?> </td>
							<td><?php echo $detail_data['item_desc']; ?></td>
							<td><?php echo $detail_data['item_dimension']; ?></td>
							<td style="text-align: center;"><?php echo $detail_data['item_qty']; ?></td>
							<td style="text-align: right;"><?php echo format_currency($detail_data['harga_barang'], $currency_icon); ?></td>
							<td><?php echo item_discount($detail_data['disc_type'], $detail_data['item_disc']); ?></td>
							<td style="text-align: right;"><?php echo format_currency($detail_data['total_harga'], $currency_icon); ?></td>
						</tr>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endif; ?>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<?php $tots_pot = array(); ?>
		<?php $tots_harga = array_sum($tot_harga); ?>
		<tr>
			<td colspan="8"></td>
			<th colspan="2">Total Price</th>
			<td style="text-align: right;"><?php echo format_currency($tots_harga, $currency_icon); ?></td>
			<?php echo form_input(array('type' => 'hidden', 'name' => 'txtKdMaster', 'id' => 'idTxtKdMaster', 'value' => $kd_msalesorder)); ?>
		</tr>
		<?php $jml_pot = count($tot_potongan); ?>
		<?php $no = 0; ?>
		<?php foreach ($tot_potongan as $tot) : ?>
			<?php $no++; ?>
			<?php $tots_pot[] = $tot->total_nilai; ?>
			<tr>
				<td colspan="8"></td>
				<td colspan="2">
					<?php echo $tot->nm_kolom; ?>
					<a href="javascript:void(0);" id="idBtnHapusPot" class="btn btn-sm btn-danger pull-right" title="Hapus Special Discount" onclick="return confirm('Anda akan menghapus special discount?')?hapusSpecialDiscount('<?php echo $tot->kd_dharga_so; ?>', '<?php echo $tot->msalesorder_kd; ?>'):false;">
						<i class="fa fa-trash"></i>
					</a>
				</td>
				<td style="text-align: right;"><?php echo format_currency($tot->total_nilai, $currency_icon); ?></td>
			</tr>
			<?php if ($no == $jml_pot) : ?>
				<?php $tot_pot = array_sum($tots_pot); ?>
				<?php $tots_harga = $tots_harga - $tot_pot; ?>
				<tr>
					<td colspan="8"></td>
					<th colspan="2">Total Price</th>
					<td style="text-align: right;"><?php echo format_currency($tots_harga, $currency_icon); ?></td>
				</tr>
			<?php endif; ?>
		<?php endforeach; ?>
		<tr>
			<td colspan="8"></td>
			<td colspan="2">Ongkir</td>
			<td style="text-align: right;">
				<div id="idTextOngkir"><?php echo format_currency($jml_ongkir, $currency_icon); ?></div>
				<div id="idFormTxtOngkir" style="display: none;"><?php echo form_input(array('name' => 'txtOngkir', 'id' => 'idTxtOngkir', 'value' => $jml_ongkir, 'style' => 'width:150px;text-align:right;')); ?></div>
			</td>
		</tr>
		<tr>
			<td colspan="8"></td>
			<td colspan="2">Installasi</td>
			<td style="text-align: right;">
				<div id="idTextInstall"><?php echo format_currency($jml_install, $currency_icon); ?></div>
				<div id="idFormTxtInstall" style="display: none;"><?php echo form_input(array('name' => 'txtInstall', 'id' => 'idTxtInstall', 'value' => $jml_install, 'style' => 'width:150px;text-align:right;')); ?></div>
			</td>
		</tr>
		<?php $tots_harga = ($tots_harga + $jml_ongkir + $jml_install); ?>
		<tr>
			<td colspan="8"></td>
			<th colspan="2">Total Price</th>
			<td style="text-align: right;"><?php echo format_currency($tots_harga, $currency_icon); ?></td>
		</tr>
	</tfoot>
</table>
<div id="idFormTotalHarga"></div>