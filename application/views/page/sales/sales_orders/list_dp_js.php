<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	first_load('idBoxFormLoader', 'idBoxFormContent');

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function pushSap(jenis, id)
	{
		$.ajax({
			url: "<?php echo base_url($class_link.'/sales_order_view/submit_dp_to_sap/'); ?>",
			type: "POST",
			data:  {jenis_dp: jenis, id: id},
			beforeSend: function(){
				$('.btn-push-sap').prop('disabled', true);
			},
			success: function(resp){
				if(resp.code == 200){
					notify(resp.status, resp.pesan, 'success');
					bukaListDp('<?= $kd_msalesorder ?>')
				}else{
					notify(resp.status, resp.pesan, 'error');
					$('.btn-push-sap').prop('disabled', false);
				}
			},
			error: function(err){
				notify("Gagal", "Error proses ketika push DP ke SAP", 'error');
				$('.btn-push-sap').prop('disabled', false);
			}
		});
		
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }

</script>