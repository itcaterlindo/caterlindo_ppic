<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataDetailDO';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtKdMDo', 'value' => $kd_mdo));
?>

<style type="text/css">
	.tblchild {
		max-width: none;
		table-layout: fixed;
		word-wrap: break-word;
	}
</style>

<div class="row invoice-info">
	<div class="col-sm-6 invoice-col">
		No Invoice : <strong><?php echo isset($master->no_invoice) ? $master->no_invoice: '-'?></strong><br>
		No Sales Order :
		<?php 
		$no_salesorder = '-';
		if (isset($master)) {
			$no_salesorder = $master->no_salesorder;
			if ($master->tipe_customer == 'Ekspor'){
				$no_salesorder = $master->no_po;
			}
		}
		echo $no_salesorder;
		?><br>
	</div>
</div>
<div id="idErrForm"></div>
<!-- TABLE -->
<div class="table-responsive">
<table class="table table-bordered table-striped table-hover tblchild">
	<thead>
		<tr>
			<th style="text-align: center;width: 8%;">No.</th>
			<th style="text-align: center;width: 12%;">Opsi</th>
			<th style="text-align: center;width: 12%;">Code</th>
			<th style="text-align: center;width: 20%">Description</th>
			<th style="text-align: center;width: 20%;">Size</th>
			<th style="text-align: center;width: 10%">Tarif</th>
			<th style="text-align: center;width: 10%">Qty SO</th>
			<th style="text-align: center;width: 10%">Qty</th>
			<th style="text-align: center;width: 8%;">NW</th>
			<th style="text-align: center;width: 8%">GW</th>
			<th style="text-align: center;width: 25%">Note</th>
		</tr>
	</thead>
	<tbody>
		<?php
        $no = 1;
        $sumSO = 0;
        $sumDO = 0;
        foreach ($resultDOitem as $eDOitem) :
            $sumSO += $eDOitem['item_qty'];
            $sumDO += $eDOitem['stuffing_item_qty'];
        ?>
        <tr>
            <td><?php echo $no;?></td>
			<td><?php 
			$btns = array();
			$btns[] = get_btn(array('title' => 'Ubah Item', 'icon' => 'pencil', 'onclick' => 'formdetail_main(\''.$eDOitem['kd_ditem_do'].'\')'));
			$btns[] = get_btn_divider();
			$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_itemdetail(\''.$eDOitem['kd_ditem_do'].'\',)'));
			$btn_group = group_btns($btns);
			echo $btn_group;
			?></td>
            <td><?php echo $eDOitem['item_code'];?></td>
            <td><?php echo $eDOitem['item_desc'];?></td>
            <td><?php echo $eDOitem['item_dimension'];?></td>
            <td><?php echo $eDOitem['hs_code'];?></td>
            <td style="text-align: right;"><?php echo $eDOitem['item_qty'];?></td>
            <td style="text-align: right;"><?php echo $eDOitem['stuffing_item_qty'];?></td>
            <td><?php echo $eDOitem['netweight'];?></td>
            <td><?php echo $eDOitem['grossweight'];?></td>
			<td><?php echo $eDOitem['item_note_do'];?></td>
        </tr>
        <?php
        $no++;
        endforeach;
		?>
	</tbody>
	<tfoot>
		<tr style="font-weight: bold;">
			<td style="text-align: right;" colspan="7">Jumlah</td>
			<td style="text-align: right;"><div><?php echo $sumDO?></div></td>
		</tr>
	</tfoot>
</table>
</div>
<!-- END OF TABLE -->
<?php echo form_close(); ?>