<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	var jml_click = 1;
	open_form('<?php echo $id; ?>');
	tabledetail_main('<?php echo $id;?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('keyup', '.clqtydo').on('keyup', '.clqtydo', function() {
		calculate_do();
	});

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#idTableBoxLoader<?php echo $master_var; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
		open_table();
		first_load('<?php echo 'idTableBoxLoader'.$master_var; ?>', '<?php echo 'idTableBoxContent'.$master_var; ?>');
	});

	$(document).off('ifClicked', '.chk-all').on('ifClicked', '.chk-all', function() {
		jml_click = jml_click + 1;
		if (jml_click % 2) {
			$('.chk-item').iCheck('uncheck');
		} else {
			$('.chk-item').iCheck('check');
		}
	});

	$(document).off('ifChanged', '.chk-item').on('ifChanged', '.chk-item', function() {
		var jml_parent = $('input[name="chkItemParent[]"]').length;
		var jml_child = $('input[name="chkItemChild[]"]').length;
		var jml_checked_parent = $('input[name="chkItemParent[]"]:checked').length;
		var jml_checked_child = $('input[name="chkItemChild[]"]:checked').length;
		var jml_checkbox = parseInt(jml_parent) + parseInt(jml_child);
		var jml_checked = parseInt(jml_checked_parent) + parseInt(jml_checked_child);
		if (jml_checked < 1) {
			$('#idFormBtn').slideUp('fast');
			$('.chk-all').iCheck('uncheck');
		} else {
			$('#idFormBtn').slideDown('fast');
			$('.chk-all').iCheck('check');
		}
		calculate_do ();
	});

	$(document).off('submit', '#idForm<?php echo $master_var; ?>').on('submit', '#idForm<?php echo $master_var; ?>', function(e) {
		e.preventDefault();
		submit_form(this);
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function calculate_do() {
		let aValChecked = [];
		let aValDo = [];
		let sumDO = 0;
		$('input:checkbox.chk-item').each(function () {
       		let sThisVal = (this.checked ? $(this).val() : "");
			if (sThisVal != "") {
				aValChecked.push(sThisVal);
			}
  		});
		if(aValChecked) {
			aValChecked.forEach(function(item) {
				let valDO = $("input[name='txtStuffingQty["+item+"]']").val();
				aValDo.push(parseInt(valDO));
			});
			sumDO = aValDo.reduce((a, b) => a + b, 0)
		}
		if (isNaN(sumDO)){
			sumDO = 0;
		}
		$('#idjmldo').text(sumDO);
	}

	function open_form(id, tipe) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/open_form'; ?>',
			data: 'id='+id,
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
				$('#<?php echo $box_content_id; ?>').slideDown();
				/* --START ICHECK PROPERTIES-- */
				$('input[type="checkbox"].icheck').iCheck({
					checkboxClass: 'icheckbox_square-blue'
				});
				/* --END ICHECK PROPERTIES-- */
			}
		});
	}

	function tabledetail_main(id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/tabledetail_main'; ?>',
			data: 'id='+id,
			success: function(html) {
				$('#<?php echo $box_content_id_2; ?>').html(html);
			}
		});
	}

	function formdetail_main(kd_ditem_do) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/formdetail_main'; ?>',
			data: 'kd_ditem_do='+kd_ditem_do,
			success: function(html) {
				toggle_modal('Edit Item', html);
			}
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function close_form() {
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$('#<?php echo $box_id; ?>').fadeOut(function() {
				$('#<?php echo $box_id; ?>').remove();
			});
		});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
	}

	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
		boxOverlayForm('out');
	}

	function ubahStatus(kd_msales, status) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'sales/sales_orders/sales_orders_tbl/ubah_status'; ?>',
			data: 'id='+kd_msales+'&status='+status,
			success:function(html){
				$('#idErrForm').html(html.alert).fadeIn();
			}
		});
	}

	function submit_form(form_id) {
		$('#<?php echo $box_overlay_id; ?>').show();
		$('#idTableBoxAlert<?php echo $master_var; ?>').html('');
		$('#<?php echo $box_alert_id; ?>').html('');
		<?php
		foreach ($form_errs as $form_err) {
			echo '$(\'#'.$form_err.'\').html(\'\');';
		}
		?>
		$.ajax({
			url: "<?php echo base_url().$class_link.'/send_data'; ?>",
			type: "POST",
			data:  new FormData(form_id),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					if (data.slug == 'add') {
						close_form();
						$('#idTableBtnBoxAdd<?php echo $master_var; ?>').show();
						$('#idTableBoxAlert<?php echo $master_var; ?>').html(data.alert).fadeIn();
						$('#idTableSalesAlert').html(data.alert).fadeIn();
						open_table();
					}else if (data.slug == 'edit') {
						open_form('<?php echo $id; ?>');
						tabledetail_main('<?php echo $id;?>');
					}
				}
				if (data.confirm == 'error') {
					$('div.form-group input:eq(0)').focus();
					$('#<?php echo $box_alert_id; ?>').html(data.alert);
					<?php
					foreach ($form_errs as $form_err) {
						echo '$(\'#'.$form_err.'\').html(data.'.$form_err.');';
					}
					?>
				}
				$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
				$('#<?php echo $box_overlay_id; ?>').hide();
			}
		});
	}

	function action_submitDatadetail (form_id) {
		event.preventDefault();
		var form_id = document.getElementById(form_id);
		$.ajax({
			url: "<?php echo base_url().$class_link.'/action_submitDatadetail'; ?>",
			type: "POST",
			data:  new FormData(form_id),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					toggle_modal('', '');
					open_form('<?php echo $id; ?>');
					tabledetail_main('<?php echo $id;?>');
					notify (resp.status, resp.pesan, 'success');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			}
		});
	}

	function hapus_itemdetail (id) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_deletedetail'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						open_form('<?php echo $id; ?>');
						tabledetail_main('<?php echo $id;?>');
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				}
			});
		}
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }
</script>