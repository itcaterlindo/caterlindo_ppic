<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'FormDataDetailDO';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtkd_ditem_do', 'value' => $kd_ditem_do));
echo form_input(array('type' => 'hidden', 'name' => 'txtkd_mdo', 'value' => $row->mdo_kd));
?>

<div class="col-md-12">
    <div class="row">
        <div class="form-group">
            <label for='idtxtitem_code' class="col-md-2 control-label">Item Code</label>
            <div class="col-sm-4 col-xs-12">
                <div class="errInput" id="idErritem_code"></div>
                <?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtitem_code', 'id'=> 'idtxtitem_code', 'placeholder' =>'Item Code', 'readonly' => 'true', 'value'=> isset($row->item_code) ? $row->item_code: null ));?>
            </div>	
        </div>

        <div class="form-group">
            <label for="idtxtitem_desc" class="col-md-2 control-label">Deskripsi</label>
            <div class="col-sm-9 col-xs-12">
                <div class="errInput" id="idErritem_desc"></div>
                <?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtitem_desc', 'id'=> 'idtxtitem_desc', 'placeholder' =>'Deskripsi Item', 'rows' => '2', 'readonly' => 'true', 'value'=> isset($row->item_desc) ? $row->item_desc: null ));?>
            </div>
        </div>

        <div class="form-group">
            <label for="idtxtitem_dimension" class="col-md-2 control-label">Dimensi</label>
            <div class="col-sm-9 col-xs-12">
                <div class="errInput" id="idErritem_dimension"></div>
                <?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtitem_dimension', 'id'=> 'idtxtitem_dimension', 'placeholder' =>'Spesifikasi Item', 'rows' => '2', 'readonly' => 'true', 'value'=> isset($row->item_dimension) ? $row->item_dimension: null ));?>
            </div>
        </div>

        <div class="form-group">
            <label for='idtxtitem_qty' class="col-md-2 control-label">Qty SO</label>
            <div class="col-sm-3 col-xs-12">
                <div class="errInput" id="idErritem_qty"></div>
                <?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtitem_qty', 'id'=> 'idtxtitem_qty', 'placeholder' =>'Qty SO', 'readonly' => 'true', 'value'=> isset($row->item_qty) ? $row->item_qty: null ));?>
            </div>
            <label for='idtxtstuffing_item_qty' class="col-md-2 control-label">Qty DO</label>
            <div class="col-sm-3 col-xs-12">
                <div class="errInput" id="idErrstuffing_item_qty"></div>
                <?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtstuffing_item_qty', 'id'=> 'idtxtstuffing_item_qty', 'placeholder' =>'Qty DO', 'value'=> isset($row->stuffing_item_qty) ? $row->stuffing_item_qty: null ));?>
            </div>
        </div>
    </div>

    <hr>
    <div id="idFormBtn" class="form-group">
        <div class="form-group">
            <div class="col-sm-5 col-sm-offset-2 col-xs-12">
                <button type="submit" onclick="action_submitDatadetail('<?php echo $form_id; ?>')" name="btnSubmit" class="btn btn-sm btn-primary btn-flat">
                    <i class="fa fa-save"></i> Simpan
                </button>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>