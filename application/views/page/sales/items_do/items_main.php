<?php
defined('BASEPATH') or exit('No direct script script access allowed!');


/* --Masukkan setting properti untuk form-- */
$master_var = 'DataDO';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtKdMDo', 'value' => $kd_mdo));
echo form_input(array('type' => 'hidden', 'name' => 'txtKdMSo', 'value' => $msalesorder_kd));
echo form_input(array('type' => 'hidden', 'name' => 'txtSlug', 'value' => isset($slug) ? $slug:'add'));
?>

<style type="text/css">
	.tblparent {
		max-width: none;
		table-layout: fixed;
		word-wrap: break-word;
	}
</style>

<div id="idErrForm"></div>
<!-- TABLE -->
<div class="table-responsive">
<table class="table table-bordered table-striped table-hover tblparent">
	<thead>
		<tr>
			<th style="text-align: center;width: 8%;">No.</th>
			<th style="text-align: center;width: 8%; text-align: center;"><?php echo form_checkbox(array('name' => 'chkAllItem', 'class' => 'icheck chk-all', 'value' => '1')); ?></th>
			<th style="text-align: center;width: 12%;">Code</th>
			<th style="text-align: center;width: 20%">Description</th>
			<th style="text-align: center;width: 20%;">Size</th>
			<th style="text-align: center;width: 10%">Tarif</th>
			<th style="text-align: center;width: 10%">Qty SO</th>
			<th style="text-align: center;width: 10%">Qty</th>
			<th style="text-align: center;width: 8%;">NW</th>
			<th style="text-align: center;width: 8%">GW</th>
			<th style="text-align: center;width: 25%">Note</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$parent_kd = array();
		$child_kd = array();
		$stuffing_qty = array();
		foreach ($items_do as $item_do) :
			$parent_kd[] = $item_do->parent_kd;
			$child_kd[] = $item_do->child_kd;
			$master_kd = !empty($item_do->child_kd)?$item_do->child_kd:$item_do->parent_kd;
			$stuffing_qty[$master_kd][] = !empty($item_do->stuffing_item_qty)?$item_do->stuffing_item_qty:'0';
		endforeach;
		$no = 0;
		$sumSO = 0;
		$sumDO = 0;
		foreach ($so_data_parent as $so_parent) :
			if (isset($stuffing_qty[$so_parent->kd_ditem_so])) :
				$jml_do = array_sum($stuffing_qty[$so_parent->kd_ditem_so]);
			else :
				$jml_do = '0';
			endif;
			$jml_sisa = $so_parent->item_qty - $jml_do;
			$sumDO += $jml_sisa;
			if ($jml_sisa > 0) :
				$sumSO += $so_parent->item_qty;
				$no++;
				echo '<tr>';
				echo '<td>'.$no.'</td>';
				echo '<td>'.form_checkbox(array('name' => 'chkItemParent[]', 'class' => 'icheck chk-item', 'value' => $so_parent->kd_ditem_so)).'</td>';
				echo '<td>'.$so_parent->item_code.'</td>';
				echo '<td>'.$so_parent->item_desc.'</td>';
				echo '<td>'.$so_parent->item_dimension.'</td>';
				echo '<td>'.$so_parent->hs_code.'</td>';
				echo '<td style="text-align: right;">'.$so_parent->item_qty.'</td>';
				echo '<td>'.form_input(array('type' => 'number', 'name' => 'txtStuffingQty['.$so_parent->kd_ditem_so.']', 'class' => 'form-control clqtydo', 'min' => '0', 'max' => $jml_sisa, 'value' => $jml_sisa)).'</td>';
				echo '<td>'.$so_parent->netweight.'</td>';
				echo '<td>'.$so_parent->grossweight.'</td>';
				echo '<td>'.form_textarea(array('rows' => 2, 'name' => 'txtItemNote['.$so_parent->kd_ditem_so.']', 'class' => 'form-control', 'value' => $so_parent->item_note)).'</td>';
				echo form_input(array('type' => 'hidden', 'name' => 'txtItemBarcode['.$so_parent->kd_ditem_so.']', 'value' => $so_parent->item_barcode));
				echo '</tr>';
			endif;
			foreach ($so_data_child as $so_child) :
				if (isset($stuffing_qty[$so_child['kd_citem_so']])) :
					$jml_do = array_sum($stuffing_qty[$so_child['kd_citem_so']]);
				else :
					$jml_do = '0';
				endif;
				$jml_sisa = $so_child['item_qty'] - $jml_do;
				$sumDO += $jml_sisa;
				if ($so_child['ditem_so_kd'] == $so_parent->kd_ditem_so && $jml_sisa > 0) :
					$sumSO += $so_child['item_qty'];
					$no++;
					echo '<tr>';
					echo '<td>'.$no.'</td>';
					echo '<td>'.form_checkbox(array('name' => 'chkItemChild[]', 'class' => 'icheck chk-item', 'value' => $so_child['kd_citem_so'])).form_input(array('type' => 'hidden', 'name' => 'txtKdParent['.$so_child['kd_citem_so'].']', 'value' => $so_child['ditem_so_kd'])).'</td>';
					echo '<td>'.$so_child['item_code'].'</td>';
					echo '<td>'.$so_child['item_desc'].'</td>';
					echo '<td>'.$so_child['item_dimension'].'</td>';
					echo '<td>'.$so_child['hs_code'].'</td>';
					echo '<td style="text-align: right;">'.$so_child['item_qty'].'</td>';
					echo '<td>'.form_input(array('type' => 'number', 'name' => 'txtStuffingQty['.$so_child['kd_citem_so'].']', 'class' => 'form-control clqtydo', 'min' => '0', 'max' => $jml_sisa, 'value' => $jml_sisa)).'</td>';
					echo '<td>'.$so_child['netweight'].'</td>';
					echo '<td>'.$so_child['grossweight'].'</td>';
					echo '<td>'.form_textarea(array('rows' => 2, 'name' => 'txtItemNote['.$so_child['kd_citem_so'].']', 'class' => 'form-control', 'value' => $so_child['item_note'])).'</td>';
					echo form_input(array('type' => 'hidden', 'name' => 'txtItemBarcode['.$so_child['kd_citem_so'].']', 'value' => $so_child['item_barcode']));
					echo '</tr>';
				endif;
			endforeach;
		endforeach;
		?>
	</tbody>
	<tfoot>
		<tr style="font-weight: bold;">
			<td style="text-align: right;" colspan="6">Jumlah</td>
			<td style="text-align: right;"><div id="idjmlso"><?php echo $sumSO?></div></td>
			<td style="text-align: right;"><div id="idjmldo"><?php echo $sumDO?></div></td>
		</tr>
	</tfoot>
</table>
</div>
<!-- END OF TABLE -->
<?php
if ($tipe_do == 'Ekspor') :
	?>
	<?php
endif;
?>
<hr>
<div id="idFormBtn" class="form-group">
	<div class="form-group">
		<div class="col-sm-3 col-sm-offset-10 col-xs-12">
			<button type="reset" name="btnReset" class="btn btn-sm btn-default btn-flat">
				<i class="fa fa-refresh"></i> Reset
			</button>
			<button type="submit" name="btnSubmit" class="btn btn-sm btn-primary btn-flat">
				<i class="fa fa-shopping-cart"></i> Submit
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>