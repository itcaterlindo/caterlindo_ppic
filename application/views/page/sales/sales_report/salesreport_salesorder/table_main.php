<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body table-responsive no-padding">
<table id="idTable" class="table table-bordered table-striped table-hover display" style="width:100%; font-size:85%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center; vertical-align: middle" class="all">No.</th>
		<th style="width:20%; text-align:center; vertical-align: middle">No. Sales Order</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Sales Person</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Purchase Order Date</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Planning Delivery Date</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Customer Name</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Status</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Invoice No.</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Delivery Cost</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Total Exc. PPN (Rp)</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Total Inc. PPN (Rp)</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Qty Standart</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Qty Custom</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Total Qty</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Ammount Standart Exc</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Ammount Custom Exc</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Total Ammount Exc</th>
	</tr>
	</thead>
	<tbody>
		<?php 
		$no = 1;
		foreach($resultSO as $SO):
		?>
		<tr>
			<td><?= $no++ ?></td>
			<td><?= $SO['no_salesorder'] ?></td>
			<td><?= $SO['nm_salesperson'] ?></td>
			<td><?= format_date($SO['tgl_so'], 'd-m-Y') ?></td>
			<td><?= format_date($SO['tgl_kirim'], 'd-m-Y') ?></td>
			<td><?= $SO['nm_customer'] ?></td>
			<td><?= $SO['nm_jenis_customer'] ?></td>
			<td><?= str_replace(";","<br> ",$SO['no_invoice']) ?></td>
			<td><?= number_format($SO['jml_ongkir']) ?></td>
			<td><?= number_format($SO['total_harga_exc']) ?></td>
			<td><?= number_format($SO['total_harga_inc']) ?></td>
			<td><?= $SO['item_standart_qty'] ?></td>
			<td><?= $SO['item_custom_qty'] ?></td>
			<td><?= $SO['item_standart_qty'] + $SO['item_custom_qty'] ?></td>
			<td><?= number_format($SO['item_standart_harga']) ?></td>
			<td><?= number_format($SO['item_custom_harga']) ?></td>
			<td><?= number_format($SO['item_standart_harga'] + $SO['item_custom_harga']) ?></td>
		</tr>
		<?php endforeach; ?>

		<tr>
			<td class="text-right" colspan="9">TOTAL RETAIL</td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td><?= number_format($totalRetailExc) ?></td>
			<td><?= number_format($totalRetailInc) ?></td>
			<td><?= number_format($totalQtyStandartRetail) ?></td>
			<td><?= number_format($totalQtyCustomRetail) ?></td>
			<td><?= number_format($totalQtyRetail) ?></td>
			<td><?= number_format($totalAmmountStandartRetail) ?></td>
			<td><?= number_format($totalAmmountCustomRetail) ?></td>
			<td><?= number_format($totalAmmountRetail) ?></td>
		</tr>
		<tr>
			<td class="text-right" colspan="9">TOTAL DISTRIBUTOR</td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td><?= number_format($totalDistributorExc) ?></td>
			<td><?= number_format($totalDistributorInc) ?></td>
			<td><?= number_format($totalQtyStandartDistributor) ?></td>
			<td><?= number_format($totalQtyCustomDistributor) ?></td>
			<td><?= number_format($totalQtyDistributor) ?></td>
			<td><?= number_format($totalAmmountStandartDistributor) ?></td>
			<td><?= number_format($totalAmmountCustomDistributor) ?></td>
			<td><?= number_format($totalAmmountDistributor) ?></td>
		</tr>
		<tr>
			<td class="text-right" colspan="9">TOTAL RESELLER</td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td><?= number_format($totalResellerExc) ?></td>
			<td><?= number_format($totalResellerInc) ?></td>
			<td><?= number_format($totalQtyStandartReseller) ?></td>
			<td><?= number_format($totalQtyCustomReseller) ?></td>
			<td><?= number_format($totalQtyReseller) ?></td>
			<td><?= number_format($totalAmmountStandartReseller) ?></td>
			<td><?= number_format($totalAmmountCustomReseller) ?></td>
			<td><?= number_format($totalAmmountReseller) ?></td>
		</tr>
		<tr class="active">
			<td class="text-center" colspan="9">TOTAL SALES</td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td><?= number_format(array_sum(array_column($resultSO, 'total_harga_exc'))) ?></td>
			<td><?= number_format(array_sum(array_column($resultSO, 'total_harga_inc'))) ?></td>
			<td><?= number_format(array_sum(array_column($resultSO, 'item_standart_qty'))) ?></td>
			<td><?= number_format(array_sum(array_column($resultSO, 'item_custom_qty'))) ?></td>
			<td><?= number_format($totalQty) ?></td>
			<td><?= number_format(array_sum(array_column($resultSO, 'item_standart_harga'))) ?></td>
			<td><?= number_format(array_sum(array_column($resultSO, 'item_custom_harga'))) ?></td>
			<td><?= number_format($totalAmmount) ?></td>
		</tr>
		<tr class="active">
			<td class="text-center" colspan="9">PERSENTASE STANDART & CUSTOM</td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><?= ROUND((array_sum(array_column($resultSO, 'item_standart_qty')) / $totalQty) * 100)."%"  ?></td>
			<td><?= ROUND((array_sum(array_column($resultSO, 'item_custom_qty')) / $totalQty) * 100)."%"  ?></td>
			<td><?= ROUND( ($totalQty / $totalQty) * 100 )."%" ?></td>
			<td><?= ROUND((array_sum(array_column($resultSO, 'item_standart_harga')) / $totalAmmount) * 100)."%"  ?></td>
			<td><?= ROUND((array_sum(array_column($resultSO, 'item_custom_harga')) / $totalAmmount) * 100)."%"  ?></td>
			<td><?= ROUND( ($totalAmmount / $totalAmmount) * 100 )."%" ?></td>
		</tr>
	</tbody>

</table>
</div>

<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"order": [],
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"title" : "Rekap Sales Order",
				"exportOptions": {
					"columns": [0,1,2,3,4,5,6,7,8,9,10,11,12,13]
				}
			}],
		});
	}
</script>