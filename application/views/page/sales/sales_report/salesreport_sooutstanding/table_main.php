<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">

</style>
<div class="box-body table-responsive no-padding">
<table id="idTable" class="table table-bordered table-hover display" style="width:100%; font-size:85%;">
	<thead>
	<tr>
		<th style="text-align:center; vertical-align: middle" class="all">Month</th>
		<th style="text-align:center; vertical-align: middle">SO Date</th>
		<th style="text-align:center; vertical-align: middle">Stuffing Plan</th>
		<th style="text-align:center; vertical-align: middle">No. Salesorder</th>
		<th style="text-align:center; vertical-align: middle">Customer</th>
		<th style="text-align:center; vertical-align: middle">Qty</th>
		<th style="text-align:center; vertical-align: middle">Note</th>
	</tr>
	</thead>
	<tbody>
		<?php foreach($resultOutstanding as $val): ?>
			<tr>
				<td><?= format_date($val['tgl_kirim'], 'Y-M') ?></td>
				<td><?= format_date($val['tgl_so'], 'd/m/Y') ?></td>
				<td><?= format_date($val['tgl_kirim'], 'd/m/Y') ?></td>
				<td><?= $val['no_salesorder'] ?></td>
				<td><?= $val['nm_customer'] ?></td>
				<td><?= $val['item_qty'] ?></td>
				<td><?= $val['note_so'] ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<td class="text-center" colspan="5">Total : </td>
			<td><?= array_sum(array_column($resultOutstanding, 'item_qty')) ?></td>
			<td>&nbsp;</td>
		</tr>
	</tfoot>

</table>
</div>

<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"order": [],
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"title" : "Report Sales Ekspor Outstanding",
				"exportOptions": {
					"columns": [ 0,1,2,3,4,5,6 ]
				}
			}],
		});
	}
</script>