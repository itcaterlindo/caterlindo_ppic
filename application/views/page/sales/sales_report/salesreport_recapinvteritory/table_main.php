<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body table-responsive no-padding">
<table id="idTable" class="table table-bordered display" style="width:100%; font-size:85%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center; vertical-align: middle" >#</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Januari</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Februari</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Maret</th>
		<th style="width:20%; text-align:center; vertical-align: middle">April</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Mei</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Juni</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Juli</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Agustus</th>
		<th style="width:20%; text-align:center; vertical-align: middle">September</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Oktober</th>
		<th style="width:20%; text-align:center; vertical-align: middle">November</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Desember</th>
	</tr>
	</thead>
	<tbody>
		<?php foreach($recap as $key => $value): ?>
			<tr>
				<td><?= $value['lokasi'] ?></td>
				<?php for($i=1; $i<=12; $i++): ?>
				<td><?= number_format($value[$i], 2) ?></td>
				<?php endfor; ?>
			</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<td>Total</td>
			<?php for($i=1; $i<=12; $i++): ?>
				<td><?= number_format(array_sum(array_column($recap, $i)), 2) ?></td>
			<?php endfor; ?>
		</tr>
	</tfoot>
</table>
</div>

<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"order": [],
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"title" : "Rekap Inv Teritory",
				"exportOptions": {
					"columns": [0,1,2,3,4,5,6,7,8,9,10,11,12]
				}
			}],
		});
	}
</script>