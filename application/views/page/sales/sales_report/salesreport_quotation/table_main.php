<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body table-responsive no-padding">
<table id="idTable" class="table table-bordered table-hover display" style="width:100%; font-size:85%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center; vertical-align: middle" class="all">No.</th>
		<th style="width:20%; text-align:center; vertical-align: middle">No. Quotation</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Quotation Date</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Sales Person</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Company Name</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Status</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Date SO</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Price Exclude PPN (Rp)</th>
		<th style="width:20%; text-align:center; vertical-align: middle">Total Qty</th>
		<th style="width:20%; text-align:center; vertical-align: middle">No. Sales Order</th>
	</tr>

	</thead>
	<tbody>
		<?php 
		$no = 1;
		foreach($resultQO as $QO):
		?>
		<tr class="<?= $QO['no_salesorder'] == null ? "danger" : "info" ?>">
			<td><?= $no++ ?></td>
			<td><?= $QO['no_quotation'] ?></td>
			<td><?= format_date($QO['tgl_quotation'], 'd-m-Y') ?></td>
			<td><?= $QO['nm_salesperson'] ?></td>
			<td><?= $QO['nm_customer'] ?></td>
			<td><?= $QO['nm_jenis_customer'] ?></td>
			<td><?= format_date($QO['tgl_so'], 'd-m-Y') ?></td>
			<td><?= number_format($QO['total_harga_exc']) ?></td>
			<td><?= $QO['total_qty'] ?></td>
			<td><?= $QO['no_salesorder'] ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>

	<tfoot>
		<tr>
			<td class="text-right" colspan="7">TOTAL</td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td style="display: none;"></td>
			<td><?= number_format($totalPriceExc) ?></td>
			<td><?= number_format($totalQty) ?></td>
			<td style="display: none;"></td>
		</tr>
		<tr>
			<td>Persentase SO / QO</td>
			<td colspan="9"><?= $jadiSO." / ".$totalQO." = ".$persentaseSO."%" ?></td>
		</tr>
		<tr>
			<td>Persentase Price Exclude</td>
			<td colspan="9"><?= number_format($totalJadiPriceExc)." / ".number_format($totalPriceExc)." = ".number_format($persentasePrice)."%" ?></td>
		</tr>
		<tr>
			<td>Persentase Qty</td>
			<td colspan="9"><?= $totalJadiQty." / ".$totalQty." = ".$persentaseQty."%" ?></td>
		</tr>
	</tfoot>

</table>
</div>

<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"order": [],
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"title" : "Rekap Sales Quotation",
				"exportOptions": {
					"columns": [ 0,1,2,3,4,5,6,7,8,9]
				}
			}],
		});
	}
</script>