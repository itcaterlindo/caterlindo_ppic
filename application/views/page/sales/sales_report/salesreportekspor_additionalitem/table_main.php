<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body no-padding">
		<div class="col-md-12 table-responsive">
			<table id="idTable" class="table table-bordered table-hover display" style="width:100%; font-size:85%;">
				<tbody>
					<?php 
						foreach($resultAdditional as $key => $val): 
						$countDetailInv[] = count($val);
					?>
						<tr>
							<td><?= $key.";"."<br><br>".$val[0]['no_po'].";"."<br><br>".format_date($val[0]['delivery_date'], "d-m-Y") ?></td>
							<?php for($i = 0; $i < max($countDetailInv); $i++): ?>
							<td><?= 
								isset($val[$i]) ? $val[$i]['item_code'].";"."<br><br>".
								$val[$i]['item_desc'].";"."<br><br>".
								$val[$i]['item_note_do'].";"."<br><br>".
								"Qty ".$val[$i]['qty'] : "" ?>
							</td>
							<?php endfor; ?>
						</tr>
					<?php endforeach; ?>
				</tbody>
				<thead>
					<tr>
						<th>Invoice</th>
						<?php for($i = 0; $i < max($countDetailInv); $i++): ?>
							<th>#</th>
						<?php endfor; ?>
					</tr>
				</thead>
			</table>
		</div>
</div>

<script type="text/javascript">
	
	$(document).ready(function(){
		render_dt('#idTable');
	});

	function render_dt(table_elem) {
		var countColumn = <?= max($countDetailInv) ?>;
		var column = [];
		for(var i=0; i < countColumn; i ++){
			column.push(i);
		}

		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"searching": false,
			"order": [],
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"footer" : true,
				"title" : "Report Ekspor Additional Item",
				"exportOptions": {
					"columns": column
				}
			}],
		});
	}

</script>