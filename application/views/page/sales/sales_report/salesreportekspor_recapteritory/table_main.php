<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body no-padding">
	<!-- Ammount Dollar -->
	<div class="row">
		<div class="col-md-6" style="height:10vh; width:20vw">
			<canvas id="barChart"></canvas>
		</div>
		<div class="col-md-6">
			<table id="idTable" class="table table-bordered table-hover display" style="width:100%; font-size:85%;">
				<thead>
				<tr>
					<th style="text-align:center; vertical-align: middle">Teritory</th>
					<th style="text-align:center; vertical-align: middle">Ammount Distributor</th>
				</tr>
				</thead>
				<tbody>
					<?php foreach($sumTeritory as $teritory): ?>
					<tr>
						<td><?= $teritory['teritory'] ?></td>
						<td><?= number_format($teritory['ammount_distributor'],2) ?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot>
					<tr class="active"> 
						<td class="text-center" colspan="1">Total</td>
						<td style="text-align:center; vertical-align: middle"><?= number_format(array_sum(array_column($sumTeritory, "ammount_distributor")),2) ?></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<br>
	<br>
	<!-- Ammount in 20ft -->
	<div class="row">
		<div class="col-md-6" style="height:10vh; width:20vw">
			<canvas id="barChart20ft"></canvas>
		</div>
		<div class="col-md-6">
			<table id="idTable20ft" class="table table-bordered table-hover display" style="width:100%; font-size:85%;">
				<thead>
				<tr>
					<th style="text-align:center; vertical-align: middle">Teritory</th>
					<th style="text-align:center; vertical-align: middle">In 20ft</th>
				</tr>
				</thead>
				<tbody>
					<?php foreach($sumTeritory as $teritory): ?>
					<tr>
						<td><?= $teritory['teritory'] ?></td>
						<td><?= number_format($teritory['container_20ft']) ?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot>
					<tr class="active"> 
						<td class="text-center" colspan="1">Total</td>
						<td style="text-align:center; vertical-align: middle"><?= number_format(array_sum(array_column($sumTeritory, "container_20ft"))) ?></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	$(document).ready(function(){
		render_chart_ammount('#barChart');
		render_dt('#idTable');
		render_chart_20ft('#barChart20ft');
		render_dt('#idTable20ft');
	});

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"searching": false,
			"order": [],
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"footer" : true,
				"title" : "Report Ekspor Recap Teritory",
				"exportOptions": {
					"columns": [0,1]
				}
			}],
		});
	}

	function render_chart_ammount(element) {
		var source = <?= json_encode($sumTeritory) ?>,
			arrAmmount = [],
			arrTeritory = [];
		for(var item of source){
			arrTeritory.push(item.teritory);
			arrAmmount.push(item.ammount_distributor);
		}
		// Setting automatic background color
		const background_color = [];
		for(i = 0; i < arrTeritory.length; i++){
			const r = Math.floor(Math.random() * 255);
			const g = Math.floor(Math.random() * 255);
			const b = Math.floor(Math.random() * 255);
			background_color.push('rgba('+r+', '+g+', '+b+', 0.5)');
		}

		var ctx = $(element), 
		setup = {
				type: 'pie',
				data: {
					labels: arrTeritory,
					datasets: [{ 
						data: arrAmmount,
						backgroundColor: background_color,
					}]
				},
				options: {
					title: {
						display: true,
						text: 'Data Recap Teritory Ekspor'
					},
					options: {  
						responsive: true,
					}
				}
			};
		var myBar = new Chart(ctx, setup);
	}

	function render_chart_20ft(element) {
		var source = <?= json_encode($sumTeritory) ?>,
			arr20ft = [],
			arrTeritory = [];
		for(var item of source){
			arrTeritory.push(item.teritory);
			arr20ft.push(item.container_20ft);
		}
		// Setting automatic background color
		const background_color = [];
		for(i = 0; i < arrTeritory.length; i++){
			const r = Math.floor(Math.random() * 255);
			const g = Math.floor(Math.random() * 255);
			const b = Math.floor(Math.random() * 255);
			background_color.push('rgba('+r+', '+g+', '+b+', 0.5)');
		}

		var ctx = $(element), 
		setup = {
				type: 'pie',
				data: {
					labels: arrTeritory,
					datasets: [{ 
						data: arr20ft,
						backgroundColor: background_color,
					}]
				},
				options: {
					title: {
						display: true,
						text: 'Data Recap Teritory Ekspor'
					},
					options: {  
						responsive: true,
					}
				}
			};
		var myBar = new Chart(ctx, setup);
	}

</script>