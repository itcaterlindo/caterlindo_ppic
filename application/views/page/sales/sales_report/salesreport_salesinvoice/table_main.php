<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body table-responsive no-padding">
<table id="idTable" class="table table-bordered table-striped table-hover display" style="width:100%; font-size:85%;">
	<thead>
	<tr>
		<th style="text-align:center; vertical-align: middle">No.</th>
		<th style="text-align:center; vertical-align: middle">No. Sales Order</th>
		<th style="text-align:center; vertical-align: middle">Sales Person</th>
		<th style="text-align:center; vertical-align: middle">Purchase Order Date</th>
		<th style="text-align:center; vertical-align: middle">Planning Delivery Date</th>
		<th style="text-align:center; vertical-align: middle">Date Invoice</th>
		<th style="text-align:center; vertical-align: middle">Customer Code</th>
		<th style="text-align:center; vertical-align: middle">Customer Name</th>
		<th style="text-align:center; vertical-align: middle">Invoice No.</th>
		<th style="text-align:center; vertical-align: middle">Ammount Exclude PPN</th>
		<th style="text-align:center; vertical-align: middle">Ammount Include PPN</th>
		<th style="text-align:center; vertical-align: middle">Status Invoice</th>
		<th style="text-align:center; vertical-align: middle">DO Ket</th>
	</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		// echo json_encode($resultInvoice);
		foreach($resultInvoice as $inv):
			if($inv['status_invoice'] == 'Lunas'){
				$tgl_in = $inv['tgl_invoice'];
			}
			if($inv['status_invoice'] == 'DP'){
				$tgl_in = $inv['tgl_dp'];
			}
			elseif($inv['status_invoice'] == 'Termin'){
				$tgl_in = $inv['tgl_termin'];
			}
		?>
		<tr>
			<td><?= $no++ ?></td>
			<td><?= $inv['no_salesorder'] ?></td>
			<td><?= $inv['nm_salesperson'] ?></td>
			<td><?= format_date($inv['tgl_so'], 'd-m-Y') ?></td>
			<td><?= format_date($inv['tgl_kirim'], 'd-m-Y') ?></td>
			<td><?= format_date($tgl_in, 'd-m-Y') ?></td>
			<td><?= $inv['code_customer'] ?></td>
			<td><?= $inv['nm_customer'] ?></td>
			<td><?= $inv['no_invoice'] ?></td>
			<td><?= number_format($inv['total_harga_exc']) ?></td>
			<td><?= number_format($inv['total_harga_inc']) ?></td>
			<td><?= $inv['status_invoice'] ?></td>
			<td><?= $inv['do_ket'] ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>

	<tfoot>
		<tr>
			<td class="text-center" colspan="9">Total</td>
			<td> <?= number_format(array_sum(array_column($resultInvoice, "total_harga_exc"))) ?> </td>
			<td> <?= number_format(array_sum(array_column($resultInvoice, "total_harga_inc"))) ?> </td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</tfoot>

</table>
</div>

<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"order": [],
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"title" : "Rekap Sales Sales Order Invoice",
				"exportOptions": {
					"columns": [ 0,1,2,3,4,5,6,7,8,9,10,11,12]
				}
			}],
		});
	}
</script>