<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body no-padding">
	<div class="row">
		<div class="col-md-12" style="height:200px">
			<canvas id="chartAmmount"></canvas>
		</div>
		<div class="col-md-12 table-responsive">
			<table id="idTableAmmount" class="table table-bordered display" style="width:100%; font-size:85%;">
				<thead>
				<tr>
					<th style="text-align:center; vertical-align: middle">In USD</th>
					<th style="text-align:center; vertical-align: middle">Januari</th>
					<th style="text-align:center; vertical-align: middle">Februari</th>
					<th style="text-align:center; vertical-align: middle">Maret</th>
					<th style="text-align:center; vertical-align: middle">April</th>
					<th style="text-align:center; vertical-align: middle">Mei</th>
					<th style="text-align:center; vertical-align: middle">Juni</th>
					<th style="text-align:center; vertical-align: middle">Juli</th>
					<th style="text-align:center; vertical-align: middle">Agustus</th>
					<th style="text-align:center; vertical-align: middle">September</th>
					<th style="text-align:center; vertical-align: middle">Oktober</th>
					<th style="text-align:center; vertical-align: middle">November</th>
					<th style="text-align:center; vertical-align: middle">Desember</th>
					<th style="text-align:center; vertical-align: middle">Total</th>
				</tr>
				</thead>
				<tbody>
					<?php 
					foreach($resultTipeTeritory as $header): ?>
						<tr class="active"> 
							<td><?= $tahunAmmount ?></td>
							<td><?= $sourceChartAmmount[$tahunAmmount][0] = number_format( array_sum( array_column(array_column($header, 'januari'), 'ammount_distributor') ) , 2) ?></td>
							<td><?= $sourceChartAmmount[$tahunAmmount][1] = number_format( array_sum( array_column(array_column($header, 'februari'), 'ammount_distributor') ) , 2) ?></td>
							<td><?= $sourceChartAmmount[$tahunAmmount][2] = number_format( array_sum( array_column(array_column($header, 'maret'), 'ammount_distributor') ) , 2) ?></td>
							<td><?= $sourceChartAmmount[$tahunAmmount][3] = number_format( array_sum( array_column(array_column($header, 'april'), 'ammount_distributor') ) , 2) ?></td>
							<td><?= $sourceChartAmmount[$tahunAmmount][4] = number_format( array_sum( array_column(array_column($header, 'mei'), 'ammount_distributor') ) , 2) ?></td>
							<td><?= $sourceChartAmmount[$tahunAmmount][5] = number_format( array_sum( array_column(array_column($header, 'juni'), 'ammount_distributor') ) , 2) ?></td>
							<td><?= $sourceChartAmmount[$tahunAmmount][6] = number_format( array_sum( array_column(array_column($header, 'juli'), 'ammount_distributor') ) , 2) ?></td>
							<td><?= $sourceChartAmmount[$tahunAmmount][7] = number_format( array_sum( array_column(array_column($header, 'agustus'), 'ammount_distributor') ) , 2) ?></td>
							<td><?= $sourceChartAmmount[$tahunAmmount][8] = number_format( array_sum( array_column(array_column($header, 'september'), 'ammount_distributor') ) , 2) ?></td>
							<td><?= $sourceChartAmmount[$tahunAmmount][9] = number_format( array_sum( array_column(array_column($header, 'oktober'), 'ammount_distributor') ) , 2) ?></td>
							<td><?= $sourceChartAmmount[$tahunAmmount][10] = number_format( array_sum( array_column(array_column($header, 'november'), 'ammount_distributor') ) , 2) ?></td>
							<td><?= $sourceChartAmmount[$tahunAmmount][11] = number_format( array_sum( array_column(array_column($header, 'desember'), 'ammount_distributor') ) , 2) ?></td>
							<td>
								<?php 
								// Summary per tahun (Tambah per bulan)
								$sumTahunAmmount = array_sum(array_column(array_column($header, 'januari'), 'ammount_distributor')) + 
								array_sum(array_column(array_column($header, 'februari'), 'ammount_distributor')) +
								array_sum(array_column(array_column($header, 'maret'), 'ammount_distributor')) +
								array_sum(array_column(array_column($header, 'april'), 'ammount_distributor')) +
								array_sum(array_column(array_column($header, 'mei'), 'ammount_distributor')) +
								array_sum(array_column(array_column($header, 'juni'), 'ammount_distributor')) + 
								array_sum(array_column(array_column($header, 'juli'), 'ammount_distributor')) +
								array_sum(array_column(array_column($header, 'agustus'), 'ammount_distributor')) +
								array_sum(array_column(array_column($header, 'september'), 'ammount_distributor')) +
								array_sum(array_column(array_column($header, 'oktober'), 'ammount_distributor')) +
								array_sum(array_column(array_column($header, 'november'), 'ammount_distributor')) + 
								array_sum(array_column(array_column($header, 'desember'), 'ammount_distributor'));
								echo number_format($sumTahunAmmount, 2); ?>
							</td>
						</tr>
						<?php foreach($header as $result): ?>
							<tr>
								<td><?= isset($result['tipe_teritory']) ? strtoupper($result['tipe_teritory']) : null ?></td>
								<td><?= isset($result['januari']) ? number_format($result['januari']['ammount_distributor'], 2) : 0 ?></td>
								<td><?= isset($result['februari']) ? number_format($result['februari']['ammount_distributor'], 2) : 0  ?></td>
								<td><?= isset($result['maret']) ? number_format($result['maret']['ammount_distributor'], 2) : 0 ?></td>
								<td><?= isset($result['april']) ? number_format($result['april']['ammount_distributor'], 2) : 0 ?></td>
								<td><?= isset($result['mei']) ? number_format($result['mei']['ammount_distributor'], 2) : 0 ?></td>
								<td><?= isset($result['juni']) ? number_format($result['juni']['ammount_distributor'], 2) : 0 ?></td>
								<td><?= isset($result['juli']) ? number_format($result['juli']['ammount_distributor'], 2) : 0 ?></td>
								<td><?= isset($result['agustus']) ? number_format($result['agustus']['ammount_distributor'], 2) : 0 ?></td>
								<td><?= isset($result['september']) ? number_format($result['september']['ammount_distributor'], 2) : 0 ?></td>
								<td><?= isset($result['oktober']) ? number_format($result['oktober']['ammount_distributor'], 2) : 0 ?></td>
								<td><?= isset($result['november']) ? number_format($result['november']['ammount_distributor'], 2) : 0 ?></td>
								<td><?= isset($result['desember']) ? number_format($result['desember']['ammount_distributor'], 2) : 0 ?></td>
								<td><?= isset($result['total']) ? number_format($result['total']['ammount_distributor'], 2) : 0 ?></td>
							</tr>
						<?php endforeach; ?>
					<?php $tahunAmmount++; endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
	<br>
	<br>
	<div class="row">
		<div class="col-md-12" style="height:200px">
			<canvas id="chart20ft"></canvas>
		</div>
		<div class="col-md-12 table-responsive">
			<table id="idTable20ft" class="table table-bordered display" style="width:100%; font-size:85%;">
				<thead>
				<tr>
					<th style="text-align:center; vertical-align: middle">In 20ft</th>
					<th style="text-align:center; vertical-align: middle">Januari</th>
					<th style="text-align:center; vertical-align: middle">Februari</th>
					<th style="text-align:center; vertical-align: middle">Maret</th>
					<th style="text-align:center; vertical-align: middle">April</th>
					<th style="text-align:center; vertical-align: middle">Mei</th>
					<th style="text-align:center; vertical-align: middle">Juni</th>
					<th style="text-align:center; vertical-align: middle">Juli</th>
					<th style="text-align:center; vertical-align: middle">Agustus</th>
					<th style="text-align:center; vertical-align: middle">September</th>
					<th style="text-align:center; vertical-align: middle">Oktober</th>
					<th style="text-align:center; vertical-align: middle">November</th>
					<th style="text-align:center; vertical-align: middle">Desember</th>
					<th style="text-align:center; vertical-align: middle">Total</th>
				</tr>
				</thead>
				<tbody>
					<?php foreach($resultTipeTeritory as $header): ?>
						<tr class="active"> 
							<td><?= $tahun20ft ?></td>
							<td><?= $sourceChart20ft[$tahun20ft][0] = number_format( array_sum( array_column(array_column($header, 'januari'), 'container_20ft')) ) ?></td>
							<td><?= $sourceChart20ft[$tahun20ft][1] = number_format( array_sum( array_column(array_column($header, 'februari'), 'container_20ft')) ) ?></td>
							<td><?= $sourceChart20ft[$tahun20ft][2] = number_format( array_sum( array_column(array_column($header, 'maret'), 'container_20ft')) ) ?></td>
							<td><?= $sourceChart20ft[$tahun20ft][3] = number_format( array_sum( array_column(array_column($header, 'april'), 'container_20ft')) ) ?></td>
							<td><?= $sourceChart20ft[$tahun20ft][4] = number_format( array_sum( array_column(array_column($header, 'mei'), 'container_20ft')) ) ?></td>
							<td><?= $sourceChart20ft[$tahun20ft][5] = number_format( array_sum( array_column(array_column($header, 'juni'), 'container_20ft')) ) ?></td>
							<td><?= $sourceChart20ft[$tahun20ft][6] = number_format( array_sum( array_column(array_column($header, 'juli'), 'container_20ft')) ) ?></td>
							<td><?= $sourceChart20ft[$tahun20ft][7] = number_format( array_sum( array_column(array_column($header, 'agustus'), 'container_20ft')) ) ?></td>
							<td><?= $sourceChart20ft[$tahun20ft][8] = number_format( array_sum( array_column(array_column($header, 'september'), 'container_20ft')) ) ?></td>
							<td><?= $sourceChart20ft[$tahun20ft][9] = number_format( array_sum( array_column(array_column($header, 'oktober'), 'container_20ft')) ) ?></td>
							<td><?= $sourceChart20ft[$tahun20ft][10] = number_format( array_sum( array_column(array_column($header, 'november'), 'container_20ft')) ) ?></td>
							<td><?= $sourceChart20ft[$tahun20ft][11] = number_format( array_sum( array_column(array_column($header, 'desember'), 'container_20ft')) ) ?></td>
							<td>
								<?php 
								// Summary per tahun (Tambah per bulan)
								$sumTahun20ft = array_sum(array_column(array_column($header, 'januari'), 'container_20ft')) + 
								array_sum(array_column(array_column($header, 'februari'), 'container_20ft')) +
								array_sum(array_column(array_column($header, 'maret'), 'container_20ft')) +
								array_sum(array_column(array_column($header, 'april'), 'container_20ft')) +
								array_sum(array_column(array_column($header, 'mei'), 'container_20ft')) +
								array_sum(array_column(array_column($header, 'juni'), 'container_20ft')) + 
								array_sum(array_column(array_column($header, 'juli'), 'container_20ft')) +
								array_sum(array_column(array_column($header, 'agustus'), 'container_20ft')) +
								array_sum(array_column(array_column($header, 'september'), 'container_20ft')) +
								array_sum(array_column(array_column($header, 'oktober'), 'container_20ft')) +
								array_sum(array_column(array_column($header, 'november'), 'container_20ft')) + 
								array_sum(array_column(array_column($header, 'desember'), 'container_20ft'));
								echo number_format($sumTahun20ft); ?>
							</td>
						</tr>
						<?php foreach($header as $result): ?>
							<tr>
								<td><?= isset($result['tipe_teritory']) ? strtoupper($result['tipe_teritory']) : null ?></td>
								<td><?= isset($result['januari']) ? number_format($result['januari']['container_20ft']) : 0 ?></td>
								<td><?= isset($result['februari']) ? number_format($result['februari']['container_20ft']) : 0  ?></td>
								<td><?= isset($result['maret']) ? number_format($result['maret']['container_20ft']) : 0 ?></td>
								<td><?= isset($result['april']) ? number_format($result['april']['container_20ft']) : 0 ?></td>
								<td><?= isset($result['mei']) ? number_format($result['mei']['container_20ft']) : 0 ?></td>
								<td><?= isset($result['juni']) ? number_format($result['juni']['container_20ft']) : 0 ?></td>
								<td><?= isset($result['juli']) ? number_format($result['juli']['container_20ft']) : 0 ?></td>
								<td><?= isset($result['agustus']) ? number_format($result['agustus']['container_20ft']) : 0 ?></td>
								<td><?= isset($result['september']) ? number_format($result['september']['container_20ft']) : 0 ?></td>
								<td><?= isset($result['oktober']) ? number_format($result['oktober']['container_20ft']) : 0 ?></td>
								<td><?= isset($result['november']) ? number_format($result['november']['container_20ft']) : 0 ?></td>
								<td><?= isset($result['desember']) ? number_format($result['desember']['container_20ft']) : 0 ?></td>
								<td><?= isset($result['total']) ? number_format($result['total']['container_20ft']) : 0 ?></td>
							</tr>
						<?php endforeach; ?>
					<?php $tahun20ft++; endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>

</div>

<script type="text/javascript">
	
	$(document).ready(function(){
		render_chart_ammount('#chartAmmount');
		render_chart_20ft('#chart20ft');
		render_dt('#idTableAmmount');
		render_dt('#idTable20ft');
	});

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"searching": false,
			"order": [],
			"ordering": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"footer" : true,
				"title" : "Report Ekspor Comparassion Simply x Pret",
				"exportOptions": {
					"columns": [0,1,2,3,4,5,6,7,8,9,10,11,12,13]
				}
			}],
		});
	}

	function render_chart_ammount(element) {
		var sourceData = <?= json_encode($sourceChartAmmount) ?>;
		var output = [];
		// Mecah string
		sourceData = JSON.parse(
			JSON.stringify(sourceData).replace(/,(?!["{}[\]])/g, "")
		)

		// Push to output
		for (const [key, value] of Object.entries(sourceData)) {
			// Setting automatic background color
			var background_color;
			var r = Math.floor(Math.random() * 255);
			var g = Math.floor(Math.random() * 255);
			var b = Math.floor(Math.random() * 255);
			background_color = 'rgba('+r+', '+g+', '+b+', 0.5)';
			output.push({
					'label' : key, 
					'data': Object.values(value),
					borderWidth: 2,
					pointRadius: 2,
					pointHoverRadius: 2,
					fill: false,
					borderColor: background_color,
					backgroundColor: background_color,
					pointBackgroundColor: background_color,
					pointBorderColor: background_color,
					pointHoverBackgroundColor: background_color,
					pointHoverBorderColor: background_color,
			});
		}
		var ctx = $(element), 
		setup = {
				type: 'line',
				data: {
					labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
					datasets: output
				},
				options: {
					title: {
						display: true,
						text: 'Compare Simply vs Pret'
					},
					responsive:true,
    				maintainAspectRatio: false,
					tooltips: {
						callbacks: {
							"label": (tooltipItem, data) => {
								// Tooltips custom ketika ada comma separator decimal
								var label = data.datasets[tooltipItem.datasetIndex].label || '';
								return label + ' ' + (Math.round(tooltipItem.yLabel * 100) / 100).toFixed(2);
							}
						}
					},
				}
			};
		var myBar = new Chart(ctx, setup);
	}

	function render_chart_20ft(element) {
		var sourceData = <?= json_encode($sourceChart20ft) ?>;
		var output = [];
		// Push to output
		for (const [key, value] of Object.entries(sourceData)) {
			// Setting automatic background color
			var background_color;
			var r = Math.floor(Math.random() * 255);
			var g = Math.floor(Math.random() * 255);
			var b = Math.floor(Math.random() * 255);
			background_color = 'rgba('+r+', '+g+', '+b+', 0.5)';
			output.push({
					'label' : key, 
					'data': Object.values(value),
					borderWidth: 2,
					pointRadius: 2,
					pointHoverRadius: 2,
					fill: false,
					borderColor: background_color,
					backgroundColor: background_color,
					pointBackgroundColor: background_color,
					pointBorderColor: background_color,
					pointHoverBackgroundColor: background_color,
					pointHoverBorderColor: background_color,
			});
		}
		var ctx = $(element), 
		setup = {
				type: 'line',
				data: {
					labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
					datasets: output
				},
				options: {
					title: {
						display: true,
						text: 'Compare Simply vs Pret'
					},
					responsive:true,
    				maintainAspectRatio: false,
					tooltips: {
						callbacks: {
							"label": (tooltipItem, data) => {
								// Tooltips custom ketika ada comma separator decimal
								var label = data.datasets[tooltipItem.datasetIndex].label || '';
								return label + ' ' + (Math.round(tooltipItem.yLabel * 100) / 100).toFixed(2);
							}
						}
					},
				}
			};
		var myBar = new Chart(ctx, setup);
	}

</script>