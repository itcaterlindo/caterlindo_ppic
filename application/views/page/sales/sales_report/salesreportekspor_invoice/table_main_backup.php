<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body table-responsive no-padding">
<table id="idTable" class="table table-bordered table-striped table-hover display" style="width:100%; font-size:85%;">
	<thead>
	<tr>
		<th style="text-align:center; vertical-align: middle">Month</th>
		<th style="text-align:center; vertical-align: middle">PO Date</th>
		<th style="text-align:center; vertical-align: middle">Stuffing Plan</th>
		<th style="text-align:center; vertical-align: middle">Actual Shipment</th>
		<th style="text-align:center; vertical-align: middle">Code PO</th>
		<th style="text-align:center; vertical-align: middle">Invoice No.</th>
		<th style="text-align:center; vertical-align: middle">Qty</th>
		<th style="text-align:center; vertical-align: middle">Qty Pokok</th>
		<th style="text-align:center; vertical-align: middle">Progress Item</th>
		<th style="text-align:center; vertical-align: middle">Ammount PO</th>
		<th style="text-align:center; vertical-align: middle">Ammount Customer</th>
		<th style="text-align:center; vertical-align: middle">Ammount Finance 25%</th>
		<th style="text-align:center; vertical-align: middle">Ammount Discount Less 36.5%</th>
		<th style="text-align:center; vertical-align: middle">Lead Time</th>
		<th style="text-align:center; vertical-align: middle">Remarks</th>
		<th style="text-align:center; vertical-align: middle">CBM</th>
		<th style="text-align:center; vertical-align: middle">IN 20FT</th>
		<th style="text-align:center; vertical-align: middle">Shipping</th>
		<th style="text-align:center; vertical-align: middle">No. Container/Seal</th>
		<th style="text-align:center; vertical-align: middle">Vessel</th>
		<th style="text-align:center; vertical-align: middle">ETD SUB</th>
		<th style="text-align:center; vertical-align: middle">ETD SIN</th>
		<th style="text-align:center; vertical-align: middle">ETA</th>
		<th style="text-align:center; vertical-align: middle">Container Arrived</th>
		<th style="text-align:center; vertical-align: middle">No. B/L</th>
		<th style="text-align:center; vertical-align: middle">DO Ket</th>
		<th style="text-align:center; vertical-align: middle">Provinsi</th>
	</tr>
	</thead>
	<tbody>
		<?php foreach($resultInvoice as $inv): ?>
		<tr>
			<td><?= $inv['bulan'] ?></td>
			<td><?= format_date($inv['tgl_so'], 'd-m-Y') ?></td>
			<td><?= format_date($inv['tgl_kirim'], 'd-m-Y') ?></td>
			<td><?= format_date($inv['tgl_stuffing'], 'd-m-Y') ?></td>
			<td><?= $inv['no_po'] ?></td>
			<td><?= $inv['no_invoice'] ?></td>
			<td><?= $inv['qty'] ?></td>
			<td><?= $inv['qty_pokok'] ?></td>
			<td><?= "Progress" ?></td>
			<td><?= number_format($inv['ammount_po'], 2, '.', ',') ?></td>
			<td><?= number_format($inv['ammount_customer'], 2, '.', ',') ?></td>
			<td><?= number_format($inv['ammount_finance'], 2, '.', ',') ?></td>
			<td><?= number_format($inv['ammount_distributor'], 2, '.', ',') ?></td>
			<td><?= $inv['lead_time'] ?></td>
			<td><?= $inv['tipe_container'] ?></td>
			<td><?= number_format($inv['cbm'], 2, '.', '') ?></td>
			<td><?= "Progress" ?></td>
			<td><?= $inv['nm_jasakirim'] ?></td>
			<td><?= $inv['container_number']." / ".$inv['seal_number'] ?></td>
			<td><?= $inv['vessel'] ?></td>
			<td><?= $inv['etd_sub'] ?></td>
			<td><?= $inv['etd_sin'] ?></td>
			<td><?= $inv['eta'] ?></td>
			<td><?= $inv['container_arrived'] ?></td>
			<td><?= $inv['no_bl'] ?></td>
			<td><?= $inv['do_ket'] ?></td>
			<td><?= $inv['nm_provinsi'] ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>

	<tfoot>
		<tr>
			<td class="text-center" colspan="6">Total</td>
			<td> <?= array_sum(array_column($resultInvoice, "qty")) ?> </td>
			<td> <?= array_sum(array_column($resultInvoice, "qty_pokok")) ?> </td>
			<td>&nbsp;</td>
			<td> <?= number_format(array_sum(array_column($resultInvoice, "ammount_po")), 2, ".",",") ?> </td>
			<td> <?= number_format(array_sum(array_column($resultInvoice, "ammount_customer")), 2, ".",",") ?> </td>
			<td> <?= number_format(array_sum(array_column($resultInvoice, "ammount_finance")), 2, ".",",") ?> </td>
			<td> <?= number_format(array_sum(array_column($resultInvoice, "ammount_distributor")), 2, ".",",") ?> </td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</tfoot>

</table>
</div>

<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"order": [],
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"title" : "Rekap Sales Sales Order Invoice",
				"exportOptions": {
					"columns": [ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]
				}
			}],
		});
	}
</script>