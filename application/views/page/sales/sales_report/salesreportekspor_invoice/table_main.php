<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body table-responsive no-padding">
<table id="idTable" class="table table-bordered table-striped table-hover display" style="width:100%; font-size:85%;">
	<thead>
	<tr>
		<th style="text-align:center; vertical-align: middle">Month</th>
		<th style="text-align:center; vertical-align: middle">PO Date</th>
		<th style="text-align:center; vertical-align: middle">Stuffing Plan</th>
		<th style="text-align:center; vertical-align: middle">Actual Shipment</th>
		<th style="text-align:center; vertical-align: middle">Code PO</th>
		<th style="text-align:center; vertical-align: middle">Invoice No.</th>
		<th style="text-align:center; vertical-align: middle">Qty</th>
		<th style="text-align:center; vertical-align: middle">Ammount PO</th>
		<th style="text-align:center; vertical-align: middle">Ammount Customer</th>
		<th style="text-align:center; vertical-align: middle">Ammount Finance</th>
		<th style="text-align:center; vertical-align: middle">Ammount Distributor</th>
		<th style="text-align:center; vertical-align: middle">Lead Time</th>
		<th style="text-align:center; vertical-align: middle">Remarks</th>
		<th style="text-align:center; vertical-align: middle">CBM</th>
		<th style="text-align:center; vertical-align: middle">In 20ft</th>
		<th style="text-align:center; vertical-align: middle">Shipping</th>
		<th style="text-align:center; vertical-align: middle">No. Container/Seal</th>
		<th style="text-align:center; vertical-align: middle">Vessel</th>
		<th style="text-align:center; vertical-align: middle">ETD Sub</th>
		<th style="text-align:center; vertical-align: middle">ETD Sin</th>
		<th style="text-align:center; vertical-align: middle">ETA</th>
		<th style="text-align:center; vertical-align: middle">Container Arrived</th>
		<th style="text-align:center; vertical-align: middle">No. B/L</th>
		<th style="text-align:center; vertical-align: middle">Negara-Provinsi</th>
		<th style="text-align:center; vertical-align: middle">DO Note</th>
	</tr>
	</thead>
	<tbody>
		<?php foreach($resultInvoice as $inv): ?>
		<tr>
			<td><?= $inv['month'] ?></td>
			<td><?= format_date($inv['po_date'], 'd-m-Y') ?></td>
			<td><?= format_date($inv['stuffing_plan'], 'd-m-Y') ?></td>
			<td><?= format_date($inv['actual_shipment'], 'd-m-Y') ?></td>
			<td><?= $inv['code_po'] ?></td>
			<td><?= $inv['no_invoice'] ?></td>
			<td><?= $inv['qty'] ?></td>
			<td><?= number_format($inv['ammount_po'], 2, '.', ',') ?></td>
			<td><?= number_format($inv['ammount_customer'], 2, '.', ',') ?></td>
			<td><?= number_format($inv['ammount_finance'], 2, '.', ',') ?></td>
			<td><?= number_format($inv['ammount_distributor'], 2, '.', ',') ?></td>
			<td><?= $inv['lead_time'] ?></td>
			<td><?= $inv['remarks'] ?></td>
			<td><?= $inv['cbm'] ?></td>
			<td><?= !empty($inv['container_20ft']) ? $inv['container_20ft'] : 0 ?></td>
			<td><?= $inv['shipping'] ?></td>
			<td><?= $inv['no_container']." / ".$inv['remarks']." / ".$inv['no_seal'] ?></td>
			<td><?= $inv['vessel'] ?></td>
			<td><?= format_date($inv['etd_sub'], 'd-m-Y') ?></td>
			<td><?= format_date($inv['etd_sin'], 'd-m-Y') ?></td>
			<td><?= format_date($inv['eta'], 'd-m-Y') ?></td>
			<td><?= format_date($inv['container_arrived'], 'H:i') ?></td>
			<td><?= $inv['no_bl'] ?></td>
			<td><?= $inv['nm_negara']."-".$inv['nm_provinsi'] ?></td>
			<td><?= $inv['do_ket'] ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr> 
			<td class="text-center" colspan="6">Total</td>
			<td> <?= array_sum(array_column($resultInvoice, "qty")) ?> </td>
			<td> <?= number_format(array_sum(array_column($resultInvoice, "ammount_po")), 2, ".",",") ?> </td>
			<td> <?= number_format(array_sum(array_column($resultInvoice, "ammount_customer")), 2, ".",",") ?> </td>
			<td> <?= number_format(array_sum(array_column($resultInvoice, "ammount_finance")), 2, ".",",") ?> </td>
			<td> <?= number_format(array_sum(array_column($resultInvoice, "ammount_distributor")), 2, ".",",") ?> </td>
			<td> <?= array_sum(array_column($resultInvoice, "lead_time")) ?> </td>
			<td>&nbsp;</td>
			<td> <?= array_sum(array_column($resultInvoice, "cbm")) ?> </td>
			<td> <?= array_sum(array_column($resultInvoice, "container_20ft")) ?> </td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</tfoot>
</table>
</div>

<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"order": [],
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"footer" : true,
				"title" : "Report Sales Order Invoice Ekspor",
				"exportOptions": {
					"columns": [ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24 ]
				}
			}],
		});
	}
</script>