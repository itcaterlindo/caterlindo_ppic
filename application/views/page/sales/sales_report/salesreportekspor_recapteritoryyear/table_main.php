<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body no-padding">
	<div class="row">
		<div class="col-md-12" style="height:200px">
			<canvas id="idChartAmmount"></canvas>
		</div>
		<div class="col-md-12 table-responsive">
			<table id="idTableAmmount" class="table table-bordered table-hover display" style="width:100%; font-size:85%;">
				<thead>
				<tr>
					<th style="text-align:center; vertical-align: middle">In USD</th>
					<th style="text-align:center; vertical-align: middle">Januari</th>
					<th style="text-align:center; vertical-align: middle">Februari</th>
					<th style="text-align:center; vertical-align: middle">Maret</th>
					<th style="text-align:center; vertical-align: middle">April</th>
					<th style="text-align:center; vertical-align: middle">Mei</th>
					<th style="text-align:center; vertical-align: middle">Juni</th>
					<th style="text-align:center; vertical-align: middle">Juli</th>
					<th style="text-align:center; vertical-align: middle">Agustus</th>
					<th style="text-align:center; vertical-align: middle">September</th>
					<th style="text-align:center; vertical-align: middle">Oktober</th>
					<th style="text-align:center; vertical-align: middle">November</th>
					<th style="text-align:center; vertical-align: middle">Desember</th>
				</tr>
				</thead>
				<tbody>
					<?php foreach($resultTeritory as $result): ?>
						<tr>
							<td><?= isset($result['teritory']) ? $result['teritory'] : null ?></td>
							<td><?= isset($result['januari']) ? number_format($result['januari']['ammount_distributor'], 2) : 0 ?></td>
							<td><?= isset($result['februari']) ? number_format($result['februari']['ammount_distributor'], 2) : 0  ?></td>
							<td><?= isset($result['maret']) ? number_format($result['maret']['ammount_distributor'], 2) : 0 ?></td>
							<td><?= isset($result['april']) ? number_format($result['april']['ammount_distributor'], 2) : 0 ?></td>
							<td><?= isset($result['mei']) ? number_format($result['mei']['ammount_distributor'], 2) : 0 ?></td>
							<td><?= isset($result['juni']) ? number_format($result['juni']['ammount_distributor'], 2) : 0 ?></td>
							<td><?= isset($result['juli']) ? number_format($result['juli']['ammount_distributor'], 2) : 0 ?></td>
							<td><?= isset($result['agustus']) ? number_format($result['agustus']['ammount_distributor'], 2) : 0 ?></td>
							<td><?= isset($result['september']) ? number_format($result['september']['ammount_distributor'], 2) : 0 ?></td>
							<td><?= isset($result['oktober']) ? number_format($result['oktober']['ammount_distributor'], 2) : 0 ?></td>
							<td><?= isset($result['november']) ? number_format($result['november']['ammount_distributor'], 2) : 0 ?></td>
							<td><?= isset($result['desember']) ? number_format($result['desember']['ammount_distributor'], 2) : 0 ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot>
					<tr class="active"> 
						<td class="text-center" colspan="1">TOTAL</td>
						<td><?= $sourceChartAmmount[0] = number_format( array_sum( array_column(array_column($resultTeritory, 'januari'), 'ammount_distributor') ) , 2) ?></td>
						<td><?= $sourceChartAmmount[1] = number_format( array_sum( array_column(array_column($resultTeritory, 'februari'), 'ammount_distributor') ) , 2) ?></td>
						<td><?= $sourceChartAmmount[2] = number_format( array_sum( array_column(array_column($resultTeritory, 'maret'), 'ammount_distributor') ) , 2) ?></td>
						<td><?= $sourceChartAmmount[3] = number_format( array_sum( array_column(array_column($resultTeritory, 'april'), 'ammount_distributor') ) , 2) ?></td>
						<td><?= $sourceChartAmmount[4] = number_format( array_sum( array_column(array_column($resultTeritory, 'mei'), 'ammount_distributor') ) , 2) ?></td>
						<td><?= $sourceChartAmmount[5] = number_format( array_sum( array_column(array_column($resultTeritory, 'juni'), 'ammount_distributor') ) , 2) ?></td>
						<td><?= $sourceChartAmmount[6] = number_format( array_sum( array_column(array_column($resultTeritory, 'juli'), 'ammount_distributor') ) , 2) ?></td>
						<td><?= $sourceChartAmmount[7] = number_format( array_sum( array_column(array_column($resultTeritory, 'agustus'), 'ammount_distributor') ) , 2) ?></td>
						<td><?= $sourceChartAmmount[8] = number_format( array_sum( array_column(array_column($resultTeritory, 'september'), 'ammount_distributor') ) , 2) ?></td>
						<td><?= $sourceChartAmmount[9] = number_format( array_sum( array_column(array_column($resultTeritory, 'oktober'), 'ammount_distributor') ) , 2) ?></td>
						<td><?= $sourceChartAmmount[10] = number_format( array_sum( array_column(array_column($resultTeritory, 'november'), 'ammount_distributor') ) , 2) ?></td>
						<td><?= $sourceChartAmmount[11] = number_format( array_sum( array_column(array_column($resultTeritory, 'desember'), 'ammount_distributor') ) , 2) ?></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<br>
	<br>
	<div class="row">
		<div class="col-md-12" style="height:200px">
			<canvas id="idChart20ft"></canvas>
		</div>
		<div class="col-md-12 table-responsive">
			<table id="idTable20ft" class="table table-bordered table-hover display" style="width:100%; font-size:85%;">
				<thead>
				<tr>
					<th style="text-align:center; vertical-align: middle">In 20ft</th>
					<th style="text-align:center; vertical-align: middle">Januari</th>
					<th style="text-align:center; vertical-align: middle">Februari</th>
					<th style="text-align:center; vertical-align: middle">Maret</th>
					<th style="text-align:center; vertical-align: middle">April</th>
					<th style="text-align:center; vertical-align: middle">Mei</th>
					<th style="text-align:center; vertical-align: middle">Juni</th>
					<th style="text-align:center; vertical-align: middle">Juli</th>
					<th style="text-align:center; vertical-align: middle">Agustus</th>
					<th style="text-align:center; vertical-align: middle">September</th>
					<th style="text-align:center; vertical-align: middle">Oktober</th>
					<th style="text-align:center; vertical-align: middle">November</th>
					<th style="text-align:center; vertical-align: middle">Desember</th>
				</tr>
				</thead>
				<tbody>
					<?php foreach($resultTeritory as $result): ?>
						<tr>
							<td><?= isset($result['teritory']) ? $result['teritory'] : null ?></td>
							<td><?= isset($result['januari']) ? number_format($result['januari']['container_20ft']) : 0 ?></td>
							<td><?= isset($result['februari']) ? number_format($result['februari']['container_20ft']) : 0  ?></td>
							<td><?= isset($result['maret']) ? number_format($result['maret']['container_20ft']) : 0 ?></td>
							<td><?= isset($result['april']) ? number_format($result['april']['container_20ft']) : 0 ?></td>
							<td><?= isset($result['mei']) ? number_format($result['mei']['container_20ft']) : 0 ?></td>
							<td><?= isset($result['juni']) ? number_format($result['juni']['container_20ft']) : 0 ?></td>
							<td><?= isset($result['juli']) ? number_format($result['juli']['container_20ft']) : 0 ?></td>
							<td><?= isset($result['agustus']) ? number_format($result['agustus']['container_20ft']) : 0 ?></td>
							<td><?= isset($result['september']) ? number_format($result['september']['container_20ft']) : 0 ?></td>
							<td><?= isset($result['oktober']) ? number_format($result['oktober']['container_20ft']) : 0 ?></td>
							<td><?= isset($result['november']) ? number_format($result['november']['container_20ft']) : 0 ?></td>
							<td><?= isset($result['desember']) ? number_format($result['desember']['container_20ft']) : 0 ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot>
					<tr class="active"> 
					<td class="text-center" colspan="1">TOTAL</td>
						<td><?= $sourceChart20ft[0] = number_format( array_sum( array_column(array_column($resultTeritory, 'januari'), 'container_20ft') ) ) ?></td>
						<td><?= $sourceChart20ft[1] = number_format( array_sum( array_column(array_column($resultTeritory, 'februari'), 'container_20ft') ) ) ?></td>
						<td><?= $sourceChart20ft[2] = number_format( array_sum( array_column(array_column($resultTeritory, 'maret'), 'container_20ft') ) ) ?></td>
						<td><?= $sourceChart20ft[3] = number_format( array_sum( array_column(array_column($resultTeritory, 'april'), 'container_20ft') ) ) ?></td>
						<td><?= $sourceChart20ft[4] = number_format( array_sum( array_column(array_column($resultTeritory, 'mei'), 'container_20ft') ) ) ?></td>
						<td><?= $sourceChart20ft[5] = number_format( array_sum( array_column(array_column($resultTeritory, 'juni'), 'container_20ft') ) ) ?></td>
						<td><?= $sourceChart20ft[6] = number_format( array_sum( array_column(array_column($resultTeritory, 'juli'), 'container_20ft') ) ) ?></td>
						<td><?= $sourceChart20ft[7] = number_format( array_sum( array_column(array_column($resultTeritory, 'agustus'), 'container_20ft') ) ) ?></td>
						<td><?= $sourceChart20ft[8] = number_format( array_sum( array_column(array_column($resultTeritory, 'september'), 'container_20ft') ) ) ?></td>
						<td><?= $sourceChart20ft[9] = number_format( array_sum( array_column(array_column($resultTeritory, 'oktober'), 'container_20ft') ) ) ?></td>
						<td><?= $sourceChart20ft[10] = number_format( array_sum( array_column(array_column($resultTeritory, 'november'), 'container_20ft') ) ) ?></td>
						<td><?= $sourceChart20ft[11] = number_format( array_sum( array_column(array_column($resultTeritory, 'desember'), 'container_20ft') ) ) ?></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>

</div>

<script type="text/javascript">
	
	$(document).ready(function(){
		render_chart_ammount('#idChartAmmount');
		render_chart_20ft('#idChart20ft');
		render_dt('#idTableAmmount');
		render_dt('#idTable20ft');
	});

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"searching": false,
			"order": [],
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"footer" : true,
				"title" : "Report Ekspor Recap Teritory Year",
				"exportOptions": {
					"columns": [0,1,2,3,4,5,6,7,8,9,10,11,12]
				}
			}],
		});
	}

	function render_chart_ammount(elem) {
		var sourceData = <?= json_encode($sourceChartAmmount) ?>;
		var output = [];
		var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
		// Mecah string
		sourceData = JSON.parse(
			JSON.stringify(sourceData).replace(/,(?!["{}[\]])/g, "")
		)
		// Push to output
		output.push({ 'label': "Ammount Distributor", 'backgroundColor': "#5bc0de", 'data': Object.values(sourceData) });
		var ctx = $(elem), 
		setup = {
				type: 'bar',
				data: {
					labels: bulan,
					datasets: output
				},
				options: {
					title: {
						display: true,
						text: 'Recap teritory year'
					},
					responsive:true,
    				maintainAspectRatio: false,
					tooltips: {
						callbacks: {
							"label": (tooltipItem, data) => {
								// Tooltips custom ketika ada comma separator decimal
								var label = data.datasets[tooltipItem.datasetIndex].label || '';
								return label + ' ' + (Math.round(tooltipItem.yLabel * 100) / 100).toFixed(2);
							}
						}
					},
				}
			};
		var myBar = new Chart(ctx, setup);
	}

	function render_chart_20ft(elem) {
		var sourceData = <?= json_encode($sourceChart20ft) ?>;
		var output = [];
		var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
		// Mecah string
		sourceData = JSON.parse(
			JSON.stringify(sourceData).replace(/,(?!["{}[\]])/g, "")
		)
		// Push to output

		output.push({ 'label': "Ammount 20ft", 'backgroundColor': "#5bc0de", 'data': Object.values(sourceData) });
		console.log(output);

		var ctx = $(elem), 
		setup = {
				type: 'bar',
				data: {
					labels: bulan,
					datasets: output
				},
				options: {
					title: {
						display: true,
						text: 'Recap teritory year'
					},
					responsive:true,
					maintainAspectRatio: false,
					tooltips: {
						callbacks: {
							"label": (tooltipItem, data) => {
								// Tooltips custom ketika ada comma separator decimal
								var label = data.datasets[tooltipItem.datasetIndex].label || '';
								return label + ' ' + (Math.round(tooltipItem.yLabel * 100) / 100).toFixed(2);
							}
						}
					},
				}
			};
		var myBar = new Chart(ctx, setup);
	}

</script>