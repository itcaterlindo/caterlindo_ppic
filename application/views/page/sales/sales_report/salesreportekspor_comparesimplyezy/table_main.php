<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}
</style>
<div class="box-body no-padding">
	<div class="row">
		<div class="col-md-12" style="height:200px">
			<canvas id="chartAmmount"></canvas>
		</div>
		<div class="col-md-12 table-responsive">
			<table id="idTableAmount" class="table table-bordered display" style="width:100%; font-size:85%;">
				<thead>
					<tr>
						<th style="text-align:center; vertical-align: middle">In USD</th>
						<th style="text-align:center; vertical-align: middle">Januari</th>
						<th style="text-align:center; vertical-align: middle">Februari</th>
						<th style="text-align:center; vertical-align: middle">Maret</th>
						<th style="text-align:center; vertical-align: middle">April</th>
						<th style="text-align:center; vertical-align: middle">Mei</th>
						<th style="text-align:center; vertical-align: middle">Juni</th>
						<th style="text-align:center; vertical-align: middle">Juli</th>
						<th style="text-align:center; vertical-align: middle">Agustus</th>
						<th style="text-align:center; vertical-align: middle">September</th>
						<th style="text-align:center; vertical-align: middle">Oktober</th>
						<th style="text-align:center; vertical-align: middle">November</th>
						<th style="text-align:center; vertical-align: middle">Desember</th>
						<th style="text-align:center; vertical-align: middle">Total</th>
					</tr>
				</thead>
				<tbody>
				<?php
					$tahun = 0;
					$sourceChartAmmount = [];

					for ($thn = $tahunAwal; $thn <= $tahunAwal + $selisihTahun; $thn++) {
						if ($tahun !== $thn) {
							$tabel_isi = '';
							$tahun = $thn;
							$bln1 = $bln2 = $bln3 = $bln4 = $bln5 = $bln6 = $bln7 = $bln8 = $bln9 = $bln10 = $bln11 = $bln12 = 0;
							foreach ($listGroup as $l) {
								//total per group
								$total_group = ($result[$thn]['1'][$l]['total_harga']  ?:  0) + ($result[$thn]['2'][$l]['total_harga']  ?:  0) + ($result[$thn]['3'][$l]['total_harga']  ?:  0) +
									($result[$thn]['4'][$l]['total_harga']  ?:  0) + ($result[$thn]['5'][$l]['total_harga']  ?:  0) + ($result[$thn]['6'][$l]['total_harga']  ?:  0)
									+ ($result[$thn]['7'][$l]['total_harga']  ?:  0) + ($result[$thn]['8'][$l]['total_harga']  ?:  0) + ($result[$thn]['9'][$l]['total_harga']  ?:  0)
									+ ($result[$thn]['10'][$l]['total_harga']  ?:  0) + ($result[$thn]['11'][$l]['total_harga']  ?:  0) + ($result[$thn]['12'][$l]['total_harga']  ?:  0);
								

								$tabel_isi .= '<tr>
										<td>' . $l . '</td>
										<td>' . number_format(($result[$thn]['1'][$l]['total_harga']  ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['2'][$l]['total_harga']  ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['3'][$l]['total_harga']  ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['4'][$l]['total_harga']   ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['5'][$l]['total_harga']  ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['6'][$l]['total_harga']   ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['7'][$l]['total_harga']  ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['8'][$l]['total_harga']  ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['9'][$l]['total_harga']  ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['10'][$l]['total_harga']  ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['11'][$l]['total_harga']  ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['12'][$l]['total_harga']  ?:  0),0,"",",") . '</td>
										<td>' . number_format($total_group,0,"",",") . '</td>
									</tr>';

								//Total per bulan
								$bln1 += ($result[$thn]['1'][$l]['total_harga']  ?:  0);
								$bln2 += ($result[$thn]['2'][$l]['total_harga']  ?:  0);
								$bln3 += ($result[$thn]['3'][$l]['total_harga']  ?:  0);
								$bln4 += ($result[$thn]['4'][$l]['total_harga']  ?:  0);
								$bln5 += ($result[$thn]['5'][$l]['total_harga']  ?:  0);
								$bln6 += ($result[$thn]['6'][$l]['total_harga']  ?:  0);
								$bln7 += ($result[$thn]['7'][$l]['total_harga']  ?:  0);
								$bln8 += ($result[$thn]['8'][$l]['total_harga']  ?:  0);
								$bln9 += ($result[$thn]['9'][$l]['total_harga']  ?:  0);
								$bln10 += ($result[$thn]['10'][$l]['total_harga']  ?:  0);
								$bln11 += ($result[$thn]['11'][$l]['total_harga']  ?:  0);
								$bln12 += ($result[$thn]['12'][$l]['total_harga']  ?:  0);
							}
							//total semua bulan
							$blnt = $bln1 + $bln2 + $bln3 + $bln4 + $bln5 + $bln6 + $bln7 + $bln8 + $bln9 + $bln10 + $bln11 + $bln12;
							$sourceChartAmmount[$thn][0] = (string)$bln1;
							$sourceChartAmmount[$thn][1] = (string)$bln2;
							$sourceChartAmmount[$thn][2] = (string)$bln3;
							$sourceChartAmmount[$thn][3] = (string)$bln4;
							$sourceChartAmmount[$thn][4] = (string)$bln5;
							$sourceChartAmmount[$thn][5] = (string)$bln6;
							$sourceChartAmmount[$thn][6] = (string)$bln7;
							$sourceChartAmmount[$thn][7] = (string)$bln8;
							$sourceChartAmmount[$thn][8] = (string)$bln9;
							$sourceChartAmmount[$thn][9] = (string)$bln10;
							$sourceChartAmmount[$thn][10] = (string)$bln11;
							$sourceChartAmmount[$thn][11] = (string)$bln12;
							$tbl =  '<tr class="active"> 
								<td>' .  $thn . '</td>
								<td>' . number_format($bln1,0,"",",") . '</td>
								<td>' . number_format($bln2,0,"",",") . '</td>
								<td>' . number_format($bln3,0,"",",") . '</td>
								<td>' . number_format($bln4,0,"",",") . '</td>
								<td>' . number_format($bln5,0,"",",") . '</td>
								<td>' . number_format($bln6,0,"",",") . '</td>
								<td>' . number_format($bln7,0,"",",") . '</td>
								<td>' . number_format($bln8,0,"",",") . '</td>
								<td>' . number_format($bln9,0,"",",") . '</td>
								<td>' . number_format($bln10,0,"",",") . '</td>
								<td>' . number_format($bln11,0,"",",") . '</td>
								<td>' . number_format($bln12,0,"",",") . '</td>
								<td>' . number_format($blnt,0,"",",") . '</td>
							</tr>' . $tabel_isi;
							echo $tbl;
						}
					} ?>
				</tbody>
			</table>
		</div>
	</div>
	<br>
	<br>
	<div class="row">
		<div class="col-md-12" style="height:200px">
			<canvas id="chartQty"></canvas>
		</div>
		<div class="col-md-12 table-responsive">
			<table id="idTableQty" class="table table-bordered display" style="width:100%; font-size:85%;">
				<thead>
					<tr>
						<th style="text-align:center; vertical-align: middle">In Qty</th>
						<th style="text-align:center; vertical-align: middle">Januari</th>
						<th style="text-align:center; vertical-align: middle">Februari</th>
						<th style="text-align:center; vertical-align: middle">Maret</th>
						<th style="text-align:center; vertical-align: middle">April</th>
						<th style="text-align:center; vertical-align: middle">Mei</th>
						<th style="text-align:center; vertical-align: middle">Juni</th>
						<th style="text-align:center; vertical-align: middle">Juli</th>
						<th style="text-align:center; vertical-align: middle">Agustus</th>
						<th style="text-align:center; vertical-align: middle">September</th>
						<th style="text-align:center; vertical-align: middle">Oktober</th>
						<th style="text-align:center; vertical-align: middle">November</th>
						<th style="text-align:center; vertical-align: middle">Desember</th>
						<th style="text-align:center; vertical-align: middle">Total</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$tahun = $thn = 0;
					$thn = $tahunAwal;
					$sourceChartQty = [];
					for ($thn = $tahunAwal; $thn <= $tahunAwal + $selisihTahun; $thn++) {
						if ($tahun !== $thn) {
							$tabel_isi = '';
							$tahun = $thn;							
							$bln1 = $bln2 = $bln3 = $bln4 = $bln5 = $bln6 = $bln7 = $bln8 = $bln9 = $bln10 = $bln11 = $bln12 = 0;
							foreach ($listGroup as $l) {
								$total_group = ($result[$thn]['1'][$l]['item_qty']  ?:  0) + ($result[$thn]['2'][$l]['item_qty']  ?:  0) + ($result[$thn]['3'][$l]['item_qty']  ?:  0) +
									($result[$thn]['4'][$l]['item_qty']  ?:  0) + ($result[$thn]['5'][$l]['item_qty']  ?:  0) + ($result[$thn]['6'][$l]['item_qty']  ?:  0)
									+ ($result[$thn]['7'][$l]['item_qty']  ?:  0) + ($result[$thn]['8'][$l]['item_qty']  ?:  0) + ($result[$thn]['9'][$l]['item_qty']  ?:  0)
									+ ($result[$thn]['10'][$l]['item_qty']  ?:  0) + ($result[$thn]['11'][$l]['item_qty']  ?:  0) + ($result[$thn]['12'][$l]['item_qty']  ?:  0);
								

								$tabel_isi .= '<tr>
										<td>' . $l . '</td>
										<td>' . number_format(($result[$thn]['1'][$l]['item_qty']  ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['2'][$l]['item_qty']  ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['3'][$l]['item_qty']  ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['4'][$l]['item_qty']   ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['5'][$l]['item_qty']  ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['6'][$l]['item_qty']   ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['7'][$l]['item_qty']  ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['8'][$l]['item_qty']  ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['9'][$l]['item_qty']  ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['10'][$l]['item_qty']  ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['11'][$l]['item_qty']  ?:  0),0,"",",") . '</td>
										<td>' . number_format(($result[$thn]['12'][$l]['item_qty']  ?:  0),0,"",",") . '</td>
										<td>' . number_format($total_group,0,"",",") . '</td>
									</tr>';

								//Total per bulan
								$bln1 += ($result[$thn]['1'][$l]['item_qty']  ?:  0);
								$bln2 += ($result[$thn]['2'][$l]['item_qty']  ?:  0);
								$bln3 += ($result[$thn]['3'][$l]['item_qty']  ?:  0);
								$bln4 += ($result[$thn]['4'][$l]['item_qty']  ?:  0);
								$bln5 += ($result[$thn]['5'][$l]['item_qty']  ?:  0);
								$bln6 += ($result[$thn]['6'][$l]['item_qty']  ?:  0);
								$bln7 += ($result[$thn]['7'][$l]['item_qty']  ?:  0);
								$bln8 += ($result[$thn]['8'][$l]['item_qty']  ?:  0);
								$bln9 += ($result[$thn]['9'][$l]['item_qty']  ?:  0);
								$bln10 += ($result[$thn]['10'][$l]['item_qty']  ?:  0);
								$bln11 += ($result[$thn]['11'][$l]['item_qty']  ?:  0);
								$bln12 += ($result[$thn]['12'][$l]['item_qty']  ?:  0);
							}
							//total semua bulan
							$blnt = $bln1 + $bln2 + $bln3 + $bln4 + $bln5 + $bln6 + $bln7 + $bln8 + $bln9 + $bln10 + $bln11 + $bln12;
							$sourceChartQty[$thn][0] = (string)$bln1;
							$sourceChartQty[$thn][1] = (string)$bln2;
							$sourceChartQty[$thn][2] = (string)$bln3;
							$sourceChartQty[$thn][3] = (string)$bln4;
							$sourceChartQty[$thn][4] = (string)$bln5;
							$sourceChartQty[$thn][5] = (string)$bln6;
							$sourceChartQty[$thn][6] = (string)$bln7;
							$sourceChartQty[$thn][7] = (string)$bln8;
							$sourceChartQty[$thn][8] = (string)$bln9;
							$sourceChartQty[$thn][9] = (string)$bln10;
							$sourceChartQty[$thn][10] = (string)$bln11;
							$sourceChartQty[$thn][11] = (string)$bln12;

							//data header
							$tbl =  '<tr class="active"> 
								<td>' .  $thn . '</td>
								<td>' . number_format($bln1,0,"",",") . '</td>
								<td>' . number_format($bln2,0,"",",") . '</td>
								<td>' . number_format($bln3,0,"",",") . '</td>
								<td>' . number_format($bln4,0,"",",") . '</td>
								<td>' . number_format($bln5,0,"",",") . '</td>
								<td>' . number_format($bln6,0,"",",") . '</td>
								<td>' . number_format($bln7,0,"",",") . '</td>
								<td>' . number_format($bln8,0,"",",") . '</td>
								<td>' . number_format($bln9,0,"",",") . '</td>
								<td>' . number_format($bln10,0,"",",") . '</td>
								<td>' . number_format($bln11,0,"",",") . '</td>
								<td>' . number_format($bln12,0,"",",") . '</td>
								<td>' . number_format($blnt,0,"",",") . '</td>
							</tr>' . $tabel_isi;
							echo $tbl;
						}
					} ?>
				</tbody>
			</table>
		</div>
	</div>

</div>

<script type="text/javascript">
	$(document).ready(function() {
		render_chart_ammount('#chartAmmount');
		render_chart_20ft('#chartQty');
		render_dt('#idTableQty');
		render_dt('#idTableAmount');
	});

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"searching": false,
			"order": [],
			"ordering": false,
			"language": {
				"lengthMenu": "Tampilkan _MENU_ data",
				"zeroRecords": "Maaf tidak ada data yang ditampilkan",
				"info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty": "Tidak ada data yang ditampilkan",
				"search": "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing": "Sedang Memproses...",
				"paginate": {
					"first": '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last": '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next": '<span class="glyphicon glyphicon-forward"></span>',
					"previous": '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons": [{
				"extend": "excel",
				"footer": true,
				"title": "Report Ekspor Comparassion Simply x Ezy",
				"exportOptions": {
					"columns": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
				}
			}],
		});
	}

	function render_chart_ammount(element) {
		var sourceData = <?= json_encode($sourceChartAmmount) ?>;
		var output = [];
		// Mecah string
		sourceData = JSON.parse(
			JSON.stringify(sourceData).replace(/,(?!["{}[\]])/g, "")
		)

		// Push to output
		for (const [key, value] of Object.entries(sourceData)) {
			// Setting automatic background color
			var background_color;
			var r = Math.floor(Math.random() * 255);
			var g = Math.floor(Math.random() * 255);
			var b = Math.floor(Math.random() * 255);
			background_color = 'rgba(' + r + ', ' + g + ', ' + b + ', 0.5)';
			output.push({
				'label': key,
				'data': Object.values(value),
				borderWidth: 2,
				pointRadius: 2,
				pointHoverRadius: 2,
				fill: false,
				borderColor: background_color,
				backgroundColor: background_color,
				pointBackgroundColor: background_color,
				pointBorderColor: background_color,
				pointHoverBackgroundColor: background_color,
				pointHoverBorderColor: background_color,
			});
		}
		var ctx = $(element),
			setup = {
				type: 'line',
				data: {
					labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
					datasets: output
				},
				options: {
					title: {
						display: true,
						text: 'Compare Simply vs Ezy'
					},
					responsive: true,
					maintainAspectRatio: false,
					tooltips: {
						callbacks: {
							"label": (tooltipItem, data) => {
								// Tooltips custom ketika ada comma separator decimal
								var label = data.datasets[tooltipItem.datasetIndex].label || '';
								return label + ' ' + (Math.round(tooltipItem.yLabel * 100) / 100).toFixed(2);
							}
						}
					},
				}
			};
		var myBar = new Chart(ctx, setup);
	}

	function render_chart_20ft(element) {
		var sourceData = <?= json_encode($sourceChartQty) ?>;
		var output = [];
		// Push to output
		for (const [key, value] of Object.entries(sourceData)) {
			// Setting automatic background color
			var background_color;
			var r = Math.floor(Math.random() * 255);
			var g = Math.floor(Math.random() * 255);
			var b = Math.floor(Math.random() * 255);
			background_color = 'rgba(' + r + ', ' + g + ', ' + b + ', 0.5)';
			output.push({
				'label': key,
				'data': Object.values(value),
				borderWidth: 2,
				pointRadius: 2,
				pointHoverRadius: 2,
				fill: false,
				borderColor: background_color,
				backgroundColor: background_color,
				pointBackgroundColor: background_color,
				pointBorderColor: background_color,
				pointHoverBackgroundColor: background_color,
				pointHoverBorderColor: background_color,
			});
		}
		var ctx = $(element),
			setup = {
				type: 'line',
				data: {
					labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
					datasets: output
				},
				options: {
					title: {
						display: true,
						text: 'Compare Simply vs Pret'
					},
					responsive: true,
					maintainAspectRatio: false,
					tooltips: {
						callbacks: {
							"label": (tooltipItem, data) => {
								// Tooltips custom ketika ada comma separator decimal
								var label = data.datasets[tooltipItem.datasetIndex].label || '';
								return label + ' ' + (Math.round(tooltipItem.yLabel * 100) / 100).toFixed(2);
							}
						}
					},
				}
			};
		var myBar = new Chart(ctx, setup);
	}
</script>