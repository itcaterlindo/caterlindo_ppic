<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">

</style>
<div class="box-body table-responsive no-padding">
<table id="idTable" class="table table-bordered table-hover display" style="width:100%; font-size:85%;">
	<thead>
	<tr>
		<th rowspan="2" style="text-align:center; vertical-align: middle" class="all">No.</th>
		<th rowspan="2" style="text-align:center; vertical-align: middle">No. Sales Order</th>
		<th rowspan="2" style="text-align:center; vertical-align: middle">Customer Name</th>
		<th colspan="4" style="text-align:center; vertical-align: middle">Delivery Date</th>
		<th rowspan="2" style="text-align:center; vertical-align: middle">Evaluation Delivery</th>
		<th rowspan="2" style="text-align:center; vertical-align: middle">Delivery Address</th>
		<th rowspan="2" style="text-align:center; vertical-align: middle">Expedition</th>
		<th rowspan="2" style="text-align:center; vertical-align: middle">DO Note</th>
	</tr>
	<tr>
		<th style="text-align:center;">Date SO (PO)</th>
		<th style="text-align:center;">Planning Delivery Date (PO)</th>
		<th style="text-align:center;">Actual (Stuffing)</th>
		<th style="text-align:center;">Actual (FG Out)</th>
	</tr>
	</thead>
	<tbody>
		<?php 
		$no = 1;
		foreach($resultDO as $DO):
		?>
		<tr>
			<td><?= $no++ ?></td>
			<td class=""><?= $DO['no_salesorder'] ?></td>
			<td><?= $DO['nm_customer'] ?></td>
			<td><?= format_date($DO['tgl_so'], 'd-m-Y') ?></td>
			<td><?= format_date($DO['tgl_kirim'], 'd-m-Y') ?></td>
			<td><?= format_date($DO['tgl_stuffing'], 'd-m-Y') ?></td>
			<td><?= str_replace(";", "<br>", $DO['tgl_fg_out']) ?> </td>
			<td class="<?= $DO['evaluation_delivery'] == 'ontime' ? 'warning' : 'danger' ?>"><?= strtoupper($DO['evaluation_delivery']) ?></td>
			<td><?= $DO['alamat'] ?></td>
			<td><?= $DO['nm_jasakirim'] ?></td>
			<td><?= $DO['do_ket'] ?></td>
		</tr>
		<?php endforeach; ?>

		<?php if(!empty($resultDO)): ?>
			<tr>
				<td class="warning">ONTIME</td>
				<td colspan="10"><?= $countOntime == null ? 0 : $countOntime ?></td>
				<td style="display: none;"></td>
				<td style="display: none;"></td>
				<td style="display: none;"></td>
				<td style="display: none;"></td>
				<td style="display: none;"></td>
				<td style="display: none;"></td>
				<td style="display: none;"></td>
				<td style="display: none;"></td>
				<td style="display: none;"></td>
			</tr>
			<tr>
				<td class="danger">LATE</td>
				<td colspan="10"><?= $countLate == null ? 0 : $countLate ?></td>
				<td style="display: none;"></td>
				<td style="display: none;"></td>
				<td style="display: none;"></td>
				<td style="display: none;"></td>
				<td style="display: none;"></td>
				<td style="display: none;"></td>
				<td style="display: none;"></td>
				<td style="display: none;"></td>
				<td style="display: none;"></td>
			</tr>
		<?php endif; ?>

	</tbody>

</table>
</div>

<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"order": [],
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"title" : "Rekap Sales Delivery Order",
				"exportOptions": {
					"columns": [ 0,1,2,3,4,5,6,7,8,9]
				}
			}],
		});
	}
</script>