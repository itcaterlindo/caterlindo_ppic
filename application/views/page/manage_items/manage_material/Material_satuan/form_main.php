<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtrmsatuan_kd', 'name'=> 'txtrmsatuan_kd', 'value' => isset($id) ? $id: null ));

?>
<div class="form-group">
	<label for="idtxtrmsatuan_nama" class="col-md-2 control-label">Nama Satuan</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrRmsatuan_nama"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtrmsatuan_nama', 'id'=> 'idtxtrmsatuan_nama', 'placeholder' => 'Nama Satuan', 'value'=> isset($rmsatuan_nama) ? $rmsatuan_nama: null ));?>
	</div>
</div>
<div class="form-group">
	<label for="idtxtrmsatuan_ket" class="col-md-2 control-label">Keterangan Satuan</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrRmsatuan_ket"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtrmsatuan_ket', 'id'=> 'idtxtrmsatuan_ket', 'placeholder' => 'Keterangan Satuan', 'value'=> isset($rmsatuan_ket) ? $rmsatuan_ket: null ));?>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat btn-sm">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" onclick="submitData()" class="btn btn-primary btn-flat btn-sm">
			<i class="fa fa-save"></i> Simpan
		</button>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	
</script>