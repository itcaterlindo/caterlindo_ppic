<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtSts', 'name' => 'txtSts', 'value' => $sts));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtrmsatuankonversi_kd', 'name' => 'txtrmsatuankonversi_kd', 'value' => isset($id) ? $id : null));
if (isset($rowData)) {
	extract($rowData);
}
?>
<div class="form-group">
	<label for="idtxtrmsatuankonversi_from" class="col-md-2 control-label">Dari (per-)</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrrmsatuankonversi_from"></div>
		<select name="txtrmsatuankonversi_from" class="form-control clSatuan" id="idtxtrmsatuankonversi_from">
			<?php if (isset($rmsatuankonversi_from)) : ?>
				<option value="<?php echo $rmsatuankonversi_from; ?>" selected><?php echo $fr_satuan; ?></option>
			<?php endif; ?>
		</select>
	</div>
</div>
<div class="form-group">
	<label for="idtxtrmsatuankonversi_konversi" class="col-md-2 control-label">Konversi</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrrmsatuankonversi_konversi"></div>
		<?php echo form_input(array('type' => 'number', 'class' => 'form-control', 'name' => 'txtrmsatuankonversi_konversi', 'id' => 'idtxtrmsatuankonversi_konversi', 'placeholder' => 'Konversi', 'value' => isset($rmsatuankonversi_konversi) ? $rmsatuankonversi_konversi : null)); ?>
	</div>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrrmsatuankonversi_to"></div>
		<select name="txtrmsatuankonversi_to" class="form-control clSatuan" id="idtxtrmsatuankonversi_to">
			<?php if (isset($rmsatuankonversi_to)) : ?>
				<option value="<?php echo $rmsatuankonversi_to; ?>" selected><?php echo $to_satuan; ?></option>
			<?php endif; ?>
		</select>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-2 col-sm-offset-2 col-xs-12">
		<button type="submit" name="btnSubmit" onclick="submitData()" class="btn btn-primary btn-flat btn-sm">
			<i class="fa fa-save"></i> Simpan
		</button>
		<button type="reset" name="btnReset" class="btn btn-default btn-flat btn-sm">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
</script>