<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>
<div class="box-body table-responsive no-padding">
	<table id="idTableform" class="table table-bordered table-striped table-hover" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all" hidden>RM Kode</th>
			<th style="width:1%; text-align:center;" class="all">RM Item Kode</th>
			<th style="width:1%; text-align:center;" class="all">Nama Material</th>
			<th style="width:1%; text-align:center;" class="all">Spesifikasi</th>
			<th style="width:1%; text-align:center;" class="all">Stock Min</th>
		</tr>
		</thead>
		<tbody>
			<?php foreach($material_data as $key => $val){ ?>
			<tr>
				<td hidden><?= $val['rm_kd'] ?></td>
				<td><?= $val['rm_kode'] ?></td>
				<td><?= $val['rm_deskripsi'] ?></td>
				<td><?= $val['rm_spesifikasi'] ?></td>
				<td>
					<input class="form-control" type="number" name="txtStockMin" id="idtxtStockMin" value="<?= $val['rm_stock_min'] ?>">
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	var table = $('#idTableform').DataTable({
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		}
	});
</script>
