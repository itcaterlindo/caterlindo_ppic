<?php
defined('BASEPATH') or exit('No direct script access allowed!');

/* --SET DEFAULT PROPERTY UNTUK BOX-- */
$js_file = 'form_minstock_byitemgroup_js';
$box_type = 'Form';
$box_title = 'Form Edit Min Stok Material Data';
$data['master_var'] = 'MaterialDataEditStokMin';
$data['class_link'] = $class_link;
$data['box_id'] = 'id'.$box_type.'Box'.$data['master_var'];
$data['btn_hide_id'] = 'id'.$box_type.'BtnBoxHide'.$data['master_var'];
$data['btn_add_id'] = 'id'.$box_type.'BtnBoxAdd'.$data['master_var'];
$data['btn_remove_id'] = 'id'.$box_type.'BtnBoxRemove'.$data['master_var'];
$data['box_alert_id'] = 'id'.$box_type.'BoxAlert'.$data['master_var'];
$data['box_loader_id'] = 'id'.$box_type.'BoxLoader'.$data['master_var'];
$data['box_content_id'] = 'id'.$box_type.'BoxContent'.$data['master_var'];
$data['box_overlay_id'] = 'id'.$box_type.'BoxOverlay'.$data['master_var'];
/* --END OF BOX DEFAULT PROPERTY-- */

/* --END OF CUSTOM PROPERTY-- */

/* --SET BUTTONS DISINI, NILAI "FALSE" UNTUK TIDAK TAMPIL, NILAI "TRUE" UNTUK MENAMPILKAN BUTTON-- */
$btn_hide = TRUE;
$btn_add = FALSE;
$btn_remove = TRUE;
/* --END OF BUTTONS SETUP-- */
?>

<div class="box box-warning" id="<?php echo $data['box_id']; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_title; ?></h3>
		<div class="box-tools pull-right">
			<?php
			if ($btn_hide) :
				?>
				<button class="btn btn-box-tool btn-collapse" id="<?php echo $data['btn_hide_id']; ?>" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
					<i class="fa fa-minus"></i>
				</button>
				<?php
			endif;
			if ($btn_add) :
				?>
				<button class="btn btn-box-tool" id="<?php echo $data['btn_add_id']; ?>" data-toggle="tooltip" title="Tambah Data">
					<i class="fa fa-plus"></i>
				</button>
				<?php
			endif;
			if ($btn_remove) :
				?>
				<button class="btn btn-box-tool btn-remove" id="<?php echo $data['btn_remove_id']; ?>" data-widget="remove" data-toggle="tooltip" title="Close">
					<i class="fa fa-times"></i>
				</button>
				<?php
			endif;
			?>
		</div>
	</div>
	<div class="box-body">
		<div id="<?php echo $data['box_alert_id']; ?>"></div>
		<div id="<?php echo $data['box_loader_id']; ?>" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>

		<div class="row">
			<div class="form-group">
				<div class="col-md-4 col-xs-12">
					<label>Item Group</label>
					<div class="input-group date">
						<div class="input-group-addon"> <i class="fa fa-list"></i></div>
						<select class="form-control" name="slItemGroup" id="idSlItemGroup">
							<option value="">-- Pilih Item Group --</option>
							<?php foreach($item_group as $key => $val){ ?>
								<option value="<?= $val['item_group_kd'] ?>"><?= $val['item_group_name'] ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="col-md-1 col-xs-12">
					<br>
					<button class="btn btn-sm btn-primary" onclick="cariMaterialData()"> <i class="fa fa-search"></i> Cari</button>
				</div>
			</div>
		</div>
		<br>

		<div id="<?php echo $data['box_content_id']; ?>"></div>
	</div>
	<div class="box-footer">
		<div class="col-md-1 col-xs-12 pull-right">
			<button class="btn btn-sm btn-primary" onclick="submit_edit()"> <i class="fa fa-save"></i> Simpan</button>
		</div>
	</div>
	<div class="overlay" id="<?php echo $data['box_overlay_id']; ?>" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
	<?php $this->load->view('page/'.$class_link.'/'.$js_file, $data); ?>
</div>
