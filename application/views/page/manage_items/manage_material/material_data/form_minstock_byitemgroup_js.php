<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">

	open_form();
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');
	
	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#<?php echo $box_loader_id; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
		open_table();
		first_load('<?php echo 'idTableBoxLoader'.$master_var; ?>', '<?php echo 'idTableBoxContent'.$master_var; ?>');
	});

	function open_form() {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			var kdItemGroup = $('#idSlItemGroup').find(":selected").val();;
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/form_edit_minstock_main'; ?>',
				data: {kd_item_group: kdItemGroup},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function submit_edit()
	{
		if(confirm("Apakah anda yakin ingin mengubah min. stok ?")){
			/** Variabel data diambil dari form_minstock_byitemgroup_main */
			var arrKode = [], arrStok = [], arrData = {};
				var i = 0;
				table.rows().every(function(rowIndex){
					var Row = this.data();
					var kdRM = Row[0], minStock = $( this.node() ).first().find('input').val();
					arrKode.push(kdRM);
					arrStok.push(minStock);
				});
				for(var i=0; i < arrKode.length; i++){

					arrData[arrKode[i]] = arrStok[i];
				}
				
				$.ajax({
					url: "<?php echo base_url().$class_link; ?>/action_edit_minstock",
					type: "POST",
					data: {data: arrData},
					success: function(data){
						var resp = JSON.parse(data);
						if(resp.code == 200){
							notify (resp.status, resp.pesan, 'success');
							open_table();
							$('#<?php echo $box_id; ?>').remove();
							$('.errInput').html('');
						}else if (resp.code == 401){
							$.each( resp.pesan, function( key, value ) {
								$('#'+key).html(value);
							});
							generateToken (resp.csrf);
							box_overlay('out');
						}else if (resp.code == 400){
							notify (resp.status, resp.pesan, 'error');
							generateToken (resp.csrf);
						}else{
							notify ('Error', 'Error tidak Diketahui', 'error');
							generateToken (resp.csrf);
						}
					} 	        
			});
		}

	}

	function cariMaterialData()
	{
		open_form();
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitData() {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById('idFormInput');
		var btnSubmit = $('#idBtnSubmit');
		// edit atau add
		var sts =  $('#idtxtSts').val();
		if (sts == 'add'){
			url = "<?php echo base_url().$class_link; ?>/action_insert";
		}else{
			url = "<?php echo base_url().$class_link; ?>/action_update";
		}

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			beforeSend: function(){
				btnSubmit.prop("disabled", true);
			},
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					open_table();
					$('#<?php echo $box_id; ?>').remove();
					$('.errInput').html('');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
				btnSubmit.prop("disabled", false);
			} 	        
		});
	}

</script>