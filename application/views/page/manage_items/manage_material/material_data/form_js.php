<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form('<?php echo $sts; ?>','<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	<?php if ($sts == 'edit') :?>
	<?php endif;?>

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#<?php echo $box_loader_id; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
		open_table();
		first_load('<?php echo 'idTableBoxLoader'.$master_var; ?>', '<?php echo 'idTableBoxContent'.$master_var; ?>');
	});

	$(document).off('change', '#idtxtrm_kategori').on('change', '#idtxtrm_kategori', function() {
		var kategori = $('#idtxtrm_kategori').val();
		open_fill_plate (kategori);
	});

	function open_fill_plate (kategori) {
		if (kategori == '01'){
			$('#idrowplate').slideDown();
		}else{
			$('#idrowplate').slideUp();
		}
	}

	function open_form(sts, id) {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/form_main'; ?>',
				data: {'sts': sts, 'id' : id},
				success: function(html) {
					console.log(html);
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
					rendet_select2('select2');
					var kategori = $('#idtxtrm_kategori').val();
					open_fill_plate (kategori);
				}
			});
		});
	}

	function rendet_select2(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap'
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitData() {
		event.preventDefault();
		if( confirm("Apakah anda ingin menyimpan data ini?") ){
			box_overlay('in');
			var form = document.getElementById('idFormInput');
			var btnSubmit = $('#idBtnSubmit');
			// edit atau add
			var sts =  $('#idtxtSts').val();
			if (sts == 'add'){
				url = "<?php echo base_url().$class_link; ?>/action_insert";
			}else{
				url = "<?php echo base_url().$class_link; ?>/action_update";
			}

			$.ajax({
				url: url ,
				type: "POST",
				data:  new FormData(form),
				contentType: false,
				cache: false,
				processData:false,
				beforeSend: function(){
					btnSubmit.prop("disabled", true);
				},
				success: function(data){
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						open_table();
						$('#<?php echo $box_id; ?>').remove();
						$('.errInput').html('');
					}else if (resp.code == 401){
						$.each( resp.pesan, function( key, value ) {
							$('#'+key).html(value);
						});
						generateToken (resp.csrf);
						box_overlay('out');
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
					btnSubmit.prop("disabled", false);
				} 	        
			});
		}
	}

</script>