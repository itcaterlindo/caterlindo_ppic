<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
$arrayDisabled = [];
if($sts == 'edit' || $sts == 'view'){
	extract($rowData);
	$groupSelected = $rmgroup_kd;
	$kategoriSelected = $rmkategori_kd;
	$satuanSelected = $rmsatuan_kd;
	$rmsSelected = $rmgroupsup_kd;
	$rmgroupSelected = $itemgroup_kd;
	
	
}
if($sts == 'view') {
	$arrayDisabled = ['disabled' => 'disabled'];
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtrm_kd', 'name'=> 'txtrm_kd', 'value' => isset($id) ? $id: null ));

?>
<?php if ($sts == 'edit') :?>
<div class="form-group">
	<label for="idtxtrm_kode" class="col-md-2 control-label">Kode</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrrm_kode"></div>
		<?php echo form_input(array_merge(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtrm_kode', 'id'=> 'idtxtrm_kode', 'placeholder' =>'Kode Material', 'value'=> isset($rm_kode) ? $rm_kode: null ), $arrayDisabled) );?>
	</div>
</div>
<?php endif;?>
<div class="form-group">
	<label for="idtxtrm_nama" class="col-md-2 control-label">Nama Material</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrRm_nama"></div>
		<?php echo form_input(array_merge(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtrm_nama', 'id'=> 'idtxtrm_nama', 'placeholder' =>'Nama Material', 'value'=> isset($rm_nama) ? $rm_nama: null ), $arrayDisabled));?>
	</div>
	<label for="idtxtrm_oldkd" class="col-md-1 control-label">Kode Pronto</label>
	<div class="col-sm-2 col-xs-12">
		<div class="errInput" id="idErrrm_oldkd"></div>
		<?php echo form_input(array_merge(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtrm_oldkd', 'id'=> 'idtxtrm_oldkd', 'placeholder' =>'Kode Pronto', 'value'=> isset($rm_oldkd) ? $rm_oldkd: null ), $arrayDisabled));?>
	</div>
</div>

<div class="form-group">
	<label for="idtxtrm_group" class="col-md-2 control-label">Group Material</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrRm_group"></div>
		<?php echo form_dropdown('txtrm_group', $opsiGroup, isset($groupSelected)? $groupSelected : '', array_merge(array('class'=> 'form-control', 'id'=> 'idtxtrm_group'), $arrayDisabled) );?>
	</div>
</div>

<div class="form-group">
	<label for="idtxtrm_kategori" class="col-md-2 control-label">Kategori Material</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrRm_kategori"></div>
		<?php echo form_dropdown('txtrm_kategori', $opsiKategori, isset($kategoriSelected)? $kategoriSelected : '', array_merge(array('class'=> 'form-control select2', 'id'=> 'idtxtrm_kategori'), $arrayDisabled));?>
	</div>
</div>

<div class="form-group">
	<label for="idtxtrm_rms" class="col-md-2 control-label">Material Group Suplier</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrRm_rms"></div>
		<?php echo form_dropdown('txtrm_rms', $opsiRms, isset($rmsSelected)? $rmsSelected : '', array_merge(array('class'=> 'form-control select2', 'id'=> 'idtxtrm_rms'), $arrayDisabled));?>
	</div>
</div>

<div class="form-group">
	<label for="idtxtrm_rms" class="col-md-2 control-label">Item Group</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrRm_rms"></div>
		<?php echo form_dropdown('txtrm_itg', $opsiItg, isset($rmgroupSelected)? $rmgroupSelected : '', array_merge(array('class'=> 'form-control select2', 'id'=> 'idtxtrm_itg'), $arrayDisabled));?>
	</div>
</div>


<div class="form-group">
	<label for="idtxtrm_filterbom" class="col-md-2 control-label">Filter BoM</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrRm_filterbom"></div>
		<?php echo form_dropdown('txtrm_filterbom', ['1' => 'Ya', '0' => 'Tidak'], isset($rm_filterbom)? $rm_filterbom : '', array_merge(array('class'=> 'form-control', 'id'=> 'idtxtrm_filterbom'), $arrayDisabled) );?>
	</div>
</div>

<div class="form-group">
	<label for="idtxtrm_inventoryitem" class="col-md-2 control-label">Inventory Item</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrRm_inventoryitem"></div>
		<?php echo form_dropdown('txtrm_inventoryitem', ['Y' => 'Ya', 'N' => 'Tidak'], isset($inventory_item)? $inventory_item : '', array_merge(array('class'=> 'form-control', 'id'=> 'idtxtrm_inventoryitem'), $arrayDisabled) );?>
	</div>
</div>

<div class="form-group">
	<label for="idtxt_flag_active" class="col-md-2 control-label">Flag Active</label>
	<div class="col-sm-2 col-xs-12">
		<div class="errInput" id="idErr_flag_active"></div>
		<?php echo form_dropdown('txt_flag_active', ['1' => 'Active', '0' => 'Inactive'], isset($flag_active)? $flag_active : '', array_merge(array('class'=> 'form-control', 'id'=> 'idtxt_flag_active'), $arrayDisabled) );?>
	</div>
	<div class="col-sm-2 col-xs-12">
		<span> * Jika status inactive RM tidak bisa di transaksi </span>
	</div>
</div>

<hr>
<div class="form-group">
	<label for="idtxtrm_satuan" class="col-md-2 control-label">Satuan Material</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrRm_satuan"></div>
		<?php echo form_dropdown('txtrm_satuan', $opsiSatuan, isset($satuanSelected)? $satuanSelected : '', array_merge(array('class'=> 'form-control select2', 'id'=> 'idtxtrm_satuan'), $arrayDisabled));?>
	</div>
</div>

<div class="form-group">
	<label for="idtxtrmsatuansecondary_kd" class="col-md-2 control-label">Satuan Secondary Material</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrrmsatuansecondary_kd"></div>
		<?php echo form_dropdown('txtrmsatuansecondary_kd', $opsiSatuan, isset($rmsatuansecondary_kd)? $rmsatuansecondary_kd : null, array_merge(array('class'=> 'form-control select2', 'id'=> 'idtxtrmsatuansecondary_kd'), $arrayDisabled));?>
	</div>
	<label for="idtxtrm_konversiqty" class="col-md-1 control-label">Satuan Konversi</label>
	<div class="col-sm-2 col-xs-12">
		<div class="errInput" id="idErrrm_konversiqty"></div>
		<?php echo form_input(array_merge(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtrm_konversiqty', 'id'=> 'idtxtrm_konversiqty', 'placeholder' =>'Satuan Konversi' ,'value'=> isset($rm_konversiqty) ? $rm_konversiqty: 1 ), $arrayDisabled));?>
	</div>
</div>

<hr>
<div class="form-group">
	<label for="idtxtrm_stock_min" class="col-md-2 control-label">Stock Min Material</label>
	<div class="col-sm-2 col-xs-12">
		<div class="errInput" id="idErrRm_stock_min"></div>
		<?php echo form_input(array_merge(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtrm_stock_min', 'id'=> 'idtxtrm_stock_min', 'placeholder' =>'Stock Min Material' ,'value'=> isset($rm_stock_min) ? $rm_stock_min: null ), $arrayDisabled));?>
	</div>
	<label for="idtxtrm_stock_max" class="col-md-4 control-label">Stock Max Material</label>
	<div class="col-sm-2 col-xs-12">
		<div class="errInput" id="idErrRm_stock_max"></div>
		<?php echo form_input(array_merge(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtrm_stock_max', 'id'=> 'idtxtrm_stock_max', 'placeholder' =>'Stock Max Material' ,'value'=> isset($rm_stock_max) ? $rm_stock_max: null ), $arrayDisabled));?>
	</div>
</div>

<div class="form-group">
	<label for="idtxtrm_stock_safety" class="col-md-2 control-label">Stock Safety Material</label>
	<div class="col-sm-2 col-xs-12">
		<div class="errInput" id="idErrRm_stock_safety"></div>
		<?php echo form_input(array_merge(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtrm_stock_safety', 'id'=> 'idtxtrm_stock_safety', 'placeholder' =>'Stock Safety Material' ,'value'=> isset($rm_stock_safety) ? $rm_stock_safety: null ), $arrayDisabled));?>
	</div>
</div>
<hr>

<div class="form-group">
	<label for="idtxtrm_hs_code" class="col-md-2 control-label">HS Code</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrrm_hs_code"></div>
		<?php echo form_input(array_merge(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtrm_hs_code', 'id'=> 'idtxtrm_hs_code', 'placeholder' =>'HS Code' ,'value'=> isset($rm_hs_code) ? $rm_hs_code: null ), $arrayDisabled));?>
	</div>
</div>

<div class="form-group">
	<label for="idtxtrm_jenisperolehan" class="col-md-2 control-label">Jenis Perolehan</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrrm_jenisperolehan"></div>
		<?php echo form_dropdown('txtrm_jenisperolehan', ['lokal' => 'Lokal', 'impor' => 'Impor'], isset($rm_jenisperolehan)? $rm_jenisperolehan : 'lokal', array_merge(array('class'=> 'form-control', 'id'=> 'idtxtrm_jenisperolehan'), $arrayDisabled));?>
	</div>
	<label for="idtxtnegara_id" class="col-md-2 control-label">Negara Perolehan</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrnegara_id"></div>
		<?php echo form_dropdown('txtnegara_id', $opsiNegara, isset($negara_id)? $negara_id : '', array_merge(array('class'=> 'form-control select2', 'id'=> 'idtxtnegara_id'), $arrayDisabled));?>
	</div>
</div>
<hr>

<div class="row" id="idrowplate" style="display: none;">
<div class="form-group">
	<label for="idtxtrm_platepanjang" class="col-md-2 control-label">Dimensi Plate (mm) (p)</label>
	<div class="col-sm-1 col-xs-12">
		<div class="errInput" id="idErrrm_platepanjang"></div>
		<?php echo form_input(array_merge(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtrm_platepanjang', 'id'=> 'idtxtrm_platepanjang', 'placeholder' =>'p' ,'value'=> isset($rm_platepanjang) ? $rm_platepanjang: null ), $arrayDisabled));?>
	</div>
	<label for="idtxtrm_platelebar" class="col-md-1 control-label">(l)</label>
	<div class="col-sm-1 col-xs-12">
		<div class="errInput" id="idErrrm_platelebar"></div>
		<?php echo form_input(array_merge(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtrm_platelebar', 'id'=> 'idtxtrm_platelebar', 'placeholder' =>'l' ,'value'=> isset($rm_platelebar) ? $rm_platelebar: null ), $arrayDisabled));?>
	</div>
	<label for="idtxtrm_platetebal" class="col-md-1 control-label">(tebal)</label>
	<div class="col-sm-1 col-xs-12">
		<div class="errInput" id="idErrrm_platetebal"></div>
		<?php echo form_input(array_merge(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtrm_platetebal', 'id'=> 'idtxtrm_platetebal', 'placeholder' =>'tebal' ,'value'=> isset($rm_platetebal) ? $rm_platetebal: null ), $arrayDisabled));?>
	</div>
</div>

<div class="form-group">
	<label for="idtxtrm_platemassajenis" class="col-md-2 control-label">MassaJenis Plate </label>
	<div class="col-sm-2 col-xs-12">
		<div class="errInput" id="idErrrm_platemassajenis"></div>
		<?php echo form_input(array_merge(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtrm_platemassajenis', 'id'=> 'idtxtrm_platemassajenis', 'placeholder' =>'Massa Jenis' ,'value'=> isset($rm_platemassajenis) ? $rm_platemassajenis: null ), $arrayDisabled));?>
	</div>
</div>
<hr>
</div>

<div class="form-group">
	<label for="idtxtrm_merk" class="col-md-2 control-label">Merk Material</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrRm_merk"></div>
		<?php echo form_input(array_merge(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtrm_merk', 'id'=> 'idtxtrm_merk', 'placeholder' =>'Merk Material' ,'value'=> isset($rm_merk) ? $rm_merk: null ), $arrayDisabled));?>
	</div>
</div>

<div class="form-group">
	<label for="idtxtrm_alias" class="col-md-2 control-label">Alias Material</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrRm_alias"></div>
		<?php echo form_input(array_merge(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtrm_alias', 'id'=> 'idtxtrm_alias', 'placeholder' =>'Alias Material' ,'value'=> isset($rm_alias) ? $rm_alias: null ), $arrayDisabled));?>
	</div>
</div>

<div class="form-group">
	<label for="idtxtrm_deskripsi" class="col-md-2 control-label">Deskripsi Material</label>
	<div class="col-sm-8 col-xs-12">
		<div class="errInput" id="idErrRm_deskripsi"></div>
		<?php echo form_textarea(array_merge(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtrm_deskripsi', 'id'=> 'idtxtrm_deskripsi', 'placeholder' =>'Deskripsi Material', 'rows' => '3', 'value'=> isset($rm_deskripsi) ? $rm_deskripsi: null ), $arrayDisabled));?>
	</div>
</div>

<div class="form-group">
	<label for="idtxtrm_spesifikasi" class="col-md-2 control-label">Spesifikasi Material</label>
	<div class="col-sm-8 col-xs-12">
		<div class="errInput" id="idErrRm_spesifikasi"></div>
		<?php echo form_textarea(array_merge(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtrm_spesifikasi', 'id'=> 'idtxtrm_spesifikasi', 'placeholder' =>'Spesifikasi Material', 'rows' => '3', 'value'=> isset($rm_spesifikasi) ? $rm_spesifikasi: null ), $arrayDisabled));?>
	</div>
</div>
<hr>
<?php if ($sts != 'view'): ?>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat btn-sm">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" id="idBtnSubmit" name="btnSubmit" onclick="submitData()" class="btn btn-primary btn-flat btn-sm">
			<i class="fa fa-save"></i> Simpan
		</button>
	</div>
</div>
<?php 
endif;
echo form_close(); ?>
