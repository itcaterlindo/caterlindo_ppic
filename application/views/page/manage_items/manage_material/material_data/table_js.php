<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table();
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table() {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_main'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function remove_box() {
		$('#idFormBox<?php echo $master_var; ?>').remove();
	}

	function open_form_box(sts, id) {
		remove_box();
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_box'; ?>',
			data: {'sts': sts, 'id' : id},
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function form_edit_min_stock()
	{
		remove_box();
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_edit_minstock_box'; ?>',
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function edit_data(id){
		open_form_box('edit', id) 
	}

	function view_data(id){
		open_form_box('view', id) 
	}

	function hapus_data(id) {
		var conf = confirm('Apakah anda yakin menghapus item ini ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						open_table();
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				}
			});
		}
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
		box_overlay('out');
    }

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

</script>