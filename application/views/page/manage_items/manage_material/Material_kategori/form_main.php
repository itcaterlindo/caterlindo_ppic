<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));

?>
<div class="form-group">
	<label for="idtxtrmkategori_kd" class="col-md-2 control-label">Kode Kategori</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrRmkategori_kd"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtrmkategori_kd', 'id'=> 'idtxtrmkategori_kd', 'placeholder'=>'Kode Kategori' ,'value'=> isset($id) ? $id: null ));?>
	</div>
</div>
<div class="form-group">
	<label for="idtxttipeAdmin" class="col-md-2 control-label">Nama Kategori</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrRmkategori_nama"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtrmkategori_nama', 'id'=> 'idtxtrmkategori_nama', 'placeholder'=>'Nama Kategori' , 'value'=> isset($rmkategori_nama) ? $rmkategori_nama: null ));?>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-sm btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" onclick="submitData()" class="btn btn-primary btn-sm btn-flat">
			<i class="fa fa-save"></i> Simpan
		</button>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	
</script>