<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtrmgroup_kd', 'name'=> 'txtrmgroup_kd', 'value' => isset($id) ? $id: null ));

?>
<div class="form-group">
	<label for="idtxttipeAdmin" class="col-md-2 control-label">Kode Group</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrRmgroup_nama"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtrmgroup_kode', 'id'=> 'idtxtrmgroup_kode', 'placeholder' => 'Kode Group','value'=> isset($rmgroupsup_kode) ? $rmgroupsup_kode: null ));?>
	</div>
</div>
<div class="form-group">
	<label for="idtxttipeAdmin" class="col-md-2 control-label">Nama Group</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrRmgroup_nama"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtrmgroup_nama', 'id'=> 'idtxtrmgroup_nama', 'placeholder' => 'Nama Group','value'=> isset($rmgroupsup_name) ? $rmgroupsup_name: null ));?>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-sm btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" onclick="submitData()" class="btn btn-primary btn-sm btn-flat">
			<i class="fa fa-save"></i> Simpan
		</button>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	
</script>