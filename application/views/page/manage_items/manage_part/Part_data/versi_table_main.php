<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<table id="idTableVersi" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:4%; text-align:center;" class="all">Opsi</th>
		<th style="width:30%; text-align:center;">Nama Part</th>
		<th style="width:10%; text-align:center;">Versi</th>
		<th style="width:10%; text-align:center;">Tanggal Rev</th>
		<th style="width:10%; text-align:center;">User</th>
	</tr>
	</thead>
	<tbody>
	<?php 
	$no = 1;
	foreach ($resultData as $r) :?>
	<tr>
		<td><?php echo $no;?></td>
		<td><?php 
			$btns = array();
			$btns[] = get_btn(array('title' => 'Detail Item', 'icon' => 'search', 'onclick' => 'detail_data_versi(\''.$r->part_kd.'\')'));
			$btn_group = group_btns($btns);
			echo $btn_group;
		?></td>
		<td><?php echo $r->partmain_nama;?></td>
		<td><?php echo $r->part_versi;?></td>
		<td><?php echo format_date($r->part_tglinput, 'd-m-Y H:i:s');?></td>
		<td><?php echo $r->nm_admin;?></td>
	</tr>
	<?php 
	$no++;
	endforeach;?>
	</tbody>
</table>

<script type="text/javascript">    
</script>