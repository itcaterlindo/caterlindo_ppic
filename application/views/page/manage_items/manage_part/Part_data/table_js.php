<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table('<?php echo $partjenis_kd; ?>', '<?php echo $partstate_kd; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table(partjenis_kd, partstate_kd) {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_main'; ?>',
				data: {partjenis_kd:partjenis_kd, partstate_kd:partstate_kd},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function render_select2(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_datetimepicker(valClass){
		$('.'+valClass).datetimepicker({
			format: 'DD-MM-YYYY',
    	});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}


	function push_to_sap(id, param){
		var conf = confirm('Apakah anda yakin Push item ini ?');
		
		if(conf){
			$.blockUI({ overlayCSS: { backgroundColor: '#C0C0C0' } });
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/push_sap'; ?>',
				data: {'id' : id, 'stts' : param},
				success: function(resp) {

					console.log(resp);
					if (resp.code == 200) {
						notify(resp.status, resp.pesan, 'success');
						console.log(resp);
						if(param == 'bom'){
							send_sap(resp.data, resp.data_labourcost, resp.data_ovh)
						}else{
						
							send_sap_item(resp.data);
						}
						
						
					}  else if (resp.code == 400) {
						notify(resp.status, resp.pesan, 'error');
						$.unblockUI();
					} else {
						notify('Error', 'Error tidak Diketahui', 'error');
						$.unblockUI();
					}
					
				},
				error: function(xhr, status, error){
					alert("Error!" + xhr);
					console.log(xhr);
				}
			});
		}
	}

	function send_sap_item(data){

		var datatoSAP = {
			"ItemCode": data[0].partmain_nama,
			"ItemName": data[0].partmain_note,
			"PurchaseItem": "N",
			"SalesItem": "N",
			"InventoryItem": "Y",
			"KdItemGroup": "14",
			"Dimensi": "",
			"NetWeight": 0,
			"GrossWeight": 0,
			"BoxWeight": 0,
			"Length": 0,
			"Width": 0,
			"Height": 0,
			"Volume": 0,
			"PrchseItem": "N",
			"SellItem": "N",
			"InvntItem": "Y",
			"StockSafety": 1,
			"U_IDU_WEBID": data[0].partmain_kd
		};

		fetch("<?= url_api_sap() ?>AddItem", {
					method: 'POST', // or 'PUT'
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(datatoSAP),
					})
					.then((response) => response.json())
					.then((items) => {
						console.log('Success:', items);
						console.log(datatoSAP);
						if(items.ErrorCode == 0){
							notify ('200!', items.Message, 'success');
							$.unblockUI();
						    open_table('', '');
							// window.location.reload();
						}else{
							rollback_sap(datatoSAP.U_IDU_WEBID, 'item');
							open_table('', '');
							$.unblockUI();
							notify (items.ErrorCode, items.Message, 'error');
						}
					})
					.catch((error) => {
						rollback_sap(datatoSAP.U_IDU_WEBID, 'item');
						$.unblockUI();
						notify ('xxx', error, 'error');
		});
	}

	function send_sap(params, items, item){
		var lines = [];
		var lines_kacangan = [];
		var plate_jenis_main = [];
		var plate_jenis_cut_size = [];
	

		for (let index = 0; index < params.length; index++) {
			if(params[index].partdetailplate_jenis == "main"){

				if (plate_jenis_main.filter(e => e.partmain_kd === params[index].partmain_kd && e.partdetail_kd === params[index].partdetail_kd).length > 0) {
						plate_jenis_main.filter(e => e.partmain_kd == params[index].partmain_kd && e.partdetail_kd == params[index].partdetail_kd)[0].CUT_SIZE +=  parseFloat(params[index].CUT_SIZE);
					/* vendors contains the element we're looking for */
				}else{
					plate_jenis_main.push({
							"partmain_kd": params[index].partmain_kd,
							"partdetail_kd": params[index].partdetail_kd,
							"rm_kode": params[index].rm_kode,
							"CUT_SIZE": parseFloat(params[index].CUT_SIZE)
						})
				}
				
			}else if(params[index].partdetailplate_jenis == "cut_size"){
				if (plate_jenis_cut_size.filter(e => e.partmain_kd === params[index].partmain_kd && e.partdetail_kd === params[index].partdetail_kd).length > 0) {
						plate_jenis_cut_size.filter(e => e.partmain_kd == params[index].partmain_kd && e.partdetail_kd == params[index].partdetail_kd)[0].CUT_SIZE +=  parseFloat(params[index].CUT_SIZE);
					/* vendors contains the element we're looking for */
					}else{
						plate_jenis_cut_size.push({
								"partmain_kd": params[index].partmain_kd,
								"partdetail_kd": params[index].partdetail_kd,
								"rm_kode": params[index].rm_kode,
								"CUT_SIZE": parseFloat(params[index].CUT_SIZE)
							})
					}
			}else{
				if(!params[index].partdetailplate_jenis){
					lines.push({"RouteStage": params[index].bagian_kd,
							"ItemType": "I",
							"ItemCode": params[index].rm_kode, 
							"Quantity":  params[index].partdetail_qty,
							"Warehouse": "MGD051222003", 
							"IssueMethod": "B"
						})
				}
			}
			
			
		}

		for (let i = 0; i < plate_jenis_main.length; i++) {
						for (let j = 0; j < plate_jenis_cut_size.length; j++) {
							if(plate_jenis_main[i].partmain_kd == plate_jenis_cut_size[i].partmain_kd && plate_jenis_main[i].partdetail_kd == plate_jenis_cut_size[i].partdetail_kd){
									lines_kacangan.push({"RouteStage": '10',
										"ItemType": "I",
										"ItemCode": plate_jenis_main[i].rm_kode,
										"Quantity":  (plate_jenis_main[i].CUT_SIZE / plate_jenis_cut_size[i].CUT_SIZE ).toFixed(4),
										"Warehouse": "MGD051222003", 
										"IssueMethod": "B",
										"Ident" : plate_jenis_main[i].partdetail_kd,
									})
							}else{
								lines_kacangan.push({"RouteStage": '10',
										"ItemType": "I",
										"ItemCode": plate_jenis_main[i].rm_kode,
										"Quantity":  (plate_jenis_main[i].CUT_SIZE / plate_jenis_cut_size[i].CUT_SIZE ).toFixed(4),
										"Warehouse": "MGD051222003", 
										"IssueMethod": "B",
										"Ident" : plate_jenis_main[i].partdetail_kd,
									})
							}
							
						}
					}

		if(items.length != 0){

			for (let index = 0; index < items.length; index++) {
				lines.push({   "RouteStage": items[index].bagian_kd,
							"ItemType": "R",
							"ItemCode": 'LAB001', 
							"Quantity":  (items[index].partlabourcost_durasi * items[index].partlabourcost_qty).toFixed(4),
							"Warehouse": "MGD051222003", 
							"IssueMethod": "B"
						})
			}
			
		}
		// lines = lines.sort((a, b) => a.ItemType > b.ItemType ? 1 : -1);
		// lines.sort((a, b) => a.RouteStage > b.RouteStage ? 1 : -1);
		lines.sort((a, b) => a.RouteStage - b.RouteStage || a.ItemType - b.ItemType);

		if(item.length != 0){

				for (let index = 0; index < item.length; index++) {
					lines.push({   "RouteStage":'60',
								"ItemType": "R",
								"ItemCode": item[index].sap_kd,
								"Quantity":  item[index].partoverhead_qty,
								"Warehouse": "MGD051222003", 
								"IssueMethod": "B"
							})
				}

			}

			var uniqueArray = removeDuplicates(lines_kacangan, "Ident");
			Array.prototype.push.apply(lines,uniqueArray); 


			var dataToSAP = {
							"U_IDU_WEBID": params[0].partmain_kd,
							"Code": params[0].partmain_nama,
							"Name": params[0].partmain_note,
							"Quantity": '1',
							"TreeType": 'P',
							"Warehouse": 'MGD051222003', //warehouse Wip Prod,
							"Lines": lines
						}

		
					
					// console.log(dataToSAP);
					// console.log(items);
					// console.log(lines_kacangan);
					// console.log(plate_jenis_main);
					// console.log(plate_jenis_cut_size);
					// console.log(uniqueArray);
					// //rollback_sap(dataToSAP.U_IDU_WEBID);
					// rollback_sap(dataToSAP.U_IDU_WEBID, 'bom');
					// $.unblockUI();

					fetch("<?= url_api_sap33() ?>AddBOM", {
								method: 'POST', // or 'PUT'
								headers: {
									'Content-Type': 'application/json',
								},
								body: JSON.stringify(dataToSAP),
								})
								.then((response) => response.json())
								.then((items) => {
									console.log('Success:', items);
									console.log(dataToSAP);
									if(items.ErrorCode == 0){
										notify ('200!', items.Message, 'success');
										$.unblockUI();
									    open_table('', '');
										// window.location.reload();
									}else{
										rollback_sap(dataToSAP.U_IDU_WEBID, 'bom');
										open_table('', '');
										$.unblockUI();
										notify (items.ErrorCode, items.Message, 'error');
									}
								})
								.catch((error) => {
									rollback_sap(dataToSAP.U_IDU_WEBID, 'bom');
									$.unblockUI();
									notify ('xxx', error, 'error');
					});

	

		

	}

	function removeDuplicates(originalArray, prop) {
		var newArray = [];
		var lookupObject  = {};

		for(var i in originalArray) {
			lookupObject[originalArray[i][prop]] = originalArray[i];
		}

		for(i in lookupObject) {
			newArray.push(lookupObject[i]);
		}
		return newArray;
	}

	function rollback_sap(params, stts) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/rollback_sap'; ?>',
				data: {'id' : params, 'stts' : stts},
				success: function(resp) {
					if (resp.code == 200) {
						notify(resp.status, resp.pesan, 'success');
					} else if (resp.code == 401) {
						notify(resp.status, resp.pesan, 'error');
					} else if (resp.code == 400) {
						notify(resp.status, resp.pesan, 'error');
					} else {
						notify('Error', 'Error tidak Diketahui', 'error');
					}
				}
			});
			
		}

	function remove_box() {
		$('#idFormBox<?php echo $master_var; ?>').remove();
	}

	function open_form_box(sts, id) {
		remove_box();
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_box'; ?>',
			data: {'sts': sts, 'id' : id},
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	// // Untuk detail pdf
	// function open_view_detail_box(sts, id, part_kd){
	// 	$('#idDetailBox<?php //echo $master_var; ?>').remove();
	// 	$('#<?php //echo $box_alert_id; ?>').fadeOut();
	// 	$.ajax({
	// 		type: 'GET',
	// 		url: '<?php //echo base_url().$class_link.'/view_detail_box'; ?>',
	// 		data: {'sts': sts, 'id' : id, 'part_kd' : part_kd},
	// 		success: function(html) {
	// 			$('#idMainContent').prepend(html);
	// 			moveTo('idMainContent');
	// 		}
	// 	});
	// }

	function view_detailtable_box(partmain_kd) {
		window.location.assign('<?= base_url().$class_link?>/view_detailtable_box?partmain_kd='+partmain_kd);
	}

	function edit_data(id){
		open_form_box('edit', id); 
	}

	function detail_data (id){
		// open_view_detail_box('detail', id, '');
		view_detailtable_box(id);
	}

	function duplicate_data(id){
		open_form_box('duplicate', id); 
	}

	function detail_versi(partmain_kd) {
	// 	$('#idTableVersiBox<?php //echo $master_var; ?>').remove();
	// 	$.ajax({
	// 		type: 'GET',
	// 		url: '<?php //echo base_url().$class_link.'/versi_box'; ?>',
	// 		data: {'id' : id},
	// 		success: function(html) {
	// 			$('#idMainContent').prepend(html);
	// 		}
	// 	});
		window.location.assign('<?= base_url()?>/manage_items/manage_part/part_versi?partmain_kd='+partmain_kd);
	}

	function ubah_status(partmain_kd, partmain_status) {
		var conf = confirm('Apakah anda yakin mengubah item ini ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_ubah_status'; ?>',
				data: {partmain_kd:partmain_kd, partmain_status:partmain_status},
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						open_table();
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				}
			});
		}
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
		box_overlay('out');
    }

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

</script>