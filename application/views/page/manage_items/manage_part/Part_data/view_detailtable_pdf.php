<?php

class customPdf extends Tcpdf {
    public $part_tglinput; 
    public $part_versi; 
    public $partmain_nama;
    public $partjenis_nama;

    function setKeterangan ($arrayKet = []) {
        $this->part_tglinput = !empty($arrayKet['part_tglinput']) ? $arrayKet['part_tglinput'] : null;
        $this->part_versi = !empty($arrayKet['part_versi']) ? $arrayKet['part_versi'] : '-';
        $this->partmain_nama = !empty($arrayKet['partmain_nama']) ? $arrayKet['partmain_nama'] : '-';
        $this->partjenis_nama = !empty($arrayKet['partjenis_nama']) ? $arrayKet['partjenis_nama'] : '-';
    }

    public function Header() {
        $header = '
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td rowspan="2" colspan="2" style="text-align:left;"> <img src="assets/admin_assets/dist/img/logo_cat.png" width="95" height="30"> </td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="2" style="font-size:16px; font-weight:bold; text-align:right;">BILL OF MATERIAL</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td style="font-size:8px;">No Dokumen <br>Tanggal terbit <br>Rev</td>
                <td style="font-size:8px; text-align:left;">: CAT4-RND-002 <br>: 04-12-2020 <br>: 03</td>
            </tr>
        </table>';

        /** Keterangan */
        $keterangan =
        '<table cellspacing="0" cellpadding="0" border="0" style="font-size: 80%;">
            <tr>
                <td style="text-align:left;">Date</td>
                <td style="text-align:left;">: '.format_date($this->part_tglinput, 'd-m-Y').' </td>
                <td></td>
                <td style="text-align:left;">PJ Number</td>
                <td style="text-align:left;">: _____________</td>
            </tr>
            <tr>
                <td style="text-align:left;">Item Code</td>
                <td style="text-align:left;">: '.'PART'.' </td>
                <td></td>
                <td style="text-align:left;">Rev.</td>
                <td style="text-align:left;">: '.$this->part_versi.'</td>
            </tr>
            <tr>
                <td style="text-align:left;">Description</td>
                <td colspan="2" style="text-align:left;">: '.$this->partmain_nama. ' </td>
                <td style="text-align:left;">Page</td>
                <td style="text-align:left;">: '.$this->getAliasNumPage().' of '.$this->getAliasNbPages().'</td>
            </tr>
            <tr>
                <td style="text-align:left;"></td>
                <td colspan="2" style="text-align:left;"> '.'-'.' </td>
                <td></td>
                <td style="text-align:left;"></td>
                <td style="text-align:left;"></td>
            </tr>
            <tr>
                <td style="text-align:left;">Detail</td>
                <td style="text-align:left;">: '.$this->partjenis_nama.' </td>
                <td></td>
                <td style="text-align:left;"></td>
                <td style="text-align:left;"></td>
            </tr>
        

        </table>';

        
        $this->writeHTML($header, true, false, false, false, '');
        $this->writeHTML($keterangan, true, false, false, false, '');
    }
}

$pdf = new customPdf('P', 'mm', 'A4', true, 'UTF-8', false);
$title = $keterangan['partmain_nama'].'ver'.$keterangan['part_versi'];
$pdf->SetTitle($title);
// $pdf->SetAutoPageBreak(true);
$pdf->setPrintFooter(false);
// $pdf->SetMargins(10, 10, 10, true);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');
/** Margin */ 
$pdf->SetHeaderMargin(5);
$pdf->SetFooterMargin(10);
$pdf->SetMargins(5, 50, 5);

$pdf->setKeterangan($keterangan);

$pdf->AddPage();

$pdf->writeHTML($konten, true, false, false, false, '');

$created=null;$createdtgl=null;$checked1=null;$checked1tgl=null;
$checked2=null;$checked2tgl=null;$approved=null;$approvedtgl=null;
foreach ($states as $state) :
    switch ($state['partstate_kd']) {
        case 2:
            $created = $state['nm_admin'];
            $createdtgl = format_date($state['partstatelog_tglinput'], 'd-m-Y H:i:s');
            break;
        case 3:
            $checked1 = $state['nm_admin'];
            $checked1tgl = format_date($state['partstatelog_tglinput'], 'd-m-Y H:i:s');
            break;
        case 4:
            $checked2 = $state['nm_admin'];
            $checked2tgl = format_date($state['partstatelog_tglinput'], 'd-m-Y H:i:s');
            break;
        case 5:
            $approved = $state['nm_admin'];
            $approvedtgl = format_date($state['partstatelog_tglinput'], 'd-m-Y H:i:s');
            break;
    }
endforeach;

$footer = '
<table cellspacing="0" cellpadding="1" border="1" style="font-size: 75%;">
    <tr>
        <td width="15%"> Created By </td>
        <td width="20%"> : '.$created.' </td>
        <td width="20%"> '.$createdtgl.' </td>
    </tr>
    <tr>
        <td width="15%"> Checked1 By </td>
        <td width="20%"> : '.$checked1.' </td>
        <td width="20%"> '.$checked1tgl.' </td>
    </tr>
    <tr>
        <td width="15%"> Checked2 By </td>
        <td width="20%"> : '.$checked2.' </td>
        <td width="20%"> '.$checked2tgl.' </td>
    </tr>
    <tr>
        <td width="15%"> Approved By </td>
        <td width="20%"> : '.$approved.' </td>
        <td width="20%"> '.$approvedtgl.' </td>
    </tr>
</table>';

$pdf->writeHTML($footer, true, false, false, false, '');

$txtOutput = $title.'.pdf';
$pdf->Output($txtOutput, 'I');