<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
extract($rowDataMasterPart);
if($sts == 'edit'):
	extract($rowData);
endif;

?>
<div class="row invoice-info">
	<div class="col-sm-6 invoice-col">
		<b>Nama Part :</b> <?php echo isset($partmain_nama) ? $partmain_nama : '-'; ?><br>
		<b>Jenis Part :</b> <?php echo isset($partjenis_nama) ? $partjenis_nama : '-'; ?><br>
	</div>
	<div class="col-sm-6 invoice-col">
		<b>Versi :</b> <?php echo isset($part_versi) ? $part_versi : '-'; ?><br>
		<b>Tanggal :</b> <?php echo format_date($part_tgledit, 'd-m-Y H:i:s') ?><br>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<div class="col-sm-4 col-xs-12">
				<div class="btn-group">	
					<button onclick="open_formbom()" class="btn btn-default btn-sm"> <i class="fa fa-refresh"></i> Refresh </button>
					<button onclick="open_labourcost()" class="btn btn-info btn-sm"> <i class="fa fa-arrow-circle-right"></i> Labourcost </button>
				</div>
			</div>
		</div>
	</div>
</div>
<hr>
<?php 
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'placeholder' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpart_kd', 'placeholder' => 'idtxtpart_kd', 'name'=> 'txtpart_kd', 'value' => isset($part_kd) ? $part_kd: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtStsDetail', 'placeholder' => 'idtxtStsDetail', 'name'=> 'txtStsDetail', 'class' => 'tt-input'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpartdetail_kd', 'placeholder' => 'idtxtpartdetail_kd', 'name'=> 'txtpartdetail_kd', 'class' => 'tt-input'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpartjenis_kd', 'placeholder' => 'idtxtpartjenis_kd', 'name'=> 'txtpartjenis_kd', 'class' => 'tt-input', 'value' => isset($partjenis_kd) ? $partjenis_kd: null ));
?>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label for="idtxtbagian_kd" class="col-md-2 control-label">Bagian</label>
			<div class="col-sm-4 col-xs-12">
				<div class="errInput" id="idErrbagian_kd"></div>
				<?php echo form_dropdown('txtbagian_kd', $opsiBagian, isset($bagianSelected)? $bagianSelected : '', array('class'=> 'form-control', 'id'=> 'idtxtbagian_kd'));?>
			</div>	
		</div>
		
		<div class="form-group">
			<label for="idtxtjenisMaterial" class="col-md-2 control-label">Jenis Material</label>
			<div class="col-sm-4 col-xs-12">
				<div class="errInput" id="idErrjenisMaterial"></div>
				<?php echo form_dropdown('txtjenisMaterial', $opsiJenisMaterial, isset($jenisMaterialSelected)? $jenisMaterialSelected : '', array('class'=> 'form-control', 'id'=> 'idtxtjenisMaterial'));?>
			</div>	
		</div>

		<div class="form-group">
			<label for="idtxtrm_kd" class="col-md-2 control-label">Material</label>
			<div class="col-sm-6 col-xs-12">
				<div class="errInput" id="idErrrm_kd"></div>
				<?php echo form_dropdown('txtrm_kd', $opsiMaterial, isset($MaterialSelected)? $MaterialSelected : '', array('class'=> 'form-control select2', 'id'=> 'idtxtrm_kd'));?>
			</div>	
			<label for="idtxtpartdetail_qty" class="col-md-1 control-label">Qty</label>
			<div class="col-sm-2 col-xs-12">
				<div class="errInput" id="idErrpartdetail_qty"></div>
				<?php echo form_input(array('type'=>'number', 'class'=> 'form-control tt-input', 'name'=> 'txtpartdetail_qty', 'id'=> 'idtxtpartdetail_qty', 'placeholder' =>'Qty' ,'value'=> isset($partdetail_qty) ? $partdetail_qty: null ));?>
			</div>	
		</div>

		<div class="form-group">
			<label for="idtxtrm_nama" class="col-md-2 control-label">Nama Material</label>
			<div class="col-sm-4 col-xs-12">
				<div class="errInput" id="idErrrm_nama"></div>
				<?php echo form_input(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtrm_nama', 'id'=> 'idtxtrm_nama', 'placeholder' =>'Nama Material', 'readonly' => 'true', 'value'=> isset($rm_nama) ? $rm_nama: null ));?>

			</div>
			<label for="idtxtrmsatuan_nama" class="col-md-1 control-label">Satuan</label>
			<div class="col-sm-4 col-xs-12">
				<div class="errInput" id="idErrrmsatuan_nama"></div>
				<?php echo form_input(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtrmsatuan_nama', 'id'=> 'idtxtrmsatuan_nama', 'placeholder' =>'Satuan', 'readonly' => 'true', 'value'=> isset($rmsatuan_nama) ? $rmsatuan_nama: null ));?>
				<div id="iddropdown_rmsatuankd" >
					<select id="idtxtrmsatuan_kd" name="txtrmsatuan_kd" class= "form-control"> </select>
				</div>
			</div>	
		</div>

		<div class="form-group">
			<label for="idtxtrm_deskripsi" class="col-md-2 control-label">Deskripsi Material</label>
			<div class="col-sm-9 col-xs-12">
				<div class="errInput" id="idErrrm_deskripsi"></div>
				<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtrm_deskripsi', 'id'=> 'idtxtrm_deskripsi', 'placeholder' =>'Deskripsi Material', 'rows' => '2', 'readonly' => 'true', 'value'=> isset($rm_deskripsi) ? $rm_deskripsi: null ));?>
			</div>
		</div>

		<div class="form-group">
			<label for="idtxtrm_spesifikasi" class="col-md-2 control-label">Spesifikasi Material</label>
			<div class="col-sm-9 col-xs-12">
				<div class="errInput" id="idErrrm_spesifikasi"></div>
				<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtrm_spesifikasi', 'id'=> 'idtxtrm_spesifikasi', 'placeholder' =>'Spesifikasi Material', 'rows' => '2', 'readonly' => 'true', 'value'=> isset($rm_spesifikasi) ? $rm_spesifikasi: null ));?>
			</div>
		</div>

		<div class="row" id="idrowPlateDimensi" style="display:none;">
			<label for="idtxtrm_platepanjang" class="col-md-2 control-label">Dimensi Plate (p) (mm)</label>
			<div class="col-sm-1 col-xs-12">
				<div class="errInput" id="idErrrm_platepanjang"></div>
				<?php echo form_input(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtrm_platepanjang', 'id'=> 'idtxtrm_platepanjang', 'placeholder' =>'p', 'readonly' => 'true', 'value'=> isset($rm_platepanjang) ? $rm_platepanjang: null ));?>
			</div>
			<label for="idtxtrm_platelebar" class="col-md-1 control-label">(l) (mm)</label>
			<div class="col-sm-1 col-xs-12">
				<div class="errInput" id="idErrrm_platelebar"></div>
				<?php echo form_input(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtrm_platelebar', 'id'=> 'idtxtrm_platelebar', 'placeholder' =>'l', 'readonly' => 'true', 'value'=> isset($rm_platelebar) ? $rm_platelebar: null ));?>
			</div>
			<label for="idtxtrm_platetebal" class="col-md-1 control-label">(tebal) (mm)</label>
			<div class="col-sm-1 col-xs-12">
				<div class="errInput" id="idErrrm_platetebal"></div>
				<?php echo form_input(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtrm_platetebal', 'id'=> 'idtxtrm_platetebal', 'placeholder' =>'tbl', 'readonly' => 'true', 'value'=> isset($rm_platetebal) ? $rm_platetebal: null ));?>
			</div>
			<label for="idtxtrm_platemassa" class="col-md-1 control-label">(massa)(kg)</label>
			<div class="col-sm-1 col-xs-12">
				<div class="errInput" id="idErrrm_platemassa"></div>
				<?php echo form_input(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtrm_platemassa', 'id'=> 'idtxtrm_platemassa', 'placeholder' =>'tbl', 'readonly' => 'true', 'value'=> isset($rm_platemassa) ? $rm_platemassa: null ));?>
			</div>
		</div>
	</div>
	<hr>
</div>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-3 col-sm-offset-9 col-xs-12">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData()" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button type="reset" name="btnReset" onclick="resetFormMain()" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>
