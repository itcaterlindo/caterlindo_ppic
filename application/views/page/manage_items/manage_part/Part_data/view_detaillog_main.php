<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
?>

<?php foreach ($resultPartstateLog as $r) : ?>

<ul class="timeline">
    <li>
        <i class="fa fa-comments bg-yellow"></i>
        <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i> <?= format_date($r['partstatelog_tglinput'], 'd-m-Y H:i:s');?></span>
            <h4 class="timeline-header"> <strong> <?= $r['nm_admin']; ?> </strong> mengubah ke <?= $r['partstate_nama'];?> </h4>
        <div class="timeline-body"><?= $r['partstatelog_note'];?></div>
        </div>
    </li>
</ul> 

<?php endforeach;?>