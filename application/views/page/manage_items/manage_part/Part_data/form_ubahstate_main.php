<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormUbahstate';

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpart_kd', 'name'=> 'txtpart_kd', 'value' => $part_kd ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpartstate_kd', 'name'=> 'txtpartstate_kd', 'value' => isset($partstate_kd) ? $partstate_kd: null ));

?>

<div class="col-md-12">

    <?php if ($specialState) :?>
    <div class="form-group">
		<label for="idtxtopsipartstate_kd" class="col-md-2 control-label">State</label>
		<div class="col-sm-5 col-xs-12">
			<div class="errInput" id="idErropsipartstate_kd"></div>
			<?php echo form_dropdown('txtopsipartstate_kd', $opsiPartstate, isset($partstate_kd)? $partstate_kd : '', array('class'=> 'form-control', 'id'=> 'idtxtopsipartstate_kd'));?>
		</div>	
	</div>
    <?php endif;?>
    <div class="form-group">
        <label for="idtxtpartstate_note" class="col-md-2 control-label">Keterangan</label>
        <div class="col-sm-10 col-xs-12">
            <div class="errInput" id="idErrpartstate_note"></div>
            <?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtpartstate_note', 'id'=> 'idtxtpartstate_note', 'placeholder' =>'Keterangan', 'rows' => '3' ) );?>
        </div>
    </div>

</div>

<div class="col-md-12">
	<button type="submit" name="btnSubmit" id="idbtnSubmitState" onclick="submitState()" class="btn btn-primary btn-sm pull-right">
		<i class="fa fa-save"></i> Simpan
	</button>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
    $('#idtxtopsipartstate_kd').change(function(){
        $('#idtxtpartstate_kd').val(this.value);
	});
</script>