<?php

class customPdf extends Tcpdf {

    public function Header() {
        $header = '
        <table cellspacing="0" cellpadding="0" border="1">
            <tr>
                <td rowspan="2" colspan="2" style="text-align:left;"> <img src="assets/admin_assets/dist/img/logo_cat.png" width="95" height="30"> </td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="2" style="font-size:16px; font-weight:bold; text-align:right;">BILL OF MATERIAL</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td style="font-size:8px;">No Dokumen <br>Tanggal terbit <br>Rev</td>
                <td style="font-size:8px; text-align:left;">: CAT4-RND-002 <br>: 04-12-2020 <br>: 03</td>
            </tr>
        </table>';

        $this->writeHTML($header, true, false, false, false, '');
    }

    public function Footer() {
        $footer = '
        <table cellspacing="0" cellpadding="0" border="0" width="570">
            <tr style="text-align:center; font-weight:bold; font-size: 10px;">
                <td width="20%"> Dibuat Oleh, </td>
                <td width="5%"></td>
                <td width="45%" colspan="3"> Diperiksa Oleh, </td>
                <td width="5%"></td>
                <td width="20%"> Disetujui oleh, </td>
            </tr>
            <tr>
                <td height="60"></td>
                <td height="60"></td>
                <td colspan="3" height="60"></td>   
                <td height="60"></td>                
                <td height="60"></td>                
            </tr>
            <tr style="text-align:center; font-size: 10px;">
                <td style="border-top: 1px solid black;">IME</td>
                <td></td>
                <td width="20%" style="border-top: 1px solid black;">RnD Supervisor</td>
                <td width="5%"></td>
                <td width="20%" style="border-top: 1px solid black;">Head of Production</td>
                <td></td>
                <td style="border-top: 1px solid black;">Operational Manager</td>
            </tr>
        </table>';

        $this->writeHTML($footer, true, false, false, false, '');
    }
}

$pdf = new customPdf('P', 'mm', 'A4', true, 'UTF-8', false);
$title = 'BOM_'.$rowPart['partmain_nama'];
$pdf->SetTitle($title);
$pdf->SetAutoPageBreak(true);
$pdf->SetMargins(10, 10, 10, true);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');
/** Margin */ 
$pdf->SetHeaderMargin(5);
$pdf->SetFooterMargin(37);
$pdf->SetMargins(5, 25, 5);

/** Group by part */
if (!empty($resultMaterial)){
    
foreach ($resultMaterial as $element) :
    $resGroupbyPart[$element['partmain_nama']][$element['bagian_nama']][] = $element;
endforeach;

foreach($resGroupbyPart as $eachPart => $elementsPart) :

$pdf->AddPage();

/** Keterangan */
$keterangan =
        '<table cellspacing="0" cellpadding="0" border="0" style="font-size:10px;">
            <tr>
                <td style="text-align:left;">Tanggal</td>
                <td style="text-align:left;">: '.format_date($rowPart['part_tglinput'], 'd-M-Y').' </td>
                <td></td>
                <td style="text-align:left;">Nomor PJ</td>
                <td style="text-align:left;">: _____________</td>
            </tr>
            <tr>
                <td style="text-align:left;">Kode Item</td>
                <td style="text-align:left;">: '.'PART'.' </td>
                <td></td>
                <td style="text-align:left;">Rev.</td>
                <td style="text-align:left;">: '.$rowPart['part_versi'].'</td>
            </tr>
            <tr>
                <td style="text-align:left;">Deskripsi</td>
                <td colspan="2" style="text-align:left;">: '.$rowPart['partmain_nama']. ' </td>
                <td style="text-align:left;"></td>
                <td style="text-align:left;"></td>
            </tr>
            <tr>
                <td style="text-align:left;"></td>
                <td colspan="2" style="text-align:left;"> '.'-'.' </td>
                <td></td>
                <td style="text-align:left;"></td>
                <td style="text-align:left;"></td>
            </tr>
            <tr>
                <td style="text-align:left;">Detail</td>
                <td style="text-align:left;">: '.$rowPart['partjenis_nama'].' </td>
                <td></td>
                <td style="text-align:left;"></td>
                <td style="text-align:left;"></td>
            </tr>
           

        </table>';

$pdf->writeHTML($keterangan, true, false, false, false, '');

$konten =
        '<table cellspacing="0" cellpadding="1" border="0" style="font-size:10px;" width="100%">
            <tr style="text-align:center; font-weight:bold;">
                <th width="5%" style="border-left: 1px solid black; border-top: 1px solid black;">No.</th>
                <th width="20%" style="border-left: 1px solid black; border-top: 1px solid black;">Kode Material</th>
                <th colspan="2" width="57%" style="border-left: 1px solid black; border-top: 1px solid black;">Deskripsi</th>
                <th width="8%" style="border-left: 1px solid black; border-top: 1px solid black;">Jumlah</th>
                <th width="10%" style="border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black;">Satuan</th>
            </tr>';

/** Group by bagian */
$no = 1;
foreach ($resultMaterial as $element) {
    $resGroupby[$element['bagian_nama']][] = $element;
}

foreach($elementsPart as $eachBagian => $elementsBagian) :
    $massaCutsize = 0;
    $konten .=
            '<tr>
                <td width="5%" style="text-align:center; border-left: 1px solid black; border-top: 1px solid black;">'.$no.'</td>
                <td width="20%" style="text-align:left; font-weight:bold; border-left: 1px solid black; border-top: 1px solid black;"> '.$eachBagian.' </td>
                <td width="50%" style="border-left: 1px solid black; border-top: 1px solid black;"></td>
                <td width="7%" style="border-top: 1px solid black;"></td>
                <td width="8%" style="text-align:left; border-left: 1px solid black; border-top: 1px solid black;"></td>
                <td width="10%" style="text-align:left; border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black;"></td>
            </tr>';
     /** detail */
    foreach ($elementsBagian as $eachElement) :
        /** Untuk kategori plate asli taruh di kolom sebelahnya */
        $partdetail_qty = number_format($eachElement['partdetail_qty'], 3, ',', '');
        $perdetail_qtyplate = null;
        if ($eachElement['rmkategori_kd'] == '01') {
            $perdetail_qtyplate = $partdetail_qty;
            $partdetail_qty = null;
        }
        $konten .=
            '<tr>
                <td style="text-align:center; border-left: 1px solid black; border-top: 1px solid black;"></td>
                <td style="text-align:left; border-left: 1px solid black; border-top: 1px solid black;"> '.$eachElement['rm_kode'].' </td>
                <td style="border-left: 1px solid black; border-top: 1px solid black;">'.$eachElement['partdetail_deskripsi'].'/'.$eachElement['partdetail_spesifikasi'].'</td>
                <td style="text-align:right; font-style: italic; border-top: 1px solid black;">'.$perdetail_qtyplate.'</td>
                <td style="text-align:right; border-left: 1px solid black; border-top: 1px solid black;">'.$partdetail_qty.'</td>
                <td style="text-align:center; border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black;">'.$eachElement['rmsatuan_nama'].'</td>
            </tr>';
        /** Untuk Plate */
        if ($eachElement['rmkategori_kd'] == '01'):
            $massaCutsize = generate_massa_plate($eachElement['rm_platepanjang'], $eachElement['rm_platelebar'], $eachElement['rm_platetebal'], $eachElement['rm_platemassajenis']) * $eachElement['partdetail_qty'];
            $konten .=
            '<tr>
                    <td style="text-align:center; border-left: 1px solid black; border-top: 1px solid black;"></td>
                    <td style="text-align:center; border-left: 1px solid black; border-top: 1px solid black;"></td>
                    <td style="text-align:left; border-left: 1px solid black; border-top: 1px solid black;"> '.sprintf("%04s", (int)$eachElement['rm_platepanjang']).' x '.sprintf("%04s", (int)$eachElement['rm_platelebar']).' = '.
                    $eachElement['partdetail_qty'].' (Cut Size)'.'
                    </td>
                    <td style="text-align:right; font-size: 9px; font-style: italic; border-top: 1px solid black;">'.number_format($massaCutsize, 3, ',', '').'</td>
                    <td style="text-align:right; border-left: 1px solid black; border-top: 1px solid black;"></td>
                    <td style="text-align:center; border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black;">Kg</td>
                </tr>';
        endif;
        /** Detail Plate */
        foreach($resultDetailPlate as $eachPlate):
            if ($eachPlate['partdetail_kd'] == $eachElement['partdetail_kd']):
                if ($eachPlate['partdetailplate_deskripsi'] != 'Waste') {
                    $massaAdditional = null;
                    $massaMain = number_format($eachPlate['partdetailplate_massa'], 3, ',', '');
                    if ($eachPlate['partdetailplate_jenis'] == 'additional') {
                        $massaAdditional = $massaMain;
                        $massaMain = null;
                    }
                    $konten .=
                    '<tr>
                        <td style="text-align:center; border-left: 1px solid black; border-top: 1px solid black;"></td>
                        <td style="text-align:center; border-left: 1px solid black; border-top: 1px solid black;"></td>
                        <td style="text-align:left; border-left: 1px solid black; border-top: 1px solid black;"> '.$eachPlate['partdetailplate_deskripsi'].'</td>
                        <td style="text-align:right; font-size: 9px; font-style: italic; border-top: 1px solid black;">'.$massaAdditional.'</td>
                        <td style="text-align:right; border-left: 1px solid black; border-top: 1px solid black;">'.$massaMain.'</td>
                        <td style="text-align:center; border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black;">Kg</td>
                    </tr>';
                }else {
                    $konten .=
                    '<tr>
                        <td style="text-align:center; border-left: 1px solid black; border-top: 1px solid black;"></td>
                        <td style="text-align:center; border-left: 1px solid black; border-top: 1px solid black;"></td>
                        <td style="text-align:left; font-style: italic; border-left: 1px solid black; border-top: 1px solid black;"> '.$eachPlate['partdetailplate_deskripsi'].'</td>
                        <td style="text-align:right; font-size: 9px; font-style: italic; border-top: 1px solid black;"></td>
                        <td style="text-align:right; border-left: 1px solid black; border-top: 1px solid black;">'.number_format((float) ($massaCutsize - $eachPlate['partdetailplate_massa']), 3, ',', '').'</td>
                        <td style="text-align:center; border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black;">Kg</td>
                    </tr>';
                }
            endif;
        endforeach;
    endforeach;
    $no++;
endforeach;

/** Part Labourcost */
$konten .=
            '<tr>
                <td style="text-align:center; border-left: 1px solid black; border-top: 1px solid black;">'.$no.'</td>
                <td style="text-align:left; font-weight:bold; border-left: 1px solid black; border-top: 1px solid black;"> LABOUR COST </td>
                <td style="border-left: 1px solid black; border-top: 1px solid black;"></td>
                <td style="text-align:center; border-top: 1px solid black;"></td>
                <td style="text-align:left; border-left: 1px solid black; border-top: 1px solid black;"></td>
                <td style="text-align:center; border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black;"></td>
            </tr>';

foreach ($resultLabourcost as $eachLabourcost):
    if ($eachLabourcost['partmain_nama'] == $eachPart){
        $konten .=
            '<tr>
                <td style="text-align:center; border-left: 1px solid black; border-top: 1px solid black;"></td>
                <td style="text-align:left; border-left: 1px solid black; border-top: 1px solid black;"> '.$eachLabourcost['bagian_kd'].' </td>
                <td style="border-left: 1px solid black; border-top: 1px solid black;">'.$eachLabourcost['bagian_nama'].' ('.$eachLabourcost['partlabourcost_qty'].' '.$eachLabourcost['partlabourcost_satuan'].')'.'</td>
                <td style="text-align:center; border-top: 1px solid black;"></td>
                <td style="text-align:right; border-left: 1px solid black; border-top: 1px solid black;">'.number_format($eachLabourcost['partlabourcost_durasi'], 3, ',', '').'</td>
                <td style="text-align:center; border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black;">Hours</td>
            </tr>';
    }
    
endforeach;

$konten .=
            '<tr>
                <td style="text-align:center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black;"></td>
                <td style="text-align:left; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black;"></td>
                <td style="border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black;"></td>
                <td style="text-align:center; border-top: 1px solid black; border-bottom: 1px solid black;"></td>
                <td style="text-align:right; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black;"></td>
                <td style="text-align:center; border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;"></td>
            </tr>';


$konten .=
        '</table>';

$pdf->writeHTML($konten, true, false, false, false, '');

endforeach;

}


$txtOutput = $title.'.pdf';
$pdf->Output($txtOutput, 'I');