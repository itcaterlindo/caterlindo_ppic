<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_versi_table('<?php echo $id; ?>');
	first_load_versi('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function first_load_versi(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function open_versi_table(id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/versi_table_main'; ?>',
			data: {'id' : id},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
				moveTo('idMainContent');
			}
		});
	} 

	function detail_data_versi (part_kd) {
		open_view_detail_box('detail', '', part_kd);
	}

	function box_overlayVersi(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

</script>