<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputMaster';
if ($sts == 'add'){
	$part_versi = 1;
}
if(isset($rowData)){
	extract($rowData);
	$id = $partmain_kd;
	$PartJenisSelected = $partjenis_kd;
}

if(isset($rowVersi)){
	$part_kd = $rowVersi->part_kd;
	$part_versi = $rowVersi->part_versi;
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtStsMaster', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpartmain_kd', 'name'=> 'txtpartmain_kd', 'value' => isset($id) ? $id: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpart_kd', 'name'=> 'txtpart_kd', 'value' => isset($part_kd) ? $part_kd: null ));

?>
<div class="row">

    <div class="form-group">
		<label for='idtxtpartjenis_kd' class="col-md-2 control-label">Jenis Part</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpartjenis_kd"></div>
			<?php echo form_dropdown('txtpartjenis_kd', $opsiPartJenis, isset($PartJenisSelected)? $PartJenisSelected : '', array('class'=> 'form-control', 'id'=> 'idtxtpartjenis_kd'));?>
		</div>
	</div>

    <div class="form-group">
		<label for='idtxtpartmain_nama' class="col-md-2 control-label">Nama Part</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpartmain_nama"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtpartmain_nama', 'id'=> 'idtxtpartmain_nama', 'placeholder' =>'Nama Part', 'value'=> isset($partmain_nama) ? $partmain_nama: null ));?>
		</div>
	</div>

	<div class="form-group">
			<label for="idtxtpartmain_note" class="col-md-2 control-label">Note</label>
			<div class="col-sm-9 col-xs-12">
				<div class="errInput" id="idErrpartmain_note"></div>
				<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtpartmain_note', 'id'=> 'idtxtpartmain_note', 'placeholder' =>'Note', 'rows' => '2', 'value'=> isset($partmain_note) ? $partmain_note: null ));?>
			</div>
		</div>

    <div class="form-group">
		<label for='idtxtpart_versi' class="col-md-2 control-label">Versi</label>
		<div class="col-sm-1 col-xs-12">
			<div class="errInput" id="idErrpart_versi"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtpart_versi', 'id'=> 'idtxtpart_versi', 'placeholder' =>'Versi', 'readonly' => 'true', 'value'=> isset($part_versi) ? $part_versi: null ));?>
		</div>
	</div>
</div>

<hr>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-1 col-xs-12 col-sm-offset-10">
			<button type="submit" name="btnSubmit" onclick="submitDataMaster()" class="btn btn-primary btn-sm">
				<i class="fa fa-arrow-circle-right"></i> Simpan
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>
