<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<table id="idtablemain" cellspacing="0" cellpadding="1" style="font-size: 80%;" border="1" width="100%">
	<thead>
	<tr>
        <th width="5%" style="text-align: center; font-weight:bold;">No.</th>
        <th width="15%" style="text-align: center; font-weight:bold;">Kode Material</th>
        <th colspan="2" width="55%" style="text-align: center; font-weight:bold;">Deskripsi</th>
        <th width="10%" style="text-align: center; font-weight:bold;">Jumlah</th>
        <th width="15%" style="text-align: center; font-weight:bold;">Satuan</th>
    </tr>
	</thead>
    <tbody>
    <?php 
    $no = 1;
    $grandTotalGR = 0;
    $grandTotalInput = 0;
    $rGroupPart = [];
    $no = 1;
    /** Group By Bagian */
    foreach ($resultPart as $rPart) {
        $rGroupPart[$rPart['bagian_kd'].'/'.$rPart['bagian_nama']][] = $rPart;
    }
    foreach ($rGroupPart as $rBagian => $resultPerBagian) :
        $expBagian = explode('/', $rBagian);
        $subTotalGR = 0;
        $subTotalInput = 0;
    ?>
        <tr>
            <td width="5%" style="text-align: center;"><?= $no ?></td>
            <td width="15%" style="font-weight: bold;"><?= $expBagian[1] ?></td>
            <td width="45%"></td>
            <td width="10%"></td>
            <td width="10%"></td>
            <td width="15%"></td>
        </tr>
    <?php
        foreach ($resultPerBagian as $r) {
            $rmharga_hargagr = null;
            $rmharga_hargainput = null;
            $totalGR = null;
            $totalInput = null;
            if ($r['rmkategori_kd'] != '01') {
                $rmharga_hargagr = $r['rmharga_hargagr'];
                $rmharga_hargainput = $r['rmharga_hargainput'];
                $totalGR = (float) $r['partdetail_qty'] * $rmharga_hargagr;
                $totalInput = (float) $r['partdetail_qty'] * $rmharga_hargainput;
            }
            $subTotalGR += (float) $totalGR;
            $subTotalInput += (float) $totalInput;
    ?>    
        <tr>
            <td></td>
            <td><?= $r['rm_kode'].'/'.$r['rm_oldkd'] ?></td>
            <td><?= $r['partdetail_deskripsi'].'/'.$r['partdetail_spesifikasi'] ?></td>
            <td></td>
            <td style="text-align: right;"><?= number_format($r['partdetail_qty'], 3 ,',', '') ?></td>
            <td style="text-align: center;"><?= $r['rmsatuan_nama'] ?></td>
        </tr>
    <?php
        /** Detail Plate */
        foreach ($resultPlate as $rPlate0) {
            if ($rPlate0['partdetail_kd'] == $r['partdetail_kd'] && $rPlate0['partdetailplate_jenis'] == 'cut_size') :
            $massaCutSize = generate_massa_plate($rPlate0['partdetailplate_panjang'], $rPlate0['partdetailplate_lebar'], $rPlate0['partdetailplate_tebal'], $rPlate0['partdetailplate_massajenis']) * $r['partdetail_qty'];
            $massaCutSize = round($massaCutSize, 3);
        ?>
            <tr>
                <td></td>
                <td style="font-style: italic;">&nbsp;<?= $rPlate0['rm_kode'].'/'.$rPlate0['rm_oldkd']?></td>
                <td><?= sprintf('%04s', (int) $rPlate0['partdetailplate_panjang']).' x '. sprintf('%04s', (int)$rPlate0['partdetailplate_lebar']).' = '.number_format($rPlate0['partdetailplate_qty'], 2 ,',', '').' (Cut Size)' ?></td>
                <td style="text-align: right; font-style: italic;"><?= number_format($massaCutSize, 3, ',', '')?></td>
                <td></td>
                <td style="text-align: center;">KG</td>
            </tr>
        <?php 
            endif;
        }
            foreach ($resultPlate as $rPlate) {
                if ($rPlate['partdetail_kd'] == $r['partdetail_kd'] && $rPlate['partdetailplate_jenis'] != 'cut_size' ) :
                    $massaPlate = (float) generate_massa_plate($rPlate['partdetailplate_panjang'], $rPlate['partdetailplate_lebar'], $rPlate['partdetailplate_tebal'], $rPlate['partdetailplate_massajenis']) * $rPlate['partdetailplate_qty'];
                    $massaMain = $massaPlate;
                    $massaAdditional = null;
                    if ($rPlate['partdetailplate_jenis'] == 'additional') {
                        $massaAdditional = $massaMain;
                        $massaMain = null;
                    }
                    ?>
                     <tr>
                        <td></td>
                        <td style="font-style: italic;">&nbsp;<?= $rPlate['rm_kode'].'/'.$rPlate['rm_oldkd']?></td>
                        <td><?= sprintf('%04s', (int) $rPlate['partdetailplate_panjang']).' x '. sprintf('%04s', (int)$rPlate['partdetailplate_lebar']).' = '.number_format($rPlate['partdetailplate_qty'], 2 ,',', '').' '.$rPlate['partdetailplate_keterangan'].' ('.$rPlate['partdetailplate_jenis'].')' ?></td>
                        <td style="text-align: right; font-style: italic;"><?= custom_numberformat($massaAdditional, 3, '') ?></td>
                        <td style="text-align: right;"><?= custom_numberformat($massaMain, 3, '') ?></td>
                        <td style="text-align: center;">KG</td>
                    </tr>
                    <?php
                endif;
            }
            /** Plate Waste */
            foreach ($resultPlateWaste as $rPlateWaste) {
                if ($rPlateWaste['partdetail_kd'] == $r['partdetail_kd']) :
                    ?>
                     <tr>
                        <td></td>
                        <td></td>
                        <td style="font-style: italic;">Waste</td>
                        <td></td>
                        <td style="text-align: right;"><?= number_format($rPlateWaste['waste'], 3, ',', '') ?></td>
                        <td style="text-align: center;">KG</td>
                    </tr>
                    <?php
                endif;
            }
            /** Plate Used */
            foreach ($resultPlateUsed as $rPlateUsed) {
                if ($rPlateUsed['partdetail_kd'] == $r['partdetail_kd']) :
                    $subTotalGR += (float) $rPlateUsed['pricePlateUsedGR'];
                    $subTotalInput += (float) $rPlateUsed['pricePlateUsedInput'];
                    ?>
                     <tr>
                        <td></td>
                        <td></td>
                        <td style="font-weight: bold; font-style: italic;">Used</td>
                        <td></td>
                        <td style="text-align: right;"><?= number_format($rPlateUsed['plateUsed'], 3, ',', '')  ?></td>
                        <td style="text-align: center;">KG</td>
                    </tr>
                    <?php
                endif;
            }
            /** Detail Pipe */ 
            $qTotalPipe = 0;
            foreach ($resultPipe as $rPipe) {
                if ($rPipe['partdetail_kd'] == $r['partdetail_kd']) :
                    $qTotalPipe = (float) $rPipe['partdetailpipe_panjang'] * $rPipe['partdetailpipe_qty'];
                    ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><?= custom_numberformat($rPipe['partdetailpipe_panjang'],2,'').' '.$rPipe['rmsatuan_nama'].' x '.custom_numberformat($rPipe['partdetailpipe_qty'],2,'').' ('.$rPipe['partdetailpipe_keterangan'].')'?></td>
                        <td style="text-align: right;"><?= custom_numberformat($qTotalPipe,3,'')?></td>
                        <td style="text-align: right;"></td>
                        <td style="text-align: center;"><?= $rPipe['rmsatuan_nama'] ?></td>
                    </tr>
                    <?php
                endif;
            }
            
        }
        ?>
        <?php
    $grandTotalGR += (float) $subTotalGR;
    $grandTotalInput += (float) $subTotalInput;
    $no++;
    endforeach; 
    /** Labourcost */ 
    if (!empty($resultLabourcost)) :?>
    <tr>
        <td width="5%" style="text-align: center;"><?= $no ?></td>
        <td width="15%" style="font-weight: bold;">LABOUR COST</td>
        <td width="45%"></td>
        <td width="10%"></td>
        <td width="10%"></td>
        <td width="15%"></td>
    </tr>    
    
    <?php
    endif;
    $subTotalLabourcost = 0;
    $subTotalLabourcostGR = 0;
    foreach ($resultLabourcost as $rLabourcost) :
        $labourcostPriceSatuanGR = $rLabourcost['labourcostPriceSatuan'];
        $labourcostPriceTotalGR = $rLabourcost['labourcostPriceTotal']
    ?>
        <tr>
            <td style=""></td>
            <td style=""> <?= $rLabourcost['bagian_kd'] ?></td>
            <td style=""><?= $rLabourcost['bagian_nama'].' ('.$rLabourcost['partlabourcost_qty'].' '.$rLabourcost['partlabourcost_satuan'].')' ?></td>
            <td style=""></td>
            <td style="text-align: right;"><?= number_format($rLabourcost['partlabourcost_durasi'], 3, ',', '') ?></td>
            <td style="text-align: center;">Hours</td>
        </tr>

    <?php 
        $subTotalLabourcostGR += $labourcostPriceTotalGR;
        $subTotalLabourcost += $rLabourcost['labourcostPriceTotal'];
    endforeach;
    $grandTotalGR += $subTotalLabourcostGR;
    $grandTotalInput += $subTotalLabourcost;
    ?>
    </tbody>
</table>