<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpartjenis_kd', 'name'=> 'txtpartjenis_kd', 'value' => isset($id) ? $id: null ));

?>
<div class="form-group">
	<label for="idtxtpartjenis_nama" class="col-md-2 control-label">Nama Jenis Part</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrpartjenis_nama"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtpartjenis_nama', 'placeholder' => 'Nama Jenis Part', 'id'=> 'idtxtpartjenis_nama', 'autofocus' => 'autofocus' ,'value'=> isset($partjenis_nama) ? $partjenis_nama: null ));?>
	</div>
</div>
<div class="form-group">
	<label for="idtxtpartjenis_generatewo" class="col-md-2 control-label">Generate WO</label>
	<div class="col-md-4">
		<div id="idErrpartjenis_generatewo"></div>
		<?php echo form_dropdown('txtpartjenis_generatewo', array('T' => 'True', 'F' => 'False'), isset($partjenis_generatewo) ? $partjenis_generatewo:'true' , array('id' => 'idtxtpartjenis_generatewo', 'class' => 'form-control')); ?>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-sm">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" onclick="submitData()" class="btn btn-primary btn-sm">
			<i class="fa fa-save"></i> Simpan
		</button>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	
</script>