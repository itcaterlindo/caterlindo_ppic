<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormOverhead';
extract($rowDataMasterPart);
if(isset($rowData)){
	extract($rowData);
	$overheadSelected = $overhead_kd;
}
?>

<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<div class="col-sm-6 col-xs-12">
				<div class="btn-group">	
					<button onclick="open_overhead()" class="btn btn-default btn-sm"> <i class="fa fa-refresh"></i> Refresh </button>
					<button onclick="open_labourcost()" class="btn btn-info btn-sm"> <i class="fa fa-arrow-circle-left"></i> Labourcost </button>
				</div>
			</div>
		</div>
	</div>
</div>
<hr>
<?php
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpart_kd', 'name'=> 'txtpart_kd', 'value' => isset($part_kd) ? $part_kd: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtStsOverhead', 'name'=> 'txtStsOverhead', 'class' => 'tt-input', 'value' => isset($stsOverhead) ? $stsOverhead : null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxttxtpartoverhead_kd', 'name'=> 'txtpartoverhead_kd', 'class' => 'tt-input', 'value' => isset($partoverhead_kd) ? $partoverhead_kd : null) );
?>
<div class="row">
	<div class="form-group">
		<label for='idtxtoverhead_kd' class="col-md-2 control-label">Overhead</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErroverhead_kd"></div>
			<?php echo form_dropdown('txtoverhead_kd', isset($opsiOverhead) ? $opsiOverhead : [] , isset($overheadSelected)? $overheadSelected : '', array('class'=> 'form-control', 'id'=> 'idtxtoverhead_kd'));?>
		</div>
		<?php if ($stsOverhead != 'edit') :?>
		<div class="col-sm-4 col-xs-12 checkbox">
			<label> <input type="checkbox" name="txtpilihSemua" value="1"> Pilih Semua </label>
		</div>
		<?php endif;?>
	</div>

	<div class="form-group">
		<label for='idtxtpartoverhead_qty' class="col-md-2 control-label">Jumlah (Kg)</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpartoverhead_qty"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control tt-input', 'name'=> 'txtpartoverhead_qty', 'id'=> 'idtxtpartoverhead_qty', 'placeholder' =>'Qty', 'value'=> isset($partoverhead_qty) ? $partoverhead_qty: null ));?>
		</div>	
	</div>

</div>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-3 pull-right">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitDataOverhead()" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button type="reset" name="btnReset" onclick="open_overhead()" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>
