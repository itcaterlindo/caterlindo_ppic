<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
    view_detailtablecosting_main ('<?= $part_kd ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#<?php echo $box_loader_id; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
	});

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

    function changeFontSize() {
        $("#idtablemain").css({
            'fontSize': '100%'
        });
    }

    function renderdt() {
        $("#idtablemain").DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"ordering": false,
			"searching": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excelHtml5",
				"title" : "Part Costing",
				"exportOptions": {
					"columns": [ 0,1,2,3,4,5,6,7,8,9 ],
					"format": {
						body: function ( data, row, column, node ) {
							if (column === 3 || column === 4 || column === 6 || column === 7 || column === 8 || column === 9) {
								if (data.includes('Rp')){
									data = data.toString().replace('Rp ','');
									data = data.toString().replace(' ,-','');
									data = data.toString().replaceAll('.','');
									data = data.toString().replace(',','.');
								}else{
									data = data.toString().replace(',','.');
								}
							}
							return data;
						}
					}
				}
			}],
		});
    }

    function view_detailtablecosting_main (part_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/view_detailtablecosting_main'; ?>',
			data: {'part_kd' : part_kd},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
                changeFontSize();
				view_detaillog_main(part_kd);
                renderdt();
				// moveTo('idMainContent');
			}
		});
    }

	function cetak_costing(part_kd)
	{
		window.open('<?php echo base_url().$class_link;?>/cetak_costing?part_kd='+part_kd);
	}

    // Untuk detail pdf
	function open_view_detail_box(part_kd){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/view_detail_box'; ?>',
			data: {'part_kd' : part_kd},
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function view_detaillog_main(part_kd){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link_partdata.'/view_detaillog_main'; ?>',
			data: {'part_kd' : part_kd},
			success: function(html) {
				$('#idlogmain').html(html);
			}
		});
	}

    // function ubah_state (part_kd, partstate_kd, specialAction) {
    //     $.ajax({
	// 		type: 'GET',
	// 		url: '<?php //echo base_url().$class_link.'/form_ubahstate_main'; ?>',
	// 		data: {part_kd : part_kd, partstate_kd: partstate_kd, specialAction: specialAction},
	// 		success: function(html) {
	// 			toggle_modal('Form Ubah State Part', html);
	// 		}
	// 	});
    // }

	// function submitState () {
	// 	event.preventDefault();
	// 	$('#idbtnSubmitState').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
	// 	$('#idbtnSubmitState').attr('disabled', true);

	// 	var form = document.getElementById('idFormUbahstate');

	// 	$.ajax({
	// 		url: "<?php //echo base_url().$class_link; ?>/action_ubahstate" ,
	// 		type: "POST",
	// 		data:  new FormData(form),
	// 		contentType: false,
	// 		cache: false,
	// 		processData:false,
	// 		success: function(data){
	// 			// console.log(data);
	// 			var resp = JSON.parse(data);
	// 			if(resp.code == 200){
	// 				window.location.reload();
	// 			}else if (resp.code == 401){
	// 				$.each( resp.pesan, function( key, value ) {
	// 					$('#'+key).html(value);
	// 				});
	// 				generateToken (resp.csrf);
	// 			}else if (resp.code == 400){
	// 				generateToken (resp.csrf);
	// 			}else{
	// 				generateToken (resp.csrf);
	// 			}
	// 		} 	        
	// 	});
	// }

    function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

    function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

</script>