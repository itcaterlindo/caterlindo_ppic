<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<!-- <table id="idtablemain" cellspacing="0" cellpadding="1" border="1" style="font-size:8.5px;" width="100%"> -->
<table id="idtablemain" cellspacing="0" cellpadding="1" border="1" width="100%">
	<thead>
	<tr>
        <th width="5%" style="text-align: center; font-weight:bold;">No.</th>
        <th width="10%" style="text-align: center; font-weight:bold;">Kode Material</th>
        <th colspan="2" width="33%" style="text-align: center; font-weight:bold;">Deskripsi</th>
        <th width="8%" style="text-align: center; font-weight:bold;">Jumlah</th>
        <th width="6%" style="text-align: center; font-weight:bold;">Satuan</th>
        <th width="8%" style="text-align: center; font-weight:bold;">Harga GR</th>
        <th width="10%" style="text-align: center; font-weight:bold;">Total GR</th>
        <th width="10%" style="text-align: center; font-weight:bold;">Harga Costing</th>
        <th width="10%" style="text-align: center; font-weight:bold;">Total Costing</th>
    </tr>
	</thead>
    <tbody>
    <?php 
    $no = 1;
    $grandTotalGR = 0;
    $grandTotalInput = 0;
    $rGroupPart = [];
    $no = 1;
    /** Group By Bagian */
    foreach ($resultPart as $rPart) {
        $rGroupPart[$rPart['bagian_kd'].'/'.$rPart['bagian_nama']][] = $rPart;
    }
    foreach ($rGroupPart as $rBagian => $resultPerBagian) :
        $expBagian = explode('/', $rBagian);
        $subTotalGR = 0;
        $subTotalInput = 0;
    ?>
        <tr>
            <td width="5%" style="text-align: center;"><?= $no ?></td>
            <td width="10%" style="font-weight: bold;"><?= $expBagian[1] ?></td>
            <td width="28%"></td>
            <td width="5%"></td>
            <td width="8%"></td>
            <td width="6%"></td>
            <td width="8%"></td>
            <td width="10%"></td>
            <td width="10%"></td>
            <td width="10%"></td>
        </tr>
    <?php
        foreach ($resultPerBagian as $r) {
            $rmharga_hargagr = null;
            $rmharga_hargainput = null;
            $totalGR = null;
            $totalInput = null;
            if ($r['rmkategori_kd'] != '01') {
                $rmharga_hargagr = $r['rmharga_hargagr'];
                $rmharga_hargainput = $r['partdetail_hargaunit'];
                $totalGR = $r['partdetail_qty'] * round($rmharga_hargagr,2);
                $totalInput = $r['partdetail_hargatotal'];;
            }
            $subTotalGR += round($totalGR, 2);
            $subTotalInput += round($totalInput, 2);

            /** Cek harga costing vs hargaGR */
            $color = '';
            if(empty($rmharga_hargainput)){
                $color = '';
            }else if ($rmharga_hargainput < $rmharga_hargagr) {
                $color = 'orange';
            }else if($rmharga_hargainput >= $rmharga_hargagr){
                $color = 'lightgreen';
            }
    ?>    
        <tr>
            <td></td>
            <td><?= $r['rm_kode'].'/'.$r['rm_oldkd'] ?></td>
            <td><?= $r['partdetail_deskripsi'].'/'.$r['partdetail_spesifikasi'] ?></td>
            <td></td>
            <td style="text-align: right;"><?= $r['partdetail_qty'] ?></td>
            <td style="text-align: center;"><?= $r['rmsatuan_nama'] ?></td>
            <td style="text-align: right;"><?= custom_numberformat($rmharga_hargagr, 2, '') ?></td>
            <td style="text-align: right;"><?= custom_numberformat($totalGR, 2, '') ?></td>
            <td style="text-align: right;" bgcolor="<?= $color?>"><?= custom_numberformat($rmharga_hargainput, 2, '') ?></td>
            <td style="text-align: right;"><?= custom_numberformat($totalInput, 2, '') ?></td>
        </tr>
    <?php
        /** Detail Plate */
        foreach ($resultPlate as $rPlate0) {
            if ($rPlate0['partdetail_kd'] == $r['partdetail_kd'] && $rPlate0['partdetailplate_jenis'] == 'cut_size') :
            $massaCutSize = generate_massa_plate($rPlate0['partdetailplate_panjang'], $rPlate0['partdetailplate_lebar'], $rPlate0['partdetailplate_tebal'], $rPlate0['partdetailplate_massajenis']) * $rPlate0['partdetailplate_qty'];
            $massaCutSize = round($massaCutSize, 3);
        ?>
            <tr>
                <td></td>
                <td style="font-style: italic;">&nbsp;<?= $rPlate0['rm_kode'].'/'.$rPlate0['rm_oldkd']?></td>
                <td><?= sprintf('%04s', (int) $rPlate0['partdetailplate_panjang']).' x '. sprintf('%04s', (int)$rPlate0['partdetailplate_lebar']).' = '.number_format($rPlate0['partdetailplate_qty'], 2 ,',', '').' (Cut Size)' ?></td>
                <td style="text-align: right; font-style: italic;"><?= custom_numberformat($massaCutSize, 3, '') ?></td>
                <td></td>
                <td style="text-align: center;">KG</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        <?php 
            endif;
        }
            foreach ($resultPlate as $rPlate) {
                if ($rPlate['partdetail_kd'] == $r['partdetail_kd'] && $rPlate['partdetailplate_jenis'] != 'cut_size' ) :
                    $massaPlate = (float) generate_massa_plate($rPlate['partdetailplate_panjang'], $rPlate['partdetailplate_lebar'], $rPlate['partdetailplate_tebal'], $rPlate['partdetailplate_massajenis']) * $rPlate['partdetailplate_qty'];
                    $massaMain = $massaPlate;
                    $massaAdditional = null;
                    if ($rPlate['partdetailplate_jenis'] == 'additional') {
                        $massaAdditional = $massaMain;
                        $massaMain = null;
                    }
                    ?>
                     <tr>
                        <td></td>
                        <td style="font-style: italic;">&nbsp;<?= $rPlate['rm_kode'].'/'.$rPlate['rm_oldkd']?></td>
                        <td><?= sprintf('%04s', (int) $rPlate['partdetailplate_panjang']).' x '. sprintf('%04s', (int)$rPlate['partdetailplate_lebar']).' = '.number_format($rPlate['partdetailplate_qty'], 2 ,',', '').' '.$rPlate['partdetailplate_keterangan'].' ('.$rPlate['partdetailplate_jenis'].')' ?></td>
                        <td style="text-align: right; font-style: italic;"><?= custom_numberformat($massaAdditional, 3, '') ?></td>
                        <td style="text-align: right;"><?= custom_numberformat($massaMain, 3, '') ?></td>
                        <td style="text-align: center;">KG</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php
                endif;
            }
            /** Plate Waste */
            foreach ($resultPlateWaste as $rPlateWaste) {
                if ($rPlateWaste['partdetail_kd'] == $r['partdetail_kd']) :
                    ?>
                     <tr>
                        <td></td>
                        <td></td>
                        <td style="font-style: italic;">Waste</td>
                        <td></td>
                        <td style="text-align: right;"><?= number_format($rPlateWaste['waste'], 3, ',', '') ?></td>
                        <td style="text-align: center;">KG</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php
                endif;
            }
            /** Plate Used */
            foreach ($resultPlateUsed as $rPlateUsed) {
                if ($rPlateUsed['partdetail_kd'] == $r['partdetail_kd']) :
                    $subTotalGR += round($rPlateUsed['pricePlateUsedGR'], 2);
                    $subTotalInput += round ($rPlateUsed['pricePlateUsedInput'], 2);
                    /** Cek harga costing vs hargaGR */
                    $colorPlate = '';
                    if(empty($rPlateUsed['hargaKonversiInput'])){
                        $colorPlate = '';
                    }else if ($rPlateUsed['hargaKonversiInput'] < $rPlateUsed['hargaKonversiGR']) {
                        $colorPlate = 'orange';
                    }else if($rPlateUsed['hargaKonversiInput'] >= $rPlateUsed['hargaKonversiGR']){
                        $colorPlate = 'lightgreen';
                    }
                    ?>
                     <tr>
                        <td></td>
                        <td></td>
                        <td style="font-weight: bold; font-style: italic;">Used </td>
                        <td></td>
                        <td style="text-align: right;"><?= custom_numberformat($rPlateUsed['plateUsed'], 3, '')  ?></td>
                        <td style="text-align: center;">KG</td>
                        <td style="text-align: right;"><?= custom_numberformat($rPlateUsed['hargaKonversiGR'], 2, '') ?></td>
                        <td style="text-align: right;"><?= custom_numberformat($rPlateUsed['pricePlateUsedGR'], 2, '') ?></td>
                        <td style="text-align: right;" bgcolor="<?= $colorPlate ?>"><?= custom_numberformat($rPlateUsed['hargaKonversiInput'], 2, '') ?></td>
                        <td style="text-align: right;"><?= custom_numberformat($rPlateUsed['pricePlateUsedInput'], 2, '') ?></td>
                    </tr>
                    <?php
                endif;
            }

            /** Detail Pipe */ 
            $qTotalPipe = 0;
            foreach ($resultPipe as $rPipe) {
                if ($rPipe['partdetail_kd'] == $r['partdetail_kd']) :
                    $qTotalPipe = (float) $rPipe['partdetailpipe_panjang'] * $rPipe['partdetailpipe_qty'];
                    ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><?= custom_numberformat($rPipe['partdetailpipe_panjang'],2,'').' '.$rPipe['rmsatuan_nama'].' x '.custom_numberformat($rPipe['partdetailpipe_qty'],2,'').' ('.$rPipe['partdetailpipe_keterangan'].')'?></td>
                        <td style="text-align: right;"><?= custom_numberformat($qTotalPipe,3,'')?></td>
                        <td style="text-align: right;"></td>
                        <td style="text-align: center;"><?= $rPipe['rmsatuan_nama'] ?></td>
                        <td style="text-align: right;"></td>
                        <td style="text-align: right;"></td>
                        <td style="text-align: right;"></td>
                        <td style="text-align: right;"></td>
                    </tr>
                    <?php
                endif;
            }

        }
        ?>
        <tr bgcolor="yellow">
            <td></td>
            <td></td>
            <td style="font-weight: bold; text-align: right;">Sub Total :</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td style="text-align: right;"><?= custom_numberformat($subTotalGR, 2, 'IDR') ?></td>
            <td></td>
            <td style="text-align: right;"><?= custom_numberformat($subTotalInput, 2, 'IDR') ?></td>
        </tr>
        <?php
    $grandTotalGR += round($subTotalGR, 2);
    $grandTotalInput += round($subTotalInput, 2);
    $no++;
    endforeach; 
    /** Labourcost */
    if (!empty($resultLabourcost)) :
    ?>
    <tr>
        <td width="5%" style="text-align: center;"><?= $no ?></td>
        <td width="10%" style="font-weight: bold;">LABOUR COST</td>
        <td width="28%"></td>
        <td width="5%"></td>
        <td width="8%"></td>
        <td width="6%"></td>
        <td width="8%"></td>
        <td width="10%"></td>
        <td width="10%"></td>
        <td width="10%"></td>
    </tr>    
    
    <?php
    endif;
    $subTotalLabourcost = 0;
    $subTotalLabourcostGR = 0;
    foreach ($resultLabourcost as $rLabourcost) :
        $labourcostPriceSatuanGR = $rLabourcost['labourcostPriceSatuan'];
        $labourcostPriceTotalGR = $rLabourcost['labourcostPriceTotal']
    ?>
        <tr>
            <td style=""></td>
            <td style=""> <?= $rLabourcost['bagian_kd'] ?></td>
            <td style=""><?= $rLabourcost['bagian_nama'].' ('.$rLabourcost['partlabourcost_qty'].' '.$rLabourcost['partlabourcost_satuan'].')' ?></td>
            <td style=""></td>
            <td style="text-align: right;"><?= number_format($rLabourcost['partlabourcost_durasi'], 3, ',', '') ?></td>
            <td style="text-align: center;">HOURS</td>
            <td style="text-align: right;"><?= custom_numberformat($labourcostPriceSatuanGR, 2, '')?></td>
            <td style="text-align: right;"><?= custom_numberformat($labourcostPriceTotalGR, 2, '')?></td>
            <td style="text-align: right;"><?= custom_numberformat($rLabourcost['labourcostPriceSatuan'], 2, '')?></td>
            <td style="text-align: right;"><?= custom_numberformat($rLabourcost['labourcostPriceTotal'], 2, '')?></td>
        </tr>

    <?php 
        $subTotalLabourcostGR += round($labourcostPriceTotalGR, 2);
        $subTotalLabourcost += round($rLabourcost['labourcostPriceTotal'], 2);
    endforeach;
    $grandTotalGR += round($subTotalLabourcostGR, 2);
    $grandTotalInput += round($subTotalLabourcost, 2);
    if (!empty($resultLabourcost)) :
    ?>
    <tr bgcolor="yellow">
        <td></td>
        <td></td>
        <td style="font-weight: bold; text-align: right;">Sub Total :</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right;"><?= custom_numberformat($subTotalLabourcostGR, 2, 'IDR') ?></td>
        <td></td>
        <td style="text-align: right;"><?= custom_numberformat($subTotalLabourcost, 2, 'IDR') ?></td>
    </tr>
    <?php endif;?>
    <!-- Overhead -->
    <tr>
        <td width="5%" style="text-align: center;"><?= $no ?></td>
        <td width="10%" style="font-weight: bold;">OPERATIONAL / OVERHEAD</td>
        <td width="28%"></td>
        <td width="5%"></td>
        <td width="8%"></td>
        <td width="6%"></td>
        <td width="8%"></td>
        <td width="10%"></td>
        <td width="10%"></td>
        <td width="10%"></td>
    </tr>
    <?php
    $subTotalOverheadGR = 0;
    $subTotalOverhead = 0;
    foreach ($resultOverhead as $rOverhead) :
        $overheadPriceTotal = $rOverhead['partoverhead_hargatotal'];
        $overheadPriceSatuanGR = $rOverhead['partoverhead_hargaunit'];
        $overheadPriceTotalGR = $overheadPriceTotal;
    ?>
        <tr>
            <td style=""></td>
            <td style=""> <?= $rOverhead['overhead_kd'] ?></td>
            <td style=""><?= $rOverhead['overhead_nama'] ?></td>
            <td style=""></td>
            <td style="text-align: right;"><?= number_format($rOverhead['partoverhead_qty'], 3, ',', '') ?></td>
            <td style="text-align: center;">KG</td>
            <td style="text-align: right;"><?= custom_numberformat($overheadPriceSatuanGR, 2, '')?></td>
            <td style="text-align: right;"><?= custom_numberformat($overheadPriceTotalGR, 2, '')?></td>
            <td style="text-align: right;"><?= custom_numberformat($rOverhead['partoverhead_hargaunit'], 2, '')?></td>
            <td style="text-align: right;"><?= custom_numberformat($overheadPriceTotal, 2, '')?></td>
        </tr>
    <?php
        $subTotalOverheadGR += round($overheadPriceTotalGR, 2);
        $subTotalOverhead += round($overheadPriceTotal, 2);
    endforeach;
    $grandTotalGR += $subTotalOverheadGR;
    $grandTotalInput += $subTotalOverhead;
    ?>
    <tr bgcolor="yellow">
        <td></td>
        <td></td>
        <td style="font-weight: bold; text-align: right;">Sub Total :</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right;"><?= custom_numberformat($subTotalOverheadGR, 2, 'IDR') ?></td>
        <td></td>
        <td style="text-align: right;"><?= custom_numberformat($subTotalOverhead, 2, 'IDR') ?></td>
    </tr>
    
    <tr>
        <td></td>
        <td></td>
        <td style="text-align: right; font-weight: bold;">Grand Total</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right; font-weight: bold;"><?= custom_numberformat($grandTotalGR, 2, 'IDR') ?></td>
        <td></td>
        <td style="text-align: right; font-weight: bold;"><?= custom_numberformat($grandTotalInput, 2, 'IDR') ?></td>
    </tr>
    </tbody>
</table>