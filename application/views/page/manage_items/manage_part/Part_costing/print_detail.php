<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	body {
		/* font-size: 12px; */
  		margin:2mm 0mm 0mm 5mm;
	}
	/* .table>tbody>tr>td {
		padding:1px;
	}
	.table>tfoot>tr>td {
		padding:1px;
	} */
	/* @media print {
		body {transform: scale(80%);}
		.print {
			page-break-after: always;
		}
		.print-foot {
			page-break-inside: avoid;
		}
		.div-footer {
			position: fixed;
			bottom: 0;
		}
		.print-left {
			text-align: left;
		}
		.print-center {
			text-align: center;
		}
		.print-right {
			text-align: right;
		}
	} */
    /* #pageFooter {
        display: table-footer-group;
    }

    #pageFooter::before {
        counter-increment: page;
        content: counter(page);
    } */
</style>
<div class="row no-print">
	<div class="col-xs-12">
		<a href="javascript:void(0);" class="btn btn-danger no-print" onclick="window.close();" style="margin-left: 25px;">
			<i class="fa fa-ban"></i> Close
		</a>
		<a href="javascript:void(0);" class="btn btn-primary no-print" onclick="window.print();" style="margin-left: 25px;">
			<i class="fa fa-print"></i> Cetak Data
		</a>
	</div>
</div>
<div class="row invoice print">
	<div id="idbox_loader" align="middle">
		<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
	</div>
	<!-- Header -->
	<table border="0" width="100%">
		<tbody>
				<tr>
					<td style="width: 40%;">
						<img src="<?= base_url() ?>/assets/admin_assets/dist/img/logo_cat.png" width="135" height="35"> 
					</td>
				<td style="width: 60%;">

					<table border="0" style="width: 100%;"> 
						<tbody>
							<tr>
								<td colspan="3" style="text-align: right; font-size: 32px; font-weight: bold;">BILL OF MATERIAL & COSTING</td>
							</tr>
							<tr>
								<td width="45%"></td>
								<td width="35%" style="font-size: 13px; text-align: left; border-bottom: 1px solid black;">No. Dokumen</td>
								<td width="35%" style="font-size: 13px; text-align: left; border-bottom: 1px solid black;">: CAT4-FAA-002</td>
							</tr>
							<tr>
								<td></td>
								<td style="font-size: 13px; text-align: left; border-bottom: 1px solid black;">Tanggal Terbit</td>
								<td style="font-size: 13px; text-align: left; border-bottom: 1px solid black;">: 22 Sep 2023</td>
							</tr>
							<tr>
								<td></td>
								<td style="font-size: 13px; text-align: left; border-bottom: 1px solid black;">Rev</td>
								<td style="font-size: 13px; text-align: left; border-bottom: 1px solid black;">: 03</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
	<!-- End Header -->
	<br>
	<!-- HEADER MASTER -->
	<table border="0" style="width: 100%;"> 
	<tr>
		<td>Nama Part</td>
		<td>: <?= $header['partmain_nama'] ?></td>
		<td> Tanggal Part</td>
		<td>: <?= format_date($header['part_tglinput'], 'd-m-Y H:i:s') ?></td>
	</tr>
	<tr>
		<td>Jenis Part</td>
		<td>: <?= $header['partjenis_nama'] ?></td>
		<td>Versi Part</td>
		<td>: <?= $header['part_versi'] ?></td>
	</tr>
	<tr>
		<td>Status Part</td>
		<td>: 
			<?php 
				if (empty($header['partmain_status'])){ 
					echo build_span('danger', 'Tidak Aktif'); 
				}else{
					echo build_span('success', 'Aktif'); 
				}  
			?>

		</td>
		<td> State Part</td>
		<td>: <?php echo build_span($header['partstate_label'], ucwords($header['partstate_nama'])) ?></td>
	</tr>
	</table>
	<!-- END HEADER MASTER -->
	<hr>
	<br>
	<div id='idContent'></div>
	<br>
	<hr>
	<!-- Footer -->
	<?php 
		$created=null;$createdtgl=null;$checked1=null;$checked1tgl=null;
		$checked2=null;$checked2tgl=null;$approved=null;$approvedtgl=null;
		foreach ($state as $stat) :
			switch ($stat['partstate_kd']) {
				case 2:
					/** Created by */
					$created = $stat['nm_admin'];
					$createdtgl = format_date($stat['partstatelog_tglinput'], 'd-m-Y H:i:s');
					break;
				case 3:
					/** Checked 1 */
					$checked1 = $stat['nm_admin'];
					$checked1tgl = format_date($stat['partstatelog_tglinput'], 'd-m-Y H:i:s');
					break;
				case 4:
					/** Checked 2 */
					$checked2 = $stat['nm_admin'];
					$checked2tgl = format_date($stat['partstatelog_tglinput'], 'd-m-Y H:i:s');
					break;
				case 5:
					/** Approved */
					$approved = $stat['nm_admin'];
					$approvedtgl = format_date($stat['partstatelog_tglinput'], 'd-m-Y H:i:s');
					break;
			}
		endforeach;
	?>
	<table border="1">
		<tr>
			<td style="width: 20%;">Created by </td>
			<td style="width: 30%;"> : <?= $created ?></td>
			<td style="width: 25%;"><?= $createdtgl ?></td>
		</tr> 
		<tr>
			<td style="width: 20%;">Checked1 By</td>
			<td style="width: 30%;"> : <?= $checked1 ?></td>
			<td style="width: 25%;"> <?= $checked1tgl ?></td>
		</tr>
		<tr>
			<td style="width: 20%;">Checked2 By</td>
			<td style="width: 30%;"> : <?= $checked2 ?></td>
			<td style="width: 25%;"> <?= $checked2tgl ?></td>
		</tr>
		<tr>
			<td style="width: 20%;">Approved By</td>
			<td style="width: 30%;"> : <?= $approved ?></td>
			<td style="width: 25%;"> <?= $approvedtgl ?></td>
		</tr>
	</table>
	<!-- End Footer -->
</div>	
<script src="<?php echo base_url(); ?>assets/admin_assets/plugins/jQuery-3.2.1/jQuery-3.2.1.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(e){
		open_table('<?php echo $url; ?>');
	});

	function open_table(url) {
		$('#idbox_loader').fadeIn();
			$.ajax({
				type: 'GET',
				url: url,
				success: function(html) {
					$('#idContent').html(html);
					$('#idbox_loader').fadeOut();
				}
			});
	}
</script>