<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	form_tabledetail_main('<?= $sts?>', '<?= $id?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function open_form(sts, id) {
		boxOverlayForm('in');
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_main'; ?>',
			data: {'sts': sts, 'id' : id},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
				boxOverlayForm('out');
			}
		});
	}

	function open_form_labourcostharga(sts, id){
		boxOverlayForm('in');
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_linkLabourcost.'/formharga_main'; ?>',
				data: {'sts': sts, 'id' : id},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					moveTo('idMainContent');
					boxOverlayForm('out');
				}
			});
	}

	function form_tabledetail_main(sts, id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_tabledetail_main'; ?>',
			data: {'sts': sts, 'id' : id},
			success: function(html) {
				$('#<?php echo $box_content_detail_id; ?>').html(html);
			}
		});
	}

	function open_tablecosting_labourcost(sts, id) {
		boxOverlayForm('in');
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_linkLabourcost.'/tablecosting_main'; ?>',
			data: {'sts': sts, 'id' : id},
			success: function(html) {
				$('#<?php echo $box_content_detail_id; ?>').html(html);
				boxOverlayForm('out');
			}
		});
	}

	function open_table_overhead(sts, id) {
		boxOverlayForm('in');
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_linkOverhead.'/table_main'; ?>',
			data: {'sts': sts, 'id' : id},
			success: function(html) {
				$('#<?php echo $box_content_detail_id; ?>').html(html);
				boxOverlayForm('out');
			}
		});
	}

	function open_detail_platecosting(partdetail_kd){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_linkDetailPlate.'/tablecosting_main'; ?>',
			data: {'partdetail_kd' : partdetail_kd},
			success: function(html) {
				toggle_modal('Detail Plate Costing', html);			
			}
		});
	}

	function open_form_overhead(sts, id, stsoverhead, partoverhead_kd){
		boxOverlayForm('in');
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_linkOverhead.'/form_main'; ?>',
			data: {'sts': sts, 'id' : id, 'stsoverhead': stsoverhead, 'partoverhead_kd': partoverhead_kd},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
				moveTo('idMainContent');
				boxOverlayForm('out');
			}
		});
	}

	function open_labourcost(){
		var id = $('#idboxpart_kd').val();
		var sts = 'edit';
		close_boxcontent();
		open_tablecosting_labourcost(sts, id);
	}

	function open_formbom () {
		var id = $('#idboxpart_kd').val();
		var sts = 'edit';
		close_boxcontent();
		form_tabledetail_main(sts, id);
	}

	function open_overhead () {
		var id = $('#idboxpart_kd').val();
		var sts = 'edit';
		open_form_overhead(sts, id, '', '');
		open_table_overhead(sts, id);
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function close_boxcontent() {
		event.preventDefault();
		$('#<?php echo $box_content_id; ?>').html('');
	}

	function close_formeditbox(partmain_kd) {
		event.preventDefault();
		var url = '<?= base_url()?>manage_items/manage_part/part_versi?partmain_kd='+partmain_kd;
		window.location.assign(url);
	}

	function edit_data_detailharga(id){
		open_form('edit', id);
		moveTo('idMainContent');
	}

	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
		boxOverlayForm('out');
	}

	function submitData(form_id) {
		boxOverlayForm('in');
		event.preventDefault();
		var form = document.getElementById(form_id);
	
		var url = "<?php echo base_url().$class_link; ?>/action_updateharga_partdetail";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					form_tabledetail_main('<?= $sts?>', '<?= $id?>');
					close_boxcontent();
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					boxOverlayForm('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
				boxOverlayForm('out');
			} 	        
		});
	}

	function submitDataLabourcostHarga (form_id) {
		boxOverlayForm('in');
		event.preventDefault();
		var form = document.getElementById(form_id);
		var url = "<?php echo base_url().$class_linkLabourcost; ?>/action_updateharga";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					close_boxcontent();
					open_tablecosting_labourcost('<?= $sts ?>', '<?= $id ?>');
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					boxOverlayForm('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function submitDataOverhead () {
		boxOverlayForm('in');
		event.preventDefault();
		var form = document.getElementById('idFormOverhead');
		// edit atau add
		var sts =  $('#idtxtStsOverhead').val();
		url = "<?php echo base_url().$class_linkOverhead; ?>/action_insert";
		if (sts == 'edit'){
			url = "<?php echo base_url().$class_linkOverhead; ?>/action_update";
		}

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					var id = $('#idtxtpart_kd').val();
					var sts = $('#idtxtSts').val();
					open_overhead(sts, id);
					$('.errInput').html('');
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					boxOverlayForm('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	// untuk update harga material
	function action_updateharga_rm (part_kd) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			boxOverlayForm('in');
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_updateharga_rm'; ?>',
				data: {'part_kd': part_kd},
				success: function(data) {
					var resp = JSON.parse(data);
					if (resp.code == 200) {
						notify (resp.status, resp.pesan, 'success');
						form_tabledetail_main('<?= $sts?>', '<?= $id?>');
					}else {
						notify (resp.status, resp.pesan, 'error');
					}
					boxOverlayForm('out');
				}
			});
		}
	}

	function boxOverlayForm(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

	function render_select2(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_datetimepicker(valClass){
		$('.'+valClass).datetimepicker({
			format: 'DD-MM-YYYY',
    	});
	}

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }


</script>