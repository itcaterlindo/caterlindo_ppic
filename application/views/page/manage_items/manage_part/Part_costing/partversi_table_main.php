<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
	td.dt-green {background-color: green; color:white; }
</style>

<table id="idTableVersi" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:4%; text-align:center;" class="all">Opsi</th>
		<th style="width:30%; text-align:center;">Nama Part</th>
		<th style="width:10%; text-align:center;">Versi</th>
		<th style="width:10%; text-align:center;">State</th>
		<th style="width:10%; text-align:center;">Tanggal Versi</th>
		<th style="width:10%; text-align:center;">User</th>
	</tr>
	</thead>
	<tbody>
	<?php 
	$no = 1;
	foreach ($resultData as $r) :
		$dtversi = '';
		if ($r->part_kd == $versi->part_kd){
			$dtversi = 'dt-green';
		}
	?>
	<tr>
		<td class="dt-center"><?php echo $no;?></td>
		<td><?php 
			$btns = array();
			$btns[] = get_btn(array('title' => 'Lihat Part', 'icon' => 'search', 'onclick' => 'detail_part(\''.$r->part_kd.'\')'));
			$btns[] = get_btn(array('title' => 'Duplikasi Part', 'icon' => 'clone', 'onclick' => 'action_duplicatepart(\''.$r->part_kd.'\')'));
			if ($r->partstate_nama == 'approved') {
				$btns[] = get_btn(array('title' => 'Set Default Part', 'icon' => 'check', 'onclick' => 'action_defaultpart(\''.$partmain_kd.'\' , \''.$r->part_kd.'\')'));
			}
			if ($r->partstate_nama == 'editing') {
				$btns[] = get_btn(array('title' => 'Edit Part', 'icon' => 'pencil', 'onclick' => 'open_form_box(\''.'edit'.'\',\''.url_encrypt($r->part_kd).'\')'));
				$btns[] = get_btn_divider();
				$btns[] = get_btn(array('title' => 'Hapus Part', 'icon' => 'trash', 'onclick' => 'action_deletepart(\''.$r->part_kd.'\')'));
			}
			$btn_group = group_btns($btns);
			echo $btn_group;
		?></td>
		<td><?php echo $r->partmain_nama;?></td>
		<td class="dt-center <?= $dtversi ?>"><?php echo $r->part_versi;?></td>
		<td class="dt-center"><?php echo build_span ($r->partstate_label, $r->partstate_nama);?></td>
		<td><?php echo format_date($r->part_tglinput, 'Y-m-d H:i:s');?></td>
		<td><?php echo $r->nm_admin;?></td>
	</tr>
	<?php 
	$no++;
	endforeach;?>
	</tbody>
</table>

<script type="text/javascript">
	render_dt('#idTableVersi');

	function render_dt(table_elem) {
		$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
			return {
				"iStart": oSettings._iDisplayStart,
				"iEnd": oSettings.fnDisplayEnd(),
				"iLength": oSettings._iDisplayLength,
				"iTotal": oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
				"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
			};
		};

		$(table_elem).DataTable({
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"columnDefs": [
				{"data": null, "searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
				{"searchable": false, "orderable": false, "targets": 1},
			],
			"order":[3, 'asc'],
			"rowCallback": function (row, data, iDisplayIndex) {
				var info = this.fnPagingInfo();	
				var page = info.iPage;
				var length = info.iLength;
				var index = page * length + (iDisplayIndex + 1);
				$('td:eq(0)', row).html(index);
			}
		});
	}
</script>