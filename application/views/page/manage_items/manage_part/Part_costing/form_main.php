<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputHarga';
if($sts == 'edit'){
	extract($rowData);
	$rm_deskripsi = $partdetail_deskripsi.'/'.$partdetail_spesifikasi;
}
?>

<?php 
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpartdetail_kd', 'placeholder' => 'idtxtpartdetail_kd', 'name'=> 'txtpartdetail_kd', 'value' => isset($partdetail_kd) ? $partdetail_kd: null ));

?>
<div class="row">
	<div class="col-md-12">
		
		<div class="form-group">
			<label for="idtxtbagian_kd" class="col-md-2 control-label">RM Kode / Deskripsi</label>
			<div class="col-sm-2 col-xs-12">
				<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtrm_kode', 'id'=> 'idtxtrm_kode', 'placeholder' =>'Kode', 'readonly' => 'readonly', 'value'=> isset($rm_kode) ? $rm_kode: null ));?>
			</div>	
			<div class="col-sm-7 col-xs-12">
				<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtrm_deskripsi', 'id'=> 'idtxtrm_deskripsi', 'placeholder' =>'Deskripsi', 'readonly' => 'readonly', 'value'=> isset($rm_deskripsi) ? $rm_deskripsi: null ));?>
			</div>	
		</div>
		<div class="form-group">
			<label for="idtxtbagian_kd" class="col-md-2 control-label">RM Qty / Harga Unit</label>
			<div class="col-sm-2 col-xs-12">
				<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtpartdetail_qty', 'id'=> 'idtxtpartdetail_qty', 'placeholder' =>'Qty', 'readonly' => 'readonly', 'value'=> isset($partdetail_qty) ? $partdetail_qty: null ));?>
			</div>	
			<div class="col-sm-2 col-xs-12">
				<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtrmsatuan_nama', 'id'=> 'idtxtrmsatuan_nama', 'placeholder' =>'Satuan', 'readonly' => 'readonly', 'value'=> isset($rmsatuan_nama) ? $rmsatuan_nama: null ));?>
			</div>	
			<div class="col-sm-3 col-xs-12">
				<div id="idErrpartdetail_hargaunit"></div>
				<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtpartdetail_hargaunit', 'id'=> 'idtxtpartdetail_hargaunit', 'placeholder' =>'Harga Unit' ,'value'=> isset($partdetail_hargaunit) ? $partdetail_hargaunit: null ));?>
			</div>	
		</div>

	</div>
</div>

<div class="row">
	<div class="col-md-12">

		<div class="form-group">
			<div class="col-sm-2 pull-right">
				<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData('<?= $form_id; ?>')" class="btn btn-primary btn-sm">
					<i class="fa fa-save"></i> Simpan
				</button>
				<button onclick="close_boxcontent()" class="btn btn-default btn-sm">
					<i class="fa fa-times"></i> Tutup
				</button>
			</div>
		</div>

	</div> 
</div> 
<?php echo form_close(); ?>
<hr>
