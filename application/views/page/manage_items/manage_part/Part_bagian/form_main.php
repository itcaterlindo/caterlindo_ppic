<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
// echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtbagian_kd', 'name'=> 'txtbagian_kd', 'value' => isset($id) ? $id: null ));

?>
<div class="form-group">
	<label for="idtxtbagian_kd" class="col-md-2 control-label">Kode Bagian</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrbagian_kd"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtbagian_kd', 'placeholder' => 'Kode Bagian', 'id'=> 'idtxtbagian_kd', 'value'=> isset($id) ? $id: null ));?>
	</div>
</div>
<div class="form-group">
	<label for="idtxtbagian_nama" class="col-md-2 control-label">Nama Bagian</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrbagian_nama"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtbagian_nama', 'placeholder' => 'Nama Bagian', 'id'=> 'idtxtbagian_nama', 'value'=> isset($bagian_nama) ? $bagian_nama: null ));?>
	</div>
</div>
<div class="form-group">
	<label for="idtxtbagian_lokasi" class="col-md-2 control-label">Lokasi</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrbagian_lokasi"></div>
		<?php echo form_dropdown('txtbagian_lokasi', $opsiBagianLokasi, isset($bagian_lokasi)? $bagian_lokasi : '', array('class'=> 'form-control select2', 'id'=> 'idtxtbagian_lokasi'));?>
	</div>
</div>
<div class="form-group">
	<label for="idtxtbagian_nama" class="col-md-2 control-label">Nama PIC</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrpic"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtpic', 'placeholder' => 'Nama PIC', 'id'=> 'idtxtpic', 'value'=> isset($pic) ? $pic: null ));?>
	</div>
</div>

<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-sm">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" onclick="submitData()" class="btn btn-primary btn-sm">
			<i class="fa fa-save"></i> Simpan
		</button>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	
</script>