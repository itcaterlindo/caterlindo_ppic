<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputPlate';
$js_file = 'form_js';
$data['sts'] = $sts;
$data['class_link'] = $class_link;
$data['partdetail_kd'] = $partdetail_kd;

$rm_platetebal = $rowPartDetail['rm_platetebal'];
$rm_platemassajenis = $rowPartDetail['rm_platemassajenis'];
$deskripsiPart = $rowPartDetail['partmain_nama'];
$deskripsiPartDetail = $rowPartDetail['rm_kode'].'/'.$rowPartDetail['rm_deskripsi'].'/'.$rowPartDetail['rm_spesifikasi'];

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtplatedetailsts', 'placeholder' => 'idtxtplatests','value' => isset($sts) ? $sts: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtplatepartdetail_kd', 'placeholder' => 'idtxtplatepartdetail_kd', 'name'=> 'txtpartdetail_kd', 'value' => isset($rowPartDetail['partdetail_kd']) ? $rowPartDetail['partdetail_kd']: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtplatepart_kd', 'placeholder' => 'idtxtplatepart_kd', 'name'=> 'txtplatepart_kd', 'value' => isset($rowPartDetail['part_kd']) ? $rowPartDetail['part_kd']: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtplaterm_kd', 'placeholder' => 'idtxtplaterm_kd', 'name'=> 'txtplaterm_kd', 'value' => isset($rowPartDetail['rm_kd']) ? $rowPartDetail['rm_kd']: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpartdetailplate_kd', 'placeholder' => 'idtxtpartdetailplate_kd', 'name'=> 'txtpartdetailplate_kd', 'class' => 'tt-inputPlate'));
?>

<div class="col-md-12">
	<div class="row invoice-info">
		<div class="col-sm-12 invoice-col">
			<b>Nama Part :</b> <?php echo !empty($deskripsiPart) ? $deskripsiPart : '-'; ?><br>
			<b>Nama Material:</b> <?php echo !empty($deskripsiPartDetail) ? $deskripsiPartDetail : '-'; ?>
		</div>
	</div>
	<hr>
</div>

<div class="row">
	
	<div class="form-group">
		<label for="idtxtpartdetailplate_jenis" class="col-md-2 control-label">Jenis</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpartdetailplate_jenis"></div>
			<?php echo form_dropdown('txtpartdetailplate_jenis', $opsiJenis, isset($bagianSelected)? $bagianSelected : '', array('class'=> 'form-control', 'id'=> 'idtxtpartdetailplate_jenis'));?>
		</div>	
	</div>

	<div class="form-group">
		<label for="idtxtpartdetailplate_panjang" class="col-md-2 control-label">Panjang (mm)</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpartdetailplate_panjang"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control tt-inputPlate', 'name'=> 'txtpartdetailplate_panjang', 'id'=> 'idtxtpartdetailplate_panjang', 'placeholder' =>'p', 'value'=> isset($partdetailplate_panjang) ? $partdetailplate_panjang: null ));?>
		</div>
		<label for="idtxtpartdetailplate_lebar" class="col-md-1 control-label">Lebar (mm)</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpartdetailplate_lebar"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control tt-inputPlate', 'name'=> 'txtpartdetailplate_lebar', 'id'=> 'idtxtpartdetailplate_lebar', 'placeholder' =>'l', 'value'=> isset($partdetailplate_lebar) ? $partdetailplate_lebar: null ));?>
		</div>	
		<label for="idtxtpartdetailplate_qty" class="col-md-1 control-label">Qty</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpartdetailplate_qty"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control tt-inputPlate', 'name'=> 'txtpartdetailplate_qty', 'id'=> 'idtxtpartdetailplate_qty', 'placeholder' =>'Qty', 'value'=> isset($partdetailplate_qty) ? $partdetailplate_qty: null ));?>
		</div>	
	</div>

	<div class="form-group">
		<label for="idtxtrm_platetebal2" class="col-md-2 control-label">Tebal (mm)</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrrm_platetebal"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtrm_platetebal', 'id'=> 'idtxtrm_platetebal2', 'placeholder' =>'Tebal', 'readonly' => 'readonly', 'value'=> isset($rm_platetebal) ? $rm_platetebal: null ));?>
		</div>
		<label for="idtxtrm_platemassajenis2" class="col-md-1 control-label">Massa Jenis</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrrm_platemassajenis"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtrm_platemassajenis', 'id'=> 'idtxtrm_platemassajenis2', 'placeholder' =>'Massa jenis', 'readonly' => 'readonly', 'value'=> isset($rm_platemassajenis) ? $rm_platemassajenis: null ));?>
		</div>	
	</div>

	<div class="form-group">
		<label for="idtxtpartdetailplate_keterangan" class="col-md-2 control-label">Keterangan</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrpartdetailplate_keterangan"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control tt-inputPlate', 'name'=> 'txtpartdetailplate_keterangan', 'id'=> 'idtxtpartdetailplate_keterangan', 'placeholder' =>'Keterangan', 'rows' => '2', 'value'=> isset($partdetailplate_keterangan) ? $partdetailplate_keterangan: null ));?>
		</div>
	</div>

	<br>
</div>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-3 pull-right">
			<button type="reset" name="btnReset" onclick="resetFormDetailPlate()" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
			<button type="submit" name="btnSubmitPlate" id="idbtnSubmitPlate" onclick="submitDataPlate()" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>

<div class="row">
	<div class="col-md-12">
		<div id="idTableDetailPlate"></div>
	</div>
</div>

<?php $this->load->view('page/'.$class_link.'/'.$js_file, $data); ?>