<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputPipe';
$js_file = 'form_js';
$data['class_link'] = $class_link;
$rm_deskripsi = $rowRM['rm_kode'].' | '.$rowRM['rm_deskripsi'].'/'.$rowRM['rm_spesifikasi'];
$data['partdetail_kd'] = $partdetail_kd;

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpipedetailsts', 'placeholder' => 'idtxtpipedetailsts','value' => isset($sts) ? $sts: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpipepartdetail_kd', 'placeholder' => 'idtxtpipepartdetail_kd', 'name'=> 'txtpartdetail_kd', 'value' => isset($partdetail_kd) ? $partdetail_kd: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpipepart_kd', 'placeholder' => 'idtxtpipepart_kd', 'name'=> 'txtpipepart_kd', 'value' => isset($part_kd) ? $part_kd : null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpartdetailpipe_kd', 'placeholder' => 'idtxtpartdetailpipe_kd', 'name'=> 'txtpartdetailpipe_kd', 'class' => 'tt-inputPipe'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpiperm_kd', 'placeholder' => 'idtxtpiperm_kd', 'name'=> 'txtpiperm_kd', 'value' => isset($rowRM['rm_kd']) ? $rowRM['rm_kd']: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpipebagian_kd', 'placeholder' => 'idtxtpipebagian_kd', 'name'=> 'txtpipebagian_kd', 'value' => isset($bagian_kd) ? $bagian_kd: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpipejenismaterial', 'placeholder' => 'idtxtpipejenismaterial', 'name'=> 'txtpipejenismaterial', 'value' => isset($jenismaterial) ? $jenismaterial: null ));
?>

<div class="col-md-12">
	<div class="row invoice-info">
		<div class="col-sm-12 invoice-col">
			<b>Nama Part :</b> <?php echo !empty($rowPart['partmain_nama']) ? $rowPart['partmain_nama'] : '-'; ?><br>
			<b>Nama Material:</b> <?php echo !empty($rm_deskripsi) ? $rm_deskripsi : '-'; ?>
		</div>
	</div>
	<hr>
</div>

<div class="row">
	
	<div class="form-group">
		<label for="idtxtpartdetailpipe_panjang" class="col-md-2 control-label">Panjang</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpartdetailpipe_panjang"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control tt-inputPipe', 'name'=> 'txtpartdetailpipe_panjang', 'id'=> 'idtxtpartdetailpipe_panjang', 'placeholder' =>'P', 'value'=> isset($partdetailpipe_panjang) ? $partdetailpipe_panjang: null ));?>
		</div>
		<div class="col-sm-1 col-xs-12">
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'id'=> 'idtxtpipermsatuan_nama', 'disabled' =>'disabled', 'value'=> isset($rowRM['rmsatuan_nama']) ? $rowRM['rmsatuan_nama']: null ));?>
		</div>	
		<label for="idtxtpartdetailpipe_qty" class="col-md-1 control-label">Qty</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpartdetailpipe_qty"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control tt-inputPipe', 'name'=> 'txtpartdetailpipe_qty', 'id'=> 'idtxtpartdetailpipe_qty', 'placeholder' =>'Qty', 'value'=> isset($partdetailpipe_qty) ? $partdetailpipe_qty: null ));?>
		</div>	
	</div>

	<div class="form-group">
		<label for="idtxtpartdetailpipe_keterangan" class="col-md-2 control-label">Keterangan</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrpartdetailpipe_keterangan"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control tt-inputPipe', 'name'=> 'txtpartdetailpipe_keterangan', 'id'=> 'idtxtpartdetailpipe_keterangan', 'placeholder' =>'Keterangan', 'rows' => '2', 'value'=> isset($partdetailpipe_keterangan) ? $partdetailpipe_keterangan: null ));?>
		</div>
	</div>

</div>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-3 pull-right">
			<button type="submit" name="btnSubmitPipe" id="idbtnSubmitPipe" onclick="submitDataPipe('<?= $form_id ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button type="reset" name="btnReset" onclick="resetFormDetailPipe()" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>

<div class="row">
	<div class="col-md-12">
		<div id="idTableDetailPipe"></div>
	</div>
</div>

<?php $this->load->view('page/'.$class_link.'/'.$js_file, $data); ?>