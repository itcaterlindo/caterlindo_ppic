<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
    open_table_pipe_main ('<?= $partdetail_kd?>');

    function open_table_pipe_main (partdetail_kd){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/table_main'; ?>',
			data: {'partdetail_kd': partdetail_kd},
			success: function(html) {
				$('#idTableDetailPipe').html(html);
			}
		});
	}

	function edit_itempipe(partdetailpipe_kd){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/get_partDetailPipe'; ?>',
			data: {'partdetailpipe_kd' : partdetailpipe_kd},
			success: function(data) {
				var resp = JSON.parse(data);
				$('#idtxtpipedetailsts').val('edit');
				$('#idtxtpipepart_kd').val(resp.data.part_kd);
				$('#idtxtpartdetailpipe_kd').val(resp.data.partdetailpipe_kd);
				$('#idtxtpartdetailpipe_panjang').val(resp.data.partdetailpipe_panjang);
				$('#idtxtpartdetailpipe_qty').val(resp.data.partdetailpipe_qty);
				$('#idtxtpartdetailpipe_keterangan').val(resp.data.partdetailpipe_keterangan);
			}
		});
	}

	function resetLodingButtonSubmitPLate () {
		$('#idbtnSubmitPipe').html('<i class="fa fa-save"></i> Simpan');
		$('#idbtnSubmitPipe').attr('disabled', false);
	}

	function resetFormDetailPipe () {
		$('.tt-inputPipe').val('');
		$('#idtxtpipedetailsts').val('add');
	}

    function submitDataPipe (form_id) {
		event.preventDefault();
		var form = document.getElementById(form_id);
		/** Loading Button */
		$('#idbtnSubmitPipe').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtnSubmitPipe').attr('disabled', true);
		
		/** edit atau add */ 

		var sts =  $('#idtxtpipedetailsts').val();
		url = "<?php echo base_url().$class_link; ?>/action_insert";
		if (sts == 'edit'){
			url = "<?php echo base_url().$class_link; ?>/action_update";
		}

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				console.log(resp);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					let partdetail_kd = $('#idtxtpipepartdetail_kd').val();
					if (partdetail_kd === '') {
						$('#idtxtpipepartdetail_kd').val(resp.data.partdetail_kd);
						// belum ada partdetail_kd
						open_table_pipe_main (resp.data.partdetail_kd);
					}else{
						open_table_pipe_main (partdetail_kd);
					}
					let part_kd = $('#idtxtpart_kd').val();
					form_tabledetail_main('edit', part_kd);
					resetFormDetailPipe();
					generateToken (resp.csrf);
					resetLodingButtonSubmitPLate();
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					resetLodingButtonSubmitPLate();
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					resetLodingButtonSubmitPLate();
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					resetLodingButtonSubmitPLate();
					generateToken (resp.csrf);
				}
			}
		});
	}

	function delete_itempipe (id) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					console.log(resp);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						open_table_pipe_main ('<?= $partdetail_kd?>');
						let part_kd = $('#idtxtpart_kd').val();
						form_tabledetail_main('edit', part_kd);
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
					}
				}
			});
		}
	}

</script>