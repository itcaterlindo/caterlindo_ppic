<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<div class="col-md-12">
	<table id="idTablePipe" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
			<th style="width:4%; text-align:center;" class="all">Opsi</th>
			<th style="width:7%; text-align:center;">RM Kode</th>
			<th style="width:10%; text-align:center;">Panjang</th>
			<th style="width:10%; text-align:center;">Qty</th>
			<th style="width:15%; text-align:center;">Qty Total</th>
			<th style="width:15%; text-align:center;">Keterangan</th>
			<th style="width:15%; text-align:center;">Last Update</th>
		</tr>
		</thead>
		<tbody>
			<?php
			$no = 1;
			$sumTotal = 0;
			foreach ($resultDetailPlate as $each) : 
				$qTotal = (float) $each['partdetailpipe_panjang'] * $each['partdetailpipe_qty'];
			?>
			<tr>
				<td><?= $no; ?></th>
				<td><?php
					$btns = [];
					$btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_itempipe(\''.$each['partdetailpipe_kd'].'\')'));
					$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'delete_itempipe(\''.$each['partdetailpipe_kd'].'\')'));
					$btn_group = group_btns($btns);
					echo $btn_group;
				?></th>
				<td><?= $each['rm_kode']?></th>
				<td style="text-align:right;"><?= $each['partdetailpipe_panjang']?></th>
				<td style="text-align:right;"><?= $each['partdetailpipe_qty']?></th>
				<td style="text-align:right;"><?= $qTotal ?></th>
				<td><?= $each['partdetailpipe_keterangan']?></th>
				<td><?= $each['partdetailpipe_tgledit']?></th>
			</tr>
			
			<?php 
			$sumTotal += $qTotal;
			$no++;
			endforeach;?>
		</tbody>
		<tfoot>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td style="text-align:right; font-weight: bold;">Jumlah :</td>
				<td style="text-align:right; font-weight: bold;"><?= $sumTotal?></td>
				<td></td>
				<td></td>
			</tr>
		</tfoot>
	</table>
</div>

<script type="text/javascript">
</script>