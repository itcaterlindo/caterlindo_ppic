<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<div class="col-md-12">
    <button onclick="action_updateharga_detailplate('<?= $partdetail_kd; ?>')" class="btn btn-warning btn-sm"> <i class="fa fa-refresh"></i> Update Harga </button>
</div>

<div class="col-md-12">
	<table id="idTablePlateCosting" class="table table-bordered table-striped table-hover display responsive" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
			<th style="width:4%; text-align:center;" class="all">Opsi</th>
			<th style="width:4%; text-align:center;">Jenis</th>
			<th style="width:15%; text-align:center;">Dimensi</th>
			<th style="width:80%; text-align:center;">Keterangan</th>
			<th style="width:80%; text-align:center;">Qty</th>
			<th style="width:80%; text-align:center;">Massa (kg)</th>
			<th style="width:80%; text-align:center;">Harga Unit</th>
			<th style="width:80%; text-align:center;">Harga Total</th>
			<th style="width:80%; text-align:center;">Last Update</th>
		</tr>
		</thead>
	</table>
</div>

<script type="text/javascript">
	$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};
	var table = $('#idTablePlateCosting').DataTable({
		"processing": true,
		"serverSide": true,
		"ordering" : true,
		"paging" : false,
		"ajax": "<?php echo base_url().$class_link.'/tablecosting_data?partdetail_kd='.$partdetail_kd; ?>",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"columnDefs": [
			{"data": null, "searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
			{"searchable": false, "orderable": false, "targets": 1},
			{"className": "dt-left", "targets": 2},
			{"className": "dt-right", "targets": 5},
			{"className": "dt-right", "targets": 6},
			{"visible": false, "targets": 10},
		],
		"order":[10, 'desc'],
		"rowCallback": function (row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			var index = page * length + (iDisplayIndex + 1);
			$('td:eq(0)', row).html(index);
		}
    });
    
    function action_updateharga_detailplate (partdetail_kd){
        var conf = confirm('Apakah anda yakin ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_updateharga'; ?>',
				data: 'partdetail_kd='+partdetail_kd,
				success: function(data) {
                    console.log(data);
					var resp = JSON.parse(data);
					if(resp.code == 200){
                        notify (resp.status, resp.pesan, 'success');
                        open_detail_platecosting(partdetail_kd);
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				}
			});
		}
    }

    // function edit_data_labourcostharga(id) {
    //     open_form_labourcostharga('edit', id);
    // }
</script>