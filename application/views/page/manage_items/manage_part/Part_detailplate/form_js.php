<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">

    open_table_plate_main ('<?php echo $sts; ?>', '<?php echo $partdetail_kd; ?>');

    function open_table_plate_main (sts, partdetail_kd){
		$('#idTableDetailPlate').slideUp();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/table_main'; ?>',
			data: {'sts': sts,'partdetail_kd': partdetail_kd},
			success: function(html) {
				$('#idTableDetailPlate').slideDown().html(html);
			}
		});
	}

	function edit_data_detailplate(partdetailplate_kd){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/get_partDetailPlate'; ?>',
			data: {'partdetailplate_kd' : partdetailplate_kd},
			success: function(data) {
				var resp = JSON.parse(data);
				console.log(resp);
				$('#idtxtplatedetailsts').val('edit');
				$('#idtxtpartdetailplate_kd').val(resp.data.partdetailplate_kd);
				$('#idtxtpartdetailplate_jenis').val(resp.data.partdetailplate_jenis).trigger('change');
				$('#idtxtpartdetailplate_panjang').val(resp.data.partdetailplate_panjang);
				$('#idtxtpartdetailplate_lebar').val(resp.data.partdetailplate_lebar);
				$('#idtxtpartdetailplate_qty').val(resp.data.partdetailplate_qty);
				$('#idtxtpartdetailplate_keterangan').val(resp.data.partdetailplate_keterangan);
				$('#idtxtrm_platetebal2').val(resp.data.rm_platetebal);
				$('#idtxtrm_platemassajenis2').val(resp.data.rm_platemassajenis);
			}
		});
	}

	$('#idtxtpartdetailplate_jenis').change(function () {
		let plateSts = $('#idtxtplatedetailsts').val();
		cutsize_filter('<?= $partdetail_kd ?>', plateSts);
	});

	function cutsize_filter (partdetail_kd, sts) {
		var jenis = $('#idtxtpartdetailplate_jenis').val();
		if (jenis == 'cut_size') {
			$('.tt-inputPlate').prop('readonly', true);
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/get_cutsizeplate'; ?>',
				data: {'partdetail_kd' : partdetail_kd},
				success: function(data) {
					var resp = JSON.parse(data);
					if (sts == 'add') {
						$('#idtxtpartdetailplate_panjang').val(resp.data.rm_platepanjang);
						$('#idtxtpartdetailplate_qty').val(resp.data.partdetail_qty);
					}
					$('#idtxtpartdetailplate_lebar').val(resp.data.rm_platelebar);
					$('#idtxtpartdetailplate_keterangan').val('Cut Size');
				}
			});
		}else{
			$('.tt-inputPlate').prop('readonly', false);
		}
		$('.tt-inputPlate').val('');
		$('#idtxtpartdetailplate_panjang').val('');
		$('#idtxtpartdetailplate_qty').val('');
		resetLodingButtonSubmitPLate();
	}

	function resetFormDetailPlate(){
		event.preventDefault();
		$('.tt-inputPlate').val('');
		$('#idtxtpartdetailplate_panjang').val('');
		$('#idtxtpartdetailplate_qty').val('');
		$('#idtxtplatedetailsts').val('add');
		resetLodingButtonSubmitPLate ();
	}

	function resetLodingButtonSubmitPLate () {
		$('#idbtnSubmitPlate').html('<i class="fa fa-save"></i> Simpan');
		$('#idbtnSubmitPlate').attr('disabled', false);
	}

    function submitDataPlate () {
		event.preventDefault();
		var form = document.getElementById('idFormInputPlate');
		/** Loading Button */
		$('#idbtnSubmitPlate').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtnSubmitPlate').attr('disabled', true);
		
		/** edit atau add */ 

		var sts =  $('#idtxtplatedetailsts').val();
		url = "<?php echo base_url().$class_link; ?>/action_insert";
		if (sts == 'edit'){
			url = "<?php echo base_url().$class_link; ?>/action_update";
		}

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					var partdetail_kd = $('#idtxtplatepartdetail_kd').val();
					open_table_plate_main ('add', partdetail_kd);
					$('.errInput').html('');
					resetFormDetailPlate();
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					resetLodingButtonSubmitPLate();
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					resetLodingButtonSubmitPLate();
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					resetLodingButtonSubmitPLate();
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function hapus_data_detailplate (id) {
		var conf = confirm('Apakah anda yakin menghapus item ini ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						var partdetail_kd = $('#idtxtplatepartdetail_kd').val();
						open_table_plate_main ('add', partdetail_kd);
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				}
			});
		}
	}

</script>