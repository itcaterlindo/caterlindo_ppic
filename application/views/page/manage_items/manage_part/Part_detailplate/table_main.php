<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<div class="col-md-12">
	<table id="idTablePlate" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
			<th style="width:4%; text-align:center;" class="all">Opsi</th>
			<th style="width:4%; text-align:center;">Jenis</th>
			<th style="width:15%; text-align:center;">Dimensi</th>
			<th style="width:15%; text-align:center;">Qty</th>
			<th style="width:80%; text-align:center;">Keterangan</th>
			<th style="width:15%; text-align:center;">Tebal</th>
			<th style="width:15%; text-align:center;">Massa Jenis</th>
			<th style="width:80%; text-align:center;">Qty</th>
			<th style="width:80%; text-align:center;">Massa</th>
			<th style="width:80%; text-align:center;">Tgl Input</th>
		</tr>
		</thead>
	</table>
</div>

<script type="text/javascript">
	$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};
	var table = $('#idTablePlate').DataTable({
		"processing": true,
		"serverSide": true,
		"ordering" : true,
		"paging" : false,
		"ajax": "<?php echo base_url().$class_link.'/table_data?partdetail_kd='.$partdetail_kd; ?>",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"columnDefs": [
			{"data": null, "searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
			{"searchable": false, "orderable": false, "targets": 1},
			{"className": "dt-left", "targets": 2},
			{"className": "dt-right", "targets": 4},
			{"className": "dt-right", "targets": 6},
			{"className": "dt-right", "targets": 7},
			{"className": "dt-right", "targets": 8},
			{"visible": false, "targets": 9},
		],
		"order":[9, 'desc'],
		"rowCallback": function (row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			var index = page * length + (iDisplayIndex + 1);
			$('td:eq(0)', row).html(index);
		}
	});
</script>