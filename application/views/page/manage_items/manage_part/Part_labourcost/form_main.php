<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormLabourcost';
if($sts == 'edit'){
	extract($rowData);
}
?>

<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<div class="btn-group">	
				<button onclick="open_labourcost()" class="btn btn-default btn-sm"> <i class="fa fa-refresh"></i> Refresh </button>
				<button onclick="open_formbom()" class="btn btn-info btn-sm"> <i class="fa fa-arrow-circle-left"></i> Raw Material </button>
			</div>
		</div>
	</div>
</div>
<hr>
<?php 

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpart_kd', 'name'=> 'txtpart_kd', 'value' => isset($part_kd) ? $part_kd: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtStsLabourcost', 'name'=> 'txtStsLabourcost', 'class' => 'tt-input'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxttxtpartlaburcost_kd', 'name'=> 'txtpartlaburcost_kd', 'class' => 'tt-input'));
?>
<div class="row">
	<div class="form-group">
		<label for='idtxtbagian_kd' class="col-md-2 control-label">Bagian</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrbagian_kd"></div>
			<?php echo form_dropdown('txtbagian_kd', $opsiBagian, isset($bagianSelected)? $bagianSelected : '', array('class'=> 'form-control', 'id'=> 'idtxtbagian_kd'));?>
		</div>	
	</div>

	<div class="form-group">
		<label for='idtxtpartlabourcost_qty' class="col-md-2 control-label">Jumlah Pekerja</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpartlabourcost_qty"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control tt-input', 'name'=> 'txtpartlabourcost_qty', 'id'=> 'idtxtpartlabourcost_qty', 'placeholder' =>'Qty', 'value'=> isset($partlabourcost_qty) ? $partlabourcost_qty: null ));?>
		</div>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrlabourcost_satuan"></div>
			<?php echo form_dropdown('txtlabourcost_satuan', $opsiSatuan, isset($satuanSelected)? $satuanSelected : '', array('class'=> 'form-control', 'id'=> 'idtxtlabourcost_satuan'));?>
		</div>		
	</div>

	<div class="form-group">
		<label for='idtxtpartlabourcost_durasi' class="col-md-2 control-label">Durasi (Jam)</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpartlabourcost_durasi"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control tt-input', 'name'=> 'txtpartlabourcost_durasi', 'id'=> 'idtxtpartlabourcost_durasi', 'placeholder' =>'Durasi (Jam)', 'value'=> isset($partlabourcost_durasi) ? $partlabourcost_durasi: null ));?>
		</div>	
	</div>
</div>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-4 col-sm-offset-9 col-xs-12">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitDataLabourcost()" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button type="reset" name="btnReset" onclick="resetFormMainLabourcost()" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>
