<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormLabourcostHarga';
extract($rowData);
?>
<?php 

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtStsLabourcostHarga', 'name'=> 'txtSts', 'value' => isset($sts) ? $sts : null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpartlabourcost_kd', 'name'=> 'txtpartlabourcost_kd', 'value' => isset($partlabourcost_kd) ? $partlabourcost_kd : null ));
?>
<div class="row">
	<div class="form-group">
		<label for='idtxtbagian_nama' class="col-md-2 control-label">Bagian</label>
		<div class="col-sm-4 col-xs-12">
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtbagian_nama', 'id'=> 'idtxtbagian_nama', 'placeholder' =>'Nama Bagian', 'readonly' => 'readonly', 'value'=> isset($bagian_nama) ? $bagian_nama: null ));?>
		</div>	
	</div>

	<div class="form-group">
		<label for='idtxtpartlabourcost_qty' class="col-md-2 control-label">Jumlah Pekerja</label>
		<div class="col-sm-2 col-xs-12">
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtpartlabourcost_qty', 'id'=> 'idtxtpartlabourcost_qty', 'placeholder' =>'Qty', 'readonly' => 'readonly', 'value'=> isset($partlabourcost_qty) ? $partlabourcost_qty: null ));?>
		</div>
		<div class="col-sm-3 col-xs-12">
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtpartlabourcost_satuan', 'id'=> 'idtxtpartlabourcost_satuan', 'placeholder' =>'Qty', 'readonly' => 'readonly', 'value'=> isset($partlabourcost_satuan) ? $partlabourcost_satuan: null ));?>
		</div>		
	</div>

	<div class="form-group">
		<label for='idtxtpartlabourcost_durasi' class="col-md-2 control-label">Durasi (Jam)</label>
		<div class="col-sm-2 col-xs-12">
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtpartlabourcost_durasi', 'id'=> 'idtxtpartlabourcost_durasi', 'placeholder' =>'Durasi (Jam)', 'readonly' => 'readonly', 'value'=> isset($partlabourcost_durasi) ? $partlabourcost_durasi: null ));?>
		</div>	
	</div>
	<div class="form-group">
		<label for='idtxtpartlabourcost_hargaunit' class="col-md-2 control-label">Harga /unit/jam</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpartlabourcost_hargaunit"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtpartlabourcost_hargaunit', 'id'=> 'idtxtpartlabourcost_hargaunit', 'placeholder' =>'Harga', 'value'=> isset($partlabourcost_hargaunit) ? $partlabourcost_hargaunit: null ));?>
		</div>	
	</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="col-sm-2 pull-right">
                <button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitDataLabourcostHarga('<?= $form_id ?>')" class="btn btn-primary btn-sm">
                    <i class="fa fa-save"></i> Simpan
                </button>
                <button onclick="close_boxcontent()" class="btn btn-default btn-sm">
                    <i class="fa fa-times"></i> Tutup
                </button>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
