<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<?php
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtStsCosting', 'placeholder' => 'idtxtStsCosting', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpart_kdCosting', 'placeholder' => 'idtxtpart_kdCosting', 'value' => isset($id) ? $id: null ));
?>
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<div class="col-sm-12 col-xs-12">
				<div class="btn-group">	
					<?php if (cek_permission('PARTCOSTING_UPDATE')) : ?>
					<button onclick="action_updateharga_labourcost('<?= $id; ?>')" class="btn btn-warning btn-sm"> <i class="fa fa-refresh"></i> Update Harga </button>
					<?php endif; ?>
					<button onclick="open_formbom()" class="btn btn-info btn-sm"> <i class="fa fa-arrow-circle-left"></i> Raw Material </button>
					<button onclick="open_overhead()" class="btn btn-info btn-sm"> Overhead <i class="fa fa-arrow-circle-right"></i> </button>
				</div>
			</div>
		</div>
	</div>
</div>
<hr>
<table id="idTableLaburcostCosting" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:4%; text-align:center;" class="all">Opsi</th>
		<th style="width:30%; text-align:center;">Nama Bagian</th>
		<th style="width:2%; text-align:center;">Jumlah Pekerja</th>
		<th style="width:10%; text-align:center;">Durasi (Jam)</th>
		<th style="width:2%; text-align:center;">Harga Unit</th>
		<th style="width:2%; text-align:center;">Harga Total</th>
		<th style="width:2%; text-align:center;">Last Update</th>
	</tr>
	</thead>
</table>

<script type="text/javascript">
	$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};
	var table = $('#idTableLaburcostCosting').DataTable({
		"processing": true,
		"serverSide": true,
		"ordering" : true,
        "searching": false,
        "paging": false,
        "info" : false,
		"ajax": "<?php echo base_url().$class_link.'/tablecosting_data?id='.$id; ?>",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"columnDefs": [
			{"data": null, "searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
			{"searchable": false, "orderable": false, "targets": 1},
			{"className": "dt-left", "targets": 2},
            {"className": "dt-right", "targets": 3},
			{"className": "dt-right", "targets": 4},
		],
		"order":[2, 'asc'],
		"rowCallback": function (row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			var index = page * length + (iDisplayIndex + 1);
			$('td:eq(0)', row).html(index);
		}
	});

    function action_updateharga_labourcost(id) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_updateharga_batch'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
                        notify (resp.status, resp.pesan, 'success');
						open_tablecosting_labourcost('edit', '<?php echo $id; ?>');
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				}
			});
		}
    }
    
    function edit_data_labourcostharga(id) {
        open_form_labourcostharga('edit', id);
    }
    
</script>