<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form('<?= $sts?>', '<?= $id?>');
	form_tabledetail_main('<?= $sts?>', '<?= $id?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function render_satuan () {
		$("#idtxtrmsatuan_kd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?= base_url() ?>auto_complete/get_rm_satuan',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
					paramSatuan: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function satuan_selected(rmsatuan_kd, rmsatuan_nama) {
		$('#idtxtrmsatuan_kd').find('option').remove();
		var satuanSelected = $('#idtxtrmsatuan_kd');
		var option = new Option(rmsatuan_nama, rmsatuan_kd, true, true);
		satuanSelected.append(option).trigger('change');
	}

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		window.location.reload();
	});

	function remove_form_box(){
		$('#<?php echo $box_id; ?>').remove();
		open_table();
	}

	function open_form(sts, id) {
		boxOverlayForm('in');
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/form_main'; ?>',
				data: {'sts': sts, 'id' : id},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					render_select2('select2');
					render_datetimepicker('dateptimepicker');
					moveTo('idMainContent');
					boxOverlayForm('out');
					render_satuan();
					$('#iddropdown_rmsatuankd').hide('fast');
				}
			});
	}

	function open_form_labourcost(sts, id){
		boxOverlayForm('in');
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_linkLabourcost.'/form_main'; ?>',
				data: {'sts': sts, 'id' : id},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					render_select2('select2');
					moveTo('idMainContent');
					boxOverlayForm('out');
				}
			});
	}

	function form_tabledetail_main(sts, id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_tabledetail_main'; ?>',
			data: {'sts': sts, 'id' : id},
			success: function(html) {
				$('#<?php echo $box_content_detail_id; ?>').html(html);
			}
		});
	}

	function open_table_labourcost(sts, id) {
		boxOverlayForm('in');
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_linkLabourcost.'/table_main'; ?>',
				data: {'sts': sts, 'id' : id},
				success: function(html) {
					$('#<?php echo $box_content_detail_id; ?>').html(html);
					boxOverlayForm('out');
				}
			});
	}

	// function open_table_overhead(sts, id) {
	// 	boxOverlayForm('in');
	// 		$.ajax({
	// 			type: 'GET',
	// 			url: '<?php //echo base_url().$class_linkOverhead.'/table_main'; ?>',
	// 			data: {'sts': sts, 'id' : id},
	// 			success: function(html) {
	// 				$('#<?php //echo $box_content_detail_id; ?>').html(html);
	// 				boxOverlayForm('out');
	// 			}
	// 		});
	// }

	function open_detail_plate(sts, partdetail_kd, partdetailplate_kd){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_linkDetailPlate.'/form_main'; ?>',
			data: {'sts' : sts,'partdetail_kd' : partdetail_kd, 'partdetailplate_kd' : partdetailplate_kd},
			success: function(html) {
				toggle_modal('Detail Plate', html);			
			}
		});
	}

	// function open_form_overhead(sts, id, stsoverhead, partoverhead_kd){
	// 	boxOverlayForm('in');
	// 		$.ajax({
	// 			type: 'GET',
	// 			url: '<?php //echo base_url().$class_linkOverhead.'/form_main'; ?>',
	// 			data: {'sts': sts, 'id' : id, 'stsoverhead': stsoverhead, 'partoverhead_kd': partoverhead_kd},
	// 			success: function(html) {
	// 				$('#<?php //echo $box_content_id; ?>').html(html);
	// 				moveTo('idMainContent');
	// 				boxOverlayForm('out');
	// 			}
	// 		});
	// }

	function open_labourcost(){
		var id = $('#idtxtpart_kd').val();
		var sts = $('#idtxtSts').val();
		open_form_labourcost(sts, id);
		open_table_labourcost(sts, id);
	}

	function open_formbom () {
		var id = $('#idtxtpart_kd').val();
		var sts = $('#idtxtSts').val();
		open_form(sts, id);
		form_tabledetail_main(sts, id);
	}

	// function open_overhead () {
	// 	var id = $('#idtxtpart_kd').val();
	// 	var sts = $('#idtxtSts').val();
	// 	open_form_overhead(sts, id, '', '');
	// 	open_table_overhead(sts, id);
	// }

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	$(document).off('change', '#idtxtrm_kd').on('change', '#idtxtrm_kd', function(e){
		var rm_kd = $(this).val();
		if (rm_kd != 'SPECIAL') {
			getMaterialData(rm_kd);
		}
	});

	$(document).off('change', '#idtxtjenisMaterial').on('change', '#idtxtjenisMaterial', function() {
		var jnsMaterial = $(this).val();
		jenis_material(jnsMaterial);
	});

	function jenis_material(jnsMaterial){
		if (jnsMaterial == 'special'){
			$('#idtxtrm_kd').val('SPECIAL').trigger('change');
			$('#idtxtrm_nama').attr('readonly', false);
			$('#iddropdown_rmsatuankd').slideDown();
			$('#idtxtrmsatuan_nama').slideUp();
		}else if (jnsMaterial == 'std'){
			$('#idtxtrm_kd').val('').trigger('change');
			$('#idtxtrm_nama').attr('readonly', true);
			$('#iddropdown_rmsatuankd').slideUp();
			$('#idtxtrmsatuan_nama').slideDown();
			resetFormMain();
		}
	}

	function open_form_detailpipe (rm_kd, partdetail_kd) {
		let part_kd = $('#idtxtpart_kd').val();
		let sts = 'add';
		let bagian_kd = $('#idtxtbagian_kd').val();
		let jenismaterial = $('#idtxtjenisMaterial').val();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_part/part_detailpipe/form_main'; ?>',
			data: {'sts': sts,'part_kd' : part_kd, 'partdetail_kd': partdetail_kd ,'rm_kd' : rm_kd, 'bagian_kd': bagian_kd, 'jenismaterial': jenismaterial},
			success: function(html) {
				toggle_modal('Form Pipe',html);
			}
		});
	}
	
	function getMaterialData(materialKd){
		$('#idrowPlateDimensi').slideUp();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/getMaterialData'; ?>',
			data: {'id' : materialKd},
			success: function(data) {
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$('#idtxtrm_nama').val(resp.data.rm_nama);
					$('#idtxtrmsatuan_nama').val(resp.data.rmsatuan_nama);
					$('#idtxtrm_deskripsi').val(resp.data.rm_deskripsi);
					$('#idtxtrm_spesifikasi').val(resp.data.rm_spesifikasi);
					$('#idtxtrm_platepanjang').val(resp.data.rm_platepanjang);
					$('#idtxtrm_platelebar').val(resp.data.rm_platelebar);
					$('#idtxtrm_platetebal').val(resp.data.rm_platetebal);
					satuan_selected(resp.data.rmsatuan_kd, resp.data.rmsatuan_nama);
					if (resp.data.rmkategori_kd == '01'){
						$('#idrowPlateDimensi').slideDown();
						var massa = parseFloat (resp.data.rm_platepanjang) * parseFloat(resp.data.rm_platelebar) * parseFloat(resp.data.rm_platetebal) / 1000000 * parseFloat(resp.data.rm_platemassajenis); 
						$('#idtxtrm_platemassa').val(massa);
					}else if (resp.data.rmkategori_kd == '02') { 
						// untuk pipe
						open_form_detailpipe (resp.data.rm_kd, '');				
					}
					$('#idtxtpartdetail_qty').focus();
				}
			}
		});
	}

	function edit_data_detail(id){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/getDetailPart'; ?>',
			data: {'id' : id},
			success: function(data) {
				var resp = JSON.parse(data);
				console.log(resp);
				if (resp.code == 200){
					$('#idtxtpartdetail_kd').val(resp.data.partdetail_kd);
					$('#idtxtStsDetail').val('edit');
					$('#idtxtrm_kd').val(resp.data.rm_kd).trigger('change');
					$('#idtxtrm_nama').val(resp.data.partdetail_nama);
					$('#idtxtrmsatuan_nama').val(resp.data.rmsatuan_nama);
					$('#idtxtrm_deskripsi').val(resp.data.partdetail_deskripsi);
					$('#idtxtrm_spesifikasi').val(resp.data.partdetail_spesifikasi);
					$('#idtxtbagian_kd').val(resp.data.bagian_kd).trigger('change');
					$('#idtxtpartdetail_qty').val(resp.data.partdetail_qty);
					satuan_selected(resp.data.rmsatuan_kd, resp.data.rmsatuan_nama);
					if (resp.data.rm_kd == 'SPECIAL') {
						$('#idtxtrm_nama').attr('readonly', false);
						$('#iddropdown_rmsatuankd').slideDown();
						$('#idtxtrmsatuan_nama').slideUp();
					}
					moveTo('idMainContent');
				}
			}
		});
	}

	function edit_data_labourcost(id){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_linkLabourcost.'/getDetailLabourcost'; ?>',
			data: {'id' : id},
			success: function(data) {
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$('#idtxtStsLabourcost').val('edit');
					$('#idtxttxtpartlaburcost_kd').val(resp.data.partlabourcost_kd);
					$('#idtxtbagian_kd').val(resp.data.bagian_kd).trigger('change');
					$('#idtxtpartlabourcost_qty').val(resp.data.partlabourcost_qty);
					$('#idtxtlabourcost_satuan').val(resp.data.partlabourcost_satuan).trigger('change');
					$('#idtxtpartlabourcost_durasi').val(resp.data.partlabourcost_durasi);
					moveTo('idMainContent');
				}
			}
		});
	}

	function close_formeditbox(partmain_kd) {
		event.preventDefault();
		var url = '<?= base_url()?>manage_items/manage_part/part_versi?partmain_kd='+partmain_kd;
		window.location.assign(url);
	}
	
	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
		boxOverlayForm('out');
	}

	function submitData() {
		boxOverlayForm('in');
		event.preventDefault();
		var form = document.getElementById('idFormInput');
		// edit atau add
		var sts =  $('#idtxtStsDetail').val();
		url = "<?php echo base_url().$class_link; ?>/action_insert";
		if (sts == 'edit'){
			url = "<?php echo base_url().$class_link; ?>/action_update";
		}

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				console.log(resp);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					var id = $('#idtxtpart_kd').val();
					form_tabledetail_main($('#idtxtSts').val(), id)
					$('.errInput').html('');
					jenis_material('std');
					$('#idtxtrm_kd').focus();
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					boxOverlayForm('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
				boxOverlayForm('out');
			} 	        
		});
	}

	function submitDataLabourcost () {
		boxOverlayForm('in');
		event.preventDefault();
		var form = document.getElementById('idFormLabourcost');
		// edit atau add
		var sts =  $('#idtxtStsLabourcost').val();
		url = "<?php echo base_url().$class_linkLabourcost; ?>/action_insert";
		if (sts == 'edit'){
			url = "<?php echo base_url().$class_linkLabourcost; ?>/action_update";
		}

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					var id = $('#idtxtpart_kd').val();
					open_table_labourcost(sts, id);
					$('.errInput').html('');
					resetFormLabourcost();
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					boxOverlayForm('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function submitDataOverhead () {
		boxOverlayForm('in');
		event.preventDefault();
		var form = document.getElementById('idFormOverhead');
		// edit atau add
		var sts =  $('#idtxtStsOverhead').val();
		url = "<?php echo base_url().$class_linkOverhead; ?>/action_insert";
		if (sts == 'edit'){
			url = "<?php echo base_url().$class_linkOverhead; ?>/action_update";
		}

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					var id = $('#idtxtpart_kd').val();
					var sts = $('#idtxtSts').val();
					open_overhead(sts, id);
					$('.errInput').html('');
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					boxOverlayForm('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function resetFormMain() {
		$('.tt-input').val('');
		$('#idtxtrm_kd').val('').trigger('change');
		$('#idrowPlateDimensi').slideUp();
		$('#idtxtrm_nama').attr('readonly', true);
		$('#iddropdown_rmsatuankd').slideUp();
		$('#idtxtrmsatuan_nama').slideDown();
	}

	function resetFormLabourcost(){
		event.preventDefault();
		$('.tt-input').val('');
	}

	function boxOverlayForm(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

	function render_select2(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_datetimepicker(valClass){
		$('.'+valClass).datetimepicker({
			format: 'DD-MM-YYYY',
    	});
	}

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }


</script>