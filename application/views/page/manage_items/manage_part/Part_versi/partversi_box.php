<?php
defined('BASEPATH') or exit('No direct script access allowed!');

/* --SET DEFAULT PROPERTY UNTUK BOX-- */
$js_file = 'partversi_js';
$box_type = 'TableVersi';
$box_title = 'Tabel Versi Part';
$data['master_var'] = 'PartData';
$data['class_link'] = $class_link;
$data['box_id'] = 'id'.$box_type.'Box'.$data['master_var'];
$data['btn_hide_id'] = 'id'.$box_type.'BtnBoxHide'.$data['master_var'];
$data['btn_add_id'] = 'id'.$box_type.'BtnBoxAdd'.$data['master_var'];
$data['btn_remove_id'] = 'id'.$box_type.'BtnBoxRemove'.$data['master_var'];
$data['box_alert_id'] = 'id'.$box_type.'BoxAlert'.$data['master_var'];
$data['box_loader_id'] = 'id'.$box_type.'BoxLoader'.$data['master_var'];
$data['box_content_id'] = 'id'.$box_type.'BoxContent'.$data['master_var'];
$data['box_overlay_id'] = 'id'.$box_type.'BoxOverlay'.$data['master_var'];
/* --END OF BOX DEFAULT PROPERTY-- */

/* --SET VARIABEL TAMBAHAN DISINI-- */
$data['partmain_kd'] = $partmain_kd;
$data['class_link'] = $class_link;
$data['class_link_partdata'] = $class_link_partdata;
$data['class_link_costing'] = $class_link_costing;
if (isset($header)) {
    extract($header);
}
/* --END OF CUSTOM PROPERTY-- */

/* --SET BUTTONS DISINI, NILAI "FALSE" UNTUK TIDAK TAMPIL, NILAI "TRUE" UNTUK MENAMPILKAN BUTTON-- */
$btn_hide = TRUE;
$btn_add = FALSE;
$btn_remove = FALSE;
/* --END OF BUTTONS SETUP-- */
?>

<style type="text/css">
	.modal-dialog {
 		width: 1000px;
	}
</style>

<div class="box box-primary" id="<?php echo $data['box_id']; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_title; ?></h3>
		<div class="box-tools pull-right">
			<?php
			if ($btn_hide) :
				?>
				<button class="btn btn-box-tool btn-collapse" id="<?php echo $data['btn_hide_id']; ?>" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
					<i class="fa fa-minus"></i>
				</button>
				<?php
			endif;
			if ($btn_add) :
				?>
				<button class="btn btn-box-tool" id="<?php echo $data['btn_add_id']; ?>" data-toggle="tooltip" title="Tambah Data">
					<i class="fa fa-plus"></i>
				</button>
				<?php
			endif;
			if ($btn_remove) :
				?>
				<button class="btn btn-box-tool btn-remove" id="<?php echo $data['btn_remove_id']; ?>" data-widget="remove" data-toggle="tooltip" title="Close">
					<i class="fa fa-times"></i>
				</button>
				<?php
			endif;
			?>
		</div>
	</div>
	<div class="box-body">
		<div class="row invoice-info">
			<div class="col-sm-6 invoice-col">
				<b>Nama Part :</b> <?php echo !empty($partmain_nama) ? $partmain_nama : '-	'; ?><br>
				<b>Jenis Part:</b> <?php echo !empty($partjenis_nama) ? $partjenis_nama : '-'; ?> <br>
				<b>Status Part:</b> <?php 
				if (empty($partmain_status)){ 
					echo build_span('danger', 'Tidak Aktif'); 
				}else{
					echo build_span('success', 'Aktif'); 
				}  ?>
			</div>
			<div class="col-sm-6 invoice-col">
				<b>Tanggal Part :</b> <?php echo format_date($partmain_tglinput, 'd-m-Y H:i:s') ?><br>
			</div>
		</div>
        <hr>
		<div class="row">
			<div class="col-sm-12">
				<?php if (cek_permission('PARTVERSI_CREATE')) :?>
				<div class="btn-group">
					<button class="btn btn-sm btn-success" onclick="action_addpart('<?= $partmain_kd ?>')"> <i class="fa fa-plus"></i> Tambah Part </button>
				</div>
				<?php endif;?>
				<div class="btn-group pull-right">
					<button class="btn btn-sm btn-default" onclick="window.history.back()"> <i class="fa fa-arrow-circle-left"></i> Kembali </button>
				</div>
			</div>
		</div>
		<hr>
		<div id="<?php echo $data['box_alert_id']; ?>"></div>
		<div id="<?php echo $data['box_loader_id']; ?>" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="<?php echo $data['box_content_id']; ?>"></div>
	</div>
	<div class="box-footer">
	</div>
	<div class="overlay" id="<?php echo $data['box_overlay_id']; ?>" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
	<?php $this->load->view('page/'.$class_link.'/'.$js_file, $data); ?>
</div>