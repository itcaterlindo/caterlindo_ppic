<?php
defined('BASEPATH') or exit('No direct script access allowed!');

/* --SET DEFAULT PROPERTY UNTUK BOX-- */
$js_file = 'form_js';
$box_type = 'Form';
$box_title = 'Form Data Part';
$data['master_var'] = 'PartData';
$data['class_link'] = $class_link;
$data['box_id'] = 'id'.$box_type.'Box'.$data['master_var'];
$data['btn_hide_id'] = 'id'.$box_type.'BtnBoxHide'.$data['master_var'];
$data['btn_add_id'] = 'id'.$box_type.'BtnBoxAdd'.$data['master_var'];
$data['btn_remove_id'] = 'id'.$box_type.'BtnBoxRemove'.$data['master_var'];
$data['box_alert_id'] = 'id'.$box_type.'BoxAlert'.$data['master_var'];
$data['box_loader_id'] = 'id'.$box_type.'BoxLoader'.$data['master_var'];
$data['box_content_id'] = 'id'.$box_type.'BoxContent'.$data['master_var'];
$data['box_overlay_id'] = 'id'.$box_type.'BoxOverlay'.$data['master_var'];
/* --END OF BOX DEFAULT PROPERTY-- */

/* --SET VARIABEL TAMBAHAN DISINI-- */
$data['class_linkLabourcost'] = $class_linkLabourcost;
$data['id'] = $id;
$data['sts'] = $sts;
$data['box_content_detail_id'] = 'id'.$box_type.'BoxContentDetail'.$data['master_var'];
$data['class_linkDetailPlate'] = $class_linkDetailPlate;

$data['modal_title'] = 'Rekomendasi Material';
$data['content_modal_id'] = 'id'.$box_type.'ModalContentDetail'.$data['master_var'];
extract($rowDataMasterPart);
/* --END OF CUSTOM PROPERTY-- */

/* --SET BUTTONS DISINI, NILAI "FALSE" UNTUK TIDAK TAMPIL, NILAI "TRUE" UNTUK MENAMPILKAN BUTTON-- */
$btn_hide = TRUE;
$btn_add = FALSE;
$btn_remove = FALSE;
/* --END OF BUTTONS SETUP-- */
?>

<style type="text/css">
	.modal-dialog {
 		width: 1000px;
	}
</style>

<div class="box box-primary" id="<?php echo $data['box_id']; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_title; ?></h3>
		<div class="box-tools pull-right">
			<?php
			if ($btn_hide) :
				?>
				<button class="btn btn-box-tool btn-collapse" id="<?php echo $data['btn_hide_id']; ?>" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
					<i class="fa fa-minus"></i>
				</button>
				<?php
			endif;
			if ($btn_add) :
				?>
				<button class="btn btn-box-tool" id="<?php echo $data['btn_add_id']; ?>" data-toggle="tooltip" title="Tambah Data">
					<i class="fa fa-plus"></i>
				</button>
				<?php
			endif;
			if ($btn_remove) :
				?>
				<button class="btn btn-box-tool btn-remove" id="<?php echo $data['btn_remove_id']; ?>" data-widget="remove" data-toggle="tooltip" title="Close">
					<i class="fa fa-times"></i>
				</button>
				<?php
			endif;
			?>
		</div>
	</div>
	<div class="box-body">
		<input type="hidden" id="idboxpartmain_kd" placeholder="idboxpartmain_kd" value="<?= $partmain_kd?>">
		<input type="hidden" id="idboxpart_kd" placeholder="idboxpart_kd" value="<?= $part_kd?>">
		<div class="row invoice-info">
			<div class="col-sm-6 invoice-col">
				<b>Nama Part :</b> <?php echo isset($partmain_nama) ? $partmain_nama : '-'; ?><br>
				<b>Jenis Part :</b> <?php echo isset($partjenis_nama) ? $partjenis_nama : '-'; ?><br>
				<b>State Part :</b> <?php echo build_span ($partstate_label, $partstate_nama); ?><br>
			</div>
			<div class="col-sm-6 invoice-col">
				<b>Versi :</b> <?php echo isset($part_versi) ? $part_versi : '-'; ?><br>
				<b>Tanggal :</b> <?php echo format_date($part_tgledit, 'd-m-Y H:i:s') ?><br>
			</div>
		</div>
		<hr>
		<div id="<?php echo $data['box_alert_id']; ?>"></div>
		<div id="<?php echo $data['box_loader_id']; ?>" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="<?php echo $data['box_content_id']; ?>"></div>
		<div class="col-md-12">
			<hr>
			<div id="<?php echo $data['box_content_detail_id']; ?>"></div>
		</div>
	</div>
	<div class="box-footer">
	<button class="btn btn-success btn-sm pull-right" onclick="close_formeditbox('<?= $partmain_kd?>')"> <i class="fa fa-check"></i> Selesai </button>
	</div>
	<div class="overlay" id="<?php echo $data['box_overlay_id']; ?>" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
	<?php $this->load->view('page/'.$class_link.'/'.$js_file, $data); ?>
</div>

<!-- modal -->
<div class="modal fade" id="idmodal">
	<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"><?php echo $data['modal_title']; ?></h4>
		</div>
		<div class="modal-body">

			<div class="row">
				<div id="<?php echo $data['content_modal_id'];?>"></div>
			</div>
			
		</div>
		<div class="modal-footer">
		<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Keluar</button>
		</div>
	</div>
	</div>
</div>

