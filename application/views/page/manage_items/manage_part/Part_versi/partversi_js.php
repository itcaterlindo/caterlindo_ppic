<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	partversi_table_main('<?php echo $partmain_kd; ?>');
	first_load_versi('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function first_load_versi(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function partversi_table_main(partmain_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/partversi_table_main'; ?>',
			data: {'partmain_kd' : partmain_kd},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
				moveTo('idMainContent');
			}
		});
	} 

	function detail_part (part_kd) {
		window.location.assign('<?= base_url().$class_link_partdata?>/view_detailtable_box?part_kd='+part_kd);
	}

	function detail_partcosting (part_kd) {
		window.location.assign('<?= base_url().$class_link_costing?>/partcosting_box?part_kd='+part_kd);
	}

	function open_form_box(sts, id) {
		var url = '<?= base_url().$class_link?>/form_box?sts='+sts+'&id='+id;
		window.location.assign(url);
	}

	function open_formcosting_box (sts, id) {
		var url = '<?= base_url().$class_link_costing?>/form_box?sts='+sts+'&id='+id;
		window.location.assign(url);
	}

	function action_addpart(partmain_kd) {
		var conf = confirm('Apakah anda yakin menambah part versi baru ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_insertpart'; ?>',
				data: {'partmain_kd' : partmain_kd},
				success: function(data) {
					var resp = JSON.parse(data);
					if (resp.code == 200) {
						open_form_box('add', resp.part_kd)
					}else{
						notify (resp.status, resp.pesan, 'error')
					}
				}
			});
		}
	}

	function action_duplicatepart (part_kd) {
		var conf = confirm('Apakah anda yakin duplikasi item ini ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_duplicatepart'; ?>',
				data: {'part_kd' : part_kd},
				success: function(data) {
					var resp = JSON.parse(data);
					if (resp.code == 200) {
						location.reload();
					}else{
						notify (resp.status, resp.pesan, 'error')
					}
				}
			});
		}
	}

	function action_deletepart (part_kd) {
		var conf = confirm('Apakah anda yakin hapus item ini ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_deletepart'; ?>',
				data: {'part_kd' : part_kd},
				success: function(data) {
					var resp = JSON.parse(data);
					if (resp.code == 200) {
						location.reload();
					}else{
						notify (resp.status, resp.pesan, 'error')
					}
				}
			});
		}
	}

	function action_defaultpart (partmain_kd, part_kd) {
		var conf = confirm('Apakah anda yakin set default item ini ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_defaultpart'; ?>',
				data: {partmain_kd: partmain_kd, part_kd: part_kd},
				success: function(data) {
					var resp = JSON.parse(data);
					if (resp.code == 200) {
						location.reload();
					}else{
						notify (resp.status, resp.pesan, 'error')
					}
				}
			});
		}
	}

	function box_overlayVersi(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }

	// function render_select2(valClass){
	// 	$('.'+valClass).select2({
	// 		theme: 'bootstrap',
	// 		placeholder: '--Pilih Opsi--',
	// 	});
	// }

	// function render_datetimepicker(valClass){
	// 	$('.'+valClass).datetimepicker({
	// 		format: 'DD-MM-YYYY',
    // 	});
	// }

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

</script>