<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';

if ($sts == 'edit'){
	extract($rowData);
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtoverhead_kd', 'name'=> 'txtoverhead_kd', 'value' => isset($overhead_kd) ? $overhead_kd: null ));

?>

<div class="row">
	<div class="col-md-12">

	<div class="form-group">
		<label for="idtxtoverhead_nama" class="col-md-2 control-label">Nama</label>
		<div class="col-sm-5 col-xs-12">
			<div class="errInput" id="idErroverhead_nama"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtoverhead_nama', 'id'=> 'idtxtoverhead_nama', 'placeholder' =>'Nama', 'value'=> isset($overhead_nama) ? $overhead_nama: null ));?>
		</div>
	</div>

	<div class="form-group">
		<label for="idtxtoverhead_satuankd" class="col-md-2 control-label">Satuan</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErroverhead_satuankd"></div>
			<?php echo form_dropdown('txtoverhead_satuankd', isset($opsiSatuan) ? $opsiSatuan : [] , isset($overhead_satuankd)? $overhead_satuankd : '', array('class'=> 'form-control select2', 'id'=> 'idtxtoverhead_satuankd'));?>
		</div>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErroverhead_harga"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtoverhead_harga', 'id'=> 'idtxtoverhead_harga', 'placeholder' =>'Harga', 'value'=> isset($overhead_harga) ? $overhead_harga: null ));?>
		</div>	
	</div>

	<div class="form-group">
		<div class="col-sm-2 col-xs-12 pull-right">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData('<?= $form_id ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>

	</div>
</div>

<?php echo form_close(); ?>
