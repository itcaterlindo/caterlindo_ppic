<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';

if ($sts == 'edit'){
	extract($rowData);
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtbomstateuser_kd', 'name'=> 'txtbomstateuser_kd', 'value' => isset($bomstateuser_kd) ? $bomstateuser_kd: null ));

?>

<div class="row">
	<div class="col-md-12">

	<div class="form-group">
		<label for="idtxtbomstateuser_admin" class="col-md-2 control-label">Admin</label>
		<div class="col-sm-5 col-xs-12">
		<div class="errInput" id="idErrbomstateuser_admin"></div>
			<?php echo form_dropdown('txtbomstateuser_admin', isset($opsiAdmin) ? $opsiAdmin : [] , isset($bomstateuser_admin)? $bomstateuser_admin : '', array('class'=> 'form-control select2', 'id'=> 'idtxtbomstateuser_admin'));?>
		</div>
	</div>

	<div class="form-group">
		<label for="idtxtbomstate_kd" class="col-md-2 control-label">State</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrbomstate_kd"></div>
			<?php echo form_dropdown('txtbomstate_kd', isset($opsiState) ? $opsiState : [] , isset($bomstate_kd)? $bomstate_kd : '', array('class'=> 'form-control', 'id'=> 'idtxtbomstate_kd'));?>
		</div>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrbomstateuser_email"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtbomstateuser_email', 'id'=> 'idtxtbomstateuser_email', 'placeholder' =>'Email', 'value'=> isset($bomstateuser_email) ? $bomstateuser_email: null ));?>
		</div>	
	</div>
	
	<div class="form-group">
		<label for="idtxtbomstateuser_emailcc" class="col-md-2 control-label">Email CC</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrbomstateuser_emailcc"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtbomstateuser_emailcc', 'id'=> 'idtxtbomstateuser_emailcc', 'placeholder' =>'email1;email2', 'value'=> isset($bomstateuser_emailcc) ? $bomstateuser_emailcc: null ));?>
		</div>	
	</div>

	<div class="form-group">
		<div class="col-sm-2 col-xs-12 pull-right">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData('<?= $form_id ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>

	</div>
</div>

<?php echo form_close(); ?>
