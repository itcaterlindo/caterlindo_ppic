<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';

if ($sts == 'edit'){
	extract($rowData);
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtadminpermission_kd', 'name'=> 'txtadminpermission_kd', 'value' => isset($adminpermission_kd) ? $adminpermission_kd: null ));

?>

<div class="row">
	<div class="col-md-12">

	<div class="form-group">
		<label for="idtxtkd_admin" class="col-md-2 control-label">Admin</label>
		<div class="col-sm-5 col-xs-12">
		<div class="errInput" id="idErrkd_admin"></div>
			<?php echo form_dropdown('txtkd_admin', isset($opsiAdmin) ? $opsiAdmin : [] , isset($kd_admin)? $kd_admin : '', array('class'=> 'form-control select2', 'id'=> 'idtxtkd_admin'));?>
		</div>
	</div>

	<div class="form-group">
		<label for="idtxtpermission_kd" class="col-md-2 control-label">Permission</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpermission_kd"></div>
			<?php echo form_dropdown('txtpermission_kd', isset($opsiPermission) ? $opsiPermission : [] , isset($permission_kd)? $permission_kd : '', array('class'=> 'form-control', 'id'=> 'idtxtpermission_kd'));?>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-2 col-xs-12 pull-right">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData('<?= $form_id ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>

	</div>
</div>

<?php echo form_close(); ?>
