<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
    form_main('<?= $sts ?>', '<?= $id ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function form_main (sts, id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_main'; ?>',
			data: {sts: sts, id: id},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
			}
		});
	}

    function remove_formbox() {
        $('#<?= $box_id?>').remove();
    }

	function submitData() {
		box_overlay_form('in');
		event.preventDefault();
		var form = document.getElementById('idFormInput');
		// edit atau add
		var sts =  $('#idtxtSts').val();
		if (sts == 'add'){
			url = "<?php echo base_url().$class_link; ?>/action_insert";
		}else{
			url = "<?php echo base_url().$class_link; ?>/action_update";
		}

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					open_table();
					$('.errInput').html('');
                    remove_formbox();
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay_form('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
                box_overlay_form('out');
			} 	        
		});
	}

	function box_overlay_form(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

</script>