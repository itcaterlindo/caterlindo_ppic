<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';

if ($sts == 'edit'){
	extract($rowData);
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtlabourcostharga_kd', 'name'=> 'txtlabourcostharga_kd', 'value' => isset($labourcostharga_kd) ? $labourcostharga_kd: null ));

?>

<div class="row">
	<div class="col-md-12">

	<div class="form-group">
		<label for="idtxtbagian_kd" class="col-md-2 control-label">Bagian</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrbagian_kd"></div>
			<?php echo form_dropdown('txtbagian_kd', isset($opsiBagian) ? $opsiBagian : [] , isset($bagian_kd)? $bagian_kd : '', array('class'=> 'form-control', 'id'=> 'idtxtbagian_kd'));?>
		</div>	
	</div>

	<div class="form-group">
		<label for="idtxtlabourcostharga_satuan" class="col-md-2 control-label">Satuan</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrlabourcostharga_satuan"></div>
			<?php echo form_dropdown('txtlabourcostharga_satuan', isset($opsiSatuan) ? $opsiSatuan : [] , isset($labourcostharga_satuan)? $labourcostharga_satuan : '', array('class'=> 'form-control', 'id'=> 'idtxtlabourcostharga_satuan'));?>
		</div>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrlabourcostharga_harga"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtlabourcostharga_harga', 'id'=> 'idtxtlabourcostharga_harga', 'placeholder' =>'Harga', 'value'=> isset($labourcostharga_harga) ? $labourcostharga_harga: null ));?>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-2 col-xs-12 pull-right">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData('<?= $form_id ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>

	</div>
</div>

<?php echo form_close(); ?>
