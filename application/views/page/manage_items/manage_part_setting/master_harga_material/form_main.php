<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';

if ($sts == 'edit'){
	extract($rowData);
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtrmharga_kd', 'name'=> 'txtrmharga_kd', 'value' => isset($rmharga_kd) ? $rmharga_kd: null ));

?>

<div class="row">
	<div class="col-md-12">

	<div class="form-group">
		<label for='idtxtrm_kd' class="col-md-2 control-label">RM Kode / Nama</label>
		<div class="col-sm-8 col-xs-12">
			<div class="errInput" id="idErrrm_kd"></div>
			<?php echo form_dropdown('txtrm_kd', isset($opsiRm) ? $opsiRm : [] , isset($rm_kd)? $rm_kd : '', array('class'=> 'form-control select2', 'id'=> 'idtxtrm_kd'));?>
		</div>	
	</div>

	<div class="form-group">
		<label for="idtxtrmharga_hargainput" class="col-md-2 control-label">Harga Costing</label>
		<div class="col-sm-6 col-xs-12">
			<div class="errInput" id="idErrrmharga_hargainput"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtrmharga_hargainput', 'id'=> 'idtxtrmharga_hargainput', 'placeholder' =>'Harga', 'value'=> isset($rmharga_hargainput) ? $rmharga_hargainput: null ));?>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-2 col-xs-12 pull-right">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData('<?= $form_id ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>

	</div>
</div>

<?php echo form_close(); ?>
