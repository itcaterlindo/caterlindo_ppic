<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';

if ($sts == 'edit'){
	extract($rowData);
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpartstateuser_kd', 'name'=> 'txtpartstateuser_kd', 'value' => isset($partstateuser_kd) ? $partstateuser_kd: null ));

?>

<div class="row">
	<div class="col-md-12">

	<div class="form-group">
		<label for="idtxtpartstateuser_admin" class="col-md-2 control-label">Admin</label>
		<div class="col-sm-5 col-xs-12">
		<div class="errInput" id="idErrpartstateuser_admin"></div>
			<?php echo form_dropdown('txtpartstateuser_admin', isset($opsiAdmin) ? $opsiAdmin : [] , isset($partstateuser_admin)? $partstateuser_admin : '', array('class'=> 'form-control select2', 'id'=> 'idtxtpartstateuser_admin'));?>
		</div>
	</div>

	<div class="form-group">
		<label for="idtxtpartstate_kd" class="col-md-2 control-label">State</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpartstate_kd"></div>
			<?php echo form_dropdown('txtpartstate_kd', isset($opsiState) ? $opsiState : [] , isset($partstate_kd)? $partstate_kd : '', array('class'=> 'form-control', 'id'=> 'idtxtpartstate_kd'));?>
		</div>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpartstateuser_email"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtpartstateuser_email', 'id'=> 'idtxtpartstateuser_email', 'placeholder' =>'Email', 'value'=> isset($partstateuser_email) ? $partstateuser_email: null ));?>
		</div>	
	</div>
	
	<div class="form-group">
		<label for="idtxtpartstateuser_emailcc" class="col-md-2 control-label">Email CC</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpartstateuser_emailcc"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtpartstateuser_emailcc', 'id'=> 'idtxtpartstateuser_emailcc', 'placeholder' =>'email1;email2', 'value'=> isset($partstateuser_emailcc) ? $partstateuser_emailcc: null ));?>
		</div>	
	</div>

	<div class="form-group">
		<div class="col-sm-2 col-xs-12 pull-right">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData('<?= $form_id ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>

	</div>
</div>

<?php echo form_close(); ?>
