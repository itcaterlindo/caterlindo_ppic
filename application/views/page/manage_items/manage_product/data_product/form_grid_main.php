<?php
defined('BASEPATH') or exit('No direct script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataProduct';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>No</th>
			<?php echo $table_head; ?>
		</tr>
	</thead>
	<tbody>
		<?php echo $table_body; ?>
	</tbody>
</table>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="button" name="btnBack" id="idBtnBack" class="btn btn-warning btn-flat">
			<i class="fa fa-arrow-left"></i> Back
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" id="idBtnSubmit" class="btn btn-primary btn-flat">
			<i class="fa fa-shopping-cart"></i> Submit
		</button>
	</div>
</div>
<?php echo form_close(); ?>