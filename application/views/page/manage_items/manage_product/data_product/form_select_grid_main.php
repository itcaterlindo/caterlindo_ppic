<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataProduct';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>
<div class="form-group">
	<label for="idSelGridTipe" class="col-md-2 control-label">Grid Tipe</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrSelGridTipe"></div>
		<?php echo form_dropdown('selGridTipe', array('' => '-- Pilih Grid Tipe --', 'product_desc' => 'Edit Product Desc', 'product_dimension' => 'Edit Product Dimension', 'product_price' => 'Edit Product Price'), '', array('id' => 'idSelGridTipe', 'class' => 'form-control')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idSelGroup" class="col-md-2 control-label">Product Group</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrSelGroup"></div>
		<?php echo form_dropdown('selGroup', $dropdown_group, '', array('id' => 'idSelGroup', 'class' => 'form-control')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idSelCategory" class="col-md-2 control-label">Product Category</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrSelCategory"></div>
		<?php echo form_dropdown('selCategory', $dropdown_kat, '', array('id' => 'idSelCategory', 'class' => 'form-control')); ?>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" class="btn btn-primary btn-flat">
			<i class="fa fa-shopping-cart"></i> Submit
		</button>
	</div>
</div>
<?php echo form_close(); ?>