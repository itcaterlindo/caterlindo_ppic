<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form('<?php echo $type; ?>', '<?php echo $group; ?>', '<?php echo $kat; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		lihatTabel();
	});

	$(document).off('submit', '#idForm<?php echo $master_var; ?>').on('submit', '#idForm<?php echo $master_var; ?>', function(e) {
		e.preventDefault();
		submit_form(this);
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_form(type, group, kat) {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/get_form_grid'; ?>',
				data: 'type='+type+'&group='+group+'&kat='+kat,
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function close_form() {
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$('#<?php echo $box_id; ?>').fadeOut(function() {
				$('#<?php echo $box_id; ?>').remove();
			});
		});
	}

	$(document).off('click', '#idBtnBack').on('click', '#idBtnBack', function() {
		show_form_select_grid();
	});

	/*$(document).off('focusout', '.item-barcode').on('focusout', '.item-barcode', function() {
		$(this).attr('readonly', 'TRUE');
		var jml_err_barcode = 0;
		var barcode_id = $(this);
		var barcode = $(this).val();
		var kd_barang = $(this).closest('tr').find('.item-code').val();
		var err_barcode = $(this).closest('tr').find('.err-barcode');
		var val_err_barcode = $(this).closest('tr').find('.val-jml-err-barcode');
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/data_product/verify_barcode'; ?>',
			data: 'id='+barcode+'&kd_barang='+kd_barang,
			success:function(data){
				err_barcode.html(data.idErrBarcode);
				barcode_id.removeAttr('readonly');
				if (data.confirm == 'success') {
					val_err_barcode.val('0');
				}
				if (data.confirm == 'errValidation') {
					val_err_barcode.val('1');
				}

				$('.val-jml-err-barcode').each(function() {
					jml_err_barcode += parseInt(this.value);
					console.log(this.value);
				});
				if (jml_err_barcode > 0) {
					$('#idBtnSubmit').slideUp();
				} else {
					$('#idBtnSubmit').slideDown();
				}
				console.log(jml_err_barcode);
			}
		});
	});*/

	$(document).off('keyup', '.hitung_netweight').on('keyup', '.hitung_netweight', function() {
		hitung_net(this);
	});

	$(document).off('keyup', '.hitung_cbm').on('keyup', '.hitung_cbm', function() {
		hitung_cbm(this);
	});

	function hitung_net(selector) {
		var gross = $(selector).closest('tr').find('.item-grossweight').val();
		var box = $(selector).closest('tr').find('.item-boxweight').val();
		net = gross - box;
		$(selector).closest('tr').find('.item-netweight').val(net);
	}

	function hitung_cbm(selector) {
		var length = $(selector).closest('tr').find('.item-length').val();
		var width = $(selector).closest('tr').find('.item-width').val();
		var height = $(selector).closest('tr').find('.item-height').val();
		cbm = (length * width * height) / 1000000;
		$(selector).closest('tr').find('.item-cbm').val(cbm);
	}

	function submit_form(form_id) {
		$('#<?php echo $box_overlay_id; ?>').show();
		$('#idAlertTable').html('');
		$('#<?php echo $box_alert_id; ?>').html('');
		$.ajax({
			url: "<?php echo base_url().$class_link.'/submit_product_grid'; ?>",
			type: "POST",
			data:  new FormData(form_id),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					$('#<?php echo $box_content_id; ?>').slideUp();
					$('#<?php echo $box_id; ?>').remove();
					$('#idAlertTable').html(data.alert);
					lihatTabel();
				}
				if (data.confirm == 'error') {
					$('#<?php echo $box_alert_id; ?>').html(data.alert);
				}
				$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
				$('#<?php echo $box_overlay_id; ?>').hide();
				if(data.alert != null){
					notify ('Error API', data.alert, 'error');
				}
			}
		});
	}
</script>