<?php

class customPdf extends Tcpdf {
    public $part_tglinput; 
    public $part_versi; 
    public $partmain_nama;
    public $partjenis_nama;

    function setKeterangan ($arrayKet = []) {
        $this->part_tglinput = !empty($arrayKet['part_tglinput']) ? $arrayKet['part_tglinput'] : null;
        $this->part_versi = !empty($arrayKet['part_versi']) ? $arrayKet['part_versi'] : '-';
        $this->partmain_nama = !empty($arrayKet['partmain_nama']) ? $arrayKet['partmain_nama'] : '-';
        $this->partjenis_nama = !empty($arrayKet['partjenis_nama']) ? $arrayKet['partjenis_nama'] : '-';
    }

    public function Header() {
        $header = '
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td rowspan="2" colspan="2" style="text-align:left;"> <img src="assets/admin_assets/dist/img/logo_cat.png" width="95" height="30"> </td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="2" style="font-size:10px; font-weight:bold; text-align:left;">BILL OF MATERIAL & COSTING</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td style="font-size:9px;">No Dokumen <br>Tanggal terbit <br>Rev</td>
                <td style="font-size:9px; text-align:left;">: CAT4-FAA-002 <br>: 22 Sept 2023 <br>: 03</td>
            </tr>
        </table>';
        
        $this->writeHTML($header, true, false, false, false, '');
    }
}

$pdf = new customPdf('P', 'mm', 'A4', true, 'UTF-8', false);
$titleItemCode = $bomMain['item_code'];
if ($bomMain['bom_jenis'] == 'custom') {
    $titleItemCode = $bomMain['project_nopj'];
}
$title = 'BoM-'.strtoupper($bomMain['bom_jenis']).'-'.$titleItemCode;
$pdf->SetTitle($title);
$pdf->setPrintFooter(false);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');
/** Margin */ 
$pdf->SetHeaderMargin(5);
$pdf->SetFooterMargin(10);
$pdf->SetMargins(5, 25, 5);


foreach ($bom as $rBom) {
    $pdf->AddPage();

    /** Keterangan */
    $item_code = '';
    $pj_num = '_____________';
    $desc = '';
    $nm_group = '';
    $bom_jenis = $rBom['bom_jenis'];
    if ($bom_jenis == 'std') {
        $item_code = $rBom['item_code'];
        $desc = $rBom['deskripsi_barang'].'/'.$rBom['dimensi_barang'];
        $nm_group = $rBom['nm_group'];
    }else{
        $item_code = $rBom['project_itemcode'];
        $desc = $rBom['project_itemdesc'].'/'.$rBom['project_itemdimension'];
        $pj_num = $rBom['project_nopj'];
        $nm_group = 'PROJECT';
    }

    $keterangan =
    '<table cellspacing="0" cellpadding="0" border="0" style="font-size: 80%;">
        <tr>
            <td style="text-align:left;">Date</td>
            <td style="text-align:left;">: '.format_date($rBom['bom_tgledit'], 'd-m-Y H:i:s').' </td>
            <td></td>
            <td style="text-align:left;">PJ Number</td>
            <td style="text-align:left;">: '.$pj_num.'</td>
        </tr>
        <tr>
            <td style="text-align:left;">Item Code</td>
            <td style="text-align:left;">: '.$item_code.' </td>
            <td></td>
            <td style="text-align:left;">Rev.</td>
            <td style="text-align:left;">: '.$rBom['part_versi'].'</td>
        </tr>
        <tr>
            <td style="text-align:left;">Description</td>
            <td colspan="2" style="text-align:left;">: '.$desc. ' </td>
            <td style="text-align:left;">Page</td>
            <td style="text-align:left;">: '.$pdf->getAliasNumPage().' of '.$pdf->getAliasNbPages().'</td>
        </tr>
        <tr>
            <td style="text-align:left;"></td>
            <td colspan="2" style="text-align:left;"> #'.$rBom['partmain_nama'].' </td>
            <td></td>
            <td style="text-align:left;"></td>
            <td style="text-align:left;"></td>
        </tr>
        <tr>
            <td style="text-align:left;">Detail</td>
            <td style="text-align:left;">: '.$nm_group.' </td>
            <td></td>
            <td style="text-align:left;"></td>
            <td style="text-align:left;"></td>
        </tr>
    </table>';
    
    /** Footer */
    $created=null;$createdtgl=null;$checked1=null;$checked1tgl=null;
    $checked2=null;$checked2tgl=null;$approved=null;$approvedtgl=null;
    if (isset($states)) {
        foreach ($states[$rBom['part_kd']] as $state) :
            /** PENGKONDISIAN UNTUK BOM YANG ADA COSTING */
            $state_kd = $state['partstate_kd'];
            if($state_kd == 6){
                /** Created pending costing */
                $created = $state['nm_admin'];
                $createdtgl = format_date($state['partstatelog_tglinput'], 'd-m-Y H:i:s');
            }else if($state_kd == 3){
                $checked1 = $state['nm_admin'];
                $checked1tgl = format_date($state['partstatelog_tglinput'], 'd-m-Y H:i:s');
            }else if($state_kd == 4){
                $checked2 = $state['nm_admin'];
                $checked2tgl = format_date($state['partstatelog_tglinput'], 'd-m-Y H:i:s');
            }else if($state_kd == 5){
                $approved = $state['nm_admin'];
                $approvedtgl = format_date($state['partstatelog_tglinput'], 'd-m-Y H:i:s');
            }else if($state_kd == 2){
                /** Jika created costing tidak ada maka tampilkan state pending */
                $created = $state['nm_admin'];
                $createdtgl = format_date($state['partstatelog_tglinput'], 'd-m-Y H:i:s');
            }

            /** PENGKONDISIAN UNTUK BOM YANG TIDAK ADA COSTING */
            // switch ($state['partstate_kd']) {
            //     case 2:
            //         $created = $state['nm_admin'];
            //         $createdtgl = format_date($state['partstatelog_tglinput'], 'd-m-Y H:i:s');
            //         break;
            //     case 3:
            //         $checked1 = $state['nm_admin'];
            //         $checked1tgl = format_date($state['partstatelog_tglinput'], 'd-m-Y H:i:s');
            //         break;
            //     case 4:
            //         $checked2 = $state['nm_admin'];
            //         $checked2tgl = format_date($state['partstatelog_tglinput'], 'd-m-Y H:i:s');
            //         break;
            //     case 5:
            //         $approved = $state['nm_admin'];
            //         $approvedtgl = format_date($state['partstatelog_tglinput'], 'd-m-Y H:i:s');
            //         break;
            // }
            
        endforeach;
    }

    $footer = '
    <table cellspacing="0" cellpadding="1" border="1" style="font-size: 75%;">
        <tr>
            <td width="15%"> Created By </td>
            <td width="20%"> : '.$created.' </td>
            <td width="20%"> '.$createdtgl.' </td>
        </tr>
        <tr>
            <td width="15%"> Checked1 By </td>
            <td width="20%"> : '.$checked1.' </td>
            <td width="20%"> '.$checked1tgl.' </td>
        </tr>
        <tr>
            <td width="15%"> Checked2 By </td>
            <td width="20%"> : '.$checked2.' </td>
            <td width="20%"> '.$checked2tgl.' </td>
        </tr>
        <tr>
            <td width="15%"> Approved By </td>
            <td width="20%"> : '.$approved.' </td>
            <td width="20%"> '.$approvedtgl.' </td>
        </tr>
    </table>';

    
    $pdf->writeHTML($keterangan, true, false, false, false, '');
    if (isset($konten)) {
        $pdf->writeHTML($konten[$rBom['part_kd']], true, false, false, false, '');
    }
    if (isset($states)) {
        $pdf->writeHTML($footer, true, false, false, false, '');
    }
}

$txtOutput = $title.'.pdf';
$pdf->Output($txtOutput, 'I');