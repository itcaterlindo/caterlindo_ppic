<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';

if(isset($rowData)) {
	extract($rowData);
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtbom_kd', 'name'=> 'txtbom_kd', 'value' => isset($id) ? $id: null ));

?>
<div class="row">

	<div class="form-group">
		<label for='idtxtkd_msalesorder' class="col-md-2 control-label">Sales Order</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrkd_msalesorder"></div>
			<?php echo form_dropdown('txtkd_msalesorder', $opsiSO, isset($kd_msalesorder)? $kd_msalesorder : null, array('class'=> 'form-control select2', 'id'=> 'idtxtkd_msalesorder'));?>
		</div>	
	</div>
	<div class="form-group">
		<label for='idtxtproject_kd' class="col-md-2 control-label">Item SO / Project</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrproject_kd"></div>
			<?php echo form_dropdown('txtproject_kd', isset($opsiSOitem)?$opsiSOitem:array() , isset($SOitemSelected)? $SOitemSelected : '', array('class'=> 'form-control select2', 'id'=> 'idtxtproject_kd'));?>
		</div>
	</div>

    <div class="form-group">
		<label for="idtxtbom_ket" class="col-md-2 control-label">Keterangan</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrbom_ket"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control ttmaster-input', 'name'=> 'txtbom_ket', 'id'=> 'idtxtbom_ket', 'placeholder' =>'Keterangan', 'rows' => '2', 'value'=> isset($bom_ket) ? $bom_ket: null ));?>
		</div>
	</div>

</div>
<hr>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-1 col-xs-12 col-sm-offset-10">
			<button type="submit" name="btnSubmit" onclick="submitData('<?= $form_id ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	render_select2('select2');
	<?php if ($sts =='edit') :?>
		get_item_so('<?php echo $kd_msalesorder?>', '<?php echo $project_kd?>');
		$("#idtxtproject_kd").select2("trigger", "select", { 
			data: { 
				id: '<?php echo $project_kd; ?>', 
				text: '<?php echo $project_nopj.' | '.$project_itemcode; ?>', 
			}
		});
	<?php endif;?>
</script>