<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form_main('<?php echo $sts; ?>','<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#<?php echo $box_loader_id; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
		open_table('std');
		first_load('<?php echo 'idTableBoxLoader'.$master_var; ?>', '<?php echo 'idTableBoxContent'.$master_var; ?>');
	});

	function open_form_main(sts, id) {
		box_overlay_form('in');
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_main'; ?>',
			data: {'sts': sts, 'id' : id},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
				// render_select2('select2');
				// render_dropdown_partmain ();
				if (sts != 'edit') {
					get_item_so('', '');
				}
				moveTo('idMainContent');
				box_overlay_form('out');
			}
		});
	}

	function render_dropdown_partmain () {
		$("#idtxtpartmain_kd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?= base_url().$class_link ?>/get_dropdown_partmain',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
					paramPartmain: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function partmain_selected(partmain_kd, partmain_nama) {
		$('#idtxtpartmain_kd').find('option').remove();
		var partmainSelected = $('#idtxtpartmain_kd');
		var option = new Option(partmain_nama, partmain_kd, true, true);
		partmainSelected.append(option).trigger('change');
	}

	function open_table_detail(sts, id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/table_detail_main'; ?>',
			data: {'sts': sts, 'id' : id},
			success: function(html) {
				$('#<?php echo $box_content_detail_id; ?>').html(html);
			}
		});
	}

	$(document).off('change', '#idtxtkd_msalesorder').on('change', '#idtxtkd_msalesorder', function () {
		get_item_so(this.value, '');
	});

	function get_item_so(kd_msalesorder, selected) {
		var optGdg = $('#idtxtproject_kd');
		$('#idtxtproject_kd option').remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/get_item_so'; ?>',
			data: {'kd_msalesorder': kd_msalesorder},
			success: function(resp) {
				console.log(resp);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optGdg.append($('<option></option>').val(text.project_kd).html(text.project_nopj+' | '+text.project_itemcode+' / '+text.project_itemdesc+'/'+text.project_itemdimension));
					});
				}
				$('#idtxtproject_kd').val(selected);
			}
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function render_select2(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			allowClear: true,
		});
	}

	function remove_form_box(){
		$('#<?php echo $box_id; ?>').remove();
		open_table('std');
	}

	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function resetFormMain(){
		$('#idtxtpart_kd').val('').trigger('change');
	}

	function submitData(form_id){
		event.preventDefault();
		var form = document.getElementById(form_id);
		// edit atau add
		var sts =  $('#idtxtSts').val();
		var url = "<?php echo base_url().$class_link; ?>/action_insert";
		if (sts == 'edit'){
			url = "<?php echo base_url().$class_link; ?>/action_update";
		}

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				console.log(data);
				if(resp.code == 200){
					window.location.assign('<?php echo base_url().$class_link ?>/formdetail_box?id='+resp.data.bom_kd)
					notify (resp.status, resp.pesan, 'success');
					generateToken (resp.csrf);
					$('.errInput').html('');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function box_overlay_form(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

</script>