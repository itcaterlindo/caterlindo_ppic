<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';

if (isset($rowData)) {
    extract($rowData);
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtSts', 'placeholder' => 'txtSts', 'name' => 'txtSts', 'value' => $sts));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtbom_kd', 'placeholder' => 'txtbom_kd', 'name' => 'txtbom_kd', 'value' => isset($bom_kd) ? $bom_kd : null));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtfile_kd', 'placeholder' => 'txtfile_kd', 'name' => 'txtfile_kd', 'value' => isset($id) ? $id : null));

?>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="idtxtfile_nama" class="col-md-2 control-label">Nama File</label>
            <div class="col-sm-6 col-xs-12">
                <div class="errInput" id="idErrfile_nama"></div>
                <?php echo form_input(array('type' => 'text', 'class' => 'form-control', 'name' => 'txtfile_nama', 'id' => 'idtxtfile_nama', 'placeholder' => 'Nama File', 'value' => isset($row['file_namafile']) ? explode('.', $row['file_namafile'])[0] : null)); ?>
            </div>
            <?php if ($sts == 'edit') : ?>
            <div class="col-sm-2 col-xs-12">
                <input type="text" class="form-control" name="txtfileExtension" readonly="readonly" value="<?php echo isset($row['file_namafile']) ? explode('.', $row['file_namafile'])[1] : null; ?>">
            </div>
            <?php endif; ?>
        </div>
        <?php if ($sts == 'add') : ?>
            <div class="form-group">
                <label for='idtxtproject_kd' class="col-md-2 control-label">File</label>
                <div class="col-sm-4 col-xs-12">
                    <div class="errInput" id="idErrfile"></div>
                    <input type="file" name="txtfile">
                </div>
            </div>
        <?php endif; ?>
        <div class="form-group">
            <label for="idtxtfile_note" class="col-md-2 control-label">Note</label>
            <div class="col-sm-9 col-xs-12">
                <div class="errInput" id="idErrbom_ket"></div>
                <?php echo form_textarea(array('type' => 'text', 'class' => 'form-control', 'name' => 'txtfile_note', 'id' => 'idtxtfile_note', 'placeholder' => 'Note', 'rows' => '2', 'value' => isset($row['file_note']) ? $row['file_note'] : null)); ?>
            </div>
        </div>
    </div>
</div>
<hr>

<div class="col-md-12">
    <div class="form-group">
        <div class="col-sm-1 col-xs-12 col-sm-offset-10">
            <button type="submit" name="btnSubmit" onclick="submitDataFiles('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
                <i class="fa fa-save"></i> Simpan
            </button>
        </div>
    </div>
</div>
<?php echo form_close(); ?>