<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table();
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function open_table() {
		$('#<?php echo $btn_add_id ?>').slideDown();
		box_overlay('in');
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/table_main'; ?>',
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
				moveTo('idMainContent');
				box_overlay('out');
			}
		});
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function remove_box() {
		$('#idFormBox<?php echo $master_var; ?>').remove();
	}

	function open_form_box(sts, id) {
		remove_box();
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/form_box'; ?>',
			data: {
				'sts': sts,
				'id': id
			},
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function open_view_detail_box(id) {
		$('#idFormDetailBox<?php echo $master_var; ?>').remove();
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/view_detail_box'; ?>',
			data: {
				'id': id
			},
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function edit_data(id) {
		open_form_box('edit', id)
	}

	function duplicate_data(id) {
		open_form_box('duplicate', id)
	}

	function detail_data(id) {
		window.open("<?php echo base_url().'manage_items/bom/report_bom/bom_std_pdf?bom_kd='?>"+id);
	}

	function open_filter_bom() {
		var jnsBom = $('#idtxtjnsBom').val();
		open_table(jnsBom);
	}

	function view_files(id) {
		window.open('<?php echo base_url() . $class_link; ?>/viewfiles_box?id=' + id);
	}

	function bomcustomcosting_print(id) {
		window.open("<?php echo base_url().'manage_items/bom/report_bom/bom_std_costing_pdf?bom_kd='?>"+id);
	}

	function hapus_data(id) {
		var conf = confirm('Apakah anda yakin menghapus item ini ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/action_delete'; ?>',
				data: 'id=' + id,
				success: function(data) {
					var resp = JSON.parse(data);
					if (resp.code == 200) {
						notify(resp.status, resp.pesan, 'success');
						open_table('std');
					} else if (resp.code == 400) {
						notify(resp.status, resp.pesan, 'error');
						generateToken(resp.csrf);
					} else {
						notify('Error', 'Error tidak Diketahui', 'error');
						generateToken(resp.csrf);
					}
				}
			});
		}
	}

	function push_to_sap(id){
		var conf = confirm('Apakah anda yakin Push item ini ?');
		
		if(conf){
			$.blockUI({ overlayCSS: { backgroundColor: '#C0C0C0' } });
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/push_sap'; ?>',
				data: {'id' : id},
				success: function(resp) {
					
				if (resp.code == 200) {
					console.log(resp);
					notify(resp.status, resp.pesan, 'success');
					// send_sap(resp.data);
					send_sap(resp.data, resp.data_lb, resp.data_ov)
					// rollback_sap(resp.data[0].bom_kd);
					
					
				}  else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
					$.unblockUI();
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
					$.unblockUI();
				}
					
				}
			});
		}
	
	}

	function send_sap(params, items, item) {
		
		// $.unblockUI();
		var lines = [];
		var lines_kacangan = [];

		var plate_jenis_main = [];
		var plate_jenis_cut_size = [];

		var test =[];

		for (let index = 0; index < params.length; index++) {
			if(params[index].bomdetail_generatewo == 'T'){
				lines.push({"RouteStage": "60",
						"ItemType": "I",
						"ItemCode": params[index].partmain_nama, 
						"Quantity": "1",
						"Warehouse": "MGD051222003", 
						"IssueMethod": "B",
						"Ident" : "0"
					})
			}else{

				if(params[index].partdetailplate_jenis == 'main'){
					if (plate_jenis_main.filter(e => e.partmain_kd === params[index].partmain_kd && e.partdetail_kd === params[index].partdetail_kd).length > 0) {
						plate_jenis_main.filter(e => e.partmain_kd == params[index].partmain_kd && e.partdetail_kd == params[index].partdetail_kd)[0].CUT_SIZE +=  parseFloat(params[index].CUT_SIZE);
					/* vendors contains the element we're looking for */
					}else{
						plate_jenis_main.push({
								"partmain_kd": params[index].partmain_kd,
								"partdetail_kd": params[index].partdetail_kd,
								"rm_kode": params[index].rm_kode,
								"CUT_SIZE": parseFloat(params[index].CUT_SIZE)
							})
					}

				}else if(params[index].partdetailplate_jenis == 'cut_size'){
					if (plate_jenis_cut_size.filter(e => e.partmain_kd === params[index].partmain_kd && e.partdetail_kd === params[index].partdetail_kd).length > 0) {
						plate_jenis_cut_size.filter(e => e.partmain_kd == params[index].partmain_kd && e.partdetail_kd == params[index].partdetail_kd)[0].CUT_SIZE +=  parseFloat(params[index].CUT_SIZE);
					/* vendors contains the element we're looking for */
					}else{
						plate_jenis_cut_size.push({
								"partmain_kd": params[index].partmain_kd,
								"partdetail_kd": params[index].partdetail_kd,
								"rm_kode": params[index].rm_kode,
								"CUT_SIZE": parseFloat(params[index].CUT_SIZE)
							})
					}

				}else{
					if(!params[index].partdetailplate_kd){
						lines.push({"RouteStage": params[index].bagian_kd,
						"ItemType": "I",
						"ItemCode": params[index].rm_kode, 
						"Quantity": params[index].partdetail_qty,
						"Warehouse": "MGD051222003", 
						"IssueMethod": "B",
						"Ident" : params[index].partmain_kd,
					})
					}
					
				}
				
			}
			
		}

		for (let i = 0; i < plate_jenis_main.length; i++) {
						for (let j = 0; j < plate_jenis_cut_size.length; j++) {
							if(plate_jenis_main[i].partmain_kd == plate_jenis_cut_size[i].partmain_kd && plate_jenis_main[i].partdetail_kd == plate_jenis_cut_size[i].partdetail_kd){
									lines_kacangan.push({"RouteStage": '10',
										"ItemType": "I",
										"ItemCode": plate_jenis_main[i].rm_kode,
										"Quantity":  (plate_jenis_main[i].CUT_SIZE / plate_jenis_cut_size[i].CUT_SIZE ).toFixed(4),
										"Warehouse": "MGD051222003", 
										"IssueMethod": "B",
										"Ident" : plate_jenis_main[i].partdetail_kd,
									})
							}else{
								lines_kacangan.push({"RouteStage": '10',
										"ItemType": "I",
										"ItemCode": plate_jenis_main[i].rm_kode,
										"Quantity":  (plate_jenis_main[i].CUT_SIZE / plate_jenis_cut_size[i].CUT_SIZE ).toFixed(4),
										"Warehouse": "MGD051222003", 
										"IssueMethod": "B",
										"Ident" : plate_jenis_main[i].partdetail_kd,
									})
							}	
							
						}
					}

		if(items.length != 0){
				items = items.filter((value, index, self) =>
							index === self.findIndex((t) => (
								t.partmain_kd === value.partmain_kd && t.bagian_kd === value.bagian_kd
							))
							)


				for (let index = 0; index < items.length; index++) {
					lines.push({   "RouteStage": items[index].bagian_kd,
								"ItemType": "R",
								"ItemCode": 'LAB001', 
								"Quantity":  (items[index].partlabourcost_durasi * items[index].partlabourcost_qty).toFixed(4),
								"Warehouse": "MGD051222003", 
								"IssueMethod": "B",
								"Ident" : items[index].partmain_kd,
							})
				}

				}
				lines = lines.sort((a, b) => a.itemType > b.itemType ? 1 : -1);
				lines = lines.sort((a, b) => a.Ident > b.Ident ? 1 : -1);
				
				lines.sort((a, b) => a.RouteStage > b.RouteStage ? 1 : -1);
				// lines.sort((a, b) => b.Ident - a.Ident);

				if(item.length != 0){
					item = item.filter((value, index, self) =>
						index === self.findIndex((t) => (
							t.partmain_kd === value.partmain_kd && t.sap_kd === value.sap_kd
						))
						)

					for (let index = 0; index < item.length; index++) {
						lines.push({   
									"RouteStage":'60',
									"ItemType": "R",
									"ItemCode": item[index].sap_kd,
									"Quantity":  item[index].partoverhead_qty,
									"Warehouse": "MGD051222003", 
									"IssueMethod": "B",
									"Ident" : item[index].partmain_kd,
								})
					}

				}

				// lines = lines.filter((value, index, self) =>
				// 	index === self.findIndex((t) => (
				// 		t.Ident === value.Ident && t.RouteStage === value.RouteStage && t.ItemType === value.ItemType && t.Quantity === value.Quantity
				// 	))
				// 	)
				var uniqueArray = removeDuplicates(lines_kacangan, "Ident");
				Array.prototype.push.apply(lines,uniqueArray); 

				
				
				var gryfindor = lines.filter(function (wizard) {
									return wizard.ItemCode !== null;
								});
				var dataToSAP = {
						
					  "U_IDU_WEBID": params[0].bom_kd,
							"Code":  params[0].project_nopj + " " + params[0].project_itemcode,
							"Name": params[0].project_itemdesc,
							"Quantity": '1',
							"TreeType": 'P',
							"Warehouse": 'MGD051222003',
							"Lines": gryfindor
						}

			// console.log(plate_jenis_main);
			// console.log(plate_jenis_cut_size);
			// console.log(dataToSAP);
			// console.log(lines);
			// rollback_sap(dataToSAP.U_IDU_WEBID);
			// $.unblockUI();

		fetch("<?= url_api_sap33() ?>AddBOM", {
					method: 'POST', // or 'PUT'
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(dataToSAP),
					})
					.then((response) => response.json())
					.then((items) => {
						console.log('Success:', items);

						console.log(dataToSAP);
						if(items.ErrorCode == 0){
							notify ('200!', items.Message, 'success');
							$.unblockUI();
						    open_table();
							// window.location.reload();
						}else{
							rollback_sap(dataToSAP.U_IDU_WEBID);
							open_table();
							$.unblockUI();
							notify (items.ErrorCode, items.Message, 'error');
						}
					})
					.catch((error) => {
						rollback_sap(dataToSAP.U_IDU_WEBID);
						$.unblockUI();
						notify ('xxx', error, 'error');
					});


			
		// 	fetch("", {
		// 	method: 'POST', // or 'PUT'
		// 	headers: {
		// 		'Content-Type': 'application/json',
		// 	},
		// 	body: JSON.stringify(dataToSAP),
		// 	})
		// 	.then((response) => response.json())
		// 	.then((data) => {
		// 		console.log('Success:', data);
		// 		if(data.ErrorCode == 0 ){
		// 			console.log(dataToSAP);
		// 			console.log(params);
		// 			console.log(data.Message);
		// 			// open_table();
		// 			notify ('200! Sukses.', data.Message, 'success');
		// 			$.unblockUI()
		// 			window.location.reload();
		// 		}else{
		// 			console.log(dataToSAP);
		// 			console.log(params);
		// 			console.log(data.Message);
		// 			// delRollback(items.rowPO.po_kd)
		// 			$.unblockUI()
		// 			notify (data.ErrorCode + '. Gagal!', data.Message, 'error');
		// 		}
		// 	})
		// 	.catch((error) => {
		// 		$.unblockUI()
		// 		// delRollback(items.rowPO.po_kd)
		// 		notify ('000. Gagal!', error, 'error');
		// 		console.log(dataToSAP);
		// 		console.log(params);
		// 	});
		
	}

	function removeDuplicates(originalArray, prop) {
		var newArray = [];
		var lookupObject  = {};

		for(var i in originalArray) {
			lookupObject[originalArray[i][prop]] = originalArray[i];
		}

		for(i in lookupObject) {
			newArray.push(lookupObject[i]);
		}
		return newArray;
	}

	function rollback_sap(params) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/rollback_sap'; ?>',
				data: {'id' : params},
				success: function(resp) {
					if (resp.code == 200) {
						notify(resp.status, resp.pesan, 'success');
					} else if (resp.code == 401) {
						notify(resp.status, resp.pesan, 'error');
					} else if (resp.code == 400) {
						notify(resp.status, resp.pesan, 'error');
					} else {
						notify('Error', 'Error tidak Diketahui', 'error');
					}
				}
			});
			
	}



	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			styling: 'bootstrap3'
		});
		box_overlay('out');
	}

	function box_overlay(sts) {
		if (sts == 'in') {
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		} else if (sts == 'out') {
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}
</script>