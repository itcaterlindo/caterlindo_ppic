<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
    viewfiles_main('<?php echo $sts; ?>', '<?php echo $id; ?>');
    first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

    function viewfiles_main(sts = null, id) {
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url() . $class_link . '/viewfiles_main'; ?>',
            data: {
                id: id,
                st: sts
            },
            success: function(html) {
                $('#<?php echo $box_content_id; ?>').html(html);
                moveTo('idMainContent');
            }
        });
    }

    function viewfiles_form(bom_kd, id = null, sts = 'add') {
        event.preventDefault();
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url() . $class_link . '/viewfiles_form'; ?>',
            data: {
                bom_kd: bom_kd,
                id: id,
                sts: sts
            },
            success: function(html) {
                toggle_modal(sts, html);
            }
        });
    }

    function first_load(loader, content) {
        $('#' + loader).fadeOut(500, function(e) {
            $('#' + content).slideDown();
        });
    }

    function toggle_modal(modalTitle, htmlContent) {
        $('#idmodal').modal('toggle');
        $('.modal-title').text(modalTitle);
        $('#<?php echo $content_modal_id; ?>').slideUp();
        $('#<?php echo $content_modal_id; ?>').html(htmlContent);
        $('#<?php echo $content_modal_id; ?>').slideDown();
    }

    function moveTo(div_id) {
        $('html, body').animate({
            scrollTop: $('#' + div_id).offset().top - $('header').height()
        }, 1000);
    }

    function submitDataFiles(form_id) {
        event.preventDefault();
        var form = document.getElementById(form_id);
        var url = "<?php echo base_url() . $class_link; ?>/action_insert_files";

        $.ajax({
            url: url,
            type: "POST",
            data: new FormData(form),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                var resp = JSON.parse(data);
                console.log(data);
                if (resp.code == 200) {
                    toggle_modal('', '');
                    viewfiles_main('<?php echo $sts; ?>', '<?php echo $id; ?>');
                    notify(resp.status, resp.pesan, 'success');
                    $('.errInput').html('');
                } else if (resp.code == 401) {
                    $.each(resp.pesan, function(key, value) {
                        $('#' + key).html(value);
                    });
                    generateToken(resp.csrf);
                } else if (resp.code == 400) {
                    notify(resp.status, resp.pesan, 'error');
                    generateToken(resp.csrf);
                } else {
                    notify('Error', 'Error tidak Diketahui', 'error');
                    generateToken(resp.csrf);
                }
            }
        });
    }

    function hapus_data_file(id) {
        var conf = confirm('Apakah anda yakin ?');
        if (conf) {
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url() . $class_link . '/action_delete_file'; ?>',
                data: 'id=' + id,
                success: function(data) {
                    var resp = JSON.parse(data);
                    if (resp.code == 200) {
                        notify(resp.status, resp.pesan, 'success');
                        viewfiles_main('<?php echo $sts; ?>', '<?php echo $id; ?>');
                    } else if (resp.code == 400) {
                        notify(resp.status, resp.pesan, 'error');
                        generateToken(resp.csrf);
                    } else {
                        notify('Error', 'Error tidak Diketahui', 'error');
                        generateToken(resp.csrf);
                    }
                }
            });
        }
    }

    function hapus_data_detail(id) {
        var conf = confirm('Apakah anda yakin ?');
        if (conf) {
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url() . $class_link . '/action_delete_detail'; ?>',
                data: 'id=' + id,
                success: function(data) {
                    var resp = JSON.parse(data);
                    if (resp.code == 200) {
                        notify(resp.status, resp.pesan, 'success');
                        formdetail_table('<?php echo $sts; ?>', '<?php echo $id; ?>');
                    } else if (resp.code == 400) {
                        notify(resp.status, resp.pesan, 'error');
                        generateToken(resp.csrf);
                    } else {
                        notify('Error', 'Error tidak Diketahui', 'error');
                        generateToken(resp.csrf);
                    }
                }
            });
        }
    }

    function notify(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            type: type,
            styling: 'bootstrap3'
        });
    }

    function generateToken(csrf) {
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
    }
</script>