<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputDetail';
?>
<?php
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtSts', 'name' => 'txtSts', 'value' => $sts));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtbom_kd', 'name' => 'txtbom_kd', 'value' => isset($id) ? $id : null));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtbomdetail_kd', 'name' => 'txtbomdetail_kd', 'value' => isset($bomdetail['bomdetail_kd']) ? $bomdetail['bomdetail_kd'] : null));
?>
<style>
	.select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>
<div class="form-group">
	<label for='idtxtpartmain_kd' class="col-md-1 control-label">Part / Project</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrpartmain_kd"></div>
		<select name="txtpartmain_kd" id="idtxtpartmain_kd">
			<?php if (isset($bomdetail['partmain_kd'])) : ?>
				<option value="<?php echo $bomdetail['partmain_kd'] ?>" selected><?php echo $bomdetail['partmain_nama'] ?></option>
			<?php endif; ?>
		</select>
	</div>
</div>
<div class="form-group">
	<div class="col-md-2 control-label">
		<div class="checkbox">
			<label><input name="txtbomdetail_generatewo" id="idtxtbomdetail_generatewo" <?php if (isset($bomdetail['bomdetail_generatewo']) && $bomdetail['bomdetail_generatewo'] == 'T') echo 'checked'; ?> type="checkbox">Generate WO</label>
		</div>
	</div>
	<div class="col-md-2 control-label">
		<div class="checkbox">
			<label><input name="txtbomdetail_wogrouping" <?php if (isset($bomdetail['bomdetail_wogrouping']) && $bomdetail['bomdetail_wogrouping'] == 'T') echo 'checked'; ?> type="checkbox">Grouping WO</label>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-1 col-xs-12 col-sm-offset-1">
			<button type="submit" name="btnSubmit" onclick="submitDatadetail('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>
</div>
<hr>
<hr>
<?php echo form_close(); ?>

<script type="text/javascript">

</script>