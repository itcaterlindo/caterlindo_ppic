<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
?>
<ul class="todo-list list-inline" id="fileslist">
    <?php foreach ($files as $file) : ?>
        <li id="" style="width:260px;margin:10px;padding:8px;height:60px;">
            <div class="row">
                <div class="col-xs-2" style="text-align:center;vertical-align:middle;height:60px;">
                    <i class="fa fa-<?php echo icons($file['file_namafile']); ?>" style="font-size:36px;padding-top:3px;"></i>
                </div>

                <div class="col-xs-10">
                    <?php echo $file['file_namafile'] . "<br><small>" . $file['file_note'] . "</small>"; ?>

                    <div class="pull-right">
                        <?php if (cek_permission('BOMCUSTOM_FILES_VIEW')) : ?>
                            <a href="javascript:void(0)" onclick="window.open('<?php echo base_url() . $path . $file['file_uuid'] . '.' . explode('.', $file['file_namafile'])[1] ?>')" class="btn-right text-dark" title="Lihat"><i class="fa fa-eye"></i></a>&nbsp;
                        <?php endif; ?>
                        <?php if (cek_permission('BOMCUSTOM_FILES_UPDATE')) : ?>
                            <a href="javascript:void(0)" onclick="viewfiles_form('<?php echo $file['bom_kd']; ?>','<?php echo $file['file_kd'] ?>','edit')" class="btn-right text-dark" title="Edit"><i class="fa fa-pencil"></i></a>&nbsp;
                        <?php endif; ?>
                        <?php if (cek_permission('BOMCUSTOM_FILES_DELETE')) : ?>
                            <a href="javascript:void(0)" onclick="hapus_data_file('<?php echo $file['file_kd']; ?>')" class="btn-right text-red" title="Hapus"><i class="fa fa-trash-o"></i></a>&nbsp;
                        <?php endif; ?>
                    </div>

                </div>
            </div>
        </li>
    <?php endforeach; ?>
</ul>