<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	formdetail_main('<?php echo $sts; ?>', '<?php echo $id; ?>');
	formdetail_table('<?php echo $sts; ?>', '<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function formdetail_main(sts, id, bomdetail_kd = null) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/formdetail_main'; ?>',
			data: {
				'sts': sts,
				'id': id,
				'bomdetail_kd': bomdetail_kd
			},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
				render_partmain();
				moveTo('idMainContent');
			}
		});
	}

	function formdetail_table(sts, id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/formdetail_table'; ?>',
			data: {
				'sts': sts,
				'id': id
			},
			success: function(html) {
				$('#<?php echo $box_content2_id; ?>').html(html);
			}
		});
	}

	
	function edit_data_detail(id) {
		formdetail_main('edit', 'bom_kd', id);
	}

	$(document).off('select2:select', '#idtxtpartmain_kd').on('select2:select', '#idtxtpartmain_kd', function(e) {
		var data = e.params.data;
		if (data.partjenis_generatewo == 'T') {
			$('#idtxtbomdetail_generatewo').prop('checked', true);
		} else {
			$('#idtxtbomdetail_generatewo').prop('checked', false);
		}
	});

	function render_partmain() {
		$("#idtxtpartmain_kd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 2,
			ajax: {
				url: '<?php echo base_url() . $class_link ?>/get_dropdown_partmain',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function(params) {
					return {
						paramPartmain: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function render_select2(valClass) {
		$('.' + valClass).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
		});
	}

	function submitDatadetail(form_id) {
		event.preventDefault();
		var form = document.getElementById(form_id);
		var url = "<?php echo base_url() . $class_link; ?>/action_insert_detail";

		$.ajax({
			url: url,
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				var resp = JSON.parse(data);
				console.log(data);
				if (resp.code == 200) {
					formdetail_main('<?php echo $sts; ?>', '<?php echo $id; ?>');
					formdetail_table('<?php echo $sts; ?>', '<?php echo $id; ?>');
					notify(resp.status, resp.pesan, 'success');
					generateToken(resp.csrf);
					$('.errInput').html('');
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
			}
		});
	}

	function hapus_data_detail(id) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/action_delete_detail'; ?>',
				data: 'id=' + id,
				success: function(data) {
					var resp = JSON.parse(data);
					if (resp.code == 200) {
						notify(resp.status, resp.pesan, 'success');
						formdetail_table('<?php echo $sts; ?>', '<?php echo $id; ?>');
					} else if (resp.code == 400) {
						notify(resp.status, resp.pesan, 'error');
						generateToken(resp.csrf);
					} else {
						notify('Error', 'Error tidak Diketahui', 'error');
						generateToken(resp.csrf);
					}
				}
			});
		}
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			styling: 'bootstrap3'
		});
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}
</script>