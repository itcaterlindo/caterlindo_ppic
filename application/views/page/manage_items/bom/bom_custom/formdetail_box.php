<?php
defined('BASEPATH') or exit('No direct script access allowed!');

/* --SET DEFAULT PROPERTY UNTUK BOX-- */
$js_file = 'formdetail_js';
$box_type = 'Form';
$box_title = 'Form Detail BoM Custom';
$data['master_var'] = 'BOM';
$data['class_link'] = $class_link;
$data['box_id'] = 'id'.$box_type.'Box'.$data['master_var'];
$data['btn_hide_id'] = 'id'.$box_type.'BtnBoxHide'.$data['master_var'];
$data['btn_add_id'] = 'id'.$box_type.'BtnBoxAdd'.$data['master_var'];
$data['btn_remove_id'] = 'id'.$box_type.'BtnBoxRemove'.$data['master_var'];
$data['box_alert_id'] = 'id'.$box_type.'BoxAlert'.$data['master_var'];
$data['box_loader_id'] = 'id'.$box_type.'BoxLoader'.$data['master_var'];
$data['box_content_id'] = 'id'.$box_type.'BoxContent'.$data['master_var'];
$data['box_overlay_id'] = 'id'.$box_type.'BoxOverlay'.$data['master_var'];
/* --END OF BOX DEFAULT PROPERTY-- */

/* --SET VARIABEL TAMBAHAN DISINI-- */
$data['box_content2_id'] = 'id'.$box_type.'BoxContent2'.$data['master_var'];
$data['id'] = $id;
$data['sts'] = $sts;
if (isset($master)) {
    extract($master);
}
/* --END OF CUSTOM PROPERTY-- */

/* --SET BUTTONS DISINI, NILAI "FALSE" UNTUK TIDAK TAMPIL, NILAI "TRUE" UNTUK MENAMPILKAN BUTTON-- */
$btn_hide = TRUE;
$btn_add = FALSE;
$btn_remove = FALSE;
/* --END OF BUTTONS SETUP-- */
?>

<div class="box box-primary" id="<?php echo $data['box_id']; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_title; ?></h3>
		<div class="box-tools pull-right">
			<?php
			if ($btn_add) :
				?>
				<button class="btn btn-box-tool" id="<?php echo $data['btn_add_id']; ?>" data-toggle="tooltip" title="Tambah Data">
					<i class="fa fa-plus"></i>
				</button>
				<?php
			endif;
			if ($btn_hide) :
				?>
				<button class="btn btn-box-tool btn-collapse" id="<?php echo $data['btn_hide_id']; ?>" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
					<i class="fa fa-minus"></i>
				</button>
				<?php
			endif;
			if ($btn_remove) :
				?>
				<button class="btn btn-box-tool btn-remove" id="<?php echo $data['btn_remove_id']; ?>" data-widget="remove" data-toggle="tooltip" title="Close">
					<i class="fa fa-times"></i>
				</button>
				<?php
			endif;
			?>
		</div>
	</div>
	<div class="box-body">
        <div class="row invoice-info">
            <div class="col-sm-6 invoice-col">
                <b>Item Product :</b> <?php echo isset($project_itemcode) ? $project_itemcode : $item_code; ?><br>
                <b>Jenis :</b> 
                <?php 
                if (isset($bom_jenis)) {
                    if ($bom_jenis == 'std'){
                        echo 'Standart';
                    }else {
                        echo 'Custom';
                    }
                }  ?>
                <br>
                <b>No PJ : </b> <?php echo $project_nopj; ?>
            </div>
            <div class="col-sm-6 invoice-col">
                <b>Deskripsi :</b> <?php echo isset($project_itemdesc) ? $project_itemdesc : $deskripsi_barang; ?><br>
                <b>Dimensi :</b> <?php echo isset($project_itemdimension) ? $project_itemdimension : $dimensi_barang; ?><br>
            </div>
        </div>
        <hr>
		<div id="<?php echo $data['box_alert_id']; ?>"></div>
		<div id="<?php echo $data['box_loader_id']; ?>" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="<?php echo $data['box_content_id']; ?>"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="<?php echo $data['box_content2_id']; ?>"></div>
			</div>
		</div>
	</div>

    <div class="box-footer">
        <div class="col-md-12">
            <button onclick="window.history.back()" class="btn btn-success btn-sm pull-right">
                <i class="fa fa- fa-check-square-o"></i> Selesai
            </button>
        </div>
	</div>

	<div class="overlay" id="<?php echo $data['box_overlay_id']; ?>" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
	<?php $this->load->view('page/'.$class_link.'/'.$js_file, $data); ?>
</div>