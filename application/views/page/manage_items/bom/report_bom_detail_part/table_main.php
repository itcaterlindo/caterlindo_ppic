<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body table-responsive no-padding">
<table id="idTable" class="table table-bordered table-striped table-hover display nowrap" style="width:100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:10%; text-align:center;">Item_code</th>
		<th style="width:22%; text-align:left;">Top</th>
		<th style="width:22%; text-align:left;">Solid/Cb</th>
		<th style="width:22%; text-align:left;">Pipa</th>
		<th style="width:22%; text-align:left;">Packing</th>
		<th style="width:22%; text-align:left;">Lain Lain</th>
		<th style="width:22%; text-align:center;">Total Coasting</th>
	</tr>
	</thead>
	<tbody id="apn_tbdy">

  	</tbody>
</table>
</div>
<script type="text/javascript">
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/ready_data'; ?>',
				success: function(html) {
					console.log(html);
					var items = html.item;
					var coasting = html.coasting;

					// const merge = (items, coasting) => {

					// 	return items.map( (x) => {
					// 	const y = coasting.find( item => x.bom_kd === item.bom_kd);
					// 	if (y) {
					// 		return Object.assign({},x,y);
					// 	} else
					// 		return x
					// 	}).concat(coasting.filter(item => items.every( x => x.bom_kd !== item.bom_kd)));

					// 	}

					// 	arr3 = merge(items, coasting)
						create_table(coasting)

			}});

		function create_table(params) {
			
			var t = $('#idTable').DataTable({
					dom: 'Bfrtip',
					buttons: [
						
						'copy', 'csv', 'excel', 'pdf'
					],
					scrollX: true,
					"data": params,
					"pageLength": 50,
					"columns": [
						{data: 'SrNo',
							render: function (data, type, row, meta) {
									return meta.row + 1;
							}
						},
						{ "data": "item_code" },
						{"data": 'TOP',
							render: function (data, type, row, meta) {
								var rp = new Intl.NumberFormat("en-US").format(data);
									return rp;
							}
						},
						{"data": 'SOLID/CB',
							render: function (data, type, row, meta) {
								var rp = new Intl.NumberFormat("en-US").format(data);
									return rp;
							}
						},
						{"data": 'PIPA',
							render: function (data, type, row, meta) {
								var rp = new Intl.NumberFormat("en-US").format(data);
									return rp;
							}
						},
						{"data": 'PACKING',
							render: function (data, type, row, meta) {
								var rp = new Intl.NumberFormat("en-US").format(data);
									return rp;
							}
						},
						{"data": 'LAIN-LAIN',
							render: function (data, type, row, meta) {
								var rp = new Intl.NumberFormat("en-US").format(data);
									return rp;
							}
						},
						{"data": 'total_all',
							render: function (data, type, row, meta) {
								var rp = new Intl.NumberFormat("en-US").format(data);
									return rp;
							}
						},
					],
				});
				t.on( 'draw.dt', function () {
							var PageInfo = $('#example').DataTable().page.info();
							t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
								cell.innerHTML = i + 1 + PageInfo.start;
							} );
				} );
				}

</script>