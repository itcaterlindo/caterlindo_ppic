<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputMaster';

if(isset($rowData)) {
	extract($rowData);
	$id = $bom_kd;
	$BarangSelected = $kd_barang == NULL ? $rm_kd : $kd_barang;
	$JenisBOMSelected = $bom_jenis;
	$SOSelected = $kd_msalesorder;
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtStsMaster', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtbom_kd', 'name'=> 'txtbom_kd', 'value' => isset($id) ? $id: null ));

?>
<div class="row">
	
	<div class="row" id="idRowStd">
		<div class="col-md-12">
			<div class="form-group">
				<label for='idtxtbarang_kd' class="col-md-2 control-label">Item Code</label>
				<div class="col-sm-4 col-xs-12">
					<div class="errInput" id="idErrbarang_kd"></div>
					<?php echo form_dropdown('txtbarang_kd', $opsiBarang, isset($BarangSelected)? $BarangSelected : '', array('class'=> 'form-control select2', 'id'=> 'idtxtbarang_kd'));?>
				</div>	
			</div>	
		</div>
	</div>

    <div class="form-group">
		<label for="idtxtbom_ket" class="col-md-2 control-label">Keterangan</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrbom_ket"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control ttmaster-input', 'name'=> 'txtbom_ket', 'id'=> 'idtxtbom_ket', 'placeholder' =>'Keterangan', 'rows' => '2', 'value'=> isset($bom_ket) ? $bom_ket: null ));?>
		</div>
	</div>

</div>

<hr>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-1 col-xs-12 col-sm-offset-10">
			<button type="submit" name="btnSubmit" onclick="submitDataMaster()" class="btn btn-primary btn-sm">
				<i class="fa fa-arrow-circle-right"></i> Simpan
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>
<script type="text/javascript">
</script>