<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
extract($dataMaster);
if($item_code == NULL){
	$item_code = $rm_kd;
	$deskripsi_barang = $rm_deskripsi;
	$dimensi_barang = $rm_spesifikasi;
}
?>
<style>
	.select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>
<div class="row invoice-info">
	<div class="col-sm-6 invoice-col">
		<b>Item Product :</b> <?php echo isset($project_itemcode) ? $project_itemcode : $item_code; ?><br>
		<b>Jenis :</b>
		<?php
		if (isset($bom_jenis)) {
			if ($bom_jenis == 'std') {
				echo 'Standart';
			} else {
				echo 'Custom';
			}
		}  ?>
		<br>
	</div>
	<div class="col-sm-6 invoice-col">
		<b>Deskripsi :</b> <?php echo isset($project_itemdesc) ? $project_itemdesc : $deskripsi_barang; ?><br>
		<b>Dimensi :</b> <?php echo isset($project_itemdimension) ? $project_itemdimension : $dimensi_barang; ?><br>
	</div>
</div>
<hr>
<?php
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtSts', 'name' => 'txtSts', 'value' => $sts));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtbom_kd', 'name' => 'txtbom_kd', 'value' => isset($id) ? $id : null));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtbomdetail_kd', 'name' => 'txtbomdetail_kd', 'value' => isset($bomdetail['bomdetail_kd']) ? $bomdetail['bomdetail_kd'] : null));

?>
<div class="form-group">
	<label for='idtxtpartmain_kd' class="col-md-1 control-label">Part</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrpartmain_kd"></div>
		<select name="txtpartmain_kd" id="idtxtpartmain_kd">
			<?php if (isset($bomdetail['partmain_kd'])) : ?>
				<option value="<?php echo $bomdetail['partmain_kd'] ?>" selected><?php echo $bomdetail['partmain_nama'] ?></option>
			<?php endif; ?>
		</select>
	</div>
</div>
<div class="form-group">
	<div class="col-md-2 control-label">
		<div class="checkbox">
			<label><input name="txtbomdetail_generatewo" id="idtxtbomdetail_generatewo" <?php if (isset($bomdetail['bomdetail_generatewo']) && $bomdetail['bomdetail_generatewo'] == 'T') echo 'checked'; ?> type="checkbox">Generate WO</label>
		</div>
	</div>
	<div class="col-md-2 control-label">
		<div class="checkbox">
			<label><input name="txtbomdetail_wogrouping" <?php if (isset($bomdetail['bomdetail_wogrouping']) && $bomdetail['bomdetail_wogrouping'] == 'T') echo 'checked'; ?> type="checkbox">Grouping WO</label>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-1 col-xs-12 col-sm-offset-1">
			<button type="submit" name="btnSubmit" onclick="submitData()" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>
</div>
<hr>
<?php echo form_close(); ?>