<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body table-responsive no-padding">
<table id="idTable" class="table table-bordered table-striped table-hover display nowrap" style="width:100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:10%; text-align:center;">Item_code</th>
		<th style="width:22%; text-align:left;">Harga Top</th>
		<th style="width:22%; text-align:left;">Total Top</th>
		<th style="width:22%; text-align:left;">Harga Solid/CB</th>
		<th style="width:22%; text-align:left;">Total Solid/CB</th>
		<th style="width:22%; text-align:left;">Harga Lain-Lain</th>
		<th style="width:22%; text-align:left;">Total Lain-Lain</th>
	</tr>
	</thead>
	<tbody id="apn_tbdy">

  	</tbody>
</table>
</div>
<script type="text/javascript">
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/ready_data'; ?>',
				success: function(array) {
					//var items = html.item;

					const key_a = 'TOP';
					const key_b = 'SOLID/CB';
					const key_c = 'LAIN-LAIN';

					const key_d = 'part_costing_harga_plate_TOP';
					const key_e = 'part_costing_harga_plate_SOLID/CB';
					const key_f = 'part_costing_harga_plate_LAIN-LAIN';

					const vl = '';

					for (let i = 0; i < array.length; i++) {
						if (!array[i].hasOwnProperty(key_a)) {

							array[i][key_a] = vl;
							array[i][key_d] = vl;

						}
						 if(!array[i].hasOwnProperty(key_b)){

							array[i][key_b] = vl;
							array[i][key_e] = vl;

						}
						if(!array[i].hasOwnProperty(key_c)){

							array[i][key_c] = vl;
							array[i][key_f] = vl;

						}
					}
						create_table(array)

			}});

		function create_table(params) {
			//console.log(params)
			var t = $('#idTable').DataTable({
					dom: 'Bfrtip',
					buttons: [
						
						'copy', 'csv', 'excel', 'pdf'
					],
					scrollX: true,
					"data": params,
					"pageLength": 50,
					"columns": [
						{data: 'SrNo',
							render: function (data, type, row, meta) {
									return meta.row + 1;
							}
						},
						{ "data": "item_code" },
						{"data": 'part_costing_harga_plate_TOP',
							render: function (data, type, row, meta) {
								var rp = new Intl.NumberFormat("en-US").format(data);
									return rp;
							}
						},
						{"data": 'TOP',
							render: function (data, type, row, meta) {
								var rp = new Intl.NumberFormat("en-US").format(data);
									return rp;
							}
						},
						{"data": 'part_costing_harga_plate_SOLID/CB',
							render: function (data, type, row, meta) {
								var rp = new Intl.NumberFormat("en-US").format(data);
									return rp;
							}
						},
						{"data": 'SOLID/CB',
							render: function (data, type, row, meta) {
								var rp = new Intl.NumberFormat("en-US").format(data);
									return rp;
							}
						},
						{"data": 'part_costing_harga_plate_LAIN-LAIN',
							render: function (data, type, row, meta) {
								var rp = new Intl.NumberFormat("en-US").format(data);
									return rp;
							}
						},
						{"data": 'LAIN-LAIN',
							render: function (data, type, row, meta) {
								var rp = new Intl.NumberFormat("en-US").format(data);
									return rp;
							}
						},
					],
				});
				t.on( 'draw.dt', function () {
							var PageInfo = $('#example').DataTable().page.info();
							t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
								cell.innerHTML = i + 1 + PageInfo.start;
							} );
				} );
			 }

</script>