<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormUbahstate';

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtbompenawaran_kd', 'name'=> 'txtbompenawaran_kd', 'value' => $bompenawaran_kd ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtbomstate_kd', 'name'=> 'txtbomstate_kd', 'value' => isset($bomstate_kd) ? $bomstate_kd: null ));

?>

<div class="col-md-12">

    <div class="form-group">
        <label for="idtxtbomstate_note" class="col-md-2 control-label">Keterangan</label>
        <div class="col-sm-10 col-xs-12">
            <div class="errInput" id="idErrbomstate_note"></div>
            <?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtbomstate_note', 'id'=> 'idtxtbomstate_note', 'placeholder' =>'Keterangan', 'rows' => '3' ) );?>
        </div>
    </div>

</div>

<div class="col-md-12">
	<button type="submit" name="btnSubmit" id="idbtnSubmitState" onclick="submitState('<?= $form_id ?>')" class="btn btn-primary btn-sm pull-right">
		<i class="fa fa-save"></i> Simpan
	</button>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
</script>