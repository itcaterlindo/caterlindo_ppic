<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<table id="idTable" class="table table-bordered table-striped table-hover display responsive" style="width:100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;">No</th>
		<th style="width:10%; text-align:center;">Opsi</th>
		<th style="width:10%; text-align:center;">Nama Part</th>
		<th style="width:7%; text-align:center;">Versi</th>
		<th style="width:20%; text-align:center;">Note</th>
		<th style="width:7%; text-align:center;">State</th>
		<th style="width:15%; text-align:center;">Harga Costing</th>
		<th style="width:15%; text-align:center;">Harga Approved</th>
		<th style="width:15%; text-align:center;">Last Update Harga</th>
		<th style="width:15%; text-align:center;">Harga Approved</th>
	</tr>
	</thead>
    <tbody>
    <?php 
    $no = 1;
    foreach ($result as $r): ?>
    <tr>
        <td class="dt-center"><?= $no?></td>
        <td><?php 
			$btns = array();
			if (cek_permission('BOMPENAWARAN_VIEW_HARGACOSTING')) {
				$btns[] = get_btn(array('title' => 'Lihat Item', 'icon' => 'search', 'onclick' => 'preview_item_detail(\''.$r['part_kd'].'\')'));
			}
            if ($rowBom['bomstate_nama'] == 'checked') {
                $btns[] = get_btn(array('title' => 'Update Harga', 'icon' => 'pencil', 'onclick' => 'update_harga(\''.$r['bompenawaran_kd'].'\',\''.$r['part_kd'].'\')'));
            }
            $btn_group = group_btns($btns);
            echo $btn_group;
        ?></td>
        <td><?= $r['partmain_nama'] ?></td>
        <td><?= $r['part_versi'] ?></td>
        <td><?= $r['partmain_note'] ?></td>
        <td><?= build_span($r['partstate_label'], $r['partstate_nama']) ?></td>
        <td><?= custom_numberformat($r['part_hargacosting'], 2, 'IDR') ?></td>
        <td><?= custom_numberformat($r['part_hargaapproved'], 2, 'IDR') ?></td>
        <td><?= format_date($r['part_hargacosting_update'], 'Y-m-d H:i:s') ?></td>
        <td><?= $r['part_hargaapproved'] ?></td>
    </tr>
    <?php 
    $no++;
    endforeach; ?>
    </tbody>
</table>

<script type="text/javascript">
    let table = $("#idTable").DataTable({
        "dom": "Bfrtip",
        "paging": false,
        "ordering": false,
        "searching": false,
        "language" : {
            "lengthMenu" : "Tampilkan _MENU_ data",
            "zeroRecords" : "Maaf tidak ada data yang ditampilkan",
            "info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
            "infoFiltered": "(difilter dari _MAX_ total data)",
            "infoEmpty" : "Tidak ada data yang ditampilkan",
            "search" : "Cari :",
            "loadingRecords": "Memuat Data...",
            "processing":     "Sedang Memproses...",
            "paginate": {
                "first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
                "last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
                "next":       '<span class="glyphicon glyphicon-forward"></span>',
                "previous":   '<span class="glyphicon glyphicon-backward"></span>'
            }
        },
        "columnDefs": [
            <?php
            $vCosting = '{"visible": false, "targets": 6},';
            if (cek_permission('BOMPENAWARAN_VIEW_HARGACOSTING')) {
                $vCosting = '{"visible": true, "targets": 6},';
            }
            echo $vCosting;
            ?>
            {"visible": false, "targets": 9},
        ],
        "buttons" : [{
            "extend" : "excel",
            "title" : "BoM Penawaran Detail",
            "exportOptions": {
                "columns": [ 0,2,3,4,5,9,8 ]
            }
        }],
    });
</script>