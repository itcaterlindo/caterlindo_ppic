<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
    view_detailtablecosting_main ('<?= $part_kd ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#<?php echo $box_loader_id; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
	});

    function changeFontSize() {
        $("#idtablemain").css({
            'fontSize': '100%'
        });
    }

    function renderdt() {
        $("#idtablemain").DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"ordering": false,
			"searching": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"title" : "Part Costing",
				"exportOptions": {
					"columns": [ 0,1,2,3,4,5,6,7,8,9 ]
				}
			}],
		});
    }

    function view_detailtablecosting_main (part_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link_partcosting.'/view_detailtablecosting_main'; ?>',
			data: {'part_kd' : part_kd},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
                changeFontSize();
                renderdt();
			}
		});
    }

    // Untuk detail pdf
	// function open_view_detail_box(part_kd){
	// 	$.ajax({
	// 		type: 'GET',
	// 		url: '<?php //echo base_url().$class_link.'/view_detail_box'; ?>',
	// 		data: {'part_kd' : part_kd},
	// 		success: function(html) {
	// 			$('#idMainContent').prepend(html);
	// 			moveTo('idMainContent');
	// 		}
	// 	});
	// }

</script>