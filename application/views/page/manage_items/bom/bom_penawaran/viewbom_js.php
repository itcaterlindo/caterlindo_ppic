<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	viewbom_tablemain ('<?= $id ?>');
	viewbom_detaillog_main('<?= $id ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function viewbom_tablemain (id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/viewbom_tablemain'; ?>',
			data: {id: id},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
				moveTo('idMainContent');
			}
		});
	}

	function update_harga (bompenawaran_kd, part_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/formupdateharga_main'; ?>',
			data: {bompenawaran_kd: bompenawaran_kd, part_kd: part_kd},
			success: function(html) {
				toggle_modal('Form Update Harga', html);
			}
		});
	}

	function ubah_state (bompenawaran_kd, bomstate_kd) {
        $.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/formubahstate_main'; ?>',
			data: {bompenawaran_kd : bompenawaran_kd, bomstate_kd: bomstate_kd},
			success: function(html) {
				toggle_modal('Form Ubah State BoM', html);
			}
		});
    }

	function preview_item_detail(part_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/viewcosting_box'; ?>',
			data: {part_kd : part_kd},
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitDataHarga (form_id) {
		event.preventDefault();
		var form = document.getElementById(form_id);
		var url = "<?php echo base_url().$class_link; ?>/action_update_hargapart";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					toggle_modal('', '');
					viewbom_tablemain ('<?= $id ?>');
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function submitState (form_id) {
		event.preventDefault();
		$('#idbtnSubmitState').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtnSubmitState').attr('disabled', true);

		var form = document.getElementById(form_id);

		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_ubahstate" ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				console.log(resp);
				if(resp.code == 200){
					
					if(resp.data_message.send_email){
						sendNotifMail(resp.data_message.data_message_json)
					}else{
						window.location.reload();
					}
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
				}else if (resp.code == 400){
					generateToken (resp.csrf);
				}else{
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

		
	function sendNotifMail(resp) {

		var data = {
				service_id: 'service_89wfszf',
				template_id: 'bomxx',
				user_id: 'user_gqom4wS5NcBXsu8XDttVI',
				template_params: {
				bom_kode: resp.subject,
				note: resp.keterangan,
				bomdetail:resp.url,
				cc: resp.cc,
				to: resp.to
			}
		};


		//console.log(data)//
		$.ajax('https://api.emailjs.com/api/v1.0/email/send', {
		type: 'POST',
		data: JSON.stringify(data),
		contentType: 'application/json'
			}).done(function() {
				//notify ('OK! 200.', 'Berhasil.', 'success');
				window.location.reload();
			}).fail(function(error) {
				alert('Oops… ' + JSON.stringify(error));
			})
		}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }

	function viewbom_detaillog_main(bompenawaran_kd){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/viewbom_detaillog_main'; ?>',
			data: {'bompenawaran_kd' : bompenawaran_kd},
			success: function(html) {
				$('#idlogmain').html(html);
			}
		});
	}

</script>