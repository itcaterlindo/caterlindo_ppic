<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form_master('<?php echo $sts; ?>','<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#<?php echo $box_loader_id; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
		open_table('std');
		first_load('<?php echo 'idTableBoxLoader'.$master_var; ?>', '<?php echo 'idTableBoxContent'.$master_var; ?>');
	});

	$(document).off('change', '#idtxtkd_customer').on('change', '#idtxtkd_customer', function() {
		var kd_customer = $(this).val();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/get_jeniscustomer'; ?>',
			data: {'kd_customer': kd_customer},
			success: function(data) {
				var resp = JSON.parse(data);
				$('#idtxtkd_jenis_customer').val(resp.data.jenis_customer_kd).trigger('change');
			}
		});
	});

	function open_form(sts, id) {
		box_overlay_form('in');
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_main'; ?>',
			data: {'sts': sts, 'id' : id},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
				render_select2('select2', sts);
				moveTo('idMainContent');
				box_overlay_form('out');
			}
		});
	}

	function open_form_master(sts, id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_master_main'; ?>',
			data: {'sts': sts, 'id' : id},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
				render_select2('select2');
				moveTo('idMainContent');
			}
		});
	}

	function open_table_detail(sts, id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/table_detail_main'; ?>',
			data: {'sts': sts, 'id' : id},
			success: function(html) {
				$('#<?php echo $box_content_detail_id; ?>').html(html);
			}
		});
	}

	$(document).off('change', '#idtxtkd_msalesorder').on('change', '#idtxtkd_msalesorder', function () {
		get_item_so(this.value, '');
	});

	function get_item_so(kd_msalesorder, selected) {
		var optGdg = $('#idtxtproject_kd');
		$('#idtxtproject_kd option').remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/get_item_so'; ?>',
			data: {'kd_msalesorder': kd_msalesorder},
			success: function(resp) {
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optGdg.append($('<option></option>').val(text.project_kd).html(text.project_nopj+' | '+text.project_itemcode+' / '+text.project_itemdesc+'/'+text.project_itemdimension));
					});
				}
				$('#idtxtproject_kd').val(selected);
			}
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function render_select2(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			allowClear: true,
		});
	}

	function remove_form_box(){
		$('#<?php echo $box_id; ?>').remove();
		open_table('std');
	}

	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	// function resetFormMain(){
	// 	$('#idtxtpart_kd').val('').trigger('change');
	// }

	function submitData(form_id) {
		box_overlay_form('in');
		event.preventDefault();
		var form = document.getElementById(form_id);
		var url = "<?php echo base_url().$class_link; ?>/action_insert";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					var id = $('#idtxtbompenawaran_kd').val();
					var sts = $('#idtxtSts').val();
					open_form(sts, id);
					open_table_detail(sts, id);
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
				box_overlay_form('out');
			} 	        
		});
	}

	function submitDataMaster(form_id){
		event.preventDefault();
		var form = document.getElementById(form_id);
		// edit atau add
		var sts =  $('#idtxtStsMaster').val();
		var url = "<?php echo base_url().$class_link; ?>/action_master_insert";
		if (sts == 'edit'){
			url = "<?php echo base_url().$class_link; ?>/action_master_update";
		}		

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					open_form(sts, resp.data.bompenawaran_kd);
					open_table_detail(sts, resp.data.bompenawaran_kd);
					generateToken (resp.csrf);
					$('.errInput').html('');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	// function hapus_data_detail(id){
	// 	var conf = confirm('Apakah anda yakin menghapus item ini ?');
	// 	if (conf){
	// 		$.ajax({
	// 			type: 'GET',
	// 			url: '<?php //echo base_url().$class_link.'/action_delete_detail'; ?>',
	// 			data: 'id='+id,
	// 			success: function(data) {
	// 				var resp = JSON.parse(data);
	// 				if(resp.code == 200){
	// 					notify (resp.status, resp.pesan, 'success');
	// 					var sts =  $('#idtxtSts').val();
	// 					var id = $('#idtxtbom_kd').val();
	// 					open_table_detail(sts, id);
	// 				}else if (resp.code == 400){
	// 					notify (resp.status, resp.pesan, 'error');
	// 					generateToken (resp.csrf);
	// 				}else{
	// 					notify ('Error', 'Error tidak Diketahui', 'error');
	// 					generateToken (resp.csrf);
	// 				}
	// 			}
	// 		});
	// 	}
	// }

	function box_overlay_form(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

</script>