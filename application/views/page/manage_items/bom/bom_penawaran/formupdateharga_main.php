<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputHarga';
?>
<div class="col-md-12">
    <div class="row invoice-info">
        <div class="col-sm-6 invoice-col">
            <b>Part Nama :</b> <?php echo isset($header['partmain_nama']) ? $header['partmain_nama'] : '-'; ?><br>
            <b>Part Versi :</b> <?php echo isset($header['part_versi']) ? $header['part_versi'] : '-'; ?> <br>
        </div>
        <div class="col-sm-6 invoice-col">
            <b>Status :</b> <?php echo isset($header['partstate_nama']) ? build_span($header['partstate_label'], $header['partstate_nama']) : '-'; ?><br>
        </div>
    </div>
<hr>
</div>

<?php
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpart_kd', 'name'=> 'txtpart_kd', 'value' => isset($part_kd) ? $part_kd: null ));
echo form_input(array('type'=>'hidden', 'name'=> 'txtpart_hargacosting', 'value'=> isset($part_hargacosting) ? $part_hargacosting: null ));
?>

<div class="col-md-12">

    <table id="idTable" border="1" style="width:100%;">
        <thead>
        <tr>
            <th style="text-align:center;">Keterangan</th>
            <th style="text-align:center;">Total</th>
        </tr>
        </thead>
        <tbody>
            <?php
            $part_hargacosting = isset($part_hargacosting) ? $part_hargacosting: 0;
            $jeniscustomer_marginbom = isset($jenis_customer->jeniscustomer_marginbom) ? $jenis_customer->jeniscustomer_marginbom : 0;
            $hargaMargin = (float) $part_hargacosting * $jeniscustomer_marginbom / 100;
            $jeniscustomer_discbom = isset($jenis_customer->jeniscustomer_discbom) ? $jenis_customer->jeniscustomer_discbom : 0;
            $hargaDisc = (float) ((100 - $jeniscustomer_discbom) / 100) * $hargaMargin;
            $hargaPembulatan = 0;
            if (!empty($hargaDisc)){
                $hargaPembulatan = ceil($hargaDisc / 1000) * 1000;
            }
            ?>
            <tr>
                <td>Harga Costing</td>
                <td style="text-align: right;"><?php echo custom_numberformat($part_hargacosting, 2, 'IDR') ?></td>
            </tr>
            <tr>
                <td>Margin (<?php echo $jeniscustomer_marginbom ?> %)</td>
                <td style="text-align: right;"> <?php echo custom_numberformat($hargaMargin, 2, 'IDR') ?> </td>
            </tr>
            <tr>
                <td>Diskon (<?php echo $jeniscustomer_discbom ?> %)</td>
                <td style="text-align: right;"> <?php echo custom_numberformat($hargaDisc, 2, 'IDR'); ?> </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Pembulatan</td>
                <td style="text-align: right; font-weight: bold;"> <?php echo custom_numberformat($hargaPembulatan, 2, 'IDR'); ?> </td>
            </tr>
        </tbody>
    </table>
    <br>
    <div class="form-group">
        <label for='idtxtpart_hargaapproved' class="col-md-2 control-label">Harga Approved</label>
        <div class="col-sm-7 col-xs-12">
            <div class="errInput" id="idErrpart_hargaapproved"></div>
            <?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtpart_hargaapproved', 'id'=> 'idtxtpart_hargaapproved', 'placeholder' =>'Harga Approved', 'value'=> !empty($hargaPembulatan) ? $hargaPembulatan: null ));?>
        </div>	
    </div>

	<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitDataHarga('<?= $form_id ?>')" class="btn btn-primary btn-sm pull-right">
		<i class="fa fa-save"></i> Simpan
	</button>

</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	
</script>