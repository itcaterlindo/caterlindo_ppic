<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
extract($dataMaster);
?>
<div class="row invoice-info">
	<div class="col-sm-6 invoice-col">
		<b>Customer :</b> <?php echo isset($nm_customer) ? $nm_customer : '-'; ?><br>
		<b>Keterangan :</b> <?php echo isset($bompenawaran_note) ? $bompenawaran_note : '-'; ?> <br>
	</div>
	<div class="col-sm-6 invoice-col">
		<b>Status :</b> <?php echo isset($bomstate_kd) ? build_span($bomstate_label, $bomstate_nama) : '-'; ?><br>
	</div>
</div>
<hr>
<?php
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtbompenawaran_kd', 'name'=> 'txtbompenawaran_kd', 'value' => isset($id) ? $id: null ));

?>
<div class="form-group">
	<label for='idtxtpart_kd' class="col-md-2 control-label">Nama Part / Versi</label>
	<div class="col-sm-7 col-xs-12">
		<div class="errInput" id="idErrpart_kd"></div>
		<?php echo form_dropdown('txtpart_kd', $opsiPart, isset($part_kd)? $part_kd : '', array('class'=> 'form-control select2', 'id'=> 'idtxtpart_kd', 'autofocus' => 'autofocus'));?>
	</div>	
	<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData('<?= $form_id ?>')" class="btn btn-primary btn-sm">
		<i class="fa fa-plus"></i> Tambah
	</button>
</div>
<hr>
<?php echo form_close(); ?>

<script type="text/javascript">
	
</script>