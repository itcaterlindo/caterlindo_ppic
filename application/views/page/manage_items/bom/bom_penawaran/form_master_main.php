<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputMaster';

if(isset($rowData)) {
	extract($rowData);
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtStsMaster', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtbompenawaran_kd', 'name'=> 'txtbompenawaran_kd', 'value' => isset($bompenawaran_kd) ? $bompenawaran_kd: null ));

?>
<div class="row">

	<div class="col-md-12">
		<div class="form-group">
			<label for="idtxtkd_customer" class="col-md-2 control-label">Customer</label>
			<div class="col-sm-4 col-xs-12">
				<div class="errInput" id="idErrkd_customer"></div>
				<?php echo form_dropdown('txtkd_customer', $opsiCustomer, isset($kd_customer)? $kd_customer : '', array('class'=> 'form-control select2', 'id'=> 'idtxtkd_customer'));?>
			</div>	
		</div>	
		<div class="form-group">
			<label for="idtxtkd_jenis_customer" class="col-md-2 control-label">Jenis Customer</label>
			<div class="col-sm-4 col-xs-12">
				<div class="errInput" id="idErrkd_jenis_customer"></div>
				<?php echo form_dropdown('txtkd_jenis_customer', $opsiJenisCustomer, isset($kd_jenis_customer)? $kd_jenis_customer : '', array('class'=> 'form-control', 'id'=> 'idtxtkd_jenis_customer'));?>
			</div>	
		</div>	
		<div class="form-group">
			<label for="idtxtbompenawaran_note" class="col-md-2 control-label">Keterangan</label>
			<div class="col-sm-9 col-xs-12">
				<div class="errInput" id="idErrbompenawaran_note"></div>
				<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control ttmaster-input', 'name'=> 'txtbompenawaran_note', 'id'=> 'idtxtbompenawaran_note', 'placeholder' =>'Keterangan', 'rows' => '2', 'value'=> isset($bompenawaran_note) ? $bompenawaran_note: null ));?>
			</div>
		</div>
	</div>

</div>

<hr>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-1 col-xs-12 col-sm-offset-10">
			<button type="submit" name="btnSubmit" onclick="submitDataMaster('<?= $form_id ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-arrow-circle-right"></i> Simpan
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>
<script type="text/javascript">
</script>