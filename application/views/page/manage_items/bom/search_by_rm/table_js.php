<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	// open_table();
	get_data_rm();
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table() {
		$('#<?php echo $btn_add_id?>').slideDown();
		box_overlay('in');
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/table_main'; ?>',
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
				moveTo('idMainContent');
				box_overlay('out');
			}
		});
	}



	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function remove_box() {
		$('#idFormBox<?php echo $master_var; ?>').remove();
	}

	function open_form_box(sts, id) {
		remove_box();
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_box'; ?>',
			data: {'sts': sts, 'id' : id},
			success: function(html) {
				$('#idMainContent').prepend(html);
				
			}
		});
	}


	function open_form_detail_box(id){
		$('#idFormDetailBox<?php echo $master_var; ?>').remove();
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_detail_box'; ?>',
			data: {'id' : id},
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function bomstdcosting_box (id) {
		$('#idTableBoxbomstdCosting').remove();
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/bomstdcosting_box'; ?>',
			data: {'id' : id},
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function get_data_rm() {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/get_data_rm'; ?>',
			success: function(html) {
				var ht = "";
				for (let index = 0; index < html.length; index++) {
					ht+= `<option value="` + html[index].rm_kd +`">` + html[index].rm_kode +` - ` + html[index].rm_nama +` - ` + html[index].rm_deskripsi +` - ` + html[index].rm_spesifikasi +`</option>`;
						
					}
					$('#rm').append(ht);
			}
		});
	}

	function open_table() {
		$('#<?php echo $btn_add_id?>').slideDown();
		box_overlay('in');
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/table_main'; ?>',
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
				moveTo('idMainContent');
				box_overlay('out');
			}
		});
	}

	$('#rm').on('change', function () {
		open_table();
	});

	function get_data_bwp(data) { 
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/get_data_bwp?bom_kd='; ?>' + data,
			success: function(html) {
				console.log(html)
			}
		});
	 }

	function edit_data(id){
		open_form_box('edit', id) 
	}

	function duplicate_data(id){
		open_form_box('duplicate', id) 
	}

	function detail_data(id){
		window.open("<?php echo base_url().'manage_items/bom/report_bom/bom_std_pdf?bom_kd='?>"+id);
	}

	function open_filter_bom() {
		var jnsBom = $('#idtxtjnsBom').val();
		open_table(jnsBom);
	}

	function view_files(id) {
		window.open('<?php echo base_url() . $class_link; ?>/viewfiles_box?id=' + id);
	}



	function hapus_data(id) {
		var conf = confirm('Apakah anda yakin menghapus item ini ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						open_table('std');
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				}
			});
		}
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
		box_overlay('out');
    }

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

</script>