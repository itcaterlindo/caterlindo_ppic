<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body table-responsive no-padding">
<table id="idTable" class="table table-bordered table-striped table-hover display" style="width:100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:10%; text-align:center;">RM kode</th>
		<th style="width:10%; text-align:center;">RM Nama</th>
		<th style="width:10%; text-align:center;">RM qty</th>
		<th style="width:35%; text-align:left;">Part Nama</th>
		<th style="width:10%; text-align:center;">Item Product</th>
		<th style="width:23%; text-align:left;">Deskripsi</th>
		<th style="width:22%; text-align:left;">Dimensi</th>
	</tr>
	</thead>
	<tbody id="apn_tbdy">

  	</tbody>
</table>
</div>
<script type="text/javascript">
	
	$.ajax({
		type: "GET",
		url: "<?php echo base_url().$class_link.'/table_data?id='; ?>" + $('#rm').val(),
		dataType: "json",
		success: function (response) {
			console.log(response)
			var b = '';
			
			var no = 0;
			if(response.length != 0){
				

				var t = $('#idTable').DataTable({
					dom: 'Bfrtip',
					buttons: [
						
						'copy', 'csv', 'excel', 'pdf'
					],
					"data": response.data,
					"columns": [
						{data: 'SrNo',
							render: function (data, type, row, meta) {
									return meta.row + 1;
							}
						},
						{ "data": "rm_kode" },
						{ "data": "rm_nama" },
						{ "data": "partdetail_qty" },
						{ "data": "partmain_nama" },
						{ "data": "item_code" },
						{ "data": "deskripsi_barang" },
						{ "data": "dimensi_barang" }
					]
				});
				t.on( 'draw.dt', function () {
						var PageInfo = $('#example').DataTable().page.info();
						t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
							cell.innerHTML = i + 1 + PageInfo.start;
						} );
				} );
				
				// for (let index = 0; index < response.data.length; index++) {
				// 	no++;
				// 	b+=  '<tr><td>'+ no +'</td><td>'+response.data[index].rm_kode+'</td><td>'+response.data[index].rm_nama+'</td><td>'+response.data[index].partmain_nama+'</td><td>'+response.data[index].item_code+'</td><td>'+response.data[index].deskripsi_barang+'</td><td>'+response.data[index].dimensi_barang+'</td></tr>';
					
				// 	$('#apn_tbdy').append(b);
				// }
				
			}
		}
	});
	// $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
	// 	return {
	// 		"iStart": oSettings._iDisplayStart,
	// 		"iEnd": oSettings.fnDisplayEnd(),
	// 		"iLength": oSettings._iDisplayLength,
	// 		"iTotal": oSettings.fnRecordsTotal(),
	// 		"iFilteredTotal": oSettings.fnRecordsDisplay(),
	// 		"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
	// 		"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
	// 	};
	// };
	// var table = $('#idTable').DataTable({
	// 	"processing": true,
	// 	"serverSide": true,
	// 	"ordering" : true,
	// 	"ajax": "<?php //echo base_url().$class_link.'/table_data?='; ?>" + $('#rm').val(),
	// 	"language" : {
	// 		"lengthMenu" : "Tampilkan _MENU_ data",
	// 		"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
	// 		"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
	// 		"infoFiltered": "",
	// 		"infoEmpty" : "Tidak ada data yang ditampilkan",
	// 		"search" : "Cari :",
	// 		"loadingRecords": "Memuat Data...",
	// 		"processing":     "Sedang Memproses...",
	// 		"paginate": {
	// 			"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
	// 			"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
	// 			"next":       '<span class="glyphicon glyphicon-forward"></span>',
	// 			"previous":   '<span class="glyphicon glyphicon-backward"></span>'
	// 		}
	// 	},
	// 	"columnDefs": [
	// 		{"data": null, "searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
	// 		{"searchable": false, "orderable": false, "targets": 1},
	// 		{"className": "dt-left", "targets": 2},
	// 	],
	// 	"rowCallback": function (row, data, iDisplayIndex) {
	// 		var info = this.fnPagingInfo();
	// 		var page = info.iPage;
	// 		var length = info.iLength;
	// 		var index = page * length + (iDisplayIndex + 1);
	// 		$('td:eq(0)', row).html(index);
	// 	}
	// });
</script>

