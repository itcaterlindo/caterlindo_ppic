<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table();
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		get_form('');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table() {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/open_table'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function remove_box() {
		$('#idFormBox<?php echo $master_var; ?>').remove();
	}

	function get_form(id) {
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		remove_box();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/get_form'; ?>',
			data: 'id='+id,
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function hapus_data(id) {
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		$('#<?php echo $box_overlay_id; ?>').show();
		remove_box();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/delete_data'; ?>',
			data: 'id='+id,
			success: function(data) {
				$('#<?php echo $box_alert_id; ?>').html(data.alert).fadeIn();
				$('#<?php echo $box_overlay_id; ?>').hide();
				open_table();
			}
		});
	}

	function data_rak(id) {
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		$('#<?php echo $box_overlay_id; ?>').show();
		remove_box();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/data_rak'; ?>',
			data: 'id='+id,
			success: function(data) {
				window.location.assign('<?php echo base_url().'manage_items/manage_inventory/data_rak'; ?>');
			}
		});
	}
</script>