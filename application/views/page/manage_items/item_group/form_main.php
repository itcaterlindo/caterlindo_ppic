<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputMaster';
if($sts == 'edit'){
	extract($rowData);
}

?>

<?php
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxt_kd', 'name'=> 'txt_kd', 'value' => $id ));
?>
<div class="row">
	<div class="col-md-12">
	<div class="form-group">
		<label for='idtxtitemgroup_nama' class="col-md-4 control-label">Nama Item Group</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErritemgroup_name"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'nmitem_group_name', 'id'=> 'iditem_group_name', 'placeholder' =>'Nama Item. Group', 'value'=> isset($item_group_name) ? $item_group_name: null ));?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtitemgroup_nama' class="col-md-4 control-label">Gudang</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrtxtgudang_kd"></div>
			<?php echo form_dropdown('txtGudang_kd', $opsiGudang, isset($gudang_kd)? $gudang_kd : '', array('class'=> 'form-control', 'id'=> 'idtxtgudang_kd'));?>
		</div>
	</div>

<hr>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-1 col-xs-12 col-sm-offset-10">
			<button type="submit" name="btnSubmit" onclick="submitData('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>
