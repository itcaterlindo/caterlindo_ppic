<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	form_master_main('<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		form_box('');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function form_master_main(id) {
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url().$class_link.'/form_main'; ?>',
            data: {id:id},
            success: function(html) {
                $('#<?php echo $box_content_id; ?>').html(html);
                moveTo('idMainContent');
            }
        });
	}

    function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

    function submitDataMaster(form_id) {
		event.preventDefault();
		var id_form = document.getElementById(form_id);
        let url = "<?php echo base_url().$class_link; ?>/action_insert_master";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(id_form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					$('.errInput').html('');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}
</script>