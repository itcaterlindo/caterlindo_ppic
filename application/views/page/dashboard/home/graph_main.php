<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataSales';
$form_id = 'idGraph'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>
<div class="form-group">
	<div class="col-md-2 col-xs-6">
		<label for="idSelTahunAwal">Pilih Tahun</label>
		<div id="idErrSelTahunAwal"></div>
		<?php echo form_dropdown('selTahunAwal', $opt_tahun, $pilihan_tahun, array('id' => 'idSelTahunAwal', 'class' => 'form-control')); ?>
	</div>
	<div class="col-md-2 col-xs-6">
		<label for="idSelBulanAwal">Pilih Bulan</label>
		<div id="idErrSelBulanAwal"></div>
		<?php echo form_dropdown('selBulanAwal', $opt_bulan, $pilihan_bulan, array('id' => 'idSelBulanAwal', 'class' => 'form-control')); ?>
	</div>
</div>
<div class="form-group">
	<div class="col-md-2 col-xs-12">
		<label>
			<input type="checkbox" class="minimal icheck" id="idChkBanding">
			Bandingkan Data
		</label>
	</div>
</div>
<div class="form-group" id="idFormBanding" style="display: none;">
	<div class="col-md-2 col-xs-6">
		<label for="idSelTahunAkhir">Pilih Tahun</label>
		<div id="idErrSelTahunAkhir"></div>
		<?php echo form_dropdown('selTahunAkhir', $opt_tahun, $pilihan_tahun, array('id' => 'idSelTahunAkhir', 'class' => 'form-control')); ?>
	</div>
	<div class="col-md-2 col-xs-6">
		<label for="idSelBulanAkhir">Pilih Bulan</label>
		<div id="idErrSelBulanAkhir"></div>
		<?php echo form_dropdown('selBulanAkhir', $opt_bulan, $pilihan_bulan, array('id' => 'idSelBulanAkhir', 'class' => 'form-control')); ?>
	</div>
</div>
<?php
echo form_close();