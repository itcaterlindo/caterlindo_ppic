<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<!-- Info boxes -->
<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-red"><i class="ion ion-ios-pricetags"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Tipe Product</span>
				<span class="info-box-number"><?php echo number_format($jml_tipe_product, 0, 0, ','); ?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-green"><i class="ion ion-ios-pricetag"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Jumlah Products</span>
				<span class="info-box-number"><?php echo number_format($jml_product, 0, 0, ','); ?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->

	<!-- fix for small devices only -->
	<div class="clearfix visible-sm-block"></div>

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-aqua"><i class="ion ion-bag"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Total SO</span>
				<span class="info-box-number"><?php echo number_format($jml_so, 0, 0, ','); ?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-yellow"><i class="ion ion-android-contacts"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Jumlah Customer</span>
				<span class="info-box-number"><?php echo number_format($jml_customer, 0, 0, ','); ?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
</div><!-- /.row -->