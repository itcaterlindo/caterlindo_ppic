<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<div class="box-body">
	<div class="col-md-12">
	<canvas id="barChart" style="height:100px"></canvas>
	</div>

	<div class="col-md-12">
		<div class="table-responsive no-padding">
			<table id="idTable" class="table table-bordered table-striped table-hover display" style="width:100%; font-size:85%;">
			<thead>
				<tr>
					<th>&nbsp;</th>
					<?php foreach($salesOrder as $so): ?>
						<th><?= $so['tahun']?></th>
					<?php endforeach; ?>
				</tr>
			</thead>

			<tbody>
				<tr>
					<td>Retail</td>
					<?php foreach($salesOrder as $so): ?>
						<td><?= number_format($so['retail']) ?></td>
					<?php endforeach; ?>
				</tr>
				<tr>
					<td>Distributor</td>
					<?php foreach($salesOrder as $so): ?>
						<td><?= number_format($so['distributor']) ?></td>
					<?php endforeach; ?>
				</tr>
				<tr>
					<td>Reseller</td>
					<?php foreach($salesOrder as $so): ?>
						<td><?= number_format($so['reseller']) ?></td>
					<?php endforeach; ?>
				</tr>
				<tr>
					<td>Total</td>
					<?php foreach($salesOrder as $so): ?>
						<td><?= number_format($so['total_ammount_exc']) ?></td>
					<?php endforeach; ?>
				</tr>
			</tbody>

			</table>
		</div>
	</div>
</div>	
<script type="text/javascript">

	$(document).ready(function(){
		render_chart('#barChart');
	});
	
	function render_chart(element) {
		var jenis_customer = ["Retail", "Distributor", "Reseller"],
		source = <?= json_encode($salesOrder) ?>,
		tahun = [],
		ammountRetail = [],
		ammountDistributor = [],
		ammountReseller = [];
		for(var item of source){
			tahun.push(item.tahun);
			ammountRetail.push(item.retail);
			ammountDistributor.push(item.distributor);
			ammountReseller.push(item.reseller);
		}
		var ctx = $(element), 
		setup = {
				type: 'bar',
				data: {
				labels: tahun,
				datasets: [
					{
					label: jenis_customer[0],
					backgroundColor: "#0275d8",
					data: ammountRetail
					}, 
					{
					label: jenis_customer[1],
					backgroundColor: "#5bc0de",
					data: ammountDistributor
					},
					{
					label: jenis_customer[2],
					backgroundColor: "#d9534f",
					data: ammountReseller
					}
				]
				},
				options: {
					title: {
						display: true,
						text: 'Data Penjualan Sales'
					}
				}
			};
		var myBar = new Chart(ctx, setup);

	}
</script>