<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	var jml_click = 0;
	open_graph('<?php echo date('Y'); ?>', '<?php echo date('m'); ?>', '', '');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('submit', '#idForm<?php echo $master_var; ?>').on('submit', '#idForm<?php echo $master_var; ?>', function(e) {
		e.preventDefault();
		submit_form(this);
	});

	$(document).off('ifClicked', '#idChkBanding').on('ifClicked', '#idChkBanding', function() {
		jml_click = jml_click + 1;
		if (jml_click % 2) {
			$('#idFormBanding').slideDown();
		} else {
			$('#idFormBanding').slideUp();
		}
	});

	$(document).off('change', '#idChkBanding').on('change', '#idChkBanding', function() {

	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_graph(thn_mulai, bln_mulai, thn_akhir, bln_akhir) {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/open_graph'; ?>',
				data: 'thn_mulai='+thn_mulai+'&bln_mulai='+bln_mulai+'&thn_akhir='+thn_akhir+'&bln_akhir='+bln_akhir,
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
					/* --START ICHECK PROPERTIES-- */
					$('input[type="checkbox"].icheck').iCheck({
						checkboxClass: 'icheckbox_square-blue'
					});
					/* --END ICHECK PROPERTIES-- */
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}
</script>