<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_report();
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('submit', '#idReport<?php echo $master_var; ?>').on('submit', '#idReport<?php echo $master_var; ?>', function(e) {
		e.preventDefault();
		submit_report(this);
	});

	$(document).off('change', '#idSelTipePenjualan').on('change', '#idSelTipePenjualan', function() {
		$.ajax({
			url: '<?php echo base_url().$class_link.'/get_tipe_customer'; ?>',
			type: 'GET',
			data: 'tipe_penjualan='+$(this).val(),
			success: function(data) {
				$('#idSelTipeCustomer').html(data);
			}
		});
	});

	$(document).off('change', '#idSelTipeCustomer').on('change', '#idSelTipeCustomer', function() {
		$.ajax({
			url: '<?php echo base_url().$class_link.'/get_tipe_disc'; ?>',
			type: 'GET',
			data: 'tipe_customer='+$(this).val(),
			success: function(data) {
				$('#idSelTipeDiscount').html(data);
			}
		});
	});

	$(document).off('click', 'button[name="btnCancel"]').on('click', 'button[name="btnCancel"]', function() {
		$('button[name="btnGenerate"]').html('<i class="fa fa-recycle"></i> Generate');
		$('button[name="btnGenerate"]').attr({
			'class': 'btn btn-primary btn-flat'
		});
		$('#idBtnSubmit').slideUp('fast', function() {
			$('#idBtnGenerate').slideDown();
		});
		$('#idBtnCancel').slideUp('fast', function() {
			$('#idBtnReset').slideDown();
		});
	});

	function open_report() {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/open_report'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					$('.datepicker').datetimepicker({
						format: 'DD-MM-YYYY'
					});
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function submit_report(form_id) {
		$('#<?php echo $box_overlay_id; ?>').show();
		$('#<?php echo $box_alert_id; ?>').html('');
		<?php
		foreach ($form_errs as $form_err) {
			echo '$(\'#'.$form_err.'\').html(\'\');';
		}
		?>
		$('button[name="btnGenerate"]').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('button[name="btnGenerate"]').attr({
			'class': 'btn btn-default btn-flat'
		});
		$.ajax({
			url: "<?php echo base_url().$class_link.'/generate_report'; ?>",
			type: "POST",
			data:  new FormData(form_id),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					$('#idBtnGenerate').slideUp('fast', function() {
						$('#idBtnSubmit').slideDown();
					});
					$('#idBtnReset').slideUp('fast', function() {
						$('#idBtnCancel').slideDown();
					});
					$('#btnSubmit').attr('href', '<?php echo base_url(); ?>'+data.tables);
				}
				if (data.confirm == 'error') {
					$('#<?php echo $box_alert_id; ?>').html(data.alert);
					<?php
					foreach ($form_errs as $form_err) {
						echo '$(\'#'.$form_err.'\').html(data.'.$form_err.');';
					}
					?>
					$('button[name="btnGenerate"]').html('<i class="fa fa-recycle"></i> Generate');
					$('button[name="btnGenerate"]').attr({
						'class': 'btn btn-primary btn-flat'
					});
				}
				$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
				$('#<?php echo $box_overlay_id; ?>').hide();
			}
		});
	}
</script>