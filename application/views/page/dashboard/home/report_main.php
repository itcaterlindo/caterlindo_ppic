<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataSales';
$form_id = 'idReport'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtTableReport', 'id' => 'idTxtTableReport'));
?>
<div class="form-group">
	<div class="col-md-2 col-xs-4">
		<label for="idSelTipePenjualan">Tipe Penjualan</label>
		<div id="idErrSelTipePenjualan"></div>
		<?php echo form_dropdown('selTipePenjualan', array('' => '-- Pilih Tipe Penjualan --', 'Lokal' => 'Lokal', 'Ekspor' => 'Ekspor'), '', array('id' => 'idSelTipePenjualan', 'class' => 'form-control')); ?>
	</div>
	<div class="col-md-2 col-xs-4">
		<label for="idSelTipeCustomer">Tipe Customer</label>
		<div id="idErrSelTipeCustomer"></div>
		<?php echo form_dropdown('selTipeCustomer', array('' => '-- Pilih Tipe Customer --'), '', array('id' => 'idSelTipeCustomer', 'class' => 'form-control')); ?>
	</div>
	<div class="col-md-2 col-xs-4">
		<label for="idSelTipeDiscount">Tipe Discount</label>
		<div id="idErrSelDiscount"></div>
		<?php echo form_dropdown('selTipeDiscount', array('' => '-- Pilih Tipe Discount --'), '', array('id' => 'idSelTipeDiscount', 'class' => 'form-control')); ?>
	</div>
</div>
<div class="form-group">
	<div class="col-md-2 col-xs-6">
		<label for="idSelSales">Salesperson</label>
		<div id="idErrSelSales"></div>
		<?php echo form_dropdown('selSales', $opt_sales, $pilihan_sales, array('id' => 'idSelSales', 'class' => 'form-control')); ?>
	</div>
	<div class="col-md-2 col-xs-6">
		<label for="idSelStatusItem">Status Item</label>
		<div id="idErrSelStatusItem"></div>
		<?php echo form_dropdown('selStatusItem', array('' => '-- Pilih Status Item --', 'semua' => 'Semua', 'std' => 'Standard', 'custom' => 'Custom'), '', array('id' => 'idSelStatusItem', 'class' => 'form-control')); ?>
	</div>
</div>
<div class="form-group">
	<div class="col-md-2 col-xs-6">
		<label for="idTxtTglAwal">Dari Tanggal</label>
		<div id="idErrTxtTglAwal"></div>
		<?php echo form_input(array('name' => 'txtTglAwal', 'id' => 'idTxtTglAwal', 'class' => 'form-control datepicker', 'placeholder' => 'Pilih Tanggal')); ?>
	</div>
	<div class="col-md-2 col-xs-6">
		<label for="idTxtTglAkhir">Sampai Tanggal</label>
		<div id="idErrTxtTglAkhir"></div>
		<?php echo form_input(array('name' => 'txtTglAkhir', 'id' => 'idTxtTglAkhir', 'class' => 'form-control datepicker', 'placeholder' => 'Pilih Tanggal')); ?>
	</div>
</div>
<hr>
<div class="form-group">
	<div id="idBtnCancel" class="col-sm-1 col-sm-offset-2 col-xs-12" style="display: none">
		<button type="button" name="btnCancel" class="btn btn-danger btn-flat">
			<i class="fa fa-arrow-left"></i> Cancel
		</button>
	</div>
	<div id="idBtnReset" class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div id="idBtnSubmit" class="col-sm-1 col-xs-12" style="display: none">
		<a href="" target="_blank" name="btnSubmit" id="btnSubmit" class="btn btn-success btn-flat">
			<i class="fa fa-file-excel-o"></i> Export
		</a>
	</div>
	<div id="idBtnGenerate" class="col-sm-1 col-xs-12">
		<button type="submit" name="btnGenerate" class="btn btn-primary btn-flat">
			<i class="fa fa-recycle"></i> Generate
		</button>
	</div>
</div>
<?php
echo form_close();