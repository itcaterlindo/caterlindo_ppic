<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body no-padding">
	<div class="row">
		<div class="col-md-6" style="height:200px">
			<canvas id="idChartAmmountEkspor"></canvas>
		</div>
		<div class="col-md-6 table-responsive">
			<table id="idTableAmmountEkspor" class="table table-bordered table-hover display" style="width:100%; font-size:85%;">
				<thead>
				<tr>
					<th style="text-align:center; vertical-align: middle">Tahun</th>
					<th style="text-align:center; vertical-align: middle">In USD</th>
				</tr>
				</thead>
				<tbody>
					<?php foreach($recapInv as $key => $value): ?>
						<tr>
							<td><?= $key ?></td>
							<td><?= number_format($value['ammount_distributor'], 2) ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot>
					
				</tfoot>
			</table>
		</div>
	</div>
	<br>
	<br>
	<div class="row">
		<div class="col-md-6" style="height:200px">
			<canvas id="idChart20ftEkspor"></canvas>
		</div>
		<div class="col-md-6 table-responsive">
			<table id="idTable20ftEkspor" class="table table-bordered table-hover display" style="width:100%; font-size:85%;">
				<thead>
				<tr>
					<th style="text-align:center; vertical-align: middle">Tahun</th>
					<th style="text-align:center; vertical-align: middle">In 20ft</th>
				</tr>
				</thead>
				<tbody>
					<?php foreach($recapInv as $key => $value): ?>
						<tr>
							<td><?= $key ?></td>
							<td><?= $value['container_20ft'] ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot>
					
				</tfoot>
			</table>
		</div>
	</div>

</div>

<script type="text/javascript">
	
	$(document).ready(function(){
		render_chart_sales_ammount('#idChartAmmountEkspor');
		render_chart_sales_20ft('#idChart20ftEkspor');
		// render_dt('#idTableAmmount');
		// render_dt('#idTable20ft');
	});

	
	function render_chart_sales_ammount(elem) {
		var sourceData = <?= json_encode($recapInv) ?>;
		var output = [];
		var ammountDistributor = [];
		var tahun = [];
		for(const [key, value] of Object.entries(sourceData) ){
			ammountDistributor.push(value.ammount_distributor);
			tahun.push(parseInt(key));
		}
		// Push to output
		output.push({ 'label': "Ammount Distributor", 'backgroundColor': "#5bc0de", 'data': ammountDistributor });
		var ctx = $(elem), 
		setup = {
				type: 'bar',
				data: {
					labels: tahun,
					datasets: output
				},
				options: {
					title: {
						display: true,
						text: 'Recap sales ekspor year'
					},
					responsive:true,
    				maintainAspectRatio: false,
					tooltips: {
						callbacks: {
							"label": (tooltipItem, data) => {
								// Tooltips custom ketika ada comma separator decimal
								var label = data.datasets[tooltipItem.datasetIndex].label || '';
								return label + ' ' + (Math.round(tooltipItem.yLabel * 100) / 100).toFixed(2);
							}
						}
					},
				}
			};
		var myBar = new Chart(ctx, setup);
	}

	function render_chart_sales_20ft(elem) {
		var sourceData = <?= json_encode($recapInv) ?>;
		var output = [];
		var ammount20ft = [];
		var tahun = [];
		for(const [key, value] of Object.entries(sourceData) ){
			ammount20ft.push(value.container_20ft);
			tahun.push(parseInt(key));
		}
		// Push to output
		output.push({ 'label': "Ammount 20ft", 'backgroundColor': "#5bc0de", 'data': ammount20ft });
		var ctx = $(elem), 
		setup = {
				type: 'bar',
				data: {
					labels: tahun,
					datasets: output
				},
				options: {
					title: {
						display: true,
						text: 'Recap sales ekspor year'
					},
					responsive:true,
    				maintainAspectRatio: false,
					tooltips: {
						callbacks: {
							"label": (tooltipItem, data) => {
								// Tooltips custom ketika ada comma separator decimal
								var label = data.datasets[tooltipItem.datasetIndex].label || '';
								return label + ' ' + (Math.round(tooltipItem.yLabel * 100) / 100).toFixed(2);
							}
						}
					},
				}
			};
		var myBar = new Chart(ctx, setup);
	}

</script>