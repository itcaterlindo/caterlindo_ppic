<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<table id="idTable" class="table table-bordered table-striped display" style="width:100%;">
	<thead>
	<tr>
		<th style="width:20%; text-align:center;">Item Group</th>
		<th style="width:20%; text-align:center;">Permission</th>
	</tr>
	</thead>
	<tbody>
		<?php foreach($item_group as $key => $val){ ?>
			<tr>
				<td><?= $val['item_group_name'] ?></td>
				<td class="text-center">
					<div class="form-group">
						<div class="checkbox">
							<input type="checkbox" name="checkAccess[]" value="<?= $val['item_group_kd'] ?>" <?= in_array($val['item_group_kd'], array_column($item_group_admin, 'item_group_kd')) ? "checked" : "" ?> >
						</div>
					</div>
				</td>
			</tr>
		<?php } ?>
	</tbody>

	<tfoot>
	</tfoot>
</table>

<script type="text/javascript">
	// $('#idTable').DataTable();
</script>