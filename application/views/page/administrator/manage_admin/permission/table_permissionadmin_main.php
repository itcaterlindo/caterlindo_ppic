<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<table id="idTable" class="table table-bordered table-striped display" style="width:100%;">
	<thead>
	<tr>
		<th style="width:60%; text-align:center;" class="all">Menu</th>
		<th style="width:20%; text-align:center;">Permission (Tipe Admin)</th>
		<th style="width:20%; text-align:center;">Permission (Admin)</th>
	</tr>
	</thead>
	<tbody>
    <?php 
    foreach ($menu as $eachMenu):
        $arrayGroupLevel[$eachMenu['level_menu']][] = $eachMenu;
    endforeach;
    
    $txt = '';
    $txtPermission = '';
    foreach ($arrayGroupLevel[0] as $each0) :
        $txtPermission = '';$txtPermissionAdmin = '';
        if (in_array($each0['id'], $hak_menu)) :
            foreach ($permission as $eachPermission0):
                $checked = ''; $checkedAdmin = '';
                if ($eachPermission0['menu_kd'] == $each0['id']){
                    /** current permission */
                    if (in_array($eachPermission0['permission_kd'], $permissionTipeAdmin)){
                        $checked = 'checked';
                    }
                    if (in_array($eachPermission0['permission_kd'], $permissionAdmin)){
                        $checkedAdmin = 'checked';
                    }

                    $txtPermission .= 
                    '<div class="checkbox">
                      <label>
                      <input type="checkbox" '.$checked.' onclick="return false;">
                            '.$eachPermission0['permission_nama'].'
                      </label>
                    </div>';
                    $txtPermissionAdmin .= 
                    '<div class="checkbox">
                      <label>
                      <input type="checkbox" name=chkPermission[] value="'.$eachPermission0['permission_kd'].'"  '.$checkedAdmin.'>
                            '.$eachPermission0['permission_nama'].'
                      </label>
                    </div>';
                }
            endforeach;
            $txt .= 
            '<tr>
                <td><label for="idChk'.$each0['id'].'" class="col-xs-12">'.ucwords(str_replace('_', ' ', $each0['nm_menu'])).'</label></td>
                <td><div class="form-group">'.$txtPermission.'</div></td>
                <td><div class="form-group">'.$txtPermissionAdmin.'</div></td>
            </tr>';
        endif;
        foreach($arrayGroupLevel[1] as $each1):
            $txtPermission = '';$txtPermissionAdmin = '';
            if ($each1['parent_menu'] == $each0['id']):
                if (in_array($each1['id'], $hak_menu)) :
                    foreach ($permission as $eachPermission1):
                        $checked = ''; $checkedAdmin = '';
                        if ($eachPermission1['menu_kd'] == $each1['id']){
                            /** current permission */
                            if (in_array($eachPermission1['permission_kd'], $permissionTipeAdmin)){
                                $checked = 'checked';
                            }
                            if (in_array($eachPermission1['permission_kd'], $permissionAdmin)){
                                $checkedAdmin = 'checked';
                            }
                            $txtPermission .= 
                            '<div class="checkbox">
                              <label>
                              <input type="checkbox" '.$checked.' onclick="return false;">
                                    '.$eachPermission1['permission_nama'].'
                              </label>
                            </div>';
                            $txtPermissionAdmin .= 
                            '<div class="checkbox">
                              <label>
                              <input type="checkbox" name=chkPermission[] value="'.$eachPermission1['permission_kd'].'" '.$checkedAdmin.'>
                                    '.$eachPermission1['permission_nama'].'
                              </label>
                            </div>';
                        }
                    endforeach;
                    $txt .= 
                    '<tr>
                        <td><label for="idChk'.$each1['id'].'" class="col-xs-12 col-xs-offset-1">'.ucwords(str_replace('_', ' ', $each1['nm_menu'])).'</label></td>
                        <td><div class="form-group">'.$txtPermission.'</div></td>
                        <td><div class="form-group">'.$txtPermissionAdmin.'</div></td>
                    </tr>';
                endif;
                foreach ($arrayGroupLevel[2] as $each2):
                    $txtPermission = '';$txtPermissionAdmin = '';
                    if ($each2['parent_menu'] == $each1['id']):
                        if (in_array($each2['id'], $hak_menu)) :
                            foreach ($permission as $eachPermission2):
                                $checked = ''; $checkedAdmin = '';
                                if ($eachPermission2['menu_kd'] == $each2['id']){
                                    /** current permission */
                                    if (in_array($eachPermission2['permission_kd'], $permissionTipeAdmin)){
                                        $checked = 'checked';
                                    }
                                    if (in_array($eachPermission2['permission_kd'], $permissionAdmin)){
                                        $checkedAdmin = 'checked';
                                    }
                                    $txtPermission .= 
                                    '<div class="checkbox">
                                      <label>
                                      <input type="checkbox" '.$checked.' onclick="return false;">
                                            '.$eachPermission2['permission_nama'].'
                                      </label>
                                    </div>';
                                    $txtPermissionAdmin .= 
                                    '<div class="checkbox">
                                      <label>
                                      <input type="checkbox" name=chkPermission[] value="'.$eachPermission2['permission_kd'].'" '.$checkedAdmin.'>
                                            '.$eachPermission2['permission_nama'].'
                                      </label>
                                    </div>';
                                }
                            endforeach;
                        $txt .= 
                        '<tr>
                            <td><label for="idChk'.$each2['id'].'" class="col-xs-12 col-xs-offset-2">'.ucwords(str_replace('_', ' ', $each2['nm_menu'])).'</label></td>
                            <td><div class="form-group">'.$txtPermission.'</div></td>
                            <td><div class="form-group">'.$txtPermissionAdmin.'</div></td>
                        </tr>';
                        endif;
                    endif;
                endforeach;
            endif;
        endforeach;
    endforeach;
    echo $txt;
    ?>
	</tbody>

	<tfoot>
	</tfoot>
</table>

<script type="text/javascript">
	// $('#idTable').DataTable();
</script>