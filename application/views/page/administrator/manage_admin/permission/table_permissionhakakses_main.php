<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<table id="idTable" class="table table-bordered table-striped display" style="width:100%;">
	<thead>
	<tr>
		<th style="width:70%; text-align:center;" class="all">Menu</th>
		<th style="width:30%; text-align:center;">Permission</th>
	</tr>
	</thead>
	<tbody>
    <?php 
    foreach ($menu as $eachMenu):
        $arrayGroupLevel[$eachMenu['level_menu']][] = $eachMenu;
    endforeach;
    
    $txt = '';
    $txtPermission = '';
    foreach ($arrayGroupLevel[0] as $each0) :
        $txtPermission = '';
        if (in_array($each0['id'], $hak_menu)) :
            foreach ($permission as $eachPermission0):
                $checked = '';
                if ($eachPermission0['menu_kd'] == $each0['id']){
                    /** current permission */
                    if (in_array($eachPermission0['permission_kd'], $permissionTipeAdmin)){
                        $checked = 'checked';
                    }
                    $txtPermission .= 
                    '<div class="checkbox">
                      <label>
                      <input type="checkbox" name=chkPermission[] value="'.$eachPermission0['permission_kd'].'" '.$checked.'>
                            '.$eachPermission0['permission_nama'].'
                      </label>
                    </div>';
                }
            endforeach;
            $txt .= 
            '<tr>
                <td><label for="idChk'.$each0['id'].'" class="col-xs-12">'.ucwords(str_replace('_', ' ', $each0['nm_menu'])).'</label></td>
                <td><div class="form-group">'.$txtPermission.'</div></td>
            </tr>';
        endif;
        foreach($arrayGroupLevel[1] as $each1):
            $txtPermission = '';
            if ($each1['parent_menu'] == $each0['id']):
                if (in_array($each1['id'], $hak_menu)) :
                    foreach ($permission as $eachPermission1):
                        $checked = '';
                        if ($eachPermission1['menu_kd'] == $each1['id']){
                            /** current permission */
                            if (in_array($eachPermission1['permission_kd'], $permissionTipeAdmin)){
                                $checked = 'checked';
                            }
                            $txtPermission .= 
                            '<div class="checkbox">
                              <label>
                                <input type="checkbox" name=chkPermission[] value="'.$eachPermission1['permission_kd'].'" '.$checked.'>
                                    '.$eachPermission1['permission_nama'].'
                              </label>
                            </div>';
                        }
                    endforeach;
                    $txt .= 
                    '<tr>
                        <td><label for="idChk'.$each1['id'].'" class="col-xs-12 col-xs-offset-1">'.ucwords(str_replace('_', ' ', $each1['nm_menu'])).'</label></td>
                        <td><div class="form-group">'.$txtPermission.'</div></td>
                    </tr>';
                endif;
                foreach ($arrayGroupLevel[2] as $each2):
                    $txtPermission = '';
                    if ($each2['parent_menu'] == $each1['id']):
                        if (in_array($each2['id'], $hak_menu)) :
                            foreach ($permission as $eachPermission2):
                                $checked = '';
                                if ($eachPermission2['menu_kd'] == $each2['id']){
                                    /** current permission */
                                    if (in_array($eachPermission2['permission_kd'], $permissionTipeAdmin)){
                                        $checked = 'checked';
                                    }
                                    $txtPermission .= 
                                    '<div class="checkbox">
                                      <label>
                                      <input type="checkbox" name=chkPermission[] value="'.$eachPermission2['permission_kd'].'" '.$checked.'>
                                            '.$eachPermission2['permission_nama'].'
                                      </label>
                                    </div>';
                                }
                            endforeach;
                        $txt .= 
                        '<tr>
                            <td><label for="idChk'.$each2['id'].'" class="col-xs-12 col-xs-offset-2">'.ucwords(str_replace('_', ' ', $each2['nm_menu'])).'</label></td>
                            <td><div class="form-group">'.$txtPermission.'</div></td>
                        </tr>';
                        endif;
                    endif;
                endforeach;
            endif;
        endforeach;
    endforeach;
    echo $txt;
    ?>
	</tbody>

	<tfoot>
	</tfoot>
</table>

<script type="text/javascript">
	// $('#idTable').DataTable();
</script>