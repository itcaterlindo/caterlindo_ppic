<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	table_permissionadmin_main('<?php echo $kd_admin; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function table_permissionadmin_main(kd_admin) {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_permissionadmin_main'; ?>',
				data: {kd_admin:kd_admin},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function remove_box() {
		$('#<?= $box_id?>').remove();
	}

	function submitDataPermission (form_id) {
		event.preventDefault();
		var form = document.getElementById(form_id);

		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_adminpermission_insertbatch" ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					remove_box();
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					moveTo('idMainContent');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }

</script>