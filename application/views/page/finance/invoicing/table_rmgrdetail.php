<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<?php 
$formGR = 'idFormGR';
echo form_open_multipart('', array('id' => $formGR)); 
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpurchaseinvoice_kdGR', 'name'=> 'txtpurchaseinvoice_kdGR', 'value' => $id, 'placeholder' => 'idtxtpurchaseinvoice_kdGR' ));
?>
<div id="idErrpurchaseinvoice_kdGR"></div>
<table id="idTableGR" class="table table-bordered table-striped " style="width:100%; font-size: 85%">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:1%; text-align:center;"> <input type="checkbox" class="icheck" id="check_all" checked> </th>
		<th style="width:7%; text-align:center;">Tgl Datang</th>
		<th style="width:7%; text-align:center;">No Surat Jalan</th>
		<th style="width:5%; text-align:center;">No PO</th>
		<th style="width:7%; text-align:center;">RM Kode</th>
		<th style="width:20%; text-align:center;">Deskripsi Material</th>
		<th style="width:5%; text-align:center;">Qty PO</th>
		<th style="width:5%; text-align:center;">Qty GR</th>
		<th style="width:4%; text-align:center;">Satuan</th>
		<th style="width:7%; text-align:center;">Harga PO</th>
		<th style="width:10%; text-align:center;">Total PO</th>
		<th style="width:10%; text-align:center;">Harga Invoice</th>
	</tr>
	</thead>
    <tbody>
        <?php 
		$no = 1;
		$subHarga = 0;
		foreach ($goodsreceive as $each) :
			$subHarga = (float)$each['rmgr_qty'] * $each['podetail_harga'];
			$helper_currency = $each['icon_type'].' '.$each['currency_icon'].' '.$each['pemisah_angka'].' '.$each['format_akhir'];
		?>
        <tr>
            <td class="dt-center"><?php echo $no; ?></td>
			<td> <input type="checkbox" class="icheck data-check" name="txtrmgr_kds[]" value="<?php echo $each['rmgr_kd']; ?>" checked> </td>
            <td><?php echo format_date($each['rmgr_tgldatang'], 'Y-m-d') ?></td>
            <td><?php echo $each['rmgr_nosrj'] ?></td>
            <td><?php echo $each['po_no']; ?></td>
            <td><?php echo $each['rm_kode']; ?></td>
            <td><?php echo $each['podetail_deskripsi'].'/'.$each['podetail_spesifikasi']; ?></td>
            <td class="dt-right"><?php echo (float) $each['podetail_qty'];?></td>
            <td class="dt-right"><?php echo (float) $each['rmgr_qty'];?></td>
            <td><?php echo $each['rmsatuan_nama']; ?></td>
            <td class="dt-right"><?php echo format_currency($each['podetail_harga'], $helper_currency) ?></td>
            <td class="dt-right"><?php echo format_currency($subHarga, $helper_currency) ?></td>
            <td class="dt-center">
				<?php 
                echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtpodetail_kds['.$each['rmgr_kd'].']', 'value' => $each['podetail_kd'] ,'placeholder' => 'txtpodetail_kd'));
				echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtrm_kds['.$each['rmgr_kd'].']', 'value' => $each['rm_kd'] ,'placeholder' => 'txtrm_kd'));
				echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtpodetail_namas['.$each['rmgr_kd'].']', 'value' => $each['podetail_nama'] ,'placeholder' => 'txtpodetail_nama'));
				echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtpodetail_deskripsis['.$each['rmgr_kd'].']', 'value' => $each['podetail_deskripsi'] ,'placeholder' => 'txtpodetail_deskripsi'));
				echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtpodetail_spesifikasis['.$each['rmgr_kd'].']', 'value' => $each['podetail_spesifikasi'] ,'placeholder' => 'txtpodetail_spesifikasi'));
				echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtpodetail_qtys['.$each['rmgr_kd'].']', 'value' => $each['podetail_qty'] ,'placeholder' => 'txtpodetail_qty'));
				echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtrmgr_qtys['.$each['rmgr_kd'].']', 'value' => $each['rmgr_qty'] ,'placeholder' => 'txtrmgr_qty'));
				echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtrmsatuan_kds['.$each['rmgr_kd'].']', 'value' => $each['rmsatuan_kd'] ,'placeholder' => 'txtrmsatuan_kd'));
                ?>
				<input type="number" name="txtpurchaseinvoicedetail_hargaunits[<?= $each['rmgr_kd'] ?>]" class="form-control input-sm" value="<?= $each['podetail_harga']?>">
            </td>
        </tr>
        <?php 
        $no ++;
        endforeach;?>
    </tbody>
	<tfoot>
	</tfoot>
</table>
<div class="row">
	<div class="col-md-12">
		<button class="btn btn-primary btn-sm pull-right" onclick="submitDataBatch('<?= $formGR; ?>')"> <i class="fa fa-save"></i> Simpan</button>
	</div>
</div>

<?php echo form_close(); ?>

<script type="text/javascript">
	render_dt('#idTableGR');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
		});
	}
</script>