<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
echo form_open_multipart('', array('id' => 'idformDiskon', 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtpurchaseinvoice_kd', 'placeholder' => 'idtxtpurchaseinvoice_kd', 'value' => isset($id) ? $id: null ));
if (isset($row)){
	extract($row);
}
?>
<div class="row">
	<div class="col-md-12">
	
		<div class="form-group">
			<label for='idtxtpurchaseinvoice_jenisdiskon' class="col-md-2 control-label">Jenis Diskon</label>
			<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpurchaseinvoice_jenisdiskon"></div>
			<?php echo form_dropdown('txtpurchaseinvoice_jenisdiskon', [null => '-- Pilih Opsi --', 'persentase' => 'Persentase', 'nominal' => 'Nominal'], isset($purchaseinvoice_jenisdiskon) ? $purchaseinvoice_jenisdiskon : null, array('class'=> 'form-control', 'id'=> 'idtxtpurchaseinvoice_jenisdiskon'));?>
			</div>	
		</div>

		<div class="form-group" id="idpersentase">
			<label for='idtxtpurchaseinvoice_diskonpersen' class="col-md-2 control-label">Persentase (%)</label>
			<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpurchaseinvoice_diskonpersen"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtpurchaseinvoice_diskonpersen', 'id'=> 'idtxtpurchaseinvoice_diskonpersen', 'placeholder' =>'%',  'value'=> isset($purchaseinvoice_diskonpersen) ? $purchaseinvoice_diskonpersen: null ));?>
			</div>
		</div>

		<div class="form-group" id="idnominal" style="display: none;">
			<label for='idtxtpurchaseinvoice_diskonnominal' class="col-md-2 control-label">Nominal</label>
			<div class="col-sm-5 col-xs-12">
			<div class="errInput" id="idErrpurchaseinvoice_diskonnominal"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtpurchaseinvoice_diskonnominal', 'id'=> 'idtxtpurchaseinvoice_diskonnominal', 'placeholder' =>'Rp',  'value'=> isset($purchaseinvoice_diskonnominal) ? $purchaseinvoice_diskonnominal: null ));?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-4 pull-right">
					<button class="btn btn-primary btn-sm" onclick="submitDataDiskon('idformDiskon')"> <i class="fa fa-save"></i> Simpan</button>
					<button class="btn btn-danger btn-sm" onclick="action_hapus_diskon('<?php echo $id; ?>')"> <i class="fa fa-times"></i> Hapus</button>
				</div>
			</div>
		</div>

	</div>
</div>

<?php echo form_close(); ?>

<script type="text/javascript">
	jenis_diskon ('<?php echo $purchaseinvoice_jenisdiskon;?>')
</script>
