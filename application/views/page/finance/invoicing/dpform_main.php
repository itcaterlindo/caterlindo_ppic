<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
?>
<style type="text/css">
	.select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>
<?php
echo form_open_multipart('', array('id' => 'idformItemUM', 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtSts', 'value' => isset($sts) ? $sts : null, 'placeholder' => 'idtxtSts' ));
echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtpurchaseinvoice_kd', 'placeholder' => 'idtxtpurchaseinvoice_kd', 'value' => isset($id) ? $id: null ));
?>

<div class="col-md-12">
    <div class="form-group">
		<label for='idtxtpurchaseinvoice_kdUm' class="col-md-2 control-label">No Invoice</label>
		<div class="col-sm-5 col-xs-12">
            <div class="errInput" id="idErrpurchaseinvoice_kdUm"></div>
            <select name="txtpurchaseinvoice_kdUm" id="idtxtpurchaseinvoice_kdUm" class="form-control"> </select>
		</div>
        <button class="btn btn-primary btn-sm" onclick="submitDataUM('idformItemUM')"> <i class="fa fa-save"></i> Simpan</button>
	</div>
</div>

<div class="col-md-12">
    <div id="idtabledp"></div>
</div>

<?php echo form_close(); ?>

<script type="text/javascript">
	dptable_main ('<?php echo $id; ?>');
</script>