<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
if (isset($row)) {
    extract($row);
}
?>
<style type="text/css">
.select2-container {
    width: 100% !important;
    padding: 0;
}
</style>

<?php
echo form_open_multipart('', array('id' => 'idformItemEdit', 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtSts', 'value' => isset($sts) ? $sts : null, 'placeholder' => 'idtxtSts' ));
echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtpurchaseinvoicedetail_kd', 'placeholder' => 'idtxtpurchaseinvoicedetail_kd', 'value' => isset($id) ? $id: null ));
echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtpurchaseinvoice_kd', 'placeholder' => 'idtxtpurchaseinvoice_kd', 'value' => isset($purchaseinvoice_kd) ? $purchaseinvoice_kd: null ));
echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtrmgr_kd', 'placeholder' => 'idtxtrmgr_kd', 'value' => isset($rmgr_kd) ? $rmgr_kd: null ));
echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtpodetail_kd', 'placeholder' => 'idtxtpodetail_kd', 'value' => isset($podetail_kd) ? $podetail_kd: null ));
?>

<div class="col-md-12">
    <div class="form-group">
        <label for="idtxtrmgr_nosrjEdit" class="col-md-2 control-label">No PO</label>
        <div class="col-sm-4 col-xs-12">
            <div class="errInput" id="idErrpurchaseinvoicedetail_nama"></div>
            <?php echo form_input(array( 'type'=> 'text', 'class' => 'form-control cl-po_no', 'name'=> 'txtpo_no', 'id'=> 'idtxtpo_noEdit', 'readonly' => 'true', 'placeholder' => 'No PO', 'value' => isset($po_no) ? $po_no: null ));?>
        </div>
        <label for="idtxtrmgr_nosrjEdit" class="col-md-2 control-label">No Srj</label>
        <div class="col-sm-4 col-xs-12">
            <div class="errInput" id="idErrrmgr_nosrjEdit"></div>
            <?php echo form_input(array( 'type'=> 'text', 'class' => 'form-control cl-po_no', 'name'=> 'txtrmgr_nosrj', 'id'=> 'idtxtrmgr_nosrjEdit', 'readonly' => 'true', 'placeholder' => 'No Srj', 'value' => isset($rmgr_nosrj) ? $rmgr_nosrj: null ));?>
        </div>
    </div>
    <div class="form-group">
        <label for='idtxtrm_kdEdit' class="col-md-2 control-label">Barang</label>
        <div class="col-sm-8 col-xs-12">
            <div class="errInput" id="idErrrm_kd"></div>
            <select name="txtrm_kd" id="idtxtrm_kdEdit" class="form-control cl-rm_kd"> </select>
        </div>
    </div>

    <div class="form-group">
        <label for="idtxtpurchaseinvoicedetail_namaEdit" class="col-md-2 control-label">Nama Item</label>
        <div class="col-sm-4 col-xs-12">
            <div class="errInput" id="idErrpurchaseinvoicedetail_nama"></div>
            <?php echo form_input(array( 'type'=> 'text', 'class' => 'form-control cl-purchaseinvoicedetail_nama', 'name'=> 'txtpurchaseinvoicedetail_nama', 'id'=> 'idtxtpurchaseinvoicedetail_namaEdit', 'readonly' => 'true', 'placeholder' => 'Nama Item', 'value' => isset($purchaseinvoicedetail_nama) ? $purchaseinvoicedetail_nama: null ));?>
        </div>
    </div>

    <div class="form-group">
        <label for="idtxtpurchaseinvoicedetail_deskripsiEdit" class="col-md-2 control-label">Deskripsi Item</label>
        <div class="col-sm-9 col-xs-12">
            <div class="errInput" id="idErrpurchaseinvoicedetail_deskripsi"></div>
            <?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control cl-purchaseinvoicedetail_deskripsi', 'name'=> 'txtpurchaseinvoicedetail_deskripsi', 'id'=> 'idtxtpurchaseinvoicedetail_deskripsiEdit', 'placeholder' =>'Deskripsi Item', 'rows' => '2', 'readonly' => 'true', 'value'=> isset($purchaseinvoicedetail_deskripsi) ? $purchaseinvoicedetail_deskripsi: null ));?>
        </div>
    </div>

    <div class="form-group">
        <label for="idtxtpurchaseinvoicedetail_spesifikasiEdit" class="col-md-2 control-label">Spesifikasi Item</label>
        <div class="col-sm-9 col-xs-12">
            <div class="errInput" id="idErrpurchaseinvoicedetail_spesifikasi"></div>
            <?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control cl-purchaseinvoicedetail_spesifikasi', 'name'=> 'txtpurchaseinvoicedetail_spesifikasi', 'id'=> 'idtxtpurchaseinvoicedetail_spesifikasiEdit', 'placeholder' =>'Spesifikasi Item', 'rows' => '2', 'readonly' => 'true', 'value'=> isset($purchaseinvoicedetail_spesifikasi) ? $purchaseinvoicedetail_spesifikasi: null ));?>
        </div>
    </div>

    <div class="form-group">
        <label for='idtxtpurchaseinvoicedetail_qtyEdit' class="col-md-2 control-label">Qty</label>
        <div class="col-sm-3 col-xs-12">
            <div class="errInput" id="idErrpurchaseinvoicedetail_qty"></div>
            <?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtpurchaseinvoicedetail_qty', 'id'=> 'idtxtpurchaseinvoicedetail_qtyEdit', 'placeholder' =>'Qty',  'value'=> isset($purchaseinvoicedetail_qty) ? $purchaseinvoicedetail_qty: null ));?>
        </div>
        <label for='idtxtpurchaseinvoicedetail_satuankdEdit' class="col-md-1 control-label">Satuan</label>
        <div class="col-sm-3 col-xs-12">
            <div class="errInput" id="idErrpurchaseinvoicedetail_satuankd"></div>
            <select name="txtpurchaseinvoicedetail_satuankd" id="idtxtpurchaseinvoicedetail_satuankdEdit" class="form-control cl-txtpurchaseinvoicedetail_satuankd"> </select>
        </div>
    </div>

    <div class="form-group">
        <label for='idtxtpurchaseinvoicedetail_hargaunitEdit' class="col-md-2 control-label">Harga</label>
        <div class="col-sm-4 col-xs-12">
            <div class="errInput" id="idErrpurchaseinvoicedetail_hargaunit"></div>
            <?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtpurchaseinvoicedetail_hargaunit', 'id'=> 'idtxtpurchaseinvoicedetail_hargaunitEdit', 'placeholder' =>'Harga',  'value'=> isset($purchaseinvoicedetail_hargaunit) ? $purchaseinvoicedetail_hargaunit: null ));?>
        </div>
    </div>

    <div class="form-group">
        <label for='idtxtpph_kd' class="col-md-2 control-label">PPh</label>
        <div class="col-sm-3 col-xs-12">
            <div class="errInput" id="idErrpph_kd"></div>
            <?php echo form_dropdown('txtpph_kd', isset($opsiPph) ? $opsiPph : [], isset($pph_kd)? $pph_kd : '', array('class'=> 'form-control', 'id'=> 'idtxtpph_kd'));?>
        </div>
        <label for='idtxtpurchaseinvoicedetail_pphpotongpayment' class="col-md-1 control-label">Potong Payment </label>
        <div class="col-sm-3 col-xs-12">
        <?php echo form_dropdown('txtpurchaseinvoicedetail_pphpotongpayment', isset($opsiPphPayment) ? $opsiPphPayment : [null => '-- Pilih Opsi --', '1' => 'Ya', '0' => 'Tidak'], isset($purchaseinvoicedetail_pphpotongpayment)? $purchaseinvoicedetail_pphpotongpayment : '', array('class'=> 'form-control', 'id'=> 'idtxtpurchaseinvoicedetail_pphpotongpayment'));?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-primary btn-sm pull-right" onclick="submitDataDetail('idformItemEdit')"> <i class="fa fa-save"></i> Simpan</button>
        </div>
    </div>
</div>

<?php echo form_close(); ?>

<script type="text/javascript">
    render_rm();
    render_satuan();

    $(document).off('select2:selecting', '#idtxtrm_kdEdit').on("select2:selecting", '#idtxtrm_kdEdit', function(e) { 
		let dt = e.params.args.data;
		$('#idtxtpurchaseinvoicedetail_namaEdit').val(dt.rm_nama).prop('readonly', true);
		$('#idtxtpurchaseinvoicedetail_deskripsiEdit').val(dt.rm_deskripsi).prop('readonly', true);
		$('#idtxtpurchaseinvoicedetail_spesifikasiEdit').val(dt.rm_spesifikasi).prop('readonly', true);
		if(dt.id == 'SPECIAL'){
			$('#idtxtpurchaseinvoicedetail_namaEdit').prop('readonly', false);
			$('#idtxtpurchaseinvoicedetail_deskripsiEdit').prop('readonly', false);
			$('#idtxtpurchaseinvoicedetail_spesifikasiEdit').prop('readonly', false);
		}
	});

    $("#idtxtrm_kdEdit").select2("trigger", "select",   { 
        data: { 
            id: `<?php echo $rm_kd; ?>`, 
            text: `<?php echo $rm_kode ; ?>`, 
            rm_nama: `<?php echo $purchaseinvoicedetail_nama; ?>`,
            rm_deskripsi: `<?php echo $purchaseinvoicedetail_deskripsi; ?>`,
            rm_spesifikasi: `<?php echo $purchaseinvoicedetail_spesifikasi; ?>`,
        }
    });
    $("#idtxtpurchaseinvoicedetail_satuankdEdit").select2("trigger", "select", { 
        data: { 
            id: `<?php echo $purchaseinvoicedetail_satuankd; ?>`, 
            text: `<?php echo $rmsatuan_nama; ?>`, 
        }
    });

    <?php if ($rm_kd == 'SPECIAL') :?>
    $('.cl-txtpurchaseinvoicedetail_nama').prop('readonly', false);
    $('.cl-txtpurchaseinvoicedetail_deskripsi').prop('readonly', false);
    $('.cl-txtpurchaseinvoicedetail_spesifikasi').prop('readonly', false);
    <?php endif;?>
</script>