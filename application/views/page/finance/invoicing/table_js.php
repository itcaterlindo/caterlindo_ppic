<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	// open_table();
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).ready(function () {
		render_datetimepicker_yy('tahun');
		open_filter()
	});

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		form_box('add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	
	function open_filter(){
		$('#id_tahun').on('dp.change', function(e){ run_filter() })
		$('#idbulan').change(function(){
			run_filter() 
		})
	}

	function run_filter(){
			var bulan = $('#idbulan').val();
			var tahun = $('#id_tahun').val();
			console.log(bulan, tahun)
			open_table(bulan, tahun);
	}
	// function open_filter(){
	// 	$('#view_filter').click(function (e) { 
	// 		e.preventDefault();
	// 		var bulan = $('#idbulan').val();
	// 		var tahun = $('#idtahun').val();
	// 		console.log(bulan, tahun)
	// 		open_table(bulan, tahun);
	// 	});
		
	// }

	function open_table(bulan, tahun) {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link?>' + '/table_main?bulan='+ bulan +'&tahun=' + tahun,
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function render_select2(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_datetimepicker(valClass){
		$('.'+valClass).datetimepicker({
			format: 'YYYY-MM-DD',
    	});
	}

	function render_datetimepicker_yy(valClass){
		$('.'+valClass).datetimepicker({
			format: 'YYYY',
    	});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function remove_box() {
		$('#idFormBox<?php echo $master_var; ?>').remove();
	}

	function form_box(sts, id) {
		remove_box();
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_box'; ?>',
			data: {'sts': sts, 'id' : id},
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function edit_data(id){
		form_box('edit', id) 
	}

	function push_sap(id){
		event.preventDefault();
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			$.blockUI({ overlayCSS: { backgroundColor: '#C0C0C0' } }); 
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/set_to_sap'; ?>',
				data: {id: id},
				success: function(data) {
					var resp = JSON.parse(data);
					console.log(resp)
					if (resp.data.PI[0].purchaseinvoice_jenis == 'dp') {
						sendtosap_dp(resp.data) 
					} else {
						sendtosap(resp.data);
					}
					
					// if(resp.code == 200){
					// 	notify (resp.status, resp.pesan, 'success');
					// 	open_table();
					// }else{
					// 	notify (resp.status, resp.pesan, 'error');
					// }
				}
			});
		}
	}

	function sendtosap_dp(param) { 
		var data_sap = {
							"KdPurchaseOrder": param.PI[0].po_kd ? param.PI[0].po_kd : "",
							"U_IDU_APDP_INTNUM": param.PI[0].purchaseinvoice_no,
							"U_FP": param.PI[0].purchaseinvoice_faktur,
							"DocDate": param.PI[0].purchaseinvoice_tanggal,
							"Amount":  param.PI[0].purchaseinvoice_grandtotal,
							"U_IDU_WEBUSER":  param.PI[0].nm_admin
						}

						fetch("<?= url_api_sap() ?>AddAPDP", {
						method: 'POST', // or 'PUT'
						headers: {
							'Content-Type': 'application/json',
						},
						body: JSON.stringify(data_sap),
						})
						.then((response) => response.json())
						.then((data) => {
							console.log('Success:', data);
							if(data.ErrorCode == 0 ){
								// console.log(dataToSAP);
								// console.log(items);
								// console.log(data.Message);
								
								notify ('200! Sukses.', data.Message, 'success');
								open_table();
								// window.location.reload();
							}else{
								// console.log(dataToSAP);
								// console.log(items);
								// console.log(data.Message);
								open_table();
								delRollback(param.PI[0].purchaseinvoice_kd)
								notify (data.ErrorCode + '. Gagal!', data.Message, 'error');
							}
							$.unblockUI();
						})
						.catch((error) => {
							open_table();
							delRollback(param.PI[0].purchaseinvoice_kd)
							notify ('000. Gagal!', error, 'error');
							$.unblockUI();
						});

						console.log(data_sap)

	 }

	function sendtosap(param) { 

		var data_detail = [];
		for (let index = 0; index < param.PID.length; index++) {
			data_detail.push({
							"U_IDU_WEBID": param.PID[index].purchaseinvoicedetail_kd,
							"KdGRPODtl": param.PID[index].rmgr_kd,
							"Quantity": param.PID[index].purchaseinvoicedetail_qty,
							"Konversi": param.PID[index].podetail_konversi,
							"LineTotal": param.PID[index].purchaseinvoicedetail_hargaunit * param.PID[index].purchaseinvoicedetail_qty, //Line Total
							"WithholdingTax": param.PID[index].purchaseinvoicedetail_pphpotongpayment != '1' ? "N" : "Y",
						})
		}
		var pph_sap = param.PI[0].pph_sap ? param.PI[0].pph_sap : "";

		var data_header = {
						"U_IDU_WEBID": param.PI[0].purchaseinvoice_kd,
						"CardCode": param.PI[0].suplier_kode,
						"U_IDU_AP_INTNUM": param.PI[0].purchaseinvoice_no,
						"NumAtCard": param.PI[0].purchaseinvoice_no,
						"U_FP": param.PI[0].purchaseinvoice_faktur,
						"DocDate": param.PI[0].purchaseinvoice_tanggal,
						"WTCode": param.PID[0].purchaseinvoicedetail_pphpotongpayment != '1' ? "" : pph_sap,
						"DocDueDate": param.PI[0].purchaseinvoice_tanggaltermpayment,
						"DiscPrcnt": param.PI[0].purchaseinvoice_diskonpersen ? param.PI[0].purchaseinvoice_diskonpersen : "",
						"VatGroup": param.PI[0].ppn_sap ? param.PI[0].ppn_sap : "P2",
						"Comments": param.PI[0].purchaseinvoice_note ? param.PI[0].purchaseinvoice_note : "",
						"U_IDU_WEBUSER": param.PI[0].nm_admin,
						"Lines_Detail_Item": data_detail
						}
					console.log(data_header);
					console.log(param);
			fetch("<?= url_api_sap() ?>AddAPInvoice", {
			method: 'POST', // or 'PUT'
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(data_header),
			})
			.then((response) => response.json())
			.then((data) => {
				console.log('Success:', data);
				if(data.ErrorCode == 0 ){
					// console.log(dataToSAP);
					// console.log(items);
					// console.log(data.Message);
					
					notify ('200! Sukses.', data.Message, 'success');
					open_table();
					// window.location.reload();
				}else{
					// console.log(dataToSAP);
					// console.log(items);
					// console.log(data.Message);
					open_table();
					delRollback(param.PI[0].purchaseinvoice_kd)
					notify (data.ErrorCode + '. Gagal!', data.Message, 'error');
				}
				$.unblockUI();
			})
			.catch((error) => {
				open_table();
				delRollback(param.PI[0].purchaseinvoice_kd)
				notify ('000. Gagal!', error, 'error');
				$.unblockUI();
			});
	 }

	function delRollback(param) { 
		$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/rollback_to_sap'; ?>',
				data: {id: param},
				success: function(data) {
					var resp = JSON.parse(data);
					$.unblockUI();
					if(resp.code == 200){
						notify (resp.status, 'SAP Gagal, Rollback Success!', 'success');
						open_table();
					}else{
						notify (resp.status, resp.pesan, 'error');
					}
				}
			});
	 }

	function view_box(id) {
		var url = '<?php echo base_url().$class_link;?>/view_box?id='+id;
		window.location.assign(url);	

	}

	function action_hapus_master (id) {
		event.preventDefault();
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_hapus_master'; ?>',
				data: {id: id},
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						open_table();
					}else{
						notify (resp.status, resp.pesan, 'error');
					}
				}
			});
		}
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

</script>