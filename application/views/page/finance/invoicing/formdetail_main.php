<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
?>
<style type="text/css">
.select2-container {
    width: 100% !important;
    padding: 0;
}
</style>

<div class="row">
	<div class="col-md-12">

        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Goods Receive</a></li>
              <li><a href="#tab_2" data-toggle="tab">Item Lain</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
              <!-- ---------------------------------------------------------------- -->
                <?php 
                echo form_open_multipart('', array('id' => 'idformCari', 'class' => 'form-horizontal'));
                echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts, 'placeholder' => 'idtxtSts' ));
                echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpurchaseinvoicedetail_kd', 'name'=> 'txtpurchaseinvoicedetail_kd', 'placeholder' => 'idtxtpurchaseinvoicedetail_kd', 'value' => isset($id) ? $id: null ));
                echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpo_kd', 'placeholder' => 'idtxtpo_kd', 'value' => null ));
                ?>
                <div class="form-group">
                    <label for='idtxtrmgr_nosrj' class="col-md-2 control-label">No PO / Surat Jalan</label>
                    <div class="col-sm-5 col-xs-12">
                        <div class="errInput" id="idErrrmgr_nosrj"></div>
                        <select name="txtrmgr_nosrj" id="idtxtrmgr_nosrj" class="form-control"> </select>
                    </div>
                    <button class="btn btn-sm btn-default" id="idbtnpilih" onclick="pilihsrj()"> <i class="fa fa-check-circle"></i> Pilih </button>
                </div>
                <?php echo form_close(); ?>

                <div id="idtableRmgr"></div> 

                <!-- ---------------------------------------------------------------- -->

              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="tab_2">
              <!-- ---------------------------------------------------------------- -->
              <?php
                echo form_open_multipart('', array('id' => 'idformItem', 'class' => 'form-horizontal'));
                echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtSts', 'value' => $sts, 'placeholder' => 'idtxtSts' ));
                echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtpurchaseinvoice_kd', 'placeholder' => 'idtxtpurchaseinvoice_kd', 'value' => isset($id) ? $id: null ));
              ?>

              <div class="form-group">
                <label for='idtxtrm_kd' class="col-md-2 control-label">Barang</label>
                <div class="col-sm-6 col-xs-12">
                  <div class="errInput" id="idErrrm_kd"></div>
                  <select name="txtrm_kd" id="idtxtrm_kd" class="form-control cl-rm_kd"> </select>
                </div>
              </div>

              <div class="form-group">
                <label for="idtxtpurchaseinvoicedetail_nama" class="col-md-2 control-label">Nama Item</label>
                <div class="col-sm-4 col-xs-12">
                  <div class="errInput" id="idErrpurchaseinvoicedetail_nama"></div>
                  <?php echo form_input(array( 'type'=> 'text', 'class' => 'form-control cl-purchaseinvoicedetail_nama', 'name'=> 'txtpurchaseinvoicedetail_nama', 'id'=> 'idtxtpurchaseinvoicedetail_nama', 'readonly' => 'true', 'placeholder' => 'Nama Item', 'value' => isset($purchaseinvoicedetail_nama) ? $purchaseinvoicedetail_nama: null ));?>
                </div>
              </div>

              <div class="form-group">
                <label for="idtxtpurchaseinvoicedetail_deskripsi" class="col-md-2 control-label">Deskripsi Item</label>
                <div class="col-sm-9 col-xs-12">
                  <div class="errInput" id="idErrpurchaseinvoicedetail_deskripsi"></div>
                  <?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control cl-purchaseinvoicedetail_deskripsi', 'name'=> 'txtpurchaseinvoicedetail_deskripsi', 'id'=> 'idtxtpurchaseinvoicedetail_deskripsi', 'placeholder' =>'Deskripsi Item', 'rows' => '2', 'readonly' => 'true', 'value'=> isset($purchaseinvoicedetail_deskripsi) ? $purchaseinvoicedetail_deskripsi: null ));?>
                </div>
              </div>

              <div class="form-group">
                <label for="idtxtpurchaseinvoicedetail_spesifikasi" class="col-md-2 control-label">Spesifikasi Item</label>
                <div class="col-sm-9 col-xs-12">
                  <div class="errInput" id="idErrpurchaseinvoicedetail_spesifikasi"></div>
                  <?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control cl-purchaseinvoicedetail_spesifikasi', 'name'=> 'txtpurchaseinvoicedetail_spesifikasi', 'id'=> 'idtxtpurchaseinvoicedetail_spesifikasi', 'placeholder' =>'Spesifikasi Item', 'rows' => '2', 'readonly' => 'true', 'value'=> isset($purchaseinvoicedetail_spesifikasi) ? $purchaseinvoicedetail_spesifikasi: null ));?>
                </div>
              </div>

              <div class="form-group">
                <label for='idtxtpurchaseinvoicedetail_qty' class="col-md-2 control-label">Qty</label>
                <div class="col-sm-2 col-xs-12">
                  <div class="errInput" id="idErrpurchaseinvoicedetail_qty"></div>
                  <?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtpurchaseinvoicedetail_qty', 'id'=> 'idtxtpurchaseinvoicedetail_qty', 'placeholder' =>'Qty',  'value'=> isset($purchaseinvoicedetail_qty) ? $purchaseinvoicedetail_qty: null ));?>
                </div>
                <label for='idtxtpurchaseinvoicedetail_satuankd' class="col-md-1 control-label">Satuan</label>
                <div class="col-sm-2 col-xs-12">
                  <div class="errInput" id="idErrpurchaseinvoicedetail_satuankd"></div>
                  <select name="txtpurchaseinvoicedetail_satuankd" id="idtxtpurchaseinvoicedetail_satuankd" class="form-control cl-txtpurchaseinvoicedetail_satuankd"> </select>
                </div>
                <label for='idtxtpurchaseinvoicedetail_hargaunit' class="col-md-1 control-label">Harga</label>
                <div class="col-sm-2 col-xs-12">
                  <div class="errInput" id="idErrpurchaseinvoicedetail_hargaunit"></div>
                  <?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtpurchaseinvoicedetail_hargaunit', 'id'=> 'idtxtpurchaseinvoicedetail_hargaunit', 'placeholder' =>'Harga',  'value'=> isset($purchaseinvoicedetail_hargaunit) ? $purchaseinvoicedetail_hargaunit: null ));?>
                </div>
              </div>

              <div class="form-group">
                <label for='idtxtpph_kd' class="col-md-2 control-label">PPh</label>
                <div class="col-sm-3 col-xs-12">
                  <div class="errInput" id="idErrpph_kd"></div>
                  <?php echo form_dropdown('txtpph_kd', isset($opsiPph) ? $opsiPph : [], isset($pph_kd)? $pph_kd : '', array('class'=> 'form-control', 'id'=> 'idtxtpph_kd'));?>
                </div>
                <div id="idPphPayment" style="display: none;">
                  <label for='idtxtpurchaseinvoicedetail_pphpotongpayment' class="col-md-1 control-label">Potong Payment </label>
                  <div class="col-sm-3 col-xs-12">
                    <?php echo form_dropdown('txtpurchaseinvoicedetail_pphpotongpayment', isset($opsiPphPayment) ? $opsiPphPayment : ['0' => 'Tidak', '1' => 'Ya'], isset($purchaseinvoicedetail_pphpotongpayment)? $purchaseinvoicedetail_pphpotongpayment : '', array('class'=> 'form-control', 'id'=> 'idtxtpurchaseinvoicedetail_pphpotongpayment'));?>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
      		        <button class="btn btn-primary btn-sm pull-right" onclick="submitDataDetail('idformItem')"> <i class="fa fa-save"></i> Simpan</button>
                </div>
              </div>

              <?php echo form_close(); ?>

              <!-- ---------------------------------------------------------------- -->
              </div>
              <!-- /.tab-pane -->
             
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        
    </div>
</div>


