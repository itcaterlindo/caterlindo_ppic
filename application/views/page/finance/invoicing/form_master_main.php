<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputMaster';
if($sts == 'edit'){
	extract($rowData);
}

?>
<style type="text/css">
	.select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>
<?php
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtStsMaster', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpurchaseinvoice_kd', 'name'=> 'txtpurchaseinvoice_kd', 'value' => isset($id) ? $id: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpurchaseinvoice_ppn', 'name'=> 'txtpurchaseinvoice_ppn', 'value' => isset($purchaseinvoice_ppn) ? $purchaseinvoice_ppn: null ));
?>
<div class="row">

	<div class="form-group">
		<label for='idtxtpurchaseinvoice_jenis' class="col-md-2 control-label">Jenis Invoice</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpurchaseinvoice_jenis"></div>
			<?php echo form_dropdown('txtpurchaseinvoice_jenis', ['invoice' => 'Invoice', 'dp' => 'Uang Muka'], isset($purchaseinvoice_jenis)? $purchaseinvoice_jenis : '', array('class'=> 'form-control', 'id'=> 'idtxtpurchaseinvoice_jenis'));?>
		</div>
	</div>
	
    <div class="form-group">
		<label for='idtxtsuplier_kd' class="col-md-2 control-label">Suplier</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrsuplier_kd"></div>
			<select class="form-control" name="txtsuplier_kd" id="idtxtsuplier_kd"></select>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtpurchaseinvoice_no' class="col-md-2 control-label">No Invoice</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrpurchaseinvoice_no"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtpurchaseinvoice_no', 'id'=> 'idtxtpurchaseinvoice_no', 'placeholder' =>'No Invoice', 'value'=> isset($purchaseinvoice_no) ? $purchaseinvoice_no: null ));?>
		</div>
		<label for='idtxtpurchaseinvoice_tanggal' class="col-md-1 control-label">Tgl Invoice</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpurchaseinvoice_tanggal"></div>
			<div class="input-group date">			
				<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
				<?php echo form_input(array('type'=>'text', 'class'=> 'form-control datetimepicker cl-termpayment', 'name'=> 'txtpurchaseinvoice_tanggal', 'id'=> 'idtxtpurchaseinvoice_tanggal', 'placeholder' =>'yyyy-mm-dd', 'value'=> isset($purchaseinvoice_tanggal) ? $purchaseinvoice_tanggal: date('Y-m-d') ));?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtpurchaseinvoice_faktur' class="col-md-2 control-label">No Faktur</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrpurchaseinvoice_faktur"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtpurchaseinvoice_faktur', 'id'=> 'idtxtpurchaseinvoice_faktur', 'placeholder' =>'No Faktur', 'data-inputmask' => '\'mask\': [\'999.999-99.99999999\']', 'data-mask' => '','value'=> isset($purchaseinvoice_faktur) ? $purchaseinvoice_faktur: null ));?>
		</div>
		<label for='idtxtpurchaseinvoice_tanggalfaktur' class="col-md-1 control-label">Tgl Faktur</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpurchaseinvoice_tanggalfaktur"></div>
			<div class="input-group date">			
				<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
				<?php echo form_input(array('type'=>'text', 'class'=> 'form-control datetimepicker', 'name'=> 'txtpurchaseinvoice_tanggalfaktur', 'id'=> 'idtxtpurchaseinvoice_tanggalfaktur', 'placeholder' =>'yyyy-mm-dd', 'value'=> isset($purchaseinvoice_tanggalfaktur) ? $purchaseinvoice_tanggalfaktur: null ));?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtpurchaseinvoice_termpayment' class="col-md-2 control-label">Term Payment (hari)</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrpurchaseinvoice_termpayment"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control cl-termpayment', 'name'=> 'txtpurchaseinvoice_termpayment', 'id'=> 'idtxtpurchaseinvoice_termpayment', 'placeholder' =>'Term Payment', 'value'=> isset($purchaseinvoice_termpayment) ? $purchaseinvoice_termpayment: null ));?>
		</div>
		<label for='idtxtpurchaseinvoice_tanggaltermpayment' class="col-md-1 control-label">Tgl Term Payment</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpurchaseinvoice_tanggaltermpayment"></div>
			<div class="input-group date">			
				<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
				<?php echo form_input(array('type'=>'text', 'class'=> 'form-control datetimepicker', 'name'=> 'txtpurchaseinvoice_tanggaltermpayment', 'id'=> 'idtxtpurchaseinvoice_tanggaltermpayment', 'placeholder' =>'yyyy-mm-dd', 'value'=> isset($purchaseinvoice_tanggaltermpayment) ? $purchaseinvoice_tanggaltermpayment: null ));?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtpurchaseinvoice_tanggaltt' class="col-md-2 control-label">Tgl TT</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpurchaseinvoice_tanggaltt"></div>
			<div class="input-group date">			
				<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
				<?php echo form_input(array('type'=>'text', 'class'=> 'form-control datetimepicker', 'name'=> 'txtpurchaseinvoice_tanggaltt', 'id'=> 'idtxtpurchaseinvoice_tanggaltt', 'placeholder' =>'yyyy-mm-dd', 'value'=> isset($purchaseinvoice_tanggaltt) ? $purchaseinvoice_tanggaltt: null ));?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for='idtxtpph_SAP' class="col-md-2 control-label">Pph SAP</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpph_SAP"></div>
			<select class="form-control" name="txtpph_SAP" id="idtxtpph_SAP">
				<option value="">-- Pilih Pph --</option>
			</select>
		</div>
	</div>
	<div id="ap_po" style="display: none;">
		<div class="form-group">
						<label for='idtxtrmgr_nosrj' class="col-md-2 control-label">No PO</label>
						<div class="col-sm-5 col-xs-12">
							<div class="errInput" id="idErrrmgr_nosrj"></div>
							<select name="txtno_po_dp" id="idtxtno_po_dp" class="form-control"> </select>
						</div>
					</div>
		<div class="form-group">
			<label for='idtxtpurchaseinvoice_no' class="col-md-2 control-label">Ammount - Grand Total</label>
			<div class="col-sm-3 col-xs-12">
				<div class="errInput" id="idErrpurchaseinvoice_no"></div>
				<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtpurchaseinvoice_grand_total', 'id'=> 'idtxtpurchaseinvoice_grand_total', 'placeholder' =>'Ammount', 'value'=> isset($purchaseinvoice_grandtotal) ? $purchaseinvoice_grandtotal: null ));?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtpurchaseinvoice_note' class="col-md-2 control-label">Note</label>
		<div class="col-sm-6 col-xs-12">
			<div class="errInput" id="idErrpurchaseinvoice_note"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtpurchaseinvoice_note', 'id'=> 'idtxtpurchaseinvoice_note', 'placeholder' =>'Note', 'rows' => '2', 'value'=> isset($purchaseinvoice_note) ? $purchaseinvoice_note: null ));?>
		</div>
	</div>


</div>

<hr>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-1 col-xs-12 col-sm-offset-10">
			<button type="submit" name="btnSubmit" onclick="submitDataMaster('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-arrow-circle-right"></i> Simpan
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	render_suplier();
	render_pph_SAP();
	$("#idtxtpph_SAP").select2();
	<?php if ($sts == 'edit') :?>
		$("#idtxtsuplier_kd").select2("trigger", "select", { 
			data: { 
				id: '<?php echo $suplier_kd; ?>', 
				text: '<?php echo $suplier_kode.' | '.$suplier_nama; ?>', 
			}
    	});

		var data = '<option value="' + '<?php echo $pph_sap; ?>' + '" selected>  '+ '<?php echo $pph_sap.' | '.$pph_sap; ?>'+' </option>';
		$('#idtxtpph_SAP').append(data);
	<?php endif;?>
</script>
