<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body table-responsive no-padding">
<table id="idTable" border="1" cellpadding="10" style="width:100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;">No.</th>
		<th style="width:5%; text-align:center;">No PO</th>
		<th style="width:7%; text-align:center;">No Srj</th>
		<th style="width:7%; text-align:center;">Kode Barang</th>
		<th style="width:15%; text-align:center;">Deskripsi Barang</th>
		<th style="width:5%; text-align:center;">Qty</th>
		<th style="width:5%; text-align:center;">Satuan</th>
		<th style="width:5%; text-align:center;">PPh</th>
		<th style="width:10%; text-align:center;">Harga PO</th>
		<th style="width:10%; text-align:center;">Harga Invoice</th>
		<th style="width:10%; text-align:center;">Total Invoice</th>
	</tr>
	</thead>
    <tbody>
    <?php 
    $no = 1;
    $subtotal = 0; $total = 0;
    foreach ($result as $r) : 
        $subtotal = $r['purchaseinvoicedetail_hargaunit'] * $r['purchaseinvoicedetail_qty'];
        $total += $subtotal;
    ?>
    <tr>
        <td><?php echo $no; ?></td>
        <td><?php echo $r['po_no']; ?></td>
        <td><?php echo $r['rmgr_nosrj']; ?></td>
        <td><?php echo $r['rm_kode'].'/'.$r['rm_oldkd']; ?></td>
        <td><?php echo $r['purchaseinvoicedetail_deskripsi'].'/'.$r['purchaseinvoicedetail_spesifikasi']; ?></td>
        <td><?php echo $r['purchaseinvoicedetail_qty']; ?></td>
        <td><?php echo $r['rmsatuan_nama']; ?></td>
        <td> <a href="javascript:void(0);" onclick="view_pph('<?php echo $r['purchaseinvoicedetail_kd']; ?>')"><?php echo $r['pph_nama']; ?></a> </td>
        <td style="text-align:right;"><?php echo custom_numberformat($r['podetail_harga'], 2 , 'IDR'); ?></td>
        <td style="text-align:right;"><?php echo custom_numberformat($r['purchaseinvoicedetail_hargaunit'], 2 ,'IDR'); ?></td>
        <td style="text-align:right;"><?php echo custom_numberformat($subtotal, 2, 'IDR'); ?></td>
    </tr>
    <?php 
    $no ++;
    endforeach;
    $diskonrp = 0;
    $diskontxt = '';
    if ($master['purchaseinvoice_jenisdiskon'] == 'nominal') {
        $diskonrp = $master['purchaseinvoice_diskonnominal'];
    }elseif ($master['purchaseinvoice_jenisdiskon'] == 'persentase') {
        $diskonrp = $master['purchaseinvoice_diskonpersen'] / 100 * $total;
        $diskontxt = '('.$master['purchaseinvoice_diskonpersen'].'%)'; 
    }

    $uangmuka = $master['purchaseinvoice_dp'];
    $dpp = $total - $diskonrp - $uangmuka;
    $ppnRp=0; $ppntxt = '';
    
    if (!empty($master['purchaseinvoice_ppn'])) {
        
        $tgl_ppn_naik = '2022-03-31';
        $tgl_invoice = $master['purchaseinvoice_tanggal'];
        if(($tgl_ppn_naik <  $tgl_invoice) == true){
            if($master['purchaseinvoice_ppn'] == '10'){
                $master['purchaseinvoice_ppn'] = '11';
            }
            $ppnRp = $master['purchaseinvoice_ppn'] / 100 * $dpp;
            $ppntxt = '('.$master['purchaseinvoice_ppn'].'%)';
        }else{
            $ppnRp = $master['purchaseinvoice_ppn'] / 100 * $dpp;
            $ppntxt = '('.$master['purchaseinvoice_ppn'].'%)';
        }
        
    }

    $grandTotal = $dpp + $ppnRp;
    ?>
    </tbody>
    <tfoot style="font-weight: bold;">
    <tr>
        <td colspan="10" style="text-align:right;">Total Harga : </td>
        <td style="text-align:right;"><?php echo custom_numberformat($total, 2 , 'IDR'); ?></td>
    </tr>
    <tr>
        <td colspan="10" style="text-align:right;">Diskon <?php echo ucwords($master['purchaseinvoice_jenisdiskon']).' '.$diskontxt ?> :</td>
        <td style="text-align:right;"><?php echo !empty($diskonrp) ? custom_numberformat($diskonrp, 2 , 'IDR') : 0; ?></td>
    </tr>
    <tr>
        <td colspan="10" style="text-align:right;">Uang Muka :</td>
        <td style="text-align:right;"><?php echo !empty($uangmuka) ? custom_numberformat($uangmuka, 2 , 'IDR') : 0; ?></td>
    </tr>
    <tr>
        <td colspan="10" style="text-align:right;">DPP :</td>
        <td style="text-align:right;"><?php echo !empty($dpp) ? custom_numberformat($dpp, 2 , 'IDR') : 0; ?></td>
    </tr>
    <tr>
        <td colspan="10" style="text-align:right;">PPN <?php echo $ppntxt; ?> :</td>
        <td style="text-align:right;"><?php echo !empty($ppnRp) ? custom_numberformat($ppnRp, 2 , 'IDR') : 0; ?></td>
    </tr>
    <tr>
        <td colspan="10" style="text-align:right;">Grand Total :</td>
        <td style="text-align:right;"><?php echo custom_numberformat($grandTotal, 2 , 'IDR'); ?></td>
    </tr>
    </tfoot>
</table>
</div>