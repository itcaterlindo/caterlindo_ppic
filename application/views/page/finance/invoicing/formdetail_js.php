<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	formdetail_main('<?php echo $id; ?>');
	tabledetail_main ('<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		form_box('add', '');
	});

	$(document).off('change', '#idtxtpurchaseinvoice_jenisdiskon').on('change', '#idtxtpurchaseinvoice_jenisdiskon', function() {
		let val = $(this).val();
		jenis_diskon (val);
	});
	$(document).off('change', '#idtxtpph_kd').on('change', '#idtxtpph_kd', function() {
		let val = $(this).val();
		if (val != ''){
			$('#idPphPayment').slideDown();
		}else{
			$('#idPphPayment').slideUp();
		}
	});

	$(document).off('ifChanged', '#check_all').on('ifChanged', '#check_all', function(e) {
        var headerchecked = e.target.checked;
        if (headerchecked == true) {
            $('.data-check').iCheck('check');
        }else{
            $('.data-check').iCheck('uncheck');
        }
    });

	$(document).off('select2:selecting', '#idtxtrm_kd').on("select2:selecting", '#idtxtrm_kd', function(e) { 
		let dt = e.params.args.data; 
		$('#idtxtpurchaseinvoicedetail_nama').val(dt.rm_nama).prop('readonly', true);
		$('#idtxtpurchaseinvoicedetail_deskripsi').val(dt.rm_deskripsi).prop('readonly', true);
		$('#idtxtpurchaseinvoicedetail_spesifikasi').val(dt.rm_spesifikasi).prop('readonly', true);
		if(dt.id == 'SPECIAL'){
			$('#idtxtpurchaseinvoicedetail_nama').prop('readonly', false);
			$('#idtxtpurchaseinvoicedetail_deskripsi').prop('readonly', false);
			$('#idtxtpurchaseinvoicedetail_spesifikasi').prop('readonly', false);
		}
	});

	$(document).off('select2:selecting', '#idtxtrmgr_nosrj').on("select2:selecting", '#idtxtrmgr_nosrj', function(e) { 
		let dt = e.params.args.data; 
		$('#idtxtpo_kd').val(dt.po_kd);
	});

	function jenis_diskon (val) {
		if (val == 'persentase') {
			$('#idtxtpurchaseinvoice_diskonnominal').val('');
			$('#idnominal').slideUp();
			$('#idpersentase').slideDown();
		}else {
			$('#idtxtpurchaseinvoice_diskonpersen').val('');
			$('#idnominal').slideDown();
			$('#idpersentase').slideUp();
		}
	}

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function formdetail_main(id) {
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url().$class_link.'/formdetail_main'; ?>',
			data: {id: id},
            success: function(html) {
                $('#<?php echo $box_content_id; ?>').html(html);
                render_srj('<?php echo $suplier_kd; ?>', '<?php echo $purchaseinvoice_jenis; ?>');
				render_rm();
				render_satuan();
                moveTo('idMainContent');

            }
        });
	}

    function render_srj(suplier_kd, pi_jenis) {
		$("#idtxtrmgr_nosrj").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?= base_url().$class_link ?>/get_grby_suratjalan',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramSRJ: params.term, // search term
						suplier_kd: suplier_kd,
						pi_jenis : pi_jenis
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

    function render_rm() {
		$(".cl-rm_kd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 3,
			ajax: { 
				url: '<?php echo base_url() ?>/Auto_complete/get_raw_material',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramRM: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

    function render_satuan() {
		$(".cl-txtpurchaseinvoicedetail_satuankd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?php echo base_url() ?>/Auto_complete/get_rm_satuan',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramSatuan: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

    function render_dp(purchaseinvoice_kd) {
		$("#idtxtpurchaseinvoice_kdUm").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?php echo base_url().$class_link ?>/get_invoice_dp',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramInv: params.term, // search term
						purchaseinvoice_kd: purchaseinvoice_kd
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function formdetail_diskon (id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/formdetail_diskon'; ?>',
			data: {id: id},
			success: function(html) {
				toggle_modal('Diskon', html);		
			}
		});
	}

	function dpform_main (id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/dpform_main'; ?>',
			data: {id: id},
			success: function(html) {
				toggle_modal('Uang Muka', html);
				render_dp(id);
			}
		});
	}

	function formdetail_edit (id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/formdetail_edit'; ?>',
			data: {id: id},
			success: function(html) {
				toggle_modal('Edit', html);
			}
		});
	}

	function ppnform_main (id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/ppnform_main'; ?>',
			data: {id: id},
			success: function(html) {
				toggle_modal('PPN', html);
			}
		});
	}

    function table_rmgrdetail (rmgr_nosrj, po_kd, id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/table_rmgrdetail'; ?>',
			data: {rmgr_nosrj:rmgr_nosrj, po_kd: po_kd, id: id},
			success: function(html) {
				$('#idtableRmgr').html(html);
				render_icheck ('icheck');
			}
		});
	}

    function tabledetail_main (id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/tabledetail_main'; ?>',
			data: {id: id},
			success: function(html) {
                $('#<?php echo $box_tablecontent_id; ?>').html(html);
			}
		});
	}

	function dptable_main (id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/dptable_main'; ?>',
			data: {id: id},
			success: function(html) {
                $('#idtabledp').html(html);
			}
		});
	}

	function view_pph (purchaseinvoicedetail_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/view_pph'; ?>',
			data: {purchaseinvoicedetail_kd: purchaseinvoicedetail_kd},
			success: function(html) {
				toggle_modal('PPh', html);		
			}
		});
	}

    function pilihsrj () {
        event.preventDefault();
        let rmgr_nosrj = $('#idtxtrmgr_nosrj').val();
        let id = $('#idtxtpurchaseinvoicedetail_kd').val();
        let po_kd = $('#idtxtpo_kd').val();
        table_rmgrdetail (rmgr_nosrj, po_kd, id);
    }

	function render_icheck (classElement) {
		$('input[type="checkbox"].'+classElement).iCheck({
			checkboxClass: 'icheckbox_flat-blue'
		})
	}

	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function submitDataBatch (form_id) {
		event.preventDefault();
		var id_form = document.getElementById(form_id);
        let url = "<?php echo base_url().$class_link; ?>/action_insert_batch";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(id_form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					$('.errInput').html('');
					$('#idtableRmgr').html('');
					$('#idtxtrmgr_nosrj').val('').trigger('change');
					tabledetail_main ('<?php echo $id; ?>');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function submitDataDetail (form_id) {
		event.preventDefault();
		var id_form = document.getElementById(form_id);
        let url = "<?php echo base_url().$class_link; ?>/action_insert_detail";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(id_form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					$('.errInput').html('');
					if (form_id == 'idformItemEdit') {
						toggle_modal('', '');
					}else{
						document.getElementById(form_id).reset();
					}
					tabledetail_main ('<?php echo $id; ?>');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function submitDataDiskon (form_id) {
		event.preventDefault();
		var id_form = document.getElementById(form_id);
        let url = "<?php echo base_url().$class_link; ?>/action_update_diskon";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(id_form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					$('.errInput').html('');
					tabledetail_main ('<?php echo $id; ?>');
					toggle_modal('', '');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function submitDataPPN (form_id) {
		event.preventDefault();
		var id_form = document.getElementById(form_id);
        let url = "<?php echo base_url().$class_link; ?>/action_update_ppn";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(id_form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					$('.errInput').html('');
					tabledetail_main ('<?php echo $id; ?>');
					toggle_modal('', '');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function submitDataUM (form_id) {
		event.preventDefault();
		var id_form = document.getElementById(form_id);
        let url = "<?php echo base_url().$class_link; ?>/action_update_um";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(id_form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					$('.errInput').html('');
					tabledetail_main ('<?php echo $id; ?>');
					dptable_main ('<?php echo $id; ?>');
					$('#idtxtpurchaseinvoice_kdUm').val('').trigger('change');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function action_hapus_diskon (id) {
		event.preventDefault();
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_hapus_diskon'; ?>',
				data: {id: id},
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						tabledetail_main ('<?php echo $id; ?>');
						toggle_modal('', '');
					}else{
						notify (resp.status, resp.pesan, 'error');
					}
				}
			});
		}
	}

	function hapus_data_detail (id) {
		event.preventDefault();
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_hapus_detail'; ?>',
				data: {id: id},
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						tabledetail_main ('<?php echo $id; ?>');
					}else{
						notify (resp.status, resp.pesan, 'error');
					}
				}
			});
		}
	}

	function action_hapus_um (id) {
		event.preventDefault();
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_hapus_um'; ?>',
				data: {id: id},
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						dptable_main ('<?php echo $id; ?>');
						tabledetail_main ('<?php echo $id; ?>');
					}else{
						notify (resp.status, resp.pesan, 'error');
					}
				}
			});
		}
	}

	function action_update_grandtotal (id) {
		let grandtotal = $('#idtxtgrandtotal').val();
		event.preventDefault();
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_update_grandtotal'; ?>',
				data: {id: id, grandtotal: grandtotal},
				success: function(data) {
					console.log(data);
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						window.location.assign('<?php echo base_url().$class_link?>');
					}else{
						notify (resp.status, resp.pesan, 'error');
					}
				}
			});
		}
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

</script>