<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	form_master_main('<?php echo $sts; ?>', '<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		form_box('add', '');
	});

	$(document).off('select2:selecting', '#idtxtsuplier_kd').on("select2:selecting", '#idtxtsuplier_kd', function(e) { 
		let dt = e.params.args.data;
		let sts = $('#idtxtStsMaster').val();
		let purchaseinvoice_tanggal = $('#idtxtpurchaseinvoice_tanggal').val();
		if (sts != 'edit') {
			$('#idtxtpurchaseinvoice_termpayment').val(dt.suplier_termpayment);
			if (dt.suplier_includeppn == 'T') {
				$('#idtxtpurchaseinvoice_ppn').val(10);
			}else{
				$('#idtxtpurchaseinvoice_ppn').val('');
			}
		}
		render_srj(dt.id);
		calc_date_terpayment(sts, purchaseinvoice_tanggal, dt.suplier_termpayment)
	});

	
	$(document).off('change', '#idtxtpurchaseinvoice_jenis').on("change", '#idtxtpurchaseinvoice_jenis', function(e) { 
		if($(this).val() == 'dp'){
			$('#ap_po').css('display', 'block');
		}else{
			$('#ap_po').css('display', 'none');
		};
	});

	$(document).off('keyup change', '.cl-termpayment').on('keyup change', '.cl-termpayment', function() {
		let purchaseinvoice_tanggal = $('#idtxtpurchaseinvoice_tanggal').val();
		let purchaseinvoice_termpayment = $('#idtxtpurchaseinvoice_termpayment').val();
		let sts = $('#idtxtStsMaster').val();
		calc_date_terpayment(sts, purchaseinvoice_tanggal, purchaseinvoice_termpayment);
	});
	
	function calc_date_terpayment(sts, purchaseinvoice_tanggal, purchaseinvoice_termpayment) {
		if (sts != 'edit') {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/get_date_termpayment'; ?>',
				data: {sts:sts, purchaseinvoice_tanggal:purchaseinvoice_tanggal, purchaseinvoice_termpayment:purchaseinvoice_termpayment},
				success: function(data) {
					var resp = JSON.parse(data);
					$('#idtxtpurchaseinvoice_tanggaltermpayment').val(resp.termpayment_date);
				}
			});
		}
	}

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function form_master_main(sts, id) {
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url().$class_link.'/form_master_main'; ?>',
            data: {sts:sts, id:id},
            success: function(html) {
				
                $('#<?php echo $box_content_id; ?>').html(html);
                render_datetimepicker('datetimepicker');
				if($('#idtxtpurchaseinvoice_jenis').val() == 'dp'){
					$('#ap_po').css('display', 'block');
				}
                $('[data-mask]').inputmask();
                moveTo('idMainContent');
            }
        });
	}

	function render_suplier() {
		$("#idtxtsuplier_kd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 2,
			ajax: { 
				url: '<?php echo base_url() ?>/Auto_complete/get_suplier',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramSuplier: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_pph_SAP() {
		
		$.ajax({
			type: "POST",
			url: "<?= url_api_sap() ?>Search",
			data: {
					"CustomQuery": "GetPPH"
					},
			dataType: "JSON",
			success: function (response) {
				var items = '';
				for (let index = 0; index < response.length; index++) {
					items += '<option value="' + response[index].WTCode + '">  '+  response[index].WTCode + ' || ' + response[index].WTName +' </option>';
				}

				$('#idtxtpph_SAP').append(items);
				// $("#idtxtpph_SAP").select2({
				// 	theme: 'bootstrap'
				// });
			}
		});
	}
	function render_srj(suplier_kd) {
		$("#idtxtno_po_dp").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?= base_url().$class_link ?>/get_grby_suratjalan',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramSRJ: params.term, // search term
						suplier_kd: suplier_kd,
						pi_jenis : 'dp'
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

    function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

    function submitDataMaster(form_id) {
		event.preventDefault();
		var id_form = document.getElementById(form_id);
        let url = "<?php echo base_url().$class_link; ?>/action_insert_master";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(id_form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					$('.errInput').html('');
					if($('#idtxtpurchaseinvoice_jenis').val() == 'dp'){
						location.reload();
					}else{
						window.location.assign('<?php echo base_url().$class_link?>/formdetail_box?id='+resp.data.purchaseinvoice_kd);
					}
					
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}



</script>