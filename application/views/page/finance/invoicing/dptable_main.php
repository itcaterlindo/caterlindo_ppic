<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body table-responsive no-padding">
<table id="idTable" class="table table-bordered table-striped table-hover display" style="width:100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;">No.</th>
		<th style="width:4%; text-align:center;">No Invoice</th>
		<th style="width:5%; text-align:center;">Total</th>
		<th style="width:5%; text-align:center;">Opsi</th>
	</tr>
	</thead>
    <tbody>
    <?php 
    $no = 1; $jmlUM = 0;
    foreach ($result as $r) :
        $jmlUM += $r['grandTotal'];
    ?>
    <tr>
        <td><?php echo $no; ?></td>
        <td><?php echo $r['purchaseinvoice_no']; ?></td>
        <td class="dt-right"><?php echo custom_numberformat($r['grandTotal'], 2, 'IDR'); ?></td>
        <td class="dt-center"><button class="btn btn-xs btn-danger" onclick="action_hapus_um('<?php echo $r['purchaseinvoice_kd']?>')"> <i class="fa fa-trash"></i> </button></td>
    </tr>
    <?php
    $no++;
    endforeach;
    ?>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="2" style="font-weight: bold; text-align: right;">Jumlah :</td>
        <td style="font-weight: bold; text-align: right;"><?php echo custom_numberformat($jmlUM, 2, 'IDR'); ?></td>
    </tr>
    </tfoot>
</table>
</div>
<script type="text/javascript">
	
</script>