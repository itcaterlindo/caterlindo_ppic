<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<table id="idTableDetail" class="table table-bordered table-striped " style="width:100%; font-size: 90%">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;">No.</th>
		<th style="width:1%; text-align:center;">Opsi</th>
		<th style="width:4%; text-align:center;">No PO</th>
		<th style="width:4%; text-align:center;">No Srj</th>
		<th style="width:5%; text-align:center;">Kode Barang</th>
		<th style="width:15%; text-align:center;">Deskripsi Barang</th>
		<th style="width:4%; text-align:center;">Satuan</th>
		<th style="width:4%; text-align:center;">PPh</th>
		<th style="width:7%; text-align:center;">Harga PO</th>
		<th style="width:5%; text-align:center;">Harga Invoice</th>
		<th style="width:5%; text-align:center;">Qty</th>
		<th style="width:7%; text-align:center;">Total Harga</th>
	</tr>
	</thead>
    <tbody>
        <?php 
        $no = 1;
        $subtotal = 0; $total = 0;
        foreach ($result as $e): 
            $subtotal = $e['purchaseinvoicedetail_hargaunit'] * $e['purchaseinvoicedetail_qty'];
            $total += $subtotal;
            ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td><?php
                $btns = array();
                $btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'formdetail_edit(\''.$e['purchaseinvoicedetail_kd'].'\')'));
                $btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data_detail(\''.$e['purchaseinvoicedetail_kd'].'\')'));	
                $btn_group = group_btns($btns);
                echo $btn_group;
                ?></td>
                <td><?php echo $e['po_no']; ?></td>
                <td><?php echo $e['rmgr_nosrj']; ?></td>
                <td><?php echo $e['rm_kode'].'/'.$e['rm_oldkd']; ?></td>
                <td><?php echo $e['purchaseinvoicedetail_deskripsi'].'/'.$e['purchaseinvoicedetail_spesifikasi']; ?></td>
                <td><?php echo $e['rmsatuan_nama']; ?></td>
                <td> <a href="javascript:void(0);" onclick="view_pph('<?php echo $e['purchaseinvoicedetail_kd']?>')"><?php echo $e['pph_nama']; ?></a> </td>
                <td style="text-align: right;"><?php echo custom_numberformat($e['podetail_harga'], 2 , 'IDR'); ?></td>
                <td style="text-align: right;"><?php echo custom_numberformat($e['purchaseinvoicedetail_hargaunit'], 2 , 'IDR'); ?></td>
                <td style="text-align: right;"><?php echo $e['purchaseinvoicedetail_qty']; ?></td>
                <td style="text-align: right;"><?php echo custom_numberformat_round($subtotal, 2 , 'IDR'); ?></td>
            </tr>     
        <?php 
            $no++;
        endforeach;?>
    </tbody>
	<tfoot>
        <tr>
            <td colspan="11" style="text-align: right; font-weight: bold;">Total Harga :</td>
            <td style="text-align: right; font-weight: bold;"><?php echo custom_numberformat_round($total, 2, 'IDR'); ?></td>
        </tr>
        <tr>
            <td colspan="11" style="text-align: right; font-weight: bold;">
            <?php 
                $diskon = null;
                $diskonRp = 0;
                if ($master['purchaseinvoice_jenisdiskon'] == 'persentase'){
                    $diskon = $master['purchaseinvoice_diskonpersen'];
                    $diskonRp = $diskon / 100 * $total;
                }else{
                    $diskonRp = $master['purchaseinvoice_diskonnominal'];
                }
            ?>
                <?php if ($master['purchaseinvoice_jenis'] != 'dp') :?>
                <button class="btn btn-xs btn-default" onclick="formdetail_diskon('<?php echo $id; ?>')"> <i class="fa fa-plus"></i> </button>
                <?php endif;?>
                Diskon <?php echo ucwords($master['purchaseinvoice_jenisdiskon']); 
                if ($master['purchaseinvoice_jenisdiskon'] == 'persentase') {
                    echo '('.custom_numberformat_round($diskon, 0, '').'%)'; 
                }?> :
            </td>
            <td style="text-align: right; font-weight: bold;"><?php  
                echo !empty($diskonRp) ? custom_numberformat_round($diskonRp, 2, 'IDR') : 0;
            ?> </td>
        </tr>
        <tr>
            <?php
            $dp = 0;
            if (!empty($master['purchaseinvoice_dp'])) {
                $p = $master['purchaseinvoice_dp'];

                $dp = $p <= 0 ? $p * -1 : $p;
            }
            ?>
            <td colspan="11" style="text-align: right; font-weight: bold;">
                <?php if ($master['purchaseinvoice_jenis'] != 'dp') :?>
                <button class="btn btn-xs btn-default" onclick="dpform_main('<?php echo $id; ?>')"> <i class="fa fa-plus"></i> </button>
                <?php endif;?>
                Uang Muka :
            </td>
            <td style="text-align: right; font-weight: bold;"><?php  
                echo !empty($dp) ? custom_numberformat_round($dp, 2, 'IDR') : 0;
            ?></td>
        </tr>
        <tr>
            <td colspan="11" style="text-align: right; font-weight: bold;">
                DPP :
            </td>
            <td style="text-align: right; font-weight: bold;"><?php  
                $dpp = $total - $diskonRp - $dp;
                echo !empty($dpp) ? custom_numberformat_round($dpp, 2, 'IDR') : 0;
            ?></td>
        </tr>
        <tr>
            <?php 
            $ppn = 0; $ppnRp = 0; $ppntxt = '';
            if (!empty($master['purchaseinvoice_ppn'])){
                $ppnRp = $master['purchaseinvoice_ppn'] / 100 * $dpp;
                $ppntxt = '('.$master['purchaseinvoice_ppn'].'%)';
            }
            ?>
            <td colspan="11" style="text-align: right; font-weight: bold;">
                <button class="btn btn-xs btn-default" onclick="ppnform_main('<?php echo $id; ?>')"> <i class="fa fa-plus"></i> </button>
                PPN <?php echo $ppntxt; ?>:
            </td>
            <td style="text-align: right; font-weight: bold;">
            <?php  
                echo !empty($ppnRp) ? custom_numberformat_round($ppnRp, 2, 'IDR') : 0;
            ?>
            </td>
        </tr>
        <tr>
            <td colspan="11" style="text-align: right; font-weight: bold;">Grand Total :</td>
            <td style="text-align: right; font-weight: bold;"><?php 
            $grandTotal = $dpp + $ppnRp;
            echo !empty($grandTotal) ? custom_numberformat_round($grandTotal, 2, 'IDR') : 0 ; ?>
            <input type="hidden" id="idtxtgrandtotal" value="<?php echo $grandTotal; ?>">
            </td>
        </tr>
	</tfoot>
</table>

<button class="btn btn-sm btn-success pull-right" onclick="action_update_grandtotal('<?php echo $id; ?>')"> <i class="fa fa-check-circle-o"></i> Selesai </button>


<script type="text/javascript">
	// render_dt('#idTableGR');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
		});
	}
</script>