<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputMaster';
if($sts == 'edit'){
	extract($rowData);
}

?>

<?php
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpph_kd', 'name'=> 'txtpph_kd', 'value' => isset($id) ? $id: null ));
?>
<div class="row">
	<div class="col-md-12">
	<div class="form-group">
		<label for='idtxtpph_nama' class="col-md-2 control-label">Nama PPh</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpph_nama"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtpph_nama', 'id'=> 'idtxtpph_nama', 'placeholder' =>'PPh', 'value'=> isset($pph_nama) ? $pph_nama: null ));?>
		</div>
	</div>
	
    <div class="form-group">
		<label for='idtxtpph_persen' class="col-md-2 control-label">Persen (%)</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpph_persen"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtpph_persen', 'id'=> 'idtxtpph_persen', 'placeholder' =>'Persen', 'value'=> isset($pph_persen) ? $pph_persen: null ));?>
		</div>
	</div>
	</div>
</div>

<hr>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-1 col-xs-12 col-sm-offset-10">
			<button type="submit" name="btnSubmit" onclick="submitData('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>
