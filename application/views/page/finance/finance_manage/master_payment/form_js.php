<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	form_master_main('<?php echo $sts; ?>', '<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		form_box('add', '');
	});

	$(document).off('select2:selecting', '#idtxtsuplier_kd').on("select2:selecting", '#idtxtsuplier_kd', function(e) { 
		let dt = e.params.args.data;
		let sts = $('#idtxtStsMaster').val();
		let purchaseinvoice_tanggal = $('#idtxtpurchaseinvoice_tanggal').val();
		if (sts != 'edit') {
			$('#idtxtpurchaseinvoice_termpayment').val(dt.suplier_termpayment);
			if (dt.suplier_includeppn == 'T') {
				$('#idtxtpurchaseinvoice_ppn').val(10);
			}else{
				$('#idtxtpurchaseinvoice_ppn').val('');
			}
		}
		calc_date_terpayment(purchaseinvoice_tanggal, dt.suplier_termpayment)
	});

	$(document).off('keyup change', '.cl-termpayment').on('keyup change', '.cl-termpayment', function() {
		let purchaseinvoice_tanggal = $('#idtxtpurchaseinvoice_tanggal').val();
		let purchaseinvoice_termpayment = $('#idtxtpurchaseinvoice_termpayment').val();
		calc_date_terpayment(purchaseinvoice_tanggal, purchaseinvoice_termpayment);
	});
	
	function calc_date_terpayment(purchaseinvoice_tanggal, purchaseinvoice_termpayment) {
		$.ajax({
            type: 'GET',
            url: '<?php echo base_url().$class_link.'/get_date_termpayment'; ?>',
            data: {purchaseinvoice_tanggal:purchaseinvoice_tanggal, purchaseinvoice_termpayment:purchaseinvoice_termpayment},
            success: function(data) {
				var resp = JSON.parse(data);
				$('#idtxtpurchaseinvoice_tanggaltermpayment').val(resp.termpayment_date);
            }
        });
	}

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function form_master_main(sts, id) {
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url().$class_link.'/form_master_main'; ?>',
            data: {sts:sts, id:id},
            success: function(html) {
                $('#<?php echo $box_content_id; ?>').html(html);
                render_datetimepicker('datetimepicker');
                $('[data-mask]').inputmask();
                moveTo('idMainContent');
            }
        });
	}

	function render_suplier() {
		$("#idtxtsuplier_kd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 2,
			ajax: { 
				url: '<?php echo base_url() ?>/Auto_complete/get_suplier',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramSuplier: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

    function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

    function submitDataMaster(form_id) {
		event.preventDefault();
		var id_form = document.getElementById(form_id);
        let url = "<?php echo base_url().$class_link; ?>/action_insert_master";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(id_form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					$('.errInput').html('');
					window.location.assign('<?php echo base_url().$class_link?>/formdetail_box?id='+resp.data.purchaseinvoice_kd);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}



</script>