<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputSendback';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));

?>

<style type="text/css">
	.select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>

<div class="row">
	<div class="form-group">
		<label for='idtxtrmgr_tgldatangSendback' class="col-md-2 control-label">Tgl Datang</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrrmgr_tgldatangSendback"></div>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				<?php echo form_input(array('type' => 'text', 'id'=> 'idtxtrmgr_tgldatangSendback', 'name' => 'txtrmgr_tgldatangSendback', 'class'=> 'form-control datetimepicker', 'placeholder' => 'Tgl Datang', 'value' => date('d-m-Y H:i:s'))); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtrmgr_nosrjSendback' class="col-md-2 control-label">No & Tgl Surat Jalan</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrrmgr_nosrjSendback"></div>
			<?php echo form_input(array('type' => 'text', 'id'=> 'idtxtrmgr_nosrjSendback', 'name' => 'txtrmgr_nosrjSendback', 'class'=> 'form-control', 'placeholder' => 'No Surat Jalan')); ?>
		</div>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrrmgr_tglsrjSendback"></div>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				<?php echo form_input(array('type' => 'text', 'id'=> 'idtxtrmgr_tglsrjSendback', 'name' => 'txtrmgr_tglsrjSendback', 'class'=> 'form-control datepicker', 'placeholder' => 'Tgl Surat Jalan', 'value' => date('d-m-Y'))); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtwhdeliveryorder_kd' class="col-md-2 control-label">No DO</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrwhdeliveryorder_kd"></div>
			<select id="idtxtwhdeliveryorder_kd" class="form-control"> </select>
		</div>
	</div>
	
</div>

<div class="row">
	<div class="col-md-12">
		<div id="idtablewhdodetail"></div>
	</div>
</div>


<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-2 col-sm-offset-10 col-xs-12">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMainSendback" onclick="submitDataSendback('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button type="reset" name="btnReset" onclick="resetFormMain()" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>

