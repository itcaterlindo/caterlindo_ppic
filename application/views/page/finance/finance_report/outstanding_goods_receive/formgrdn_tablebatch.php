<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputWODetail';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));

?>

<style type="text/css">
    .select2-container {
        width: 100% !important;
        padding: 0;
    }
</style>

<div class="col-md-12">
    <div id="idErrdn_kd"></div>
    <input type="hidden" name="txtdn_kd" value="<?php echo $dnheader['dn_kd']; ?>">

    <div class="row">
        <div class="form-group">
            <label for='idtxtrmgr_tgldatangDnwhrm' class="col-md-1 control-label">Tgl Datang</label>
            <div class="col-sm-3 col-xs-12">
                <div class="errInput" id="idErrrmgr_tgldatangDnwhrm"></div>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <?php echo form_input(array('type' => 'text', 'id' => 'idtxtrmgr_tgldatangDnwhrm', 'name' => 'txtrmgr_tgldatangDnwhrm', 'class' => 'form-control datetimepicker', 'placeholder' => 'Tgl Datang', 'value' => date('d-m-Y H:i:s'))); ?>
                </div>
            </div>
        </div>

        <h5> <strong> Detail <?php echo $dnheader['dn_no']; ?>: </strong></h5>
        <table border="1" style="width: 100%;">
            <thead>
                <tr style="font-weight: bold; text-align: center;">
                    <td>No</td>
                    <td>No WO</td>
                    <td>Item Code</td>
                    <td>Qty</td>
                    <td>Qty Received</td>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($dndetails as $dndetail) :
                ?>
                    <tr>
                        <td style="text-align: right;"><?php echo $no; ?></td>
                        <td><?php echo $dndetail['woitem_no_wo']; ?></td>
                        <td><?php echo $dndetail['woitem_itemcode']; ?></td>
                        <td style="text-align: right;"><?php echo $dndetail['dndetail_qty']; ?></td>
                        <td style="text-align: right;"><?php echo $dndetail['sum_dndetailreceived_qty']; ?></td>
                    </tr>
                <?php 
                    $no++;
                endforeach; ?>
            </tbody>
        </table>
    </div>

    <hr>

    <div class="row">
        <div class="form-group">
            <div class="col-sm-3 col-xs-12">
                <label for='idtxtrm_kdDN'>Raw Material</label>
                <select id="idtxtrm_kdDN" class="form-control"> </select>
            </div>
            <div class="col-sm-1 col-xs-12">
                <label for='idtxtqty_dn'>Qty</label>
                <input type="number" class="form-control" id="idtxtqty_dn" placeholder="Qty">
            </div>
            <div class="col-sm-2 col-xs-12">
                <label for='idtxtsatuan_dn'>Satuan</label>
                <input type="text" class="form-control" id="idtxtsatuan_dn" placeholder="Satuan" readonly="readonly">
            </div>
            <div class="col-sm-4 col-xs-12">
                <label for='idtxtket_dn'>Remark</label>
                <input type="text" class="form-control" id="idtxtket_dn" placeholder="Remark">
            </div>
            <div class="col-sm-1 col-xs-12">
                <br>
                <button class="btn btn-sm btn-success" onclick="addDNdetail()"> <i class="fa fa-plus"></i> Tambah </button>
            </div>
        </div>
    </div>

    <div class="row">
        <table id="idtbldetailrmdnwo" border="1" style="width: 100%;">
            <thead>
                <tr style="font-weight: bold; text-align: center;">
                    <td>RM Kode</td>
                    <td>RM Deskripsi</td>
                    <td>RM Qty</td>
                    <td>RM Satuan</td>
                    <td>Remark</td>
                    <td>Opsi</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach($dngrdetails as $dngrdetail) :?>
                    <tr>
                        <td><?php echo $dngrdetail['rm_kode']; ?></td>
                        <td><?php echo $dngrdetail['rm_deskripsi'].' '.$dngrdetail['rm_spesifikasi']; ?></td>
                        <td><?php echo $dngrdetail['rmgr_qty']; ?></td>
                        <td><?php echo $dngrdetail['rmsatuan_nama']; ?></td>
                        <td><?php echo $dngrdetail['rmgr_ket']; ?></td>
                        <td></td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>

    <hr>

    <div class="form-group">
        <div class="col-sm-2 col-sm-offset-1 col-xs-12">
            <button type="submit" name="btnSubmit" id="idbtnSubmitMainGRDN" onclick="submitDataGRDN('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
                <i class="fa fa-save"></i> Simpan
            </button>
            <button type="reset" name="btnReset" onclick="resetFormMain()" class="btn btn-default btn-sm">
                <i class="fa fa-refresh"></i> Reset
            </button>
        </div>
    </div>
</div>

<?php echo form_close(); ?>