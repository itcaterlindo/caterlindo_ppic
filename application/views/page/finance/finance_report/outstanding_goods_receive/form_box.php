<?php
defined('BASEPATH') or exit('No direct script access allowed!');

/* --SET DEFAULT PROPERTY UNTUK BOX-- */
$js_file = 'form_js';
$box_type = 'Form';
$box_title = 'Goods Receive Detail';
$data['master_var'] = 'GoodsReceiveDetail';
$data['class_link'] = $class_link;
$data['box_id'] = 'id'.$box_type.'Box'.$data['master_var'];
$data['btn_hide_id'] = 'id'.$box_type.'BtnBoxHide'.$data['master_var'];
$data['btn_add_id'] = 'id'.$box_type.'BtnBoxAdd'.$data['master_var'];
$data['btn_remove_id'] = 'id'.$box_type.'BtnBoxRemove'.$data['master_var'];
$data['box_alert_id'] = 'id'.$box_type.'BoxAlert'.$data['master_var'];
$data['box_loader_id'] = 'id'.$box_type.'BoxLoader'.$data['master_var'];
$data['box_content_id'] = 'id'.$box_type.'BoxContent'.$data['master_var'];
$data['box_overlay_id'] = 'id'.$box_type.'BoxOverlay'.$data['master_var'];
/* --END OF BOX DEFAULT PROPERTY-- */

/* --SET VARIABEL TAMBAHAN DISINI-- */
$data['box_contentSendback_id'] = 'id'.$box_type.'BoxContentSendback'.$data['master_var'];
$data['box_contentGRWO_id'] = 'id'.$box_type.'BoxGRWO'.$data['master_var'];
/* --END OF CUSTOM PROPERTY-- */

/* --SET BUTTONS DISINI, NILAI "FALSE" UNTUK TIDAK TAMPIL, NILAI "TRUE" UNTUK MENAMPILKAN BUTTON-- */
$btn_hide = TRUE;
$btn_add = FALSE;
$btn_remove = FALSE;
/* --END OF BUTTONS SETUP-- */
?>

<div class="" id="<?php echo $data['box_id']; ?>">
	<?php $this->load->view('page/'.$class_link.'/'.$js_file, $data); ?>
</div>
<?php //$this->load->view('page/'.$class_link.'/table_box', $data); ?>