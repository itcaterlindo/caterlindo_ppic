<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form();
	open_formSendback();
	open_form_grdn();
	open_table_box('<?php echo date('Y-m-d'); ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_main'; ?>',
			data: {sts:'add', id:''},
			success: function(html) {
			}
		});
	});

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		window.location.assign('<?php echo base_url();?>/purchasing/purchase_manage/purchasing_manage_main');		
	});

	// $(document).off('keyup', '#idFormInput').on('keyup', '#idFormInput', function(e) {
	// 	if (e.which == 13) {
	// 		submitData();
	// 		return false;
	// 	}
	// });

	$(document).off('select2:select', '#idtxtpo_kd').on('select2:select', '#idtxtpo_kd', function() {
		event.preventDefault();
		var po_kd = $(this).val();
		if (po_kd == ''){
			alert('Pilih PO');
		}else{
			box_overlay('in');
			open_table_podetail (po_kd);
			box_overlay('out');
		}
	});

	$(document).off('select2:select', '#idtxtwhdeliveryorder_kd').on('select2:select', '#idtxtwhdeliveryorder_kd', function() {
		event.preventDefault();
		let whdeliveryorder_kd = $(this).val();
		if (whdeliveryorder_kd == ''){
			alert('Pilih DO');
		}else{
			box_overlay('in');
			open_table_whdodetail (whdeliveryorder_kd);
			box_overlay('out');
		}
	});

	var dndetail_rm_kd = ''; 
	var dndetail_rm_kode = ''; 
	var dndetail_rm_deskripsi = ''; 
	var dndetail_rmsatuan_nama = ''; 
	$(document).off('select2:select', '#idtxtrm_kdDN').on('select2:select', '#idtxtrm_kdDN', function(e) {
		var data = e.params.data;
		$('#idtxtsatuan_dn').val(data.rmsatuan_nama);
		$('#idtxtqty_dn').focus();
		dndetail_rm_kd = data.rm_kd;
		dndetail_rm_kode = data.rm_kode;
		dndetail_rm_deskripsi = data.rm_deskripsi + data.rm_spesifikasi
		dndetail_rmsatuan_nama = data.rmsatuan_nama;
	});


	$(document).off('select2:select', '#idtxtdn_kd').on('select2:select', '#idtxtdn_kd', function() {
		event.preventDefault();
		formgrdn_tablebatch($(this).val());
	});
	
	var seqDNdetail = 1;
	function addDNdetail () {
		event.preventDefault();
		var qty = $('#idtxtqty_dn').val();
		var remark = $('#idtxtket_dn').val();
		if (dndetail_rm_kode != '' || qty != '') {
			var rowHtml = '<tr>'+
				'<td>'+dndetail_rm_kode+
					'<input type="hidden" name="txtseqs[]" value="'+seqDNdetail+'">'+
					'<input type="hidden" name="txtrm_kds['+seqDNdetail+']" value="'+dndetail_rm_kd+'">'+
					'<input type="hidden" name="txtrmgr_qtys['+seqDNdetail+']['+dndetail_rm_kd+']" value="'+qty+'">'+
					'<input type="hidden" name="txtrmgr_kets['+seqDNdetail+']['+dndetail_rm_kd+']" value="'+remark+'">'+
				'</td>'+
				'<td>'+dndetail_rm_deskripsi+'</td>'+
				'<td style="text-align: right;">'+qty+'</td>'+
				'<td>'+dndetail_rmsatuan_nama+'</td>'+
				'<td>'+remark+'</td>'+
				'<td style="text-align: center;"><button class="btn btn-sm btn-danger btnremove"> <i class="fa fa-trash"></i> </button></td>'
				+'</tr>';
			$('#idtbldetailrmdnwo').append(rowHtml);
			seqDNdetail++;
		}else{
			notify ('Gagal', 'RM / Qty Kosong', 'error');			
		}

		/** Reset Form */
		$('#idtxtsatuan_dn').val('');
		$('#idtxtqty_dn').val('');
		$('#idtxtket_dn').val('');
		$('#idtxtrm_kdDN').val('').trigger('change');
		dndetail_rm_kode = '';
		dndetail_rm_deskripsi = '';
		qty = '';
		dndetail_rmsatuan_nama = '';
	}

	$(document).off('click', '.btnremove').on('click', '.btnremove', function() {
		$(this).closest('tr').remove();
	});

	function open_table_podetail (po_kd) {
		$('#idtablepo').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_podetail'; ?>',
				data: {po_kd:po_kd},
				success: function(html) {
					$('#idtablepo').html(html);
					$('#idtablepo').slideDown();
				}
			});
		});
	}

	function open_table_whdodetail (whdeliveryorder_kd) {
		$('#idtablewhdodetail').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_whdodetail'; ?>',
				data: {whdeliveryorder_kd:whdeliveryorder_kd},
				success: function(html) {
					$('#idtablewhdodetail').html(html);
					$('#idtablewhdodetail').slideDown();
				}
			});
		});
	}	

	function open_form(){
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/form_main'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					render_datetime('datepicker', 'DD-MM-YYYY');
					moveTo('idMainContent');
					render_po();
				}
			});
		});
	}

	function open_formSendback(){
		$('#<?php echo $box_contentSendback_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/formsendback_main'; ?>',
				success: function(html) {
					$('#<?php echo $box_contentSendback_id; ?>').html(html);
					$('#<?php echo $box_contentSendback_id; ?>').slideDown();
					render_datetime('datetimepicker', 'DD-MM-YYYY HH:mm:ss');
					render_datetime('datepicker', 'DD-MM-YYYY');
					moveTo('idMainContent');
					render_do_sendback();
				}
			});
		});
	}

	function open_form_grdn(){
		$('#<?php echo $box_contentGRWO_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/formgrdn_main'; ?>',
				success: function(html) {
					$('#<?php echo $box_contentGRWO_id; ?>').html(html);
					$('#<?php echo $box_contentGRWO_id; ?>').slideDown();
					render_deliverywhrm();
				}
			});
		});
	}

	function formgrdn_tablebatch(dn_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/formgrdn_tablebatch'; ?>',
			data: {dn_kd: dn_kd},
			success: function(html) {
				$('#idtablewhdndetail').html(html);
				render_datetime('datetimepicker', 'DD-MM-YYYY HH:mm:ss');
				render_rawmaterial();
			}
		});
	}

	function render_po() {
		$("#idtxtpo_kd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 2,
			ajax: { 
				url: '<?= base_url().$class_link ?>/get_po_param',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramPO: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_rawmaterial() {
		$('#idtxtrm_kdDN').select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 2,
			ajax: {
				url: '<?php echo base_url() ?>auto_complete/get_raw_material',
				type: "get",
				dataType: 'json',
				delay: 500,
				data: function(params) {
					return {
						paramRM: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}


	function render_do_sendback () {
		$("#idtxtwhdeliveryorder_kd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 2,
			ajax: { 
				url: '<?= base_url().$class_link ?>/get_whdo',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramWhdo: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_deliverywhrm() {
		$("#idtxtdn_kd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 2,
			ajax: { 
				url: '<?= base_url().$class_link ?>/get_deliverywhrm',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramDnWhrm: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_datetime(valClass, valFormat){
		$('.'+valClass).datetimepicker({
            format: valFormat,
        });
	}

	function open_table_box(date) {
		$('#idTableBox<?php echo $master_var;?>').remove();
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_box'; ?>',
				data: {date:date},
				success: function(html) {
					$('#idMainContent').append(html);
				}
			});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitData() {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById('idFormInput');
		var po_kd = $('#idtxtpo_kd').val();

		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_insert_batch" ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					open_table_podetail (po_kd);
					open_table_box('<?php echo date('Y-m-d'); ?>');
					$('.errInput').html('');
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
					moveTo('idMainContent');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function submitDataSendback(form_id) {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById(form_id);

		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_insert_batch_sendback" ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					let whdeliveryorder_kd = $('#idtxtwhdeliveryorder_kd').val();
					open_table_whdodetail (whdeliveryorder_kd);
					open_table_box('<?php echo date('Y-m-d'); ?>');
					$('.errInput').html('');
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
					moveTo('idMainContent');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function submitDataGRDN(form_id) {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById(form_id);

		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_insert_batch_dn" ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					formgrdn_tablebatch($('#idtxtdn_kd').val());
					open_table_box('<?php echo date('Y-m-d'); ?>');
					$('.errInput').html('');
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
					moveTo('idMainContent');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function resetFormMain() {
		// $('.tt-input').val('');
		// $('#idtxtpodetail_kd').val('').trigger('change');
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
		box_overlay('out');
    }

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

</script>