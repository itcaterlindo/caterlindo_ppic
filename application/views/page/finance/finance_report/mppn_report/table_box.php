<?php
defined('BASEPATH') or exit('No direct script access allowed!');

/* --SET DEFAULT PROPERTY UNTUK BOX-- */
$js_file = 'table_js';
$box_type = 'Report';
$box_title = 'Report PPNM';
$data['master_var'] = 'PPNM';
$data['class_link'] = $class_link;
$data['box_id'] = 'id'.$box_type.'Box'.$data['master_var'];
$data['btn_hide_id'] = 'id'.$box_type.'BtnBoxHide'.$data['master_var'];
$data['btn_add_id'] = 'id'.$box_type.'BtnBoxAdd'.$data['master_var'];
$data['btn_remove_id'] = 'id'.$box_type.'BtnBoxRemove'.$data['master_var'];
$data['box_alert_id'] = 'id'.$box_type.'BoxAlert'.$data['master_var'];
$data['box_loader_id'] = 'id'.$box_type.'BoxLoader'.$data['master_var'];
$data['box_content_id'] = 'id'.$box_type.'BoxContent'.$data['master_var'];
$data['box_overlay_id'] = 'id'.$box_type.'BoxOverlay'.$data['master_var'];
/* --END OF BOX DEFAULT PROPERTY-- */

/* --SET VARIABEL TAMBAHAN DISINI-- */
/* --END OF CUSTOM PROPERTY-- */

/* --SET BUTTONS DISINI, NILAI "FALSE" UNTUK TIDAK TAMPIL, NILAI "TRUE" UNTUK MENAMPILKAN BUTTON-- */
$btn_hide = TRUE;
$btn_add = cek_permission('PURCHASEINVOICE_CREATE');
$btn_remove = FALSE;
/* --END OF BUTTONS SETUP-- */
?>
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
<div class="box box-primary" id="<?php echo $data['box_id']; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_title; ?></h3>
		<div class="box-tools pull-right">
			<?php
			if ($btn_add) :
				?>
				<button class="btn btn-box-tool" id="<?php echo $data['btn_add_id']; ?>" data-toggle="tooltip" title="Tambah Data">
					<i class="fa fa-plus"></i>
				</button>
				<?php
			endif;
			if ($btn_hide) :
				?>
				<button class="btn btn-box-tool btn-collapse" id="<?php echo $data['btn_hide_id']; ?>" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
					<i class="fa fa-minus"></i>
				</button>
				<?php
			endif;
			if ($btn_remove) :
				?>
				<button class="btn btn-box-tool btn-remove" id="<?php echo $data['btn_remove_id']; ?>" data-widget="remove" data-toggle="tooltip" title="Close">
					<i class="fa fa-times"></i>
				</button>
				<?php
			endif;
			?>
		</div>
	</div>
	<div class="box-body">
		<form role="form">
			<div class="row">
				<div class="form-group col-md-2">
					<label for="idstart" class="control-label">Bulan</label>
					<div class="input-group date">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
						<select name="filt_bulan" id="idbulan" class="form-control">
							<option value="ALL">ALL</option>
							<option value="01" <?php if(date("m")=='01'){ print ' selected'; }?> >Jan</option>
							<option value="02" <?php if(date("m")=='02'){ print ' selected'; }?> >Feb</option>
							<option value="03" <?php if(date("m")=='03'){ print ' selected'; }?> >Mar</option>
							<option value="04" <?php if(date("m")=='04'){ print ' selected'; }?> >Apr</option>
							<option value="05" <?php if(date("m")=='05'){ print ' selected'; }?> >Mei</option>
							<option value="06" <?php if(date("m")=='06'){ print ' selected'; }?> >Jun</option>
							<option value="07" <?php if(date("m")=='07'){ print ' selected'; }?> >Jul</option>
							<option value="08" <?php if(date("m")=='08'){ print ' selected'; }?> >Ags</option>
							<option value="09" <?php if(date("m")=='09'){ print ' selected'; }?> >Sep</option>
							<option value="10" <?php if(date("m")=='10'){ print ' selected'; }?> >Okt</option>
							<option value="11" <?php if(date("m")=='11'){ print ' selected'; }?> >Nov</option>
							<option value="12" <?php if(date("m")=='12'){ print ' selected'; }?> >Des</option>
						</select>
					</div>
				</div>
				<!-- <div class="form-group col-md-2">
					<label for="idend" class="control-label">Tahun</label>
					<div class="input-group date">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
						<input type="text" name="tahun" value="" id="idtahun" class="form-control datetimepicker" placeholder="Tahun">
					</div>
				</div> -->

				<div class="form-group col-md-2">
					<label for="idend" class="control-label">Tahun</label>
					<div class="input-group date">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
						<?php echo form_input(array('type'=>'text', 'class'=> 'form-control tahun', 'name'=> 'txtcreated_payment', 'id'=> 'id_tahun', 'placeholder' =>'Tahun', 'value'=> isset($tanggal_input) ? $tanggal_input: null ));?>
					</div>
				</div>

				<!-- <div class="form-group col-md-1">
					<br>
					<button class="btn btn-default btn-sm" id="view_filter"> <i class="fa fa-search"></i> Tampilkan</button>
				</div> -->
			</div>
		</form>
	</div>
	<div class="box-body">
		<div id="<?php echo $data['box_alert_id']; ?>"></div>
		<div id="<?php echo $data['box_loader_id']; ?>" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="<?php echo $data['box_content_id']; ?>"></div>
	</div>
	<div class="box-footer">
	</div>
	<div class="overlay" id="<?php echo $data['box_overlay_id']; ?>" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
	<?php $this->load->view('page/'.$class_link.'/'.$js_file, $data); ?>
</div>