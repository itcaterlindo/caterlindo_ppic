<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body table-responsive no-padding">
<table id="idTable" class="table table-bordered table-striped table-hover display nowrap" style="width:100%; font-size: 95%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:5%; text-align:center;">Sup Code</th>
		<th style="width:7%; text-align:center;">Sup Nm</th>
		<th style="width:7%; text-align:center;">Bank Nm</th>
		<th style="width:7%; text-align:center;">No Rek</th>
		<th style="width:17%; text-align:center;">Sup NPWP</th>
		<th style="width:17%; text-align:center;">No PO</th>
		<th style="width:17%; text-align:center;">Faktur</th>
		<th style="width:17%; text-align:center;">Inv No</th>
		<th style="width:17%; text-align:center;">No Srj</th>
		<th style="width:7%; text-align:center;">Tgl Inv</th>
		<th style="width:7%; text-align:center;">Dpp</th>
		<th style="width:7%; text-align:center;">Ppn</th>
		<th style="width:17%; text-align:center;">Nm Brg</th>
		<th style="width:17%; text-align:center;">Tgl Payment</th>
		<th style="width:17%; text-align:center;">Rek Bank</th>
	</tr>
	</thead>
</table>
</div>
<script type="text/javascript">
	var bulan = '<?php echo $bulan ?>';
	var tahun = '<?php echo $tahun ?>';
	$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};
	var table = $('#idTable').DataTable({
		"processing": true,
		"ordering" : true,
		"scrollX": true,
		"dom": 'Bfrtip',
        "buttons": [
            'copy', 'csv', 'excel', 'pdf'
        ],
		"ajax": "<?php echo base_url().$class_link.'/table_data'; ?>" + '?bulan='+ bulan +'&tahun=' + tahun,
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"columnDefs": [
			{"data": null, "searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
			{"className": "dt-center", "targets": 1},
			{"className": "dt-center", "targets": 2},
			{"className": "dt-center", "targets": 3},
			{"className": "dt-center", "targets": 4},
			{"className": "dt-center", "targets": 5},
			{"className": "dt-center", "targets": 6},
			{"className": "dt-center", "targets": 7},
			{"className": "dt-center", "targets": 8},
			{"className": "dt-center", "targets": 9},
			{"className": "dt-center", "targets": 10},
			{"className": "dt-center", "targets": 11},
			{"className": "dt-center", "targets": 12},
			{"className": "dt-center", "targets": 13},
			{"className": "dt-center", "targets": 14},
			{"className": "dt-center", "targets": 15},
		],
		"order":[11, 'desc'],
		"rowCallback": function (row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			var index = page * length + (iDisplayIndex + 1);
			$('td:eq(0)', row).html(index);
		}
	});
</script>