<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}
</style>
<div class="box-body table-responsive no-padding">
	<table id="idTable" class="table table-bordered table-striped" style="width:100%; font-size: 95%;">
		<thead>
			<tr>
				<th style="width:1%; text-align:center;" class="all">No.</th>
				<th style="width:5%; text-align:center;">Tgl Invoice</th>
				<th style="width:5%; text-align:center;">Tgl Payment</th>
				<th style="width:5%; text-align:center;">No Invoice</th>
				<th style="width:5%; text-align:center;">Supplier</th>
				<th style="width:5%; text-align:center;">No PO</th>
				<th style="width:8%; text-align:center;">Nama PPh</th>
				<th style="width:8%; text-align:center;">PPh</th>
				<th style="width:8%; text-align:center; display: none;">PPh</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$no = 1;
			$sumPphNominal = 0;
			foreach ($result as $r) :
				$sumPphNominal += abs($r['purchaseinvoicedetail_pphnominal']);
			?>
				<tr>
					<td><?php echo $no; ?></td>
					<td><?php echo $r['purchaseinvoice_tanggal']; ?></td>
					<td><?php echo $r['purchaseinvoice_tanggaltermpayment']; ?></td>
					<td><?php echo "<a href='javascript:void(0);' onclick=window.open('" . base_url() . "finance/invoicing/view_box?id={$r['purchaseinvoice_kd']}')>{$r['purchaseinvoice_no']}</a> ; " ?></td>
					<td><?php echo $r['suplier_kode'] . ' / ' . $r['suplier_nama']; ?></td>
					<td><?php echo $r['po_no']; ?></td>
					<td><?php echo $r['pph_nama']; ?></td>
					<td style="text-align: right;"><?php echo custom_numberformat(abs($r['purchaseinvoicedetail_pphnominal']), 2, 'IDR'); ?></td>
					<td style="display: none;"> <?php echo abs($r['purchaseinvoicedetail_pphnominal']); ?></td>
				</tr>
			<?php
				$no++;
			endforeach;
			?>
		</tbody>
		<tfoot>
			<tr style="font-weight: bold; font-size: 125%;">
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td class="dt-right">Total :</td>
				<td class="dt-right"><?php echo custom_numberformat($sumPphNominal, 2, 'IDR') ?></td>
			</tr>
		</tfoot>
	</table>
</div>
<script type="text/javascript">
	renderdt();

	function renderdt() {
		$("#idTable").DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"ordering": false,
			"searching": true,
			"language": {
				"lengthMenu": "Tampilkan _MENU_ data",
				"zeroRecords": "Maaf tidak ada data yang ditampilkan",
				"info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty": "Tidak ada data yang ditampilkan",
				"search": "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing": "Sedang Memproses...",
				"paginate": {
					"first": '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last": '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next": '<span class="glyphicon glyphicon-forward"></span>',
					"previous": '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons": [{
				"extend": "excel",
				"title": "Report PPh Invoicing",
				"footer": true,
				"exportOptions": {
					"columns": [0, 1, 2, 3, 4, 5, 6, 8]
				}
			}],
		});
	}
</script>