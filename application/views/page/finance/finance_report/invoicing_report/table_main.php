<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body table-responsive no-padding">
<table id="idTable" class="table table-bordered table-striped" style="width:100%; font-size: 95%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:5%; text-align:center;">Supplier</th>
		<th style="width:5%; text-align:center;">No Invoice</th>
		<th style="width:5%; text-align:center;">No PO</th>
		<th style="width:8%; text-align:center;">Total Harga</th>
		<th style="width:8%; text-align:center;">PPh</th>
		<th style="width:8%; text-align:center;">Harga Netto</th>
		<th style="width:5%; text-align:center; display: none;">TotalHarga</th>
		<th style="width:5%; text-align:center; display: none;">PPh</th>
		<th style="width:5%; text-align:center; display: none;">HargaNetto</th>
	</tr>
	</thead>
	<tbody>
	<?php 
	$no = 1;
	$grandSubtotal = 0; $grandSubtotalPph = 0; $grandSubtotalNetto = 0;
	foreach($result as $r => $element) :
		$sumSubtotal = 0; $sumSubtotalPph = 0; $sumNetto = 0;?>
	<tr style="font-weight: bold; font-size: 125%;">
		<td colspan="9"><?php $exp = explode('/', $r); 
		echo "Periode $exp[0] Dari $exp[1] s/d $exp[2]";
		?></td>
		<td style="display: none;"></td>
		<td style="display: none;"></td>
		<td style="display: none;"></td>
		<td style="display: none;"></td>
		<td style="display: none;"></td>
		<td style="display: none;"></td>
		<td style="display: none;"></td>
		<td style="display: none;"></td>
		<td style="display: none;"></td>
	</tr>
		<?php
		foreach ($element as $e) :
			$sumSubtotal += $e['sumGrandtotal'];
			$sumSubtotalPph += $e['sumPurchaseinvoicedetail_pphnominal'];
			$netto = $e['sumGrandtotal'];
			if ($e['sumPurchaseinvoicedetail_pphnominal'] <= 0) {
				$netto = $e['sumGrandtotal'] + $e['sumPurchaseinvoicedetail_pphnominal'];
			}
			$sumNetto += $netto;
		?>
	<tr>
		<td><?php echo $no; ?></td>
		<td><?php echo $e['suplier_kode'].' / '.$e['suplier_nama']; ?></td>
		<td><?php 
		$expInvNo = explode (' ; ', $e['purchaseinvoice_no']);
		$expInvKd = explode (' ; ', $e['purchaseinvoice_kd']);
		for ($i=0; $i<count($expInvNo); $i++){
			echo "<a href='javascript:void(0);' onclick=view_invoice('$expInvKd[$i]')>$expInvNo[$i]</a> ; ";
		}
		 ?>
		</td>
		<td><?php echo $e['po_unique']; ?></td>
		<td class="dt-right"><?php echo custom_numberformat($e['sumGrandtotal'], 2, 'IDR'); ?></td>
		<td class="dt-right"><?php echo !empty($e['sumPurchaseinvoicedetail_pphnominal']) ? custom_numberformat($e['sumPurchaseinvoicedetail_pphnominal'], 2, 'IDR') : 0; ?></td>
		<td class="dt-right"><?php echo custom_numberformat($netto, 2, 'IDR'); ?></td>
		<td class="dt-right" style="display:none;"><?php echo $e['sumGrandtotal']; ?></td>
		<td class="dt-right" style="display:none;"><?php echo !empty($e['sumPurchaseinvoicedetail_pphnominal']) ?$e['sumPurchaseinvoicedetail_pphnominal'] : 0 ; ?></td>
		<td class="dt-right" style="display:none;"><?php echo $netto; ?></td>
	</tr>
	<?php 
		$no++;
		endforeach;
		?>
	<tr style="font-weight: bold;">
		<td></td>
		<td></td>
		<td></td>
		<td class="dt-right">SubTotal (<?php echo "Periode $exp[0]"?>) :</td>
		<td class="dt-right"><?php echo custom_numberformat($sumSubtotal, 2, 'IDR')?></td>
		<td class="dt-right"><?php echo custom_numberformat($sumSubtotalPph, 2, 'IDR')?></td>
		<td class="dt-right"><?php echo custom_numberformat($sumNetto, 2, 'IDR')?></td>
		<td style="display: none;"><?php echo $sumSubtotal; ?></td>
		<td style="display: none;"><?php echo $sumSubtotalPph; ?></td>
		<td style="display: none;"><?php echo $sumNetto; ?></td>
	</tr>
		<?php
		$grandSubtotal += $sumSubtotal;
		$grandSubtotalPph += $sumSubtotalPph;
		$grandSubtotalNetto += $sumNetto;
	endforeach; ?>
	</tbody>
	<tfoot>
		<tr style="font-weight: bold; font-size: 125%;">
			<td></td>
			<td></td>
			<td></td>
			<td class="dt-right">Total :</td>
			<td class="dt-right"><?php echo custom_numberformat($grandSubtotal, 2, 'IDR')?></td>
			<td class="dt-right"><?php echo custom_numberformat($grandSubtotalPph, 2, 'IDR')?></td>
			<td class="dt-right"><?php echo custom_numberformat($grandSubtotalNetto, 2, 'IDR')?></td>
			<td style="display: none;"><?php echo $grandSubtotal; ?></td>
			<td style="display: none;"><?php echo $grandSubtotalPph; ?></td>
			<td style="display: none;"><?php echo $grandSubtotalNetto; ?></td>
		</tr>
	</tfoot>
</table>
</div>
<script type="text/javascript">
	renderdt();

	function renderdt() {
        $("#idTable").DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"ordering": false,
			"searching": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"title" : "Report Invoicing",
				"footer" : true,
				"exportOptions": {
					"columns": [ 0,1,2,3,7,8,9 ]
				}
			}],
		});
    }
</script>