<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">

	$(document).ready(function () {
		render_datetimepicker_yy('tahun');
		open_filter()
	});
	
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		form_box('add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_filter(){
		$('#id_tahun').on('dp.change', function(e){ run_filter() })
		$('#idbulan').change(function(){
			run_filter() 
		})
	}

	function run_filter(){
			var bulan = $('#idbulan').val();
			var tahun = $('#id_tahun').val();
			console.log(bulan, tahun)
			open_table(bulan, tahun);
	}

	function open_table(bulan, tahun) {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link?>' + '/table_main?bulan='+ bulan +'&tahun=' + tahun,
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	

	function render_select2(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_datetimepicker(valClass){
		$('.'+valClass).datetimepicker({
			format: 'YYYY-MM-DD',
    	});
	}
	function render_datetimepicker_yy(valClass){
		$('.'+valClass).datetimepicker({
			format: 'YYYY',
    	});
	}


	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function remove_box() {
		$('#idFormBox<?php echo $master_var; ?>').remove();
	}

	function form_box(sts, id) {
		remove_box();
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_box'; ?>',
			data: {'sts': sts, 'id' : id},
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function edit_data(id){
		form_box('edit', id) 
	}

	function view_box(id) {
		var url = '<?php echo base_url().$class_link;?>/view_box?id='+id;
		window.location.assign(url);	

	}

	function action_hapus_master (id) {
		event.preventDefault();
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_hapus_master'; ?>',
				data: {id: id},
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						open_table();
					}else{
						notify (resp.status, resp.pesan, 'error');
					}
				}
			});
		}
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

</script>