<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body table-responsive no-padding">
<table id="idTable" border="1" cellpadding="10" style="width:100%;">
    <thead>
        <tr>
            <th style="width:3%; text-align:center;" class="all">No.</th>
            <th style="width:8%; text-align:center;">Tgl Invoice</th>
            <th style="width:10%; text-align:center;">Tgl Termpayment</th>
            <th style="width:8%; text-align:center;">No Invoice</th>
            <th style="width:10%; text-align:center;">No Faktur</th>
            <th style="width:15%; text-align:center;">Nama Suplier</th>
            <th style="width:10%; text-align:center;">Note</th>
            <th style="width:15%; text-align:center;">Tgl Update</th>
            <th style="width:10%; text-align:center;">Grand Total Invoice</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $no = 1;
        $subtotal = 0; $total = 0;
        foreach ($result as $each): 
            ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td style="text-align: center;"><?php echo format_date($each['purchaseinvoice_tanggal'], 'Y-m-d') ?></td>
                <td style="text-align: center;"><?php echo $each['purchaseinvoice_tanggaltermpayment'] ?></td>
                <td style="text-align: center;"><?php echo $each['purchaseinvoice_no']; ?></td>
                <td><?php echo $each['purchaseinvoice_faktur']; ?></td>
                <td style="text-align: center;"><?php echo $each['suplier_nama']; ?></td>
                <td><?php echo $each['purchaseinvoice_note']; ?></td>
			    <td style="text-align: center;"><?php echo format_date($each['purchaseinvoice_tanggal'], 'Y-m-d') ?></td>
                <td style="text-align: right;"><?php echo custom_numberformat($each['detail_grand_total'], 2 , 'IDR'); ?></td>
            </tr>     
        <?php 
            $no++;
        endforeach;?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="8" style="text-align: right; font-weight: bold;">Total Harga :</td>
            <td style="text-align: right; font-weight: bold;"><?php echo custom_numberformat($grand_totalnya[0]['Totalnya'], 2, 'IDR'); ?></td>
        </tr>
        <tr>
            <td colspan="8" style="text-align: right; font-weight: bold;">Lain-lain :</td>
            <td style="text-align: right; font-weight: bold;">-----------------------</td>
        </tr>
        <tr>
            <td colspan="8" style="text-align: right; font-weight: bold;">Grand Total :</td>
            <td style="text-align: right; font-weight: bold;"><?php 
            $grandTotal = 0;
            echo !empty($grand_totalnya[0]['Totalnya']) ? custom_numberformat($grand_totalnya[0]['Totalnya'], 2, 'IDR') : 0 ; ?>
            <input type="hidden" id="idtxtgrandtotal" value="<?php echo $grand_totalnya[0]['Totalnya']; ?>">
            </td>
        </tr>
	</tfoot>
</table>
</div>