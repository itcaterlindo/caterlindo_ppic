<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<table id="idTableDetail" class="table table-bordered table-striped " style="width:100%; font-size: 90%">
	<thead>
		<th style="width:3%; text-align:center;" class="all">No.</th>
        <th style="width:5%; text-align:center;">Opsi</th>
		<th style="width:8%; text-align:center;">Tgl Invoice</th>
		<th style="width:10%; text-align:center;">Tgl Termpayment</th>
		<th style="width:8%; text-align:center;">No Invoice</th>
		<th style="width:10%; text-align:center;">No Faktur</th>
		<th style="width:15%; text-align:center;">Nama Suplier</th>
		<th style="width:10%; text-align:center;">Note</th>
		<th style="width:15%; text-align:center;">Tgl Update</th>
        <th style="width:10%; text-align:center;">Grand Total Invoice</th>
	</tr>
	</thead>
    <tbody>
        <?php 
        $no = 1;
        $subtotal = 0; $total = 0;
        foreach ($result as $each): 
            ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td><?php
                $btns = array();
                $btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data_detail(\''.$each['detpayment_kd'].'\')'));	
                $btn_group = group_btns($btns);
                echo $btn_group;
                ?></td>
                <td style="text-align: center;"><?php echo format_date($each['purchaseinvoice_tanggal'], 'Y-m-d') ?></td>
                <td style="text-align: center;"><?php echo $each['purchaseinvoice_tanggaltermpayment'] ?></td>
                <td style="text-align: center;"><?php echo $each['purchaseinvoice_no']; ?></td>
                <td><?php echo $each['purchaseinvoice_faktur']; ?></td>
                <td style="text-align: center;"><?php echo $each['suplier_nama']; ?></td>
                <td><?php echo $each['purchaseinvoice_note']; ?></td>
			    <td style="text-align: center;"><?php echo format_date($each['purchaseinvoice_tgledit'], 'Y-m-d') ?></td>
                <td style="text-align: right;"><?php echo custom_numberformat_round($each['detail_grand_total'], 2 , 'IDR'); ?></td>
            </tr>     
        <?php 
            $no++;
        endforeach;?>
    </tbody>
	<tfoot>
        <tr>
            <td colspan="9" style="text-align: right; font-weight: bold;">Total Harga :</td>
            <td style="text-align: right; font-weight: bold;"><?php echo (count($grand_totalnya) != 0) ? custom_numberformat_round($grand_totalnya[0]['Totalnya'], 2, 'IDR') : 0 ; ?></td>
        </tr>
        <tr>
            <td colspan="9" style="text-align: right; font-weight: bold;">Lain-lain :</td>
            <td style="text-align: right; font-weight: bold;">-----------------------</td>
        </tr>
        <tr>
            <td colspan="9" style="text-align: right; font-weight: bold;">Grand Total :</td>
            <td style="text-align: right; font-weight: bold;"><?php 
            $grandTotal = 0;
            echo (count($grand_totalnya) != 0) ? custom_numberformat_round($grand_totalnya[0]['Totalnya'], 2, 'IDR') : 0 ; ?>
            <input type="hidden" id="idtxtgrandtotal" value="<?php echo (count($grand_totalnya) != 0) ? custom_numberformat_round($grand_totalnya[0]['Totalnya'], 2, 'IDR') : 0 ; ?>">
            </td>
        </tr>
	</tfoot>
</table>

<button class="btn btn-sm btn-success pull-right" onclick="action_update_grandtotal('<?php echo $id; ?>')"> <i class="fa fa-check-circle-o"></i> Selesai </button>


<script type="text/javascript">
	// render_dt('#idTableGR');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
		});
	}
</script>