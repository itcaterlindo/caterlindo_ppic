<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="col-md-12">
    <div class="box-body table-responsive no-padding">
    <table id="idTable" border="1" cellpadding="10" style="width:100%;">
        <thead>
        <tr>
            <th style="width:1%; text-align:center;">PPh</th>
            <th style="width:1%; text-align:center;">Potong payment</th>
            <th style="width:5%; text-align:center;">Persentase(%)</th>
            <th style="width:7%; text-align:center;">PPh</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php echo $row['pph_nama'] ?></td>
                <td><?php 
                $purchaseinvoicedetail_pphpotongpayment = 'Tidak';
                if ($row['purchaseinvoicedetail_pphpotongpayment'] == '1') {
                    $purchaseinvoicedetail_pphpotongpayment = 'Ya';
                }
                echo $purchaseinvoicedetail_pphpotongpayment;
                ?></td>
                <td class="dt-right"><?php echo $row['purchaseinvoicedetail_pphpersen'] ?></td>
                <td class="dt-right"><?php echo custom_numberformat($row['purchaseinvoicedetail_pphnominal'], 2, 'IDR') ?></td>
            </tr>
        </tbody>
    </table>
    </div>
</div>