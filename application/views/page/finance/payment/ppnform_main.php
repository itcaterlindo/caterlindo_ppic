<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
if (isset($master)) {
    extract($master);
}
?>

<?php
echo form_open_multipart('', array('id' => 'idformItemPPN', 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtSts', 'value' => isset($sts) ? $sts : null, 'placeholder' => 'idtxtSts' ));
echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtpurchaseinvoice_kd', 'placeholder' => 'idtxtpurchaseinvoice_kd', 'value' => isset($purchaseinvoice_kd) ? $purchaseinvoice_kd: null ));
?>

<div class="col-md-12">
    <div class="form-group">
		<label for='idtxtpurchaseinvoice_ppn' class="col-md-2 control-label">PPN (%)</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrpurchaseinvoice_ppn"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtpurchaseinvoice_ppn', 'id'=> 'idtxtpurchaseinvoice_ppn', 'placeholder' =>'PPN', 'value'=> isset($purchaseinvoice_ppn) ? $purchaseinvoice_ppn: null ));?>
		</div>
	</div>

    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-primary btn-sm pull-right" onclick="submitDataPPN('idformItemPPN')"> <i class="fa fa-save"></i> Simpan</button>
        </div>
    </div>
</div>

<?php echo form_close(); ?>