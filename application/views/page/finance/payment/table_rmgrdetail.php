<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<?php 
$formGR = 'idFormGR';
echo form_open_multipart('', array('id' => $formGR)); 
// echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpurchaseinvoice_kdGR', 'name'=> 'txtpurchaseinvoice_kdGR', 'value' => $id, 'placeholder' => 'idtxtpurchaseinvoice_kdGR' ));
?>
<div id="idErrpurchaseinvoice_kdGR"></div>
<table id="idTableGR" class="table table-bordered table-striped " style="width:100%; font-size: 85%">
	<thead>
	<tr>
	<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:5%; text-align:center;">Tgl Invoice</th>
		<th style="width:5%; text-align:center;">Tgl Termpayment</th>
		<th style="width:7%; text-align:center;">No Invoice</th>
		<th style="width:7%; text-align:center;">No Faktur</th>
		<th style="width:10%; text-align:center;">Nama Suplier</th>
		<th style="width:10%; text-align:center;">Grand Total Invoice</th>
		<th style="width:10%; text-align:center;">Grand Total - Pph Bila ada</th>
		<th style="width:10%; text-align:center;">Pph</th>
		<th style="width:5%; text-align:center;">Tgl Update</th>
	</tr>
	</thead>
    <tbody>
        <?php 
		$no = 1;
		$subHarga = 0;
		foreach ($qData as $each) :
		?>
        <tr>
            <td class="dt-center"><?php echo $no; ?></td>
            <td><?php echo format_date($each['purchaseinvoice_tanggal'], 'Y-m-d') ?></td>
            <td><?php echo $each['purchaseinvoice_termpayment'] ?></td>
            <td><?php echo $each['purchaseinvoice_no']; ?></td>
            <td><?php echo $each['purchaseinvoice_faktur']; ?></td>
            <td><?php echo $each['suplier_nama']; ?></td>
			<td><?php echo ($totalnya != '0') ? $totalnya : $each['purchaseinvoice_grandtotal'];?></td>
			 <td class="dt-center">
				<?php
					echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtsup_kd', 'value' => $each['suplier_kd'],'placeholder' => 'txtrmgr_qty'));
					echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtpi_kd', 'value' => $each['purchaseinvoice_kd'] ,'placeholder' => 'txtrmsatuan_kd'));
				?>
				<input type="hidden" name="payment_kd" id="payment_kd" class="form-control input-sm" value="">
				<input type="number" name="txtgrand_total" class="form-control input-sm" value="<?php echo ($totalnya != '0') ? $totalnya : $total_min_ph;?>">
            </td>
			<td><?php echo $nominal_pph; ?></td>
			<td><?php echo format_date($each['purchaseinvoice_tgledit'], 'Y-m-d') ?></td>
        </tr>
        <?php 
        $no ++;
        endforeach;?>
    </tbody>
	<tfoot>
	</tfoot>
</table>
<div class="row">
	<div class="col-md-12">
		<button class="btn btn-primary btn-sm pull-right" onclick="submitDataBatch('<?= $formGR; ?>')"> <i class="fa fa-save"></i> Simpan</button>
	</div>
</div>

<?php echo form_close(); ?>

<script type="text/javascript">
	render_dt('#idTableGR');
	$('#payment_kd').val($('#dumm_payment_kd').val());
	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				},
				"columnDefs": [
					{"data": null, "searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
					{"searchable": false, "orderable": false, "targets": 1},
					{"className": "dt-center", "targets": 2},
					{"className": "dt-center", "targets": 3},
					{"className": "dt-center", "targets": 4},
					{"className": "dt-center", "targets": 5},
				],
					},
		});
	}
</script>