<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
?>
<style type="text/css">
.select2-container {
    width: 100% !important;
    padding: 0;
}
</style>

<div class="row">
	<div class="col-md-12">

        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Purchase Invoice</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
              <!-- ---------------------------------------------------------------- -->
                <?php 
                echo form_open_multipart('', array('id' => 'idformCari', 'class' => 'form-horizontal'));
                ?>
                <div class="form-group">
                    <label for='idtxtrmgr_nosrj' class="col-md-2 control-label">No Invoice / Data Invoice</label>
                    <div class="col-sm-5 col-xs-12">
                        <div class="errInput" id="idErrrmgr_nosrj"></div>
                        <select name="txtrmgr_nosrj" id="idtxtrmgr_nosrj" class="form-control"> </select>
                    </div>
                    <input type="hidden" name="suplier_kd" id="idsuplier_kd" value="<?php echo isset($suplier_kd) ? $suplier_kd : ''; ?>">
                    <button class="btn btn-sm btn-default" id="idbtnpilih" onclick="pilihsrj()"> <i class="fa fa-check-circle"></i> Pilih </button>
                </div>
                <?php echo form_close(); ?>

                <div id="idtableRmgr"></div> 

                <!-- ---------------------------------------------------------------- -->

              </div>
              <!-- /.tab-pane -->
             
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        
    </div>
</div>


