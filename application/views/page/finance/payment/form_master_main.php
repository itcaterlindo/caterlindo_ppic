<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputMaster';
if($sts == 'edit'){
	extract($rowData);
}

?>
<style type="text/css">
	.select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>
<?php
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtStsMaster', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'id_payment_kd', 'name'=> 'id_payment_kd', 'value' => isset($id) ? $id: null ));
?>
<div class="row">	
    <div class="form-group">
		<label for='idtxtsuplier_kd' class="col-md-2 control-label">Suplier</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrsuplier_kd"></div>
			<select class="form-control" name="txtsuplier_kd" id="idtxtsuplier_kd"></select>
		</div>
	</div>
	<div class="form-group">
		<label for='idtxtmpayment' class="col-md-2 control-label">Pembayaran</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrmpayment"></div>
			<select class="form-control" name="txtmpayment" id="idtxtmpayment"></select>
		</div>
	</div>
	<div class="form-group">
		<label for='idtxtpurchaseinvoice_tanggaltt' class="col-md-2 control-label">Tgl Payment</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpurchaseinvoice_tanggaltt"></div>
			<div class="input-group date">			
				<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
				<?php echo form_input(array('type'=>'text', 'class'=> 'form-control datetimepicker', 'name'=> 'txtcreated_payment', 'id'=> 'idtxtcreated_payment', 'placeholder' =>'yyyy-mm-dd', 'value'=> isset($tanggal_input) ? $tanggal_input: null ));?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for='idtxtpurchaseinvoice_no' class="col-md-2 control-label">No Check</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrno_check"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtno_check', 'id'=> 'idtxtno_check', 'placeholder' =>'No Check', 'value'=> isset($no_check) ? $no_check: null ));?>
		</div>
	</div>

</div>

<hr>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-1 col-xs-12 col-sm-offset-10">
			<button type="submit" name="btnSubmit" onclick="submitDataMaster('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-arrow-circle-right"></i> Simpan
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	render_suplier();
	render_mpayment();
	<?php if ($sts == 'edit') :?>
		$("#idtxtsuplier_kd").select2("trigger", "select", { 
			data: { 
				id: '<?php echo $suplier_kd; ?>', 
				text: '<?php echo $suplier_kode.' | '.$suplier_nama; ?>', 
			}
		});

		$("#idtxtmpayment").select2("trigger", "select", { 
			data: { 
				id: '<?php echo $m_payment_kd; ?>', 
				text: '<?php echo $nm_bank.' | '.$no_rek; ?>', 
			}
		});
	<?php endif;?>
</script>
