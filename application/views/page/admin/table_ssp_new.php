<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$t_opsi = isset($t_opsi)?$t_opsi:TRUE;
$t_uri = isset($t_uri)?$t_uri:'';
$t_col_def = isset($t_col_def)?$t_col_def:'';
$t_order = isset($t_order)?$t_order:'';
$table_id = isset($table_id)?$table_id:'dataTable';
$datatable_properties = isset($datatable_properties)?$datatable_properties:'';
?>
<table id="<?php echo $table_id; ?>" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
    <thead>
        <tr>
            <th style="width:1%;" class="all">No.</th>
            <?php
            if ($t_opsi == TRUE) :
                ?>
                <th style="width:1%; text-align:center;" class="all">Opsi</th>
                <?php
            endif;

            if (isset($t_header) && is_array($t_header)) :
                foreach ($t_header as $column => $width) :
                    echo '<th style="width:'.$width.'; text-align:center;" class="all">'.$column.'</th>';
                endforeach;
            endif;
            ?>
        </tr>
    </thead>
</table>

<script>
    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };
    var table = $('#<?php echo $table_id; ?>').DataTable({
        "processing": true,
        "serverSide": true,
        "ordering" : true,		
        <?php
        if (isset($datatable_properties) && is_array($datatable_properties)) :
            foreach ($datatable_properties as $value) :
                echo $value;
            endforeach;
        endif;
        ?>
        "ajax": "<?php echo $t_uri; ?>",
        "language" : {
            "lengthMenu" : "Tampilkan _MENU_ data",
            "zeroRecords" : "Maaf tidak ada data yang ditampilkan",
            "info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
            "infoFiltered": "(difilter dari _MAX_ total data)",
            "infoEmpty" : "Tidak ada data yang ditampilkan",
            "search" : "Cari :",
            "loadingRecords": "Memuat Data...",
            "processing":     "Sedang Memproses...",
            "paginate": {
                "first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
                "last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
                "next":       '<span class="glyphicon glyphicon-forward"></span>',
                "previous":   '<span class="glyphicon glyphicon-backward"></span>'
            }
        },
        "columnDefs": [
            {"data": null, "searchable": false, "orderable": false, "targets": 0},
            <?php
            if (isset($t_column) && is_array($t_column)) :
                $no = 0;
                foreach ($t_column as $column => $width) :
                    $no++;
                    echo '{"data": "'.$column.'",width:"'.$width.'", "targets": '.$no.'},';
                endforeach;
            endif;
            ?>
        ],
        "order": <?php echo $t_order; ?>,
        "rowCallback": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        }
    });
</script>