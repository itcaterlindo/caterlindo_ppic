<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$f_attr = isset($f_attr)?$f_attr:'';
$f_box_class = isset($f_box_class)?$f_box_class:'';
$form_title = isset($form_title)?$form_title:'';
$btn_close = isset($btn_close)?$btn_close:TRUE;
$id_btn_close = isset($id_btn_close)?$id_btn_close:'idBtnTutup';
$form_url = isset($form_url)?$form_url:'';
$form_att = isset($form_att)?$form_att:'';
$form_hidden = isset($form_hidden)?$form_hidden:'';
$form_close_att = isset($form_close_att)?$form_close_att:'';
$form_box = isset($form_box)?$form_box:'';
$form_alert = isset($form_alert)?$form_alert:'';
$form_overlay = isset($form_overlay)?$form_overlay:'';
?>
<div class="box <?php echo $f_box_class; ?>" <?php echo $f_attr; ?>>
  <?php echo $this->load->get_section('form_css'); ?>
  <div class="box-header with-border">
    <h3 id="idTitleForm" class="box-title">Form <?php echo $form_title; ?></h3>
    <div class="box-tools pull-right">
      <button class="btn btn-box-tool" id="idBtnCollapseForm" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan"><i class="fa fa-minus"></i></button>
      <?php
      if ($btn_close == TRUE) :
        ?>
        <button class="btn btn-box-tool" id="<?php echo $id_btn_close; ?>" data-widget="remove" data-toggle="tooltip" title="Tutup"><i class="fa fa-times"></i></button>
        <?php
      endif;
      ?>
    </div>
  </div>
  <?php
  echo form_open($form_url, $form_att, $form_hidden);
  ?>
  <div class="box-body">
    <div id="<?php echo $form_box; ?>">
      <div id="<?php echo $form_alert; ?>"></div>
      <?php
      if (isset($form_field) && is_array($form_field)) :
        foreach ($form_field as $input) :
          foreach ($input as $field) :
            echo $field;
          endforeach;
        endforeach;
      endif;
      ?>
    </div>
  </div>
  <div class="box-footer">
    <?php
    if (isset($form_btn) && is_array($form_btn)) :
        foreach ($form_btn as $btns) :
          foreach ($btns as $btn) :
            echo $btn;
          endforeach;
        endforeach;
    endif;
    ?>
  </div>
  <?php
  echo form_close($form_close_att);
  if (!empty($form_overlay)) :
    ?>
    <div class="overlay" id="<?php echo $form_overlay; ?>" style="display: none;">
      <i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
    </div>
    <?php
  endif;
  echo $this->load->get_section('form_js');
  ?>
</div>