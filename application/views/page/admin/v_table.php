<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$t_box_class = isset($t_box_class)?$t_box_class:'';
$t_attr = isset($t_attr)?$t_attr:'';
$id_btn_close = isset($id_btn_close)?$id_btn_close:'idBtnTutup';
$id_btn_tambah = isset($id_btn_tambah)?$id_btn_tambah:'idBtnTambah';
$id_btn_grid = isset($id_btn_grid)?$id_btn_grid:'idBtnGrid';
$btn_grid_edit = isset($btn_grid_edit)?$btn_grid_edit:FALSE;
$btn_add = isset($btn_add)?$btn_add:TRUE;
$btn_close = isset($btn_close)?$btn_close:FALSE;
$table_title = isset($table_title)?$table_title:'';
$table_alert = isset($table_alert)?$table_alert:'';
$table_loader = isset($table_loader)?$table_loader:'';
$table_name = isset($table_name)?$table_name:'';
$table_body = isset($table_body)?$table_body:'';
$table_overlay = isset($table_overlay)?$table_overlay:'';
?>
<div class="box <?php echo $t_box_class; ?>" <?php echo $t_attr; ?>>
  <div class="box-header with-border">
    <h3 id="idTitleTable" class="box-title">Tabel <?php echo $table_title; ?></h3>
    <div class="box-tools pull-right">
      <?php
      if ($this->session->update_access == '1' && $btn_grid_edit == TRUE) :
        ?>
        <button class="btn btn-box-tool" id="<?php echo $id_btn_grid; ?>" data-toggle="tooltip" title="Grid Edit"><i class="fa fa-table"></i></button>
        <?php
      endif;
      if ($this->session->create_access == '1' && $btn_add == TRUE) :
        ?>
        <button class="btn btn-box-tool" id="<?php echo $id_btn_tambah; ?>" data-toggle="tooltip" title="Tambah"><i class="fa fa-plus"></i></button>
        <?php
      endif;
      ?>
      <button class="btn btn-box-tool" id="idBtnCollapseTable" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan"><i class="fa fa-minus"></i></button>
      <?php
      if ($btn_close == TRUE) :
        ?>
        <button class="btn btn-box-tool" id="<?php echo $id_btn_close; ?>" data-widget="remove" data-toggle="tooltip" title="Tutup"><i class="fa fa-times"></i></button>
        <?php
      endif;
      ?>
    </div>
  </div>
  <div class="box-body">
    <div id="<?php echo $table_alert; ?>"></div>
    <?php
    if (!empty($table_loader)) :
      ?>
      <div id="<?php echo $table_loader; ?>" align="middle" style="display:none;">
        <i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
      </div>
      <?php
    endif;
    ?>
    <div id="<?php echo $table_name; ?>"><?php echo $table_body; ?></div>
  </div>
  <div class="box-footer"></div>
  <?php
  if (!empty($table_overlay)) :
    ?>
    <div class="overlay" id="<?php echo $table_overlay; ?>" style="display: none;">
      <i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
    </div>
    <?php
  endif;
  echo $this->load->get_section('table_js');
  ?>
</div>