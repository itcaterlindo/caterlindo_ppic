<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$t_box_class = isset($t_box_class)?$t_box_class:'';
$f_attr = isset($f_attr)?$f_attr:'';
$table_title = isset($table_title)?$table_title:'';
$table_alert = isset($table_alert)?$table_alert:'';
$table_loader = isset($table_loader)?$table_loader:'';
$table_overlay = isset($table_overlay)?$table_overlay:'';
$table_desc = isset($table_desc)?$table_desc:'';
$table_num = isset($table_num)?$table_num:'';
$t_body_properties = isset($t_body_properties)?$t_body_properties:'';
$form_url = isset($form_url)?$form_url:'';
$form_att = isset($form_att)?$form_att:'';
$form_hidden = isset($form_hidden)?$form_hidden:'';
$form_close_att = isset($form_close_att)?$form_close_att:'';
?>
<div class="box <?php echo $t_box_class; ?>" <?php echo $f_attr; ?>>
	<div class="box-header with-border">
		<h3 id="idTitleTable" class="box-title"><?php echo $table_title; ?></h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool" id="idBtnCollapseForm" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
				<i class="fa fa-minus"></i>
			</button>
			<button class="btn btn-box-tool" id="idBtnTutup" data-widget="remove" data-toggle="tooltip" title="Tutup">
				<i class="fa fa-times"></i>
			</button>
		</div>
	</div>
	<?php
	echo form_open($form_url, $form_att, $form_hidden);
	?>
	<div class="box-body">
		<div id="<?php echo $table_alert; ?>"></div>
		<?php
		if (!empty($table_loader)) :
			?>
			<div id="<?php echo $table_loader; ?>" align="middle" style="display:none;">
				<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
			</div>
			<?php
		endif;
		?>

		<?php
		if (!empty($table_desc)) :
			foreach ($table_desc as $key => $values) :
				foreach ($values as $value) :
					echo $value;
				endforeach;
			endforeach;
		endif;
		?>

		<table class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<?php
					if (!empty($table_num)) :
						?>
						<th style="width:1%;">No.</th>
						<?php
					endif;

					if (isset($t_header) && is_array($t_header)) :
						foreach ($t_header as $value) :
							echo $value;
						endforeach;
					endif;
					?>
				</tr>
			</thead>
			<tbody <?php echo $t_body_properties; ?>>
				<?php
				if (isset($t_body) && is_array($t_body)) :
					foreach ($t_body as $body => $values) :
						foreach ($values as $value) :
							echo $value;
						endforeach;
					endforeach;
				endif;
				?>
			</tbody>
		</table>

	</div>
	<div class="box-footer">
		<?php
		if (isset($form_btn) && is_array($form_btn)) :
			foreach ($form_btn as $btns) :
				foreach ($btns as $btn) :
					echo $btn;
				endforeach;
			endforeach;
		endif;
		?>
	</div>
	<?php
	echo form_close($form_close_att);
	if (!empty($table_overlay)) :
		?>
		<div class="overlay" id="<?php echo $table_overlay; ?>" style="display: none;">
			<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
		</div>
		<?php
	endif;
 	echo $this->load->get_section('form_js');
	?>
</div>