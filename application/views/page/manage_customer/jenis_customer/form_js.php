<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form('<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#idTableBoxLoader<?php echo $master_var; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
		open_table();
		first_load('<?php echo 'idTableBoxLoader'.$master_var; ?>', '<?php echo 'idTableBoxContent'.$master_var; ?>');
		$('#idTableBtnBoxAdd<?php echo $master_var; ?>').slideDown();
	});

	$(document).off('submit', '#idForm<?php echo $master_var; ?>').on('submit', '#idForm<?php echo $master_var; ?>', function(e) {
		e.preventDefault();
		submit_form(this);
	});

	function open_form(id, kd_mso, tipe) {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/open_form'; ?>',
				data: 'id='+id,
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
					$('#idTxtNm').focus();
					$('.class-form-diskon').slideDown();
				}
			});
		});
	}

	function close_form() {
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$('#<?php echo $box_id; ?>').fadeOut(function() {
				$('#<?php echo $box_id; ?>').remove();
			});
		});
	}

	function submit_form(form_id) {
		$('#<?php echo $box_overlay_id; ?>').show();
		$('#<?php echo $box_alert_id; ?>').html('');
		<?php
		foreach ($form_errs as $form_err) {
			echo '$(\'#'.$form_err.'\').html(\'\');';
		}
		?>
		$.ajax({
			url: "<?php echo base_url().$class_link.'/send_data'; ?>",
			type: "POST",
			data:  new FormData(form_id),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					close_form();
					$('#idTableBtnBoxAdd<?php echo $master_var; ?>').show();
					$('#idTableBoxAlert<?php echo $master_var; ?>').html(data.alert).fadeIn();
					open_table();
				}
				if (data.confirm == 'error') {
					$('#idTxtNm').focus();
					$('#<?php echo $box_alert_id; ?>').html(data.alert);
					<?php
					foreach ($form_errs as $form_err) {
						echo '$(\'#'.$form_err.'\').html(data.'.$form_err.');';
					}
					?>
				}
				$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
				$('#<?php echo $box_overlay_id; ?>').hide();
				if(data.alert != null){
					notify ('Error API', data.alert, 'error');
				}
			}
		});
	}

	$(document).off('click', '#idBtnTambahDiskon').on('click', '#idBtnTambahDiskon', function() {
		tambah_form_diskon();
	});

	$(document).off('click', '.btn-hapus-diskon').on('click', '.btn-hapus-diskon', function() {
		$(this).closest('.class-form-diskon').slideUp(function() {
			$(this).remove();
		});
	});

	function tambah_form_diskon() {
		$.ajax({
			url: "<?php echo base_url().$class_link.'/get_form_diskon'; ?>",
			type: "GET",
			success: function(html) {
				$('#idFormDefaultDiskon').append(html);
				$('.class-form-diskon').slideDown();
			}
		});
	}
</script>