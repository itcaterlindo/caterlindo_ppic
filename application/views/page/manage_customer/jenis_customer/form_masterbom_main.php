<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
?>

<?php
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtkd_jenis_customer', 'name'=> 'txtkd_jenis_customer', 'value' => isset($id) ? $id: null ));
?>

<div class="col-md-12">
    <div class="form-group">
        <label for='idtxtjeniscustomer_marginbom' class="col-md-2 control-label">Margin (%)</label>
        <div class="col-sm-4 col-xs-12">
            <div class="errInput" id="idErrjeniscustomer_marginbom"></div>
            <?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtjeniscustomer_marginbom', 'id'=> 'idtxtjeniscustomer_marginbom', 'placeholder' =>'Margin', 'value'=> !empty($rowData->jeniscustomer_marginbom) ? $rowData->jeniscustomer_marginbom: null ));?>
        </div>	
    </div>

    <div class="form-group">
        <label for='idtxtjeniscustomer_discbom' class="col-md-2 control-label">Diskon (%)</label>
        <div class="col-sm-4 col-xs-12">
            <div class="errInput" id="idErrjeniscustomer_discbom"></div>
            <?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtjeniscustomer_discbom', 'id'=> 'idtxtjeniscustomer_discbom', 'placeholder' =>'Margin', 'value'=> !empty($rowData->jeniscustomer_discbom) ? $rowData->jeniscustomer_discbom: null ));?>
        </div>	
    </div>

	<button type="submit" onclick="submitDataMasterBom('<?= $form_id ?>')" class="btn btn-primary btn-sm pull-right">
		<i class="fa fa-save"></i> Simpan
	</button>

</div>
<?php echo form_close(); ?>
