<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table();
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		get_form('');
		$(this).slideUp();
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table() {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/open_table'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function get_form(id) {
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		$('#idFormBox<?php echo $master_var; ?>').remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/get_form'; ?>',
			data: 'id='+id,
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function delete_data(id) {
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		$('#<?php echo $box_overlay_id; ?>').show();
		$('#idFormBox<?php echo $master_var; ?>').remove();
		$('#idBoxItems<?php echo $master_var; ?>').remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/delete_data'; ?>',
			data: 'id='+id,
			success: function(data) {
				$('#<?php echo $box_alert_id; ?>').html(data.alert).fadeIn();
				$('#<?php echo $box_overlay_id; ?>').hide();
				open_table();
			}
		});
	}

	function ubah_master_bom(id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_masterbom_main'; ?>',
			data: 'id='+id,
			success: function(html) {
				toggle_modal('Form Master BoM', html);
			}
		});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function submitDataMasterBom(form_id) {
		event.preventDefault();
		$('#<?php echo $box_alert_id; ?>').html('');
		var form = document.getElementById(form_id);
		$.ajax({
			url: "<?php echo base_url().$class_link.'/action_submitMasterBom'; ?>",
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					toggle_modal('','');
					$('#idTableBoxAlert<?php echo $master_var; ?>').html(data.alert).fadeIn();
				}
			}
		});
	}
</script>