<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'JenisCustomer';
$form_id = 'idForm'.$master_var;
			
// select option untuk price category
$this->db->from('tb_set_dropdown')->where(array('jenis_select' => 'price_category'));
$query = $this->db->get();
$result = $query->result();
$pil_price = array('' => '-- Pilih Price Category --');
foreach ($result as $row) :
	$id = $this->security->xss_clean($row->id);
	$nm_select = $this->security->xss_clean($row->nm_select);
	$pil_price[$id] = $this->security->xss_clean($nm_select);
endforeach;
$sel_price = $master['kd_price_category'];
$attr_price = array('id' => 'idSelPrice', 'class' => 'form-control');

// select option untuk manage item customer
$this->db->from('tb_set_dropdown')->where(array('jenis_select' => 'manage_items'))->where_not_in('nm_select', array('Semua', 'Tidak Ada'));
$query = $this->db->get();
$result = $query->result();
$pil_item = array('' => '-- Pilih Manage Item --');
foreach ($result as $row) :
	$id = $this->security->xss_clean($row->id);
	$nm_select = $this->security->xss_clean($row->nm_select);
	$pil_item[$id] = $this->security->xss_clean($nm_select);
endforeach;
$sel_item = $master['kd_manage_items'];
$attr_item = array('id' => 'idSelItem', 'class' => 'form-control');

// select option untuk PPN dari SAP
$pil_ppn = array('' => '-- Pilih PPN --');
foreach ($ppn as $row) :
	$id = $row->Code."|".$row->Rate;
	$nm_select = $row->Name;
	$pil_ppn[$id] = $nm_select;
endforeach;
$sel_ppn = $master['kd_ppn_sap']."|".(int)$master['set_ppn'];
$attr_ppn = array('id' => 'idTxtSetPpn', 'class' => 'form-control');

// select option untuk term payment dari SAP
$pil_term = array('' => '-- Pilih Term of Payment --');
foreach ($term_payment as $row) :
	$id = $row->GroupNum."|".$row->ExtraDays;
	$nm_select = $row->PymntGroup;
	$pil_term[$id] = $nm_select;
endforeach;
$sel_term = $master['kd_term_payment_sap']."|".$master['term_payment_waktu'];
$attr_term = array('id' => 'idTxtTermWaktu', 'class' => 'form-control');

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtKd', 'id' => 'idTxtKd', 'value' => $master['kd_jenis_customer']));
echo form_input(array('type' => 'hidden', 'name' => 'txtKdSap', 'id' => 'idTxtKdSap', 'value' => $master['kd_sap']));
?>
<div class="form-group">
	<label for="idTxtNm" class="col-md-2 control-label">Nama Jenis Customer</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrNm"></div>
		<?php echo form_input(array('name' => 'txtNm', 'id' => 'idTxtNm', 'class' => 'form-control', 'placeholder' => 'Nama Jenis Customer', 'value' => $master['nm_jenis_customer'])); ?>
	</div>
</div>
<div class="form-group">
	<label for="idSelPrice" class="col-md-2 control-label">Price Category</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrPrice"></div>
		<?php echo form_dropdown('selPrice', $pil_price, $sel_price, $attr_price); ?>
	</div>
</div>
<div class="form-group">
	<label for="idSelItem" class="col-md-2 control-label">Tipe Customer</label>
	<div class="col-sm-2 col-xs-12">
		<div id="idErrItem"></div>
		<?php echo form_dropdown('selItem', $pil_item, $sel_item, $attr_item); ?>
	</div>
</div>
<hr>
<div class="form-group">
<label for="idTxtSetPpn" class="col-md-2 control-label">Set Default PPN (%)</label>
	<div class="col-sm-2 col-xs-12">
		<div id="idErrSetPpn"></div>
		<?php echo form_dropdown('txtSetPpn', $pil_ppn, $sel_ppn, $attr_ppn); ?>
	</div>
	<!-- <label for="idTxtSetPpn" class="col-md-2 control-label">Set Default PPN (%)</label>
	<div class="col-sm-2 col-xs-12">
		<div id="idErrSetPpn"></div>
		<?php echo form_input(array('name' => 'txtSetPpn', 'id' => 'idTxtSetPpn', 'class' => 'form-control', 'placeholder' => 'Set Default PPN (%)', 'value' => $master['set_ppn'])); ?>
	</div> -->
	<label for="idTxtBarcode" class="col-md-2 control-label">Custom Barcode</label>
	<div class="col-sm-2 col-xs-12">
		<div id="idErrBarcode"></div>
		<?php echo form_input(array('name' => 'txtBarcode', 'id' => 'idTxtBarcode', 'class' => 'form-control', 'placeholder' => 'Custom Barcode', 'value' => $master['custom_barcode'])); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtTermFormat" class="col-md-2 control-label">Format Term Payment</label>
	<div class="col-sm-8 col-xs-12">
		<div id="idErrTermFormat"></div>
		<?php echo form_textarea(array('name' => 'txtTermFormat', 'id' => 'idTxtTermFormat', 'class' => 'form-control', 'placeholder' => 'Term Payment Format', 'value' => $master['term_payment_format'], 'rows' => '3')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtTermWaktu" class="col-md-2 control-label">Term of Payment</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrTermWaktu"></div>
		<?php echo form_dropdown('txtTermWaktu', $pil_term, $sel_term, $attr_term); ?>
	</div>
</div>
<!-- <div class="form-group">
	<label for="idTxtTermWaktu" class="col-md-2 control-label">Masa Tenggang Term Payment (Hari)</label>
	<div class="col-sm-1 col-xs-12">
		<div id="idErrTermWaktu"></div>
		<?php echo form_input(array('name' => 'txtTermWaktu', 'id' => 'idTxtTermWaktu', 'class' => 'form-control', 'placeholder' => 'Term Payment Waktu', 'value' => $master['term_payment_waktu'])); ?>
	</div>
</div> -->
<hr>
<div id="idFormDefaultDiskon">
	<?php echo $form_diskon; ?>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" class="btn btn-primary btn-flat">
			<i class="fa fa-save"></i> Submit
		</button>
	</div>
</div>
<?php
echo form_close();