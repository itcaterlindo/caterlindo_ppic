<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form('<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#idTableBoxLoader<?php echo $master_var; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
		open_table();
		first_load('<?php echo 'idTableBoxLoader'.$master_var; ?>', '<?php echo 'idTableBoxContent'.$master_var; ?>');
	});

	$(document).off('change', '#idSelNegara').on('change', '#idSelNegara', function() {
		var id_negara = $(this).val();
		get_selprovinsi(id_negara, '', 'idSelProvinsi');
	});

	$(document).off('change', '#idSelProvinsi').on('change', '#idSelProvinsi', function() {
		var id_negara = $('#idSelNegara').val();
		var id_provinsi = $(this).val();
		get_selkota(id_negara, id_provinsi, '', 'idSelKota');
	});

	$(document).off('change', '#idSelKota').on('change', '#idSelKota', function() {
		var id_negara = $('#idSelNegara').val();
		var id_provinsi = $('#idSelProvinsi').val();
		var id_kota = $(this).val();
		get_selkecamatan(id_negara, id_provinsi, id_kota, '', 'idSelKecamatan');
	});

	$(document).off('submit', '#idForm<?php echo $master_var; ?>').on('submit', '#idForm<?php echo $master_var; ?>', function(e) {
		e.preventDefault();
		submit_form(this);
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function get_selprovinsi(kd_negara, selected, id_dropdown) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'lokasi/dropdown_provinsi'; ?>',
			data: 'kd_negara='+kd_negara,
			success: function(data) {
				$('#'+id_dropdown).html(data);
				$('#'+id_dropdown).val(selected);
				$('#'+id_dropdown).trigger('change');
				if (kd_negara != '') {
					$('#idSelProvinsi').focus();
				}
			}
		});
	}

	function get_selkota(kd_negara, kd_provinsi, selected, id_dropdown) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'lokasi/dropdown_kota'; ?>',
			data: 'kd_negara='+kd_negara+'&kd_provinsi='+kd_provinsi,
			success: function(data) {
				$('#'+id_dropdown).html(data);
				$('#'+id_dropdown).val(selected);
				$('#'+id_dropdown).trigger('change');
				if (kd_provinsi != '') {
					$('#idSelKota').focus();
				}
			}
		});
	}

	function get_selkecamatan(kd_negara, kd_provinsi, kd_kota, selected, id_dropdown) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'lokasi/dropdown_kecamatan'; ?>',
			data: 'kd_negara='+kd_negara+'&kd_provinsi='+kd_provinsi+'&kd_kota='+kd_kota,
			success: function(data) {
				$('#'+id_dropdown).html(data);
				$('#'+id_dropdown).val(selected);
				$('#'+id_dropdown).trigger('change');
				if (kd_kota != '') {
					$('#idTxtAlamat').focus();
				}
			}
		});
	}

	function open_form(id, kd_mso, tipe) {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/open_form'; ?>',
				data: 'id='+id,
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					$('.select2').select2({
						theme: 'bootstrap',
						width: '100%'
					});
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function close_form() {
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$('#<?php echo $box_id; ?>').fadeOut(function() {
				$('#<?php echo $box_id; ?>').remove();
			});
		});
	}

	function submit_form(form_id) {
		$('#<?php echo $box_overlay_id; ?>').show();
		$('#<?php echo $box_alert_id; ?>').html('');
		<?php
		foreach ($form_errs as $form_err) {
			echo '$(\'#'.$form_err.'\').html(\'\');';
		}
		?>
		$.ajax({
			url: "<?php echo base_url().$class_link.'/send_data'; ?>",
			type: "POST",
			data:  new FormData(form_id),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					close_form();
					$('#idTableBtnBoxAdd<?php echo $master_var; ?>').show();
					$('#idTableBoxAlert<?php echo $master_var; ?>').html(data.alert).fadeIn();
					open_table();
				}
				if (data.confirm == 'error') {
					$('div.form-group input:eq(0)').focus();
					$('#<?php echo $box_alert_id; ?>').html(data.alert);
					<?php
					foreach ($form_errs as $form_err) {
						echo '$(\'#'.$form_err.'\').html(data.'.$form_err.');';
					}
					?>
				}
				$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
				$('#<?php echo $box_overlay_id; ?>').hide();
			}
		});
	}
</script>