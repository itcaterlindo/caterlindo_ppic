<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'JasaKirim';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtKd', 'value' => $kd_jasakirim));
?>
<div class="form-group">
	<label for="idTxtNama" class="col-md-2 control-label">Nama Jasa Pengiriman</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrNama"></div>
		<?php echo form_input(array('name' => 'txtNama', 'id' => 'idTxtNama', 'class' => 'form-control', 'value' => $nm_jasakirim, 'placeholder' => 'Nama Jasa Pengiriman', 'autofocus' => 'autofocus')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idSelNegara" class="col-md-2 control-label">Negara</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrNegara"></div>
		<?php echo form_dropdown('selNegara', $data_negara, $negara_kd, array('id' => 'idSelNegara', 'class' => 'form-control select2')); ?>
	</div>
	<label for="idSelProvinsi" class="col-md-2 control-label">Provinsi</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrProvinsi"></div>
		<?php echo form_dropdown('selProvinsi', $data_provinsi, $provinsi_kd, array('id' => 'idSelProvinsi', 'class' => 'form-control select2')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idSelKota" class="col-md-2 control-label">Kota</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrKota"></div>
		<?php echo form_dropdown('selKota', $data_kota, $kota_kd, array('id' => 'idSelKota', 'class' => 'form-control select2')); ?>
	</div>
	<label for="idSelKecamatan" class="col-md-2 control-label">Kecamatan</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrKecamatan"></div>
		<?php echo form_dropdown('selKecamatan', $data_kecamatan, $kecamatan_kd, array('id' => 'idSelKecamatan', 'class' => 'form-control select2')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtAlamat" class="col-md-2 control-label">Alamat</label>
	<div class="col-sm-6 col-xs-12">
		<div id="idErrAlamat"></div>
		<?php echo form_textarea(array('name' => 'txtAlamat', 'id' => 'idTxtAlamat', 'class' => 'form-control', 'value' => $alamat, 'placeholder' => 'Alamat', 'rows' => '3')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtKodePos" class="col-md-2 control-label">KodePos</label>
	<div class="col-sm-2 col-xs-12">
		<div id="idErrKodePos"></div>
		<?php echo form_input(array('name' => 'txtKodePos', 'id' => 'idTxtKodePos', 'class' => 'form-control', 'value' => $kode_pos, 'placeholder' => 'KodePos')); ?>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" class="btn btn-primary btn-flat">
			<i class="fa fa-save"></i> Submit
		</button>
	</div>
</div>
<?php
echo form_close();