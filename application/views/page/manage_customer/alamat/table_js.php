<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table();
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');
	first_load('<?php echo $box_loader_idp ?>', '<?php echo $box_content_idp; ?>');
	first_load('<?php echo $box_loader_idk ?>', '<?php echo $box_content_idk; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		form_main("");
	});
	$(document).off('click', '#<?php echo $btn_add_idp; ?>').on('click', '#<?php echo $btn_add_idp; ?>', function() {
		form_mainp("");
	});
	$(document).off('click', '#<?php echo $btn_add_idk; ?>').on('click', '#<?php echo $btn_add_idk; ?>', function() {
		form_maink("");
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table() {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_main'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
				}
			});
		});


		$('#<?php echo $btn_add_idp; ?>').slideDown();
		$('#<?php echo $box_content_idp; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_mainp'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_idp; ?>').html(html);
					$('#<?php echo $box_content_idp; ?>').slideDown();
					moveTo('idMainContent');
				}
			});
		});

		$('#<?php echo $btn_add_idk; ?>').slideDown();
		$('#<?php echo $box_content_idp; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_maink'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_idk; ?>').html(html);
					$('#<?php echo $box_content_idk; ?>').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function form_main (id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_main'; ?>',
			data: {id: id},
			success: function(html) {
				toggle_modal('Form', html);
				get_last_code_negara();
			}
		});
	}
	
	function form_mainp (id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_main_prov'; ?>',
			data: {id: id},
			success: function(html) {
				toggle_modal('Form', html);
				get_prov_code();
			}
		});
	}

	function form_maink (id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_main_kota'; ?>',
			data: {id: id},
			success: function(html) {
				toggle_modal('Form', html);
				get_kota_code();
			}
		});
	}
	function get_last_code_negara () {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/get_last_code'; ?>',
			success: function(html) {
		 		$('#id_negara').val(parseInt(html[0].q) + 1);
			}
		});
	}

	function get_prov_code() { 
		$('#negara').change(function () { 
			var id = $(this).val();
			$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/get_last_code_prov'; ?>' + '?id='+id,
			success: function(html) {
				if(html[0].q){
					var items = parseInt(html[0].q) + 1;
					if(counting(items) == 1){
						items = '00' + items;
					}else if(counting(items) == 2){
						items = '0' + items;
					}
					$('#id_prov').val(items);
				}else{
					$('#id_prov').val('001');
				}
		 		
			}
		});
		});
	 }

	 function counting(num) {
		return String(num).split('').reduce(
			(count, digit) => count + 1, 0);
	}

	function get_kota_code() { 
		$('#negara_k, #prov_k').change(function () { 
			var negara_kd =  $('#negara_k').val();
			var provinsi_kd =  $('#prov_k').val();
			if(negara_kd != "-" && provinsi_kd != "-"){
				var id = $(this).val();
					$.ajax({
					type: 'GET',
					url: '<?php echo base_url().$class_link.'/get_last_code_kota'; ?>' + '?negara_kd='+negara_kd+'&prov_kd='+provinsi_kd,
					success: function(html) {
						if(html[0].q){
							var items = parseInt(html[0].q) + 1;
							if(counting(items) == 1){
								items = '00' + items;
							}else if(counting(items) == 2){
								items = '0' + items;
							}
							$('#id_kota').val(items);
						}else{
							$('#id_kota').val('001');
						}
						
					}
				});
			}
		});
	 }

	 function counting(num) {
		return String(num).split('').reduce(
			(count, digit) => count + 1, 0);
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function edit_data(id){
		form_main(id) 
	}

	function hapus_data (id) {
		event.preventDefault();
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_hapus'; ?>',
				data: {id: id},
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						open_table();
					}else{
						notify (resp.status, resp.pesan, 'error');
					}
				}
			});
		}
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function submitData(form_id) {
		event.preventDefault();
		var id_form = document.getElementById(form_id);
        let url = "<?php echo base_url().$class_link; ?>/action_submit";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(id_form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					$('.errInput').html('');
					toggle_modal('', '');
					open_table();
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function submitDatap(form_id) {
		event.preventDefault();
		var id_form = document.getElementById(form_id);
        let url = "<?php echo base_url().$class_link; ?>/action_submit_prov";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(id_form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					$('.errInput').html('');
					toggle_modal('', '');
					open_table();
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function submitDatak(form_id) {
		event.preventDefault();
		var id_form = document.getElementById(form_id);
        let url = "<?php echo base_url().$class_link; ?>/action_submit_kota";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(id_form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					$('.errInput').html('');
					toggle_modal('', '');
					open_table();
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
			$('#<?php echo $box_overlay_idp; ?>').fadeIn();
			$('#<?php echo $box_overlay_idk; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
			$('#<?php echo $box_overlay_idp; ?>').fadeOut();
			$('#<?php echo $box_overlay_idk; ?>').fadeOut();
		}
	}	

</script>