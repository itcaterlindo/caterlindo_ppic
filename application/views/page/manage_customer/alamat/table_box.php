<?php
defined('BASEPATH') or exit('No direct script access allowed!');

/* --SET DEFAULT PROPERTY UNTUK BOX-- */
$js_file = 'table_js';
$box_type = 'Table';
$box_title = 'Table Negara';
$data['master_var'] = 'negara';
$data['class_link'] = $class_link;
$data['box_id'] = 'id'.$box_type.'Box'.$data['master_var'];
$data['btn_hide_id'] = 'id'.$box_type.'BtnBoxHide'.$data['master_var'];
$data['btn_add_id'] = 'id'.$box_type.'BtnBoxAdd'.$data['master_var'];
$data['btn_remove_id'] = 'id'.$box_type.'BtnBoxRemove'.$data['master_var'];
$data['box_alert_id'] = 'id'.$box_type.'BoxAlert'.$data['master_var'];
$data['box_loader_id'] = 'id'.$box_type.'BoxLoader'.$data['master_var'];
$data['box_content_id'] = 'id'.$box_type.'BoxContent'.$data['master_var'];
$data['box_overlay_id'] = 'id'.$box_type.'BoxOverlay'.$data['master_var'];


$js_file = 'table_js';
$box_type = 'Table';
$box_titlep = 'Table Provinsi';
$data['master_varp'] = 'provinsi';
$data['class_link'] = $class_link;
$data['box_idp'] = 'id'.$box_type.'Box'.$data['master_varp'];
$data['btn_hide_idp'] = 'id'.$box_type.'BtnBoxHide'.$data['master_varp'];
$data['btn_add_idp'] = 'id'.$box_type.'BtnBoxAdd'.$data['master_varp'];
$data['btn_remove_idp'] = 'id'.$box_type.'BtnBoxRemove'.$data['master_varp'];
$data['box_alert_idp'] = 'id'.$box_type.'BoxAlert'.$data['master_varp'];
$data['box_loader_idp'] = 'id'.$box_type.'BoxLoader'.$data['master_varp'];
$data['box_content_idp'] = 'id'.$box_type.'BoxContent'.$data['master_varp'];
$data['box_overlay_idp'] = 'id'.$box_type.'BoxOverlay'.$data['master_varp'];



$js_file = 'table_js';
$box_type = 'Table';
$box_titlek = 'Table Kota';
$data['master_vark'] = 'kota';
$data['class_link'] = $class_link;
$data['box_idk'] = 'id'.$box_type.'Box'.$data['master_vark'];
$data['btn_hide_idk'] = 'id'.$box_type.'BtnBoxHide'.$data['master_vark'];
$data['btn_add_idk'] = 'id'.$box_type.'BtnBoxAdd'.$data['master_vark'];
$data['btn_remove_idk'] = 'id'.$box_type.'BtnBoxRemove'.$data['master_vark'];
$data['box_alert_idk'] = 'id'.$box_type.'BoxAlert'.$data['master_vark'];
$data['box_loader_idk'] = 'id'.$box_type.'BoxLoader'.$data['master_vark'];
$data['box_content_idk'] = 'id'.$box_type.'BoxContent'.$data['master_vark'];
$data['box_overlay_idk'] = 'id'.$box_type.'BoxOverlay'.$data['master_vark'];
/* --END OF BOX DEFAULT PROPERTY-- */

/* --SET VARIABEL TAMBAHAN DISINI-- */
$data['content_modal_id'] = 'id'.$box_type.'ModalContentDetail'.$data['master_var'];
$data['content_modal_idp'] = 'id'.$box_type.'ModalContentDetail'.$data['master_varp'];
$data['content_modal_idk'] = 'id'.$box_type.'ModalContentDetail'.$data['master_vark'];
/* --END OF CUSTOM PROPERTY-- */

/* --SET BUTTONS DISINI, NILAI "FALSE" UNTUK TIDAK TAMPIL, NILAI "TRUE" UNTUK MENAMPILKAN BUTTON-- */
$btn_hide = TRUE;
$btn_add = cek_permission('ALAMAT_CREATE');
$btn_remove = FALSE;

$btn_hidek = TRUE;
$btn_removek = FALSE;
/* --END OF BUTTONS SETUP-- */
?>

		<?php
			if ($btn_add) :
				?>
				<button type="button"  id="<?php echo $data['btn_add_id']; ?>" class="btn btn-primary">Add Negara</button>
				<button type="button" id="<?php echo $data['btn_add_idp']; ?>" class="btn btn-primary">Add Propinsi</button>
				<button type="button" id="<?php echo $data['btn_add_idk']; ?>" class="btn btn-primary">Add Kota</button>
				<?php
			endif;
		?>

<br>
<br>

<!-- //table Kota -->
<div class="box box-primary" id="<?php echo $data['box_idk']; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_titlek; ?></h3>
		<div class="box-tools pull-right">
			<?php
			if ($btn_hidek) :
				?>
				<button class="btn btn-box-tool btn-collapse" id="<?php echo $data['btn_hide_idk']; ?>" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
					<i class="fa fa-minus"></i>
				</button>
				<?php
			endif;
			if ($btn_removek) :
				?>
				<button class="btn btn-box-tool btn-remove" id="<?php echo $data['btn_remove_idk']; ?>" data-widget="remove" data-toggle="tooltip" title="Close">
					<i class="fa fa-times"></i>
				</button>
				<?php
			endif;
			?>
		</div>
	</div>
	<div class="box-body">
		<div id="<?php echo $data['box_alert_idk']; ?>"></div>
		<div id="<?php echo $data['box_loader_idk']; ?>" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="<?php echo $data['box_content_idk']; ?>"></div>
	</div>
	<div class="box-footer">
	</div>
	<div class="overlay" id="<?php echo $data['box_overlay_idk']; ?>" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
	<?php $this->load->view('page/'.$class_link.'/'.$js_file, $data); ?>
</div>

<!-- //table negara -->
<div class="box box-primary collapsed-box" id="<?php echo $data['box_id']; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_title; ?></h3>
		<div class="box-tools pull-right">
			<?php
			if ($btn_hide) :
				?>
				<button class="btn btn-box-tool btn-collapse" id="<?php echo $data['btn_hide_id']; ?>" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
					<i class="fa fa-minus"></i>
				</button>
				<?php
			endif;
			if ($btn_remove) :
				?>
				<button class="btn btn-box-tool btn-remove" id="<?php echo $data['btn_remove_id']; ?>" data-widget="remove" data-toggle="tooltip" title="Close">
					<i class="fa fa-times"></i>
				</button>
				<?php
			endif;
			?>
		</div>
	</div>
	<div class="box-body">
		<div id="<?php echo $data['box_alert_id']; ?>"></div>
		<div id="<?php echo $data['box_loader_id']; ?>" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="<?php echo $data['box_content_id']; ?>"></div>
	</div>
	<div class="box-footer">
	</div>
	<div class="overlay" id="<?php echo $data['box_overlay_id']; ?>" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
	
</div>

<!-- //table propinsi -->
<div class="box box-primary collapsed-box" id="<?php echo $data['box_idp']; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_titlep; ?></h3>
		<div class="box-tools pull-right">
			<?php
			if ($btn_hide) :
				?>
				<button class="btn btn-box-tool btn-collapse" id="<?php echo $data['btn_hide_idp']; ?>" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
					<i class="fa fa-minus"></i>
				</button>
				<?php
			endif;
			if ($btn_remove) :
				?>
				<button class="btn btn-box-tool btn-remove" id="<?php echo $data['btn_remove_idp']; ?>" data-widget="remove" data-toggle="tooltip" title="Close">
					<i class="fa fa-times"></i>
				</button>
				<?php
			endif;
			?>
		</div>
	</div>
	<div class="box-body">
		<div id="<?php echo $data['box_alert_idp']; ?>"></div>
		<div id="<?php echo $data['box_loader_idp']; ?>" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="<?php echo $data['box_content_idp']; ?>"></div>
	</div>
	<div class="box-footer">
	</div>
	<div class="overlay" id="<?php echo $data['box_overlay_idp']; ?>" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
	<?php $this->load->view('page/'.$class_link.'/'.$js_file, $data); ?>
</div>






<!-- modal -->
<div class="modal fade" id="idmodal">
	<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"><?php echo $data['modal_title']; ?></h4>
		</div>
		<div class="modal-body">

			<div class="row">
				<div id="<?php echo $data['content_modal_id'];?>"></div>
			</div>
			
		</div>
		<div class="modal-footer">
		<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Keluar</button>
		</div>
	</div>
	</div>
</div>

<div class="modal fade" id="idmodalp">
	<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"><?php echo $data['modal_titlep']; ?></h4>
		</div>
		<div class="modal-body">

			<div class="row">
				<div id="<?php echo $data['content_modal_idp'];?>"></div>
			</div>
			
		</div>
		<div class="modal-footer">
		<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Keluar</button>
		</div>
	</div>
	</div>
</div>

<div class="modal fade" id="idmodalk">
	<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"><?php echo $data['modal_titlek']; ?></h4>
		</div>
		<div class="modal-body">

			<div class="row">
				<div id="<?php echo $data['content_modal_idk'];?>"></div>
			</div>
			
		</div>
		<div class="modal-footer">
		<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Keluar</button>
		</div>
	</div>
	</div>
</div>