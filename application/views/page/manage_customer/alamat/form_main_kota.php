<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputMasterP';
if($sts == 'edit'){
	extract($rowData);
}

?>

<?php
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxt_kd', 'name'=> 'txt_kd', 'value' => $id ));
?>
<div class="row">
	<div class="col-md-12">
	<div class="form-group">
		<label for='idtxtcustgroup_nama' class="col-md-3 control-label">Kode Kota</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrcustgroup_name"></div>
			<input type="text" class="form-control" name="id_kota" id="id_kota" value="<?php echo isset($kd_kota) ? $kd_kota: null ?>" readonly>
		</div>
	</div>
	<div class="col-md-12">
	<div class="form-group">
		<label for='idtxtcustgroup_nama' class="col-md-3 control-label">Negara</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrcustgroup_name"></div>
			<select class="form-control" name="negara_k" id="negara_k">
			<option value="-">-- Pilih negara --</option>
			<?php foreach($negara as $q){
				if (isset($negara_kd)) {
					if($negara_kd == $q->kd_negara){
				?>
					<option value="<?php echo $q->kd_negara ?>" selected><?php echo $q->nm_negara ?></option>
				<?php
					}
				}
				?>
				<option value="<?php echo $q->kd_negara ?>"><?php echo $q->nm_negara ?></option>
				<?php
			}
			
			?>
			</select>
		</div>
	</div>
		</div>
	<div class="col-md-12">
	<div class="form-group">
		<label for='idtxtcustgroup_nama' class="col-md-3 control-label">Provinsi</label>	
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrcustgroup_name"></div>
			<select class="form-control" name="prov_k" id="prov_k">
			<option value="-">-- Pilih Provinsi --</option>
			<?php foreach($prov as $q){
				if (isset($provinsi_kd)) {
					if($provinsi_kd == $q->kd_provinsi && $negara_kd == $q->negara_kd){
				?>
					<option value="<?php echo $q->kd_provinsi ?>" selected><?php echo $q->nm_provinsi ?></option>
				<?php
					}
				}
				?>
				<option value="<?php echo $q->kd_provinsi ?>"><?php echo $q->nm_provinsi ?></option>
				<?php
			}
			
			?>
			</select>
		</div>
	</div>
	</div>
	<div class="col-md-12">
	<div class="form-group">
		<label for='idtxtcustgroup_nama' class="col-md-3 control-label">Kota</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrcustgroup_name"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'nm_kota', 'id'=> 'nm_kota', 'placeholder' =>'Nama Kota', 'value'=> isset($nm_kota) ? $nm_kota: null ));?>
		</div>
	</div>
	</div>
<hr>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-1 col-xs-12 col-sm-offset-10">
			<button type="submit" name="btnSubmit" onclick="submitDatak('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>
