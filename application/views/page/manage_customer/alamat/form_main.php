<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputMaster';
if($sts == 'edit'){
	extract($rowData);
}

?>

<?php
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxt_kd', 'name'=> 'txt_kd', 'value' => $id ));
?>
<div class="row">
	<div class="col-md-12">
	<div class="form-group">
		<label for='idtxtcustgroup_nama' class="col-md-3 control-label">ID Negara</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrcustgroup_name"></div>
			<input type="text" class="form-control" name="id_negara" id="id_negara" readonly>
		</div>
	</div>
	<div class="col-md-12">
	<div class="form-group">
		<label for='idtxtcustgroup_nama' class="col-md-3 control-label">Negara</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrcustgroup_name"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'nm_negara', 'id'=> 'nm_negara', 'placeholder' =>'Nama Negara', 'value'=> isset($nm_negara) ? $nm_negara: null ));?>
		</div>
	</div>
<hr>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-1 col-xs-12 col-sm-offset-10">
			<button type="submit" name="btnSubmit" onclick="submitData('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>
