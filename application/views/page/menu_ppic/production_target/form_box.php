<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$js_file = 'form_js';
$box_title = 'Form Production Target';
$data['class_link'] = $class_link;
$box_type = 'Table';
$data['master_var'] = 'ProductionTarget';
$data['box_id'] = 'id' . $box_type . 'Box' . $data['master_var'];
$data['box_content_id'] = 'id' . $box_type . 'BoxContent' . $data['master_var'];
$data['box_loader_id'] = 'id' . $box_type . 'BoxLoader' . $data['master_var'];
$data['btn_add_id'] = 'id' . $box_type . 'BtnBoxAdd' . $data['master_var'];
$data['btn_remove_id'] = 'id' . $box_type . 'BtnBoxRemove' . $data['master_var'];
if (isset($row)) {
	$bulan_target = $row->bulan_target;
	$tahun_target = $row->tahun_target;
	$angka_target = $row->angka_target;
}
?>

<div class="box box-primary" id="<?php echo $data['box_id']; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_title; ?></h3>
		<div class="box-tools pull-right">
			<!-- <button class="btn btn-box-tool" id="<?php //echo $data['btn_add_id'];
														?>" data-toggle="tooltip" title="Tambah"><i class="fa fa-plus"></i></button> -->
			<button class="btn btn-box-tool btn-collapse" id="idBtnBoxTableSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
				<i class="fa fa-minus"></i>
			</button>
			<button class="btn btn-box-tool btn-remove" id="<?php echo $data['btn_remove_id']; ?>" data-widget="remove" data-toggle="tooltip" title="Close">
				<i class="fa fa-times"></i>
			</button>
		</div>
	</div>
	<div class="box-body">
		<div id="<?php echo $data['box_loader_id']; ?>" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="<?php echo $data['box_content_id']; ?>" style="display: none;"></div>

		<?php
		echo form_open('', array('id' => 'idForm', 'class' => 'form-horizontal'));
		echo form_input(array('type' => 'hidden', 'name' => 'txtslug', 'id' => 'idtxtslug', 'placeholder' => 'slug', 'value' => isset($slug) ? $slug : 'add'));
		echo form_input(array('type' => 'hidden', 'name' => 'txtid_target', 'id' => 'idtxtid_target', 'placeholder' => 'id_target', 'value' => isset($id_target) ? $id_target : null));
		?>

		<div class="form-group">
			<label for='idtxtbln_target' class="col-md-2 control-label">Bulan</label>
			<div class="col-sm-3 col-xs-12">
				<select class="form-control" name="txtbln_target" id="idtxtbln_target">
					<option value="1" <?php if($bulan_target == "1"){echo 'selected="selected"';} ?>>Januari</option>
					<option value="2" <?php if($bulan_target == "2"){echo 'selected="selected"';} ?>>Februari</option>
					<option value="3" <?php if($bulan_target == "3"){echo 'selected="selected"';} ?>>Maret</option>
					<option value="4" <?php if($bulan_target == "4"){echo 'selected="selected"';} ?>>April</option>
					<option value="5" <?php if($bulan_target == "5"){echo 'selected="selected"';} ?>>Mei</option>
					<option value="6" <?php if($bulan_target == "6"){echo 'selected="selected"';} ?>>Juni</option>
					<option value="7" <?php if($bulan_target == "7"){echo 'selected="selected"';} ?>>Juli</option>
					<option value="8" <?php if($bulan_target == "8"){echo 'selected="selected"';} ?>>Agustus</option>
					<option value="9" <?php if($bulan_target == "9"){echo 'selected="selected"';} ?>>September</option>
					<option value="10" <?php if($bulan_target == "10"){echo 'selected="selected"';} ?>>Oktober</option>
					<option value="11" <?php if($bulan_target == "11"){echo 'selected="selected"';} ?>>November</option>
					<option value="12" <?php if($bulan_target == "12"){echo 'selected="selected"';} ?>>Desember</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for='idtxttahun_target' class="col-md-2 control-label">Tahun</label>
			<div class="col-sm-3 col-xs-12">
				<select class="form-control" name="txttahun_target" id="idtxttahun_target">
					<?php
					foreach ($year as $v) {
						if($v == $tahun_target){$slc = 'selected="selected"';}else{$slc="";}
						echo '<option value="' . $v . '" ' . $slc . '>' . $v . '</option>';
					}
					?>

				</select>
			</div>
		</div>
		<!-- <br>
		<br> -->
		<div class="form-group">
			<label for='idtxtangka_target' class="col-md-2 control-label">Target (harian)</label>
			<div class="col-sm-5 col-xs-12">
				<div class="err" id="iderrangka_target"></div>
				<?php echo form_input(array('type' => 'text', 'class' => 'form-control', 'name' => 'txtangka_target', 'id' => 'idtxtangka_target', 'placeholder' => 'Target Harian', 'value' => isset($angka_target) ? $angka_target : null)); ?>
			</div>
		</div>

	</div>
	<div class="box-footer">
		<button class="btn btn-sm btn-success pull-right" onclick="submitData('idForm')"> <i class="fa fa-save"></i> Simpan</button>
	</div>
	<?php echo form_close(); ?>
	<div class="overlay" id="idBoxTableOverlay" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
</div>

<script type="text/javascript">
	first_load('<?php echo $data['box_loader_id']; ?>', '<?php echo $data['box_content_id']; ?>');
</script>
<?php
//$this->load->view('page/'.$class_link.'/'.$js_file, $data); 
?>