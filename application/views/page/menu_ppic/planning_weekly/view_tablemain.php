<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }
</style>

<table id="idtablemain" cellspacing="0" cellpadding="1" style="font-size: 80%;" border="1" width="100%">
    <thead>
        <tr>
            <th width="5%" style="text-align: center; font-weight:bold;">No.</th>
            <th width="30%" style="text-align: center; font-weight:bold;">Product Code</th>
            <th width="15%" style="text-align: center; font-weight:bold;">Qty</th>
            <th width="20%" style="text-align: center; font-weight:bold;">No WO</th>
            <th width="15%" style="text-align: center; font-weight:bold;">Keterangan</th>
            <th width="15%" style="text-align: center; font-weight:bold;">Material Req</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        $sumQty = 0;
        foreach ($results as $r) :
            $sumQty += $r['planningweeklydetail_qty'];
        ?>
            <tr>
                <td width="5%" style="text-align: center;"><?php echo $no; ?></td>
                <td width="30%"><?php echo $r['woitem_itemcode']; ?></td>
                <td width="15%" style="text-align: right;"><?php echo $r['planningweeklydetail_qty']; ?></td>
                <td width="20%" style="text-align: center;"><?php echo $r['woitem_no_wo']; ?></td>
                <td width="15%" style="text-align: left;"><?php echo $r['planningweeklydetail_note']; ?></td>
                <td width="15%" style="text-align: left;">
                    <?php
                    $html = '<ul>'; 
                    foreach($materialreqdetails as $materialreqdetail) :
                        if ($materialreqdetail['planningweeklydetail_kd'] == $r['planningweeklydetail_kd']) {
                            $html .= "<li>{$materialreqdetail['materialreq_no']} / {$materialreqdetail['materialreqdetail_qty']}</li>";
                        }
                    endforeach;
                    $html .= '<ul>';
                    echo $html;
                    ?>
                </td>
            </tr>
        <?php
            $no++;
        endforeach; ?>
    </tbody>
    <tfoot>
        <tr style="font-weight: bold;">
            <td colspan="2" style="text-align: right;">Jumlah : </td>
            <td style="text-align: right;"><?php echo $sumQty; ?></td>
            <td></td>
            <td></td>
        </tr>
    </tfoot>
</table>