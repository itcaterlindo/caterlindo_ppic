
<script type="text/javascript">
    view_tablemain('<?php echo $id; ?>');
    view_log('<?php echo $id; ?>');

    function view_tablemain(id){
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url().$class_link.'/view_tablemain'; ?>',
            data: {id: id},
            success: function(html) {
                $('#<?php echo $box_content_id; ?>').html(html);
				$('#idtablemain').css({fontSize: '100%'});
            }
        });
	}

	function ubah_state(id, wftransition_kd) {
		 $.ajax({
            type: 'GET',
            url: '<?php echo base_url().$class_link.'/formubahstate_main'; ?>',
            data: {id:id, wftransition_kd: wftransition_kd},
            success: function(html) {
                toggle_modal('Ubah State', html);
            }
        });
	}

	function view_log(id){
		$.ajax({
            type: 'GET',
            url: '<?php echo base_url().$class_link.'/view_log'; ?>',
            data: {id: id},
            success: function(html) {
                $('#idMainContent').append(html);
            }
        });
	}

	function submitDataState (form_id) {
		event.preventDefault();
		$('#idbtnSubmit'+form_id).html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtnSubmit'+form_id).attr('disabled', true);
		var form = document.getElementById(form_id);

		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_change_state" ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					//window.location.reload();
					if(resp.stts.status == 1){
						sendNotifMail(resp)
						//window.location.reload();
					}else{
						window.location.reload();
					}
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function sendNotifMail(resp) {

		var data = {
				service_id: 'service_89wfszf',
				template_id: 'planing_wxx',
				user_id: 'user_gqom4wS5NcBXsu8XDttVI',
				template_params: {
				pwno: resp.msg.subjcet,
				note: resp.msg.keterangan,
				urlxx:resp.msg.url,
				cc: '',
				to: resp.stts.to
			}
		};


		//console.log(data)//
		$.ajax('https://api.emailjs.com/api/v1.0/email/send', {
		type: 'POST',
		data: JSON.stringify(data),
		contentType: 'application/json'
			}).done(function() {
				notify ('OK! 200.', 'Berhasil.', 'success');
				window.location.reload();
			}).fail(function(error) {
				notify('Oops… ' + JSON.stringify(error));
			})
		}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }
</script>