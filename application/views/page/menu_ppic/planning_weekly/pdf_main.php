<?php
class customPdf extends Tcpdf
{
    public $planningweekly_no;
    public $bagian_nama;
    public $planningweekly_start;
    public $planningweekly_end;

    public function setsHeader($headerData)
    {
        $this->planningweekly_no = $headerData['planningweekly_no'];
        $this->bagian_nama = $headerData['bagian_nama'];
        $this->planningweekly_start = $headerData['planningweekly_start'];
        $this->planningweekly_end = $headerData['planningweekly_end'];
    }

    public function Header()
    {
        $header = '
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td width="30%" rowspan="4" style="text-align:left;"> <img src="assets/admin_assets/dist/img/logo_cat.png" width="135" height="35"> </td>
                <td width="35%" rowspan="4" style="text-align: center; "></td>
                <td width="35%" colspan="2" style="text-align:right; font-size: 120%; font-weight:bolt;"> PRODUCTION PLANNING</td>
            </tr>
            <tr>
                <td style="text-align: left; font-size: 75%;">No Dokumen</td>
                <td style="text-align: left; font-size: 75%;">: CAT4-PRO-005</td>
            </tr>
            <tr>    
                <td style="text-align: left; font-size: 75%;">Tanggal Terbit</td>
                <td style="text-align: left; font-size: 75%;">: 30-12-2021</td>
            </tr>
            <tr>
                <td style="text-align: left; font-size: 75%;">Rev</td>
                <td style="text-align: left; font-size: 75%;">: 05</td>
            </tr>
        </table>';

        /** Keterangan */
        $keterangan =
            '<table cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 80%;">
            <tr>
                <td width="20%"> No </td>
                <td width="30%"> : ' . $this->planningweekly_no . ' </td>
                <td width="20%"> Divisi </td>
                <td width="30%"> : ' . $this->bagian_nama . ' </td>
            </tr>
            <tr>
                <td width="20%"> Start Date </td>
                <td width="30%"> : ' . $this->planningweekly_start . ' </td>
                <td width="20%"> Tgl Cetak </td>
                <td width="30%"> : ' . date('Y-m-d') . ' </td>
            </tr>
            <tr>
                <td width="20%"> Finish Date </td>
                <td width="30%"> : ' . $this->planningweekly_end . ' </td>
                <td></td>    
                <td></td>    
            </tr>
        </table>';

        $this->writeHTML($header, true, false, false, false, '');
        $this->writeHTML($keterangan, true, false, false, false, '');
    }
}

$pdf = new customPdf('P', 'mm', 'A4', true, 'UTF-8', false);
$title = $master['planningweekly_no'];
$pdf->SetTitle($title);
$pdf->SetAutoPageBreak(true, 10);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');
$pdf->setPrintFooter(true);

/** Margin */
$pdf->SetHeaderMargin(5);
$pdf->SetFooterMargin(7);
$pdf->SetMargins(8, 42, 8);

$dataHeader = array(
    'bagian_nama' => "{$master['divisi_nama']}({$master['bagian_nama']})",
    'planningweekly_no' => $master['planningweekly_no'],
    'planningweekly_start' => $master['planningweekly_start'],
    'planningweekly_end' => $master['planningweekly_end'],
);
$pdf->setsHeader($dataHeader);

$pdf->AddPage();


$pdf->writeHTML($konten, true, false, false, false, '');

$footer = '<table cellspacing="0" cellpadding="0" border="1" style="font-size: 80%;" width="70%">';
foreach ($logs as $log) {
    $footer .= 
    ' <tr>
        <td>'.$log['wfstate_nama'].' by</td>
        <td> :'.$log['nm_admin'].'</td>
        <td>'.$log['planningweeklylog_tglinput'].'</td>
    </tr>';
}                      
$footer .= '</table>';

$pdf->writeHTML($footer, true, false, false, false, '');


$txtOutput = $title . '.pdf';
$pdf->Output($txtOutput, 'I');
