<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	table_main();
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		form_master_box('add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function table_main() {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/table_main'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').slideDown().html(html);
					moveTo('idMainContent');
				}
			});
		});
	}

	function form_master_box(slug, id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/form_master_box'; ?>',
			data: {
				slug: slug,
				id: id
			},
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function render_select2(element) {
		$(element).select2({
			theme: 'bootstrap',
			width: '100%',
			placeholder: '-- Pilih Opsi --',
		});
	}

	function render_datetimepicker(element) {
		$(element).datetimepicker({
			format: 'YYYY-MM-DD',
		});
	}

	function ubah_status(id = null, status = 'pending') {
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/action_update_status'; ?>',
				data: {
					id: id,
					status: status
				},
				success: function(html) {
					table_main();
				}
			});
		}
	}

	function select_soitem(param) {
		event.preventDefault();
		let kd_msalesorder = $(param).data('kd_msalesorder');
		let item_code = $(param).data('item_code');
		let item_desc = $(param).data('item_desc');
		let item_dimension = $(param).data('item_dimension');
		let kd_ditem_so = $(param).data('kd_ditem_so');
		let kd_citem_so = $(param).data('kd_citem_so');
		let so = $(param).data('so');
		let so_item = $(param).data('so_item');
		toggle_modal('', '');
		$('#idtxtkd_msalesorder').val(kd_msalesorder);
		$('#idtxtkd_ditem_so').val(kd_ditem_so);
		$('#idtxtkd_citem_so').val(kd_citem_so);
		$('#idtxtno_salesorder').val(so);
		$('#idtxtso_item').val(so_item);
		let slug = $('#idtxtslug').val();
		if (slug == 'add') {
			$('#idtxtproject_itemcode').val(item_code);
			$('#idtxtproject_itemdesc').val(item_desc);
			$('#idtxtproject_itemdimension').val(item_dimension);
		}
	}

	function clear_so() {
		event.preventDefault();
		$('.dt-so').val('');
		toggle_modal('', '');
	}

	function render_so(idSelect) {
		$("#" + idSelect).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 2,
			ajax: {
				url: '<?php echo base_url() ?>auto_complete/get_salesorder',
				type: "get",
				dataType: 'json',
				delay: 500,
				data: function(params) {
					return {
						paramSO: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function edit_data(id) {
		form_master_box('edit', id);
		// window.location.assign('<?php echo base_url() . $class_link ?>/form_box?id=' + id);
	}
	
	function cetak_data(id) {
		window.open('<?php echo base_url() . $class_link ?>/pdf_main?id=' + id);
	}

	function lihat_data(id) {
		window.location.assign('<?php echo base_url() . $class_link ?>/view_box?id=' + id);
	}

	function toggle_modal(modalTitle, htmlContent) {
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			delay: 2500,
			styling: 'bootstrap3'
		});
	}

	function submitData(form_id) {
		event.preventDefault();
		var form = document.getElementById('idForm');
		var url = "<?php echo base_url() . $class_link . '/action_insert'; ?>";
		$.ajax({
			url: url,
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				var resp = JSON.parse(data);
				console.log(resp);
				if (resp.code == 200) {
					$('#idTableBoxMaterialRequisition').remove();
					table_main();
					notify(resp.status, resp.pesan, 'success');
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
			}
		});
	}

	function hapus_data(id) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			var url = '<?php echo base_url() . $class_link ?>/action_delete_master';
			$.ajax({
				url: url,
				type: 'GET',
				data: 'id=' + id,
				success: function(data) {
					var resp = JSON.parse(data);
					if (resp.code == 200) {
						notify(resp.pesan, 'Data Terhapus', 'success');
						table_main();
					} else {
						notify('Gagal', resp.pesan, 'error');
					}
				}
			});
		}
	}
</script>