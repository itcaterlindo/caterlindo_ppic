<?php
defined('BASEPATH') or exit('No direct script access allowed');
$form_id = 'idFormMaster';
if (isset($rowData)) {
    extract($rowData);
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtslug', 'name' => 'txtslug', 'value' => isset($slug) ? $slug : null));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtplanningweekly_kd', 'name' => 'txtplanningweekly_kd', 'value' => isset($id) ? $id : null));
?>

<div class="col-md-12">
    <button class="btn btn-success btn-sm" onclick="event.preventDefault();; window.location.assign('<?php echo base_url() . '/menu_ppic/planning_weekly'; ?>');"> <i class="fa fa-check"></i> Selesai</button>
    <!-- <button class="btn btn-sm btn-default pull-right" onclick="window.history.back()"> <i class="fa fa-arrow-circle-left"></i> Kembali </button> -->

    <hr>
</div>

<div class="col-md-12">
    <div class="form-group">
        <label for="idjeniswo" class="col-sm-2 control-label">List WO</label>
        <div class="col-md-2 col-xs-12">
            <div class="errClass" id="idErrjeniswo"></div>
            <?php echo form_dropdown('txtjeniswo', ['SUGGESTED' => 'SUGGESTED', 'ALL' => 'ALL'], isset($bagian_kd) ? $bagian_kd : '', array('id' => 'idtxtjeniswo', 'class' => 'form-control select2')); ?>
        </div>
        <div class="col-md-2 col-xs-12">
            <button class="btn btn-sm btn-default" onclick="cariWo()"> <i class="fa fa-search"></i> Cari</button>
        </div>
    </div>
   
</div>

<!-- /.box-body -->
<?php echo form_close(); ?>