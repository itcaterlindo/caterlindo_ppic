<?php
defined('BASEPATH') or exit('No direct script access allowed');
$form_id = 'idFormMaster';
if (isset($rowData)) {
    extract($rowData);
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtslug', 'name' => 'txtslug', 'value' => isset($slug) ? $slug : null));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtplanningweekly_kd', 'name' => 'txtplanningweekly_kd', 'value' => isset($planningweekly_kd) ? $planningweekly_kd : null));
?>

<div class="col-md-12">
    <div class="form-group">
        <label for="iddivisi_kd" class="col-sm-2 control-label" onclick="auto_select()">Divisi</label>
        <div class="col-md-4">
            <div class="errClass" id="idErrdivisi_kd"></div>
            <?php echo form_dropdown('txtdivisi_kd', isset($opsiBagian) ? $opsiBagian : [], isset($divisi_kd) ? $divisi_kd : '', array('id' => 'idtxtdivisi_kd', 'class' => 'form-control select2', 'onchange' => 'auto_select()')); ?>
        </div>
    </div>
    <div class="form-group">
        <label for="idbagian_kd" class="col-sm-2 control-label">Bagian (BOM)</label>
        <div class="col-md-4">
            <div class="errClass" id="idErrbagian_kd"></div>
            <?php echo form_dropdown('txtbagian_kd', isset($opsiBagian) ? $opsiBagian : [], isset($bagian_kd) ? $bagian_kd : '', array('id' => 'idtxtbagian_kd', 'class' => 'form-control select2')); ?>
        </div>
    </div>
    <div class="form-group">
        <label for="idplanningweekly_start" class="col-sm-2 control-label">Start</label>
        <div class="col-md-4 col-xs-12">
            <div id="idErrplanningweekly_start"></div>
            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <?php echo form_input(array('type' => 'text', 'id' => 'idtxtplanningweekly_start', 'name' => 'txtplanningweekly_start', 'class' => 'form-control datetimepicker', 'placeholder' => 'Start', 'value' => isset($planningweekly_start) ? $planningweekly_start: date('Y-m-d'))); ?>
            </div>
        </div>
        <label for="idplanningweekly_end" class="col-sm-1 control-label">End</label>
        <div class="col-md-4 col-xs-12">
            <div id="idErrplanningweekly_end"></div>
            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <?php echo form_input(array('type' => 'text', 'id' => 'idtxtplanningweekly_end', 'name' => 'txtplanningweekly_end', 'class' => 'form-control datetimepicker', 'placeholder' => 'End', 'value' => isset($planningweekly_end) ? $planningweekly_end: date('Y-m-d'))); ?>
            </div>
        </div>
    </div>

<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2 col-xs-12">
        <button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitDataMaster('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm btn-flat">
            <i class="fa fa-save"></i> Simpan
        </button>
        <button type="reset" name="btnReset" class="btn btn-default btn-sm btn-flat">
            <i class="fa fa-refresh"></i> Reset
        </button>
    </div>
</div>
</div>

<!-- /.box-body -->
<?php echo form_close(); ?>