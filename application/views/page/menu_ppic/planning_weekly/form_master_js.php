<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	form_master_main('<?php echo $slug; ?>', '<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function form_master_main(slug, id) {
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/form_master_main'; ?>',
				data: {
					slug: slug,
					id: id
				},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').slideDown().html(html);
					render_select2('.select2');
					render_datetimepicker('.datetimepicker')
					moveTo('idMainContent');
				}
			});
		});
	}

	function submitDataMaster(form_id) {
		box_overlay_formmaster('in');
		event.preventDefault();
		var form = document.getElementById(form_id);
		var url = '<?php echo base_url() . $class_link . '/action_insert_master'; ?>';

		$.ajax({
			url: url,
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				var resp = JSON.parse(data);
				console.log(resp);
				if (resp.code == 200) {
					window.location.assign('<?php echo base_url() . $class_link ?>/form_box?id=' + resp.data.id);
					notify(resp.status, resp.pesan, 'success');
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
				box_overlay_formmaster('out');
			}
		});
	}

	function toggle_modal(modalTitle, htmlContent) {
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function box_overlay_formmaster(sts) {
		if (sts == 'in') {
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		} else if (sts == 'out') {
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

	function auto_select() {

		let divisi = document.getElementById('idtxtdivisi_kd').value;
		// console.log(divisi);

		/** menyesuaikan divisi dan bagian */
		if(divisi == 11 || divisi == 51){
			divisi -= 1;
		}else if(divisi == 90){
			divisi = 20;
		}
		$('[name=txtbagian_kd]').val(divisi);

		let bagian = document.getElementById('idtxtbagian_kd').value;
		// console.log(bagian);

		let divisi_txt = $('[name=txtbagian_kd]').find(":selected").text();
		document.getElementById('select2-idtxtbagian_kd-container').innerHTML = divisi_txt;
	}
</script>