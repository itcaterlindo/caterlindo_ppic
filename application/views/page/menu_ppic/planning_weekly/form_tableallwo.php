<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$form_id = 'idFormBatchAll';
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<?php echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal')); ?>
		<input type="hidden" name="txtplanningweekly_kd" value="<?php echo $id; ?>">
		<table id="idTableSuggested" class="table table-bordered table-striped table-hover display" style="width:100%; font-size: 90%;">
			<thead>
				<tr>
					<th style="width:1%; text-align: center;">Opsi</th>
					<th style="width:10%; text-align: center;">No WO</th>
					<th style="width:10%; text-align: center;">Item Code</th>
					<th style="width:10%; text-align: center;">Jenis</th>
					<th style="width:15%; text-align: center;">Deskripsi</th>
					<th style="width:10%; text-align: center;">Qty WO</th>
					<th style="width:10%; text-align: center;">Qty Input</th>
					<th style="width:10%; text-align: center;">Note</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($results as $r) : ?>
					<tr>
						<td><input type="checkbox" name="txtwoitem_kds[]" value="<?php echo $r['woitem_kd']; ?>"></td>
						<td><?php echo $r['woitem_no_wo']; ?></td>
						<td><?php echo $r['woitem_itemcode']; ?></td>
						<td><?php echo $r['woitem_jenis']; ?></td>
						<td><?php echo "{$r['woitem_deskripsi']} {$r['woitem_dimensi']}"; ?></td>
						<td><?php echo $r['woitem_qty']; ?></td>
						<td><input type="number" class="form-control" placeholder="Qty" name="txtplanningweeklydetail_qty[<?php echo $r['woitem_kd']; ?>]" value="<?php echo $r['woitem_qty']; ?>"></td>
						<td> <textarea name="txtplanningweeklydetail_notes[<?php echo $r['woitem_kd']; ?>]" class="form-control" placeholder="Note" rows="1"></textarea></td>
					</tr>
				<?php
				endforeach; ?>
			</tbody>
		</table>
		<hr>
		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-11 col-xs-12">
				<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitDataBatch('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
					<i class="fa fa-save"></i> Simpan
				</button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>


<script type="text/javascript">
	$('#idTableSuggested').DataTable({
		"paging": false,
		"ordering": true,
		"searching": true,
		"scrollY": "250px",
		"info": false,
		"language": {
			"lengthMenu": "Tampilkan _MENU_ data",
			"zeroRecords": "Maaf tidak ada data yang ditampilkan",
			"info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "(difilter dari _MAX_ total data)",
			"infoEmpty": "Tidak ada data yang ditampilkan",
			"search": "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing": "Sedang Memproses...",
			"paginate": {
				"first": '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last": '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next": '<span class="glyphicon glyphicon-forward"></span>',
				"previous": '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"order": [1, 'asc'],
	});
</script>