<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	form_main('<?php echo $slug; ?>', '<?php echo $id; ?>');
	table_detail_main('<?php echo $slug; ?>', '<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');
	first_load('<?php echo $box_loader_id2; ?>', '<?php echo $box_content_id2; ?>');

	function form_main(slug, id) {
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/form_main'; ?>',
				data: {
					slug: slug,
					id: id
				},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').slideDown().html(html);
					moveTo('idMainContent');
				}
			});
		});
	}

	function table_detail_main(slug, id) {
		$('#<?php echo $box_content_id_detail; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/table_detail_main'; ?>',
				data: {
					slug: slug,
					id: id
				},
				success: function(html) {
					$('#<?php echo $box_content_id_detail; ?>').slideDown().html(html);
					moveTo('idMainContent');
				}
			});
		});
	}

	function cariWo() {
		box_overlay('in');
		let id = $('#idtxtplanningweekly_kd').val();
		let jeniswo = $('#idtxtjeniswo').val();
		let url = '';
		switch (jeniswo) {
			case 'ALL':
				url = '<?php echo base_url() . $class_link . '/form_tableallwo'; ?>';
				break;
			case 'SUGGESTED':
				url = '<?php echo base_url() . $class_link . '/form_tablesuggestwo'; ?>';
				break;
		}
		event.preventDefault();
		$('#<?php echo $box_content_id2; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: url,
				data: {
					id: id
				},
				success: function(html) {
					$('#<?php echo $box_content_id2; ?>').slideDown().html(html);
				},
				complete: function() {
					box_overlay('out');
				}
			});
		});

	}

	function submitDataBatch(form_id) {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById(form_id);
		var url = '<?php echo base_url() . $class_link . '/action_insert_detail_batch'; ?>';

		$.ajax({
			url: url,
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				console.log(data);
				var resp = JSON.parse(data);
				if (resp.code == 200) {
					notify(resp.status, resp.pesan, 'success');
					table_detail_main('<?php echo $slug; ?>', '<?php echo $id; ?>');
					$('#<?php echo $box_content_id2; ?>').html('');
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
				box_overlay('out');
			}
		});
	}

	function hapus_data_detail(id) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			var url = '<?php echo base_url() . $class_link ?>/action_delete_detail';
			$.ajax({
				url: url,
				type: 'GET',
				data: 'id=' + id,
				success: function(data) {
					var resp = JSON.parse(data);
					if (resp.code == 200) {
						notify(resp.status, resp.pesan, 'success');
						table_detail_main('<?php echo $slug; ?>', '<?php echo $id; ?>');
					} else {
						notify('Gagal', resp.pesan, 'error');
					}
				}
			});
		}
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			delay: 2500,
			styling: 'bootstrap3'
		});
	}

	function toggle_modal(modalTitle, htmlContent) {
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function box_overlay(sts) {
		if (sts == 'in') {
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		} else if (sts == 'out') {
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}
</script>