<?php
defined('BASEPATH') or exit('No direct script access allowed!');
extract($master);

if ($ket_note_so == 'available') :
	?>
	<div class="row invoice-info print-foot" style="margin-top: 20px;">
		Catatan :
		<?php echo $note_so; ?>
	</div>
	<?php
endif;

$tipe_cust = strtolower($tipe_customer);
if ($tipe_cust == 'lokal') :
	?>
	<div class="row invoice-info print-foot">
		<div class="col-xs-3" style="text-align: center;">
			<div style="margin-bottom: 100px;">Dibuat Oleh,</div>
			<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
			<div><b>Local Sales Support</b></div>
		</div>
		<div class="col-xs-3" style="text-align: center;">
			<div style="margin-bottom: 100px;">Diperiksa Oleh,</div>
			<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
			<div><b>Local Sales & Marketing</b></div>
		</div>
		<div class="col-xs-3" style="text-align: center;">
			<div style="margin-bottom: 100px;">Disetujui Oleh,</div>
			<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
			<div><b>Head of Production, Sales & Marketing</b></div>
		</div>
		<div class="col-xs-3" style="text-align: center;">
			<div style="margin-bottom: 100px;">Diketahui Oleh,</div>
			<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
			<div><b>Operational Manager</b></div>
		</div>
	</div>
	<!-- <table class="div-footer" style="width: 100%;">
		<tbody>
			<tr>
				<td class="print-left" style="width: 30%; text-align: left;"><?php //echo $no_form; ?></td>
				<td class="print-center" style="width: 40%; text-align: center;">rev. : 02</td>
				<td class="print-right" style="width: 30%; text-align: right;"><?php //echo date('d-M-Y'); ?></td>
			</tr>
		</tbody>
	</table> -->
	<?php
elseif ($tipe_cust == 'ekspor') :
	?>
	<div class="row invoice-info print-foot">
		<div class="col-xs-3" style="text-align: center;">
			<div style="margin-bottom: 100px;">Approved by,</div>
			<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
		</div>
		<div class="col-xs-3"></div>
		<div class="col-xs-3"></div>
		<div class="col-xs-3" style="text-align: center;">
			<div style="margin-bottom: 100px;">Prepared by,</div>
			<hr style="margin-bottom: 5px;border-top: 2px solid #adadad;">
		</div>
	</div>
	<?php
endif;
?>
<hr>
	<table class="div-footer" style="width: 50%; " border="0">
		<tbody>
			<tr>
				<td>
					<img src="<?php echo base_url(); ?>assets/admin_assets/dist/img/LogoISO2021.jpg" alt="" >
				</td>
			</tr>
		</tbody>
	</table>