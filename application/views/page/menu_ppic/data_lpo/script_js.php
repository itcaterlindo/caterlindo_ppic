<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	lihat_data();
	first_load('idBoxTableLoader', 'idBoxTableContent');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function get_form(kd_mso) {
		$('#idAlertDetail').html('');
		$('#idFormBoxDataProductionOrder').remove();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/get_form'; ?>',
			type: 'GET',
			data: 'kd_mso='+kd_mso,
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function lihat_data() {
		$('#idBoxTableContent').hide();
		$('#idBoxTableContent').html('');
		$('#idBoxTableLoader').show();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/open_table'; ?>',
			success: function(table) {
				$('#idBoxTableContent').html(table);
				moveTo('idMainContent');
			}
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function detail_data(kode) {
		$('#idBoxDetail').remove();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/open_detail'; ?>',
			type: 'GET',
			data: 'kode='+kode,
			success: function(html) {
				$('#idMainContent').prepend(html);
				first_load('idBoxDetailLoader', 'idBoxDetailContent');
				moveTo('idMainContent');
			}
		});
	}

	function ubah_status(kode, status, no_so) {
		$('#idBoxTableOverlay').show();
		$.ajax({
			url: '<?php echo base_url().'sales/sales_orders/sales_orders_tbl/ubah_status'; ?>',
			type: 'GET',
			data: 'id='+kode+'&status='+status+'&no_so='+no_so,
			success: function(notif) {
				$('#idBoxDetail').remove();
				$('#idBoxTableOverlay').fadeOut(function(){
					$('#idAlertTable').html(notif.alert);
					lihat_data();
					first_load('idBoxTableLoader', 'idBoxTableContent');
				});
			}
		});
	}

	function cancel_so(kode, status, no_so) {
		$('#idBoxTableOverlay').show();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/ubah_status'; ?>',
			type: 'GET',
			data: 'kode='+kode+'&status='+status+'&no_so='+no_so,
			success: function(notif) {
				$('#idBoxDetail').remove();
				$('#idBoxTableOverlay').fadeOut(function(){
					$('#idAlertTable').html(notif);
					lihat_data();
					first_load('idBoxTableLoader', 'idBoxTableContent');
				});
			}
		});
	}
</script>