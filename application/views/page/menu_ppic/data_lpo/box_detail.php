<?php
defined('BASEPATH') or exit('No direct script access allowed!');
extract($master);
?>

<div class="box box-success" id="idBoxDetail">
	<div class="box-header with-border">
		<h3 class="box-title">Detail Local Production Order</h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool btn-collapse" id="idBtnBoxDetailSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
				<i class="fa fa-minus"></i>
			</button>
			<button class="btn btn-box-tool btn-remove" id="idBtnBoxDetailClose" data-widget="remove" data-toggle="tooltip" title="Close">
				<i class="fa fa-times"></i>
			</button>
		</div>
	</div>
	<div class="box-body">
		<div id="idBoxDetailLoader" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="idAlertDetail"></div>
		<div id="idBoxDetailContent" style="display: none;">
			<div class="row">
				<div class="col-xs-12">
					<a href="<?php echo base_url().$class_link.'/cetak/'.$kd_msalesorder; ?>" class="btn btn-primary no-print" target="_blank">
						<i class="fa fa-print"></i> Cetak Data
					</a>
					<button id="btnFormTglKirim" class="btn btn-default no-print" onclick="get_form('<?php echo $kd_msalesorder; ?>')">
						<i class="fa fa-table"></i> Input Tgl Kirim
					</button>
				</div>
			</div>
			<hr>
			<?php $this->load->view('page/'.$class_link.'/detail'); ?>
			<?php
			if ($status_so == 'process_lpo') :
				?>
				<!-- <a href="javascript:void(0);" class="btn btn-danger btn-flat pull-right" onclick="return confirm('EPO/LPO Nomor <?php echo $no_salesorder; ?> akan diproses Work Orders?')?ubah_status('<?php echo $kd_msalesorder; ?>', 'process_wo', '<?php echo $no_salesorder; ?>'):false">
					<i class="fa fa-bullhorn"></i> Process Work Orders
				</a> -->
				<?php
			endif;
			?>
		</div>
	</div>
	<div class="box-footer">
	</div>
	<div class="overlay" id="idBoxDetailOverlay" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
</div>