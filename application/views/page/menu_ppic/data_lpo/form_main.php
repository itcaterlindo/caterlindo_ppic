<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataProductionOrder';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>
<div class="form-group">
	<label for="idTxtTglKirim" class="col-md-2 control-label">Tgl Kirim</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrKdMso"></div>
		<div id="idErrTglKirim"></div>
		<?php echo form_input(array('type' => 'hidden', 'name' => 'txtKdMso', 'value' => $master->kd_msalesorder)); ?>
		<?php echo form_input(array('name' => 'txtTglKirim', 'id' => 'idTxtTglKirim', 'class' => 'form-control datepicker', 'value' => format_date($master->tgl_kirim, 'Y-m-d'), 'placeholder' => 'Tgl Kirim')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtTglKirim" class="col-md-2 control-label">Tgl Selesai</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrTglSelesai"></div>
		<?php echo form_input(array('name' => 'txtTglSelesai', 'id' => 'idTxtTglSelesai', 'class' => 'form-control datepicker', 'value' => format_date($master->tgl_selesai, 'Y-m-d'), 'placeholder' => 'Tgl Selesai')); ?>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" class="btn btn-primary btn-flat">
			<i class="fa fa-shopping-cart"></i> Submit
		</button>
	</div>
</div>
<?php echo form_close(); ?>