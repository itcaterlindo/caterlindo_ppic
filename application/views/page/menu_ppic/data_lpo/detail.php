<?php
defined('BASEPATH') or exit('No direct script access allowed!');
extract($master);
extract($format_laporan);
$color = color_status($status_so);
$word = process_status($status_so);
$tipe_cust = strtolower($tipe_customer);

if ($tipe_cust == 'lokal') :
	?>
	<div class="row invoice-info">
		<div class="col-sm-4 invoice-col">
			<b><?php echo trans_tipe($tipe_customer); ?> Production Order No./Date :</b> #<?php echo $no_salesorder.'/'.$tgl_so; ?>
		</div>
		<div class="col-sm-4 invoice-col">
			<b>Sales Order No./Date :</b> #<?php echo $no_salesorder.'/'.$tgl_so; ?><br>
			<div class="no-print">
				<b>Sales Order Status :</b> <?php echo bg_label($word, $color); ?><br>
			</div>
		</div>
		<div class="col-sm-4 invoice-col">
			<b>Tanggal Kirim :</b> #<?php echo $tgl_kirim; ?>
		</div>
	</div>
	<hr style="margin-top: 5px;margin-bottom: 5px;">
	<?php
endif;
?>
<div class="row">
	<?php
	if ($tipe_cust == 'lokal') :
		?>
		<div class="col-sm-4 invoice-col">
			Sales
			<address>
				<strong><?php echo $nm_salesperson; ?></strong><br>
				PT Caterlindo<br>
				Jln Industri No.18, Trosobo<br>
				Taman, Sidoarjo Regency, East Java 61262<br>
				Phone : <?php echo $telp_sales; ?><br>
				Email : <?php echo $email_sales; ?>
			</address>
		</div>
		<div class="col-sm-4 invoice-col">
			Customer
			<address>
				<strong><?php echo $nm_customer; ?></strong><br>
				<?php echo $contact_customer; ?><br>
				<?php echo $alamat_satu_customer; ?><br>
				<?php echo $alamat_dua_customer; ?><br>
				Phone : <?php echo $telp_customer; ?><br>
				Email : <?php echo $email_customer; ?>
			</address>
		</div>
		<div class="col-sm-4 invoice-col">
			Deliver To
			<address>
				<strong><?php echo $nm_kirim; ?></strong><br>
				<?php echo $contact_kirim; ?><br>
				<?php echo $alamat_satu_kirim; ?><br>
				<?php echo $alamat_dua_kirim; ?><br>
				Phone : <?php echo $telp_kirim; ?><br>
				Email : <?php echo $email_kirim; ?>
			</address>
		</div>
		<?php
	elseif ($tipe_cust == 'ekspor') :
		?>
		<div style="font-size: 12px;">
			<div class="row">
				<div class="col-xs-6">
					<div class="col-sm-2 col-xs-5"><strong>PO Number</strong></div>
					<div class="col-xs-7"> : <?php echo $no_po; ?></div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div class="col-sm-2 col-xs-5"><strong>Date</strong></div>
					<div class="col-xs-7"> : <?php echo format_date($tgl_so, 'd-M-Y'); ?></div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div class="col-sm-2 col-xs-5"><strong>Stuffing Plan Date</strong></div>
					<div class="col-xs-7"> : <?php echo $tgl_stuffing != '0000-00-00'?format_date($tgl_stuffing, 'd/m/Y'):'-'; ?></div>
				</div>
				<div class="col-xs-6">
					<div class="col-sm-2 col-xs-5"><strong>PPIC Sign</strong></div>
					<div class="col-xs-7"> : </div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div class="col-sm-2 col-xs-5"><strong>Shipping Date</strong></div>
					<div class="col-xs-7"> : <?php echo $tgl_kirim != '0000-00-00'?format_date($tgl_kirim, 'd/m/Y'):'-'; ?></div>
				</div>
				<div class="col-xs-6">
					<div class="col-sm-2 col-xs-5"><strong>Container Required</strong></div>
					<div class="col-xs-7"> : <?php echo $tipe_container; ?></div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div class="col-sm-2 col-xs-5"><strong>Customer</strong></div>
					<div class="col-xs-7"> : <?php echo $nm_customer; ?></div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div class="col-sm-2 col-xs-5"><strong>Customer Contact</strong></div>
					<div class="col-xs-7"> : <?php echo $nm_kirim; ?></div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div class="col-sm-2 col-xs-5"></div>
					<div class="col-xs-7"><?php echo $alamat_satu_customer; ?></div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div class="col-sm-2 col-xs-5"></div>
					<div class="col-xs-7"><?php echo $alamat_dua_customer; ?></div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div class="col-sm-2 col-xs-5"><strong>Partner Comments</strong></div>
					<div class="col-xs-7"> : <?php echo $note_so; ?></div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div class="col-sm-2 col-xs-5"><strong>Exim / Sales Note</strong></div>
					<div class="col-xs-7"> : </div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div class="col-sm-2 col-xs-5"><strong>Order Details</strong></div>
					<div class="col-xs-7"> : </div>
				</div>
			</div>
		</div>
		<?php
	endif;
	?>
</div>
<hr>
<table style="column-width: 10px;width: 100%;" border="1">
	<thead>
		<tr>
			<th style="width: 1%;text-align: center;">No.</th>
			<?php
			if ($tipe_cust == 'lokal') :
				?>
				<th style="width: 180px;text-align: center;">Prod Code</th>
				<th style="width: 60px;text-align: center;">Status</th>
				<th style="width: 380px;text-align: center;">Description</th>
				<th style="width: 100px;text-align: center;">Dimension</th>
				<th style="width: 280px;text-align: center;">Note</th>
				<?php
			elseif ($tipe_cust == 'ekspor') :
				?>
				<th style="width: 30%;text-align: center;" colspan="2">Description</th>
				<th style="width: 20%;text-align: center;">Size</th>
				<th style="width: 10%;text-align: center;">Tariff</th>
				<?php
			endif;
			?>
			<th style="width: 5%;text-align: center;">Qty</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		$tot_qty = array();
		foreach ($master_item as $item_data) :
			$no_child = 0;
			$rowspan = 0;
			foreach ($detail_item as $detail_data) :
				if ($detail_data['ditem_so_kd'] == $item_data->kd_ditem_so) :
					$no_child++;
				endif;
				$rowspan = 1 + $no_child;
			endforeach;
			$rowspans = empty($rowspan)?'':'rowspan="'.$rowspan.'"';
			$no++;
			$tot_qty[] = $item_data->item_qty;
			?>
			<tr>
				<td <?php echo $rowspans; ?> style="vertical-align: middle;"><?php echo $no; ?></td>
				<?php
				if ($tipe_cust == 'lokal') :
					?>
					<td><?php echo $item_data->item_code; ?></td>
					<td><?php echo item_stat($item_data->item_status); ?></td>
					<td><?php echo $item_data->item_desc; ?></td>
					<td><?php echo $item_data->item_dimension; ?></td>
					<td><?php echo $item_data->item_note; ?></td>
					<?php
				elseif ($tipe_cust == 'ekspor') :
					?>
					<td><?php echo $item_data->item_code; ?></td>
					<td><?php echo $item_data->item_desc; ?></td>
					<td><?php echo $item_data->item_dimension; ?></td>
					<td><?php echo $item_data->hs_code; ?></td>
					<?php
				endif;
				?>
				<td style="text-align: center;"><?php echo $item_data->item_qty; ?></td>
			</tr>
			<?php
			foreach ($detail_item as $detail_data) :
				if ($detail_data['ditem_so_kd'] == $item_data->kd_ditem_so) :
					$tot_qty[] = $detail_data['item_qty'];
					?>
					<tr>
						<?php
						if ($tipe_cust == 'lokal') :
							?>
							<td><?php echo $detail_data['item_code']; ?></td>
							<td><?php echo item_stat($detail_data['item_status']); ?></td>
							<td><?php echo $detail_data['item_desc']; ?></td>
							<td><?php echo $detail_data['item_dimension']; ?></td>
							<td><?php echo $detail_data['item_note']; ?></td>
							<?php
						elseif ($tipe_cust == 'ekspor') :
							?>
							<td><?php echo $detail_data['item_code']; ?></td>
							<td><?php echo $detail_data['item_desc']; ?></td>
							<td><?php echo $detail_data['item_dimension']; ?></td>
							<td><?php echo $detail_data['hs_code']; ?></td>
							<?php
						endif;
						?>
						<td style="text-align: center;"><?php echo $detail_data['item_qty']; ?></td>
					</tr>
					<?php
				endif;
			endforeach;
		endforeach;
		$total_qty = array_sum($tot_qty);

		if ($tipe_cust == 'lokal') :
			?>
			<tr>
				<td colspan="5"></td>
				<td style="text-align: left;"><b>Total Unit</b></td>
				<td style="text-align: center;"><?php echo $total_qty; ?></td>
			</tr>
			<tr>
				<td colspan="7">
					<?php
					if ($page_type == 'print') :
						if (empty($term_payment)) :
							echo $laporan_footer;
						else :
							echo $term_payment.'<br />'.$footer;
						endif;
					endif;
					?>
				</td>
			</tr>
			<?php
		elseif ($tipe_cust == 'ekspor') :
			?>
			<tr>
				<td></td>
				<td></td>
				<td style="text-align: left;"><b>Grand Total</b></td>
				<td></td>
				<td></td>
				<td style="text-align: center;"><?php echo $total_qty; ?></td>
			</tr>
			<tr>
				<td colspan="7">
					<?php
					if ($page_type == 'print') :
						if (empty($term_payment)) :
							echo $laporan_footer;
						else :
							echo $term_payment.'<br />'.$footer;
						endif;
					endif;
					?>
				</td>
			</tr>
			<?php
		endif;
		?>
	</tbody>
</table>
<hr>