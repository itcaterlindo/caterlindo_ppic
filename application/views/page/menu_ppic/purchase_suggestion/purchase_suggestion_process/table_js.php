<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	table_main();

	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		formmaster_main('');
	});

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function table_main() {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/table_main'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').slideDown().html(html);
					moveTo('idMainContent');
				}
			});
		});
	}

	function edit_data(purchasesuggestion_kd) {
		formmaster_main(purchasesuggestion_kd);
	}

	function view_data(purchasesuggestion_kd) {
		window.location.assign('<?php echo base_url().$class_link.'/summarymaterial_box?purchasesuggestion_kd='; ?>'+purchasesuggestion_kd);
	}

	function hapus_data(purchasesuggestion_kd) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/action_delete_suggest'; ?>',
				data: {
					purchasesuggestion_kd: purchasesuggestion_kd
				},
				success: function(resp) {
					if (resp.code == 200) {
						notify(resp.status, resp.pesan, 'success');
						table_main();
					} else {
						notify(resp.status, resp.pesan, 'error');
					}
				}
			});
		}
	}
	
	function formmaster_main(purchasesuggestion_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/formmaster_main'; ?>',
			data: {
				purchasesuggestion_kd: purchasesuggestion_kd
			},
			success: function(html) {
				toggle_modal('Form Master', html);
				render_select2('.select2');
			}
		});
	}

	function toggle_modal(modalTitle, htmlContent) {
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function render_select2(element) {
		$(element).select2({
			theme: 'bootstrap',
			width: '100%',
			placeholder: '-- Pilih Opsi --',
		});
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}
	
	function submitDataMaster(form_id) {
		event.preventDefault();
		var form = document.getElementById(form_id);
		var url = '<?php echo base_url() . $class_link . '/action_insert_master'; ?>';

		$.ajax({
			url: url,
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(resp) {
				console.log(resp);
				if (resp.code == 200) {
					var wo_kd = $('#idtxtwo_kd').val();
					window.location.assign('<?php echo base_url().$class_link?>/formso_box?purchasesuggestion_kd='+resp.data.purchasesuggestion_kd);
					notify(resp.status, resp.pesan, 'success');
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
			}
		});
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			delay: 2500,
			styling: 'bootstrap3'
		});
	}
</script>