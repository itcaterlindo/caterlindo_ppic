<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$groupByTglSuggest = array();
foreach ($result as $r){
    $groupByTglSuggest[$r['purchasesuggestionmaterial_suggestpr_tanggal']][] = $r;
}
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}

	td.dt-blue {
		background-color: blue;
		color: white;
	}

	td.dt-green {
		background-color: green;
		color: white;
	}

	td.dt-red {
		background-color: red;
		color: white;
	}
</style>

<table id="idTable" border="1" style="width:100%;">
	<thead>
		<tr>
			<th style="width:1%; text-align:center;">No.</th>
			<th style="width:7%; text-align:center;">tgl Suggest PR</th>
			<th style="width:7%; text-align:center;">RM Kode</th>
			<th style="width:7%; text-align:center;">Deskripsi</th>
			<th style="width:7%; text-align:center;">Qty</th>
			<th style="width:7%; text-align:center;">Satuan</th>
			<th style="width:7%; text-align:center;">Leadtime Supplier (hari)</th>
		</tr>
	</thead>
	<tbody>
        <?php 
        $no=1;
        foreach($groupByTglSuggest as $eTgl => $elements):?>
        <tr style="font-weight: bold; background-color: gray;">
            <td></td>
            <td><?php echo $eTgl; ?></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
            <?php foreach ($elements as $el) :?>
                <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $eTgl; ?></td>
                    <td><?php echo $el['rm_kode']; ?></td>
                    <td><?php echo "{$el['purchasesuggestionmaterial_rm_deskripsi']} {$el['purchasesuggestionmaterial_rm_spesifikasi']}"; ?></td>
                    <td style="text-align: right;" ><?php echo $el['sum_purchasesuggestionmaterial_qty']; ?></td>
                    <td><?php echo $el['rmsatuan_nama']; ?></td>
                    <td style="text-align: right;"><?php echo $el['purchasesuggestionmaterial_leadtimesupplier_hari']; ?></td>
                </tr>
            <?php 
            $no++;
            endforeach; ?>
        <?php endforeach; ?>
	</tbody>
</table>