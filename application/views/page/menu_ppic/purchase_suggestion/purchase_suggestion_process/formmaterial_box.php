<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$js_file = 'formmaterial_js';
$box_title = 'Purchase Suggestion Process - Material';
$data['class_link'] = $class_link;
$box_type = 'Table';
$data['master_var'] = 'WorkOrder';
$data['box_id'] = 'id' . $box_type . 'Box' . $data['master_var'];
$data['box_content_id'] = 'id' . $box_type . 'BoxContent' . $data['master_var'];
$data['box_loader_id'] = 'id' . $box_type . 'BoxLoader' . $data['master_var'];
$data['btn_add_id'] = 'id' . $box_type . 'BtnBoxAdd' . $data['master_var'];

$data['purchasesuggestion_kd'] = $purchasesuggestion_kd;
$data['modal_title'] = $data['master_var'] . 'modalTitle';
$data['content_modal_id'] = $data['master_var'] . 'content_modal_id';
?>

<style type="text/css">
	.modal-dialog {
		width: 1100px;
	}
</style>

<div class="col-md-12">
	<div class="box box-primary" id="<?php echo $data['box_id']; ?>">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $box_title; ?></h3>
			<div class="box-tools pull-right">
				<button class="btn btn-box-tool btn-collapse" id="idBtnBoxTableSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
					<i class="fa fa-minus"></i>
				</button>
			</div>
		</div>
		<div class="box-body">
			<button class="btn btn-sm btn-danger" onclick="action_generate_materialso('<?php echo $purchasesuggestion_kd; ?>')"> <i class="fa fa-check"></i> 1. Generate Material SO</button>
			<button class="btn btn-sm btn-danger" onclick="action_generate_material_leadtimesupplier('<?php echo $purchasesuggestion_kd; ?>')"> <i class="fa fa-check"></i> 2. Generate LeadTime Supplier</button>
			<button class="btn btn-sm btn-danger" onclick="action_generate_material_suggestpr('<?php echo $purchasesuggestion_kd; ?>')"> <i class="fa fa-check"></i> 3. Generate SuggestPR Tanggal</button>

			<div class="btn-group">
				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-search"></i> View Material <span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0);" onclick="tablematerialbyso_main('<?php echo $purchasesuggestion_kd; ?>')" >View Group By SO Material</a></li>
					<li><a href="javascript:void(0);" onclick="tablematerialbytglsuggest_main('<?php echo $purchasesuggestion_kd; ?>')" >View Group By Tgl Suggest</a></li>
				</ul>
			</div>

			<button class="btn btn-sm btn-primary pull-right" onclick="window.location.assign('<?php echo base_url() . $class_link . '/summarymaterial_box?purchasesuggestion_kd=' . $purchasesuggestion_kd; ?>')"> Next <i class="fa fa-arrow-right"></i> </button>
			<hr>
			<div class="row invoice-info">
				<div class="col-sm-6 invoice-col">
					<b>PurchaseSuggest Kode :</b> <?php echo isset($row['purchasesuggestion_kode']) ? $row['purchasesuggestion_kode'] : '-'; ?><br>
					<b>PurchaseSuggest Status :</b> <?php echo isset($row['purchasesuggestion_state']) ? strtoupper($row['purchasesuggestion_state']) : '-'; ?> <br>
				</div>
				<div class="col-sm-6 invoice-col">
					<b>Note :</b> <?php echo isset($row['purchasesuggestion_note']) ? $row['purchasesuggestion_note'] : '-'; ?><br>
				</div>
			</div>

			<hr>
			<div id="<?php echo $data['box_content_id']; ?>"></div>
		</div>
		<div class="box-footer">
		</div>
		<div class="overlay" id="idBoxTableOverlay" style="display: none;">
			<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
		</div>
	</div>
</div>

<?php $this->load->view('page/' . $class_link . '/' . $js_file, $data); ?>

<!-- modal -->
<div class="modal fade" id="idmodal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><?php echo $data['modal_title']; ?></h4>
			</div>
			<div class="modal-body">

				<div class="row">
					<div class="col-md-12">
						<div id="<?php echo $data['content_modal_id']; ?>"></div>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Keluar</button>
			</div>
		</div>
	</div>
</div>