<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}

	td.dt-red{
		background-color: red;
	}
	
</style>

<table id="idTable" class="table table-bordered table-striped table-hover display responsive" style="width:100%; font-size: 90%;">
	<thead>
		<tr>
			<th style="width:1%; text-align:center;">No.</th>
			<th style="width:1%; text-align:center;">Opsi</th>
			<th style="width:7%; text-align:center;">No Salesorder</th>
			<th style="width:7%; text-align:center;">Nama Barang</th>
			<th style="width:7%; text-align:center;">Deskripsi</th>
			<th style="width:7%; text-align:center;">Qty</th>
			<th style="width:7%; text-align:center;">BoM</th>
			<th style="width:7%; text-align:center;">Tgl Edit</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>

<script type="text/javascript">
	$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};
	$.ajax({
		type: "GET",
		url: "<?php echo base_url() . $class_link . '/tablebarang_data?purchasesuggestion_kd='.$purchasesuggestion_kd; ?>",
		success: function (response) {
			var table = $('#idTable').DataTable({
				"processing": true,
				"data": response,
				"ordering": true,
				"paging" : false,
				"scrollY": '400px',
				"scrollX": '300px',
				"language": {
					"lengthMenu": "Tampilkan _MENU_ data",
					"zeroRecords": "Maaf tidak ada data yang ditampilkan",
					"info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
					"infoFiltered": "",
					"infoEmpty": "Tidak ada data yang ditampilkan",
					"search": "Cari :",
					"loadingRecords": "Memuat Data...",
					"processing": "Sedang Memproses...",
					"paginate": {
						"first": '<span class="glyphicon glyphicon-fast-backward"></span>',
						"last": '<span class="glyphicon glyphicon-fast-forward"></span>',
						"next": '<span class="glyphicon glyphicon-forward"></span>',
						"previous": '<span class="glyphicon glyphicon-backward"></span>'
					}
				},
				"columns" : [
					{
						"data": "id",
						render: function (data, type, row, meta) {
							return meta.row + meta.settings._iDisplayStart + 1;
						}
					},
					{ "data" : "purchasesuggestionsobarang_kd",
						render: function (data, type, row, meta) {
							return `<div align='center'><div class='btn-group'><button type='button' class='btn btn-info btn-flat dropdown-toggle' data-toggle='dropdown'>Opsi <span class='caret'></span></button><ul class='dropdown-menu'><li><a href='javascript:void(0);' title="Edit Item" onclick="edit_data(`+data+`)"><i class='fa fa-pencil'></i> Edit Item</a></li><li><a href='javascript:void(0);' title="Delete Item" onclick="delete_data(`+data+`)"><i class='fa fa-trash'></i> Delete Item</a></li></ul></div></div>`;
						}
					},
					{ "data" : "purchasesuggestionsobarang_kd",
						render: function (data, type, row, meta) {
							return "";
						}
					},
					{ "data" : "item_code" },
					{ "data" : "purchasesuggestionsobarang_deskripsi" },
					{ "data" : "purchasesuggestionsobarang_qty" },
					{ "data" : "itemcode_bom" },
					{ "data" : "purchasesuggestionsobarang_tgledit" }
				]
			});
		}
	});
	
</script>