<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputMaster';

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtpurchasesuggestionsobarang_kd', 'name' => 'txtpurchasesuggestionsobarang_kd', 'placeholder' => 'idtxtpurchasesuggestionsobarang_kd', 'value' => isset($purchasesuggestionsobarang_kd) ? $purchasesuggestionsobarang_kd : null));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtaction', 'name' => 'txtaction', 'placeholder' => 'idtxtpurchasesuggestionsobarang_kd', 'value' => $action));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtpurchasesuggestion_kd', 'name' => 'txtpurchasesuggestion_kd', 'placeholder' => 'idtxtpurchasesuggestion_kd', 'value' => $purchasesuggestion_kd));

?>
<div class="row">
    <div class="form-group">
        <label for="" class="col-md-2 control-label">Nama Barang</label>
        <div class="col-sm-8 col-xs-12">
            <?php if($action == 'edit'){ ?>
                <input type="text" class="form-control" readonly="readonly" value="<?php echo $row['item_code']; ?>">
            <?php }else{ ?>
                <div class="errInput" id="idErrItemCode"></div>
                <select name="txtitemcode" class="form-control" id="idtxtitemcode">
                </select>
            <?php } ?>
        </div>
    </div>
    <div class="form-group">
        <label for="" class="col-md-2 control-label">Deskripsi</label>
        <div class="col-sm-8 col-xs-12">
            <textarea id="idtxtdeskripsi" name="txtdeskripsi" cols="30" class="form-control" rows="2" <?= $action == 'edit' ? 'readonly' : '' ?>><?php echo "{$row['purchasesuggestionsobarang_deskripsi']} {$row['purchasesuggestionsobarang_dimensi']}"; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="" class="col-md-2 control-label">Qty</label>
        <div class="errInput" id="idErrQty"></div>
        <div class="col-sm-2 col-xs-8">
            <input type="number" class="form-control" name="txtpurchasesuggestionsobarang_qty" id="idtxtpurchasesuggestionsobarang_qty" value="<?php echo isset($row['purchasesuggestionsobarang_qty']) ? $row['purchasesuggestionsobarang_qty'] : 0; ?>">
        </div>
    </div>
    <hr>

    <div class="form-group">
        <label for='idtxtbom_kd' class="col-md-2 control-label">BoM</label>
        <div class="col-sm-8 col-xs-12">
            <div class="errInput" id="idErrbom_kd"></div>
            <select name="txtbom_kd" class="form-control" id="idtxtbom_kd">
                <?php if (isset($row['bom_kd']) && !empty($row['bom_kd'])) : ?>
                    <option value="<?php echo $row['bom_kd']; ?>" selected><?php echo $row['itemcode_bom']; ?></option>
                <?php endif; ?>
            </select>
        </div>
    </div>
</div>
<hr>

<div class="col-md-12">
    <div class="form-group">
        <div class="col-sm-1 col-xs-12 col-md-offset-2">
            <button type="submit" name="btnSubmit" onclick="submitDataSObarang('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
                <i class="fa fa-save"></i> Simpan
            </button>
        </div>
    </div>
</div>
<?php echo form_close(); ?>