<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
    tablematerial_main('<?php echo $purchasesuggestion_kd; ?>');

    first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

    function moveTo(div_id) {
        $('html, body').animate({
            scrollTop: $('#' + div_id).offset().top - $('header').height()
        }, 1000);
    }

    function first_load(loader, content) {
        $('#' + loader).fadeOut(500, function(e) {
            $('#' + content).slideDown();
        });
    }

    function tablematerial_main(purchasesuggestion_kd) {
        $('#<?php echo $btn_add_id; ?>').slideDown();
        $('#<?php echo $box_content_id; ?>').slideUp(function() {
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url() . $class_link . '/tablematerial_main'; ?>',
                data: {
                    purchasesuggestion_kd: purchasesuggestion_kd
                },
                success: function(html) {
                    $('#<?php echo $box_content_id; ?>').slideDown().html(html);
                    moveTo('idMainContent');
                }
            });
        });
    }

    function tablematerialbyso_main(purchasesuggestion_kd) {
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url() . $class_link . '/tablematerialbyso_main'; ?>',
            data: {
                purchasesuggestion_kd: purchasesuggestion_kd
            },
            success: function(html) {
                toggle_modal('Group By SO', html)
            }
        });
    }

    function tablematerialbytglsuggest_main(purchasesuggestion_kd) {
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url() . $class_link . '/tablematerialbytglsuggest_main'; ?>',
            data: {
                purchasesuggestion_kd: purchasesuggestion_kd
            },
            success: function(html) {
                toggle_modal('Group By Tgl Suggest', html)
            }
        });
    }

    function edit_data(purchasesuggestionmaterial_kd) {
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url() . $class_link . '/formmaterial_main'; ?>',
            data: {
                purchasesuggestionmaterial_kd: purchasesuggestionmaterial_kd
            },
            success: function(html) {
                toggle_modal('Form SO material', html);
                render_datetimepicker('datetime'); 
            }
        });
    }

    function render_select2(element) {
        $(element).select2({
            theme: 'bootstrap',
            width: '100%',
            placeholder: '-- Pilih Opsi --',
        });
    }

    function render_datetimepicker(valClass) {
        $('.' + valClass).datetimepicker({
            format: 'YYYY-MM-DD',
        });
    }

    function generateToken(csrf) {
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
    }

    function submitDataSOmaterial(form_id) {
        event.preventDefault();
        var form = document.getElementById(form_id);
        var url = '<?php echo base_url() . $class_link . '/action_insert_prsuggestsomaterial'; ?>';

        $.ajax({
            url: url,
            type: "POST",
            data: new FormData(form),
            contentType: false,
            cache: false,
            processData: false,
            success: function(resp) {
                console.log(resp);
                if (resp.code == 200) {
                    var wo_kd = $('#idtxtwo_kd').val();
                    reload_materialtable();
                    toggle_modal('', '');
                    notify(resp.status, resp.pesan, 'success');
                } else if (resp.code == 401) {
                    $.each(resp.pesan, function(key, value) {
                        $('#' + key).html(value);
                    });
                    generateToken(resp.csrf);
                } else if (resp.code == 400) {
                    notify(resp.status, resp.pesan, 'error');
                    generateToken(resp.csrf);
                } else {
                    notify('Error', 'Error tidak Diketahui', 'error');
                    generateToken(resp.csrf);
                }
            }
        });
    }

    function action_generate_materialso(purchasesuggestion_kd) {
        var conf = confirm('Apakah anda yakin ?');
        if (conf) {
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url() . $class_link . '/action_generate_material'; ?>',
                data: {
                    purchasesuggestion_kd: purchasesuggestion_kd
                },
                success: function(resp) {
                    if (resp.code == 200) {
                        notify(resp.status, resp.pesan, 'success');
                        tablematerial_main('<?php echo $purchasesuggestion_kd; ?>');
                    } else {
                        notify(resp.status, resp.pesan, 'error');
                    }
                }
            });
        }
    }

    function action_generate_material_leadtimesupplier(purchasesuggestion_kd) {
        var conf = confirm('Apakah anda yakin ?');
        if (conf) {
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url() . $class_link . '/action_generate_material_leadtimesupplier'; ?>',
                data: {
                    purchasesuggestion_kd: purchasesuggestion_kd
                },
                success: function(resp) {
                    if (resp.code == 200) {
                        notify(resp.status, resp.pesan, 'success');
                        tablematerial_main('<?php echo $purchasesuggestion_kd; ?>');
                    } else {
                        notify(resp.status, resp.pesan, 'error');
                    }
                }
            });
        }
    }

    function action_generate_material_suggestpr (purchasesuggestion_kd) {
        var conf = confirm('Apakah anda yakin ?');
        if (conf) {
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url() . $class_link . '/action_generate_material_suggestpr'; ?>',
                data: {
                    purchasesuggestion_kd: purchasesuggestion_kd
                },
                success: function(resp) {
                    console.log(resp);
                    if (resp.code == 200) {
                        notify(resp.status, resp.pesan, 'success');
                        tablematerial_main('<?php echo $purchasesuggestion_kd; ?>');
                    } else {
                        notify(resp.status, resp.pesan, 'error');
                    }
                }
            });
        }
    }

    function toggle_modal(modalTitle, htmlContent) {
        $('#idmodal').modal('toggle');
        $('.modal-title').text(modalTitle);
        $('#<?php echo $content_modal_id; ?>').slideUp();
        $('#<?php echo $content_modal_id; ?>').html(htmlContent);
        $('#<?php echo $content_modal_id; ?>').slideDown();
    }

    function notify(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            type: type,
            delay: 2500,
            styling: 'bootstrap3'
        });
    }
</script>