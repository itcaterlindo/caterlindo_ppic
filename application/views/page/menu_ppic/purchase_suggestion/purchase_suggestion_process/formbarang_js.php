<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	tablebarang_main('<?php echo $purchasesuggestion_kd; ?>');

	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('form_add_master', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function tablebarang_main(purchasesuggestion_kd){
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/tablebarang_main'; ?>',
                data: {
                    purchasesuggestion_kd:purchasesuggestion_kd
                },
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').slideDown().html(html);
					moveTo('idMainContent');
				}
			});
		});
	}

	function edit_data(purchasesuggestionsobarang_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/formbarang_main'; ?>',
			data: {
				purchasesuggestionsobarang_kd:purchasesuggestionsobarang_kd, 
				action: 'edit'
			},
			success: function(html) {
				toggle_modal('Form SO Barang', html);
				render_bom_std();
			}
		});
	}

	function delete_data(purchasesuggestionsobarang_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/action_delete_barang'; ?>',
			data: {
				purchasesuggestionsobarang_kd:purchasesuggestionsobarang_kd, 
				action: 'edit'
			},
			success: function(resp) {
				if (resp.code == 200){
					notify(resp.status, resp.pesan, 'success');
					tablebarang_main('<?php echo $purchasesuggestion_kd; ?>');
				}else{
					notify(resp.status, resp.pesan, 'error');
				}
			}
		});
	}

	function add_data(purchasesuggestionsobarang_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/formbarang_main'; ?>',
			data: {
				action: 'add',
				purchasesuggestion_kd: '<?= $purchasesuggestion_kd ?>'
			},
			success: function(html) {
				toggle_modal('Form SO Barang', html);
				render_barang();
				render_bom_std();
			}
		});
	}

	function render_bom_std() {
		$('#idtxtbom_kd').select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 1,
			ajax: { 
				url: '<?php echo base_url() ?>/Auto_complete/get_bom_std',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramBOM: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_barang() {
		$('#idtxtitemcode').select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 1,
			ajax: { 
				url: '<?php echo base_url() ?>/Auto_complete/get_barang_select2',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramItemCode: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		}).on('change', function (e) {
			$.ajax({
				type: 'GET',
				dataType: 'json',
				url: '<?php echo base_url() . '/Auto_complete/get_barang_byid'; ?>',
				data: {
					kd_barang: $(this).val()
				},
				success: function(resp) {
					$('#idtxtdeskripsi').val(resp.deskripsi_barang);
				}
			});
		});
	}

	function render_select2(element) {
		$(element).select2({
			theme: 'bootstrap',
			width: '100%',
			placeholder: '-- Pilih Opsi --',
		});
	}

	function render_datetimepicker(valClass){
		$('.'+valClass).datetimepicker({
			format: 'YYYY-MM-DD',
    	});
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitDataSObarang(form_id){
		event.preventDefault();
		var form = document.getElementById(form_id);
		var url = '<?php echo base_url() . $class_link . '/action_insert_prsuggestsobarang'; ?>';

		$.ajax({
			url: url,
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(resp) {
				console.log(resp);
				if (resp.code == 200) {
					var wo_kd = $('#idtxtwo_kd').val();
					tablebarang_main('<?php echo $purchasesuggestion_kd; ?>');
					toggle_modal('', '');
					notify(resp.status, resp.pesan, 'success');
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
			}
		});
	}

	function action_generate_itemso(purchasesuggestion_kd) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf){
			$('#idBoxTableOverlay').fadeIn();
			$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/action_generate_itemso'; ?>',
			data: {
				purchasesuggestion_kd:purchasesuggestion_kd
			},
			success: function(resp) {
				if (resp.code == 200){
					notify(resp.status, resp.pesan, 'success');
					tablebarang_main('<?php echo $purchasesuggestion_kd; ?>');
				}else{
					notify(resp.status, resp.pesan, 'error');
				}
				$('#idBoxTableOverlay').fadeOut();
			}
		});
		}
	}

	function action_add_item(purchasesuggestion_kd) {
		add_data();
	}

    function toggle_modal(modalTitle, htmlContent) {
        $('#idmodal').modal('toggle');
        $('.modal-title').text(modalTitle);
        $('#<?php echo $content_modal_id; ?>').slideUp();
        $('#<?php echo $content_modal_id; ?>').html(htmlContent);
        $('#<?php echo $content_modal_id; ?>').slideDown();
    }

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			delay: 2500,
			styling: 'bootstrap3'
		});
	}
</script>