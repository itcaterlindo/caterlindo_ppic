<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputMaster';
if (isset($rowData)) {
    extract($rowData);
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtpurchasesuggestionso_kd', 'name' => 'txtpurchasesuggestionso_kd', 'placeholder' => 'idtxtpurchasesuggestionso_kd', 'value' => isset($purchasesuggestionso_kd) ? $purchasesuggestionso_kd : null));

?>
<div class="row">

    <div class="form-group">
        <label for='idtxtpurchasesuggestionso_leadtime_produksi' class="col-md-3 control-label">Lead Time Produksi (hari)</label>
        <div class="col-sm-4 col-xs-12">
            <div class="errInput" id="idErrpurchasesuggestionso_leadtime_produksi"></div>
            <?php echo form_input(array('type' => 'number', 'class' => 'form-control', 'name' => 'txtpurchasesuggestionso_leadtime_produksi', 'id' => 'idtxtpurchasesuggestionso_leadtime_produksi', 'placeholder' => 'Lead Time Hari', 'value' => isset($row['purchasesuggestionso_leadtime_produksi']) ? $row['purchasesuggestionso_leadtime_produksi'] : null)); ?>
        </div>
    </div>

    <div class="form-group">
        <label for="idtxtpurchasesuggestionso_tglselesai_produksi" class="col-md-3 control-label">Tgl Selesai</label>
        <div class="col-sm-4 col-xs-12">
            <div class="errInput" id="idErrpurchasesuggestionso_tglselesai_produksi"></div>
            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <?php echo form_input(array('type' => 'text', 'class' => 'form-control datetime', 'name' => 'txtpurchasesuggestionso_tglselesai_produksi', 'id' => 'idtxtpurchasesuggestionso_tglselesai_produksi', 'placeholder' => 'Tgl Selesai Prod', 'value' => isset($row['purchasesuggestionso_tglselesai_produksi']) ? $row['purchasesuggestionso_tglselesai_produksi'] : null)); ?>
            </div>
        </div>
    </div>


</div>

<hr>

<div class="col-md-12">
    <div class="form-group">
        <div class="col-sm-1 col-xs-12 col-md-offset-3">
            <button type="submit" name="btnSubmit" onclick="submitDataSO('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
                <i class="fa fa-save"></i> Simpan
            </button>
        </div>
    </div>
</div>
<?php echo form_close(); ?>