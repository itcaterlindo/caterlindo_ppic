<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
    summarymaterial_main('<?php echo $purchasesuggestion_kd; ?>');

    first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

    function moveTo(div_id) {
        $('html, body').animate({
            scrollTop: $('#' + div_id).offset().top - $('header').height()
        }, 1000);
    }

    function first_load(loader, content) {
        $('#' + loader).fadeOut(500, function(e) {
            $('#' + content).slideDown();
        });
    }

    function summarymaterial_main(purchasesuggestion_kd) {
        $('#<?php echo $btn_add_id; ?>').slideDown();
        $('#<?php echo $box_content_id; ?>').slideUp(function() {
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url() . $class_link . '/summarymaterial_main'; ?>',
                data: {
                    purchasesuggestion_kd: purchasesuggestion_kd
                },
                success: function(html) {
                    $('#<?php echo $box_content_id; ?>').slideDown().html(html);
                    moveTo('idMainContent');
                }
            });
        });
    }

    function generateToken(csrf) {
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
    }

    function notify(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            type: type,
            delay: 2500,
            styling: 'bootstrap3'
        });
    }
</script>