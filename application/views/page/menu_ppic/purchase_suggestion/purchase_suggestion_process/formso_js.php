<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	tableso_main('<?php echo $purchasesuggestion_kd; ?>');

	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('form_add_master', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function tableso_main(purchasesuggestion_kd) {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/tableso_main'; ?>',
				data: {
					purchasesuggestion_kd: purchasesuggestion_kd
				},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').slideDown().html(html);
					moveTo('idMainContent');
				}
			});
		});
	}

	function edit_data(purchasesuggestionso_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/formso_main'; ?>',
			data: {
				purchasesuggestionso_kd: purchasesuggestionso_kd
			},
			success: function(html) {
				toggle_modal('Form SO', html);
				render_datetimepicker('datetime');
			}
		});
	}

	function render_select2(element) {
		$(element).select2({
			theme: 'bootstrap',
			width: '100%',
			placeholder: '-- Pilih Opsi --',
		});
	}

	function render_datetimepicker(valClass) {
		$('.' + valClass).datetimepicker({
			format: 'YYYY-MM-DD',
		});
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitDataSO(form_id) {
		event.preventDefault();
		var form = document.getElementById(form_id);
		var url = '<?php echo base_url() . $class_link . '/action_insert_prsuggestso'; ?>';

		$.ajax({
			url: url,
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(resp) {
				console.log(resp);
				if (resp.code == 200) {
					var wo_kd = $('#idtxtwo_kd').val();
					tableso_main('<?php echo $purchasesuggestion_kd; ?>');
					toggle_modal('', '');
					notify(resp.status, resp.pesan, 'success');
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
			}
		});
	}

	function action_generate_startproduksi(purchasesuggestion_kd) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/action_generate_startproduksi'; ?>',
				data: {
					purchasesuggestion_kd: purchasesuggestion_kd
				},
				success: function(resp) {
					if (resp.code == 200) {
						notify(resp.status, resp.pesan, 'success');
						tableso_main('<?php echo $purchasesuggestion_kd; ?>');
					} else {
						notify(resp.status, resp.pesan, 'error');
					}
				}
			});
		}
	}

	function delete_data(purchasesuggestionso_kd) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/action_delete_suggest_so'; ?>',
				data: {
					purchasesuggestionso_kd: purchasesuggestionso_kd
				},
				success: function(resp) {
					if (resp.code == 200) {
						notify(resp.status, resp.pesan, 'success');
						tableso_main('<?php echo $purchasesuggestion_kd; ?>');
					} else {
						notify(resp.status, resp.pesan, 'error');
					}
				}
			});
		}
	}

	function toggle_modal(modalTitle, htmlContent) {
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			delay: 2500,
			styling: 'bootstrap3'
		});
	}
</script>