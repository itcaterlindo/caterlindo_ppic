<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$groupByTglSuggest = array();
foreach ($result as $r){
    $groupByTglSuggest[$r['purchasesuggestionmaterial_suggestpr_tanggal']][] = $r;
}
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}

	td.dt-blue {
		background-color: blue;
		color: white;
	}

	td.dt-green {
		background-color: green;
		color: white;
	}

	td.dt-red {
		background-color: red;
		color: white;
	}
</style>

<table id="idTable" border="1" style="width:100%;">
	<thead>
		<tr>
			<th style="width:1%; text-align:center;">No.</th>
			<th style="width:7%; text-align:center;">Tgl Suggest PR</th>
			<th style="width:7%; text-align:center;">RM Kode</th>
			<th style="width:7%; text-align:center;">Deskripsi</th>
			<th style="width:7%; text-align:center;">Satuan</th>
			<th style="width:7%; text-align:center;">Leadtime Supplier (hari)</th>
			<th style="width:7%; text-align:center;">Stok Awal</th>
			<th style="width:7%; text-align:center;">Min. Stok</th>
			<th style="width:7%; text-align:center;">Outstanding PR</th>
			<th style="width:7%; text-align:center;">Outstanding PO</th>
			<th style="width:7%; text-align:center;">Qty SO</th>
			<th style="width:7%; text-align:center;">Qty Sisa</th>
			<th style="width:7%; text-align:center;">Qty Suggest</th>
		</tr>
	</thead>
	<tbody>
        <?php 
        $no=1;
        foreach($groupByTglSuggest as $eTgl => $elements):?>
        <tr style="font-weight: bold; background-color: gray;">
            <td></td>
            <td><?php echo $eTgl; ?></td>
            <td></td>
			<td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
			<td></td>
            <td></td>
			<td></td>
            <td></td>
			<td></td>
        </tr>
            <?php foreach ($elements as $el) :?>
                <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $eTgl; ?></td>
                    <td><?php echo $el['rm_kode']; ?></td>
                    <td><?php echo "{$el['purchasesuggestionmaterial_rm_deskripsi']} {$el['purchasesuggestionmaterial_rm_spesifikasi']}"; ?></td>
                    <td><?php echo $el['rmsatuan_nama']; ?></td>
                    <td style="text-align: right;"><?php echo $el['purchasesuggestionmaterial_leadtimesupplier_hari']; ?></td>
					<td style="text-align: right;"><?php echo number_format($el['rm_stock_awal']); ?></td>
					<td style="text-align: right;"><?php echo number_format($el['rm_stock_min']); ?></td>
					<td style="text-align: right;"><?php echo number_format($el['sisa_pr']); ?></td>
					<td style="text-align: right;"><?php echo number_format($el['sisa_po']); ?></td>
					<td style="text-align: right;" ><?php echo $el['sum_purchasesuggestionmaterial_qty'] * $el['partdetail_qty']; ?></td>
					<td style="text-align: right;"><?php echo number_format($el['qty_sisa']); ?></td>
					<td style="text-align: right;"><?php echo number_format($el['qty_suggest']); ?></td>
				</tr>
            <?php 
            $no++;
            endforeach; ?>
        <?php endforeach; ?>
	</tbody>
</table>


<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"ordering": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"title" : "Summary Material Purchase Suggestion",
				"exportOptions": {
					"columns": [0,1,2,3,4,5,6,7,8,9,10,11,12]
				}
			}],
		});
	}
</script>