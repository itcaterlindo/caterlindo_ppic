<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputMaster';

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtpurchasesuggestion_kd', 'name' => 'txtpurchasesuggestion_kd', 'placeholder' => 'idtxtpurchasesuggestion_kd', 'value' => isset($purchasesuggestion_kd) ? $purchasesuggestion_kd : null));

?>
<div class="row">
	<?php if (empty($purchasesuggestion_kd)) : ?>
		<div class="form-group">
			<label for='idtxtkd_msalsorders' class="col-md-2 control-label">SO</label>
			<div class="col-sm-9 col-xs-12">
				<div class="errInput" id="idErrkd_msalsorders"></div>
				<?php echo form_multiselect('txtkd_msalsorders[]', isset($opsiSO) ? $opsiSO : [], isset($soSelected) ? $soSelected : [], array('class' => 'form-control select2', 'id' => 'idtxtkd_msalsorders', 'multiple' => 'multiple')); ?>
			</div>
		</div>
	<?php endif; ?>

	<div class="form-group">
		<label for='idtxtpurchasesuggestion_leadtime_prpo' class="col-md-2 control-label">Lead Time PR to PO</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpurchasesuggestion_leadtime_prpo"></div>
			<?php echo form_input(array('type' => 'number', 'class' => 'form-control', 'name' => 'txtpurchasesuggestion_leadtime_prpo', 'id' => 'idtxtpurchasesuggestion_leadtime_prpo', 'placeholder' => 'Lead Time Hari', 'value' => isset($rowData['purchasesuggestion_leadtime_prpo']) ? $rowData['purchasesuggestion_leadtime_prpo'] : null)); ?>
		</div>
	</div>

	<div class="form-group">
		<label for="idtxtidtxtpurchasesuggestion_note" class="col-md-2 control-label">Note</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErridtxtpurchasesuggestion_note"></div>
			<?php echo form_textarea(array('type' => 'text', 'class' => 'form-control', 'name' => 'txtpurchasesuggestion_note', 'id' => 'idtxtpurchasesuggestion_note', 'placeholder' => 'Note', 'rows' => '2', 'value' => isset($rowData['purchasesuggestion_note']) ? $rowData['purchasesuggestion_note'] : null)); ?>
		</div>
	</div>
</div>

<hr>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-md-2 col-xs-12 col-md-offset-2">
			<button type="submit" name="btnSubmit" onclick="submitDataMaster('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>