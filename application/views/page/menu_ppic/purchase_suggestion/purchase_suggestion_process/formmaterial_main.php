<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputMaster';

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtpurchasesuggestionmaterial_kd', 'name' => 'txtpurchasesuggestionmaterial_kd', 'placeholder' => 'idtxtpurchasesuggestionmaterial_kd', 'value' => isset($purchasesuggestionmaterial_kd) ? $purchasesuggestionmaterial_kd : null));

?>
<div class="row">
    <div class="form-group">
        <label for="" class="col-md-3 control-label">RM Kode</label>
        <div class="col-sm-8 col-xs-12">
            <input type="text" class="form-control" readonly="readonly" value="<?php echo $row['rm_kode']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="" class="col-md-3 control-label">Deskripsi</label>
        <div class="col-sm-8 col-xs-12">
            <textarea cols="30" class="form-control" rows="2" readonly="readonly"><?php echo "{$row['purchasesuggestionmaterial_rm_deskripsi']} {$row['purchasesuggestionmaterial_rm_spesifikasi']}"; ?></textarea>
        </div>
    </div>
    <hr>

    <div class="form-group">
        <label for='idtxtpurchasesuggestionmaterial_leadtimesupplier_hari' class="col-md-3 control-label">Leadtime Supplier (hari)</label>
        <div class="col-sm-8 col-xs-12">
            <div class="errInput" id="idErrpurchasesuggestionmaterial_leadtimesupplier_hari"></div>
            <input type="number" name="txtpurchasesuggestionmaterial_leadtimesupplier_hari" class="form-control" value="<?php echo $row['purchasesuggestionmaterial_leadtimesupplier_hari']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label for='idtxtpurchasesuggestionmaterial_suggestpr_tanggal' class="col-md-3 control-label">Tgl Suggest PR</label>
        <div class="col-sm-8 col-xs-12">
            <div class="errInput" id="idErrpurchasesuggestionmaterial_suggestpr_tanggal"></div>
            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="text" name="txtpurchasesuggestionmaterial_suggestpr_tanggal" class="form-control datetime" value="<?php echo $row['purchasesuggestionmaterial_suggestpr_tanggal']; ?>">
            </div>
        </div>
    </div>
</div>
<hr>

<div class="col-md-12">
    <div class="form-group">
        <div class="col-sm-1 col-xs-12 col-md-offset-2">
            <button type="submit" name="btnSubmit" onclick="submitDataSOmaterial('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
                <i class="fa fa-save"></i> Simpan
            </button>
        </div>
    </div>
</div>
<?php echo form_close(); ?>