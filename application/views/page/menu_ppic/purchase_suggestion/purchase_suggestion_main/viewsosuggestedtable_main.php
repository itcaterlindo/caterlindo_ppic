<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$groupBySO = array();
foreach ($result as $r){
    $groupBySO[$r['kd_msalesorder']][] = $r;
}
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}
</style>

<table id="idTable" border="1" style="width:100%;">
	<thead>
		<tr>
			<th style="width:1%; text-align:center;">No.</th>
			<th style="width:7%; text-align:center;">Sales Order</th>
			<th style="width:7%; text-align:center;">Customer</th>
			<th style="width:7%; text-align:center;">Mulai Produksi</th>
			<th style="width:7%; text-align:center;">Selesai Produksi</th>
		</tr>
	</thead>
	<tbody>
        <?php 
        $no = 1;
        foreach($result as $r):?>
        <tr>
            <td><?php echo $no; ?></td>
            <td><?php 
                $no_salesorder = $r['no_salesorder'];
                if ($r['tipe_customer'] == 'Ekspor'){
                    $no_salesorder = $r['no_po'];
                }
                echo $no_salesorder; ?>
            </td>
            <td><?php echo $r['nm_customer']; ?></td>
            <td><?php echo $r['purchasesuggestionso_tglmulai_produksi']; ?></td>
            <td><?php echo $r['purchasesuggestionso_tglselesai_produksi']; ?></td>
        </tr>
        <?php $no++;endforeach; ?>
	</tbody>
</table>