<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	viewsuggestpr_tablemain('<?php echo date('Y-m-d'); ?>', '<?php echo date('Y-m-d'); ?>');

	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('form_add_master', '');
	});

	$(document).off('keyup', '.clinputqty').on('keyup', '.clinputqty', function(e) {
		var rmkd = $(this).data("rmkd");
		var qty = parseFloat($('.clprqty'+rmkd).val());
		var konversi = parseFloat($('.clprkonversi'+rmkd).val());
		var qtykonversi = qty * konversi
		$('.clprqtykonversi'+rmkd).val(qtykonversi ? qtykonversi : 0);
	});

	$(document).off('click', '#idcheckall').on('click', '#idcheckall', function() {
		if ($(this).is(':checked')) {
			$('.classCheck').prop('checked', true);
		} else {
			$('.classCheck').prop('checked', false);
		}
	});

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function viewsuggestpr_tablemain(periodestart, periodeend) {
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/viewsuggestpr_tablemain'; ?>',
				data: {
					periodestart: periodestart,
					periodeend:periodeend
				},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').slideDown().html(html);
					render_dt_suggestPR('#idtableSuggestPR');
					render_datetimepicker('datetime');
					render_satuan();
					moveTo('idMainContent');
					$('#idtxtpr_kdsuggest').val($('#idtxtpr_kd').val());
				}
			});
		});
	}

	function edit_data(purchasesuggestionso_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/formso_main'; ?>',
			data: {
				purchasesuggestionso_kd: purchasesuggestionso_kd
			},
			success: function(html) {
				toggle_modal('Form SO', html);
				render_datetimepicker('datetime');
			}
		});
	}

	function cari_suggestpr() {
		event.preventDefault();
		var periodestart = $('#idtxtperiodestart').val();
		var periodeend = $('#idtxtperiodeend').val();
		viewsuggestpr_tablemain(periodestart, periodeend);
	}

	function render_select2(element) {
		$(element).select2({
			theme: 'bootstrap',
			width: '100%',
			placeholder: '-- Pilih Opsi --',
		});
	}

	function render_datetimepicker(valClass) {
		$('.' + valClass).datetimepicker({
			format: 'YYYY-MM-DD',
		});
	}

	function render_satuan() {
		$(".clsatuan").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?php echo base_url() ?>/Auto_complete/get_rm_satuan',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramSatuan: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_dt_suggestPR(table_elem) {
		$(table_elem).DataTable({
			"scrollX": true,
			"scrollY": '300px',
			"paging": false,
			"language": {
				"lengthMenu": "Tampilkan _MENU_ data",
				"zeroRecords": "Maaf tidak ada data yang ditampilkan",
				"info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty": "Tidak ada data yang ditampilkan",
				"search": "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing": "Sedang Memproses...",
				"paginate": {
					"first": '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last": '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next": '<span class="glyphicon glyphicon-forward"></span>',
					"previous": '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"columnDefs": [{ 
				"targets": [0, 1],
				"searchable": false,
				"orderable": false
			}]
		});
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitDataSuggestPR(form_id) {
		event.preventDefault();
		var form = document.getElementById(form_id);
		var url = '<?php echo base_url() . $class_link . '/action_insert_prsuggest_batch'; ?>';

		$.ajax({
			url: url,
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(resp) {
				console.log(resp);
				if (resp.code == 200) {
					var wo_kd = $('#idtxtwo_kd').val();
					toggle_modal('', '');
					reload_tableprderail();
					notify(resp.status, resp.pesan, 'success');
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
			}
		});
	}

	function toggle_modal(modalTitle, htmlContent) {
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			delay: 2500,
			styling: 'bootstrap3'
		});
	}
</script>