<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputMaster';
if (isset($rowData)) {
    extract($rowData);
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtpr_kdsuggest', 'name' => 'txtpr_kd', 'placeholder' => 'idtxtpr_kdsuggest', 'value' => isset($pr_kd) ? $pr_kd : null));

?>
<div class="row">
    <div class="col-md-12">
        <table id="idtableSuggestPR" class="table table-bordered table-striped table-hover display nowrap" style="width:100%; font-size: 90%;">
            <thead>
                <tr style="font-weight: bold;">
                    <td style="width: 3%; text-align:center;">No</td>
                    <td style="width: 3%; text-align:center;"><input type="checkbox" id="idcheckall" checked="checked"></td>
                    <td style="width: 4%; text-align:center;">RM Kode</td>
                    <td style="width: 10%; text-align:center;">RM Deskripsi</td>
                    <td style="width: 10%; text-align:center;">SuggestPR Qty</td>
                    <td style="width: 10%; text-align:center;">Satuan</td>
                    <td style="width: 5%; text-align:center;">SuggestPR Konversi</td>
                    <td style="width: 5%; text-align:center;">SuggestPR Qty Konversi</td>
                    <td style="width: 10%; text-align:center;">Satuan Konversi</td>
                    <td style="width: 35%; text-align:center;">Due Date</td>
                    <td style="width: 25%; text-align:center;">Remarks</td>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($results as $r) : ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td> 
                            <input type="checkbox" class="classCheck" checked="checked" name="txtrm_kds[]" value="<?php echo $r['rm_kd']; ?>"> 
                            <input type="hidden" name="txtprdetail_nama[<?php echo $r['rm_kd']; ?>]" value="<?php echo $r['rm_nama']?>">
                            <input type="hidden" name="txtprdetail_deskripsi[<?php echo $r['rm_kd']; ?>]" value="<?php echo $r['rm_deskripsi']?>">
                            <input type="hidden" name="txtprdetail_spesifikasi[<?php echo $r['rm_kd']; ?>]" value="<?php echo $r['rm_spesifikasi']?>">
                        </td>
                        <td><?php echo $r['rm_kode']; ?></td>
                        <td><?php echo "{$r['rm_deskripsi']} {$r['rm_spesifikasi']}"; ?> <br>
                            <table border="1" style="width: 100% ;font-size: 80%;">
                                <tr>
                                    <td style="font-weight: bold;">RM Stok Min</td>
                                    <td style="text-align: right;"><?php echo $r['rm_stock_min']; ?></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">RM Stok</td>
                                    <td style="text-align: right;"><?php echo $r['rmstokmaster_qty']; ?></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">RM PR</td>
                                    <td style="text-align: right;"><?php echo $r['rmOpenPR']; ?></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">RM PO</td>
                                    <td style="text-align: right;"><?php echo $r['rmOpenPO']; ?></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">RM Demand</td>
                                    <td style="text-align: right;"><?php echo $r['rmDemand']; ?></td>
                                </tr>
                            </table>
                        </td>
                        <td style="text-align: center;"> <input name="txtprdetail_qty[<?php echo $r['rm_kd']; ?>]" type="number" class="clinputqty clprqty<?php echo $r['rm_kd']; ?>" data-rmkd="<?php echo $r['rm_kd']; ?>" value="<?php $qty = $r['rm_suggest_qty'] / $r['rm_konversiqty']; echo $qty; ?>"> </td>
                        <td>
                            <select name="txtrmsatuan_kd[<?php echo $r['rm_kd']; ?>]" class="clsatuan" id="">
                                <option value="<?php echo $r['satuan_secondary_kd']; ?>" selected><?php echo $r['satuan_secondary_nama']; ?></option>
                            </select>
                        </td>
                        <td style="text-align: center;"> <input name="txtprdetail_konversi[<?php echo $r['rm_kd']; ?>]" type="number" class="clinputqty clprkonversi<?php echo $r['rm_kd']; ?>" data-rmkd="<?php echo $r['rm_kd']; ?>" value="<?php echo $r['rm_konversiqty']; ?>"> </td>
                        <td style="text-align: center;"> <input name="txtprdetail_qtykonversi[<?php echo $r['rm_kd']; ?>]" type="number" class="clinputqty clprqtykonversi<?php echo $r['rm_kd']; ?>" data-rmkd="<?php echo $r['rm_kd']; ?>" readonly="readonly" value="<?php echo $r['rm_suggest_qty']; ?>"> </td>
                        <td><?php echo $r['rmsatuan_nama']; ?></td>
                        <td>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input name="txtprdetail_duedate[<?php echo $r['rm_kd']; ?>]" type="text" class="datetime" placeholder="yyyy-mm-dd" value="<?php echo date('Y-m-d', strtotime("+{$r['rm_duedatesupplier']} days")); ?>">
                            </div>
                        </td>
                        <td>
                            <textarea name="txtprdetail_remarks[<?php echo $r['rm_kd']; ?>]" class="" id="" cols="0" rows="2"></textarea>
                        </td>
                    </tr>
                <?php
                    $no++;
                endforeach; ?>
            </tbody>
        </table>

        <div class="form-group">
            <div class="col-sm-1 col-xs-12 pull-right">
                <button type="submit" name="btnSubmit" onclick="submitDataSuggestPR('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
                    <i class="fa fa-save"></i> Simpan
                </button>
            </div>
        </div>
    </div>

</div>
<!-- <hr>

<div class="col-md-12">
    <div class="form-group">
        <div class="col-sm-1 col-xs-12 pull-right">
            <button type="submit" name="btnSubmit" onclick="submitDataSuggestPR('<?php //secho $form_id; 
                                                                                    ?>')" class="btn btn-primary btn-sm">
                <i class="fa fa-save"></i> Simpan
            </button>
        </div>
    </div>
</div> -->
<?php echo form_close(); ?>