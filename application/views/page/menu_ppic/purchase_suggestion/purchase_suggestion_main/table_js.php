<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	view_calendar();
	viewsosuggestedtable_main();

	function view_calendar() {
		document.addEventListener('DOMContentLoaded', function() {
			var calendarEl = document.getElementById('<?php echo $box_content_id; ?>');

			var calendar = new FullCalendar.Calendar(calendarEl, {
				headerToolbar: {
					left: 'prev,next today',
					center: 'title',
					right: 'dayGridMonth,listWeek'
				},
				editable: false,
				selectable: true,
				businessHours: true,
				dayMaxEvents: true, // allow "more" link when too many events

				events: {
					url: '<?php echo base_url() . $class_link ?>/get_events',
					type: 'GET',
					failure: function() {
						document.getElementById('script-warning').style.display = 'block'
					}
				},
			});

			calendar.render();
		});
	}

	function viewsosuggestedtable_main(){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/viewsosuggestedtable_main'; ?>',
			success: function(html) {
				$('#idtableso').html(html);
			}
		});
	}

	function view_calendarmain() {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/view_calendarmain'; ?>',
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
			}
		});
	}

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			delay: 2500,
			styling: 'bootstrap3'
		});
	}
</script>