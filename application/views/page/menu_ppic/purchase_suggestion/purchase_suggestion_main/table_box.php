<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$js_file = 'table_js';
$box_title = 'Purchase Demand';
$data['class_link'] = $class_link;
$box_type = 'Table';
$data['master_var'] = 'WorkOrder';
$data['box_id'] = 'id' . $box_type . 'Box' . $data['master_var'];
$data['box_content_id'] = 'id' . $box_type . 'BoxContent' . $data['master_var'];
$data['box_loader_id'] = 'id' . $box_type . 'BoxLoader' . $data['master_var'];
$data['btn_add_id'] = 'id' . $box_type . 'BtnBoxAdd' . $data['master_var'];
?>

<div class="col-md-4">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $box_title; ?></h3>
		</div>
		<div class="box-body">
			<div id="idtableso"></div>
		</div>
	</div>
</div>
<div class="col-md-8">
	<div class="box box-primary" id="<?php echo $data['box_id']; ?>">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $box_title; ?></h3>
			<div class="box-tools pull-right">
				<button class="btn btn-box-tool" id="<?php echo $data['btn_add_id']; ?>" data-toggle="tooltip" title="Tambah"><i class="fa fa-plus"></i></button>
				<button class="btn btn-box-tool btn-collapse" id="idBtnBoxTableSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
					<i class="fa fa-minus"></i>
				</button>
			</div>
		</div>
		<div class="box-body">
			<div id="<?php echo $data['box_content_id']; ?>"></div>
		</div>
		<div class="box-footer">
		</div>
		<div class="overlay" id="idBoxTableOverlay" style="display: none;">
			<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
		</div>
	</div>
</div>


<?php
$this->load->view('page/' . $class_link . '/' . $js_file, $data); ?>