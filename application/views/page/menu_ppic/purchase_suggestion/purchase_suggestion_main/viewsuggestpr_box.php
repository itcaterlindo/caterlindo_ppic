<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$js_file = 'viewsuggestpr_js';
$box_title = 'Suggest PR - Material';
$data['class_link'] = $class_link;
$box_type = 'Table';
$data['master_var'] = 'WorkOrder';
$data['box_id'] = 'id' . $box_type . 'Box' . $data['master_var'];
$data['box_content_id'] = 'id' . $box_type . 'BoxContent' . $data['master_var'];
$data['box_loader_id'] = 'id' . $box_type . 'BoxLoader' . $data['master_var'];
$data['btn_add_id'] = 'id' . $box_type . 'BtnBoxAdd' . $data['master_var'];

$data['modal_title'] = $data['master_var'] . 'modalTitle';
$data['content_modal_id'] = $data['master_var'] . 'content_modal_id';
?>

<style type="text/css">
	.select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>

<div class="col-md-12">
	<div class="box box-default" id="<?php echo $data['box_id']; ?>">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $box_title; ?></h3>
		</div>
		<div class="box-body">
			<div class="row invoice-info">
				<div class="col-sm-4 invoice-col">
					<b>Requistion No :</b> <?php echo isset($headerPR['pr_no']) ? $headerPR['pr_no'] : '-'; ?><br>
					<div class="no-print">
						<b>Requistion Status :</b> <?php echo build_badgecolor($headerPR['wfstate_badgecolor'], $headerPR['wfstate_nama']); ?> <br>
					</div>
				</div>
				<div class="col-sm-4 invoice-col">
					<b>Requisition User :</b> <?php echo isset($headerPR['nm_admin']) ? $headerPR['nm_admin'] : '-'; ?> <br>
					<b>Requisition Bagian :</b> <?php echo isset($bagian_nama) ? $bagian_nama : '-'; ?>
				</div>
				<div class="col-sm-4 invoice-col">
					<b>Requisition Tanggal :</b> <?php echo format_date($headerPR['pr_tglinput'], 'd-m-Y'); ?>
				</div>
			</div>
			<hr>

			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label for="idtxtperiodestart">Periode Start</label>
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control datetime" id="idtxtperiodestart" placeholder="yyyy-mm-dd" value="<?php echo date('Y-m-d'); ?>">
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label for="idtxtperiodeend">Periode End</label>
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control datetime" id="idtxtperiodeend" placeholder="yyyy-mm-dd" value="<?php echo date('Y-m-d'); ?>">
						</div>
					</div>
				</div>
				<div class="col-md-1">
					<br>
					<button class="btn btn-sm btn-default" onclick="cari_suggestpr()"> <i class="fa fa-search"></i> Cari</button>
				</div>
			</div>

			<div id="<?php echo $data['box_content_id']; ?>"></div>
		</div>
	</div>

</div>
<div class="box-footer">
</div>
<div class="overlay" id="idBoxTableOverlay" style="display: none;">
	<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
</div>
</div>
</div>

<?php $this->load->view('page/' . $class_link . '/' . $js_file, $data); ?>

<!-- modal -->
<div class="modal fade" id="idmodal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><?php echo $data['modal_title']; ?></h4>
			</div>
			<div class="modal-body">

				<div class="row">
					<div class="col-md-12">
						<div id="<?php echo $data['content_modal_id']; ?>"></div>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Keluar</button>
			</div>
		</div>
	</div>
</div>