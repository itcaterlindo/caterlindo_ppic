<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">

</style>
<div class="box-body table-responsive no-padding">
    <table id="idTable" class="table table-bordered table-hover display" style="width:100%; font-size:90%;">
        <thead>
            <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:left;">Month</th>
                <th style="text-align:right;">Output</th>
                <th style="text-align:right;">Reject(Part/Set)</th>
                <th style="text-align:right;">Reject/Output</th>
                <th style="text-align:right;">Target</th>
                <th style="text-align:right;">Output (-) Reject</th>
                <th style="text-align:right;">Output/Target</th>
                <th style="text-align:right;">Overtime</th>
                <th style="text-align:right;">Days</th>

            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            if ($reportData) {
                foreach ($reportData as $key => $val) {
                    $total_output = 0;
                    // untuk tahun dalam judul file excel
                    if ($val['id_bulan'] == 1) {
                        echo '<input type="hidden" value="' . $val['year'] . '" id="data_year">';
                        echo '<input type="hidden" value="' . $val['division'] . '" id="data_divisi">';
                    }
                    $qty_reject = (int)$val['qty_reject'];

                    foreach ($val['data_arr']['Part'] as $v) {
                        if($v['bagian_nama'] == $val['division']){
                            $total_output += $v['sum_dndetail_qty'] ?? 0;   
                        }                     
                    }

                    /** Load data Part Top */
                    foreach ($val['data_arr']['PartTop'] as $v) {
                        if($v['bagian_nama'] == $val['division']){
                            $total_output += $v['sum_dndetail_qty'] ?? 0;   
                        }                        
                    }

                    /** Load data Packing */
                    foreach ($val['data_arr']['Packing'] as $v) {
                        if($v['bagian_nama'] == $val['division']){
                            $total_output += $v['sum_dndetail_qty'] ?? 0;   
                        }                         
                    }

                    $total_output += $qty_reject;

                    if ($qty_reject == 0 and $total_output == 0) {
                        $reject_prosentase = 0;
                    } else {
                        $reject_prosentase = ($qty_reject / $total_output) * 100;
                    }

                    $data_target = $val['data_target']['target'];
                    $day_active = $val['data_target']['day_active'];
                    $total_output2 = $total_output - $qty_reject;
                    $output_target = ($total_output / $data_target) * 100;

                    /** Jika semua data 0 jangan tampilkan */
                    // if ($val['data_arr']['MARKING'] <= 0 && $val['data_arr']['BENDING'] <= 0 && $val['data_arr']['BENDING_PIPE'] <= 0 && $val['data_arr']['CUSTOM'] <= 0 && $val['data_arr']['ASSEMBLING'] <= 0 && $val['data_arr']['FINISHING'] <= 0 && $val['data_arr']['PACKING'] <= 0)
                    //     continue;
            ?>
                    <tr>
                        <td class="text-center"><?= $no++ ?></td>
                        <td><?= date('F', mktime(0, 0, 0, $val['id_bulan'], 10)) ?></td>
                        <td class="text-right"> <?= $total_output ?> </td>
                        <td class="text-right"> <?= $qty_reject ?> </td>
                        <td class="text-right"> <?= round($reject_prosentase, 2) ?> %</td>
                        <td class="text-right"> <?= $data_target ?></td>
                        <td class="text-right"> <?= $total_output2 ?></td>
                        <td class="text-right"> <?= round($output_target, 2) ?> %</td>
                        <td class="text-right"> - </td>
                        <td class="text-right"> <?= $day_active ?> days</td>
                    </tr>
                <?php } ?>
        </tbody>
    <?php } ?>
    </table>
</div>

<script type="text/javascript">
    var year = $('#data_year').val();
    var divisi = $('#data_divisi').val();
    render_dt('#idTable');

    function render_dt(table_elem) {
        $(table_elem).DataTable({
            "dom": 'Bfrtip',
            "paging": false,
            "order": [],
            "language": {
                "lengthMenu": "Tampilkan _MENU_ data",
                "zeroRecords": "Maaf tidak ada data yang ditampilkan",
                "info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
                "infoFiltered": "(difilter dari _MAX_ total data)",
                "infoEmpty": "Tidak ada data yang ditampilkan",
                "search": "Cari :",
                "loadingRecords": "Memuat Data...",
                "processing": "Sedang Memproses...",
                "paginate": {
                    "first": '<span class="glyphicon glyphicon-fast-backward"></span>',
                    "last": '<span class="glyphicon glyphicon-fast-forward"></span>',
                    "next": '<span class="glyphicon glyphicon-forward"></span>',
                    "previous": '<span class="glyphicon glyphicon-backward"></span>'
                }
            },
            "buttons": [{
                "extend": "excel",
                "title": "Report Delivery Note " + divisi + " " + year,
                "exportOptions": {
                    "columns": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                }
            }],
        });
    }
</script>