<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">

</style>
<div class="box-body table-responsive no-padding">
    <table id="idTable" class="table table-bordered table-hover display" style="width:100%; font-size:90%;">
        <thead>
            <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:left;">Month</th>
                <th style="text-align:right;">MARKING</th>
                <th style="text-align:right;">BENDING</th>
                <th style="text-align:right;">BENDING PIPE</th>
                <th style="text-align:right;">CUSTOM</th>
                <th style="text-align:right;">ASSEMBLING</th>
                <th style="text-align:right;">FINISHING</th>
                <th style="text-align:right;">PACKING</th>

            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            // echo json_encode($reportData);
            // exit();
            if ($reportData) {
                foreach ($reportData as $key => $val) {
                    // untuk tahun dalam judul file excel
                    if ($val['id_bulan'] == 1) {
                        echo '<input type="hidden" value="' . $val['year'] . '" id="data_year">';
                    }

                    $lastdate = date("t", strtotime($val['year'] . '-' . $val['id_bulan'] . '-01'));
                    $marking = 0;
                    $bending = 0;
                    $bending_pipe = 0;
                    $custom = 0;
                    $assembling = 0;
                    $finishing = 0;
                    $packing = 0;

                    for ($n = 1; $n <= $lastdate; $n++) {


                        foreach ($val['data_arr']['Part'] as $k => $v) {
                            if ($n == (int)date('d', strtotime($v['tgl_dn']))) {
                                if ($v['bagian_nama'] == 'MARKING') {
                                    $marking += $v['sum_dndetail_qty'] ?? 0;
                                } elseif ($v['bagian_nama'] == 'BENDING') {
                                    $bending += $v['sum_dndetail_qty'] ?? 0;
                                } elseif ($v['bagian_nama'] == 'BENDING_PIPE') {
                                    $bending_pipe += $v['sum_dndetail_qty'] ?? 0;
                                } elseif ($v['bagian_nama'] == 'CUSTOM') {
                                    $custom += $v['sum_dndetail_qty'] ?? 0;
                                } elseif ($v['bagian_nama'] == 'ASSEMBLING') {
                                    $assembling += $v['sum_dndetail_qty'] ?? 0;
                                } elseif ($v['bagian_nama'] == 'FINISHING') {
                                    $finishing += $v['sum_dndetail_qty'] ?? 0;
                                } elseif ($v['bagian_nama'] == 'PACKING') {
                                    $packing += $v['sum_dndetail_qty'] ?? 0;
                                } else {
                                    continue;
                                }
                            }
                        }

                        foreach ($val['data_arr']['PartTop'] as $k => $v) {
                            if ($n == (int)date('d', strtotime($v['tgl_dn']))) {
                                if ($v['bagian_nama'] == 'MARKING') {
                                    $marking += $v['sum_dndetail_qty'] ?? 0;
                                } elseif ($v['bagian_nama'] == 'BENDING') {
                                    $bending += $v['sum_dndetail_qty'] ?? 0;
                                } elseif ($v['bagian_nama'] == 'BENDING_PIPE') {
                                    $bending_pipe += $v['sum_dndetail_qty'] ?? 0;
                                } elseif ($v['bagian_nama'] == 'CUSTOM') {
                                    $custom += $v['sum_dndetail_qty'] ?? 0;
                                } elseif ($v['bagian_nama'] == 'ASSEMBLING') {
                                    $assembling += $v['sum_dndetail_qty'] ?? 0;
                                } elseif ($v['bagian_nama'] == 'FINISHING') {
                                    $finishing += $v['sum_dndetail_qty'] ?? 0;
                                } elseif ($v['bagian_nama'] == 'PACKING') {
                                    $packing += $v['sum_dndetail_qty'] ?? 0;
                                } else {
                                    continue;
                                }
                            }
                        }

                        foreach ($val['data_arr']['Packing'] as $k => $v) {
                            if ($n == (int)date('d', strtotime($v['tgl_dn']))) {
                                if ($v['bagian_nama'] == 'MARKING') {
                                    $marking += $v['sum_dndetail_qty'] ?? 0;
                                } elseif ($v['bagian_nama'] == 'BENDING') {
                                    $bending += $v['sum_dndetail_qty'] ?? 0;
                                } elseif ($v['bagian_nama'] == 'BENDING_PIPE') {
                                    $bending_pipe += $v['sum_dndetail_qty'] ?? 0;
                                } elseif ($v['bagian_nama'] == 'CUSTOM') {
                                    $custom += $v['sum_dndetail_qty'] ?? 0;
                                } elseif ($v['bagian_nama'] == 'ASSEMBLING') {
                                    $assembling += $v['sum_dndetail_qty'] ?? 0;
                                } elseif ($v['bagian_nama'] == 'FINISHING') {
                                    $finishing += $v['sum_dndetail_qty'] ?? 0;
                                } elseif ($v['bagian_nama'] == 'PACKING') {
                                    $packing += $v['sum_dndetail_qty'] ?? 0;
                                } else {
                                    continue;
                                }
                            }
                        }
                    }


                    /** Jika semua data 0 jangan tampilkan */
                    // if ($val['data_arr']['MARKING'] <= 0 && $val['data_arr']['BENDING'] <= 0 && $val['data_arr']['BENDING_PIPE'] <= 0 && $val['data_arr']['CUSTOM'] <= 0 && $val['data_arr']['ASSEMBLING'] <= 0 && $val['data_arr']['FINISHING'] <= 0 && $val['data_arr']['PACKING'] <= 0)
                    //     continue;
            ?>
                    <tr>
                        <td class="text-center"><?= $no++ ?></td>
                        <td><?= date('F', mktime(0, 0, 0, $val['id_bulan'], 10)) ?></td>
                        <td class="text-right"> <?= $marking ?> </td>
                        <td class="text-right"> <?= $bending ?> </td>
                        <td class="text-right"> <?= $bending_pipe ?> </td>
                        <td class="text-right"> <?= $custom ?> </td>
                        <td class="text-right"> <?= $assembling ?> </td>
                        <td class="text-right"> <?= $finishing ?> </td>
                        <td class="text-right"> <?= $packing ?> </td>
                    </tr>
                <?php } ?>
        </tbody>
        <!-- <tfoot>
            <tr>
                <td colspan="2" class="text-center">TOTAL</td>
                <td class="text-right"> <?= array_sum(array_column($reportData, 'MARKING')) ?> </td>
                <td class="text-right"> <?= array_sum(array_column($reportData, 'BENDING')) ?> </td>
                <td class="text-right"> <?= array_sum(array_column($reportData, 'BENDING_PIPE')) ?> </td>
                <td class="text-right"> <?= array_sum(array_column($reportData, 'CUSTOM')) ?> </td>
                <td class="text-right"> <?= array_sum(array_column($reportData, 'ASSEMBLING')) ?> </td>
                <td class="text-right"> <?= array_sum(array_column($reportData, 'FINISHING')) ?> </td>
                <td class="text-right"> <?= array_sum(array_column($reportData, 'PACKING')) ?> </td>
            </tr>
        </tfoot> -->
    <?php } ?>
    </table>
</div>

<script type="text/javascript">
    var year = $('#data_year').val();
    render_dt('#idTable');

    function render_dt(table_elem) {
        $(table_elem).DataTable({
            "dom": 'Bfrtip',
            "paging": false,
            "order": [],
            "language": {
                "lengthMenu": "Tampilkan _MENU_ data",
                "zeroRecords": "Maaf tidak ada data yang ditampilkan",
                "info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
                "infoFiltered": "(difilter dari _MAX_ total data)",
                "infoEmpty": "Tidak ada data yang ditampilkan",
                "search": "Cari :",
                "loadingRecords": "Memuat Data...",
                "processing": "Sedang Memproses...",
                "paginate": {
                    "first": '<span class="glyphicon glyphicon-fast-backward"></span>',
                    "last": '<span class="glyphicon glyphicon-fast-forward"></span>',
                    "next": '<span class="glyphicon glyphicon-forward"></span>',
                    "previous": '<span class="glyphicon glyphicon-backward"></span>'
                }
            },
            "buttons": [{
                "extend": "excel",
                "title": "Report Delivery Note Yearly " + year,
                "exportOptions": {
                    "columns": [0, 1, 2, 3, 4, 5, 6, 7, 8]
                }
            }],
        });
    }
</script>