<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">

</style>
<div class="box-body table-responsive no-padding">
	<table id="idTable" class="table table-bordered table-hover display" style="width:100%; font-size:90%;">
		<thead>
			<tr>
				<th style="text-align:center;">No.</th>
				<th style="text-align:left;">Item Code</th>
				<th style="text-align:right;">MARKING</th>
				<th style="text-align:right;">BENDING</th>
				<th style="text-align:right;">BENDING PIPE</th>
				<th style="text-align:right;">CUSTOM</th>
				<th style="text-align:right;">ASSEMBLING</th>
				<th style="text-align:right;">FINISHING</th>
				<th style="text-align:right;">PACKING</th>

			</tr>
		</thead>
		<tbody>
			<?php
			$no = 1;
			// echo json_encode($result);
			// exit();
			if ($result) {
				/** extract kd barang */
				$item = array();
				foreach ($result['Part'] as $k => $v) {
					$n_item = count($item) - 1;
					$i = 0;
					$st = false;
					for( $i = 0; $i <= $n_item; $i++ ) {
						if( $item[$i] == $v['kd_barang'] ){
            				$st = true;
						}
					}

					if($st == false){
						array_push($item, $v['kd_barang']);
					}
				}
				foreach ($result['PartTop'] as $k => $v) {
					$n_item = count($item) - 1;
					$i = 0;
					$st = false;
					for( $i = 0; $i <= $n_item; $i++ ) {
						if( $item[$i] == $v['kd_barang'] ){
            				$st = true;
						}
					}

					if($st == false){
						array_push($item, $v['kd_barang']);
					}
					// if (empty(array_search($v['kd_barang'], $item))) {
					// 	array_push($item, $v['kd_barang']);
					// 	if($v['kd_barang'] == 'PRD020817000001'){
					// 		echo array_search($v['kd_barang'], $item);
					// 	}
					// }
				}
				foreach ($result['Packing'] as $k => $v) {
					$n_item = count($item) - 1;
					$i = 0;
					$st = false;
					for( $i = 0; $i <= $n_item; $i++ ) {
						if( $item[$i] == $v['kd_barang'] ){
            				$st = true;
						}
					}

					if($st == false){
						array_push($item, $v['kd_barang']);
					}
				}
				// echo json_encode($item);exit();
				foreach ($item as $kd) {
					$marking = 0;
					$bending = 0;
					$bending_pipe = 0;
					$custom = 0;
					$assembling = 0;
					$finishing = 0;
					$packing = 0;

					// /** Load data Part */
					foreach ($result['Part'] as $key => $val) {
						if ($kd == $val['kd_barang']) {
							if ($val['bagian_nama'] == 'MARKING') {
								$marking += $val['sum_dndetail_qty'] ?? 0;
							} elseif ($val['bagian_nama'] == 'BENDING') {
								$bending += $val['sum_dndetail_qty'] ?? 0;
							} elseif ($val['bagian_nama'] == 'BENDING_PIPE') {
								$bending_pipe += $val['sum_dndetail_qty'] ?? 0;
							} elseif ($val['bagian_nama'] == 'CUSTOM') {
								$custom += $val['sum_dndetail_qty'] ?? 0;
							} elseif ($val['bagian_nama'] == 'ASSEMBLING') {
								$assembling += $val['sum_dndetail_qty'] ?? 0;
							} elseif ($val['bagian_nama'] == 'FINISHING') {
								$finishing += $val['sum_dndetail_qty'] ?? 0;
							} elseif ($val['bagian_nama'] == 'PACKING') {
								$packing += $val['sum_dndetail_qty'] ?? 0;
							} else {
								continue;
							}

							$itemcode = $val['woitem_itemcode'];
						}
					}

					/** Load data Packing */
					foreach ($result['Packing'] as $key => $val) {
						if ($kd == $val['kd_barang']) {
							if ($val['bagian_nama'] == 'MARKING') {
								$marking += $val['sum_dndetail_qty'] ?? 0;
							} elseif ($val['bagian_nama'] == 'BENDING') {
								$bending += $val['sum_dndetail_qty'] ?? 0;
							} elseif ($val['bagian_nama'] == 'BENDING_PIPE') {
								$bending_pipe += $val['sum_dndetail_qty'] ?? 0;
							} elseif ($val['bagian_nama'] == 'CUSTOM') {
								$custom += $val['sum_dndetail_qty'] ?? 0;
							} elseif ($val['bagian_nama'] == 'ASSEMBLING') {
								$assembling += $val['sum_dndetail_qty'] ?? 0;
							} elseif ($val['bagian_nama'] == 'FINISHING') {
								$finishing += $val['sum_dndetail_qty'] ?? 0;
							} elseif ($val['bagian_nama'] == 'PACKING') {
								$packing += $val['sum_dndetail_qty'] ?? 0;
							} else {
								continue;
							}
							$itemcode = $val['woitem_itemcode'];
						}
					}

					/** Load data Part Top */
					foreach ($result['PartTop'] as $key => $val) {

						if ($kd == $val['kd_barang']) {
							if ($val['bagian_nama'] == 'MARKING') {
								$marking += $val['sum_dndetail_qty'] ?? 0;
							} elseif ($val['bagian_nama'] == 'BENDING') {
								$bending += $val['sum_dndetail_qty'] ?? 0;
							} elseif ($val['bagian_nama'] == 'BENDING_PIPE') {
								$bending_pipe += $val['sum_dndetail_qty'] ?? 0;
							} elseif ($val['bagian_nama'] == 'CUSTOM') {
								$custom += $val['sum_dndetail_qty'] ?? 0;
							} elseif ($val['bagian_nama'] == 'ASSEMBLING') {
								$assembling += $val['sum_dndetail_qty'] ?? 0;
							} elseif ($val['bagian_nama'] == 'FINISHING') {
								$finishing += $val['sum_dndetail_qty'] ?? 0;
							} elseif ($val['bagian_nama'] == 'PACKING') {
								$packing += 0; //$val['sum_dndetail_qty'] ?? 0;
							} else {
								continue;
							}
							$itemcode = $val['woitem_itemcode'];
						}
					}

					// untuk judul file excel
					if ($no == 1) {
						echo '<input type="hidden" value="' . $bulanthn . '" id="judul_xl">';
					}

					if ($kd) {
						if($marking > 0 OR $bending > 0 OR $bending_pipe > 0 OR $custom > 0 OR $assembling > 0 OR $finishing > 0 OR $packing > 0){
						
						if($itemcode == ''){
							// $itemcode = $val['woitem_itemcode2'];
							$itemcode = get_rm_nama($kd)->rm_name ?? 'no name';
						}
							echo '<tr>
                            <td class="text-center">' . $no . '</td>
                            <td>' . $itemcode . '</td>
                            <td class="text-right">' . $marking . '</td>
                            <td class="text-right">' . $bending . '</td>
                            <td class="text-right">' .  $bending_pipe . '</td>
                            <td class="text-right">' .  $custom . '</td>
                            <td class="text-right">' .  $assembling . '</td>
                            <td class="text-right">' .  $finishing . '</td>
                            <td class="text-right">' .  $packing . '</td>
                        </tr>';
						}
						//TOTAL
						$n_marking += $marking;
						$n_bending += $bending;
						$n_bending_pipe += $bending_pipe;
						$n_custom += $custom;
						$n_assembling += $assembling;
						$n_finishing += $finishing;
						$n_packing += $packing;
						$no += 1;
					}
					
				}

			?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2" class="text-center">TOTAL</td>
				<td class="text-right"> <?= $n_marking ?> </td>
				<td class="text-right"> <?= $n_bending ?> </td>
				<td class="text-right"> <?= $n_bending_pipe ?> </td>
				<td class="text-right"> <?= $n_custom ?> </td>
				<td class="text-right"> <?= $n_assembling ?> </td>
				<td class="text-right"> <?= $n_finishing ?> </td>
				<td class="text-right"> <?= $n_packing ?> </td>
			</tr>
		</tfoot>
	<?php } ?>
	</table>
</div>

<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"order": [],
			"language": {
				"lengthMenu": "Tampilkan _MENU_ data",
				"zeroRecords": "Maaf tidak ada data yang ditampilkan",
				"info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty": "Tidak ada data yang ditampilkan",
				"search": "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing": "Sedang Memproses...",
				"paginate": {
					"first": '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last": '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next": '<span class="glyphicon glyphicon-forward"></span>',
					"previous": '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons": [{
				"extend": "excel",
				"title": "Report Delivery Note",
				"exportOptions": {
					"columns": [0, 1, 2, 3, 4, 5, 6, 7, 8]
				}
			}],
		});
	}
</script>