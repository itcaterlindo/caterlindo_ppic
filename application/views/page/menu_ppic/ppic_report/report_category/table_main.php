<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">

</style>
<div class="box-body table-responsive no-padding">
	<table id="idTable" class="table table-bordered table-hover display" style="width:100%; font-size:90%;">
		<thead>
			<tr>
				<th style="text-align:center;">No.</th>
				<th style="text-align:left;">Produk</th>
				<th style="text-align:right;">Jumlah</th>
				<th style="text-align:right;">Kg</th>
				<th style="text-align:right;">Nilai (Rp)</th>
				<th style="text-align:right;">Qty Lokal</th>
				<th style="text-align:right;">Rp Lokal</th>
				<th style="text-align:right;">Qty Ekspor</th>
				<th style="text-align:right;">Rp Ekspor</th>				
				<th style="text-align:right;">Presentase Produksi yang diekspor</th>
				<th style="text-align:right;">Kode negara tujuan ekspor</th>

			</tr>
		</thead>
		<tbody>
			<?php
			$no = 1;
			// echo json_encode($result);
			// exit();
			if ($result) {

				foreach ($result as $v) {
					$var_helper = ($v['rp_lokal'] + $v['rp_ekspor']) / ($v['jumlah_lokal'] + $v['jumlah_ekspor']);

					if(is_nan($var_helper)){
						$nilai = 0;
						$persn = 0;
					}else{
						$nilai = number_format(round($v['jumlah'] * $var_helper), 0, "", ",");
						$persn = round(($v['jumlah_ekspor'] / $v['jumlah']) * 100);
					}

					echo '<tr>
                            <td class="text-center">' . $no . '</td>
                            <td>' . $v['nm_kat_barang'] . '</td>
                            <td class="text-right">' . number_format($v['jumlah'], 0, "", ",") . '</td>
                            <td class="text-right">' . number_format(round($v['kg']), 0, "", ",") . '</td>
							<td class="text-right">' . $nilai . '</td>
                            <td class="text-right">' . number_format(round($v['jumlah_lokal']), 0, "", ",") . '</td>
                            <td class="text-right">' . number_format(round($v['rp_lokal']), 0, "", ",") . '</td>
                            <td class="text-right">' . number_format(round($v['jumlah_ekspor']), 0, "", ",") . '</td>
                            <td class="text-right">' . number_format(round($v['rp_ekspor']), 0, "", ",") . '</td>                            
                            <td class="text-right">' . $persn . '%</td>
                            <td class="text-right"></td>
                        </tr>';

					$no += 1;
				}

			?>
		</tbody>

	<?php } ?>
	</table>
</div>

<script type="text/javascript">
	var monthAwal = $('#idtxtMonthAwal').val();
	var yearAwal = $('#idtxtYearAwal').val();
	var monthAkhir = $('#idtxtMonthAkhir').val();
	var yearAkhir = $('#idtxtYearAkhir').val();
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"order": [],
			"language": {
				"lengthMenu": "Tampilkan _MENU_ data",
				"zeroRecords": "Maaf tidak ada data yang ditampilkan",
				"info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty": "Tidak ada data yang ditampilkan",
				"search": "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing": "Sedang Memproses...",
				"paginate": {
					"first": '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last": '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next": '<span class="glyphicon glyphicon-forward"></span>',
					"previous": '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons": [{
				"extend": "excel",
				"title": "Report DN Kategori (" + monthAwal + "-" + yearAwal + " sampai " + monthAkhir + "-" + yearAkhir + ")",
				"exportOptions": {
					"columns": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
				}
			}],
		});
	}
</script>