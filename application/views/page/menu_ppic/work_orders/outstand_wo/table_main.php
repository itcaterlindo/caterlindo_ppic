<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}
</style>
<div class="box-body table-responsive no-padding">
	<table id="idTable" class="table table-bordered table-striped table-hover display" style="font-size: 90% ;width:100%;">
		<thead>
			<tr>
				<th style="width:1%; text-align:center;">No.</th>
				<th style="width:7%; text-align:center;">Rm Kode</th>
				<th style="width:17%; text-align:center;">Rm Nama</th>
				<th style="width:10%; text-align:center;">Sisa PO</th>
				<th style="width:10%; text-align:center;">Sisa PR</th>
				<th style="width:10%; text-align:center;">Sisa WO</th>
				<th style="width:10%; text-align:center;">Stok Material</th>
				<th style="width:13%; text-align:center;">Jml Kebutuhan All Item</th>
				<th style="width:10%; text-align:center;">Jml MRC</th>
				<th style="width:10%; text-align:center;">Jml Material Outstanding</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$no = 1;
			foreach ($items as $wip) : ?>
				<tr style="font-weight: bold; font-size: 105%;;">
					<td><?php echo $no; ?></td>
					<td><?php echo $wip->rm_kode; ?></td>
					<td><?php echo "{$wip->rm_deskripsi} / {$wip->rm_nama}"; ?></td>
					<td style="text-align: center;"><?php echo $wip->sisa_po; ?></td>
					<td style="text-align: center;"><?php echo $wip->sisa_pr; ?></td>
					<td style="text-align: center;"><?php echo $wip->sisa_wo; ?></td>
					<td style="text-align: center;"><?php echo $wip->qty_aft_mutasi; ?></td>
					<td style="text-align: center;"><?php echo $wip->all_material_by_woitem; ?></td>
					<td style="text-align: center;"><?php echo $wip->MRC ? $wip->MRC : 0; ?></td>
					<td style="text-align: center;"><?php echo $wip->Final_outstanding ? $wip->Final_outstanding: $wip->all_material_by_woitem; ?></td>
				</tr>
			<?php
				$no++;
			endforeach; ?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"scrollX": true,
			"paging": false,
			"ordering": false,
			"scrollX": true,
			"scrollY": "500px",
			"info": false,
			"language": {
				"lengthMenu": "Tampilkan _MENU_ data",
				"zeroRecords": "Maaf tidak ada data yang ditampilkan",
				"info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty": "Tidak ada data yang ditampilkan",
				"search": "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing": "Sedang Memproses...",
				"paginate": {
					"first": '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last": '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next": '<span class="glyphicon glyphicon-forward"></span>',
					"previous": '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons": [{
				"extend": "excelHtml5",
				"title": "Work Order Position",
				"exportOptions": {
					"columns": [':visible']
				}
			}],
		});
	}
</script>