<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}
</style>
<div class="box-body table-responsive no-padding">
	<table id="idTable" class="table table-bordered table-striped table-hover display" style="font-size: 90% ;width:100%;">
		<thead>
			<tr>
				<th style="width:1%; text-align:center;">No.</th>
				<th style="width:7%; text-align:center;">No WO</th>
				<th style="width:17%; text-align:center;">Item Code</th>
				<th style="width:7%; text-align:center;">Qty</th>
				<th style="width:10%; text-align:center;">Jenis</th>
				<th style="width:7%; text-align:center;">Status</th>
				<th style="width:17%; text-align:center;">Note</th>
				<?php foreach ($bagians as $bagian) : ?>
					<th style="text-align: center; font-size: 90%; background-color: gray;"><?php echo $bagian['bagian_nama'] ?></th>
				<?php endforeach; ?>
			</tr>
		</thead>
		<tbody>
			<?php
			$no = 1;
			foreach ($wips as $wip) : ?>
				<tr style="font-weight: bold; font-size: 105%;;">
					<td><?php echo $no; ?></td>
					<td><?php echo $wip['item_code']; ?></td>
					<td><?php echo "{$wip['deskripsi_barang']} / {$wip['dimensi_barang']}"; ?></td>
					<td style="text-align: right;"><?php echo $wip['wip_qty']; ?></td>
					<td></td>
					<td></td>
					<td></td>
					<?php
					for ($i = 0; $i < count($bagians); $i++) :
					?>
						<td></td>
					<?php
					endfor; ?>
				</tr>
				<?php foreach ($woCompletes as $woComplete) :
					/** WO Complete */
					if ($woComplete['kd_barang'] == $wip['kd_barang']) : ?>
						<tr>
							<td></td>
							<td><?php echo $woComplete['woitem_no_wo']; ?></td>
							<td><?php echo $woComplete['woitem_itemcode']; ?></td>
							<td style="text-align: right;"><?php echo $woComplete['woitem_qty']; ?></td>
							<td><?php echo $woComplete['woitem_jenis']; ?></td>
							<td><?php echo $woComplete['woitem_status']; ?></td>
							<td><?php echo $woComplete['woitem_note']; ?></td>
							<?php
							if (isset($woitemMappings[$woComplete['woitem_kd']])) :
								$completeMappings = $woitemMappings[$woComplete['woitem_kd']];
								foreach ($completeMappings as $completeMapping) : ?>
									<td style="text-align: right;"><?php echo $completeMapping['qty'] ?></td>
								<?php
								endforeach;
							else :
								for ($i = 0; $i < count($bagians); $i++) :
								?>
									<td style="text-align: right;">0</td>
							<?php
								endfor;
							endif; ?>
						</tr>
						<?php
						foreach ($woParts as $woPart) :
							/** WO Part */
							if (
								$woPart['woitemdetail_parent'] == $woComplete['woitemdetail_kd'] ||
								$woPart['woitemso_kd'] == $woComplete['woitemso_kd']
							) :
						?>
								<tr>
									<td></td>
									<td><?php echo $woPart['woitem_no_wo']; ?></td>
									<td><?php echo $woPart['woitem_itemcode']; ?></td>
									<td style="text-align: right;"><?php echo $woPart['woitem_qty']; ?></td>
									<td><?php echo $woPart['woitem_jenis']; ?></td>
									<td><?php echo $woPart['woitem_status']; ?></td>
									<td><?php echo $woPart['woitem_note']; ?></td>
									<?php
									if (isset($woitemMappings[$woPart['woitem_kd']])) :
										$mappings = $woitemMappings[$woPart['woitem_kd']];
										foreach ($mappings as $mapping) : ?>
											<td style="text-align: right;"><?php echo $mapping['qty'] ?></td>
										<?php
										endforeach;
									else :
										for ($i = 0; $i < count($bagians); $i++) :
										?>
											<td style="text-align: right;">0</td>
									<?php
										endfor;
									endif; ?>
								</tr>
						<?php
							endif;
						endforeach; ?>
				<?php endif;
				endforeach; ?>
			<?php
				$no++;
			endforeach; ?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"scrollX": true,
			"paging": false,
			"ordering": false,
			"scrollX": true,
			"scrollY": "500px",
			"info": false,
			"language": {
				"lengthMenu": "Tampilkan _MENU_ data",
				"zeroRecords": "Maaf tidak ada data yang ditampilkan",
				"info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty": "Tidak ada data yang ditampilkan",
				"search": "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing": "Sedang Memproses...",
				"paginate": {
					"first": '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last": '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next": '<span class="glyphicon glyphicon-forward"></span>',
					"previous": '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons": [{
				"extend": "excelHtml5",
				"title": "Work Order Position",
				"exportOptions": {
					"columns": [':visible']
				}
			}],
		});
	}
</script>