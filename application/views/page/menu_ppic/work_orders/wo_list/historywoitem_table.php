<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }
</style>
<div class="col-md-12">
    <div class="row invoice-info">
        <div class="col-sm-6 invoice-col">
            <b>No Terbit :</b> #<?php echo $master['wo_noterbit']; ?><br>
            <b>Tgl Terbit :</b> <?php echo $master['wo_tanggal']; ?>
        </div>
        <div class="col-sm-6 invoice-col">
            <b>No WO : </b> <?php echo $master['woitem_no_wo']; ?> <br>
            <b>Item Code:</b> <?php echo $master['woitem_itemcode']; ?> <br>
            <b>Deskripsi:</b> <?php echo "{$master['woitem_deskripsi']} {$master['woitem_dimensi']}"; ?>
        </div>
    </div>
    <hr>

    <div class="row">
        <div class="col-md-4">
            <div class="box-body table-responsive no-padding">
                <table id="idTable" border="1" style="width:100%; font-size: 100%;">
                    <thead>
                        <tr>
                            <th style="width:10%; text-align:center;">Bagian</th>
                            <th style="width:7%; text-align:center;">Qty</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr style="font-size: 110%; font-weight: bold; text-decoration: underline;">
                            <td>Qty WO</td>
                            <td style="text-align:right;"><?php echo $master['woitem_qty']; ?></td>
                        </tr>
                        <?php
                        foreach ($bagianQtys as $bagianQty) : ?>
                            <tr>
                                <td><?php echo ucwords($bagianQty['bagian_nm']); ?></td>
                                <td style="text-align:right;"><?php echo $bagianQty['qty']; ?></td>
                            </tr>
                        <?php
                        endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <hr>
    <h4><u> History </u></h4>
    <div class="row">
        <div class="col-md-12">
            <div class="box-body table-responsive no-padding">
                <table id="idTable" border="1" style="width:100%; font-size: 90%;">
                    <thead>
                        <tr>
                            <th style="width:10%; text-align:center;">Tgl Transaksi</th>
                            <th style="width:7%; text-align:center;">No WO</th>
                            <th style="width:7%; text-align:center;">No DN</th>
                            <th style="width:7%; text-align:center;">Item Code</th>
                            <th style="width:2%; text-align:center;">Item Status</th>
                            <th style="width:10%; text-align:center;">Deskripsi</th>
                            <th style="width:7%; text-align:center;">Asal</th>
                            <th style="width:7%; text-align:center;">Tujuan</th>
                            <th style="width:7%; text-align:center;">Qty</th>
                            <th style="width:10%; text-align:center;">Remark</th>
                            <th style="width:5%; text-align:center;">Tgl Selesai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($result as $r) : ?>
                            <tr>
                                <td><?php echo $r['dndetailreceived_tgledit']; ?></td>
                                <td><?php echo $r['woitem_no_wo']; ?></td>
                                <td><?php echo $r['dn_no']; ?></td>
                                <td><?php echo $r['woitem_itemcode']; ?></td>
                                <td><?php echo $r['woitem_status']; ?></td>
                                <td><?php echo "{$r['woitem_deskripsi']} {$r['woitem_dimensi']}"; ?></td>
                                <td><?php echo $r['bagian_asal']; ?></td>
                                <td><?php echo $r['bagian_tujuan']; ?></td>
                                <td class="dt-right"><?php echo $r['dndetailreceived_qty']; ?></td>
                                <td><?php echo $r['dndetailreceived_remark'] ?></td>
                                <td><?php echo $r['woitem_tglselesai']; ?></td>
                            </tr>
                        <?php
                        endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <h4><u> PYR </u></h4>
    <div class="row">
        <div class="col-md-12">
            <div class="box-body table-responsive no-padding">
                <table id="idTable" border="1" style="width:100%; font-size: 90%;">
                    <thead>
                        <tr>
                            <th style="width:7%; text-align:center;">Tgl Transaksi</th>
                            <th style="width:7%; text-align:center;">No WO</th>
                            <th style="width:7%; text-align:center;">Item Code</th>
                            <th style="width:2%; text-align:center;">Item Status</th>
                            <th style="width:15%; text-align:center;">Deskripsi</th>
                            <th style="width:5%; text-align:center;">Jenis WO</th>
                            <th style="width:5%; text-align:center;">Qty</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($pyrdatas['woitem'] as $woitem) : ?>
                            <tr>
                                <td><?php echo $woitem['woitempyr_tglinput']; ?></td>
                                <td><?php echo $woitem['woitem_no_wo']; ?></td>
                                <td><?php echo $woitem['woitem_itemcode']; ?></td>
                                <td><?php echo strtoupper($woitem['woitem_status']); ?></td>
                                <td><?php echo "{$woitem['woitem_deskripsi']} {$woitem['woitem_dimensi']}"; ?></td>
                                <td><?php echo strtoupper($woitem['woitem_jenis']); ?></td>
                                <td class="dt-right"><?php echo $woitem['woitem_qty']; ?></td>
                            </tr>
                            <?php 
                            foreach ($pyrdatas['child'] as $child) :
                                if ($child['woitemdetail_parent'] == $woitem['woitemdetail_kd']):
                            ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><?php echo $child['partjenis_nama']; ?></td>
                                <td><?php echo $child['partmain_nama']; ?></td>
                                <td><?php echo $child['woitemdetail_generatewo']; ?></td>
                                <td class="dt-right"><?php echo $child['woitemdetail_qty']; ?></td>
                            </tr>
                            <?php
                                endif;
                            endforeach;
                        endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>