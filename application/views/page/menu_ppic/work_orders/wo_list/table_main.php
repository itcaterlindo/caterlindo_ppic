<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}
</style>
<div class="box-body table-responsive no-padding">
	<table id="idTable" class="table table-bordered table-striped table-hover display" style="width:100%;">
		<thead>
			<tr>
				<th style="width:1%; text-align:center;">No.</th>
				<th style="width:1%; text-align:center;">Opsi</th>
				<th style="width:7%; text-align:center;">No WO</th>
				<th style="width:7%; text-align:center;">No MRC</th>
				<th style="width:7%; text-align:center;">Item Code</th>
				<th style="width:7%; text-align:center;">Item Status</th>
				<th style="width:7%; text-align:center;">Qty</th>
				<th style="width:7%; text-align:center;">Deskripsi</th>
				<th style="width:7%; text-align:center;">Tgl Selesai</th>
				<th style="width:7%; text-align:center;">No Terbit</th>
				<th style="width:7%; text-align:center;">Note</th>
				<th style="width:7%; text-align:center;">Status</th>
				<th style="width:7%; text-align:center;">UpdatedAt</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$no = 1;
			foreach ($woitems as $woitem) : ?>
				<tr>
					<td><?php echo $no; ?></td>
					<td>
						<?php
						$btns = [];
						$btns[] = get_btn(array('title' => 'History Item', 'icon' => 'list', 'onclick' => 'historywoitem_table(\'' . $woitem['woitem_kd'] . '\')'));
					
						/** Check button status */
						if($woitem['woitem_prosesstatus'] == 'workorder'){
							$btns[] = get_btn(array('title' => 'Finish', 'icon' => 'check', 'onclick' => 'ubah_status(\'' . $woitem['woitem_kd'] . '\' , \'finish\')'));	
							$btns[] = get_btn(array('title' => 'Cancel', 'icon' => 'close', 'onclick' => 'ubah_status(\'' . $woitem['woitem_kd'] . '\' , \'cancel\')'));		
						}else{
							$btns[] = "";
						}

						$btn_group = group_btns($btns);

						echo $btn_group;
						?>
					</td>
					<td><?php echo $woitem['woitem_no_wo']; ?></td>
					<td><?php echo $woitem['mrc_no']; ?></td>
					<td><?php echo $woitem['woitem_itemcode']; ?></td>
					<td><?php echo $woitem['woitem_status']; ?></td>
					<td class="dt-right"><?php echo $woitem['woitem_qty']; ?></td>
					<td><?php echo $woitem['woitem_deskripsi'] . $woitem['woitem_dimensi']; ?></td>
					<td><?php echo $woitem['woitem_tglselesai']; ?></td>
					<td><?php echo $woitem['wo_noterbit']; ?></td>
					<td><?php echo $woitem['woitem_note']; ?></td>
					<td class="dt-center">
						<?php
						$woitemrd = process_status($woitem['woitem_prosesstatus']);
						$color = 'yellow';
						switch ($woitem['woitem_prosesstatus']) {
							case 'workorder':
								$color = 'yellow';
								break;
							case 'finish':
								$color = 'green';
								break;
							default:
								$color = 'yellow';
						}

						echo bg_label($woitemrd, $color); ?>
					</td>
					<td><?php echo $woitem['woitem_tgledit']; ?></td>
				</tr>
			<?php
				$no++;
			endforeach; ?>
		</tbody>
	</table>
</div>

<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"paging": false,
			"language": {
				"lengthMenu": "Tampilkan _MENU_ data",
				"zeroRecords": "Maaf tidak ada data yang ditampilkan",
				"info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty": "Tidak ada data yang ditampilkan",
				"search": "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing": "Sedang Memproses...",
				"paginate": {
					"first": '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last": '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next": '<span class="glyphicon glyphicon-forward"></span>',
					"previous": '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"order": [0, 'asc'],
		});
	}
</script>