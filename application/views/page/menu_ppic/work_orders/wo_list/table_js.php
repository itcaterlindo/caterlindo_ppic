<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	table_main('<?php echo $year; ?>', '<?php echo $month; ?>', '<?php echo $woitem_prosesstatus; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('form_add_master', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function table_main(Y, m, woitem_prosesstatus) {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/table_main'; ?>',
				data: {
					Y: Y,
					m: m,
					woitem_prosesstatus: woitem_prosesstatus
				},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').slideDown().html(html);
					moveTo('idMainContent');
				}
			});
		});
	}

	function historywoitem_table(woitem_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/historywoitem_table'; ?>',
			data: {
				woitem_kd:woitem_kd
			},
			success: function(html) {
				toggle_modal("History WO", html)
			}
		});
	}

	function ubah_status(woitem_kd, status){
		if( confirm("Apakah anda yakin ingin mengubah status tersebut ?") ){
			var url = '<?php echo base_url() . $class_link . '/ubah_status'; ?>';
			$.ajax({
				url: url,
				type: "GET",
				data: {woitem_kd: woitem_kd, status: status},
				success: function(data) {
					
					var resp = JSON.parse(data);
					console.log(resp)
					if (resp.code == 200) {
						notify(resp.status, resp.pesan, 'success');
						table_main('<?php echo $year; ?>', '<?php echo $month; ?>', '<?php echo $woitem_prosesstatus; ?>');
					}else if (resp.code == 400) {
						notify(resp.status, resp.pesan, 'error');
						generateToken(resp.csrf);
					} else {
						notify('Error', 'Error tidak Diketahui', 'error');
						generateToken(resp.csrf);
					}
			}})
		}
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function render_select2(element) {
		$(element).select2({
			theme: 'bootstrap',
			width: '100%',
			placeholder: '-- Pilih Opsi --',
		});
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			delay: 2500,
			styling: 'bootstrap3'
		});
	}
</script>