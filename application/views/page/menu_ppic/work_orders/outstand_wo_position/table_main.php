<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}
</style>
<div class="box-body table-responsive no-padding">
	<table id="idTable" class="table table-bordered display" style="width:100%;">
		<thead>
			<tr>
				<th rowspan="2" style="width:1%; text-align:center; vertical-align: middle;">No.</th>
				<th rowspan="2" style="width:7%; text-align:center; vertical-align: middle;">No WO</th>
				<th rowspan="2" style="width:7%; text-align:center; vertical-align: middle;">Item Code</th>
				<th rowspan="2" style="width:7%; text-align:center; vertical-align: middle;">Item Status</th>
				<th rowspan="2" style="width:7%; text-align:center; vertical-align: middle;">Qty</th>
				<th rowspan="2" style="width:7%; text-align:center; vertical-align: middle;">Deskripsi</th>
				<th rowspan="2" style="width:7%; text-align:center; vertical-align: middle;">Note</th>
				<th style="width:7%; text-align:center; vertical-align: middle;" colspan="<?= count($bagian) ?>">BAGIAN</th>
				<th rowspan="2" style="width:7%; text-align:center; vertical-align: middle;">PYR</th>
				<th rowspan="2" style="width:7%; text-align:center; vertical-align: middle;">Outstanding</th>
			</tr>
			<tr>
				<?php foreach($bagian as $key => $val): ?>
					<th style="width:7%; text-align:center; vertical-align: middle;" ><?= $val['bagian_nama'] ?></th>
				<?php endforeach; ?>
			</tr>
			
		</thead>
		<tbody>
			<?php
			$no = 1;
			foreach ($master_recap as $key => $val) : ?>
				<tr class="<?= $val['header']['qty_outstanding'] > 0 ? "bg-danger" : "" ?>">
					<td><?= $no ?></td>
					<td><?= $val['header']['woitem_no_wo'] ?></td>
					<td><?= $val['header']['woitem_itemcode'] ?></td>
					<td><?= $val['header']['woitem_status'] ?></td>
					<td><?= $val['header']['woitem_qty'] ?></td>
					<td><?= $val['header']['woitem_deskripsi'] ?></td>
					<td><?= $val['header']['woitem_note'] ?></td>
					<?php foreach($bagian as $keyBagian => $valBagian): ?>
						<td><?= !empty($val['qty_bagian'][$valBagian['bagian_nama']]) ? $val['qty_bagian'][$valBagian['bagian_nama']] : 0; ?></td>
					<?php endforeach; ?>
					<td><?= $val['header']['qty_pyr'] ?></td>
					<td><?= $val['header']['qty_outstanding'] ?></td>
				</tr>
			<?php
				$no++;
			endforeach; ?>
		</tbody>
	</table>
</div>

<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"scrollX": true,
			"paging": false,
			"ordering": false,
			"scrollX": true,
			"scrollY": "500px",
			"info": false,
			"language": {
				"lengthMenu": "Tampilkan _MENU_ data",
				"zeroRecords": "Maaf tidak ada data yang ditampilkan",
				"info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty": "Tidak ada data yang ditampilkan",
				"search": "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing": "Sedang Memproses...",
				"paginate": {
					"first": '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last": '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next": '<span class="glyphicon glyphicon-forward"></span>',
					"previous": '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"order": [0, 'asc'],
			"buttons": [{
				"extend": "excelHtml5",
				"title": "Work Order Outstanding Position",
				"exportOptions": {
					"columns": [':visible']
				}
			}],
		});
	}
</script>