<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }

    tr.dt-red {
        background-color: red;
        color: white;
    }
</style>
<div class="col-md-12">
    <?php
    $aGroupPlanned = [];
    $aGroupPlanning = [];
    foreach ($stokPlanned as $eachPlanned0) {
        $aGroupPlanned[$eachPlanned0['kd_msalesorder'] . '#' . $eachPlanned0['wo_noterbit'] . '#' . $eachPlanned0['no_salesorder'] . '#' . $eachPlanned0['tipe_customer'] . '#' . $eachPlanned0['no_po']][] = $eachPlanned0;
    }
    foreach ($stokPlanning as $eachPlanning0) {
        $aGroupPlanning[$eachPlanning0['kd_msalesorder'] . '#' . $eachPlanning0['wo_noterbit'] . '#' . $eachPlanning0['no_salesorder'] . '#' . $eachPlanning0['tipe_customer'] . '#' . $eachPlanning0['no_po']][] = $eachPlanning0;
    }
    /** Item yang akan di proses wo */
    $woPlanning_kd_barangs = array_column($woPlanning, 'kd_barang');
    ?>
    <table id="idtableWOitemPlanning" class="table table-bordered display responsive classtableWOitem" style="font-size:95%;">
        <thead>
            <tr>
                <th style="text-align: center; width: 1%;">No.</th>
                <th style="text-align: center; width: 7%;">Item Code</th>
                <th style="text-align: center; width: 10%;">Item Desc</th>
                <th style="text-align: center; width: 2%;">Qty FG</th>
                <th style="text-align: center; width: 2%;">Qty WIP</th>
                <!-- <th style="text-align: center; width: 2%;">Qty WO</th> -->
                <th style="text-align: center; width: 1%;">Qty Total</th>
                <?php
                foreach ($aGroupPlanned as $eachGroupPlanned => $element) {
                    $expEach = explode('#', $eachGroupPlanned);
                    $no_salesorder = $expEach[2];
                    if ($expEach[3] == 'Ekspor') {
                        if($expEach[1]){
                            $no_salesorder = $expEach[4] ?  $expEach[4] : $expEach[2];
                        }else {
                           // $no_salesorder = $expEach[4] ?  $expEach[4] : $expEach[2];
                            $no_salesorder = $expEach[4] ?  $expEach[2] ." - ". $expEach[4] : $expEach[2];
                        }
                       
                    }
                    $thRow = '<th style="text-align: center; width: 1%;">' . $expEach[1] . '<br>' . $no_salesorder . '</th>';
                    $thRow .= '<th style="text-align: center; width: 1%;">Sisa</th>';
                    echo $thRow;
                }
                ?>
                <th style="text-align: center; width: 1%; background-color: grey; color:black;"></th>
                <?php
                foreach ($aGroupPlanning as $eachGroupPlanning => $elementPlanning) {
                    $expEach = explode('#', $eachGroupPlanning);
                    $no_salesorder = $expEach[2];
                    if ($expEach[3] == 'Ekspor') {
                        $no_salesorder = $expEach[4];
                    }
                    $thRow = '<th style="text-align: center; width: 1%;">' . $expEach[1] . '<br>' . $no_salesorder . '</th>';
                    $thRow .= '<th style="text-align: center; width: 1%;">Sisa</th>';
                    echo $thRow;
                }
                ?>
                <th style="text-align: center; width: 1%;">Stock <br> Status</th>
                <th style="text-align: center; width: 1%;">Stock <br> Min</th>
                <th style="text-align: center; width: 1%;">Stock <br> Max</th>

            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            $msg = '';
            foreach ($stokFG as $eachFG) :
                $wip_qty = 0;
                $qtyTotal = 0;
                $selisih = 0;
                $qty_pln = 0;
                
                /** Stok WIP */
                foreach ($stokWIP as $eachWIP) :
                    if ($eachWIP['kd_barang'] == $eachFG['kd_barang']) {
                        $wip_qty = $eachWIP['wip_qty'];
                    }
                endforeach;
                $qtyTotal = $eachFG['fg_qty'] + $wip_qty;
                /** Wo Planning */
                // $qtywoplanning = 0;
                // if (!empty($woPlanning)) {
                //     foreach ($woPlanning as $eachWoPlanning) {
                //         if ($eachWoPlanning['kd_barang'] == $eachFG['kd_barang']) {
                //             $qtywoplanning = $eachWoPlanning['qtywoplanning'];
                //         }
                //     }
                //     $qtyTotal = $qtyTotal + $qtywoplanning;
                // }

                $tampil = '<tr>';
                $tampil .= '<td class="dt-center">' . $no . '</td>';
                $tampil .= '<td>' . $eachFG['item_code'] . '</td>';
                $tampil .= '<td>' . $eachFG['deskripsi_barang'] . '/' . $eachFG['dimensi_barang'] . '</td>';
                $tampil .= '<td class="dt-right">' . $eachFG['fg_qty'] . '</td>';
                $tampil .= '<td class="dt-right">' . $wip_qty . '</td>';
                // $tampil .= '<td class="dt-right" style="background-color: orange; color:black;">' . $qtywoplanning . '</td>';
                $tampil .= '<td class="dt-right" style="background-color: aqua; color:black;">' . $qtyTotal . '</td>';
                /** planned */
                $sisa = $qtyTotal;
                $color = 'green';
                foreach ($aGroupPlanned as $eachGroupPlanned2 => $element2) {
                    $expEach = explode('#', $eachGroupPlanned2);
                    $qtyplanned = 0;
                    $trRow = '<td style="text-align: center; width: 1%;">' . $qtyplanned . '</td>';
                    $trRow .= '<td style="text-align: center; width: 1%; background-color: ' . $color . '; color:white;">' . $sisa . '</td>';
                    foreach ($element2 as $eachElement2) {
                        if ($eachElement2['kd_msalesorder'] == $expEach[0] && $eachElement2['kd_barang'] == $eachFG['kd_barang']) {
                            $qtyplanned = $eachElement2['qtyplanned'];
                            $sisa = $sisa - $qtyplanned;
                            $color = 'green';
                            $msg = '';
                            $barang_stock_min = !empty($eachFG['barang_stock_min']) ? $eachFG['barang_stock_min'] : 0;
                            $barang_stock_max = !empty($eachFG['barang_stock_max']) ? $eachFG['barang_stock_max'] : 0;
                            if ($sisa < $barang_stock_min) {
                                $color = 'red';
                                $msg = 'kurang';
                            } elseif ($sisa > $barang_stock_max) {
                                $color = 'red';
                                $msg = 'lebih';
                            } else {
                                $colorPlanning = 'green';
                                $msg = '';
                            }
                            $trRow = '<td style="text-align: center; width: 1%;">' . $qtyplanned . '</td>';
                            $trRow .= '<td style="text-align: center; width: 1%; background-color: ' . $color . '; color:white;">' . $sisa  . '</td>';
                            $qty_pln += $qtyplanned;
                        }
                    }
                    $tampil .= $trRow;
                }
                $tampil .= '<td style="background-color: grey; color:black;"></td>';
                /** Planning */
                foreach ($aGroupPlanning as $eachGroupPlanning2 => $elementPlanning2) {
                    $expEach = explode('#', $eachGroupPlanning2);
                    $qtyplanning = 0;
                    $colorPlanning = $color;
                    $trRowPlanning = '<td style="text-align: center; width: 1%;">' . $qtyplanning . '</td>';
                    $trRowPlanning .= '<td style="text-align: center; width: 1%; background-color: ' . $colorPlanning . '; color:white;">' . $sisa . '</td>';
                    foreach ($elementPlanning2 as $eachElementPlanning2) {
                        if ($eachElementPlanning2['kd_msalesorder'] == $expEach[0] && $eachElementPlanning2['kd_barang'] == $eachFG['kd_barang']) {
                            $qtyplanning = $eachElementPlanning2['qtyplanning'];
                            $sisa = $sisa - $qtyplanning;
                            $msg = '';
                            $barang_stock_min = !empty($eachFG['barang_stock_min']) ? $eachFG['barang_stock_min'] : 0;
                            $barang_stock_max = !empty($eachFG['barang_stock_max']) ? $eachFG['barang_stock_max'] : 0;
                            if ($sisa < $barang_stock_min) {
                                $colorPlanning = 'red';
                                $msg = 'kurang';
                            } elseif ($sisa > $barang_stock_max) {
                                $colorPlanning = 'red';
                                $msg = 'lebih'; 
                            } else {
                                $colorPlanning = 'green';
                                $msg = '';
                            }
                            $trRowPlanning = '<td style="text-align: center; width: 1%;">' . $qtyplanning . '</td>';
                            $trRowPlanning .= '<td style="text-align: center; width: 1%; background-color: ' . $colorPlanning . '; color:white;">' . $sisa . '</td>';
                        }
                    }
                    $tampil .= $trRowPlanning;
                }
                $tampil .= '<td style="text-align: center; width: 1%;">' . $msg . '</td>';
                $tampil .= '<td style="text-align: center; width: 1%;">' . $eachFG['barang_stock_min'] . '</td>';
                $tampil .= '<td style="text-align: center; width: 1%;">' . $eachFG['barang_stock_max'] . '</td>';

                $tampil .= '</tr>';
                // if ($qtyTotal == $sisa && (in_array($eachFG['kd_barang'], $woPlanning_kd_barangs) == false)) {
                     //continue;
                // } else {
                     $no++;
                     $pez = $eachFG['fg_qty'] + $qty_pln + $qty_pln;
                     if($pez != 0){
                         echo $tampil;
                     }
                   
                // }
            endforeach;
            ?>
        </tbody>
    </table>
</div>



<script type="text/javascript">
    render_dt('#idtableWOitemPlanning');

    function render_dt(table_elem) {
        $(table_elem).DataTable({
            "dom": 'Bfrtip',
            "responsive": false,
            "paging": false,
            "scrollX": true,
            "scrollY": "500px",
            "language": {
                "lengthMenu": "Tampilkan _MENU_ data",
                "zeroRecords": "Maaf tidak ada data yang ditampilkan",
                "info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
                "infoFiltered": "(difilter dari _MAX_ total data)",
                "infoEmpty": "Tidak ada data yang ditampilkan",
                "search": "Cari :",
                "loadingRecords": "Memuat Data...",
                "processing": "Sedang Memproses...",
                "paginate": {
                    "first": '<span class="glyphicon glyphicon-fast-backward"></span>',
                    "last": '<span class="glyphicon glyphicon-fast-forward"></span>',
                    "next": '<span class="glyphicon glyphicon-forward"></span>',
                    "previous": '<span class="glyphicon glyphicon-backward"></span>'
                }
            },
            "fixedColumns": {
                "leftColumns": 6
            },
            "buttons": [{
                "extend": "excelHtml5",
                "title": "Work Order Planning",
                "exportOptions": {
                    "columns": [':visible']
                }
            }],
        });
    }
</script>