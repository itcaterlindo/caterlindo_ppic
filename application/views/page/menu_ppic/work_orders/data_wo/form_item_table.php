<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
    tr.dt-red {background-color: red; color:white; }
</style>

<?php 
    $groups = [];
    $arrayCustom = [];
    $groupResult = [];
    foreach ($woitemsos as $woitemso0) {
        if ($woitemso0['woitemso_status'] == 'std') {
                $key = $woitemso0['kd_barang'];
                /** jika tidak ada key */
                if (!array_key_exists($key, $groups)) {
                    $groups[$key] = array(
                        'kd_barang' => $key,
                        'woitemso_itemcode' => $woitemso0['woitemso_itemcode'],
                        'woitemso_itemdeskripsi' => $woitemso0['woitemso_itemdeskripsi'],
                        'woitemso_itemdimensi' => $woitemso0['woitemso_itemdimensi'],
                        'woitemso_qty' => $woitemso0['woitemso_qty'],
                        'woitemso_status' => $woitemso0['woitemso_status'],
                        'bom_kd' => $woitemso0['bom_kd'],
                        'item_code' => $woitemso0['item_code'],
                        'woitemso_nopj' => null,
                    );
                } else {
                    $groups[$key]['woitemso_qty'] = $groups[$key]['woitemso_qty'] + $woitemso0['woitemso_qty'];
                }
        }else {
            /** Custom */
            $arrayCustom[] = array(
                'kd_barang' => $woitemso0['kd_barang'],
                'woitemso_itemcode' => $woitemso0['woitemso_itemcode'],
                'woitemso_itemdeskripsi' => $woitemso0['woitemso_itemdeskripsi'],
                'woitemso_itemdimensi' => $woitemso0['woitemso_itemdimensi'],
                'woitemso_qty' => $woitemso0['woitemso_qty'],
                'woitemso_status' => $woitemso0['woitemso_status'],
                'bom_kd' => $woitemso0['bom_kd'],
                'item_code' => $woitemso0['item_code'],
                'woitemso_nopj' => $woitemso0['woitemso_nopj'],
            );
        }
    }

    foreach ($groups as $each){
        $groupResult[] = $each;
    }
    $arrayAfterGroup = array_merge($groupResult, $arrayCustom);
?>

<div class="col-md-12">
    <!-- <div class="box-body table-responsive no-padding"> -->
    <table id="idtableWOitem" class="table table-bordered display responsive idtableWOitem" style="font-size:95%;">
        <thead>
            <tr>
                <th style="text-align: center; width: 1%;">No.</th>
                <th style="text-align: center; width: 5%;">Item Code</th>
                <th style="text-align: center; width: 2%;">Item<br>Status</th>
                <th style="text-align: center; width: 10%;">Deskripsi</th>
                <th style="text-align: center; width: 1%;">Qty WO</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $no = 1;
            foreach ($arrayAfterGroup as $woitemso) :
            ?>
                <tr>
                    <td class="dt-center"><?=$no;?></td>
                    <td><?= $woitemso['woitemso_itemcode'];?></td>
                    <td><?= $woitemso['woitemso_status'];?></td>
                    <td><?= $woitemso['woitemso_itemdeskripsi'].'/'.$woitemso['woitemso_itemdimensi'];?></td>
                    <td class="dt-right"><?= $woitemso['woitemso_qty'];?></td>
                </tr>
            <?php
                $no++;
            endforeach;
            ?>
        </tbody>
    </table>
    <!-- </div> -->
</div>



<script type="text/javascript">
    render_dt('#idtableWOitem');

    function render_dt(table_elem) {
		$(table_elem).DataTable({
			"autoWidth": true,
            "responsive": true,
            "paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
		});
	}
</script>