<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form('<?php echo $url; ?>');
    first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

    $(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('form_add_master', '');
	});

    $(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		open_table();
        $('#<?php echo $btn_add_id; ?>').slideDown();
	});

	function open_form(url) {
        $('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id;?>').hide( function() {
			$.ajax({
				type: 'GET',
				url: url,
				success:function(html){
					$('#<?php echo $box_content_id;?>').show().html(html);
					render_select2('.select2');
			        moveTo('idMainContent');
				}
			});
		});
	}

	function proses_woitem (wo_kd, woitem_kd) {
		event.preventDefault();
		var conf = confirm('Apakah kamu yakin ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link?>/action_process_woitem_custom',
				data: {wo_kd:wo_kd, woitem_kd:woitem_kd},
				success:function(resp){
					console.log(resp);
					if (resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						open_form_table_item (wo_kd);
					}
				}
			});
		}
	}

	function submitFormMaster() {
		$('#idbtnsubmitMaster').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtnsubmitMaster').attr('disabled', true);

		event.preventDefault();
		var form = document.getElementById('idFormMaster');
		
		$.ajax({
			url: "<?php echo base_url().$class_link.'/action_insert_master'; ?>" ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				console.log(resp);
				if(resp.code == 200){
					window.location.assign('<?php echo base_url().$class_link?>/form_item_step0_box?wo_kd='+resp.wo_kd);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

	
</script>