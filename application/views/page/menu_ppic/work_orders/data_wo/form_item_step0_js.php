<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
    form_item_step0_tablemain('<?php echo $wo_kd; ?>');
    first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

    function form_item_step0_tablemain(wo_kd) {
        $('#<?php echo $btn_add_id; ?>').slideDown();
        $('#<?php echo $box_content_id; ?>').slideUp(function() {
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url() . $class_link . '/form_item_step0_tablemain'; ?>',
                data: {
                    wo_kd: wo_kd
                },
                success: function(html) {
                    $('#<?php echo $box_content_id; ?>').slideDown().html(html);
                    moveTo('idMainContent');
                }
            });
        });
    }

    function itemadditionalso_table(wo_kd) {
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url() . $class_link . '/itemadditionalso_table'; ?>',
            data: {
                wo_kd: wo_kd
            },
            success: function(html) {
                toggle_modal('Additional SO', html);
            }
        });
    }

    function itemunproseswo_table(wo_kd) {
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url() . $class_link . '/itemunproseswo_table'; ?>',
            data: {
                wo_kd: wo_kd
            },
            success: function(html) {
                toggle_modal('Unprocess WO', html);
            }
        });
    }

    function toggle_modal(modalTitle, htmlContent) {
        $('#idmodal').modal('toggle');
        $('.modal-title').text(modalTitle);
        $('#<?php echo $content_modal_id; ?>').slideUp();
        $('#<?php echo $content_modal_id; ?>').html(htmlContent);
        $('#<?php echo $content_modal_id; ?>').slideDown();
    }

    function process_step0(params) {
        var conf = confirm('Apakah anda yakin ?');
        if (conf) {
            let wo_kd = $(params).attr("data-wo_kd");
            let csrf = $(params).attr("data-csrf");

            box_overlay_formitem('in');
            var url = '<?php echo base_url() . $class_link . '/action_process_wo_step0'; ?>';
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    '<?php echo $this->security->get_csrf_token_name(); ?>': csrf,
                    'wo_kd': wo_kd
                },
                success: function(data) {
                    var resp = JSON.parse(data);
                    if (resp.code == 200) {
                        notify(resp.status, resp.pesan, 'success');
                        window.location.assign("<?php echo base_url() . $class_link; ?>/form_item_step1_box?wo_kd=" + wo_kd);
                    } else {
                        notify('Error', 'Gagal Proses', 'error');
                        generateToken(resp.csrf);
                    }
                    box_overlay_formitem('out');
                }
            });
        }
    }

    function submitDataAdditionalSO() {
        event.preventDefault();
        var form = document.getElementById('idFormaddSO');
        var url = '<?php echo base_url() . $class_link . '/action_insert_itemadditionalso'; ?>';

        $.ajax({
            url: url,
            type: "POST",
            data: new FormData(form),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                var resp = JSON.parse(data);
                console.log(resp);
                if (resp.code == 200) {
                    window.location.reload();
                    notify(resp.status, resp.pesan, 'success');
                } else if (resp.code == 401) {
                    $.each(resp.pesan, function(key, value) {
                        $('#' + key).html(value);
                    });
                    generateToken(resp.csrf);
                } else if (resp.code == 400) {
                    notify(resp.status, resp.pesan, 'error');
                    generateToken(resp.csrf);
                } else {
                    notify('Error', 'Error tidak Diketahui', 'error');
                    generateToken(resp.csrf);
                }
            }
        });
    }

    function generateToken(csrf) {
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
    }

    function first_load(loader, content) {
        $('#' + loader).fadeOut(500, function(e) {
            $('#' + content).slideDown();
        });
    }

    function moveTo(div_id) {
        $('html, body').animate({
            scrollTop: $('#' + div_id).offset().top - $('header').height()
        }, 1000);
    }

    function box_overlay_formitem(sts) {
        if (sts == 'in') {
            $('#<?php echo $box_overlay_id; ?>').fadeIn();
        } else if (sts == 'out') {
            $('#<?php echo $box_overlay_id; ?>').fadeOut();
        }
    }

    function render_dt(table_elem) {
        $(table_elem).DataTable({
            "responsive": false,
            "paging": false,
            "language": {
                "lengthMenu": "Tampilkan _MENU_ data",
                "zeroRecords": "Maaf tidak ada data yang ditampilkan",
                "info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
                "infoFiltered": "(difilter dari _MAX_ total data)",
                "infoEmpty": "Tidak ada data yang ditampilkan",
                "search": "Cari :",
                "loadingRecords": "Memuat Data...",
                "processing": "Sedang Memproses...",
                "paginate": {
                    "first": '<span class="glyphicon glyphicon-fast-backward"></span>',
                    "last": '<span class="glyphicon glyphicon-fast-forward"></span>',
                    "next": '<span class="glyphicon glyphicon-forward"></span>',
                    "previous": '<span class="glyphicon glyphicon-backward"></span>'
                }
            },
        });
    }

    function render_select2(element) {
        $(element).select2({
            theme: 'bootstrap',
            width: '100%',
            placeholder: '-- Pilih Opsi --',
        });
    }

    function notify(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            type: type,
            delay: 2500,
            styling: 'bootstrap3'
        });
    }
</script>