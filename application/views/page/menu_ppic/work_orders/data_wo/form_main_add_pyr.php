<?php
defined('BASEPATH') or exit('No direct script access allowed');
$form_id = 'idFormAddWOitemdetailPYR';

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtslugWOitemdetail', 'name' => 'txtslugWOitemdetail', 'value' => $slug));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtwo_kd', 'name' => 'txtwo_kd'));
echo form_input(array('type' => 'hidden', 'id' => 'idwoitem_kd', 'name' => 'woitem_kd'));
echo form_input(array('type' => 'hidden', 'id' => 'idwoitem_qty', 'name' => 'woitem_qty'));
echo form_input(array('type' => 'hidden', 'id' => 'idwoitem_status', 'name' => 'woitem_status'));
echo form_input(array('type' => 'hidden', 'id' => 'idinspectionproduct_kd', 'name' => 'inspectionproduct_kd'));
echo form_input(array('type' => 'hidden', 'id' => 'idjenis_item', 'name' => 'jenis_item'));
?>

<div class="col-md-6">

    <div class="form-group">
        <label for="idRefference" class="col-md-2 control-label">Refference by </label>
        <div class="col-md-4">
            <div id="idErrjenis"></div>
            <select name="txtRefference" id="idTxtRefference" class="form-control">
                <option value="inspection">Inspection</option>
                <!-- <option value="wo">WO</option> -->
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="idwoitem_no_wo" class="col-md-2 control-label">No. WO / Inspection</label>
        <div class="col-md-6">
            <div id="idErrwoitem_no_wo"></div>
            <select name="woitem_no_wo" id="idwoitem_no_wo" class="form-control select2" data-allow-clear="true"></select>
        </div>
    </div>

    <div id="div-inspection-rawmaterial" hidden>
        <div class="form-group">
            <label for="idInspectionRawMaterial" class="col-md-2 control-label">Inspection Rawmaterial</label>
            <div class="col-md-4">
                <div id="idErrInspectionRawMaterial"></div>
                <select name="txtInspectionRawMaterial" id="idTxtInspectionRawMaterial" class="form-control select2" data-allow-clear="true"></select>
            </div>
        </div>
    </div>

    <table border="1" style="width: 100%;">
        <tr>
            <td width="15%" style="font-weight: bold;">ItemCode</td>
            <td width="35%">
                <div id="idErrwoitem_itemcode"></div>
                <div id="idwoitem_itemcode" style="padding-top:6px;">-</div>
            </td>
            <td width="15%" style="font-weight: bold;">Jenis</td>
            <td width="35%"><div id="idwoitem_jenis" style="padding-top:6px;">-</div></td>
        </tr>
        <tr>
            <td style="font-weight: bold;">Qty</td>
            <td><div id="idtxtwoitem_qty" style="padding-top:6px;">-</div></td>
            <td style="font-weight: bold;">Status</td>
            <td><div id="idtxtwoitem_status" style="padding-top:6px;">-</div></td>
        </tr>
        <tr>
            <td style="font-weight: bold;">Tgl Selesai</td>
            <td><div id="idwoitem_tglselesai" style="padding-top:6px;">-</div></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <br>
    <table border="1" style="width: 100%;">
        <tr>
            <td style="font-weight: bold;">Part :</td>
        </tr>
        <tr>
            <td> <div id="idpartdetailwo"></div> </td>
        </tr>
    </table>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label for="idqty_pyr" class="col-md-2 control-label">Qty PYR</label>
        <div class="col-md-3">
            <div id="idErrqty_pyr"></div>
            <input type="number" name="qty_pyr" id="idqty_pyr" class="form-control tt-input" placeholder="Qty PYR" readonly>
        </div>
        <label for="idtxtwoitem_satuan" class="col-md-2 control-label">Satuan</label>
        <div class="col-md-3">
            <?php echo form_dropdown('txtwoitem_satuan', isset($opsiSatuan) ? $opsiSatuan : array(), isset($satuanSelected) ? $satuanSelected : null, array('id' => 'idtxtwoitem_satuan', 'class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <label for="idtxtwoitem_tglselesai" class="col-md-2 control-label">Tgl Selesai</label>
        <div class="col-md-4">
            <div id="idErrwoitemdetail_tglselesai"></div>
            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="text" name="txtwoitem_tglselesai" id="idtxtwoitem_tglselesai" class="form-control datetimepicker tt-input" placeholder="Tgl Selesai" value="<?= isset($woitem_tglselesai) ? $woitem_tglselesai : null; ?>">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="idtxtwoitem_note" class="col-md-2 control-label">Keterangan</label>
        <div class="col-md-9">
            <div id="idErrwoitemdetail_note"></div>
            <textarea name="txtwoitem_note" cols="40" rows="3" id="idtxtwoitem_note" class="form-control  tt-input" placeholder="Keterangan"><?php echo isset($woitem_note) ? $woitem_note : 'PYR'; ?></textarea>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-4 col-sm-offset-9 col-xs-12">
        <button type="button" name="btnSubmit" id="idbtnSubmitMain" class="btn btn-sm btn-primary">
            <i class="fa fa-save"></i> Simpan
        </button>
        <button type="reset" name="btnReset" onclick="resetForm()" class="btn btn-sm btn-default">
            <i class="fa fa-refresh"></i> Reset
        </button>
    </div>
</div>
<hr>

<!-- /.box-body -->
<?php echo form_close(); ?>

<!-- Table Data PYR -->
<div class="row">   
    <div class="col-md-12">
        <button type="button" id="btnRefresh" class="btn btn-sm btn-warning" onclick="refreshTable()">
            <i class="fa fa-refresh"></i> Refresh
        </button>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <table class="table" id="tblPyr">
            <thead>
            <tr>
                <th>No.</th>
                <th>#</th>
                <th>Part Code</th>
                <th>Status</th>
                <th>Deskripsi</th>
                <th>Qty</th>
                <th>Satuan</th>
                <th>Keterangan</th>
                <th>Tgl Selesai</th>
                <th>Status</th>
                <th>WO</th>
                <th>WO Reject / Inspection</th>
                <th>No. Terbit</th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
        </table>
    </div>
</div>


<script type="text/javascript">

    /** INIT TABLE */
    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};
	var table = $('#tblPyr').DataTable({
		"processing": true,
		// "serverSide": true,
		"ordering" : false,
		"ajax": "<?php echo base_url().$class_link.'/table_data_pyr'; ?>",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"columnDefs": [
			{"data": null, "searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
			{"searchable": false, "orderable": false, "targets": 1},
			{"className": "dt-center", "targets": 1},
		],
		// "order": [ [13, 'asc'], [12, 'desc'] ],
		"rowCallback": function (row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			var index = page * length + (iDisplayIndex + 1);
			$('td:eq(0)', row).html(index);
		}
	});
    /** END INIT TABLE */

    $( document ).ready(function() {
        render_datetimepicker('.datetimepicker')
        render_wo_inspection();
    });
    
  
    $('#idwoitem_no_wo').on("select2:selecting", function(e) { 
        let dt = e.params.args.data;
        $('#idwoitem_itemcode').text(dt.woitem_itemcode);
        $('#idtxtwoitem_qty').text(dt.woitem_qty);
        $('#idtxtwoitem_status').text(dt.woitem_status.toUpperCase());
        $('#idwoitem_tglselesai').text(dt.woitem_tglselesai);
        $('#idwoitem_jenis').text(dt.woitem_jenis.toUpperCase());

        $('#idtxtwo_kd').val(dt.wo_kd);
        $('#idwoitem_kd').val(dt.woitem_kd);
        $('#idwoitem_qty').val(dt.woitem_qty);
        if(dt.jenis_item == 'product'){
            $('#idqty_pyr').val(dt.woitem_qty);
        }else{
            $('#idqty_pyr').val(0);
        }
        $('#idwoitem_status').val(dt.woitem_status);
        $('#idinspectionproduct_kd').val(dt.kd_inspectionproduct);
        $('#idjenis_item').val(dt.jenis_item);
        $('#idtxtwoitem_tglselesai').focus();

        if(dt.jenis_item == 'rawmaterial'){
            /** Jika WO yang dipilih adalah rawmaterial maka tampilkan inputan pilih inspection raw material */
            $('#div-inspection-rawmaterial').show();
            render_rawmaterial_inspection();
        }

        if (dt.woitem_jenis == 'packing') {
            // Get Detail Part
            getDetailPartWoitemPacking(dt.woitem_kd);
        }
    });

    $('#idTxtInspectionRawMaterial').on("select2:selecting", function(e) { 
        let dt = e.params.args.data;

        $('#idqty_pyr').val(dt.inspectionrawmaterial_qty_reject);
        $('#idinspectionproduct_kd').val(dt.id);
        $('#idtxtwoitem_tglselesai').focus();
    });

    $('#idwoitem_no_wo').on("select2:unselecting", function(e) { 
        resetForm();
    });

    $('#idTxtInspectionRawMaterial').on("select2:unselecting", function(e) { 
        $('#idqty_pyr').val(0);
        $('#idinspectionproduct_kd').val('');
    });

    function getDetailPartWoitemPacking(woitem_kd){
        $.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/get_item_woitemdetail_packing',
			data: {
				woitem_kd: woitem_kd
			},
			success: function(data) {
                var resp = JSON.parse(data);
                html = '';
                $.each( resp, function( key, value ) {
                    html += '<label><input type="checkbox" name="txtwoitemdetail_kds[]" value="'+value.woitemdetail_kd+'"> '+value.partmain_nama+' </label><br>';
                });
				$('#idpartdetailwo').html(html);
			}
		});
    }

    /** Javascript untuk form add PYR */
	$('#idbtnSubmitMain').on('click', function() {
		submitDataWOItemPyr();
    });
	/** End javascript untuk form add PYR */

    function submitDataWOItemPyr() {
		event.preventDefault();
        if( confirm("Apakah anda ingin menyimpan data tersebut?") ){
            var form = document.getElementById('idFormAddWOitemdetailPYR');
            var url = '<?php echo base_url() . $class_link . '/action_insert_woitempyr'; ?>';
            $.ajax({
            	url: url,
            	type: "POST",
            	data: new FormData(form),
            	contentType: false,
            	cache: false,
            	processData: false,
            	success: function(data) {
            		var resp = JSON.parse(data);
            		if (resp.code == 200) {
            			notify(resp.status, resp.pesan, 'success');
                        resetForm();
            			generateToken(resp.csrf);
                        refreshTable();
            		} else if (resp.code == 401) {
            			$.each(resp.pesan, function(key, value) {
            				$('#' + key).html(value);
            			});
            			generateToken(resp.csrf);
            		} else if (resp.code == 400) {
            			notify(resp.status, resp.pesan, 'error');
            			generateToken(resp.csrf);
            		} else {
            			notify('Error', 'Error tidak Diketahui', 'error');
            			generateToken(resp.csrf);
            		}
            	}
            });
        }
	}

    function cancel_wo(woitem_kd) {
        var conf = confirm('Apakah anda yakin ingin menghapus WO item PYR tersebut?');
		if (conf) {
			var url = '<?php echo base_url() . $class_link ?>/action_cancel_woitem';
			$.ajax({
				url: url,
				type: 'GET',
				data: 'id=' + woitem_kd,
				success: function(data) {
					var resp = JSON.parse(data);
					if (resp.code == 200) {
						notify(resp.status, resp.pesan, 'success');
                        refreshTable();
					} else {
						notify('Gagal', resp.pesan, 'error');
					}
				}
			});
		}
	}

    function render_datetimepicker(element){
		$(element).datetimepicker({
			format: 'YYYY-MM-DD',
    	});
	}

    function render_wo_inspection(){
		/** Render WO Select2 */
		$("#idwoitem_no_wo").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?= base_url().$class_link ?>/get_no_wo_inspection',
				type: "GET",
				dataType: 'JSON',
				delay: 250,
				data: function (params) {
					return {
						paramWO: params.term, // search term 
                        paramRefference: $('#idTxtRefference').val()
					};
				},
				processResults: function (response) {
                    return {
						results: response
					};
				},
				cache: true
			}
		});
	}

    function render_rawmaterial_inspection(){
		/** Render WO Select2 */
		$("#idTxtInspectionRawMaterial").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?= base_url().$class_link ?>/get_inspection_rawmaterial',
				type: "GET",
				dataType: 'JSON',
				delay: 250,
				data: function (params) {
					return {
						param: params.term, // search term 
                        paramRMKode: $('#idwoitem_itemcode').text()
					};
				},
				processResults: function (response) {
                    return {
						results: response
					};
				},
				cache: true
			}
		});
	}

    function refreshTable()
    {
        table.ajax.reload();
    }

    function resetForm()
    {
        
        $('#idFormAddWOitemdetailPYR').trigger("reset");
        $('#idwoitem_itemcode').text('-');
        $('#idtxtwoitem_qty').text('-');
        $('#idwoitem_itemcode').text('-');
        $('#idtxtwoitem_status').text('-');
        $('#idwoitem_tglselesai').text('-');
        $('#idwoitem_jenis').text('-');

        $('#idwoitem_no_wo').val('').change();
        $('#idTxtInspectionRawMaterial').val('').change();
        $('#idtxtwo_kd').val('');
        $('#idwoitem_kd').val('');
        $('#idwoitem_qty').val('');
        $('#idqty_pyr').val('');
        $('#idwoitem_status').val('');
        $('#idinspectionproduct_kd').val('');
        $('#idjenis_item').val('');
        $('#idqty_pyr').focus();
        $('#idpartdetailwo').html('');
        $('#div-inspection-rawmaterial').hide();
    }

    function item_type(type, slug) {
		if (type == 'custom') {
			$('#idtxtItem_desc').attr('readonly', false);
            $('#idtxtItemDimension').attr('readonly', false);
            $('#idtxtbarang_kd').val('PRD020817000573'); // kd_barang custom item
		}
		else if (type == 'std') {
            $('#idtxtItem_desc').attr('readonly', true);
            $('#idtxtItemDimension').attr('readonly', true);
            $('#idTxtProductCode').val(''); // kd_barang std
            $('#idtxtItem_desc').val(''); // kd_barang std
            $('#idtxtItemDimension').val(''); // kd_barang std
            $('#idtxtbarang_kd').val(''); // kd_barang std
		}
	}

    function edit_item(id) {
        var url = '<?php echo base_url() . $class_link; ?>/form_additem?source=edit_editItem&kd_ditem_wo=' + id;
        $('#idAdditem').load(url);
        moveTo('idMainContent');
    }

    function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
			delay: 2500,
            styling: 'bootstrap3'
        });
    }

	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

</script>