<?php
defined('BASEPATH') OR exit('No direct script access allowed');

echo form_open('', array('id' => 'idFormMaster', 'class' => 'form-horizontal'));
?>
<div class="box-body">
    <div class="form-group">
        <label class="col-md-1 control-label">No Sales Order</label>
        <div class="col-sm-9 col-xs-12">
        <?php echo form_multiselect('txtkd_msalesorder[]', isset($opsiMsalesorders) ? $opsiMsalesorders : array() , isset($msalesOrderSelected)? $msalesOrderSelected : '', array('class'=> 'form-control select2', 'id'=> 'idtxtkd_msalesorder', 'multiple' => 'multiple'));?>
        </div>
    </div>

    <div class="form-group">
		<label for="idtxtwo_note" class="col-md-1 control-label">Keterangan</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrwo_note"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtwo_note', 'id'=> 'idtxtwo_note', 'placeholder' =>'Keterangan', 'rows' => '2', 'value'=> isset($wo_note) ? $wo_note: null ));?>
		</div>
	</div>
</div>

<div class="box-footer">
    <button id="idbtnsubmitMaster" onclick="submitFormMaster()" type="submit" class="btn btn-primary btn-sm pull-right"><i class="fa fa-save"></i> Simpan</button>
</div>

<?php echo form_close(); ?>