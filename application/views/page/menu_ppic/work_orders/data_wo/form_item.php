<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$form_id = 'idFormAddWOitemso';
if (isset($rowData)){
    extract($rowData);
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtslugWOitem', 'name'=> 'txtslugWOitem', 'value' => $slug ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtwo_kd', 'name'=> 'txtwo_kd', 'value' => isset($wo_kd) ? $wo_kd: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtbarang_kd', 'name'=> 'txtbarang_kd', 'class' => 'tt-input' ,'value' => isset($kd_barang) ? $kd_barang: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtwoitemso_kd', 'name'=> 'txtwoitemso_kd', 'class' => 'tt-input' ,'value' => isset($woitemso_kd) ? $woitemso_kd: null ));
?>

<div class="col-md-6">
    <div class="form-group">
        <label for="idTxtProductCode" class="col-md-2 control-label">Product Code</label>
        <div class="col-md-6">
            <div class="errClass" id="idErrProductCode"></div>
            <div id="idTextDesc" style="padding-top:6px;"><?php echo isset($woitemso_itemcode) ? $woitemso_itemcode : '-';?></div>
            <input type="text" name="txtProductCode" id="idTxtProductCode" class="form-control" placeholder="Product Code" autocomplete="off" spellcheck="false" value="<?=isset($woitem_itemcode) ? $woitem_itemcode:null;?>" <?php if ($slug == 'edit') { echo 'style="display: none;"'; }?> >
        </div>
    </div>
    <?php if ($slug != 'edit') :?>
    <div class="form-group">
        <label for="idItemStatus" class="col-md-2 control-label">Status</label>
        <div class="col-md-4">
            <div class="errClass" id="idErrItemStatus"></div>
            <?php echo form_dropdown('txtItem_status', array('std' => 'Standard', 'custom' => 'Custom'), isset($item_status) ? $item_status:'std' , array('id' => 'idtxtItem_status', 'class' => 'form-control')); ?>
        </div>
    </div>
    <?php endif;?>
    <div class="form-group">
        <label for="idItemJenisProses" class="col-md-2 control-label">Jenis Proses</label>
        <div class="col-md-4">
            <div class="errClass" id="idErrItemJenisProses"></div>
            <?php echo form_dropdown('txtItem_jenis_proses', array('produksi' => 'Produksi', 'jasa' => 'Jasa'), isset($woitemso_jenis_proses) ? $woitemso_jenis_proses:'produksi' , array('id' => 'idtxtItem_jenis_proses', 'class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <label for="idTxtItemDesc" class="col-md-2 control-label">Description</label>
        <div class="col-md-9">
            <div class="errClass" id="idErrItemDesc"></div>
            <textarea name="txtItem_desc" cols="40" rows="3" id="idtxtItem_desc" class="form-control  tt-input" placeholder="Description" readonly="true" ><?php echo isset($woitemso_itemdeskripsi) ? $woitemso_itemdeskripsi:null;?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="idtxtItemDimension" class="col-md-2 control-label">Dimension</label>
        <div class="col-md-9">
            <textarea name="txtItemDimension" cols="40" rows="3" id="idtxtItemDimension" class="form-control  tt-input" placeholder="Dimension" readonly="true"><?php echo isset($woitemso_itemdimensi) ? $woitemso_itemdimensi:null;?></textarea>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label for="idbom_kd" class="col-md-2 control-label">BOM</label>
        <div class="col-md-9">
            <div id="idErrbom_kd"></div>
            <?php echo form_dropdown('txtbom_kd', isset($opsiboms) ? $opsiboms : array(), isset($bomSelected) ? $bomSelected: null , array('id' => 'idtxtbom_kd', 'class' => 'form-control select2 classBom_kd')); ?>
        </div>
    </div>
    <div class="form-group">
        <label for="idtxtwoitem_qtyso" class="col-md-2 control-label">Qty SO</label>
        <div class="col-sm-2 col-xs-12">
            <div id="idTextDesc" style="padding-top:6px;"><?=isset($woitemso_qtyso) ? $woitemso_qtyso:'-';?></div>
        </div>
        <label for="idtxtwoitem_qty" class="col-md-1 control-label">Qty WO</label>
        <div class="col-sm-2 col-xs-12">
            <div class="errClass" id="idErrwoitem_qty"></div>
            <input type="number" name="txtwoitem_qty" id="idtxtwoitem_qty" class="form-control tt-input" placeholder="Qty" value="<?=isset($woitemso_qty) ? $woitemso_qty:null;?>" >
        </div>
    </div>
    <div class="form-group">
        <label for="idtxtwoitemso_tglselesai" class="col-md-2 control-label">Tgl Selesai</label>
        <div class="col-md-4">
            <div class="errClass" id="idErrwoitemso_tglselesai"></div>
            <div class="input-group date">
               <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="text" name="txtwoitemso_tglselesai" id="idtxtwoitemso_tglselesai" class="form-control datepicker tt-input" placeholder="yyyy-mm-dd" value="<?=isset($woitemso_tglselesai) ? $woitemso_tglselesai:null;?>" >
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-4 col-sm-offset-9 col-xs-12">
        <button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitDataWOItemSO()" class="btn btn-primary btn-sm btn-flat">
            <i class="fa fa-save"></i> Simpan
        </button>
        <button type="reset" name="btnReset" class="btn btn-default btn-sm btn-flat" onclick="open_form_item ('add', '<?php echo $wo_kd;?>', '')">
            <i class="fa fa-refresh"></i> Reset
        </button>
        <button type="text" name="btnTutup" class="btn btn-warning btn-sm btn-flat" onclick="close_form_item ()">
            <i class="fa fa-close"></i> Tutup
        </button>
    </div>
</div>
<hr>
    
<!-- /.box-body -->
<?php echo form_close(); ?>

<script type="text/javascript">
    <?php if ($slug == 'edit') : ?>
        get_bom('<?php echo $woitemso_status;?>', '<?php echo $bom_kd; ?>');
    <?php endif;?>
    /* --start product typeahead.js-- */
	var Product = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		url: '<?php echo base_url(); ?>auto_complete/get_barang_rm_wip',
		remote: {
			url: '<?php echo base_url(); ?>auto_complete/get_barang_rm_wip',
			prepare: function (query, settings) {
				settings.type = 'GET';
				settings.contentType = 'application/json; charset=UTF-8';
				settings.data = 'item_code='+query;
				
				return settings;
			},
			wildcard: '%QUERY'
		}
	});

	$('#idTxtProductCode').typeahead(null, {
		limit: 50,
		minLength: 2,	
		display: 'item_code',
		source: Product.ttAdapter()
	});

	$('#idTxtProductCode').bind('typeahead:select', function(obj, selected) {
		$('#idtxtItem_desc').val(selected.deskripsi_barang);
		$('#idtxtItemDimension').val(selected.dimensi_barang);
		$('#idtxtbarang_kd').val(selected.kd_barang);
        get_bomkd (selected.kd_barang);
        $('#idtxtbom_kd').focus();
        if (selected.item_code == 'CUSTOM ITEM'){
            $('#idtxtItem_desc').attr('readonly', false);
            $('#idtxtItemDimension').attr('readonly', false);
        }
	});
	/* --end of product typeahead.js-- */

    $(document).off('change', '#idtxtItem_status').on('change', '#idtxtItem_status', function(e){
		item_type($(this).val(), '');
	});

    function item_type(type, slug) {
		if (type == 'custom') {
			$('#idtxtItem_desc').attr('readonly', false);
            $('#idtxtItemDimension').attr('readonly', false);
            $('#idtxtbarang_kd').val('PRD020817000573'); // kd_barang custom item
		}
		else if (type == 'std') {
            $('#idtxtItem_desc').attr('readonly', true);
            $('#idtxtItemDimension').attr('readonly', true);
            $('#idTxtProductCode').val(''); // kd_barang std
            $('#idtxtItem_desc').val(''); // kd_barang std
            $('#idtxtItemDimension').val(''); // kd_barang std
            $('#idtxtbarang_kd').val(''); // kd_barang std
		}
        /** Dropdown BOM if not editing slug */
        if (slug != 'edit') {
            get_bom(type, '');
        }
	}

    function reset_form_woitem(){
        $('#idTxtProductCode').val('');
        $('.tt-input').val('');
        $('#idtxtbom_kd').val('').trigger('change');
    }

</script>