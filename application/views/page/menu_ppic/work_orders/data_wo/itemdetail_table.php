<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }

    td.dt-green {
        background-color: green;
        color: white;
    }
</style>

<table id="idtableDetailWO" class="table table-bordered table-striped table-hover display responsive">
    <thead>
        <tr style="font-size:95%;">
            <th style="text-align: center; width: 1%;">No.</th>
            <th style="text-align: center; width: 1%;">Aksi</th>
            <th style="text-align: center; width: 5%;">WO / No. Project</th>
            <th style="text-align: center; width: 12%;">Part Code</th>
            <th style="text-align: center; width: 4%;">Status</th>
            <th style="text-align: center; width: 5%;">Deskripsi</th>
            <th style="text-align: center; width: 1%;">Qty</th>
            <th style="text-align: center; width: 1%;">Satuan</th>
            <th style="text-align: center; width: 15%;">Keterangan</th>
            <th style="text-align: center; width: 5%;">Tgl Selesai</th>
            <th style="text-align: center; width: 3%;">Status</th>
        </tr>
    </thead>
    <tbody style="font-size:85%;">
        <?php
        /** Get part detail untuk wopacking */
        $arrayPackingPart = [];
        foreach ($itemdetail as $r_itemdetail2) {
            if ($r_itemdetail2['woitem_jenis'] == 'part') {
                if ($r_itemdetail2['woitemdetail_generatewo'] == 'T') {
                    $arrayPackingPart[] = $r_itemdetail2;
                }
            }
        }
        $no = 1;
        foreach ($items as $r_item) :
        ?>
            <tr>
                <td style="text-align: center;"><?= $no; ?></td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Opsi <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <!-- <li><a href="javascript:void(0);" title="Detail Item" onclick="detail_woitem('<?php echo $r_item['woitem_kd']; ?>')"><i class="fa fa-list"></i> Detail Item</a></li> -->
                            <li><a href="javascript:void(0);" title="Edit Item" onclick="itemdetail_form ('edit', '<?php echo $r_item['woitem_kd']; ?>')"><i class="fa fa-pencil"></i> Edit Item</a></li>
                            <!-- <li><a href="javascript:void(0);" title="Cancel Item" onclick="cancel_woitem('<?php echo $r_item['woitem_kd']; ?>')"><i class="fa fa-close"></i> Cancel Item</a></li> -->
                        </ul>
                    </div>
                </td>
                <td><?= $r_item['woitem_no_wo']; ?></td>
                <td><?= $r_item['woitem_itemcode']; ?></td>
                <td><?= $r_item['woitem_status']; ?></td>
                <td><?= $r_item['woitem_deskripsi'] . $r_item['woitem_dimensi']; ?></td>
                <td style="text-align: right;" class="dt-green"><?= $r_item['woitem_qty']; ?></td>
                <td><?= $r_item['woitem_satuan']; ?></td>
                <td><?= $r_item['woitem_note']; ?></td>
                <td><?= $r_item['woitem_tglselesai']; ?></td>
                <td><?php
                    switch ($r_item['woitem_prosesstatus']) {
                        case 'workorder':
                            $color = 'yellow';
                            break;
                        case 'finish':
                            $color = 'green';
                            break;
                        case 'cancel':
                            $color = 'red';
                            break;
                    }
                    echo bg_label($r_item['woitem_prosesstatus'], $color) ?></td>
            </tr>
        <?php
            $no++;
        endforeach;
        ?>
    </tbody>
    <tfoot>
    </tfoot>
</table>

<script type="text/javascript">
    render_dt('#idtableDetailWO');

    function edit_item(id) {
        var url = '<?php echo base_url() . $class_link; ?>/form_additem?source=edit_editItem&kd_ditem_wo=' + id;
        $('#idAdditem').load(url);
        moveTo('idMainContent');
    }

    function render_dt(table_elem) {
        $(table_elem).DataTable({
            "scrollX": true,
            "autoWidth": true,
            "responsive": false,
            "paging": false,
            "language": {
                "lengthMenu": "Tampilkan _MENU_ data",
                "zeroRecords": "Maaf tidak ada data yang ditampilkan",
                "info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
                "infoFiltered": "(difilter dari _MAX_ total data)",
                "infoEmpty": "Tidak ada data yang ditampilkan",
                "search": "Cari :",
                "loadingRecords": "Memuat Data...",
                "processing": "Sedang Memproses...",
                "paginate": {
                    "first": '<span class="glyphicon glyphicon-fast-backward"></span>',
                    "last": '<span class="glyphicon glyphicon-fast-forward"></span>',
                    "next": '<span class="glyphicon glyphicon-forward"></span>',
                    "previous": '<span class="glyphicon glyphicon-backward"></span>'
                }
            },
            "columnDefs": [{
                "searchable": false,
                "orderable": false,
                "className": "dt-center",
                "targets": 0
            }, ],
        });
    }
</script>