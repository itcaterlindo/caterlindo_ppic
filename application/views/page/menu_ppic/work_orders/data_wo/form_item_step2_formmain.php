<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputDetail';
?>
<?php
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtSts', 'name' => 'txtSts', 'value' => $sts));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtwoitemdetail_kd', 'name' => 'txtwoitemdetail_kd', 'value' => isset($woitemdetail['woitemdetail_kd']) ? $woitemdetail['woitemdetail_kd'] : null));
?>

<div class="form-group">
	<label for="idtxtwoitem_qty" class="col-md-1 control-label">WO Terbit</label>
	<div id="iderrwoitemdetail_kd"></div>
	<div class="col-md-4">
		<input type="text" class="form-control" readonly="readonly" value="<?php echo $woitemdetail['wo_noterbit']; ?>">
	</div>
</div>
<div class="form-group">
	<label for="idtxtwoitem_qty" class="col-md-1 control-label">Barang</label>
	<div class="col-md-4">
		<input type="text" class="form-control" readonly="readonly" value="<?php echo isset($woitemdetail['woitemso_itemcode']) ? $woitemdetail['woitemso_itemcode'] : '-' ?>">
	</div>
	<label for="idtxtwoitem_qty" class="col-md-1 control-label">Part</label>
	<div class="col-md-4">
		<input type="text" class="form-control" readonly="readonly" value="<?php echo isset($woitemdetail['partmain_nama']) ? $woitemdetail['partmain_nama'] : '-' ?>">
	</div>
</div>

<div class="form-group">
	<div class="col-md-1"></div>
	<div class="col-md-2 control-label">
		<div class="checkbox">
			<label><input name="txtwoitemdetail_generatewo" id="idtxtwoitemdetail_generatewo" <?php if (isset($woitemdetail['woitemdetail_generatewo']) && $woitemdetail['woitemdetail_generatewo'] == 'T') echo 'checked'; ?> type="checkbox">Generate WO</label>
		</div>
	</div>
	<div class="col-md-2 control-label">
		<div class="checkbox">
			<label><input name="txtwoitemdetail_wogrouping" <?php if (isset($woitemdetail['woitemdetail_wogrouping']) && $woitemdetail['woitemdetail_wogrouping'] == 'T') echo 'checked'; ?> type="checkbox">Grouping WO</label>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-1 col-xs-12 col-sm-offset-1">
			<button type="submit" name="btnSubmit" onclick="submitDatadetailItem('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>
</div>
<hr>
<hr>
<?php echo form_close(); ?>

<script type="text/javascript">

</script>