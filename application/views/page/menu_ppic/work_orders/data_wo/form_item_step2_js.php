<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
    form_item_step2_tablemain('<?php echo $wo_kd; ?>');
    first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

    function form_item_step2_tablemain(wo_kd) {
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url() . $class_link . '/form_item_step2_tablemain'; ?>',
            data: {
                wo_kd: wo_kd
            },
            success: function(html) {
                $('#<?php echo $box_content_id; ?>').slideDown().html(html);
                moveTo('idMainContent');
            }
        });
    }

    function previewresultwo_table(element) {
        let wo_kd = $(element).attr("data-wo_kd");
        $.ajax({
                type: 'GET',
                url: '<?php echo base_url() . $class_link ?>/previewresultwo_table',
                data: {
                    wo_kd: wo_kd,
                },
                success: function(html) {
                    toggle_modal("Preview WO", html);
                }
            });
    }

    function process_step1(element) {
        var conf = confirm('Apakah anda yakin ?');
        if (conf) {
            let id = $(element).attr('id');
            let wo_kd = $(element).attr("data-wo_kd");
            let csrf = $(element).attr("data-csrf");
            let slug = $(element).attr("data-slug");

            $('#' + id).html('<i class="fa fa-spinner fa-pulse"></i> Loading');
            $('#' + id).attr('disabled', true);

            var url = '<?php echo base_url() . $class_link . '/action_process_wo_step1'; ?>';
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    '<?php echo $this->security->get_csrf_token_name(); ?>': csrf,
                    'wo_kd': wo_kd,
                    'slug': slug
                },
                success: function(data) {
                   
                    var resp = JSON.parse(data);
                    console.log(resp);
                    if (resp.code == 200) {
                        notify(resp.status, resp.pesan, 'success');
                        window.location.reload();
                    } else {
                        notify('Error', 'Gagal Proses', 'error');
                        generateToken(resp.csrf);
                    }
                }
            });
        }
    }

    function process_wo_step2(element) {
        event.preventDefault();
        var conf = confirm('Apakah kamu yakin ?');
        if (conf) {
            let id = $(element).attr('id');
            let wo_kd = $(element).data('wo_kd');
            let slug = $(element).data('slug');

            $('#' + id).html('<i class="fa fa-spinner fa-pulse"></i> Loading');
            $('#' + id).attr('disabled', true);

            $.ajax({
                type: 'GET',
                url: '<?php echo base_url() . $class_link ?>/action_process_wo_step2',
                data: {
                    wo_kd: wo_kd,
                    slug: slug
                },
                success: function(resp) {
                    console.log(resp);
                    if (resp.code == 200) {
                        window.location.assign('<?php echo base_url() . $class_link ?>/itemdetail_box?wo_kd=' + resp.data.wo_kd);
                        notify(resp.status, resp.pesan, 'success');
                    }
                }
            });
        }
    }

    function submitDatadetailItem(form_id) {
        event.preventDefault();
        let form = document.getElementById(form_id);
        let sts = $('#idtxtSts').val();
        let url = "<?php echo base_url() . $class_link; ?>/action_update_woitemdetail";

        $.ajax({
            url: url,
            type: "POST",
            data: new FormData(form),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                var resp = JSON.parse(data);
                if (resp.code == 200) {
                    notify(resp.status, resp.pesan, 'success');
                    toggle_modal('', '');
                    form_item_step2_tablemain('<?php echo $wo_kd; ?>');
                    generateToken(resp.csrf);
                } else if (resp.code == 401) {
                    $.each(resp.pesan, function(key, value) {
                        $('#' + key).html(value);
                    });
                    generateToken(resp.csrf);
                } else if (resp.code == 400) {
                    notify(resp.status, resp.pesan, 'error');
                    generateToken(resp.csrf);
                } else {
                    notify('Error', 'Error tidak Diketahui', 'error');
                    generateToken(resp.csrf);
                }
            }
        });

    }

    function edit_item(id) {
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url() . $class_link . '/form_item_step2_formmain'; ?>',
            data: {
                id: id
            },
            success: function(html) {
                toggle_modal('Edit', html)
            }
        });
    }

    function toggle_modal(modalTitle, htmlContent) {
        $('#idmodal').modal('toggle');
        $('.modal-title').text(modalTitle);
        $('#<?php echo $content_modal_id; ?>').slideUp();
        $('#<?php echo $content_modal_id; ?>').html(htmlContent);
        $('#<?php echo $content_modal_id; ?>').slideDown();
    }

    function generateToken(csrf) {
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
    }

    function first_load(loader, content) {
        $('#' + loader).fadeOut(500, function(e) {
            $('#' + content).slideDown();
        });
    }

    function moveTo(div_id) {
        $('html, body').animate({
            scrollTop: $('#' + div_id).offset().top - $('header').height()
        }, 1000);
    }

    function render_select2(element) {
        $(element).select2({
            theme: 'bootstrap',
            width: '100%',
            placeholder: '-- Pilih Opsi --',
        });
    }

    function notify(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            type: type,
            delay: 2500,
            styling: 'bootstrap3'
        });
    }
</script>