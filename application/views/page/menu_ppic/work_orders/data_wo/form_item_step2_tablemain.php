<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }

    tr.dt-red {
        background-color: red;
        color: white;
    }
</style>

<div class="row invoice-info">
	<div class="col-sm-6 invoice-col">
		<b>Tanggal Work Order :</b> #<?=format_date($wo['wo_tanggal'], 'd-M-Y')?><br>
		<b>Status :</b> <?php echo bg_label(process_status($wo['wo_status']), color_status($wo['wo_status'])); ?>
	</div>
	<div class="col-sm-6 invoice-col">
		<b>No Terbit :</b> #<?php echo isset($wo['wo_noterbit']) ? $wo['wo_noterbit']: '-'; ?> <br>
		<b>No Sales Order :</b> #<?php echo isset($no_salesorders) ? $no_salesorders: '-'; ?>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <?php if ($wo['wo_status'] == 'pending') : ?>
            <button class="btn btn-sm btn-default" onclick="previewresultwo_table(this)" data-wo_kd="<?php echo $wo_kd; ?>" > <i class="fa fa-list"></i> Preview Result WO</button>
            <button class="btn btn-sm btn-danger" id="idbtnreprocess" onclick="process_step1(this)" data-wo_kd="<?php echo $wo_kd; ?>" data-csrf="<?php echo $this->security->get_csrf_hash(); ?>" data-slug="reprocess"> <i class="fa fa-refresh"></i> Reprocess</button>
            <button class="btn btn-sm btn-danger" id="idbtnprosesstep2" onclick="process_wo_step2(this)" data-wo_kd="<?php echo $wo_kd; ?>" data-slug="first"> <i class="fa fa-bullhorn"></i> Proses WO Step 2</button>
        <?php endif; ?>
        <?php if ($wo['wo_status'] == 'process_wo') : ?>
            <button class="btn btn-sm btn-default" onclick="window.location.assign('<?php echo base_url() . $class_link; ?>/itemdetail_box?wo_kd=<?php echo $wo_kd; ?>')"> <i class="fa fa-arrow-right"></i> View Next Detail WO</button>
        <?php endif; ?>

    </div>
</div>

<div class="col-md-12">
    <table id="idtableWOstep1" class="table table-bordered display" style="font-size:95%;">
        <thead>
            <tr>
                <th style="text-align: center; width: 1%;">No</th>
                <th style="text-align: center; width: 1%;">Opsi</th>
                <th style="text-align: center; width: 5%;">Item Code</th>
                <th style="text-align: center; width: 5%;">Nama Part</th>
                <th style="text-align: center; width: 5%;">Jenis Part</th>
                <th style="text-align: center; width: 2%;">Item<br>Status</th>
                <th style="text-align: center; width: 10%;">Deskripsi</th>
                <th style="text-align: center; width: 1%;">Qty WO</th>
                <th style="text-align: center; width: 1%;">Generate WO</th>
                <th style="text-align: center; width: 1%;">Grouping WO</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            // echo json_encode($woitemdetails);
            foreach ($woitemdetails as $woitemdetail) :
                $color = '';
                if (empty($woitemdetail['woitemdetail_parent'])) {
                    $color = 'gray';    
                }
            ?>
                <tr style="background-color: <?php echo $color; ?>">
                    <td class="dt-center"><?= $no; ?></td>
                    <td class="dt-center">
                        <?php if (!empty($woitemdetail['woitemdetail_parent']) && $wo['wo_status'] == 'pending') : ?>
                            <button class="btn btn-xs btn-warning" title="Edit Item" onclick="edit_item('<?php echo $woitemdetail['woitemdetail_kd'] ?>')"> <i class="fa fa-pencil"></i> </button>
                        <?php endif; ?>
                    </td>
                    <td><?= $woitemdetail['woitemso_itemcode']; ?></td>
                    <td><?= $woitemdetail['partmain_nama'] ?></td>
                    <td><?= $woitemdetail['partjenis_nama'] ?></td>
                    <td><?= $woitemdetail['woitemso_status']; ?></td>
                    <td><?= $woitemdetail['woitemso_itemdeskripsi'] . '/' . $woitemdetail['woitemso_itemdimensi']; ?></td>
                    <td class="dt-right"><?= $woitemdetail['woitemso_qty']; ?></td>
                    <td class="dt-right"><?= choice_enum($woitemdetail['woitemdetail_generatewo']); ?></td>
                    <td class="dt-right"><?= choice_enum($woitemdetail['woitemdetail_wogrouping']); ?></td>
                </tr>
            <?php
                $no++;
            endforeach;
            ?>
        </tbody>
    </table>
</div>



<script type="text/javascript">
    render_dt('#idtableWOstep1');

    function render_dt(table_elem) {
        $(table_elem).DataTable({
            // "autoWidth": true,
            // "responsive": true,
            "paging": false,
            "language": {
                "lengthMenu": "Tampilkan _MENU_ data",
                "zeroRecords": "Maaf tidak ada data yang ditampilkan",
                "info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
                "infoFiltered": "(difilter dari _MAX_ total data)",
                "infoEmpty": "Tidak ada data yang ditampilkan",
                "search": "Cari :",
                "loadingRecords": "Memuat Data...",
                "processing": "Sedang Memproses...",
                "paginate": {
                    "first": '<span class="glyphicon glyphicon-fast-backward"></span>',
                    "last": '<span class="glyphicon glyphicon-fast-forward"></span>',
                    "next": '<span class="glyphicon glyphicon-forward"></span>',
                    "previous": '<span class="glyphicon glyphicon-backward"></span>'
                }
            },
        });
    }
</script>