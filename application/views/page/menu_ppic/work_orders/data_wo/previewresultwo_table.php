<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }

    td.dt-green {
        background-color: green;
        color: white;
    }
</style>

    <table id="idtablePreviewWO" class="table table-bordered table-striped table-hover display">
        <thead>
            <tr style="font-size:95%;">
                <th style="text-align: center; width: 1%;">No.</th>
                <th style="text-align: center; width: 12%;">Part Code</th>
                <th style="text-align: center; width: 4%;">Status</th>
                <th style="text-align: center; width: 1%;">Qty</th>
                <th style="text-align: center; width: 1%;">Satuan</th>
                <th style="text-align: center; width: 15%;">Keterangan</th>
            </tr>
        </thead>
        <tbody style="font-size:85%;">
            <?php
            $no = 1;
            foreach ($woitems as $wojenis => $elements) :
            ?>
                <tr>
                    <td style="font-weight: bold;"><?php echo strtoupper($wojenis); ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php
                foreach ($elements as $el) : ?>
                    <tr>
                        <td style="text-align: center;"><?= $no; ?></td>
                        <td style="text-align: left;"><?= $el['woitem_itemcode']; ?></td>
                        <td style="text-align: left;"><?= $el['woitem_jenis']; ?></td>
                        <td style="text-align: right;" class="dt-green"><?= $el['woitem_qty']; ?></td>
                        <td style="text-align: left;"><?= $el['woitem_satuan']; ?></td>
                        <td style="text-align: left;"><?= $el['woitem_note']; ?></td>
                    </tr>
                <?php
                    $no++;
                endforeach; ?>
            <?php
            endforeach;
            ?>
        </tbody>
        <tfoot>
        </tfoot>
    </table>
</div>

<script type="text/javascript">
    render_dt('#idtablePreviewWO');

    function render_dt(table_elem) {
        $(table_elem).DataTable({
            "ordering": false,
            "paging": false,
            "language": {
                "lengthMenu": "Tampilkan _MENU_ data",
                "zeroRecords": "Maaf tidak ada data yang ditampilkan",
                "info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
                "infoFiltered": "(difilter dari _MAX_ total data)",
                "infoEmpty": "Tidak ada data yang ditampilkan",
                "search": "Cari :",
                "loadingRecords": "Memuat Data...",
                "processing": "Sedang Memproses...",
                "paginate": {
                    "first": '<span class="glyphicon glyphicon-fast-backward"></span>',
                    "last": '<span class="glyphicon glyphicon-fast-forward"></span>',
                    "next": '<span class="glyphicon glyphicon-forward"></span>',
                    "previous": '<span class="glyphicon glyphicon-backward"></span>'
                }
            },
            "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "className": "dt-center",
                    "targets": 0
                },
            ],
        });
    }
</script>