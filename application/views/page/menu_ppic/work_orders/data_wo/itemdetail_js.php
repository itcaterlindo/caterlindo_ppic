<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_itemdetail_table('<?php echo $wo_kd; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('form_add_master', '');
	});

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		open_table();
		$('#<?php echo $btn_add_id; ?>').slideDown();
	});

	function itemdetail_formpyr(slug, wo_kd, id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/itemdetail_formpyr',
			data: {
				slug: slug,
				wo_kd: wo_kd,
				id: id
			},
			success: function(html) {
				$('#<?php echo $box_form_content_id; ?>').html(html);
				render_datetimepicker('.datetimepicker');
			}
		});
	}

	function itemdetail_form(slug, id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/itemdetail_form',
			data: {
				slug: slug,
				id: id
			},
			success: function(html) {
				$('#<?php echo $box_form_content_id; ?>').html(html);
				render_datetimepicker('.datetimepicker');
			}
		});
		moveTo('idMainContent');
	}

	function open_itemdetail_table(wo_kd) {
		// event.preventDefault();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/itemdetail_table',
			data: {
				wo_kd: wo_kd
			},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
			}
		});
	}

	function submitDataWOItemPyr() {
		box_overlay_itemdetail('in');
		event.preventDefault();
		var form = document.getElementById('idFormAddWOitemdetailPYR');
		var url = '<?php echo base_url() . $class_link . '/action_insert_woitempyr'; ?>';

		$.ajax({
			url: url,
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				console.log(data);
				var resp = JSON.parse(data);
				if (resp.code == 200) {
					var wo_kd = $('#idtxtwo_kd').val();
					open_itemdetail_table('<?php echo $wo_kd; ?>');
					itemdetail_formpyr('add', '<?php echo $wo_kd; ?>', '');
					notify(resp.status, resp.pesan, 'success');
					generateToken(resp.csrf);
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
				box_overlay_itemdetail('out');
			}
		});
	}

	function submitDataWOItemEdit() {
		box_overlay_itemdetail('in');
		event.preventDefault();
		var form = document.getElementById('idFormAddWOitemEdit');
		var url = '<?php echo base_url() . $class_link . '/action_update_woitem'; ?>';

		$.ajax({
			url: url,
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				console.log(data);
				var resp = JSON.parse(data);
				if (resp.code == 200) {
					open_itemdetail_table('<?php echo $wo_kd; ?>');
					tutupForm();
					notify(resp.status, resp.pesan, 'success');
					generateToken(resp.csrf);
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
				box_overlay_itemdetail('out');
			}
		});
	}

	function cancel_woitem(id) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			box_overlay_itemdetail('in');
			var url = '<?php echo base_url() . $class_link ?>/action_cancel_woitem';
			$.ajax({
				url: url,
				type: 'GET',
				data: 'id=' + id,
				success: function(data) {
					var resp = JSON.parse(data);
					if (resp.code == 200) {
						notify(resp.status, resp.pesan, 'success');
						open_itemdetail_table('<?php echo $wo_kd; ?>');
					} else {
						notify('Gagal', resp.pesan, 'error');
					}
					box_overlay_itemdetail('out');
				}
			});
		}
	}

	function render_datetimepicker(element){
		$(element).datetimepicker({
			format: 'YYYY-MM-DD',
    	});
	}

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function tutupForm() {
		event.preventDefault();
		$('#<?php echo $box_form_content_id; ?>').html('');
	}

	function box_overlay_itemdetail(sts) {
		if (sts == 'in') {
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		} else if (sts == 'out') {
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
			delay: 2500,
            styling: 'bootstrap3'
        });
    }

	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}
</script>