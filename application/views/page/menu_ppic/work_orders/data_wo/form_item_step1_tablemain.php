<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }

    tr.dt-red {
        background-color: red;
        color: white;
    }
</style>

<div class="row invoice-info">
    <div class="col-sm-6 invoice-col">
        <b>Tanggal Work Order :</b> #<?= format_date($wo['wo_tanggal'], 'd-M-Y') ?><br>
        <b>Status :</b> <?php echo bg_label(process_status($wo['wo_status']), color_status($wo['wo_status'])); ?>
    </div>
    <div class="col-sm-6 invoice-col">
        <b>No Terbit :</b> #<?php echo isset($wo['wo_noterbit']) ? $wo['wo_noterbit'] : '-'; ?> <br>
        <b>No Sales Order :</b> #<?php echo isset($no_salesorders) ? $no_salesorders : '-'; ?>
    </div>
</div>

<div class="row">
    <hr>
    <div id="idAddItem"></div>
</div>

<div class="col md-12">
    <div class="btn-group">
        <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown"> Lihat Data
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu" role="menu">
            <li><a href="javascript:void(0);" onclick="tableplanning_box ('<?php echo $wo_kd ?>')">Detail Item Planning</a></li>
        </ul>
    </div>
    <button class="pull-right btn btn-sm btn-default" onclick="window.location.assign('<?php echo base_url() . $class_link ?>')"> <i class="fa fa-arrow-circle-left"></i> Kembali </button>
    <?php if ($wo['wo_status'] == 'pending') : ?>
        <button class="btn btn-sm btn-success" onclick="open_form_item ('add', '<?php echo $wo_kd; ?>', '')"> <i class="fa fa-plus"></i> Tambah Item</button>
        <button class="btn btn-sm btn-danger" title="Hitung kebutuhan" data-csrf="<?php echo $this->security->get_csrf_hash(); ?>" data-wo_kd="<?php echo $wo_kd; ?>" onclick="process_generate_qtywo(this)"> <i class="fa fa-refresh"></i> Generate Qty WO</button>
        <?php if ($nextstep == 'true') : ?>
            <button class="btn btn-sm btn-danger" title="Proses setting Grouping apa tidak" data-csrf="<?php echo $this->security->get_csrf_hash(); ?>" data-wo_kd="<?php echo $wo_kd; ?>" onclick="process_step1(this)"> <i class="fa fa-arrow-right"></i> Next Step 1</button>
        <?php else : ?>
            <button class="btn btn-sm btn-danger" title="Proses setting Grouping apa tidak" data-csrf="<?php echo $this->security->get_csrf_hash(); ?>" data-wo_kd="<?php echo $wo_kd; ?>" onclick="process_step1(this)" disabled> <i class="fa fa-arrow-right"></i> Next Step 1</button>
        <?php endif; ?>
    <?php endif; ?>
    <?php if ($wo['wo_status'] == 'process_wo') : ?>
        <button class="btn btn-sm btn-default" onclick="window.location.assign('<?php echo base_url() . $class_link; ?>/form_item_step2_box?wo_kd=<?php echo $wo_kd; ?>')"> <i class="fa fa-arrow-right"></i> View Next Step 1</button>
    <?php endif; ?>

</div>
<hr>

<div class="col-md-12">
    <table id="idtableWOitemSO" class="table table-bordered display responsive classtableWOitem" style="font-size:95%;">
        <thead>
            <tr>
                <th style="text-align: center; width: 1%;">No.</th>
                <th style="text-align: center; width: 2%;">Aksi</th>
                <th style="text-align: center; width: 10%;">Item Code</th>
                <th style="text-align: center; width: 2%;">Item<br>Status</th>
                <th style="text-align: center; width: 25%;">Deskripsi</th>
                <th style="text-align: center; width: 1%;">Qty SO</th>
                <th style="text-align: center; width: 1%;">Qty WO</th>
                <th style="text-align: center; width: 5%;">Bill Of<br>Material</th>
                <th style="text-align: center; width: 2%;">Status</th>
                <th style="text-align: center; width: 2%;">UpdatedAt</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach ($woitemsos as $woitemso) :
            ?>
                <tr>
                    <td class="dt-center"><?= $no; ?></td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Opsi <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <?php if ($woitemso['woitemso_prosesstatus'] != 'cancel') : ?>
                                    <?php if ($woitemso['woitemso_prosesstatus'] != 'null_bom') : ?>
                                        <li><a href="javascript:void(0);" title="Lihat BoM" onclick=view_bom("<?php echo $woitemso['bom_kd_real'] ? $woitemso['bom_kd_real'] : $woitemso['bom_kd']; ?>","<?php echo $woitemso['woitemso_kd_real']; ?>")><i class="fa fa-search"></i> Lihat BoM</a></li>
                                    <?php endif; ?>
                                    <?php if ($woitemso['woitemso_prosesstatus'] != 'workorder') : ?>
                                        <li><a href="javascript:void(0);" title="Edit Item" onclick=edit_woitemso("<?php echo $woitemso['wo_kd_real'] ? $woitemso['wo_kd_real'] : $woitemso['wo_kd'] ; ?>","<?php echo $woitemso['woitemso_kd_real']; ?>")><i class="fa fa-pencil"></i> Edit Item</a></li>
                                    <?php endif; ?>
                                    <li class="divider"></li>
                                    <?php if ($woitemso['woitemso_prosesstatus'] != 'workorder') : ?>
                                        <li><a href="javascript:void(0);" title="Cancel Item" onclick=onclick=ubahstatus_woitemso("<?php echo $woitemso['woitemso_kd_real']; ?>","cancel")><i class="fa fa-close"></i> Cancel Item</a></li>
                                        <!-- <li><a href="javascript:void(0);" title="Delete Item" onclick=del_woitemso("<?php echo $woitemso['woitemso_kd_real']; ?>")><i class="fa fa-trash"></i> Delete Item</a></li> -->
                                    <?php endif; ?>
                                <?php else : ?>
                                    <li><a href="javascript:void(0);" title="Cancel Item" onclick=ubahstatus_woitemso("<?php echo $woitemso['woitemso_kd_real']; ?>","editing")><i class="fa fa-close"></i> Editing Item</a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </td>
                    <td><?= $woitemso['itemcode_real'] ? $woitemso['itemcode_real'] : $woitemso['woitemso_itemcode']; ?></td>
                    <td><?= $woitemso['woitemso_status']; ?></td>
                    <td><?= ($woitemso['woitemsodetail_itemdeskripsi'] ? $woitemso['woitemsodetail_itemdeskripsi'] : $woitemso['woitemso_itemdeskripsi'])  . '/' . ($woitemso['woitemsodetail_itemdimensi'] ? $woitemso['woitemsodetail_itemdimensi'] : $woitemso['woitemso_itemdimensi']); ?></td>
                    <td class="dt-right"><?= $woitemso['real_qty']; ?></td>
                    <td class="dt-right" style="background-color: green; color:white;"><?= $woitemso['woitemso_qty']; ?></td>
                    <td><?php
                        $bomCode = $woitemso['item_code'] == NULL ? $woitemso['rm_kode'] : $woitemso['item_code'];
                        if ($woitemso['woitemso_status'] == 'custom') :
                            $bomCode = $woitemso['woitemso_nopj'];
                        endif;
                        echo $bomCode;
                        ?></td>
                    <td><?php
                        $msg = strtoupper(str_replace('_', ' ', $woitemso['woitemso_prosesstatus']));
                        switch ($woitemso['woitemso_prosesstatus']) {
                            case 'editing':
                                $type = 'warning';
                                break;
                            case 'null_bom':
                                $type = 'danger';
                                break;
                            case 'workorder':
                                $type = 'success';
                                break;
                            case 'cancel':
                                $type = 'danger';
                                break;
                        }
                        echo buildLabel($type, $msg);
                        ?></td>
                    <td><?php echo $woitemso['woitemso_tgledit']; ?></td>
                </tr>
            <?php
                $no++;
            endforeach;
            ?>
        </tbody>
    </table>
</div>