<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }

    tr.dt-red {
        background-color: red;
        color: white;
    }
</style>

<div class="col md-12">
    <?php if ($wo['wo_status'] == 'pending') : ?>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-warning dropdown-toggle" data-toggle="dropdown"> List Item
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="javascript:void(0);" onclick="itemadditionalso_table ('<?php echo $wo_kd ?>')">Item Additional SO</a></li>
                <li><a href="javascript:void(0);" onclick="itemunproseswo_table ('<?php echo $wo_kd ?>')">Item Unproses WO</a></li>
            </ul>
        </div>
        <button class="btn btn-sm btn-danger" title="Proses Group By Item Product" data-csrf="<?php echo $this->security->get_csrf_hash(); ?>" data-wo_kd="<?php echo $wo_kd; ?>" onclick="process_step0(this)"> <i class="fa fa-arrow-right"></i> Next Step 0</button>
    <?php endif; ?>
    <?php if ($wo['wo_status'] == 'process_wo') : ?>
        <button class="btn btn-sm btn-default" onclick="window.location.assign('<?php echo base_url() . $class_link; ?>/form_item_step1_box?wo_kd=<?php echo $wo_kd; ?>')"> <i class="fa fa-arrow-right"></i> View Next Step 0</button>
    <?php endif; ?>

</div>

<div class="col-md-12">
    <table id="idtableWOitemsoDetail" class="table table-bordered display responsive" style="font-size:95%;">
        <thead>
            <tr>
                <th style="text-align: center; width: 1%;">No.</th>
                <th style="text-align: center; width: 10%;">No SO</th>
                <th style="text-align: center; width: 10%;">Item Code</th>
                <th style="text-align: center; width: 2%;">Item<br>Status</th>
                <th style="text-align: center; width: 25%;">Deskripsi</th>
                <th style="text-align: center; width: 1%;">Qty SO</th>
                <th style="text-align: center; width: 1%;">Status</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach ($woitemsodetails as $woitemsodetail) :
            ?>
                <tr>
                    <td class="dt-center"><?= $no; ?></td>

                    <td><?php
                        $no_salesorder = $woitemsodetail['no_salesorder'];
                        if ($woitemsodetail['tipe_customer'] == 'Ekspor') {
                            $no_salesorder = $woitemsodetail['no_po'];
                        }
                        echo $no_salesorder;
                        ?></td>
                    <td><?= $woitemsodetail['woitemsodetail_itemcode']; ?></td>
                    <td><?= $woitemsodetail['woitemsodetail_status']; ?></td>
                    <td><?= $woitemsodetail['woitemsodetail_itemdeskripsi'] . '/' . $woitemsodetail['woitemsodetail_itemdimensi']; ?></td>
                    <td class="dt-right" style="background-color: green; color:white;"><?= $woitemsodetail['woitemsodetail_qty']; ?></td>
                    <td><?php
                        $msg = strtoupper(str_replace('_', ' ', $woitemsodetail['woitemsodetail_prosesstatus']));
                        switch ($woitemsodetail['woitemsodetail_prosesstatus']) {
                            case 'editing':
                                $type = 'warning';
                                break;
                            case 'null_bom':
                                $type = 'danger';
                                break;
                            case 'workorder':
                                $type = 'success';
                                break;
                            case 'cancel':
                                $type = 'danger';
                                break;
                        }
                        echo buildLabel($type, $msg);
                        ?></td>
                </tr>
            <?php
                $no++;
            endforeach;
            ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    render_dt('#idtableWOitemsoDetail');
</script>