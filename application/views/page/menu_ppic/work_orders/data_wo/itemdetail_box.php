<?php
defined('BASEPATH') or exit('No direct script access allowed!');

/* --SET DEFAULT PROPERTY UNTUK BOX-- */
$js_file = 'itemdetail_js';
$box_type = 'Form';
$box_title = 'Form Work Order';
$data['master_var'] = 'WorkOrderDetail';
$data['class_link'] = $class_link;
$data['box_id'] = 'id'.$box_type.'Box'.$data['master_var'];
$data['btn_hide_id'] = 'id'.$box_type.'BtnBoxHide'.$data['master_var'];
$data['btn_add_id'] = 'id'.$box_type.'BtnBoxAdd'.$data['master_var'];
$data['btn_remove_id'] = 'id'.$box_type.'BtnBoxRemove'.$data['master_var'];
$data['box_alert_id'] = 'id'.$box_type.'BoxAlert'.$data['master_var'];
$data['box_loader_id'] = 'id'.$box_type.'BoxLoader'.$data['master_var'];
$data['box_content_id'] = 'id'.$box_type.'BoxContent'.$data['master_var'];
$data['box_overlay_id'] = 'id'.$box_type.'BoxOverlay'.$data['master_var'];
/* --END OF BOX DEFAULT PROPERTY-- */

/* --SET VARIABEL TAMBAHAN DISINI-- */
$data['wo_kd'] = $wo_kd;
$data['box_form_content_id'] = 'id'.$box_type.'BoxFormContent'.$data['master_var'];
/* --END OF CUSTOM PROPERTY-- */

/* --SET BUTTONS DISINI, NILAI "FALSE" UNTUK TIDAK TAMPIL, NILAI "TRUE" UNTUK MENAMPILKAN BUTTON-- */
$btn_hide = TRUE;
$btn_add = FALSE;
$btn_remove = TRUE;
/* --END OF BUTTONS SETUP-- */
?>

<div class="box box-warning" id="<?php echo $data['box_id']; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_title; ?></h3>
		<div class="box-tools pull-right">
			<?php
			if ($btn_hide) :
				?>
				<button class="btn btn-box-tool btn-collapse" id="<?php echo $data['btn_hide_id']; ?>" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
					<i class="fa fa-minus"></i>
				</button>
				<?php
			endif;
			if ($btn_add) :
				?>
				<button class="btn btn-box-tool" id="<?php echo $data['btn_add_id']; ?>" data-toggle="tooltip" title="Tambah Data">
					<i class="fa fa-plus"></i>
				</button>
				<?php
			endif;
			if ($btn_remove) :
				?>
				<button class="btn btn-box-tool btn-remove" id="<?php echo $data['btn_remove_id']; ?>" data-widget="remove" data-toggle="tooltip" title="Close">
					<i class="fa fa-times"></i>
				</button>
				<?php
			endif;
			?>
		</div>
	</div>
	<div class="box-body">
		<div id="<?php echo $data['box_alert_id']; ?>"></div>
		<div id="<?php echo $data['box_loader_id']; ?>" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div class="row invoice-info">
			<div class="col-sm-6 invoice-col">
				<b>Tanggal Work Order :</b> #<?=format_date($wo['wo_tanggal'], 'd-M-Y')?><br>
				<b>Status :</b> <?php echo bg_label(process_status($wo['wo_status']), color_status($wo['wo_status'])); ?>
			</div>
			<div class="col-sm-6 invoice-col">
				<b>No Terbit</b> #<?php echo isset($wo['wo_noterbit']) ? $wo['wo_noterbit']: '-'; ?> <br>
				<b>No Sales Order</b> #<?php echo isset($no_salesorders) ? $no_salesorders: '-'; ?>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<!-- <button class="btn btn-sm btn-warning" onclick="itemdetail_formpyr ('add', '<?php echo $wo_kd;?>', '')"><i class="fa fa-plus"></i> Tambah Item PYR</button> -->
				<button class="pull-right btn btn-sm btn-default" onclick="window.location.assign('<?php echo base_url().$class_link?>')"> <i class="fa fa-arrow-circle-left"></i> Kembali </button>
			</div>
		</div>
		<hr>
		<div id="<?php echo $data['box_form_content_id']; ?>"></div>
		<div id="<?php echo $data['box_content_id']; ?>"></div>
	</div>
	<div class="box-footer">
	</div>
	<div class="overlay" id="<?php echo $data['box_overlay_id']; ?>" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
	<?php $this->load->view('page/'.$class_link.'/'.$js_file, $data); ?>
</div>