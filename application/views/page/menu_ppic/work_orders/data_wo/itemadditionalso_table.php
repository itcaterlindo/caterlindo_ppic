<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$form_id = 'idFormaddSO';
?>
<style type="text/css">
    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }

    td.dt-green {
        background-color: green;
        color: white;
    }
</style>
<?php
echo form_open('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idadditionalsowo_kd', 'name' => 'additionalsowo_kd', 'value' => isset($wo_kd) ? $wo_kd : null));
?>
<table id="idtableAdditionalso" class="table table-bordered table-striped table-hover display responsive">
    <thead>
        <tr style="font-size:95%;">
            <th style="text-align: center; width: 1%;">No.</th>
            <th style="text-align: center; width: 1%;">
                <input type="checkbox" class="icheck headercheck">
            </th>
            <th style="text-align: center; width: 3%;">No Salesorder</th>
            <th style="text-align: center; width: 10%;">Prod Code</th>
            <th style="text-align: center; width: 3%;">Status</th>
            <th style="text-align: center; width: 15%;">Deskripsi</th>
            <th style="text-align: center; width: 15%;">Dimensi</th>
            <th style="text-align: center; width: 2%;">Qty</th>
            <th style="text-align: center; width: 3%;">Status</th>
        </tr>
    </thead>
    <tbody style="font-size:100%;">
        <?php
        $no = 1;
        foreach ($itemsoadditional as $r_itemsoadditional) :
        ?>
            <tr>
                <td><?= $no; ?></td>
                <td style="text-align: center;">
                    <input type="checkbox" class="icheck chkwoitemso" name="soitems[]" value="<?php echo $r_itemsoadditional['msalesorder_kd'] . '#' . $r_itemsoadditional['parent_kd'] . '#' . $r_itemsoadditional['child_kd'] ?>">
                </td>
                <td><?php
                    foreach ($salesorders as $r_salesorder) {
                        if ($r_salesorder['kd_msalesorder'] == $r_itemsoadditional['msalesorder_kd']) {
                            $no_salesorder = $r_salesorder['no_salesorder'];
                            if ($r_salesorder['tipe_customer'] == 'Ekspor') {
                                $no_salesorder = $r_salesorder['no_po'];
                            }
                        }
                    }
                    echo $no_salesorder;
                    ?></td>
                <td><?= $r_itemsoadditional['item_code']; ?></td>
                <td><?= $r_itemsoadditional['item_status'];; ?></td>
                <td><?= $r_itemsoadditional['item_desc'];; ?></td>
                <td><?= $r_itemsoadditional['item_dimension'];; ?></td>
                <td style="text-align: right;"><?= $r_itemsoadditional['item_qty']; ?></td>
                <td style="text-align: center;"><?= $r_itemsoadditional['order_tipe']; ?></td>
            </tr>
        <?php
            $no++;
        endforeach;
        ?>
    </tbody>
    <tfoot>
    </tfoot>
</table>

<div class="col-md-12">
    <button id="idbtnSubmitMain" onclick="submitDataAdditionalSO()" class="btn btn-sm btn-primary pull-right">
        <i class="fa fa-plus"></i> Add Item
    </button>
</div>

<?php echo form_close(); ?>
<script type="text/javascript">
    render_icheck("icheck");
    
    $('.headercheck').on('ifChanged', function(e) {
        var headerchecked = e.target.checked;
        if (headerchecked == true) {
            $('.chkwoitemso').iCheck('check');
        } else {
            $('.chkwoitemso').iCheck('uncheck');
        }
    });

    function render_icheck(classElement) {
        $('input[type="checkbox"].' + classElement).iCheck({
            checkboxClass: 'icheckbox_flat-blue'
        })
    }


</script>