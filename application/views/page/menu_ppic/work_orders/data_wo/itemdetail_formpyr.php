<?php
defined('BASEPATH') or exit('No direct script access allowed');
$form_id = 'idFormAddWOitemdetailPYR';

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtslugWOitemdetail', 'name' => 'txtslugWOitemdetail', 'value' => $slug));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtwo_kd', 'name' => 'txtwo_kd', 'value' => isset($wo_kd) ? $wo_kd : null));
echo form_input(array('type' => 'hidden', 'id' => 'idwoitem_kd', 'name' => 'woitem_kd'));
echo form_input(array('type' => 'hidden', 'id' => 'idwoitem_qty', 'name' => 'woitem_qty'));
echo form_input(array('type' => 'hidden', 'id' => 'idwoitem_status', 'name' => 'woitem_status'));
?>

<div class="col-md-6">

    <div class="form-group">
        <label for="idwoitem_no_wo" class="col-md-2 control-label">No WO</label>
        <div class="col-md-6">
            <div id="idErrwoitem_no_wo"></div>
            <input type="text" name="woitem_no_wo" id="idwoitem_no_wo" class="form-control" placeholder="No WO" autocomplete="off" spellcheck="false">
        </div>
    </div>
    <table border="1" style="width: 100%;">
        <tr>
            <td width="15%" style="font-weight: bold;">ItemCode</td>
            <td width="35%">
                <div id="idErrwoitem_itemcode"></div>
                <div id="idwoitem_itemcode" style="padding-top:6px;">-</div>
            </td>
            <td width="15%" style="font-weight: bold;">Jenis</td>
            <td width="35%"><div id="idwoitem_jenis" style="padding-top:6px;">-</div></td>
        </tr>
        <tr>
            <td style="font-weight: bold;">Qty</td>
            <td><div id="idtxtwoitem_qty" style="padding-top:6px;">-</div></td>
            <td style="font-weight: bold;">Status</td>
            <td><div id="idtxtwoitem_status" style="padding-top:6px;">-</div></td>
        </tr>
        <tr>
            <td style="font-weight: bold;">Tgl Selesai</td>
            <td><div id="idwoitem_tglselesai" style="padding-top:6px;">-</div></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <br>
    <table border="1" style="width: 100%;">
        <tr>
            <td style="font-weight: bold;">Part :</td>
        </tr>
        <tr>
            <td> <div id="idpartdetailwo"></div> </td>
        </tr>
    </table>
</div>

<div class="col-md-6">

    <div class="form-group">
        <label for="idqty_pyr" class="col-md-2 control-label">Qty PYR</label>
        <div class="col-md-3">
            <div id="idErrqty_pyr"></div>
            <input type="number" name="qty_pyr" id="idqty_pyr" class="form-control tt-input" placeholder="Qty PYR">
        </div>
        <label for="idtxtwoitem_satuan" class="col-md-2 control-label">Satuan</label>
        <div class="col-md-3">
            <?php echo form_dropdown('txtwoitem_satuan', isset($opsiSatuan) ? $opsiSatuan : array(), isset($satuanSelected) ? $satuanSelected : null, array('id' => 'idtxtwoitem_satuan', 'class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <label for="idtxtwoitem_tglselesai" class="col-md-2 control-label">Tgl Selesai</label>
        <div class="col-md-4">
            <div id="idErrwoitemdetail_tglselesai"></div>
            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="text" name="txtwoitem_tglselesai" id="idtxtwoitem_tglselesai" class="form-control datetimepicker tt-input" placeholder="Tgl Selesai" value="<?= isset($woitem_tglselesai) ? $woitem_tglselesai : null; ?>">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="idtxtwoitem_note" class="col-md-2 control-label">Keterangan</label>
        <div class="col-md-9">
            <div id="idErrwoitemdetail_note"></div>
            <textarea name="txtwoitem_note" cols="40" rows="3" id="idtxtwoitem_note" class="form-control  tt-input" placeholder="Keterangan"><?php echo isset($woitem_note) ? $woitem_note : 'PYR'; ?></textarea>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-4 col-sm-offset-9 col-xs-12">
        <button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitDataWOItemPyr()" class="btn btn-sm btn-primary">
            <i class="fa fa-save"></i> Simpan
        </button>
        <button type="reset" name="btnReset" class="btn btn-sm btn-default">
            <i class="fa fa-refresh"></i> Reset
        </button>
        <button type="text" name="btnClose" class="btn btn-sm btn-warning" onclick="tutupForm()">
            <i class="fa fa-close"></i> Tutup
        </button>
    </div>
</div>
<hr>

<!-- /.box-body -->
<?php echo form_close(); ?>

<script type="text/javascript">
    <?php if ($slug == 'edit') : ?>
        item_type('<?php echo $woitemdetail_itemstatus; ?>');
        $('.classEdit').slideDown('fast');
        $('.classAdd').slideUp('fast');
        $('#iddivbom_kd').slideUp('fast');
    <?php endif; ?>

    /* --start product typeahead.js-- */
    var Product = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        url: '<?php echo base_url(); ?>auto_complete/get_no_wo',
        remote: {
            url: '<?php echo base_url(); ?>auto_complete/get_no_wo',
            prepare: function(query, settings) {
                settings.type = 'GET';
                settings.contentType = 'application/json; charset=UTF-8';
                settings.data = 'woitem_no_wo=' + query;

                return settings;
            },
            wildcard: '%QUERY'
        }
    });

    $('#idwoitem_no_wo').typeahead(null, {
        limit: 50,
        minLength: 2,
        display: 'woitem_no_wo',
        source: Product.ttAdapter()
    });

    $('#idwoitem_no_wo').bind('typeahead:select', function(obj, selected) {
        $('#idwoitem_itemcode').text(selected.woitem_itemcode);
        $('#idtxtwoitem_qty').text(selected.woitem_qty);
        $('#idwoitem_itemcode').text(selected.woitem_itemcode);
        $('#idtxtwoitem_status').text(selected.woitem_status.toUpperCase());
        $('#idwoitem_tglselesai').text(selected.woitem_tglselesai);
        $('#idwoitem_jenis').text(selected.woitem_jenis.toUpperCase());
        $('#idwoitem_kd').val(selected.woitem_kd);
        $('#idwoitem_qty').val(selected.woitem_qty);
        $('#idwoitem_status').val(selected.woitem_status);
        $('#idqty_pyr').focus();
        if (selected.woitem_jenis == 'packing') {
            // Get Detail Part
            getDetailPartWoitemPacking(selected.woitem_kd);
        }
    });
    /* --end of product typeahead.js-- */

    function getDetailPartWoitemPacking(woitem_kd){
        $.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/get_item_woitemdetail_packing',
			data: {
				woitem_kd: woitem_kd
			},
			success: function(data) {
                var resp = JSON.parse(data);
                html = '';
                $.each( resp, function( key, value ) {
                    html += '<label><input type="checkbox" name="txtwoitemdetail_kds[]" value="'+value.woitemdetail_kd+'"> '+value.partmain_nama+' </label><br>';
                });
				$('#idpartdetailwo').html(html);
			}
		});
    }
</script>