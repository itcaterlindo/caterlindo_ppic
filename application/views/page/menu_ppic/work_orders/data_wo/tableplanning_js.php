<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	tableplanning_main('<?php echo $wo_kd; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('form_add_master', '');
	});

	function tableplanning_main(wo_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/tableplanning_main',
			data: {
				wo_kd: wo_kd
			},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
			}
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}
</script>