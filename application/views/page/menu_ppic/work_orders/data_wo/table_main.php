<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}
</style>

<table id="idTable" class="table table-bordered table-striped table-hover display responsive" style="width:100%;">
	<thead>
		<tr>
			<th style="width:1%; text-align:center;">No.</th>
			<th style="width:1%; text-align:center;">Opsi</th>
			<th style="width:7%; text-align:center;">Tanggal WO</th>
			<th style="width:7%; text-align:center;">No Terbit</th>
			<th style="width:30%; text-align:center;">No PO</th>
			<th style="width:30%; text-align:center;">Note</th>
			<th style="width:7%; text-align:center;">WO Status</th>
			<th style="width:7%; text-align:center;">UpdatedAt</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		foreach ($wos as $wo) : ?>
			<tr>
				<td><?php echo $no; ?></td>
				<td>
					<?php
					$btns = [];
					$btns[] = get_btn(array('title' => 'Detail Item', 'icon' => 'list', 'onclick' => 'detail_wo_item(\'' . $wo['wo_kd'] . '\')'));
					if ($wo['wo_status'] == 'process_wo') {
						$btns[] = get_btn(array('title' => 'Detail Work Order', 'icon' => 'list', 'onclick' => 'itemdetail_box(\'' . $wo['wo_kd'] . '\')'));
					}
					if ($wo['wo_status'] == 'pending') {
						$btns[] = get_btn_divider();
						$btns[] = get_btn(array('title' => 'Cancel Work Order', 'icon' => 'times-circle', 'onclick' => 'cancel_wo(\'' . $wo['wo_kd'] . '\')'));
					}

					$btn_group = group_btns($btns);

					echo $btn_group;
					?>
				</td>
				<td><?php echo $wo['wo_tanggal']; ?></td>
				<td><?php echo $wo['wo_noterbit']; ?></td>
				<td><?php echo $wo['no_salesorders']; ?></td>
				<td><?php echo $wo['wo_note']; ?></td>
				<td><?php
					$word = process_status($wo['wo_status']);
					$color = color_status($wo['wo_status']);
					echo bg_label($word, $color); ?>
				</td>
				<td><?php echo $wo['wo_tgledit']; ?></td>
			</tr>
		<?php
			$no++;
		endforeach; ?>
	</tbody>
</table>

<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"scrollX": false,
			"paging": true,
			"language": {
				"lengthMenu": "Tampilkan _MENU_ data",
				"zeroRecords": "Maaf tidak ada data yang ditampilkan",
				"info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty": "Tidak ada data yang ditampilkan",
				"search": "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing": "Sedang Memproses...",
				"paginate": {
					"first": '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last": '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next": '<span class="glyphicon glyphicon-forward"></span>',
					"previous": '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"order":[7, 'desc'],
		});
	}
</script>