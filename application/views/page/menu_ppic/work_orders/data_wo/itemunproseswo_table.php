<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }

    td.dt-green {
        background-color: green;
        color: white;
    }
</style>
<?php
$form_id = 'idFormAddUnprosesWO';

echo form_open('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtunproseswo_kd', 'name' => 'txtunproseswo_kd', 'value' => isset($wo_kd) ? $wo_kd : null));
?>
<table id="idtableUnprosesWO" class="table table-bordered table-striped table-hover display responsive">
    <thead>
        <tr style="font-size:95%;">
            <th style="text-align: center; width: 1%;">No.</th>
            <th style="text-align: center; width: 1%;">
                <input type="checkbox" class="icheck headercheck">
            </th>
            <th style="text-align: center; width: 3%;">No Terbit</th>
            <th style="text-align: center; width: 5%;">Sales Order</th>
            <th style="text-align: center; width: 10%;">Prod Code</th>
            <th style="text-align: center; width: 3%;">Status</th>
            <th style="text-align: center; width: 15%;">Deskripsi</th>
            <th style="text-align: center; width: 15%;">Dimensi</th>
            <th style="text-align: center; width: 2%;">Qty</th>
            <th style="text-align: center; width: 3%;">Status</th>
        </tr>
    </thead>
    <tbody style="font-size:100%;">
        <?php
        $no = 1;
        foreach ($items as $r_item) :
        ?>
            <tr>
                <td><?= $no; ?></td>
                <td style="text-align: center;">
                    <input type="checkbox" class="icheck chkwoitemso" name="woitemsodetailkds[]" value="<?php echo $r_item['woitemsodetail_kd'] ?>">
                    <input type="hidden" name="woitemsokds[<?php echo $r_item['woitemsodetail_kd']; ?>]" value="<?php echo $r_item['woitemso_kd'] ?>">
                </td>
                <td><?= $r_item['wo_noterbit']; ?></td>
                <td><?php
                    $no_salesorder = $r_item['no_salesorder'];
                    if ($r_item['tipe_customer'] == 'Ekspor') {
                        $no_salesorder = $r_item['no_po'];
                    }
                    echo $no_salesorder;
                    ?></td>
                <td><?= $r_item['woitemso_itemcode']; ?></td>
                <td><?= $r_item['woitemso_status'];; ?></td>
                <td><?= $r_item['woitemso_itemdeskripsi'];; ?></td>
                <td><?= $r_item['woitemso_itemdimensi'];; ?></td>
                <td style="text-align: right;"><?= $r_item['woitemso_qty']; ?></td>
                <td style="text-align: center;">
                    <?php
                    $msg = strtoupper(str_replace('_', ' ', $r_item['woitemso_prosesstatus']));
                    switch ($r_item['woitemso_prosesstatus']) {
                        case 'editing':
                            $type = 'warning';
                            break;
                        case 'null_bom':
                            $type = 'danger';
                            break;
                        case 'workorder':
                            $type = 'success';
                            break;
                    }
                    echo buildLabel($type, $msg);
                    ?></td>
            </tr>
        <?php
            $no++;
        endforeach;
        ?>
    </tbody>
    <tfoot>
    </tfoot>
</table>

<div class="col-md-12">
    <button id="idbtnSubmitMain" onclick="submitDataUnprosesWO()" class="btn btn-primary btn-sm pull-right">
        <i class="fa fa-plus"></i> Add Item
    </button>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
    $('.headercheck').on('ifChanged', function(e) {
        var headerchecked = e.target.checked;
        if (headerchecked == true) {
            $('.chkwoitemso').iCheck('check');
        } else {
            $('.chkwoitemso').iCheck('uncheck');
        }
    });

    render_icheck('icheck');
    render_dt('#idtableUnprosesWO');

    function submitDataUnprosesWO() {
        event.preventDefault();
        var form = document.getElementById('idFormAddUnprosesWO');
        var url = '<?php echo base_url() . 'menu_ppic/data_wo/action_submit_unproseswo'; ?>';

        $.ajax({
            url: url,
            type: "POST",
            data: new FormData(form),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                console.log(data);
                var resp = JSON.parse(data);
                if (resp.code == 200) {
                    window.location.reload();
                    notify(resp.status, resp.pesan, 'success');
                    generateToken(resp.csrf);
                } else if (resp.code == 401) {
                    $.each(resp.pesan, function(key, value) {
                        $('#' + key).html(value);
                    });
                    generateToken(resp.csrf);
                } else if (resp.code == 400) {
                    notify(resp.status, resp.pesan, 'error');
                    generateToken(resp.csrf);
                } else {
                    notify('Error', 'Error tidak Diketahui', 'error');
                    generateToken(resp.csrf);
                }
            }
        });
    }

    function render_icheck(classElement) {
        $('input[type="checkbox"].' + classElement).iCheck({
            checkboxClass: 'icheckbox_flat-blue'
        })
    }

    function render_dt(table_elem) {
        $(table_elem).DataTable({
            // "scrollX": "100%",
            // "scrollY": "400px",
            "autoWidth": true,
            "responsive": false,
            "paging": false,
            "language": {
                "lengthMenu": "Tampilkan _MENU_ data",
                "zeroRecords": "Maaf tidak ada data yang ditampilkan",
                "info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
                "infoFiltered": "(difilter dari _MAX_ total data)",
                "infoEmpty": "Tidak ada data yang ditampilkan",
                "search": "Cari :",
                "loadingRecords": "Memuat Data...",
                "processing": "Sedang Memproses...",
                "paginate": {
                    "first": '<span class="glyphicon glyphicon-fast-backward"></span>',
                    "last": '<span class="glyphicon glyphicon-fast-forward"></span>',
                    "next": '<span class="glyphicon glyphicon-forward"></span>',
                    "previous": '<span class="glyphicon glyphicon-backward"></span>'
                }
            },
            "columnDefs": [
                // {"searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
                {
                    "searchable": false,
                    "orderable": false,
                    "targets": 1
                },
            ],
        });
    }
</script>