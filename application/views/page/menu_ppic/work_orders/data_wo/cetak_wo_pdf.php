<?php
class customPdf extends Tcpdf {

    public $wo_tanggal;
    public function setsFooter ($val) {
        $this->wo_tanggal = $val;
    }
    
    public function Header() {
        $header = '
        <table cellspacing="0" cellpadding="0" border="1" width="100%" style="font-size:11px;">
            <tr>
                <td width="15%" rowspan="2" style="text-align:left;"> <img src="assets/admin_assets/dist/img/logo_cat.png" width="100" height="35"> </td>
                <td width="45%" style="text-align:center; font-weight:bold;"> FORMULIR </td>
                <td width="15%" rowspan="2" style="text-align:left;"> No Dokumen / Rev <br> Tanggal Terbit <br> Halaman </td>
                <td width="25%" rowspan="2" style="text-align:left;"> : CAT4-052 / 01 <br> : 22-01-2019 <br> : </td>
            </tr>
            <tr>
                <td style="text-align:center; font-size: 16px; font-weight:bold;"> WORK ORDER </td>
            </tr>
        </table>';

        $this->writeHTML($header, true, false, false, false, '');
    }

    public function Footer () {
        $ttd = 
            '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:9px;">
                <tr>
                    <td style="text-align:center;">'.format_date($this->wo_tanggal, 'd-M-Y').'</td>
                    <td style=""></td>
                    <td style="text-align:center;">'.format_date($this->wo_tanggal, 'd-M-Y').'</td>
                </tr>
                <tr>
                    <td style="text-align:center;">Dibuat oleh,</td>
                    <td style=""></td>
                    <td style="text-align:center;">Mengetahui,</td>
                </tr>
                <tr>
                    <td height="60" style="text-align:center;"></td>
                    <td height="60" style=""></td>
                    <td height="60" style="text-align:center;"></td>
                </tr>
                <tr>
                    <td style="text-align:center;">PPIC</td>
                    <td style=""></td>
                    <td style="text-align:center;">Head of Production</td>
                </tr>
               
            </table>';
            
        $this->writeHTML($ttd, true, false, false, false, '');
    }

}

$pdf = new customPdf('L', 'mm', 'A4', true, 'UTF-8', false);
$title = 'WO';
$pdf->SetTitle($title);
$pdf->SetAutoPageBreak(true);
$pdf->SetMargins(10, 10, 10, true);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');
;
/** Margin */ 
$pdf->SetHeaderMargin(5);
$pdf->SetFooterMargin(45);
$pdf->SetMargins(8, 25, 8);

/** Footer Data */
$pdf->setsFooter($rowWO->wo_tanggal);

$pdf->AddPage();
$konten =
        '<table cellspacing="0" cellpadding="1" border="1" style="font-size:9px">
            <tr>
                <th width="3%" rowspan="2" style="text-align: center; font-weight:bold;">No.</th>
                <th width="7%" rowspan="2" style="text-align: center; font-weight:bold;">WO / No. Project</th>
                <th width="17%" rowspan="2" style="text-align: center; font-weight:bold;">Kode Produk</th>
                <th width="13%" rowspan="2" style="text-align: center; font-weight:bold;">Dimensi</th>
                <th width="25%"colspan="5" style="text-align: center; font-weight:bold;">Jumlah</th>
                <th width="5%" rowspan="2" style="text-align: center; font-weight:bold;">Satuan</th>
                <th width="21%" rowspan="2" style="text-align: center; font-weight:bold;">Keterangan</th>
                <th width="8%" rowspan="2" style="text-align: center; font-weight:bold;">Tanggal Selesai</th>
            </tr>
            <tr>
                <th style="text-align: center; font-weight:bold;">Project</th>
                <th style="text-align: center; font-weight:bold;">Top</th>
                <th style="text-align: center; font-weight:bold;">Solid / CB</th>
                <th style="text-align: center; font-weight:bold;">Sink Bowl</th>
                <th style="text-align: center; font-weight:bold;">Pipa</th>
            </tr>';

/** Result Data */
if (!empty($items)):
$no = 1;
foreach($items as $item):
    $woitemdetail_tglselesai = $item->woitemdetail_tglselesai;
    $konten .= 
        '<tr>
            <td style="text-align:right;">'.$no.'</td>
            <td style="text-align:center;">'.$item->woitemdetail_no_wo.'</td>
            <td style="text-align:left;">'.$item->woitemdetail_itemcode.'</td>
            <td style="text-align:left;">'.$item->woitemdetail_itemdimension.'</td>
            <td style="text-align:right;">'.$item->woitemdetail_project.'</td>
            <td style="text-align:right;">'.$item->woitemdetail_top.'</td>
            <td style="text-align:right;">'.$item->woitemdetail_solid.'</td>
            <td style="text-align:right;">'.$item->woitemdetail_sinkbowl.'</td>
            <td style="text-align:right;">'.$item->woitemdetail_pipa.'</td>
            <td style="text-align:center;">'.ucwords($item->woitemdetail_satuan).'</td>
            <td style="text-align:left;">'.$item->woitemdetail_keterangan.'</td>
            <td style="text-align:left;">'.format_date($woitemdetail_tglselesai, 'd-M-Y').'</td>
        </tr>';
    $no++;
endforeach;

endif;


$konten .=
        '</table>';

$pdf->writeHTML($konten, true, false, false, false, '');

$txtOutput = $title.'.pdf';
$pdf->Output($txtOutput, 'I');