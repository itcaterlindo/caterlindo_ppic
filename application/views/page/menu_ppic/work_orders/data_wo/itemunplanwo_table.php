<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }

    td.dt-green {
        background-color: green;
        color: white;
    }
</style>

<table id="idtableUnplanWO" class="table table-bordered table-striped table-hover display responsive nowrap">
    <thead>
        <tr style="font-size:95%;">
            <th style="text-align: center; width: 1%;">No.</th>
            <th style="text-align: center; width: 3%;">Opsi</th>
            <th style="text-align: center; width: 10%;">No Terbit</th>
            <th style="text-align: center; width: 10%;">Item Product</th>
            <th style="text-align: center; width: 10%;">Item Deskripsi</th>
            <th style="text-align: center; width: 10%;">Item Dimensi</th>
            <th style="text-align: center; width: 3%;">Qty</th>
            <th style="text-align: center; width: 5%;">Item SO Status</th>
        </tr>
    </thead>
    <tbody style="font-size:100%;">
        <?php
        $no = 1;
        foreach ($items as $r_item) :
        ?>
            <tr>
                <td style="text-align: center;"><?= $no; ?></td>
                <td style="text-align: center;">
                    <div class="btn-group">
                        <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Opsi <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);" onclick="itemunplanwo_form('<?php echo $wo_kd; ?>', '<?php echo $r_item['woitemsoadd_kd']; ?>')"> <i class="fa fa-check"></i> Planning SO</a></li>
                        </ul>
                    </div>
                </td>
                <td><?= $r_item['wo_noterbit']; ?></td>
                <td><?= $r_item['woitemsoadd_itemcode']; ?></td>
                <td><?= $r_item['woitemsoadd_itemdeskripsi']; ?></td>
                <td><?= $r_item['woitemsoadd_itemdimensi']; ?></td>
                <td style="text-align: right;"><?= $r_item['woitemsoadd_qty']; ?></td>
                <td style="text-align: center;">
                    <?php
                    $msg = strtoupper(str_replace('_', ' ', $r_item['woitemso_prosesstatus']));
                    switch ($r_item['woitemso_prosesstatus']) {
                        case 'editing':
                            $type = 'warning';
                            break;
                        case 'null_bom':
                            $type = 'danger';
                            break;
                        case 'workorder':
                            $type = 'success';
                            break;
                    }
                    echo buildLabel($type, $msg);
                    ?></td>
            </tr>
        <?php
            $no++;
        endforeach;
        ?>
    </tbody>
    <tfoot>
    </tfoot>
</table>
<!-- </div> -->
<script type="text/javascript">
    render_dt('#idtableUnplanWO');

    function render_dt(table_elem) {
        $(table_elem).DataTable({
            "autoWidth": true,
            "responsive": false,
            "paging": false,
            "language": {
                "lengthMenu": "Tampilkan _MENU_ data",
                "zeroRecords": "Maaf tidak ada data yang ditampilkan",
                "info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
                "infoFiltered": "(difilter dari _MAX_ total data)",
                "infoEmpty": "Tidak ada data yang ditampilkan",
                "search": "Cari :",
                "loadingRecords": "Memuat Data...",
                "processing": "Sedang Memproses...",
                "paginate": {
                    "first": '<span class="glyphicon glyphicon-fast-backward"></span>',
                    "last": '<span class="glyphicon glyphicon-fast-forward"></span>',
                    "next": '<span class="glyphicon glyphicon-forward"></span>',
                    "previous": '<span class="glyphicon glyphicon-backward"></span>'
                }
            },
            "columnDefs": [
                {
                    "searchable": false,
                    "orderable": false,
                    "targets": 1
                },
            ],
        });
    }
</script>