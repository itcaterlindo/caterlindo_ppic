<?php
defined('BASEPATH') or exit('No direct script access allowed');
$form_id = 'idFormAddWOitemEdit';
if ($slug == 'edit') {
    extract($rowData);
    $woitem_tglselesai = format_date($woitem_tglselesai, 'd-m-Y');
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtslugWOitemdetail', 'name' => 'txtslugWOitemdetail', 'value' => $slug));
echo form_input(array('type' => 'hidden', 'id' => 'idwoitem_kd', 'name' => 'woitem_kd', 'value' => isset($woitem_kd) ? $woitem_kd : null));
?>

<div class="col-md-4">
    <div class="form-group">
        <label for="idwoitem_no_wo" class="col-md-2 control-label">No WO</label>
        <div class="col-md-6">
            <div id="idErrwoitem_no_wo"></div>
            <div id="idwoitem_no_wo" style="padding-top:6px;"><?php echo isset($woitem_no_wo) ? $woitem_no_wo : '-' ?></div>
        </div>
    </div>
    <div class="form-group">
        <label for="idwoitem_itemcode" class="col-md-2 control-label">ItemCode</label>
        <div class="col-md-8">
            <div id="idErrwoitem_itemcode"></div>
            <div id="idwoitem_itemcode" style="padding-top:6px;"><?php echo isset($woitem_itemcode) ? $woitem_itemcode : '-' ?></div>
        </div>
    </div>
    <div class="form-group">
        <label for="idtxtwoitem_qty" class="col-md-2 control-label">Qty</label>
        <div class="col-md-8">
            <div id="idtxtwoitem_qty" style="padding-top:6px;"><?php echo isset($woitem_qty) ? $woitem_qty : '-' ?></div>
        </div>
    </div>
    <div class="form-group">
        <label for="idtxtwoitem_status" class="col-md-2 control-label">Status</label>
        <div class="col-md-8">
            <div id="idtxtwoitem_status" style="padding-top:6px;"><?php echo isset($woitem_status) ? $woitem_status : '-' ?></div>
        </div>
    </div>

</div>

<div class="col-md-8">
    <div class="form-group">
        <label for="idtxtwoitem_deskripsi" class="col-md-2 control-label">Deskripsi</label>
        <div class="col-md-9">
            <div id="idErrwoitemdetail_deskripsi"></div>
            <textarea name="txtwoitem_deskripsi" cols="40" rows="2" id="idtxtwoitem_deskripsi" class="form-control  tt-input" placeholder="Deskripsi"><?php echo isset($woitem_deskripsi) ? $woitem_deskripsi : null; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="idtxtwoitem_dimensi" class="col-md-2 control-label">Dimensi</label>
        <div class="col-md-9">
            <div id="idErrwoitemdetail_dimensi"></div>
            <textarea name="txtwoitem_dimensi" cols="40" rows="2" id="idtxtwoitem_dimensi" class="form-control  tt-input" placeholder="Dimensi"><?php echo isset($woitem_dimensi) ? $woitem_dimensi : null; ?></textarea>
        </div>
    </div>

    <div class="form-group">
        <label for="idtxtwoitem_satuan" class="col-md-2 control-label">Satuan</label>
        <div class="col-md-3">
            <?php echo form_dropdown('txtwoitem_satuan', isset($opsiSatuan) ? $opsiSatuan : array(), isset($woitem_satuan) ? $woitem_satuan : null, array('id' => 'idtxtwoitem_satuan', 'class' => 'form-control')); ?>
        </div>
        <label for="idtxtwoitem_tglselesai" class="col-md-2 control-label">Tgl Selesai</label>
        <div class="col-md-4">
            <div id="idErrwoitemdetail_tglselesai"></div>
            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="text" name="txtwoitem_tglselesai" id="idtxtwoitem_tglselesai" class="form-control datetimepicker tt-input" placeholder="Tgl Selesai" value="<?= isset($woitem_tglselesai) ? $woitem_tglselesai : null; ?>">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="idtxtwoitem_note" class="col-md-2 control-label">Keterangan</label>
        <div class="col-md-9">
            <div id="idErrwoitemdetail_note"></div>
            <textarea name="txtwoitem_note" cols="40" rows="2" id="idtxtwoitem_note" class="form-control  tt-input" placeholder="Keterangan"><?php echo isset($woitem_note) ? $woitem_note : null; ?></textarea>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-4 col-sm-offset-9 col-xs-12">
        <button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitDataWOItemEdit()" class="btn btn-sm btn-primary">
            <i class="fa fa-save"></i> Simpan
        </button>
        <button type="reset" name="btnReset" class="btn btn-sm btn-default">
            <i class="fa fa-refresh"></i> Reset
        </button>
        <button type="text" name="btnClose" class="btn btn-sm btn-warning" onclick="tutupForm()">
            <i class="fa fa-close"></i> Tutup
        </button>
    </div>
</div>
<hr>

<!-- /.box-body -->
<?php echo form_close(); ?>

<script type="text/javascript">

</script>