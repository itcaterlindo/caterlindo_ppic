<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	form_item_main('<?php echo $wo_kd; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function form_item_main(wo_kd) {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/form_item_main'; ?>',
				data: {
					wo_kd: wo_kd
				},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').slideDown().html(html);
					moveTo('idMainContent');
				}
			});
		});
	}

	function open_form_item(slug, wo_kd, woitemso_kd) {
		box_overlay_formitem('in');
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/form_item',
			data: {
				slug: slug,
				wo_kd: wo_kd,
				woitemso_kd: woitemso_kd
			},
			success: function(html) {
				$('#idAddItem').html(html);
				render_select2('.select2');
				render_datetimepicker('datepicker')
				if (slug == 'add') {
					get_bom('std', '');
				}
				box_overlay_formitem('out');
			}
		});
	}

	function open_form_table_item(wo_kd) {
		box_overlay_formitem('in');
		event.preventDefault();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/form_item_table',
			data: {
				wo_kd: wo_kd
			},
			success: function(html) {
				$('#idTableItem').html(html);
				box_overlay_formitem('out');
			}
		});
	}

	function open_form_table_item_planning(wo_kd) {
		box_overlay_formitem('in');
		event.preventDefault();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/form_item_table_planning',
			data: {
				wo_kd: wo_kd
			},
			success: function(html) {
				$('#idTableItem').html(html);
				box_overlay_formitem('out');
			}
		});
	}

	function get_bom(bom_jenis, selected) {
		var optBom = $('.classBom_kd');
		$('.classBom_kd option').remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/get_bom'; ?>',
			data: {
				'bom_jenis': bom_jenis
			},
			success: function(resp) {
				if (resp.code == 200) {
					$.each(resp.data, function(val, text) {
						var val = text.itemcode;
						if (text.bom_jenis == 'custom') {
							val = text.itemcode + ' | ' + text.bom_ket;
						}
						optBom.append($('<option></option>').val(text.bom_kd).html(text.itemcode + ' | ' + text.bom_ket));
					});
				}
				$('.classBom_kd').val(selected).trigger('change');
			}
		});
	}

	function itemunproseswo_box(wo_kd) {
		event.preventDefault();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/itemunproseswo_box',
			data: {
				wo_kd: wo_kd
			},
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function itemunplanwo_box(wo_kd) {
		event.preventDefault();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/itemunplanwo_box',
			data: {
				wo_kd: wo_kd
			},
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function itemadditionalso_box(wo_kd) {
		event.preventDefault();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/itemadditionalso_box',
			data: {
				wo_kd: wo_kd
			},
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function open_form_table_so(wo_kd) {
		box_overlay_formitem('in');
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/form_item_table_so',
			data: {
				wo_kd: wo_kd
			},
			success: function(html) {
				$('#idTableItem').html(html);
				box_overlay_formitem('out');
			}
		});
	}

	function process_wo(wo_kd, jenisProses) {
		event.preventDefault();
		var conf = confirm('Apakah kamu yakin ?');
		if (conf) {
			$('#idbtnProcessWO').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
			$('#idbtnProcessWO').attr('disabled', true);

			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link ?>/action_process_wo',
				data: {
					wo_kd: wo_kd,
					jenisProses: jenisProses
				},
				success: function(resp) {
					console.log(resp);
					if (resp.code == 200) {
						$('#idFormBoxWorkOrder').remove();
						window.location.reload();
						notify(resp.status, resp.pesan, 'success');
					}
				}
			});
		}
	}

	function process_wo_step1(wo_kd, jenisProses = null) {
		event.preventDefault();
		var conf = confirm('Apakah kamu yakin ?');
		if (conf) {
			$('#idbtnProcessWO').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
			$('#idbtnProcessWO').attr('disabled', true);

			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link ?>/action_process_wo_step1',
				data: {
					wo_kd: wo_kd,
					jenisProses: jenisProses
				},
				success: function(resp) {
					console.log(resp);
					if (resp.code == 200) {
						$('#idFormBoxWorkOrder').remove();
						window.location.assign('<?php echo base_url() . $class_link ?>/form_item_step1_box?wo_kd=' + resp.data.wo_kd);
						// window.location.reload();
						notify(resp.status, resp.pesan, 'success');
					}
				}
			});
		}
	}


	function view_bom(bom_kd) {
		var url = '<?php echo base_url() ?>manage_items/bom/report_bom/bom_std_pdf?bom_kd=' + bom_kd;
		var htmlContent = '<object type="application/pdf" width="100%" height="600px" data="' + url + '"></object>';
		toggle_modal('Lihat BoM', htmlContent);
	}

	function toggle_modal(modalTitle, htmlContent) {
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function render_datetimepicker(valClass) {
		$('.' + valClass).datetimepicker({
			format: 'YYYY-MM-DD',
		});
	}

	function submitDataWOItemSO() {
		box_overlay_formitem('in');
		event.preventDefault();
		var form = document.getElementById('idFormAddWOitemso');
		var url = '<?php echo base_url() . $class_link . '/action_insert_woitemso'; ?>';
		var slug = $('#idtxtslugWOitem').val();
		if (slug == 'edit') {
			url = '<?php echo base_url() . $class_link . '/action_update_woitemso'; ?>';
		}

		$.ajax({
			url: url,
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				console.log(data);
				var resp = JSON.parse(data);
				if (resp.code == 200) {
					var wo_kd = $('#idtxtwo_kd').val();
					open_form_item('add', wo_kd, '');
					open_form_table_so(wo_kd);
					notify(resp.status, resp.pesan, 'success');
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
					box_overlay_formitem('out');
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
				box_overlay_formitem('out');
			}
		});
	}

	function del_woitemso(woitemso_kd) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			box_overlay_formitem('in');
			var url = '<?php echo base_url() . $class_link ?>/action_delete_wo_itemso';
			$.ajax({
				url: url,
				type: 'GET',
				data: {
					woitemso_kd: woitemso_kd
				},
				success: function(data) {
					var resp = JSON.parse(data);
					if (resp.code == 200) {
						notify(resp.pesan, 'Data Terhapus', '');
						var wo_kd = $('#idtxtmainwo_kd').val();
						open_form_table_so(wo_kd);
					} else {
						notify('Gagal', resp.pesan, 'error');
					}
					box_overlay_formitem('out');
				}
			});
		}
	}

	function ubahstatus_woitemso(woitemso_kd, status) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			box_overlay_formitem('in');
			var url = '<?php echo base_url() . $class_link ?>/action_ubahstatus_woitemso';
			$.ajax({
				url: url,
				type: 'GET',
				data: {
					woitemso_kd: woitemso_kd,
					status
				},
				success: function(data) {
					var resp = JSON.parse(data);
					if (resp.code == 200) {
						notify(resp.pesan, 'Data Terupdate', 'success');
						var wo_kd = $('#idtxtmainwo_kd').val();
						open_form_table_so(wo_kd);
					} else {
						notify('Gagal', resp.pesan, 'error');
					}
					box_overlay_formitem('out');
				}
			});
		}
	}

	function get_bomkd(kd_barang) {
		var sts = $('#idtxtItem_status').val();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/get_bomkd'; ?>',
			data: {
				'kd_barang': kd_barang
			},
			success: function(resp) {
				if (resp.code == 200) {
					$('#idtxtbom_kd').val(resp.data.bom_kd).trigger('change');
				}
			}
		});
	}

	function edit_woitemso(wo_kd, woitemso_kd) {
		var slug = 'edit';
		open_form_item(slug, wo_kd, woitemso_kd);
		moveTo('idMainContent');
	}

	function close_form_item() {
		event.preventDefault();
		$('#idAddItem').html('');
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function box_overlay_formitem(sts) {
		if (sts == 'in') {
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		} else if (sts == 'out') {
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

	function render_select2(element) {
		$(element).select2({
			theme: 'bootstrap',
			width: '100%',
			placeholder: '-- Pilih Opsi --',
		});
	}

	function render_icheck(classElement) {
		$('input[type="checkbox"].' + classElement).iCheck({
			checkboxClass: 'icheckbox_flat-green'
		})
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			delay: 2500,
			styling: 'bootstrap3'
		});
	}
</script>