<?php
defined('BASEPATH') or exit('No direct script access allowed!');
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtmainwo_kd', 'name'=> 'txtmainwo_kd', 'value' => isset($wo_kd) ? $wo_kd: null ));
?>

<div class="row invoice-info">
	<div class="col-sm-6 invoice-col">
		<b>Tanggal Work Order :</b> #<?=format_date($wo['wo_tanggal'], 'd-M-Y')?><br>
		<b>Status :</b> <?php echo bg_label(process_status($wo['wo_status']), color_status($wo['wo_status'])); ?>
	</div>
	<div class="col-sm-6 invoice-col">
		<b>No Terbit :</b> #<?php echo isset($wo['wo_noterbit']) ? $wo['wo_noterbit']: '-'; ?> <br>
		<b>No Sales Order :</b> #<?php echo isset($no_salesorders) ? $no_salesorders: '-'; ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <!-- <hr> -->
			<!-- <div class="btn-group">
				<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown"> Lihat Data
				<span class="caret"></span>
				<span class="sr-only">Toggle Dropdown</span>
				</button>
				<ul class="dropdown-menu" role="menu">
				<li><a href="javascript:void(0);" onclick="open_form_table_so ('<?php echo $wo['wo_kd']?>')">Detail SO</a></li>
				<li><a href="javascript:void(0);" onclick="open_form_table_item ('<?php echo $wo['wo_kd']?>')">Detail Item</a></li>
				<li><a href="javascript:void(0);" onclick="open_form_table_item_planning ('<?php echo $wo['wo_kd']?>')">Detail Item Planning</a></li>
				</ul>
			</div>
			<button class="pull-right btn btn-sm btn-default" onclick="window.location.assign('<?php echo base_url().$class_link?>')"> <i class="fa fa-arrow-circle-left"></i> Kembali </button> -->
			<?php if ($wo['wo_status'] == 'pending') :?>
			<!-- <div class="btn-group">
				<button type="button" class="btn btn-sm btn-warning dropdown-toggle" data-toggle="dropdown"> List Item
				<span class="caret"></span>
				<span class="sr-only">Toggle Dropdown</span>
				</button>
				<ul class="dropdown-menu" role="menu">
				<li><a href="javascript:void(0);" onclick="itemunproseswo_box ('<?php echo $wo['wo_kd']?>')">Item Belum Proses WO</a></li>
				<li><a href="javascript:void(0);" onclick="itemadditionalso_box ('<?php echo $wo['wo_kd']?>')">Item Additional SO</a></li>
				</ul>
			</div>
			<button class="btn btn-sm btn-success" onclick="open_form_item ('add', '<?php echo $wo_kd;?>', '')"> <i class="fa fa-plus"></i> Tambah Item</button>
			<button class="btn btn-sm btn-danger" onclick="process_wo_step1('<?php echo $wo['wo_kd']?>', '')"> <i class="fa fa-bullhorn"></i> Proses WO Step 1</button> -->
			<?php endif; ?>
			<!-- <button class="btn btn-sm btn-danger" onclick="process_wo('<?php echo $wo['wo_kd']?>', 'reproses')"> <i class="fa fa-bullhorn"></i> ReProses WO</button> -->
    </div>
</div>

<div class="row">
	<hr>
	<div id="idAddItem"></div>
</div>

<div class="row">
	<div class="col-md-12">
		<div id="idTableItem"></div>
	</div>
</div>

<script type="text/javascript">
	open_form_table_so_detail ('<?php echo $wo_kd;?>');
</script>