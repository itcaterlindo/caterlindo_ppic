<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$form_id = 'idFormInput';

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
// echo form_input(array( 'type'=> 'text', 'id' => 'idtxtdn_kd', 'name'=> 'txtdn_kd', 'placeholder' => 'idtxtdn_kd' ,'class' => 'tt-input', 'value' => isset($dn_kd) ? $dn_kd: null));
?>
<style type="text/css">
.select2-container {
    width: 100% !important;
    padding: 0;
}
</style>

<div class="col-md-12">
	<div class="form-group">
		<label for='idtxtkd_msalesorderForm' class="col-md-2 control-label">Sales Order</label>
		<div class="col-sm-6 col-xs-12">
			<select class="form-control" name="txtkd_msalesorderForm" id="idtxtkd_msalesorderForm"></select>
		</div>
        <button class="btn btn-sm btn-default" onclick="cari_soitem()"> <i class="fa fa-search"></i> Cari </button>
		<button class="btn btn-sm btn-warning" onclick="clear_so()" data-toggle="tooltip" title="Clear SO"> <i class="fa fa-times"></i> Clear SO</button>
	</div>
</div>
<div class="col-md-12">
    <div id="idfindso_table"></div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
    render_so ('idtxtkd_msalesorderForm');
</script>