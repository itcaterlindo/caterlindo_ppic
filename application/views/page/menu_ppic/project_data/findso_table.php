<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<table id="idTable" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
	<thead>
		<tr>
			<th style="width:1%;">No.</th>
			<th style="width:10%;">Item Code</th>
			<th style="width:15%;">Item Desc</th>
			<th style="width:15%;">Item Dimensi</th>
			<th style="width:1%; text-align:center;" >Opsi</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	$no = 1; 
	foreach ($result as $r) :
		$so = $r['no_salesorder'].' | '.$r['nm_customer'];
		$so_item = $r['item_desc'].' | '.$r['item_desc'].'/'.$r['item_dimension'];
		if ($r['tipe_customer'] == 'Ekspor') {
			$so = $r['no_po'].' | '.$r['nm_customer'];
		}
	?>
		<tr>
			<td><?php echo $no; ?></td>
			<td><?php echo $r['item_code']; ?></td>
			<td><?php echo $r['item_desc']; ?></td>
			<td><?php echo $r['item_dimension']; ?></td>
			<td style="text-align:center;">
			<button class="btn btn-xs btn-default" 
			data-kd_msalesorder="<?php echo $r['kd_msalesorder']; ?>"
			data-so="<?php echo $so; ?>"
			data-item_code="<?php echo $r['item_code']; ?>"
			data-item_desc="<?php echo $r['item_desc']; ?>"
			data-item_dimension="<?php echo $r['item_dimension']; ?>"
			data-kd_ditem_so="<?php echo $r['kd_ditem_so']; ?>"
			data-kd_citem_so="<?php echo $r['kd_citem_so']; ?>"
			data-so_item="<?php echo $so_item; ?>"
			onclick="select_soitem(this)"> <i class="fa fa-check"></i> Pilih</button>
			</td>
		</tr>
	<?php
		$no++;
	endforeach;
	?>
	</tbody>
</table>