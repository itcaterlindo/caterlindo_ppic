<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$js_file = 'form_js';
$box_title = 'Form Project';
$data['class_link'] = $class_link;
$box_type = 'Table';
$data['master_var'] = 'MaterialRequisition';
$data['box_id'] = 'id'.$box_type.'Box'.$data['master_var'];
$data['box_content_id'] = 'id'.$box_type.'BoxContent'.$data['master_var'];
$data['box_loader_id'] = 'id'.$box_type.'BoxLoader'.$data['master_var'];
$data['btn_add_id'] = 'id'.$box_type.'BtnBoxAdd'.$data['master_var'];
$data['btn_remove_id'] = 'id'.$box_type.'BtnBoxRemove'.$data['master_var'];
if (isset($row)) {
	extract($row);
	if (!empty($kd_msalesorder)) {
		$salesorder = $no_salesorder.' | '.$nm_customer;
		$so_item = $parent_itemcode.' | '.$parent_itemdesc.'/'.$parent_itemdimension;
		if (!empty($kd_citem_so)) {
			$so_item = $child_itemcode.' | '.$child_itemdesc.'/'.$child_itemdimension;
		}
	}
}
?>

<div class="box box-primary" id="<?php echo $data['box_id']; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_title; ?></h3>
		<div class="box-tools pull-right">
			<!-- <button class="btn btn-box-tool" id="<?php //echo $data['btn_add_id'];?>" data-toggle="tooltip" title="Tambah"><i class="fa fa-plus"></i></button> -->
			<button class="btn btn-box-tool btn-collapse" id="idBtnBoxTableSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
				<i class="fa fa-minus"></i>
			</button>
			<button class="btn btn-box-tool btn-remove" id="<?php echo $data['btn_remove_id']; ?>" data-widget="remove" data-toggle="tooltip" title="Close">
				<i class="fa fa-times"></i>
			</button>
		</div>
	</div>
	<div class="box-body">
		<div id="<?php echo $data['box_loader_id']; ?>" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="<?php echo $data['box_content_id'];?>" style="display: none;"></div>

		<?php 
		echo form_open('', array('id' => 'idForm', 'class' => 'form-horizontal')); 
		echo form_input(array('type'=>'hidden', 'name'=> 'txtslug', 'id'=> 'idtxtslug', 'placeholder' =>'slug', 'value'=> isset($slug) ? $slug: 'add' ));
		echo form_input(array('type'=>'hidden', 'name'=> 'txtproject_kd', 'id'=> 'idtxtproject_kd', 'placeholder' =>'project_kd', 'value'=> isset($project_kd) ? $project_kd: null ));
			if ($slug == 'edit') :
		?>
            <div class="form-group">
                <label for='idtxtproject_nopj' class="col-md-2 control-label">No PJ</label>
                <div class="col-sm-3 col-xs-12">
                    <?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtproject_nopj', 'id'=> 'idtxtproject_nopj', 'placeholder' =>'No PJ', 'value'=> isset($project_nopj) ? $project_nopj: null ));?>
                </div>
            </div>
		<?php endif;?>
            <div class="form-group">
                <label for='idtxtno_salesorder' class="col-md-2 control-label">Sales Order</label>
                <div class="col-sm-3 col-xs-12">
					<?php 
					echo form_input(array('type'=>'text', 'class'=> 'form-control dt-so', 'name'=> 'txtno_salesorder', 'id'=> 'idtxtno_salesorder', 'placeholder' =>'Sales Order', 'readonly' => 'readonly', 'value'=> isset($salesorder) ? $salesorder: null ));
					echo form_input(array('type'=>'hidden', 'class'=>'dt-so', 'name'=> 'txtkd_msalesorder', 'id'=> 'idtxtkd_msalesorder', 'placeholder' =>'Sales Order', 'readonly' => 'readonly', 'value'=> isset($kd_msalesorder) ? $kd_msalesorder: null ));
					echo form_input(array('type'=>'hidden', 'class'=>'dt-so', 'name'=> 'txtkd_ditem_so', 'id'=> 'idtxtkd_ditem_so', 'placeholder' =>'Sales Order', 'readonly' => 'readonly', 'value'=> isset($kd_ditem_so) ? $kd_ditem_so: null ));
					echo form_input(array('type'=>'hidden', 'class'=>'dt-so', 'name'=> 'txtkd_citem_so', 'id'=> 'idtxtkd_citem_so', 'placeholder' =>'Sales Order', 'readonly' => 'readonly', 'value'=> isset($kd_citem_so) ? $kd_citem_so: null ));
					?>
                </div>
				<!-- <button class="btn btn-xs btn-warning" onclick="clear_so()" data-toggle="tooltip" title="Clear SO"> <i class="fa fa-times"></i> </button> -->
				<button class="btn btn-sm btn-default" onclick="cari_so()"> <i class="fa fa-search"></i> Cari </button>
            </div>

            <div class="form-group">
                <label for='idtxtso_item' class="col-md-2 control-label">Sales Order Item</label>
                <div class="col-sm-5 col-xs-12">
                    <?php echo form_input(array('type'=>'text', 'class'=> 'form-control dt-so', 'name'=> 'txtso_item', 'id'=> 'idtxtso_item', 'placeholder' =>'Sales Order Item', 'readonly' => 'readonly', 'value'=> isset($so_item) ? $so_item: null ));?>
                </div>
            </div>

            <div class="form-group">
                <label for="idtxtproject_itemcode" class="col-md-2 control-label">Item Code</label>
                <div class="col-sm-3 col-xs-12">
					<div class="err" id="iderrproject_itemcode"></div>
                    <?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtproject_itemcode', 'id'=> 'idtxtproject_itemcode', 'placeholder' =>'Item Code', 'value'=> isset($project_itemcode) ? $project_itemcode: null ));?>
                </div>
            </div>	
           
			<div class="form-group">
				<label for="idtxtproject_itemdesc" class="col-md-2 control-label">Item Desc</label>
				<div class="col-md-8">
					<div id="idErrLaporanFooter"></div>
					<?php echo form_textarea(array('name' => 'txtproject_itemdesc', 'id' => 'idtxtproject_itemdesc', 'class' => 'form-control', 'placeholder' => 'Item Desc', 'rows' => '2',  'value' => isset($project_itemdesc) ? $project_itemdesc: null )); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="idtxtproject_itemdimension" class="col-md-2 control-label">Item Dimensi</label>
				<div class="col-md-8">
					<div id="idErrLaporanFooter"></div>
					<?php echo form_textarea(array('name' => 'txtproject_itemdimension', 'id' => 'idtxtproject_itemdimension', 'class' => 'form-control', 'placeholder' => 'Item Dimensi', 'rows' => '2',  'value' => isset($project_itemdimension) ? $project_itemdimension: null )); ?>
				</div>
			</div>

	</div>
	<div class="box-footer">
		<button class="btn btn-sm btn-success pull-right" onclick="submitData('idForm')"> <i class="fa fa-save"></i> Simpan</button>
	</div>
        <?php echo form_close(); ?>
	<div class="overlay" id="idBoxTableOverlay" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
</div>

<script type="text/javascript">
    first_load('<?php echo $data['box_loader_id']; ?>', '<?php echo $data['box_content_id']; ?>');
</script>
<?php
//$this->load->view('page/'.$class_link.'/'.$js_file, $data); ?>

