<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$form_id = 'idFormInput';

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtuser_kd', 'name' => 'txtuser_kd', 'placeholder' => 'idtxtuser_kd', 'class' => 'tt-input', 'value' => isset($id) ? $id : null));
?>

<style type="text/css">
	.select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>

<div class="form-group">
	<label for='idtxtadmin_kd' class="col-md-2 control-label">User</label>
	<div class="col-sm-8 col-xs-12">
		<div class="errInput" id="idErridtxtadmin_kd"></div>
		<?php
		echo form_dropdown('txtadmin_kd', isset($opsiUser) ? $opsiUser : [], isset($id) ? $id : '', array('class' => 'form-control select2', 'id' => 'idtxtadmin_kd')); ?>
	</div>
</div>
<div class="form-group">
	<label for='idtxtstate_kd' class="col-md-2 control-label">State</label>
	<div class="col-sm-8 col-xs-12">
		<div class="errInput" id="idErridtxtstate_kd"></div>
		<?php foreach($state as $key => $val){ ?>
			<input type="checkbox" name="txtstate_kd[]" id="idTxtState_kd[]" value="<?= $val['inspectionproductstate_kd'] ?>" <?= in_array($val['inspectionproductstate_kd'], array_column($admin_state, 'inspectionproductstate_kd')) ? 'checked' : '' ?> >
			<?= $val['inspectionproductstate_nama'] ?> 
		<?php } ?>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-4 col-sm-offset-2 col-xs-12">
		<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitDataState('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
			<i class="fa fa-save"></i> Simpan
		</button>
	</div>
</div>

<?php echo form_close(); ?>