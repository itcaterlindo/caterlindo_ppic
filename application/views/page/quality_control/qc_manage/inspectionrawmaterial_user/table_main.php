<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}
</style>
<div class="box-body table-responsive no-padding">

<!-- START TAB -->
<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">User</a></li>
			<li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">User State</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
			<button class="btn btn-sm btn-primary" onclick="form_main('add', '')" id="idBtnTampil"> <i class="fa fa-save"></i> Tambah Data User</button>
			<h6>&nbsp;</h6>
				<table id="idTable" class="table table-bordered table-striped table-hover display" style="width:100%;">
					<thead>
						<tr>
							<th style="width:1%;" class="all">No.</th>
							<th style="width:1%; text-align:center;">Opsi</th>
							<th style="width:10%;" class="all">User</th>
							<th style="width:10%;" class="all">Tipe User</th>
							<th style="width:10%;" class="all">Bagian</th>
							<th style="width:10%;" class="all">Approved Bagian</th>
							<th style="width:10%;" class="all">Approved Penyebab</th>
							<th style="width:10%;" class="all">UpdatedAt</th>
						</tr>
					</thead>
				</table>

			</div>
			<div class="tab-pane" id="tab_2">
			<button class="btn btn-sm btn-primary" onclick="form_main_state('add', '')" id="idBtnTampil"> <i class="fa fa-save"></i> Tambah Data User State</button>
			<h6>&nbsp;</h6>
				<table id="idTableState" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
					<thead>
						<tr>
							<th style="width:1%;" class="all">No.</th>
							<th style="width:1%; text-align:center;">Opsi</th>
							<th style="width:10%;" class="all">User</th>
							<th style="width:10%;" class="all">Tipe User</th>
							<th style="width:10%;" class="all">State</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	<!-- END TAB -->
</div>

<script type="text/javascript">
	$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};
	var table = $('#idTable').DataTable({
		"processing": true,
		"ordering" : false,
		"ajax": "<?php echo base_url().$class_link.'/table_data'; ?>",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"columnDefs": [
			{"data": null, "searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
			{"searchable": false, "orderable": false, "targets": 1},
		],
		"rowCallback": function (row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			var index = page * length + (iDisplayIndex + 1);
			$('td:eq(0)', row).html(index);
		}
	});

	var tableState = $('#idTableState').DataTable({
		"processing": true,
		"ordering" : false,
		"ajax": "<?php echo base_url().$class_link.'/table_data_state'; ?>",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"columnDefs": [
			{"data": null, "searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
			{"searchable": false, "orderable": false, "targets": 1},
		],
		"rowCallback": function (row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			var index = page * length + (iDisplayIndex + 1);
			$('td:eq(0)', row).html(index);
		}
	});
</script>