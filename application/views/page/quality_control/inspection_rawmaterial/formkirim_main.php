<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtKd', 'value' => empty($inspection) ? '' : $inspection['kd_inspectionrawmaterial']));
?>
<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Tanggal Inspection</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrTglInspection"></div>
		<?php echo form_input(array('name' => 'txtTglInspection', 'id' => 'idTxtTglInspection', 'class' => 'form-control datetimepicker', 'placeholder' => 'Tanggal Inspection', 'value' => empty($inspection) ? '' : format_date($inspection['inspectionrawmaterial_tanggal'], 'd-m-Y') )); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">No. Delivery Material</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrNoDrm"></div>
		<select class="form-control select2" name="txtNoDrm" id="idTxtNoDrm" data-allow-clear="true">
			<?php if( !empty($inspection) ){ ?>
				<option value="<?= $inspection['deliverymaterial_kd'] ?>" selected><?= $inspection['deliverymaterial_no'] ?></option>
			<?php } ?>
		</select>
	</div>
</div>
<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">RM Kode</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrRmKode"></div>
		<?php echo form_input(array('name' => 'txtRmKode', 'id' => 'idTxtRmKode', 'class' => 'form-control', 'placeholder' => 'RM Kode', 'readonly' => 'readonly','value' => empty($inspection) ? '' : $inspection['rm_kode'])); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Nama Material</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrRmDeskripsi"></div>
		<?php echo form_input(array('name' => 'txtRmDeskripsi', 'id' => 'idTxtRmDeskripsi', 'class' => 'form-control', 'placeholder' => 'Nama Material', 'readonly' => 'readonly', 'value' => empty($inspection) ? '' : $inspection['rm_deskripsi'])); ?>
	</div>
</div>

<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Tipe (Spesifikasi)</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrTipe"></div>
		<?php echo form_input(array('name' => 'txtTipe', 'id' => 'idTxtTipe', 'class' => 'form-control', 'placeholder' => 'Tipe', 'readonly' => 'readonly', 'value' => empty($inspection) ? '' : $inspection['rm_spesifikasi'])); ?>
	</div>
</div>

<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Qty</label>
	<div class="col-sm-1 col-xs-12">
		<div id="idErrQty"></div>
		<div id="idErrCheckQty"></div>
		<?php echo form_input(array('name' => 'txtQty', 'id' => 'idTxtQty', 'type' => 'number', 'class' => 'form-control', 'placeholder' => 'Qty', 'value' => empty($inspection) ? '' : $inspection['inspectionrawmaterial_qty'])); ?>
	</div>
</div>

<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Posisi Ditemukan</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrBagianKd"></div>
		<select class="form-control select2" name="txtBagianKd" id="idTxtBagianKd" data-allow-clear="true">	
			<option value="">-- Pilih Opsi --</option>
			<?php foreach($bagian as $key => $val){ ?>
				<option <?= $inspection['inspectionrawmaterial_bagian_kd'] == $val['bagian_kd'] ? 'selected' : '' ?> value="<?= $val['bagian_kd'] ?>"><?= $val['bagian_nama'] ?></option>
			<?php } ?>
		</select>
	</div>
</div>
<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Tindakan</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrTindakan"></div>
		<select class="form-control select2" name="txtTindakan" id="idTxtTindakan" data-allow-clear="true">
			<option value="">-- Pilih Opsi --</option>
			<option value="reject" <?= $inspection['inspectionrawmaterial_tindakan'] == 'reject' ? 'selected' : '' ?> >Reject</option>
			<option value="repair" <?= $inspection['inspectionrawmaterial_tindakan'] == 'repair' ? 'selected' : '' ?> >Repair</option>
		</select>
	</div>
</div>
<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Qty Reject/Repair</label>
	<div class="col-sm-1 col-xs-12">
		<div id="idErrQtyReject"></div>
		<div id="idErrCheckQtyReject"></div>
		<?php echo form_input(array('name' => 'txtQtyReject', 'id' => 'idTxtQtyReject', 'type' => 'number', 'class' => 'form-control', 'placeholder' => 'Qty Reject/Repair', 'value' => empty($inspection) ? '' : $inspection['inspectionrawmaterial_qty_reject'])); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Bagian Penyebab</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrPenyebabBagian"></div>
		<select class="form-control select2" name="txtPenyebabBagian" id="idTxtPenyebabBagian" data-allow-clear="true">	
			<option value="">-- Pilih Opsi --</option>
			<?php foreach($bagian as $key => $val){ ?>
				<option <?= $inspection['inspectionrawmaterial_penyebab_bagian_kd'] == $val['bagian_kd'] ? 'selected' : '' ?> value="<?= $val['bagian_kd'] ?>"><?= $val['bagian_nama'] ?></option>
			<?php } ?>
		</select>
	</div>
</div>
<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Penyebab</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrPenyebab"></div>
		<textarea class="form-control" name="txtPenyebab" id="idTxtPenyebab" cols="20" rows="5"><?= empty($inspection) ? '' : $inspection['inspectionrawmaterial_penyebab'] ?></textarea>
	</div>
</div>
<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Detail Ketidaksesuaian</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrKeterangan"></div>
		<textarea class="form-control" name="txtKeterangan" id="idTxtKeterangan" cols="20" rows="5"><?= empty($inspection) ? '' : $inspection['inspectionrawmaterial_keterangan'] ?></textarea>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" onclick="resetForm()" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" onclick="submitData()" class="btn btn-primary btn-flat">
			<i class="fa fa-save"></i> Submit
		</button>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">

	$(document).ready(function(e){
		render_drm();
		$('#idTxtKategori').select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
		});
		$('#idTxtTglInspection').datetimepicker({
			format: 'DD-MM-YYYY',
		});
		$('#idTxtTindakan').on('change', function(e){
			e.preventDefault();
			let txtvalue = $(this).val();
			if(txtvalue == 'repair'){
				$('#repair-oleh').show();
			}else{
				$('#repair-oleh').hide();
			}
		});
	});

	$(document).off('select2:selecting', '#idTxtNoDrm').on("select2:selecting", '#idTxtNoDrm', function(e) { 
		let dt = e.params.args.data;
		$('#idTxtRmKode').val(dt.rm_kode);
		$('#idTxtRmDeskripsi').val(dt.rm_deskripsi);
		$('#idTxtTipe').val(dt.rm_spesifikasi);
		$('#idTxtQty').val(dt.deliverymaterial_qty);
	});

	$(document).off('select2:unselecting', '#idTxtNoDrm').on("select2:unselecting", '#idTxtNoDrm', function(e) { 
		$('#idTxtRmKode').val(null);
		$('#idTxtNamaMaterial').val(null);
		$('#idTxtBagianKd').val('');
		$('#idTxtPenyebabBagian').val('');
		$('#idTxtTindakan').val('');
		$('#idTxtQty').val('');
	});

	function render_drm(){
		/** Render WO Select2 */
		$("#idTxtNoDrm").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?= base_url().$class_link ?>/get_drm',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramDRM: params.term, // search term 
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	/** Regenerate token */
	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitData() {
		event.preventDefault();
		if(confirm('Apakah anda ingin menyimpan data tersebut ? ')){
			var form = document.getElementById('<?php echo $form_id; ?>');
			$.ajax({
				url: "<?php echo base_url().$class_link; ?>/action_submit",
				type: "POST",
				data:  new FormData(form),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						generateToken (resp.csrf);
						remove_form();
						refreshTable();
					}else if (resp.code == 401){
						$.each( resp.pesan, function( key, value ) {
							$('#'+key).html(value);
						});
						generateToken (resp.csrf);
						box_overlay('out');
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				} 	        
			});
		}
	}

	$('#idbtnReset').click(function(){
		resetForm();
	});
	
	/** Reset Form */
	function resetForm(){
		var form = document.getElementById('<?php echo $form_id; ?>');
		$("#idTxtInspectionKd").empty();
		form.reset();
	}

</script>