<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table();
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		get_form('');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table() {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/tablekirim_main'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown(html);
				}
			});
		});
	}

	function refreshTable(){
		table.ajax.reload();
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function remove_box() 
	{
		$('#idFormBox<?php echo $master_var; ?>').remove();
		$('#idTableBox<?php echo $master_var; ?>').remove();
	}

	function get_form(id) {
		remove_box();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/formkirim_box'; ?>',
			data: 'id='+id,
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function action_kirim(delivery_kd,inspectionrawmaterial_kd,param){
		/** Variabel param = 'baik' atau 'reject' */
		var conf = confirm('Apakah anda yakin ingin kirim data qty ini ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_kirim_delivery'; ?>',
				data: {delivery_kd: delivery_kd, inspectionrawmaterial_kd:inspectionrawmaterial_kd ,param: param},
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						refreshTable();
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
					}
				}
			});
		}

	}

	function history_item(inspectionrawmaterial_kd){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/history_item'; ?>',
			data: {inspectionrawmaterial_kd:inspectionrawmaterial_kd},
			success: function(data) {
				$('#modal').modal('show');
				$('#modal-history').html(data)
			}
		});
	}

	function cetak(id) {
		window.open('<?php echo base_url().$class_link;?>/report_pdf?id='+id);
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }
	

</script>