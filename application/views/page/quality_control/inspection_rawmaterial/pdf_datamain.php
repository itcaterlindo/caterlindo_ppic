<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">

    /* table { border: 2px solid black 
    }

    td { border: thin solid black 
    } */

    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }
</style>

<table id="idtablemain" cellspacing="0" cellpadding="1" style="font-size: 80%; border: 1px solid black; border-collapse: collapse;"  width="100%">
    <tbody>
       <!-- <tr>
            <td rowspan="3" style="text-align: center; vertical-align: middle; border: 1px solid black; border-collapse: collapse;">Produk</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
       </tr> -->
       <tr>
            <td>Jenis Material </td>
            <td>: <?= $master['rm_nama'] ?> / <?= $master['rm_spesifikasi'] ?></td>
            <td align="left">Quantity : <?= $master['inspectionrawmaterial_qty_reject'] ?></td>
       </tr>
       <tr>
            <td>Deskripsi Material </td>
            <td>: <?= $master['rm_deskripsi'] ?></td>
       </tr>
       <tr>
            <td>Tgl Datang </td>
            <td>: <?= format_date($master['rmgr_tgldatang'], 'd-m-Y') ?></td>
       </tr>
       <tr>
            <td>No. SJ </td>
            <td>: <?= $master['rmgr_nosrj'] ?></td>
       </tr>
       <tr>
            <td>Suplier </td>
            <td>: <?= $master['suplier_nama'] ?></td>
       </tr>
    </tbody>
</table>

<h6>&nbsp;</h6>

<table id="idtableKetidaksesuaian" cellspacing="0" cellpadding="1" style="font-size: 80%; border: 1px solid black; border-collapse: collapse;"  width="100%">
    <tbody>
        <tr>
          <td style="border: 1px solid black; text-align: center">Detail Ketidaksesuaian</td>
          <td style="border: 1px solid black; text-align: center">Posisi Barang</td>
        </tr>
        <tr>
          <td style="border-right: 1px solid black;">&nbsp;</td>
          <td><?= !empty($master['bagian_nama']) ? $master['bagian_nama'] : '' ?></td>
        </tr>
        <tr>
          <td style="border-right: 1px solid black;"><?= $master['inspectionrawmaterial_keterangan'] ?></td>
          <td><?= !empty($master['nm_admin2']) ? 'Approved oleh, '.$master['nm_admin2'] : '' ?></td>
        </tr>
        <tr>
          <td style="border-right: 1px solid black;">&nbsp;</td>
          <td><?= !empty($master['nm_admin2']) ? format_date($master['approved_bagian_tgl'], "d-m-Y H:i:s") : '' ?></td>
        </tr>
    </tbody>
</table>

<h6>&nbsp;</h6>

<table id="idtableKetidaksesuaian" cellspacing="0" cellpadding="1" style="font-size: 80%; border: 1px solid black; border-collapse: collapse;"  width="100%">
    <tbody>
        <tr>
          <td style="border: 1px solid black; text-align: center">Penyebab</td>
          <td style="border: 1px solid black; text-align: center">SIGN</td>
        </tr>
        <tr>
          <td style="border-right: 1px solid black;">&nbsp;</td>
          <td><?= !empty($master['bagian_nama3']) ? $master['bagian_nama3'] : '' ?></td>
        </tr>
        <tr>
          <td style="border-right: 1px solid black;"><?= $master['inspectionrawmaterial_penyebab'] ?></td>
          <td><?= !empty($master['nm_admin3']) ? 'Approved oleh, '.$master['nm_admin3'] : '' ?></td>
        </tr>
        <tr>
          <td style="border-right: 1px solid black;">&nbsp;</td>
          <td><?= !empty($master['nm_admin3']) ? format_date($master['approved_penyebab_tgl'], "d-m-Y H:i:s") : '' ?></td>
        </tr>
    </tbody>
</table>

<h6>&nbsp;</h6>

<table id="idtableTindakan" cellspacing="0" cellpadding="1" style="font-size: 80%; border: 1px solid black; border-collapse: collapse;"  width="100%">
    <tbody>
        <tr>
          <td style="border: 1px solid black; text-align: center">Tindakan</td>
        </tr>
        <tr>
          <td style="border-right: 1px solid black;"><input type="checkbox" name="box" value="1" readonly="true" checked="<?= $master['inspectionrawmaterial_tindakan'] == 'reject' ? 'checked' : '' ?>"> Reject</td>
        </tr>
        <tr>
          <td style="border-right: 1px solid black;"><input type="checkbox" name="box" value="1" readonly="true" checked="<?= $master['inspectionrawmaterial_tindakan'] == 'repair' ? 'checked' : '' ?>"> Repair</td>
        </tr>
    </tbody>
</table>

<br/>
<br/>
<br/>