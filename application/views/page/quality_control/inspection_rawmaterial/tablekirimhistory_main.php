<table id="idTableHistory" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
    <thead>
    <tr>
        <th style="text-align:left;">Tanggal Inspection</th>
        <th style="text-align:left;">No. Inspection</th>
        <th style="text-align:left;">Tanggal Delivery Material</th>
        <th style="text-align:left;">No. Delivery Material</th>
        <th style="text-align:left;">RM Kode</th>
        <th style="text-align:left;">Nama Material</th>
        <th style="text-align:left;">Tipe Material</th>
        <th style="text-align:left;">Qty</th>
        <th style="text-align:left;">Keterangan</th>
    </tr>
    </thead>
    <tbody>
        <?php foreach($history as $key => $val){ ?>
            <tr>
                <td><?= $val['inspectionrawmaterial_tanggal'] ?></td>
                <td><?= $val['inspectionrawmaterial_no'] ?></td>
                <td><?= $val['deliverymaterial_tanggal'] ?></td>
                <td><?= $val['deliverymaterial_no'] ?></td>
                <td><?= $val['inspectionrawmaterial_rm_kode'] ?></td>
                <td><?= $val['rm_deskripsi'] ?></td>
                <td><?= $val['rm_spesifikasi'] ?></td>
                <td><?= $val['qty'] ?></td>
                <td><?= $val['keterangan'] ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>