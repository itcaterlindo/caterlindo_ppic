<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
$master_var = 'FilterTelusurWorkorder';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtbarang_kd', 'name'=> 'txtbarang_kd' ));
?>
  <style>  
        /* Overlay style */  
        #overlay {  
            display: none;  
            position: fixed;  
            top: 0;  
            left: 0;  
            width: 100%;  
            height: 100%;  
            background-color: rgba(0, 0, 0, 0.5);  
            color: white;  
            font-size: 2em;  
            text-align: center;  
            justify-content: center;  
            align-items: center;  
            z-index: 10;  
        }  
    </style>  
<div class="row">

	<div class="col-sm-6">
		<div class="form-group">
			<label for="idtxtWarehouse">No WO</label>
			<div id="idErrWarehouse"></div>
			 <select class="form-control no_wo" name="no_wo[]" multiple="multiple" id="no_wo">
					<option value="1">1</option>
					<option value="1">2</option>
					<option value="1">3</option>
					<option value="1">4</option>
					<option value="1">5</option>
					<option value="1">6</option>
					<option value="1">7</option>
					<option value="1">8</option>
					<option value="1">9</option>
			 </select>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			<br>
			<button class="btn btn-success" type="button" id="idBtnSubmit"> <i class="fa fa-search"></i> Proses</button>
		</div>
	</div>

</div>
<hr>	
<?php echo form_close(); ?>
<div id="overlay">Processing...</div>  

<script type="text/javascript">

	$(document).ready(function () {
	/* --start product typeahead.js-- */
	var Product = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			url: '<?php echo base_url('auto_complete/get_barang'); ?>',
			remote: {
				url: '<?php echo base_url('auto_complete/get_barang'); ?>',
				prepare: function (query, settings) {
					settings.type = 'GET';
					settings.contentType = 'application/json; charset=UTF-8';
					settings.data = 'item_code='+query;

					return settings;
				},
				wildcard: '%QUERY'
			}
		});

		$('#idtxtItemCode').typeahead(null, {
			limit: 50,
			minLength: 1,
			name: 'product_search',
			display: 'item_code',
			valueKey: 'get_product',
			source: Product.ttAdapter()
		});

		$('#idtxtItemCode').bind('typeahead:select', function(obj, selected) {
			$('#idtxtbarang_kd').val(selected.kd_barang);
			$('#idBtnSubmit').focus();
		});
		/* --end of product typeahead.js-- */
	}); 

	$('#idBtnSubmit').click(function(e) {
		const overlay = document.getElementById("overlay");  
            overlay.style.display = "flex";
		e.preventDefault();
		var form = document.getElementById('<?php echo $form_id; ?>');
		var datax = $('#no_wo').val().toString();
		var url = '<?php echo base_url().$class_link;?>/table_main?no_wo='+datax;
		
			// console.log(url);
			$.ajax({
			url: url,
			type: "GET",
			success: function(resp){
				//alert('jalan');
				 console.log(resp);
				 open_table(resp);
			// 	if(resp.code == 200){
					
			// 		generateToken (resp.csrf);
			// 	}else if (resp.code == 401){
			// 		$.each( resp.pesan, function( key, value ) {
			// 			$('#'+key).html(value);
			// 		});
			// 		generateToken (resp.csrf);
			// 	}else if (resp.code == 400){
			// 			notify (resp.status, resp.pesan, 'error');
			// 			generateToken (resp.csrf);
			// 		}else{
			// 			notify ('Error', 'Error tidak Diketahui', 'error');
			// 		generateToken (resp.csrf);
			// 	}
			} 	        
		});

	});

	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}
	
</script>