<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<div class="row invoice-info">
	<center>
		<h3 type="text" style="font-weight: bold;" id="idJuduLaporan"> Laporan Telusur Workorder </h3>
	</center>
</div>
<hr>
<div class="row invoice-info">
 <table style="column-width: 100%;width: 100%;" border="1">
	<thead>
		<tr>
			<th style="text-align:center;">No.</th>
			<th style="text-align:center;">No. WO</th>
			<th style="text-align:center;">WO Item Code</th>
			<th style="text-align:center;">Wo qty</th>
			<th style="text-align:center;">No. PO</th>
			<th style="text-align:center;">Qty Out</th>
			<th style="text-align:center;">Tgl DO</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		if ($datax){
			foreach ($datax as $eachData):
			?>
				<tr>
					<td><?php echo $no;?></td>
					<td><?php echo $eachData['woitem_no_wo'];?></td>
					<td><?php echo $eachData['woitem_itemcode'];?></td>
					<td><?php echo $eachData['woitem_qty'];?></td>
					<td><?php echo $eachData['no_po']?></td>
					<td style="text-align:center;"><?php echo $eachData['jml_out'];?></td>
					<td><?php echo $eachData['tgl_do']?></td>
				</tr>
			<?php
			$no++;
			endforeach;
		}
		?>
	</tbody>
</table>
<hr> 
</div>