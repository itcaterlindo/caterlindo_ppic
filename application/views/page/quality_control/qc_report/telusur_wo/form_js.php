<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	$('#<?= $box_loader_id?>').hide();
	open_form_main();
	open_table_box();
	
	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_form_main() {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link?>/form_main',
			success:function(html){
				$('#<?php echo $box_content_id; ?>').html(html);
				datepick('.datepicker');
				render_woitem()
			}
		});
		moveTo('idMainContent');
	}

	function render_woitem() {
		$("#no_wo").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: {
				url: '<?php echo base_url().$class_link?>/get_woitem',
				type: "get",
				dataType: 'json',
				delay: 500,
				data: function(params) {
					return {
						paramWO: params.term // search term
					};
				},
				processResults: function(response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function open_table_box() {
		var url = '<?php echo base_url().$class_link?>/table_box';
		$.ajax({
			type: 'GET',
			url: url,
			success:function(html){
				$('#idMainContent').append(html);
			}
		});
		// moveTo('idMainContent');
	}

	function datepick(element) {
		$(element).datetimepicker({
			format: 'DD-MM-YYYY'
		});
	}

	function select2(element) {
		$(element).select2();
	}


	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
			// delay: 2500,
            styling: 'bootstrap3'
        });	
    }
</script>