<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<div class="row invoice-info">
	<center>
		<h3 type="text" style="font-weight: bold;" id="idJuduLaporan"> Laporan Telusur Raw Material </h3>
		<h4> Periode <?php echo $startdate.' s/d '.$enddate; ?> </h4>
	</center>
</div>
<div class="row invoice-info">
		<table>
			<tr>
				<td width="100">Product Code</td>
				<td width="10">:</td>
				<td><?php echo $barang->item_code?></td>
			</tr>
			<tr>
				<td width="100">Barcode</td>
				<td width="10">:</td>
				<td><?php echo $barang->item_barcode?></td>
			</tr>
			<tr>
				<td width="100">Deskripsi</td>
				<td width="10">:</td>
				<td><?php echo $barang->deskripsi_barang?></td>
			</tr>
			<tr>
				<td width="100">Dimensi</td>
				<td width="10">:</td>
				<td><?php echo $barang->dimensi_barang?></td>
			</tr>
		</table>
	<!-- </div> -->
</div>
<hr>
<div class="row invoice-info">
 <table style="column-width: 100%;width: 100%;" border="1">
	<thead>
		<tr>
			<th style="text-align:center;">No.</th>
			<th style="text-align:center;">No. WO Packing</th>
			<th style="text-align:center;">WO Item Code</th>
			<th style="text-align:center;">No. WO</th>
			<th style="text-align:center;">WO Part</th>
			<th style="text-align:center;">No. DN</th>
			<th style="text-align:center;">No. MRC</th>
			<th style="text-align:center;">Code SRJ</th>
			<th style="text-align:center;">Batch</th>
			<th style="text-align:center;">RM Kode</th>
			<th style="text-align:center;">RM Deskripsi</th>
			<th style="text-align:center;">Suplier</th>
			<th style="text-align:center;">Uraian</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		if ($rowData){
			foreach ($rowData as $eachData):
			?>
				<tr>
					<td><?php echo $no;?></td>
					<td><?php echo $eachData['no_wo_packing'];?></td>
					<td><?php echo $eachData['item_code'];?></td>
					<td><?php echo $eachData['no_wo'];?></td>
					<td><?php echo $eachData['woitem_itemcode'];?></td>
					<td><?php echo $eachData['dn_no']?></td>
					<td><?php echo $eachData['materialreceipt_no']?></td>
					<td><?php echo $eachData['rmgr_code_srj'];?></td>
					<td><?php echo $eachData['batch'];?></td>
					<td><?php echo $eachData['rm_kode'];?></td>
					<td><?php echo $eachData['rm_deskripsi']." | ".$eachData['rm_spesifikasi'];?></td>
					<td><?php echo $eachData['suplier_kode']." | ".$eachData['suplier_nama'];?></td>
					<td><?php echo $eachData['uraian']?></td>
				</tr>
			<?php
			$no++;
			endforeach;
		}
		?>
	</tbody>
</table>
<hr> 
</div>