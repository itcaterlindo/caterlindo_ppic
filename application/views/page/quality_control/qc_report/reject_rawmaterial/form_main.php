<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
$master_var = 'FilterRejectRawMaterial';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtbarang_kd', 'name'=> 'txtbarang_kd' ));
?>
<div class="row">

	<div class="col-sm-2">
		<div class="form-group">
			<label for="idtxtYear">Year</label>
			<div id="idErrYear"></div>
			<div class="input-group date">
				<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
				</div>
				<select class="form-control" name="txtYear" id="idtxtYear">
					<?php foreach($optionYear as $year){ ?>
						<option value="<?= $year ?>" <?= $year == date('Y') ? 'selected' : '' ?> ><?= $year ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>

	<div class="col-sm-2">
		<div class="form-group">
			<label for="idtxtMonth">Month</label>
			<div id="idErrMonth"></div>
			<div class="input-group date">
				<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
				</div>
				<select class="form-control" name="txtMonth" id="idtxtMonth">
					<?php foreach($optionMonth as $month){ ?>
						<option value="<?= $month ?>" <?= $month == date('m') ? 'selected' : '' ?> ><?= $month ?></option>
					<?php } ?>
				</select>	
			</div>
		</div>
	</div>
	
	<div class="col-sm-2">
		<div class="form-group">
			<br>
			<button class="btn btn-success" id="idBtnSubmit"> <i class="fa fa-search"></i> Proses</button>
		</div>
	</div>
</div>
<hr>	
<?php echo form_close(); ?>

<script type="text/javascript">

	$('#idBtnSubmit').click(function(e) {
		e.preventDefault();
		var form = document.getElementById('<?php echo $form_id; ?>');
		var url = '<?php echo base_url().$class_link;?>/table_main';

		$.ajax({
			url: url,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if(resp.code == 200){
					open_table(resp.data);
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});

	});

	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}
	
</script>