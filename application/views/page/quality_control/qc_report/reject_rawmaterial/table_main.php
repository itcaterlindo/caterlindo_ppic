<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<div class="row invoice-info">
	<center>
		<h3 type="text" style="font-weight: bold;" id="idJuduLaporan"> Laporan Reject Raw Material </h3>
	</center>
</div>
<hr>
<div class="row invoice-info">
 <table style="column-width: 100%;width: 100%;" border="1">
	<thead>
		<tr>
			<th style="text-align:center;">No.</th>
			<th style="text-align:center;">Material</th>
			<th style="text-align:center;">Jumlah Cek</th>
			<th style="text-align:center;">Jumlah Reject</th>
			<th style="text-align:center;">Persentase (%)</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		if ($reportData){
			foreach ($reportData as $eachData):
			?>
				<tr>
					<td><?php echo $no;?></td>
					<td><?php echo $eachData['rm_deskripsi'];?></td>
					<td><?php echo $eachData['sum_inspectionrawmaterial_qty'];?></td>
					<td><?php echo $eachData['sum_inspectionrawmaterial_qty_reject'];?></td>
					<td><?php echo ROUND(($eachData['sum_inspectionrawmaterial_qty_reject'] / $eachData['sum_inspectionrawmaterial_qty']) * 100, 2)."%" ;?></td>
				</tr>
			<?php
			$no++;
			endforeach;
		}
		?>
	</tbody>
</table>
<hr> 
</div>