<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table(url) {
		$('#<?php echo $box_loader_id; ?>').fadeIn();
		$('#<?php echo $box_content_id; ?>').slideUp();
			$.ajax({
				type: 'GET',
				url: url,
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					$('#<?php echo $box_loader_id; ?>').fadeOut();
				}
			});
	}

	function remove_table() {
		$('#<?php echo $box_content_id; ?>').slideUp();
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

</script>