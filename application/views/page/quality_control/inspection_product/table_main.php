<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
	<!-- START TAB -->
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Master</a></li>
			<li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Lokasi Ditemukan</a></li>
			<li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Penyebab</a></li>
			<li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Detail Repair</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_1">
				<table id="idTable" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
					<thead>
					<tr>
						<th style="text-align:center;" class="all">No.</th>
						<th style="text-align:center;" class="all">Opsi</th>
						<th style="text-align:left;">Tanggal</th>
						<th style="text-align:left;">No. Inspection</th>
						<th style="text-align:left;">Nama Produk</th>
						<th style="text-align:left;">No. WO</th>
						<th style="text-align:left;">Bagian</th>
						<th style="text-align:left;">Tindakan</th>
						<th style="text-align:left;">Qty</th>
						<th style="text-align:left;">Posisi Penyebab</th>
						<th style="text-align:left;">Penyebab</th>
						<th style="text-align:left;">Detail Ketidaksesuaian</th>
						<th style="text-align:left;">Last Modified</th>
						<th style="text-align:left;">Status</th>
					</tr>
					</thead>
				</table>
			</div>
			<div class="tab-pane" id="tab_2">
				<table id="idTableLokasiDitemukan" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
					<thead>
					<tr>
						<th style="text-align:center;" class="all">No.</th>
						<th style="text-align:center;" class="all">Opsi</th>
						<th style="text-align:left;">Tanggal</th>
						<th style="text-align:left;">No. Inspection</th>
						<th style="text-align:left;">Nama Produk</th>
						<th style="text-align:left;">No. WO</th>
						<th style="text-align:left;">Tindakan</th>
						<th style="text-align:left;">Qty</th>
						<th style="text-align:left;">Lokasi Ditemukan</th>
						<th style="text-align:left;">Detail Ketidaksesuaian</th>
						<th style="text-align:left;">Tgl Approved</th>
						<th style="text-align:left;">Status</th>
					</tr>
					</thead>
				</table>
			</div>
			<div class="tab-pane" id="tab_3">
				<table id="idTablePenyebab" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
					<thead>
					<tr>
						<th style="text-align:center;" class="all">No.</th>
						<th style="text-align:center;" class="all">Opsi</th>
						<th style="text-align:left;">Tanggal</th>
						<th style="text-align:left;">No. Inspection</th>
						<th style="text-align:left;">Nama Produk</th>
						<th style="text-align:left;">No. WO</th>
						<th style="text-align:left;">Tindakan</th>
						<th style="text-align:left;">Qty</th>
						<th style="text-align:left;">Posisi Penyebab</th>
						<th style="text-align:left;">Penyebab</th>
						<th style="text-align:left;">Detail Ketidaksesuaian</th>
						<th style="text-align:left;">Tgl Approved</th>
						<th style="text-align:left;">Status</th>
					</tr>
					</thead>
				</table>
			</div>
			<div class="tab-pane" id="tab_4">
				<table id="idTableRepair" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
					<thead>
					<tr>
						<th style="text-align:center;" class="all">No.</th>
						<th style="text-align:center;" class="all">Opsi</th>
						<th style="text-align:left;">Bagian</th>
						<th style="text-align:left;">Tanggal</th>
						<th style="text-align:left;">No. Inspection</th>
						<th style="text-align:left;">Nama Produk</th>
						<th style="text-align:left;">No. WO</th>
						<th style="text-align:left;">Tindakan</th>
						<th style="text-align:left;">Qty</th>
						<th style="text-align:left;">Detail Ketidaksesuaian</th>
						<th style="text-align:left;">Tgl Approved</th>
						<th style="text-align:left;">Status</th>
					</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	<!-- END TAB -->

<script type="text/javascript">
	$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};
	var table = $('#idTable').DataTable({
		"processing": true,
		// "serverSide": true,
		"ordering" : false,
		"ajax": "<?php echo base_url().$class_link.'/table_data'; ?>",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"columnDefs": [
			{"data": null, "searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
			{"searchable": false, "orderable": false, "targets": 1},
			{"className": "dt-center", "targets": 2},
		],
		// "order": [ [13, 'asc'], [12, 'desc'] ],
		"rowCallback": function (row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			var index = page * length + (iDisplayIndex + 1);
			$('td:eq(0)', row).html(index);
		}
	});
	var tableLokasiDitemukan = $('#idTableLokasiDitemukan').DataTable({
		"processing": true,
		// "serverSide": true,
		"ordering" : false,
		"ajax": "<?php echo base_url().$class_link.'/table_data_bagian'; ?>",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"columnDefs": [
			{"data": null, "searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
			{"searchable": false, "orderable": false, "targets": 1},
			{"className": "dt-center", "targets": 2},
		],
		// "order": [ [13, 'asc'], [12, 'desc'] ],
		"rowCallback": function (row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			var index = page * length + (iDisplayIndex + 1);
			$('td:eq(0)', row).html(index);
		}
	});
	var tablePenyebab = $('#idTablePenyebab').DataTable({
		"processing": true,
		// "serverSide": true,
		"ordering" : false,
		"ajax": "<?php echo base_url().$class_link.'/table_data_penyebab'; ?>",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"columnDefs": [
			{"data": null, "searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
			{"searchable": false, "orderable": false, "targets": 1},
			{"className": "dt-center", "targets": 2},
		],
		// "order": [ [13, 'asc'], [12, 'desc'] ],
		"rowCallback": function (row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			var index = page * length + (iDisplayIndex + 1);
			$('td:eq(0)', row).html(index);
		}
	});
	var tableRepair = $('#idTableRepair').DataTable({
		"processing": true,
		// "serverSide": true,
		"ordering" : false,
		"ajax": "<?php echo base_url().$class_link.'/table_data_repair'; ?>",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"columnDefs": [
			{"data": null, "searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
			{"searchable": false, "orderable": false, "targets": 1},
			{"className": "dt-center", "targets": 2},
		],
		// "order": [ [13, 'asc'], [12, 'desc'] ],
		"rowCallback": function (row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			var index = page * length + (iDisplayIndex + 1);
			$('td:eq(0)', row).html(index);
		}
	});
</script>