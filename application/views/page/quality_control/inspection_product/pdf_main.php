<?php
class customPdf extends Tcpdf
{
    public $materialreq_no;
    public $bagian_nama;
    public $materialreq_tanggal;

    public function setsHeader($headerData)
    {
        $this->_no = $headerData['_no'];
        $this->_tanggal = $headerData['_tanggal'];
    }

    public function Header()
    {
        $header = '
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td width="30%" rowspan="4" style="text-align:left;"> <img src="assets/admin_assets/dist/img/logo_cat.png" width="135" height="35"> </td>
                <td width="35%" style="text-align:right; font-size: 120%; font-weight:bolt;"> </td>
                <td width="35%" colspan="2" style="text-align:left; font-size: 100%; font-weight:bolt;">KETIDAKSESUAIAN PRODUK</td>
            </tr>
            <tr>
                <td style="text-align: left; font-size: 70%;"></td>
                <td style="text-align: left; font-size: 70%; border-bottom: 0.5px solid black;">No. Dokumen</td>
                <td style="text-align: left; font-size: 70%; border-bottom: 0.5px solid black;">: CAT4-QLC-007</td>
            </tr>
            <tr>
                <td style="text-align: left; font-size: 70%;"></td>
                <td style="text-align: left; font-size: 70%; border-bottom: 0.5px solid black;">Tanggal Terbit</td>
                <td style="text-align: left; font-size: 70%; border-bottom: 0.5px solid black;">: 14 Jun 2024</td>
            </tr>
            <tr>
                <td style="text-align: left; font-size: 70%;"></td>
                <td style="text-align: left; font-size: 70%; border-bottom: 0.5px solid black;">Rev</td>
                <td style="text-align: left; font-size: 70%; border-bottom: 0.5px solid black;">: 03</td>
            </tr>
        </table>';

        /** Keterangan */
        $keterangan =
            '<table cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 80%;">
            <tr>
                <td width="20%"> No. </td>
                <td width="30%"> : ' . $this->_no . ' </td>
            </tr>
            <tr>
                <td width="20%"> Tanggal </td>
                <td width="30%"> : ' . date('d-m-Y', strtotime($this->_tanggal)) . ' </td>
            </tr>
        </table>';

        $this->writeHTML($header, true, false, false, false, '');
        $this->writeHTML($keterangan, true, false, false, false, '');
    }
}

$pdf = new customPdf('P', 'mm', 'A4', true, 'UTF-8', false);
$title = 'Laporan Ketidaksesuaian Produk';
$pdf->SetTitle($title);
$pdf->SetAutoPageBreak(true, 10);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');
$pdf->setPrintFooter(false);

/** Margin */
$pdf->SetHeaderMargin(5);
$pdf->SetFooterMargin(7);
$pdf->SetMargins(8, 42, 8);

$dataHeader = array(
    '_no' => $master['inspectionproduct_no'],
    '_tanggal' => date('d-m-Y', strtotime($master['inspectionproduct_tanggal'])),
);
$pdf->setsHeader($dataHeader);

$pdf->AddPage();


$pdf->writeHTML($konten, true, false, false, false, '');
// Foreach footer TTD
$created = null; $createdTgl = null; $approved = null; $approvedTgl = null; $diketahui = null; $diketahuiTgl = null;
foreach($footer_data as $value):
    switch ($value['inspectionproductlogstate_kd']) {
        case '5':
            $created = $value['nm_admin'];
            $createdTgl = format_date($value['inspectionproductlog_tglinput'], "d-m-Y H:i:s");
            break;
        case '2':
            $approved = $value['nm_admin'];
            $approvedTgl = format_date($value['inspectionproductlog_tglinput'], "d-m-Y H:i:s");
            break;
        case '3':
            $diketahui = $value['nm_admin'];
            $diketahuiTgl = format_date($value['inspectionproductlog_tglinput'], "d-m-Y H:i:s");
            break;
    }
endforeach;

/** Footer */
$footer = '<table id="idtablePass" cellspacing="0" cellpadding="1" style="font-size: 80%;" border="0"  width="100%">';
$footer .= '<tbody>
            <tr>
                <td width="15%">Dibuat oleh </td>
                <td>: '.$created.", &nbsp; ".$createdTgl .'</td>
            </tr>
            <tr>
                <td>Disetujui oleh </td>
                <td>: '.$approved.", &nbsp; ".$approvedTgl .'</td>
            </tr>
            <tr>
                <td>Diketahui oleh </td>
                <td>: '.$diketahui.", &nbsp; ".$diketahuiTgl .'</td>
            </tr>
            </tbody>';
$footer .= '</table>';

$pdf->writeHTML($footer, true, false, false, false, '');


$txtOutput = $title . '.pdf';
$pdf->Output($txtOutput, 'I');
