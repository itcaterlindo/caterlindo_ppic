<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtKd', 'value' => empty($inspection) ? '' : $inspection['kd_inspectionproduct']));
?>
<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Tanggal Inspection</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrTglInspection"></div>
		<?php echo form_input(array('name' => 'txtTglInspection', 'id' => 'idTxtTglInspection', 'class' => 'form-control datetimepicker', 'placeholder' => 'Tanggal Inspection', 'value' => empty($inspection) ? '' : format_date($inspection['inspectionproduct_tanggal'], 'd-m-Y') )); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">No. WO</label>
	<div class="col-sm-2 col-xs-12">
		<div id="idErrNoWo"></div>
		<select class="form-control select2" name="txtNoWo" id="idTxtNoWo" data-allow-clear="true">
			<?php if( !empty($inspection) ){ ?>
				<option value="<?= $inspection['inspectionproduct_woitem_kd'] ?>" selected><?= $inspection['woitem_no_wo'] ?></option>
			<?php } ?>
		</select>
	</div>
</div>
<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Item Code</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrItemCode"></div>
		<?php echo form_input(array('name' => 'txtItemCode', 'id' => 'idTxtItemCode', 'class' => 'form-control', 'placeholder' => 'Item Code', 'value' => empty($inspection) ? '' : $inspection['inspectionproduct_item_code'])); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Posisi</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrBagianKd"></div>
		<select class="form-control select2" name="txtBagianKd" id="idTxtBagianKd" data-allow-clear="true">
			<?php if( !empty($inspection) ){ ?>
				<option value="<?= $inspection['inspectionproduct_bagian_kd'] ?>" selected><?= $inspection['bagian_nama'] ?></option>
			<?php } ?>
		</select>
	</div>
</div>
<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Tindakan</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrTidakan"></div>
		<select class="form-control select2" name="txtTindakan" id="idTxtTindakan" data-allow-clear="true">
			<option value="">-- Pilih Opsi --</option>
			<option value="reject" <?= $inspection['inspectionproduct_tindakan'] == 'reject' ? 'selected' : '' ?> >Reject</option>
			<option value="repair" <?= $inspection['inspectionproduct_tindakan'] == 'repair' ? 'selected' : '' ?> >Repair</option>
			<option value="downgrade" <?= $inspection['inspectionproduct_tindakan'] == 'downgrade' ? 'selected' : '' ?> >Downgrade</option>
		</select>
	</div>
</div>

<div class="form-group" id="repair-oleh" <?= $inspection['inspectionproduct_tindakan'] != 'repair' ? 'hidden' : '' ?> >
	<label for="idTxtInspection" class="col-md-2 control-label">Repair Oleh</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrRepair"></div>
			<div class="checkbox">
				<?php foreach($bagian as $bag){ ?>
					<label>
						<input type="checkbox" name="txtRepairBagian[]" value="<?= $bag['bagian_kd'] ?>" <?= in_array($bag['bagian_kd'], array_column($repairBagian, 'bagian_kd')) ? 'checked' : '' ?> > <?= $bag['bagian_nama'] ?>
					</label> <br>
				<?php } ?>
			</div>
	</div>
</div>

<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Qty WO</label>
	<div class="col-sm-1 col-xs-12">
		<div id="idErrQtyWo"></div>
		<?php echo form_input(array('name' => 'txtQtyWo', 'id' => 'idTxtQtyWo', 'class' => 'form-control', 'placeholder' => 'Qty WO', 'readonly' => 'readonly')); ?>
	</div>
</div>

<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Qty</label>
	<div class="col-sm-1 col-xs-12">
		<div id="idErrQty"></div>
		<div id="idErrCheckQty"></div>
		<?php echo form_input(array('name' => 'txtQty', 'id' => 'idTxtQty', 'type' => 'number', 'class' => 'form-control', 'placeholder' => 'Qty', 'value' => empty($inspection) ? '' : $inspection['inspectionproduct_qty'])); ?>
	</div>
</div>

<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Posisi Penyebab</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrPenyebabBagian"></div>
			<select class="form-control select2" name="txtPenyebabBagian" id="idTxtPenyebabBagian" data-allow-clear="true">
				<?php if( !empty($inspection) ){ ?>
					<option value="<?= $inspection['inspectionproduct_penyebab_bagian_kd'] ?>" selected><?= $inspection['bagian_nama3'] ?></option>
				<?php } ?>
			</select>
	</div>
</div>
<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Penyebab</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrPenyebab"></div>
		<textarea class="form-control" name="txtPenyebab" id="idTxtPenyebab" cols="20" rows="5"><?= empty($inspection) ? '' : $inspection['inspectionproduct_penyebab'] ?></textarea>
	</div>
</div>
<div class="form-group">
	<label for="idTxtInspection" class="col-md-2 control-label">Detail Ketidaksesuaian</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrKeterangan"></div>
		<textarea class="form-control" name="txtKeterangan" id="idTxtKeterangan" cols="20" rows="5"><?= empty($inspection) ? '' : $inspection['inspectionproduct_keterangan'] ?></textarea>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" onclick="resetForm()" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" onclick="submitData()" class="btn btn-primary btn-flat">
			<i class="fa fa-save"></i> Submit
		</button>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">

	$(document).ready(function(e){
		render_wo();
		render_bagian('#idTxtBagianKd');
		render_bagian_penyebab('#idTxtPenyebabBagian');
		$('#idTxtKategori').select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
		});
		$('#idTxtTglInspection').datetimepicker({
			format: 'DD-MM-YYYY',
		});
		$('#idTxtTindakan').on('change', function(e){
			e.preventDefault();
			let txtvalue = $(this).val();
			if(txtvalue == 'repair'){
				$('#repair-oleh').show();
			}else{
				$('#repair-oleh').hide();
			}
		});
	});

	$(document).off('select2:selecting', '#idTxtNoWo').on("select2:selecting", '#idTxtNoWo', function(e) { 
		let dt = e.params.args.data;
		$('#idTxtItemCode').val(dt.item_code);
		$('#idTxtQtyWo').val(dt.woitem_qty);
	});

	$(document).off('select2:unselecting', '#idTxtNoWo').on("select2:unselecting", '#idTxtNoWo', function(e) { 
		$('#idTxtItemCode').val(null);
		$('#idTxtQtyWo').val(null);
		$('#idTxtBagianKd').empty();
		$('#idTxtPenyebabBagian').empty();
	});

	function render_wo(){
		/** Render WO Select2 */
		$("#idTxtNoWo").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?= base_url().$class_link ?>/get_wo',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramWO: params.term, // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_bagian(id){
		/** Render DN Select2 */
		$(id).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?= base_url().$class_link ?>/get_all_bagian_bydn',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramDN: params.term, // search term
						paramWO: $('#idTxtNoWo').val()
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_bagian_penyebab(id){
		/** Render DN Select2 */
		$(id).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?= base_url().$class_link ?>/get_all_bagian_bydn',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramDN: params.term, // search term
						paramWO: $('#idTxtNoWo').val()
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	/** Regenerate token */
	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitData() {
		event.preventDefault();
		if(confirm('Apakah anda ingin menyimpan data tersebut ? ')){
			var form = document.getElementById('<?php echo $form_id; ?>');
			$.ajax({
				url: "<?php echo base_url().$class_link; ?>/action_submit",
				type: "POST",
				data:  new FormData(form),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						generateToken (resp.csrf);
						remove_form();
						refreshTable();
					}else if (resp.code == 401){
						$.each( resp.pesan, function( key, value ) {
							$('#'+key).html(value);
						});
						generateToken (resp.csrf);
						box_overlay('out');
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				} 	        
			});
		}
	}

	$('#idbtnReset').click(function(){
		resetForm();
	});
	
	/** Reset Form */
	function resetForm(){
		var form = document.getElementById('<?php echo $form_id; ?>');
		$("#idTxtNoWo").empty();
		form.reset();
	}

</script>