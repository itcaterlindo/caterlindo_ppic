<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table();
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		get_form('');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table() {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_main'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown(html);
				}
			});
		});
	}

	function refreshTable(){
		table.ajax.reload();
		tableLokasiDitemukan.ajax.reload();
		tablePenyebab.ajax.reload();
		tableRepair.ajax.reload();
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function remove_box() 
	{
		$('#idFormBox<?php echo $master_var; ?>').remove();
		$('#idTableBox<?php echo $master_var; ?>').remove();
	}

	function get_form(id) {
		remove_box();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_box'; ?>',
			data: 'id='+id,
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function buat_dn(){
		$('#modal').modal('show');
	}

	function cetak(id) {
		window.open('<?php echo base_url().$class_link;?>/report_pdf?id='+id);
	}

	function hapus_data(id) {
		// $('#<?php //echo $box_overlay_id; ?>').show();
		var conf = confirm('Apakah anda yakin menghapus item ini ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					// console.log(resp);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						refreshTable();
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
					}
					// $('#<?php //echo $box_overlay_id; ?>').hide();
				}
			});
		}
	}

	function action_state(id, state) {
		// $('#<?php //echo $box_overlay_id; ?>').show();
		var conf = confirm('Apakah anda yakin ingin proses data ini ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_state'; ?>',
				data: {id: id, state: state},
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						refreshTable();
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
					}
					// $('#<?php //echo $box_overlay_id; ?>').hide();
				}
			});
		}
	}

	function approved_bagian(id){
		// $('#<?php //echo $box_overlay_id; ?>').show();
		var conf = confirm('Apakah anda yakin ingin approved data bagian ini ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_approved_bagian'; ?>',
				data: {id: id},
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						refreshTable();
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
					}
					// $('#<?php //echo $box_overlay_id; ?>').hide();
				}
			});
		}
	}

	function approved_penyebab(id){
		// $('#<?php //echo $box_overlay_id; ?>').show();
		var conf = confirm('Apakah anda yakin ingin approved data penyebab ini ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_approved_penyebab'; ?>',
				data: {id: id},
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						refreshTable();
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
					}
					// $('#<?php //echo $box_overlay_id; ?>').hide();
				}
			});
		}
	}

	function approved_repair(id, bagian_kd){
		// $('#<?php //echo $box_overlay_id; ?>').show();
		var conf = confirm('Apakah anda yakin ingin approved data repair ini ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_approved_repair'; ?>',
				data: {id: id, bagian_kd: bagian_kd},
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						refreshTable();
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
					}
					// $('#<?php //echo $box_overlay_id; ?>').hide();
				}
			});
		}
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }
	

</script>