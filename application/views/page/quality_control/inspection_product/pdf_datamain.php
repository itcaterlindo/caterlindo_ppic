<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">

    /* table { border: 2px solid black 
    }

    td { border: thin solid black 
    } */

    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }
</style>

<table id="idtablemain" cellspacing="0" cellpadding="1" style="font-size: 80%; border: 1px solid black; border-collapse: collapse;"  width="100%">
    <tbody>
       <!-- <tr>
            <td rowspan="3" style="text-align: center; vertical-align: middle; border: 1px solid black; border-collapse: collapse;">Produk</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
       </tr> -->
       <tr>
            <td>Item / Deskripsi Produk </td>
            <td>: <?= $master['inspectionproduct_item_code'] ?> / <?= $master['woitem_deskripsi'] ?></td>
            <td>&nbsp;</td>
       </tr>
       <tr>
            <td>No. WO / PO </td>
            <td>: <?= $master['woitem_no_wo'] ?></td>
            <td align="left">Quantity : <?= $master['inspectionproduct_qty'] ?></td>
       </tr>
    </tbody>
</table>

<h6>&nbsp;</h6>

<table id="idtableKetidaksesuaian" cellspacing="0" cellpadding="1" style="font-size: 80%; border: 1px solid black; border-collapse: collapse;"  width="100%">
    <tbody>
        <tr>
          <td style="border: 1px solid black; text-align: center">Detail Ketidaksesuaian</td>
          <td style="border: 1px solid black; text-align: center">Posisi Barang</td>
        </tr>
        <tr>
          <td style="border-right: 1px solid black;">&nbsp;</td>
          <td><?= !empty($master['bagian_nama']) ? $master['bagian_nama'] : '' ?></td>
        </tr>
        <tr>
          <td style="border-right: 1px solid black;"><?= $master['inspectionproduct_keterangan'] ?></td>
          <td><?= !empty($master['nm_admin2']) ? 'Approved oleh, '.$master['nm_admin2'] : '' ?></td>
        </tr>
        <tr>
          <td style="border-right: 1px solid black;">&nbsp;</td>
          <td><?= !empty($master['nm_admin2']) ? format_date($master['approved_bagian_tgl'], "d-m-Y H:i:s") : '' ?></td>
        </tr>
    </tbody>
</table>

<h6>&nbsp;</h6>

<table id="idtablePenyebab" cellspacing="0" cellpadding="1" style="font-size: 80%; border: 1px solid black; border-collapse: collapse;"  width="100%">
    <tbody>
        <tr>
          <td style="border: 1px solid black; text-align: center">Penyebab</td>
          <td style="border: 1px solid black; text-align: center">SIGN</td>
        </tr>
        <tr>
          <td style="border-right: 1px solid black;">&nbsp;</td>
          <td><?= !empty($master['bagian_nama3']) ? $master['bagian_nama3'] : '' ?></td>
        </tr>
        <tr>
          <td style="border-right: 1px solid black;"><?= $master['inspectionproduct_penyebab'] ?></td>
          <td><?= !empty($master['nm_admin3']) ? 'Approved oleh, '.$master['nm_admin3'] : '' ?></td>
        </tr>
        <tr>
          <td style="border-right: 1px solid black;">&nbsp;</td>
          <td><?= !empty($master['nm_admin3']) ? format_date($master['approved_penyebab_tgl'], "d-m-Y H:i:s") : '' ?></td>
        </tr>
    </tbody>
</table>

<h6>&nbsp;</h6>

<table id="idtableTindakan" cellspacing="0" cellpadding="1" style="font-size: 80%; border: 1px solid black; border-collapse: collapse;"  width="100%">
    <tbody>
        <tr>
          <td style="border: 1px solid black; text-align: center">Tindakan</td>
          <td style="border: 1px solid black; text-align: center">SIGN</td>
        </tr>
        <tr>
          <td style="border-right: 1px solid black;"><input type="checkbox" name="box" value="1" readonly="true" checked="<?= $master['inspectionproduct_tindakan'] == 'reject' ? 'checked' : '' ?>"> Reject</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td style="border-right: 1px solid black;"><input type="checkbox" name="box" value="1" readonly="true" checked="<?= $master['inspectionproduct_tindakan'] == 'repair' ? 'checked' : '' ?>"> Repair, Oleh :</td>
          <td>&nbsp;</td>
        </tr>
        <?php foreach($master_bagian as $bag){ $approved_repair_sign = null; ?>
        <tr>
          <td style="border-right: 1px solid black;">&nbsp; &nbsp; &nbsp; <input type="checkbox" name="box" value="1" readonly="true" checked="<?= in_array($bag['bagian_kd'], array_column($repair_bagian, 'bagian_kd')) ? 'checked' : '' ?>"> <?= $bag['bagian_nama'] ?> </td>
          <?php 
          /** Get data repair */
          foreach($repair_bagian as $key => $val){ 
              if($val['bagian_kd'] == $bag['bagian_kd'] && $val['flag_approved'] == '1'){
                  $approved_repair_sign = $val['nm_admin'].", ".format_date($val['tgl_approved'], "d-m-Y H:i:s");    
              }
          }
          /** End get data repair */
          ?>
          <td><?= $approved_repair_sign ?></td>
        </tr>
        <?php } ?>
        <tr>
          <td style="border-right: 1px solid black;"><input type="checkbox" name="box" value="1" readonly="true" checked="<?= $master['inspectionproduct_tindakan'] == 'downgrade' ? 'checked' : '' ?>"> Downgrade</td>
          <td>&nbsp;</td>
        </tr>
    </tbody>
</table>

<br/>
<br/>
<br/>