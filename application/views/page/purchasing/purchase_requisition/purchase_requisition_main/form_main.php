<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
extract($rowDataMasterPR);

?>
<div class="row invoice-info">
	<div class="col-sm-4 invoice-col">
		<b>Requistion No :</b> <?php echo isset($pr_no) ? $pr_no : '-'; ?><br>
		<div class="no-print">
			<b>Requistion Status :</b> <?php echo build_badgecolor($wfstate_badgecolor, $wfstate_nama); ?> <br>
		</div>
	</div>
	<div class="col-sm-4 invoice-col">
		<b>Requisition User :</b> <?php echo isset($nm_admin) ? $nm_admin : '-'; ?> <br>
		<b>Requisition Bagian :</b> <?php echo isset($bagian_nama) ? $bagian_nama : '-'; ?>
	</div>
	<div class="col-sm-4 invoice-col">
		<b>Requisition Tanggal :</b> <?php echo format_date($pr_tglinput, 'd-m-Y'); ?>
	</div>
</div>
<hr>

<?php if ( cek_permission('PURCHASEREQUISITION_VIEW_SUGGEST') ) :?>
<div class="row">
	<div class="col-md-12">
		<button class="btn btn-sm btn-warning" onclick="suggestpr('<?php echo $pr_kd; ?>')"> <i class="fa fa-search"></i> View Suggest PR</button>
	</div>
</div>
<hr>
<?php endif; ?>

<?php
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtSts', 'name' => 'txtSts', 'placeholder' => 'idtxtSts', 'value' => $sts));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtpr_kd', 'name' => 'txtpr_kd', 'placeholder' => 'idtxtpr_kd', 'value' => isset($pr_kd) ? $pr_kd : null));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtStsDetail', 'placeholder' => 'idtxtStsDetail', 'name' => 'txtStsDetail', 'class' => 'tt-input'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtRm_kd', 'placeholder' => 'idtxtRm_kd', 'name' => 'txtRm_kd', 'class' => 'tt-input'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtprdetail_kd', 'name' => 'txtprdetail_kd', 'placeholder' => 'idtxtprdetail_kd', 'class' => 'tt-input'));
?>

<div class="row">
	<div class="form-group">
		<label for='idtxtRm_jenis' class="col-md-2 control-label">Jenis Material</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrRm_jenis"></div>
			<?php echo form_dropdown('txtRm_jenis', $opsiJenisMaterial, isset($MaterialJenisSelected) ? $MaterialJenisSelected : '', array('class' => 'form-control', 'id' => 'idtxtRm_jenis')); ?>
		</div>
		<button class="btn btn-sm btn-success" id="idbtnCari" onclick="cariUnit()"> <i class="fa fa-search"></i> Cari </button>
	</div>

	<div class="form-group">
		<label for='idtxtrm_kode' class="col-md-2 control-label">Kode Material</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrrm_kode"></div>
			<?php echo form_input(array('type' => 'text', 'id' => 'idtxtrm_kode', 'placeholder' => 'Kode Material', 'name' => 'txtrm_kode', 'readonly' => 'true', 'class' => 'form-control tt-input')); ?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtprdetail_nama' class="col-md-2 control-label">Nama Material</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrprdetail_nama"></div>
			<?php echo form_input(array('type' => 'text', 'class' => 'form-control tt-input', 'name' => 'txtprdetail_nama', 'id' => 'idtxtprdetail_nama', 'placeholder' => 'Nama Material', 'readonly' => 'true', 'value' => isset($txtprdetail_nama) ? $txtprdetail_nama : null)); ?>
		</div>
		<!-- <label for='idtxtrmsatuan_kd' class="col-md-1 control-label">Satuan</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrrmsatuan_kd"></div>
			<?php //echo form_dropdown('txtrmsatuan_kd', $opsiSatuan, isset($satuanSelected)? $satuanSelected : '', array('class'=> 'form-control select2', 'id'=> 'idtxtrmsatuan_kd'));
			?>
		</div>	 -->
	</div>

	<div class="form-group">
		<label for="idtxtprdetail_deskripsi" class="col-md-2 control-label">Deskripsi Material</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrprdetail_deskripsi"></div>
			<?php echo form_textarea(array('type' => 'text', 'class' => 'form-control tt-input', 'name' => 'txtprdetail_deskripsi', 'id' => 'idtxtprdetail_deskripsi', 'placeholder' => 'Deskripsi Material', 'rows' => '2', 'value' => isset($prdetail_deskripsi) ? $prdetail_deskripsi : null)); ?>
		</div>
	</div>

	<div class="form-group">
		<label for="idtxtprdetail_spesifikasi" class="col-md-2 control-label">Spesifikasi Material</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrprdetail_spesifikasi"></div>
			<?php echo form_textarea(array('type' => 'text', 'class' => 'form-control tt-input', 'name' => 'txtprdetail_spesifikasi', 'id' => 'idtxtprdetail_spesifikasi', 'placeholder' => 'Spesifikasi Material', 'rows' => '2', 'value' => isset($prdetail_spesifikasi) ? $prdetail_spesifikasi : null)); ?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtprdetail_qty' class="col-md-2 control-label">Qty</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrprdetail_qty"></div>
			<?php echo form_input(array('type' => 'number', 'class' => 'form-control tt-input tt-konversi', 'name' => 'txtprdetail_qty', 'id' => 'idtxtprdetail_qty', 'placeholder' => 'Qty', 'value' => isset($prdetail_qty) ? $prdetail_qty : null)); ?>
		</div>

		<label for='idtxtrmsatuan_kd' class="col-md-1 control-label">Satuan</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrrmsatuan_kd"></div>
			<?php echo form_dropdown('txtrmsatuan_kd', $opsiSatuan, isset($satuanSelected) ? $satuanSelected : '', array('class' => 'form-control select2', 'id' => 'idtxtrmsatuan_kd')); ?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtprdetail_konversi' class="col-md-2 control-label">Konversi</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrprdetail_konversi"></div>
			<?php echo form_input(array('type' => 'number', 'class' => 'form-control tt-input tt-konversi', 'name' => 'txtprdetail_konversi', 'id' => 'idtxtprdetail_konversi', 'placeholder' => 'Qty', 'value' => isset($prdetail_konversi) ? $prdetail_konversi : null)); ?>
		</div>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrprdetail_qtykonversi"></div>
			<?php echo form_input(array('type' => 'number', 'class' => 'form-control tt-input', 'name' => 'txtprdetail_qtykonversi', 'id' => 'idtxtprdetail_qtykonversi', 'placeholder' => 'Qty Konversi', 'readonly' => 'readonly', 'value' => isset($prdetail_qtykonversi) ? $prdetail_qtykonversi : null)); ?>
		</div>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrprdetail_satuankonversi"></div>
			<input type="hidden" name="txtprdetail_satuankonversi_kd" id="idtxtprdetail_satuankonversi_kd" class="tt-input" placeholder="idtxtprdetail_satuankonversi_kd">
			<?php echo form_input(array('type' => 'text', 'class' => 'form-control tt-input', 'name' => 'txtprdetail_satuankonversi', 'id' => 'idtxtprdetail_satuankonversi', 'placeholder' => 'Satuan Konversi', 'readonly' => 'readonly', 'value' => isset($prdetail_satuankonversi) ? $prdetail_satuankonversi : null)); ?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtprdetail_duedate' class="col-md-2 control-label">Due Date</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrprdetail_duedate"></div>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				<?php echo form_input(array('type' => 'text', 'class' => 'form-control dateptimepicker tt-input', 'name' => 'txtprdetail_duedate', 'id' => 'idtxtprdetail_duedate', 'placeholder' => 'Due Date', 'value' => isset($prdetail_duedate) ? $prdetail_duedate : null)); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for='idtxtprdetail_duedate' class="col-md-2 control-label">Budgeting by</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrprdetail_duedate"></div>
			<div class="input-group">
			<select name="txtbudgeting_detail_kd" class="form-control form-control-sm budgeting_by" id="budgeting_by">
				<option value="">-- Pilih Items --</option>
				</select>	
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="idtxtprdetail_remarks" class="col-md-2 control-label">Remarks</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrprdetail_remarks"></div>
			<?php echo form_textarea(array('type' => 'text', 'class' => 'form-control tt-input', 'name' => 'txtprdetail_remarks', 'id' => 'idtxtprdetail_remarks', 'placeholder' => 'Remarks', 'rows' => '2', 'value' => isset($prdetail_remarks) ? $prdetail_remarks : null)); ?>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-4 col-sm-offset-9 col-xs-12">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData()" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button type="reset" name="btnReset" onclick="resetFormMain()" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
			<button onclick="event.preventDefault(); window.location.assign('<?php echo base_url() . $class_link; ?>')" class="btn btn-success btn-sm">
				<i class="fa fa-check"></i> Selesai
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>