<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form('<?php echo isset($sts) ? $sts : null; ?>', '<?php echo isset($id) ? $id : null; ?>');
	open_table_detail('<?php echo isset($sts) ? $sts : null; ?>', '<?php echo isset($id) ? $id : null; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#<?php echo $box_loader_id; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
		open_table();
		first_load('<?php echo 'idTableBoxLoader'.$master_var; ?>', '<?php echo 'idTableBoxContent'.$master_var; ?>');
	});

	$(document).off('keyup', '.tt-konversi').on('keyup', '.tt-konversi', function() {
		var qtykonversi = parseFloat($('#idtxtprdetail_qty').val()) * parseFloat($('#idtxtprdetail_konversi').val());
		if (isNaN(qtykonversi)){
			qtykonversi = 0;
		}
		$('#idtxtprdetail_qtykonversi').val(qtykonversi);
	});

	function open_form(sts, id) {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/form_main'; ?>',
				data: {'sts': sts,'id' : id},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					render_select2('select2');
					render_datetimepicker('dateptimepicker');
					render_budgeting();
					moveTo('idMainContent');
				}
			});
		});
	}
	
	function render_budgeting() {
		$.ajax({
			type: "GET",
			url: "https://caterlindo.co.id/caterlindo_support/public/api/support/budgeting/v1/getAllDetailbudgeting",
			dataType: "json",
			success: function (response) {
				if(response){
					var riwayat = '';
                        for(var i of response) {
                            riwayat += `<option value="`+i.id+`">`+i.text+`</option>`
                        }
                    }
                    $("#budgeting_by").append(riwayat);

                    $('#budgeting_by').select2({
                       	 	placeholder: '-- Cari Items --',
                    });
			}
		});
	}

	function open_table_detail(sts = 'add', id = null) {
		$('#<?php echo $box_content_detail_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_detail_main'; ?>',
				data: {'sts': sts, 'id' : id},
				success: function(html) {
					$('#<?php echo $box_content_detail_id; ?>').html(html);
					$('#<?php echo $box_content_detail_id; ?>').slideDown();
				}
			});
		});
	}

	function open_rekomendasi_material(){
		event.preventDefault();
		var id = $('#idtxtpr_kd').val();
		open_table_rekomendasi_material('add', id);
	}
	
	function open_rekomendasi_po(){
		event.preventDefault();
		var id = $('#idtxtpr_kd').val();
		open_rekomendasi_po('add', id);
	}

	function open_table_rekomendasi_material(sts, id){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_linkRekomendasi.'/table_rekomendasi_material'; ?>',
			data: {'sts': sts, 'id' : id},
			success: function(html) {
				toggle_modal('Rekomendasi Dari Stok Gudang RM', html);
			}
		});
	}

	function open_rekomendasi_po(sts, id){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_linkRekomendasi.'/form_rekomendasi_po'; ?>',
			data: {'sts': sts, 'id' : id},
			success: function(html) {
				toggle_modal('Rekomendasi Dari PO', html);
				render_select2('select2');
			}
		});
	}

	function cariUnit (){
		event.preventDefault();
		var jns = $('#idtxtRm_jenis').val();
		var id = $('#idtxtpr_kd').val();

		if (jns == 'STD'){			
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link;?>/table_partial_material',
				data: {id: id},
				success: function(html) {
					toggle_modal('Material', html);
				}
			});
		}
	}

	function suggestpr(pr_kd){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'menu_ppic/purchase_suggestion/purchase_suggestion_main/viewsuggestpr_box?pr_kd=';?>'+pr_kd,
			data: {pr_kd: pr_kd},
			success: function(html) {
				toggle_modal('Suggest PR', html);
			}
		});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	$(document).off('change', '#idtxtRm_kd').on('change', '#idtxtRm_kd', function(e){
		var pilihan = $(this).val();
		getMaterialData(pilihan);
	});

	function selectRM(id){
		event.preventDefault();
		getMaterialData(id);
		toggle_modal('', '')
	}

	$(document).off('change', '#idtxtRm_jenis').on('change', '#idtxtRm_jenis', function(e){
		var pilihan = $(this).val();
		if (pilihan == 'NEW'){
			$('#idtxtRm_kd').val('SPECIAL').trigger('change');
			$('#idtxtprdetail_nama').prop('readonly', false);
			$('#idbtnCari').slideUp();
		}else if(pilihan == 'STD'){
			$('#idtxtprdetail_nama').prop('readonly', true);
			$('#idbtnCari').slideDown();
			resetFormMain();
		}
	});
	
	function getMaterialData(materialKd){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/getMaterialData'; ?>',
			data: {'id' : materialKd},
			success: function(data) {
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$('#idtxtRm_kd').val(resp.data.rm_kd);
					$('#idtxtrm_kode').val(resp.data.rm_kode);
					$('#idtxtprdetail_nama').val(resp.data.rm_nama);
					$('#idtxtrmsatuan_kd').val(resp.data.rmsatuansecondary_kd).trigger('change');
					$('#idtxtprdetail_deskripsi').val(resp.data.rm_deskripsi);
					$('#idtxtprdetail_spesifikasi').val(resp.data.rm_spesifikasi);
					$('#idtxtprdetail_konversi').val(resp.data.rm_konversiqty);
					$('#idtxtprdetail_satuankonversi_kd').val(resp.data.rmsatuan_kd);
					$('#idtxtprdetail_satuankonversi').val(resp.data.rmsatuan_nama);
					$('#idtxtprdetail_qty').focus();
				}
			}
		});
	}

	function edit_data_detail(id){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/getDetailPR'; ?>',
			data: {'id' : id},
			success: function(data) {
				var resp = JSON.parse(data);
				if (resp.code == 200){
					if (resp.data.rm_kd == 'SPECIAL') {
						$('#idtxtprdetail_nama').prop('readonly', false);
						$('#idtxtprdetail_deskripsi').prop('readonly', false);
						$('#idtxtprdetail_spesifikasi').prop('readonly', false);
					}
					$('#idtxtprdetail_kd').val(resp.data.prdetail_kd);
					$('#idtxtrm_kode').val(resp.data.rm_kode);
					$('#idtxtStsDetail').val('edit');
					$('#idtxtRm_kd').val(resp.data.rm_kd);
					$('#idtxtrmsatuan_kd').val(resp.data.rmsatuan_kd).trigger('change');
					$('#idtxtprdetail_nama').val(resp.data.prdetail_nama);
					$('#idtxtprdetail_deskripsi').val(resp.data.prdetail_deskripsi);
					$('#idtxtprdetail_spesifikasi').val(resp.data.prdetail_spesifikasi);
					$('#idtxtprdetail_qty').val(resp.data.prdetail_qty);
					$('#idtxtprdetail_konversi').val(resp.data.prdetail_konversi);
					$('#idtxtprdetail_qtykonversi').val(resp.data.prdetail_qtykonversi);
					$('#idtxtprdetail_duedate').val(resp.data.prdetailduedate_format);
					$('#idtxtprdetail_remarks').val(resp.data.prdetail_remarks);
					moveTo('idMainContent');

					if(resp.data.budgeting_detail_kd != null){
						setValueBudgeting(resp.data.budgeting_detail_kd);
					}
				}
			}
		});
	}

	function setValueBudgeting(params) { 
		$("#budgeting_by").val(params).trigger("change");
	 }

	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
		boxOverlayForm('out');
	}

	function submitData() {
		boxOverlayForm('in');
		event.preventDefault();
		var form = document.getElementById('idFormInput');
		// edit atau add
		var sts =  $('#idtxtStsDetail').val();
		url = "<?php echo base_url().$class_link; ?>/action_insert";
		if (sts == 'edit'){
			url = "<?php echo base_url().$class_link; ?>/action_update";
		}

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					var id = $('#idtxtpr_kd').val();
					open_table_detail(sts, id)
					$('.errInput').html('');

					$('#idtxtRm_jenis').val('STD').trigger('change');
					$("#budgeting_by").val(null).trigger("change");
					$('#idtxtprdetail_deskripsi').prop('readonly', false);
					$('#idtxtprdetail_spesifikasi').prop('readonly', false);
					resetFormMain();
					$('#idbtnCari').focus();

					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					boxOverlayForm('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function submitRekomendasi (){
		event.preventDefault();
		var form = document.getElementById('idFormRekomendasi');
		url = "<?php echo base_url().$class_link; ?>/action_rekomendasi_insert";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				console.log(resp);
				// if(resp.code == 200){
				// 	open_form(sts, resp.data.pr_kd);
				// 	open_table_detail(sts, resp.data.pr_kd);
				// 	$('.errInput').html('');
				// }else if (resp.code == 401){
				// 	$.each( resp.pesan, function( key, value ) {
				// 		$('#'+key).html(value);
				// 	});
				// 	generateToken (resp.csrf);
				// }else if (resp.code == 400){
				// 	notify (resp.status, resp.pesan, 'error');
				// 	generateToken (resp.csrf);
				// }else{
				// 	notify ('Error', 'Error tidak Diketahui', 'error');
				// 	generateToken (resp.csrf);
				// }
			} 	        
		});
	}

	function resetFormMain(){
		event.preventDefault();
		$('.tt-input').val('');
		$('#idtxtRm_kd').val('').trigger('change');
		$('#idtxtrmsatuan_kd').val('').trigger('change');
	}

	function boxOverlayForm(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

	function render_select2(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_datetimepicker(valClass){
		$('.'+valClass).datetimepicker({
			format: 'YYYY-MM-DD',
    	});
	}

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }

</script>