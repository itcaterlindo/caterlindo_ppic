<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	
	
	open_form_master('<?php echo $sts; ?>', '<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	var data_all_ticket = [];
	var p_array = [];
	var final_data = [];
	
	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#<?php echo $box_loader_id; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
		open_table();
		first_load('<?php echo 'idTableBoxLoader'.$master_var; ?>', '<?php echo 'idTableBoxContent'.$master_var; ?>');
		
	});

	function open_form_master(sts, id){
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/form_master_main'; ?>',
				data: {'sts': sts, 'id' : id},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
					render_materialreq()
				}
			});
		});
	}

	function render_materialreq(){
		$.ajax({
			type: "GET",
			url: "https://caterlindo.co.id/caterlindo_support/public/api/getTicketActive",
			dataType: "json",
			success: function (response) {
				if(response.length != 0){
					data_all_ticket = response;
					$.each(response, function (i, item) {
					
						$('#get_ticket').append($('<option>', { 
							value: item.opsi,
							text : item.ticket_kode + ' || ' + item.aset_nama + ' || ' + item.problem
						}));
					});
				}
				change_status();
				
			}
		});
	
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
		boxOverlayForm('out');
	}

	function change_status() {	
		// alert('jalab')
			$('#get_ticket').on('change', function (e) {
				var id = $(this).val();
				var text = $(this).find("option:selected").text();
					// var data = e.params.data;
					// console.log(data);
					
					if (!p_array.find(o => o.id === id && o.text === text)){
						p_array.push({'id' : id, 'text' : text})
						$( "#append_tab" ).append(`<tr>
													<td>`+ text +`</td>
													<td><button type="button" id="del_data" ident="`+ id +`" class="btn btn-primary btn-sm del_data">
															<i class="fa fa-save"></i> Delete
														</button>
													</td>
												</tr>`);
												run_action();
												
					}else{
						alert('data sudah Ada')
					}
				});
	}

		
		function run_action(){
			$('.del_data').click(function (e) { 
				e.preventDefault();
				console.log(typeof($(this).attr('ident')))
				$(this).closest('tr').remove()
				p_array = p_array.filter(p_array => p_array.id != $(this).attr('ident'));
			});
		}

		function submitDataMaster(){
			event.preventDefault();
			let form = document.getElementById('idFormInputMaster');
			let sts =  $('#idtxtStsMaster').val();
				
			console.log(p_array);
			$.ajax({
				url: "<?php echo base_url().$class_link; ?>/action_master_insert",
				type: "POST",
				data:  new FormData(form),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){
					var resp = JSON.parse(data);
					if(resp.code == 200){
						
						if(p_array.length != 0)	{
						
							for (let index = 0; index < p_array.length; index++) {
									final_data.push({'pr_kd' : resp.id, 'ticket_kd' : p_array[index].id, 'keterangan' : p_array[index].text});	
							}

							send_ajax(resp.data.id)
						}else{
							window.location.assign('<?php echo base_url().$class_link?>/formmain_box?id='+resp.data.id);
						}
						
						$('.errInput').html('');
					}else if (resp.code == 401){
						$.each( resp.pesan, function( key, value ) {
							$('#'+key).html(value);
						});
						generateToken (resp.csrf);
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				} 	        
			});
		}


		function send_ajax(items) {
			
			
				// console.log(final_data);
				
				$.ajax({
					type: "POST",
					url: "<?php echo base_url().$class_link; ?>/action_insert_ticket",
					data: {'data' : final_data},
					dataType: "json",
					success: function (response) {
						console.log(response);
						window.location.assign('<?php echo base_url().$class_link?>/formmain_box?id='+items);
					}
				});
			
		
		}



		

	

</script>