<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$form_id = 'idFormMaterial';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpo_kdMaterial', 'name'=> 'txtpo_kdMaterial', 'value' => $id ));
// echo json_encode($resultData);die();
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<div class="col-md-12 table-responsive">
    <table id="idTableMaterial" class="table table-hover table-striped table-bordered" style="width:100%; font-size:90%;">
        <thead>
        <tr>
            <th style="width:2%; text-align:center;">Kode</th>
            <th style="width:2%; text-align:center;">Kode Lama</th>
            <th style="width:2%; text-align:center;">Grup</th>
            <!-- <th style="width:5%; text-align:center;">Nama Material</th> -->
            <th style="width:15%; text-align:center;">Deskripsi</th>
            <th style="width:15%; text-align:center;">Spesifikasi</th>
            <th style="width:7%; text-align:center;">Merk</th>
            <th style="width:7%; text-align:center;">Alias</th>
            <th style="width:3%; text-align:center;">Aksi</th>
        </tr>
        </thead>
        <tbody>
        <?php 
        $no = 1;
        foreach ($resultData as $row): ?>
            <tr>
                <td> <a href="javasript:void(0);" onclick="selectRM('<?php echo $row['rm_kd']?>')"> <strong> <?php echo $row['rm_kode'];?> </strong> </a></td>
                <td><?php echo $row['rm_oldkd'];?></td>
                <td><?php echo $row['rmgroup_nama'];?></td>
                <!-- <td><?php //echo $row['rm_nama'];?></td> -->
                <td><?php echo $row['rm_deskripsi'];?></td>
                <td><?php echo $row['rm_spesifikasi'];?></td>
                <td><?php echo $row['rm_merk'];?></td>
                <td><?php echo $row['rm_alias'];?></td>
                <td style="text-align:center;"> <button class="btn btn-xs btn-success" onclick="selectRM('<?php echo $row['rm_kd']?>')"><i class="fa fa-check-circle"></i> Pilih</button> </td>
            </tr>
        <?php 
            $no++;
        endforeach;?>
        </tbody>
    </table>
    <hr>
</div>

<?php echo form_close(); ?>

<script type="text/javascript">

	render_dt('#idTableMaterial');

	function render_dt(table_elem) {
        // Setup - add a text input to each footer cell
        $(table_elem+' thead tr').clone(true).appendTo( table_elem+' thead' );
        $(table_elem+' thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            if (title != 'Aksi') {
                $(this).html( '<input type="text" size="10" placeholder="Cari '+title+'" />' );
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                });
            }
        });

		var table = $(table_elem).DataTable({
			"paging": true,
            "searching": true,
            "ordering": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
            "columnDefs": [
                {"searchable": true, "orderable": true, "targets": 0},
                {"searchable": true, "orderable": true, "className": "dt-center", "targets": 7},
            ],
		});
	}

</script>