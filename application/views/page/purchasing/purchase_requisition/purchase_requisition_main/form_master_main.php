<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputMaster';
extract($rowDataUser);
if($sts == 'edit'){
    extract($rowData);
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtStsMaster', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpr_kd', 'name'=> 'txtpr_kd', 'value' => isset($pr_kd) ? $pr_kd: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpr_user', 'name'=> 'txtpr_user', 'value' => isset($kd_admin) ? $kd_admin: null ));

?>
<div class="row">
	
    <div class="form-group">
		<label for='idtxtpr_tanggal' class="col-md-2 control-label">Tanggal PR</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpr_tanggal"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtpr_tanggal', 'id'=> 'idtxtpr_tanggal', 'placeholder' =>'PR Tanggal', 'readonly' => 'true', 'value'=> isset($pr_tanggal) ? $pr_tanggal: date('Y-m-d') ));?>
		</div>
	</div>
    <div class="form-group">
		<label for='idtxtpr_user' class="col-md-2 control-label">PR User</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpr_user"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtpr_nmuser', 'id'=> 'idtxtpr_nmuser', 'placeholder' =>'PR User', 'readonly' => 'true', 'value'=> isset($nm_admin) ? $nm_admin: null ));?>
		</div>
		<label for='idtxtpr_bagian' class="col-md-1 control-label">Bagian</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpr_bagian"></div>
			<?php echo form_dropdown('txtpr_bagian', $opsiBagian, isset($BagianSelected)? $BagianSelected : '', array('class'=> 'form-control select2', 'id'=> 'idtxtpr_bagian'));?>
		</div>
	</div>

	<div class="form-group">
        <label for="get_ticket" class="col-sm-2 control-label">Ticket by</label>
        <div class="col-md-4">
            <div class="errClass" id="idErrmaterialreq_kds"></div>
            <select name="txtticket_kd" id="get_ticket" class="form-control select2">
				<option value="">--Pilih Ticket--</option>
			</select>
        </div>
	</div>
	<div class="form-group">
		<div class="col-md-2">

		</div>
		<div class="col-md-5">
			<table class="table table-striped">
				<thead>
					<tr>
					<th>Ticket</th>
					<th>Opsi</th>
					</tr>
				</thead>
					<tbody id="append_tab">
						<?php if ($sts === 'edit') : ?>
							<?php foreach($data_prtk as $items):?>
								<tr>
									<td><?php echo $items['keterangan'] ?></td>
									<td><button type="button" id="del_data_edt" ident="<?php echo $items['pht_kd'] ?>" class="btn btn-primary btn-sm del_data_edt">
											<i class="fa fa-save"></i> Delete
										</button>
									</td>
								</tr>
							<?php endforeach ?>
						<?php endif; ?>
					</tbody>
				
				
			</table>
			<button type="button" id="cet_btn">Cek Data</button>
		</div>
		<div class="col-md-5">

		</div>
	</div>

	<div class="form-group">
		<label for="idtxtpr_note" class="col-sm-2 control-label">Notes</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrpr_note"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtpr_note', 'id'=> 'idtxtpr_note', 'placeholder' =>'Notes', 'rows' => '2', 'value'=> isset($pr_note) ? $pr_note: null ));?>
		</div>
	</div>

</div>
<hr>
<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-1 col-xs-12 col-sm-offset-10">
			<button type="submit" name="btnSubmit" onclick="submitDataMaster()" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>
</div>

<script>
	$(document).ready(function () {
		var p_array = []
		$("#get_ticket").select2();	



			$('.del_data_edt', '#append_tab').on('click', function () {
				var id = $(this).attr('ident');
				var currentRow= $(this).closest("tr"); 
				$.ajax({
					type: "GET",
					url: "<?php echo base_url().$class_link; ?>/action_delete_detail_ticket",
					data: {id : id},
					dataType: "JSON",
					success: function (response) {
						if(response.code == 200){
							currentRow.remove()
						}else{
							alert('Error', 'Error tidak Diketahui');
						}
					}
				});
			});
				
		
		
	});



</script>
<?php echo form_close(); ?>
