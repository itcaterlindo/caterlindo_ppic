<?php
defined('BASEPATH') or exit('No direct script access allowed!');

/* --SET DEFAULT PROPERTY UNTUK BOX-- */
$js_file = 'table_js';
$box_type = 'Table';
$box_title = 'Table Purchase Requisition';
$data['master_var'] = 'PurchaseRequisition';
$data['class_link'] = $class_link;
$data['box_id'] = 'id'.$box_type.'Box'.$data['master_var'];
$data['btn_hide_id'] = 'id'.$box_type.'BtnBoxHide'.$data['master_var'];
$data['btn_add_id'] = 'id'.$box_type.'BtnBoxAdd'.$data['master_var'];
$data['btn_remove_id'] = 'id'.$box_type.'BtnBoxRemove'.$data['master_var'];
$data['box_alert_id'] = 'id'.$box_type.'BoxAlert'.$data['master_var'];
$data['box_loader_id'] = 'id'.$box_type.'BoxLoader'.$data['master_var'];
$data['box_content_id'] = 'id'.$box_type.'BoxContent'.$data['master_var'];
$data['box_overlay_id'] = 'id'.$box_type.'BoxOverlay'.$data['master_var'];
/* --END OF BOX DEFAULT PROPERTY-- */

/* --SET VARIABEL TAMBAHAN DISINI-- */
$data['class_linkReport'] = $class_linkReport;
$data['detail_class_link'] = $detail_class_link;
$data['params'] = $params;
/* --END OF CUSTOM PROPERTY-- */

/* --SET BUTTONS DISINI, NILAI "FALSE" UNTUK TIDAK TAMPIL, NILAI "TRUE" UNTUK MENAMPILKAN BUTTON-- */
$btn_hide = TRUE;
$btn_add = TRUE;
$btn_remove = FALSE;
/* --END OF BUTTONS SETUP-- */
?>

<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">Filter</h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool btn-collapse" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan"><i class="fa fa-minus"></i></button>
		</div>
		<div class="box-body">
			<form role="form">
				<div class="form-group col-sm-2">
                  	<label for="exampleInputEmail1" >Tahun</label>
					<input type="number" name="Y" class="form-control" placeholder="Tahun" value="<?php echo !empty($tahun) ? $tahun : date('Y'); ?>">
                </div>
                <div class="form-group col-sm-3">
                  	<label for="exampleInputPassword1">Bulan</label>
                  	<?php echo form_dropdown('m', $months, !empty($bulan)? $bulan : date('m'), array('class'=> 'form-control'));?>
                </div>
                <div class="form-group col-sm-3">
                  	<label for="exampleInputPassword1">Status</label>
                  	<?php echo form_dropdown('wfstate_kd', $states, !empty($wfstate_kd)? $wfstate_kd : 'ALL', array('class'=> 'form-control'));?>
                </div>
                <div class="form-group col-sm-1">
					<br>
					<button class="btn btn-sm btn-default"> <i class="fa fa-search"></i> Cari </button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="box box-primary" id="<?php echo $data['box_id']; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_title; ?></h3>
		<div class="box-tools pull-right">
			<?php
			if ($btn_add) :
				?>
				<button class="btn btn-box-tool" id="<?php echo $data['btn_add_id']; ?>" data-toggle="tooltip" title="Tambah Data">
					<i class="fa fa-plus"></i>
				</button>
				<?php
			endif;
			if ($btn_hide) :
				?>
				<button class="btn btn-box-tool btn-collapse" id="<?php echo $data['btn_hide_id']; ?>" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
					<i class="fa fa-minus"></i>
				</button>
				<?php
			endif;
			if ($btn_remove) :
				?>
				<button class="btn btn-box-tool btn-remove" id="<?php echo $data['btn_remove_id']; ?>" data-widget="remove" data-toggle="tooltip" title="Close">
					<i class="fa fa-times"></i>
				</button>
				<?php
			endif;
			?>
		</div>
	</div>
	<div class="box-body">
		<div id="<?php echo $data['box_alert_id']; ?>"></div>
		<div id="<?php echo $data['box_loader_id']; ?>" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="<?php echo $data['box_content_id']; ?>"></div>
	</div>
	<div class="box-footer">
	</div>
	<div class="overlay" id="<?php echo $data['box_overlay_id']; ?>" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
	<?php $this->load->view('page/'.$class_link.'/'.$js_file, $data); ?>
</div>