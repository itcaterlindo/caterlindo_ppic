<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$form_id = 'idFormRekomendasi';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpr_kdRekomendasi', 'name'=> 'txtpr_kdRekomendasi', 'value' => $id ));
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="col-md-12">
    <table id="idTable" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
        <thead>
        <tr>
            <th style="width:1%; text-align:center;"><input type="checkbox" id="check_all" ></th>
            <th style="width:1%; text-align:center;">No</th>
            <th style="width:30%; text-align:center;">Nama Material</th>
            <th style="width:3%; text-align:center;">Stock</th>
            <th style="width:7%; text-align:center;">Qty PR</th>
            <th style="width:10%; text-align:center;">DueDate</th>
            <th style="width:25%; text-align:center;">Remarks</th>
        </tr>
        </thead>
        <tbody>
        <?php 
        $no = 1;
        foreach ($resultData as $row): ?>
            <tr>
                <td style="text-align:center;"><input type="checkbox" class="data-check" name="txtrm_kd[]" value="<?php echo $row['rm_kd'];?>"></td>
                <td><?php echo $no;?></td>
                <td><?php echo $row['rm_nama'];?></td>
                <td><?php echo $row['rm_stock'];?></td>
                <td> <input type="number" name="txtQty[<?php echo $row['rm_kd']; ?>]" class="form-control" placeholder="Qty" > </td>
                <td> <input type="text" name="txtBarcode" class="form-control datepicker" placeholder="DueDate" > </td>
                <td> <input type="text" name="txtBarcode" class="form-control" placeholder="Remarks" > </td>
            </tr>
        <?php 
            $no++;
        endforeach;?>
        </tbody>
    </table>
    <hr>
</div>

<div class="col-md-12">
    <div class="col-sm-1 col-xs-12 col-sm-offset-11">
        <button type="submit" onclick="submitRekomendasi()" class="btn btn-success"> <i class="fa fa-save"></i> Buat PR </button>
    </div>
</div>

<script type="text/javascript">
	$("#check_all").click(function () {
        $(".data-check").prop('checked', $(this).prop('checked'));
    });

	render_dt('#idTableGrid');
    render_datetimepicker('datepicker');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"scrollX": true,
			"scrollY": "300px",
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
		});
	}

</script>