<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$form_id = 'idFormRekomendasiPO';
$js_file = 'rekomendasi_po_js';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpr_kdRekomendasi', 'name'=> 'txtpr_kdRekomendasi', 'value' => $id ));
?>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for='idtxtmsalesorder_kd' class="col-md-2 control-label">Pilih SO</label>
            <div class="col-sm-7 col-xs-12">
                <div class="errInput" id="idErrmsalesorder_kd"></div>
                <?php echo form_dropdown('txtRm_kd', $opsiSO, isset($opsiSOSelected)? $opsiSOSelected : '', array('class'=> 'form-control select2', 'multiple'=>'multiple' ,'id'=> 'idtxtmsalesorder_kd'));?>
            </div>	
            <div class="col-sm-1 col-xs-12">
                <button type="submit" onclick="submitCariSO()" class="btn btn-success">Cari</button>
            </div>	
        </div>
    </div>
</div>
<hr>

<div class="row">
    <div id="idTableRekomendariPO"></div>
</div>
<?php echo form_close(); ?>
<?php $this->load->view('page/'.$class_link.'/'.$js_file); ?>