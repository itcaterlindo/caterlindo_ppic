<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table('<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		window.location.assign('<?php echo base_url().$head_class_link;?>')
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table(id) {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_main'; ?>',
				data: {id:id},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
					view_log('<?php echo $id; ?>');
				}
			});
		});
	}

	function ubah_status(pr_kd, level, akses){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/ubah_status_main'; ?>',
			data: {pr_kd:pr_kd, level:level, akses:akses},
			success: function(html) {
				toggle_modal('Keterangan Ubah Status', html);
			}
		});
	}

	function ubah_state(id = null, wftransition_kd = null) {
		event.preventDefault();
		 $.ajax({
            type: 'GET',
            url: '<?php echo base_url().$class_link.'/formubahstate_main'; ?>',
            data: {id:id, wftransition_kd: wftransition_kd},
            success: function(html) {
                toggle_modal('Ubah State', html);
            }
        });
	}

	function view_log(id){
		$.ajax({
            type: 'GET',
            url: '<?php echo base_url().$class_link.'/view_log'; ?>',
            data: {id: id},
            success: function(html) {
                $('#idMainContent').append(html);
            }
        });
	}

	function action_closeorder (pr_kd = null) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_closeorder'; ?>',
				data: {pr_kd: pr_kd},
				success: function(data) {
					let resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						window.location.reload();
					}else{
						notify (resp.status, resp.pesan, 'error');
					}
				}
			});
		}
	}

	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitStatus() {
		$('#idbtnSubmitMain').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtnSubmitMain').attr('disabled', true);

		event.preventDefault();
		var form = document.getElementById('idFormInputStatus');

		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_ubah_status" ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					location.reload();
					$('.errInput').html('');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
				}else if (resp.code == 400){
					generateToken (resp.csrf);
				}else{
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function submitDataState (form_id) {
		event.preventDefault();
		$('#idbtnSubmit'+form_id).html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtnSubmit'+form_id).attr('disabled', true);
		var form = document.getElementById(form_id);

		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_change_state" ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					//window.location.reload();
					if(resp.stts.status == 1){
						sendNotifMail(resp)
						//window.location.reload();
					}else{
						window.location.reload();
					}
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function sendNotifMail(resp) {

			var data = {
					service_id: 'service_89wfszf',
					template_id: 'purchase_rxx',
					user_id: 'user_gqom4wS5NcBXsu8XDttVI',
					template_params: {
					nopr: resp.msg.subjcet,
					note: resp.msg.keterangan,
					urlxx:resp.msg.url,
					cc: '',
					to: resp.stts.to
				}
			};


		//console.log(data)//
			$.ajax('https://api.emailjs.com/api/v1.0/email/send', {
			type: 'POST',
			data: JSON.stringify(data),
			contentType: 'application/json'
				}).done(function() {
					notify ('OK! 200.', 'Berhasil.', 'success');
					window.location.reload();
				}).fail(function(error) {
					notify('Oops… ' + JSON.stringify(error));
				})
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function kembali() {
		event.preventDefault();
		window.location.assign('<?php echo base_url().$head_class_link;?>')
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }

</script>