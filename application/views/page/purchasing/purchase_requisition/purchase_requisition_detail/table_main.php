<?php
defined('BASEPATH') or exit('No direct script access allowed!');
if (!empty($rowPR)){
	extract($rowPR);
}
?>

<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
	table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
</style>

<div class="row invoice-info">
	<div class="col-sm-4 invoice-col">
		<b>PurchaseRequisition No :</b> <?php echo isset($pr_no) ? $pr_no : '-'; ?><br>
		<div class="no-print">
			<b>PurchaseRequisition Status :</b>  <?php echo build_badgecolor($wfstate_badgecolor, $wfstate_nama); ?> <br>
		</div>
	</div>
	<div class="col-sm-4 invoice-col">
		<b>RequestedBy :</b> <?php echo isset($nm_admin) ? $nm_admin : '-'; ?><br>
		<b>Selesai :</b> <?php echo choice_enum($pr_closeorder); ?><br>
	</div>
	<div class="col-sm-4 invoice-col">
		<b>PurchaseRequisition Tanggal :</b> <?php echo format_date($pr_tanggal, 'd-m-Y'); ?>	</div>
</div>

<hr>

<table id="idTable" class="table table-bordered table-striped table-hover display table-responsive" style="width:100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:5%; text-align:center;">Kode</th>
		<th style="width:7%; text-align:center;">Nama Produk</th>
		<th style="width:15%; text-align:center;">Deskripsi</th>
		<th style="width:5%; text-align:center;">Qty</th>
		<th style="width:5%; text-align:center;">Unit</th>
		<th style="width:5%; text-align:center;">Konversi</th>
		<th style="width:5%; text-align:center;">QtyKonversi</th>
		<th style="width:7%; text-align:center;">Duedate</th>
		<th style="width:10%; text-align:center;">Remarks</th>
		<th style="width:5%; text-align:center;">No / Qty PO</th>
		<th style="width:5%; text-align:center;">TglDatang / Qty</th>
	</tr>
	</thead>

	<tbody>
	<?php 
	/** Group by tgldelivery */
	if (!empty($resultPRdetail)):
	?>
		<?php
		$no = 1;
		foreach($resultPRdetail as $r) :
			$refPO = '';
			$refGR = '';
			if (isset($result_detailPO)){
				$refPO = '<ul>'; $refGR = '<ul>';
				foreach ($result_detailPO as $eachDetailPO):
					if ($eachDetailPO['prdetail_kd'] == $r['prdetail_kd']){
						$refPO .= '<li>'.$eachDetailPO['po_no'].' / '.$eachDetailPO['podetail_qty'].'</li>';
						/** Cek GR */
						if (isset($result_detailGR)) {
							foreach($result_detailGR as $rGR) {
								if ($rGR['podetail_kd'] == $eachDetailPO['podetail_kd']) {
									$refGR .= '<li>'.format_date($rGR['rmgr_tgldatang'], 'Y-m-d').' / '.$rGR['rmgr_qty'].'</li>';
								}
							}
						}
					}

				endforeach;
				$refPO .= '</ul>';
				$refGR .= '</ul>';
			}
		?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $r['rm_kode'].'/'.$r['rm_oldkd']; ?></td>
				<td><?php echo $r['prdetail_nama']; ?></td>
				<td><?php echo $r['prdetail_deskripsi'].'; '.$r['prdetail_spesifikasi'];; ?></td>
				<td style="text-align:right;"><?php echo $r['prdetail_qty']; ?></td>
				<td><?php echo $r['rmsatuan_nama']; ?></td>
				<td style="text-align: right;"><?php echo $r['prdetail_konversi']; ?></td>
				<td style="text-align: right;"><?php echo $r['prdetail_qtykonversi']; ?></td>
				<td style="text-align:center;"><?php echo empty($r['prdetail_duedate']) ? $r['prdetail_duedate']: format_date($r['prdetail_duedate'], 'd-M-Y') ; ?></td>
				<td><?php echo $r['prdetail_remarks']; ?></td>
				<td> <?php echo $refPO;?></td>
				<td> <?php echo $refGR;?></td>
			</tr>
		<?php 
		$no++; 
		endforeach; ?>
	</tbody>
	<?php endif;?>
</table>
<hr>
<div class="row">
	<div class="col-md-6">
		<?php echo $generateButtonAction; 
		if ($pr_closeorder == '0'):
		?>
		<button class="btn btn-default" onclick="action_closeorder('<?php echo $pr_kd; ?>')"> <i class="fa fa-check"></i> Selesai </button>
		<?php endif; ?>
	</div>
	<div class="col-md-6">
		<button class="btn btn-default btn-sm pull-right" onclick="window.history.back()"> <i class="fa fa-arrow-circle-left"></i> Kembali</button>
	</div>
</div>

<script type="text/javascript">
</script>