<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
?>

<?php foreach ($resultData as $r) : ?>

<ul class="timeline">
    <li>
        <i class="fa fa-comments bg-yellow"></i>

        <div class="timeline-item">
        <span class="time"><i class="fa fa-clock-o"></i> <?= format_date($r['prlogstatus_tglinput'], 'd-m-Y H:i:s');?></span>

        <h3 class="timeline-header"> <strong> <?= $r['nm_admin']; ?> </strong> mengubah ke <?= $r['prstatus_nama'];?> </h3>

        <div class="timeline-body"><?= $r['prlogstatus_keterangan'];?></div>
        </div>
    </li>
</ul> 

<?php endforeach;?>