<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputStatus';

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpr_kd', 'name'=> 'txtpr_kd', 'value' => $pr_kd ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtprstatus_level', 'name'=> 'txtprstatus_level', 'value' => isset($prstatus_level) ? $prstatus_level: null ));

?>

<div class="col-md-12">

    <?php if ($akses == 'full') :
        $PRStatusSelected = $prstatus_level;
    ?>
    <div class="form-group">
		<label for='idtxtprstatus_kd' class="col-md-2 control-label">Status</label>
		<div class="col-sm-5 col-xs-12">
			<div class="errInput" id="idErrprstatus_kd"></div>
			<?php echo form_dropdown('txtprstatus_kd', $opsiPRstatus, isset($PRStatusSelected)? $PRStatusSelected : '', array('class'=> 'form-control', 'id'=> 'idtxtprstatus_kd'));?>
		</div>	
	</div>
<?php endif;?>
    <div class="form-group">
        <label for="idtxtprlogstatus_keterangan" class="col-md-2 control-label">Keterangan</label>
        <div class="col-sm-10 col-xs-12">
            <div class="errInput" id="idErrprlogstatus_keterangan"></div>
            <?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtprlogstatus_keterangan', 'id'=> 'idtxtprlogstatus_keterangan', 'placeholder' =>'Keterangan', 'rows' => '3', 'value'=> isset($prlogstatus_keterangan) ? $prlogstatus_keterangan: null ));?>
        </div>
    </div>

</div>

<div class="col-md-12">
	<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitStatus()" class="btn btn-primary btn-sm pull-right">
		<i class="fa fa-save"></i> Simpan
	</button>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
    $('#idtxtprstatus_kd').change(function(){
        $('#idtxtprstatus_level').val(this.value);
	});
</script>