<?php

class customPdf extends Tcpdf {
    public $pr_tanggal;
    public $pr_no;

    public function setsHeader($headerData){
        $this->pr_tanggal = $headerData['pr_tanggal'];
        $this->pr_no = $headerData['pr_no'];
    }

    public function Header() {
        $header = '
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td width="`20%" rowspan="3" style="text-align:left;"> <img src="assets/admin_assets/dist/img/logo_cat.png" width="95" height="30"> </td>
                <td width="47%" rowspan="3" style="font-size:16px; font-weight:bold; text-align:center;">PURCHASE REQUISITION</td>
                <td width="17%" style="font-size:10px; text-align:left; border-bottom: 1px solid black;">No. Dokumen</td>
                <td width="15%" style="font-size:10px; text-align:left; border-bottom: 1px solid black;">: CAT4-PUR-004</td>
            </tr>
            <tr>
                <td style="font-size:10px; text-align:left; border-bottom: 1px solid black;">Tanggal Terbit</td>
                <td style="font-size:10px; text-align:left; border-bottom: 1px solid black;">: 17-Juli-2020</td>
            </tr>
            <tr>
                <td style="font-size:10px; text-align:left; border-bottom: 1px solid black;">Revisi</td>
                <td style="font-size:10px; text-align:left; border-bottom: 1px solid black;">: 01</td>
            </tr>

        </table>';

        $keterangan =
        '<table cellspacing="0" cellpadding="0" border="0" style="font-size:10px;">
            <tr>
                <td width="17%" style="text-align:left;">Requisition Date</td>
                <td style="text-align:left;"> : '.format_date($this->pr_tanggal, 'd-m-Y').' </td>
                <td width="10%"></td>
                <td style="text-align:left;">Reg. Number</td>
                <td style="text-align:left;">: '.$this->pr_no.'</td>
                <td style="text-align:left;">Page</td>
                <td style="text-align:left;">: '.$this->getAliasNumPage().' of '.$this->getAliasNbPages().'</td>
            </tr>       

        </table>';

        $this->writeHTML($header, true, false, false, false, '');
        $this->writeHTML($keterangan, true, false, false, false, '');
    }


}

$pdf = new customPdf('P', 'mm', 'A4', true, 'UTF-8', false);
$title = 'PR_'.$rowPR['pr_no'];
$pdf->SetTitle($title);
$pdf->SetAuthor('Caterlindo ERP System');
$pdf->SetDisplayMode('real', 'default');
$pdf->SetAutoPageBreak(true, 15);
/** Margin */ 
$pdf->SetHeaderMargin(5);
$pdf->SetFooterMargin(10);
$pdf->SetMargins(8, 35, 8);
$pdf->setPrintFooter(false);

/** PR Note */
$dataHeader = array(
    'pr_tanggal' => $rowPR['pr_tanggal'],
    'pr_no' => $rowPR['pr_no'],
);
$pdf->setsHeader($dataHeader);

$pdf->AddPage();

$konten =
        '<table width="100%" cellspacing="0" cellpadding="1" border="1" style="font-size:10px">
            <tr style="text-align:center; font-weight:bold;">
                <th width="9%" >No. Item / Account</th>
                <th width="42%" >Description</th>
                <th width="7%">Qty</th>
                <th width="6%">Unit</th>
                <th width="12%">Due Date</th>
                <th width="24%">Remarks</th>
            </tr>';

/** Result Data */
if (!empty($resultPR)){

foreach($resultPR as $eachPR) :
    $konten .=
            '<tr style="font-size: 9px;">
                <td style="text-align:center;">'.$eachPR['rm_kode'].' / '.$eachPR['rm_oldkd'].'</td>
                <td style="text-align:left;"> '.$eachPR['prdetail_deskripsi'].'/'.$eachPR['prdetail_spesifikasi'].' </td>
                <td style="text-align:right;"> '.$eachPR['prdetail_qty'].' </td>
                <td style="text-align:left;">'.$eachPR['rmsatuan_nama'].'</td>
                <td style="text-align:left;">'.format_date($eachPR['prdetail_duedate'], 'd-m-Y').'</td>
                <td style="text-align:left;">'.$eachPR['prdetail_remarks'].'</td>
            </tr>';
endforeach;

$konten .=
        '</table>';

$pdf->writeHTML($konten, true, false, false, false, '');

$ttd = 
'<table width="100%" cellspacing="0" cellpadding="1" border="1" style="font-size:9px;">
    <tr>
        <td style="border-top: 1px solid black; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;"><strong> Notes: </strong><br>'.$rowPR['pr_note'].'</td>
        <td style="text-align:left; border-top: 1px solid black; ">';
$ttd .= '<table>';
foreach ($logs as $log) {
    $ttd .=
             '<tr>
                <td>'.$log['wfstate_nama'].' By</td>
                <td> : '.$log['nm_admin'].'</td>
                <td>'.$log['prlogstatus_tglinput'].'</td>
             </tr>';
}
$ttd .= '</table>';
        $ttd .= '</td>
    </tr>
</table>';
$pdf->writeHTML($ttd, true, false, false, false, '');
}

$txtOutput = $title.'.pdf';
$pdf->Output($txtOutput, 'I');