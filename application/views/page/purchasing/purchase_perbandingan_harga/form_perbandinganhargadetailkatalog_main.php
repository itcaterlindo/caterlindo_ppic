<?php
defined('BASEPATH') or exit('No direct script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputDetailKatalog';
$rmdeskripsi = '';
$perbandinganhargadetailkatalog_kd = '';
extract($rowData);
$rmdeskripsi = $rm_kode.'|'.$prdetail_nama.'/'.$prdetail_deskripsi.'/'.$prdetail_spesifikasi;

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtStsFormDetailKatalog', 'name'=> 'txtStsFormDetailKatalog', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtperbandinganharga_kd', 'name'=> 'txtperbandinganharga_kd', 'value' => isset($perbandinganharga_kd) ? $perbandinganharga_kd: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtperbandinganhargadetail_kd', 'name'=> 'txtperbandinganhargadetail_kd', 'value' => isset($perbandinganhargadetail_kd) ? $perbandinganhargadetail_kd: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtperbandinganhargadetailkatalog_kd', 'name'=> 'txtperbandinganhargadetailkatalog_kd', 'value' => isset($perbandinganhargadetailkatalog_kd) ? $perbandinganhargadetailkatalog_kd: null ));
?>
<style>
.modal-open .select2-container--open { z-index: 999999 !important; width:100% !important; }
</style>

<div class="col-md-12">
	<div class="row invoice-info">
		<div class="col-sm-6 invoice-col">
			<b>Raw Material :</b> <?php echo isset($rmdeskripsi) ? $rmdeskripsi : '-'; ?><br>
			<div class="no-print">
				<b>Suplier :</b>  <?php echo isset($suplier_nama) ? $suplier_nama : '-'; ?> <br>
			</div>
		</div>
		<div class="col-sm-6 invoice-col">
			<b>Tanggal :</b> <?php echo isset($perbandinganhargadetailkatalog_tglkatalog) ? $perbandinganhargadetailkatalog_tglkatalog : '-'; ?><br>
		</div>
	</div>
<hr>
</div>


<div class="form-group">
	<label class="col-md-1 control-label">Status</label>
	<div class="col-sm-5 col-xs-12">
		<div class="checkbox">
			<label> <input type="checkbox" name="txtperbandinganhargadetailkatalog_selected" 
			<?php if (isset($perbandinganhargadetailkatalog_selected) && !empty($perbandinganhargadetailkatalog_selected)) echo 'checked';?> value="1">Pilih</label>
		</div>
	</div>
</div>
<?php if ($sts=='add') :?>
<div class="form-group">
	<label for="idtxtsuplier_kd" class="col-md-1 control-label">Supplier</label>
	<div class="col-sm-6 col-xs-12">
		<div class="errInput" id="idErrsuplier_kd"></div>
		<?php echo form_dropdown('txtsuplier_kd', $opsiSuplier, isset($suplier_kd)? $suplier_kd : '', array('class'=> 'form-control select2', 'id'=> 'idtxtsuplier_kd'));?>
	</div>
</div>
<?php endif;?>
<div class="form-group">
	<label for="idtxtperbandinganhargadetailkatalog_rmkd" class="col-md-1 control-label">Raw Material</label>
	<div class="col-sm-5 col-xs-12">
		<div class="errInput" id="idErrperbandinganhargadetailkatalog_rmkd"></div>
		<input type="hidden" name="txtrm_kd" value="<?= isset($rm_kd) ? $rm_kd: null?>" />
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtperbandinganhargadetailkatalog_rmkd', 'id'=> 'idtxtperbandinganhargadetailkatalog_rmkd', 'disabled' => 'true', 'placeholder' =>'RM', 'value'=> isset($rmdeskripsi) ? $rmdeskripsi: null ));?>
	</div>
</div>
<div class="form-group">
	<label for="idtxtperbandinganhargadetailkatalog_katalogharga" class="col-md-1 control-label">Harga Katalog</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrperbandinganhargadetailkatalog_katalogharga"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtperbandinganhargadetailkatalog_katalogharga', 'id'=> 'idtxtperbandinganhargadetailkatalog_katalogharga', 'disabled' => 'true', 'placeholder' =>'Harga', 'value'=> isset($perbandinganhargadetailkatalog_katalogharga) ? custom_numberformat($perbandinganhargadetailkatalog_katalogharga, 2, 'IDR'): null ));?>
	</div>
	<label for="idtxtsatuan_pr" class="col-md-1 control-label">Satuan</label>
	<div class="col-sm-2 col-xs-12">
		<div class="errInput" id="idErrsatuan_pr"></div>
		<input type="hidden" name="txtrmsatuan_kd" value="<?= isset($rmsatuan_kd) ? $rmsatuan_kd: null?>" />
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtsatuan_pr', 'id'=> 'idtxtsatuan_pr', 'disabled' => 'true', 'placeholder' =>'Harga', 'value'=> isset($satuan_perbandingankatalog) ? $satuan_perbandingankatalog: null ));?>
	</div>
	<label for="idtxtprdetail_qty" class="col-md-1 control-label">Qty</label>
	<div class="col-sm-2 col-xs-12">
		<div class="errInput" id="idErrprdetail_qty"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtprdetail_qty', 'id'=> 'idtxtprdetail_qty', 'disabled' => 'true', 'placeholder' =>'Harga', 'value'=> isset($prdetail_qty) ? $prdetail_qty: null ));?>
	</div>
</div>
<div class="form-group">
	<label for="idtxtperbandinganhargadetailkatalog_updateharga" class="col-md-1 control-label">Harga</label>
	<div class="col-sm-3 col-xs-12">
		<div class="errInput" id="idErrperbandinganhargadetailkatalog_updateharga"></div>
		<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtperbandinganhargadetailkatalog_updateharga', 'id'=> 'idtxtperbandinganhargadetailkatalog_updateharga', 'placeholder' =>'Harga', 'value'=> isset($perbandinganhargadetailkatalog_updateharga) ? $perbandinganhargadetailkatalog_updateharga: null ));?>
	</div>
	<?php if ($sts=='add') :?>
	<label for="idtxtrmsatuan_kd" class="col-md-1 control-label">Satuan</label>
	<div class="col-sm-3 col-xs-12">
		<div class="errInput" id="idErrrmsatuan_kd"></div>
		<?php echo form_dropdown('txtrmsatuan_kdSelected', $opsiSatuan, isset($rmsatuan_kd)? $rmsatuan_kd : '', array('class'=> 'form-control select2', 'id'=> 'idtxtrmsatuan_kd'));?>
	</div>
	<?php endif;?>
</div>
<div class="form-group">
	<label for="idtxtperbandinganhargadetailkatalog_qty" class="col-md-1 control-label">Qty</label>
	<div class="col-sm-3 col-xs-12">
		<div class="errInput" id="idErrperbandinganhargadetailkatalog_qty"></div>
		<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtperbandinganhargadetailkatalog_qty', 'id'=> 'idtxtperbandinganhargadetailkatalog_qty', 'placeholder' =>'Qty', 'value'=> isset($perbandinganhargadetailkatalog_qty) ? $perbandinganhargadetailkatalog_qty: null ));?>
	</div>
</div>
<div class="form-group">
	<label for="idtxtperbandinganhargadetailkatalog_keterangan" class="col-md-1 control-label">Keterangan</label>
	<div class="col-sm-9 col-xs-12">
		<div class="errInput" id="idErrperbandinganhargadetailkatalog_keterangan"></div>
		<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtperbandinganhargadetailkatalog_keterangan', 'id'=> 'idtxtperbandinganhargadetailkatalog_keterangan', 'placeholder' =>'Keterangan', 'rows' => '2', 'value'=> isset($perbandinganhargadetailkatalog_keterangan) ? $perbandinganhargadetailkatalog_keterangan: null ));?>
	</div>
</div>
<hr>

<div class="col-md-12">
<div class="form-group">
	<div class="col-sm-2 col-xs-12 pull-right">
		<?php // if (isset($perbandinganhargadetailkatalog_status) && $perbandinganhargadetailkatalog_status == 'additional') :?>
		<button type="submit" name="btnSubmit" onclick="hapus_datadetailkatalog('<?= $perbandinganhargadetailkatalog_kd?>')" class="btn btn-danger btn-sm">
			<i class="fa fa-trash"></i> Hapus
		</button>
		<?php //endif;?>
		<button type="submit" name="btnSubmit" onclick="submitDataKatalog('<?= $form_id?>')" class="btn btn-primary btn-sm">
			<i class="fa fa-save"></i> Simpan
		</button>
	</div>
</div>
</div>
<?php echo form_close(); ?>
