<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<input type="hidden" id="idtxtDetailperbandinganharga_kd" value="<?= $perbandinganharga_kd ?>"/>
<table id="idTableHargadetail" class="table table-bordered table-striped table-hover display" style="width:100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;">No</th>
		<th style="width:4%; text-align:center;">Opsi</th>
		<th style="width:7%; text-align:center;">No PR</th>
		<th style="width:7%; text-align:center;">Kode RM</th>
		<th style="width:20%; text-align:center;">Deskripsi RM</th>
		<th style="width:20%; text-align:center;">Remarks PR</th>
		<th style="width:7%; text-align:center;">Qty</th>
		<th style="width:7%; text-align:center;">Satuan</th>
	</tr>
	</thead>
    <tbody>
    <?php 
    $no = 1;
    foreach ($r_perbandinganharga_detail as $eachPerbandinganharga) :?>
    <tr>
        <td><?= $no ?></td>
        <td><?php
			$btns = array();
			$btns[] = get_btn(array('title' => 'Delete Item', 'icon' => 'trash', 'onclick' => 'hapus_datadetail(\''.$eachPerbandinganharga['perbandinganhargadetail_kd'].'\')'));	
			$btn_group = group_btns($btns);
			echo $btn_group;
		?>
		</td>
        <td><?= $eachPerbandinganharga['pr_no'] ?></td>
        <td><?= $eachPerbandinganharga['rm_kode'] ?></td>
        <td><?= $eachPerbandinganharga['prdetail_nama'].'/'.$eachPerbandinganharga['prdetail_deskripsi'].'/'.$eachPerbandinganharga['prdetail_spesifikasi'] ?></td>
        <td><?= $eachPerbandinganharga['prdetail_remarks'] ?></td>
		<td><?= $eachPerbandinganharga['prdetail_qty'] ?></td>
        <td><?= $eachPerbandinganharga['rmsatuan_nama'] ?></td>
    </tr>
    <?php 
    $no ++;
    endforeach;?>
    </tbody>
</table>
<button class="btn btn-success btn-sm pull-right" onclick="actionGenerateKatalog()"> <i class="fa fa-arrow-circle-right"></i> Lanjut </button>
<script type="text/javascript">
	var table = $('#idTableHargadetail').DataTable({
		"ordering" : true,
		"paging" : false,
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"columnDefs": [
			{"searchable": false, "orderable": false, "targets": 0},
			{"searchable": false, "orderable": false, "targets": 1},
		],
	});

	function hapus_datadetail (id) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_deletedetail'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						let perbandinganharga_kd = $('#idtxtperbandinganharga_kd').val();
						table_perbandinganhargadetail_main (perbandinganharga_kd);
						selectPR();
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
					}
				}
			});
		}
	}
</script>
