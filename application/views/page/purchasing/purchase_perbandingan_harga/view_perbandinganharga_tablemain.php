<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$form_id = 'idFormDetailKatalog';
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<table id="idtablemain" cellspacing="0" cellpadding="0" border="1" width="100%">
	<thead>
	<tr style="font-size: 75%; font-weight: bold;">
		<th style="width:3%; text-align:center;">No</th>
		<th style="width:8%; text-align:center;">Kode</th>
		<th style="width:20%; text-align:center;">Deskripsi</th>
		<th style="width:5%; text-align:center;">Qty</th>
		<th style="width:5%; text-align:center;">Satuan</th>
		<th style="width:11%; text-align:center;">Harga</th>
		<th style="width:11%; text-align:center;">Perubahan Harga</th>
		<th style="width:12%; text-align:center;">Konfirmasi</th>
		<th style="width:25%; text-align:center;">Keterangan</th>
	</tr>
	</thead>
    <tbody>
    <?php 
	$no = 1;
	$konfirmasi = '-';
	
	foreach ($r_perbandinganhargadetail as $e_perbandinganhargadetail) :
		if (!empty($e_perbandinganhargadetail['satuan_po'])) {
			$konfirmasi = '('.$e_perbandinganhargadetail['satuan_po'].')'.' / '.$e_perbandinganhargadetail['po_no'].'|'.$e_perbandinganhargadetail['suplier_kode'];
		}
	?>
    <tr style="font-size: 60%; font-weight: bold;">
        <td style="width:3%; text-align:center;"><?= $no?></td>
        <td style="width:8%;"><?= $e_perbandinganhargadetail['rm_kode'].' / '.$e_perbandinganhargadetail['rm_oldkd'] ?></td>
        <td style="width:20%;"><?= $e_perbandinganhargadetail['prdetail_deskripsi'].'/'.$e_perbandinganhargadetail['prdetail_spesifikasi'] ?></td>
        <td style="width:5%; text-align:right;"><?= $e_perbandinganhargadetail['prdetail_qty'] ?></td>
        <td style="width:5%; text-align:center;"><?= $e_perbandinganhargadetail['satuan_pr'] ?></td>
		<td style="width:11%; text-align:right;"><?php echo !empty($e_perbandinganhargadetail['podetail_harga']) ? custom_numberformat($e_perbandinganhargadetail['podetail_harga'], 2, 'IDR'): null ?></td>
		<td style="width:11%;"></td>
		<td style="width:12%;"><?php echo $konfirmasi; ?></td>
		<td style="width:25%;"></td>
    </tr>
	<?php 

		foreach ($r_perbandinganhargadetailKatalog as $e_perbandinganhargadetailKatalog) {
			$ppn = 'Tdk';
			$jt = '-';
			if ($e_perbandinganhargadetailKatalog['perbandinganhargadetail_kd'] == $e_perbandinganhargadetail['perbandinganhargadetail_kd']) :
				if ($e_perbandinganhargadetailKatalog['suplier_includeppn'] == 'T') {
					$ppn = 'Ya';
				}
				$jt = !empty($e_perbandinganhargadetailKatalog['suplier_termpayment']) ? $e_perbandinganhargadetailKatalog['suplier_termpayment'].' hari' : '-';
				/** Cek selected */
				$color = '';
				if ($e_perbandinganhargadetailKatalog['perbandinganhargadetailkatalog_selected'] == 1) { 
					$color = 'light gray'; 
				}
			?>
				<tr style="font-size: 60%; background-color:<?= $color; ?>;">
					<td></td>
					<td><?= $e_perbandinganhargadetailKatalog['rm_kode'] ?></td>
					<td><?= $e_perbandinganhargadetailKatalog['suplier_kode'].' | '.$e_perbandinganhargadetailKatalog['suplier_nama'].' / PPN: '.$ppn.' / JT: '.$jt ?></td>
					<td style="text-align:right;"><?= $e_perbandinganhargadetailKatalog['perbandinganhargadetailkatalog_qty'] ?></td>
					<td style="text-align:center;"><?= $e_perbandinganhargadetailKatalog['satuan_katalog'] ?></td>
					<td style="text-align:right;"><?= custom_numberformat($e_perbandinganhargadetailKatalog['perbandinganhargadetailkatalog_katalogharga'], 2, 'IDR') ?></td>
					<td style="text-align:right;"><?= custom_numberformat($e_perbandinganhargadetailKatalog['perbandinganhargadetailkatalog_updateharga'], 2, 'IDR') ?> </td>
					<td> <?php echo $e_perbandinganhargadetailKatalog['perbandinganhargadetailkatalog_tglkatalog'] ?></td>
					<td><?= $e_perbandinganhargadetailKatalog['perbandinganhargadetailkatalog_keterangan']?></td>
					<!-- <td><?php //echo $e_perbandinganhargadetailKatalog['perbandinganhargadetailkatalog_tglkatalog'] ?></td> -->
				</tr>
			<?php
			endif;
		}
    $no ++;
    endforeach;?>
    </tbody>
</table>
