<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
$form_id_2 = 'idFormInput2';
if($sts == 'edit'){
	extract($rowData);
}
echo form_open_multipart('', array('id' => $form_id.'_first', 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtperbandinganharga_kd', 'class'=>'clPerbandinganharga_kd' ,'name'=> 'txtperbandinganharga_kd', 'value' => isset($id) ? $id: null ));
?>

<?php if ($sts == 'edit'):?>
<script>$('.tt-addPR-ED').slideUp();</script>
<button class="btn btn-default btn-sm" onclick="event.preventDefault(); selectPR(''); $('.tt-detailPerbandingan').slideDown(); $('.tt-addPR-ED').slideUp();"> <i class="fa fa-plus"></i> Form Tambah Data</button>
<button class="btn btn-secondary btn-sm" onclick="event.preventDefault(); selectPR('info'); $('.tt-addPR-ED').slideDown(); $('.tt-detailPerbandingan').slideUp();"> <i class="fa fa-plus"></i> Form Tambah PR</button>
<button onclick="event.preventDefault(); $('.tt-detailPerbandingan').slideUp(); $('.tt-addPR-ED').slideUp();" class="btn btn-warning btn-sm tt-detailPerbandingan"><i class="fa fa-times"></i> Tutup</button>


<div class="form-group tt-addPR-ED">
	<label for='idtxtpr_kd' class="col-md-2 control-label">No PR</label>
	<div class="col-sm-6 col-xs-12">
		<div class="errInput" id="idErrpr_kd"></div>
		<?php echo form_multiselect('txtpr_kd[]', $opsiPR, isset($pr_kds)? $pr_kds : [], array('class'=> 'form-control select2', 'id'=> 'idtxtpr_kd_add', 'multiple'=>'multiple'));?>
	</div>
	<button class="btn btn-sm btn-success" onclick="selectPR('info')"> <i class="fa fa-check"></i> Pilih</button>
</div>
<hr class="tt-paramNoPR">
<?php 
echo form_close();
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal')); ?>
<div id="info" class=" tt-addPR-ED"></div>
<div class="form-group tt-addPR-ED">
	<div class="col-sm-1 col-xs-12 pull-right">
		<button type="submit" name="btnSubmit" onclick="submitData('<?= $form_id?>', 'info')" class="btn btn-primary btn-sm">
			<i class="fa fa-save"></i> Simpan
		</button>
	</div>
</div>
<hr>
<?php echo form_close(); ?>
<?php endif;?>


<div class="form-group tt-paramNoPR">
	<label for='idtxtpr_kd' class="col-md-2 control-label">No PR</label>
	<div class="col-sm-6 col-xs-12">
		<div class="errInput" id="idErrpr_kd"></div>
		<?php echo form_multiselect('txtpr_kd[]', $opsiPR, isset($pr_kds)? $pr_kds : [], array('class'=> 'form-control select2', 'id'=> 'idtxtpr_kd', 'multiple'=>'multiple'));?>
	</div>
	<button class="btn btn-sm btn-success" onclick="selectPR('')"> <i class="fa fa-check"></i> Pilih</button>
</div>
<hr class="tt-paramNoPR">
<?php 
echo form_close();
echo form_open_multipart('', array('id' => $form_id_2, 'class' => 'form-horizontal')); ?>
<div id="idprdetail" class="tt-detailPerbandingan"></div>
<div class="form-group tt-detailPerbandingan">
	<div class="col-sm-1 col-xs-12 pull-right">
		<button type="submit" name="btnSubmit" onclick="submitData('<?= $form_id_2?>', '')" class="btn btn-primary btn-sm">
			<i class="fa fa-save"></i> Simpan
		</button>
	</div>
</div>
<hr>
<?php echo form_close(); ?>
<script type="text/javascript">
	<?php if ($sts == 'edit'): ?>
		selectPR();
		$('.tt-paramNoPR').hide();
		$('.tt-detailPerbandingan').hide();
	<?php endif;?>
</script>