<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$form_id = 'idFormDetailKatalog';
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<table id="idtablemain" cellspacing="0" cellpadding="0" border="1" width="100%">
	<thead>
	<tr style="font-size: 75%; font-weight: bold;">
		<th style="width:3%; text-align:center;">No</th>
		<th style="width:8%; text-align:center;">Kode</th>
		<th style="width:26%; text-align:center;">Deskripsi</th>
		<th style="width:6%; text-align:center;">No PR</th>
		<th style="width:6%; text-align:center;">Qty PR</th>
		<th style="width:6%; text-align:center;">Satuan PR</th>
		<th style="width:6%; text-align:center;">Qty</th>
		<th style="width:6%; text-align:center;">Satuan</th>
		<th style="width:11%; text-align:center;">Harga</th>
		<th style="width:11%; text-align:center;">Perubahan Harga</th>
		<th style="width:10%; text-align:center;">Keterangan</th>
	</tr>
	</thead>
    <tbody>
    <?php 
	$no = 1;
    // $konfirmasi = '-';
    $aGroupSuplier = [];
    foreach ($r_perbandinganhargadetail as $e_perbandinganhargadetail) {
        $aGroupSuplier[$e_perbandinganhargadetail['suplier_kode'].'#'.$e_perbandinganhargadetail['suplier_nama'].'#'.$e_perbandinganhargadetail['suplier_includeppn'].'#'.$e_perbandinganhargadetail['suplier_termpayment']][] = $e_perbandinganhargadetail;
    }
    krsort($aGroupSuplier);
    foreach ($aGroupSuplier as $e_aGroupSuplier => $element) :
        $aGroup = explode('#', $e_aGroupSuplier);
        $ppn = 'Tdk';
        $jt = '-';
        if ($aGroup[2] == 'T') {
            $ppn = 'Ya';
        }
        $jt = !empty($aGroup[3]) ? $aGroup[3].' hari' : '-';
	?>
    <tr style="font-size: 60%; font-weight: bold;">
        <td style="width:3%; text-align:center;"><?= $no?></td>
        <td style="width:8%;"><?= !empty($aGroup[0]) ? $aGroup[0] : '-' ?></td>
        <td style="width:26%;"><?= !empty($aGroup[1]) ? $aGroup[1] : '-' ?></td>
        <td style="width:6%; text-align:right;"></td>
        <td style="width:6%; text-align:right;"></td>
        <td style="width:6%; text-align:right;"></td>
        <td style="width:6%; text-align:center;"></td>
        <td style="width:6%; text-align:center;"></td>
		<td style="width:11%; text-align:right;"></td>
		<td style="width:11%;"></td>
		<td style="width:10%;"><?= 'PPN: '.$ppn.' / JT: '.$jt?></td>
    </tr>
    <?php 
        foreach ($element as $e_element) :
            /** Cek selected */
            $color = '';
            if ($e_element['perbandinganhargadetailkatalog_selected'] == 1) { 
                $color = 'lightgray'; 
            }
            ?>
             <tr style="font-size: 60%; background-color:<?= $color; ?>;">
                <td style="width:3%; text-align:center;"></td>
                <td style="width:8%;"><?= $e_element['rm_kode'].' / '.$e_element['rm_oldkd'] ?></td>
                <td style="width:26%;"><?= $e_element['prdetail_deskripsi'].'/'.$e_element['prdetail_spesifikasi'] ?></td>
                <td style="width:6%; text-align:left;"><?= $e_element['pr_no']?></td>
                <td style="width:6%; text-align:right;"><?= $e_element['prdetail_qty']?></td>
                <td style="width:6%; text-align:center;"><?= $e_element['satuan_pr']?></td>
                <td style="width:6%; text-align:right;"><?= $e_element['perbandinganhargadetailkatalog_qty']?></td>
                <td style="width:6%; text-align:center;"><?= $e_element['satuan_katalog']?></td>
                <td style="width:11%; text-align:right;"><?= custom_numberformat($e_element['perbandinganhargadetailkatalog_katalogharga'], 2, 'IDR')?></td>
                <td style="width:11%; text-align:right;"><?= custom_numberformat($e_element['perbandinganhargadetailkatalog_updateharga'], 2, 'IDR')?></td>
                <td style="width:10%;"><?= $e_element['perbandinganhargadetailkatalog_keterangan']?></td>
            </tr>
            <?php
        endforeach;
    $no ++;
    endforeach;?>
    </tbody>
</table>
