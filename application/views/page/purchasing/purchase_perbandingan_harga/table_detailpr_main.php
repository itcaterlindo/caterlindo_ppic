<?php
defined('BASEPATH') or exit('No direct script access allowed!');

?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<input type="hidden" name="txtTabelsts" value="<?= $sts?>" placeholder="txtTabelsts"/>
<input type="hidden" name="txtTabelpr_kd" value="<?= $pr_kd?>" placeholder="txtTabelpr_kd" />
<input type="hidden" name="txtTabelperbandinganharga_kd" class="clPerbandinganharga_kd" value="<?= $perbandinganharga_kd?>" placeholder="txtTabelperbandinganharga_kd" />

<table id="idTableDetailPR" class="table table-bordered table-striped table-hover display" style="width:100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No</th>
		<th style="width:4%; text-align:center;"><input type="checkbox" id="check_all" class="icheck" ></th>
		<th style="width:7%; text-align:center;">No PR</th>
		<th style="width:7%; text-align:center;">Kode RM</th>
		<th style="width:10%; text-align:center;">Deskripsi RM</th>
		<th style="width:20%; text-align:center;">Remarks PR</th>
		<th style="width:7%; text-align:center;">Qty</th>
		<th style="width:7%; text-align:center;">Satuan</th>
	</tr>
	</thead>
    <tbody>
    <?php 
    $no = 1;
    foreach ($r_prdetail as $eachDetail) :?>
    <tr>
        <td><?= $no ?></td>
        <td> 
		<input type="checkbox" class="icheck data-check" name="chkprdetail_kd[]" style="text-align:center;"value="<?= $eachDetail['prdetail_kd']?>"/> 
		<input type="hidden" name="podetail_kds[<?= $eachDetail['prdetail_kd'] ?>]" value="<?= $eachDetail['podetail_kd']?>"/> 
		</td>
        <td><?= $eachDetail['pr_no'] ?></td>
        <td><?= $eachDetail['rm_kode'] ?></td>
        <td><?= $eachDetail['prdetail_nama'].'/'.$eachDetail['prdetail_deskripsi'].'/'.$eachDetail['prdetail_spesifikasi'] ?></td>
        <td><?= $eachDetail['prdetail_remarks'] ?></td>
        <td><?= $eachDetail['prdetail_qty'] ?></td>
        <td><?= $eachDetail['rmsatuan_nama'] ?></td>
    </tr>
    <?php 
    $no ++;
    endforeach;?>
    </tbody>
</table>
<script type="text/javascript">
 
	$('#check_all').on('ifChanged', function(e) {
        var headerchecked = e.target.checked;
        if (headerchecked == true) {
            $('.data-check').iCheck('check');
        }else{
            $('.data-check').iCheck('uncheck');
        }
    });

	var table = $('#idTableDetailPR').DataTable({
		"ordering" : true,
		"paging" : false,
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"columnDefs": [
			{"searchable": false, "orderable": false, "targets": 0},
			{"searchable": false, "orderable": false, "targets": 1},
		],
	});

		
	$.fn.dataTable.ext.errMode = 'none';

</script>
