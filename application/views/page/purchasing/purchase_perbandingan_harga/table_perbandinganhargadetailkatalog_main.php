<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$form_id = 'idFormDetailKatalog';
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<?php echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal')); ?>
<input type="hidden" id="idtxtperbandinganharga_kd" name="txtperbandinganharga_kd" value="<?= $perbandinganharga_kd ?>" placeholder="txtperbandinganharga_kd"/>
<table id="idTable" class="table table-bordered table-striped display" style="width:100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;">No</th>
		<th style="width:4%; text-align:center;">Opsi</th>
		<th style="width:7%; text-align:center;">Kode</th>
		<th style="width:20%; text-align:center;">Deskripsi</th>
		<th style="width:7%; text-align:center;">Qty</th>
		<th style="width:7%; text-align:center;">Satuan</th>
		<th style="width:10%; text-align:center;">Harga</th>
		<th style="width:7%; text-align:center;">Perubahan Harga</th>
		<th style="width:17%; text-align:center;">Konfirmasi</th>
		<th style="width:17%; text-align:center;">Keterangan</th>
		<th style="width:7%; text-align:center;">Last Update <br> Katalog </th>
	</tr>
	</thead>
    <tbody>
    <?php 
	$no = 1;
	$konfirmasi = '-';

	foreach ($r_perbandinganhargadetail as $e_perbandinganhargadetail) :
		if (!empty($e_perbandinganhargadetail['satuan_po'])) {
			$konfirmasi = '('.$e_perbandinganhargadetail['satuan_po'].')'.' / '.$e_perbandinganhargadetail['po_no'].'|'.$e_perbandinganhargadetail['suplier_nama'];
		}
	?>
    <tr style="font-weight: bold;">
        <td><?= $no?></td>
        <td><?php
			$btns = array();
			$btns[] = get_btn(array('title' => 'Tambah Pembanding', 'icon' => 'plus', 'onclick' => 'form_perbandinganhargadetailkatalog_main(\''.'add'.'\', \''.$e_perbandinganhargadetail['perbandinganhargadetail_kd'].'\', \''.''.'\')'));	
			$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_datadetailFormKatalog(\''.$e_perbandinganhargadetail['perbandinganhargadetail_kd'].'\')'));	
			$btn_group = group_btns($btns);
			echo $btn_group;
		?>
		</td>
        <td><?= $e_perbandinganhargadetail['rm_kode'] ?></td>
        <td><?= $e_perbandinganhargadetail['prdetail_nama'].'/'.$e_perbandinganhargadetail['prdetail_deskripsi'].'/'.$e_perbandinganhargadetail['prdetail_spesifikasi'] ?></td>
        <td><?= $e_perbandinganhargadetail['prdetail_qty'] ?></td>
        <td><?= $e_perbandinganhargadetail['satuan_pr'] ?></td>
		<td style="text-align:right;"><?php echo !empty($e_perbandinganhargadetail['podetail_harga']) ? custom_numberformat($e_perbandinganhargadetail['podetail_harga'], 2, 'IDR'): null ?></td>
		<td></td>
		<td><?php echo $konfirmasi; ?></td>
		<td></td>
		<td><?= format_date($e_perbandinganhargadetail['podetail_tglinput'], 'Y-m-d H:i:s') ?></td>
    </tr>
	<?php 
	// $limit = 3; // Set limit di sini  
	// $count = 0;  
		foreach ($r_perbandinganhargadetailKatalog as $e_perbandinganhargadetailKatalog) {
			// if ($count >= $limit) {  
			// 	break; // Keluar dari loop jika limit tercapai  
			// }  
			if ($e_perbandinganhargadetailKatalog['perbandinganhargadetail_kd'] == $e_perbandinganhargadetail['perbandinganhargadetail_kd']) :
				/** Cek selected */
				$color = '';
				if ($e_perbandinganhargadetailKatalog['perbandinganhargadetailkatalog_selected'] == 1) { 
					$color = 'lightgreen'; 
				}
			?>
				<tr style="background-color:<?= $color; ?>;">
					<td></td>
					<td style="text-align: center;">
					<button type="button" class="btn btn-default btn-sm" onclick="form_perbandinganhargadetailkatalog_main ('edit', '<?= $e_perbandinganhargadetailKatalog['perbandinganhargadetail_kd']?>', '<?= $e_perbandinganhargadetailKatalog['perbandinganhargadetailkatalog_kd']?>')"><i class="fa fa-check-square-o"></i></button>
					</td>
					<td><?= $e_perbandinganhargadetailKatalog['rm_kode'] ?></td>
					<td><?= $e_perbandinganhargadetailKatalog['suplier_kode'].' | '.$e_perbandinganhargadetailKatalog['suplier_nama'] ?></td>
					<td><?= $e_perbandinganhargadetailKatalog['perbandinganhargadetailkatalog_qty'] ?></td>
					<td><?= $e_perbandinganhargadetailKatalog['satuan_katalog'] ?></td>
					<td style="text-align:right;"><?= custom_numberformat($e_perbandinganhargadetailKatalog['perbandinganhargadetailkatalog_katalogharga'], 2, 'IDR') ?></td>
					<td style="text-align:right;"><?= custom_numberformat($e_perbandinganhargadetailKatalog['perbandinganhargadetailkatalog_updateharga'], 2, 'IDR') ?> </td>
					<td style="font-style: italic;"><?= $e_perbandinganhargadetailKatalog['perbandinganhargadetailkatalog_status']?></td>
					<td><?= $e_perbandinganhargadetailKatalog['perbandinganhargadetailkatalog_keterangan']?></td>
					<td><?= $e_perbandinganhargadetailKatalog['perbandinganhargadetailkatalog_tglkatalog'] ?></td>
				</tr>
			<?php
			// $count++; 
			endif;
			 
		}
    $no ++;
    endforeach;?>
    </tbody>
</table>
<button class="btn btn-primary btn-sm pull-right" onclick="selesai()"> <i class="fa fa-check-circle"></i> Selesai </button>
<?php echo form_close(); ?>