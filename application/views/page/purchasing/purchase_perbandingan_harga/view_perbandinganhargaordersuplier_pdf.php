<?php

class customPdf extends Tcpdf {
    public $perbandinganharga_no; 
    public $pr_no; 
    public $perbandinganharga_tglinput;

    function setKeterangan ($arrayKet = []) {
        $this->perbandinganharga_no = !empty($arrayKet['perbandinganharga_no']) ? $arrayKet['perbandinganharga_no'] : null;
        $this->pr_no = !empty($arrayKet['pr_no']) ? $arrayKet['pr_no'] : '-';
        $this->perbandinganharga_tglinput = !empty($arrayKet['perbandinganharga_tgledit']) ? $arrayKet['perbandinganharga_tgledit'] : '-';
    }

    public function Header() {
        $header = '
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td rowspan="4" width="20%" style="text-align:center;"> <img src="assets/admin_assets/dist/img/logo_cat.png" width="95" height="30"> </td>
                <td rowspan="4" width="50%" style="font-size:13px; font-weight:bold; text-align:right; text-align:center;">PERBANDINGAN HARGA <br> GROUP BY SUPPLIER</td>
                <td width="15%" style="font-size:9px;"></td>
                <td width="15%" style="font-size:9px;"></td>
            </tr>
            <tr>
                <td style="font-size:9px; text-align:left;"></td>
                <td style="font-size:9px; text-align:left;"></td>
            </tr>
            <tr>
                <td style="font-size:9px; text-align:left;"></td>
                <td style="font-size:9px; text-align:left;"></td>
            </tr>

        </table>';

        /** Keterangan */
        $keterangan =
        '<table cellspacing="0" cellpadding="0" border="0" style="font-size: 80%;">
            <tr>
                <td width="20%" style="text-align:left;">No Perbandingan</td>
                <td width="35%"style="text-align:left;">: '.$this->perbandinganharga_no.' </td>
                <td width="5%"></td>
                <td width="20%" style="text-align:left;">Tanggal</td>
                <td width="20%" style="text-align:left;">: '.format_date($this->perbandinganharga_tglinput, 'Y-m-d H:i:s').'</td>
            </tr>
            <tr>
                <td style="text-align:left;">No Request</td>
                <td style="text-align:left;">: '.$this->pr_no.' </td>
                <td></td>
                <td style="text-align:left;">Page</td>
                <td style="text-align:left;">: '.$this->getAliasNumPage().' of '.$this->getAliasNbPages().'</td>
            </tr>      
        </table>';

        
        $this->writeHTML($header, true, false, false, false, '');
        $this->writeHTML($keterangan, true, false, false, false, '');
    }
}

$pdf = new customPdf('P', 'mm', 'A4', true, 'UTF-8', false);
$title = $header['perbandinganharga_no'];
$pdf->SetTitle($title);

$pdf->setPrintFooter(false);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');
/** Margin */ 
$pdf->SetHeaderMargin(5);
$pdf->SetFooterMargin(10);
$pdf->SetMargins(5, 35, 5);

$pdf->setKeterangan($header);

$pdf->AddPage();

$pdf->writeHTML($konten, true, false, false, false, '');

$created=null;$createdtgl=null;$checked1=null;$checked1tgl=null;
$checked2=null;$checked2tgl=null;$approved=null;$approvedtgl=null;

$txtOutput = $title.'.pdf';
$pdf->Output($txtOutput, 'I');