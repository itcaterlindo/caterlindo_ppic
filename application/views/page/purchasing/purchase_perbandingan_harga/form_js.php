<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form('<?php echo $sts; ?>','<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#<?php echo $box_loader_id; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
		open_table();
		first_load('<?php echo 'idTableBoxLoader'.$master_var; ?>', '<?php echo 'idTableBoxContent'.$master_var; ?>');
	});

	function open_form(sts, id) {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/form_main'; ?>',
				data: {'sts': sts, 'id' : id},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					render_select2('select2');
					table_perbandinganhargadetail_main (id);
					moveTo('idMainContent');
				}
			});
		});
	}

	function selectPR (params) {
		box_overlayForm('in');
		event.preventDefault();
		let sts = $('#idtxtSts').val();
		let pr_kd = '';
		if(params == 'info'){
			pr_kd = $('#idtxtpr_kd_add').val();
		}else{
			pr_kd = $('#idtxtpr_kd').val();
		}

		let perbandinganharga_kd = $('#idtxtperbandinganharga_kd').val();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/table_detailpr_main'; ?>',
			data: {'sts': sts,'pr_kd': pr_kd, 'perbandinganharga_kd':perbandinganharga_kd},
			success: function(html) {
				console.log(html);
				if(params == 'info'){
					$('#info').html('');
					$('#info').html(html);
					box_overlayForm('out');
					render_icheck ('icheck');
				}else{
					$('#idprdetail').html('');
					$('#idprdetail').html(html);
					box_overlayForm('out');
				    render_icheck ('icheck');
				}
				
			}
		});
	}

	function table_perbandinganhargadetail_main (perbandinganharga_kd) {
		box_overlayForm('in');
		event.preventDefault();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/table_perbandinganhargadetail_main'; ?>',
			data: {'perbandinganharga_kd': perbandinganharga_kd},
			success: function(html) {
				$('#<?php echo $box_content2_id; ?>').html(html);
				box_overlayForm('out');
			}
		});
	}

	function table_perbandinganhargadetailkatalog_main (perbandinganharga_kd) {
		box_overlayForm('in');
		event.preventDefault();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/table_perbandinganhargadetailkatalog_main'; ?>',
			data: {'perbandinganharga_kd': perbandinganharga_kd},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
				$('#<?php echo $box_content2_id; ?>').html('');
				box_overlayForm('out');
			}
		});
	}

	function form_perbandinganhargadetailkatalog_main (sts, perbandinganhargadetail_kd, perbandinganhargadetailkatalog_kd) {
		event.preventDefault();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_perbandinganhargadetailkatalog_main'; ?>',
			data: {'sts': sts, 'perbandinganhargadetail_kd' : perbandinganhargadetail_kd, 'perbandinganhargadetailkatalog_kd': perbandinganhargadetailkatalog_kd},
			success: function(html) {
				toggle_modal('Perbandingan Harga Katalog', html);
				render_select2('select2');
			}
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitData(form_id, params) {
		box_overlayForm('in');
		event.preventDefault();
		var id_form = document.getElementById(form_id);
		// edit atau add
		var sts =  $('#idtxtSts').val();
		let	url = "<?php echo base_url().$class_link; ?>/action_insert";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(id_form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					table_perbandinganhargadetail_main (resp.data);
					$('.clPerbandinganharga_kd').val(resp.data);
					$('.errInput').html('');
					selectPR('');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
				box_overlayForm('out');
			} 	        
		});
	}

	function submitDataKatalog(form_id) {
		box_overlayForm('in');
		event.preventDefault();
		var id_form = document.getElementById(form_id);
		console.log(id_form);
		let	url = "<?php echo base_url().$class_link; ?>/action_selected";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(id_form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					toggle_modal('', '');
					let perbandinganharga_kd = $('#idtxtperbandinganharga_kd').val();
					table_perbandinganhargadetailkatalog_main (perbandinganharga_kd);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
				box_overlayForm('out');
			} 	        
		});
	}

	function actionGenerateKatalog() {
		event.preventDefault();
		let perbandinganharga_kd = $('#idtxtDetailperbandinganharga_kd').val();
		action_generate_katalog (perbandinganharga_kd);
		moveTo('idMainContent');
	}

	function action_generate_katalog (perbandinganharga_kd) {
		event.preventDefault();
		box_overlayForm('in');
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/action_generate_katalog'; ?>',
			data: {'perbandinganharga_kd': perbandinganharga_kd},
			success: function(data) {
				box_overlayForm('out');
				table_perbandinganhargadetailkatalog_main (perbandinganharga_kd);
			}
		});
	}

	function hapus_datadetailkatalog (id) {
		event.preventDefault();
		var conf = confirm('Apakah anda yakin ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_deletedetailkatalog'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						toggle_modal('', '');
						let perbandinganharga_kd = $('#idtxtperbandinganharga_kd').val();
						table_perbandinganhargadetailkatalog_main (perbandinganharga_kd);
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
					}
				}
			});
		}
	}

	function hapus_datadetailFormKatalog (id) {
		event.preventDefault();
		var conf = confirm('Apakah anda yakin ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_deletedetail'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						let perbandinganharga_kd = $('#idtxtperbandinganharga_kd').val();
						table_perbandinganhargadetailkatalog_main (perbandinganharga_kd);
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
					}
				}
			});
		}
	}
	
	function box_overlayForm(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

	function selesai() {
		event.preventDefault();
		$('#idFormBoxperbandinganHarga').remove();
		open_table();
		first_load('<?php echo 'idTableBoxLoader'.$master_var; ?>', '<?php echo 'idTableBoxContent'.$master_var; ?>');
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

</script>