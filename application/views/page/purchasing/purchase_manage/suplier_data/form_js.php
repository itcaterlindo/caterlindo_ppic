<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form('<?php echo $sts; ?>','<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#<?php echo $box_loader_id; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
		open_table();
		first_load('<?php echo 'idTableBoxLoader'.$master_var; ?>', '<?php echo 'idTableBoxContent'.$master_var; ?>');
	});

	function open_form(sts, id) {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/form_main'; ?>',
				data: {'sts': sts, 'id' : id},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					$('[data-mask]').inputmask();
					render_select2('select2')
					moveTo('idMainContent');
				}
			});
		});
	}

	function close_form() {
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$('#<?php echo $box_id; ?>').fadeOut(function() {
				$('#<?php echo $box_id; ?>').remove();
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function render_select2(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
		});
	}

	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitData() {
		box_overlay('in');
		event.preventDefault();
		var id_form = document.getElementById('idFormInput');
		// edit atau add
		var sts =  $('#idtxtSts').val();
		if (sts == 'add'){
			url = "<?php echo base_url().$class_link; ?>/action_insert";
		}else{
			url = "<?php echo base_url().$class_link; ?>/action_update";
		}
		$.blockUI({ overlayCSS: { backgroundColor: '#C0C0C0' } });

		$.ajax({
			url: url,
			type: "POST",
			data: new FormData(id_form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(resp){
				// var resp = JSON.parse(items);
				console.log(resp);
				if(resp.code == 200){
					sendTosap(resp.data, resp.data_rollback);
					// notify (resp.status, resp.pesan, 'success');
					// open_table();
					$('.errInput').html('');
				}
				else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function sendTosap(item, data_rollback){

		var sts =  $('#idtxtSts').val();
		if (sts == 'add'){
			url_api = "<?= url_api_sap() ?>AddBP";
		}else{
			url_api = " <?= url_api_sap() ?>EditBP";
		}
		
		const ppn = $('#idtxtsuplier_includeppn option:selected').text().split("|");
		console.log(item);

		var dataToSAp = {U_IDU_WEBID:item.suplier_kd,
						CardCode:item.suplier_kode,
						VendorGroupNum: item.top_sap,
						VendorVatGroup: item.ppn_sap,
						CardType: 'S',
						U_IDU_BadanUsaha:item.badanusaha_kd,
						CardName:item.suplier_nama,
						Street:item.suplier_alamat,
						CntctPrsn:item.suplier_cp,
						DebPayAcct:item.debpayacc_sap,
						Phone1:item.suplier_telpon1,
						Phone2:item.suplier_telpon2,
						Fax:item.suplier_fax,
						Email:item.suplier_email,
						Currency : (item.kd_currency === "MCN271017001" ? 'IDR' : 'USD'),
						LicTradNum: '',
						U_IDU_KodeJenisVendor:'',
						U_IDU_KodeJenisCustomer:'',
						ZipCode:'',
						U_IDU_WEBUSER:item.admin_kd,
						KdGroup:'14',
						Lines_ShipTo:{
							Street:'',
							ZipCode:'',
							Phone1:'',
							Phone2:'',
							Email:'',
							Fax:'',
							U_IDU_BadanUsaha:'',
							U_IDU_NamaCustomer:'',
							U_IDU_CP:''
		}};


		fetch(url_api, {
		method: 'POST', // or 'PUT'
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(dataToSAp),
		})
		.then((response) => response.json())
		.then((data) => {
			console.log('Success:', data);
			console.log('Datanya:', dataToSAp);
			if(data.ErrorCode == 0 ){
				open_table();
				notify ('200! Sukses.', data.Message, 'success');
				$.unblockUI()
				close_form();
			}else{
				if (sts == 'add'){
					delRollback(item.suplier_kd)
				}else{
					updRollback(data_rollback)
				}
				$.unblockUI()
				notify (data.ErrorCode + '. Gagal!', data.Message, 'error');
			}
			
		})
		.catch((error) => {
			if (sts == 'add'){
					delRollback(item.suplier_kd)
				}else{
					updRollback(data_rollback)
				}
				$.unblockUI()
			notify ('000. Gagal!', error, 'error');
		});
    }

	function delRollback(id){
		// console.log('oke manok');
		$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, 'Save SAP Failed. Rollback Success.', 'success');
						// open_table();
						// close_form()
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
					$.unblockUI()
				}
			});
	}

	function updRollback(data_rollback) {
		console.log(data_rollback)
		console.log($('#idtxtSts').val())
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_update_rollback",
			type: "POST",
			data: data_rollback,
			success: function(items){
				var resp = JSON.parse(items);
				
				if(resp.code == 200){
					console.log(resp)
					notify (resp.status, 'Save SAP Failed. Rollback Update Success.', 'success');
					// notify (resp.status, resp.pesan, 'success');
					// open_table();
					// close_form();
					$('.errInput').html('');
				}
				else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
				$.unblockUI()
			} 	        
		});
	 }

	

</script>