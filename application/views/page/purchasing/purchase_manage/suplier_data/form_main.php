<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
if($sts == 'edit'){
	extract($rowData);
	$BadanusahaSelected = $badanusaha_kd;
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtsuplier_kd', 'name'=> 'txtsuplier_kd', 'value' => isset($id) ? $id: null ));

?>
<?php if ($sts == 'edit') :?>
<div class="form-group">
	<label for='idtxtsuplier_kode' class="col-md-2 control-label">Kode Suplier</label>
	<div class="col-sm-3 col-xs-12">
		<div class="errInput" id="idErrsuplier_kode"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtsuplier_kode', 'id'=> 'idtxtsuplier_kode', 'placeholder' =>'Kode Suplier' ,'value'=> isset($suplier_kode) ? $suplier_kode: null ));?>
	</div>
</div>
<?php endif;?>
<div class="form-group">
	<label for='idtxtsuplier_nama' class="col-md-2 control-label">Nama Suplier</label>
	<div class="col-sm-2 col-xs-12">
		<div class="errInput" id="idErrSuplier_badanusaha"></div>
		<?php echo form_dropdown('txtsuplier_badanusaha', $opsiBadanusaha, isset($BadanusahaSelected)? $BadanusahaSelected : '18', array('class'=> 'form-control', 'id'=> 'idtxtsuplier_badanusaha'));?>
	</div>
	<div class="col-sm-7 col-xs-12">
		<div class="errInput" id="idErrSuplier_nama"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtsuplier_nama', 'id'=> 'idtxtsuplier_nama', 'placeholder' =>'Nama Suplier' ,'value'=> isset($suplier_nama) ? $suplier_nama: '' ));?>
	</div>
</div>

<div class="form-group">
	<label for="idtxtsuplier_alamat" class="col-md-2 control-label">Alamat Suplier</label>
	<div class="col-sm-8 col-xs-12">
		<div class="errInput" id="idErrSuplier_alamat"></div>
		<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtsuplier_alamat', 'id'=> 'idtxtsuplier_alamat', 'placeholder' =>'Alamat Suplier', 'rows' => '3', 'value'=> isset($suplier_alamat) ? $suplier_alamat: '' ));?>
	</div>
</div>

<div class="form-group">
	<label for="idtxtsuplier_cp" class="col-md-2 control-label">Contact Person</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrSuplier_cp"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtsuplier_cp', 'id'=> 'idtxtsuplier_cp', 'placeholder' =>'Contact Person' ,'value'=> isset($suplier_cp) ? $suplier_cp: '' ));?>
	</div>
	<label for="idtxtsuplier_email" class="col-md-1 control-label">Email</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrSuplier_email"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtsuplier_email', 'id'=> 'idtxtsuplier_email', 'placeholder' =>'Email' ,'value'=> isset($suplier_email) ? $suplier_email: '' ));?>
	</div>
	
</div>

<div class="form-group">
	<label for="idtxtsuplier_telpon1" class="col-md-2 control-label">Telpon1</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrsuplier_telpon1"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtsuplier_telpon1', 'id'=> 'idtxtsuplier_telpon1', 'data-inputmask' => '\'mask\': [\'+99-999-999-999-99\']', 'data-mask' => '', 'placeholder' =>'Telpon1' ,'value'=> isset($suplier_telpon1) ? $suplier_telpon1: '' ));?>
	</div>
	<label for="idtxtsuplier_telpon2" class="col-md-1 control-label">Telpon2</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrsuplier_telpon2"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtsuplier_telpon2', 'id'=> 'idtxtsuplier_telpon2', 'data-inputmask' => '\'mask\': [\'+99-999-999-999-99\']', 'data-mask' => '', 'placeholder' =>'Telpon2' ,'value'=> isset($suplier_telpon2) ? $suplier_telpon2: '' ));?>
	</div>
</div>

<div class="form-group">
	<label for="idtxtsuplier_fax" class="col-md-2 control-label">Fax</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrSuplier_fax"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtsuplier_fax', 'id'=> 'idtxtsuplier_fax', 'placeholder' =>'Fax' ,'value'=> isset($suplier_fax) ? $suplier_fax: '' ));?>
	</div>
	<label for="idtxtsuplier_npwp" class="col-md-1 control-label">NPWP</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrSuplier_npwp"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtsuplier_npwp', 'id'=> 'idtxtsuplier_npwp', 'data-inputmask' => '\'mask\': [\'99.999.999-9.999.999\']', 'data-mask' => '', 'placeholder' =>'NPWP' ,'value'=> isset($suplier_npwp) ? $suplier_npwp: '' ));?>
	</div>
</div>
<hr>
<div class="form-group">
	<label for='idtxtbank_kd' class="col-md-2 control-label">Bank</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrbank_kd"></div>
		<?php echo form_dropdown('txtbank_kd', $opsiBank, isset($bank_kd)? $bank_kd : '', array('class'=> 'form-control', 'id'=> 'idtxtbank_kd'));?>
	</div>
	<label for='idtxtsuplier_banknama' class="col-md-1 control-label">Atas Nama</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrsuplier_banknama"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtsuplier_banknama', 'id'=> 'idtxtsuplier_banknama', 'placeholder' =>'Atas Nama' ,'value'=> isset($suplier_banknama) ? $suplier_banknama: '' ));?>
	</div>
</div>

<div class="form-group">
	<label for='idtxtsuplier_banknorek' class="col-md-2 control-label">No Rek</label>
	<div class="col-sm-3 col-xs-12">
		<div class="errInput" id="idErrsuplier_banknorek"></div>
		<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtsuplier_banknorek', 'id'=> 'idtxtsuplier_banknorek', 'placeholder' =>'No Rek' ,'value'=> isset($suplier_banknorek) ? $suplier_banknorek: '' ));?>
	</div>
</div>
<hr>


<div class="form-group">
	<label for="idtxtsuplier_termpayment" class="col-md-2 control-label">Term Payment (hari)</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrsuplier_includeppn"></div>
		<?php echo form_dropdown('txtsuplier_termpayment', $data_top, isset($top_sap)? $top_sap : '', array('class'=> 'form-control', 'id'=> 'idtxtsuplier_termpayment'));?>
	</div>
</div>


<div class="form-group">
	<label for="idtxtsuplier_leadtime" class="col-md-2 control-label">LeadTime</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrSuplier_leadtime"></div>
		<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtsuplier_leadtime', 'id'=> 'idtxtsuplier_leadtime', 'placeholder' =>'Leadtime' ,'value'=> isset($suplier_leadtime) ? $suplier_leadtime: '' ));?>
	</div>
</div>

<div class="form-group">
	<label for='idtxtsuplier_includeppn' class="col-md-2 control-label">PPN</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrsuplier_includeppn"></div>
		<?php echo form_dropdown('txtsuplier_includeppn', $data_ppn, isset($ppn_sap)? $ppn_sap : '', array('class'=> 'form-control', 'id'=> 'idtxtsuplier_includeppn'));?>
	</div>
	<label for='idtxtkd_currency' class="col-md-1 control-label">Currency</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrkd_currency"></div>
		<?php echo form_dropdown('txtkd_currency', isset($opsiCurrency)? $opsiCurrency : '', isset($kd_currency)? $kd_currency : '', array('class'=> 'form-control', 'id'=> 'idtxtkd_currency'));?>
	</div>
</div>
<div class="form-group">
	<label for='idtxtsuplier_payto' class="col-md-2 control-label">Pay To</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrsuplier_payto"></div>
		<?php echo form_dropdown('txtsuplier_payto', $opsiPayto, isset($suplier_payto)? $suplier_payto : '', array('class'=> 'form-control select2', 'id'=> 'idtxtsuplier_payto'));?>
	</div>
</div>
<div class="form-group">
	<label for='idtxtsuplier_payto' class="col-md-2 control-label">DebPay Acc</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrsuplier_payto"></div>
		<select name="txtsuplier_debpayacc" class= 'form-control select2' id="idtxtsuplier_payto">
			<option value="201020101" selected>Creditors Ledger</option>
			<option value="101020101">Debtors Ledger</option>
		</select>
	</div>
</div>

<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-sm">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" onclick="submitData()" class="btn btn-primary btn-sm">
			<i class="fa fa-save"></i> Simpan
		</button>
	</div>
</div>
<?php echo form_close(); ?>