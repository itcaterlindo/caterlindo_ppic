<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';

if ($sts == 'edit'){
	extract($rowData);
	$AdminSelected = $pruser_user;
	$PRStatusSelected = $prstatus_kd;
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpruser_kd', 'name'=> 'txtpruser_kd', 'value' => isset($pruser_kd) ? $pruser_kd: null ));

?>

<div class="row">
	<div class="form-group">
		<label for='idtxtpruser_user' class="col-md-2 control-label">User</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpruser_user"></div>
			<?php echo form_dropdown('txtpruser_user', $opsiAdmin, isset($AdminSelected)? $AdminSelected : '', array('class'=> 'form-control select2', 'id'=> 'idtxtpruser_user'));?>
		</div>	
	</div>

	<div class="form-group">
		<label for='idtxtpruser_email' class="col-md-2 control-label">Email</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpruser_email"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtpruser_email', 'id'=> 'idtxtpruser_email', 'placeholder' =>'Email', 'value'=> isset($pruser_email) ? $pruser_email: null ));?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtprstatus_kd' class="col-md-2 control-label">Status</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrprstatus_kd"></div>
			<?php echo form_dropdown('txtprstatus_kd', $opsiPRstatus, isset($PRStatusSelected)? $PRStatusSelected : '', array('class'=> 'form-control', 'id'=> 'idtxtprstatus_kd'));?>
		</div>	
	</div>

</div>
<hr>
<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-4 col-sm-offset-2 col-xs-12">
			<button type="reset" name="btnReset" onclick="resetFormMain()" class="btn btn-default btn-flat">
				<i class="fa fa-refresh"></i> Reset
			</button>
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData()" class="btn btn-primary">
					<i class="fa fa-save"></i> Simpan
				</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>
