<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
if($sts == 'edit'){
	extract($rowData);
	$SuplierSelected = $suplier_kd;
	$MaterialSelected = $rm_kd;
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));

echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtkatalog_kd', 'name'=> 'txtkatalog_kd', 'value' => isset($id) ? $id: null ));
?>

<div class="form-group">
	<label for='idtxtsuplier_kd' class="col-md-2 control-label">Suplier</label>
	<div class="col-sm-6 col-xs-12">
		<div class="errInput" id="idErrSuplier_kd"></div>
		<?php echo form_dropdown('txtsuplier_kd', $opsiSuplier, isset($SuplierSelected)? $SuplierSelected : '', array('class'=> 'form-control select2', 'id'=> 'idtxtsuplier_kd'));?>
	</div>
</div>

<div class="form-group">
	<label for='idtxtmaterial_kd' class="col-md-2 control-label">Material</label>
	<div class="col-sm-6 col-xs-12">
		<div class="errInput" id="idErrMaterial_kd"></div>
		<?php echo form_dropdown('txtmaterial_kd', $opsiMaterial, isset($MaterialSelected)? $MaterialSelected : '', array('class'=> 'form-control select2', 'id'=> 'idtxtmaterial_kd'));?>
	</div>	
</div>

<div class="form-group">
	<label for='idtxtkatalog_harga' class="col-md-2 control-label">Harga</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrKatalog_harga"></div>
		<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtkatalog_harga', 'id'=> 'idtxtkatalog_harga', 'placeholder' =>'Harga' ,'value'=> isset($katalog_harga) ? $katalog_harga: null ));?>
	</div>
	<label for='idtxtkd_currency' class="col-md-1 control-label">Currency</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrkd_currency"></div>
		<?php echo form_dropdown('txtkd_currency', $opsiCurrency, isset($kd_currency)? $kd_currency : null, array('class'=> 'form-control select2', 'id'=> 'idtxtkd_currency'));?>
	</div>	
</div>

<div class="form-group">
	<label for='idtxtkatalog_satuankd' class="col-md-2 control-label">Satuan</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrkatalog_satuankd"></div>
		<?php echo form_dropdown('txtkatalog_satuankd', $opsiSatuan, isset($katalog_satuankd)? $katalog_satuankd : '', array('class'=> 'form-control select2', 'id'=> 'idtxtkatalog_satuankd'));?>
	</div>
	<label for='idtxtkatalog_konversi' class="col-md-1 control-label">Konversi</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrKatalog_konversi"></div>
		<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtkatalog_konversi', 'id'=> 'idtxtkatalog_konversi', 'placeholder' =>'Konversi' ,'value'=> isset($katalog_konversi) ? $katalog_konversi: null ));?>
	</div>	
</div>

<div class="form-group">
	<label for='idtxtkatalog_duedate' class="col-md-2 control-label">Due Date (Hari)</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrKatalog_duedate"></div>
		<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtkatalog_duedate', 'id'=> 'idtxtkatalog_duedate', 'placeholder' =>'Due Date' ,'value'=> isset($katalog_duedate) ? $katalog_duedate: null ));?>
	</div>
	<label for='idtxtkatalog_aktif' class="col-md-1 control-label">Aktif</label>
	<div class="col-sm-4 col-xs-12">
		<div class="errInput" id="idErrkatalog_aktif"></div>
		<?php echo form_dropdown('txtkatalog_aktif', ['1' => 'Ya', '0' => 'Tidak'], isset($katalog_aktif)? $katalog_aktif : '1', array('class'=> 'form-control', 'id'=> 'idtxtkatalog_aktif'));?>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" onclick="submitData()" class="btn btn-primary btn-flat">
			<i class="fa fa-save"></i> Submit
		</button>
	</div>
</div>
<?php echo form_close(); ?>
