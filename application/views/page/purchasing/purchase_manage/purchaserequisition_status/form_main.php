<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';

if ($sts == 'edit'){
	extract($rowData);
	$opsiSendemailSelected = $prstatus_sendemail;
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtprstatus_kd', 'name'=> 'txtprstatus_kd', 'value' => isset($prstatus_kd) ? $prstatus_kd: null ));

?>

<div class="row">
	<div class="form-group">
		<label for='idtxtprstatus_nama' class="col-md-2 control-label">Nama</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrprstatus_nama"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtprstatus_nama', 'id'=> 'idtxtprstatus_nama', 'placeholder' =>'Nama', 'value'=> isset($prstatus_nama) ? $prstatus_nama: null ));?>
		</div>
	</div>
	
	<div class="form-group">
		<label for='idtxtprstatus_level' class="col-md-2 control-label">Level</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrprstatus_level"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control tt-input', 'name'=> 'txtprstatus_level', 'id'=> 'idtxtprstatus_level', 'placeholder' =>'Level', 'value'=> isset($prstatus_level) ? $prstatus_level: null ));?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtprstatus_label' class="col-md-2 control-label">Label</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrprstatus_label"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtprstatus_label', 'id'=> 'idtxtprstatus_label', 'placeholder' =>'Label', 'value'=> isset($prstatus_label) ? $prstatus_label: null ));?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtprstatus_sendemail' class="col-md-2 control-label">Send Email</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrprstatus_sendemail"></div>
			<?php echo form_dropdown('txtprstatus_sendemail', $opsiSendemail, isset($opsiSendemailSelected)? $opsiSendemailSelected : '', array('class'=> 'form-control', 'id'=> 'idtxtprstatus_sendemail'));?>
		</div>	
	</div>

</div>
<hr>
<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-4 col-sm-offset-2 col-xs-12">
			<button type="reset" name="btnReset" onclick="resetFormMain()" class="btn btn-default btn-flat">
				<i class="fa fa-refresh"></i> Reset
			</button>
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData()" class="btn btn-primary">
					<i class="fa fa-save"></i> Simpan
				</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>
