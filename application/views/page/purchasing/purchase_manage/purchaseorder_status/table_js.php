<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
    open_table();
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		window.location.assign('<?php echo base_url();?>/purchasing/purchase_manage/purchasing_manage_main');		
	});

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_main'; ?>',
			data: {sts:'add', id:''},
			success: function(html) {
				toggle_modal('Tambah Status', html );
			}
		});
		
	});

	function open_table() {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_main'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function edit_data(id){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_main'; ?>',
			data: {sts:'edit', id:id},
			success: function(html) {
				toggle_modal('Edit User', html );
			}
		});
	}

	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitData() {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById('idFormInput');
		// edit atau add
		var sts =  $('#idtxtSts').val();
		if (sts == 'add'){
			url = "<?php echo base_url().$class_link; ?>/action_insert";
		}else{
			url = "<?php echo base_url().$class_link; ?>/action_update";
		}

		$.ajax({
			url: url,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					open_table();
					toggle_modal('', '')
					$('.errInput').html('');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function hapus_data(id) {
		var conf = confirm('Apakah anda yakin menghapus item ini ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						open_table();
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				}
			});
		}
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
		box_overlay('out');
    }

	function resetFormMain() {
		$('.tt-input').val('');	
	}

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

</script>