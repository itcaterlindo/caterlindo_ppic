<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';

if ($sts == 'edit'){
	extract($rowData);
	$opsiSendemailSelected = $postatus_sendemail;
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpostatus_kd', 'name'=> 'txtpostatus_kd', 'value' => isset($postatus_kd) ? $postatus_kd: null ));

?>

<div class="row">
	<div class="form-group">
		<label for='idtxtpostatus_nama' class="col-md-2 control-label">Nama</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpostatus_nama"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtpostatus_nama', 'id'=> 'idtxtpostatus_nama', 'placeholder' =>'Nama', 'value'=> isset($postatus_nama) ? $postatus_nama: null ));?>
		</div>
	</div>
	
	<div class="form-group">
		<label for='idtxtpostatus_level' class="col-md-2 control-label">Level</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpostatus_level"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control tt-input', 'name'=> 'txtpostatus_level', 'id'=> 'idtxtpostatus_level', 'placeholder' =>'Level', 'value'=> isset($postatus_level) ? $postatus_level: null ));?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtpostatus_label' class="col-md-2 control-label">Label</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpostatus_label"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtpostatus_label', 'id'=> 'idtxtpostatus_label', 'placeholder' =>'Label', 'value'=> isset($postatus_label) ? $postatus_label: null ));?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtpostatus_sendemail' class="col-md-2 control-label">Send Email</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpostatus_sendemail"></div>
			<?php echo form_dropdown('txtpostatus_sendemail', $opsiSendemail, isset($opsiSendemailSelected)? $opsiSendemailSelected : '', array('class'=> 'form-control', 'id'=> 'idtxtpostatus_sendemail'));?>
		</div>	
	</div>

</div>
<hr>
<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-4 col-sm-offset-2 col-xs-12">
			<button type="reset" name="btnReset" onclick="resetFormMain()" class="btn btn-default btn-flat">
				<i class="fa fa-refresh"></i> Reset
			</button>
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData()" class="btn btn-primary">
					<i class="fa fa-save"></i> Simpan
				</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>
