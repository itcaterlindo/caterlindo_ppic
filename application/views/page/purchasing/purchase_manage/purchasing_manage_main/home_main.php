<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<div class="row">
	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-teal">
		<div class="inner">
			<h3>Status PO</h3>

			<p>Status Purchase Order</p>
		</div>
		<div class="icon">
			<i class="ion ion-bag"></i>
		</div>
		<a href="purchaseorder_status" class="small-box-footer">Manage <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-teal">
		<div class="inner">
			<h3>Users PO</h3>

			<p>User Purchase Order</p>
		</div>
		<div class="icon">
			<i class="ion ion-bag"></i>
		</div>
		<a href="purchaseorder_users" class="small-box-footer">Manage <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-aqua">
		<div class="inner">
			<h3>Status PR</h3>

			<p>Status Purchase Requisition</p>
		</div>
		<div class="icon">
			<i class="ion ion-bag"></i>
		</div>
		<a href="purchaserequisition_status" class="small-box-footer">Manage <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-aqua">
		<div class="inner">
			<h3>Users PR</h3>

			<p>User Purchase Requisition</p>
		</div>
		<div class="icon">
			<i class="ion ion-bag"></i>
		</div>
		<a href="purchaserequisition_users" class="small-box-footer">Manage <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	
</div>