<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<table id="idTable" class="table table-bordered table-striped table-hover display responsive" style="width:100%; font-size:90%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:7%; text-align:center;">Tgl PO</th>
		<th style="width:7%; text-align:center;">Tgl Plan</th>
		<th style="width:7%; text-align:center;">Tgl Datang</th>
		<th style="width:10%; text-align:center;">RM Deskripsi</th>
		<th style="width:7%; text-align:center;">RM lama</th>
		<th style="width:7%; text-align:center;">RM kode</th>
		<th style="width:10%; text-align:center;">Suplier</th>
		<th style="width:5%; text-align:center;">Qty</th>
		<th style="width:5%; text-align:center;">Satuan</th>
		<th style="width:5%; text-align:center;">Harga Satuan</th>
		<th style="width:5%; text-align:center;">No PO</th>
		<th style="width:10%; text-align:center;">No Surat Jalan</th>
		<th style="width:7%; text-align:center;">Code Srj</th>
		<th style="width:7%; text-align:center;">Tgl Input GR</th>
	</tr>
	</thead>
	<tbody>
	<?php 
	$no = 1;
	foreach ($result_grdetail as $r) : ?>
	<tr>
		<td class="dt-center"><?= $no?></td>
		<td><?= format_date($r['po_tanggal'], 'd-m-Y')?></td>
		<td><?= format_date($r['prdetail_duedate'], 'd-m-Y')?></td>
		<td><?= format_date($r['rmgr_tgldatang'], 'd-m-Y')?></td>
		<td><?php 
		$desc = $r['podetail_deskripsi'].'/'.$r['podetail_spesifikasi']; 
		if ($r['rmgr_type'] == 'BACK') {
			$desc = $r['whdeliveryorderdetail_deskripsi'].'/'.$r['whdeliveryorderdetail_spesifikasi']; 
		}
		echo $desc
		?></td>
		<td><?= $r['rm_oldkd']?></td>
		<td><?= $r['rm_kode']?></td>
		<td><?= $r['nm_select'].' '.$r['suplier_nama']?></td>
		<td class="dt-right"><?= $r['rmgr_qty']?></td>
		<td class="dt-left"><?php
		$rmsatuan = $r['rmsatuan_nama'];
		if ($r['rmgr_type'] == 'BACK') {
			$rmsatuan = $r['rmsatuan_whdodetail'];
		}
		echo $rmsatuan;
		?></td>
		<td><?= $r['podetail_harga']?></td>
		<td><?= $r['po_no']?></td>
		<td><?= $r['rmgr_nosrj']?></td>
		<td><?= $r['rmgr_code_srj']?></td>
		<td><?=  format_date($r['rmgr_tglinput'], 'd-m-Y')?></td>
		
	</tr>
	<?php 
	$no++;
	endforeach;?>
	</tbody>
</table>

<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [
				{
					"extend" : "excel",
					"title" : "Report Incoming Material",
					"exportOptions": {
						"columns": [ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14 ]
					}
				},{
                extend: 'print',
				// messageTop: function () {
                //     return `<p style="font-size:18pt">
				// 		Kertas Kerja Perhitungan Fisik Aset dan Inventaris<br>
				// 		Periode :  `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</p>`;
                // },
				// messageTop: function () {
                //     return `<p style="font-size:22pt">
				// 		Laporan Asset ditemukan<br>
				// 		Periode :  Desember - 2021</p>`;
                // },
				exportOptions: {
					columns: [ 0, 1, 2, 4, 5, 6, 7 ]
				},
				title: `&nbsp;`,
				// title: 'PT Caterlindo -- Asset di stoktake Periode "' + $('#periode_nomor_seri').val() +'"',
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '13pt' )
                        .prepend(`<div class="container" style="margin-left:-10px;"><br>
												<div style="clear: both; display: inline-block; overflow: hidden; white-space: nowrap; margin-left: 130%;">
												<h1 style="font-size:50px;"><b>Report Incoming Material</b></h1>
												</div>

												<div class="row">
													<div class="col-md-8">
													<img src="https://caterlindo.co.id/web/Images/Logo-Utama.png" style="width: 70%; height: 140px; margin-top:-98px;" />
													</div>
												
													<div class="col-md-4">
													<table class="table" style="margin-left: 130%;">
														<tbody>
														<tr>
															<th scope="row" style="text-align: left;"><p>No. Dokumen</p></th>
															<td><p>CAT4-WRH-001</p></td>
														</tr>
														<tr>
															<th scope="row" style="text-align: left;"><p >Tanggal Terbit</p></th>
															<td><p>31 Aug 2023</p></td>
														</tr>
														<tr>
															<th scope="row" style="text-align: left;"><p>Revisi</p></th>
															<td><p>01</p></td>
														</tr>
														</tbody>
													</table>
													</div>
												</div>
											</div>
											</div>`);
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
						var last = null;
						var current = null;
						var bod = [];
		
						var css = '@page { size: potrait }',
							head = win.document.head || win.document.getElementsByTagName('head')[0],
							style = win.document.createElement('style');
		
						style.type = 'text/css';
						style.media = 'print';
		
						if (style.styleSheet)
						{
							style.styleSheet.cssText = css;
						}
						else
						{
							style.appendChild(win.document.createTextNode(css));
						}
		
						head.appendChild(style);
                }}, 

				
		]
		});
	}
</script>