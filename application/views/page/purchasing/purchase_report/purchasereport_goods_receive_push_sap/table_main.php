<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body table-responsive no-padding">
<table id="idTable" class="table table-bordered table-striped table-hover display" style="width:100%; font-size:85%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:3%; text-align:center;">No. SJ</th>
		<th style="width:3%; text-align:center;">No. PO</th>
		<th style="width:3%; text-align:center;">Kode Suplier</th>
		<th style="width:3%; text-align:center;">Nama Suplier</th>
		<th style="width:3%; text-align:center;">Keterangan</th>
		<th style="width:3%; text-align:center;">Tgl Datang</th>
		<th style="width:3%; text-align:center;">Status Push SAP</th>
	</tr>
	</thead>
	<tbody>
	<?php 
	$no = 1;
	foreach ($resultGR as $r) : ?>
	<tr>
		<td style="text-align: center;"><?= $no?></td>
		<td><?= $r['rmgr_nosrj'] ?></td>
		<td><?= $r['po_no'] ?></td>
		<td><?= $r['suplier_kode'] ?></td>
		<td><?= $r['suplier_nama'] ?></td>
		<td><?= $r['rmgr_ket'] ?></td>
		<td><?= str_replace(' | ', '</br>', $r['rmgr_tgldatang']) ?></td>
		<td> <?= $r['status_sap'] == 0 ? '<span class="label label-danger">Belum</span>' : '<span class="label label-success">Sudah</span>'  ?></td>
	</tr>
	<?php 
	$no++;
	endforeach;?>
	</tbody>
</table>
</div>

<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"title" : "Rekap PO GR Push SAP",
				"exportOptions": {
					"columns": [ 0,1,2,3,4,5,6,7 ]
				}
			}],
		});
	}
</script>