<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	var hot;
	const sourceDataObject = [
				{
					"suplier_kd": "",
					"suplier_nama": "",
					"material": "",
					"leadtime": "",
					"po_no": "",
					"pr_no": "",
					"pr_date": "",
					"po_date": "",
					"gr_plan": "",
					"ppic_plan": "",
					"recive": "",
					"real_leadtime": "",
					"__children": [
						{
							"material": "",
							"leadtime": "",
							"po_no": "",
							"pr_no": "",
							"pr_date": "",
							"po_date": "",
							"gr_plan": "",
							"ppic_plan": "",
							"recive": "",
							"real_leadtime": "",
						}
					]
				}
		];
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

    $(document).ready(function () {
        render_select2('select2');
		render_select22('select22');
		render_selectpo('po_no');
		open_table();
		search();
		$('.po').hide();


		$('.filter').on('change', function () {
			if($(this).val() == 'po'){
				$('.po').hide();
				$('.sup').show();
				$('.tahun').show();
				$('.bulan').show();
			}else{
				$('.po').show();
				$('.sup').hide();
				$('.tahun').hide();
				$('.bulan').hide();
				
			}
		});


    });
    
	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}


	function open_table() {
		
		var example = document.getElementById('example2');
		hot = new Handsontable(example, {
			data: sourceDataObject,
			colWidths: 260,
			rowHeaders: true,
			nestedRows: true,
			contextMenu: true,
			className: "htCenter",
			colWidths: [],
			colHeaders: [],
			width: '100%',
			height: 1000,
			rowHeights: 23,
			rowHeaders: true,
		});
		// randerData('', '', '');
	}
	
	function search(){
		$('#search').click(function (e) { 
			e.preventDefault();
			var suplier = $('.select22').val();
			var bulan = $('#bulan').val();
			var tahun = $('#tahun').val();
			var filter = $('.filter').val();
			var po_no = $('.po_no').val();

			randerData(suplier, bulan, tahun, filter, po_no );
		});
	}

	function randerData(suplier, bulan, tahun, filter, po_no) {
        $.ajax({
				type: 'GET',
				data: {'sup' : suplier, 'bulan': bulan, 'tahun' : tahun, 'filter' : filter, 'po_no' : po_no},
				url: '<?php echo base_url().$class_link.'/getData'; ?>',
				success: function(html) {
						console.log(html)
					if(html.length != 0){

						if(filter == 'detail'){
							hot.updateSettings({
								colWidths: [100, 400, 100, 70, 120, 120, 150, 150, 150, 150, 150, 100],
								colHeaders: 
										['Kode', 'Suplier', 'No Po', 'No Req', 'No Seri', 'Po Date', 'GR Plan', 'Ppic Plan',  'Material', 'Qty Order', 'Item']
								
								
							});
							hot.loadData(html)
						}else{
							hot.updateSettings({
								colWidths: [100, 400, 100, 70, 100, 100, 120, 120, 150, 150, 150, 150, 150, 400],
								colHeaders: ['Kode', 'Suplier', 'Material', 'Leadtime', 'Real Leadtime', 'Ppic Leadtime', 'No Po', 'No Req', 'Req Date', 'Po Date', 'GR Plan', 'Ppic Plan', 'Recive', 'Note']
									
							});
							hot.loadData(html)
						}
						notify('200', 'Sukses.', 'success');
						

					}else{
						hot.loadData(sourceDataObject)
						notify ('404', 'Data Kosong.', 'error');
						console.log(html);
						console.log('kosong')	
					}
					
				}
			});
    }

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

    function render_select2(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_select22(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			multiple: true,
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_selectpo(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			multiple: true,
			placeholder: '--Pilih Opsi--',
		});
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
		box_overlay('out');
    }

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

</script>