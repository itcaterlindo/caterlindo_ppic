<?php
defined('BASEPATH') or exit('No direct script access allowed!');

/* --SET DEFAULT PROPERTY UNTUK BOX-- */
$js_file = 'table_js';
$box_type = 'Table';
$box_title = 'Table Report Lead Time Suplier';
$data['master_var'] = 'ReportLeadTimeSuplier';
$data['class_link'] = $class_link;
$data['box_id'] = 'id'.$box_type.'Box'.$data['master_var'];
$data['btn_hide_id'] = 'id'.$box_type.'BtnBoxHide'.$data['master_var'];
$data['btn_add_id'] = 'id'.$box_type.'BtnBoxAdd'.$data['master_var'];
$data['btn_remove_id'] = 'id'.$box_type.'BtnBoxRemove'.$data['master_var'];
$data['box_alert_id'] = 'id'.$box_type.'BoxAlert'.$data['master_var'];
$data['box_loader_id'] = 'id'.$box_type.'BoxLoader'.$data['master_var'];
$data['box_content_id'] = 'id'.$box_type.'BoxContent'.$data['master_var'];
$data['box_overlay_id'] = 'id'.$box_type.'BoxOverlay'.$data['master_var'];
/* --END OF BOX DEFAULT PROPERTY-- */

/* --SET VARIABEL TAMBAHAN DISINI-- */
/* --END OF CUSTOM PROPERTY-- */

/* --SET BUTTONS DISINI, NILAI "FALSE" UNTUK TIDAK TAMPIL, NILAI "TRUE" UNTUK MENAMPILKAN BUTTON-- */
$btn_hide = TRUE;
$btn_add = FALSE;
$btn_remove = FALSE;
/* --END OF BUTTONS SETUP-- */
?>

<div class="box box-primary" id="<?php echo $data['box_id']; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_title; ?></h3>
		<div class="box-tools pull-right">
			<?php
			if ($btn_add) :
				?>
				<button class="btn btn-box-tool" id="<?php echo $data['btn_add_id']; ?>" data-toggle="tooltip" title="Tambah Data">
					<i class="fa fa-plus"></i>
				</button>
				<?php
			endif;
			if ($btn_hide) :
				?>
				<button class="btn btn-box-tool btn-collapse" id="<?php echo $data['btn_hide_id']; ?>" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
					<i class="fa fa-minus"></i>
				</button>
				<?php
			endif;
			if ($btn_remove) :
				?>
				<button class="btn btn-box-tool btn-remove" id="<?php echo $data['btn_remove_id']; ?>" data-widget="remove" data-toggle="tooltip" title="Close">
					<i class="fa fa-times"></i>
				</button>
				<?php
			endif;
			?>
		</div>
	</div>
	<div class="box-body">
        <div class="row">
			<div class="form-group col-md-3">
                <label for="formGroupExampleInput">Filter</label>
                <select class="form-control filter" name="filter" id="filter">
                    <option value="po">Per-Suplier</option>
                    <option value="detail">Per-PO</option>
                </select>
            </div>
			<div class="form-group col-md-3 po" style="display: block;">
                <label for="formGroupExampleInput">Pilih PO</label>
                <?php echo form_dropdown('po_no', isset($po) ? $po : [], !empty($po_no) ? $po_no : 'ALL', array('class'=> 'form-control po_no'));?>
            </div>
			<div class="form-group col-md-3 sup">
                <label for="formGroupExampleInput">Pilih Suplier</label>
                <?php echo form_dropdown('suplier_kd', isset($supliers) ? $supliers : [], !empty($suplier_kd) ? $suplier_kd : 'ALL', array('class'=> 'form-control cl-filter select22'));?>
            </div>
		</div>
		<div class="row">
            <div class="form-group col-md-3 tahun">
                <label for="formGroupExampleInput">Tahun</label>
                <input type="number" class="form-control tahun" id="tahun" name="tahun">
                <script>document.getElementById("tahun").value = (new Date().getFullYear());</script>
            </div>
            <div class="form-group col-md-3 bulan">
                <label for="formGroupExampleInput">Bulan</label>
                <select class="form-control select2" name="bulan" id="bulan">
                    <option selected>-- Open Select--</option>
                    <option value="01">Jan</option>
                    <option value="02">Feb</option>
                    <option value="03">Mar</option>
                    <option value="04">Apr</option>
                    <option value="05">Mei</option>
                    <option value="06">Jun</option>
                    <option value="07">Jul</option>
                    <option value="08">Ags</option>
                    <option value="09">Sep</option>
                    <option value="10">Okt</option>
                    <option value="11">Nov</option>
                    <option value="12">Des</option>
                </select>
            </div>
			<!-- <div class="form-group col-md-3 bulan">
                <label for="formGroupExampleInput">Semester</label>
                <select class="form-control select2" name="smt" id="smt">
                    <option selected>-- Open Select--</option>
                    <option value="01">1</option>
                    <option value="02">2</option>
                </select>
            </div> -->
		</div>
		<div class="row">
			<div class="form-group col-md-5">
				<label for="formGroupExampleInput"></label>
                <button type="button" id="search" class="btn btn-primary"><i class="fa fa-eye"></i>&nbsp;Search</button>
            </div>
        </div>
        <br>
        <hr>
		<div id="<?php echo $data['box_alert_id']; ?>"></div>
		<div id="<?php echo $data['box_loader_id']; ?>" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div>
			<div class="box-body table-responsive no-padding">
				<div id="example2"></div>
			</div>
		</div>
	</div>
	<div class="box-footer">
	</div>
	<div class="overlay" id="<?php echo $data['box_overlay_id']; ?>" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
	<?php $this->load->view('page/'.$class_link.'/'.$js_file, $data); ?>
</div>