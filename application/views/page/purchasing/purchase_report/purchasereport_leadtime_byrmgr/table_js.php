<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	var hot;
	const sourceDataObject = [];
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

    $(document).ready(function () {
        render_select2('select2');
		render_select22('select22');
		render_selectpo('po_no');
		open_table();
		search();
		$('.po').hide();


		$('#smt').on('change', function () {
			// console.log($(this).val());
			if($(this).val() == '0'){
				$('.bulan').show();
			}else{
				$('.bulan').hide();				
			}
		});

		$('#bulan').on('change', function () {
			if($(this).val() == '0'){
				$('.smt').show();
			}else{
				$('.smt').hide();				
			}
		});


    });
    
	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}


	function open_table() {
		
		var example = document.getElementById('example2');
		hot = new Handsontable(example, {
			data: sourceDataObject,
			colWidths: 260,
			rowHeaders: false,
			nestedRows: true,
			contextMenu: true,
			className: "htCenter",
			colWidths: [],
			colHeaders: [],
			width: '100%',
			height: 1000,
			rowHeights: 23,
			contextMenu: ['copy', 'cut'],
			beforeCopy: function(data) {
			var headers = [];
			var selection = this.getSelectedRange();
			var startCol = Math.min(selection[0].from.col, selection[0].to.col);
			var endCol = Math.max(selection[0].from.col, selection[0].to.col);

			for (var i = startCol; i <= endCol; i++) {
				headers.push(this.getColHeader(i));
			}
			
			data.splice(0, 0, headers);
			}
			// cells: function (row, col, prop,value) {
			// 	var cellProperties = {};          
			// 	cellProperties.renderer = firstRowRenderer;
			// return cellProperties;
			// }
		});
		// randerData('', '', '');
	}

	function ReturnQty(instance, td, row, col, prop, value, cellProperties) {
		Handsontable.renderers.TextRenderer.apply(this, arguments);
		if(!value || value === '' || value == null ) {                        
			td.innerHTML = "9";
		}    
	}

	function LeadtimePercent(instance, td, row, col, prop, value, cellProperties){
		Handsontable.renderers.TextRenderer.apply(this, arguments);
    	var per = instance.getDataAtCell(row, col - 1) / instance.getDataAtCell(row, col - 10);
		var kali = per * 100;
		td.innerHTML = Math.round(kali) + '%';
	}
	function returnPercent(instance, td, row, col, prop, value, cellProperties){
		Handsontable.renderers.TextRenderer.apply(this, arguments);
    	var per = instance.getDataAtCell(row, col - 1) / instance.getDataAtCell(row, col - 3);
		var kali = per * 100;
		var ok_string = String((Math.round(kali * 100) / 100).toFixed(2) + '%');
		td.innerHTML = ok_string;
	}

	function LtScore(instance, td, row, col, prop, value, cellProperties){
		Handsontable.renderers.TextRenderer.apply(this, arguments);
    	var per = instance.getDataAtCell(row, col - 2) / instance.getDataAtCell(row, col - 11);
		var kali = Math.round(per * 100);
		// td.innerHTML = kali;
		td.style.fontWeight = 'bold';
		if(kali == 0){
			td.innerHTML = '5';
		}
		else if(kali > 0 && kali < 26){
			td.innerHTML = '4';
		}else if(kali > 25 && kali < 51){
			td.innerHTML = '3';
		}else if(kali > 50 && kali < 76){
			td.innerHTML = '2';
		}else if(kali > 75 && kali < 101){
			td.innerHTML = '1';
		}
		
	}

	function returnScore(instance, td, row, col, prop, value, cellProperties){
		Handsontable.renderers.TextRenderer.apply(this, arguments);
    	var per = instance.getDataAtCell(row, col - 2) / instance.getDataAtCell(row, col - 4);
		var kali = Math.round(per * 100);

		td.style.fontWeight = 'bold';
		if(kali == 0){
			td.innerHTML = '5';
		}
		else if(kali > 0 && kali < 26){
			td.innerHTML = '4';
		}else if(kali > 25 && kali < 51){
			td.innerHTML = '3';
		}else if(kali > 50 && kali < 76){
			td.innerHTML = '2';
		}else if(kali > 75 && kali < 101){
			td.innerHTML = '1';
		}
		
	}
	
	function search(){
		$('#search').click(function (e) { 
			e.preventDefault();
			var suplier = $('.select22').val();
			var bulan = $('#bulan').val();
			var tahun = $('#tahun').val();
			var smt = $('#smt').val();
			console.log(suplier, bulan, tahun)
			randerData(suplier, bulan, tahun, smt);
		});
	}

	function randerData(suplier, bulan, tahun, smt) {
        $.ajax({
				type: 'GET',
				data: {'sup' : suplier, 'bulan': bulan, 'tahun' : tahun, 'smt' : smt},
				url: '<?php echo base_url().$class_link.'/lossData'; ?>',
				success: function(html) {
						
					if(html.length != 0){
						for (let key in html) {
							delete html[key].suplier_kd;
							delete html[key].sup_kd;
							delete html[key].rmgroupsup_kd_real;
							delete html[key].rmgroupsup_kd;
							if(html[key].return_count_real == null){
								html[key].return_count_real = 0;
							}
						}
						console.log(html)

							hot.updateSettings({
								colWidths: [100, 250, 150, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90],
								colHeaders: 
										['Kode suplier', 'Suplier Nama', 'Material', 'Po Count', 'GR Count', 'Return Count', 'Po Qty', 'GR Qty', 'Return Qty',  'Return%', 'Return Score', 'On_LT', 'Late_LT', 'LT%', 'LT Score', 'Short_LT', 'Long_LT', 'Avg LT'],
								// cells: function(row, col) {
								// 	var cellProperties = {};

								// 	if (col === 13) {
								// 		cellProperties.renderer = LeadtimePercent; 
								// 	}

								// 	if (col === 14) {
								// 		cellProperties.renderer = LtScore; 
								// 	}
								// 	if (col === 10) {
								// 		cellProperties.renderer = returnScore; 
								// 	}

								// 	if (col === 9) {
								// 		cellProperties.renderer = returnPercent; 
								// 	}

								// 	if (col === 5) {
								// 		cellProperties.renderer = ReturnQty; 
								// 	}

								// 	return cellProperties;
								// }
								
								
								});
							hot.loadData(html)
							notify('200', 'Sukses.', 'success');

					}else{
						hot.loadData(sourceDataObject);
						notify ('404', 'Data Kosong.', 'error');
						console.log(html);
						console.log('kosong')	
					}
					
				}
			});
    }

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

    function render_select2(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_select22(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			multiple: true,
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_selectpo(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			multiple: true,
			placeholder: '--Pilih Opsi--',
		});
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
		box_overlay('out');
    }

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

</script>