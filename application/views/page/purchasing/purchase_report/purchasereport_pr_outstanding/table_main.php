<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="box-body table-responsive no-padding">
<table id="idTable" class="table table-bordered table-striped table-hover display" style="width:100%; font-size:95%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:5%; text-align:center;">Tgl PR</th>
		<th style="width:5%; text-align:center;">No PR</th>
		<th style="width:5%; text-align:center;">RM Kode</th>
		<th style="width:5%; text-align:center;">RM Kode<br>Pronto</th>
		<th style="width:10%; text-align:center;">Deskripsi</th>
		<th style="width:5%; text-align:center;">Qty</th>
		<th style="width:5%; text-align:center;">Satuan</th>
		<th style="width:5%; text-align:center;">Duedate</th>
		<th style="width:3%; text-align:center;">Requested</th>
		<th style="width:10%; text-align:center;">Notes</th>
		<th style="width:5%; text-align:center;">PO / Qty</th>
	</tr>
	</thead>
	<tbody>
	<?php 
	$no = 1;
	foreach ($resultPR as $r) : ?>
	<tr>
		<td><?= $no?></td>
		<td><?= format_date($r['pr_tglinput'], 'Y-m-d')?></td>
		<td><?= $r['pr_no']?></td>
		<td><?= $r['rm_kode']?></td>
		<td><?= $r['rm_oldkd']?></td>
		<td><?= $r['prdetail_nama'].'/'.$r['prdetail_deskripsi'].'/'.$r['prdetail_spesifikasi'] ?></td>
		<td class="dt-right"><?= $r['prdetail_qty']?></td>
		<td><?= $r['rmsatuan_nama']?></td>
		<td><?= format_date($r['prdetail_duedate'], 'Y-m-d')?></td>
		<td><?= $r['nm_admin']?></td>
		<td><?= $r['prdetail_remarks']?></td>
		<td><?php 
			foreach ($resultPO as $rPO) {
				if ($rPO['prdetail_kd'] == $r['prdetail_kd']) {
					echo $rPO['po_no'].' / '.($rPO['podetail_qty'] * $rPO['podetail_konversi']).'<br>';
				}
			}
		?></td>
	</tr>
	<?php 
	$no++;
	endforeach;?>
	</tbody>
</table>
</div>
<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"title" : "Rekap PR Outstanding",
				"exportOptions": {
					"columns": [ 0,1,2,3,4,5,6,7,8,9,10,11 ]
				}
			}],
		});
	}
</script>