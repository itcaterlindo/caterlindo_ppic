<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
extract($rowDataMasterPO);
if($sts == 'edit'){
	extract($rowData);
}

if (!empty($result_relasipopr)) {
	foreach ($result_relasipopr as $each_relasipopr){
		$arraypr_no[] = $each_relasipopr['pr_no'];
		$arraypr_kd[] = $each_relasipopr['pr_kd'];
	}
	$imppr_no = implode(',', $arraypr_no);
	$imppr_kd = implode('_', $arraypr_kd);
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'placeholder' => 'idtxtSts','value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpo_kd', 'name'=> 'txtpo_kd', 'placeholder' => 'idtxtpo_kd' ,'value' => isset($po_kd) ? $po_kd: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtStsDetail', 'name'=> 'txtStsDetail', 'placeholder' => 'idtxtStsDetail', 'class' => 'tt-input'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpodetail_kd', 'name'=> 'txtpodetail_kd', 'placeholder' => 'idtxtpodetail_kd', 'class' => 'tt-input'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtkatalog_kd', 'name'=> 'txtkatalog_kd', 'placeholder' => 'idtxtkatalog_kd' ,'class' => 'tt-input'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtrm_kd', 'name'=> 'txtrm_kd', 'placeholder' => 'idtxtrm_kd' ,'class' => 'tt-input'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtrelasipopr_kd', 'name'=> 'txtrelasipopr_kd', 'placeholder' => 'idtxtrelasipopr_kd', 'value' => isset($imppr_kd) ? $imppr_kd: null ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtprdetail_kd', 'name'=> 'txtprdetail_kd', 'placeholder' => 'idtxtprdetail_kd', 'class' => 'tt-input'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtkd_currency', 'name'=> 'txtkd_currency', 'placeholder' => 'idtxtkd_currency', 'value' => isset($kd_currency) ? $kd_currency : 'MCN271017001'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtcurrency_type', 'name'=> 'txtcurrency_type', 'placeholder' => 'idtxtcurrency_type', 'value' => isset($currency_type) ? $currency_type : 'primary'));

?>

<div class="row invoice-info">
	<div class="col-sm-4 invoice-col">
		<b>PurchaseOrder No :</b> <?php echo isset($po_no) ? $po_no : '-'; ?><br>
		<b>PurchaseOrder Status :</b> <?php echo build_badgecolor($wfstate_badgecolor, $wfstate_nama); ?> <br>
	</div>
	<div class="col-sm-4 invoice-col">
		<b>Suplier :</b> <?php echo isset($suplier_nama) ? $suplier_nama : '-'; ?><br>
		<b>PurchaseRequisition No :</b> <?php echo isset($imppr_no) ? $imppr_no : '-'; ?>
	</div>
	<div class="col-sm-4 invoice-col">
		<b>PurchaseOrder Tanggal :</b> <?php echo format_date($po_tglinput, 'd-m-Y') ?>	</div>
</div>

<hr>

<div class="row">
	<div class="form-group">
		<label for='idtxtRm_jenis' class="col-md-2 control-label">Jenis PO</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrRm_jenis"></div>
			<?php echo form_dropdown('txtRm_kd', $opsiJenisMaterial, isset($MaterialJenisSelected)? $MaterialJenisSelected : '', array('class'=> 'form-control', 'id'=> 'idtxtRm_jenis'));?>
		</div>	
		<button id="idBtnCariUnit" class="btn btn-sm btn-success" onclick="cariUnit()"> <i class="fa fa-search"></i> Cari </button>
	</div>

	<div id="idAdditionalSpecial" class="form-group" style="display: none;">
		<label for='idtxtReferensiSpecial' class="col-md-2 control-label">Referensi Special</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrReferensiSpecial"></div>
			<?php echo form_dropdown('txtRm_kd', $opsiReferensiSpecial, '', array('class'=> 'form-control', 'id'=> 'idtxtReferensiSpecial'));?>
		</div>	
		<button id="idBtnCariSpecial" class="btn btn-sm btn-warning" onclick="cariSpecial()"> <i class="fa fa-search"></i> Referensi Special </button>
	</div>

	<div class="form-group">
		<label for='idtxtpodetail_nama' class="col-md-2 control-label">Nama Item</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpodetail_nama"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtpodetail_nama', 'id'=> 'idtxtpodetail_nama', 'placeholder' =>'Nama Item', 'readonly' => 'true', 'value'=> isset($podetail_nama) ? $podetail_nama: null ));?>
		</div>
	</div>

	<div class="form-group">
		<label for="idtxtpodetail_deskripsi" class="col-md-2 control-label">Deskripsi Item</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrpodetail_deskripsi"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtpodetail_deskripsi', 'id'=> 'idtxtpodetail_deskripsi', 'placeholder' =>'Deskripsi Item', 'rows' => '2', 'value'=> isset($podetail_deskripsi) ? $podetail_deskripsi: null ));?>
		</div>
	</div>

	<div class="form-group">
		<label for="idtxtpodetail_spesifikasi" class="col-md-2 control-label">Spesifikasi Item</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrpodetail_spesifikasi"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtpodetail_spesifikasi', 'id'=> 'idtxtpodetail_spesifikasi', 'placeholder' =>'Spesifikasi Item', 'rows' => '2', 'value'=> isset($podetail_spesifikasi) ? $podetail_spesifikasi: null ));?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtpodetail_jenis' class="col-md-2 control-label">Jenis</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrpodetail_jenis"></div>
			<?php 
			echo form_dropdown('txtpodetail_jenis', $opsiJenisPO, 'material', array('class'=> 'form-control', 'id'=> 'idtxtpodetail_jenis'));?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtpodetail_harga' class="col-md-2 control-label">Harga Unit (<?php echo $currency_nm; ?>)</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrpodetail_harga"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control tt-input', 'name'=> 'txtpodetail_harga', 'id'=> 'idtxtpodetail_harga', 'placeholder' =>'Harga Unit' ,'value'=> isset($podetail_harga) ? $podetail_harga: null ));?>
		</div>
		<?php if ($currency_type != 'primary'):?>
		<label for='idtxtpodetail_konversicurrency' class="col-md-2 control-label">Konversi Currency</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpodetail_konversicurrency"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control tt-input', 'name'=> 'txtpodetail_konversicurrency', 'id'=> 'idtxtpodetail_konversicurrency', 'placeholder' =>'Currency' ,'value'=> isset($podetail_konversicurrency) ? $podetail_konversicurrency: null ));?>
		</div>
		<div class="col-sm-1 col-xs-12">
			<button class="btn btn-sm btn-default" onclick="form_currency('<?php echo $kd_currency; ?>')"> <i class="fa fa-search"></i> Currency </button>
		</div>
		<?php endif;?>
	</div>

	<div class="form-group">
		<label for='idtxtpodetail_qty' class="col-md-2 control-label">Qty</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpodetail_qty"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control tt-input cl-konversi', 'name'=> 'txtpodetail_qty', 'id'=> 'idtxtpodetail_qty', 'placeholder' =>'Qty' ,'value'=> isset($podetail_qty) ? $podetail_qty: null ));?>
		</div>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrmsatuan_kd"></div>
			<?php 
			echo form_dropdown('txtrmsatuan_kd', $opsiSatuan, isset($satuanSelected)? $satuanSelected : '', array('class'=> 'form-control select2', 'id'=> 'idtxtrmsatuan_kd'));?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtpodetail_konversi' class="col-md-2 control-label">Konversi</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpodetail_konversi"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control tt-input cl-konversi', 'name'=> 'txtpodetail_konversi', 'id'=> 'idtxtpodetail_konversi', 'placeholder' =>'Konversi' ,'value'=> isset($podetail_konversi) ? $podetail_konversi: null ));?>
		</div>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpodetail_konversiresult"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control tt-input', 'name'=> 'txtpodetail_konversiresult', 'id'=> 'idtxtpodetail_konversiresult', 'readonly' => 'readonly', 'placeholder' =>'Konversi Result' ,'value'=> isset($podetail_konversiresult) ? $podetail_konversiresult: null ));?>
		</div>	
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrpodetail_satuankonversi"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtpodetail_satuankonversi', 'id'=> 'idtxtpodetail_satuankonversi', 'placeholder' =>'Satuan Konversi', 'readonly' => 'readonly' ));?>
		</div>	
	</div>

	<div class="form-group">
		<label for='idtxtpodetail_tgldelivery' class="col-md-2 control-label">Delivery</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrpodetail_tgldelivery"></div>
			<div class="input-group date">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				<?php echo form_input(array('type'=>'text', 'class'=> 'form-control dateptimepicker tt-input', 'name'=> 'txtpodetail_tgldelivery', 'id'=> 'idtxtpodetail_tgldelivery', 'placeholder' =>'Tgl Delivery' ,'value'=> isset($podetail_tgldelivery) ? $podetail_tgldelivery: null ));?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="idtxtpodetail_remark" class="col-md-2 control-label">Remark</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrpodetail_remark"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtpodetail_remark', 'id'=> 'idtxtpodetail_remark', 'placeholder' =>'Remark', 'rows' => '2', 'value'=> isset($podetail_remark) ? $podetail_remark: null ));?>
		</div>
	</div>

</div>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-4 col-sm-offset-9 col-xs-12">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData()" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button type="reset" name="btnReset" onclick="resetFormMain()" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
			<button onclick="selesai()" class="btn btn-success btn-sm">
				<i class="fa fa-check-circle-o"></i> Selesai
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>
