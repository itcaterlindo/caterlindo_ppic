<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form_master('<?php echo $sts; ?>', '<?php echo $id; ?>', '<?php echo $is_upn; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#<?php echo $box_loader_id; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
		open_table();
		first_load('<?php echo 'idTableBoxLoader'.$master_var; ?>', '<?php echo 'idTableBoxContent'.$master_var; ?>');
	});

	$(document).off('keyup', '.cl-konversi').on('keyup', '.cl-konversi', function() {
		var podetail_qty = $('#idtxtpodetail_qty').val();
		var podetail_konversi = $('#idtxtpodetail_konversi').val();
		$('#idtxtpodetail_konversiresult').val(parseFloat(podetail_qty * podetail_konversi));		
	});

	function open_form_master(sts, id, is_upn){
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/form_master_main'; ?>',
				data: {'sts': sts, 'id' : id, 'is_upn' : is_upn},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					render_select2('select2');
					moveTo('idMainContent');
				}
			});
		});
	}


	function open_form(sts, id) {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/form_main'; ?>',
				data: {'sts': sts, 'id' : id},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					render_select2('select2');
					render_datetimepicker('dateptimepicker');
					moveTo('idMainContent');
				}
			});
		});
	}

	function open_table_detail(sts, id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/table_detail_main'; ?>',
			data: {'sts': sts, 'id' : id},
			success: function(html) {
				$('#<?php echo $box_content_detail_id; ?>').html(html);
			}
		});
	}

	function cariUnit (){
		event.preventDefault();
		// active animate loading
		$('#idBtnCariUnit').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idBtnCariUnit').attr('disabled', true);

		var jns = $('#idtxtRm_jenis').val();
		var id = $('#idtxtpo_kd').val();
		var pr_kd = $('#idtxtrelasipopr_kd').val();
		var url = '';
		
		switch (jns) { 
			case 'KTG': 
				url = '<?php echo base_url().$class_link;?>/table_partial_katalog';
				modalTitle = 'Katalog';
				break;
			case 'PR': 
				url = '<?php echo base_url().$class_link;?>/table_partial_pr';
				modalTitle = 'Purchase Requisition';
				break;
			case 'STD': 
				url = '<?php echo base_url().$class_link;?>/table_partial_material';
				modalTitle = 'Material';
				break;
			case 'NEW': 
				alert('Tidak untuk material Baru');
				return false;
				break;		
			default:
				alert('Pilih dahulu');
				return false;
		}
		
		$.ajax({
			type: 'GET',
			url: url,
			data: {'id' : id, 'pr_kd': pr_kd },
			success: function(html) {
				toggle_modal(modalTitle, html);
				render_datetimepicker('dateptimepicker');
				// deactive animate loading
				$('#idBtnCariUnit').html('<i class="fa fa-search"></i> Cari');
				$('#idBtnCariUnit').attr('disabled', false);
			}
		});
	}

	function form_currency (kd_currency) {
		event.preventDefault();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link;?>/form_currency',
			data: {kd_currency:kd_currency},
			success: function(html) {
				toggle_modal('Currency Fiskal', html);
				render_datetimepicker('datetimepicker');
			}
		});
	}

	function get_currency (idbtn) {
		$('#idbntgetcurrency').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbntgetcurrency').attr('disabled', true);
		let kd_currency = $('#idtxtkd_currency').val();
		let tgl_currency = $('#idtxttglcurrency').val();
		event.preventDefault();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link;?>/get_currency',
			data: {kd_currency:kd_currency, tgl_currency:tgl_currency},
			success: function(data) {
				var resp = JSON.parse(data);
				$('#idtxtcurrencynominal').val(resp.nilai);
				$('#idbntgetcurrency').html('<i class="fa fa-search"></i> Currency');
				$('#idbntgetcurrency').attr('disabled', false);
			},
			error: function(xhr, status, error) {
				var err = eval("(" + xhr.responseText + ")");
				notify ('Error', err.Message, 'error');
				$('#idbntgetcurrency').html('<i class="fa fa-search"></i> Currency');
				$('#idbntgetcurrency').attr('disabled', false);
			}
		});
	}

	function pilihCurrency () {
		event.preventDefault();
		let nilai = $('#idtxtcurrencynominal').val();
		$('#idtxtpodetail_konversicurrency').val(nilai);
		toggle_modal('', '');
	}

	function cariSpecial(){
		event.preventDefault();
		// active animate loading
		$('#idBtnCariSpecial').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idBtnCariSpecial').attr('disabled', true);

		var jns = $('#idtxtReferensiSpecial').val();
		var id = $('#idtxtpo_kd').val();
		var url = '';
		
		switch (jns) { 
			case 'STD': 
				url = '<?php echo base_url().$class_link;?>/table_partial_material';
				modalTitle = 'Referensi Material';
				break;
			case 'SPECIAL': 
				url = '<?php echo base_url().$class_link;?>/table_partial_special';
				modalTitle = 'Referensi Material';
				break;	
			default:
				alert('Pilih dahulu');
				return false;
		}
		
		$.ajax({
			type: 'GET',
			url: url,
			data: {'id' : id },
			success: function(html) {
				toggle_modal(modalTitle, html);
				render_datetimepicker('dateptimepicker');
				// deactive animate loading
				$('#idBtnCariSpecial').html('<i class="fa fa-search"></i> Cari Referensi');
				$('#idBtnCariSpecial').attr('disabled', false);
			}
		});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function selectRM (po_kd, rm_kd){
		event.preventDefault();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/getMaterial'; ?>',
			data: {'id' : rm_kd, 'po_kd' : po_kd},
			success: function(data) {
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$('#idtxtStsDetail').val('add');					
					$('#idtxtrm_kd').val(resp.data.rm_kd);
					$('#idtxtpodetail_nama').val(resp.data.rm_nama);
					$('#idtxtrmsatuan_kd').val(resp.data.rmsatuan_kd).trigger('change');
					$('#idtxtpodetail_satuankonversi').val(resp.data.satuan_secondary_nama);
					$('#idtxtpodetail_deskripsi').val(resp.data.rm_deskripsi);
					$('#idtxtpodetail_spesifikasi').val(resp.data.rm_spesifikasi);
					$('#idtxtkatalog_kd').val(resp.data.katalog_kd);
					$('#idtxtpodetail_harga').val(resp.data.katalog_harga);
					$('#idtxtpodetail_konversi').val(resp.data.katalog_konversi);
					if (resp.data.katalog_konversi == null){
						$('#idtxtpodetail_konversi').val(resp.data.rm_konversiqty);
					}
					if (resp.data.rm_kd == 'SPECIAL') {
						$('#idtxtpodetail_nama').prop('readonly', false);
						$('#idtxtpodetail_deskripsi').prop('readonly', false);
						$('#idtxtpodetail_spesifikasi').prop('readonly', false);
					}
					toggle_modal('', '');
					moveTo('idMainContent');
				}else{
					notify ('Error', resp.pesan, 'error');
					toggle_modal('', '');

				}
			}
		});
	}

	function selectPRdetail (row){
		event.preventDefault();
		var prdetail_kd = row.getAttribute("data-prdetailkd");
		var prdetail_qty = row.getAttribute("data-prdetailqty");
		var po_kd = $('#idtxtpo_kd').val();

		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/getDetailPR'; ?>',
			data: {'id' : prdetail_kd, 'po_kd': po_kd},
			success: function(data) {
				var resp = JSON.parse(data);
				console.log(resp);
				if (resp.code == 200){
					// change attribut on spesial item
					if (resp.data.rm_kd == 'SPECIAL'){
						$('#idtxtpodetail_nama').prop('readonly', false);
						$('#idAdditionalSpecial').show();
					}
					$('#idtxtStsDetail').val('add');
					$('#idtxtprdetail_kd').val(resp.data.prdetail_kd);					
					$('#idtxtrm_kd').val(resp.data.rm_kd);
					$('#idtxtpodetail_nama').val(resp.data.prdetail_nama);
					$('#idtxtrmsatuan_kd').val(resp.data.rmsatuan_kd).trigger('change');
					$('#idtxtpodetail_satuankonversi').val(resp.data.default_rmsatunnama);
					$('#idtxtpodetail_deskripsi').val(resp.data.prdetail_deskripsi);
					$('#idtxtpodetail_spesifikasi').val(resp.data.prdetail_spesifikasi);
					$('#idtxtpodetail_harga').val(resp.data.katalog_harga);
					$('#idtxtpodetail_qty').val(prdetail_qty);
					$('#idtxtpodetail_konversi').val(resp.data.katalog_konversi);
					$('#idtxtkatalog_kd').val(resp.data.katalog_kd);
					$('#idtxtpodetail_tgldelivery').val(resp.data.formatDuedate);
					$('#idtxtpodetail_remark').val(resp.data.prdetail_remarks);
					var konversi = resp.data.katalog_konversi;
					if (resp.data.katalog_konversi == null){
						$('#idtxtpodetail_konversi').val(resp.data.prdetail_konversi);
						konversi = resp.data.prdetail_konversi;
					}
					$('#idtxtpodetail_konversiresult').val(parseFloat(resp.data.prdetail_qty * konversi));
					toggle_modal('', '');
					moveTo('idMainContent');
				}else{
					notify ('Error', resp.pesan, 'error');
					toggle_modal('', '');
				}
			}
		});
	}

	function selectSpecial(row){
		event.preventDefault();
		$('#idtxtrm_kd').val('SPECIAL');
		$('#idtxtpodetail_nama').val(row.getAttribute("data-podetailnama"));
		$('#idtxtpodetail_deskripsi').val(row.getAttribute("data-podetaildeskripsi"));
		$('#idtxtpodetail_spesifikasi').val(row.getAttribute("data-podetailspesifikasi"));
		$('#idtxtpodetail_konversi').val(row.getAttribute("data-podetailkonversi"));
		$('#idtxtpodetail_harga').val(row.getAttribute("data-podetailharga"));
		$('#idtxtrmsatuan_kd').val(row.getAttribute("data-podetailsatuan"));
		toggle_modal('', '');	
	}

	function edit_data_detail(id){
		var rm_kd = '';
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/getDetailPO'; ?>',
			data: {'id' : id},
			success: function(data) {
				var resp = JSON.parse(data);console.log(resp);
				if (resp.code == 200){
					rm_kd = resp.data.rm_kd;
					if (rm_kd == 'SPECIAL'){
						$('#idtxtpodetail_nama').prop('readonly', false);
						$('#idAdditionalSpecial').show();
					}
					$('#idtxtpodetail_kd').val(resp.data.podetail_kd);
					$('#idtxtkatalog_kd').val(resp.data.katalog_kd);
					$('#idtxtStsDetail').val('edit');
					$('#idtxtpodetail_nama').val(resp.data.podetail_nama);
					$('#idtxtrm_kd').val(rm_kd);
					$('#idtxtrmsatuan_kd').val(resp.data.podetail_rmsatuan_kd).trigger('change');
					$('#idtxtpodetail_satuankonversi').val(resp.data.rmsatuan_namadefault);
					$('#idtxtpodetail_deskripsi').val(resp.data.podetail_deskripsi);
					$('#idtxtpodetail_spesifikasi').val(resp.data.podetail_spesifikasi);
					$('#idtxtpodetail_jenis').val(resp.data.podetail_jenis).trigger('change');
					$('#idtxtpodetail_harga').val(resp.data.podetail_harga);
					$('#idtxtpodetail_konversicurrency').val(resp.data.podetail_konversicurrency);
					$('#idtxtpodetail_qty').val(resp.data.podetail_qty);
					$('#idtxtpodetail_konversi').val(resp.data.podetail_konversi);
					$('#idtxtpodetail_tgldelivery').val(resp.data.podetail_tgldeliveryFormat);
					$('#idtxtpodetail_remark').val(resp.data.podetail_remark);
					moveTo('idMainContent');
				}
			}
		});
	}

	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
		boxOverlayForm('out');
	}

	function submitData() {
		boxOverlayForm('in');
		event.preventDefault();
		var form = document.getElementById('idFormInput');
		// edit atau add
		var sts =  $('#idtxtStsDetail').val();
		url = "<?php echo base_url().$class_link; ?>/action_insert";
		if (sts == 'edit'){
			url = "<?php echo base_url().$class_link; ?>/action_update";
		}

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					var id = $('#idtxtpo_kd').val();
					open_table_detail(sts, id)
					// $('#<?php //echo $box_id; ?>').remove();
					$('.errInput').html('');
					resetFormMain();
					$('#idtxtRm_jenis').focus();
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					boxOverlayForm('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function submitDataMaster(){
		event.preventDefault();
		var form = document.getElementById('idFormInputMaster');
		// edit atau add
		let sts =  $('#idtxtStsMaster').val();
		let url = "<?php echo base_url().$class_link; ?>/action_master_insert";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				console.log(resp);
				console.log(<?php echo $is_upn; ?>);
				if(resp.code == 200 && <?php echo $is_upn; ?> == 1){
					notify (resp.status, resp.pesan, 'sukses');
					selesai();
					$('.errInput').html('');
				}else if(resp.code == 200 && <?php echo $is_upn; ?> == 0){
					open_form(sts, resp.data.po_kd);
					open_table_detail(sts, resp.data.po_kd);
					$('.errInput').html('');
				}
				else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function submitKatalog (){
		event.preventDefault();
		var sts = $('#idtxtSts').val();
		var id = $('#idtxtpo_kd').val();
		var form = document.getElementById('idFormKatalog');
		url = "<?php echo base_url().$class_link; ?>/action_katalog_insert";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					open_table_detail(sts, id);
					toggle_modal('', '');
					$('.errInput').html('');
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function resetFormMain(){
		event.preventDefault();
		$('.tt-input').val('');
		$('#idtxtrmsatuan_kd').val('').trigger('change');
		$('#idtxtpodetail_nama').prop('readonly', true);
		$('#idAdditionalSpecial').hide();
		$('#idtxtpodetail_jenis').val('material').trigger('change');
	}

	function boxOverlayForm(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

	function selesai () {
		event.preventDefault();
		window.location.reload();
	}
</script>