<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$form_id = 'idFormKatalog';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpo_kdKatalog', 'name'=> 'txtpo_kdKatalog', 'value' => $id ));
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<div class="col-md-12">
    <table id="idTableKatalog" class="table table-bordered table-striped table-hover" style="width:100%;">
        <thead>
        <tr>
            <th style="width:1%; text-align:center;"><input type="checkbox" id="check_all" ></th>
            <th style="width:30%; text-align:center;">Nama Material</th>
            <th style="width:10%; text-align:center;">Harga</th>
            <th style="width:7%; text-align:center;">Konversi</th>
            <th style="width:7%; text-align:center;">Qty</th>
            <th style="width:7%; text-align:center;">Delivery</th>
            <th style="width:7%; text-align:center;">Last Update</th>
            <th style="width:7%; text-align:center; display: none;">rm_kd</th>
            <th style="width:7%; text-align:center; display: none;">rm_nama</th>
            <th style="width:7%; text-align:center; display: none;">rm_deskripsi</th>
        </tr>
        </thead>
        <tbody>
        <?php 
        $no = 1;
        foreach ($resultData as $row): ?>
            <tr>
                <td style="text-align:center;"><input type="checkbox" class="data-check" name="txtkatalog_kd[]" value="<?php echo $row['katalog_kd'];?>"></td>
                <td><?php echo $row['rm_kode'];?></td>
                <td> <input type="number" name="txtHarga[<?php echo $row['katalog_kd'];?>]" placeholder="Harga" value="<?php echo $row['katalog_harga']; ?>"> </td>
                <td> <input type="number" name="txtKonversi[<?php echo $row['katalog_kd'];?>]" placeholder="Konversi" value="<?php echo $row['katalog_konversi']; ?>"> </td>
                <td> <input type="number" name="txtQty[<?php echo $row['katalog_kd'];?>]" placeholder="Qty" value=""> </td>
                <td> <input type="text" name="txtDelivery[<?php echo $row['katalog_kd'];?>]" class="dateptimepicker" placeholder="Delivery" value=""> </td>
                <td><?php echo format_date($row['katalog_tgledit'], 'd-m-Y H:i:s');?></td>
                <td style="display: none;"> <input type="text" name="txtRm_kd[<?php echo $row['katalog_kd'];?>]" placeholder="Delivery" value="<?php echo $row['rm_kd']; ?>"> </td>
                <td style="display: none;"> <input type="text" name="txtRm_nama[<?php echo $row['katalog_kd'];?>]" placeholder="Delivery" value="<?php echo $row['rm_nama']; ?>"> </td>
                <td style="display: none;"> <input type="text" name="txtRm_deskripsi[<?php echo $row['katalog_kd'];?>]" placeholder="Delivery" value="<?php echo $row['rm_deskripsi']; ?>"> </td>
            </tr>
        <?php 
            $no++;
        endforeach;?>
        </tbody>
    </table>
    <hr>
</div>

<div class="col-md-12">
    <div class="col-sm-1 col-xs-12 col-sm-offset-10">
        <button type="submit" onclick="submitKatalog()" class="btn btn-success btn-sm"> <i class="fa fa-plus"></i> Tambah Data </button>
    </div>
</div>
<?php echo form_close(); ?>
<script type="text/javascript">
	$("#check_all").click(function () {
        $(".data-check").prop('checked', $(this).prop('checked'));
    });

	render_dt('#idTableKatalog');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"paging": false,
            "searching": true,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
            "columnDefs": [
                {"searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
            ],
		});
	}

</script>