<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$form_id = 'idFormPR';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpo_kdPR', 'name'=> 'txtpo_kdPR', 'value' => $id ));
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<div class="col-md-12">
    <table id="idTablePR" class="table table-bordered table-striped table-hover" style="width:100%; font-size:90%;">
        <thead>
        <tr>
            <th style="width:7%; text-align:center;">Kode</th>
            <th style="width:5%; text-align:center;">No PR</th>
            <th style="width:30%; text-align:center;">Deskripsi PR</th>
            <th style="width:7%; text-align:center;">Duedate PR</th>
            <th style="width:5%; text-align:center;">Qty PR</th>
            <th style="width:5%; text-align:center;">Satuan</th>
            <th style="width:5%; text-align:center;">Aksi</th>
        </tr>
        </thead>
        <tbody>
        <?php 
        $no = 1;
        foreach ($resultPRdetails as $row): ?>
            <?php 
                $prdetail_qty = $row['prdetail_qty'];
                foreach($resultPOdetails as $eachresultPOdetail){
                    if ($eachresultPOdetail['prdetail_kd']  == $row['prdetail_kd']){
                        $prdetail_qty = (float) $prdetail_qty - $eachresultPOdetail['podetail_qty'];
                    }
                }
                if ($prdetail_qty <= 0){
                    continue;
                }
            ?>
            <tr>
                <td><?php echo $row['rm_kode'];?></td>
                <td><?php echo $row['pr_no'];?></td>
                <td><?php echo $row['prdetail_deskripsi'].'/'.$row['prdetail_spesifikasi'] ;?></td>
                <td><?php echo format_date($row['prdetail_duedate'], 'd-m-Y');?></td>
                <?php 
                    echo '<td style="text-align:right;">'.number_format((float)$prdetail_qty, 2, '.', '').'</td>';
                ?>
                <td><?php echo $row['rmsatuan_nama'];?></td>                
                <td style="text-align:center;"> <button class="btn btn-xs btn-success" onclick="selectPRdetail(this)" data-prdetailkd="<?php echo $row['prdetail_kd']?>" data-prdetailqty="<?php echo number_format((float)$prdetail_qty, 2, '.', ''); ?>"><i class="fa fa-check-circle"></i> Pilih</button> </td>
            </tr>
        <?php 
            $no++;
        endforeach;?>
        </tbody>
    </table>
    <hr>
</div>

<?php echo form_close(); ?>

<script type="text/javascript">

	render_dt('#idTablePR');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"paging": true,
            "searching": true,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
            "columnDefs": [
                {"searchable": true, "orderable": true, "className": "dt-center", "targets": 5},
            ],
		});
	}

</script>