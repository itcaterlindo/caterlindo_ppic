<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputMaster';
if($sts == 'edit'){
	extract($rowData);
	$SuplierSelected = $suplier_kd;
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtStsMaster', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpo_kd', 'name'=> 'txtpo_kd', 'value' => isset($po_kd) ? $po_kd: null ));


?>
<div class="row">
	<div class="form-group idnya_no_po" id="idnya_no_po">
		<label for="idtxtpo_no" class="col-md-1 control-label">No PO</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrpo_no"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control dateptimepicker tt-input', 'name'=> 'txtpo_no', 'id'=> 'idtxtpo_no', 'placeholder' =>'No PO' ,'value'=> isset($po_nomor) ? $po_nomor: null ));?>
		</div>
	</div>
	
    <div class="form-group" id="idnya_suplier">
		<label for='idtxtsuplier_kd' class="col-md-1 control-label">Suplier</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrsuplier_kd"></div>
			<?php echo form_dropdown('txtsuplier_kd', $opsiSuplier, isset($SuplierSelected)? $SuplierSelected : '', array('class'=> 'form-control select2', 'id'=> 'idtxtsuplier_kd'));?>
		</div>
	</div>

    <div class="form-group" id="idnya_no_pr">
		<label for='idtxtpr_kd' class="col-md-1 control-label">No PR</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpr_kd"></div>
			<?php echo form_multiselect('txtpr_kd[]', $opsiPR, isset($PRSelected)? $PRSelected : '', array('class'=> 'form-control select2', 'id'=> 'idtxtpr_kd', 'multiple' => 'multiple'));?>
		</div>
	</div>

    <div class="form-group">
		<label for="idtxtpo_note" class="col-sm-1 control-label">Notes</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrpo_note"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtpo_note', 'id'=> 'idtxtpo_note', 'placeholder' =>'Notes', 'rows' => '2', 'value'=> isset($po_note) ? $po_note: null ));?>
		</div>
	</div>

</div>

<hr>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-1 col-xs-12 col-sm-offset-10">
			<button type="submit" name="btnSubmit" onclick="submitDataMaster()" class="btn btn-primary btn-sm">
				<i class="fa fa-arrow-circle-right"></i> Simpan
			</button>
		</div>
	</div>
</div>


<script>
	if('<?php echo $is_upn; ?>' == 1){
		$("#idnya_suplier").hide();
		$("#idnya_no_po").hide();
		$("#idnya_no_pr").hide();
	}
</script>

<?php echo form_close(); ?>
