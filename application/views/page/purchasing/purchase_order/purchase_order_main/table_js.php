<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	var is_upn = 0;
	open_table('<?php echo $params; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('add', '', 0);
	});

	$(document).off('keyup', '#idpr_no').on('keyup', '#idpr_no', function() {
		let pr_no = $('#idpr_no').val();
		check_pr_no (pr_no)
		
	});

	function check_pr_no (pr_no = null) {
		if (pr_no){
			$('.cl-filter').prop('disabled', true);
		}else{
			$('.cl-filter').prop('disabled', false);
		}
	}

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table(params = null) {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_main'; ?>',
				data: {params: params},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					render_select2('select2');
					let pr_no = $('#idpr_no').val();
					check_pr_no (pr_no);
					moveTo('idMainContent');
				}
			});
		});
	}

	function view_detail_box(po_kd) {
		event.preventDefault();
		$('#idDetailBox<?php echo $master_var; ?>').remove();
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/view_detail_box'; ?>',
			data: {'po_kd' : po_kd},
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function render_select2(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_datetimepicker(valClass){
		$('.'+valClass).datetimepicker({
			format: 'DD-MM-YYYY',
    	});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function remove_box() {
		$('#idFormBox<?php echo $master_var; ?>').remove();
	}

	function open_form_box(sts, id, is_upn) {
		remove_box();
		$('#<?php echo $box_alert_id; ?>').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_box'; ?>',
			data: {'sts': sts, 'id' : id, 'is_upn' : is_upn},
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}



	function edit_data(id){
		open_form_box('edit', id, 0) 
	}

	function lihat_data(id){
		var url = '<?php echo base_url().$detail_class_link?>?id='+id;
		window.location.assign(url);	
	}

	function push_to_sap(id){
		var conf = confirm('Apakah anda yakin Push item ini ?');
		$.blockUI({ overlayCSS: { backgroundColor: '#C0C0C0' } });
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/push_to_sap'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						sendToSAP(resp.data);
						notify (resp.status, resp.pesan, 'success');
						open_table();
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				}
			});
		}
	}
	function sendToSAP(items){
		
		var itm_detail = [];

		for (let i = 0; i < items.resultPOdetail.length; i++) {
			itm_detail.push({
							"U_IDU_WEBID": (items.resultPOdetail[i].podetail_kd) ? items.resultPOdetail[i].podetail_kd : "",
							"ItemCode": (items.resultPOdetail[i].rm_kode) ? items.resultPOdetail[i].rm_kode : "",
							"Dscription": (items.resultPOdetail[i].podetail_deskripsi) ? items.resultPOdetail[i].podetail_deskripsi : "",
							"U_IDU_Spesifikasi": (items.resultPOdetail[i].podetail_spesifikasi) ? items.resultPOdetail[i].podetail_spesifikasi : "",
							"KdCurrency": (items.resultPOdetail[i].kd_currency) ? items.resultPOdetail[i].kd_currency : "",
							"Quantity": (items.resultPOdetail[i].podetail_qty) ? items.resultPOdetail[i].podetail_qty : "",
							"Konversi": (items.resultPOdetail[i].podetail_konversi) ? items.resultPOdetail[i].podetail_konversi : "",
							"UomName": (items.resultPOdetail[i].rmsatuan_nama) ? items.resultPOdetail[i].rmsatuan_nama : "",
							"UnitPrice": (items.resultPOdetail[i].podetail_harga) ? items.resultPOdetail[i].podetail_harga  : "",
							"LineTotal": (items.resultPOdetail[i].podetail_jmlharga) ? items.resultPOdetail[i].podetail_jmlharga : "",
							"ShipDate": (items.resultPOdetail[i].podetail_tgldelivery) ? items.resultPOdetail[i].podetail_tgldelivery : "",
						 })
		}

		var dataToSAP = {
						"U_IDU_WEBID": (items.rowPO.po_kd) ? items.rowPO.po_kd : "",
						"U_IDU_PO_INTNUM": items.rowPO.po_no ? items.rowPO.po_no : "",
						"U_IDU_PR_INTNUM": items.imppr_no ? items.imppr_no : "",
						"NumAtCard": items.rowPO.po_no ? items.rowPO.po_no : "",
						"DocDate": items.rowPO.po_tanggal ? items.rowPO.po_tanggal : "",
						"Comments": "",
						"CardCode": items.rowPO.suplier_kode ? items.rowPO.suplier_kode : "",
						"U_IDU_WEBUSER": items.rowPO.nm_admin ? items.rowPO.nm_admin : "",
						"Lines_Detail_Item": itm_detail
		}

		fetch("<?= url_api_sap() ?>AddPurchaseOrder", {
		method: 'POST', // or 'PUT'
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(dataToSAP),
		})
		.then((response) => response.json())
		.then((data) => {
			console.log('Success:', data);
			if(data.ErrorCode == 0 ){
				console.log(dataToSAP);
				console.log(items);
				console.log(data.Message);
				// open_table();
				notify ('200! Sukses.', data.Message, 'success');
				$.unblockUI()
				window.location.reload();
			}else{
				console.log(dataToSAP);
				console.log(items);
				console.log(data.Message);
				delRollback(items.rowPO.po_kd)
				$.unblockUI()
				notify (data.ErrorCode + '. Gagal!', data.Message, 'error');
			}
		})
		.catch((error) => {
			$.unblockUI()
			delRollback(items.rowPO.po_kd)
			notify ('000. Gagal!', error, 'error');
		});
	}

	function delRollback(id){
		$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_rollback_po'; ?>',
				data: 'po_kd='+id,
				success: function(data) {
					console.log(data);
					// var resp = JSON.parse(data);
					// if(resp.code == 200){
						notify ('xxx!', 'Save SAP Failed. Rollback Success.', 'success');
						// window.location.reload();
					// 	open_table();
					// }else if (resp.code == 400){
					// 	notify (resp.status, resp.pesan, 'error');
					// 	generateToken (resp.csrf);
					// }else{
					// 	notify ('Error', 'Error tidak Diketahui', 'error');
					// 	generateToken (resp.csrf);
					// }
				}
			});
	}

	function cetak_data(id){
		var url = '<?php echo base_url().'purchasing/purchase_order/purchase_order_report'?>?id='+id;
		window.open(url, '_blank', 'width=1024,height=768,scrollbars=yes,menubar=no,status=yes,resizable=yes,screenx=0,screeny=0');
	}
	function update_note(id){
		is_upn = 1;
		open_form_box('edit', id, is_upn)
	}

	function hapus_data(id) {
		var conf = confirm('Apakah anda yakin menghapus item ini ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						open_table();
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				}
			});
		}
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
		box_overlay('out');
    }

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

</script>