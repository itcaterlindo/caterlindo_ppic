<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$form_id = 'idFormSpecial';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpo_kdSpecial', 'name'=> 'txtpo_kdSpecial', 'value' => $id ));
// echo json_encode($resultData);die();
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<div class="col-md-12 table-responsive">
    <table id="idTableSpecial" class="table table-hover table-striped table-bordered" style="width:100%; font-size:90%;">
        <thead>
        <tr>
            <th style="width:2%; text-align:center;">Kode</th>
            <th style="width:5%; text-align:center;">Nama Material</th>
            <th style="width:15%; text-align:center;">Deskripsi</th>
            <th style="width:15%; text-align:center;">Spesifikasi</th>
            <th style="width:3%; text-align:center;">Aksi</th>
        </tr>
        </thead>
        <tbody>
        <?php 
        $no = 1;
        foreach ($resultData as $row): ?>
            <tr>
                <td><?php echo $row['rm_kd'];?></td>
                <td><?php echo $row['podetail_nama'];?></td>
                <td><?php echo $row['podetail_deskripsi'];?></td>
                <td><?php echo $row['podetail_spesifikasi'];?></td>
                <td style="text-align:center;"> 
                    <button class="btn btn-xs btn-success" onclick="selectSpecial(this)" 
                    data-podetailnama="<?php echo $row['podetail_nama'];?>" 
                    data-podetaildeskripsi="<?php echo $row['podetail_deskripsi'];?>" 
                    data-podetailspesifikasi="<?php echo $row['podetail_spesifikasi'];?>"
                    data-podetailkonversi="<?php echo $row['podetail_konversi'];?>" 
                    data-podetailharga="<?php echo $row['podetail_harga'];?>" 
                    data-podetailsatuan="<?php echo $row['rmsatuan_kd'];?>" 
                    >
                    <i class="fa fa-check-circle"></i> Pilih</button> 
                </td>
            </tr>
        <?php 
            $no++;
        endforeach;?>
        </tbody>
    </table>
    <hr>
</div>

<?php echo form_close(); ?>

<script type="text/javascript">

	render_dt('#idTableSpecial');

	function render_dt(table_elem) {
        // Setup - add a text input to each footer cell
        $(table_elem+' thead tr').clone(true).appendTo( table_elem+' thead' );
        $(table_elem+' thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Cari '+title+'" />' );
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            });
        });

		var table = $(table_elem).DataTable({
			"paging": true,
            "searching": true,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
            "columnDefs": [
                {"searchable": true, "orderable": true, "targets": 0},
                {"searchable": true, "orderable": false, "className": "dt-center", "targets": 4},
            ],
		});
	}

</script>