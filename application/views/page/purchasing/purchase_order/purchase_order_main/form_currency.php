<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputCurrency';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtkd_currency', 'name'=> 'txtkd_currency', 'placeholder' => 'idtxtkd_currency', 'value' => isset($kd_currency) ? $kd_currency : 'MCN271017001'));
?>

<div class="row">

    <div class="form-group">
        <label for='idtxttglcurrency' class="col-md-2 control-label">Tanggal Currency</label>
        <div class="col-md-2 col-xs-12">
            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="text" name="txttglcurrency" value="<?php echo date('d-m-Y')?>" id="idtxttglcurrency" class="form-control datetimepicker" placeholder="Tanggal Currency">
            </div>
        </div>
        <div class="col-sm-1 col-xs-12">
            <button class="btn btn-sm btn-default" id="idbntgetcurrency" onclick="get_currency('idbntgetcurrency')"> <i class="fa fa-search"></i> Currency </button>
		</div>
    </div>
	
	<div class="form-group">
		<label for='idtxtcurrencynominal' class="col-md-2 control-label">Currency Fiskal</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrcurrencynominal"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtcurrencynominal', 'id'=> 'idtxtcurrencynominal', 'placeholder' =>'Currency', 'value'=> null ));?>
		</div>
		
	</div>

</div>

<div class="form-group">
	<div class="col-sm-3 col-sm-offset-2 col-xs-12">
		<button name="btnSubmit" id="idbtnSubmitMain" onclick="pilihCurrency()" class="btn btn-default btn-sm">
			<i class="fa fa-check"></i> Pilih
		</button>
	</div>
</div>
<?php echo form_close(); ?>
