<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
if (!empty($rowPO)){
	extract ($rowPO);
}

if (!empty($result_relasipopr)) {
	foreach ($result_relasipopr as $each_relasipopr){
		$arraypr_no[] = $each_relasipopr['pr_no'];
		$arraypr_kd[] = $each_relasipopr['pr_kd'];
	}
	$imppr_no = implode(',', $arraypr_no);
}
?>

<div class="row invoice-info">
	<div class="col-sm-4 invoice-col">
		<b>PurchaseOrder No :</b> <?php echo isset($po_no) ? $po_no : '-'; ?><br>
		<div class="no-print">
			<b>PurchaseOrder Status :</b>  <?php echo isset($po_statusLabel) ? $po_statusLabel : '-'; ?> <br>
		</div>
	</div>
	<div class="col-sm-4 invoice-col">
		<b>Suplier :</b> <?php echo isset($suplier_nama) ? $suplier_nama : '-'; ?><br>
		<b>PurchaseRequisition No :</b> <?php echo isset($imppr_no) ? $imppr_no : '-'; ?>
	</div>
	<div class="col-sm-4 invoice-col">
		<b>PurchaseOrder Tanggal :</b> <?php echo format_date($po_tglinput, 'd-m-Y'); ?>	</div>
</div>

<hr>

<div class="row">
	<div class="col-md-12">
	<table id="idTable" class="table table-bordered table-striped table-hover display table-responsive nowrap" style="width:100%;">
		<thead>
			<tr>
				<th style="width:1%; text-align:center;" class="all">No.</th>
				<th style="width:5%; text-align:center;">Kode</th>
				<th style="width:7%; text-align:center;">Nama Produk</th>
				<th style="width:10%; text-align:center;">Deskripsi</th>
				<th style="width:7%; text-align:center;">Qty</th>
				<th style="width:7%; text-align:center;">Unit</th>
				<th style="width:7%; text-align:center;">Duedate</th>
				<th style="width:7%; text-align:center;">Remarks</th>
			</tr>
		</thead>

	<tbody>
	<?php 
	if (!empty($resultPRdetail)) :
		foreach($resultPRdetail as $element):
			$groupByDelivery[$element['pr_no']] [] = $element;
		endforeach;

		foreach ($groupByDelivery as $pr_no => $e):
		?>
			<tr> <td colspan="8" style="background-color:gray;"> <strong> <?php echo 'No PR : '.$pr_no; ?> </strong> </td> </tr>
		<?php
			$no = 1;
			foreach ($e as $r):
			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $r['rm_kode']; ?></td>
				<td><?php echo $r['prdetail_nama']; ?></td>
				<td><?php echo $r['prdetail_deskripsi'].'; '.$r['prdetail_spesifikasi']; ?></td>
				<td style="text-align: right;"><?php echo $r['prdetail_qty']; ?></td>
				<td><?php echo $r['rmsatuan_nama']; ?></td>
				<td><?php echo $r['prdetail_duedate']; ?></td>
				<td><?php echo $r['prdetail_remarks']; ?></td>
			</tr>
			<?php
				$no++;
			endforeach;
		endforeach;
	?>
		<tr>
			<td></td>
		</tr>
	<?php 
	endif;?>
	</tbody>

	</div>
</div>
