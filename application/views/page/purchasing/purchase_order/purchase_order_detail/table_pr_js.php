<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table('<?php echo $id; ?>', '<?php echo $batch_pr; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#<?php echo $box_loader_id; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
		first_load('<?php echo 'idTableBoxLoader'.$master_var; ?>', '<?php echo 'idTableBoxContent'.$master_var; ?>');
	});

	function open_table(id, batch_pr){
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_pr_main'; ?>',
				data: {'id' : id, 'batch_pr': batch_pr},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

</script>