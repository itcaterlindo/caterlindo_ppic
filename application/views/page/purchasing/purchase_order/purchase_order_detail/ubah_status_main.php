<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputStatus';

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpo_kd', 'name'=> 'txtpo_kd', 'value' => $po_kd ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtpostatus_level', 'name'=> 'txtpostatus_level', 'value' => isset($postatus_level) ? $postatus_level: null ));

?>

<div class="col-md-12">

    <?php if ($akses == 'full') :
        $POStatusSelected = $postatus_level;
    ?>
    <div class="form-group">
		<label for='idtxtpostatus_kd' class="col-md-2 control-label">Status</label>
		<div class="col-sm-5 col-xs-12">
			<div class="errInput" id="idErrpostatus_kd"></div>
			<?php echo form_dropdown('txtpostatus_kd', $opsiPOstatus, isset($POStatusSelected)? $POStatusSelected : '', array('class'=> 'form-control', 'id'=> 'idtxtpostatus_kd'));?>
		</div>	
	</div>
<?php endif;?>
    <div class="form-group">
        <label for="idtxtpologstatus_keterangan" class="col-md-2 control-label">Keterangan</label>
        <div class="col-sm-10 col-xs-12">
            <div class="errInput" id="idErrpologstatus_keterangan"></div>
            <?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtpologstatus_keterangan', 'id'=> 'idtxtpologstatus_keterangan', 'placeholder' =>'Keterangan', 'rows' => '3', 'value'=> isset($pologstatus_keterangan) ? $pologstatus_keterangan: null ));?>
        </div>
    </div>

</div>

<div class="col-md-12">
	<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitStatus()" class="btn btn-primary btn-sm pull-right">
		<i class="fa fa-save"></i> Simpan
	</button>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
    $('#idtxtpostatus_kd').change(function(){
        $('#idtxtpostatus_level').val(this.value);
	});
</script>