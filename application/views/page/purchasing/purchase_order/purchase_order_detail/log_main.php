<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
?>

<div class="col-md-12">
    <ul class="timeline">
        <li>
            <i class="fa fa-user bg-aqua"></i>

            <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>

            <h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your friend request</h3>
            </div>
        </li>
    </ul>            
</div>