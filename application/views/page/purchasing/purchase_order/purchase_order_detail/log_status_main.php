<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
?>

<?php foreach ($resultData as $r) : ?>

<ul class="timeline">
    <li>
        <i class="fa fa-comments bg-yellow"></i>

        <div class="timeline-item">
        <span class="time"><i class="fa fa-clock-o"></i> <?= $r['pologstatus_tglinput'];?></span>

        <h3 class="timeline-header"> <strong> <?= $r['nm_admin']; ?> </strong> mengubah ke <?= $r['postatus_nama'];?> </h3>

        <div class="timeline-body"><?= $r['pologstatus_keterangan'];?></div>
        </div>
    </li>
</ul> 

<?php endforeach;?>