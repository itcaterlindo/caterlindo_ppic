<?php
defined('BASEPATH') or exit('No direct script access allowed!');
if (!empty($rowPO)){
	extract($rowPO);
}

if (!empty($result_relasipopr)) {
	foreach ($result_relasipopr as $each_relasipopr){
		$arraypr_no[] = $each_relasipopr['pr_no'];
		$arraypr_kd[] = $each_relasipopr['pr_kd'];
	}
	$imppr_no = implode(',', $arraypr_no);
	$imppr_kd = implode('_', $arraypr_kd);
}

?>

<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
	table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
</style>

<div class="row invoice-info">
	<div class="col-sm-4 invoice-col">
		<b>PurchaseOrder No :</b> <?php echo isset($po_no) ? $po_no : '-'; ?><br>
		<div class="no-print">
			<b>PurchaseOrder Status :</b>  <?php echo build_badgecolor($wfstate_badgecolor, $wfstate_nama); ?> <br>
		</div>
	</div>
	<div class="col-sm-4 invoice-col">
		<b>Suplier :</b> <?php echo isset($suplier_nama) ? $suplier_nama : '-'; ?><br>
		<b>PurchaseRequisition No :</b> <?php echo isset($imppr_no) ? $imppr_no : '-'; ?>
	</div>
	<div class="col-sm-4 invoice-col">
		<b>PurchaseOrder Tanggal :</b> <?php echo format_date($po_tglinput, 'd-m-Y'); ?> <br>
		<b>Selesai :</b> <?php echo choice_enum($po_closeorder); ?>	</div>
</div>

<hr>

<table id="idTable" class="table table-bordered table-striped table-hover display" style="width:100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:5%; text-align:center;">Kode</th>
		<th style="width:7%; text-align:center;">Nama Produk</th>
		<th style="width:10%; text-align:center;">Deskripsi</th>
		<th style="width:10%; text-align:center;">Remark</th>
		<th style="width:7%; text-align:center;">Konversi</th>
		<th style="width:7%; text-align:center;">NoPR / QtyPR</th>
		<th style="width:7%; text-align:center;">QtyPO</th>
		<th style="width:7%; text-align:center;">Tgl/Qty Datang</th>
		<th style="width:7%; text-align:center;">Harga Unit</th>
		<th style="width:7%; text-align:center;">Jml Harga</th>
	</tr>
	</thead>

	<tbody>
	<?php 
	/** Group by tgldelivery */
	if (!empty($resultPOdetail)):
	$podetail_jmlharga = 0;
	foreach($resultPOdetail as $element):
		$groupByDelivery[$element['podetail_tgldelivery']] [] = $element;
		$podetail_jmlharga += $element['podetail_jmlharga'];
	endforeach;
	ksort($groupByDelivery);

	foreach ($groupByDelivery as $tgldelivery => $each) :
		$tgldelivery = !empty($tgldelivery) ? $tgldelivery : '-'; 
		$tgldelivery = $tgldelivery == '-' ? $tgldelivery : format_date($tgldelivery, 'd-M-Y'); 
	?>

	<tr>
		<td colspan="11"><strong> <?php echo 'Delivery : '.$tgldelivery; ?> </strong></td>
	</tr>
		<?php 
		$no = 1;
		foreach($each as $r) :
			$currency_helper_total = $currency_helper[$r['kd_currency']];
			/** Goodsreceived */
			$rmgr = '<ul>';
			foreach ($result_goodsreceive as $eGoodsreceive) {
				if ($eGoodsreceive['podetail_kd'] == $r['podetail_kd']) {
					$rmgr .= '<li>'.format_date($eGoodsreceive['rmgr_tgldatang'], 'Y-m-d').'/'.$eGoodsreceive['rmgr_qty'].'</li>';
				}
			}
			$rmgr .= '<ul>';

		?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $r['rm_kode'].'/'.$r['rm_oldkd']; ?></td>
				<td><?php echo $r['podetail_nama']; ?></td>
				<td><?php echo $r['podetail_deskripsi'].'/'.$r['podetail_spesifikasi']; ?></td>
				<td><?php echo $r['podetail_remark']; ?></td>
				<td style="text-align:right;"><?php echo $r['podetail_konversi']; ?></td>
				<td style="text-align:right;"><?php echo $r['pr_no'].' / '.$r['prdetail_qty']; ?></td>
				<td style="text-align:right;"><?php echo $r['podetail_qty']; ?></td>
				<td style="text-align:right;"><?php echo $rmgr; ?></td>
				<td style="text-align:right;"><?php echo format_currency($r['podetail_harga'], $currency_helper[$r['kd_currency']]); ?></td>
				<td style="text-align:right;"><?php echo format_currency($r['podetail_jmlharga'], $currency_helper[$r['kd_currency']]); ?></td>
			</tr>
		<?php 
		$no++; 
		endforeach; ?>
	<?php 
	endforeach;
	
	?>
	</tbody>

	<tfoot>
		<tr>
			<td style="text-align:right;" colspan="10"> <strong> Total Harga : </strong> </td>
			<td style="text-align:right;"> <strong> <?php echo format_currency($podetail_jmlharga, $currency_helper_total);?>  </strong></td>
		</tr>
	</tfoot>
	<?php endif;?>
</table>
<hr>
<div class="row">
	<div class="col-md-6">
		<?php echo $generateButtonAction;
		if ($po_closeorder == '0'):
		?>
		<button class="btn btn-default" onclick="action_closeorder('<?php echo $po_kd; ?>')"> <i class="fa fa-check"></i> Selesai </button>
		<?php endif; ?>
	</div>
	<?php if (isset($imppr_kd)) :?>
	<div class="col-md-6">
		<button class="btn btn-info btn-sm pull-right" onclick="open_pr_box('<?php echo $id; ?>','<?php echo $imppr_kd; ?>')"> <i class="fa fa-search"></i> Lihat PR</button>
		<button class="btn btn-default btn-sm pull-right" onclick="kembali()"> <i class="fa fa-arrow-circle-left"></i> Kembali</button>
	</div>
	<?php endif;?>
</div>

<script type="text/javascript">
	// $('#idTable').DataTable();
</script>