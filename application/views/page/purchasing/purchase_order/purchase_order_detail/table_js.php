<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table('<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		window.location.assign('<?php echo base_url().$head_class_link;?>')
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table(id) {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_main'; ?>',
				data: {id:id},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
					view_log('<?php echo $id; ?>');
				}
			});
		});
	}

	function open_pr_box(id, batch_pr){
		$('#idTableBoxPurchaseRequisition').remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/table_pr_box'; ?>',
			data: {id:id, batch_pr:batch_pr},
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function log_status(id){
		$('#<?php echo $content_logstatus_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/log_status_main'; ?>',
				data: {id:id},
				success: function(html) {
					$('#<?php echo $content_logstatus_id; ?>').html(html);
					$('#<?php echo $content_logstatus_id; ?>').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function ubah_status(po_kd, level, akses){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/ubah_status_main'; ?>',
			data: {po_kd:po_kd, level:level, akses:akses},
			success: function(html) {
				toggle_modal('Keterangan Ubah Status', html);
			}
		});
	}

	function ubah_state(id = null, wftransition_kd = null) {
		event.preventDefault();
		 $.ajax({
            type: 'GET',
            url: '<?php echo base_url().$class_link.'/formubahstate_main'; ?>',
            data: {id:id, wftransition_kd: wftransition_kd},
            success: function(html) {
                toggle_modal('Ubah State', html);
            }
        });
	}

	function view_log(id){
		$.ajax({
            type: 'GET',
            url: '<?php echo base_url().$class_link.'/view_log'; ?>',
            data: {id: id},
            success: function(html) {
                $('#idMainContent').append(html);
            }
        });
	}

	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function action_closeorder (po_kd = null) {
		var conf = confirm('Apakah anda yakin ?');

		// console.log(po_kd);
		if (conf){
			$.blockUI({ overlayCSS: { backgroundColor: '#C0C0C0' } });
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_closeorder'; ?>',
				data: {po_kd: po_kd},
				success: function(data) {
					let resp = JSON.parse(data);
					if(resp.code == 200){
						close_to_sap(po_kd);
						// notify (resp.status, resp.pesan, 'success');
						// window.location.reload();
					}else{
						notify (resp.status, resp.pesan, 'error');
					}
				}
			});
		}
	}

	function close_to_sap(po_kd) {
		
		fetch("<?= url_api_sap() ?>ClosePurchaseOrder", {
		method: 'POST', // or 'PUT'
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({'U_IDU_WEBID': po_kd}),
		})
		.then((response) => response.json())
		.then((data) => {
			console.log('Success:', data);
			if(data.ErrorCode == 0 ){
				// console.log(dataToSAP);
				// console.log(items);
				// console.log(data.Message);
				// open_table();
				notify ('200! Sukses.', data.Message, 'success');
				$.unblockUI()
				window.location.reload();
			}else{
				// console.log(dataToSAP);
				// console.log(items);
				// console.log(data.Message);
				CloseRollback(po_kd)
				$.unblockUI()
				notify (data.ErrorCode + '. Gagal!', data.Message, 'error');
			}
		})
		.catch((error) => {
			CloseRollback(po_kd)
			$.unblockUI()
			notify ('000. Gagal!', error, 'error');
		});
		
	}

	function CloseRollback(po_kd)
	{
		$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_rollbackcloseorder'; ?>',
				data: {po_kd: po_kd},
				success: function(data) {
					let resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, 'Save SAP Gagal! Rollback Sukses.', 'success');
						$.unblockUI()
						setTimeout(window.location.reload(), 5000);
					}else{
						$.unblockUI()
						notify (resp.status, resp.pesan, 'error');
					}
				}
			});
		
	}
	function submitStatus() {
		$('#idbtnSubmitMain').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtnSubmitMain').attr('disabled', true);

		event.preventDefault();
		var form = document.getElementById('idFormInputStatus');

		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_ubah_status" ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					location.reload();
					$('.errInput').html('');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
				}else if (resp.code == 400){
					generateToken (resp.csrf);
				}else{
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function submitDataState (form_id) {
		$.blockUI({ overlayCSS: { backgroundColor: '#C0C0C0' } });
		event.preventDefault();
		$('#idbtnSubmit'+form_id).html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtnSubmit'+form_id).attr('disabled', true);
		var form = document.getElementById(form_id);

		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_change_state" ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				//console.log(data);
				var resp = JSON.parse(data);
				console.log(resp);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					//window.location.reload();
					if(resp.stts.status == 1){
						sendNotifMail(resp)
						//window.location.reload();
					}else{
						window.location.reload();
					}
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	
	function sendNotifMail(resp) {
		if(resp.stts.to == "dian@caterlindo.co.id"){
			var data = {
					service_id: 'service_89wfszf',
					template_id: 'purchase_oxx',
					user_id: 'user_gqom4wS5NcBXsu8XDttVI',
					template_params: {
					nopo: resp.msg.subjcet,
					note: "PO anda sudah di Approve, Next step berikutnya.",
					urlxx:resp.msg.url,
					cc: '',
					to:resp.stts.to
				}
			};
		}else{
			var data = {
					service_id: 'service_89wfszf',
					template_id: 'purchase_oxx',
					user_id: 'user_gqom4wS5NcBXsu8XDttVI',
					template_params: {
					nopo: resp.msg.subjcet,
					note: resp.msg.keterangan,
					urlxx:resp.msg.url,
					cc: '',
					to: resp.stts.to
				}
			};
		}


		//console.log(data)//
			$.ajax('https://api.emailjs.com/api/v1.0/email/send', {
			type: 'POST',
			data: JSON.stringify(data),
			contentType: 'application/json'
				}).done(function() {
					notify ('OK! 200.', 'Berhasil.', 'success');
					window.location.reload();
				}).fail(function(error) {
					notify('Oops… ' + JSON.stringify(error));
				})
	}


	function sendToSAP(items){
		
		var itm_detail = [];

		for (let i = 0; i < items.resultPOdetail.length; i++) {
			itm_detail.push({
							"U_IDU_WEBID": (items.resultPOdetail[i].podetail_kd) ? items.resultPOdetail[i].podetail_kd : "",
							"ItemCode": (items.resultPOdetail[i].rm_kode) ? items.resultPOdetail[i].rm_kode : "",
							"Dscription": (items.resultPOdetail[i].podetail_deskripsi) ? items.resultPOdetail[i].podetail_deskripsi : "",
							"U_IDU_Spesifikasi": (items.resultPOdetail[i].podetail_spesifikasi) ? items.resultPOdetail[i].podetail_spesifikasi : "",
							"KdCurrency": (items.resultPOdetail[i].kd_currency) ? items.resultPOdetail[i].kd_currency : "",
							"Quantity": (items.resultPOdetail[i].podetail_qty) ? items.resultPOdetail[i].podetail_qty : "",
							"Konversi": (items.resultPOdetail[i].podetail_konversi) ? items.resultPOdetail[i].podetail_konversi : "",
							"UomName": (items.resultPOdetail[i].rmsatuan_nama) ? items.resultPOdetail[i].rmsatuan_nama : "",
							"UnitPrice": (items.resultPOdetail[i].podetail_harga) ? items.resultPOdetail[i].podetail_harga : "",
							"LineTotal": (items.resultPOdetail[i].podetail_jmlharga) ? items.resultPOdetail[i].podetail_jmlharga : "",
							"ShipDate": (items.resultPOdetail[i].podetail_tgldelivery) ? items.resultPOdetail[i].podetail_tgldelivery : "",
						 })
		}

		var dataToSAP = {
						"U_IDU_WEBID": (items.rowPO.po_kd) ? items.rowPO.po_kd : "",
						"U_IDU_PO_INTNUM": items.rowPO.po_no ? items.rowPO.po_no : "",
						"U_IDU_PR_INTNUM": items.imppr_no ? items.imppr_no : "",
						"DocDate": items.rowPO.po_tanggal ? items.rowPO.po_tanggal : "",
						"Comments": items.rowPO.po_note ? items.rowPO.po_note : "",
						"CardCode": items.rowPO.suplier_kode ? items.rowPO.suplier_kode : "",
						"U_IDU_WEBUSER": items.rowPO.nm_admin ? items.rowPO.nm_admin : "",
						"Lines_Detail_Item": itm_detail
		}

		fetch("<?= url_api_sap() ?>AddPurchaseOrder", {
		method: 'POST', // or 'PUT'
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(dataToSAP),
		})
		.then((response) => response.json())
		.then((data) => {
			console.log('Success:', data);
			if(data.ErrorCode == 0 ){
				console.log(dataToSAP);
				console.log(items);
				console.log(data.Message);
				// open_table();
				notify ('200! Sukses.', data.Message, 'success');
				$.unblockUI()
				window.location.reload();
			}else{
				console.log(dataToSAP);
				console.log(items);
				console.log(data.Message);
				delRollback(items.rowPO.po_kd)
				$.unblockUI()
				notify (data.ErrorCode + '. Gagal!', data.Message, 'error');
			}
		})
		.catch((error) => {
			$.unblockUI()
			delRollback(items.rowPO.po_kd)
			notify ('000. Gagal!', error, 'error');
		});
	}

	function delRollback(id){
		$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_rollback_po'; ?>',
				data: 'po_kd='+id,
				success: function(data) {
					console.log(data);
					// var resp = JSON.parse(data);
					// if(resp.code == 200){
						notify ('xxx!', 'Save SAP Failed. Rollback Success.', 'success');
						// window.location.reload();
					// 	open_table();
					// }else if (resp.code == 400){
					// 	notify (resp.status, resp.pesan, 'error');
					// 	generateToken (resp.csrf);
					// }else{
					// 	notify ('Error', 'Error tidak Diketahui', 'error');                  
					// 	generateToken (resp.csrf);
					// }
				}
			});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function kembali() {
		event.preventDefault();
		window.history.back()
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }

</script>