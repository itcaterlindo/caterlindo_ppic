<?php

class customPdf extends Tcpdf {
    public $suplier_kode;
    public $po_no;
    public $suplier_nama;
    public $po_tglinput;
    public $suplier_alamat;
    public $originator;
    public $suplier_telpon;

    public function setsHeader($headerData){
        $this->suplier_kode = $headerData['suplier_kode'];
        $this->po_no = $headerData['po_no'];
        $this->suplier_nama = $headerData['suplier_nama'];
        $this->po_tglinput = format_date($headerData['po_tglinput'], 'd-M-Y'); 
        $this->suplier_alamat = $headerData['suplier_alamat'];
        $this->originator = $headerData['originator'];
        $this->suplier_telpon = $headerData['suplier_telpon'];
    }

    public function Header() {
        $header = '
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td width="40%" style="text-align:left;"> <img src="assets/admin_assets/dist/img/logo_cat.png" width="155" height="45"> </td>
                <td width="10%"> </td>
                <td width="50%" style="text-align:left;"> <img src="assets/admin_assets/dist/img/logo_bsi.png" width="955" height="170"> </td>
            </tr>
        </table>';

        $cat = '<table border="0" width="50%" style="font-size: 9px;">
            <tr>
                <td>CAT4-PUR-001/03/28-08-2020</td>
            </tr>
        </table>';

        /** Keterangan */
        $keterangan =
        '<table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td width="50%" style="font-size:22px; text-align:left; font-weight:bold;">SUPPLIER</td>
                <td width="50%" colspan="2" style="font-size:24px; text-align:left; font-weight:bold;">PURCHASE ORDER</td>
            </tr>
            <tr>
                <td width="50%" style="font-size:10px; text-align:left;">'.$this->suplier_kode.'</td>
                <td width="15%" style="font-size:14px; text-align:left; font-weight:bold;">Order No</td>
                <td width="35%" style="font-size:14px; text-align:left; font-weight:bold;">: '.$this->po_no.'</td>
            </tr>
            <tr>
                <td width="50%" style="font-size:10px; text-align:left;">'.$this->suplier_nama.'</td>
                <td width="15%" style="font-size:10px; text-align:left;">Date</td>
                <td width="35%" style="font-size:10px; text-align:left;">: '.$this->po_tglinput.'</td>
            </tr>
            <tr>
                <td width="50%" style="font-size:8px; text-align:left;">'.$this->suplier_alamat.'</td>
                <td width="15%" style="font-size:10px; text-align:left;">Originator</td>
                <td width="35%" style="font-size:10px; text-align:left;">: '.$this->originator.'</td>
            </tr>
            <tr>
                <td width="50%" style="font-size:8px; text-align:left;">'.$this->suplier_telpon.'</td>
                <td width="15%" style="font-size:10px; text-align:left;">Page</td>
                <td width="35%" style="font-size:10px; text-align:left;">: '.$this->getAliasNumPage().' of '.$this->getAliasNbPages().'</td>
            </tr>

        </table>';

        $this->writeHTML($header, true, false, false, false, '');
        $this->writeHTML($cat, true, false, false, false, '');
        $this->writeHTML($keterangan, true, false, false, false, '');
    }

}

$pdf = new customPdf('P', 'mm', 'A4', true, 'UTF-8', false);
$title = 'PO_'.$rowPO['po_no'];
$pdf->SetTitle($title);
$pdf->SetAutoPageBreak(true);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');
$pdf->setPrintFooter(false);

/** Margin */ 
$pdf->SetHeaderMargin(5);
$pdf->SetFooterMargin(10);
$pdf->SetMargins(8, 75, 8);

/** Approval */
$pending = ''; $checked1 = ''; $checked2 = ''; $approved = '';
$tglpending = ''; $tglchecked1 = ''; $tglchecked2 = ''; $tglapproved = '';
foreach ($resultLogStatus as $eachLog):
    switch ($eachLog['postatus_kd']) {
        case 2:
            $pending = $eachLog['nm_karyawan'];
            $tglpending = $eachLog['tgl_maxtrans'];
            break;
        case 3:
            $checked1 = $eachLog['nm_karyawan'];
            $tglchecked1 = $eachLog['tgl_maxtrans'];
            break;
        case 4:
            $checked2 = $eachLog['nm_karyawan'];
            $tglchecked2 = $eachLog['tgl_maxtrans'];
            break;
        case 5:
            $approved = $eachLog['nm_karyawan'];
            $tglapproved = $eachLog['tgl_maxtrans'];
            break;
    }
endforeach;

$dataHeader = array(
    'suplier_kode' => $rowPO['suplier_kode'],
    'po_no' => $rowPO['po_no'],
    'suplier_nama' => $rowPO['nm_select'].' '.$rowPO['suplier_nama'],
    'po_tglinput' => format_date($rowPO['po_tanggal'], 'd-m-Y'),
    'suplier_alamat' => $rowPO['suplier_alamat'],
    'originator' => $originator,
    'suplier_telpon' => $rowPO['suplier_telpon1'],
);
$pdf->setsHeader($dataHeader);

$pdf->AddPage();

$konten =
        '<table cellspacing="0" cellpadding="1" border="0" style="font-size:10px">
            <tr style="text-align:center; font-size:10px; font-weight: bold;">
                <th width="14%" style="border-left: 1px solid black; border-top: 1px solid black;">Product Code</th>
                <th width="39%" style="border-left: 1px solid black; border-top: 1px solid black;">Description</th>
                <th width="8%" rowspan="2" style="border-left: 1px solid black; border-right: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black;">Qty</th>
                <th width="10%" style="border-top: 1px solid black;">UOM</th>
                <th width="14%" style="border-top: 1px solid black; border-left: 1px solid black;">Unit Price</th>
                <th width="15%" style="border-left: 1px solid black; border-right: 1px solid black; border-top: 1px solid black;">Amount</th>
            </tr>
            <tr style="text-align:center; font-size:10px; font-style: italic;">
                <th width="14%" style="border-left: 1px solid black; border-bottom: 1px solid black;">Kode Produk</th>
                <th width="39%" style="border-left: 1px solid black; border-bottom: 1px solid black;">Keterangan</th>
                <th width="10%" style="border-bottom: 1px solid black;">Satuan</th>
                <th width="14%" style="border-bottom: 1px solid black; border-left: 1px solid black;">Harga / Unit</th>
                <th width="15%" style="border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">Jumlah</th>
            </tr>';

/** Result Data */
if (!empty($resultPOdetail)):
$totalAmount = 0;

foreach($resultPOdetail as $element):
    $groupByDelivery[$element['podetail_tgldelivery']] [] = $element;
    /** Hitung Jumlah */
    $totalAmount += $element['podetail_jmlharga'];
endforeach;
ksort($groupByDelivery);

foreach ($groupByDelivery as $tgldelivery => $each) :
    $konten .= 
       '<tr>
            <td width="14%" style="border-left: 1px solid black; border-bottom: 1px solid black; border-top: 1px solid black;"></td>
            <td colspan="5" style="font-weight: bold; border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-top: 1px solid black;">DELIVERY : '.format_date($tgldelivery, 'd-M-Y').'</td>
        </tr>
        ';
    foreach ($each as $r):
    $currency_helper_total = $currency_helper[$r['kd_currency']];
    $konten .=
        '<tr style="font-size:10px;">
            <td width="14%" style="border-left: 1px solid black; font-size: 9px;">'.$r['rm_kode'].' / '.$r['rm_oldkd'].'</td>
            <td width="39%" style="border-left: 1px solid black;">'.$r['podetail_deskripsi'].'/'.$r['podetail_spesifikasi'].'/'.nl2br($r['podetail_remark']).'</td>
            <th width="8%" style="text-align: right; border-left: 1px solid black; border-right: 1px solid black;">'.$r['podetail_qty'].'</th>
            <td width="10%" style="text-align: center;">'.$r['rmsatuan_nama'].'</td>
            <td width="14%" style="text-align: right; border-left: 1px solid black;">'.format_currency($r['podetail_harga'], $currency_helper[$r['kd_currency']]).'</td>
            <td width="15%" style="text-align: right; border-left: 1px solid black; border-right: 1px solid black;">'.format_currency($r['podetail_jmlharga'], $currency_helper[$r['kd_currency']]).'</td>
        </tr>';
    endforeach;
endforeach;

$konten .= 
    '<tr style="font-size:10px;">
        <td colspan="2" style="text-align: left; border-top: 1px solid black; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">NOTE : </td>
        <td style="border-top: 1px solid black;"></td>
        <td style="border-top: 1px solid black;"></td>
        <td style="text-align: right; font-weight:bold; border-top: 1px solid black;">TOTAL : </td>
        <td style="text-align: right; font-weight:bold; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; border-top: 1px solid black;">'.format_currency($totalAmount, $currency_helper_total).'</td>
    </tr>';

$konten .=
    '<tr style="font-size:10px;">
    <td colspan="6" style="text-align: left; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; border-top: 1px solid black;">
    <i>';
    $konten .= 'PLEASE SEND THIS ORDER AS CONFIRMATION BY FAX OR EMAIL <br>
        MOHON DIFAX / EMAIL KEMBALI SEBAGAI KONFIRMASI <br>';
    $konten .= '  - Please Send According to the Schedule <br>
       Mohon dikirim sesuai plan PPIC <br>
    - Please Provide Bank Account Number on Billing <br>
       Wajib mencantumkan no rekening pada penagihan <br>';
if (!empty($rowPO['suplier_termpayment'])) {
    $konten .=' - Term Payment '.$rowPO['suplier_termpayment'].' days <br>';
}
if ($rowPO['suplier_includeppn'] == 'T') {
    $konten .= ' - The price exclude tax <br> Harga belum termasuk PPN <br>';
}
$konten .= nl2br($rowPO['po_note']);
$konten .= '</i></td>
    </tr>';

$konten .=
        '</table>';

$pdf->writeHTML($konten, true, false, false, false, '');

endif;

$ttd = 
    '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:10px;">
        <tr>
            <td width="215" style="font-size: 7px;">
                <strong>TRADING TERMS: </strong>
            </td>
            <td width="85" style=""></td>
            <td width="70" style=""></td>
            <td width="85" style=""></td>
            <td width="85" style=""></td>
        </tr>
        <tr>
            <td width="215" style="font-size: 7px;">Please note for terms below : <br><i>Mohon diperhatikan hal hal berikut untuk kelancaran pembayaran :</i></td>
            <td width="85" style="text-align:center; font-size:9px; font-weight:bold;">RECEIPT</td>
            <td rowspan="4" colspan="3">
                <table cellspacing="0" cellpadding="0" border="0" style="font-size:10px;">';
    foreach ($logs as $log) {
        $ttd .=
        '<tr>
           <td width="30%">'.$log['wfstate_nama'].' By</td>
           <td width="30%" style="font-size:9px;"> <u>: '.$log['nm_admin'].'</u></td>
           <td width="40%" style="font-size:9px;">'.format_date($log['pologstatus_tglinput'], 'd-M-Y H:i:s').'</td>
        </tr>';
    }
$ttd .=         '</table>
            </td>
        </tr>
        <tr>
            <td width="215" style="font-size: 7px;">1. Order number must be listed in every correspondence<br><i>&nbsp;&nbsp;&nbsp;&nbsp;Nomor PO harus disertakan dalam setiap korespondensi</i></td>
            <td width="85" style="text-align:center;"><u></u></td>
        </tr>
        <tr>
            <td width="215" style="font-size: 7px;">2. Order fulfillment should be in accordance with PO <br><i>&nbsp;&nbsp;&nbsp;&nbsp;Pemenuhan pesanan sebaiknya sesuai PO</i></td>
            <td width="85" style="text-align:center;"><u></u></td>
        </tr>
        <tr>
            <td width="215" style="font-size: 7px;">3. Please attach order number on your delivery order and invoice<br><i>&nbsp;&nbsp;&nbsp;&nbsp;Harap mencantumkan No PO pada SJ/DO dan Nota Tagihan</i></td>
            <td width="85" style="text-align:center; font-size: 9px; font-weight:bold; border-top: 1px solid black;">SUPPLIER</td>
        </tr>
    </table>';
$pdf->writeHTML($ttd, true, false, false, false, '');

$txtOutput = $title.'.pdf';
$pdf->Output($txtOutput, 'I');