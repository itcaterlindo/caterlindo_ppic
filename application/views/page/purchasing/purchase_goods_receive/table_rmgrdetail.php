<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<?php 
$formGR = 'idFormGR';
echo form_open_multipart('', array('id' => $formGR)); ?>
<table id="idTablePO" class="table table-bordered table-striped " style="width:100%; font-size: 85%">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:7%; text-align:center;">Tgl Datang</th>
		<th style="width:7%; text-align:center;">No Surat Jalan</th>
		<th style="width:5%; text-align:center;">No PO</th>
		<th style="width:7%; text-align:center;">RM Kode</th>
		<th style="width:20%; text-align:center;">Deskripsi Material</th>
		<th style="width:5%; text-align:center;">Qty PO</th>
		<th style="width:5%; text-align:center;">Qty Goods Receive</th>
		<th style="width:4%; text-align:center;">Satuan</th>
		<th style="width:7%; text-align:center;">Harga PO</th>
		<th style="width:10%; text-align:center;">Total PO</th>
	</tr>
	</thead>
    <tbody>
        <?php 
		$no = 1;
		$subHarga = 0;
		$totalHarga = 0;
		foreach ($goodsreceive as $each) :
			$subHarga = (float)$each['rmgr_qty'] * $each['podetail_harga'];
			$totalHarga += $subHarga;
			$helper_currency = $each['icon_type'].' '.$each['currency_icon'].' '.$each['pemisah_angka'].' '.$each['format_akhir'];
		?>
        <tr>
            <td class="dt-center"><?php echo $no; ?></td>
            <td><?php echo format_date($each['rmgr_tgldatang'], 'Y-m-d') ?></td>
            <td><?php echo $each['rmgr_nosrj'] ?></td>
            <td><?php echo $each['po_no']; ?></td>
            <td><?php echo $each['rm_kode']; ?></td>
            <td><?php echo $each['podetail_deskripsi'].'/'.$each['podetail_spesifikasi']; ?></td>
            <td class="dt-right">
            <?php 
                echo (float) $each['podetail_qty'];?>
            </td>
            <td class="dt-right">
            <?php 
                echo (float) $each['rmgr_qty'];?>
            </td>
            <td><?php echo $each['rmsatuan_nama']; ?></td>
            <td class="dt-right">
            <?php 
                echo format_currency($each['podetail_harga'], $helper_currency) ?>
            </td>
            <td class="dt-right">
            <?php 
                echo format_currency($subHarga, $helper_currency) ?>
            </td>
        
        </tr>
        <?php 
        $no ++;
        endforeach;?>
    </tbody>
	<tfoot>
	<tr>
		<th style="text-align:center;"></th>
		<th style="text-align:center;"></th>
		<th style="text-align:center;"></th>
		<th style="text-align:center;"></th>
		<th style="text-align:center;"></th>
		<th style="text-align:center;"></th>
		<th style="text-align:center;"></th>
		<th style="text-align:center;"></th>
		<th style="text-align:center;"></th>
		<th style="text-align:center;">Total</th>
		<th style="text-align:center;"><?php echo format_currency($totalHarga, $helper_currency) ?></th>
	</tr>
	</tfoot>
</table>
<button type="button" class="btn btn-primary btn-sm pull-right" onclick="PushToSap()"> <i class="fa fa-save"></i> Push To Sap</button>

<?php echo form_close(); ?>

<script type="text/javascript">
	render_dt('#idTablePO');

	$('.tt-harga').on('keyup', function(){
		var subtotal = 0;
		var total = 0;
		var fTotal = '';
		var fSubtotal = '';
		$('.tt-harga').each(function(){
			var $this = $(this);
			var quantity = parseFloat($(this).val());
			var rmgr_qty = parseFloat($(this).siblings('.tt-rmgr_qty').val());
			var rmgr_kd = $(this).siblings('.tt-rmgr_kd').val();
			
			subtotal = quantity*rmgr_qty;
			fSubtotal = number_format (subtotal, 2, ',', '.'); 
			$('#idsubtotalGR'+rmgr_kd).text(fSubtotal);
			total += subtotal;
		})
		fTotal = number_format (total, 2, ',', '.'); 
		$('.totalGR').text(fTotal);
	})

	function number_format (number, decimals, dec_point, thousands_sep) {
		// Strip all characters but numerical ones.
		number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
		var n = !isFinite(+number) ? 0 : +number,
			prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
			sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
			dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
			s = '',
			toFixedFix = function (n, prec) {
				var k = Math.pow(10, prec);
				return '' + Math.round(n * k) / k;
			};
		// Fix for IE parseFloat(0.55).toFixed(0) = 0;
		s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
		if (s[0].length > 3) {
			s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
		}
		if ((s[1] || '').length < prec) {
			s[1] = s[1] || '';
			s[1] += new Array(prec - s[1].length + 1).join('0');
		}
		return 'Rp '+s.join(dec) + ' ,-';
	}

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
		});
	}
</script>