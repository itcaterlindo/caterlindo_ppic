<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
echo form_open_multipart('', array('id' => 'idFormCari', 'class' => 'form-horizontal'));

// echo form_input(array( 'type'=> 'text', 'id' => 'idtxtpodetail_konversi', 'name'=> 'txtpodetail_konversi', 'placeholder' => 'idtxtpodetail_konversi', 'class' => 'tt-input'));
?>

<div class="row">

	<div class="form-group">
		<label for='idtxtrmgr_nosrj' class="col-md-2 control-label">No PO/Surat Jalan</label>
		<div class="col-sm-6 col-xs-12">
			<div class="errInput" id="idErrpo_kd"></div>
			<select name="txtrmgr_nosrj" id="idtxtrmgr_nosrj" class="form-control"> </select>
		</div>
		<button class="btn btn-sm btn-success" id="idbtnpilih"> <i class="fa fa-check-circle"></i> Pilih </button>
	</div>
	
</div>
<?php echo form_close(); ?>

<?php //echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal')); ?>

<div class="row">
	<div class="col-md-12">
		<div id="idtablepo"></div>
	</div>
</div>

<!-- <hr>
<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-2 col-sm-offset-10 col-xs-12">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData()" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button type="reset" name="btnReset" onclick="resetFormMain()" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
		</div>
	</div>
</div> -->
<?php //echo form_close(); ?>

<script type="text/javascript">
	
</script>
