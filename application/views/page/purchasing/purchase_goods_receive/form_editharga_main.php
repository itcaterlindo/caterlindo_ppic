<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputHarga';
extract($rowData);
$rmgr_deskripsi = $podetail_deskripsi.'/'.$podetail_spesifikasi;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>
<input type="hidden" name="txtrmgr_kd" placeholder="txtrmgr_kd" value="<?= isset($rmgr_kd) ? $rmgr_kd: null ?>">
<input type="hidden" name="txtrmgr_qtykonversi" placeholder="txtrmgr_qtykonversi" value="<?= isset($rmgr_qtykonversi) ? $rmgr_qtykonversi: null ?>">
<input type="hidden" name="txtpodetail_kd" placeholder="txtpodetail_kd" value="<?= isset($podetail_kd) ? $podetail_kd: null ?>">
<input type="hidden" name="txtrm_kd" placeholder="txtrm_kd" value="<?= isset($rm_kd) ? $rm_kd: null ?>">


<div class="row invoice-info">
    <div class="col-sm-12">
        <div class="col-sm-6 invoice-col">
            <b>No PO :</b> <?php echo isset($po_no) ? $po_no : '-'; ?><br>
            <b>Supplier :</b> <?php echo isset($suplier_nama) ? $suplier_nama : '-'; ?> <br>
        </div>
        <div class="col-sm-6 invoice-col">
            <b>Surat Jalan :</b> <?php echo isset($rmgr_nosrj) ? $rmgr_nosrj : '-'; ?><br>
        </div>
    </div>
</div>
<hr>

<div class="col-sm-12">
    <div class="form-group">
        <label for="idtxtrmgr_nosrj" class="col-md-2 control-label">Material</label>
        <div class="col-sm-2 col-xs-12">
            <input type="text" class="form-control" placeholder="Kode" readonly="readonly" value="<?= isset($rm_kode) ? $rm_kode : null ?>">
        </div>
        <div class="col-sm-8 col-xs-12">
            <input type="text" class="form-control" placeholder="Deskripsi" readonly="readonly" value="<?= isset($rmgr_deskripsi) ? $rmgr_deskripsi : null ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="idtxtrmgr_nosrj" class="col-md-2 control-label">Qty</label>
        <div class="col-sm-4 col-xs-12">
            <input type="number" class="form-control" name="txtrmgr_qty" placeholder="Harga Unit" readonly="readonly" value="<?= isset($rmgr_qty) ? $rmgr_qty : null ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="idtxtrmgr_hargaunit" class="col-md-2 control-label">Harga</label>
        <div class="col-sm-6 col-xs-12">
            <div class="errInput" id="idErrrmgr_hargaunit"></div>
            <input type="number" class="form-control" name="txtrmgr_hargaunit" placeholder="Harga Unit" value="<?= isset($rmgr_hargaunit) ? $rmgr_hargaunit : null ?>">
        </div>
    </div>
    <button class="btn btn-sm btn-primary pull-right" onclick="submitFormEditHarga('<?= $form_id; ?>')"> <i class="fa fa-save"></i> Simpan </button>
</div>
<?php echo form_close(); ?>