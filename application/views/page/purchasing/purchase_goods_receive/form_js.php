<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form();
	open_table_box('<?php echo date('Y-m-d'); ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	$(document).off('click', '#idbtnpilih').on('click', '#idbtnpilih', function() {
		event.preventDefault();
		var rmgr_nosrj = $('#idtxtrmgr_nosrj').val();
		if (rmgr_nosrj == null){
			alert('Pilih Surat Jalan');
		}else{
			box_overlay('in');
			open_table_rmgrdetail (rmgr_nosrj);
			box_overlay('out');
		}
	});

	function open_table_rmgrdetail (rmgr_nosrj) {
		box_overlay('in');
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/table_rmgrdetail'; ?>',
			data: {rmgr_nosrj:rmgr_nosrj},
			success: function(html) {
				$('#idtablepo').html(html);
				box_overlay('out')
			}
		});
	}	

	function open_form(){
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/form_main'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
					render_srj();
				}
			});
		});
	}

	function render_srj() {
		$("#idtxtrmgr_nosrj").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?= base_url().$class_link ?>/get_grby_suratjalan',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramSRJ: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}
	
	function open_table_box(date) {
		$('#idTableBox<?php echo $master_var;?>').remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/table_box'; ?>',
			data: {date:date},
			success: function(html) {
				$('#idMainContent').append(html);
				render_datetime('.datetimepicker');
			}
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	// function submitData(form_id) {
	// 	event.preventDefault();
	// 	var form = document.getElementById(form_id);

	// 	$.ajax({
	// 		url: "<?php //echo base_url().$class_link; ?>/action_insert" ,
	// 		type: "POST",
	// 		data:  new FormData(form),
	// 		contentType: false,
	// 		cache: false,
	// 		processData:false,
	// 		success: function(data){
	// 			var resp = JSON.parse(data);
	// 			if(resp.code == 200){
	// 				notify (resp.status, resp.pesan, 'success');
	// 				open_table_rmgrdetail (resp.data.rmgr_nosrj)
	// 				open_table_box('<?php //echo date('Y-m-d'); ?>');
	// 				generateToken (resp.csrf);
	// 			}else if (resp.code == 401){
	// 				$.each( resp.pesan, function( key, value ) {
	// 					$('#'+key).html(value);
	// 				});
	// 				generateToken (resp.csrf);
	// 				moveTo('idMainContent');
	// 			}else if (resp.code == 400){
	// 				notify (resp.status, resp.pesan, 'error');
	// 				generateToken (resp.csrf);
	// 			}else{
	// 				notify ('Error', 'Error tidak Diketahui', 'error');
	// 				generateToken (resp.csrf);
	// 			}
	// 		} 	        
	// 	});
	// }

	function PushToSap() { 
		$.blockUI({ overlayCSS: { backgroundColor: '#C0C0C0' } }); 
		try{
				var param = $('#idtxtrmgr_nosrj').text();

				param = param.split(' || ');

				var param_nyel = {'rmgr_code_srj' : param[0].replace(" ", ""), 'no_srj' : param[1]}
				
				$.ajax({
					type: "GET",
					url: "<?php echo base_url().$class_link; ?>/getGR",
					data: param_nyel,
					success: function(response) {
						sendSap(response, param_nyel);
					}
				});
			}
		catch(error){
			open_table();
			$.unblockUI();
			notify ('First Error!', error, 'error');
		}
	 }

	function sendSap(param, param_nyel) { 

					
				try
					{
					var result = [];
					var final = [];

					param.reduce(function(res, value) {
					if (!res[value.rmgr_kd]) {
						res[value.rmgr_kd] = {
							"U_IDU_WEBID": value.rmgr_kd,
							"KdPurchaseOrderDtl": value.podetail_kd,
							"Quantity": 0,
							"Konversi": value.podetail_konversi,
							"KdWarehouse": value.kd_warehouse ? value.kd_warehouse : "MGD260719003",
						};
						result.push(res[value.rmgr_kd])
					}
					res[value.rmgr_kd].Quantity += parseFloat(value.rmgr_qty);
					return res;
					}, {});


					result.reduce(function(res, value) {
					if (!res[value.KdPurchaseOrderDtl]) {
						res[value.KdPurchaseOrderDtl] = {
							"U_IDU_WEBID": value.U_IDU_WEBID,
							"KdPurchaseOrderDtl": value.KdPurchaseOrderDtl,
							"Quantity": 0,
							"Konversi": value.Konversi,
							"KdWarehouse": value.KdWarehouse,
						};
						final.push(res[value.KdPurchaseOrderDtl])
					}
					res[value.KdPurchaseOrderDtl].Quantity += parseFloat(value.Quantity);
					return res;
					}, {});
				
					var data_to_sap = {
						"U_IDU_WEBID": param[0].rmgr_code_srj,
						"DocDate": param[0].rmgr_tgldatang,
						"NumAtCard": param[0].rmgr_nosrj,
						"Comments": param[0].rmgr_ket ? param[0].rmgr_ket : "",
						"U_IDU_NoSJ": param[0].rmgr_nosrj,
						"U_IDU_TglSJ": param[0].rmgr_tglsrj,
						"U_IDU_WEBUSER": param[0].nm_admin,
						"Lines_Detail_Item": final
					}

					console.log(data_to_sap);
				

					fetch("<?= url_api_sap() ?>AddGRPO", {
								method: 'POST', // or 'PUT'
								headers: {
									'Content-Type': 'application/json',
								},
								body: JSON.stringify(data_to_sap),
								})
								.then((response) => response.json())
								.then((items) => {
									console.log('Success:', items);
									console.log(data_to_sap);
									if(items.ErrorCode == 0){
										open_table();
										notify ('200', 'SAP OK!', 'success');
										$.unblockUI();
								// 	open_table();
										window.location.reload();
									}else{
										delRollback(param_nyel) 
										open_table();
										$.unblockUI();
										notify ('Callback error', items.Message, 'error');
									}
								})
								.catch((error) => {
									open_table();
									delRollback(param_nyel);
									$.unblockUI();
									notify ('Error Ajax', error, 'error');
								});
							}
				catch(error){
									open_table();
									delRollback(param_nyel);
									$.unblockUI();
									notify ('Second Error!', error, 'error');
				};
	}

	function delRollback(param) { 
		$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_rollback'; ?>',
				data: param,
				success: function(data) {
					console.log(data);
					// var resp = JSON.parse(data);
					// if(resp.code == 200){
						notify ('xxx!', 'Save SAP Failed. Rollback Success.', 'Gagal!');
					// 	open_table();
					// }else if (resp.code == 400){
					// 	notify (resp.status, resp.pesan, 'error');
					// 	generateToken (resp.csrf);
					// }else{
					// 	notify ('Error', 'Error tidak Diketahui', 'error');
					// 	generateToken (resp.csrf);
					// }
				}
			});
  }

	// function submitDataBatch (form_id) {
	// 	event.preventDefault();
	// 	var form = document.getElementById(form_id);

	// 	$.ajax({
	// 		url: "<?php //echo base_url().$class_link; ?>/action_insertbatch" ,
	// 		type: "POST",
	// 		data:  new FormData(form),
	// 		contentType: false,
	// 		cache: false,
	// 		processData:false,
	// 		success: function(data){
	// 			console.log(data);
	// 			var resp = JSON.parse(data);
	// 			if(resp.code == 200){
	// 				notify (resp.status, resp.pesan, 'success');
	// 				open_form();
	// 				open_table_box('<?php echo date('Y-m-d'); ?>');
	// 				generateToken (resp.csrf);
	// 			}else if (resp.code == 401){
	// 				$.each( resp.pesan, function( key, value ) {
	// 					$('#'+key).html(value);
	// 				});
	// 				generateToken (resp.csrf);
	// 				moveTo('idMainContent');
	// 			}else if (resp.code == 400){
	// 				notify (resp.status, resp.pesan, 'error');
	// 				generateToken (resp.csrf);
	// 			}else{
	// 				notify ('Error', 'Error tidak Diketahui', 'error');
	// 				generateToken (resp.csrf);
	// 			}
	// 		} 	        
	// 	});
	// }

	function render_datetime(valClass){
		$(valClass).datetimepicker({
            format: 'DD-MM-YYYY',
        });
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
		box_overlay('out');
    }

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}
</script>