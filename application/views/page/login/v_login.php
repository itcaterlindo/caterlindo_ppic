<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $login_title; ?></title>
		<link rel="shortcut icon" href='<?php echo base_url().'assets/admin_assets/dist/img/setting_img/'.$favicon; ?>'>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		
		<!-- Bootstrap 3.3.5 -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_assets/bootstrap/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_assets/bootstrap/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_assets/dist/css/AdminLTE.min.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition login-page">
		<div class="login-box">
			<div class="login-logo" style="text-shadow: 4px 4px 0px #11206d;">
				<a href="<?php echo base_url(); ?>" style="color:white !important;"><b>Admin</b> Login</a>
			</div><!-- /.login-logo -->
			<div class="login-box-body">
				<p class="login-box-msg">Login untuk masuk sistem</p>
				<?php
				$attributes = array('id' => 'idFormLogin');
				echo form_open_multipart('', $attributes);
				?>
				<div id="idErrUsername"></div>
				<div class="form-group has-feedback">
					<input type="text" id="idTxtUsername" name="txtUser" class="form-control" placeholder="User ID" autofocus>
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				</div>
				<div id="idErrPassword"></div>
				<div class="form-group has-feedback">
					<input type="password" id="idTxtPass" name="txtPass" class="form-control" placeholder="Password">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-4 pull-right">
						<button type="submit" name="btnLogin" class="btn btn-primary btn-block btn-flat" onmouseover="this.style.boxShadow='0px 0px 3px black';" onmouseout="this.style.boxShadow='';">Login</button>
					</div><!-- /.col -->
				</div>
				<?php
				echo form_close();
				?>

			</div><!-- /.login-box-body -->
		</div><!-- /.login-box -->

		<!-- jQuery 2.1.4 -->
		<script src="<?php echo base_url(); ?>assets/admin_assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<!-- Bootstrap 3.3.5 -->
		<script src="<?php echo base_url(); ?>assets/admin_assets/bootstrap/js/bootstrap.min.js"></script>
		<!--BACKSTRETCH-->
		<!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin_assets/plugins/backstretch/jquery.backstretch.min.js"></script>
		<script>
			$.backstretch("<?php echo base_url(); ?>assets/admin_assets/dist/img/setting_img/<?php echo $login_bg_img; ?>", {speed: 500});
		</script>

		<script type="text/javascript">
			$('#idTxtUsername').focus();
			$(document).on('submit', '#idFormLogin', function(e) {
				$('#idErrUsername').html('');
				$('#idErrPassword').html('');
				$('button[name="btnLogin"]').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
				$('button[name="btnLogin"]').attr({
					'class': 'btn btn-default btn-block btn-flat'
				});
				$('#idTxtUsername').attr({'readonly': 'TRUE'});
				$('#idTxtPass').attr({'readonly': 'TRUE'});
				e.preventDefault();
				$.ajax({
					url: "<?php echo base_url(); ?>auth/login/verifyLogin",
					type: "POST",
					data:  new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					success: function(data){
						if (data.confirm == 'success') {
							$('button[name="btnLogin"]').html('Berhasil!');
							$('button[name="btnLogin"]').attr({
								'class': 'btn btn-success btn-block btn-flat'
							});
							// alert(data.msg);
							// window.location.replace("<?php //echo base_url(); ?>dashboard");
							window.location.replace("<?php echo $uri; ?>");
						}
						if (data.confirm == 'errValidation') {
							$('#idTxtUsername').removeAttr('readonly');
							$('#idTxtPass').removeAttr('readonly');
							$('button[name="btnLogin"]').html('Gagal!');
							$('button[name="btnLogin"]').attr({
								'class': 'btn btn-danger btn-block btn-flat'
							});
							$('#idTxtUsername').focus();
							$('input[name="csrf_ppic_software"]').val(data.csrf);
							$('#idErrUsername').append(data.idErrUsername);
							$('#idErrPassword').append(data.idErrPassword);
						}
					} 	        
				});
				return false;
			});
		</script>
		
	</body>
</html>