<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>



<p id="kurx_now"><?php echo "KURS Dolar saat ini: " . $kurs; ?></p>
<table id="idTable" class="table table-bordered table-striped table-hover display responsive" style="width:100%; font-size:90%;">
	
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:10%; text-align:center;">RM Deskripsi</th>	
		<th style="width:7%; text-align:center;">RM kode</th>
		<!-- <th style="width:10%; text-align:center;">Suplier</th> -->
		<!-- <th style="width:5%; text-align:center;">Qty</th> -->
		<th style="width:5%; text-align:center;">Total Qty Konv</th>
		<th style="width:5%; text-align:center;">Total Hrg Konv</th>
		<th style="width:5%; text-align:center;">Total Hrg Konv USD</th>
		<!-- <th style="width:5%; text-align:center;">Satuan</th> -->
		<!-- <th style="width:7%; text-align:center;">Code Srj</th> -->
	</tr>
	</thead>
	<tbody>
	<?php 
	$no = 1;
	foreach ($result_grdetail as $r) : ?>
	<tr>
		<td class="dt-center"><?= $no?></td>
		<td><?php 
		$desc = $r['rm_deskripsi'].'/'.$r['rm_spesifikasi']; 
		echo $desc
		?></td>
		<td><?= $r['rm_kode']?></td>
		<td class="dt-right"><?= $r['total_qty_conf']?></td>
		<td class="dt-right"><?= $r['total_hrg_conf']?></td>
		<td class="dt-right xample"><?php if ($r['kd_currency'] == "MCN271017002") echo $r['total_hrg_conf'] * $kurs; ?></td>  
		
	</tr>
	<?php 
	$no++;
	endforeach;?>
	</tbody>
</table>

<script type="text/javascript">
	render_dt('#idTable');
	

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [
				{
					"extend" : "excel",
					"title" : "Report GR Material",
					"exportOptions": {
						"columns": [ 0,1,2,3,4,5]
					}
				},{
                extend: 'print',
				// messageTop: function () {
                //     return `<p style="font-size:18pt">
				// 		Kertas Kerja Perhitungan Fisik Aset dan Inventaris<br>
				// 		Periode :  `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</p>`;
                // },
				// messageTop: function () {
                //     return `<p style="font-size:22pt">
				// 		Laporan Asset ditemukan<br>
				// 		Periode :  Desember - 2021</p>`;
                // },
				exportOptions: {
					columns: [ 0, 1, 2, 4, 5, 6, 7 ]
				},
				title: `&nbsp;`,
				// title: 'PT Caterlindo -- Asset di stoktake Periode "' + $('#periode_nomor_seri').val() +'"',
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10t' )
                        .prepend(`<div class="container" style="margin-left:-10px;"><br>
									<div class="row">
										<div class="col-md-6">
											<img src="https://caterlindo.co.id/web/Images/Logo-Utama.png" style="width:25%; height: 85px;  margin-top:-2%;" />	
										</div>
										<div class="col-md-6">
											<div style="display:inline-block; float: right; margin-top:-8%;">
												<h1 style="font-size:24pt; font-family: 'Arial', sans-serif; "><b>REPORT INCOMING MATERIAL</b></h1>
													<table style="border: 0px; font-family: 'Arial', sans-serif;">
														<tr>
															<th style="text-align: left;"><p>No. Dokumen &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></th>
															<td><p>: CAT4-WRH-001</p></td>
														</tr>
														<tr>
															<th style="text-align: left;"><p >Tanggal Terbit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></th>
															<td><p>: 31 Aug 2023</p></td>
														</tr>
														<tr>
															<th style="text-align: left;"><p>Revisi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></th>
															<td><p>: 01</p></td>
														</tr>
													</table>
											</div>	
										</div>		
									</div>
								</div>`);
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
						var last = null;
						var current = null;
						var bod = [];
		
						var css = '@page { size: potrait }',
							head = win.document.head || win.document.getElementsByTagName('head')[0],
							style = win.document.createElement('style');
		
						style.type = 'text/css';
						style.media = 'print';
		
						if (style.styleSheet)
						{
							style.styleSheet.cssText = css;
						}
						else
						{
							style.appendChild(win.document.createTextNode(css));
						}
		
						head.appendChild(style);
                }}, 

				
		],
		"columnDefs": [
		{  
			targets: 5,  // Targeting fourth column, "Age"  
			render: function(data, type, row) { 
				const idrFormat = new Intl.NumberFormat('id-ID', {  
						style: 'currency',  
						currency: 'IDR',  
					}).format(Math.round(data));  
				return (data ? idrFormat : "");
			}  
		},  
		]
		});
	}

	


</script>