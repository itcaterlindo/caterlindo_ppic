<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table('<?php echo date('Y-m-d'); ?>', '<?php echo date('Y-m-d'); ?>', '');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table(dari, sampai, kurs) {
		box_overlay('in');
		// $('#<?php //echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_main'; ?>',
				data: {dari:dari, sampai:sampai, kurs:kurs},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					// $('#<?php //echo $box_content_id; ?>').slideDown();
					render_datetimepicker('datetimepicker');
					box_overlay('out');
					moveTo('idMainContent');
				}
			});
		// });
	}



	function cariIncomingMaterial () {
		event.preventDefault();
		var dari = $('#idtxtdari').val();
		var sampai = $('#idtxtsampai').val();

		$.ajax({
				type: 'GET',
				url: 'https://v6.exchangerate-api.com/v6/957f4cc26cd6688754e65f42/latest/USD',
				success: function(html) {
					// const idrFormat = new Intl.NumberFormat('id-ID', {  
					// 	style: 'currency',  
					// 	currency: 'IDR',  
					// }).format(Math.round(html.conversion_rates.IDR)); 
					console.log(html.conversion_rates.IDR)
					var kurs = html.conversion_rates.IDR;
					open_table(dari, sampai, kurs);

					
				}
		});
		
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
		box_overlay('out');
    }

	function render_datetimepicker(valClass){
		$('.'+valClass).datetimepicker({
			format: 'DD-MM-YYYY',
    	});
	}

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	
	

</script>