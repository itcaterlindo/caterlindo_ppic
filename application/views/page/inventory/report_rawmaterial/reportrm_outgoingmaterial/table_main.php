<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<table id="idTable" class="table table-bordered table-striped table-hover display responsive" style="width:100%; font-size:90%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:10%; text-align:center;">DO</th>
		<th style="width:15%; text-align:center;">RM Deskripsi</th>
		<th style="width:5%; text-align:center;">RM lama</th>
		<th style="width:5%; text-align:center;">RM kode</th>
		<th style="width:10%; text-align:center;">To</th>
		<th style="width:5%; text-align:center;">Qty / Qty Konversi</th>
		<th style="width:5%; text-align:center;">Satuan</th>
		<th style="width:5%; text-align:center;">Kirim Kembali</th>
		<th style="width:5%; text-align:center;">Return (Qty/Qty Konversi)</th>
	</tr>
	</thead>
	<tbody>
	<?php 
	$no = 1;
	foreach ($whresults as $el) : 
		$htmlReturn = '<ul>';
		foreach ($returns as $ret):
			if ($ret['whdeliveryorderdetail_kd'] == $el['whdeliveryorderdetail_kd']){
				$htmlReturn .= '<li>('.format_date($ret['rmgr_tgldatang'], 'Y-m-d').'/'.$ret['rmgr_nosrj'].')'.$ret['rmgr_qty'].' / '.$ret['rmgr_qtykonversi'].'</li>';
			}
		endforeach;
	?>
	<tr>
		<td><?php echo $no; ?></td>
		<td><?php echo $el['whdeliveryorder_no'].' | '.$el['whdeliveryorder_tanggal'].' / '.$el['nm_karyawan'].$el['suplier_nama']; ?></td>
		<td><?php echo $el['whdeliveryorderdetail_deskripsi'].'/'.$el['whdeliveryorderdetail_spesifikasi']; ?></td>
		<td><?php echo $el['rm_oldkd']; ?></td>
		<td><?php echo $el['rm_kode']; ?></td>
		<td><?php echo $el['nm_karyawan'].$el['suplier_nama']; ?></td>
		<td style="text-align: right;"><?php echo $el['whdeliveryorderdetail_qty'].' / '.$el['whdeliveryorderdetail_qtykonversi']; ?></td>
		<td><?php echo $el['rmsatuan_nama']; ?></td>
		<td><?php echo choice_enum($el['whdeliveryorder_sendback']); ?></td>
		<td><?php 
		$htmlReturn .= '</ul>';
		echo $htmlReturn;
		 ?></td>
	</tr>
	<?php
		$no++;
	endforeach;?>
	</tbody>
</table>

<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"title" : "Report Outgoing Material",
				"exportOptions": {
					"columns": [ 0,1,2,3,4,5,6,7]
				}
			}],
		});
	}
</script>