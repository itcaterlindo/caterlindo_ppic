<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataRuang';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtKd', 'value' => $kd_rak_ruang));
echo form_input(array('type' => 'hidden', 'name' => 'txtKdRak', 'value' => $rak_kd));
echo form_input(array('type' => 'hidden', 'name' => 'txtKdGudang', 'value' => $gudang_kd));
?>
<div class="form-group">
	<label for="idTxtNmRuang" class="col-md-2 control-label">Nama Ruang</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrNmRuang"></div>
		<?php echo form_input(array('name' => 'txtNmRuang', 'id' => 'idTxtNmRuang', 'class' => 'form-control', 'placeholder' => 'Nama Ruang', 'value' => $nm_rak_ruang)); ?>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" class="btn btn-primary btn-flat">
			<i class="fa fa-save"></i> Submit
		</button>
	</div>
</div>
<?php echo form_close(); ?>