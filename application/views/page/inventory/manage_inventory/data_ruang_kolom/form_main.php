<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataRuangKolom';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtKdRakRuangKolom', 'value' => isset($rakruangkolom_kd)?$rakruangkolom_kd:null)  );
echo form_input(array('type' => 'hidden', 'name' => 'txtKdRakRuang', 'value' => $kd_rak_ruang));
echo form_input(array('type' => 'hidden', 'name' => 'txtKdRak', 'value' => $rak_kd));
echo form_input(array('type' => 'hidden', 'name' => 'txtKdGudang', 'value' => $gudang_kd));
?>
<div class="form-group">
	<label for="idTxtNmRuang" class="col-md-2 control-label">Nama Ruang Kolom</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrNmRuang"></div>
		<?php echo form_input(array('name' => 'txtNmRuangKolom', 'id' => 'idTxtNmRuangKolom', 'class' => 'form-control', 'placeholder' => 'Nama Ruang Kolom', 'value' => isset($rakruangkolom_nama)?$rakruangkolom_nama:null) ); ?>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" class="btn btn-primary btn-flat">
			<i class="fa fa-save"></i> Submit
		</button>
	</div>
</div>
<?php echo form_close(); ?>