<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataDeliveryMaterialUser';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtSts', 'name'=> 'txtSts', 'value' => $sts ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtdeliverymaterialuser_kd', 'name'=> 'txtdeliverymaterialuser_kd', 'value' => isset($id) ? $id: null ));

?>
<div class="form-group">
	<label for="idtxttipeAdmin" class="col-md-2 control-label">Tipe Admin</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrTipeAdmin"></div>
		<?php echo form_dropdown('txttipeAdmin', $opsiTipeAdmin, isset($selectedTipeAdmin)? $selectedTipeAdmin: null, ['id'=> 'idtxttipeAdmin', 'class'=> 'form-control']);?>
	</div>
</div>
<div class="form-group">
	<label for="idtxtnmGudang" class="col-md-2 control-label">Nama Gudang</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrNmGudang"></div>
		<?php echo form_dropdown('txtnmGudang', $opsiGudang, isset($selectedGudang)? $selectedGudang: null, ['id'=> 'idtxtnmGudang', 'class'=> 'form-control']);?>
	</div>
</div>
<div class="form-group">
	<label for="idtxtnmGudang" class="col-md-2 control-label">Flag Terima</label>
	<div class="col-sm-4 col-xs-12">
		<div id="idErrFlagTerima"></div>
		<?= form_checkbox(['name' => 'chkTerima', 'id' => 'idChkTerima', 'value' => 1, 'checked' => $flag_terima == '1' ? TRUE : FALSE ]); ?>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" onclick="submitData()" class="btn btn-primary btn-flat">
			<i class="fa fa-save"></i> Submit
		</button>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">

	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitData() {
		event.preventDefault();
		var conf = confirm('Apakah anda yakin ingin menyimpan data ini ?');
		if(conf){
			box_overlay('in');
			var form = document.getElementById('<?php echo $form_id; ?>');
			// edit atau add
			var sts =  $('#idtxtSts').val();
			var url = '';
			if (sts == 'add'){
				url = "<?php echo base_url().$class_link; ?>/action_insert";
			}else{
				url = "<?php echo base_url().$class_link; ?>/action_update";
			}

			$.ajax({
				url: url ,
				type: "POST",
				data:  new FormData(form),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						open_table();
						$('#idFormBoxFormUserDeliveryMaterial').remove();
					}else if (resp.code == 401){
						$.each( resp.pesan, function( key, value ) {
							$('#'+key).html(value);
						});
						generateToken (resp.csrf);
						box_overlay('out');
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				} 	        
			});
		}
	}
	
</script>