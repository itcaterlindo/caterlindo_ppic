<?php
defined('BASEPATH') or exit('No direct script access allowed!');

if (!empty($item_row->item_barcode)) :
	?>
	<div class="form-group" style="display: none;">
		<div class="form-group">
			<label class="col-md-3 control-label">Barcode :</label>
			<div class="col-sm-4 col-xs-12" style="padding-top: 7px;">
				<?php echo $item_row->item_barcode; ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label">Item Code :</label>
			<div class="col-sm-4 col-xs-12" style="padding-top: 7px;">
				<?php echo $item_row->item_code; ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label">Deskripsi :</label>
			<div class="col-sm-4 col-xs-12" style="padding-top: 7px;">
				<?php echo $item_row->deskripsi_barang; ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label">Dimensi :</label>
			<div class="col-sm-4 col-xs-12" style="padding-top: 7px;">
				<?php echo $item_row->dimensi_barang; ?>
			</div>
		</div>
	</div>
	<?php
else :
	?>
	<div class="form-group" data-dismiss="alert">
		<label class="col-md-2 control-label"></label>
		<div class="col-sm-4 col-xs-12">
			<?php echo buildAlert('warning', 'Peringatan!<br>', 'Maaf Barcode <strong><u>'.$input_barcode.'</u></strong> yang anda inputkan tidak terdaftar dalam sistem!'); ?>
		</div>
	</div>
	<?php
endif;
