<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	var csrf_hash = '<?php echo $this->security->get_csrf_hash(); ?>';
	open_form('<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		$('#idTableBoxLoader<?php echo $master_var; ?>').show();
		$('#idTableBoxContent<?php echo $master_var; ?>').html();
		open_table();
		first_load('<?php echo 'idTableBoxLoader'.$master_var; ?>', '<?php echo 'idTableBoxContent'.$master_var; ?>');
	});

	$(document).off('change', '#idSelInputOption').on('change', '#idSelInputOption', function() {
		var input_val = $(this).val();
		$('input[name="txtKdBarang"]').val('');
		if (input_val == 'item_code') {
			$('.form-input-barcode').slideUp(function() {
				$('.form-input-item-code').slideDown();
				$('#idTxtItemCode').val('');
				$('#idTxtItemCode').focus();
			});
		} else if (input_val == 'barcode') {
			$('.form-input-item-code').slideUp(function() {
				$('.form-input-barcode').slideDown();
				$('.cl-item-details').html('');
				$('#idTxtItemBarcode').val('');
				$('#idTxtItemBarcode').focus();
			});
		}
	});

	$(document).off('submit', '#idForm<?php echo $master_var; ?>').on('submit', '#idForm<?php echo $master_var; ?>', function(e) {
		e.preventDefault();
		submit_form(this);
	});

	function open_form(id) {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/open_form'; ?>',
				data: 'id='+id,
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
					$('#idSelInputOption').focus();
					$('.datepickerbootstrap').datetimepicker({
						format: 'DD-MM-YYYY'
					});
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function submit_form(form_id) {
		$('#<?php echo $box_overlay_id; ?>').show();
		$('#<?php echo $box_alert_id; ?>').html('');
		<?php
		$no = 0;
		foreach ($form_errs as $form_err) {
			echo $no == 0?"":"\n\t\t";
			echo '$(\'#'.$form_err.'\').slideUp();';
			$no++;
		}
		echo "\n";
		?>
		$.ajax({
			url: "<?php echo base_url().$class_link.'/send_data'; ?>",
			type: "POST",
			data:  new FormData(form_id),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					if (data.input_option == 'barcode') {
						$('#idFormBoxAlertDataHistoriBarang').html(data.alert).fadeIn();
					} else if (data.input_option == 'item_code') {
						remove_box('#idFormBoxDataHistoriBarang');
						$('#idTableBoxAlert<?php echo $master_var; ?>').html(data.alert).fadeIn();
					}
					open_table();
				}
				if (data.confirm == 'error') {
					$('#<?php echo $box_alert_id; ?>').html(data.alert);
					<?php
					$no = 0;
					foreach ($form_errs as $form_err) {
						echo $no == 0?"":"\n\t\t\t\t\t";
						echo 'if (data.'.$form_err.' != \'\') {';
							echo '$(\'#'.$form_err.'\').html(data.'.$form_err.').slideDown();';
						echo '}';
						$no++;
					}
					echo "\n";
					?>
				}
				if (data.input_option == 'barcode') {
					$('#idTxtItemBarcode').focus();
					$('#idTxtItemBarcode').val('');
				} else if (data.input_option == 'item_code') {
					$('#idTxtItemCode').focus();
				}
				$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
				$('#<?php echo $box_overlay_id; ?>').hide();
			}
		});
	}

	/*$(document).off('keypress', '#idTxtItemBarcode').on('keypress', '#idTxtItemBarcode', function(e) {
		if (e.which == '10' || e.which == '13') {
			e.preventDefault();
			var item_barcode = $(this).val();
			$(this).val('');
			barcode_get_item_detail(item_barcode);
		}
	});

	function barcode_get_item_detail(item_barcode) {
		$('.cl-item-details').slideUp('fast');
		$.ajax({
			url: '<?php echo base_url($class_link.'/get_item_data'); ?>',
			type: 'GET',
			data: {'item_barcode': item_barcode},
			success: function(data) {
				$('input[name="txtKdBarang"]').val(data.kd_barang);
				$('input[name="txtItemCode"]').val(data.item_code);
				if (data.kd_barang != '') {
					$('input[name="txtJmlItem"]').focus();
				}
				$('.cl-item-details').html(data.view);
			}
		});
		$('.cl-item-details').slideDown('fast', function() {
			$('.form-group').slideDown('fast');
		});
	}*/
</script>