<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
$input_opts = ['' => '-- Pilih Input Option --', 'item_code' => 'Item Code', 'barcode' => 'Barcode Scanner'];

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataHistoriBarang';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtKd', 'value' => $kd_rak_histori));
echo form_input(array('type' => 'hidden', 'name' => 'txtKdRakRuang', 'value' => $rak_ruang_kd));
echo form_input(array('type' => 'hidden', 'name' => 'txtKdRak', 'value' => $rak_kd));
echo form_input(array('type' => 'hidden', 'name' => 'txtKdGudang', 'value' => $gudang_kd));
echo form_input(array('type' => 'hidden', 'name' => 'txtKdBarang', 'value' => $barang_kd));
?>
<div class="form-group">
	<label for="idTxtItemCode" class="col-md-2 control-label">Input Option</label>
	<div class="col-sm-3 col-xs-12">
		<div id="idErrInputOption"></div>
		<?php
		echo form_dropdown('selInputOption', $input_opts, '', ['id' => 'idSelInputOption', 'class' => 'form-control']);
		?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtTglTransaksi" class="col-md-2 control-label">Tanggal</label>
	<div class="col-sm-2 col-xs-12">
		<div id="idErrTglTransaksi"></div>
		<?php echo form_input(array('name' => 'txtTglTransaksi', 'id' => 'idTxtTglTransaksi', 'class' => 'form-control datepickerbootstrap', 'placeholder' => 'Tanggal', 'value' => $tgl_transaksi)); ?>
	</div>
</div>
<div class="form-input-item-code" style="display: none;">
	<div class="form-group">
		<label for="idTxtItemCode" class="col-md-2 control-label">Item Code</label>
		<div class="col-sm-3 col-xs-12">
			<div id="idErrItemCode"></div>
			<div id="scrollable-dropdown-menu">
				<?php echo form_input(array('name' => 'txtItemCode', 'id' => 'idTxtItemCode', 'class' => 'form-control', 'placeholder' => 'Item Code')); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="idTxtJmlItem" class="col-md-2 control-label">Jumlah Item</label>
		<div class="col-sm-2 col-xs-12">
			<div id="idErrJmlItem"></div>
			<?php echo form_input(array('name' => 'txtJmlItem', 'id' => 'idTxtJmlItem', 'class' => 'form-control', 'placeholder' => 'Jumlah Item', 'value' => $qty)); ?>
		</div>
	</div>
</div>
<div class="form-input-barcode" style="display: none;">
	<div class="form-group">
		<label for="idTxtItemBarcode" class="col-md-2 control-label">Item Barcode</label>
		<div class="col-sm-3 col-xs-12">
			<div id="idErrItemBarcode"></div>
			<?php echo form_input(array('name' => 'txtItemBarcode', 'id' => 'idTxtItemBarcode', 'class' => 'form-control', 'placeholder' => 'Item Barcode')); ?>
		</div>
	</div>
	<div class="cl-item-details">
		<?php
		if (isset($item_code)) :
			$data['item_row'] = (object) ['item_barcode' => $item_barcode, 'item_code' => $item_code, 'deskripsi_barang' => $deskripsi_barang, 'dimensi_barang' => $dimensi_barang];
			$this->load->view('page/'.$class_link.'/form_detail_item', $data);
		endif;
		?>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" class="btn btn-primary btn-flat">
			<i class="fa fa-save"></i> Submit
		</button>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	/* --start product typeahead.js-- */
	var Product = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		url: '<?php echo base_url('auto_complete/get_item_detail'); ?>',
		remote: {
			url: '<?php echo base_url('auto_complete/get_item_detail'); ?>',
			prepare: function (query, settings) {
				settings.type = 'GET';
				settings.contentType = 'application/json; charset=UTF-8';
				settings.data = 'item_code='+query;

				return settings;
			},
			wildcard: '%QUERY'
		}
	});

	$('#idTxtItemCode').typeahead(null, {
		limit: 50,
		minLength: 1,
		name: 'product_search',
		display: 'item_code',
		valueKey: 'get_product',
		source: Product.ttAdapter()
	});

	$('#idTxtItemCode').bind('typeahead:select', function(obj, selected) {
		$('input[name="txtKdBarang"]').val(selected.kd_barang);
	});
	/* --end of product typeahead.js-- */
</script>