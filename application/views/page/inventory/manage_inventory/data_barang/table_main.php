<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<div class="row" style="margin-bottom: 10px;">
	<div class="col-md-6">
		<a href="<?php echo base_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/data_warehouse'); ?>" class="btn btn-flat btn-primary" title="Kembali ke Data Gudang">Data Gudang</a>
		<a href="<?php echo base_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/data_rak'); ?>" class="btn btn-flat btn-info" title="Kembali ke Data Rak">Data Rak</a>
		<a href="<?php echo base_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/data_ruang'); ?>" class="btn btn-flat btn-warning" title="Kembali ke Data Ruang">Data Ruang</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<table id="idTable" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
			<thead>
				<tr>
					<th style="width:1%; text-align:center;" class="all">No.</th>
					<th style="width:4%; text-align:center;" class="all">Opsi</th>
					<th style="width:5%; text-align:center;">Code</th>
					<th style="width:10%; text-align:left;">Item Barcode</th>
					<th style="width:10%; text-align:left;">Item Code</th>
					<th style="width:25%; text-align:left;">Description</th>
					<th style="width:15%; text-align:left;">Dimension</th>
					<th style="width:10%; text-align:center;">Qty Masuk</th>
					<th style="width:10%; text-align:center;">Qty Keluar</th>
					<th style="width:10%; text-align:center;">Total Qty</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<script type="text/javascript">
	$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};
	var table = $('#idTable').DataTable({
		"processing": true,
		"serverSide": true,
		"ordering" : true,
		"ajax": "<?php echo base_url().$class_link.'/table_data'; ?>",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"columnDefs": [
			{"data": null, "searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
			{"searchable": false, "orderable": false, "targets": 1},
			{"className": "dt-center", "visible": false, "targets": 2},
			{"className": "dt-center", "targets": 7},
			{"className": "dt-center", "targets": 8},
			{"className": "dt-center", "targets": 9},
		],
		"order":[2, 'asc'],
		"rowCallback": function (row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			var index = page * length + (iDisplayIndex + 1);
			var total = data[7] - data[8];
			$('td:eq(0)', row).html(index);
			$('td:eq(8)', row).html(total);
		}
	});
</script>