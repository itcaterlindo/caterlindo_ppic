<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$this->load->model(['modul_inventory/td_rak_histori']);
$history_result = $_SESSION['modul_inventory']['items_history'];
?>

<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
			<thead>
				<tr>
					<th style="width:1%; text-align:center;" class="all">No.</th>
					<th style="width:4%; text-align:center;" class="all">Opsi</th>
					<th style="width:10%; text-align:left;">Gudang</th>
					<th style="width:10%; text-align:left;">Rak</th>
					<th style="width:10%; text-align:left;">Ruangan</th>
					<th style="width:15%; text-align:left;">Item Code</th>
					<th style="width:20%; text-align:left;">Description</th>
					<th style="width:10%; text-align:left;">Dimension</th>
					<th style="width:10%; text-align:center;">Qty</th>
					<th style="width:10%; text-align:center;">Status</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$no = 0;
				foreach ($history_result as $row) :
					$no++;
					echo '<tr>';
						echo '<td style=\'text-align: center;\'>'.$no.'</td>';
						echo '<td style=\'text-align: center;\'>'.$this->td_rak_histori->tbl_histori_btn($row->kd_rak_histori, $row->item_code).'</td>';
						echo '<td>'.$_SESSION['modul_inventory']['nm_gudang'].'</td>';
						echo '<td>'.$_SESSION['modul_inventory']['nm_rak'].'</td>';
						echo '<td>'.$_SESSION['modul_inventory']['nm_rak_ruang'].'</td>';
						echo '<td>'.$row->item_code.'</td>';
						echo '<td>'.$row->deskripsi_barang.'</td>';
						echo '<td>'.$row->dimensi_barang.'</td>';
						echo '<td style=\'text-align: center;\'>'.$row->qty.'</td>';
						echo '<td style=\'text-align: center;\'>'.ucwords($row->status).'</td>';
					echo '</tr>';
				endforeach;
				?>
			</tbody>
		</table>
	</div>
</div>
