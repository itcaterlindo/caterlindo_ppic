<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	var csrf_hash = '<?php echo $this->security->get_csrf_hash(); ?>';
	open_content({'<?php echo $this->security->get_csrf_token_name(); ?>': csrf_hash}, '<?php echo base_url($class_link.'/open_table'); ?>', '#idTableBoxContentBarangMasuk', '');
	first_load('.idTableBoxLoaderBarangMasuk', '.idTableBoxContentBarangMasuk');
</script>
