<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$tipe_barang_opts = ['' => '-- Pilih Tipe Barang --', 'finish_good' => 'Finish Good', 'raw_material' => 'Raw Material'];
$form_id = 'idFormSelect'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>
<div class="form-group">
	<label for="idSelTipeBarang" class="col-md-2 control-label">Tipe Barang</label>
	<div class="col-sm-2 col-xs-12">
		<div id="idErrTipeBarang"></div>
		<?php echo form_dropdown('selTipeBarang', $tipe_barang_opts, '', ['id' => 'idSelTipeBarang', 'class' => 'form-control']); ?>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" class="btn btn-primary btn-flat">
			<i class="fa fa-save"></i> Submit
		</button>
	</div>
</div>
<?php echo form_close(); ?>
