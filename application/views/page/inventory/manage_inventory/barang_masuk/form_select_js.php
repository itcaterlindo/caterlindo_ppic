<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	var csrf_hash = '<?php echo $this->security->get_csrf_hash(); ?>';
	open_content({'<?php echo $this->security->get_csrf_token_name(); ?>': csrf_hash , 'master_var': '<?php echo $master_var; ?>'}, '<?php echo base_url($class_link.'/open_form_select'); ?>', '#idFormSelectBoxContentBarangMasuk', '#idSelTipeBarang');
	first_load('#idFormSelectBoxLoaderBarangMasuk', '#idFormSelectBoxContentBarangMasuk');

	$(document).off('submit', '#idFormSelectBarangMasuk').on('submit', '#idFormSelectBarangMasuk', function(e) {
		e.preventDefault();
		get_barang_table(this, '#idSelTipeBarang');
	});

	function open_content(data_arr, method_url, box_element, focus_element) {
		$(box_element).slideUp(function(){
			$.ajax({
				type: 'POST',
				url: method_url,
				data: data_arr,
				success: function(data) {
					// CSRF for security reasons
					csrf_hash = data.csrf_hash;
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf_hash);

					$(box_element).html(data.view);
					$(box_element).slideDown();
					moveTo('#idMainContent');
					if (focus_element != '') {
						$(focus_element).focus();
					}
				}
			});
		});
	}

	function moveTo(element) {
		$('html, body').animate({ scrollTop: $(element).offset().top - $('header').height() }, 1000);
	}

	function first_load(loader, content) {
		$(loader).fadeOut(500, function(e){
			$(content).slideDown();
		});
	}

	function remove_box(box_element) {
		$(box_element).slideUp('fast', function() {
			$(this).remove();
		});
	}

	function get_barang_table(form_id, focus_element) {
		remove_box('#idTableBoxBarangMasuk');
		$('#<?php echo $box_alert_id; ?>').html('');
		<?php
		$no = 0;
		foreach ($form_errs as $form_err) {
			echo $no == 0?"":"\n\t\t";
			echo '$(\'#'.$form_err.'\').slideUp();';
			$no++;
		}
		echo "\n";
		?>
		$.ajax({
			url: '<?php echo base_url($class_link.'/send_form_select_data'); ?>',
			type: "POST",
			data:  new FormData(form_id),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					$('#idMainContent').append(data.view);
				} else if (data.confirm == 'error') {
					$('#<?php echo $box_alert_id; ?>').html(data.alert);
					<?php
					$no = 0;
					foreach ($form_errs as $form_err) {
						echo $no == 0?"":"\n\t\t\t\t\t";
						echo 'if (data.'.$form_err.' != \'\') {';
							echo '$(\'#'.$form_err.'\').html(data.'.$form_err.').slideDown();';
						echo '}';
						$no++;
					}
					echo "\n";
					?>
				}
				if (focus_element != '') {
					$(focus_element).focus();
				}
				$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
			}
		});
	}
</script>