<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	$(document).ready(function() {
		render_datetimepicker('.datepicker');
		first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');
		render_rawmaterial();
	});

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function table_main(startdate, enddate, jnstransaksi, jnssatuan, rm_kd) {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/table_main'; ?>',
				data: {
					'startdate': startdate,
					'enddate': enddate,
					'jnstransaksi': jnstransaksi,
					'jnssatuan': jnssatuan,
					'rm_kd': rm_kd
				},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').slideDown().html(html);
					moveTo('idMainContent');
				}
			});
		});
	}

	function render_rawmaterial() {
		$('#idtxtrm_kd').select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 2,
			ajax: {
				url: '<?php echo base_url() ?>auto_complete/get_raw_material',
				type: "get",
				dataType: 'json',
				delay: 500,
				data: function(params) {
					return {
						paramRM: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function processKartuStok() {  
		event.preventDefault();
		let startdate = $('#idtxtstartdate').val();
		let enddate = $('#idtxtenddate').val();
		let jnstransaksi = $('#idtxtjnstransaksi').val();
		let jnssatuan = $('#idtxtjnssatuan').val();
		let rm_kd = $('#idtxtrm_kd').val();
		table_main(startdate, enddate, jnstransaksi, jnssatuan, rm_kd)
	}

	function render_datetimepicker(element) {
		$(element).datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		});
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			delay: 2500,
			styling: 'bootstrap3'
		});
	}

	function cetak_kartu_stok(){
		let startdate = $('#idtxtstartdate').val();
		let enddate = $('#idtxtenddate').val();
		let jnstransaksi = $('#idtxtjnstransaksi').val();
		let jnssatuan = $('#idtxtjnssatuan').val();
		let rm_kd = $('#idtxtrm_kd').val();
		
		window.open('<?php echo base_url().$class_link;?>/cetak_kartu_stok?startdate='+startdate+'&enddate='+enddate+'&jnstransaksi='+jnstransaksi+'&jnssatuan='+jnssatuan+'&rm_kd='+rm_kd);
	}

	function xls_kartu_stok(){
		let startdate = $('#idtxtstartdate').val();
		let enddate = $('#idtxtenddate').val();
		let jnstransaksi = $('#idtxtjnstransaksi').val();
		let jnssatuan = $('#idtxtjnssatuan').val();
		let rm_kd = $('#idtxtrm_kd').val();
		
		window.open('<?php echo base_url().$class_link;?>/xls_kartu_stok?startdate='+startdate+'&enddate='+enddate+'&jnstransaksi='+jnstransaksi+'&jnssatuan='+jnssatuan+'&rm_kd='+rm_kd);
	}

</script>