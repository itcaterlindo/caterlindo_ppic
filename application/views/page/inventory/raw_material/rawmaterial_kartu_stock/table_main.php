<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}

	td.dt-blue {
		background-color: blue;
		color: white;
	}

	td.dt-green {
		background-color: green;
		color: white;
	}

	td.dt-red {
		background-color: red;
		color: white;
	}

	td.dt-purple {
		background-color: purple;
		color: white;
	}
</style>
<div class="row invoice-info">
	<center>
		<h3 type="text" style="font-weight: bold;" id="idJuduLaporan"> Laporan Kartu Stok </h3>
		<h4> Periode <?php echo $startdate . ' s/d ' . $enddate; ?> </h4>
	</center>
	<hr>
</div>
<div class="row invoice-info">
	<div class="col-md-6">
		<table border="0" style="width: 100%;">
			<tr>
				<td width="24%">RM Kode</td>
				<td width="1%">:</td>
				<td width="75%"><?php echo $material['rm_kode'].' / '.$material['rm_oldkd'] ?></td>
			</tr>
			<tr>
				<td>RM Deskripsi</td>
				<td>:</td>
				<td><?php echo $material['rm_deskripsi'] ?></td>
			</tr>
			<tr>
				<td>RM Spesifikasi</td>
				<td>:</td>
				<td><?php echo $material['rm_spesifikasi'] ?></td>
			</tr>
			<tr>
				<td>RM Satuan</td>
				<td>:</td>
				<td><?php echo $material['rmsatuan_nama'] ?></td>
			</tr>
			<tr>
				<td>RM Satuan Secondary</td>
				<td>:</td>
				<td><?php echo $material['satuan_secondary_nama'] ?></td>
			</tr>
		</table>
	</div>
	<div class="col-md-6">
		<table border="0" style="width: 100%;">
			<tr>
				<td width="24%">LastOpname Periode</td>
				<td width="1%">:</td>
				<td width="75%"><?php echo $rmstokmaster['rmstokmaster_stokopname_tgl'] ?></td>
			</tr>
			<tr>
				<td>LastOpname Qty</td>
				<td>:</td>
				<td><?php echo $rmstokmaster['rmstokmaster_stokopname_qty'].' '.$material['rmsatuan_nama']. ' ('. $rmstokmaster['rmstokmaster_stokopname_qty'] / $material['rm_konversiqty'].' '.$material['satuan_secondary_nama'].')' ?></td>
			</tr>
			<tr>
				<td>CurrentStok Qty</td>
				<td>:</td>
				<td><?php echo $rmstokmaster['rmstokmaster_qty'].' '.$material['rmsatuan_nama'].' ('.$rmstokmaster['rmstokmaster_qty'] / $material['rm_konversiqty'].' '.$material['satuan_secondary_nama'].')' ?></td>
			</tr>
		</table>
	</div>
</div>
<hr>
<div class="row invoice-info">
	<table id="idtabledtl" style="column-width: 10px;width: 100%;" border="1">
		<thead>
			<tr>
				<th style="width:1%; text-align:center;">No.</th>
				<th style="width:4%; text-align:center;">Tgl Transaksi</th>
				<th style="width:10%; text-align:center;">Uraian</th>
				<th style="width:3%; text-align:center;">Satuan</th>
				<th style="width:3%; text-align:center;">Qty IN</th>
				<th style="width:3%; text-align:center;">Qty OUT</th>
				<th style="width:3%; text-align:center;">Balance</th>
			</tr>
		</thead>
		<tbody>
			<?php
			 	$no = 1; 
				foreach($kartu_stock as $key => $value){ 
			?>
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $value['trans_tgl'] ?></td>
				<td><?= $value['trans_bukti_uraian'] ?></td>
				<td class="dt-center"><?= $value['trans_satuan'] ?></td>
				<td class="dt-right dt-green"><?= $value['trans_qty_in'] ?></td>
				<td class="dt-right dt-red"><?= $value['trans_qty_out'] ?></td>
				<td class="dt-right dt-blue"><?= number_format($value['trans_qty_balance'],2,'.','') ?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>