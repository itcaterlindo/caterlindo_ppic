<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$js_file = 'table_js';
$box_title = 'Material Kartu Stok';
$data['class_link'] = $class_link;
$box_type = 'Table';
$data['master_var'] = 'MaterialRequisition';
$data['box_id'] = 'id' . $box_type . 'Box' . $data['master_var'];
$data['box_content_id'] = 'id' . $box_type . 'BoxContent' . $data['master_var'];
$data['box_loader_id'] = 'id' . $box_type . 'BoxLoader' . $data['master_var'];
$data['btn_add_id'] = 'id' . $box_type . 'BtnBoxAdd' . $data['master_var'];

$data['modal_title'] = 'Modal Default';
$data['content_modal_id'] = 'idModal' . $box_type . $data['master_var'];
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Filter</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool btn-collapse" id="idBtnBoxTableSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <div id="<?php echo $data['box_loader_id']; ?>" align="middle">
            <i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group">
                    <label for="idtxtstartdate">Start Date</label>
                    <div id="idErrStartdate"></div>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control datepicker" id="idtxtstartdate" name="txtstartdate" placeholder="Start Date" value="<?php echo date('Y-m-d H:i:s') ?>">
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label for="idtxtenddate">End Date</label>
                    <div id="idErrEnddate"></div>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control datepicker" id="idtxtenddate" name="txtenddate" placeholder="End Date" value="<?php echo date('Y-m-d H:i:s') ?>">
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label for="idtxtjnstransaksi">Jenis Transaksi</label>
                    <div id="idErrJnsTransaksi"></div>
                    <?php
                    echo form_dropdown('txtjnstransaksi', $opsiTransaksi, null, array('class' => 'form-control', 'id' => "idtxtjnstransaksi"));
                    ?>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label for="idtxtjnssatuan">Jenis Satuan</label>
                    <div id="idErrjnssatuan"></div>
                    <?php
                    echo form_dropdown('txtjnssatuan', $opsiSatuan, null, array('class' => 'form-control', 'id' => "idtxtjnssatuan"));
                    ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="idtxtrm_kd">Raw Material</label>
                    <div id="idErrtm_kd"></div>
                    <select name="txtrm_kd" class="form-control" id="idtxtrm_kd">
                    </select>
                </div>
            </div>
            <div class="col-sm-1">
                <div class="form-group">
                    <br>
                    <button class="btn btn-success" id="idBtnSubmit" onclick="processKartuStok()"> <i class="fa fa-search"></i> Proses</button>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
    </div>
</div>

<div class="box box-primary" id="<?php echo $data['box_id']; ?>">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo $box_title; ?></h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" id="<?php echo $data['btn_add_id']; ?>" data-toggle="tooltip" title="Tambah"><i class="fa fa-plus"></i></button>
            <button class="btn btn-box-tool btn-collapse" id="idBtnBoxTableSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
			<div class="col-md-12">
				<button class="btn btn-sm btn-info no-print" id="btn-cetak-excel" onclick="xls_kartu_stok()"><i class="fa fa-file-excel-o"></i> Cetak Kartu Stok (Excel) </button>
                <button class="btn btn-sm btn-info no-print" id="btn-cetak-pdf" onclick="cetak_kartu_stok()"><i class="fa fa-print"></i> Cetak Kartu Stok (PDF) </button>
			</div>
		</div>
        <div id="<?php echo $data['box_loader_id']; ?>" align="middle">
            <i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f; display: none;"></i>
        </div>
        <div class="col-md-12">
            <div id="<?php echo $data['box_content_id']; ?>" style="display: none;"></div>
        </div>
    </div>
    <div class="box-footer">
    </div>
    <div class="overlay" id="idBoxTableOverlay" style="display: none;">
        <i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
    </div>
</div>

<?php
$this->load->view('page/' . $class_link . '/' . $js_file, $data); ?>

<!-- modal -->
<div class="modal fade" id="idmodal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo $data['modal_title']; ?></h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div id="<?php echo $data['content_modal_id']; ?>"></div>
                </div>
                <hr> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Keluar</button>
            </div>
        </div>
    </div>
</div>