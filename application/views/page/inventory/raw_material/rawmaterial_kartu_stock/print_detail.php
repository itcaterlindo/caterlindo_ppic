<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	body {
		/* font-size: 12px; */
  		margin:2mm 0mm 0mm 5mm;
	}
	/* .table>tbody>tr>td {
		padding:1px;
	}
	.table>tfoot>tr>td {
		padding:1px;
	} */
	/* @media print {
		body {transform: scale(80%);}
		.print {
			page-break-after: always;
		}
		.print-foot {
			page-break-inside: avoid;
		}
		.div-footer {
			position: fixed;
			bottom: 0;
		}
		.print-left {
			text-align: left;
		}
		.print-center {
			text-align: center;
		}
		.print-right {
			text-align: right;
		}
	} */
    /* #pageFooter {
        display: table-footer-group;
    }

    #pageFooter::before {
        counter-increment: page;
        content: counter(page);
    } */
</style>
<div class="row no-print">
	<div class="col-xs-12">
		<a href="javascript:void(0);" class="btn btn-danger no-print" onclick="window.close();" style="margin-left: 25px;">
			<i class="fa fa-ban"></i> Close
		</a>
		<a href="javascript:void(0);" class="btn btn-primary no-print" onclick="window.print();" style="margin-left: 25px;">
			<i class="fa fa-print"></i> Cetak Data
		</a>
	</div>
</div>
<div class="row invoice print">
	<div id="idbox_loader" align="middle">
		<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
	</div>
	<!-- Header -->
	<table border="0" width="100%">
		<tbody>
				<tr>
					<td style="width: 40%;">
						<img src="<?= base_url() ?>/assets/admin_assets/dist/img/logo_cat.png" width="135" height="35"> 
					</td>
				<td style="width: 60%;">

					<table border="0" style="width: 100%;"> 
						<tbody>
							<tr>
								<td colspan="3" style="text-align: right; font-size: 32px; font-weight: bold;">KARTU STOCK MATERIAL</td>
							</tr>
							<tr>
								<td width="45%"></td>
								<td width="35%" style="font-size: 13px; text-align: left; border-bottom: 1px solid black;">No. Dokumen</td>
								<td width="35%" style="font-size: 13px; text-align: left; border-bottom: 1px solid black;">: CAT4-WRH-006</td>
							</tr>
							<tr>
								<td></td>
								<td style="font-size: 13px; text-align: left; border-bottom: 1px solid black;">Tanggal</td>
								<td style="font-size: 13px; text-align: left; border-bottom: 1px solid black;">: 31 Aug 2023</td>
							</tr>
							<tr>
								<td></td>
								<td style="font-size: 13px; text-align: left; border-bottom: 1px solid black;">Revisi</td>
								<td style="font-size: 13px; text-align: left; border-bottom: 1px solid black;">: 01</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
	<!-- End Header -->
	<hr>
	<br>
	<div id='idContent'></div>
</div>	
<script src="<?php echo base_url(); ?>assets/admin_assets/plugins/jQuery-3.2.1/jQuery-3.2.1.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(e){
		open_table('<?php echo $url; ?>');
	});

	function open_table(url) {
		$('#idbox_loader').fadeIn();
			$.ajax({
				type: 'GET',
				url: url,
				success: function(html) {
					$('#idContent').html(html);
					$('#idbox_loader').fadeOut();
				}
			});
	}
</script>