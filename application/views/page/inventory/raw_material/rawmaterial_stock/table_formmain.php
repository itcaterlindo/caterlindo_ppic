<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
if (!empty($rmstokopname_kd)) {
	extract($row);
}
?>
<button class="btn btn-default" onclick="get_rmstokopnamedetail_inside_modal('<?php echo $back_rmstokopnamedetail_kd; ?>')"> <i class="fa fa-arrow-left"></i> Back </button>
<button class="btn btn-default" onclick="get_rmstokopnamedetail_inside_modal('<?php echo $next_rmstokopnamedetail_kd; ?>')"> Next <i class="fa fa-arrow-right"></i> </button>
<hr>
<?php
$form_id = 'idFormInput';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>
<input type="hidden" name="txtrmstokopnamedetail_kd" value="<?php echo isset($row['rmstokopnamedetail_kd']) ? $row['rmstokopnamedetail_kd'] : null; ?>">
<div class="row">

	<div class="form-group">
		<label for="idtxtrm_kode" class="col-md-3 control-label">RM Kode</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrrm_kode"></div>
            <?php echo form_input(array('type' => 'text', 'id' => 'idtxtrm_kode', 'name' => 'txtrm_kode', 'class' => 'form-control datetimepicker', 'placeholder' => 'RM Kode', 'readonly' => 'readonly', 'value' => isset($row['rm_kode']) ? $row['rm_kode'] : null)); ?>
		</div>
	</div>

	<div class="form-group">
		<label for="idtxtrm_deskripsi" class="col-md-3 control-label">RM Deskripsi</label>
		<div class="col-md-8 col-xs-12">
			<div id="idErrrm_deskripsi"></div>
            <textarea name="txtrm_deskripsi" id="idtxtrm_deskripsi" readonly="readonly" class="form-control" cols="30" rows="2"><?php echo isset($row['rm_deskripsi']) ? $row['rm_deskripsi'].$row['rm_spesifikasi'] : null ?></textarea>
		</div>
	</div>

	<div class="form-group">
		<label for="idtxtrm_deskripsi" class="col-md-3 control-label">RM Current Stok</label>
		<div class="col-md-3 col-xs-12">
            <input type="number" class="form-control" readonly="readonly" value="<?php echo isset($row['rmstokmaster_qty']) ? $row['rmstokmaster_qty'] : null ;?>">
        </div>
		<div class="col-md-3 col-xs-12">
            <input type="text" class="form-control" readonly="readonly" value="<?php echo isset($row['rmsatuan_nama']) ? $row['rmsatuan_nama'] : null ;?>">
        </div>
	</div>

	<div class="form-group">
		<label for="idtxtrmstokopnamedetail_tgledit" class="col-md-3 control-label">Last Modified</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrrmstokopnamedetail_tgledit"></div>
            <?php echo form_input(array('type' => 'text', 'id' => 'idtxtrmstokopnamedetail_tgledit', 'name' => 'txtrmstokopnamedetail_tgledit', 'class' => 'form-control datetimepicker', 'placeholder' => 'RM Kode', 'readonly' => 'readonly', 'value' => isset($row['rmstokopnamedetail_tgledit']) ? $row['rmstokopnamedetail_tgledit'] : null)); ?>
		</div>
	</div>

    <hr>

    <div class="form-group">
		<label for="idtxtrmstokopnamedetail_qty" class="col-md-3 control-label">Qty Opname</label>
		<div class="col-md-3 col-xs-12">
            <input type="number" name="txtrmstokopnamedetail_qty" id="idtxtrmstokopnamedetail_qty" class="form-control clqtyopname" value="<?php echo isset($row['rmstokopnamedetail_qty']) ? $row['rmstokopnamedetail_qty'] : null ;?>">
        </div>
		<div class="col-md-3 col-xs-12">
            <select name="txtrmstokopnamedetail_satuan" id="idtxtrmstokopnamedetail_satuan" class="form-control">
                <option value="<?php echo isset($row['rmstokopnamedetail_satuan']) ? $row['rmstokopnamedetail_satuan'] : null?>" selected><?php echo isset($row['satuan_opname_nama']) ? $row['satuan_opname_nama'] : null; ?></option>
            </select>
        </div>
	</div>

	<div class="form-group">
		<label for="idtxtrmstokopnamedetail_konversi" class="col-md-3 control-label">Konversi</label>
		<div class="col-md-3 col-xs-12">
            <input type="number" name="txtrmstokopnamedetail_konversi" id="idtxtrmstokopnamedetail_konversi" class="form-control clqtyopname" value="<?php echo isset($row['rmstokopnamedetail_konversi']) ? $row['rmstokopnamedetail_konversi'] : 1; ?>">
        </div>
		<div class="col-md-3 col-xs-12">
            <input type="number" name="txtrmstokopnamedetail_qtykonversi" id="idtxtrmstokopnamedetail_qtykonversi" class="form-control" readonly="readonly" value="<?php echo isset($row['rmstokopnamedetail_qtykonversi']) ? $row['rmstokopnamedetail_qtykonversi'] : 1; ?>">
        </div>
	</div>


	<div class="form-group">
		<label for="idtxtrmstokopnamedetail_remark" class="col-md-3 control-label">Remark</label>
		<div class="col-sm-6 col-xs-12">
			<textarea name="txtrmstokopnamedetail_remark" class="form-control" id="idtxtrmstokopnamedetail_remark" rows="2" placeholder="Remark"><?php echo isset($row['rmstokopnamedetail_remark']) ? $row['rmstokopnamedetail_remark']: null;?></textarea>
		</div>
	</div>

</div>

<hr>
<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-4 col-md-offset-2 col-xs-12">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitDataOpnameDetail('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button type="reset" name="btnReset" onclick="event.preventDefault(); toggle_modal('','')" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>