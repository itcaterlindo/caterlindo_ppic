<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}

	.select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>
<table id="idTable" class="table table-bordered table-striped table-hover display responsive" style="width:100%;">
	<thead>
		<tr>
			<th style="width:1%; text-align:center;">No.</th>
			<th style="width:1%; text-align:center;"> Opsi </th>
			<th style="width:4%; text-align:center;">RM Kode</th>
			<th style="width:10%; text-align:center;">RM Deskripsi</th>
			<th style="width:3%; text-align:center;">Last Opname Tgl</th>
			<th style="width:3%; text-align:center;">Last Opname Qty</th>
			<th style="width:3%; text-align:center;">Current Stok</th>
			<th style="width:3%; text-align:center;">Stok Min</th>
			<th style="width:3%; text-align:center;">Stok Min Suggest</th>
			<th style="width:3%; text-align:center;">SO Kartu Stok</th>
			<th style="width:3%; text-align:center;">Satuan</th>
			<th style="width:3%; text-align:center;">Last Modified</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		foreach ($results as $r) : 
			$bgcolorCurstock = '';
			$bgcolorLaststock = '';
			if($r['rmstokmaster_qty'] < 0) {
				$bgcolorCurstock = 'orangered';
			}
			if( ROUND($r['rmstokmaster_qty'], 2) !== ROUND($r['last_stock'], 2) ){
				$bgcolorLaststock = 'orangered';
			}
		?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php
					$btns = array();
					$btns[] = get_btn(array('title' => 'Detail Mutasi', 'icon' => 'list', 'onclick' => 'view_detailmutasi(\'' . $r['rmstokmaster_kd'] . '\')'));
					$btns[] = get_btn(array('title' => 'Recalculate Current Stock', 'icon' => 'search', 'onclick' => 'action_recalculate_stok(\'' . $r['rmstokmaster_kd'] . '\')'));
					$btn_group = group_btns($btns);
					echo $btn_group;
					?></td>
				<td><?php echo $r['rm_kode'] . ' / ' . $r['rm_oldkd']; ?></td>
				<td><?php echo $r['rm_deskripsi'] . ' ' . $r['rm_spesifikasi']; ?></td>
				<td><?php echo $r['rmstokmaster_stokopname_tgl']; ?></td>
				<td style="text-align: right;"><?php echo $r['rmstokmaster_stokopname_qty']; ?></td>
				<td style="text-align: right; background-color: <?php echo $bgcolorCurstock; ?>;"><?php echo $r['rmstokmaster_qty']; ?></td>
				<td><?php echo $r['rm_stock_min']; ?></td>
				<td><?php echo round($r['suggest']); ?></td>
				<td style="background-color: <?php echo $bgcolorLaststock; ?>;"><?php echo $r['last_stock']; ?></td>
				<td><?php echo $r['rmsatuan_nama']; ?></td>
				<td><?php echo $r['rmstokmaster_tgledit']; ?></td>
			</tr>
		<?php
			$no++;
		endforeach; ?>
	</tbody>
</table>

<script type="text/javascript">
	var table = $('#idTable').DataTable({
		"fixedHeader": true,
		"dom": 'Bfrtip',
		"serverSide": false,
		"paging": false,
		"searching": true,
		"ordering": true,
		"info": false,
		"language": {
			"lengthMenu": "Tampilkan _MENU_ data",
			"zeroRecords": "Maaf tidak ada data yang ditampilkan",
			"info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty": "Tidak ada data yang ditampilkan",
			"search": "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing": "Sedang Memproses...",
			"paginate": {
				"first": '<span class="fas fa-fast-backward"></span>',
				"last": '<span class="fas fa-fast-forward"></span>',
				"next": '<span class="fas fa-forward"></span>',
				"previous": '<span class="fas fa-backward"></span>'
			}
		},
		// "order":[9, 'desc'],
		"buttons": [{
			"extend": "excelHtml5",
			"title": "RM Stok",
			"exportOptions": {
				"columns": [0, 2, 3, 4, 5, 6, 7, 8]
			}
		}],
	});
</script>