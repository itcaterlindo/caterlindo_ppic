<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	$(document).ready(function() {
		first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');
		table_main();
	});

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function table_main() {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/table_main'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').slideDown().html(html);
					moveTo('idMainContent');
				}
			});
		});
	}

	function table_kartustock(startdate, enddate, rm_kd) {
		var jnstransaksi = 'ALL';
		var jnssatuan = '';
		var html = '';
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . 'inventory/raw_material/rawmaterial_kartu_stock/table_main'; ?>',
			data: {
				'startdate': startdate,
				'enddate': enddate,
				'jnstransaksi': jnstransaksi,
				'jnssatuan': jnssatuan,
				'rm_kd': rm_kd
			},
			success: function(html) {
				toggle_modal('Kartu Stok', html);
			}
		});
		return html;
	} 

	function view_detailmutasi(rmstokmaster_kd){
		event.preventDefault();
		var enddate = '<?php echo date('Y-m-d H:i:s'); ?>';
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link.'/get_rmstokmaster'; ?>',
			data: {
				'rmstokmaster_kd': rmstokmaster_kd
			},
			success: function(data) {
				var resp = JSON.parse(data);
				table_kartustock(resp.rmstokmaster_stokopname_tgl, enddate, resp.rm_kd);
			}
		});
	}

	function action_recalculate_stok (rmstokmaster_kd) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link.'/action_recalculate_stok'; ?>',
				data: {
					'rmstokmaster_kd': rmstokmaster_kd
				},
				success: function(data) {
					var resp = JSON.parse(data);
					if (resp.code == 200) {
						notify(resp.status, resp.pesan, 'success');
						window.location.reload();
					}else{
						notify(resp.status, resp.pesan, 'error');
					}
				}
			});
		}
	}

	function toggle_modal(modalTitle, htmlContent) {
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitDataOpnameDetail(form_id) {
		event.preventDefault();
		var form = document.getElementById(form_id);

		$.ajax({
			url: "<?php echo base_url() . $class_link; ?>/action_stokopnamdedetail_update",
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				var resp = JSON.parse(data);
				console.log(data);
				if (resp.code == 200) {
					notify(resp.status, resp.pesan, 'success');
					get_rmstokopnamedetail_inside_modal(nextPosID);
					// toggle_modal('', '');
					$('.errInput').html('');
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
					moveTo('idMainContent');
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
			}
		});
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			delay: 2500,
			styling: 'bootstrap3'
		});
	}

</script>