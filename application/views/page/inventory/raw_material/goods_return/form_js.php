<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form();
	open_table_box('<?php echo date('Y-m-d'); ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_main'; ?>',
			data: {sts:'add', id:''},
			success: function(html) {
			}
		});
	});

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		window.location.assign('<?php echo base_url();?>/purchasing/purchase_manage/purchasing_manage_main');		
	});

	$(document).off('keyup', '#idFormInput').on('keyup', '#idFormInput', function(e) {
		if (e.which == 13) {
			submitData();
			return false;
		}
	});

	$(document).off('click', '#idbtnpilih').on('click', '#idbtnpilih', function() {
		event.preventDefault();
		var po_kd = $('#idtxtpo_kd').val();
		if (po_kd == ''){
			alert('Pilih PO');
		}else{
			box_overlay('in');
			open_table_grdetail (po_kd);
			box_overlay('out');
		}
	});

	function open_table_grdetail (po_kd) {
		$('#idtablepo').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/grdetail_table'; ?>',
				data: {po_kd:po_kd},
				success: function(html) {
					$('#idtablepo').html(html);
					$('#idtablepo').slideDown();
				}
			});
		});
	}	

	function open_form(){
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/form_main'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					render_datetime('datetimepicker', 'DD-MM-YYYY HH:mm:ss');
					render_datetime('datepicker', 'DD-MM-YYYY');
					moveTo('idMainContent');
					render_po();
				}
			});
		});
	}

	function render_po() {
		$("#idtxtpo_kd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 3,
			ajax: { 
				url: '<?= base_url().$class_link ?>/get_poreceived',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramPO: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_datetime(valClass, valFormat){
		$('.'+valClass).datetimepicker({
            format: valFormat,
        });
	}

	function open_table_box(date) {
		$('#idTableBox<?php echo $master_var;?>').remove();
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_box'; ?>',
				data: {date:date},
				success: function(html) {
					$('#idMainContent').append(html);
				}
			});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitData() {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById('idFormInput');
		var po_kd = $('#idtxtpo_kd').val();

		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_insert_batch" ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					open_table_grdetail (po_kd);
					open_table_box('<?php echo date('Y-m-d'); ?>');
					$('.errInput').html('');
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
					moveTo('idMainContent');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function resetFormMain() {
		// $('.tt-input').val('');
		// $('#idtxtpodetail_kd').val('').trigger('change');
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
		box_overlay('out');
    }

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

</script>