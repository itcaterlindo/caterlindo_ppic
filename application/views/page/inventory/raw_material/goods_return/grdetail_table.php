<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<input type="hidden" name="txtpo_kd" value="<?php echo $po_kd; ?>">
<table id="idTableGR" class="table table-bordered table-striped" style="width:100%; font-size: 95%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;">No</th>
		<th style="width:7%; text-align:center;">Tgl Datang</th>
		<th style="width:7%; text-align:center;">No Srj</th>
		<th style="width:5%; text-align:center;">Kode Mateial</th>
		<th style="width:20%; text-align:center;">Deskripsi Material</th>
		<th style="width:7%; text-align:center;">Qty PO</th>
		<th style="width:7%; text-align:center;">Qty Receive</th>
		<th style="width:10%; text-align:center;">Keterangan</th>
		<th style="width:5%; text-align:center;">Qty Return </th>
	</tr>
	</thead>
    <tbody>
        <?php 
        $no = 1;
		foreach ($results as $each) :
		?>
        <tr>
            <td class="dt-center"> <?php echo $no; ?> </td>
            <td><?php echo format_date($each['rmgr_tgldatang'], 'Y-m-d H:i:s'); ?></td>
            <td><?php echo $each['rmgr_nosrj']; ?></td>
            <td><?php echo $each['rm_kode'].' | '.$each['rm_oldkd']; ?></td>
            <td><?php echo $each['podetail_deskripsi'].'/'.$each['podetail_spesifikasi']; ?></td>
            <td class="dt-right">
            <?php 
                echo (float) $each['podetail_qty'];?>
            </td>
            <td class="dt-right">
            <?php 
                echo (float) $each['rmgr_qty'];?>
            </td>
			<td><?php echo $each['rmgr_ket']; ?></td>
			<td>
				<input type="hidden" name="txtrmgr_kds[]" value="<?php echo $each['rmgr_kd']?>">
				<input type="hidden" name="txtrm_kds[<?php echo $each['rmgr_kd']; ?>]" value="<?php echo $each['rm_kd']?>">
				<input type="text" name="txtrmgr_qtys[<?php echo $each['rmgr_kd']; ?>]" class="form-control input-sm" placeholder="Qty" >
			</td>
        </tr>
        <?php 
        $no ++;
        endforeach;?>
    </tbody>
</table>

<script type="text/javascript">
	render_dt('#idTableGR');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"paging": false,
			"ordering": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
		});
	}
</script>