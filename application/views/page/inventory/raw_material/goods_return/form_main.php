<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>

<div class="row">
	<div class="form-group">
		<label for='idtxtrmgr_tgldatang' class="col-md-2 control-label">Tgl Return</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrrmgr_tgldatang"></div>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				<?php echo form_input(array('type' => 'text', 'id'=> 'idtxtrmgr_tgldatang', 'name' => 'txtrmgr_tgldatang', 'class'=> 'form-control datetimepicker', 'placeholder' => 'Tgl Datang', 'value' => date('d-m-Y H:i:s'))); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtpo_kd' class="col-md-2 control-label">No PO</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpo_kd"></div>
			<select id="idtxtpo_kd" class="form-control"> </select>
		</div>
		<div class="col-sm-1 col-xs-12">
			<button class="btn btn-sm btn-success" id="idbtnpilih"> <i class="fa fa-check-circle"></i> Pilih </button>
		</div>
	</div>

</div>

<div class="row">
	<div class="col-md-12">
		<div id="idtablepo"></div>
	</div>
</div>

<hr>
<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-2 col-sm-offset-10 col-xs-12">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData()" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button type="reset" name="btnReset" onclick="resetFormMain()" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	$('[data-mask]').inputmask();
</script>
