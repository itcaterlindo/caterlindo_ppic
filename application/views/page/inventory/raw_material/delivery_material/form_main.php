<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>

<div class="row">
	<div class="form-group">
		<label for='idtxtdeliverymaterial_tanggal' class="col-md-2 control-label">Tgl Kirim</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrdeliverymaterial_tanggal"></div>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				<?php echo form_input(array('type' => 'text', 'id' => 'idtxtdeliverymaterial_tanggal', 'name' => 'txtdeliverymaterial_tanggal', 'class' => 'form-control datepicker', 'placeholder' => 'Tgl Surat Jalan', 'value' => date('Y-m-d'))); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtrm_jenis' class="col-md-2 control-label">Code SRJ</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrCodeSrj"></div>
			<select id="idTxtCodeSrj" name="txtCodeSrj" class="form-control" data-allow-clear="true"> </select>
			* Diisi untuk delivery material / boleh dikosongkan
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtrm_jenis' class="col-md-2 control-label">Jenis Material</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrpo_kd"></div>
			<select id="idtxtrm_jenis" class="form-control"> 
				<option value="STD">Standart</option>
				<option value="CUSTOM">Custom</option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtrm_opsi' class="col-md-2 control-label">Raw Material</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrrm_kd"></div>
			<input type="hidden" id="idtxtrm_kd" name="txtrm_kd" class="clMaterial" placeholder="idtxtrm_kd">
			<input type="hidden" id="idtxtrmgr_kd" name="txtrmgr_kd" class="clMaterial" placeholder="idtxtrmgr_kd">
			<input type="hidden" id="idtxtpodetail_kd" name="txtpodetail_kd" class="clMaterial" placeholder="idtxtpodetail_kd">
			<input type="hidden" id="idtxtdeliverymaterial_satuan_kd" name="txtdeliverymaterial_satuan_kd" class="clMaterial" placeholder="idtxtdeliverymaterial_satuan_kd">
			<select id="idtxtrm_opsi" class="form-control"> </select>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtrm_deskripsi' class="col-md-2 control-label">Deskripsi Material</label>
		<div class="col-sm-6 col-xs-12">
			<div class="errInput" id="idErrrm_deskripsi"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control clMaterial', 'name'=> 'txtrm_deskripsi', 'id'=> 'idtxtrm_deskripsi', 'placeholder' =>'Deskripsi Material', 'readonly' => 'readonly', 'rows' => '2', 'value'=> isset($rm_deskripsi) ? $rm_deskripsi: null ));?>
		</div>
	</div>
	<div class="form-group">
		<label for='idtxtdeliverymaterial_qty' class="col-md-2 control-label">Qty</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrdeliverymaterial_qty"></div>
			<div class="errInput" id="idErrdeliverymaterial_check_stock"></div>
			<?php echo form_input(array('type' => 'number', 'id' => 'idtxtdeliverymaterial_qty', 'name' => 'txtdeliverymaterial_qty', 'class' => 'form-control', 'placeholder' => 'Qty')); ?>
		</div>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrrmsatuan_nama"></div>
			<?php echo form_input(array('type' => 'text', 'id' => 'idtxtrmsatuan_nama', 'name' => 'txtrmsatuan_nama', 'class' => 'form-control clMaterial', 'readonly' => 'readonly', 'placeholder' => 'Satuan')); ?>
		</div>
	</div>

	<!-- Tampilkan berdasarkan setting user FG warehouse -->
	<div class="form-group">
		<label for='idtxtdeliverymaterial_qty' class="col-md-2 control-label">Dari Gudang</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrdeliverymaterial_qty"></div>
			 <select name="txtname_from_gudang" id="idtxtname_from_gudang" class="form-control">
				<?php foreach($gudang as $key => $val){ ?>
					<option value="<?= $val['kd_gudang'] ?>"><?= $val['nm_gudang'] ?></option>
				<?php } ?>
			 </select>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtdeliverymaterial_qty' class="col-md-2 control-label">Stock di Gudang</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrdeliverymaterial_qty"></div>
			<?php echo form_input(array('type' => 'number', 'id' => 'idtxtstockl_qty', 'name' => 'txtstockl_qty', 'class' => 'form-control', 'readonly' => 'readonly', 'placeholder' => 'Stock qty', 'value' => 0)); ?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtdeliverymaterial_qty' class="col-md-2 control-label">Stock by SRJ</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrdeliverymaterials_stock_srj"></div>
			<?php echo form_input(array('type' => 'number', 'id' => 'idtxtstock_srj', 'name' => 'txtstock_srj', 'class' => 'form-control', 'readonly' => 'readonly', 'placeholder' => 'Stock by SRJ', 'value' => 0)); ?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtbagian_kd' class="col-md-2 control-label">Ke Gudang</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrbagian_kd"></div>
			<select id="idtxtbagian_kd" name="txtbagian_kd" class="form-control"> </select>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtdeliverymaterial_remark' class="col-md-2 control-label">Remark</label>
		<div class="col-sm-6 col-xs-12">
			<div class="errInput" id="idErrdeliverymaterial_remark"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtdeliverymaterial_remark', 'id'=> 'idtxtdeliverymaterial_remark', 'placeholder' =>'Deskripsi Material', 'rows' => '2', 'value'=> isset($deliverymaterial_remark) ? $deliverymaterial_remark: null ));?>
		</div>
	</div>

	<div class="form-group">
		<label for="chkInventoryItem" class="col-md-2 control-label">Inventory Item</label>
		<div class="col-sm-6 col-xs-12">
			<div class="errInput" id="idErrInventoryItem"></div>
			<div class="checkbox">
				<label>
					<?php echo form_checkbox(['name' => 'chkInventoryItem', 'id' => 'idChkInventoryItem', 'value' => 1, 'checked' => FALSE]); ?> * Silahkan centang jika ingin mengurangi QTY di <b>KARTU STOCK</b>, stock di gudang tidak boleh 0
				</label>
			</div>
		</div>
	</div>

</div>

<div class="row">
	<div class="col-md-12">
		<div id="idtablepo"></div>
	</div>
</div>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-2 col-sm-offset-2 col-xs-12">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData('<?php echo $form_id?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button type="reset" name="btnReset" onclick="resetFormMain()" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	$('[data-mask]').inputmask();
</script>