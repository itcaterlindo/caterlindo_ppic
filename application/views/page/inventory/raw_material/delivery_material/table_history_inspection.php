<div class="row">
    <div class="col-md-3">
        <label for="">No. Delivery Material</label>
    </div>
    <div class="col-md-9">
        <label for="">: <?= $inspection_rawmaterial[0]['deliverymaterial_no'] ?></label>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <label for="">Material</label>
    </div>
    <div class="col-md-9">
        <label for="">: <?= $inspection_rawmaterial[0]['rm_kode']." / ".$inspection_rawmaterial[0]['rm_deskripsi']." / ".$inspection_rawmaterial[0]['rm_spesifikasi'] ?></label>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <label for="">Qty Kirim</label>
    </div>
    <div class="col-md-9">
        <label for="">: <?= $inspection_rawmaterial[0]['deliverymaterial_qty'] ?></label>
    </div>
</div>

<br>
<table class="table">
    <thead>
        <tr>
            <th>No.</th>
            <th>No. Inspection Material</th>
            <th>Qty</th>
            <th>Qty Reject</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
       <?php $no = 1; foreach($inspection_rawmaterial as $key => $value){ ?>
            <tr>
                <td><?= $no++; ?></td>
                <td><?= $value['inspectionrawmaterial_no'] ?></td>
                <td><?= $value['inspectionrawmaterial_qty'] ?></td>
                <td><?= $value['inspectionrawmaterial_qty_reject'] ?></td>
                <td><?= build_span($value['inspectionrawmaterialstate_span'], $value['inspectionrawmaterialstate_nama']) ?></td>
            </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td class="text-center" colspan="2">TOTAL</td>
            <td> <?= array_sum(array_column($inspection_rawmaterial, 'inspectionrawmaterial_qty')) ?></td>
            <td> <?= array_sum(array_column($inspection_rawmaterial, 'inspectionrawmaterial_qty_reject')) ?></td>
            <td>&nbsp;</td>
        </tr>
    </tfoot>
</table>