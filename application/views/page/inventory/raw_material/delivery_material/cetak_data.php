<?php

class customPdf extends Tcpdf {
    public $date;

    public function setsHeader($headerData){
        $this->date = $headerData['date'];
    }

    public function Header() {
        $header = '
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td width="`20%" rowspan="3" style="text-align:left;"> <img src="assets/admin_assets/dist/img/logo_cat.png" width="95" height="30"> </td>
                <td width="47%" rowspan="3" style="font-size:16px; font-weight:bold; text-align:center;">GENERAL MATERIAL OUT</td>
                <td width="17%" style="font-size:10px; text-align:left; border-bottom: 1px solid black;">No. Dokumen</td>
                <td width="15%" style="font-size:10px; text-align:left; border-bottom: 1px solid black;">: CAT4-045</td>
            </tr>
            <tr>
                <td style="font-size:10px; text-align:left; border-bottom: 1px solid black;">Tanggal Terbit</td>
                <td style="font-size:10px; text-align:left; border-bottom: 1px solid black;">: 01-Des-2018</td>
            </tr>
            <tr>
                <td style="font-size:10px; text-align:left; border-bottom: 1px solid black;">Revisi</td>
                <td style="font-size:10px; text-align:left; border-bottom: 1px solid black;">: 02</td>
            </tr>

        </table>';

        $keterangan =
        '<table cellspacing="0" cellpadding="0" border="0" style="font-size:10px;">
            <tr>
                <td width="8%" style="text-align:left;">Date</td>
                <td style="text-align:left;"> : '.format_date($this->date, 'd-m-Y').' </td>
                <td width="45%"></td>
                <td style="text-align:right;">No</td>
                <td width="5%"></td>
                <td style="">: </td>
            </tr>       

        </table>';

        $this->writeHTML($header, true, false, false, false, '');
        $this->writeHTML($keterangan, true, false, false, false, '');
    }

    public function Footer() {
        $ttd = '<table border="0" style="text-align: center; font-size: 75%; margin-top: 100%">
                    <tr>
                        <td width="26%">  </td>
                        <td width="11%"> </td>
                        <td width="26%">  </td>
                        <td width="11%"> </td>
                        <td width="26%"></td>
                    </tr>
                    <tr>
                        <td width="26%"></td>
                        <td width="11%"></td>
                        <td width="26%"></td>
                        <td width="11%"></td>
                        <td width="26%"></td>
                    </tr>
                    <tr>
                        <td width="26%"></td>
                        <td width="11%"></td>
                        <td width="26%"></td>
                        <td width="11%"></td>
                        <td width="26%"></td>
                    </tr>
                    <tr>
                        <td width="26%"></td>
                        <td width="11%"></td>
                        <td width="26%"></td>
                        <td width="11%"></td>
                        <td width="26%"></td>
                    </tr>
                    <tr>
                        <td width="26%"></td>
                        <td width="11%"></td>
                        <td width="26%"></td>
                        <td width="11%"></td>
                        <td width="26%"></td>
                        
                    </tr>
                </table>';

        $this->writeHTML($ttd, true, false, false, false, '');
    }


}

$pdf = new customPdf('P', 'mm', 'A4', true, 'UTF-8', false);
$title = 'GRO_'.$date;
$pdf->SetTitle($title);
$pdf->SetAuthor('Caterlindo ERP System');
$pdf->SetDisplayMode('real', 'default');
$pdf->SetAutoPageBreak(true, 15);
/** Margin */ 
$pdf->SetHeaderMargin(5);
$pdf->SetFooterMargin(50);
$pdf->SetMargins(8, 35, 8);
$pdf->setPrintFooter(true);

/** PR Note */
$dataHeader = array(
    'date' => $date
);
$pdf->setsHeader($dataHeader);

$pdf->AddPage();

$konten =
        '<table width="100%" cellspacing="0" cellpadding="1" border="1" style="font-size:10px">
            <tr style="text-align:center; font-weight:bold;">
                <th width="3%" >No</th>
                <th width="27%" >Material</th>
                <th width="10%">Mtrl Code</th>
                <th width="10%">Old Mtrl Code</th>
                <th width="6%">Qty</th>
                <th width="7%">Satuan</th>
                <th width="12%">Division</th>
                <th width="14%">Remark</th>
                <th width="12%">Signature</th>
            </tr>';

/** Result Data */
if (!empty($data_general)){
    $no = 1; 
foreach($data_general as $eachPR) :
    $desc = ($eachPR->rm_spesifikasi != '' ) ? $eachPR->rm_spesifikasi : $eachPR->rm_deskripsi;
    $desc = ($desc != '' ) ? $desc : $eachPR->podetail_deskripsi;
    $konten .=
            '<tr style="font-size: 9px;">
                <td style="text-align:center;">'. $no .'</td>
                <td style="text-align:left;"> '.$eachPR->rm_nama.'/'. $desc .' </td>
                <td style="text-align:center;"> '.$eachPR->rm_kode.' </td>
                <td style="text-align:center;"> '.$eachPR->rm_oldkd.' </td>
                <td style="text-align:center;">'.$eachPR->materialreqgeneral_qty.'</td>
                <td style="text-align:center;">'. $eachPR->rmsatuan_nama .'</td>
                <td style="text-align:center;">'.$eachPR->bagian_nama.'</td>
                <td style="text-align:center;">'. $eachPR->materialreqgeneral_remark .'</td>
                <td style="text-align:left;"></td>
            </tr>';
            $no++;
endforeach;

$konten .=
        '</table>
        <br>
        <br>
        <table border="0" style="text-align: center; font-size: 75%; margin-top: 100%">
                    <tr>
                        <td width="26%">  </td>
                        <td width="11%"> </td>
                        <td width="26%">  </td>
                        <td width="11%"> </td>
                        <td width="26%">'. $_SESSION['tipe_admin'] .'</td>
                    </tr>
                    <tr>
                        <td width="26%"></td>
                        <td width="11%"></td>
                        <td width="26%"></td>
                        <td width="11%"></td>
                        <td width="26%"></td>
                    </tr>
                    <tr>
                        <td width="26%"></td>
                        <td width="11%"></td>
                        <td width="26%"></td>
                        <td width="11%"></td>
                        <td width="26%"></td>
                    </tr>
                    <tr>
                        <td width="26%"></td>
                        <td width="11%"></td>
                        <td width="26%"></td>
                        <td width="11%"></td>
                        <td width="26%"></td>
                    </tr>
                    <tr>
                        <td width="26%"></td>
                        <td width="11%"></td>
                        <td width="26%"></td>
                        <td width="11%"></td>
                        <td width="26%">(_______________________)<br>'. $_SESSION['nm_admin'] .'</td>
                        
                    </tr>
                </table>';

$pdf->writeHTML($konten, true, false, false, false, '');

}

$txtOutput = $title.'.pdf';
$pdf->Output($txtOutput, 'I');