<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form('<?php echo isset($sts) ? $sts: null; ?>', '<?php echo isset($id) ? $id: null; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_form(sts = null, id = null){
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/formmaster_main'; ?>',
				data: {sts:sts, id:id},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					render_datetime('datetimepicker', 'YYYY-MM-DD');
					render_karyawan();
					render_suplier();
					moveTo('idMainContent');
				}
			});
		});
	}

	function render_karyawan(selected = null) {
		$("#idtxtkd_karyawan").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 2,
			ajax: { 
				url: '<?php echo base_url().'auto_complete' ?>/get_karyawan',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramKaryawan: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
		if (selected){
			var parseSelected = JSON.parse(selected);
			$("#idtxtkd_karyawan").append($('<option></option>').val(parseSelected.id).html(parseSelected.text));
		}
	}

	function render_suplier(selected = null) {
		$("#idtxtsuplier_kd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 2,
			ajax: { 
				url: '<?php echo base_url().'auto_complete' ?>/get_suplier',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramSuplier: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
		if (selected){
			var parseSelected = JSON.parse(selected);
			$("#idtxtsuplier_kd").append($('<option></option>').val(parseSelected.id).html(parseSelected.text));
		}
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitData() {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById('idFormInput');
		var po_kd = $('#idtxtpo_kd').val();

		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_insert_batch" ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					open_table_grdetail (po_kd);
					open_table_box('<?php echo date('Y-m-d'); ?>');
					$('.errInput').html('');
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
					moveTo('idMainContent');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function submitDataMaster(form_id) {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById(form_id);

		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_insert_master" ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					generateToken (resp.csrf);
					window.location.assign('<?php echo base_url().$class_link?>/form_box?id='+resp.data.whdeliveryorder_kd);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
					moveTo('idMainContent');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

</script>