<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
if (isset($row)){
    extract($row);
}
$form_id = 'idForm';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtsts', 'placeholder' => 'Sts', 'value' => isset($sts) ? $sts : null ));
echo form_input(array('type' => 'hidden', 'name' => 'txtwhdeliveryorder_kd', 'placeholder' => 'Id', 'value' => isset($id) ? $id : null ));
echo form_input(array('type' => 'hidden', 'name' => 'txtwhdeliveryorderdetail_kd', 'placeholder' => 'txtwhdeliveryorderdetail_kd', 'value' => isset($whdeliveryorderdetail_kd) ? $whdeliveryorderdetail_kd : null ));
?>

<div class="row">
    <div class="form-group">
		<label for='idtxtrm_kd' class="col-md-2 control-label">Material</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrrm_kd"></div>
			<select id="idtxtrm_kd" name="txtrm_kd" class="form-control"> </select>
		</div>
	</div>
	<div class="form-group">
		<label for='idtxtwhdeliveryorderdetail_nama' class="col-md-2 control-label">Nama</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrwhdeliveryorderdetail_nama"></div>
			<?php echo form_input(array('type' => 'text', 'id'=> 'idtxtwhdeliveryorderdetail_nama', 'name' => 'txtwhdeliveryorderdetail_nama', 'readonly' => 'readonly', 'class'=> 'form-control', 'placeholder' => 'Nama', 'value' => isset($whdeliveryorderdetail_nama) ? $whdeliveryorderdetail_nama : null )); ?>
		</div>
	</div>
    <div class="form-group">
		<label for="idtxtwhdeliveryorderdetail_deskripsi" class="col-md-2 control-label">Deskripsi</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrwhdeliveryorderdetail_deskripsi"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtwhdeliveryorderdetail_deskripsi', 'readonly' => 'readonly', 'id'=> 'idtxtwhdeliveryorderdetail_deskripsi', 'placeholder' =>'Deskripsi', 'rows' => '2', 'value'=> isset($whdeliveryorderdetail_deskripsi) ? $whdeliveryorderdetail_deskripsi: null ));?>
		</div>
	</div>
    <div class="form-group">
		<label for="idtxtwhdeliveryorderdetail_spesifikasi" class="col-md-2 control-label">Spesifikasi</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrwhdeliveryorderdetail_spesifikasi"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtwhdeliveryorderdetail_spesifikasi', 'readonly' => 'readonly', 'id'=> 'idtxtwhdeliveryorderdetail_spesifikasi', 'placeholder' =>'Spesifikasi', 'rows' => '2', 'value'=> isset($whdeliveryorderdetail_spesifikasi) ? $whdeliveryorderdetail_spesifikasi: null ));?>
		</div>
	</div>
	<div class="form-group">
		<label for='idtxtwhdeliveryorderdetail_qty' class="col-md-2 control-label">Qty</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrwhdeliveryorderdetail_qty"></div>
			<?php echo form_input(array('type' => 'number', 'id'=> 'idtxtwhdeliveryorderdetail_qty', 'name' => 'txtwhdeliveryorderdetail_qty', 'class'=> 'form-control', 'placeholder' => 'Qty', 'value' => isset($whdeliveryorderdetail_qty) ? $whdeliveryorderdetail_qty: null )); ?>
		</div>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrwhdeliveryorderdetail_satuankd"></div>
            <select name="txtwhdeliveryorderdetail_satuankd" id="idtxtwhdeliveryorderdetail_satuankd" class="form-control"> </select>
		</div>
	</div>
	<div class="form-group">
		<label for='idtxtwhdeliveryorderdetail_konversi' class="col-md-2 control-label">Konversi</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrwhdeliveryorderdetail_konversi"></div>
			<?php echo form_input(array('type' => 'number', 'id'=> 'idtxtwhdeliveryorderdetail_konversi', 'name' => 'txtwhdeliveryorderdetail_konversi', 'class'=> 'form-control', 'placeholder' => 'Konversi', 'value' => isset($whdeliveryorderdetail_konversi) ? $whdeliveryorderdetail_konversi: null )); ?>
		</div>
		<div class="col-sm-3 col-xs-12">
			<?php echo form_input(array('type' => 'text', 'id'=> 'idtxtwhdeliveryorderdetail_konversisatuan', 'name' => 'txtwhdeliveryorderdetail_konversisatuan', 'readonly' => 'readonly', 'class'=> 'form-control', 'placeholder' => 'Satuan RM', 'value' => isset($whdeliveryorderdetail_konversi) ? $whdeliveryorderdetail_konversi: null )); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="idtxtwhdeliveryorderdetail_remark" class="col-md-2 control-label">Remark</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrwhdeliveryorderdetail_remark"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtwhdeliveryorderdetail_remark', 'id'=> 'idtxtwhdeliveryorderdetail_remark', 'placeholder' =>'Remark', 'rows' => '2', 'value'=> isset($whdeliveryorderdetail_remark) ? $whdeliveryorderdetail_remark: null ));?>
		</div>
	</div>
	<div class="form-group">
		<label for='idtxtwhdeliveryorderdetail_reducestok' class="col-md-2 control-label">Ambil Stok</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrwhdeliveryorderdetail_reducestok"></div>
			<?php echo form_dropdown('txtwhdeliveryorderdetail_reducestok', [1 => 'Ya', 0 =>'Tidak'], isset($whdeliveryorderdetail_reducestok)? $whdeliveryorderdetail_reducestok : '', array('class'=> 'form-control', 'id'=> 'idtxtwhdeliveryorderdetail_reducestok'));?>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="form-group pull-right">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button onclick="event.preventDefault();window.location.reload();" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
			<button onclick="event.preventDefault();window.location.assign('<?php echo base_url().$class_link; ?>');" class="btn btn-default btn-sm">
				<i class="fa fa-check"></i> Selesai
			</button>
	</div>
</div>
<hr>
<?php echo form_close(); ?>

<script type="text/javascript">
    render_rm();
    render_satuan();
<?php 
    if (isset($row)) :
?>
    $("#idtxtwhdeliveryorderdetail_satuankd").select2("trigger", "select", { 
        data: { 
            id: '<?php echo $whdeliveryorderdetail_satuankd; ?>', 
            text: '<?php echo $rmsatuan_nama; ?>', 
        }
    });
    $("#idtxtrm_kd").select2("trigger", "select", { 
        data: { 
            id: '<?php echo $rm_kd; ?>', 
            text: '<?php echo $rm_kode.' | '.$whdeliveryorderdetail_deskripsi; ?>', 
            rm_nama: '<?php echo $whdeliveryorderdetail_nama; ?>', 
            rm_deskripsi: '<?php echo $whdeliveryorderdetail_deskripsi; ?>', 
            rm_spesifikasi: '<?php echo $whdeliveryorderdetail_spesifikasi; ?>', 
            rmsatuan_kd: '<?php echo $whdeliveryorderdetail_satuankd; ?>', 
            rmsatuan_nama: '<?php echo $rmsatuan_nama; ?>', 
        }
    });
<?php
    endif;
?>
</script>
