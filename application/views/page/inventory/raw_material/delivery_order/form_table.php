<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<table id="idTableGR" class="table table-bordered table-striped" style="width:100%; font-size: 100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;">No</th>
		<th style="width:1%; text-align:center;">Opsi</th>
		<th style="width:7%; text-align:center;">Kode RM</th>
		<th style="width:10%; text-align:center;">Deskripsi</th>
		<th style="width:5%; text-align:center;">Qty</th>
		<th style="width:5%; text-align:center;">Satuan</th>
		<th style="width:5%; text-align:center;">Qty Konversi</th>
		<th style="width:3%; text-align:center;">Ambil <br> Stok</th>
		<th style="width:10%; text-align:center;">Remark</th>
		<th style="width:7%; text-align:center;">Modified</th>
	</tr>
	</thead>
    <tbody>
        <?php 
        $no = 1;
		foreach ($results as $each) :
		?>
        <tr>
            <td class="dt-center"> <?php echo $no; ?> </td>
            <td class="dt-center"> 
                <div class="btn-group">
                  <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
                    Opsi
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="javascript:void(0);" onclick="delete_item('<?php echo $each['whdeliveryorderdetail_kd']; ?>')"> <i class="fa fa-trash"></i> Hapus Item</a></li>
                  </ul>
                </div>
            </td>
            <td><?php echo $each['rm_kode'].' | '.$each['rm_oldkd']; ?></td>
            <td><?php echo $each['whdeliveryorderdetail_deskripsi'].' / '.$each['whdeliveryorderdetail_spesifikasi']; ?></td>
            <td class="dt-right">
            <?php 
                echo (float) $each['whdeliveryorderdetail_qty'];?>
            </td>
            <td><?php echo $each['rmsatuan_nama'];?></td>
            <td class="dt-right"><?php echo (float) $each['whdeliveryorderdetail_qtykonversi'];?></td>
            <td><?php echo !empty($each['whdeliveryorderdetail_reducestok']) ? 'Ya' : 'Tidak';?></td>
			<td><?php echo $each['whdeliveryorderdetail_remark']; ?></td>
			<td><?php echo $each['whdeliveryorderdetail_tgledit']; ?></td>
        </tr>
        <?php 
        $no ++;
        endforeach;?>
    </tbody>
</table>

<script type="text/javascript">
	render_dt('#idTableGR');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"paging": false,
			"ordering": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
		});
	}
</script>