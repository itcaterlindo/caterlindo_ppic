<?php

class customPdf extends Tcpdf {
    public $whdeliveryorder_to; 
    public $whdeliveryorder_tanggal; 
    public $whdeliveryorder_no;
    public $whdeliveryorder_vehiclenumber;

    function setHeaderVal ($arrayKet = []) {
        $this->whdeliveryorder_to = !empty($arrayKet['whdeliveryorder_to']) ? $arrayKet['whdeliveryorder_to'] : $arrayKet['set_name'] . '<br>'. $arrayKet['whdeliveryorder_note'];
        $this->whdeliveryorder_tanggal = !empty($arrayKet['whdeliveryorder_tanggal']) ? $arrayKet['whdeliveryorder_tanggal'] : '-';
        $this->whdeliveryorder_no = !empty($arrayKet['whdeliveryorder_no']) ? $arrayKet['whdeliveryorder_no'] : '-';
        $this->whdeliveryorder_vehiclenumber = !empty($arrayKet['whdeliveryorder_vehiclenumber']) ? $arrayKet['whdeliveryorder_vehiclenumber'] : '-';
    }

    public function Header() {
        $header = '
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td rowspan="3" colspan="2" style="font-size:8px; text-align:left;"> <img src="assets/admin_assets/dist/img/logo_cat.png" width="95" height="30">
                <br>
                Jl. Industri Trosobo No.18 <br>
                (Raya Surabaya-Mojokerto KM. 24) <br>
                Trosobo, Sidoarjo 61257, Indonesia <br>
                Telp. 62-31-787 1075, 787 2843, 787 7272 <br>
                Fax. 62-31-788 7513
                </td>
                <td></td>
                <td></td>
                <td colspan="2" style="font-size:16px; font-weight:bold; text-align:right;">DELIVERY ORDER</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td style="font-size:8px;">No Dokumen <br>Tanggal terbit <br>Rev</td>
                <td style="font-size:8px; text-align:left;">: CAT4-WRH-002 <br>: 31-05-2021 <br>: 02</td>
            </tr>
          
            <tr>
                <td></td>
                <td style="font-size:9px;">From</td>
                <td colspan="2" style="font-size:9px;">: RM. Warehouse</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td style="font-size:9px;">To</td>
                <td colspan="2" style="font-size:9px;">: '.$this->whdeliveryorder_to.'</td>
            </tr>
        </table>';

         /** Keterangan */
         $keterangan =
         '<table cellspacing="0" cellpadding="0" border="0" style="font-size: 80%;">
             <tr>
                 <td width="20%" style="text-align:left;">Date</td>
                 <td width="35%" style="text-align:left;">: '.format_date($this->whdeliveryorder_tanggal, 'd-M-Y').' </td>
                 <td width="5%"></td>
                 <td width="20%" style="text-align:left;">DO Number</td>
                 <td width="20%" style="text-align:left;">: '.$this->whdeliveryorder_no.' </td>
             </tr>
             <tr>
                 <td style="text-align:left;">Vehicle Number</td>
                 <td style="text-align:left;">: '.$this->whdeliveryorder_vehiclenumber.' </td>
                 <td></td>
                 <td style="text-align:left;">Page</td>
                 <td style="text-align:left;">: - of - </td>
             </tr>      
         </table>';
 
         
         $this->writeHTML($header, true, false, false, false, '');
         $this->writeHTML($keterangan, true, false, false, false, '');
    }

}
$get_layers = array("Copy1:Pembawa/Penerima", "Copy2:RM Warehouse", "Copy3:Finance/Purcashing", "Copy4:Security");

$pdf = new customPdf('L', 'mm', 'A5', true, 'UTF-8', false);
$title = 'WHDO_'.$master['whdeliveryorder_no'];

foreach ($get_layers as $x) {
        $pdf->SetTitle($title);
        $pdf->SetAutoPageBreak(true);
        $pdf->setPrintFooter(false);
        $pdf->SetMargins(10, 10, 10, true);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');
        /** Margin */ 
        $pdf->SetHeaderMargin(5);
        $pdf->SetFooterMargin(10);
        $pdf->SetMargins(10, 63, 5);

        $pdf->setHeaderVal($master);

        $pdf->AddPage();

        $footer = '
                <table cellspacing="0" cellpadding="0" border="0" style="font-size: 80%;">
                    <tr>
                        <td rowspan="3" width="67%">
                            <table cellspacing="0" cellpadding="0" border="1" style="font-size: 80%;" width="70%">';
        foreach ($logs as $log) {
            $footer .= 
            ' <tr>
                <td>'.$log['wfstate_nama'].' by</td>
                <td> :'.$log['nm_admin'].'</td>
                <td>'.$log['whdeliveryordertransitionlog_tglinput'].'</td>
            </tr>';
        }
                            
        $footer .=          '</table>
                        </td>
                        <td width="15%" style="text-align: center;">Received by, </td>
                        <td width="3%"></td>
                        <td width="15%" style="text-align: center;"> Security Checked, </td>
                    </tr>

                    <tr>
                        <td height="35" width="15%"></td>
                        <td height="35" width="3%"></td>
                        <td height="35" width="15%"></td>
                    </tr>
                    <tr>
                        <td width="15%" style="text-align: center;">_______________</td>
                        <td width="3%"></td>
                        <td width="15%" style="text-align: center;">_______________</td>
                    </tr>
                    <tr>
                    <td width="30%" style="text-align: left;">*'. $x .'</td>
                    <td width="3%"></td>
                    <td width="30%" style="text-align: left;"></td>
                </tr>
                
                </table>';

        $pdf->writeHTML($konten, true, false, false, false, '');
        $pdf->writeHTML($footer, true, false, false, false, '');
}


$txtOutput = $title.'.pdf';
$pdf->Output($txtOutput, 'I');