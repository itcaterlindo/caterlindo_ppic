<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
if (isset($row)){
    extract($row);
}
$form_id = 'idFormGR';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtsts', 'placeholder' => 'Sts', 'value' => isset($sts) ? $sts : null ));
echo form_input(array('type' => 'hidden', 'name' => 'txtwhdeliveryorder_kd', 'placeholder' => 'Id', 'value' => isset($id) ? $id : null ));
?>

<style type="text/css">
	.select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>

<div class="row">
    <div class="form-group">
		<label for='idtxtpo_kd' class="col-md-2 control-label">PO</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpo_kd"></div>
			<select id="idtxtpo_kd" name="txtpo_kd" class="form-control"> </select>
		</div>
	</div>

    <div id="idgrtable"></div>

</div>

<div class="col-md-12">
	<div class="form-group pull-right">
			<button type="submit" name="btnSubmit" onclick="submitDataGRBatch('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button onclick="event.preventDefault();window.location.reload();" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
			<button onclick="event.preventDefault();window.location.assign('<?php echo base_url().$class_link; ?>');" class="btn btn-default btn-sm">
				<i class="fa fa-check"></i> Selesai
			</button>
	</div>
</div>
<hr>
<?php echo form_close(); ?>