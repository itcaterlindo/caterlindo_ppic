<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
if (isset($row)){
	extract($row);
}
$form_id = 'idFormMaster';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtsts', 'placeholder' => 'Sts', 'value' => isset($sts) ? $sts : null ));
echo form_input(array('type' => 'hidden', 'name' => 'txtid', 'placeholder' => 'Id', 'value' => isset($id) ? $id : null ));
echo form_input(array('type' => 'hidden', 'name' => 'txtwhdeliveryorder_status', 'placeholder' => 'Id', 'value' => isset($whdeliveryorder_status) ? $whdeliveryorder_status : null ));
?>

<div class="row">
	<div class="form-group">
		<label for='idtxtwhdeliveryorder_no' class="col-md-2 control-label">No</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrwhdeliveryorder_no"></div>
			<?php echo form_input(array('type' => 'text', 'id'=> 'idtxtwhdeliveryorder_no', 'name' => 'txtwhdeliveryorder_no', 'class'=> 'form-control', 'readonly' => 'readonly', 'placeholder' => 'No', 'value' => isset($whdeliveryorder_no) ? $whdeliveryorder_no : null )); ?>
		</div>
	</div>
	<div class="form-group">
		<label for='idtxtwhdeliveryorder_tanggal' class="col-md-2 control-label">Tanggal</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrwhdeliveryorder_tanggal"></div>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				<?php echo form_input(array('type' => 'text', 'id'=> 'idtxtwhdeliveryorder_tanggal', 'name' => 'txtwhdeliveryorder_tanggal', 'class'=> 'form-control datetimepicker', 'placeholder' => 'Tanggal', 'readonly' => 'readonly','value' => isset($whdeliveryorder_tanggal) ? $whdeliveryorder_tanggal : date('Y-m-d'))); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for='idtxtwhdeliveryorder_vehiclenumber' class="col-md-2 control-label">Vehicle Number</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrwhdeliveryorder_vehiclenumber"></div>
			<?php echo form_input(array('type' => 'text', 'id'=> 'idtxtwhdeliveryorder_vehiclenumber', 'name' => 'txtwhdeliveryorder_vehiclenumber', 'class'=> 'form-control', 'placeholder' => 'Vehicle Number', 'value' => isset($whdeliveryorder_vehiclenumber) ? $whdeliveryorder_vehiclenumber: null )); ?>
		</div>
	</div>
	<hr>
	<div class="form-group">
		<label for="idtxtsuplier_kd" class="col-md-2 control-label">To (Suplier)</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrsuplier_kd"></div>
			<select id="idtxtsuplier_kd" name="txtsuplier_kd" class="form-control"> </select>
		</div>
		<label for="idtxtkd_karyawan" class="col-md-1 control-label">To (Karyawan)</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrkd_karyawan"></div>
			<select id="idtxtkd_karyawan" name="txtkd_karyawan" class="form-control"> </select>
		</div>
	</div>
	<div class="form-group">
		<label for="idtxtwhdeliveryorder_note" class="col-md-2 control-label">To (Nama)</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrwhdeliveryorder_note"></div>
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'set_name', 'id'=> 'idset_name', 'placeholder' =>'Name', 'rows' => '2', 'value'=> isset($set_name) ? $set_name: null ));?>
		</div>
	</div>
	<div class="form-group">
		<label for="idtxtwhdeliveryorder_note" class="col-md-2 control-label">Alamat</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrwhdeliveryorder_note"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtwhdeliveryorder_note', 'id'=> 'idtxtwhdeliveryorder_note', 'placeholder' =>'Alamat', 'rows' => '2', 'value'=> isset($whdeliveryorder_note) ? $whdeliveryorder_note: null ));?>
			<p>*Bila Form select to-supplier / to-karyawan telah di isi, maka kosongi form ini</p>
		</div>
	
	</div>
	
	<hr>
	<div class="form-group">
		<label for="idtxtwhdeliveryorder_sendback" class="col-md-2 control-label">Kirim Kembali</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrwhdeliveryorder_sendback"></div>
			<?php echo form_dropdown('txtwhdeliveryorder_sendback', [0 => 'Tidak', 1 => 'Ya'], isset($whdeliveryorder_sendback) ? $whdeliveryorder_sendback: 0, array('id' => 'idtxtwhdeliveryorder_sendback', 'class' => 'form-control select2')); ?>
		</div>
	</div>

	

</div>

<hr>
<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-2 col-sm-offset-10 col-xs-12">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitDataMaster('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>
<script type="text/javascript">
	$("#idtxtsuplier_kd").change(function (e) { 
		e.preventDefault();
		var selectedValue = $(this).find('option:selected').text();;  
		$("#idset_name").val('');

		set_name(selectedValue);
		// $("#idset_name").val($(this).text());
		
	});
	$("#idtxtkd_karyawan").change(function (e) { 
		e.preventDefault();
		var selectedValue = $(this).find('option:selected').text();;  
		$("#idset_name").val('');
		set_name(selectedValue);
		//$("#idset_name").val($(this).text());
		
	});

	function set_name (param) { 
		$("#idset_name").val(param);
	 }
	render_karyawan('<?php echo isset($karyawanSelected) ? json_encode($karyawanSelected) : null; ?>');
	render_suplier('<?php echo isset($suplierSelected) ? json_encode($suplierSelected) : null; ?>');
</script>
