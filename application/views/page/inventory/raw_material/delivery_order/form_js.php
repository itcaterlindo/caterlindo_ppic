<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	form_main('<?php echo isset($sts) ? $sts: null; ?>', '<?php echo isset($id) ? $id: null; ?>');
	formgr_main('<?php echo isset($sts) ? $sts: null; ?>', '<?php echo isset($id) ? $id: null; ?>');
	form_table('<?php echo isset($sts) ? $sts: null; ?>', '<?php echo isset($id) ? $id: null; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

    $(document).off('select2:selecting', '#idtxtrm_kd').on("select2:selecting", '#idtxtrm_kd', function(e) { 
		let dt = e.params.args.data;
		console.log(dt);
        if (dt.id != 'SPECIAL'){
		    $('#idtxtwhdeliveryorderdetail_nama').val(dt.rm_nama).prop('readonly', true);
		    $('#idtxtwhdeliveryorderdetail_deskripsi').val(dt.rm_deskripsi).prop('readonly', true);
		    $('#idtxtwhdeliveryorderdetail_spesifikasi').val(dt.rm_spesifikasi).prop('readonly', true);
			$('#idtxtwhdeliveryorderdetail_konversisatuan').val(dt.rmsatuan_nama);
            $("#idtxtwhdeliveryorderdetail_satuankd").select2("trigger", "select", { 
                data: { 
                    id: dt.rmsatuan_kd, 
                    text: dt.rmsatuan_nama, 
                }
            });
        }else{
            $('#idtxtwhdeliveryorderdetail_nama').val(dt.rm_nama).prop('readonly', false);
		    $('#idtxtwhdeliveryorderdetail_deskripsi').val(dt.rm_deskripsi).prop('readonly', false);
		    $('#idtxtwhdeliveryorderdetail_spesifikasi').val(dt.rm_spesifikasi).prop('readonly', false);
        }
        $('#idtxtwhdeliveryorderdetail_qty').focus();
	});

    $(document).off('select2:selecting', '#idtxtpo_kd').on("select2:selecting", '#idtxtpo_kd', function(e) { 
		let dt = e.params.args.data;
		formgr_table(dt.id);
	});

	$(document).off('ifChanged', '#check_all').on('ifChanged', '#check_all', function(e) {
        var headerchecked = e.target.checked;
        if (headerchecked == true) {
            $('.data-check').iCheck('check');
        }else{
            $('.data-check').iCheck('uncheck');
        }
    });

	function form_main(sts, id, whdeliveryorderdetial_kd = null){
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url().$class_link.'/form_main'; ?>',
            data: {sts: sts, id: id, whdeliveryorderdetial_kd:whdeliveryorderdetial_kd},
            success: function(html) {
                $('#<?php echo $box_content_id; ?>').html(html);
                moveTo('idMainContent');
            }
        });
	}

	function formgr_main(sts, id){
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url().$class_link.'/formgr_main'; ?>',
            data: {sts: sts, id: id},
            success: function(html) {
                $('#<?php echo $box_contentGR_id; ?>').html(html);
				render_po();
            }
        });
	}

	function formgr_table(id){
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url().$class_link.'/formgr_table'; ?>',
            data: {id: id},
            success: function(html) {
                $('#idgrtable').html(html);
				render_icheck ('icheck');
            }
        });
	}

	function form_table(sts, id){
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url().$class_link.'/form_table'; ?>',
            data: {sts: sts, id: id},
            success: function(html) {
                $('#<?php echo $box_contentTable_id; ?>').html(html);
                $('#<?php echo $box_contentTable_id; ?>').slideDown();
            }
        });
	}

	function render_icheck (classElement) {
		$('input[type="checkbox"].'+classElement).iCheck({
			checkboxClass: 'icheckbox_flat-blue'
		})
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

    function render_rm() {
		$("#idtxtrm_kd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 3,
			ajax: { 
				url: '<?php echo base_url().'auto_complete' ?>/get_raw_material',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramRM: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

    function render_satuan() {
		$("#idtxtwhdeliveryorderdetail_satuankd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?php echo base_url().'auto_complete' ?>/get_rm_satuan',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramSatuan: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

    function render_po() {
		$("#idtxtpo_kd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 2,
			ajax: { 
				url: '<?php echo base_url().$class_link ?>/get_pogreturn',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramPO: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function submitData(form_id) {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById(form_id);
		var po_kd = $('#idtxtpo_kd').val();

		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_insert" ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					form_main('<?php echo isset($sts) ? $sts: null; ?>', '<?php echo isset($id) ? $id: null; ?>');
					form_table('<?php echo isset($sts) ? $sts: null; ?>', '<?php echo isset($id) ? $id: null; ?>');
					$('.errInput').html('');
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}
                box_overlay('out');
			} 	        
		});
	}

	function submitDataGRBatch(form_id) {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById(form_id);

		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_insertGR_batch" ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
                    form_main('<?php echo isset($sts) ? $sts: null; ?>', '<?php echo isset($id) ? $id: null; ?>');
					form_table('<?php echo isset($sts) ? $sts: null; ?>', '<?php echo isset($id) ? $id: null; ?>');
					let po_kd = $('#idtxtpo_kd').val();	
					formgr_table(po_kd);
					$('.errInput').html('');
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
                box_overlay('out');
			} 	        
		});
	}

	function delete_item(id){
		event.preventDefault();
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete_detail'; ?>',
				data: {id: id},
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						form_table('<?php echo isset($sts) ? $sts: null; ?>', '<?php echo isset($id) ? $id: null; ?>');
					}else{
						notify (resp.status, resp.pesan, 'error');
					}
				}
			});
		}
	}

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

    function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }

</script>