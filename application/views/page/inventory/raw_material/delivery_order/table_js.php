<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
    open_table('<?php echo $date; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		formmaster_box('add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table(date){
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_main'; ?>',
				data: {date:date},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
				}
			});
		});
	}

	function view_box(id){
		window.location.assign('<?php echo base_url().$class_link?>/view_box?id='+id);
	}

	function view_pdf(id){
		window.open('<?php echo base_url().$class_link?>/view_pdf?id='+id);
	}

	function formmaster_box (sts, id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/formmaster_box'; ?>',
			data: {sts:sts, id:id},
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}	

	function edit_data (id) {
		formmaster_box('edit', id);
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function hapus_data(id) {
		var conf = confirm('Apakah anda yakin menghapus item ini ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						open_table('<?php echo $date; ?>');
						var po_kd = $('#idtxtpo_kd').val();
						open_table_grdetail (po_kd);
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				},
				error: function (request, status, error) {
					notify (status, error, 'error');	
				}
			});
		}
	}

	function render_datetime(valClass, valFormat){
		$('.'+valClass).datetimepicker({
            format: valFormat,
        });
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }
</script>