<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<table id="idTableGR" class="table table-bordered table-striped" style="width:100%; font-size: 100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;">No</th>
		<th style="width:1%; text-align:center;"><input type="checkbox" class="icheck" id="check_all" checked></th>
		<th style="width:7%; text-align:center;">PO</th>
		<th style="width:7%; text-align:center;">Kode RM</th>
		<th style="width:10%; text-align:center;">Deskripsi</th>
		<th style="width:5%; text-align:center;">Qty</th>
		<th style="width:5%; text-align:center;">Satuan</th>
		<th style="width:5%; text-align:center;">Qty Konversi</th>
		<th style="width:10%; text-align:center;">Remark</th>
		<th style="width:7%; text-align:center;">Modified</th>
	</tr>
	</thead>
    <tbody>
        <?php 
        $no = 1;
		foreach ($results as $each) :
		?>
        <tr>
            <td class="dt-center"> <?php echo $no; ?> </td>
            <td class="dt-center"> 
               <input type="checkbox" name="rmgr_kds[]"class="icheck data-check" checked value="<?php echo $each['rmgr_kd']; ?>">
            </td>
            <td><?php echo $each['po_no']; ?></td>
            <td><?php echo $each['rm_kode'].' | '.$each['rm_oldkd']; ?></td>
            <td><?php echo $each['podetail_deskripsi'].' / '.$each['podetail_spesifikasi']; ?></td>
            <td class="dt-right">
            <?php 
                echo (float) abs($each['rmgr_qty']);?>
            </td>
            <td><?php echo $each['rmsatuan_nama'];?></td>
            <td class="dt-right">
            <?php 
                echo (float) abs($each['rmgr_qtykonversi']);?>
            </td>
			<td> <input type="text" name="txtrmgr_kets[<?php echo $each['rmgr_kd']; ?>]" class="form-control form-control-sm" placeholder="Remark"> <?php echo $each['rmgr_ket']; ?></td>
			<td><?php echo $each['rmgr_tgldatang']; ?></td>
        </tr>
        <?php 
        $no ++;
        endforeach;?>
    </tbody>
</table>
