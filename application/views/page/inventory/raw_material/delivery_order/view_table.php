<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<table id="idtablemain" cellspacing="0" cellpadding="1" style="font-size: 85%;" border="1" width="100%">
	<thead>
	<tr>
        <th width="15%" style="text-align: center; font-weight:bold;">Qty</th>
        <th width="15%" style="text-align: center; font-weight:bold;">UOM</th>
        <th width="45%" style="text-align: center; font-weight:bold;">Description</th>
        <th width="25%" style="text-align: center; font-weight:bold;">Remark</th>
    </tr>
	</thead>
    <tbody>
    <?php 
    foreach ($results as $result) :
    ?>
        <tr>
            <td width="15%" style="text-align: right;"><?= $result['whdeliveryorderdetail_qty'] ?></td>
            <td width="15%"><?= $result['rmsatuan_nama'] ?></td>
            <td width="45%"><?= $result['whdeliveryorderdetail_deskripsi'].'/'.$result['whdeliveryorderdetail_spesifikasi'] ?></td>
            <td width="25%"><?= $result['whdeliveryorderdetail_remark'] ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody> 
</table>