<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>

<div class="row">

	<div class="form-group">
		<label for="idtxtrmstokadj_waktu" class="col-md-2 control-label">Tgl Adj</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrrmstokadj_waktu"></div>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				<?php echo form_input(array('type' => 'text', 'id' => 'idtxtrmstokadj_waktu', 'name' => 'txtrmstokadj_waktu', 'class' => 'form-control datetimepicker', 'placeholder' => 'Tgl Datang', 'value' => date('Y-m-d H:i:s'))); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="idtxtrmstokadj_type" class="col-md-2 control-label">Jenis Adj</label>
		<div class="col-md-2 col-xs-12">
			<div id="idErrrmstokadj_type"></div>
			<select name="txtrmstokadj_type" id="idtxtrmstokadj_type" class="form-control">
				<option value="IN">IN</option>
				<option value="OUT">OUT</option>
				<option value="IN-OUT">IN-OUT</option>
				<option value="OUT-IN">OUT-IN</option>
			</select>
		</div>
	</div>
	<div id="div-add-out-in" style="display: none;">
		<div class="form-group">
			<label for="" class="col-md-2 control-label">Jumlah OUT</label>
			<div class="col-md-2 col-xs-12">
				<input type="number" id="out-value" class="form-control" value="1">
			</div>
			<div class="col-md-2 col-xs-12">
				<button id="btn-add-out-in" class="btn btn-primary" hidden><i class="fa fa-plus"></i></button>
			</div>
		</div>
	</div>
	<div id="div-add-in-out" style="display: none;">
		<div class="form-group">
			<label for="" class="col-md-2 control-label">Jumlah IN</label>
			<div class="col-md-2 col-xs-12">
				<input type="number" id="in-value" class="form-control" value="1">
			</div>
			<div class="col-md-2 col-xs-12">
				<button id="btn-add-in-out" class="btn btn-primary" hidden><i class="fa fa-plus"></i></button>
			</div>
		</div>
	</div>
	<hr>
	<!-- DIV INPUT -->
	<div id="div-input-form">
		<!-- UNTUK APPEND DARI BACKEND BERDASARKAN ADJUSMENT TYPE GET INPUTAN DENGAN JS AJAX -->
	</div>
	<!-- END DIV INPUT -->

</div>

<hr>
<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-2 col-md-offset-2 col-xs-12">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button type="reset" name="btnReset" onclick="resetFormMain()" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>