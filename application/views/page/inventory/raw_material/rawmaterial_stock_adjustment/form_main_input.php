    <!-- DIV INPUT -->
    <div id="div-input-data">
		<div id="form-index<?= $form_index ?>">
			<!-- JENIS IN ATAU OUT -->
			<input type="hidden" name="txtJenistransaksi[]" id="idTxtJenisTransaksi[]" value="<?= $jenis_transaksi ?>">
			<div class="form-group">
				<label for="idtxtrm_kd" class="col-md-2 control-label"> Raw Material (<?= $jenis_transaksi ?>)</label>
				<div id="idErrrm_kd" class="idErrrm_kd"></div>
				<div class="col-md-4 col-xs-12">
					<select name="txtrm_kd[]" class="form-control idtxtrm_kd" id="idtxtrm_kd<?= $form_index ?>" data-allow-clear="true"></select>
				</div>
			</div>

			<div class="form-group">
				<label for="idtxtrmstokadj_qty" class="col-md-2 control-label">Stock Qty</label>
				<div class="col-sm-2 col-xs-12">
					<input type="text" name="txtrmcurrentstock[]" id="idtxtrmcurrentstock<?= $form_index ?>" class="form-control" placeholder="Stock Qty" readonly>
				</div>
				<div class="col-sm-2 col-xs-12">
					<input type="text" name="txtrmsatuan[]" id="idtxtrmsatuan<?= $form_index ?>" class="form-control" placeholder="Satuan" readonly>
				</div>
			</div>

			<div class="form-group">
				<label for="idtxtrmstokadj_qty" class="col-md-2 control-label">Qty</label>
				<div class="col-sm-2 col-xs-12">
					<div id="idErrrmstokadj_qty" class="idErrrmstokadj_qty"></div>
					<input type="number" name="txtrmstokadj_qty[]" id="idtxtrmstokadj_qty<?= $form_index ?>" class="form-control" placeholder="Qty">
				</div>
			</div>

			<div class="form-group">
				<label for='idtxtgudang' class="col-md-2 control-label">Gudang</label>
				<div class="col-sm-3 col-xs-12">
					<div class="errInput" id="idErrGudang"></div>
					<?php if($this->session->userdata('tipe_admin') == 'Admin'){ ?>
						<select id="idtxtgudang_kd[]" name="txtgudang_kd[]" class="form-control"> 
							<?php foreach($gudang as $key => $val){ ?>
								<option value="<?= $val['kd_gudang'] ?>" <?= $val['kd_gudang'] == "MGD260719003" ? "selected" : "" ?> ><?= $val['nm_gudang'] ?></option>
							<?php } ?>
						</select>
					<?php }else{
						echo form_input(array('type' => 'text', 'id' => 'idtxtgudang_nm[]', 'name' => 'txtgudang_nm[]', 'class' => 'form-control', 'readonly' => 'readonly', 'placeholder' => 'Gudang', 'value' => $gudang_nm));
						echo form_input(array('type' => 'hidden', 'id' => 'idtxtgudang_kd[]', 'name' => 'txtgudang_kd[]', 'class' => 'form-control', 'readonly' => 'readonly', 'placeholder' => 'Gudang', 'value' => $gudang_user['kd_gudang'])); 
					} ?>
				</div>
			</div>

			<div class="form-group">
				<label for="idtxtrmstokadj_remark" class="col-md-2 control-label">Note</label>
				<div class="col-sm-4 col-xs-12">
					<textarea name="txtrmstokadj_remark[]" class="form-control" id="idtxtrmstokadj_remark[]" rows="2" placeholder="Note"></textarea>
				</div>
			</div>

		</div>
	</div>
	<hr>
	<!-- END DIV INPUT -->

<script type="text/javascript">
	$('#idtxtrm_kd<?= $form_index ?>').select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 2,
			ajax: {
				url: '<?php echo base_url() ?>auto_complete/get_raw_material_stock',
				type: "get",
				dataType: 'json',
				delay: 500,
				data: function(params) {
					return {
						paramRM: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});

	$(document).off('select2:selecting', '#idtxtrm_kd<?= $form_index ?>').on('select2:selecting', '#idtxtrm_kd<?= $form_index ?>', function(e) {
		let dt = e.params.args.data;
		$('#idtxtrmcurrentstock<?= $form_index ?>').val(dt.stock_qty);
		$('#idtxtrmsatuan<?= $form_index ?>').val(dt.rmsatuan_nama);
	});

	$(document).off('select2:unselecting', '#idtxtrm_kd<?= $form_index ?>').on('select2:unselecting', '#idtxtrm_kd<?= $form_index ?>', function(e) {
		let dt = e.params.args.data;
		$('#idtxtrmcurrentstock<?= $form_index ?>').val('');
		$('#idtxtrmsatuan<?= $form_index ?>').val('');
	});

</script>