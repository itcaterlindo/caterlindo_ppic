<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	$(document).ready(function() {
		first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');
		form_main();
		table_main('<?php echo date('Y-m-d'); ?>');
		render_datetime('#idtxtrmstokadj_waktu', 'YYYY-MM-DD');
		render_datetime('#idtxttglAdj', 'YYYY-MM-DD');
	});

	$(document).off('click', '#btn-add-out-in').on('click', '#btn-add-out-in', function() {
		event.preventDefault();
		show_input_form('OUT-IN');
	});

	$(document).off('click', '#btn-add-in-out').on('click', '#btn-add-in-out', function() {
		event.preventDefault();
		show_input_form('IN-OUT');
	});

	function show_input_form(jnsAdj){
		/** Fungsi parameter untuk show textbox out-in atau in-out */
		$('#div-input-form').empty();
		let value = jnsAdj == 'OUT-IN' ? $('#out-value').val() : $('#in-value').val();
		let renderIn = false;
		for(let x=0; x < parseInt(value); x++){
			if(jnsAdj == 'OUT-IN'){
				add_form_input('OUT', x);
			}else{
				add_form_input('IN', x);
			}
			if(x == parseInt(value)){
				renderIn = true;
			}	
		};
		if(renderIn = true){
			setTimeout(function () { 
				if(jnsAdj == 'OUT-IN'){
					add_form_input('IN', parseInt(value));		
				}else{
					add_form_input('OUT', parseInt(value));
				}
 			}, 1000); // delay in ms      
		}
	}

	function add_form_input(jenis_transaksi, form_index)
	{
		/** Form index berfungsi untuk identitas form, example nilai 0 = generate 1 form, nilai 1 = generate 2 form, nilai 2 = generate 3 form */
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/form_main_input'; ?>',
			data: {
				'jenis_transaksi': jenis_transaksi,
				'form_index': form_index
			},
			success: function(html) {
				$('#div-input-form').append(html);
			}
		});
	}

	$(document).off('change', '#idtxtrmstokadj_type').on('change', '#idtxtrmstokadj_type', function() {
		var jenisAdj = $(this).val();
		if(jenisAdj == 'IN'){
			$('#div-input-form').empty();
			add_form_input('IN', 0);
			$('#div-add-out-in').hide();
			$('#div-add-in-out').hide();
		}else if(jenisAdj == 'OUT'){
			$('#div-input-form').empty();
			add_form_input('OUT', 0);
			$('#div-add-out-in').hide();
			$('#div-add-in-out').hide();
		}else if(jenisAdj == 'OUT-IN'){
			$('#div-add-in-out').hide();
			$('#div-input-form').empty();
			$('#div-add-out-in').show();
		}else if (jenisAdj == 'IN-OUT'){
			$('#div-add-out-in').hide();
			$('#div-input-form').empty();
			$('#div-add-in-out').show();
		}
	});


	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function table_main(date) {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_table_id; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/table_main'; ?>',
				data: {
					'date': date
				},
				success: function(html) {
					$('#<?php echo $box_content_table_id; ?>').slideDown().html(html);
					$('#idtxttglAdj').val(date.substring(0, 10));
					moveTo('idMainContent');
				}
			});
		});
	}

	function showAdjustment() {
		event.preventDefault();
		var date = $('#idtxttglAdj').val();
		table_main(date);
	}

	function form_main() {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/form_main'; ?>',
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').slideDown().html(html);
				add_form_input('IN', 0);
				render_datetime('#idtxtrmstokadj_waktu', 'YYYY-MM-DD');
				moveTo('idMainContent');
			}
		});
	}

	function render_datetime(valAttr, valFormat) {
		$(valAttr).datetimepicker({
			format: valFormat,
		});

	}

	// function render_rawmaterial(id) {
	// 	$(id).select2({
	// 		theme: 'bootstrap',
	// 		placeholder: '--Pilih Opsi--',
	// 		minimumInputLength: 2,
	// 		ajax: {
	// 			url: '<?php echo base_url() ?>auto_complete/get_raw_material_stock',
	// 			type: "get",
	// 			dataType: 'json',
	// 			delay: 500,
	// 			data: function(params) {
	// 				return {
	// 					paramRM: params.term
	// 				};
	// 			},
	// 			processResults: function(response) {
	// 				return {
	// 					results: response
	// 				};
	// 			},
	// 			cache: true
	// 		}
	// 	});
	// }
	
	function toggle_modal(modalTitle, htmlContent) {
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function actSubmitData(form_id)
	{
		var form = document.getElementById(form_id);
		$.ajax({
			url: "<?php echo base_url() . $class_link; ?>/action_insert",
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				var resp = JSON.parse(data);
				if (resp.code == 200) {
					notify(resp.status, resp.pesan, 'success');
					var date = $('#idtxtrmstokadj_waktu').val();
					form_main();
					table_main(date);
					$('.errInput').html('');
					generateToken(resp.csrf);
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('.' + key).html(value);
					});
					generateToken(resp.csrf);
					moveTo('idMainContent');
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					resetFormMain();
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					resetFormMain();
					generateToken(resp.csrf);
				}
			}
		});
	}

	function submitData(form_id) {
		event.preventDefault();
		let qty_stok = $('#idtxtrmstokadj_qty_stok').val();
		let qty_inputan = $('#idtxtrmstokadj_qty').val();
		// Cek ketika qty inputan melebihi qty stok dikasih alert
		if( Math.floor(qty_inputan) > Math.floor(qty_stok) ){
			if(confirm('Qty inputan melebihi current stok, apakah anda ingin melanjutkan?')){	
				actSubmitData(form_id);
			}
		}else{
			if(confirm('Apakah anda yakin ingin menyimpan data tersebut?')){	
				actSubmitData(form_id);
			}
		}
	}

	function hapus_data(id) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						var date = $('#idtxttglAdj').val();
						table_main(date);
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				},
				error: function (request, status, error) {
					notify (status, error, 'error');	
				}
			});
		}
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			delay: 2500,
			styling: 'bootstrap3'
		});
	}
</script>