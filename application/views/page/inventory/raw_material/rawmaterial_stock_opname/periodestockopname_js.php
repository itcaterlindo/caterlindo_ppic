<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	table_main();

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function(e) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/periodestockopname_formmain'; ?>',
			data:{
				slug: 'add',
				rmstokopnamejenis_kd: ''
			},
			success: function(html) {
				toggle_modal('Add', html);
				render_datetimepicker('#idtxtrmstokopname_periode');
			}
		});
	});

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function table_main() {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/periodestockopname_tablemain'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').slideDown().html(html);
					moveTo('idMainContent');
				}
			});
		});
	}

	function edit_data(rmstokopname_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/periodestockopname_formmain'; ?>',
			data:{
				slug: 'edit',
				rmstokopname_kd: rmstokopname_kd
			},
			success: function(html) {
				toggle_modal('Edit', html);
				render_datetimepicker('#idtxtrmstokopname_periode');
			}
		});
	}

	function view_opname(rmstokopname_kd){
		window.location.assign('<?php echo base_url().$class_link.'/view_main?rmstokopname_kd='?>'+rmstokopname_kd);
	}

	function render_datetimepicker(element) {
		$(element).datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		});
	}

	function periodestockopname_formrmtabledetail(rmstokopname_kd){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/periodestockopname_formrmtabledetail'; ?>',
			data:{
				slug: 'edit',
				rmstokopname_kd: rmstokopname_kd
			},
			success: function(html) {
				$('#<?php echo $content_modal_id; ?>').html(html);
			}
		});
	}


	function toggle_modal(modalTitle, htmlContent) {
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitDataPeriode(form_id) {
		event.preventDefault();
		var form = document.getElementById(form_id);

		$.ajax({
			url: "<?php echo base_url() . $class_link; ?>/action_periodestockopname",
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				var resp = JSON.parse(data);
				console.log(data);
				if (resp.code == 200) {
					notify(resp.status, resp.pesan, 'success');
					periodestockopname_formrmtabledetail(resp.data.rmstokopname_kd);
					$('.errInput').html('');
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
					moveTo('idMainContent');
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
			}
		});
	}

	function submitBatchDetailMaterial(form_id) {
		event.preventDefault();
		var form = document.getElementById(form_id);

		$.ajax({
			url: "<?php echo base_url() . $class_link; ?>/action_periodestockopname_detailbatch",
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				var resp = JSON.parse(data);
				console.log(data);
				if (resp.code == 200) {
					notify(resp.status, resp.pesan, 'success');
					toggle_modal('', '');
					table_main();
					$('.errInput').html('');
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
					moveTo('idMainContent');
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
			}
		});
	}

	function hapus_data(id) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_periodestockopnamedetail_delete'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						table_main();
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				},
				error: function (request, status, error) {
					notify (status, error, 'error');	
				}
			});
		}
	}

	function action_set_status(id,rmstokopname_status) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_periodestockopname_setstatus'; ?>',
				data: {
					id: id,
					rmstokopname_status: rmstokopname_status
				},
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						if (rmstokopname_status == 'active') {
							window.location.assign('<?php echo base_url().$class_link; ?>');
						}else{
							table_main();
						}
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				},
				error: function (request, status, error) {
					notify (status, error, 'error');	
				}
			});
		}
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			delay: 2500,
			styling: 'bootstrap3'
		});
	}
</script>