<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }

    td.dt-blue {
        background-color: blue;
        color: white;
    }

    td.dt-green {
        background-color: green;
        color: white;
    }

    td.dt-red {
        background-color: red;
        color: white;
    }

    td.dt-purple {
        background-color: purple;
        color: white;
    }
</style>
<?php
$form_id = 'idFormBatch';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>
<div id="idErrrmstokopnamejenis_kd"></div>
<input type="hidden" name="txtrmstokopnamejenis_kd" id="idtxtrmstokopnamejenis_kd" value="<?php echo isset($rmstokopnamejenis_kd) ? $rmstokopnamejenis_kd : null; ?>">

<div class="form-group">
    <label for="idtxtrmstokopnamejenis_nama" class="col-md-2 control-label">Nama Jenis Opname</label>
    <div class="col-sm-6 col-xs-12">
        <div id="idErrrmstokopnamejenis_nama"></div>
        <input type="text" name="txtrmstokopnamejenis_nama" id="idtxtrmstokopnamejenis_nama" class="form-control" placeholder="Nama" value="<?php echo isset($rmstokopnamejenis_nama) ? $rmstokopnamejenis_nama : null; ?>">
    </div>
</div>

<div class="col-md-12">
    <table id="idTableBatch" class="table table-bordered table-striped table-hover display responsive" style="width:100%;">
        <thead>
            <tr>
                <th style="width:5%; text-align:center;" class="all">
                    <input type="checkbox" id="idcheckall">
                </th>
                <th style="width:25%; text-align:center;" class="all">Kode</th>
                <th style="width:70%; text-align:center;">Deskripsi</th>
            </tr>
        </thead>
    </table>
    <hr>
    <button class="btn btn-sm btn-primary" onclick="submitBatchDetailMaterial('<?php echo $form_id; ?>')"> <i class="fa fa-save"></i> Simpan </button>
</div>

<?php echo form_close(); ?>
<script type="text/javascript">
    var tableBatch = $('#idTableBatch').DataTable({
        "processing": true,
        "serverSide": false,
        "ordering": true,
        "paging": false,
        "scrollY": 400,
        "ajax": "<?php echo base_url() . $class_link . '/jenisstockopname_formbatch_datamaterial?rmstokopnamejenis_kd=' . $rmstokopnamejenis_kd; ?>",
        "language": {
            "lengthMenu": "Tampilkan _MENU_ data",
            "zeroRecords": "Maaf tidak ada data yang ditampilkan",
            "info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
            "infoFiltered": "",
            "infoEmpty": "Tidak ada data yang ditampilkan",
            "search": "Cari :",
            "loadingRecords": "Memuat Data...",
            "processing": "Sedang Memproses...",
            "paginate": {
                "first": '<span class="glyphicon glyphicon-fast-backward"></span>',
                "last": '<span class="glyphicon glyphicon-fast-forward"></span>',
                "next": '<span class="glyphicon glyphicon-forward"></span>',
                "previous": '<span class="glyphicon glyphicon-backward"></span>'
            }
        },
        "columnDefs": [{
            "orderable": false,
            "targets": 0,
            "render": function(data, type, full, meta) {
                var htmlChecked = '';
                if (full[3] == 'checked') {
                    htmlChecked = 'checked="checked"';
                }
                var html = '<input type="checkbox" name="txtrm_kds[]" class="classCheck" value="' + data + '" ' + htmlChecked + ' >';
                return html;
            }
        }],
        "order": [1, 'asc'],
    });

    $('#idcheckall').click(function() {
        if ($(this).is(':checked')) {
            $('.classCheck').prop('checked', true);
        } else {
            $('.classCheck').prop('checked', false);
        }
    });
</script>