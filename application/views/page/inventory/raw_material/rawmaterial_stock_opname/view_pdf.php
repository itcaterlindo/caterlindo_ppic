<?php
class customPdf extends Tcpdf
{
    public $rmstokopname_kode;
    public $rmstokopnamejenis_nama;
    public $rmstokopname_periode;

    public function setsHeader($headerData)
    {
        $this->rmstokopname_kode = $headerData['rmstokopname_kode'];
        $this->rmstokopnamejenis_nama = $headerData['rmstokopnamejenis_nama'];
        $this->rmstokopname_periode = $headerData['rmstokopname_periode'];
    }

    public function Header()
    {
        $header = '
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td width="30%" rowspan="4" style="text-align:left;"> <img src="assets/admin_assets/dist/img/logo_cat.png" width="135" height="35"> </td>
                <td width="35%" rowspan="4" style="text-align: center; "></td>
                <td width="35%" colspan="2" style="text-align:right; font-size: 120%; font-weight:bolt;"> RM STOK OPNAME</td>
            </tr>
            <tr>
                <td style="text-align: left; font-size: 75%;">No Dokumen</td>
                <td style="text-align: left; font-size: 75%;">: -</td>
            </tr>
            <tr>    
                <td style="text-align: left; font-size: 75%;">Tanggal Terbit</td>
                <td style="text-align: left; font-size: 75%;">: -</td>
            </tr>
            <tr>
                <td style="text-align: left; font-size: 75%;">Rev</td>
                <td style="text-align: left; font-size: 75%;">: -</td>
            </tr>
        </table>';

        /** Keterangan */
        $keterangan =
            '<table cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 80%;">
            <tr>
                <td width="20%"> No </td>
                <td width="30%"> : ' . $this->rmstokopname_kode . ' </td>
                <td width="20%"> Jenis </td>
                <td width="30%"> : ' . $this->rmstokopnamejenis_nama . ' </td>
            </tr>
            <tr>
                <td width="20%"> Periode </td>
                <td width="30%"> : ' . $this->rmstokopname_periode . ' </td>
                <td width="20%"> </td>
                <td width="30%">  </td>
            </tr>
            <tr>
                <td width="20%"> </td>
                <td width="30%"> </td>
                <td></td>    
                <td></td>    
            </tr>
        </table>';

        $this->writeHTML($header, true, false, false, false, '');
        $this->writeHTML($keterangan, true, false, false, false, '');
    }
}

$pdf = new customPdf('P', 'mm', 'A4', true, 'UTF-8', false);
$title = 'RM Stok Opname '.$dataHeader['rmstokopname_kode'];
$pdf->SetTitle($title);
$pdf->SetAutoPageBreak(true, 10);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');
$pdf->setPrintFooter(true);

/** Margin */
$pdf->SetHeaderMargin(5);
$pdf->SetFooterMargin(7);
$pdf->SetMargins(8, 42, 8);

$pdf->setsHeader($dataHeader);

$pdf->AddPage();


$pdf->writeHTML($konten, true, false, false, false, '');

// $footer = '<table cellspacing="0" cellpadding="0" border="1" style="font-size: 80%;" width="70%">';
// foreach ($logs as $log) {
//     $footer .= 
//     ' <tr>
//         <td>'.$log['wfstate_nama'].' by</td>
//         <td> :'.$log['nm_admin'].'</td>
//         <td>'.$log['materialreceiptlog_tglinput'].'</td>
//     </tr>';
// }                      
// $footer .= '</table>';

// $pdf->writeHTML($footer, true, false, false, false, '');


$txtOutput = $title . '.pdf';
$pdf->Output($txtOutput, 'I');
