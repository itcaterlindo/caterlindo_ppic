<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$js_file = 'view_js';
$box_title = 'View RM Stok Opname';
$data['class_link'] = $class_link;
$box_type = 'Table';
$data['master_var'] = 'MaterialPeriodeStockOpname';
$data['box_id'] = 'id' . $box_type . 'Box' . $data['master_var'];
$data['box_content_id'] = 'id' . $box_type . 'BoxContent' . $data['master_var'];
$data['box_loader_id'] = 'id' . $box_type . 'BoxLoader' . $data['master_var'];
$data['btn_add_id'] = 'id' . $box_type . 'BtnBoxAdd' . $data['master_var'];

$data['modal_title'] = 'Modal Default';
$data['content_modal_id'] = 'idModal' . $box_type . $data['master_var'];
?>

<style type="text/css">
    .modal-dialog {
        width: 1000px;
    }
</style>

<div class="box box-primary" id="<?php echo $data['box_id']; ?>">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo $box_title; ?></h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-toggle="tooltip" title="Back" onclick="window.history.back()"> <i class="fa fa-arrow-circle-left"></i> </button>
            <button class="btn btn-box-tool" id="<?php echo $data['btn_add_id']; ?>" data-toggle="tooltip" title="Tambah"><i class="fa fa-plus"></i></button>
            <button class="btn btn-box-tool btn-collapse" id="idBtnBoxTableSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <div class="row invoice-info">
            <div class="col-sm-6 invoice-col">
                <b>StokOpname Kode :</b> <?php echo isset($row['rmstokopname_kode']) ? $row['rmstokopname_kode'] : '-'; ?><br>
                <b>StokOpname Periode :</b> <?php echo isset($row['rmstokopname_periode']) ? $row['rmstokopname_periode'] : '-'; ?> <br>
                <b>StokOpname Status :</b> <?php echo isset($row['rmstokopname_status_badge']) ? $row['rmstokopname_status_badge'] : '-'; ?><br>
            </div>
            <div class="col-sm-6 invoice-col">
                <b>StokOpname Jenis :</b> <?php echo isset($row['rmstokopnamejenis_nama']) ? $row['rmstokopnamejenis_nama'] : '-'; ?> <br>
                <b>StokOpname Note :</b> <?php echo isset($row['rmstokopname_note']) ? $row['rmstokopname_note'] : '-'; ?>
            </div>
        </div>
        <hr>
        <button class="btn btn-sm btn-default" onclick="window.open('<?php echo base_url().$class_link.'/view_pdf?rmstokopname_kd='.$rmstokopname_kd; ?>')"> <i class="fa fa-file-pdf-o"></i> Cetak PDF</button>
        <hr>
        <div id="<?php echo $data['box_loader_id']; ?>" align="middle">
            <i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f; display: none;"></i>
        </div>
        <div class="col-md-12">
            <div id="<?php echo $data['box_content_id']; ?>" style="display: none;"></div>
        </div>
    </div>
    <div class="box-footer">
    </div>
    <div class="overlay" id="idBoxTableOverlay" style="display: none;">
        <i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
    </div>
</div>

<?php
$this->load->view('page/' . $class_link . '/' . $js_file, $data); ?>

<!-- modal -->
<div class="modal fade" id="idmodal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo $data['modal_title']; ?></h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div id="<?php echo $data['content_modal_id']; ?>"></div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Keluar</button>
            </div>
        </div>
    </div>
</div>