<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<!-- <style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}
</style> -->

<table id="idTable" border="1" width="100%" style="font-size: 80%;">
	<thead>
		<tr style="font-weight: bold;">
			<th style="width:3%; text-align:center;">No.</th>
			<th style="width:10%; text-align:center;">RM Kode</th>
			<th style="width:15%; text-align:center;">RM Deskripsi</th>
			<th style="width:10%; text-align:center;">Qty Current Stock / Satuan</th>
			<th style="width:10%; text-align:center;">Qty Current Stock Secondary / Satuan</th>
			<th style="width:10%; text-align:center;">Qty </th>
			<th style="width:10%; text-align:center;">Satuan</th>
			<th style="width:5%; text-align:center;">Konversi</th>
			<th style="width:10%; text-align:center;">Qty Konversi</th>
			<th style="width:17%; text-align:center;">Remark</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		foreach ($detailRMopnames as $detailRMopname) :
			$rm_qtykonversi = !empty($detailRMopname['rm_qtykonversi']) ? $detailRMopname['rm_qtykonversi'] : 1;
			$satuan_secondary_qty = (float) $detailRMopname['qty_aft_mutasi'] / $rm_qtykonversi;
			/** bg color check */
			$bgcolor = 'yellow';
			if ($detailRMopname['rmstokopnamedetail_qtykonversi'] == $satuan_secondary_qty) {
				$bgcolor = 'green';
			}
		?>
			<tr style="font-size: 80%;">
				<td style="width: 3%;" ><?php echo $no; ?></td>
				<td style="width: 10%;"><?php echo $detailRMopname['rm_kode'] . ' / ' . $detailRMopname['rm_oldkd']; ?></td>
				<td style="width: 15%;"><?php echo $detailRMopname['rm_deskripsi'] . ' ' . $detailRMopname['rm_spesifikasi']; ?></td>
				<td style="width: 10%; text-align: right;"><?php echo $detailRMopname['qty_aft_mutasi'] . ' ' . $detailRMopname['rmsatuan_nama']; ?></td>
				<td style="width: 10%; text-align: right;"><?php echo $satuan_secondary_qty . ' ' . $detailRMopname['satuan_secondary_nama']; ?></td>
				<td style="width: 10%; text-align: right; font-weight: bold;"><?php echo $detailRMopname['rmstokopnamedetail_qty']; ?></td>
                <td style="width: 10%; text-align: center; font-weight: bold;"><?php echo $detailRMopname['satuan_opname_nama']; ?></td>
				<td style="width: 5%; text-align: right; font-weight: bold;"><?php echo $detailRMopname['rmstokopnamedetail_konversi']; ?></td>
				<td style="width: 10%; text-align: right; font-weight: bold; background-color: <?php echo $bgcolor ?>;"><?php echo $detailRMopname['rmstokopnamedetail_qtykonversi']; ?></td>
				<td style="width: 17%;"><?php echo $detailRMopname['rmstokopnamedetail_remark']; ?></td>
			</tr>
		<?php
			$no++;
		endforeach; ?>
	</tbody>
</table>
