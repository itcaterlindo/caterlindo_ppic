<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
    $(document).ready(function() {
        first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');
        view_table('<?php echo $rmstokopname_kd; ?>');
    });


    function moveTo(div_id) {
        $('html, body').animate({
            scrollTop: $('#' + div_id).offset().top - $('header').height()
        }, 1000);
    }

    function first_load(loader, content) {
        $('#' + loader).fadeOut(500, function(e) {
            $('#' + content).slideDown();
        });
    }

    function view_table(rmstokopname_kd) {
        $('#<?php echo $btn_add_id; ?>').slideDown();
        $('#<?php echo $box_content_id; ?>').slideUp(function() {
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url() . $class_link . '/view_table'; ?>',
                data: {
                    'rmstokopname_kd': rmstokopname_kd
                },
                success: function(html) {
                    $('#<?php echo $box_content_id; ?>').slideDown().html(html);
                    render_dt();
                    moveTo('idMainContent');
                    $("#idTable").css({
                        fontSize: 16
                    });
                }
            });
        });
    }

    function render_dt() {
        $('#idTable').DataTable({
			"dom": 'Bfrtip',
			"serverSide": false,
			"paging": false,
			"searching": false,
			"ordering": false,
			"info": false,
			"language": {
				"lengthMenu": "Tampilkan _MENU_ data",
				"zeroRecords": "Maaf tidak ada data yang ditampilkan",
				"info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "",
				"infoEmpty": "Tidak ada data yang ditampilkan",
				"search": "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing": "Sedang Memproses...",
				"paginate": {
					"first": '<span class="fas fa-fast-backward"></span>',
					"last": '<span class="fas fa-fast-forward"></span>',
					"next": '<span class="fas fa-forward"></span>',
					"previous": '<span class="fas fa-backward"></span>'
				}
			},
			"buttons": [{
				"extend": "excelHtml5",
				"title": "RM Stok Opname",
				"exportOptions": {
					"columns": [0,1,2,3,4,5,6,7,8,9]
				}
			}],
		});
    }

    function toggle_modal(modalTitle, htmlContent) {
        $('#idmodal').modal('toggle');
        $('.modal-title').text(modalTitle);
        $('#<?php echo $content_modal_id; ?>').slideUp();
        $('#<?php echo $content_modal_id; ?>').html(htmlContent);
        $('#<?php echo $content_modal_id; ?>').slideDown();
    }

    function generateToken(csrf) {
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
    }

    function notify(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            type: type,
            delay: 2500,
            styling: 'bootstrap3'
        });
    }
</script>