<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	$(document).ready(function() {
		first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');
		table_main('<?php echo $rmstokopname_kd; ?>');
	});

	
	$(document).off('keyup', '.clqtyopname').on('keyup', '.clqtyopname', function(e) {
		var rmstokopnamedetail_qty = $('#idtxtrmstokopnamedetail_qty').val();
		var rmstokopnamedetail_konversi = $('#idtxtrmstokopnamedetail_konversi').val();
		$('#idtxtrmstokopnamedetail_qtykonversi').val(parseFloat(rmstokopnamedetail_qty) * parseFloat(rmstokopnamedetail_konversi));
	});

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function table_main(rmstokopname_kd) {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/table_main'; ?>',
				data: {
					'rmstokopname_kd': rmstokopname_kd
				},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').slideDown().html(html);
					moveTo('idMainContent');
					render_rawmaterial_satuan();
				}
			});
		});
	}

	get_active_rmdetail();
	var rmstokponamedetail_kds = [];
	function get_active_rmdetail() {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/get_active_rmdetail'; ?>',
			success: function(data) {
				$.each( data, function( key, value ) {
					rmstokponamedetail_kds.push(value.rmstokopnamedetail_kd);
				});
			}
		});
	}

	var currentPos;
	var nextPosID;
	var backPosID;
	function action_editopname(rmstokopnamedetail_kd) {
		currentPos = rmstokponamedetail_kds.indexOf(rmstokopnamedetail_kd);
		nextPosID = rmstokponamedetail_kds[currentPos+1];
		backPosID = rmstokponamedetail_kds[currentPos-1];
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/table_formmain'; ?>',
			data: {
				'rmstokopnamedetail_kd': rmstokopnamedetail_kd,
				'next_rmstokopnamedetail_kd': nextPosID,
				'back_rmstokopnamedetail_kd': backPosID,
			},
			success: function(html) {
				toggle_modal('Edit', html)
				$('#idtxtrmstokopnamedetail_qty').focus();
				render_rawmaterial_satuan('#idtxtrmstokopnamedetail_satuan');
			}
		});
	}

	function get_rmstokopnamedetail_inside_modal(rmstokopnamedetail_kd){
		event.preventDefault();
		currentPos = rmstokponamedetail_kds.indexOf(rmstokopnamedetail_kd);
		nextPosID = rmstokponamedetail_kds[currentPos+1];
		backPosID = rmstokponamedetail_kds[currentPos-1];
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/table_formmain'; ?>',
			data: {
				'rmstokopnamedetail_kd': rmstokopnamedetail_kd,
				'next_rmstokopnamedetail_kd': nextPosID,
				'back_rmstokopnamedetail_kd': backPosID,
			},
			success: function(html) {
				$('#<?php echo $content_modal_id; ?>').html(html);
				$('#idtxtrmstokopnamedetail_qty').focus();
				render_rawmaterial_satuan('#idtxtrmstokopnamedetail_satuan');
			}
		});
	}

	function render_rawmaterial_satuan(attrib) {
		$(attrib).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: {
				url: '<?php echo base_url() ?>auto_complete/get_rm_satuan',
				type: "get",
				dataType: 'json',
				delay: 500,
				data: function(params) {
					return {
						paramSatuan: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_datetimepicker(element) {
		$(element).datetimepicker({
			format: 'YYYY-MM-DD',
		});
	}

	function lihat_data(id) {
		window.location.assign('<?php echo base_url() . $class_link ?>/view_box?id=' + id);
	}

	function toggle_modal(modalTitle, htmlContent) {
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitDataOpnameDetail(form_id) {
		event.preventDefault();
		var form = document.getElementById(form_id);

		$.ajax({
			url: "<?php echo base_url() . $class_link; ?>/action_stokopnamdedetail_update",
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				var resp = JSON.parse(data);
				console.log(data);
				if (resp.code == 200) {
					notify(resp.status, resp.pesan, 'success');
					get_rmstokopnamedetail_inside_modal(nextPosID);
					// toggle_modal('', '');
					// table_main('<?php echo $rmstokopname_kd; ?>');
					$('.errInput').html('');
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
					moveTo('idMainContent');
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
			}
		});
	}

	function action_stokopname_finish(rmstokopname_kd) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/action_periodestockopname_setstatus'; ?>',
				data: {
					'id': rmstokopname_kd,
					'rmstokopname_status': 'finish'
				},
				success: function(html) {
					window.location.reload();
				}
			});
		}
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			delay: 2500,
			styling: 'bootstrap3'
		});
	}

</script>