<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	table_main();

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function(e) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/jenisstockopname_formbatch'; ?>',
			data:{
				slug: 'add',
				rmstokopnamejenis_kd: ''
			},
			success: function(html) {
				toggle_modal('Add', html);
			}
		});
	});

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function table_main() {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/jenisstockopname_tablemain'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').slideDown().html(html);
					moveTo('idMainContent');
				}
			});
		});
	}

	function edit_data(rmstokopnamejenis_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/jenisstockopname_formbatch'; ?>',
			data:{
				slug: 'edit',
				rmstokopnamejenis_kd: rmstokopnamejenis_kd
			},
			success: function(html) {
				toggle_modal('Edit', html);
			}
		});
	}

	function render_rawmaterial() {
		$('#idtxtrm_kd').select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 2,
			ajax: {
				url: '<?php echo base_url() ?>auto_complete/get_raw_material',
				type: "get",
				dataType: 'json',
				delay: 500,
				data: function(params) {
					return {
						paramRM: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function processKartuStok() {
		event.preventDefault();
		let startdate = $('#idtxtstartdate').val();
		let enddate = $('#idtxtenddate').val();
		let jnstransaksi = $('#idtxtjnstransaksi').val();
		let jnssatuan = $('#idtxtjnssatuan').val();
		let rm_kd = $('#idtxtrm_kd').val();
		table_main(startdate, enddate, jnstransaksi, jnssatuan, rm_kd)
	}

	function render_datetimepicker(element) {
		$(element).datetimepicker({
			format: 'YYYY-MM-DD',
		});
	}

	function cetak_data(id) {
		window.open('<?php echo base_url() . $class_link ?>/pdf_main?id=' + id);
	}

	function lihat_data(id) {
		window.location.assign('<?php echo base_url() . $class_link ?>/view_box?id=' + id);
	}

	function toggle_modal(modalTitle, htmlContent) {
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitBatchDetailMaterial(form_id) {
		event.preventDefault();
		var form = document.getElementById(form_id);

		$.ajax({
			url: "<?php echo base_url() . $class_link; ?>/action_jenisstockopnamedetail_batch",
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				var resp = JSON.parse(data);
				console.log(data);
				if (resp.code == 200) {
					notify(resp.status, resp.pesan, 'success');
					var rmstokopnamejenis_kd = $('#idtxtrmstokopnamejenis_kd').val();
					edit_data(rmstokopnamejenis_kd);
					table_main();
					$('.errInput').html('');
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
					moveTo('idMainContent');
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
			}
		});
	}

	function hapus_data(id) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_jenisstockopnamedetail_delete'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						table_main();
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				},
				error: function (request, status, error) {
					notify (status, error, 'error');	
				}
			});
		}
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			delay: 2500,
			styling: 'bootstrap3'
		});
	}
</script>