<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}

	.select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>
<hr>
<table id="idTable" class="table table-bordered table-striped table-hover display responsive" style="width:100%;">
	<thead>
		<tr>
			<th style="width:1%; text-align:center;">No.</th>
			<th style="width:1%; text-align:center;"> Opsi </th>
			<th style="width:4%; text-align:center;">RM Kode</th>
			<th style="width:10%; text-align:center;">RM Deskripsi</th>
			<th style="width:3%; text-align:center;">Qty Current Stock / Satuan</th>
			<th style="width:3%; text-align:center;">Qty Current Stock Secondary / Satuan</th>
			<th style="width:3%; text-align:center;">Qty Opname / Satuan</th>
			<th style="width:3%; text-align:center;">Konversi / Qty Konversi</th>
			<th style="width:3%; text-align:center;">Remark</th>
			<th style="width:3%; text-align:center;">ModifiedAt</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		foreach ($detailRMopnames as $detailRMopname) :
			$rm_qtykonversi = !empty($detailRMopname['rm_qtykonversi']) ? $detailRMopname['rm_qtykonversi'] : 1;
			$satuan_secondary_qty = (float) $detailRMopname['qty_aft_mutasi'] / $rm_qtykonversi;
			/** bg color check */
			$bgcolor = 'yellow';
			if ($detailRMopname['rmstokopnamedetail_qtykonversi'] == $satuan_secondary_qty) {
				$bgcolor = 'green';
			}
		?>
			<tr>
				<td><?php echo $no; ?></td>
				<td style="text-align: center;">
					<?php
					$btns = array();
					$btns[] = get_btn(array('title' => 'Edit', 'icon' => 'pencil', 'onclick' => 'action_editopname(\'' . $detailRMopname['rmstokopnamedetail_kd'] . '\')'));
					// $btns[] = get_btn(array('title' => 'Lihat Kartu Stok', 'icon' => 'search', 'onclick' => 'view_kartustok(\'' . $detailRMopname['rmstokopnamedetail_kd'] . '\')'));
					$btn_group = group_btns($btns);
					echo $btn_group;
					?>
				</td>
				<td><?php echo $detailRMopname['rm_kode'] . ' / ' . $detailRMopname['rm_oldkd']; ?></td>
				<td><?php echo $detailRMopname['rm_deskripsi'] . ' ' . $detailRMopname['rm_spesifikasi']; ?></td>
				<td style="text-align: right;"><?php echo $detailRMopname['qty_aft_mutasi'] . ' ' . $detailRMopname['rmsatuan_nama']; ?></td>
				<td style="text-align: right;"><?php echo $satuan_secondary_qty . ' ' . $detailRMopname['satuan_secondary_nama']; ?></td>
				<td style="text-align: right; font-weight: bold;">
					<?php echo $detailRMopname['rmstokopnamedetail_qty'] . ' ' . $detailRMopname['satuan_opname_nama']; ?>
				</td>
				<td style="text-align: right; font-weight: bold; background-color: <?php echo $bgcolor ?>;"><?php echo $detailRMopname['rmstokopnamedetail_konversi'] . ' | ' . $detailRMopname['rmstokopnamedetail_qtykonversi'] . ' ' . $detailRMopname['rmsatuan_nama'] ?></td>
				<td><?php echo $detailRMopname['rmstokopnamedetail_remark']; ?></td>
				<td><?php echo $detailRMopname['rmstokopnamedetail_tgledit']; ?></td>
			</tr>
		<?php
			$no++;
		endforeach; ?>
	</tbody>
</table>

<script type="text/javascript">
		var table = $('#idTable').DataTable({
			"dom": 'Bfrtip',
			"serverSide": false,
			"paging": false,
			"searching": true,
			"ordering": true,
			"info": false,
			"language": {
				"lengthMenu": "Tampilkan _MENU_ data",
				"zeroRecords": "Maaf tidak ada data yang ditampilkan",
				"info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "",
				"infoEmpty": "Tidak ada data yang ditampilkan",
				"search": "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing": "Sedang Memproses...",
				"paginate": {
					"first": '<span class="fas fa-fast-backward"></span>',
					"last": '<span class="fas fa-fast-forward"></span>',
					"next": '<span class="fas fa-forward"></span>',
					"previous": '<span class="fas fa-backward"></span>'
				}
			},
			// "order":[9, 'desc'],
			"buttons": [{
				"extend": "excelHtml5",
				"title": "RM Stok Opname",
				"exportOptions": {
					"columns": [0, 2,3,4,5,6,7,8,9]
				}
			}],
		});
</script>