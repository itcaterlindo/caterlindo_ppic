<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
if (!empty($rmstokopname_kd)) {
	extract($row);
}

$form_id = 'idFormInputPeriode';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>
<input type="hidden" name="txtrmstokopname_kd" value="<?php echo isset($rmstokopname_kd) ? $rmstokopname_kd : null; ?>">
<div class="row">

	<div class="form-group">
		<label for="idtxtrmstokopname_periode" class="col-md-2 control-label">Tgl Opname</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrrmstokopname_periode"></div>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				<?php echo form_input(array('type' => 'text', 'id' => 'idtxtrmstokopname_periode', 'name' => 'txtrmstokopname_periode', 'class' => 'form-control datetimepicker', 'placeholder' => 'Periode', 'value' => isset($rmstokopname_periode) ? $rmstokopname_periode : date('Y-m-d H:i:s'))); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="idtxtrmstokopnamejenis_kd" class="col-md-2 control-label">Jenis Opname</label>
		<div class="col-md-3 col-xs-12">
			<div id="idErrrmstokopnamejenis_kd"></div>
			<?php echo form_dropdown('txtrmstokopnamejenis_kd', $opsiRmstokopnamejenis, isset($rmstokopnamejenis_kd)? $rmstokopnamejenis_kd : null, array('class'=> 'form-control', 'id'=> 'idtxtrmstokopnamejenis_kd'));?>
		</div>
	</div>

	<div class="form-group">
		<label for="idtxtrmstokopname_note" class="col-md-2 control-label">Note</label>
		<div class="col-sm-4 col-xs-12">
			<textarea name="txtrmstokopname_note" class="form-control" id="idtxtrmstokopname_note" rows="2" placeholder="Note"><?php echo isset($rmstokopname_note) ? $rmstokopname_note: null;?></textarea>
		</div>
	</div>

</div>

<hr>
<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-4 col-md-offset-2 col-xs-12">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitDataPeriode('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button type="reset" name="btnReset" onclick="resetFormMain()" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>