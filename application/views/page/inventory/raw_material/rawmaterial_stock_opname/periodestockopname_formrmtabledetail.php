<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormBatchPeriodeRM';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>
<div id="idErrtxtrmstokopname_kd"></div>
<input type="hidden" name="txtrmstokopname_kd" value="<?php echo isset($rmstokopname_kd) ? $rmstokopname_kd : null; ?>">

<div class="col-md-12">
    <table id="idTableBatchPeriodeRM" class="table table-bordered table-striped table-hover display responsive" style="width:100%;">
        <thead>
            <tr>
                <th style="width:5%; text-align:center;" class="all">
                    <input type="checkbox" id="idcheckall">
                </th>
                <th style="width:25%; text-align:center;" class="all">Kode</th>
                <th style="width:70%; text-align:center;">Deskripsi</th>
            </tr>
        </thead>
    </table>
    <hr>
    <button class="btn btn-sm btn-primary" onclick="submitBatchDetailMaterial('<?php echo $form_id; ?>')"> <i class="fa fa-save"></i> Simpan </button>

</div>
<script type="text/javascript">
     var tableBatch = $('#idTableBatchPeriodeRM').DataTable({
        "processing": true,
        "serverSide": false,
        "ordering": true,
        "paging": false,
        "scrollY": 400,
        "ajax": "<?php echo base_url() . $class_link . '/periodestockopname_formrmtabledetail_data?rmstokopname_kd=' . $rmstokopname_kd; ?>",
        "language": {
            "lengthMenu": "Tampilkan _MENU_ data",
            "zeroRecords": "Maaf tidak ada data yang ditampilkan",
            "info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
            "infoFiltered": "",
            "infoEmpty": "Tidak ada data yang ditampilkan",
            "search": "Cari :",
            "loadingRecords": "Memuat Data...",
            "processing": "Sedang Memproses...",
            "paginate": {
                "first": '<span class="glyphicon glyphicon-fast-backward"></span>',
                "last": '<span class="glyphicon glyphicon-fast-forward"></span>',
                "next": '<span class="glyphicon glyphicon-forward"></span>',
                "previous": '<span class="glyphicon glyphicon-backward"></span>'
            }
        },
        "columnDefs": [{
            "orderable": false,
            "targets": 0,
            "render": function(data, type, full, meta) {
                var htmlChecked = '';
                if (full[3] == 'checked') {
                    htmlChecked = 'checked="checked"';
                }
                var html = '<input type="checkbox" name="txtrm_kds[]" class="classCheck" value="' + data + '" ' + htmlChecked + ' >';
                return html;
            }
        }],
        "order": [1, 'asc'],
    });

    $('#idcheckall').click(function() {
        if ($(this).is(':checked')) {
            $('.classCheck').prop('checked', true);
        } else {
            $('.classCheck').prop('checked', false);
        }
    });
</script>