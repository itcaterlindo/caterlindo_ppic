<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	form_master_main('<?php echo $slug; ?>', '<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');
	render_datetimepicker('.datetimepicker');
	

	$(document).off('select2:select', '#idtxtbagian_kd').on('select2:select', '#idtxtbagian_kd', function() {
		event.preventDefault();
		var bagian_kd = $(this).val();
		render_materialreq(bagian_kd)
			
	});

	$(document).off('select2:select', '#idtxtsrj_kd').on('select2:select', '#idtxtsrj_kd', function() {
		event.preventDefault();
		var kd_srj = $(this).val();
		get_batch(kd_srj)
			
	});

	/** Event ketika clear di kdsrj di click */
	$(document).off('select2:unselecting', '#idtxtsrj_kd').on('select2:unselecting', '#idtxtsrj_kd', function() {
		event.preventDefault();
		var kd_srj = "";
		$("#idtxtmaterialreq_batch").val('');
		get_batch(kd_srj)
	});

	function form_master_main(slug, id) {
		$('#<?php echo $box_content_id; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/form_master_main'; ?>',
				data: {
					slug: slug,
					id: id
				},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').slideDown().html(html);
					render_select2('.select2');
					render_datetimepicker('.datetimepicker')
					moveTo('idMainContent');

					var bagian_kd = $('#idtxtbagian_kd').val();
					if(bagian_kd != ""){
						render_materialreq(bagian_kd)	
					}
				}
			});
		});
	}

	function form_master_tablemain (slug, id){
		$('#<?php echo $box_content_tablemaster_id; ?>').slideUp(function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/form_master_tablemain'; ?>',
				data: {
					slug: slug,
					id: id
				},
				success: function(html) {
					$('#<?php echo $box_content_tablemaster_id; ?>').slideDown().html(html);
				}
			});
		});
	}

	function render_materialreq(bagian_kd){
		$("#idtxtmaterialreq_kds").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?= base_url().$class_link ?>/get_materialreqs',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramMaterialreq: params.term, // search term
						bagian_kd: bagian_kd
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function submitDataMaster(form_id) {
		box_overlay_formmaster('in');
		event.preventDefault();
		var form = document.getElementById(form_id);
		var url = '<?php echo base_url() . $class_link . '/action_insert_master'; ?>';

		$.ajax({
			url: url,
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				var resp = JSON.parse(data);
				console.log(resp);
				if (resp.code == 200) {
					window.location.assign('<?php echo base_url() . $class_link ?>/form_box?id=' + resp.data.id);
					notify(resp.status, resp.pesan, 'success');
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
				box_overlay_formmaster('out');
			}
		});
	}

	
	function get_batch(kd_srj) {
		$("#idtxtmaterialreq_batch").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?php echo base_url() ?>/Auto_complete/get_batch_from_goodsreceive',
				type: "get",
				dataType: 'json',
				data: function (params) {
					return {
						paramSRJ: kd_srj,
						paramTerm: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function toggle_modal(modalTitle, htmlContent) {
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function box_overlay_formmaster(sts) {
		if (sts == 'in') {
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		} else if (sts == 'out') {
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}
</script>