<?php
defined('BASEPATH') or exit('No direct script access allowed!');

/* --SET DEFAULT PROPERTY UNTUK BOX-- */
$js_file = 'view_js';
$box_type = 'Table';
$box_title = 'View Material Receipt';
$data['master_var'] = 'MaterialReceipt';
$data['class_link'] = $class_link;
$data['box_id'] = 'id'.$box_type.'Box'.$data['master_var'];
$data['btn_hide_id'] = 'id'.$box_type.'BtnBoxHide'.$data['master_var'];
$data['btn_add_id'] = 'id'.$box_type.'BtnBoxAdd'.$data['master_var'];
$data['btn_remove_id'] = 'id'.$box_type.'BtnBoxRemove'.$data['master_var'];
$data['box_alert_id'] = 'id'.$box_type.'BoxAlert'.$data['master_var'];
$data['box_loader_id'] = 'id'.$box_type.'BoxLoader'.$data['master_var'];
$data['box_content_id'] = 'id'.$box_type.'BoxContent'.$data['master_var'];
$data['box_overlay_id'] = 'id'.$box_type.'BoxOverlay'.$data['master_var'];
/* --END OF BOX DEFAULT PROPERTY-- */

/* --SET VARIABEL TAMBAHAN DISINI-- */
$data['modal_title'] = 'Form';
$data['content_modal_id'] = 'id'.$box_type.'ModalContentDetail'.$data['master_var'];

$data['id'] = $id;
/* --END OF CUSTOM PROPERTY-- */

/* --SET BUTTONS DISINI, NILAI "FALSE" UNTUK TIDAK TAMPIL, NILAI "TRUE" UNTUK MENAMPILKAN BUTTON-- */
$btn_hide = TRUE;
$btn_add = FALSE;
$btn_remove = TRUE;
/* --END OF BUTTONS SETUP-- */
?>

<div class="box box-primary" id="<?php echo $data['box_id']; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_title; ?></h3>
		<div class="box-tools pull-right">
			<?php
			if ($btn_add) :
				?>
				<button class="btn btn-box-tool" id="<?php echo $data['btn_add_id']; ?>" data-toggle="tooltip" title="Tambah Data">
					<i class="fa fa-plus"></i>
				</button>
				<?php
			endif;
			if ($btn_hide) :
				?>
				<button class="btn btn-box-tool btn-collapse" id="<?php echo $data['btn_hide_id']; ?>" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
					<i class="fa fa-minus"></i>
				</button>
				<?php
			endif;
			if ($btn_remove) :
				?>
				<button class="btn btn-box-tool btn-remove" id="<?php echo $data['btn_remove_id']; ?>" data-widget="remove" data-toggle="tooltip" title="Close">
					<i class="fa fa-times"></i>
				</button>
				<?php
			endif;
			?>
		</div>
	</div>
	<div class="box-body">
		<!-- <div id="<?php //echo $data['box_alert_id']; ?>"></div>
		<div id="<?php //echo $data['box_loader_id']; ?>" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div> -->
		<div class="row invoice-info">
			<div class="col-sm-6 invoice-col">
				<b>MaterialReceipt No :</b> <?php echo isset($header['materialreceipt_no']) ? $header['materialreceipt_no'] : '-'; ?><br>
				<b>MaterialReceipt Status :</b> <?php echo build_badgecolor($header['wfstate_badgecolor'], $header['wfstate_nama']); ?> <br>
			</div>
			<div class="col-sm-6 invoice-col">
				<b>Date :</b> <?php echo isset($header['materialreceipt_tanggal']) ? $header['materialreceipt_tanggal'] : '-'; ?><br>
				<b>MatereialReq No:</b> <?php echo isset($header['materialreq_nos']) ? $header['materialreq_nos'] : '-'; ?>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<div id="<?php echo $data['box_content_id']; ?>"></div>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<?php echo $generate_button; ?>
		<button class="btn btn-sm btn-default pull-right" onclick="window.history.back()"> <i class="fa fa-arrow-circle-left"></i> Kembali </button>
	</div>
	<div class="overlay" id="<?php echo $data['box_overlay_id']; ?>" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
	<?php $this->load->view('page/'.$class_link.'/'.$js_file, $data); ?>
</div>


<!-- modal -->
<div class="modal fade" id="idmodal">
	<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"><?php echo $data['modal_title']; ?></h4>
		</div>
		<div class="modal-body">

			<div class="row">
				<div id="<?php echo $data['content_modal_id'];?>"></div>
			</div>
			
		</div>
		<div class="modal-footer">
		<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Keluar</button>
		</div>
	</div>
	</div>
</div>