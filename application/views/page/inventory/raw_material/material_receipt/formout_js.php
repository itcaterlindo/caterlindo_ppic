<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	formout_main('<?php echo $slug; ?>', '<?php echo $id; ?>');
	formout_tabledetail('<?php echo $slug; ?>', '<?php echo $id; ?>');
	formout_tabledetailmaterial('<?php echo $slug; ?>', '<?php echo $id; ?>');
	formout_tabledetailrmbom('<?php echo $slug; ?>', '<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');
	first_load('<?php echo $box_loader_id2; ?>', '<?php echo $box_content_id2; ?>');

	$('#btnSelesai').on('click', function(e){
		e.preventDefault();
		// BUTTON SELESAI BERFUNGSI UNTUK CEK APAKAH ADA QTY_OUT YANG MELEBIHI CURRENT STOCK
		// Jika variabel ini true maka tampilkan modal (qty_out > current_stock)
		var qtyOutOverCurrentStock = false;
		// Table based on BOM
		var arrDataBased = [];
		var htmlTableBased = "";
		$("#idTableForm tr").not(':first').each(function(){
			var currentRow = $(this);
			var col1_value=currentRow.find("td:eq(0)").text();
			var col2_value=currentRow.find("td:eq(1)").text();
			var col3_value=currentRow.find("td:eq(2)").text();
			var col4_value=currentRow.find("td:eq(3)").text();
			var col5_value=currentRow.find("td:eq(4)").text();
			if( parseFloat(col4_value) > parseFloat(col5_value)){
				htmlTableBased += '<tr> <td>'+col1_value+'</td> <td>'+col2_value+'</td> <td>'+col3_value+'</td> <td>'+col4_value+'</td> <td>'+col5_value+'</td> </tr>';
				qtyOutOverCurrentStock = true;
			}
			var obj = {};
			obj.rm_kode=col1_value;
			obj.rm_deskripsi=col2_value;
			obj.qty_bom=col3_value;
			obj.qty_out=col4_value;
			obj.current_stock=col5_value;
			arrDataBased.push(obj);
		});
		// Table based not on BOM
		var arrDataNotBased = [];
		var htmlTableNotBased = "";
		$("#idTableFormNot tr").not(':first').each(function(){
			var currentRow = $(this);
			var col1_value=currentRow.find("td:eq(0)").text();
			var col2_value=currentRow.find("td:eq(1)").text();
			var col3_value=currentRow.find("td:eq(2)").text();
			var col4_value=currentRow.find("td:eq(3)").text();
			if( parseFloat(col3_value) > parseFloat(col4_value)){
				htmlTableNotBased += '<tr> <td>'+col1_value+'</td> <td>'+col2_value+'</td> <td>'+col3_value+'</td> <td>'+col4_value+'</td> </tr>';
				qtyOutOverCurrentStock = true;
			}
			var obj = {};
			obj.rm_kode=col1_value;
			obj.rm_deskripsi=col2_value;
			obj.qty_out=col3_value;
			obj.current_stock=col4_value;
			arrDataNotBased.push(obj);
		});
		if(qtyOutOverCurrentStock == true){
			$('.table-based-body').html(htmlTableBased);
			$('.table-not-based-body').html(htmlTableNotBased);
			$('.modal-alert-qty-out').modal('show');
		}else{
			window.location.assign('<?php echo base_url() . $class_link; ?>');
		}

	});
	

	$(document).off('click', '#idcheckall').on('click', '#idcheckall', function() {
		if ($(this).is(':checked')) {
			$('.classCheck').prop('checked', true);
		} else {
			$('.classCheck').prop('checked', false);
		}
	});

	$(document).off('select2:selecting', '#idtxtrm_kd').on("select2:selecting", '#idtxtrm_kd', function(e) {
		let dt = e.params.args.data;
		$('#idtxtrm_deskripsi').val(dt.rm_deskripsi + dt.rm_spesifikasi);
		$('#idtxtmaterialreceiptdetailrm_satuankonversi').val(dt.rmsatuan_kd);
		$('#idtxtrmsatuan_nama').val(dt.rmsatuan_nama);
		$('#idtxtmaterialreceiptdetailrm_satuan').append($("<option selected></option>").val(dt.rmsatuan_kd).text(dt.rmsatuan_nama)).trigger('change');
		$('#idtxtmaterialreceiptdetailrm_konversi').val(1);
		$('#idtxtmaterialreceiptdetailrm_nama').val(dt.rm_nama);
		$('#idtxtmaterialreceiptdetailrm_deskripsi').val(dt.rm_deskripsi);
		$('#idtxtmaterialreceiptdetailrm_spesifikasi').val(dt.rm_spesifikasi);
		$('#idtxtmaterialreceiptdetailrm_qty').focus();
		render_konversi(dt.rmsatuan_kd, dt.rmsatuan_kd);
		render_codesrj(dt.rm_kd);
	});

	$(document).off('keyup', '.clKonversi').on('keyup', '.clKonversi', function() {
		let materialreceiptdetailrm_qty = $('#idtxtmaterialreceiptdetailrm_qty').val();
		let materialreceiptdetailrm_konversi = $('#idtxtmaterialreceiptdetailrm_konversi').val();
		sumKonversiResult(materialreceiptdetailrm_qty, materialreceiptdetailrm_konversi)
	});

	$(document).off('select2:selecting', '#idtxtmaterialreceiptdetailrm_satuan').on("select2:selecting", '#idtxtmaterialreceiptdetailrm_satuan', function(e) {
		let dt = e.params.args.data;
		var rmsatuan_kd_to = $('#idtxtmaterialreceiptdetailrm_satuan').val();
		render_konversi(dt.id, rmsatuan_kd_to);
	});

	$(document).off('select2:selecting', '#idtxtkonversi').on("select2:selecting", '#idtxtkonversi', function(e) {
		var dt = e.params.args.data;
		var materialreceiptdetailrm_qty = $('#idtxtmaterialreceiptdetailrm_qty').val();
		$('#idtxtmaterialreceiptdetailrm_konversi').val(dt.rmsatuankonversi_konversi);
		sumKonversiResult(materialreceiptdetailrm_qty, dt.rmsatuankonversi_konversi)
	});

	$(document).off('select2:selecting', '#idtxtmaterialreceiptdetailrm_kodesrj').on("select2:selecting", '#idtxtmaterialreceiptdetailrm_kodesrj', function(e) {
		var dt = e.params.args.data;
		render_batch(dt.rmgr_code_srj);
	});

	function sumKonversiResult(materialreceiptdetailrm_qty, materialreceiptdetailrm_konversi){
		$('#idtxtmaterialreceiptdetailrm_qtykonversi').val(materialreceiptdetailrm_qty * materialreceiptdetailrm_konversi);
	}

	function formout_main(slug, id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/formout_main'; ?>',
			data: {
				slug: slug,
				id: id
			},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
				render_rm();
				render_satuan();
				render_woitemreceipt(id);
				$('#idtxtmaterialreceiptdetailrm_kodesrj, #idtxtmaterialreceiptdetailrm_batch').select2({
					theme: 'bootstrap',
					placeholder: '--Pilih Opsi--',
				});
			}
		});
	}

	function render_rm() {
		$("#idtxtrm_kd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 3,
			ajax: {
				url: '<?php echo base_url() . 'auto_complete' ?>/get_raw_material',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function(params) {
					return {
						paramRM: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_satuan() {
		$("#idtxtmaterialreceiptdetailrm_satuan, .clMaterialreceiptdetailrm_satuan").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: {
				url: '<?php echo base_url() ?>auto_complete/get_rm_satuan',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function(params) {
					return {
						paramSatuan: params.term // search term
					};
				},
				processResults: function(response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_konversi(rmsatuan_kd_from = null, rmsatuan_kd_to = null) {
		$("#idtxtkonversi").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?php echo base_url() ?>/Auto_complete/get_rm_satuan_konversi',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						rmsatuan_kd_from: rmsatuan_kd_from,
						rmsatuan_kd_to: rmsatuan_kd_to,
						paramSatuan: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_woitemreceipt(materialreceipt_kd) {
		$("#idtxtmaterialreceiptdetail_kd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?php echo base_url().$class_link; ?>/get_woitemreceipt',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						materialreceipt_kd: materialreceipt_kd,
						paramWoitemreceipt: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_codesrj(rm_kd){
		$("#idtxtmaterialreceiptdetailrm_kodesrj").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?php echo base_url() ?>/Auto_complete/get_kdsrj_from_goodsreceive_by_rm',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramRMKD: rm_kd,
						paramTerm: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_batch(kd_srj){
		$("#idtxtmaterialreceiptdetailrm_batch").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?php echo base_url() ?>/Auto_complete/get_batch_from_goodsreceive',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramSRJ: kd_srj, 
						paramTerm: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function formout_tabledetail(slug, id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/formout_tabledetail'; ?>',
			data: {
				slug: slug,
				id: id
			},
			success: function(html) {
				$('#<?php echo $box_content_id_detail; ?>').slideDown().html(html);
			}
		});
	}

	function formout_tabledetailmaterial(slug, id) {
		$('#<?php echo $box_overlay_id3; ?>').fadeIn();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/formout_tabledetailmaterial'; ?>',
			data: {
				slug: slug,
				id: id
			},
			success: function(html) {
				$('#<?php echo $box_content_id_detail_material; ?>').html(html);
				$('#<?php echo $box_overlay_id3; ?>').fadeOut();
			}
		});
	}

	function formout_tabledetailrmbom(slug, id){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/formout_tabledetailrmbomv2'; ?>',
			data: {
				slug: slug,
				id: id
			},
			success: function(html) {
				$('#<?php echo $box_content_tblrmbom_id; ?>').html(html);
				render_satuan();
			}
		});
	}

	function submitData(form_id) {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById(form_id);
		var url = '<?php echo base_url() . $class_link . '/action_insert_detail_rawmaterial'; ?>';

		$.ajax({
			url: url,
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				console.log(data);
				var resp = JSON.parse(data);
				if (resp.code == 200) {
					notify(resp.status, resp.pesan, 'success');
					formout_main('<?php echo $slug; ?>', '<?php echo $id; ?>');
					formout_tabledetailmaterial('<?php echo $slug; ?>', '<?php echo $id; ?>');
					formout_tabledetail('<?php echo $slug; ?>', '<?php echo $id; ?>');
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
				box_overlay('out');
			}
		});
	}

	function submitDataBatch(form_id) {
        box_overlay('in');
        event.preventDefault();
        var form = document.getElementById(form_id);
        var url = '<?php echo base_url() . $class_link . '/action_insert_detail_rawmaterial_batch_v2'; ?>';

        $.ajax({
            url: url,
            type: "POST",
            data: new FormData(form),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                console.log(data);
                var resp = JSON.parse(data);
                if (resp.code == 200) {
                    notify(resp.status, resp.pesan, 'success');
                    formout_tabledetailmaterial('<?php echo $slug; ?>', '<?php echo $id; ?>');
					formout_tabledetail('<?php echo $slug; ?>', '<?php echo $id; ?>');
                    formout_tabledetailrmbom('<?php echo $slug; ?>', '<?php echo $id; ?>');
                } else if (resp.code == 401) {
                    $.each(resp.pesan, function(key, value) {
                        $('#' + key).html(value);
                    });
                    generateToken(resp.csrf);
                } else if (resp.code == 400) {
                    notify(resp.status, resp.pesan, 'error');
                    generateToken(resp.csrf);
                } else {
                    notify('Error', 'Error tidak Diketahui', 'error');
                    generateToken(resp.csrf);
                }
                box_overlay('out');
            }
        });
    }

	function hapus_item(id) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			var url = '<?php echo base_url() . $class_link ?>/action_delete_detail_rawmaterial';
			$.ajax({
				url: url,
				type: 'GET',
				data: 'id=' + id,
				success: function(data) {
					var resp = JSON.parse(data);
					if (resp.code == 200) {
						notify(resp.status, resp.pesan, 'success');
						formout_tabledetail('<?php echo $slug; ?>', '<?php echo $id; ?>');
						formout_tabledetailmaterial('<?php echo $slug; ?>', '<?php echo $id; ?>');
						formout_tabledetailrmbom('<?php echo $slug; ?>', '<?php echo $id; ?>');
					} else {
						notify('Gagal', resp.pesan, 'error');
					}
				}
			});
		}
	}

	function action_recalculate(id) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			var url = '<?php echo base_url() . $class_link ?>/action_recalculate_detail_rawmaterial';
			$.ajax({
				url: url,
				type: 'GET',
				data: 'id=' + id,
				success: function(data) {
					var resp = JSON.parse(data);
					if (resp.code == 200) {
						notify(resp.status, resp.pesan, 'success');
						window.location.reload();
					} else {
						notify('Gagal', resp.pesan, 'error');
					}
				}
			});
		}
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			delay: 2500,
			styling: 'bootstrap3'
		});
	}

	function toggle_modal(modalTitle, htmlContent) {
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function box_overlay(sts) {
		if (sts == 'in') {
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		} else if (sts == 'out') {
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}
</script>