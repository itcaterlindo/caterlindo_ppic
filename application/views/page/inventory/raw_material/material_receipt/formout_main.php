<?php
defined('BASEPATH') or exit('No direct script access allowed');
$form_id = 'idForm';
if (isset($rowData)) {
	extract($rowData);
}
?>
<style>
	.select2-container {
    width: 100% !important;
    padding: 0;
}
</style>

<?php
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtslug', 'name' => 'txtslug', 'value' => isset($slug) ? $slug : null));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtmaterialreceipt_kd', 'name' => 'txtmaterialreceipt_kd', 'value' => isset($id) ? $id : null));
?>

<!-- <div class="col-md-12"> -->
	<div class="form-group">
		<label for="idtxtmaterialreceiptdetail_kd" class="col-md-2 control-label">No WO</label>
		<div class="errInput" id="idErrmaterialreceiptdetail_kd"></div>
		<div class="col-sm-4 col-xs-12">
			<select name="txtmaterialreceiptdetail_kd" id="idtxtmaterialreceiptdetail_kd" class="form-control"></select>
		</div>
	</div>

	<div class="form-group">
		<label for="idtxtrm_kd" class="col-md-2 control-label">Raw Material</label>
		<div class="col-sm-6 col-xs-12">
			<div class="errInput" id="idErrrm_kd"></div>
			<input type="hidden" name="txtmaterialreceiptdetailrm_satuankonversi" id="idtxtmaterialreceiptdetailrm_satuankonversi">
			<input type="hidden" name="txtmaterialreceiptdetailrm_nama" id="idtxtmaterialreceiptdetailrm_nama">
			<input type="hidden" name="txtmaterialreceiptdetailrm_deskripsi" id="idtxtmaterialreceiptdetailrm_deskripsi">
			<input type="hidden" name="txtmaterialreceiptdetailrm_spesifikasi" id="idtxtmaterialreceiptdetailrm_spesifikasi">
			<select id="idtxtrm_kd" name="txtrm_kd" class="form-control"> </select>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtrm_deskripsi' class="col-md-2 control-label">Deskripsi Material</label>
		<div class="col-sm-6 col-xs-12">
			<div class="errInput" id="idErrrm_deskripsi"></div>
			<?php echo form_textarea(array('type' => 'text', 'class' => 'form-control clMaterial', 'name' => 'txtrm_deskripsi', 'id' => 'idtxtrm_deskripsi', 'placeholder' => 'Deskripsi Material', 'readonly' => 'readonly', 'rows' => '2', 'value' => isset($rm_deskripsi) ? $rm_deskripsi : null)); ?>
		</div>
	</div>
	<div class="form-group">
		<label for='idtxtmaterialreceiptdetailrm_qty' class="col-md-2 control-label">Qty Receipt</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrmaterialreceiptdetailrm_qty"></div>
			<?php echo form_input(array('type' => 'number', 'id' => 'idtxtmaterialreceiptdetailrm_qty', 'name' => 'txtmaterialreceiptdetailrm_qty', 'class' => 'form-control clKonversi', 'placeholder' => 'Qty')); ?>
		</div>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrmaterialreceiptdetailrm_satuan"></div>
			<select id="idtxtmaterialreceiptdetailrm_satuan" name="txtmaterialreceiptdetailrm_satuan" class="form-control"> </select>
		</div>
	</div>
	<div class="form-group">
		<label for='idtxtmaterialreceiptdetailrm_konversi' class="col-md-2 control-label">Konversi</label>
		<div class="col-sm-2 col-xs-12">
			<select name="txtkonversi" id="idtxtkonversi" class="form-control"></select>
		</div>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrmaterialreceiptdetailrm_konversi"></div>
			<?php echo form_input(array('type' => 'number', 'id' => 'idtxtmaterialreceiptdetailrm_konversi', 'name' => 'txtmaterialreceiptdetailrm_konversi', 'class' => 'form-control clKonversi', 'placeholder' => 'Qty')); ?>
		</div>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrmaterialreceiptdetailrm_qtykonversi"></div>
			<?php echo form_input(array('type' => 'number', 'id' => 'idtxtmaterialreceiptdetailrm_qtykonversi', 'name' => 'txtmaterialreceiptdetailrm_qtykonversi', 'class' => 'form-control', 'placeholder' => 'Qty', 'readonly' => 'readonly')); ?>
		</div>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrrmsatuan_nama"></div>
			<?php echo form_input(array('type' => 'text', 'id' => 'idtxtrmsatuan_nama', 'name' => 'txtrmsatuan_nama', 'class' => 'form-control clMaterial', 'readonly' => 'readonly', 'placeholder' => 'Satuan')); ?>
		</div>
	</div>
	<div class="form-group">
		<label for='idtxtrm_kdsrj' class="col-md-2 control-label">Kode SRJ</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrrm_kdsrj"></div>
			<select class="form-control select2" name="txtmaterialreceiptdetailrm_kodesrj" id="idtxtmaterialreceiptdetailrm_kodesrj" data-allow-clear='true'>
				<option value="">--Pilih Opsi--</option>
				<option value="<?= $masterMRC['rmgr_code_srj'] ?>" selected><?= $masterMRC['rmgr_code_srj'] ?></option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for='idtxtrm_batch' class="col-md-2 control-label">Batch</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrrm_batch"></div>
			<select class="form-control select2" name="txtmaterialreceiptdetailrm_batch" id="idtxtmaterialreceiptdetailrm_batch" data-allow-clear='true'>
				<option value="">--Pilih Opsi--</option>
				<option value="<?= $masterMRC['batch'] ?>" selected><?= $masterMRC['batch'] ?></option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-4 col-sm-offset-2 col-xs-12">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button name="btnSubmit" id="idbtnSubmitMain" onclick="event.preventDefault(); window.location.reload();" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
		</div>
	</div>

<!-- </div> -->

<!-- /.box-body -->
<?php echo form_close(); ?>