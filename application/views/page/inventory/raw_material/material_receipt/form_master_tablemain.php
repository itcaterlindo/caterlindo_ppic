<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }
</style>

<div class="col-md-12">
    <div class="form-group">
        <label for="idmaterialreq_kds" class="col-sm-2 control-label"> </label>
        <div class="col-md-6">
            <div class="errClass" id="idErrmaterialreq_kds"></div>
            <table id="idtablemain" cellspacing="0" cellpadding="1" style="font-size: 80%;" border="1" width="100%">
                <thead>
                    <tr>
                        <th width="5%" style="text-align: center; font-weight:bold;">No.</th>
                        <th width="30%" style="text-align: center; font-weight:bold;">No Material Requisition</th>
                        <th width="15%" style="text-align: center; font-weight:bold;">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    foreach ($results as $r) :
                    ?>
                        <tr>
                            <td width="5%" style="text-align: center;"><?php echo $no; ?></td>
                            <td width="30%"><?php echo $r['materialreq_no']; ?></td>
                            <td width="15%" style="text-align: center;">
                                <button class="btn btn-xs btn-danger" onclick="action_delete_master_detail('<?php echo $r['materialreceiptreq_kd']; ?>')"> <i class="fa fa-trash"></i> </button>
                            </td>
                        </tr>
                    <?php
                        $no++;
                    endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>