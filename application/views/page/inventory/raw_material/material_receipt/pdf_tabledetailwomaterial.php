<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }
</style>
<table id="idtablemain" cellspacing="0" cellpadding="1" border="1" width="100%">
    <thead>
        <tr style="font-size: 80%;">
            <td width="7%" style="text-align: center; font-weight:bold;">No WO</td>
            <td width="15%" style="text-align: center; font-weight:bold;">Item Code</td>
            <td width="10%" style="text-align: center; font-weight:bold;">Jenis</td>
            <td width="25%" style="text-align: center; font-weight:bold;">Deskripsi</td>
            <td width="13%" style="text-align: center; font-weight:bold;">Qty Unit</td>
            <td width="10%" style="text-align: center; font-weight:bold;">Qty Total</td>
            <td width="10%" style="text-align: center; font-weight:bold;">Qty Out</td>
            <td width="10%" style="text-align: center; font-weight:bold;">Satuan</td>
        </tr>
    </thead>
    <tbody>
        <?php
        $arrRMBasedByBoms = [];
        foreach ($materialDetailByReceipts as $materialDetailByReceipt) : ?>
            <tr style="font-size: 70%; font-weight: bold;">
                <td width="7%" style="text-align: center;"><?php echo $materialDetailByReceipt['woitem_no_wo']; ?></td>
                <td width="15%"><?php echo $materialDetailByReceipt['woitem_itemcode']; ?></td>
                <td width="10%"><?php echo $materialDetailByReceipt['bagian_nama'] . " / " . ucwords($materialDetailByReceipt['woitem_jenis']); ?></td>
                <td width="25%"><?php echo "{$materialDetailByReceipt['woitem_deskripsi']} {$materialDetailByReceipt['woitem_dimensi']}"; ?></td>
                <td width="13%" style="text-align: right;"><?php echo $materialDetailByReceipt['materialreceiptdetail_qty']; ?></td>
                <td width="10%"></td>
                <td width="10%"></td>
                <td width="10%">Unit</td>
            </tr>
            <?php foreach ($materialDetailByReceipt['detailParts'] as $detailPart) : ?>
                <tr style="font-size: 60%;">
                    <td width="7%"></td>
                    <td width="15%"><?php echo $detailPart['rm_oldkd']; ?></td>
                    <td width="10%" style="text-align: center;"><?php echo $detailPart['rm_kode']; ?></td>
                    <td width="25%"><?php echo "{$detailPart['partdetail_deskripsi']} {$detailPart['partdetail_spesifikasi']}"; ?></td>
                    <td width="13%" style="text-align: right;"><?php echo $detailPart['sum_partdetail_qty']; ?></td>
                    <td width="10%" style="text-align: right;">
                        <?php
                        $qtyTotal = $detailPart['sum_partdetail_qty'] * $materialDetailByReceipt['materialreceiptdetail_qty'];
                        echo number_format($qtyTotal, 2);
                        ?>
                    </td>
                    <td width="10%" style="text-align: right;">
                        <?php
                        if (isset($materialOutByWOs[$materialDetailByReceipt['woitem_kd']])) :
                            foreach ($materialOutByWOs[$materialDetailByReceipt['woitem_kd']] as $materialOutByWO) :
                                if ($materialOutByWO['rm_kd'] == $detailPart['rm_kd']) :
                                    echo $materialOutByWO['sum_materialreceiptdetailrm_qtykonversi'];
                                    $arrRMBasedByBoms[$materialDetailByReceipt['woitem_kd']][] = $detailPart['rm_kd'];
                                endif;
                            endforeach;
                        endif; ?>
                    </td>
                    <td width="10%"><?php echo $detailPart['rmsatuan_nama']; ?></td>
                </tr>
                <?php
            endforeach;
            if (isset($materialOutByWOs[$materialDetailByReceipt['woitem_kd']])) :
                $arrRMBasedByBoms[$materialDetailByReceipt['woitem_kd']] = isset($arrRMBasedByBoms[$materialDetailByReceipt['woitem_kd']]) ? $arrRMBasedByBoms[$materialDetailByReceipt['woitem_kd']] : [];
                foreach ($materialOutByWOs[$materialDetailByReceipt['woitem_kd']] as $eachOut) :
                    if (!in_array($eachOut['rm_kd'], $arrRMBasedByBoms[$materialDetailByReceipt['woitem_kd']])) :
                ?>
                        <tr style="font-size: 60%;">
                            <td width="7%"></td>
                            <td width="15%"><?php echo $eachOut['rm_oldkd']; ?></td>
                            <td width="10%" style="text-align: center;"><?php echo $eachOut['rm_kode']; ?></td>
                            <td width="25%"><?php echo "{$eachOut['materialreceiptdetailrm_deskripsi']} {$eachOut['materialreceiptdetailrm_spesifikasi']}"; ?></td>
                            <td width="13%" style="text-align: right;"></td>
                            <td width="10%"></td>
                            <td width="10%" style="text-align: right;"><?php echo $eachOut['sum_materialreceiptdetailrm_qtykonversi']; ?></td>
                            <td width="10%"><?php echo $eachOut['satuankonversi_nama']; ?></td>
                        </tr>
        <?php
                    endif;
                endforeach;
            endif;
        endforeach; ?>
    </tbody>
</table>