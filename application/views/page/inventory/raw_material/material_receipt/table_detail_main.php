<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}
</style>

<button class="btn btn-sm btn-warning" id="idbtndeleteitem"> <i class="fa fa-check-circle-o"></i> Hapus Item</button>
<hr>
<table id="idTable" class="table table-bordered table-striped table-hover display" style="width:100%; font-size: 90%;">
	<thead>
		<tr>
			<th style="width:1%; text-align: center;">No.</th>
			<th style="width:1%; text-align: center;"> <input type="checkbox" class="chkitem" id="idchkall"> </th>
			<th style="width:10%; text-align: center;">No WO</th>
			<th style="width:10%; text-align: center;">Item Code</th>
			<th style="width:20%; text-align: center;">Deskripsi</th>
			<th style="width:10%; text-align: center;">Jenis</th>
			<th style="width:5%; text-align: center;">Qty</th>
			<th style="width:15%; text-align: center;">Note</th>
			<th style="width:15%; text-align: center;">Kode SRJ</th>
			<th style="width:15%; text-align: center;">Batch</th>
			<th style="width:10%; text-align: center;">ModifiedAt</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		foreach ($results as $r) : ?>
			<tr>
				<td><?php echo $no; ?></td>
				<td> <input type="checkbox" name="txtmaterialreceiptdetail_kds[]" class="classCheckwoitemdetail" value="<?php echo $r['materialreceiptdetail_kd']; ?>"> </td>
				<td><?php echo $r['woitem_no_wo']; ?></td>
				<td><?php echo $r['woitem_itemcode']; ?></td>
				<td><?php echo "{$r['woitem_deskripsi']} / {$r['woitem_dimensi']}"; ?></td>
				<td><?php echo $r['woitem_jenis']; ?></td>
				<td><?php echo $r['materialreceiptdetail_qty']; ?></td>
				<td><?php echo $r['materialreceiptdetail_remark']; ?></td>
				<td><?php echo $r['rmgr_code_srj']; ?></td>
				<td><?php echo $r['batch']; ?></td>
				<td><?php echo $r['materialreceiptdetail_tgledit']; ?></td>
			</tr>
		<?php
			$no++;
		endforeach; ?>
	</tbody>
</table>

<script type="text/javascript">
	$('#idchkall').on('click', function() {
		if ($(this).is(':checked')) {
			$('.classCheckwoitemdetail').prop('checked', true);
		} else {
			$('.classCheckwoitemdetail').prop('checked', false);
		}
	});

	$('#idbtndeleteitem').click(function() {
		var txtmaterialreceiptdetail_kds = [];
		// Read all checked checkboxes
		$("input:checkbox[class=classCheckwoitemdetail]:checked").each(function() {
			txtmaterialreceiptdetail_kds.push($(this).val());
		});
		var url = '<?php echo base_url() . $class_link . '/action_insert_detail_batch'; ?>';

		if (txtmaterialreceiptdetail_kds.length > 0) {
			var confirmdelete = confirm("Apakah Anda yakin?");
			if (confirmdelete == true) {
				$.ajax({
					url: '<?php echo base_url() . $class_link . '/action_delete_detail_batch'; ?>',
					type: 'GET',
					data: {
						txtmaterialreceiptdetail_kds: txtmaterialreceiptdetail_kds
					},
					success: function(data) {
						var resp = JSON.parse(data);
						if (resp.code == 200) {
							notify(resp.status, resp.pesan, 'success');
							window.location.reload();
						}else{
							notify(resp.status, resp.pesan, 'error');
						}
					}
				});
			}
		}
	});
	
	var table = $('#idTable').DataTable({
		"processing": true,
		"serverSide": false,
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"order":[8, 'desc'],
	});
</script>