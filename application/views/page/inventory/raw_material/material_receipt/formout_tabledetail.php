<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<table id="idTableDetailOut" class="table table-bordered table-striped table-hover display responsive" style="width:100%; font-size: 90%;">
	<thead>
		<tr>
			<th style="width:1%; text-align: center;">No.</th>
			<th style="width:1%; text-align: center;" >Opsi</th>
			<th style="width:25%; text-align: center;">No WO</th>
			<th style="width:25%; text-align: center;">Raw Material</th>
			<th style="width:7%; text-align: center;">Qty</th>
			<th style="width:10%; text-align: center;">Satuan</th>
			<th style="width:7%; text-align: center;">Qty Konversi</th>
			<th style="width:7%; text-align: center;">Satuan Konversi</th>	
			<th style="width:7%; text-align: center;">Kode SRJ</th>
			<th style="width:7%; text-align: center;">Batch</th>		
			<th style="width:10%; text-align: center;">ModifiedAt</th>
		</tr>
	</thead>
</table>

<script type="text/javascript">
	$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};
	var table = $('#idTableDetailOut').DataTable({
		"processing": true,
		"serverSide": false,
		"ajax": "<?php echo base_url().$class_link.'/formout_tabledetail_data?id='.$id; ?>",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"columnDefs": [
			{"data": null, "searchable": false, "className": "dt-center", "targets": 0},
			{"className": "dt-right", "targets": 4},
			{"className": "dt-right", "targets": 6},
		],
		"order":[8, 'desc'],
		"rowCallback": function (row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			var index = page * length + (iDisplayIndex + 1);
			$('td:eq(0)', row).html(index);
		}
	});
</script>