<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$js_file = 'table_js';
$box_title = 'Table Material Receipt';
$data['class_link'] = $class_link;
$box_type = 'Table';
$data['master_var'] = 'MaterialRequisition';
$data['box_id'] = 'id' . $box_type . 'Box' . $data['master_var'];
$data['box_content_id'] = 'id' . $box_type . 'BoxContent' . $data['master_var'];
$data['box_loader_id'] = 'id' . $box_type . 'BoxLoader' . $data['master_var'];
$data['btn_add_id'] = 'id' . $box_type . 'BtnBoxAdd' . $data['master_var'];

$data['modal_title'] = 'Modal Default';
$data['content_modal_id'] = 'idModal' . $box_type . $data['master_var'];
?>

<div class="box box-primary" id="<?php echo $data['box_id']; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_title; ?></h3>
		<div class="box-tools pull-right">
			<?php if (cek_permission('MATERIALRECEIPT_CREATE')) : ?>
				<button class="btn btn-box-tool" id="<?php echo $data['btn_add_id']; ?>" data-toggle="tooltip" title="Tambah"><i class="fa fa-plus"></i></button>
			<?php endif; ?>
			<button class="btn btn-box-tool btn-collapse" id="idBtnBoxTableSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<div class="box-body">
		<div id="<?php echo $data['box_loader_id']; ?>" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="<?php echo $data['box_content_id']; ?>" style="display: none;"></div>
	</div>
	<div class="box-footer">
	</div>
	<div class="overlay" id="idBoxTableOverlay" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
</div>

<?php
$this->load->view('page/' . $class_link . '/' . $js_file, $data); ?>

<!-- modal -->
<div class="modal fade" id="idmodal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><?php echo $data['modal_title']; ?></h4>
			</div>
			<div class="modal-body">

				<div class="row">
					<div id="<?php echo $data['content_modal_id']; ?>"></div>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Keluar</button>
			</div>
		</div>
	</div>
</div>