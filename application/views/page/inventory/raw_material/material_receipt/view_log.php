<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
?>

<?php foreach ($result as $r) : ?>

<ul class="timeline">
    <li>
        <i class="fa fa-comments bg-yellow"></i>
        <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i> <?= format_date($r['materialreceiptlog_tglinput'], 'Y-m-d H:i:s');?></span>
            <h4 class="timeline-header"> <strong> <?= $r['nm_admin']; ?> </strong> <small> mengubah dari <strong> <?= $r['state_source']?> </strong> ke  <strong><?= $r['state_dst'];?> </strong> </small> </h4>
        <div class="timeline-body"><?= $r['materialreceiptlog_note'];?></div>
        </div>
    </li>
</ul> 

<?php endforeach;?>