<?php
defined('BASEPATH') or exit('No direct script access allowed');
$form_id = 'idFormMaster';
if (isset($rowData)) {
    extract($rowData);
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtslug', 'name' => 'txtslug', 'value' => isset($slug) ? $slug : null));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtmaterialreceipt_kd', 'name' => 'txtmaterialreceipt_kd', 'value' => isset($materialreceipt_kd) ? $materialreceipt_kd : null));
?>

<div class="col-md-12">
    <div class="form-group">
        <label for="idtxtbagian_kd" class="col-sm-2 control-label">Tanggal</label>
        <div class="col-md-2">
            <div class="errClass" id="idErrmaterialreq_tgl"></div>
            <?php echo form_input(array('type' => 'text', 'id' => 'idtxtmaterialreq_tgl', 'name' => 'txtmaterialreq_tgl', 'class' => 'form-control datetimepicker', 'placeholder' => 'Tanggal', 'value' => isset($materialreceipt_tanggal) ? $materialreceipt_tanggal : date('Y-m-d'))); ?>
        </div>
    </div>
    <div class="form-group">
        <label for="idtxtbagian_kd" class="col-sm-2 control-label">Bagian</label>
        <div class="col-md-4">
            <div class="errClass" id="idErrbagian_kd"></div>
            <?php echo form_dropdown('txtbagian_kd', isset($opsiBagians) ? $opsiBagians : [], isset($bagian_kd)? $bagian_kd : [], array('class'=> 'form-control select2', 'id'=> 'idtxtbagian_kd'));?>
        </div>
    </div>
    <div class="form-group">
        <label for="idmaterialreq_kds" class="col-sm-2 control-label">Material Req</label>
        <div class="col-md-4">
            <div class="errClass" id="idErrmaterialreq_kds"></div>
            <select name="txtmaterialreq_kds[]" id="idtxtmaterialreq_kds" class="form-control select2" multiple="multiple">
                <?php
                if (isset($receiptReqs)):
                    foreach($receiptReqs as $receiptReq):
                        echo "<option value='{$receiptReq['materialreq_kd']}' selected>{$receiptReq['materialreq_no']} | {$receiptReq['bagian_nama']}</option>";
                    endforeach;
                endif;
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="idtxtsrj_kd" class="col-sm-2 control-label">Kode Surat Jalan</label>
        <div class="col-md-3">
            <div class="errClass" id="idErrsrj_kd"></div>
            <?php echo form_dropdown('txtsrj_kd', isset($opsiGoodsReceives) ? $opsiGoodsReceives : [], isset($rmgr_code_srj)? $rmgr_code_srj : [], array('class'=> 'form-control select2', 'id'=> 'idtxtsrj_kd', 'data-allow-clear' => 'true'));?>
        </div>
    </div>
    <div class="form-group">
        <label for="idtxtbatch" class="col-sm-2 control-label">Batch</label>
        <div class="col-md-2">
            <div class="errClass" id="idErrBatch"></div>
            <select class="form-control select2" name="txtmaterialreq_batch" id="idtxtmaterialreq_batch" data-allow-clear='true'>
                <option value="">--Pilih Opsi--</option>
                <?php if( isset($batch) ){ ?>
                    <option value="<?= $batch ?>" selected><?= $batch ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
    <label for="idtxtbatch" class="col-sm-2 control-label">&nbsp;</label>
        <div class="col-md-12">
            <button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitDataMaster('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm btn-flat">
                <i class="fa fa-save"></i> Simpan
            </button>
        </div>
    </div>
</div>

<!-- /.box-body -->
<?php echo form_close(); ?>