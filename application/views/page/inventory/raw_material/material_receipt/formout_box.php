<?php
defined('BASEPATH') or exit('No direct script access allowed!');

$js_file = 'formout_js';
$box_title = 'Form Material Receipt';
$data['class_link'] = $class_link;
$box_type = 'Table';
$data['master_var'] = 'MaterialReceipt';
$data['box_id'] = 'id' . $box_type . 'Box' . $data['master_var'];
$data['box_content_id'] = 'id' . $box_type . 'BoxContent' . $data['master_var'];
$data['box_loader_id'] = 'id' . $box_type . 'BoxLoader' . $data['master_var'];
$data['btn_add_id'] = 'id' . $box_type . 'BtnBoxAdd' . $data['master_var'];
$data['btn_remove_id'] = 'id' . $box_type . 'BtnBoxRemove' . $data['master_var'];
$data['box_overlay_id'] = 'id' . $box_type . 'BoxOverlay' . $data['master_var'];

$data['box_content_id2'] = 'id' . $box_type . 'BoxContent2' . $data['master_var'];
$data['box_loader_id2'] = 'id' . $box_type . 'BoxLoader2' . $data['master_var'];
$data['box_overlay_id2'] = 'id' . $box_type . 'BoxOverlay2' . $data['master_var'];
$data['box_overlay_id3'] = 'id' . $box_type . 'BoxOverlay3' . $data['master_var'];

$data['box_content_id_detail'] = 'id' . $box_type . 'BoxContentDetail' . $data['master_var'];
$data['box_content_id_detail_material'] = 'id' . $box_type . 'BoxContentDetailMaterial' . $data['master_var'];
$data['box_content_tblrmbom_id'] = 'id' . $box_type . 'BoxContentDetailMaterialBoM' . $data['master_var'];

$data['modal_title'] = 'Modal Default';
$data['content_modal_id'] = 'idModal' . $box_type . $data['master_var'];

$data['class_link'] = $class_link;
$data['id'] = $id;
$data['slug'] = $slug;
?>

<div class="box box-primary" id="<?php echo $data['box_id']; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_title; ?></h3>
		<div class="box-tools pull-right">
			<!-- <button class="btn btn-box-tool" id="<?php //echo $data['btn_add_id'];
														?>" data-toggle="tooltip" title="Tambah"><i class="fa fa-plus"></i></button> -->
			<button class="btn btn-box-tool btn-collapse" id="idBtnBoxTableSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<div class="box-body">
		<div id="<?php echo $data['box_loader_id']; ?>" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div class="row invoice-info">
			<div class="col-sm-6 invoice-col">
				<b>MaterialReceipt No :</b> <?php echo isset($header['materialreceipt_no']) ? $header['materialreceipt_no'] : '-'; ?><br>
				<b>MaterialReceipt Status :</b> <?php echo build_badgecolor($header['wfstate_badgecolor'], $header['wfstate_nama']); ?> <br>
			</div>
			<div class="col-sm-6 invoice-col">
				<b>Date :</b> <?php echo isset($header['materialreceipt_tanggal']) ? $header['materialreceipt_tanggal'] : '-'; ?><br>
				<b>MatereialReq No:</b> <?php echo isset($header['materialreq_nos']) ? $header['materialreq_nos'] : '-'; ?>
			</div>
		</div>
		<hr>
		<button class="btn btn-default btn-sm" onclick="event.preventDefault(); window.location.assign('<?php echo base_url() . $class_link . '/form_box?id=' . $id; ?>');"> <i class="fa fa-arrow-circle-left"></i> Back </button>
		<button class="btn btn-warning btn-sm" onclick="action_recalculate('<?php echo $id; ?>')"> <i class="fa fa-refresh"></i> Recalculate </button>
		<button class="btn btn-success btn-sm" id="btnSelesai"> <i class="fa fa-check"></i> Selesai </button>
		<hr>
		<div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
				<li class="active"><a href="#tab_1" data-toggle="tab">Based on BoM</a></li>
				<li><a href="#tab_2" data-toggle="tab">Not Based on BoM</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1">
				<div id="<?php echo $data['box_content_tblrmbom_id']; ?>"></div>
				</div>
				<div class="tab-pane" id="tab_2">
					<div class="col-md-12">
						<div id="<?php echo $data['box_content_id']; ?>"></div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div class="box-footer">
	</div>
	<div class="overlay" id="<?php echo $data['box_overlay_id']; ?>" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
</div>

<div class="row">
    <!-- Col1 -->
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo "Table Detail Out"; ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool btn-collapse" id="idBtnBoxTableSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>	
            </div>
            <div class="box-body">
                <div id="<?php echo $data['box_loader_id2']; ?>" align="middle">
                    <i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
                </div>
                <div id="<?php echo $data['box_content_id_detail']; ?>" style="display: none;"></div>
            </div>
            <div class="box-footer">
            </div>
            <div class="overlay" id="<?php echo $data['box_overlay_id2']; ?>" style="display: none;">
                <i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
            </div>
        </div>
    </div>
    <!-- Col2 -->
    <div class="col-md-5">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo "Table Detail Material BOM"; ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool btn-collapse" id="idBtnBoxTableSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
				<button class="btn btn-sm btn-default" onclick="event.preventDefault(); formout_tabledetailmaterial('', '<?php echo $id; ?>')"> <i class="fa fa-refresh"></i> Refresh</button>
				<hr>
                <!-- <div id="<?php echo $data['box_loader_id3']; ?>" align="middle">
                    <i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
                </div> -->
                <div id="<?php echo $data['box_content_id_detail_material']; ?>"></div>
            </div>
            <div class="box-footer">
            </div>
            <div class="overlay" id="<?php echo $data['box_overlay_id3']; ?>" style="display: none;">
                <i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('page/' . $class_link . '/' . $js_file, $data); ?>


<!-- modal -->
<div class="modal fade" id="idmodal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><?php echo $data['modal_title']; ?></h4>
			</div>
			<div class="modal-body">

				<div class="row">
					<div id="<?php echo $data['content_modal_id']; ?>"></div>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Keluar</button>
			</div>
		</div>
	</div>
</div>

<!-- modal alert ketika qty out melebihi current stock -->
<div class="modal fade modal-alert-qty-out" id="modalAlertQtyOut">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Terdapat material <b>QTY OUT MELEBIHI CURRENT STOCK</b>, apakah anda ingin melanjutkan?</h4>
			</div>
			<div class="modal-body">
			<label for=""> Based BOM</label>
			<table class="table table-striped table-based">
				<thead>
					<tr>
						<th scope="col">RM Kode</th>
						<th scope="col">RM Deskripsi</th>
						<th scope="col">Qty BOM</th>
						<th scope="col">Qty OUT</th>
						<th scope="col">Current Stock</th>
					</tr>
				</thead>
				<tbody class="table-based-body">
					<!-- CONTENT -->
				</tbody>
			</table>
			<hr>
			<label for=""> Not Based BOM</label>
			<table class="table table-striped table-not-based">
				<thead>
					<tr>
						<th scope="col">RM Kode</th>
						<th scope="col">RM Deskripsi</th>
						<th scope="col">Qty OUT</th>
						<th scope="col">Current Stock</th>
					</tr>
				</thead>
				<tbody class="table-not-based-body">
					<!-- CONTENT -->
				</tbody>
			</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary pull-left" onclick="window.location.assign('<?php echo base_url() . $class_link; ?>');"><i class="fa fa-arrow-right"></i> Lanjutkan</button>
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
			</div>
		</div>
	</div>
</div>