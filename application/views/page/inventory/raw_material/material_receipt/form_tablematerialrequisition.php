<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$form_id = 'idFormBatchAll';
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}
</style>
<?php echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal')); ?>
<button class="btn btn-default btn-sm" onclick="event.preventDefault(); window.location.assign('<?php echo base_url() . $class_link . '/formout_box?id=' . $id; ?>');"> <i class="fa fa-arrow-circle-right"></i> Next</button>
<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitDataBatch('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
<hr>
<div class="row">
	<div class="col-md-12">
		<div id="idErrmaterialreceipt_kd"></div>
		<input type="hidden" name="txtmaterialreceipt_kd" value="<?php echo $id; ?>">
		<table id="idTableForm" class="table table-bordered table-striped table-hover display" style="width:100%; font-size: 90%;">
			<thead>
				<tr>
					<th style="width:1%; text-align: center;">
						<input type="checkbox" id="idcheckall">
					</th>
					<th style="width:10%; text-align: center;">No Requisition</th>
					<th style="width:10%; text-align: center;">No WO</th>
					<th style="width:10%; text-align: center;">Item Code</th>
					<th style="width:10%; text-align: center;">Jenis</th>
					<th style="width:15%; text-align: center;">Deskripsi</th>
					<th style="width:10%; text-align: center;">Qty Requisition</th>
					<th style="width:10%; text-align: center;">Qty Receipt</th>
					<th style="width:10%; text-align: center;">Kode SRJ</th>
					<th style="width:10%; text-align: center;">Batch</th>
					<th style="width:10%; text-align: center;">Remark</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($results as $r) : ?>
					<tr>
						<td><input type="checkbox" name="txtmaterialreqdetail_kds[]" class="classCheck" value="<?php echo $r['materialreqdetail_kd']; ?>"></td>
						<td><?php echo $r['materialreq_no']; ?></td>
						<td><?php echo $r['woitem_no_wo']; ?></td>
						<td><?php echo $r['woitem_itemcode']; ?></td>
						<td><?php echo $r['woitem_jenis']; ?></td>
						<td><?php echo "{$r['woitem_deskripsi']} {$r['woitem_dimensi']}"; ?></td>
						<td><?php echo $r['materialreqdetail_qty']; ?></td>
						<td>
							<input type="hidden" name="txtmaterialreceiptreq_kds[<?php echo $r['materialreqdetail_kd']; ?>]" value="<?php echo $r['materialreceiptreq_kd'] ?>">
							<input type="hidden" name="txtwoitem_kds[<?php echo $r['materialreqdetail_kd']; ?>]" value="<?php echo $r['woitem_kd'] ?>">
							<input type="hidden" name="txtmaterialreq_kds[<?php echo $r['materialreqdetail_kd']; ?>]" value="<?php echo $r['materialreq_kd'] ?>">
							<input type="number" class="form-control" placeholder="Qty" name="txtmaterialreceiptdetail_qty[<?php echo $r['materialreqdetail_kd']; ?>]" value="<?php echo $r['materialreqdetail_qty']; ?>">
						</td>
						<td><input type="text" class="form-control" placeholder="Kode SRJ" name="txtsrj_kd[<?php echo $r['materialreqdetail_kd']; ?>]" value="<?php echo $r['rmgr_code_srj'] ?>"></td>
						<td><input type="text" class="form-control" placeholder="Batch" name="txtbatch[<?php echo $r['materialreqdetail_kd']; ?>]" value="<?php echo $r['batch'] ?>"></td>
						<td> <textarea name="txtmaterialreceiptdetail_remarks[<?php echo $r['materialreqdetail_kd']; ?>]" class="form-control" placeholder="Remark" rows="1"></textarea></td>
					</tr>
				<?php
				endforeach; ?>
			</tbody>
		</table>
		<hr>
		<?php echo form_close(); ?>
	</div>
</div>


<script type="text/javascript">
	$('#idTableForm').DataTable({
		"paging": false,
		"ordering": true,
		"searching": true,
		"scrollY": "250px",
		"info": false,
		"language": {
			"lengthMenu": "Tampilkan _MENU_ data",
			"zeroRecords": "Maaf tidak ada data yang ditampilkan",
			"info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "(difilter dari _MAX_ total data)",
			"infoEmpty": "Tidak ada data yang ditampilkan",
			"search": "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing": "Sedang Memproses...",
			"paginate": {
				"first": '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last": '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next": '<span class="glyphicon glyphicon-forward"></span>',
				"previous": '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"order": [1, 'asc'],
	});
</script>