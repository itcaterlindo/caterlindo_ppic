<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }
</style>
<h5>Detail WO Receipt</h5>
<table id="idtablemain" cellspacing="0" cellpadding="1" border="1" width="100%">
    <thead>
        <tr style="font-size: 80%;">
            <td width="15%" style="text-align: center; font-weight:bold;">No WO</td>
            <td width="20%" style="text-align: center; font-weight:bold;">Item Code</td>
            <td width="10%" style="text-align: center; font-weight:bold;">Jenis</td>
            <td width="40%" style="text-align: center; font-weight:bold;">Deskripsi</td>
            <td width="15%" style="text-align: center; font-weight:bold;">Qty Receipt</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach($resultDetails as $resultDetail): ?>
        <tr style="font-size: 80%;">
            <td width="15%" style="text-align: center;"><?php echo $resultDetail['woitem_no_wo'];?></td>
            <td width="20%"><?php echo $resultDetail['woitem_itemcode'];?></td>
            <td width="10%"><?php echo $resultDetail['woitem_jenis'];?></td>
            <td width="40%"><?php echo "{$resultDetail['woitem_deskripsi']} {$resultDetail['woitem_dimensi']}";?></td>
            <td width="15%" style="text-align: right;"><?php echo $resultDetail['materialreceiptdetail_qty'];?></td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>
<h5>Detail Material Receipt Out</h5>
<table id="idtablemain2" cellspacing="0" cellpadding="1" border="1" width="100%">
    <thead>
        <tr style="font-size: 80%;">
            <th width="5%" style="text-align: center; font-weight:bold;">No.</th>
            <th width="20%" style="text-align: center; font-weight:bold;">RM Code</th>
            <th width="50%" style="text-align: center; font-weight:bold;">RM Desc</th>
            <th width="10%" style="text-align: center; font-weight:bold;">Qty</th>
            <th width="15%" style="text-align: center; font-weight:bold;">UoM</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        
        foreach ($resultMaterialDetails as $resultMaterialDetail) :
        ?>
            <tr style="font-size: 80%;">
                <td width="5%" style="text-align: center;"><?php echo $no; ?></td>
                <td width="20%" style="text-align: center;"><?php echo "{$resultMaterialDetail['rm_oldkd']} / {$resultMaterialDetail['rm_kode']}"; ?></td>
                <td width="50%" style="text-align: left;"><?php echo "{$resultMaterialDetail['materialreceiptdetailrm_deskripsi']} {$resultMaterialDetail['materialreceiptdetailrm_spesifikasi']}"; ?></td>
                <td width="10%" style="text-align: right;"><?php echo $resultMaterialDetail['sum_materialreceiptdetailrm_qtykonversi']; ?></td>
                <td width="15%" style="text-align: left;"><?php echo $resultMaterialDetail['satuankonversi_nama']; ?></td>
            </tr>
        <?php
            $no++;
        endforeach; ?>
    </tbody>
    <tfoot>
    </tfoot>
</table>