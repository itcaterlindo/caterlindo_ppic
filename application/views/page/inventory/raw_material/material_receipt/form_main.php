<?php
defined('BASEPATH') or exit('No direct script access allowed');
$form_id = 'idFormMaster';
if (isset($rowData)) {
    extract($rowData);
}

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtslug', 'name' => 'txtslug', 'value' => isset($slug) ? $slug : null));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtmaterialreceipt_kd', 'name' => 'txtmaterialreceipt_kd', 'value' => isset($id) ? $id : null));
?>

<div class="col-md-12">
    <div class="form-group">
        <div id="idtabledetailreq"></div>
    </div>   
</div>

<!-- /.box-body -->
<?php echo form_close(); ?>