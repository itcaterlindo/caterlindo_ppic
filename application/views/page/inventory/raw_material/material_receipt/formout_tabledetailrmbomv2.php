<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$form_id = 'idFormBatch';
?>
<style type="text/css">
    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }
</style>
<?php
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>
<input type="hidden" name="txtmaterialreceipt_kd" value="<?php echo $id; ?>">
<table id="idtablemainBatch" class="table table-bordered table-striped table-hover display" style="font-size: 80%;" width="100%">
    <thead>
        <tr>
            <th width="2%" style="text-align: center; font-weight:bold;">
                <input type="checkbox" id="idcheckall">
            </th>
            <th width="2%" style="text-align: center; font-weight:bold;">No</th>
            <th width="5%" style="text-align: center; font-weight:bold;">No WO</th>
            <th width="5%" style="text-align: center; font-weight:bold;">RM Kode</th>
            <th width="5%" style="text-align: center; font-weight:bold;">RM Deskripsi</th>
            <th width="5%" style="text-align: center; font-weight:bold;">Qty BoM</th>
            <th width="5%" style="text-align: center; font-weight:bold;">Satuan BoM</th>
            <th width="10%" style="text-align: center; font-weight:bold;">Qty Out</th>
            <th width="10%" style="text-align: center; font-weight:bold;">Satuan Out</th>
            <th width="10%" style="text-align: center; font-weight:bold;">Konversi</th>
            <th width="7%" style="text-align: center; font-weight:bold; background-color: aqua;">Qty Konversi</th>
            <th width="8%" style="text-align: center; font-weight: bold; "> Qty Sudah Out</th>
            <th width="8%" style="text-align: center; font-weight: bold; "> Kode SRJ</th>
            <th width="8%" style="text-align: center; font-weight: bold; "> Batch</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($materialDetailByReceipts as $r) :
        ?>
            <tr style="font-weight: bold; background-color: gray;">
                <td></td>
                <td><?php echo $no; ?></td>
                <td><?php echo $r['woitem_no_wo']; ?></td>
                <td colspan="2"><?php echo "{$r['woitem_deskripsi']} {$r['woitem_dimensi']}" ?></td>
                <td style="text-align: right;"><?php echo $r['materialreceiptdetail_qty']; ?></td>
                <td><? echo $r['woitem_satuan']; ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php
            $no++;
            foreach ($r['detailParts'] as $dtParts) :
                $dtParts['rm_kd'] = $dtParts['materialreceiptdetail_kd'] . '-' . $dtParts['rm_kd'];

                $qtyBalanced = round($r['materialreceiptdetail_qty'] * $dtParts['sum_partdetail_qty'], 2);
                $qtyBomReceipt = $qtyBalanced;
                if (isset($resultOuts[$dtParts['rm_kd']])) {
                    $qtyBalanced = round($qtyBalanced - $resultOuts[$dtParts['rm_kd']], 2);
                }
            ?>
                <tr>
                    <td><input type="checkbox" class="classCheck" name="txtchkrmkds[]" value="<?php echo $dtParts['rm_kd']; ?>"></td>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $r['woitem_no_wo']; ?></td>
                    <td><?php echo $dtParts['rm_kode']; ?></td>
                    <td>
                        <input type="hidden" name="txtmaterialreceiptdetailrm_namaBatch[<?php echo $dtParts['rm_kd']; ?>]" value="<?php echo $dtParts['partdetail_nama'] ?>">
                        <input type="hidden" name="txtmaterialreceiptdetailrm_deskripsiBatch[<?php echo $dtParts['rm_kd']; ?>]" value="<?php echo urlencode($dtParts['partdetail_deskripsi']) ?>">
                        <input type="hidden" name="txtmaterialreceiptdetailrm_spesifikasiBatch[<?php echo $dtParts['rm_kd']; ?>]" value="<?php echo urlencode($dtParts['partdetail_spesifikasi']) ?>">
                        <?php echo "{$dtParts['partdetail_deskripsi']} {$dtParts['partdetail_spesifikasi']}"; ?>
                    </td>
                    <td style="text-align: right;"><?php echo $dtParts['sum_partdetail_qty']; ?></td>
                    <td>
                        <?php echo $dtParts['rmsatuan_nama']; ?>
                        <input type="hidden" id="idtxtmaterialreceiptdetailrm_satuankonversiBatch<?php echo $dtParts['rm_kd']; ?>" name="txtmaterialreceiptdetailrm_satuankonversiBatch[<?php echo $dtParts['rm_kd']; ?>]" value="<?php echo $dtParts['rmsatuan_kd']; ?>">
                    </td>
                    <td>
                        <input type="text" name="txtmaterialreceiptdetailrm_qtyBatch[<?php echo $dtParts['rm_kd']; ?>]" id="idtxtmaterialreceiptdetailrm_qtyBatch<?php echo $dtParts['rm_kd']; ?>" class="form-control input-sm clQtyOut" data-rm_kd="<?php echo $dtParts['rm_kd']; ?>" value="<?php echo $qtyBalanced; ?>">
                    </td>
                    <td>
                        <select name="txtmaterialreceiptdetailrm_satuanBatch[<?php echo $dtParts['rm_kd'] ?>]" class="form-control input-sm clMaterialreceiptdetailrm_satuan" data-rm_kd="<?php echo $dtParts['rm_kd']; ?>" id="idtxtmaterialreceiptdetailrm_satuanBatch<?php echo $dtParts['rm_kd']; ?>">
                            <option value="<?php echo $dtParts['rmsatuan_kd'] ?>"><?php echo $dtParts['rmsatuan_nama']; ?></option>
                        </select>
                    </td>
                    <td>
                        <input type="number" step="any" name="txtmaterialreceiptdetailrm_konversiBatch[<?php echo $dtParts['rm_kd']; ?>]" id="idtxtmaterialreceiptdetailrm_konversiBatch<?php echo $dtParts['rm_kd']; ?>" data-rm_kd="<?php echo $dtParts['rm_kd']; ?>" class="form-control input-sm clKonversi" value="1">
                    </td>
                    <td>
                        <select name="" class="form-control input-sm clMaterialreceiptdetailrm_satuankonversis" id="idtxtmaterialreceiptdetailrm_satuankonversis<?php echo $dtParts['rm_kd']; ?>" data-rm_kd="<?php echo $dtParts['rm_kd']; ?>"></select>
                    </td>
                    <td>
                        <input type="number" name="txtmaterialreceiptdetailrm_qtykonversiBatch[<?php echo $dtParts['rm_kd']; ?>]" id="idtxtmaterialreceiptdetailrm_qtykonversiBatch<?php echo $dtParts['rm_kd']; ?>" readonly="readonly" class="form-control input-sm" value="<?php echo $qtyBalanced; ?>">
                    </td>
                    <td>
                        <input type="number" id="idtxtmaterialreceiptdetailrm_qtyOut<?php echo $dtParts['rm_kd']; ?>" class="form-control input-sm" readonly="readonly" placeholder="Qty" value="<?php echo isset($resultOuts[$dtParts['rm_kd']]) ? $resultOuts[$dtParts['rm_kd']] : 0; ?>">
                    </td>
                    <td>
                        <input type="text" name="txtmaterialreceiptdetailrm_kodesrj[<?php echo $dtParts['rm_kd']; ?>]" id="idtxtmaterialreceiptdetailrm_kodesrj<?php echo $dtParts['rm_kd']; ?>" class="form-control input-sm" placeholder="Kode SRJ" value="<?php echo isset($dtParts['rmgr_code_srj']) ? $dtParts['rmgr_code_srj'] : ""; ?>">
                    </td>
                    <td>
                        <input type="text" name="txtmaterialreceiptdetailrm_batch[<?php echo $dtParts['rm_kd']; ?>]" id="idtxtmaterialreceiptdetailrm_batch<?php echo $dtParts['rm_kd']; ?>" class="form-control input-sm" placeholder="Batch" value="<?php echo isset($dtParts['batch']) ? $dtParts['batch'] : ""; ?>">
                    </td>
                </tr>
            <?php
                $no++;
            endforeach; ?>

            <!-- <tr>
                <td><input type="checkbox" class="classCheck" name="txtchkrmkds[]" value="<?php echo $r['rm_kd']; ?>"></td>
                <td><?php echo $r['rm_kode']; ?></td>
                <td style="text-align: left;">
                    <input type="hidden" name="txtmaterialreceiptdetailrm_namaBatch[<?php echo $r['rm_kd']; ?>]" value="<?php echo $r['partdetail_nama'] ?>">
                    <input type="hidden" name="txtmaterialreceiptdetailrm_deskripsiBatch[<?php echo $r['rm_kd']; ?>]" value="<?php echo urlencode($r['partdetail_deskripsi']) ?>">
                    <input type="hidden" name="txtmaterialreceiptdetailrm_spesifikasiBatch[<?php echo $r['rm_kd']; ?>]" value="<?php echo urlencode($r['partdetail_spesifikasi']) ?>">
                    <?php echo "{$r['partdetail_deskripsi']} {$r['partdetail_spesifikasi']}"; ?>
                </td>
                <td style="text-align: right;" id="idtxtbomqty<?php echo $r['rm_kd']; ?>"><?php echo $r['sum_partdetail_qty']; ?></td>
                <td style="text-align: left;">
                    <input type="hidden" id="idtxtmaterialreceiptdetailrm_satuankonversiBatch<?php echo $r['rm_kd']; ?>" name="txtmaterialreceiptdetailrm_satuankonversiBatch[<?php echo $r['rm_kd']; ?>]" value="<?php echo $r['rmsatuan_kd']; ?>">
                    <?php echo $r['rmsatuan_nama']; ?>
                </td>
                <td>
                    <input type="text" name="txtmaterialreceiptdetailrm_qtyBatch[<?php echo $r['rm_kd']; ?>]" id="idtxtmaterialreceiptdetailrm_qtyBatch<?php echo $r['rm_kd']; ?>" class="form-control input-sm clQtyOut" data-rm_kd="<?php echo $r['rm_kd']; ?>" value="<?php echo $qtyBalanced; ?>">
                </td>
                <td>
                    <select name="txtmaterialreceiptdetailrm_satuanBatch[<?php echo $r['rm_kd'] ?>]" class="form-control input-sm clMaterialreceiptdetailrm_satuan" data-rm_kd="<?php echo $r['rm_kd']; ?>" id="idtxtmaterialreceiptdetailrm_satuanBatch<?php echo $r['rm_kd']; ?>">
                        <option value="<?php echo $r['rmsatuan_kd'] ?>"><?php echo $r['rmsatuan_nama']; ?></option>
                    </select>
                </td>
                <td>
                    <select name="" class="form-control input-sm clMaterialreceiptdetailrm_satuankonversis" id="idtxtmaterialreceiptdetailrm_satuankonversis<?php echo $r['rm_kd']; ?>" data-rm_kd="<?php echo $r['rm_kd']; ?>"></select>
                    <input type="number" step="any" name="txtmaterialreceiptdetailrm_konversiBatch[<?php echo $r['rm_kd']; ?>]" id="idtxtmaterialreceiptdetailrm_konversiBatch<?php echo $r['rm_kd']; ?>" data-rm_kd="<?php echo $r['rm_kd']; ?>" class="form-control input-sm clKonversi" value="1">
                </td>
                <td>
                    <input type="number" name="txtmaterialreceiptdetailrm_qtykonversiBatch[<?php echo $r['rm_kd']; ?>]" id="idtxtmaterialreceiptdetailrm_qtykonversiBatch<?php echo $r['rm_kd']; ?>" readonly="readonly" class="form-control input-sm" value="<?php echo $qtyBalanced; ?>">
                </td>
                <td>
                    <input type="number" id="idtxtmaterialreceiptdetailrm_qtyOut<?php echo $r['rm_kd']; ?>" class="form-control input-sm" readonly="readonly" placeholder="Qty" value="<?php echo isset($resultOuts[$r['rm_kd']]) ? $resultOuts[$r['rm_kd']] : 0; ?>">
                </td>
            </tr> -->
        <?php
        endforeach; ?>
    </tbody>
    <tfoot>
    </tfoot>
</table>
<button class="btn btn-sm btn-primary" onclick="submitDataBatch('<?php echo $form_id; ?>')"> <i class="fa fa-save"></i> Simpan</button>

<?php echo form_close(); ?>

<script type="text/javascript">
    $('.clQtyOut, .clKonversi').on('keyup', function(e) {
        var thisQtyOutKey = $(this).attr('data-rm_kd');
        sumQtyKonversi(thisQtyOutKey);
    });

    function sumQtyKonversi(thisQtyOutKey) {
        var thisQtyOut = $('#idtxtmaterialreceiptdetailrm_qtyBatch' + thisQtyOutKey).val();
        var thisKonversi = $('#idtxtmaterialreceiptdetailrm_konversiBatch' + thisQtyOutKey).val();

        var idtxtmaterialreceiptdetailrm_qtykonversiBatch = 'idtxtmaterialreceiptdetailrm_qtykonversiBatch' + thisQtyOutKey;
        var materialreceiptdetailrm_qtyOut = $('#idtxtmaterialreceiptdetailrm_qtyOut' + thisQtyOutKey).val();
        var qtyOutKonversi = thisQtyOut * thisKonversi;
        $('#' + idtxtmaterialreceiptdetailrm_qtykonversiBatch).val(qtyOutKonversi);
    }

    $('.clMaterialreceiptdetailrm_satuankonversis').on("select2:selecting", function(e) {
        var dt = e.params.args.data;
        var thisSatuanKonversiKey = $(this).attr('data-rm_kd');
        $('#idtxtmaterialreceiptdetailrm_konversiBatch' + thisSatuanKonversiKey).val(dt.rmsatuankonversi_konversi);
        sumQtyKonversi(thisSatuanKonversiKey);
    });

    $('.clMaterialreceiptdetailrm_satuan').on("select2:selecting", function(e) {
        let dt = e.params.args.data;
        var thisSatuanOutKey = $(this).attr('data-rm_kd');
        var rmsatuan_kd_from = dt.id;
        var rmsatuan_kd_to = $('#idtxtmaterialreceiptdetailrm_satuankonversiBatch' + thisSatuanOutKey).val();
        render_konversi_tblbatch('#idtxtmaterialreceiptdetailrm_satuankonversis' + thisSatuanOutKey, rmsatuan_kd_from, rmsatuan_kd_to);
    });

    function render_konversi_tblbatch(target, rmsatuan_kd_from = null, rmsatuan_kd_to = null) {
        $(target).select2({
            theme: 'bootstrap',
            placeholder: '--Pilih Opsi--',
            minimumInputLength: 0,
            ajax: {
                url: '<?php echo base_url() ?>/Auto_complete/get_rm_satuan_konversi',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        rmsatuan_kd_from: rmsatuan_kd_from,
                        rmsatuan_kd_to: rmsatuan_kd_to,
                        paramSatuan: params.term // search term
                    };
                },
                processResults: function(response) {
                    return {
                        results: response
                    };
                },
                cache: true
            }
        });
    }

    // Setup - add a text input to each footer cell
    $('#idtablemainBatch thead tr').clone(true).appendTo('#idtablemainBatch thead');
    $('#idtablemainBatch thead tr:eq(0) th').each(function(i) {
        var title = $(this).text();
        var iShowSearch = [2, 3, 4];
        if (iShowSearch.includes(i)) {
            $(this).html('<input type="text" placeholder="Cari ' + title + '" />');
            $('input', this).on('keyup change', function() {
                if (table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        } else {
            $(this).html('-');

        }
    });

    var table = $('#idtablemainBatch').DataTable({
        "paging": false,
        "ordering": true,
        "searching": true,
        "scrollY": "300px",
        "scrollX": true,
        "info": false,
        "language": {
            "lengthMenu": "Tampilkan _MENU_ data",
            "zeroRecords": "Maaf tidak ada data yang ditampilkan",
            "info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
            "infoFiltered": "(difilter dari _MAX_ total data)",
            "infoEmpty": "Tidak ada data yang ditampilkan",
            "search": "Cari :",
            "loadingRecords": "Memuat Data...",
            "processing": "Sedang Memproses...",
            "paginate": {
                "first": '<span class="glyphicon glyphicon-fast-backward"></span>',
                "last": '<span class="glyphicon glyphicon-fast-forward"></span>',
                "next": '<span class="glyphicon glyphicon-forward"></span>',
                "previous": '<span class="glyphicon glyphicon-backward"></span>'
            }
        },
        "columnDefs": [{
                "orderable": false,
                "searchable": false,
                "className": "dt-center",
                "targets": 0
            },
            {
                "orderable": false,
                "targets": [2, 3, 4]
            }
        ],
        "order": [1, 'asc'],
    });
</script>