<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$form_id = 'idFormBatchAll';
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<h4> <u> Table RM based on BoM </u> </h4>
		<table id="idTableForm" class="table table-bordered table-striped table-hover display clTblOut" style="width:100%; font-size: 90%;">
			<thead>
				<tr>
					<th style="width:7%; text-align: center;">RM Kode</th>
					<th style="width:20%; text-align: center;">RM Deskripsi</th>
					<th style="width:10%; text-align: center;">Qty BOM</th>
					<th style="width:10%; text-align: center;">Qty Out</th>
					<th style="width:10%; text-align: center;">Current Stock</th>
					<th style="width:10%; text-align: center;">Satuan</th>
					<th style="width:10%; text-align: center;">Status</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$rmBasedOnBOMs = [];
				foreach ($results as $r) :
					$qtyOut = 0;
					$sum_partdetail_qty = round($r['sum_partdetail_qty'], 2);
					foreach ($resultRMOuts as $resultRMOut) {
						if ($resultRMOut['rm_kd'] == $r['rm_kd']) {
							$qtyOut = round($resultRMOut['sum_materialreceiptdetailrm_qtykonversi'], 2);
							$rmBasedOnBOMs[] = $resultRMOut['rm_kd'];
						}
					}
				?>
					<tr>
						<td><?php echo $r['rm_kode']; ?></td>
						<td><?php echo "{$r['partdetail_deskripsi']} {$r['partdetail_spesifikasi']}"; ?></td>
						<td class="dt-right"><?php echo round($sum_partdetail_qty, 2); ?></td>
						<td class="dt-right"><?php echo $qtyOut; ?></td>
						<td class="dt-right"><?php echo $r['current_stock'] ?></td>
						<td><?php echo $r['rmsatuan_nama']; ?></td>
						<td><?php
							$cekQtyBalance = $qtyOut - $sum_partdetail_qty;
							if ($cekQtyBalance > 0) {
								$status = 'lebih';
								$statusHtml = build_span('warning', $status);
							} elseif ($cekQtyBalance < 0) {
								$status = 'kurang';
								$statusHtml = build_span('warning', $status);
							} else {
								$status = 'OK';
								$statusHtml = build_span('success', $status);
							}
							echo $statusHtml; ?></td>
					</tr>
				<?php
				endforeach; ?>
			</tbody>
		</table>
		<h4> <u> Table RM not based on BoM </u> </h4>
		<table id="idTableFormNot" class="table table-bordered table-striped table-hover display clTblOut" style="width:100%; font-size: 90%;">
			<thead>
			<tr>
					<th style="width:7%; text-align: center;">RM Kode</th>
					<th style="width:20%; text-align: center;">RM Deskripsi</th>
					<th style="width:10%; text-align: center;">Qty Out</th>
					<th style="width:10%; text-align: center;">Current Stock</th>
					<th style="width:10%; text-align: center;">Satuan</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($resultRMOuts as $resultRMOut2): 
					if (in_array($resultRMOut2['rm_kd'], $rmBasedOnBOMs) ){
						continue;
					}
					?>
					<tr>
						<td><?php echo $resultRMOut2['rm_kode'];?></td>
						<td><?php echo $resultRMOut2['materialreceiptdetailrm_deskripsi'];?></td>
						<td><?php echo round($resultRMOut2['sum_materialreceiptdetailrm_qtykonversi'], 2);?></td>
						<td><?php echo $resultRMOut2['current_stock'];?></td>
						<td><?php echo $resultRMOut2['rmsatuan_nama'];?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<input type="hidden" name="7">
</div>


<script type="text/javascript">
	$('.clTblOut').DataTable({
		"paging": false,
		"ordering": true,
		"searching": true,
		"scrollY": "250px",
		"info": false,
		"language": {
			"lengthMenu": "Tampilkan _MENU_ data",
			"zeroRecords": "Maaf tidak ada data yang ditampilkan",
			"info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "(difilter dari _MAX_ total data)",
			"infoEmpty": "Tidak ada data yang ditampilkan",
			"search": "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing": "Sedang Memproses...",
			"paginate": {
				"first": '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last": '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next": '<span class="glyphicon glyphicon-forward"></span>',
				"previous": '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"order": [0, 'asc'],
	});
</script>