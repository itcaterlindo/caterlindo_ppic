<?php
class customPdf extends Tcpdf
{
    public $materialreceipt_no;
    public $bagian_nama;
    public $materialreq_nos;
    public $materialreceipt_tanggal;

    public function setsHeader($headerData)
    {
        $this->materialreceipt_no = $headerData['materialreceipt_no'];
        $this->bagian_nama = $headerData['bagian_nama'];
        $this->materialreq_nos = $headerData['materialreq_nos'];
        $this->materialreceipt_tanggal = $headerData['materialreceipt_tanggal'];
    }

    public function Header()
    {
        $header = '
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td width="30%" rowspan="4" style="text-align:left;"> <img src="assets/admin_assets/dist/img/logo_cat.png" width="135" height="35"> </td>
                <td width="35%" rowspan="4" style="text-align: center; "></td>
                <td width="35%" colspan="2" style="text-align:right; font-size: 100%; font-weight:bolt;"> DETAIL MATERIAL RECEIPT</td>
            </tr>
            <tr>
                <td style="text-align: left; font-size: 75%;">No Dokumen</td>
                <td style="text-align: left; font-size: 75%;">: CAT4-WRH-005</td>
            </tr>
            <tr>    
                <td style="text-align: left; font-size: 75%;">Tanggal Terbit</td>
                <td style="text-align: left; font-size: 75%;">: 25 February 2022</td>
            </tr>
            <tr>
                <td style="text-align: left; font-size: 75%;">Rev</td>
                <td style="text-align: left; font-size: 75%;">: 00</td>
            </tr>
        </table>';

        /** Keterangan */
        $keterangan =
            '<table cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 80%;">
            <tr>
                <td width="20%"> No </td>
                <td width="30%"> : ' . $this->materialreceipt_no . ' </td>
                <td width="20%"> Divisi </td>
                <td width="30%"> : ' . $this->bagian_nama . ' </td>
            </tr>
            <tr>
                <td width="20%"> Requisition No </td>
                <td width="30%"> : ' . $this->materialreq_nos . ' </td>
                <td width="20%"> Tanggal</td>
                <td width="30%"> : '.$this->materialreceipt_tanggal.'   </td>
            </tr>
            <tr>
                <td width="20%"> </td>
                <td width="30%"> </td>
                <td></td>    
                <td></td>    
            </tr>
        </table>';

        $this->writeHTML($header, true, false, false, false, '');
        $this->writeHTML($keterangan, true, false, false, false, '');
    }
}

$pdf = new customPdf('P', 'mm', 'A4', true, 'UTF-8', false);
$title = $master['materialreceipt_no'];
$pdf->SetTitle($title);
$pdf->SetAutoPageBreak(true, 10);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');
$pdf->setPrintFooter(true);

/** Margin */
$pdf->SetHeaderMargin(5);
$pdf->SetFooterMargin(7);
$pdf->SetMargins(8, 42, 8);

$dataHeader = array(
    'bagian_nama' => $master['bagian_nama'],
    'materialreceipt_no' => $master['materialreceipt_no'],
    'materialreq_nos' => $master['materialreq_nos'],
    'materialreceipt_tanggal' => $master['materialreceipt_tanggal']
);
$pdf->setsHeader($dataHeader);

$pdf->AddPage();


$pdf->writeHTML($konten, true, false, false, false, '');

// $footer = '<table cellspacing="0" cellpadding="0" border="1" style="font-size: 80%;" width="70%">';
// foreach ($logs as $log) {
//     $footer .= 
//     ' <tr>
//         <td>'.$log['wfstate_nama'].' by</td>
//         <td> :'.$log['nm_admin'].'</td>
//         <td>'.$log['materialreceiptlog_tglinput'].'</td>
//     </tr>';
// }                      
// $footer .= '</table>';

// $pdf->writeHTML($footer, true, false, false, false, '');


$txtOutput = $title . '.pdf';
$pdf->Output($txtOutput, 'I');
