<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));

?>

<div class="row">
<div class="form-group">
		<label for='idtxtrmgr_nosrj' class="col-md-2 control-label">Code SRJ</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrrmgr_nosrj"></div>
			<?php echo form_input(array('type' => 'text', 'id'=> 'idtxtrmgr_code_srj', 'name' => 'txtrmgr_code_srj', 'class'=> 'form-control', 'placeholder' => 'Code Surat Jalan')); ?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtrmgr_tgldatang' class="col-md-2 control-label">Tgl Datang</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrrmgr_tgldatang"></div>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				<?php echo form_input(array('type' => 'text', 'id'=> 'idtxtrmgr_tgldatang', 'name' => 'txtrmgr_tgldatang', 'class'=> 'form-control datetimepicker', 'placeholder' => 'Tgl Datang', 'value' => date('d-m-Y H:i:s'))); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtrmgr_nosrj' class="col-md-2 control-label">No & Tgl Surat Jalan</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrrmgr_nosrj"></div>
			<?php echo form_input(array('type' => 'text', 'id'=> 'idtxtrmgr_nosrj', 'name' => 'txtrmgr_nosrj', 'class'=> 'form-control', 'placeholder' => 'No Surat Jalan')); ?>
		</div>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrrmgr_tglsrj"></div>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				<?php echo form_input(array('type' => 'text', 'id'=> 'idtxtrmgr_tglsrj', 'name' => 'txtrmgr_tglsrj', 'class'=> 'form-control datepicker', 'placeholder' => 'Tgl Surat Jalan', 'value' => date('d-m-Y'))); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for='idtxtrmgr_nosrj' class="col-md-2 control-label">WareHouse</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrrmgr_nosrj"></div>
			<select class="form-control" name="txtkd_warehouse" id="idtxtkd_warehouse">
				<option value="MGD260719003">Gudang RM Caterlindo</option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtpo_kd' class="col-md-2 control-label">No PO</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrpo_kd"></div>
			<select id="idtxtpo_kd" class="form-control"> </select>
		</div>
	</div>
	
</div>

<div class="row">
	<div class="col-md-12">
		<div id="idtablepo"></div>
	</div>
</div>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-2 col-sm-offset-10 col-xs-12">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData()" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button type="reset" name="btnReset" onclick="resetFormMain()" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	$('[data-mask]').inputmask();

	// function get_code_srj(){
		$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/get_code_srj'; ?>',
				success: function(html) {
					$('#idtxtrmgr_code_srj').val(html);
					// $('#idtx	trmgr_code_srj').prop('readonly', true);
				}
			});
	// }
</script>
