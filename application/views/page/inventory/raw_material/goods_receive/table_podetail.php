<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<input type="hidden" name="txtpo_kd" value="<?php echo $po_kd; ?>">
<table id="idTablePO" class="table table-bordered table-striped " style="width:100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:7%; text-align:center;">Tgl Delivery</th>
		<th style="width:5%; text-align:center;">Kode Mateial</th>
		<th style="width:20%; text-align:center;">Deskripsi Material</th>
		<th style="width:7%; text-align:center;">Qty PO</th>
		<th style="width:7%; text-align:center;">Qty Konversi PO</th>
		<th style="width:7%; text-align:center;">Qty Receive</th>
		<th style="width:7%; text-align:center;">Satuan</th>
		<th style="width:10%; text-align:center;">Batch</th>
		<th style="width:10%; text-align:center;">Keterangan</th>
	</tr>
	</thead>
    <tbody>
        <?php 
        $no = 1;
		foreach ($podetails as $each) :
			if ($each['podetail_qty'] == 0){
				continue;
			}
		?>
        <tr>
            <td class="dt-center"><?php echo $no; ?></td>
            <td><?php echo empty($each['podetail_tgldelivery']) ? $each['podetail_tgldelivery'] : format_date($each['podetail_tgldelivery'], 'd-m-Y'); ?></td>
            <td><?php echo $each['rm_kode']; ?></td>
            <td><?php echo $each['podetail_deskripsi'].'/'.$each['podetail_spesifikasi']; ?></td>
            <td class="dt-right">
            <?php 
                echo (float) $each['podetail_qty'];?>
            </td>
            <td class="dt-right">
            <?php 
                echo (float) $each['podetail_qty'] * $each['podetail_konversi'];?>
            </td>
			
            <td class="dt-center">
                <?php 
                echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtpodetail_kd['.$each['podetail_kd'].']', 'value' => $each['podetail_kd'] ,'placeholder' => 'txtpodetail_kd'));
                echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtpodetail_konversi['.$each['podetail_kd'].']', 'value' => $each['podetail_konversi'] ,'placeholder' => 'txtpodetail_konversi'));
				echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtrm_kd['.$each['podetail_kd'].']', 'value' => $each['rm_kd'] ,'placeholder' => 'txtrm_kd'));
				echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtpodetail_harga['.$each['podetail_kd'].']', 'value' => $each['podetail_harga'] ,'placeholder' => 'txtpodetail_harga'));
				echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtpodetail_konversicurrency['.$each['podetail_kd'].']', 'value' => $each['podetail_konversicurrency'] ,'placeholder' => 'txtpodetail_koneversicurrency'));
				echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtkd_currency['.$each['podetail_kd'].']', 'value' => $each['kd_currency'] ,'placeholder' => 'txtpodetail_koneversicurrency'));
                ?>
                <input class= "form-control" type="text" name="txtrmgr_qty[<?php echo $each['podetail_kd'];?>]" placeholder="Qty">
            </td>
			<td class="dt-left">
            <?php 
                echo $each['satuan'];?>
            </td>
			<td>
                <input class= "form-control" type="text" name="txtrmgr_batch[<?php echo $each['podetail_kd'];?>]" placeholder="Batch">
			</td>
			<td>
                <input class= "form-control" type="text" name="txtrmgr_ket[<?php echo $each['podetail_kd'];?>]" placeholder="Keterangan">
			</td>
        </tr>
        <?php 
        $no ++;
        endforeach;?>
    </tbody>
</table>

<script type="text/javascript">
	render_dt('#idTablePO');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
		});
	}
</script>