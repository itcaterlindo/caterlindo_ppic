<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInputSendback';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));

?>

<style type="text/css">
	.select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>

<div class="row">
	
	<div class="form-group">
		<label for='idtxtdn_kd' class="col-md-2 control-label">No DN</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrdn_kd"></div>
			<select id="idtxtdn_kd" class="form-control"> </select>
		</div>
	</div>
	<hr>
	<div></div>
	
	
</div>

<div class="row">
	<div class="col-md-12">
		<div id="idtablewhdndetail"></div>
	</div>
</div>

<?php echo form_close(); ?>

