<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<input type="hidden" name="txtwhdeliveryorder_kd" value="<?php echo $whdeliveryorder_kd; ?>">
<table id="idTablePOsendback" class="table table-bordered table-striped " style="width:100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:7%; text-align:center;">Tgl Delivery</th>
		<th style="width:5%; text-align:center;">Kode Mateial</th>
		<th style="width:20%; text-align:center;">Deskripsi Material</th>
		<th style="width:7%; text-align:center;">Qty DO</th>
		<th style="width:7%; text-align:center;">Qty Konversi DO</th>
		<th style="width:7%; text-align:center;">Qty Receive</th>
		<th style="width:10%; text-align:center;">Keterangan</th>
	</tr>
	</thead>
    <tbody>
        <?php 
        $no = 1;
		foreach ($whdeliveryorderdetails as $each) :
		?>
        <tr>
            <td class="dt-center"><?php echo $no; ?></td>
            <td><?php echo empty($each['whdeliveryorder_tanggal']) ? $each['whdeliveryorder_tanggal'] : format_date($each['whdeliveryorder_tanggal'], 'd-m-Y'); ?></td>
            <td><?php echo $each['rm_kode']; ?></td>
            <td><?php echo $each['whdeliveryorderdetail_deskripsi'].'/'.$each['whdeliveryorderdetail_spesifikasi']; ?></td>
            <td class="dt-right">
            <?php 
                echo (float) $each['qty'];?>
            </td>
            <td class="dt-right">
            <?php 
                echo (float) $each['qty_konversi'];?>
            </td>
            <td class="dt-center">
                <?php 
                echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtwhdeliveryorderdetail_kd[]', 'value' => $each['whdeliveryorderdetail_kd'] ,'placeholder' => 'txtwhdeliveryorderdetail_kd'));
				echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtrm_kd['.$each['whdeliveryorderdetail_kd'].']', 'value' => $each['rm_kd'] ,'placeholder' => 'txtrm_kd'));
				echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtwhdeliveryorderdetail_qty['.$each['whdeliveryorderdetail_kd'].']', 'value' => $each['whdeliveryorderdetail_qty'] ,'placeholder' => 'txtwhdeliveryorderdetail_qty'));
				echo form_input(array( 'type'=> 'hidden', 'name'=> 'txtwhdeliveryorderdetail_qtykonversi['.$each['whdeliveryorderdetail_kd'].']', 'value' => $each['whdeliveryorderdetail_qtykonversi'] ,'placeholder' => 'txtwhdeliveryorderdetail_qtykonversi'));
                ?>
                <input class= "form-control" type="text" name="txtrmgr_qty[<?php echo $each['whdeliveryorderdetail_kd'];?>]" placeholder="Qty">
            </td>
			<td>
                <input class= "form-control" type="text" name="txtrmgr_ket[<?php echo $each['whdeliveryorderdetail_kd'];?>]" placeholder="Keterangan">
			</td>
        </tr>
        <?php 
        $no ++;
        endforeach;?>
    </tbody>
</table>

<script type="text/javascript">
	render_dt('#idTablePOsendback');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
		});
	}
</script>