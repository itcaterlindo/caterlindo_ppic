<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
    open_table('<?php echo $date; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table(date){
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_main'; ?>',
				data: {date:date},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					render_datetime('datepicker', 'DD-MM-YYYY');
					// moveTo('idMainContent');
				}
			});
		});
	}

	function showGoodsReceive(){
		var date = $('#idtxttgl').val();
		open_table(date);
	}

	// function push_to_sap(id){
	// 	event.preventDefault();
	// 	var conf = confirm('Apakah anda yakin ?');
	// 	if (conf) {
	// 		$.blockUI({ overlayCSS: { backgroundColor: '#C0C0C0' } }); 
	// 		$.ajax({
	// 			type: 'GET',
	// 			url: '<?php //echo base_url().$class_link.'/set_to_sap'; ?>',
	// 			data: {id: id},
	// 			success: function(data) {
	// 				var resp = JSON.parse(data);
	// 				sendTosap(resp.data);
	// 				// sendtosap(resp.data);
	// 				// if(resp.code == 200){
	// 				// 	notify (resp.status, resp.pesan, 'success');
	// 				// 	open_table();
	// 				// }else{
	// 				// 	notify (resp.status, resp.pesan, 'error');
	// 				// }
	// 			}
	// 		});
	// 	}
	// }
	// function sendTosap(data) { 
	// 	var data_to_sap = {
	// 						"U_IDU_WEBID": data.rmgr_kd,
	// 						"KdPurchaseOrder": data.po_kd,
	// 						"U_IDU_GRPO_INTNUM": data.rm_kd,
	// 						"DocDate": data.rmgr_tgldatang,
	// 						"Comments": data.rmgr_ket ? data.rmgr_ket : "",
	// 						"U_IDU_NoSJ": data.rmgr_nosrj,
	// 						"U_IDU_TglSJ": data.rmgr_tglsrj,
	// 						"U_IDU_WEBUSER": data.admin_kd,
	// 						"Lines_Detail_Item": [{
	// 							"U_IDU_WEBID": data.rmgr_kd,
	// 							"KdPurchaseOrderDtl": data.podetail_kd,
	// 							"Quantity": data.rmgr_qty,
	// 							"Konversi": data.podetail_konversi,
	// 							"KdWarehouse": 'MGD260719003',
	// 						}]
	// 					};
						

	// 				fetch("//url_api_sap() AddGRPO", {
	// 				method: 'POST', // or 'PUT'
	// 				headers: {
	// 					'Content-Type': 'application/json',
	// 				},
	// 				body: JSON.stringify(data_to_sap),
	// 				})
	// 				.then((response) => response.json())
	// 				.then((items) => {
	// 					console.log('Success:', items);
	// 					console.log(data_to_sap);
	// 					if(items.ErrorCode == 0){
	// 						open_table();
	// 						notify ('200', 'SAP OK!', 'success');
	// 						$.unblockUI();
	// 				// 	open_table();
	// 						// window.location.reload();
	// 					}else{
	// 						delRollback(data.rmgr_kd) 
	// 						open_table();
	// 						$.unblockUI();
	// 						notify (items.ErrorCode, items.Message, 'error');
	// 					}
	// 				})
	// 				.catch((error) => {
	// 					open_table();
	// 					delRollback(data.rmgr_kd);
	// 					$.unblockUI();
	// 					notify ('xxx', error, 'error');
	// 				});

					

	//  }

	 function delRollback(param) { 
	
		$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_rollback'; ?>',
				data: 'id='+param,
				success: function(data) {
					console.log(data);
					// var resp = JSON.parse(data);
					// if(resp.code == 200){
						notify ('xxx!', 'Save SAP Failed. Rollback Success.', 'Gagal!');
					// 	open_table();
					// }else if (resp.code == 400){
					// 	notify (resp.status, resp.pesan, 'error');
					// 	generateToken (resp.csrf);
					// }else{
					// 	notify ('Error', 'Error tidak Diketahui', 'error');
					// 	generateToken (resp.csrf);
					// }
				}
			});
	  }

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function hapus_data(id) {
		var conf = confirm('Apakah anda yakin menghapus item ini ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						open_table('<?php echo $date; ?>');
						var po_kd = $('#idtxtpo_kd').val();
						open_table_podetail (po_kd);
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				}
			});
		}
	}	

</script>