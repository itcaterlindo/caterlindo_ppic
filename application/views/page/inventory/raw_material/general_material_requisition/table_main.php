<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
	td.dt-blue {background-color: blue; color:white; }
	td.dt-green {background-color: green; color:white; }
	td.dt-red {background-color: red; color:white; }
	td.dt-purple {background-color: purple; color:white; }
</style>
<div class="box-body table-responsive no-padding">
	<table id="idTable" class="table table-bordered table-striped table-hover display table-responsive" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
			<th style="width:4%; text-align:center;" class="all">Opsi</th>
			<th style="width:10%; text-align:center;">Tanggal</th>
			<th style="width:10%; text-align:center;">Bagian</th>
			<th style="width:5%; text-align:center;">RM Kode</th>
			<th style="width:30%; text-align:center;">Deskripsi</th>
			<th style="width:10%; text-align:center;">Qty</th>
			<th style="width:10%; text-align:center;">Satuan</th>
			<th style="width:20%; text-align:center;">Remark</th>
			<th style="width:10%; text-align:center;">ModifiedAt</th>
		</tr>
		</thead>
	</table>
</div>

<script type="text/javascript">
	$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};
	var table = $('#idTable').DataTable({
		"dom": 'Bfrtip',
		"processing": true,
		"serverSide": true,
		"ordering" : true,
		"paging": false,
		"ajax": "<?php echo base_url().$class_link.'/table_data?date='.$date; ?>",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		"columnDefs": [
			{"data": null, "searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
			{"searchable": false, "orderable": false, "targets": 1},
		],
		"order":[9, 'desc'],
		"rowCallback": function (row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			var index = page * length + (iDisplayIndex + 1);
			$('td:eq(0)', row).html(index);
		},
		"buttons" : [{
			"extend" : "excel",
			"title" : "Report General Material Requisition "+$('#idtxttgl').val(),
			"footer" : false,
			"exportOptions": {
				"columns": [ 2,3,4,5,6,7,8 ]
			}
		}, {
                extend: 'collection',
                text: 'Cetak PDF/Print',
                action: function ( e, dt, node, config ) {
                    cetak_data();
                }
            }],
	});
</script>