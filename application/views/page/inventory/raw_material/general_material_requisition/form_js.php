<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form();
	open_table_box('<?php echo date('Y-m-d'); ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_main'; ?>',
			data: {sts:'add', id:''},
			success: function(html) {
			}
		});
	});
			
	$(document).off('change', '#idtxtrm_jenis').on('change', '#idtxtrm_jenis', function() {
		var paramOpsi = $(this).val();
		render_opsi(paramOpsi)
	});

	$(document).off('select2:select', '#idtxtrm_opsi').on('select2:select', '#idtxtrm_opsi', function(e) {
		let data = e.params.data;
		$('#idtxtrm_kd').val(data.rm_kd);
		$('#idtxtrm_deskripsi').text(data.text);
		$('#idtxtmaterialreqgeneral_satuan_kd').val(data.rmsatuan_kd);
		$('#idtxtrmsatuan_nama').val(data.rmsatuan_nama);
		$('#idtxtmaterialreqgeneral_qty_stok').val( Math.floor(data.stock_qty));
		if ($('#idtxtrm_jenis').val() == 'CUSTOM' ){
			$('#idtxtrmgr_kd').val(data.rmgr_kd);
			$('#idtxtpodetail_kd').val(data.podetail_kd);
			$('#idtxtmaterialreqgeneral_qty').val(data.qty);
		}
	});	

	function open_form(){
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/form_main'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					render_datetime('datepicker', 'YYYY-MM-DD');
					moveTo('idMainContent');
					render_rm();
					render_bagian();
				}
			});
		});
	}

	function render_opsi(paramOpsi) {
		if(paramOpsi == 'STD') {
			render_rm();
		}else{
			render_special();
		}
		$('.clMaterial').val('');
	}

	function render_rm() {
		$("#idtxtrm_opsi").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 2,
			ajax: { 
				url: '<?= base_url().'auto_complete' ?>/get_raw_material_stock',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramRM: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_bagian() {
		$("#idtxtbagian_kd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?= base_url().'auto_complete' ?>/get_bagian',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramBagian: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_special () {
		$("#idtxtrm_opsi").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 2,
			ajax: { 
				url: '<?= base_url().$class_link ?>/get_podetail_special',
				type: "get",
				dataType: 'json',
				delay: 300,
				data: function (params) {
					return {
						paramSpecial: params.term // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_datetime(valClass, valFormat){
		$('.'+valClass).datetimepicker({
            format: valFormat,
        });
	}

	function open_table_box(date) {
		$('#idTableBox<?php echo $master_var;?>').remove();
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_box'; ?>',
				data: {date:date},
				success: function(html) {
					$('#idMainContent').append(html);
				}
			});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function actSubmitData(form_id)
	{
		box_overlay('in');
		var form = document.getElementById(form_id);
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_insert" ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					let tgl_requisition = $('#idtxtmaterialreqgeneral_tanggal').val();
					notify (resp.status, resp.pesan, 'success');
					open_form();
					open_table_box(tgl_requisition);
					$('.errInput').html('');
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
					moveTo('idMainContent');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function submitData(form_id) {
		event.preventDefault();
		let po_kd = $('#idtxtpo_kd').val();
		let qty_stok = $('#idtxtmaterialreqgeneral_qty_stok').val();
		let qty_inputan = $('#idtxtmaterialreqgeneral_qty').val();
		// Cek ketika qty inputan melebihi qty stok dikasih alert
		if( Math.floor(qty_inputan) > Math.floor(qty_stok) ){
			if(confirm('Qty inputan melebihi current stok, apakah anda ingin melanjutkan?')){	
				actSubmitData(form_id);
			}
		}else{
			if(confirm('Apakah anda yakin ingin menyimpan data tersebut?')){	
				actSubmitData(form_id);
			}
		}
	}

	function submitDataSendback(form_id) {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById(form_id);

		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_insert_batch_sendback" ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					let whdeliveryorder_kd = $('#idtxtwhdeliveryorder_kd').val();
					open_table_whdodetail (whdeliveryorder_kd);
					open_table_box('<?php echo date('Y-m-d'); ?>');
					$('.errInput').html('');
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
					moveTo('idMainContent');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					resetFormMain();
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function resetFormMain() {
		// $('.tt-input').val('');
		// $('#idtxtpodetail_kd').val('').trigger('change');
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
		box_overlay('out');
    }

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}	

</script>