<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
$form_id = 'idFormInput';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>

<div class="row">
	<div class="form-group">
		<label for='idtxtmaterialreqgeneral_tanggal' class="col-md-2 control-label">Tgl Requisition</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrmaterialreqgeneral_tanggal"></div>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				<?php echo form_input(array('type' => 'text', 'id' => 'idtxtmaterialreqgeneral_tanggal', 'name' => 'txtmaterialreqgeneral_tanggal', 'class' => 'form-control datepicker', 'placeholder' => 'Tgl Surat Jalan', 'value' => date('Y-m-d'))); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtrm_jenis' class="col-md-2 control-label">Jenis Material</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrpo_kd"></div>
			<select id="idtxtrm_jenis" class="form-control"> 
				<option value="STD">Standart</option>
				<option value="CUSTOM">Custom</option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtrm_opsi' class="col-md-2 control-label">Raw Material</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrrm_kd"></div>
			<input type="hidden" id="idtxtrm_kd" name="txtrm_kd" class="clMaterial" placeholder="idtxtrm_kd">
			<input type="hidden" id="idtxtrmgr_kd" name="txtrmgr_kd" class="clMaterial" placeholder="idtxtrmgr_kd">
			<input type="hidden" id="idtxtpodetail_kd" name="txtpodetail_kd" class="clMaterial" placeholder="idtxtpodetail_kd">
			<input type="hidden" id="idtxtmaterialreqgeneral_satuan_kd" name="txtmaterialreqgeneral_satuan_kd" class="clMaterial" placeholder="idtxtmaterialreqgeneral_satuan_kd">
			<select id="idtxtrm_opsi" class="form-control"> </select>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtrm_deskripsi' class="col-md-2 control-label">Deskripsi Material</label>
		<div class="col-sm-6 col-xs-12">
			<div class="errInput" id="idErrrm_deskripsi"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control clMaterial', 'name'=> 'txtrm_deskripsi', 'id'=> 'idtxtrm_deskripsi', 'placeholder' =>'Deskripsi Material', 'readonly' => 'readonly', 'rows' => '2', 'value'=> isset($rm_deskripsi) ? $rm_deskripsi: null ));?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtbagian_kd' class="col-md-2 control-label">Qty Stock</label>
		<div class="col-sm-2 col-xs-12">
			<div class="errInput" id="idErrStokQty"></div>
			<?php echo form_input(array('type' => 'number', 'id' => 'idtxtmaterialreqgeneral_qty_stok', 'name' => 'txtmaterialreqgeneral_qty_stok', 'class' => 'form-control', 'readonly' => 'readonly')); ?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtmaterialreqgeneral_qty' class="col-md-2 control-label">Qty</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrmaterialreqgeneral_qty"></div>
			<?php echo form_input(array('type' => 'number', 'id' => 'idtxtmaterialreqgeneral_qty', 'name' => 'txtmaterialreqgeneral_qty', 'class' => 'form-control', 'placeholder' => 'Qty')); ?>
		</div>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrrmsatuan_nama"></div>
			<?php echo form_input(array('type' => 'text', 'id' => 'idtxtrmsatuan_nama', 'name' => 'txtrmsatuan_nama', 'class' => 'form-control clMaterial', 'readonly' => 'readonly', 'placeholder' => 'Satuan')); ?>
		</div>
	</div>
	

	<div class="form-group">
		<label for='idtxtbagian_kd' class="col-md-2 control-label">Bagian</label>
		<div class="col-sm-4 col-xs-12">
			<div class="errInput" id="idErrbagian_kd"></div>
			<select id="idtxtbagian_kd" name="txtbagian_kd" class="form-control"> </select>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtgudang' class="col-md-2 control-label">Gudang</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrGudang"></div>
			<?php if($this->session->userdata('tipe_admin') == 'Admin'){ ?>
				<select id="idtxtgudang_kd" name="txtgudang_kd" class="form-control"> 
					<?php foreach($gudang as $key => $val){ ?>
						<option value="<?= $val['kd_gudang'] ?>" <?= $val['kd_gudang'] == "MGD260719003" ? "selected" : "" ?> ><?= $val['nm_gudang'] ?></option>
					<?php } ?>
				</select>
			<?php }else{
				echo form_input(array('type' => 'text', 'id' => 'idtxtgudang_nm', 'name' => 'txtgudang_nm', 'class' => 'form-control', 'readonly' => 'readonly', 'placeholder' => 'Gudang', 'value' => $gudang_nm));
			 	echo form_input(array('type' => 'hidden', 'id' => 'idtxtgudang_kd', 'name' => 'txtgudang_kd', 'class' => 'form-control', 'readonly' => 'readonly', 'placeholder' => 'Gudang', 'value' => $gudang_user['kd_gudang'])); 
			} ?>
		</div>
	</div>

	<div class="form-group">
		<label for='idtxtmaterialreqgeneral_remark' class="col-md-2 control-label">Remark</label>
		<div class="col-sm-6 col-xs-12">
			<div class="errInput" id="idErrmaterialreqgeneral_remark"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtmaterialreqgeneral_remark', 'id'=> 'idtxtmaterialreqgeneral_remark', 'placeholder' =>'Deskripsi Material', 'rows' => '2', 'value'=> isset($materialreqgeneral_remark) ? $materialreqgeneral_remark: null ));?>
		</div>
	</div>

</div>

<div class="row">
	<div class="col-md-12">
		<div id="idtablepo"></div>
	</div>
</div>

<div class="col-md-12">
	<div class="form-group">
		<div class="col-sm-2 col-sm-offset-2 col-xs-12">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData('<?php echo $form_id?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button type="reset" name="btnReset" onclick="resetFormMain()" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	$('[data-mask]').inputmask();
</script>