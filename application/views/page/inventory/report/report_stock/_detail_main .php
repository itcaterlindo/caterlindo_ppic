<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
?>
<div class="row">
	<div class="col-xs-12">
		<a href="<?php echo base_url().'report/stock_report/report_today'; ?>" class="btn btn-success no-print" style="margin-left: 25px;">
			<i class="fa fa-file-excel-o"></i> Export Excel
		</a>
		<a href="<?php echo base_url().'report/stock_report/report_today_baru'; ?>" class="btn btn-success no-print" style="margin-left: 25px;">
			<i class="fa fa-file-excel-o"></i> Export Excel Baru
		</a>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-xs-12">
		<h2 class="page-header" style="margin-top: -20px;margin-bottom: 5px;text-align: center;">
			REPORT STOCK FINISH GOOD <?php echo date('d-M-Y'); ?>
		</h2>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<table class="table table-bordered table-striped table-hover" id="idTable" style="width:100%">
			<thead>
				<tr>
					<th>No</th>
					<th>Kode Barang</th>
					<th>Nama Stock</th>
					<th>No Batch</th>
					<th>Qty Data</th>
					<th>Qty Kartu Stock</th>
					<th>Selisih</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$no = 0;
				foreach ($report as $row) :
					$no++;
					$qty_data = !empty($row->fn_qty)?$row->fn_qty:'0';
					$qty_sisa = !empty($row->fn_sisa)?$row->fn_sisa:'0';
					$selisih = $qty_data - $qty_sisa;
					$color = $selisih > 0?'background-color:red;':'';
					echo '<tr>';
					echo '<td>'.$no.'</td>';
					echo '<td>'.$row->fc_kdstock.'</td>';
					echo '<td>'.$row->fv_namastock.'</td>';
					echo '<td style="text-align:center;">'.$row->fc_barcode.'</td>';
					echo '<td style="text-align:center;">'.$qty_data.'</td>';
					echo '<td style="text-align:center;">'.$qty_sisa.'</td>';
					echo '<td style="text-align:center;'.$color.'">'.$selisih.'</td>';
					echo '</tr>';
				endforeach;
				?>
			</tbody>
		</table>
	</div>
</div>