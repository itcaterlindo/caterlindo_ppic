<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
?>
<div class="row">
	<div class="col-xs-12">
		<a href="<?php echo base_url().$class_link.'/report_today_baru'; ?>" class="btn btn-sm btn-info no-print" style="margin-left: 25px;">
			<i class="fa fa-file-excel-o"></i> Export Excel
		</a>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-xs-12">
		<h2 class="page-header" style="margin-top: -20px;margin-bottom: 5px;text-align: center;">
			REPORT STOCK FINISH GOOD <?php echo strtoupper($nm_gudang) . ' '. date('d-M-Y'); ?>
		</h2>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<table class="table table-bordered table-striped table-hover" id="idTable" style="width:100%">
			<thead>
				<tr>
					<th>No</th>
					<th>Kode Barang</th>
					<th>Nama Stock</th>
					<th>Qty Data</th>
					<th>Qty Kartu Stock</th>
					<th>Selisih</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$no = 0;
				foreach ($report as $row) :
					$no++;
					$selisih = $row['qtyData'] - $row['qtyStock'];
					$color = $selisih != 0?'background-color:red;':'';
					echo '<tr>';
					echo '<td>'.$no.'</td>';
					echo '<td>'.$row['item_code'].'</td>';
					echo '<td>'.$row['nama_stock'].'</td>';
					echo '<td style="text-align:right;">'.$row['qtyData'].'</td>';
					echo '<td style="text-align:right;">'.$row['qtyStock'].'</td>';
					echo '<td style="text-align:right;'.$color.'">'.$selisih.'</td>';
					echo '</tr>';
				endforeach;
				?>
			</tbody>
		</table>
	</div>
</div>