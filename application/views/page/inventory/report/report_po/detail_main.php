<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
// echo json_encode($data_mso);die();
?>
<style>
	table, th, td {
		border: 1px solid black;
	}
	td { 
		padding: 8px;
	}
</style>
<div class="page-header">
	REPORT STOCK FINISH GOOD <?php echo date('d-M-Y'); ?>
</div>
<div id="contentHeader" class="row">
	<div class="col-xs-12 dv">
		<!-- <table id="idTable" class="table table-striped"> -->
		<table id="idTable">
			<thead>
				<tr>
					<th style="width: 1%; text-align: center;">No</th>
					<th style="width: 100px; text-align: center;">Kode Barang</th>
					<th style="width: 75px; text-align: center;">No Batch</th>
					<th style="width: 75px; text-align: center;">Qty Data</th>
					<?php
					foreach ($data_mso as $rowSO => $detailRow) :
						foreach ($master_mso as $eachMaster){
							if ($eachMaster['kd_msalesorder'] == $rowSO){
								$tgl_stuffing = '';
								if ($eachMaster['tipe_customer'] == 'Lokal'){
									if (!empty($tgl_stuffing)){
										$tgl_stuffing = format_date($eachMaster['tgl_stuffing'], 'd-M-Y');
									}else{
										if (!empty($eachMaster['tgl_kirim'])){
											$tgl_stuffing = format_date($eachMaster['tgl_kirim'], 'd-M-Y');
										}
									}
									$rowSO = $eachMaster['no_salesorder'].'<br>'.$tgl_stuffing;
								}else{
									$rowSO = $eachMaster['no_po'].'<br>'.$tgl_stuffing;
								}
							}
						}
						echo '<th style="width: 75px; text-align: center;">'.$rowSO.'</th>';
						echo "\r\n";
						echo '<th style="width: 1%; text-align: center;">Kode Barang</th>';
						echo "\r\n";
						echo '<th style="width: 1%; text-align: center;">Sisa</th>';
						echo "\r\n";
					endforeach;
					?>
					<th style="width: 1%; text-align: center;">Selisih</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$no = 1;
				$jml_data = 0;
				foreach ($report as $row) :
					// $no++;
					$qty_data = !empty($row['fg_qty'])?$row['fg_qty']:0;
					$selisih = $qty_data;

					 
					$tampil = '<tr>';
					$tampil .= "\r\n";
					$tampil .= '<td style="text-align: center;">'.$no.'</td>';
					$tampil .= "\r\n";
					$tampil .= '<td>'.$row['item_code'].'</td>';
					$tampil .= "\r\n";
					$tampil .= '<td style="text-align: center;">'.$row['item_barcode'].'</td>';
					$tampil .= "\r\n";
					$tampil .= '<td style="text-align: center;">'.$qty_data.'</td>';
					$tampil .= "\r\n";
					foreach ($data_mso as $each => $detailMso) :
						$tot_item = 0;
						foreach ($detailMso as $eee){
							if ($eee['barang_kd'] == $row['barang_kd']){
								$tot_item = $eee['item_qty'];
							}
						}
						$selisih = $selisih - $tot_item;
						$color = $selisih < 0?'background-color:red;':'background-color: green;';
						$tampil .= '<td style="text-align: center;"">'.$tot_item.'</td>';
						$tampil .= "\r\n";
						$tampil .= '<td style="text-align: center;"">'.$row['item_barcode'].'</td>';
						$tampil .= "\r\n";
						$tampil .= '<td style="text-align:center;'.$color.' color:white;">'.$selisih.'</td>';
						$tampil .= "\r\n";
					endforeach;
					$color = $selisih < 0?'background-color:red;':'background-color: green;';
					$sisa = $selisih;
					$tampil .= '<td style="text-align:center;'.$color.' color:white;">'.$sisa.'</td>';
					$tampil .= "\r\n";
					$tampil .= '</tr>';
					$tampil .= "\r\n";

					if ($sisa != $qty_data){
						echo $tampil;
						$no++;
					}
				endforeach;
				?>
			</tbody>
			
		</table>
	</div>
</div>