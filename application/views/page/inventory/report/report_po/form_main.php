<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'DataReportPo';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>
<h4 class="title"><u>Pilih No PO yang ingin ditampilkan</u> | Jumlah Data : <?php echo count($data_mso); ?></h4>
<div class="col-xs-3">
	<?php
	$no = 0;
	$divider = round(count($data_mso) / 4);
	foreach ($data_mso as $mso) :
		if (empty($mso->tgl_kirim) || $mso->tgl_kirim == '0000-00-00') {
			continue;
		}
		$no++;
		if ($mso->tipe_customer == 'Lokal') :
			$no_po = $mso->no_salesorder;
		elseif ($mso->tipe_customer == 'Ekspor') :
			$no_po = $mso->no_po;
		endif;
		$tgl_view = !empty($mso->tgl_stuffing)?$mso->tgl_stuffing:$mso->tgl_kirim;
		$tgl_view = !empty($tgl_view) && $tgl_view != '0000-00-00'?format_date($tgl_view, 'd-M-y'):'-';
		$no_po .= $tgl_view != '-'?' - '.$tgl_view:'';
		$tutup = $no == $divider?'</div><div class="col-xs-3">':'';
		$no = $no == $divider?0:$no;
		?>
		<div class="form-group">
			<label for="idChk<?php echo $mso->kd_msalesorder; ?>">
				<?php echo form_checkbox(array('name' => 'chkMenu[]', 'id' => 'idChk'.$mso->kd_msalesorder, 'class' => 'icheck chk-item', 'value' => $mso->kd_msalesorder, 'checked' => true)); echo $no_po; ?>
			</label>
		</div>
		<?php
		echo $tutup;
	endforeach;
	?>
</div>
<div class="row">
	<div class="col-xs-12">
		<hr>
		<div class="form-group">
			<div class="col-sm-1 col-sm-offset-1 col-xs-12" style="margin-right: 15px;">
				<button type="button" name="btnUnchkAll" class="btn btn-default btn-flat" onclick="uncheck_all();">
					<i class="fa fa-times-circle-o"></i> Uncheck All
				</button>
			</div>
			<div class="col-sm-1 col-xs-12">
				<button type="button" name="btnChkAll" class="btn btn-success btn-flat" onclick="check_all();">
					<i class="fa fa-check"></i> Check All
				</button>
			</div>
			<div class="col-sm-1 col-xs-12">
				<button type="submit" name="btnSubmit" class="btn btn-primary btn-flat">
					<i class="fa fa-forward"></i> Submit
				</button>
			</div>
		</div>
	</div>
</div>
<?php echo form_close(); ?>