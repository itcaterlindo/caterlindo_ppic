<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_detail('<?php echo $msalesorder_kd; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	// function moveTo(div_id) {
	// 	$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	// }

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function open_detail(msalesorder_kd) {
		$('#<?php echo $box_overlay_id; ?>').show();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/open_detail'; ?>',
				data: {msalesorder_kd:msalesorder_kd},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					render_dt('#idTable');
					$('#<?php echo $box_overlay_id; ?>').fadeOut();
				}
			});
		});
	}

	// function render_dt(table_elem) {
	// 	$(table_elem).DataTable({
	// 		"scrollY": 500,
	// 		"scrollX": true,
	// 		"fixedColumns": true,
	// 		"paging": false,
	// 		"fixedColumns": {
	// 			"leftColumns": 4,
	// 			"rightColumns": 1,
	// 		},
	// 	});
	// 	moveTo('contentHeader');
	// }
	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"scrollX": true,
			"scrollY": "500px",
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"scrollCollapse": true,
			"fixedColumns": {
				"leftColumns": 4,
				"rightColumns": 1
			},
			"buttons" : [{
				"extend" : "excel",
				"title" : "Rekap Stok Finishgood PO <?php echo date('d-M-Y'); ?>",
				"exportOptions": {
                    "columns": [ ':visible' ]
                }
			}]
		});
		moveTo('contentHeader');
	}

	// function fnExcelReport(id_table) {
	// 	var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
	// 	var textRange;
	// 	var j = 0;
	// 	tab = document.getElementById(id_table); // id of table

	// 	for (j = 0 ; j < tab.rows.length ; j++)  {
	// 		tab_text = tab_text+tab.rows[j].innerHTML+"</tr>";
	// 		//tab_text=tab_text+"</tr>";
	// 	}

	// 	tab_text = tab_text+"</table>";
	// 	tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
	// 	tab_text = tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
	// 	tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // removes input params

	// 	var ua = window.navigator.userAgent;
	// 	var msie = ua.indexOf("MSIE"); 

	// 	// If Internet Explorer
	// 	if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
	// 		txtArea1.document.open("txt/html","replace");
	// 		txtArea1.document.write(tab_text);
	// 		txtArea1.document.close();
	// 		txtArea1.focus(); 
	// 		sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
	// 	} else {
	// 		//other browser not tested on IE 11
	// 		sa = window.open('Content-Type:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
	// 	}

	// 	return (sa);
	// }
</script>