<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form();
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('submit', '#idForm<?php echo $master_var; ?>').on('submit', '#idForm<?php echo $master_var; ?>', function(e) {
		e.preventDefault();
		submit_form(this);
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function open_form() {
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/open_form'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					moveTo('idMainContent');
					/* --START ICHECK PROPERTIES-- */
					$('input[type="checkbox"].icheck').iCheck({
						checkboxClass: 'icheckbox_square-blue'
					});
					/* --END ICHECK PROPERTIES-- */
				}
			});
		});
	}

	function check_all() {
		$('.chk-item').iCheck('check');
	}

	function uncheck_all() {
		$('.chk-item').iCheck('uncheck');
	}

	function submit_form(form_id) {
		$('#<?php echo $box_overlay_id; ?>').show();
		$('#<?php echo $box_alert_id; ?>').html('');
		<?php
		foreach ($form_errs as $form_err) {
			echo '$(\'#'.$form_err.'\').html(\'\');';
		}
		?>
		$.ajax({
			url: "<?php echo base_url().$class_link.'/send_data'; ?>",
			type: "POST",
			data:  new FormData(form_id),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				get_detail(resp.msalesorder_kd);
				$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(resp.csrf);
				$('#<?php echo $box_overlay_id; ?>').hide();
			}
		});
	}

	function get_detail(kd_mso) {
		$('#idDetailBox<?php echo $master_var; ?>').remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/get_detail'; ?>',
			data: 'kd_mso='+kd_mso,
			success: function(html) {
				$('#idMainContent').append(html);
			}
		});
	}
</script>