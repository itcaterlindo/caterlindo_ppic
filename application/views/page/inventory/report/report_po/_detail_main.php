<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
?>

<!-- <div class="row">
	<div class="col-xs-12">
		<a href="javascript:void(0);" onclick="fnExcelReport('idTable');" class="btn btn-success no-print" style="margin-left: 25px;">
			<i class="fa fa-file-excel-o"></i> Export Excel
		</a>
	</div>
</div>
<hr> -->
<div class="page-header">
	REPORT STOCK FINISH GOOD <?php echo date('d-M-Y'); ?>
</div>
<div id="contentHeader" class="row">
	<div class="col-xs-12">
		<table id="idTable" class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width: 1%; text-align: center;">No</th>
					<th style="width: 100px; text-align: center;">Kode Barang</th>
					<th style="width: 75px; text-align: center;">No Batch</th>
					<th style="width: 75px; text-align: center;">Qty Data</th>
					<?php
					foreach ($data_mso as $row) :
						$tgl_view = !empty($row->tgl_stuffing)?$row->tgl_stuffing:$row->tgl_kirim;
						$tgl_view = !empty($tgl_view) && $tgl_view != '0000-00-00'?format_date($tgl_view, 'd-m-y'):'-';
						if ($row->tipe_customer == 'Lokal') :
							echo '<th style="width: 75px; text-align: center;">'.$row->no_salesorder.'<br>'.$tgl_view.'</th>';
							echo "\r\n";
						elseif ($row->tipe_customer == 'Ekspor') :
							echo '<th style="width: 75px; text-align: center;">'.$row->no_po.'<br>'.$tgl_view.'</th>';
							echo "\r\n";
						endif;
						echo '<th style="width: 1%; text-align: center;">Sisa</th>';
						echo "\r\n";
					endforeach;
					?>
					<th style="width: 1%; text-align: center;">Selisih</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$no = 0;
				$jml_data = 0;
				foreach ($item_parent as $item) :
					$parent_item_qty[$item->msalesorder_kd][$item->item_barcode][] = $item->item_qty;
				endforeach;
				foreach ($item_child as $item) :
					$child_item_qty[$item->msalesorder_kd][$item->item_barcode][] = $item->item_qty;
				endforeach;
				foreach ($report as $row) :
					$no++;
					$qty_data = !empty($row->fn_qty)?$row->fn_qty:'0';
					$qty_sisa = !empty($row->fn_sisa)?$row->fn_sisa:'0';
					$selisih = $qty_data;
					echo '<tr>';
					echo "\r\n";
					echo '<td style="text-align: center;">'.$no.'</td>';
					echo "\r\n";
					echo '<td>'.$row->fc_kdstock.'</td>';
					echo "\r\n";
					echo '<td style="text-align: center;">'.$row->fc_barcode.'</td>';
					echo "\r\n";
					echo '<td style="text-align: center;">'.$qty_data.'</td>';
					echo "\r\n";
					foreach ($data_mso as $mso) :
						if (isset($parent_item_qty[$mso->kd_msalesorder][$row->fc_barcode])) :
							$tot_item_parent = array_sum($parent_item_qty[$mso->kd_msalesorder][$row->fc_barcode]);
						else :
							$tot_item_parent = 0;
						endif;
						if (isset($child_item_qty[$mso->kd_msalesorder][$row->fc_barcode])) :
							$tot_item_child = array_sum($child_item_qty[$mso->kd_msalesorder][$row->fc_barcode]);
						else :
							$tot_item_child = 0;
						endif;
						$tot_item = $tot_item_parent + $tot_item_child;
						$selisih = $selisih - $tot_item;
						$color = $selisih < 0?'background-color:red;':'background-color: green;';
						echo '<td style="text-align: center;"">'.$tot_item.'</td>';
						echo "\r\n";
						echo '<td style="text-align:center;'.$color.' color:white;">'.$selisih.'</td>';
						echo "\r\n";
					endforeach;
					$color = $selisih < 0?'background-color:red;':'background-color: green;';
					echo '<td style="text-align:center;'.$color.' color:white;">'.$selisih.'</td>';
					echo "\r\n";
					echo '</tr>';
					echo "\r\n";
				endforeach;
				?>
			</tbody>
		</table>
	</div>
</div>