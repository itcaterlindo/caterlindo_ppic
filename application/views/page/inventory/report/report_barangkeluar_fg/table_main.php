<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
	td.dt-blue {background-color: blue; color:white; }
	td.dt-green {background-color: green; color:white; }
	td.dt-red {background-color: red; color:white; }
	td.dt-purple {background-color: purple; color:white; }
</style>
<label for="">#No Sales Order : <?php echo $identitasSO['no_salesorder'].'/'.$identitasSO['nm_customer']; ?></label><br>
<hr>

<div class="row">
	<div class="col-md-12">
		<table id="idTable" class="table table-bordered table-striped table-hover display" style="width:100%; font-size:85%;">
			<thead>
				<tr>
					<th style="width:1%; text-align:center;" class="all">No.</th>
					<th style="width:10%; text-align:center;">Item Code</th>
					<th style="width:10%; text-align:center;">Batch</th>
					<th style="width:15%; text-align:center;">Barcode</th>
					<th style="width:4%; text-align:center;">Awal</th>
					<th style="width:4%; text-align:center;">Keluar</th>
					<th style="width:4%; text-align:center;">Sisa</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$no = 1;
				foreach ($resultData as $each):
					$sisa = $each['fgout_qty_awal'] - $each['fgout_qty'];
					?>
					<tr>
						<td><?php echo $no?></td>
						<td><?php echo $each['item_code']?></td>
						<td><?php echo $each['fgbarcode_batch']?></td>
						<td><?php echo $each['fgbarcode_barcode']?></td>
						<td style="text-align:right;"><?php echo $each['fgout_qty_awal']?></td>
						<td style="text-align:right;"><?php echo $each['fgout_qty']?></td>
						<td style="text-align:right;"><?php echo $sisa;?></td>
					</tr>
					<?php
					$no ++;
				endforeach;
				?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
render_dt('#idTable', '1350px');
function render_dt(table_elem, scrollY) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"scrollX": true,
			"scrollY": scrollY,
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"title" : "Rekap SO & Barang Keluar <?php echo $identitasSO['no_salesorder'].'_'.$identitasSO['nm_customer']; ?>",
				"exportOptions": {
					"columns": [ 0,1,2,3,4,5,6 ]
				}
			}],
		});
	}
</script>