<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table('<?php echo $kd_msalesorder; ?>');
	open_detail_salesorder('<?php echo $kd_msalesorder; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table(kd_msalesorder, sts) {
		$('#<?php echo $box_content_id; ?>').slideDown(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_main'; ?>',
				data: {kd_msalesorder : kd_msalesorder, sts : sts},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
				}
			});
		});
	}

	function open_detail_salesorder(kd_msalesorder) {
		$('#<?php echo $box_loader_id_2; ?>').fadeIn();
		$('#idmainSalesorder').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/detail_salesorder_main'; ?>',
				data: {kd_msalesorder : kd_msalesorder},
				success: function(html) {
					$('#idmainSalesorder').slideDown().html(html);
					$('#<?php echo $box_loader_id_2; ?>').fadeOut();
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

</script>