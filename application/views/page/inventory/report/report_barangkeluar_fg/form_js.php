<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	$('#<?= $box_loader_id?>').hide();
	open_form_main();
	
	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_form_main() {
		$('#idFormBarangKeluarFG').slideUp().remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link?>/form_main',
			success:function(html){
				$('#<?php echo $box_content_id; ?>').slideDown().append(html);
				$('#idTxtBarcode').focus();
				render_select2('select2');
			}
		});
		moveTo('idMainContent');
	}

	function render_select2(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			allowClear: true,
		});
		$('.'+valClass).val('').trigger('change');
	}

	function open_table_box(kd_msalesorder) {
		$('#idRowBarangKeluar').slideDown().remove();
		var url = '<?php echo base_url().$class_link?>/table_box';
		$.ajax({
			type: 'GET',
			data: {kd_msalesorder : kd_msalesorder},
			url: url,
			success:function(html){
				$('#idMainContent').append(html);
			}
		});
	}

	function submitHistorySO(){
		event.preventDefault();
		var msalesorder_kd = $('#idtxtmsalesorder_kd').val();
		open_table_box(msalesorder_kd);
	}
	
	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

</script>