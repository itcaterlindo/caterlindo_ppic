<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	$('#<?= $box_loader_id?>').hide();
	open_form_main();
	
	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_form_main() {
		$('#idFormBarangKeluarFG').slideUp().remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link?>/form_history_main',
			success:function(html){
				$('#<?php echo $box_content_id; ?>').slideDown().append(html);
				render_select2('select2');
			}
		});
		moveTo('idMainContent');
	}

	function render_select2(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			allowClear: true,
		});
		$('.'+valClass).val('').trigger('change');
	}

    function submitHistorySO(){
		event.preventDefault();
		var msalesorder_kd = $('#idtxtmsalesorder_kd').val();
		open_table_box(msalesorder_kd);
	}

	function open_table_box(kd_msalesorder) {
		$('#idRowBarangKeluar').slideDown().remove();
		var url = '<?php echo base_url().$class_link?>/table_box';
		$.ajax({
			type: 'GET',
			data: {kd_msalesorder : kd_msalesorder},
			url: url,
			success:function(html){
				$('#idMainContent').append(html);
			}
		});
	}

	function cariHistorySO(){
		event.preventDefault();
		open_form_history();
	}
	
	function submitHistorySO(){
		event.preventDefault();
		var msalesorder_kd = $('#idtxtmsalesorder_kd').val();
		open_table_box(msalesorder_kd);
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
			// delay: 2500,
            styling: 'bootstrap3'
        });	
		box_overlay('out');
		audio_notif(type);
    }
	
	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

	function audio_notif(type){
		if (type == 'success'){
			var audio = new Audio("<?php echo base_url().'/assets/admin_assets/dist/sound/success.ogg'?>");
		}else{
			var audio = new Audio("<?php echo base_url().'/assets/admin_assets/dist/sound/fail.ogg'?>");
		}
		
		audio.play();
	}
</script>