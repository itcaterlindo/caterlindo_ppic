<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'HistoryOUT';
$form_id = 'idForm'.$master_var;
?>

<?php 
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal '));
?>
<div class="row">

    <div class="form-group">
        <label for="idtxtProductCode" class="col-md-2 control-label">No Sales Order</label>
        <div class="col-md-6 col-xs-12">
            <div id="idErrNo_salesorder"></div>
            <?php echo form_dropdown('txtmsalesorder_kd', $opsiSO, isset($SOSelected)? $SOSelected : '', array('class'=> 'form-control select2', 'id'=> 'idtxtmsalesorder_kd'));?>
        </div>
        <div class="col-md-1 col-xs-12"> 
            <button class="btn btn-success" id="idbtnCariSO" onclick="submitHistorySO()" ><i class="fa fa-search"></i> Cari</button>
        </div>
    </div>

</div>
<?php echo form_close(); ?>

