<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<div class="row invoice-info">
	<center>
		<h3 type="text" style="font-weight: bold;" id="idJuduLaporan"> Laporan Kartu Stok <?php echo $nm_gudang; ?></h3>
		<h4> Periode <?php echo $startdate.' s/d '.$enddate; ?> </h4>
	</center>
</div>
<div class="row invoice-info">
		<table>
			<tr>
				<td width="100">Product Code</td>
				<td width="10">:</td>
				<td><?php echo $barang->item_code?></td>
			</tr>
			<tr>
				<td width="100">Barcode</td>
				<td width="10">:</td>
				<td><?php echo $barang->item_barcode?></td>
			</tr>
			<tr>
				<td width="100">Deskripsi</td>
				<td width="10">:</td>
				<td><?php echo $barang->deskripsi_barang?></td>
			</tr>
			<tr>
				<td width="100">Dimensi</td>
				<td width="10">:</td>
				<td><?php echo $barang->dimensi_barang?></td>
			</tr>
		</table>
	<!-- </div> -->
</div>
<hr>
<div class="row invoice-info">
 <table style="column-width: 10px;width: 100%;" border="1">
	<thead>
		<tr>
			<th style="width:1%; text-align:center;">No.</th>
			<th style="width:4%; text-align:center;">Tgl Transaksi</th>
			<th style="width:4%; text-align:center;">Prod Code</th>
			<th style="width:10%; text-align:center;">Uraian</th>
			<th style="width:5%; text-align:center;">Barcode</th>
			<th style="width:3%; text-align:center;" class="dt-purple">Awal</th>
			<th style="width:3%; text-align:center;">Masuk</th>
			<th style="width:3%; text-align:center;">Keluar</th>
			<th style="width:3%; text-align:center;">Sisa</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		if ($rowData){
			foreach ($rowData as $eachData):
			?>
				<tr>
					<td style="text-align:center;"><?php echo $no;?></td>
					<td style="text-align:left;"><?php echo format_date($eachData['tgl_trans'], 'd-m-Y H:i:s');?></td>
					<td style="text-align:left;"><?php echo $eachData['item_code'];?></td>
					<td style="text-align:left;"><?php echo $eachData['uraian'];?></td>
					<td style="text-align:left;"><?php echo $eachData['barcode'];?></td>
					<td style="text-align:right;" class="dt-purple"><?php echo $eachData['awal'];?></td>
					<td style="text-align:right;" class="dt-green"><?php echo $eachData['masuk'];?></td>
					<td style="text-align:right;" class="dt-red"><?php echo $eachData['keluar'];?></td>
					<td style="text-align:right;" class="dt-blue"><?php echo $eachData['sisa'];?></td>
				</tr>
			<?php
			$no++;
			endforeach;
		}
		?>
	</tbody>
</table>
<hr> 
</div>