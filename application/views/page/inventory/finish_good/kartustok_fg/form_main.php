<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
$master_var = 'FilterKartuStok';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtbarang_kd', 'name'=> 'txtbarang_kd' ));
?>
<div class="row">
	<div class="col-sm-2">
		<div class="form-group">
			<label for="idtxtStartdate">Start Date</label>
			<div id="idErrStartdate"></div>
			<div class="input-group date">
				<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
				</div>
				<input type="text" class="form-control datepicker" id="idtxtStartdate" name="txtStartdate" placeholder="Start Date" value="<?php echo date('d-m-Y')?>">
			</div>
		</div>
	</div>
	<div class="col-sm-2">
		<div class="form-group">
			<label for="idtxtEnddate">End Date</label>
			<div id="idErrEnddate"></div>
			<div class="input-group date">
				<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
				</div>
				<input type="text" class="form-control datepicker" id="idtxtEnddate" name="txtEnddate" placeholder="End Date" value="<?php echo date('d-m-Y')?>">
			</div>
		</div>
	</div>
	<div class="col-sm-2">
		<div class="form-group">
			<label for="idtxtWarehouse">Lokasi Warehouse</label>
			<div id="idErrWarehouse"></div>
			<?php 
			echo form_dropdown('txtWarehouse', $opsiWarehouse, null, array('class'=>'form-control', 'id' => 'idtxtWarehouse'));
			?>
		</div>
	</div>
	<div class="col-sm-2">
		<div class="form-group">
			<label for="idtxtJnsTransaksi">Jenis Transaksi</label>
			<div id="idErrJnsTransaksi"></div>
			<?php
			echo form_dropdown('txtJnsTransaksi', $opsiTransaksi, null, array('class'=>'form-control', 'id'=>"idtxtJnsTransaksi"));
			?>
		</div>
	</div>
	<div class="col-sm-2">
		<div class="form-group">
			<label for="idtxtItemCode">Prod Code</label>
			<div id="idErrItemCode"></div>
			<input type="text" class="form-control" id="idtxtItemCode" name="txtItemCode" placeholder="Prod Code" >
		</div>
	</div>
	<div class="col-sm-2">
		<div class="form-group">
			<br>
			<button class="btn btn-success" id="idBtnSubmit"> <i class="fa fa-search"></i> Proses</button>
		</div>
	</div>
</div>
<hr>	
<?php echo form_close(); ?>

<script type="text/javascript">

	/* --start product typeahead.js-- */
	var Product = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			url: '<?php echo base_url('auto_complete/get_barang'); ?>',
			remote: {
				url: '<?php echo base_url('auto_complete/get_barang'); ?>',
				prepare: function (query, settings) {
					settings.type = 'GET';
					settings.contentType = 'application/json; charset=UTF-8';
					settings.data = 'item_code='+query;

					return settings;
				},
				wildcard: '%QUERY'
			}
		});

		$('#idtxtItemCode').typeahead(null, {
			limit: 50,
			minLength: 1,
			name: 'product_search',
			display: 'item_code',
			valueKey: 'get_product',
			source: Product.ttAdapter()
		});

		$('#idtxtItemCode').bind('typeahead:select', function(obj, selected) {
			$('#idtxtbarang_kd').val(selected.kd_barang);
			$('#idBtnSubmit').focus();
		});
		/* --end of product typeahead.js-- */


	$('#idBtnSubmit').click(function(e) {
		e.preventDefault();
		var form = document.getElementById('<?php echo $form_id; ?>');
		var url = '<?php echo base_url().$class_link;?>/table_main';

		$.ajax({
			url: url,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if(resp.code == 200){
					open_table(resp.data);
					generateToken (resp.csrf);
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});

	});

	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}
	
</script>