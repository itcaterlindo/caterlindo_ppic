<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table(url) {
		$('#<?php echo $box_loader_id; ?>').fadeIn();
		$('#<?php echo $box_content_id; ?>').slideUp();
			$.ajax({
				type: 'GET',
				url: url,
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
					$('#<?php echo $box_loader_id; ?>').fadeOut();
				}
			});
	}

	function remove_table() {
		$('#<?php echo $box_content_id; ?>').slideUp();
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function edit_lokasi(id){
		$('#idFormBoxLokasiFG').remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_lokasi_box'; ?>',
			data: { id: id, },
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function cetak_kartu_stok(){
		var startdate = $('#idtxtStartdate').val();
		var enddate = $('#idtxtEnddate').val();
		var warehouse = $('#idtxtWarehouse').val();
		var jns_transaksi = $('#idtxtJnsTransaksi').val();
		var barang_kd = $('#idtxtbarang_kd').val();
		
		window.open('<?php echo base_url().$class_link;?>/cetak_kartu_stok?startdate='+startdate+'&enddate='+enddate+'&warehouse='+warehouse+'&jns_transaksi='+jns_transaksi+'&barang_kd='+barang_kd);
	}

	function xls_kartu_stok(){
		var startdate = $('#idtxtStartdate').val();
		var enddate = $('#idtxtEnddate').val();
		var warehouse = $('#idtxtWarehouse').val();
		var jns_transaksi = $('#idtxtJnsTransaksi').val();
		var barang_kd = $('#idtxtbarang_kd').val();

		window.location.assign('<?php echo base_url().$class_link;?>/xls_kartu_stok?startdate='+startdate+'&enddate='+enddate+'&warehouse='+warehouse+'&jns_transaksi='+jns_transaksi+'&barang_kd='+barang_kd);
	}

</script>