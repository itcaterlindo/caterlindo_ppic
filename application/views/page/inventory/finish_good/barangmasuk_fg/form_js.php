<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	$('#<?= $box_loader_id?>').hide();
	open_form_main('<?php echo isset($kd_gudang) ? $kd_gudang: null?>');
	open_table_box();

	$(document).off('keyup', '#idTxtBarcode').on('keyup', '#idTxtBarcode', function(e) {
		let code = e.keyCode || e.which;
		let val = $(this).val();
		if (code == 13) {
			if(val.length == 19 ){
				let metode = $('#idtxtMetodeInput').val();
				if (metode == 'MANUAL') {
					$('#idtxtQtyin').focus();
				}else{
					submitData();
				}
			}else{
				notify('Gagal', 'Barcode tidak sesuai', 'error');
				$(this).val('');
			}
			e.preventDefault();
			return false;
		}
	});

	$(document).off('keyup', '#idtxtQtyin').on('keyup', '#idtxtQtyin', function(e) {
		var code = e.keyCode || e.which;
		if (code == 13){
			submitData();
		}
	})
	
	function get_warehouse_grid(){
		var optGdg = $('#idtxtGudangGrid');
		$('#idtxtGudangGrid option').remove();
		optGdg.append($('<option></option>').val('').html('--Pilih Warehouse--'));
		$.ajax({
			type: "GET",
			url: "<?php echo base_url().'finish_good/barangrak_fg'; ?>/get_gudang",
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optGdg.append($('<option></option>').val(text.kd_gudang).html(text.nm_gudang));
					});
				}
			} 	        
		});
	}

	function get_rak_grid(valGdg){
		var optRak = $('#idtxtRakGrid');
		$('#idtxtRakGrid option').remove();
		optRak.append($('<option></option>').val('').html('--Pilih Rak--'));
		$.ajax({
			url: "<?php echo base_url().'finish_good/barangrak_fg'; ?>/get_rak",
			type: "GET",
			data: 'gudang_kd='+valGdg,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optRak.append($('<option></option>').val(text.kd_rak).html(text.nm_rak));
					});
				}
			} 	        
		});
	}

	function get_ruang_grid(valRak){
		var optRuang = $('#idtxtRuangGrid');
		$('#idtxtRuangGrid option').remove();
		optRuang.append($('<option></option>').val('').html('--Pilih Ruang--'));
		$.ajax({
			url: "<?php echo base_url().'finish_good/barangrak_fg'; ?>/get_ruang",
			type: "GET",
			data: 'rak_kd='+valRak,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optRuang.append($('<option></option>').val(text.kd_rak_ruang).html(text.nm_rak_ruang));
					});
				}
			} 	        
		});
	}

	function get_ruang_kolom_grid(valRuang){
		var optRuangKolom = $('#idtxtRuangKolomGrid');
		$('#idtxtRuangKolomGrid option').remove();
		optRuangKolom.append($('<option></option>').val('').html('--Pilih Ruang Kolom--'));
		$.ajax({
			url: "<?php echo base_url().'finish_good/barangrak_fg'; ?>/get_ruang_kolom",
			type: "GET",
			data: 'kd_rak_ruang='+valRuang,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optRuangKolom.append($('<option></option>').val(text.rakruangkolom_kd).html(text.rakruangkolom_nama));
					});
				}
			} 	        
		});
	}
	
	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_form_main(kd_gudang) {
		$('#<?php echo $box_content_id; ?>').load('<?php echo base_url().$class_link?>/form_main?kd_gudang='+kd_gudang);
		moveTo('idMainContent');
	}

	function open_table_box() {
		var url = '<?php echo base_url().$class_link?>/table_box';
		$.ajax({
			type: 'GET',
			url: url,
			success:function(html){
				$('#idMainContent').append(html);
			}
		});
	}

	function submitData() {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById('idForm');
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_insert",
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				//console.log(data);
				var resp = JSON.parse(data);
				console.log(resp);
				if(resp.code == 200){
					$('.clErr').html('');
					notify (resp.status, resp.pesan, 'success');
					resetForm();
					open_table('<?php echo date('Y-m-d'); ?>');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					resetForm();
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					resetForm();
				}
				box_overlay('out');
			} 	        
		});
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
			// delay: 2500,
            styling: 'bootstrap3'
        });	
		audio_notif(type);
    }

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

	function audio_notif(type){
		if (type == 'success'){
			var audio = new Audio("<?php echo base_url().'/assets/admin_assets/dist/sound/success.ogg'?>");
		}else{
			var audio = new Audio("<?php echo base_url().'/assets/admin_assets/dist/sound/fail.ogg'?>");
		}
		audio.play();
	}

	function resetForm(){
		$('#idtxtQtyin').val('1');
		$('#idTxtBarcode').val('').focus();
	}
	
</script>