<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idForm';
?>

<?php 
echo form_open_multipart('', array('id' => 'idForm', 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtkd_gudang', 'name' => 'txtkd_gudang', 'placeholder' => 'idtxtkd_gudang', 'value' => isset($gudang->kd_gudang) ? $gudang->kd_gudang : null));
?>
<div class="row">
	<div class="col-md-12">
		
		<div class="form-group">
			<label for="idtxtMetodeInput" class="col-md-2 control-label">Metode Input</label>
			<div class="col-md-4 col-xs-12">
				<div id="idErrMetodeInput" class="clErr"></div>
				<select name="txtMetodeInput" id="idtxtMetodeInput" class="form-control">
					<option value="OTOMATIS" selected>Otomatis</option>
					<option value="MANUAL">Manual</option>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-2 control-label" >Lokasi</label>
			<div class="col-md-2">
				<select id="idtxtGudangLokasi" class="form-control">
					<option value="<?php echo $gudang->kd_gudang; ?>"><?php echo $gudang->nm_gudang; ?></option>
				</select>
			</div>
			<div class="col-md-2">
				<select id="idtxtRakLokasi" class="form-control">
				</select>
			</div>
			<div class="col-md-2">
				<div id="idErrRuang"></div>
				<select id="idtxtRuangLokasi" class="form-control">
				</select>
			</div>
			<div class="col-md-2">
				<div id="idErrrakruangkolom_kd"></div>
				<select name="txtrakruangkolom_kd" id="idtxtRuangKolomLokasi" class="form-control">
				</select>
			</div>
		</div>

		<hr>
		
		<div class="form-group">
			<label for="idTxtBarcode" class="col-md-2 control-label">Barcode</label>
			<div class="col-md-4 col-xs-12">
				<div id="idErrBarcode" class="clErr"></div>
				<?php echo form_input(array('name' => 'txtBarcode', 'id' => 'idTxtBarcode', 'class' => 'form-control input-lg ff-input', 'placeholder' => 'Barcode', 'value' => '', 'maxlength'=>'19', 'autofocus' => 'autofocus', 'autocomplete' => 'off' )); ?>
			</div>
			<label for="idtxtQtyin" class="col-md-1 control-label">Qty</label>
			<div class="col-md-2 col-xs-12">
				<div id="idErrQtyin"></div>
				<?php echo form_input(array('type' => 'number', 'name' => 'txtQtyin', 'id' => 'idtxtQtyin', 'class' => 'form-control input-lg ff-input', 'placeholder' => 'Qty', 'value' => 1)); ?>
			</div>
		</div>

		<hr>
	<?php echo form_close(); ?>
	</div>
</div>

<div class="form-group">
	<div class="col-md-4 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" id="idbtnReset" onclick="resetForm()" class="btn btn-default">
			<i class="fa fa-refresh"></i> Reset
		</button>
		<button name="btnSubmit" id="idbtnSubmit" onclick="submitData()" class="btn btn-primary">
			<i class="fa fa-save"></i> Simpan
		</button>
	</div> 
</div>
<script type="text/javascript">

	get_rak_lokasi('<?php echo $gudang->kd_gudang; ?>', '');

	function get_rak_lokasi(valGdg, selected){
		var optRak = $('#idtxtRakLokasi');
		$('#idtxtRakLokasi option').remove();
		optRak.append($('<option></option>').val('').html('--Pilih Rak--'));
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/get_rak",
			type: "GET",
			data: 'gudang_kd='+valGdg,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optRak.append($('<option></option>').val(text.kd_rak).html(text.nm_rak));
					});
				}
				$('#idtxtRakLokasi').val(selected);
			} 	        
		});
	}

	function get_ruang_lokasi(valRak, selected){
		var optRuang = $('#idtxtRuangLokasi');
		$('#idtxtRuangLokasi option').remove();
		optRuang.append($('<option></option>').val('').html('--Pilih Ruang--'));
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/get_ruang",
			type: "GET",
			data: 'rak_kd='+valRak,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optRuang.append($('<option></option>').val(text.kd_rak_ruang).html(text.nm_rak_ruang));
					});
				}
				$('#idtxtRuangLokasi').val(selected);
			} 	        
		});
	}

	function get_ruangKolom_lokasi(valRuang, selected){
		var optRuang = $('#idtxtRuangKolomLokasi');
		$('#idtxtRuangKolomLokasi option').remove();
		optRuang.append($('<option></option>').val('').html('--Pilih Ruang Kolom--'));
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/get_ruang_kolom",
			type: "GET",
			data: 'kd_rak_ruang='+valRuang,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optRuang.append($('<option></option>').val(text.rakruangkolom_kd).html(text.rakruangkolom_nama));
					});
				}
				$('#idtxtRuangKolomLokasi').val(selected);
			} 	        
		});
	}

	$('#idtxtRakLokasi').on('change', function () {
		get_ruang_lokasi(this.value, '');
	});

	$('#idtxtRuangLokasi').on('change', function () {
		get_ruangKolom_lokasi(this.value, '');
	});
</script>