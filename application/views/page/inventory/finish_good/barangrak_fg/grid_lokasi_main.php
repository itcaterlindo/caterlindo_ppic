<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
$master_var = 'LokasiRak';
$form_id = 'idForm'.$master_var;
?>

<?php 
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>

<div class="form-group">
	<label class="col-sm-1 control-label" >Lokasi</label>
	<div class="col-sm-2">
		<select id="idtxtGudangGrid" class="form-control">
		</select>
	</div>
	<div class="col-sm-2">
		<select id="idtxtRakGrid" class="form-control">
		</select>
	</div>
	<div class="col-sm-2">
		<div id="idErrRuang"></div>
		<select id="idtxtRuangGrid" class="form-control">
		</select>
	</div>
	<div class="col-sm-2">
		<div id="idErrRuangKolomGrid"></div>
		<select name="txtRuangKolomGrid" id="idtxtRuangKolomGrid" class="form-control">
		</select>
	</div>
</div>
<hr>
<div id="loadContentGrid"></div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" class="btn btn-primary btn-flat">
			<i class="fa fa-save"></i> Submit
		</button>
	</div>
</div>

<?php 
echo form_close(); 
?>

<script type="text/javascript">
	get_warehouse_grid();
	open_table_blm_out();

	function open_table_blm_out () {
		$.ajax({
			type: "GET",
			url: "<?php echo base_url().$class_link; ?>/grid_lokasi_table",
			success: function(html){
				$('#loadContentGrid').html(html);
			} 	        
		});
	}

	function get_warehouse_grid(){
		var optGdg = $('#idtxtGudangGrid');
		$('#idtxtGudangGrid option').remove();
		optGdg.append($('<option></option>').val('').html('--Pilih Warehouse--'));
		$.ajax({
			type: "GET",
			url: "<?php echo base_url().$class_link; ?>/get_gudang",
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optGdg.append($('<option></option>').val(text.kd_gudang).html(text.nm_gudang));
					});
				}
			} 	        
		});
	}

	function get_rak_grid(valGdg){
		var optRak = $('#idtxtRakGrid');
		$('#idtxtRakGrid option').remove();
		optRak.append($('<option></option>').val('').html('--Pilih Rak--'));
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/get_rak",
			type: "GET",
			data: 'gudang_kd='+valGdg,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optRak.append($('<option></option>').val(text.kd_rak).html(text.nm_rak));
					});
				}
			} 	        
		});
	}

	function get_ruang_grid(valRak){
		var optRuang = $('#idtxtRuangGrid');
		$('#idtxtRuangGrid option').remove();
		optRuang.append($('<option></option>').val('').html('--Pilih Ruang--'));
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/get_ruang",
			type: "GET",
			data: 'rak_kd='+valRak,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optRuang.append($('<option></option>').val(text.kd_rak_ruang).html(text.nm_rak_ruang));
					});
				}
			} 	        
		});
	}

	function get_ruang_kolom_grid(valRuang){
		var optRuangKolom = $('#idtxtRuangKolomGrid');
		$('#idtxtRuangKolomGrid option').remove();
		optRuangKolom.append($('<option></option>').val('').html('--Pilih Ruang Kolom--'));
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/get_ruang_kolom",
			type: "GET",
			data: 'kd_rak_ruang='+valRuang,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optRuangKolom.append($('<option></option>').val(text.rakruangkolom_kd).html(text.rakruangkolom_nama));
					});
				}
			} 	        
		});
	}

	$('#idtxtGudangGrid').on('change', function () {
		$('#idtxtRuangKolomGrid option').remove();
		get_rak_grid(this.value, '');
	});

	$('#idtxtRakGrid').on('change', function () {
		get_ruang_grid(this.value, '');
	});

	$('#idtxtRuangGrid').on('change', function () {
		get_ruang_kolom_grid(this.value, '');
	});

	$('#<?php echo $form_id; ?>').submit(function(e) {
		box_overlay('in');
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_update_lokasirak_batch",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
                    generateToken (resp.csrf);
					open_table_blm_out();
					box_overlay('out');
				}else if (resp.code == 401){
                    $('#idErrRuangKolomGrid').html(resp.pesan.idErrRuangKolomGrid);
                    generateToken (resp.csrf);
					box_overlay('out');
                }else if(resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
					box_overlay('out');
				}else{
                    notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
					box_overlay('out');
                }
			} 	        
		});
		
		// $("#<?php //echo $form_id; ?>").unbind('submit');
		return false;
	});
	
</script>