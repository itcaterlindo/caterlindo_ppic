<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table(jnsFilter, gudang, rak, ruangKolom, barcode, barang_kd) {
		$('#<?php echo $box_overlay_id; ?>').fadeIn();
		$('#<?php echo $box_content_id; ?>').slideDown(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_main'; ?>',
				data: { jnsFilter: jnsFilter, gudang: gudang, rak: rak, ruangKolom: ruangKolom, barcode: barcode, barang_kd: barang_kd},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					// moveTo('idMainContent');
				}
			});
		});
		$('#<?php echo $box_overlay_id; ?>').fadeOut();
	}

	function remove_table() {
		$('#<?php echo $box_content_id; ?>').slideUp();
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function edit_lokasi(id){
		$('#idFormBoxLokasiFG').remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/form_lokasi_box'; ?>',
			data: { id: id, },
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function gridEdit(){
		$('#idFormBoxGridLokasiFG').remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/grid_lokasi_box'; ?>',
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

</script>