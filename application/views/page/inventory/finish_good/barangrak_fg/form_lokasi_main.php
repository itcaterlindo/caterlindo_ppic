<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
$master_var = 'LokasiRak';
$form_id = 'idForm'.$master_var;
if ($rowData != 'kosong'){
	extract($rowData);
}
?>

<?php 
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtfgin_kd', 'name' => 'txtfgin_kd', 'value' => isset($fgin_kd) ? $fgin_kd : null ));
?>

<div class="form-group">
	<label class="col-sm-2 control-label" >Barcode</label>
	<div class="col-sm-3">
		<?php echo form_input(['type'=>'text', 'class'=>'form-control', 'readonly'=>'true', 'value' => isset($fgbarcode_barcode) ? $fgbarcode_barcode:null ]);?>
	</div>
	<div class="col-sm-3">
		<?php echo form_input(['type'=>'text', 'class'=>'form-control', 'readonly'=>'true', 'value' => format_date ($fgin_tglinput, 'd-m-Y H:i:s') ]);?>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label" >Product Code</label>
	<div class="col-sm-4">
		<?php echo form_input(['type'=>'text', 'class'=>'form-control', 'readonly'=>'true', 'value' => isset($item_code) ? $item_code:null ]);?>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label" >Description</label>
	<div class="col-sm-4">
		<?php echo form_textarea(['rows'=> '2', 'class'=>'form-control', 'readonly'=>'true', 'value' => isset($fgbarcode_desc) ? $fgbarcode_desc:null]);?>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label" >Dimension</label>
	<div class="col-sm-4">
		<?php echo form_textarea(['rows'=> '2', 'class'=>'form-control', 'readonly'=>'true', 'value' => isset($fgbarcode_dimensi) ? $fgbarcode_dimensi:null ]);?>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label" >Lokasi</label>
	<div class="col-sm-2">
		<select name="txtGudang" id="idtxtGudangLokasi" class="form-control">
		</select>
	</div>
	<div class="col-sm-2">
		<select name="txtRak" id="idtxtRakLokasi" class="form-control">
		</select>
	</div>
	<div class="col-sm-2">
		<div id="idErrRuang"></div>
		<select name="txtRuang" id="idtxtRuangLokasi" class="form-control">
		</select>
	</div>
	<div class="col-sm-2">
		<div id="idErrRuangKolom"></div>
		<select name="txtRuangKolom" id="idtxtRuangKolomLokasi" class="form-control">
		</select>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" class="btn btn-primary btn-flat">
			<i class="fa fa-save"></i> Submit
		</button>
	</div>
</div>

<?php 
echo form_close(); 
?>

<script type="text/javascript">
	get_warehouse_lokasi('<?php echo $gudang_kd; ?>');
	get_rak_lokasi('<?php echo $gudang_kd; ?>', '<?php echo $rak_kd; ?>');
	get_ruang_lokasi('<?php echo $rak_kd; ?>', '<?php echo $kd_rak_ruang; ?>');
	get_ruangKolom_lokasi('<?php echo $kd_rak_ruang; ?>', '<?php echo $rakruangkolom_kd; ?>');

	function get_warehouse_lokasi(selected){
		var optGdg = $('#idtxtGudangLokasi');
		$('#idtxtGudangLokasi option').remove();
		optGdg.append($('<option></option>').val('').html('--Pilih Warehouse--'));
		$.ajax({
			type: "GET",
			url: "<?php echo base_url().$class_link; ?>/get_gudang",
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optGdg.append($('<option></option>').val(text.kd_gudang).html(text.nm_gudang));
					});
				}
					$('#idtxtGudangLokasi').val(selected);  /** Untuk selected */
			} 	        
		});
	}

	function get_rak_lokasi(valGdg, selected){
		var optRak = $('#idtxtRakLokasi');
		$('#idtxtRakLokasi option').remove();
		optRak.append($('<option></option>').val('').html('--Pilih Rak--'));
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/get_rak",
			type: "GET",
			data: 'gudang_kd='+valGdg,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optRak.append($('<option></option>').val(text.kd_rak).html(text.nm_rak));
					});
				}
				$('#idtxtRakLokasi').val(selected);
			} 	        
		});
	}

	function get_ruang_lokasi(valRak, selected){
		var optRuang = $('#idtxtRuangLokasi');
		$('#idtxtRuangLokasi option').remove();
		optRuang.append($('<option></option>').val('').html('--Pilih Ruang--'));
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/get_ruang",
			type: "GET",
			data: 'rak_kd='+valRak,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optRuang.append($('<option></option>').val(text.kd_rak_ruang).html(text.nm_rak_ruang));
					});
				}
				$('#idtxtRuangLokasi').val(selected);
			} 	        
		});
	}

	function get_ruangKolom_lokasi(valRuang, selected){
		var optRuang = $('#idtxtRuangKolomLokasi');
		$('#idtxtRuangKolomLokasi option').remove();
		optRuang.append($('<option></option>').val('').html('--Pilih Ruang Kolom--'));
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/get_ruang_kolom",
			type: "GET",
			data: 'kd_rak_ruang='+valRuang,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optRuang.append($('<option></option>').val(text.rakruangkolom_kd).html(text.rakruangkolom_nama));
					});
				}
				$('#idtxtRuangKolomLokasi').val(selected);
			} 	        
		});
	}

	$('#idtxtGudangLokasi').on('change', function () {
		$('#idtxtRuangKolomLokasi option').remove();
		get_rak_lokasi(this.value, '');
	});

	$('#idtxtRakLokasi').on('change', function () {
		get_ruang_lokasi(this.value, '');
	});

	$('#idtxtRuangLokasi').on('change', function () {
		get_ruangKolom_lokasi(this.value, '');
	});

	// $(document).on('submit', '#<?php //echo $form_id; ?>', function(e) {
	$('#<?php echo $form_id; ?>').submit(function(e) {
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_update_lokasirak",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
                    $('#idErrProductCode').html('');
					notify (resp.status, resp.pesan, 'success');
					$('#idFormBoxLokasiFG').slideUp();
					remove_table()
                    // generateToken (resp.csrf);
				}else if (resp.code == 401){
                    $('#idErrRuangKolom').html(resp.pesan.idErrRuangKolom);
                    generateToken (resp.csrf);
                }else if (rsp.code == 400){
                    notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
                }else{
                    notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
                }
			} 	        
		});
		
		// $("#<?php //echo $form_id; ?>").unbind('submit');
		return false;
	});
	
</script>