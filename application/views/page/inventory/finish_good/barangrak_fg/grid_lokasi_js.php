<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form();

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_form() {
		$('#<?php echo $box_content_id; ?>').slideDown(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/grid_lokasi_main'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

</script>