<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'PenyimpananRak';
$form_id = 'idForm'.$master_var;
?>

<?php 
//echo form_open_multipart('', array('id' => $form_id, ));
//echo form_input(array('type' => 'hidden', 'id' => 'idtxtKdBarang', 'name' => 'txtKdBarang', 'class'=>'ff-input', 'value' => ''));
?>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">
			<label for="idtxtFilter">Jenis Filter</label>
			<div id="idErrJnsFilter"></div>
			<?php echo form_dropdown('txtFilter', $opsi, null, ['id'=> 'idtxtFilter', 'class'=> 'form-control']); ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<br>
			<button id="idbtnFilter" class="btn btn-sm btn-success"> <i class="fa fa-check-circle"></i> Pilih</button>
		</div>
	</div>
</div>

<hr>

<div  class="row tt-filter" style="display: none;" id="idFilterBarcode">
	<div class="col-sm-4">
		<div class="form-group">
			<label class="col-sm-3 control-label">Barcode</label>
			<div class="col-sm-8">
			<?php 
			echo form_input(array('id'=> 'idtxtBarcode', 'name' => 'txtBarcode', 'class'=> 'form-control', 'placeholder'=>'Input Barcode'));
			?>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<button id="btnSubmitBarcode" class="btn btn-sm btn-info"><i class="fa fa-search"></i> Tampilkan</button>
	</div>
</div>

<div  class="row tt-filter" style="display: none;" id="idFilterProductCode">
	<div class="col-sm-4">
		<div class="form-group">
			<label class="col-sm-3 control-label">Product Code</label>
			<div class="col-sm-8">
			<?php 
			echo form_input(array('type'=> 'hidden', 'id'=> 'idtxtbarang_kd', 'name' => 'txtbarang_kd'));
			echo form_input(array('id'=> 'idtxtProductcode', 'name' => 'txtProductcode', 'class'=> 'form-control', 'placeholder' => 'Input product code'));
			?>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<button id="idbtnSubmitProductcode" class="btn btn-sm btn-info"><i class="fa fa-search"></i> Tampilkan</button>
	</div>
</div>

<div class="row tt-filter" style="display: none;" id="idFilterRuang">
	<div class="col-sm-2">
		<div class="form-group">
			<label for="idtxtGudang">Warehouse</label>
			<?php 
			echo form_dropdown('txtGudang', '', '', ['id'=> 'idtxtGudang', 'class'=> 'form-control']);
			?>
		</div>
	</div>
	<div class="col-sm-2">
		<div class="form-group">
			<label for="idtxtRak">Rak</label>
				<?php echo form_dropdown('txtRak', null, null, ['id'=> 'idtxtRak', 'class'=> 'form-control']);?>
		</div>
	</div>
	<div class="col-sm-2">
		<div class="form-group">
			<label for="idtxtRuang">Ruang</label>
			<?php echo form_dropdown('txtRuang', null, null, ['id'=> 'idtxtRuang', 'class'=> 'form-control']);?>
		</div>
	</div>
	<div class="col-sm-2">
		<div class="form-group">
			<label for="idtxtRuangKolom">Ruang Kolom</label>
			<?php echo form_dropdown('txtRuangKolom', null, null, ['id'=> 'idtxtRuangKolom', 'class'=> 'form-control']);?>
		</div>
	</div>
	<div class="col-sm-3">
		<br>
		<button type="submit" id="idbtnSubmitFormRuang" class="btn btn-sm btn-info"><i class="fa fa-search"></i> Tampilkan</button>
	</div>
	<hr>
</div>
<?php //echo form_close(); ?>

<script type="text/javascript">
	$(document).ready(function () {
		/* --start product typeahead.js-- */
		var Product = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			url: '<?php echo base_url('auto_complete/get_barang'); ?>',
			remote: {
				url: '<?php echo base_url('auto_complete/get_barang'); ?>',
				prepare: function (query, settings) {
					settings.type = 'GET';
					settings.contentType = 'application/json; charset=UTF-8';
					settings.data = 'item_code='+query;

					return settings;
				},
				wildcard: '%QUERY'
			}
		});

		$('#idtxtProductcode').typeahead(null, {
			limit: 50,
			minLength: 1,
			name: 'product_search',
			display: 'item_code',
			valueKey: 'get_product',
			source: Product.ttAdapter()
		});

		$('#idtxtProductcode').bind('typeahead:select', function(obj, selected) {
			$('#idtxtbarang_kd').val(selected.kd_barang);
			$('#idbtnSubmitProductcode').focus();
		});
		/* --end of product typeahead.js-- */
	});

	$('#idtxtGudang').on('change', function () {
		$('#idtxtRuang option').remove();
		get_rak(this.value);
	});

	$('#idtxtRak').on('change', function () {
		get_ruang(this.value);
	});

	$('#idtxtRuang').on('change', function () {
		get_ruang_kolom(this.value);
	});

	function get_warehouse(){
		var optGdg = $('#idtxtGudang');
		$('#idtxtGudang option').remove();
		optGdg.append($('<option></option>').val('').html('--Pilih Warehouse--'));
		$.ajax({
			type: "GET",
			url: "<?php echo base_url().$class_link; ?>/get_gudang",
			// contentType: false,
			// cache: false,
			// processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optGdg.append($('<option></option>').val(text.kd_gudang).html(text.nm_gudang));
					});
				}
			} 	        
		});
	}

	function get_rak(valGdg){
		var optRak = $('#idtxtRak');
		$('#idtxtRak option').remove();
		optRak.append($('<option></option>').val('').html('--Pilih Rak--'));
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/get_rak",
			type: "GET",
			// contentType: false,
			data: 'gudang_kd='+valGdg,
			// cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optRak.append($('<option></option>').val(text.kd_rak).html(text.nm_rak));
					});
				}
			} 	        
		});
	}

	function get_ruang(valRak){
		var optRuang = $('#idtxtRuang');
		$('#idtxtRuang option').remove();
		optRuang.append($('<option></option>').val('').html('--Pilih Ruang--'));
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/get_ruang",
			type: "GET",
			// contentType: false,
			data: 'rak_kd='+valRak,
			// cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optRuang.append($('<option></option>').val(text.kd_rak_ruang).html(text.nm_rak_ruang));
					});
				}
			} 	        
		});
	}

	function get_ruang_kolom(valRuang){
		var optRuangKolom = $('#idtxtRuangKolom');
		$('#idtxtRuangKolom option').remove();
		optRuangKolom.append($('<option></option>').val('').html('--Pilih Ruang Kolom--'));
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/get_ruang_kolom",
			type: "GET",
			data: 'kd_rak_ruang='+valRuang,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optRuangKolom.append($('<option></option>').val(text.rakruangkolom_kd).html(text.rakruangkolom_nama));
					});
				}
			} 	        
		});
	}

	$('#btnSubmitForm').click(function(){
		event.preventDefault();
		
	});

	/** Filter pencarian berdasarkan */
	$('#btnSubmitBarcode').click(function(){
		var jnsFilter = $('#idtxtFilter').val();
		var barcode = $.trim($('#idtxtBarcode').val());
		event.preventDefault();
		open_table(jnsFilter, null, null, null, barcode, null);
	});

	$('#idbtnSubmitProductcode').click(function(){
		var jnsFilter = $('#idtxtFilter').val();
		var barang_kd = $.trim($('#idtxtbarang_kd').val());
		event.preventDefault();
		open_table(jnsFilter, null, null, null, null, barang_kd);
	});

	$('#idbtnSubmitFormRuang').click(function(){
		var jnsFilter = $('#idtxtFilter').val();
		var ruangKolom = $.trim($('#idtxtRuangKolom').val());
		event.preventDefault();
		open_table(jnsFilter, null, null, ruangKolom, null, null);
	});
	
	$('#idbtnFilter').click(function(e){
		e.preventDefault();
		var code = $('#idtxtFilter').val();
		switch (code) {
		case '0':
			resetFilter();
			break;
		case '1':
			resetFilter();
			open_table(code, null, null, null, null, null);
			break;
		case '2':
			resetFilter();
			open_table(code, null, null, null, null, null);
			break;
		case '3':
			resetFilter();
			$('#idtxtBarcode').val('');
			$('#idFilterBarcode').slideDown();
			break;
		case '4':
			resetFilter();
			$('#idtxtProductcode').val('');
			$('#idFilterProductCode').slideDown();

			break;
		case '5':
			resetFilter();
			get_warehouse();
			$('#idtxtGudang').val('');
			$('#idFilterRuang').slideDown();
			break;
		}
	});

	function resetFilter() {
		$('.tt-filter').slideUp();
	}

	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}
	
</script>