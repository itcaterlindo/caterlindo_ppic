<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
	td.dt-blue {background-color: blue; color:white; }
	td.dt-green {background-color: green; color:white; }
</style>

<div class="row">
	<div class="col-md-12">
		<table id="idTableGrid" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%; font-size:100%;">
			<thead>
				<tr>
				<th style="width:1%; text-align:center;"><input type="checkbox" id="check_all" ></th>
					<th style="width:1%; text-align:center;">No.</th>
					<th style="width:10%; text-align:center;">Tgl Masuk</th>
					<th style="width:10%; text-align:center;">Item Code</th>
					<th style="width:15%; text-align:center;">Barcode</th>
					<th style="width:20%; text-align:center;">Deskripsi</th>
					<th style="width:1%; text-align:center;">Qty</th>
					<th style="width:5%; text-align:center;">Lokasi</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$no = 1;
				foreach ($resultData as $row): ?>
					<tr>
						<td style="text-align:center;"><input type="checkbox" class="data-check" name="txtFginkd[]" value="<?php echo $row['fgin_kd'];?>"></td>
						<td><?php echo $no;?></td>
						<td><?php echo format_date($row['fgin_tglinput'], 'Y-m-d H:i:s');?></td>
						<td><?php echo $row['item_code'];?></td>
						<td><?php echo $row['fgbarcode_barcode'];?></td>
						<td><?php echo $row['fgbarcode_desc'].'/'.$row['fgbarcode_dimensi'];?></td>
						<td><?php echo $row['fgin_qty'];?></td>
						<td><?php echo $row['rakruangkolom_kd'];?></td>
					</tr>
				<?php 
					$no++;
				endforeach;?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	$("#check_all").click(function () {
        $(".data-check").prop('checked', $(this).prop('checked'));
    });

	render_dt('#idTableGrid');

	function render_dt(table_elem) {
		$(table_elem).DataTable({
			"scrollX": true,
			"scrollY": "300px",
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
		});
	}

</script>