<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	$('#<?= $box_loader_id?>').hide();
	open_form_main();
	open_table_box();
	
	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_form_main() {
		$('#<?php echo $box_content_id; ?>').load('<?php echo base_url().$class_link?>/form_main');
		moveTo('idMainContent');
	}

	function open_table_box() {
		var url = '<?php echo base_url().$class_link?>/table_box';
		$.ajax({
			type: 'GET',
			url: url,
			success:function(html){
				$('#idMainContent').append(html);
			}
		});
		// moveTo('idMainContent');
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
			// delay: 2500,
            styling: 'bootstrap3'
        });	
    }
</script>