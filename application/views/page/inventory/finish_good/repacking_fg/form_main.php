<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
// $input_opts = ['' => '-- Pilih Input Option --', 'item_code' => 'Item Code', 'barcode' => 'Barcode Scanner'];

/* --Masukkan setting properti untuk form-- */
$master_var = 'repackingFG';
$form_id = 'idForm'.$master_var;
?>

<?php 
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtFgbarcodekd', 'name' => 'txtFgbarcodekd', 'class'=>'ff-input', 'value' => ''));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtFgkd', 'name' => 'txtfgKd', 'class'=>'ff-input', 'value' => ''));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtbarangKd', 'name' => 'txtbarangKd', 'class'=>'ff-input', 'value' => ''));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtitemBarcode', 'name' => 'txtitemBarcode', 'class'=>'ff-input', 'value' => ''));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtfgin_kd', 'name' => 'txtfgin_kd', 'class'=>'ff-input', 'value' => ''));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtkd_rak_ruangKolom', 'name' => 'txtkd_rak_ruangKolom', 'class'=>'ff-input', 'value' => ''));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtkd_dn', 'name' => 'txtkd_dn', 'class'=>'ff-input', 'value' => ''));
?>

<div class="nav-tabs-custom">
<ul class="nav nav-tabs">
	<li class="active"><a href="#qty" data-toggle="tab" aria-expanded="true">Repack QTY</a></li>
	<li class=""><a href="#replace" data-toggle="tab" aria-expanded="false">Repack Replace</a></li>
</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="qty">
			<!-- TAB 1 -->
			<div class="form-group">
				<label for="idtxtProductCode" class="col-md-2 control-label">Barcode</label>
				<div class="col-md-4 col-xs-12">
					<div id="idErrBarcode"></div>
					<?php echo form_input(array('name' => 'txtBarcode', 'id' => 'idtxtBarcode', 'class' => 'form-control ff-input input-lg', 'placeholder' => 'Barcode', 'value' => '', 'maxlength'=>'19', 'autocomplete' => 'off')); ?>
				</div>
				<label for="idtxtQty" class="col-md-1 control-label">Qty</label>
				<div class="col-md-2 col-xs-12">
					<div id="idErrQty"></div>
					<?php echo form_input(array('type' => 'number', 'name' => 'txtQty', 'id' => 'idtxtQty', 'class' => 'form-control ff-input', 'placeholder' => 'Qty', 'value' => '', 'readonly'=>'true')); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="idtxtProdcode" class="col-md-2 control-label">Product Code</label>
				<div class="col-md-4 col-xs-12">
					<div id="idErrProdcode"></div>
					<?php echo form_input(array('name' => 'txtProdcode', 'id' => 'idtxtProdcode', 'class' => 'form-control ff-input	', 'placeholder' => 'Product Code', 'value' => '', 'readonly'=>'true')); ?>
				</div>
				<label for="idtxtBatchShift" class="col-md-1 control-label">Batch-shift</label>
				<div class="col-md-2 col-xs-12">
					<div id="idErrBatchShift"></div>
					<?php echo form_input(array('type' => 'text', 'name' => 'txtBatchShift', 'id' => 'idtxtBatchShift', 'class' => 'form-control ff-input', 'placeholder' => 'Batch-shift', 'value' => '', 'readonly'=>'true')); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="idtxtDescription" class="col-md-2 control-label">Description</label>
				<div class="col-md-4 col-xs-12">
					<div id="idErrDescription"></div>
					<?php echo form_textarea(array('name' => 'txtDescription', 'id' => 'idTxtDescription', 'rows'=>'2', 'class' => 'form-control ff-input', 'placeholder' => 'Description', 'readonly'=>'true')); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="idtxtDimension" class="col-md-2 control-label">Dimension</label>
				<div class="col-md-4 col-xs-12">
					<div id="idErrDimension"></div>
					<?php echo form_textarea(array('name' => 'txtDimension', 'id' => 'idTxtDimension', 'rows'=>'2', 'class' => 'form-control ff-input', 'placeholder' => 'Dimension', 'readonly'=>'true')); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="idtxtNote" class="col-md-2 control-label">Note</label>
				<div class="col-md-4 col-xs-12">
					<div id="idErrNote"></div>
					<?php echo form_textarea(array('name' => 'txtNote', 'id' => 'idTxtNote', 'rows'=>'2', 'class' => 'form-control ff-input', 'placeholder' => 'Note')); ?>
				</div>
			</div>
			<hr>
			<div class="row">
			<table id="idtabelRepack" class="table table-bordered table-striped table-hover display responsive nowrap">
				<thead>
					<tr style="width:10%; text-align:center; font-weight: bold">
						<td style="width:10%; text-align:center;"> Product Code </td>
						<td style="width:10%; text-align:center;"> Description </td>
						<td style="width:10%; text-align:center;"> Dimension </td>
						<td style="width:10%; text-align:center;"> Qty </td>
						<td style="width:10%; text-align:center;"> Aksi </td>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			</div>
			<hr>
			<?php echo form_close(); ?>
			<!-- END TAB 1 -->
		</div>

		<div class="tab-pane" id="replace">
			<?php 
				echo form_open_multipart('', array('id' => $form_id."replace", 'class' => 'form-horizontal'));
				echo form_input(array('type' => 'hidden', 'id' => 'idtxtFgbarcodekdReplace', 'name' => 'txtFgbarcodekdReplace', 'class'=>'ff-input', 'value' => ''));
				echo form_input(array('type' => 'hidden', 'id' => 'idtxtFgkdReplace', 'name' => 'txtfgKdReplace', 'class'=>'ff-input', 'value' => ''));
				echo form_input(array('type' => 'hidden', 'id' => 'idtxtbarangKdReplace', 'name' => 'txtbarangKdReplace', 'class'=>'ff-input', 'value' => ''));
				echo form_input(array('type' => 'hidden', 'id' => 'idtxtitemBarcodeReplace', 'name' => 'txtitemBarcodeReplace', 'class'=>'ff-input', 'value' => ''));
				echo form_input(array('type' => 'hidden', 'id' => 'idtxtfgin_kdReplace', 'name' => 'txtfgin_kdReplace', 'class'=>'ff-input', 'value' => ''));
				echo form_input(array('type' => 'hidden', 'id' => 'idtxtkd_rak_ruangKolomReplace', 'name' => 'txtkd_rak_ruangKolomReplace', 'class'=>'ff-input', 'value' => ''));
				echo form_input(array('type' => 'hidden', 'id' => 'idtxtkd_dnReplace', 'name' => 'txtkd_dnReplace', 'class'=>'ff-input', 'value' => ''));
			?>
			<!-- TAB 2 -->
			<div class="form-group">
				<label for="idtxtProductCode" class="col-md-2 control-label">Barcode</label>
				<div class="col-md-4 col-xs-12">
					<div id="idErrBarcodeReplace"></div>
					<?php echo form_input(array('name' => 'txtBarcodeReplace', 'id' => 'idtxtBarcodeReplace', 'class' => 'form-control ff-input input-lg', 'placeholder' => 'Barcode', 'value' => '', 'maxlength'=>'19', 'autocomplete' => 'off')); ?>
				</div>
				<label for="idtxtQty" class="col-md-1 control-label">Qty</label>
				<div class="col-md-2 col-xs-12">
					<div id="idErrQtyReplace"></div>
					<?php echo form_input(array('type' => 'number', 'name' => 'txtQtyReplace', 'id' => 'idtxtQtyReplace', 'class' => 'form-control ff-input', 'placeholder' => 'Qty', 'value' => '', 'readonly'=>'true')); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="idtxtProdcode" class="col-md-2 control-label">Product Code</label>
				<div class="col-md-4 col-xs-12">
					<div id="idErrProdcodeReplace"></div>
					<?php echo form_input(array('name' => 'txtProdcodeReplace', 'id' => 'idtxtProdcodeReplace', 'class' => 'form-control ff-input', 'placeholder' => 'Product Code', 'value' => '', 'readonly'=>'true')); ?>
				</div>
				<label for="idtxtBatchShift" class="col-md-1 control-label">Batch-shift</label>
				<div class="col-md-2 col-xs-12">
					<div id="idErrBatchShiftReplace"></div>
					<?php echo form_input(array('type' => 'text', 'name' => 'txtBatchShiftReplace', 'id' => 'idtxtBatchShiftReplace', 'class' => 'form-control ff-input', 'placeholder' => 'Batch-shift', 'value' => '', 'readonly'=>'true')); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="idtxtDescription" class="col-md-2 control-label">Description</label>
				<div class="col-md-4 col-xs-12">
					<div id="idErrDescriptionReplace"></div>
					<?php echo form_textarea(array('name' => 'txtDescriptionReplace', 'id' => 'idTxtDescriptionReplace', 'rows'=>'2', 'class' => 'form-control ff-input', 'placeholder' => 'Description', 'readonly'=>'true')); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="idtxtDimension" class="col-md-2 control-label">Dimension</label>
				<div class="col-md-4 col-xs-12">
					<div id="idErrDimensionReplace"></div>
					<?php echo form_textarea(array('name' => 'txtDimensionReplace', 'id' => 'idTxtDimensionReplace', 'rows'=>'2', 'class' => 'form-control ff-input', 'placeholder' => 'Dimension', 'readonly'=>'true')); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="idtxtNote" class="col-md-2 control-label">Note</label>
				<div class="col-md-4 col-xs-12">
					<div id="idErrNoteReplace"></div>
					<?php echo form_textarea(array('name' => 'txtNoteReplace', 'id' => 'idTxtNoteReplace', 'rows'=>'2', 'class' => 'form-control ff-input', 'placeholder' => 'Note')); ?>
				</div>
			</div>

			<hr>
			<div class="form-group">
				<div class="col-md-12 col-xs-12 text-center">
				<label class="control-label">** DIUBAH KE ITEM **</label>
				</div>
			</div>
			<hr>
			
			<div class="form-group">
				<label for="idtxtItemCode" class="col-md-2 control-label">Item Code</label>
				<div class="col-md-4 col-xs-12">
					<div id="idErrItemCodeBaruReplace"></div>
					<?php 
					echo form_input(array('type' => 'hidden', 'id' => 'idtxtbarangKdBaruReplace', 'name' => 'txtbarangKdBaruReplace', 'class'=>'ff-input', 'value' => ''));
					echo form_input(array('name' => 'txtItemCodeBaruReplace', 'id' => 'idtxtItemCodeBaruReplace', 'class' => 'form-control ff-input', 'placeholder' => 'Item Code', 'value' => '')); 
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="idtxtDescription" class="col-md-2 control-label">Description</label>
				<div class="col-md-4 col-xs-12">
					<div id="idErrDescriptionBaruReplace"></div>
					<?php echo form_textarea(array('name' => 'txtDescriptionBaruReplace', 'id' => 'idTxtDescriptionBaruReplace', 'rows'=>'2', 'class' => 'form-control ff-input', 'placeholder' => 'Description', 'readonly'=>'true')); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="idtxtDimension" class="col-md-2 control-label">Dimension</label>
				<div class="col-md-4 col-xs-12">
					<div id="idErrDimensionBaruReplace"></div>
					<?php echo form_textarea(array('name' => 'txtDimensionBaruReplace', 'id' => 'idTxtDimensionBaruReplace', 'rows'=>'2', 'class' => 'form-control ff-input', 'placeholder' => 'Dimension', 'readonly'=>'true')); ?>
				</div>
			</div>

			<hr>
			<?php 
			echo form_close(); 
			?>
			<!-- END TAB 2 -->
		</div>
	</div>
</div>



<div class="form-group">
	<div class="col-md-4 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" id="idbtnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
		<button name="btnSubmit" id="idbtnSubmit" onclick=submitData() class="btn btn-primary btn-flat">
			<i class="fa fa-save"></i> Submit
		</button>
	</div> 
</div>
<?php //echo form_close(); ?>

<script type="text/javascript">

	$('#idtxtBarcode').keyup(function(e){
		var val = $(this).val();
		var code = e.keyCode || e.which;
		if (code == 13){ /** Untuk enter = 13 */
			$.ajax({
				url: "<?php echo base_url().$class_link; ?>/get_barcode_fg",
				type: "GET",
				data:  "id="+val,
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){
					var resp = JSON.parse(data);
					// console.log(resp);
					if (resp.code == 200){
					// 	// $('#idTxtBarcode').attr('readonly', true);
						$('#idtxtFgbarcodekd').val(resp.data.fgbarcode_kd);
						$('#idtxtFgkd').val(resp.data.fg_kd);
						$('#idtxtfgin_kd').val(resp.data.fgin_kd);
						$('#idtxtbarangKd').val(resp.data.barang_kd);
						$('#idtxtProdcode').val(resp.data.item_code);
						$('#idtxtitemBarcode').val(resp.data.item_barcode);
						$('#idTxtDescription').val(resp.data.fgbarcode_desc);
						$('#idTxtDimension').val(resp.data.fgbarcode_dimensi);
						$('#idtxtQty').val(resp.data.fgin_qty);
						$('#idtxtkd_rak_ruangKolom').val(resp.data.rakruangkolom_kd);
						$('#idtxtkd_dn').val(resp.data.kd_dn);
						var barcode = $('#idtxtBarcode').val();
						$('#idtxtBatchShift').val(barcode.substr(11,8));
						appendTable(resp.data.item_code, resp.data.fgbarcode_desc, resp.data.fgbarcode_dimensi)
					}else if (resp.code == 400) {
						notify(resp.status, resp.pesan, 'error');
					// 	$('#idTxtBarcode').val('');
					// 	$('#idTxtBarcode').focus();
					// 	box_overlay('out');
					}
				} 	        
			});
		}
		e.preventDefault();
		return false;
	});

	$('#idtxtBarcodeReplace').keyup(function(e){
		var val = $(this).val();
		var code = e.keyCode || e.which;
		if (code == 13){ /** Untuk enter = 13 */
			$.ajax({
				url: "<?php echo base_url().$class_link; ?>/get_barcode_fg",
				type: "GET",
				data:  "id="+val,
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){
					var resp = JSON.parse(data);
					// console.log(resp);
					if (resp.code == 200){
					// 	// $('#idTxtBarcode').attr('readonly', true);
						$('#idtxtFgbarcodekdReplace').val(resp.data.fgbarcode_kd);
						$('#idtxtFgkdReplace').val(resp.data.fg_kd);
						$('#idtxtfgin_kdReplace').val(resp.data.fgin_kd);
						$('#idtxtbarangKdReplace').val(resp.data.barang_kd);
						$('#idtxtProdcodeReplace').val(resp.data.item_code);
						$('#idtxtitemBarcodeReplace').val(resp.data.item_barcode);
						$('#idTxtDescriptionReplace').val(resp.data.fgbarcode_desc);
						$('#idTxtDimensionReplace').val(resp.data.fgbarcode_dimensi);
						$('#idtxtQtyReplace').val(resp.data.fgin_qty);
						$('#idtxtkd_rak_ruangKolomReplace').val(resp.data.rakruangkolom_kd);
						$('#idtxtkd_dnReplace').val(resp.data.kd_dn);
						var barcode = $('#idtxtBarcodeReplace').val();
						$('#idtxtBatchShiftReplace').val(barcode.substr(11,8));
						$('#idTxtItemCodeBaruReplace').focus();
					}else if (resp.code == 400) {
						notify(resp.status, resp.pesan, 'error');
					}
				} 	        
			});
		}
		e.preventDefault();
		return false;
	});

	$( document ).ready(function() {
		/* --start product typeahead.js-- */
		var Product = new Bloodhound({
				datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				url: '<?php echo base_url('auto_complete/get_barang'); ?>',
				remote: {
					url: '<?php echo base_url('auto_complete/get_barang'); ?>',
					prepare: function (query, settings) {
						settings.type = 'GET';
						settings.contentType = 'application/json; charset=UTF-8';
						settings.data = 'item_code='+query;

						return settings;
					},
					wildcard: '%QUERY'
				}
			});

		$('#idtxtItemCodeBaruReplace').typeahead(null, {
			limit: 50,
			minLength: 1,
			name: 'product_search',
			display: 'item_code',
			valueKey: 'get_product',
			source: Product.ttAdapter()
		});

		$('#idtxtItemCodeBaruReplace').bind('typeahead:select', function(obj, selected) {
			$('#idTxtDescriptionBaruReplace').val(selected.deskripsi_barang);
			$('#idTxtDimensionBaruReplace').val(selected.dimensi_barang);
			$('#idtxtbarangKdBaruReplace').val(selected.kd_barang);
			$('#idbtnSubmit').focus();
		});
		/* --end of product typeahead.js-- */
	});
	
	/** append table */
	function appendTable(item_code, item_desc, item_dimensi){
		var dataAppend = 
		'<tr>'+
			'<td>'+item_code+'</td>'+
			'<td>'+item_desc+'</td>'+
			'<td>'+item_dimensi+'</td>'+
			'<td><input type="number" name="txtQtyRepack[]" id="idtxtQtyRepack" class="form-control" placeholder="Qty" ></td>'+
			'<td style="text-align:center;"><button type="button" class="btn btn-success btn-xs" onclick="addRepack()" data-toggle="tooltip" data-original-title="Tambah Repack"><i class="fa fa-plus"></i></button></td>'+
		'</tr>';
		$('#idtabelRepack tbody').append(dataAppend);
	}

	function addRepack(){
		var item_code = $('#idtxtProdcode').val();
		var item_desc = $('#idTxtDescription').val();
		var item_dimensi = $('#idTxtDimension').val();
		appendTable(item_code, item_desc, item_dimensi);
	}

	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitData() {
		event.preventDefault();
		if( confirm("Apakah anda ingin menyimpan data tersebut ?") ){
			box_overlay('in');
			var tab = $('.tab-content .active').attr('id');
			if(tab == 'qty'){
				// QTY
				var form = document.getElementById('<?php echo $form_id; ?>');
				var url = "<?php echo base_url().$class_link; ?>/action_insert";
			}else{
				// REPLACE
				var form = document.getElementById('<?php echo $form_id."replace"; ?>');
				var url = "<?php echo base_url().$class_link; ?>/action_insert_replace";
			}
			$.ajax({
				url: url,
				type: "POST",
				data:  new FormData(form),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){
					var resp = JSON.parse(data);
					// console.log(resp);
					if(resp.code == 200){
						$('#idErrBarcode').html('');
						$('#idErrQty').html('');
						notify (resp.status, resp.pesan, 'success');
						generateToken (resp.csrf);
						resetForm();
						open_table();
						box_overlay('out');
					}else if (resp.code == 401){
						$.each( resp.pesan, function( key, value ) {
							$('#'+key).html(value);
						});
						generateToken (resp.csrf);
						box_overlay('out');
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				} 	        
			});
		}
	}

	$('#idbtnReset').click(function(){
		resetForm();
	});
	
	/** Reset Form */
	function resetForm(){
		$('.ff-input').val('');
		$('#idtabelRepack tbody tr').slideUp().remove();
	}
	
</script>