<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
	td.dt-blue {background-color: blue; color:white; }
	td.dt-green {background-color: green; color:white; }
	td.dt-purple {background-color: purple; color:white; }
</style>

<div class="row">
	<div class="col-md-12">
		<table id="idTable" class="table table-bordered table-striped table-hover display responsive" style="width:100%;">
			<thead>
				<tr>
					<th style="width:1%; text-align:center;" class="all">No.</th>
					<!-- <th style="width:4%; text-align:center;" class="all">Opsi</th> -->
					<th style="width:10%; text-align:center;">Tgl Input</th>
					<th style="width:10%; text-align:center;">Item Code</th>
					<th style="width:10%; text-align:center;">Item Barcode</th>
					<th style="width:15%; text-align:center;">Deskripsi</th>
					<th style="width:15%; text-align:center;">Dimensi</th>
					<th style="width:15%; text-align:center;">Jns Repacking</th>
					<th style="width:5%; text-align:center;">Awal</th>
					<th style="width:5%; text-align:center;">Repacking</th>
					<th style="width:5%; text-align:center;">Sisa</th>
				</tr>
			</thead>
			<tbody>
			<?php
				$no = 1;
				foreach ($resultData as $eachData):
					$jns_repack = $eachData['fgrepack_status'];
					if ($jns_repack == 'IN'){
						$barcode = $eachData['barcode_in'];
						$desc = $eachData['desc_in'];
						$dimensi = $eachData['dimensi_in'];
						$awal = $eachData['awal_in'];
						$sisa = $awal + $eachData['fgrepack_qty'];
					}else{
						$barcode = $eachData['barcode_out'];
						$desc = $eachData['desc_out'];
						$dimensi = $eachData['dimensi_out'];
						$awal = $eachData['awal_out'];
						$sisa = $awal - $eachData['fgrepack_qty'];
					}
					?>
					<tr>
						<td style="text-align:center;"><?php echo $no; ?></td>
						<!-- <td style="text-align:center;">-</td> -->
						<td style="text-align:center;"><?php echo format_date($eachData['fgrepack_tglinput'], 'd-m-Y H:i:s'); ?></td>
						<td style="text-align:center;"><?php echo $eachData['item_code']; ?></td>
						<td style="text-align:center;"><?php echo $barcode; ?></td>
						<td style="text-align:center;"><?php echo $desc; ?></td>
						<td style="text-align:center;"><?php echo $dimensi; ?></td>
						<td style="text-align:center;"><?php echo $jns_repack; ?></td>
						<td style="text-align:center;" class="dt-purple"><?php echo $awal; ?></td>
						<td style="text-align:center;" class="dt-blue"><?php echo $eachData['fgrepack_qty']; ?></td>
						<td style="text-align:center;" class="dt-green"><?php echo $sisa; ?></td>
					</tr>
					<?php
					$no++;
				endforeach;
				?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};
	var table = $('#idTable').DataTable({
		// "processing": true,
		// "serverSide": true,
		// "ordering" : true,
		// "ajax": "<?php //echo base_url().$class_link.'/table_data?fgrepack_tglinput='.$fgrepack_tglinput; ?>",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
		// "columnDefs": [
		// 	{"data": null, "searchable": false, "orderable": false, "className": "dt-center", "targets": 0},
		// 	{"searchable": false, "orderable": false, "targets": 1},
		// 	{"className": "dt-left", "targets": 2},
		// 	{"className": "dt-left", "targets": 3},
		// 	{"className": "dt-left", "targets": 4},
		// 	{"className": "dt-left", "targets": 5},
		// 	{"className": "dt-left", "targets": 6},
		// 	{"className": "dt-left", "targets": 7},
		// 	{"className": "dt-right dt-purple", "targets": 8},
		// 	{"className": "dt-right dt-green", "targets": 9},
		// 	{"className": "dt-right dt-blue", "targets": 10},
		// ],
		// "order":[2, 'desc'],
		// "rowCallback": function (row, data, iDisplayIndex) {
		// 	var info = this.fnPagingInfo();
		// 	var page = info.iPage;
		// 	var length = info.iLength;
		// 	var index = page * length + (iDisplayIndex + 1);
		// 	var sisa = 0;
		// 	if (data[7] == 'Adj Barang Masuk'){
		// 		sisa = parseInt(data[8]) + parseInt(data[9]);
		// 	}else{
		// 		sisa = parseInt(data[8]) - parseInt(data[9]);
		// 	}
		// 	$('td:eq(0)', row).html(index);
		// 	$('td:eq(10)', row).html(sisa);
		// }
	});
</script>