<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table();
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table() {
		$('#<?php echo $box_content_id; ?>').slideDown(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_main'; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					// $('#<?php //echo $box_content_id; ?>').slideDown();
					// moveTo('idMainContent');
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function hapus_data(id) {
		// $('#<?php //echo $box_overlay_id; ?>').show();
		var conf = confirm('Apakah anda yakin menghapus item ini ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					// console.log(resp);
					if(resp.code == 201){
						notify (resp.status, resp.pesan, '');
						open_table();
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
					}
					// $('#<?php //echo $box_overlay_id; ?>').hide();
				}
			});
		}
		
	}

</script>