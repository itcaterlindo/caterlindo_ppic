<?php
defined('BASEPATH') or exit('No direct script access allowed!');
// var_dump($identitasSO);
?>
<label for="">#No Sales Order : <?php 
$no_salesorder = $identitasSO['no_salesorder'];
if ($identitasSO['tipe_customer'] == 'Ekspor') {
	$no_salesorder = $identitasSO['no_po'];
}
echo $no_salesorder.'/'.$identitasSO['nm_customer']; ?></label><br>
<hr>
 <table id="idtblSO" class="table table-bordered table-striped table-hover" style="column-width: 10px;width: 100%; font-size: 100%;" border="1">
	<thead>
		<tr>
			<th style="width: 1%; text-align: center;">No.</th>
			<th style="width: 15%;text-align: center;">Prod Code</th>
			<th style="width: 10%; text-align: center;">Jenis</th>
			<th style="width: 10%; text-align: center;">Qty SO</th>
			<th style="width: 10%; text-align: center;">Qty Out</th>
			<th style="width: 15%; text-align: center;">Status</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		$sumQTY_so = 0;
		$totalQTY_out = 0;

		/** Untuk pendukung identifikasi awal kd barang ada apa tidak di td_finishgood_out */
		$aBarang_kd_out = array();
		foreach ($fg_out as $temp){
			$aBarang_kd_out []= $temp['barang_kd'];
		}
		// ksort($item_so_grouping);
		foreach ($item_so_grouping as $each_item):
			?>
			<tr>
				<td style="text-align: center;"><?php echo $no; ?></td>
				<td style="text-align: left;"><?php echo $each_item['item_code']; ?></td>
				<td style="text-align: left;"><?php echo $each_item['nm_group']; ?></td>
				<!-- <td style="text-align: left;"><?php //echo $each_item['deskripsi_barang'].' '.$each_item['dimensi_barang'] ; ?></td> -->
				<!-- <td style="text-align: left;"><?php //echo $each_item['dimensi_barang']; ?></td> -->
				<td style="width: 20px; text-align: right;"><?php echo $each_item['qty']; ?></td>
				<?php
					$qtyout = 0;
					$tempCol = '';
					if (in_array($each_item['barang_kd'], $aBarang_kd_out)){
						foreach ($fg_out as $each_fgout):
							if ($each_fgout['barang_kd'] == $each_item['barang_kd']){
								$qtyout += $each_fgout['qtyout'];
							}
						endforeach;
						$tempCol .= '<td style="width: 20px; text-align: right;">'.$qtyout.'</td>';
					}else{
						$tempCol .= '<td style="width: 20px; text-align: right;">'.$qtyout.'</td>';
					}
					/** Cek status */
					$ceksts = $each_item['qty'] - $qtyout;
					if ($ceksts == 0) {
						$tempCol .= '<td style="width: 20px; text-align: center;"><span class="label label-success">Finish</span></td>';
					}elseif($ceksts > 0 ){
						$tempCol .= '<td style="width: 20px; text-align: center;"><span class="label label-warning">Kurang</span></td>';
					}else{
						$tempCol .= '<td style="width: 20px; text-align: center;"><span class="label label-warning">Lebih</span></td>';
					}
					echo $tempCol;

					$totalQTY_out += $qtyout;
				?>				
			</tr>
			
			<?php
			$sumQTY_so += $each_item['qty'];
			$no++;
		endforeach;
		?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="3" style="text-align: right;"> <strong>Total Qty</strong></td>
			<td style="width: 20px; text-align: right;"> <strong> <?php echo $sumQTY_so; ?> </strong> </td>
			<td style="width: 20px; text-align: right;"> <strong> <?php echo $totalQTY_out; ?> </strong></td>
			<td style="width: 20px; text-align: right;"></td>
		</tr>
	</tfoot>
</table>
<hr>
<label for="">#Additional Item </label>
<hr>
<table id="idtblExclude" class="table table-bordered table-striped table-hover" style="column-width: 10px;width: 100%; font-size: 100%;" border="1">
	<thead>
		<tr>
			<th style="width: 1%; text-align: center;">No.</th>
			<th style="width: 15%;text-align: center;">Prod Code</th>
			<th style="width: 10%; text-align: center;">Qty Out</th>
			<th style="width: 15%; text-align: center;">Status</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		// var_dump($fg_out);
		$arrSO_out = array();
		foreach ($item_so_grouping as $tempSO):
			$arrSO_out []= $tempSO['barang_kd'];
		endforeach;
		
		$no = 1;
		$sumFgOut_exc = 0;
		foreach ($fg_out as $eachFg_out):
			if (!in_array($eachFg_out['barang_kd'], $arrSO_out)) {
			?>
			<tr>
				<td style="text-align: center;"><?php echo $no; ?></td>
				<td style="text-align: left;"><?php echo $eachFg_out['item_code']; ?></td>
				<td style="text-align: right;"><?php echo $eachFg_out['qtyout']; ?></td>
				<td style="text-align: center;"><span class="label label-warning">Additional</span></td>
			</tr>
			<?php
				$sumFgOut_exc += $eachFg_out['qtyout'];
			}
			$no++;
		endforeach;
		?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="2" style="text-align: right;"> <strong>Total Qty Additional</strong></td>
			<td style="width: 10%; text-align: right;"> <strong> <?php echo $sumFgOut_exc; ?> </strong> </td>
			<td style="width: 15%; text-align: right;"></td>
		</tr>
	</tfoot>
</table>


<script type="text/javascript">

render_dt('#idtblSO', '800px');
render_dt('#idtblExclude', '200px');

function render_dt(table_elem, scrollY) {
		$(table_elem).DataTable({
			"scrollX": true,
			"scrollY": scrollY,
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
		});
	}
</script>