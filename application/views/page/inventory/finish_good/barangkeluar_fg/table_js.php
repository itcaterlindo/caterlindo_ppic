<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table('<?php echo $kd_msalesorder; ?>');
	open_detail_salesorder('<?php echo $kd_msalesorder; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table(kd_msalesorder, sts) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/table_main'; ?>',
			data: {kd_msalesorder : kd_msalesorder, sts : sts},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
			}
		});
	}

	function open_detail_salesorder(kd_msalesorder) {
		$('#<?php echo $box_loader_id_2; ?>').fadeIn();
		$('#idmainSalesorder').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/detail_salesorder_main'; ?>',
				data: {kd_msalesorder : kd_msalesorder},
				success: function(html) {
					$('#idmainSalesorder').slideDown().html(html);
					$('#<?php echo $box_loader_id_2; ?>').fadeOut();
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function hapus_data(id) {
		var conf = confirm('Apakah anda yakin menghapus item ini ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					console.log(resp);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, '');
						tablemain_datareload();
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
					}
				}
			});
		}
	}

	$('#idbtnCekSO').click(function(){
		var kd_msalesorder = '<?php echo $kd_msalesorder; ?>';
		open_detail_salesorder(kd_msalesorder);
	});

	$('#idbtnRefresh').click(function(){
		$('#idFormBoxOverlaytblBarangKeluar').fadeIn('xfast');
		tablemain_datareload();
		$('#idFormBoxOverlaytblBarangKeluar').fadeOut('xfast');
	});

	
	
</script>