<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idForm';
?>

<?php 
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal '));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtkd_gudang', 'placeholder' => 'idtxtkd_gudang','name' => 'txtkd_gudang',  'value' => isset($kd_gudang) ? $kd_gudang: null));
?>

<div class="form-group">
	<label for="idtxtProductCode" class="col-md-2 control-label">No Sales Order</label>
	<div class="col-md-5 col-xs-12">
		<div id="idErrNo_salesorder"></div>
		<?php echo form_dropdown('txtNo_salesorder', $opsiSO, isset($SOSelected)? $SOSelected : '', array('class'=> 'form-control select2', 'id'=> 'idtxtNo_salesorder'));?>
	</div>
	<div class="col-md-1 col-xs-12"> 
		<button class="btn btn-success btn-sm" id="idbtnPilihSO" ><i class="fa fa-check-circle"></i> Pilih</button>
	</div>
</div>
<div class="form-group">
	<label for="idTxtBarcode" class="col-md-2 control-label">Barcode</label>
	<div class="col-md-3 col-xs-12">
		<div id="idErrBarcode"></div>
		<?php echo form_input(array('name' => 'txtBarcode', 'id' => 'idTxtBarcode', 'class' => 'form-control input-lg', 'placeholder' => 'Barcode', 'value' => '', 'maxlength'=>'19', 'readonly'=>'true', 'autocomplete' => 'off')); ?>
	</div>
	<label for="idTxtBarcode" class="col-md-1 control-label">Qty</label>
	<div class="col-md-1 col-xs-12">
		<div id="idErrQty"></div>
		<?php echo form_input(array('name' => 'txtQty', 'id' => 'idtxtQty', 'class' => 'form-control input-lg', 'placeholder' => 'Qty','readonly'=>'true')); ?>
	</div>
</div>

<hr>
<?php echo form_close(); ?>
<div class="form-group">
	<div class="col-md-4 col-sm-offset-2 col-xs-12">
		<button name="btnSubmit" id="idbtnSubmit" onclick=submitData() class="btn btn-primary">
			<i class="fa fa-save"></i> Simpan
		</button>
		<button type="reset" name="btnReset" id="idbtnReset" onclick ="resetForm()" class="btn btn-default">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
</div>

<script type="text/javascript">

	$(document).keydown(function(e){
		var code = e.keyCode || e.which;
		if (code == 13){
			e.preventDefault();
			return false;
		}
	});	
	
</script>