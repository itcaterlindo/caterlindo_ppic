<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');
	open_form_main('<?php echo !empty($kd_gudang) ? $kd_gudang: null; ?>');

	$(document).off('click', '#idbtnPilihSO').on('click', '#idbtnPilihSO', function(e) {
		e.preventDefault();
		var no_so = $("#idtxtNo_salesorder").val();
		if (no_so != null ){
			$('#idTxtBarcode').attr('readonly', false);
			$('#idTxtBarcode').focus();
			open_table_box(no_so);
		}else{
			alert('Pilih No Sales Order dahulu !!!');
		}
	});

	$(document).off('keyup', '#idTxtBarcode').on('keyup', '#idTxtBarcode', function(e) {
		var code = e.keyCode || e.which;
		val = $(this).val();
		if (code == 13){ /** Untuk enter = 13 tab = 9 */
			if(val.length == 19 ){
				submitData();
			}else{
				notify('Gagal', 'Input Tidak Sesuai', 'error');
				resetForm();
			}
		}
		e.preventDefault();
		return false;
	});
	
	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_form_main(kd_gudang) {
		$('#idFormBarangKeluarFG').slideUp().remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link?>/form_main',
			data: {kd_gudang:kd_gudang},
			success:function(html){
				$('#<?php echo $box_content_id; ?>').slideDown().append(html);
				$('#idTxtBarcode').focus();
				render_select2('select2');
			}
		});
		moveTo('idMainContent');
	}

	function render_select2(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			allowClear: false,
		});
		// $('.'+valClass).val('').trigger('change');
	}

	function open_table_box(kd_msalesorder) {
		$('#idRowBarangKeluar').slideDown().remove();
		var url = '<?php echo base_url().$class_link?>/table_box';
		$.ajax({
			type: 'GET',
			data: {kd_msalesorder : kd_msalesorder},
			url: url,
			success:function(html){
				$('#idMainContent').append(html);
			}
		});
	}

	function submitData() {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById('idForm');
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_insert",
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					$('#idErrBarcode').html('');
					notify (resp.status, resp.pesan, 'success');
					resetForm();
					$('#idTxtBarcode').focus();
					tablemain_datareload();
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					resetForm();
					$('#idTxtBarcode').focus();
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					resetForm();
					$('#idTxtBarcode').focus();
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					resetForm();
					$('#idTxtBarcode').focus();
				}
				box_overlay('out');
			} 	        
		});
	}

	function resetForm(){
		$('#idTxtBarcode').val('');
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
		box_overlay('out');
		audio_notif(type);
    }
	
	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn('xfast');
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut('xfast');
		}
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function audio_notif(type){
		if (type == 'success'){
			var audio = new Audio("<?php echo base_url().'/assets/admin_assets/dist/sound/success.ogg'?>");
		}else{
			var audio = new Audio("<?php echo base_url().'/assets/admin_assets/dist/sound/fail.ogg'?>");
		}
		
		audio.play();
	}
</script>