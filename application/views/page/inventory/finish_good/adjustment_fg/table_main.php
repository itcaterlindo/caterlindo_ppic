<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-blue {background-color: blue; color:white; }
	td.dt-green {background-color: green; color:white; }
	td.dt-purple {background-color: purple; color:white; }
</style>

<div class="row">
	<div class="col-md-12">
		<table id="idTable" class="table table-bordered table-striped table-hover display" style="width:100%;">
			<thead>
				<tr>
					<th style="width:1%; text-align:center;" class="all">No.</th>
					<th style="width:10%; text-align:center;">Tgl Input</th>
					<th style="width:10%; text-align:center;">Item Code</th>
					<th style="width:10%; text-align:center;">Item Barcode</th>
					<th style="width:20%; text-align:center;">Deskripsi</th>
					<th style="width:5%; text-align:center;">Jns Adjustment</th>
					<th style="width:3%; text-align:center;">Awal</th>
					<th style="width:3%; text-align:center;">Adjustment</th>
					<th style="width:3%; text-align:center;">Sisa</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$no = 1;
				foreach ($resultData as $eachData):
					$jns_adj = $eachData['fgadj_status'];
					if ($jns_adj == 'IN'){
						$barcode = $eachData['barcode_in'];
						$desc = $eachData['desc_in'];
						$dimensi = $eachData['dimensi_in'];
						$awal = $eachData['awal_in'];
						$sisa = $awal + $eachData['fgadj_qty'];
					}else{
						$barcode = $eachData['barcode_out'];
						$desc = $eachData['desc_out'];
						$dimensi = $eachData['dimensi_out'];
						$awal = $eachData['awal_out'];
						$sisa = $awal - $eachData['fgadj_qty'];
					}
					?>
					<tr>
						<td style="text-align:center;"><?php echo $no; ?></td>
						<td style="text-align:left;"><?php echo format_date($eachData['fgadj_tglinput'], 'Y-m-d H:i:s'); ?></td>
						<td style="text-align:left;"><?php echo $eachData['item_code']; ?></td>
						<td style="text-align:left;"><?php echo $barcode; ?></td>
						<td style="text-align:left;"><?php echo $desc.' '.$dimensi; ?></td>
						<td style="text-align:center;"><?php echo $jns_adj; ?></td>
						<td style="text-align:right;" class="dt-purple"><?php echo $awal; ?></td>
						<td style="text-align:right;" class="dt-blue"><?php echo $eachData['fgadj_qty']; ?></td>
						<td style="text-align:right;" class="dt-green"><?php echo $sisa; ?></td>
					</tr>
					<?php
					$no++;
				endforeach;
				?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	render_dt('#idTable');

	function render_dt(attrib = null) {
		$(attrib).DataTable({
			"scrollX": true,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
		});
	}
	
</script>