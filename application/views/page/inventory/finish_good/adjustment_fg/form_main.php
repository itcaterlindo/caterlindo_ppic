<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'AdjFG';
$form_id = 'idForm'.$master_var;
?>

<?php 
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'placeholder' => 'idtxtFgbarcodekd', 'id' => 'idtxtFgbarcodekd', 'name' => 'txtFgbarcodekd', 'class'=>'ff-input', 'value' => ''));
echo form_input(array('type' => 'hidden', 'placeholder' => 'idtxtbarang_kd', 'id' => 'idtxtbarang_kd', 'name' => 'txtbarang_kd', 'class'=>'ff-input', 'value' => ''));
echo form_input(array('type' => 'hidden', 'placeholder' => 'idtxtFgkd', 'id' => 'idtxtFgkd', 'name' => 'txtfgKd', 'class'=>'ff-input', 'value' => ''));
echo form_input(array('type' => 'hidden', 'placeholder' => 'idtxtkd_rak_ruangKolom', 'id' => 'idtxtkd_rak_ruangKolom', 'name' => 'txtkd_rak_ruangKolom', 'class'=>'ff-input', 'value' => ''));
echo form_input(array('type' => 'hidden', 'placeholder' => 'idtxtfgin_kd', 'id' => 'idtxtfgin_kd', 'name' => 'txtfgin_kd', 'class'=>'ff-input', 'value' => ''));
?>

<div class="form-group">
	<label for="idtxtJnsAdj" class="col-md-2 control-label">Jenis Adjustment</label>
	<div class="col-md-4 col-xs-12">
		<div id="idErrJnsAdj"></div>
		<select name="txtJnsAdj" id="idtxtJnsAdj" class="form-control">
			<option value="">-- Pilih Opsi --</option>
			<option value="IN">Transaksikan Masuk</option>
			<option value="OUT">Transaksikan Keluar</option>
		</select>
	</div>
</div>
<div class="form-group">
	<label for="idtxtProductCode" class="col-md-2 control-label">Barcode</label>
	<div class="col-md-4 col-xs-12">
		<div id="idErrBarcode"></div>
		<?php echo form_input(array('name' => 'txtBarcode', 'id' => 'idtxtBarcode', 'class' => 'form-control ff-input input-lg', 'placeholder' => 'Barcode', 'value' => '', 'maxlength'=>'19', 'autocomplete' => 'off')); ?>
	</div>
	<label for="idtxtQty" class="col-md-1 control-label">Qty</label>
	<div class="col-md-2 col-xs-12">
		<div id="idErrQty"></div>
		<?php echo form_input(array('type' => 'number', 'name' => 'txtQty', 'id' => 'idtxtQty', 'class' => 'form-control ff-input', 'placeholder' => 'Qty', 'value' => '', 'readonly' => 'true')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idtxtKet" class="col-md-2 control-label">Keterangan</label>
	<div class="col-md-8 col-xs-12">
		<div id="idErrKet"></div>
		<?php echo form_textarea(array('name' => 'txtKet', 'id' => 'idtxtKet', 'rows'=>'2', 'class' => 'form-control ff-input', 'placeholder' => 'Keterangan')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idtxtDescription" class="col-md-2 control-label">Deskripsi</label>
	<div class="col-md-8 col-xs-12">
		<div id="idErrDescription"></div>
		<?php echo form_textarea(array('name' => 'txtDescription', 'id' => 'idTxtDescription', 'rows'=>'2', 'class' => 'form-control ff-input', 'placeholder' => 'Description', 'readonly'=>'true')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idtxtDimension" class="col-md-2 control-label">Dimensi</label>
	<div class="col-md-8 col-xs-12">
		<div id="idErrDimension"></div>
		<?php echo form_textarea(array('name' => 'txtDimension', 'id' => 'idTxtDimension', 'rows'=>'2', 'class' => 'form-control ff-input', 'placeholder' => 'Dimension', 'readonly'=>'true')); ?>
	</div>
</div>
<hr>
<?php echo form_close(); ?>
<div class="form-group">
	<div class="col-md-4 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" id="idbtnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
		<button name="btnSubmit" id="idbtnSubmit" onclick=submitData() class="btn btn-primary btn-flat">
			<i class="fa fa-save"></i> Submit
		</button>
	</div> 
</div>

<script type="text/javascript">

	$('#idtxtBarcode').keyup(function(e){
		var val = $(this).val();
		var code = e.keyCode || e.which;
		if (code == 13){ /** Untuk enter = 13 */
			box_overlay('in');
			$.ajax({
				url: "<?php echo base_url().$class_link; ?>/get_barcode_fg",
				type: "GET",
				data:  "id="+val,
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){
					var resp = JSON.parse(data);
					// console.log(resp);
					if (resp.code == 200){
						$('#idtxtfgin_kd').val(resp.data.fgin_kd);
						$('#idtxtFgbarcodekd').val(resp.data.fgbarcode_kd);
						$('#idtxtFgkd').val(resp.data.fg_kd);
						$('#idTxtDescription').val(resp.data.fgbarcode_desc);
						$('#idTxtDimension').val(resp.data.fgbarcode_dimensi);
						$('#idtxtkd_rak_ruangKolom').val(resp.data.rakruangkolom_kd);
						$('#idtxtbarang_kd').val(resp.data.barang_kd);
						/** untuk label */
						$('#idtxtItemStock').text('Stock '+resp.data.item_code+' :');
						$('#idtxtStock').text(resp.data.fg_qty);
						/** fokus qty */
						$('#idtxtQty').val(resp.data.fgin_qty);
						$('#idtxtQty').focus();
						box_overlay('out');
					}else if (resp.code == 400) {
						notify(resp.status, resp.pesan, 'error');
					}
				} 	        
			});
		}
		e.preventDefault();
		return false;
	});

	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitData() {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById('<?php echo $form_id; ?>');
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_insert",
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					$('#idErrBarcode').html('');
					$('#idErrQty').html('');
					notify (resp.status, resp.pesan, 'success');
					generateToken (resp.csrf);
					resetForm();
					$('#idTxtBarcode').focus();
					open_table();
					box_overlay('out');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	$('#idbtnReset').click(function(){
		resetForm();
	});
	
	/** Reset Form */
	function resetForm(){
		$('.ff-input').val('');
		$('#idtxtJnsAdj').val('');
		$('#idtxtItemStock').text('-');
		$('#idtxtStock').text('0');
	}
	
</script>