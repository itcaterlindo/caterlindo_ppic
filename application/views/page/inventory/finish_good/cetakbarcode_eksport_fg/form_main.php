<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$form_id = 'idForm';
?>

<?php 
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal '));
?>

<div class="form-group">
	<label for="idtxtkd_msalesorder" class="col-md-2 control-label">No Sales Order</label>
	<div class="col-md-5 col-xs-12">
		<div id="idErrkd_msalesorder"></div>
		<?php echo form_dropdown('txtkd_msalesorder', isset($opsiSO) ? $opsiSO : [], isset($SOSelected)? $SOSelected : '', array('class'=> 'form-control select2', 'id'=> 'idtxtkd_msalesorder'));?>
	</div>
	<div class="col-md-1"> 
		<button class="btn btn-success" onclick="submitData()" ><i class="fa fa-check-circle"></i> Pilih</button>
	</div>
</div>
<?php echo form_close(); ?>
