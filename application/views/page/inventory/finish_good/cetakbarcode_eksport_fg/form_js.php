<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');
	open_form_main();
	
	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function open_form_main() {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link?>/form_main',
			success:function(html){
				$('#<?php echo $box_content_id; ?>').html(html);
				render_select2('select2');
			}
		});
		moveTo('idMainContent');
	}

	function render_select2(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
		});
	}

	function detail_box(kd_msalesorder) {
		$('#idDetailBoxBarcodeEksport').remove();
		$.ajax({
			type: 'GET',
			data: {kd_msalesorder : kd_msalesorder},
			url: '<?php echo base_url().$class_link?>/detail_box',
			success:function(html){
				$('#idMainContent').append(html);
			}
		});
	}

    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitData() {
		event.preventDefault();
		var kd_msalesorder = $('#idtxtkd_msalesorder').val();
		if (kd_msalesorder == null ) {
			alert('Input Kosong');
		}else {
			detail_box(kd_msalesorder);
		}
	}
	
	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}
</script>