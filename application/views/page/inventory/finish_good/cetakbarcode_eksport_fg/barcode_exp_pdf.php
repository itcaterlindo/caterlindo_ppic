<?php

$pageLayout = array(50, 70); 
// $pdf = new Tcpdf('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf = new Tcpdf('L', 'mm', $pageLayout, true, 'UTF-8', false);
$title = 'Barcode-'.$no_po;
$pdf->SetTitle($title);
$pdf->SetAutoPageBreak(true);
$pdf->SetMargins(2, 2, 2, true);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');
// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

foreach ($resultItem as $item):
    for ($i=1; $i<=$item['item_qty']; $i++) :
    $pdf->AddPage();

    // Image example with resizing
    $pdf->Image('assets/admin_assets/dist/img/logo_ss_dark.jpg', 8, 5, 28, 8, 'JPG', '', '', false, 150, '', false, false, 0, false, false, false);

    // define barcode style
    $style = array(
        'position' => '',
        'align' => 'C',
        'stretch' => false,
        'fitwidth' => true,
        'cellfitalign' => '',
        'border' => false,
        'hpadding' => 'auto',
        'vpadding' => 'auto',
        'fgcolor' => array(0,0,0),
        'bgcolor' => false, //array(255,255,255),
        'text' => true,
        'font' => 'helveticaB',
        'fontsize' => 12,
        'stretchtext' => 14
    );
    // EAN 13
    $pdf->write1DBarcode($item['bcdexp_barcode'], 'EAN13', 10, 13, 43, 22, 2, $style, 'N');

    // Description
    // $itemcode = $item['item_code'].'/'.$item['item_qty'].'/'.$i;
    $itemcode = $item['item_code'];
    $itemdesc = $item['deskripsi_barang'];
    $itemdimen = $item['dimensi_barang'];
    $pdf->SetFillColor(255, 255, 255);
    $pdf->setCellPaddings(0, 0, 0, 0);
    $pdf->MultiCell(60, 5, $itemcode."\n".$itemdesc."\n".$itemdimen, 0, 'L', 1, 1, 8, 32, true, 0, false, true, 16, 'M', true);

    endfor;
endforeach;

$txtOutput = $title.'.pdf';
$pdf->IncludeJS("print();");
$pdf->Output($txtOutput, 'I');