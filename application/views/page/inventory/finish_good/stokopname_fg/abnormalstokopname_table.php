<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<div class="row">
	<div class="col-md-12" style="text-align:center">
		<h4> Rekap Abnormal Stok Opname <?php echo $nm_gudang?> </h4>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table style="font-size : 90%;" class="table table-bordered table-striped table-hover" border="1" id="idtable">
			<thead>
				<tr>
					<th style="width: 1%; text-align: center;">No.</th>
					<th style="width: 8%;text-align: center;">Barcode</th>
					<th style="width: 6%;text-align: center;">Prod Code</th>
					<th style="width: 15%; text-align: center;">Description</th>
					<th style="width: 12%; text-align: center;">Penyimpanan Opname</th>
					<th style="width: 12%; text-align: center;">Qty</th>
					<th style="width: 3%; text-align: center;">Status</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$no = 1;		
				foreach ($result as $r):
					?>
					<tr>
						<td style="text-align: center;"><?php echo $no; ?></td>
						<td style="text-align: left;"><?php echo $r['fgbarcode_barcode']; ?></td>
						<td style="text-align: left;"><?php echo $r['item_code']; ?></td>
						<td style="text-align: left;"><?php echo $r['fgbarcode_desc'] .' '. $r['fgbarcode_dimensi']; ?></td>
						<td style="text-align: left;"><?php echo $r['rakRuang_opname']; ?></td>
						<td style="text-align: right;"><?php echo $r['fgstopnamedetail_qty']; ?></td>
						<td style="text-align: left;"><?php echo $r['status']; ?></td>
					</tr>
					
					<?php
					$no++;
				endforeach;
				?>
				
			</tbody>
		</table>
	</div>
</div>
<hr> 

<script type="text/javascript">

render_dt('#idtable');

function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"scrollX": true,
			"scrollY": "500px",
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"title" : "Rekap Abnormal Opname"
			}],

		});
	}
</script>