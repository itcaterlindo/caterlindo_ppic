<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

// var_dump($this->session->all_userdata());
if (!empty($rowStopname)){
	extract($rowStopname);
}

/* --Masukkan setting properti untuk form-- */
$master_var = 'stokOpnameFG';
$form_id = 'idForm'.$master_var;

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
/** Variable tambahan */
?>

<div class="row">
	<div class="col-md-12">
		<div class="form-goup">
			<label for="idtxtProductCode" class="col-md-1 control-label">Buat Periode</label>
			<div class="col-md-4 col-xs-12">
				<?php echo form_dropdown('txtkd_gudang', $opsiGudang, isset($gudangSelected)? $gudangSelected : '', array('class'=> 'form-control', 'id'=> 'idtxtkd_gudang'));?>
			</div>

			<div class="col-md-4">
				<div class="input-group date">
					<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
					</div>
					<input type="text" class="form-control datetimepicker" id="idtxtTglPeriode" name="txtTglPeriode" placeholder="Tgl Periode" value="<?php echo date('Y-m-d')?>">
				</div>
			</div>
			<div class="col-md-1 scol-xs-12">
				<button name="btnSubmit" id="idbtnSubmit" onclick="action_submit_periode()" class="btn btn-sm btn-primary">
					<i class="fa fa-save"></i> Simpan
				</button>
			</div>

		</div>

	</div>
</div>

<?php echo form_close(); ?>
<hr>