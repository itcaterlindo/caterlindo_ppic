<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	$('#<?= $box_loader_id?>').hide();
	open_form_main();
	
	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_form_main() {
		$('#idFormBarangKeluarFG').slideUp().remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link?>/form_main',
			success:function(html){
				$('#<?php echo $box_content_id; ?>').slideDown().append(html);
				open_table_box($('#idtxtfgstopname_kd').val());
			}
		});
		moveTo('idMainContent');
	}

	function open_table_box(fgstopname_kd) {
		var url = '<?php echo base_url().$class_link?>/table_box';
		$.ajax({
			type: 'GET',
			data: {fgstopname_kd : fgstopname_kd},
			url: url,
			success:function(html){
				$('#idMainContent').append(html);
			}
		});
	}

	function setperiode () {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/setperiode';?>',
			success:function(html){
				toggle_modal('', html)
			}
		});
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
			// delay: 2500,
            styling: 'bootstrap3'
        });	
		box_overlay('out');
		audio_notif(type);
    }

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function audio_notif(type){
		if (type == 'success'){
			var audio = new Audio("<?php echo base_url().'/assets/admin_assets/dist/sound/success.ogg'?>");
		}else{
			var audio = new Audio("<?php echo base_url().'/assets/admin_assets/dist/sound/fail.ogg'?>");
		}
		
		audio.play();
	}
</script>