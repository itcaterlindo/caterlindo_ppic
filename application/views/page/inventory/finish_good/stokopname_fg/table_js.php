<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table('<?php echo $fgstopname_kd; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table(fgstopname_kd) {
		$('#<?php echo $box_content_id; ?>').slideDown(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_main'; ?>',
				data: {fgstopname_kd : fgstopname_kd},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
				}
			});
		});
	}

	function open_rekap_opname_box (jnsRekap, fgstopname_kd){
		$('#idFormBoxRekapStokOpname').slideUp().remove();
		$('#<?php echo $box_content_id; ?>').slideDown(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/rekap_opname_box'; ?>',
				data: {jnsRekap : jnsRekap, fgstopname_kd : fgstopname_kd},
				success: function(html) {
					$('#idMainContent').prepend(html);
					moveTo('idMainContent');
				}
			});
		});
	}

	function rekap_opname_penyimpanan() {
		open_rekap_opname_box ('penyimpanan', '<?php echo $fgstopname_kd; ?>');
	}

	function rekap_opname_stok() {
		open_rekap_opname_box ('stok', '<?php echo $fgstopname_kd; ?>');
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function hapus_data(id) {
		var conf = confirm('Apakah anda yakin menghapus item ini ?');
		if (conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete'; ?>',
				data: 'id='+id,
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, '');
						var fgstopname_kd = $('#idtxtfgstopname_kd').val();
						open_table(fgstopname_kd);
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
					}
				}
			});
		}
	}
	
</script>