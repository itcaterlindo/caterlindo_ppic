<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
    masteropname_form();
	masteropname_table();
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');
	
	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

    function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function masteropname_table() {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link?>/masteropname_table',
			success:function(html){
				$('#<?php echo $box_tablecontent_id; ?>').html(html);
			}
		});
	}

	function masteropname_form() {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link?>/masteropname_form',
			success:function(html){
				$('#<?php echo $box_content_id; ?>').html(html);
                render_datetimepicker('datetimepicker')
		        moveTo('idMainContent');
			}
		});
	}

	function action_submit_periode(){
		event.preventDefault();
		var conf = confirm('Apakah Anda yakin set periode stok opname ?');
		if (conf){
			var stopname_periode = $('#idtxtTglPeriode').val();
			var kd_gudang = $('#idtxtkd_gudang').val();
			$.ajax({
				url: "<?php echo base_url().$class_link; ?>/action_submit_periode",
				type: "GET",
				data:  { "stopname_periode" : stopname_periode, "kd_gudang" :kd_gudang },
				success: function(data){
					var resp = JSON.parse(data);
					if (resp.code == 200){
						notify(resp.status, resp.pesan, 'success');
						masteropname_table();
					}else if (resp.code == 400){
						notify(resp.status, resp.pesan, 'error');
						masteropname_table();
					}else{
						notify('Gagal', 'Input Tidak Sesuai', 'error');
					}
				} 	        
			});
		}
	}

	function action_master_opname (id){
		event.preventDefault();
		var conf = confirm('Apakah Anda yakin merubah status ?');
		if (conf){
			var sts = id.split('/');
			sts  = sts[1];
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_master_opname'; ?>',
				data: {id : id},
				success: function(data) {
					var resp = JSON.parse(data);
					console.log(resp);
					if(resp.code == 200){
						notify(resp.status, resp.pesan, 'success');
						if(sts == 'aktif' || sts == 'non_aktif'){
							$('#idtxtPeriode').val(resp.data.fgstopname_periode);
							$('#idtxtfgstopname_kd').val(resp.data.fgstopname_kd);
						}else{
							$('#idtxtPeriode').val('');
							$('#idtxtfgstopname_kd').val('');
						}
						masteropname_table();
					}else if(resp.code == 400){
						notify(resp.status, resp.pesan, 'warning');
					}else{

					}
				}
			});
		}
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
		box_overlay('out');
    }

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

    function render_datetimepicker(valClass){
		$('.'+valClass).datetimepicker({
			format: 'YYYY-MM-DD'
		});
	}

</script>