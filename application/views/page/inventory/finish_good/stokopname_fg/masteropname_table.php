<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<div class="row">
	<table id="idTable2" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
		<thead>
			<tr>
				<th style="width:1%; text-align:center;" class="all">No.</th>
				<th style="width:4%; text-align:center;" class="all">Opsi</th>
				<th style="width:5%; text-align:center;">Lokasi Gudang</th>
				<th style="width:5%; text-align:center;">Periode</th>
				<th style="width:5%; text-align:center;">Tgl Periode</th>
				<th style="width:5%; text-align:center;">Status</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$no = 1;
			foreach ($resultOpname as $eachData):
				?>
				<tr>
					<td style="text-align:center;"><?php echo $no; ?></td>
					<td style="text-align:center;">
						<div class="btn-group">
							<!-- <button type="button" class="btn btn-sm btn-info">Opsi</button> -->
							<button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								Opsi
								<span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#" onclick="action_master_opname('<?php echo $eachData['fgstopname_kd']; ?>/aktif')"><i class="fa fa-check"></i>Aktif</a></li>
								<li><a href="#" onclick="action_master_opname('<?php echo $eachData['fgstopname_kd']; ?>/non_aktif')"><i class="fa fa-stop"></i>Non Aktif</a></li>
								<li><a href="#" onclick="action_master_opname('<?php echo $eachData['fgstopname_kd']; ?>/finish')"><i class="fa fa-thumbs-up"></i>Finish</a></li>
								<li class="divider"></li>
								<li><a href="#" onclick="action_master_opname('<?php echo $eachData['fgstopname_kd']; ?>/delete')"><i class="fa fa-trash"></i>Delete</a></li>
							</ul>
						</div>
					</td>
					<td style="text-align:center;"><?php echo $eachData['nm_gudang']; ?></td>
					<td style="text-align:center;"><?php echo $eachData['fgstopname_periode']; ?></td>
					<td style="text-align:center;"><?php echo $eachData['fgstopname_tglperiode']; ?></td>
					<td style="text-align:center;">
					<?php 
						if ($eachData['fgstopname_status'] == 'finish'){
							echo buildLabel('success', $eachData['fgstopname_status']);
						}else{
							echo buildLabel('warning', $eachData['fgstopname_status']);
						}
						 ?>
					
					</td>
				</tr>
				<?php
				$no++;
			endforeach;
			?>
		</tbody>
	</table>
</div>

<script type="text/javascript">
	$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};
	var table = $('#idTable2').DataTable({
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
				"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
				"next":       '<span class="glyphicon glyphicon-forward"></span>',
				"previous":   '<span class="glyphicon glyphicon-backward"></span>'
			}
		},
	});

</script>