<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_main('<?php echo $url; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_main(url) {
		$('#<?php echo $box_content_id; ?>').slideDown(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo $url; ?>',
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					moveTo('idMainContent');
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

</script>