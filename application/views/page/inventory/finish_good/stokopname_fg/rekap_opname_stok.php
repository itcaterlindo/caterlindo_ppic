<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<div class="row">
	<div class="col-md-12" style="text-align:center">
		<h4> Rekap Stok Opname <?php echo $nm_gudang?> Berdasarkan Stok </h4>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
	<table style="font-size : 100%;" class="table table-bordered table-striped table-hover" border="1" id="idtableRekapStok">
		<thead>
			<tr>
				<th style="width: 1%; text-align: center;">No.</th>
				<th style="width: 10%;text-align: center;">Prod Code</th>
				<th style="width: 25%; text-align: center;">Deskripsi Barang</th>
				<th style="width: 3%; text-align: center;">Qty Data</th>
				<th style="width: 3%; text-align: center;">Qty Opname</th>
				<th style="width: 5%; text-align: center;">Status</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$no = 1;
			$sumQtyData = 0;
			$sumQtyOpname = 0;	
			foreach ($resultOpnameQty as $each_item):
				if ($each_item['statusQty'] == 'sama'){
					$status = '<span class="label label-success">Sama</span>';
				}else{
					$status = '<span class="label label-warning">Beda</span>';
				}
				
				?>
				<tr>
					<td style="text-align: center;"><?php echo $no; ?></td>
					<td style="text-align: left;"><?php echo $each_item['item_code']; ?></td>
					<td style="text-align: left;"><?php echo $each_item['item_desc'].' '.$each_item['item_dimensi']; ?></td>
					<td style="text-align: right;"><?php echo $each_item['qty_data']; ?></td>
					<td style="text-align: right;"><?php echo $each_item['qty_opname']; ?></td>
					<td style="text-align: center;"><?php echo $status; ?></td>
				</tr>
				
				<?php
				$no++;
				$sumQtyData += $each_item['qty_data'];
				$sumQtyOpname += $each_item['qty_opname'];
			endforeach;
			?>
		</tbody>
		<tfoot>
			<th>
				<td colspan="2" style="text-align: right; font-weight:bold;"> Total Qty </td>
				<td style="text-align: right; font-weight:bold;"><?php echo $sumQtyData; ?></td>
				<td style="text-align: right; font-weight:bold;"><?php echo $sumQtyOpname; ?></td>
				<td></td>
			</th>
		</tfoot>
	</table>
	</div>
</div>
<script type="text/javascript">

render_dt('#idtableRekapStok');

function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"scrollX": true,
			"scrollY": "500px",
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"title" : "Rekap Opname Berdasar Stok"
			}],
		});
	}
</script>