<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

// var_dump($this->session->all_userdata());
if (!empty($rowStopname)){
	extract($rowStopname);
}

/* --Masukkan setting properti untuk form-- */
$master_var = 'stokOpnameFG';
$form_id = 'idForm'.$master_var;

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
/** Variable tambahan */
echo form_input(array('type' => 'hidden', 'id' => 'idtxtfgstopname_kd', 'name' => 'txtfgstopname_kd', 'value' => isset($fgstopname_kd) ? $fgstopname_kd:null ));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtfgin_kd', 'name' => 'txtfgin_kd', 'class' => 'ff-input' ));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtfg_kd', 'name' => 'txtfg_kd', 'class' => 'ff-input' ));
?>

<style type="text/css">
	.modal-dialog {
 		width: 900px;
	}
</style>

<div class="form-group">
	<label for="idtxtLokasi" class="col-md-2 control-label">Periode Stok Opname</label>
	<div class="col-md-3 col-xs-12">
		<div id="idErrPeriode"></div>
		<?php
		echo form_input(array('type' => 'text', 'id' => 'idtxtPeriode', 'name' => 'txtPeriode', 'class'=>'form-control input-lg', 'readonly' => 'true', 'placeholder'=>'Periode Stok Opname','value' => isset($fgstopname_periode) ? $fgstopname_periode: null ));
		?>
	</div>
</div>
<div class="form-group">
	<label for="idtxtMetodeInput" class="col-md-2 control-label">Metode Input</label>
	<div class="col-md-4 col-xs-12">
		<div id="idErrMetodeInput"></div>
		<select name="txtMetodeInput" id="idtxtMetodeInput" class="form-control" readonly="readonly">
			<option value="OTOMATIS" selected>Otomatis</option>
			<!-- <option value="MANUAL">Manual</option> -->
		</select>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label" >Lokasi</label>
	<div class="col-sm-2">
		<select name="txtGudang" id="idtxtGudangLokasi" class="form-control">
		</select>
	</div>
	<div class="col-sm-2">
		<select name="txtRak" id="idtxtRakLokasi" class="form-control">
		</select>
	</div>
	<div class="col-sm-2">
		<div id="idErrRuang"></div>
		<select name="txtRuang" id="idtxtRuangLokasi" class="form-control">
		</select>
	</div>
	<div class="col-sm-2">
		<div id="idErrRuangKolom"></div>
		<select name="txtRuangKolom" id="idtxtRuangKolomLokasi" class="form-control">
		</select>
	</div>
</div>
<div class="form-group">
	<label for="idTxtBarcode" class="col-md-2 control-label">Barcode</label>
	<div class="col-md-3 col-xs-12">
		<div id="idErrBarcode"></div>
		<?php echo form_input(array('name' => 'txtBarcode', 'id' => 'idTxtBarcode', 'class' => 'form-control input-lg ff-input', 'placeholder' => 'Barcode', 'value' => '', 'maxlength'=>'19', 'autocomplete' => 'off')); ?>
	</div>
	<label for="idTxtBarcode" class="col-md-1 control-label">Qty</label>
	<div class="col-md-1 col-xs-12">
		<div id="idErrQty"></div>
		<?php echo form_input(array('type'=> 'number', 'name' => 'txtQty', 'id' => 'idtxtQty', 'class' => 'form-control ff-input', 'placeholder' => 'Qty')); ?>
	</div>
</div>
<div class="form-group">
	<label for="idtxtDimension" class="col-md-2 control-label">Keterangan</label>
	<div class="col-md-3 col-xs-12">
		<div id="idErrKet"></div>
			<?php echo form_textarea(array('name' => 'txtKet', 'id' => 'idtxtKet', 'rows'=>'2', 'class' => 'form-control ff-input', 'placeholder' => 'Keterangan')); ?>
		</div>
	</div>
</div> 

<hr>
<?php echo form_close(); ?>
<div class="form-group">
	<div class="col-md-4 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" id="idbtnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
		<button name="btnSubmit" id="idbtnSubmit" onclick=submitData() class="btn btn-primary btn-flat">
			<i class="fa fa-save"></i> Submit
		</button>
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function() {
		$(window).keydown(function(event){
			if(event.keyCode == 13) {
				event.preventDefault();
				return false;
			}
		});

		/** set awal gudang */
		get_warehouse_lokasi('<?php echo $kd_gudang; ?>');
	});

	$('#idTxtBarcode').keyup(function (e) {
		var code = e.keyCode || e.which;
		var val = $(this).val();
		if (code == 13) {
			if(val.length == 19 ){
				var metode = $('#idtxtMetodeInput').val();
				if (metode == 'OTOMATIS'){
					submitData();
				}else if (metode == 'MANUAL'){
					$('#idtxtQty').focus();
					box_overlay('out');
				}
			}else{
				$('#idTxtBarcode').val('');
				notify('Gagal', 'Input Tidak Sesuai', 'error');
			}
		}
		e.preventDefault();
		return false;
	});

	// function get_by_barcode(metode, val){
	// 	box_overlay('in');
	// 	$.ajax({
	// 		url: "<?php //echo base_url().$class_link; ?>/get_barcode_from_stock",
	// 		type: "GET",
	// 		data:  "id="+val,
	// 		contentType: false,
	// 		cache: false,
	// 		processData:false,
	// 		success: function(data){
	// 			var resp = JSON.parse(data);
	// 			console.log(resp);
	// 			if (resp.code == 200){
	// 				$('#idtxtfg_kd').val(resp.data.fg_kd);
	// 				$('#idtxtfgin_kd').val(resp.data.fgin_kd);
	// 				$('#idtxtQty').val(resp.data.fgin_qty);
	// 				$('#idtxtKet').val('');
					
	// 				if (metode == 'OTOMATIS'){
	// 					submitData();
	// 				}else if (metode == 'MANUAL'){
	// 					$('#idtxtQty').focus();
	// 					box_overlay('out');
	// 				}
	// 			}else if (resp.code == 400){
	// 				notify(resp.status, resp.pesan, 'error');
	// 				$('#idTxtBarcode').val('');
	// 			}
	// 		} 	        
	// 	});
			
	// 	event.preventDefault();
	// 	return false;
	// }

	function setOpname (){
		event.preventDefault();
		open_table_master();
		first_set_detepicker();
		$('#idmodal').modal('toggle');
	}	

	function first_set_detepicker(){
		$('.datepicker').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	}

	function open_table_master(){
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/table_master_opname",
			type: "GET",
			success: function(html){
				$('#idContent').html(html);
			} 	        
		});
	}

	/** Submit dari qty press enter */
	$('#idtxtQty').keyup(function(e){
		e.preventDefault();
		var code = e.keyCode || e.which;
		if (code == 13){
			submitData();
		}
	});

	function get_warehouse_lokasi(selected){
		var optGdg = $('#idtxtGudangLokasi');
		$('#idtxtGudangLokasi option').remove();
		optGdg.append($('<option></option>').val('').html('--Pilih Warehouse--'));
		$.ajax({
			type: "GET",
			url: "<?php echo base_url()?>/inventory/finish_good/barangrak_fg/get_gudang",
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optGdg.append($('<option></option>').val(text.kd_gudang).html(text.nm_gudang));
					});
				}
				$('#idtxtGudangLokasi').val(selected);  /** Untuk selected */
			} 	        
		});
		get_rak_lokasi(selected, '');
	}

	function get_rak_lokasi(valGdg, selected){
		var optRak = $('#idtxtRakLokasi');
		$('#idtxtRakLokasi option').remove();
		optRak.append($('<option></option>').val('').html('--Pilih Rak--'));
		$.ajax({
			url: "<?php echo base_url()?>/inventory/finish_good/barangrak_fg/get_rak",
			type: "GET",
			data: 'gudang_kd='+valGdg,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optRak.append($('<option></option>').val(text.kd_rak).html(text.nm_rak));
					});
				}
				$('#idtxtRakLokasi').val(selected);
			} 	        
		});
	}

	function get_ruang_lokasi(valRak, selected){
		var optRuang = $('#idtxtRuangLokasi');
		$('#idtxtRuangLokasi option').remove();
		optRuang.append($('<option></option>').val('').html('--Pilih Ruang--'));
		$.ajax({
			url: "<?php echo base_url()?>/inventory/finish_good/barangrak_fg/get_ruang",
			type: "GET",
			data: 'rak_kd='+valRak,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optRuang.append($('<option></option>').val(text.kd_rak_ruang).html(text.nm_rak_ruang));
					});
				}
				$('#idtxtRuangLokasi').val(selected);
			} 	        
		});
	}

	function get_ruangKolom_lokasi(valRuang, selected){
		var optRuang = $('#idtxtRuangKolomLokasi');
		$('#idtxtRuangKolomLokasi option').remove();
		optRuang.append($('<option></option>').val('').html('--Pilih Ruang Kolom--'));
		$.ajax({
			url: "<?php echo base_url()?>/inventory/finish_good/barangrak_fg/get_ruang_kolom",
			type: "GET",
			data: 'kd_rak_ruang='+valRuang,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if (resp.code == 200){
					$.each( resp.data, function(val, text) {
						optRuang.append($('<option></option>').val(text.rakruangkolom_kd).html(text.rakruangkolom_nama));
					});
				}
				$('#idtxtRuangKolomLokasi').val(selected);
			} 	        
		});
	}

	$('#idtxtGudangLokasi').on('change', function () {
		$('#idtxtRuangLokasi option').remove();
		get_rak_lokasi(this.value, '');
	});

	$('#idtxtRakLokasi').on('change', function () {
		get_ruang_lokasi(this.value, '');
	});

	$('#idtxtRuangLokasi').on('change', function () {
		get_ruangKolom_lokasi(this.value, '');
	});

	/** Regenerate token */
    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function submitData() {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById('<?php echo $form_id; ?>');
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_insert",
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					generateToken (resp.csrf);
					resetForm();
					var fgstopname_kd = $('#idtxtfgstopname_kd').val();
					open_table(fgstopname_kd);
					box_overlay('out');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					resetForm();
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					resetForm();
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					resetForm();
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	$('#idbtnReset').click(function(){
		resetForm();
	});
	
	/** Reset Form */
	function resetForm(){
		$('.ff-input').val('');
	}
	
</script>