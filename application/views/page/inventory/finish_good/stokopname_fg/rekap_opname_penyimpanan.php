<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<div class="row">
	<div class="col-md-12" style="text-align:center">
		<h4> Rekap Stok Opname <?php echo $nm_gudang?> Berdasarkan Penyimpanan </h4>
	</div>
</div>
<!-- <hr> -->
<div class="row">
	<div class="col-md-12">
		<table style="font-size : 90%;" class="table table-bordered table-striped table-hover" border="1" id="idtable">
			<thead>
				<tr>
					<th style="width: 1%; text-align: center;">No.</th>
					<th style="width: 8%;text-align: center;">Barcode</th>
					<th style="width: 6%;text-align: center;">Prod Code</th>
					<th style="width: 15%; text-align: center;">Description</th>
					<th style="width: 12%; text-align: center;">Penyimpanan Data</th>
					<th style="width: 12%; text-align: center;">Penyimpanan Opname</th>
					<th style="width: 12%; text-align: center;">Keterangan</th>
					<th style="width: 3%; text-align: center;">Status</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$no = 1;		
				foreach ($item_opname as $each_item):
					if ($each_item['statusRuang'] == 'sama'){
						$statusRuang = '<span class="label label-success">'.$each_item['statusRuang'].'</span>';
					}else{
						$statusRuang = '<span class="label label-warning">'.$each_item['statusRuang'].'</span>';
					}
					
					?>
					<tr>
						<td style="text-align: center;"><?php echo $no; ?></td>
						<td style="text-align: left;"><?php echo $each_item['fgbarcode_barcode']; ?></td>
						<td style="text-align: left;"><?php echo $each_item['item_code']; ?></td>
						<td style="text-align: left;"><?php echo $each_item['fgbarcode_desc'] .' '. $each_item['fgbarcode_dimensi']; ?></td>
						<td style="text-align: left; display"><?php echo $each_item['rakRuang_data']; ?></td>
						<td style="text-align: left;"><?php echo $each_item['rakRuang_opname']; ?></td>
						<td style="text-align: left;"><?php echo $each_item['opnameKeterangan']; ?></td>
						<td style="text-align: center;"><?php echo $statusRuang; ?></td>
					</tr>
					
					<?php
					$no++;
				endforeach;
				?>
				
			</tbody>
		</table>
	</div>
</div>
<hr> 

<script type="text/javascript">

render_dt('#idtable');

function render_dt(table_elem) {
		$(table_elem).DataTable({
			"dom": 'Bfrtip',
			"scrollX": true,
			"scrollY": "500px",
			"paging": false,
			"language" : {
				"lengthMenu" : "Tampilkan _MENU_ data",
				"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
				"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
				"infoFiltered": "(difilter dari _MAX_ total data)",
				"infoEmpty" : "Tidak ada data yang ditampilkan",
				"search" : "Cari :",
				"loadingRecords": "Memuat Data...",
				"processing":     "Sedang Memproses...",
				"paginate": {
					"first":      '<span class="glyphicon glyphicon-fast-backward"></span>',
					"last":       '<span class="glyphicon glyphicon-fast-forward"></span>',
					"next":       '<span class="glyphicon glyphicon-forward"></span>',
					"previous":   '<span class="glyphicon glyphicon-backward"></span>'
				}
			},
			"buttons" : [{
				"extend" : "excel",
				"title" : "Rekap Opname Berdasar Penyimpanan"
			}],

		});
	}
</script>