<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	$('#<?= $box_loader_id ?>').hide();

	$(document).ready(function() {
    	open_form_main();
		open_table_box();
	});
	
	$(document).off('ifClicked', '#idcheckall').on('ifClicked', '#idcheckall', function() {
		if (this.checked) {
			$('.tt-check').iCheck('uncheck');
		} else {
			$('.tt-check').iCheck('check');
		}
	});

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function open_form_main() {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/form_main'; ?>',
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
				$('.select2').select2({
					theme: 'bootstrap',
					placeholder: '--Pilih Opsi--',
				});
				render_DN();
				moveTo('idMainContent');
			}
		});
	}

	function open_table_box() {
		$.ajax({
			url: "<?php echo base_url() . $class_link; ?>/table_box",
			type: "GET",
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				$('#idMainContent').append(data);
			}
		});
	}

	function cariDnrecaived() {
		let id = $('#idtxtdn_kd').val();
		tabledn_main(id);
	}

	function tabledn_main(id) {
		event.preventDefault();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link . '/tabledn_main'; ?>',
			data: {
				id: id
			},
			success: function(html) {
				$('#idtabledn').html(html);
				$('input[type="checkbox"].icheck').iCheck({
					checkboxClass: 'icheckbox_square-blue'
				});
			}
		});
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			// delay: 2500,
			styling: 'bootstrap3'
		});
		box_overlay('out');
	}

	function box_overlay(sts) {
		if (sts == 'in') {
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		} else if (sts == 'out') {
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

	function submitGenerateBarcodeDn(form_id) {
		event.preventDefault();
		var form = document.getElementById(form_id);

		$.ajax({
			url: "<?php echo base_url() . $class_link; ?>/action_generatebarcode_dn",
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function() {
				$('#idbtnsubmitbatch').attr('disabled', true);
			},
			success: function(data) {
				console.log(data);
				var resp = JSON.parse(data);
				if (resp.code == 200) {
					let id = $('#idtxtdn_kd').val();
					tabledn_main(id);
					notify(resp.status, resp.pesan, 'success');
					generateToken(resp.csrf);
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
			},
			complete: function() {
				$('#idbtnsubmitbatch').removeAttr('disabled');
			}
		});
	}

	function render_DN()
	{
		$("#idtxtKdDN").select2({
			width: 'auto',
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: { 
				url: '<?= base_url() ?>/Auto_complete/get_delivery_note_packing',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramDN: params.term, // search term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}
	
</script>