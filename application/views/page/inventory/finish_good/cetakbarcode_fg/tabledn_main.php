<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<?php 
echo form_open_multipart('', array('id' => 'idtablednreceived', 'class' => 'form-horizontal')); 
echo form_input(array('type' => 'hidden', 'id' => 'idtxtdn_kd', 'name' => 'txtdn_kd', 'value' => $id));
?>
<div class="row">
	<div class="col-md-12">
		<table id="idTableDnreceived" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;	font-size: 95%;">
			<thead>
				<tr>
					<th style="width:1%; text-align:center;">No.</th>
					<th style="width:4%; text-align:center;"> <input type="checkbox" class="icheck" id="idcheckall" checked> </th>
					<th style="width:5%; text-align:center;">Batch</th>
					<th style="width:7%; text-align:left;">No Wo</th>
					<th style="width:7%; text-align:left;">Item Code</th>
					<th style="width:15%; text-align:left;">Deskripsi</th>
					<th style="width:15%; text-align:left;">Dimensi</th>
					<th style="width:5%; text-align:left;">Qty</th>
					<th style="width:7%; text-align:left;">Tgl Barcode</th>
				</tr>
			</thead>
            <tbody>
                <?php
                $no =1; 
				foreach($result as $r) : 
					$val = 'checked';
					$ttclass = '';
					if (!empty($r['dndetailreceived_tglbarcodefg'])){ 
						$val = 'disabled'; 
						$ttclass = 'disabled';
					}
				?>
                <tr>
                    <td><?php echo $no; ?></td>
                    <td style="text-align:center;"> 
						<input type="checkbox" class="icheck tt-check<?php echo $ttclass; ?>" name="txtdndetailreceived_kd[]" value="<?php echo $r['dndetailreceived_kd'];?>" <?php echo $val;?>> 
					</td>
                    <td><?php echo $r['dndetailreceived_tanggal']?></td>
                    <td><?php echo $r['woitem_no_wo']?></td>
                    <td><?php echo $r['woitemso_itemcode']?></td>
                    <td><?php echo $r['woitemso_itemdeskripsi']?></td>
                    <td><?php echo $r['woitemso_itemdimensi']?></td>
                    <td class="dt-right"><?php echo $r['dndetailreceived_qty']?></td>
                    <td><?php echo $r['dndetailreceived_tglbarcodefg']?></td>
                </tr>
                <?php 
                $no++;
                endforeach;?>
            </tbody>
		</table>
		<button class="btn btn-sm btn-primary pull-right" id="idbtnsubmitbatch" onclick="submitGenerateBarcodeDn('idtablednreceived')"> <i class="fa fa-save"></i> Simpan </button>
	</div>
</div>
<?php echo form_close(); ?>
<script type="text/javascript">
	
</script>