<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'CetakBarcodeFG';
$form_id = 'idForm'.$master_var;
$formdn_id = 'idFormdn'.$master_var;
?>

<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tab_1" data-toggle="tab">Delivery Note</a></li>
		<li><a href="#tab_2" data-toggle="tab">Barcode</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1">
		<!------------------------------>
		<?php
			echo form_open_multipart('', array('id' => $formdn_id, 'class' => 'form-horizontal'));
		?>
			<div class="form-group">
				<label for="idtxtTglProduksi" class="col-md-1 control-label">No DN</label>
				<div class="col-md-4 col-xs-12">
					<div id="idErrShift"></div>
					<?php echo form_dropdown('txtdn_kd', isset($opsiDN) ? $opsiDN : null, null , array('id'=> 'idtxtdn_kd','class'=>'form-control select2')); ?>
				</div>
				<div class="col-md-1 col-xs-12">
					<button class="btn btn-sm btn-default" onclick="cariDnrecaived()"> <i class="fa fa-search"></i> Cari </button>
				</div>
			</div>
		<?php echo form_close(); ?>
		<div id="idtabledn"></div>
		<!------------------------------>
		</div>
		<!-- /.tab-pane -->
		<div class="tab-pane" id="tab_2">

		<!------------------------------>
			<?php
			echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
			echo form_input(array('type' => 'hidden', 'id' => 'idtxtKdBarang', 'name' => 'txtKdBarang', 'value' => ''));
			?>
			<div class="form-group">
				<label for="idtxtTglProduksi" class="col-md-2 control-label">Tanggal Produksi</label>
				<div class="col-md-2 col-xs-12">
					<div id="idErrTglProduksi"></div>
					<?php
					echo form_input(array('type' => 'text', 'id'=> 'idtxtTglProduksi','name' => 'txtTglProduksi' ,'class' => 'form-control datetimepicker', 'placeholder'=> 'dd-mm-yyyy', 'value' => date('d-m-Y',strtotime("-1 days")) ));
					?>
				</div>
				<div class="col-md-2 col-xs-12">
					<div id="idErrShift"></div>
					<?php
					echo form_dropdown('txtShift', array('1' => 'Shift 1', '2' => 'Shift 2'), '' ,array('id'=> 'idtxtShift','class'=>'form-control'));
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="idtxtBatch" class="col-md-2 control-label">No Batch</label>
				<div class="col-md-2 col-xs-12">
					<div id="idErrNoBatch"></div>
					<?php
					echo form_input(array('type' => 'text', 'id' => 'idtxtBatch', 'name' => 'txtBatch', 'class'=>'form-control', 'readonly' => 'true', 'placeholder'=>'No Batch','value' => ''));
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="idTxtKdDN" class="col-md-2 control-label">No. DN</label>
				<div class="col-md-2 col-xs-12">
					<div id="idErrKdDN"></div>
					<?php echo form_dropdown('txtKdDN', array('' => '--Pilih Opsi--'), '' ,array('id'=> 'idtxtKdDN','class'=>'form-control', 'data-allow-clear' => true)); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="idTxtItemCode" class="col-md-2 control-label">Status</label>
				<div class="col-md-3 col-xs-12">
					<div id="idErrStatus"></div>
					<?php
					echo form_dropdown('txtStatus', array('std' => 'Standart', 'custom' => 'Custom'), 'std' ,array('id'=>'idtxtStatus', 'class'=>'form-control'));
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="idtxtProductCode" class="col-md-2 control-label">Product Code</label>
				<div class="col-md-3 col-xs-12">
					<div id="idErrProductCode"></div>
					<?php
					echo form_input(array('type' => 'text', 'id' => 'idtxtProductCode','name' => 'txtProductCode', 'class'=>'form-control', 'placeholder'=> 'Product Code','value' => ''));
					?>
				</div>
				<label for="idtxtQtyItem" class="col-md-1 control-label">Qty Item</label>
				<div class="col-md-1 col-xs-12">
					<div id="idErrQtyItem"></div>
					<?php
					echo form_input(array('type' => 'number', 'id' => 'idtxtQtyItem','name' => 'txtQtyItem', 'class'=>'form-control', 'placeholder'=> 'Qty','value' => ''));
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="idTxtBarcode" class="col-md-2 control-label">Barcode</label>
				<div class="col-md-3 col-xs-12">
					<div id="idErrBarcode"></div>
					<?php echo form_input(array('name' => 'txtBarcode', 'id' => 'idTxtBarcode', 'class' => 'form-control', 'placeholder' => 'Barcode', 'readonly'=> 'true','value' => '')); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="idtxtDescription" class="col-md-2 control-label">Description</label>
				<div class="col-md-4 col-xs-12">
				<div id="idErrDescription"></div>
					<?php echo form_textarea(array('name' => 'txtDescription', 'id' => 'idTxtDescription', 'rows'=>'2', 'class' => 'form-control', 'placeholder' => 'Description', 'readonly'=>'true')); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="idtxtDimension" class="col-md-2 control-label">Dimension (LxDxH) (mm)</label>
				<div class="col-md-4 col-xs-12">
				<div id="idErrDimension"></div>
					<?php echo form_textarea(array('name' => 'txtDimension', 'id' => 'idTxtDimension', 'rows'=>'2', 'class' => 'form-control', 'placeholder' => 'Description', 'readonly'=>'true')); ?>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-4 col-sm-offset-2 col-xs-12">
					<button type="reset" name="btnReset" id="idbtnReset" class="btn btn-default btn-flat">
						<i class="fa fa-refresh"></i> Reset
					</button>
					<button type="submit" name="btnSubmit" id="idbtnSubmit" class="btn btn-primary btn-flat">
						<i class="fa fa-save"></i> Submit
					</button>
				</div>
			</div>
			<?php echo form_close(); ?>
		<!------------------------------>

		</div>
	</div>
	<!-- /.tab-content -->
</div>
<!-- nav-tabs-custom -->

<script type="text/javascript">
	$(document).ready(function(){
		 // datepicker
		$('.datetimepicker').datetimepicker({
			format: 'DD-MM-YYYY',
    	});

		/* --start product typeahead.js-- */
		var Product = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			url: '<?php echo base_url('auto_complete/get_barang'); ?>',
			remote: {
				url: "<?php echo base_url().$class_link; ?>/get_barang",
				prepare: function (query, settings) {
					settings.type = 'GET';
					settings.contentType = 'application/json; charset=UTF-8';
					settings.data = 'item_code='+query+'&dn_kd='+$('#idtxtKdDN').val();

					return settings;
				},
				wildcard: '%QUERY'
			}
		});

		$('#idtxtProductCode').typeahead(null, {
			limit: 50,
			minLength: 1,
			name: 'product_search',
			display: 'item_code',
			valueKey: 'get_product',
			source: Product.ttAdapter()
		});

		$('#idtxtProductCode').bind('typeahead:select', function(obj, selected) {
			$('#idtxtKdBarang').val(selected.kd_barang);
			$('#idTxtBarcode').val(selected.item_barcode);
			$('#idTxtDescription').val(selected.deskripsi_barang);
			$('#idTxtDimension').val(selected.dimensi_barang);
			$('#idtxtQtyItem').val('1');
			$('#idtxtQtyItem').focus();
		});
		/* --end of product typeahead.js-- */

		/** Set no batch */
		no_batch($('#idtxtTglProduksi').val());

	});


	/** Rubah tanggal produksi */
	$('#idtxtTglProduksi').on('dp.change', function(e){ 
		no_batch($(this).val());
        /** Open table */
        // open_table_main($(this).val());
	});

	/** Rubah standart /custom */
	$('#idtxtStatus').change(function(){
		item_status ($(this).val())
	});

	function item_status (sts) {
		if(sts == 'custom'){
			$('#idtxtKdBarang').val('PRD020817000573');
			$('#idtxtProductCode').val('CUSTOM ITEM');
			$('#idtxtProductCode').attr('readonly', true);
			$('#idTxtBarcode').val('899000');
			$('#idTxtDescription').attr('readonly', false);
			$('#idTxtDimension').attr('readonly', false);
            $('#idtxtStatus').val(sts);
			$('#idtxtQtyItem').val('1');
			$('#idtxtQtyItem').focus();
		}else {
			$('#idtxtProductCode').val('');
			$('#idtxtProductCode').attr('readonly', false);
			$('#idTxtBarcode').val('');
			$('#idTxtDescription').val('');
			$('#idTxtDimension').val('');
			$('#idTxtDescription').attr('readonly', true);
			$('#idTxtDimension').attr('readonly', true);
            $('#idtxtStatus').val(sts);
		}
	}

	function no_batch(date) {
		var tglProduksi = date;
		var tgl = tglProduksi.substring(0,2);
		var bln = tglProduksi.substring(3,5);
		var tahun = tglProduksi.substring(8,10);
		var batch = tahun.concat(bln, tgl);
		$('#idtxtBatch').val(batch);		
	}

    function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
    }

	$(document).on('submit', '#<?php echo $form_id; ?>', function(e) {
		box_overlay('in');		
		$('#idbtnSubmit').attr( 'disabled',true);
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().$class_link; ?>/action_insert",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					$('#idErrProductCode').html('');
					notify (resp.status, resp.pesan, 'success');
					generateToken (resp.csrf);
					item_status ('std')
					$('#idtxtQtyItem').val('');
					open_table($('#idtxtTglProduksi').val());
					$('#idbtnSubmit').attr( 'disabled',false);
					box_overlay('out');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					$('#idbtnSubmit').attr( 'disabled',false);
					box_overlay('out');
				}else if (rsp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					$('#idbtnSubmit').attr( 'disabled',false);
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					$('#idbtnSubmit').attr( 'disabled',false);
					generateToken (resp.csrf);
				}
			} 	        
		});		
		
		return false;
	});
	
</script>