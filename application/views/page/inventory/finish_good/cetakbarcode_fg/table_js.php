<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<!-- <script src="<?php //echo base_url();?>/assets/admin_assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script> -->
<script type="text/javascript">
	$(document).ready(function(){
        $('#idtxttglBatchBoxtable').datetimepicker({
            format: 'DD-MM-YYYY',
        });
	});

	open_table('<?php echo $tglprod?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function set_tglprodBoxtbl(tglprod){
		$('#idtxttglBatchBoxtable').val(tglprod);
	}

	$('#idBtnTampil').click(function(){
		var tglprod = $('#idtxttglBatchBoxtable').val();
		open_table(tglprod);
	});

	function open_table(tglprod) {
		set_tglprodBoxtbl(tglprod);
		$('#<?php echo $box_content_id; ?>').slideDown(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/table_main?tglprod='; ?>'+tglprod,
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function cetak_barcode(id){
		var url = '<?php echo base_url().$class_link?>/cetak_barcode/'+id;
		window.open(url);
	}

	function cetakBarcodeBatch(){
		var tglCetak = $('#idtxttglBatchBoxtable').val();
		var url = '<?php echo base_url().$class_link?>/cetak_barcode_date/'+tglCetak;
		window.open(url);
	}

	function del_data (id) {
		var conf = confirm('Apakah anda yakin menghapus item ini ?');
		if (conf) {
			$.ajax({
				url: "<?php echo base_url().$class_link; ?>/action_delete",
				type: "GET",
				data:  "id="+id,
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){
					var resp = JSON.parse(data);
					console.log(resp);
					if(resp.code == 200){
						$('#idErrProductCode').html('');
						notify (resp.status, resp.pesan, '');
						open_table($('#idtxttglBatchBoxtable').val());
					}else{
						notify (resp.status, resp.pesan, 'error');
						open_table($('#idtxttglBatchBoxtable').val());
					}
				} 	        
			});
		}
	}

</script>