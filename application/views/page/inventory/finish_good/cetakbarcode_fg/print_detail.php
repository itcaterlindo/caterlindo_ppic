<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	body {
		/* font-size: 12px; */
  		margin:2mm 0mm 0mm 5mm;
	}
	/* .table>tbody>tr>td {
		padding:1px;
	}
	.table>tfoot>tr>td {
		padding:1px;
	} */
	/* @media print {
		body {transform: scale(80%);}
		.print {
			page-break-after: always;
		}
		.print-foot {
			page-break-inside: avoid;
		}
		.div-footer {
			position: fixed;
			bottom: 0;
		}
		.print-left {
			text-align: left;
		}
		.print-center {
			text-align: center;
		}
		.print-right {
			text-align: right;
		}
	} */
    /* #pageFooter {
        display: table-footer-group;
    }

    #pageFooter::before {
        counter-increment: page;
        content: counter(page);
    } */
</style>
<div class="row no-print">
	<div class="col-xs-12">
		<a href="javascript:void(0);" class="btn btn-danger no-print" onclick="window.close();" style="margin-left: 25px;">
			<i class="fa fa-ban"></i> Close
		</a>
		<a href="javascript:void(0);" class="btn btn-primary no-print" onclick="window.print();" style="margin-left: 25px;">
			<i class="fa fa-print"></i> Cetak Data
		</a>
	</div>
</div>
<div class="row invoice print">
	<?php 
	if ($source == 'date'){
		$this->load->view('page/'.$class_link.'/detail_date');
	}else{
		$this->load->view('page/'.$class_link.'/detail'); 
	}
	?>
</div>	