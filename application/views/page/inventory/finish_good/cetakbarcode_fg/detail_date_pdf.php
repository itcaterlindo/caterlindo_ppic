<?php

$pageLayout = array(30, 70); 
// $pdf = new Tcpdf('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf = new Tcpdf('L', 'mm', $pageLayout, true, 'UTF-8', false);
$title = 'QRcode-'.$date;
$pdf->SetTitle($title);
$pdf->SetAutoPageBreak(true);
$pdf->SetMargins(2, 2, 2, true);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');
// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

foreach ($rowData as $row):
    $pdf->AddPage();

    $txtQrcode = $row->fgbarcode_barcode;
    // Number Barcode
    $pdf->SetFillColor(255, 255, 255);
    $pdf->setCellPaddings(0, 0, 0, 0);
    $pdf->MultiCell(32, 5, $txtQrcode."\n", 1, 'J', 1, 1, 29, 7, true, 0, false, true, 6, 'M', true);

    // Description
    $itemcode = $row->item_code;
    $itemdesc = $row->fgbarcode_desc;
    $itemdimen = $row->fgbarcode_dimensi;
    $pdf->SetFillColor(255, 255, 255);
    $pdf->setCellPaddings(0, 0, 0, 0);
    $pdf->MultiCell(32, 5, $itemcode."\n".$itemdesc."\n".$itemdimen, 1, 'J', 1, 1, 29, 12, true, 0, false, true, 16, 'M', true);

    // set style for barcode
    $style = array(
        'border' => 1,
        'vpadding' => 1,
        'hpadding' => 1,
        'fgcolor' => array(0,0,0),
        'bgcolor' => false, //array(255,255,255)
        'module_width' => 1, // width of a single module in points
        'module_height' => 1 //   height of a single module in points
    );
    // QRCODE,H : QR-CODE Best error correction
    $pdf->write2DBarcode($txtQrcode, 'QRCODE,H', 8, 7, 21, 21, $style, 'N');
endforeach;

$txtOutput = $title.'.pdf';
$pdf->IncludeJS("print();");
$pdf->Output($txtOutput, 'I');