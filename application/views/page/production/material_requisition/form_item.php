<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$form_id = 'idFormInput';

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtmaterialreqdetail_kd', 'name'=> 'txtmaterialreqdetail_kd', 'placeholder' => 'idtxtmaterialreqdetail_kd' ,'class' => 'tt-input', 'value' => isset($materialreqdetail_kd) ? $materialreqdetail_kd: null));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtmaterialreq_kd', 'name'=> 'txtmaterialreq_kd', 'placeholder' => 'idtxtmaterialreq_kd', 'value' => isset($materialreq_kd) ? $materialreq_kd: null));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtwoitemdetailSlug', 'name'=> 'txtwoitemdetailSlug', 'placeholder' => 'idtxtwoitemdetailSlug' , 'value' => isset($slug) ? $slug: null));
?>

<div class="row">
	<div class="form-group">
		<label for='idtxtidtxtplanningweeklydetail_kd' class="col-md-2 control-label">Nomor WO</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErridtxtplanningweeklydetail_kd"></div>
			<input type="hidden" name="txtwoitem_kd" id="idtxtwoitem_kd">
			<select id="idtxtplanningweeklydetail_kd" name="txtplanningweeklydetail_kd" class="form-control"> </select>
		</div>
	</div>	
	<div class="form-group">
		<label for='idtxtplanningweekly_no' class="col-md-2 control-label">No WO Planning</label>
		<div class="col-sm-3 col-xs-12">
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtplanningweekly_no', 'id'=> 'idtxtplanningweekly_no', 'placeholder' =>'Product Code', 'readonly' => 'readonly','value'=> isset($planningweekly_no) ? $planningweekly_no: null ));?>
		</div>
	</div>	
	<div class="form-group">
		<label for='idtxtwoitem_itemcode' class="col-md-2 control-label">Nama Part</label>
		<div class="col-sm-3 col-xs-12">
			<?php echo form_input(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtwoitem_itemcode', 'id'=> 'idtxtwoitem_itemcode', 'placeholder' =>'Product Code', 'readonly' => 'readonly','value'=> isset($woitem_itemcode) ? $woitem_itemcode: null ));?>
		</div>
		<div class="col-sm-1">
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control tt-input', 'name'=> 'txtqty_max', 'id'=> 'idtxtqty_max', 'placeholder' =>'Qty', 'readonly' => 'readonly' ));?>
		</div>
	</div>
	<div class="form-group">
		<label for='idtxtwoitem_deskripsi' class="col-md-2 control-label">Deskripsi</label>
		<div class="col-sm-3 col-xs-12">
			<textarea name="txtwoitem_deskripsi" class="form-control" id="idtxtwoitem_deskripsi" cols="30" rows="2" readonly="readonly"></textarea>
		</div>
	</div>	
	<div class="form-group">
		<label for='idtxtmaterialreqdetail_qty' class="col-md-2 control-label">Qty</label>
		<div class="col-sm-3 col-xs-12">
			<div class="errInput" id="idErrmaterialreqdetail_qty"></div>
			<?php echo form_input(array('type'=>'number', 'class'=> 'form-control tt-input', 'name'=> 'txtmaterialreqdetail_qty', 'id'=> 'idtxtmaterialreqdetail_qty', 'placeholder' =>'Qty', 'value'=> isset($materialreqdetail_qty) ? $materialreqdetail_qty: null ));?>
		</div>	
	</div>	
	<div class="form-group">
		<label for="idtxtmaterialreqdetail_remark" class="col-md-2 control-label">Remark</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrmaterialreqdetail_remark"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control tt-input', 'name'=> 'txtmaterialreqdetail_remark', 'id'=> 'idtxtmaterialreqdetail_remark', 'placeholder' =>'Remark', 'rows' => '2', 'value'=> isset($materialreqdetail_remark) ? $materialreqdetail_remark: null ));?>
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group">
		<div class="col-sm-4 col-sm-offset-2 col-xs-12">
			<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData()" class="btn btn-sm btn-primary">
				<i class="fa fa-save"></i> Simpan
			</button>
			<button type="reset" name="btnReset" onclick="resetFormMain()" class="btn btn-default btn-sm">
				<i class="fa fa-refresh"></i> Reset
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); ?>