<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$form_id = 'idFormBatch';
?>
<style type="text/css">
    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }
</style>
<?php
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
?>
<hr>
<button class="btn btn-sm btn-primary" onclick="submitDataBatch('<?php echo $form_id; ?>')"> <i class="fa fa-save"></i> Simpan</button>
<button class="btn btn-sm btn-success" onclick="event.preventDefault();window.location.reload()"> <i class="fa fa-check"></i> Selesai </button>
<hr>
<div id="idErrmaterialreq_kd"></div>
<input type="hidden" name="txtmaterialreq_kd" id="idtxtmaterialreq_kd" value="<?php echo $id; ?>">
<table id="idtablemainBatch" class="table table-bordered table-striped table-hover display" style="font-size: 90%;">
    <thead>
        <tr>
            <th width="2%" style="text-align: center; font-weight:bold;">
                <input type="checkbox" id="idcheckall">
            </th>
            <th width="5%" style="text-align: center; font-weight:bold;">PlanningWeekly No</th>
            <th width="10%" style="text-align: center; font-weight:bold;">WO No</th>
            <th width="15%" style="text-align: center; font-weight:bold;">WO Item Code</th>
            <th width="20%" style="text-align: center; font-weight:bold;">WO Deskripsi</th>
            <th width="5%" style="text-align: center; font-weight:bold;">Qty PlanningWeekly</th>
            <th width="7%" style="text-align: center; font-weight:bold;">Qty</th>
            <th width="7%" style="text-align: center; font-weight:bold;">Qty Sum Requisition</th>
            <th width="10%" style="text-align: center; font-weight:bold;">Remark</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($results as $r) :
            $qtyBalanced = $r['planningweeklydetail_qty'] - $r['sum_materialreqdetail_qty'];
            if (empty($qtyBalanced)){
                continue;
            }
        ?>
            <tr>
                <td><input type="checkbox" class="classCheck" name="txtplanningweeklydetail_kds[]" value="<?php echo $r['planningweeklydetail_kd']; ?>"></td>
                <td><?php echo $r['planningweekly_no']; ?></td>
                <td><?php echo $r['woitem_no_wo']; ?></td>
                <td><?php echo $r['woitem_itemcode']; ?></td>
                <td style="text-align: left;"><?php echo "{$r['woitem_deskripsi']} {$r['woitem_dimensi']}"; ?></td>
                <td style="text-align: right;"><?php echo $r['planningweeklydetail_qty']; ?></td>
                <td style="text-align: right;">
                    <input type="hidden" name="txtwoitem_kds[<?php echo $r['planningweeklydetail_kd']; ?>]" value="<?php echo $r['woitem_kd'];?>">
                    <input type="hidden" name="txtmaterialreqdetailweekly_kds[<?php echo $r['planningweeklydetail_kd']; ?>]" value="<?php echo $r['materialreqdetailweekly_kd'];?>">
                    <input type="number" name="txtmaterialreqdetail_qtys[<?php echo $r['planningweeklydetail_kd']; ?>]" class="form-control input-sm" value="<?php echo $qtyBalanced; ?>">
                </td>
                <td style="text-align: right;"><?php echo !empty($r['sum_materialreqdetail_qty']) ? $r['sum_materialreqdetail_qty'] : 0; ?></td>
                <td style="text-align: right;">
                    <input type="text" name="txtmaterialreqdetail_remarks[<?php echo $r['planningweeklydetail_kd']; ?>]" class="form-control input-sm" value="" placeholder="Remark">
                </td>
            </tr>
        <?php
        endforeach; ?>
    </tbody>
    <tfoot>
    </tfoot>
</table>

<?php echo form_close(); ?>

<script type="text/javascript">
    // $('.clQtyOut, .clKonversi').on('keyup', function(e) {
    //     var thisQtyOutKey = $(this).attr('data-rm_kd');
    //     sumQtyKonversi(thisQtyOutKey);
    // });

    // function sumQtyKonversi(thisQtyOutKey) {
    //     var thisQtyOut = $('#idtxtmaterialreceiptdetailrm_qtyBatch' + thisQtyOutKey).val();
    //     var thisKonversi = $('#idtxtmaterialreceiptdetailrm_konversiBatch' + thisQtyOutKey).val();

    //     var idtxtmaterialreceiptdetailrm_qtykonversiBatch = 'idtxtmaterialreceiptdetailrm_qtykonversiBatch' + thisQtyOutKey;
    //     var materialreceiptdetailrm_qtyOut = $('#idtxtmaterialreceiptdetailrm_qtyOut' + thisQtyOutKey).val();
    //     var qtyOutKonversi = thisQtyOut * thisKonversi;
    //     $('#' + idtxtmaterialreceiptdetailrm_qtykonversiBatch).val(qtyOutKonversi);
    // }

    // $('.clMaterialreceiptdetailrm_satuankonversis').on("select2:selecting", function(e) {
    //     var dt = e.params.args.data;
    //     var thisSatuanKonversiKey = $(this).attr('data-rm_kd');
    //     $('#idtxtmaterialreceiptdetailrm_konversiBatch' + thisSatuanKonversiKey).val(dt.rmsatuankonversi_konversi);
    //     sumQtyKonversi(thisSatuanKonversiKey);
    // });

    // $('.clMaterialreceiptdetailrm_satuan').on("select2:selecting", function(e) {
    //     let dt = e.params.args.data;
    //     var thisSatuanOutKey = $(this).attr('data-rm_kd');
    //     var rmsatuan_kd_from = dt.id;
    //     var rmsatuan_kd_to = $('#idtxtmaterialreceiptdetailrm_satuankonversiBatch' + thisSatuanOutKey).val();
    //     render_konversi_tblbatch('#idtxtmaterialreceiptdetailrm_satuankonversis' + thisSatuanOutKey, rmsatuan_kd_from, rmsatuan_kd_to);
    // });

    // function render_konversi_tblbatch(target, rmsatuan_kd_from = null, rmsatuan_kd_to = null) {
    //     $(target).select2({
    //         theme: 'bootstrap',
    //         placeholder: '--Pilih Opsi--',
    //         minimumInputLength: 0,
    //         ajax: {
    //             url: '<?php echo base_url() ?>/Auto_complete/get_rm_satuan_konversi',
    //             type: "get",
    //             dataType: 'json',
    //             delay: 250,
    //             data: function(params) {
    //                 return {
    //                     rmsatuan_kd_from: rmsatuan_kd_from,
    //                     rmsatuan_kd_to: rmsatuan_kd_to,
    //                     paramSatuan: params.term // search term
    //                 };
    //             },
    //             processResults: function(response) {
    //                 return {
    //                     results: response
    //                 };
    //             },
    //             cache: true
    //         }
    //     });
    // }

    $('#idtablemainBatch').DataTable({
        "paging": false,
        "ordering": true,
        "searching": true,
        "scrollY": "250px",
        "info": false,
        "language": {
            "lengthMenu": "Tampilkan _MENU_ data",
            "zeroRecords": "Maaf tidak ada data yang ditampilkan",
            "info": "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
            "infoFiltered": "(difilter dari _MAX_ total data)",
            "infoEmpty": "Tidak ada data yang ditampilkan",
            "search": "Cari :",
            "loadingRecords": "Memuat Data...",
            "processing": "Sedang Memproses...",
            "paginate": {
                "first": '<span class="glyphicon glyphicon-fast-backward"></span>',
                "last": '<span class="glyphicon glyphicon-fast-forward"></span>',
                "next": '<span class="glyphicon glyphicon-forward"></span>',
                "previous": '<span class="glyphicon glyphicon-backward"></span>'
            }
        },
        "order": [1, 'asc'],
    });
</script>