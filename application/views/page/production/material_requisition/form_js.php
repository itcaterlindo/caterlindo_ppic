<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form_master('<?php echo $slug; ?>', '<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('form_add_master', '');
	});

	$(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		open_table();
		$('#<?php echo $btn_add_id; ?>').slideDown();
	});

	function open_form_master(slug, id) {
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/form_master',
			data: {
				slug: slug,
				id: id
			},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
				render_select2('.select2');
				moveTo('idMainContent');
				if (slug == 'edit') {
					var divisi_kd = $('#idtxtdivisi_kd').val();
					render_planning(divisi_kd);
				}
			}
		});
	}

	function open_form_main(slug, materialreq_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/form_main',
			data: {
				slug: slug,
				materialreq_kd: materialreq_kd
			},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
			}
		});
	}

	function form_item(slug, materialreq_kd, bagian_kd, id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/form_item',
			data: {
				slug: slug,
				materialreq_kd: materialreq_kd,
				id: id
			},
			success: function(html) {
				$('#idformItem').html(html);
				render_wo(bagian_kd);
			}
		});
	}

	function form_item_tablewoplanning(materialreq_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/form_item_tablewoplanning',
			data: {
				materialreq_kd: materialreq_kd,
			},
			success: function(html) {
				$('#idtableplanningweeklyItem').html(html);
			}
		});
	}

	$(document).off('click', '#idcheckall').on('click', '#idcheckall', function() {
		if ($(this).is(':checked')) {
			$('.classCheck').prop('checked', true);
		} else {
			$('.classCheck').prop('checked', false);
		}
	});

	function form_item_table(materialreq_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/form_item_table',
			data: {
				materialreq_kd: materialreq_kd
			},
			success: function(html) {
				$('#idtableItem').html(html);
			}
		});
	}

	function render_wo(bagian_kd) {
		$("#idtxtplanningweeklydetail_kd").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 2,
			ajax: {
				url: '<?php echo base_url() . $class_link; ?>/get_woitem',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function(params) {
					return {
						paramWO: params.term, // search term
						bagian_kd: bagian_kd
					};
				},
				processResults: function(response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_planning(divisi_kd) {
		$("#idtxtplanningweekly_kds").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: {
				url: '<?php echo base_url() . $class_link; ?>/get_planningweekly',
				type: "get",
				dataType: 'json',
				delay: 250,
				data: function(params) {
					return {
						paramPlanningweekly: params.term, // search term
						divisi_kd: divisi_kd
					};
				},
				processResults: function(response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	$(document).off('select2:select', '#idtxtplanningweeklydetail_kd').on('select2:select', '#idtxtplanningweeklydetail_kd', function(e) {
		let data = e.params.data;
		$('#idtxtplanningweekly_no').val(data.planningweekly_no);
		$('#idtxtwoitem_kd').val(data.woitem_kd);
		$('#idtxtwoitem_itemcode').val(data.woitem_itemcode);
		$('#idtxtwoitem_deskripsi').text(data.woitem_deskripsi + ' ' + data.woitem_dimensi);
		$('#idtxtqty_max').val(data.qty);
		$('#idtxtmaterialreqdetail_qty').val(data.qty);
		$('#idtxtmaterialreqdetail_qty').focus();
	});

	$(document).off('select2:select', '#idtxtdivisi_kd').on('select2:select', '#idtxtdivisi_kd', function(e) {
		let data = e.params.data;
		render_planning(data.id);
	});

	function toggle_modal(modalTitle, htmlContent) {
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function submitFormMaster(form_id) {
		$('#idbtnsubmitMaster').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtnsubmitMaster').attr('disabled', true);

		event.preventDefault();
		var form = document.getElementById(form_id);
		var url = "<?php echo base_url() . $class_link . '/action_insert_master'; ?>";

		$.ajax({
			url: url,
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				var resp = JSON.parse(data);
				console.log(resp);
				if (resp.code == 200) {
					open_form_main('add', resp.data.materialreq_kd);
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
					box_overlay('out');
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
				$('#idbtnsubmitMaster').html('<i class="fa fa-arrow-right"></i> Lanjut');
				$('#idbtnsubmitMaster').attr('disabled', false);
			}
		});
	}

	function submitData() {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById('idFormInput');
		var url = '<?php echo base_url() . $class_link . '/action_insert_materialreqdetail'; ?>';

		$.ajax({
			url: url,
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				console.log(data);
				var resp = JSON.parse(data);
				if (resp.code == 200) {
					var materialreq_kd = $('#idtxtmaterialreq_kd').val();
					var bagian_kd = $('#idtxtbagian_kd').val();
					form_item('add', materialreq_kd, bagian_kd, '');
					form_item_table(materialreq_kd);
					notify(resp.status, resp.pesan, 'success');
					generateToken(resp.csrf);
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
					box_overlay('out');
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
				box_overlay('out');
			}
		});
	}

	function submitDataBatch(form_id) {
        box_overlay('in');
        event.preventDefault();
        var form = document.getElementById(form_id);
        var url = '<?php echo base_url() . $class_link . '/action_insert_materialreqdetail_batch'; ?>';

        $.ajax({
            url: url,
            type: "POST",
            data: new FormData(form),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                console.log(data);
                var resp = JSON.parse(data);
                if (resp.code == 200) {
                    notify(resp.status, resp.pesan, 'success');
					form_item_table(resp.data.materialreq_kd);
					form_item_tablewoplanning(resp.data.materialreq_kd);
                } else if (resp.code == 401) {
                    $.each(resp.pesan, function(key, value) {
                        $('#' + key).html(value);
                    });
                    generateToken(resp.csrf);
                } else if (resp.code == 400) {
                    notify(resp.status, resp.pesan, 'error');
                    generateToken(resp.csrf);
                } else {
                    notify('Error', 'Error tidak Diketahui', 'error');
                    generateToken(resp.csrf);
                }
                box_overlay('out');
            }
        });
    }

	function hapus_item(id) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			box_overlay('in');
			var url = '<?php echo base_url() . $class_link ?>/action_delete_materialreq_detail';
			$.ajax({
				url: url,
				type: 'GET',
				data: 'id=' + id,
				success: function(data) {
					console.log(data);
					var resp = JSON.parse(data);
					if (resp.code == 200) {
						notify(resp.status, resp.pesan, 'success');
						var materialreq_kd = $('#idtxtmaterialreq_kd').val()
						form_item_table(materialreq_kd);
						form_item_tablewoplanning(materialreq_kd);
					} else {
						notify('Gagal', resp.pesan, 'error');
					}
					box_overlay('out');
				}
			});
		}
	}

	function box_overlay(sts) {
		if (sts == 'in') {
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		} else if (sts == 'out') {
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}
</script>