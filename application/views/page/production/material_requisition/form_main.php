<?php
defined('BASEPATH') or exit('No direct script access allowed!');
echo form_input(array('type' => 'hidden', 'id' => 'idtxtbagian_kd', 'name' => 'txtbagian_kd', 'placeholder' => 'idtxtbagian_kd', 'value' => isset($materialreq['bagian_kd']) ? $materialreq['bagian_kd'] : null));
?>
<style>
	.select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>

<div class="row invoice-info">
	<div class="col-sm-6 invoice-col">
		<b>Tanggal Material Requisition :</b> #<?= format_date($materialreq['materialreq_tanggal'], 'd-m-Y') ?><br>
		<b>Divisi :</b> <?php echo "{$materialreq['divisi_nama']}({$materialreq['bagian_nama']})"; ?>
	</div>
	<div class="col-sm-6 invoice-col">
		<b>No Material Requisition : </b> #<?php echo $materialreq['materialreq_no']; ?> <br>
		<b>No Planningweekly : </b> #<?php echo $planningweekly_nos; ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div id="idtableplanningweeklyItem"></div>
	</div>
</div>
<hr>
<div class="row">
	<div id="idtableItem"></div>
</div>

<script type="text/javascript">
	form_item('add', '<?php echo $materialreq_kd; ?>', '<?php echo $materialreq['bagian_kd']; ?>', '');
	form_item_table('<?php echo $materialreq_kd; ?>');
	form_item_tablewoplanning('<?php echo $materialreq_kd; ?>');
</script>