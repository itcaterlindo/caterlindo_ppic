<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
    td.dt-center {
        text-align: center;
    }

    td.dt-right {
        text-align: right;
    }

    td.dt-left {
        text-align: left;
    }
</style>

<table id="idtablemain" cellspacing="0" cellpadding="1" style="font-size: 80%;" border="1" width="100%">
    <thead>
        <tr>
            <th width="5%" style="text-align: center; font-weight:bold;">No.</th>
            <th width="30%" style="text-align: center; font-weight:bold;">WO Number</th>
            <th width="15%" style="text-align: center; font-weight:bold;">Product Code</th>
            <th width="20%" style="text-align: center; font-weight:bold;">Qty</th>
            <th width="30%" style="text-align: center; font-weight:bold;">Remark</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        $sumQty = 0;
        foreach ($results as $r) :
            $sumQty += $r['materialreqdetail_qty'];
        ?>
            <tr>
                <td width="5%" style="text-align: center;"><?php echo $no; ?></td>
                <td width="30%" style="text-align: center;"><?php echo $r['woitem_no_wo']; ?></td>
                <td width="15%"><?php echo $r['woitem_itemcode']; ?></td>
                <td width="20%" style="text-align: right;"><?php echo $r['materialreqdetail_qty']; ?></td>
                <td width="30%" style="text-align: left;"><?php echo $r['materialreqdetail_remark']; ?></td>
            </tr>
        <?php
            $no++;
        endforeach; ?>
    </tbody>
    <tfoot>
        <tr style="font-weight: bold;">
            <td></td>
            <td></td>
            <td style="text-align: right;">Jumlah : </td>
            <td style="text-align: right;"><?php echo $sumQty; ?></td>
            <td></td>
        </tr>
    </tfoot>
</table>