<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (isset($rowData)):
    extract($rowData);
    $bagianSelected = $bagian_kd;
endif;

echo form_open('', array('id' => 'idFormMaster', 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtmasterSlug', 'name'=> 'txtmasterSlug', 'value' => $slug ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtmaterialreq_kd', 'name'=> 'txtmaterialreq_kd', 'value' => $id ));
?>
<div class="box-body">
    <div class="form-group">
        <label for="idtxtdivisi_kd" class="col-sm-1 control-label">Divisi</label>
        <div class="col-sm-4">
        <div id="idErrdivisi_kd"></div>
        <?php echo form_dropdown('txtdivisi_kd', isset($opsiBagian) ? $opsiBagian : array() , isset($divisi_kd)? $divisi_kd : '', array('class'=> 'form-control select2', 'id'=> 'idtxtdivisi_kd'));?>
        </div>
    </div>
    <div class="form-group">
        <label for="idtxtbagian_kd" class="col-sm-1 control-label">Bagian BoM</label>
        <div class="col-sm-4">
        <div id="idErrbagian_kd"></div>
        <?php echo form_dropdown('txtbagian_kd', isset($opsiBagian) ? $opsiBagian : array() , isset($bagianSelected)? $bagianSelected : '', array('class'=> 'form-control select2', 'id'=> 'idtxtbagian_kd'));?>
        </div>
    </div>
    <div class="form-group">
        <label for="idtxtbagian_kd" class="col-sm-1 control-label">Planning Weekly</label>
        <div class="col-sm-4">
            <select name="txtplanningweekly_kds[]" id="idtxtplanningweekly_kds" class="form-control select2" multiple="multiple">
            <?php
                if (isset($planningWeeklys)):
                    foreach($planningWeeklys as $planningWeekly):
                        echo "<option value='{$planningWeekly['planningweekly_kd']}' selected>{$planningWeekly['planningweekly_no']} | {$planningWeekly['bagian_nama']}</option>";
                    endforeach;
                endif;
            ?>
            </select>
        </div>
    </div>
</div>

<div class="box-footer">
    <button id="idbtnsubmitMaster" onclick="submitFormMaster('idFormMaster')" class="btn btn-primary btn-sm col-sm-offset-1"><i class="fa fa-save"></i> Simpan</button>
</div>

<?php echo form_close(); ?>