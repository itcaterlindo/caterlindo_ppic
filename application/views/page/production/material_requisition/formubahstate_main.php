<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
if (isset($row)){
	extract($row);
}
$form_id = 'idFormState';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtsts', 'placeholder' => 'Sts', 'value' => isset($sts) ? $sts : null ));
echo form_input(array('type' => 'hidden', 'name' => 'txtid', 'placeholder' => 'Id', 'value' => isset($id) ? $id : null ));
echo form_input(array('type' => 'hidden', 'name' => 'txtwftransition_kd', 'placeholder' => 'Id', 'value' => isset($wftransition_kd) ? $wftransition_kd : null ));
?>

<div class="row">
    <div class="col-md-12">
	<div class="form-group">
		<label for="idtxtmaterialreqlog_note" class="col-md-2 control-label">Keterangan</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrmaterialreqlog_note"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtmaterialreqlog_note', 'id'=> 'idtxtmaterialreqlog_note', 'placeholder' =>'Keterangan', 'rows' => '2', 'value'=> isset($materialreqlog_note) ? $materialreqlog_note: null ));?>
		</div>
	</div>
    <div class="form-group">
		<div class="col-sm-2 col-sm-offset-10 col-xs-12">
			<button type="submit" name="btnSubmit" id="idbtnSubmit<?php echo $form_id; ?>" onclick="submitDataState('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>
	</div>
    </div>
</div>	
<?php echo form_close(); ?>
