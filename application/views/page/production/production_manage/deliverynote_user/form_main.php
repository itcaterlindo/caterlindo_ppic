<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$form_id = 'idFormInput';

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtdnuser_kd', 'name' => 'txtdnuser_kd', 'placeholder' => 'idtxtdnuser_kd', 'class' => 'tt-input', 'value' => isset($dnuser_kd) ? $dnuser_kd : null));
?>

<style type="text/css">
	.select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>

<div class="form-group">
	<label for='idtxtadmin_kd' class="col-md-2 control-label">User</label>
	<div class="col-sm-8 col-xs-12">
		<div class="errInput" id="idErridtxtadmin_kd"></div>
		<?php
		echo form_dropdown('txtadmin_kd', isset($opsiUser) ? $opsiUser : [], isset($admin_kd) ? $admin_kd : '', array('class' => 'form-control select2', 'id' => 'idtxtadmin_kd')); ?>
	</div>
</div>
<div class="form-group">
	<label for='idtxtkd_tipe_admin' class="col-md-2 control-label">Tipe User</label>
	<div class="col-sm-8 col-xs-12">
		<div class="errInput" id="idErridtxtkd_tipe_admin"></div>
		<?php
		echo form_dropdown('txtkd_tipe_admin', isset($opsiTipeUser) ? $opsiTipeUser : [], isset($kd_tipe_admin) ? $kd_tipe_admin : '', array('class' => 'form-control select2', 'id' => 'idtxtkd_tipe_admin')); ?>
	</div>
</div>
<div class="form-group">
	<label for='idtxtkd_bagian' class="col-md-2 control-label">Bagian</label>
	<div class="col-sm-8 col-xs-12">
		<div class="errInput" id="idErridtxtkd_bagian"></div>
		<?php
		echo form_dropdown('txtkd_bagian', isset($opsiBagian) ? $opsiBagian : [], isset($kd_bagian) ? $kd_bagian : '', array('class' => 'form-control select2', 'id' => 'idtxtkd_bagian')); ?>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-4 col-sm-offset-2 col-xs-12">
		<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
			<i class="fa fa-save"></i> Simpan
		</button>
	</div>
</div>

<?php echo form_close(); ?>