<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	form_main('<?php echo $id; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	function form_main(id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/form_main',
			data: {
				id: id
			},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
			}
		});
	}

	function form_item(slug, dn_asal, dn_tujuan, dn_kd, dndetail_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/form_item',
			data: {
				slug: slug,
				dn_asal: dn_asal,
				dn_tujuan: dn_tujuan,
				dn_kd: dn_kd,
				dndetail_kd: dndetail_kd
			},
			success: function(html) {
				$('#idformItem').html(html);
				render_woitem(dn_asal, dn_tujuan, dndetail_kd);
			}
		});
	}

	function render_woitem(dn_asal, dn_tujuan, dndetail_kd) {
		$("#idtxtwoitem_kdOpsi").select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			minimumInputLength: 0,
			ajax: {
				url: '<?= base_url() . $class_link ?>/get_woitem',
				type: "get",
				dataType: 'json',
				delay: 500,
				data: function(params) {
					return {
						dn_asal: dn_asal,
						dn_tujuan: dn_tujuan,
						dndetail_kd: dndetail_kd,
						paramWO: params.term // search term
					};
				},
				processResults: function(response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	$(document).off('select2:select', '#idtxtwoitem_kdOpsi').on('select2:select', '#idtxtwoitem_kdOpsi', function(e) {
		let dt = e.params.data;
		console.log(dt)
		$('#idtxtwoitem_kd').val(dt.woitem_kd);
		$('#idtxtwoitem_itemcode').val(dt.woitem_itemcode);
		$('#idtxtdndetail_qtymax').val(dt.woitem_qty);
		$('#idtxtdndetail_qty').val(dt.woitem_qty);
		$('#idtxtkd_barang').val(dt.kd_barang);
		$('#idtxtdndetail_qty').focus();
	});

	function form_item_table(dn_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/form_item_table',
			data: {
				dn_kd: dn_kd
			},
			success: function(html) {
				$('#idtableItem').html(html);
			}
		});
	}

	function add_detail_item(dndetail_kd) {
		event.preventDefault();
		let dn_kd = $('#idtxtdn_kd').val();
		let dn_asal = $('#idtxtdn_asal').val();
		let dn_tujuan = $('#idtxtdn_tujuan').val();
		// get_woitem (dn_asal, dn_tujuan, dndetail_kd);
		form_item('add_detail', dn_asal, dn_tujuan, dn_kd, dndetail_kd);
		moveTo('idformItem');
	}

	function submitData() {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById('idFormInput');
		var url = '<?php echo base_url() . $class_link . '/action_insert_dndetail'; ?>';

		/** Cek Qty sebelum submit */
		var max = parseInt($('#idtxtdndetail_qtymax').val());
		var qty = parseInt($('#idtxtdndetail_qty').val());

		$.ajax({
			url: url,
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				console.log(data);
				var resp = JSON.parse(data);
				if (resp.code == 200) {
					let dn_kd = $('#idtxtdn_kd').val();
					let dn_asal = $('#idtxtdn_asal').val();
					let dn_tujuan = $('#idtxtdn_tujuan').val();
					form_item('add', dn_asal, dn_tujuan, dn_kd);
					form_item_table(dn_kd);
					notify(resp.status, resp.pesan, 'success');
					generateToken(resp.csrf);
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
					box_overlay('out');
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
				box_overlay('out');
			}
		});
		// }
	}

	function resetFormMain() {
		event.preventDefault();
		let dn_kd = $('#idtxtdn_kd').val();
		let dn_asal = $('#idtxtdn_asal').val();
		let dn_tujuan = $('#idtxtdn_tujuan').val();
		form_item('add', dn_asal, dn_tujuan, dn_kd);
		moveTo('idMainContent');
	}

	// function reset_form_item () {
	// 	$('.tt-input').val('');
	// 	var bagian_kd = $('#idtxtbagian_kd').val();
	// 	get_woitem (bagian_kd);
	// }

	function hapus_data(id) {
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			box_overlay('in');
			var url = '<?php echo base_url() . $class_link ?>/action_delete_dndetail';
			$.ajax({
				url: url,
				type: 'GET',
				data: 'id=' + id,
				success: function(data) {
					console.log(data);
					var resp = JSON.parse(data);
					if (resp.code == 200) {
						notify(resp.pesan, 'Data Terhapus', 'success');
						let dn_kd = $('#idtxtdn_kd').val();
						let dn_asal = $('#idtxtdn_asal').val();
						let dn_tujuan = $('#idtxtdn_tujuan').val();
						form_item('add', dn_asal, dn_tujuan, dn_kd);
						form_item_table(dn_kd);
					} else {
						notify('Gagal', resp.pesan, 'error');
					}
					box_overlay('out');
				}
			});
		}
	}

	// // function edit_woitem(wo_kd, id) {
	// //     var slug = 'edit';
	// // 	open_form_item (slug, wo_kd, id);
	// // 	moveTo('idMainContent');
	// // }

	// function render_datetimepicker (valParam) {
	// 	$(valParam).datetimepicker({
	//         format: 'DD-MM-YYYY',
	//     });
	// }

	function box_overlay(sts) {
		if (sts == 'in') {
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		} else if (sts == 'out') {
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

	function first_load(loader, content) {
		$('#' + loader).fadeOut(500, function(e) {
			$('#' + content).slideDown();
		});
	}

	function render_select2(element) {
		$(element).select2({
			theme: 'bootstrap',
			width: '100%',
			placeholder: '-- Pilih Opsi --',
		});
	}

	function generateToken(csrf) {
		$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function moveTo(div_id) {
		$('html, body').animate({
			scrollTop: $('#' + div_id).offset().top - $('header').height()
		}, 1000);
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			delay: 2500,
			styling: 'bootstrap3'
		});
	}
</script>