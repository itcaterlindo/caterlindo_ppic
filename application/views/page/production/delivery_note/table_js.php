<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_table();
    first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

    $(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

    function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function edit_master(id){
		open_form_box('edit', id);
	}

	function open_table() {
        $('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id;?>').slideUp( function() {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url() . $class_link . '/open_table'; ?>',
				success:function(html){
					$('#<?php echo $box_content_id;?>').slideDown().html(html);
			        moveTo('idMainContent');
				}
			});
		});
	}

	function open_form_box(slug, id){
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url().$class_link.'/open_form_box'; ?>',
            data: {slug:slug, id:id},
            success:function(html){
                $('#idMainContent').prepend(html);
                moveTo('idMainContent');
            }
        });
	}

	function edit_data(id) {
		window.location.assign('<?php echo base_url().$class_link?>/form_main_box?dn_kd='+id);
	}

	function lihat_data(id) {
		window.location.assign('<?php echo base_url().$class_link?>/view_box?dn_kd='+id);
	}

	function cetak_data (id) {
		window.open('<?php echo base_url().$class_link?>/pdf_tablemain?id='+id);
	}

	function hapus_data(id) {
		var conf  = confirm('Apakah anda yakin ?');
        if(conf){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete'; ?>',
				data: {id: id},
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						open_table();
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
				}
			});
		}
	}
	
	function render_select2(element){
		$(element).select2({
			theme: 'bootstrap',
			width: '100%',
			placeholder: '-- Pilih Opsi --',
		});
	}

	function render_datetimepicker(element){
		$(element).datetimepicker({
			format: 'DD-MM-YYYY',
    	});
	}

	function cetak_wo (wo_kd) {
		window.open('<?php echo base_url().$class_link;?>/cetak_pdf_wo/'+wo_kd);
	}

	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

    function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
			delay: 2500,
            styling: 'bootstrap3'
        });
    }
</script>