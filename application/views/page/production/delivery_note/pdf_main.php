<?php
class customPdf extends Tcpdf {
    public $suplier_kode;
    public $po_no;
    public $suplier_nama;
    public $po_tglinput;
    public $suplier_alamat;
    public $originator;
    public $suplier_telpon;

    public function setsHeader($headerData){
        $this->dn_no = $headerData['dn_no'];
        $this->bagian_asal = $headerData['bagian_asal'];
        $this->bagian_tujuan = $headerData['bagian_tujuan'];
        $this->dn_tanggal = format_date($headerData['dn_tanggal'], 'Y-m-d'); 
    }

    public function Header() {
        $header = '
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td width="30%" style="text-align:left;"> <img src="assets/admin_assets/dist/img/logo_cat.png" width="135" height="35"> </td>
                <td width="40%" style="text-align: center; font-weight:bolt;"> <u>Nota Pengiriman Antar Bagian</u> <br> <i>(DELIVERY NOTE) </i></td>
                <td width="15%" style="text-align:left; font-size: 70%;"> No Dokumen / Rev <br> Tanggal Terbit</td>
                <td width="15%" style="text-align:left; font-size: 70%;"> : CAT4-PRO-001/05 <br> : 14 Oktober 2022</td>
            </tr>
        </table>';

        /** Keterangan */
        $keterangan =
        '<table cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 80%;">
            <tr>
                <td width="20%"> Nota No. </td>
                <td width="30%"> : <u>'.$this->dn_no.'</u> </td>
                <td width="20%"> Tanggal </td>
                <td width="30%"> : <u>'.$this->dn_tanggal.'</u> </td>
            </tr>
            <tr>
                <td> Dari Divisi </td>
                <td width="30%"> : <u>'.$this->bagian_asal.'</u> </td>
                <td width="20%"> Ke Divisi </td>
                <td width="30%"> : <u>'.$this->bagian_tujuan.'</u> </td>
            </tr>

        </table>';

        $this->writeHTML($header, true, false, false, false, '');
        $this->writeHTML($keterangan, true, false, false, false, '');
    }

}

$pdf = new customPdf('L', 'mm', 'A5', true, 'UTF-8', false);
$title = $master['dn_no'];
$pdf->SetTitle($title);
$pdf->SetAutoPageBreak(true);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');
$pdf->setPrintFooter(false);

/** Margin */ 
$pdf->SetHeaderMargin(5);
$pdf->SetFooterMargin(10);
$pdf->SetMargins(8, 35, 8);

$dataHeader = array(
    'dn_no' => $master['dn_no'],
    'bagian_asal' => $master['bagian_asal'],
    'bagian_tujuan' => $master['bagian_tujuan'],
    'dn_tanggal' => format_date($master['dn_tanggal'], 'd-m-Y'),
);
$pdf->setsHeader($dataHeader);

$pdf->AddPage();


$pdf->writeHTML($konten, true, false, false, false, '');

$footerPengirim = '<tr>
<td width="10%"> Pengirim </td>
<td width="2%">:</td>
<td width="40%"> '.$master['pengirim']['nm_admin'].' '.format_date($master['pengirim']['dnlog_tglinput'], 'd-M-Y H:i:s').' </td>
<td width="10%"> Diketahui </td>
<td width="2%">:</td>
<td width="40%"> '.$master['approve']['nm_admin'].' '.format_date($master['pengirim']['dnlog_tglinput'], 'd-M-Y H:i:s').' </td>
</tr>';  

// Foreach ketika ada DN diterima parsial
foreach($master['penerima'] as $value):
    $footerPenerima .= '<tr>
    <td width="10%"> Penerima </td>
    <td width="2%">:</td>
    <td width="40%"> '.$value['nm_admin'].' '.format_date($value['dnlog_tglinput'], 'd-M-Y H:i:s').' </td>
    </tr>';
endforeach;

$ttd = 
'<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<table border="0" style="text-align: left; font-size: 75%;"> 
'.$footerPengirim.$footerPenerima.'
</table>';

$pdf->writeHTML($ttd, true, false, false, false, '');


$txtOutput = $title.'.pdf';
$pdf->Output($txtOutput, 'I');