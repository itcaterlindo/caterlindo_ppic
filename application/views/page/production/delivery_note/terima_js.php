<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	terima_table(<?php echo $id; ?>);

	$(document).off('ifClicked', '#idcheckall').on('ifClicked', '#idcheckall', function() {
		if (this.checked) {
			$('.tt-check').iCheck('uncheck');
		} else {
			$('.tt-check').iCheck('check');
		}
	});

	function render_datetime(valClass, valFormat) {
		$('.' + valClass).datetimepicker({
			format: valFormat,
		});
	}

	function terima_table(id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url() . $class_link ?>/terima_table',
			data: {
				id: id
			},
			success: function(html) {
				$('#<?php echo $box_content_id; ?>').html(html);
				render_datetime('datetimepicker', 'YYYY-MM-DD');
				$('input[type="checkbox"].icheck').iCheck({
					checkboxClass: 'icheckbox_square-blue'
				});
			}
		});
	}

	function submitDataTerima(form_id) {
		event.preventDefault();
		var form = document.getElementById(form_id);
		let url = '<?php echo base_url() . $class_link . '/action_insert_dnreceived'; ?>';

		$.ajax({
			url: url,
			type: "POST",
			data: new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function() {
				$('#idbtnsubmit').attr('disabled', true);
				boxOverlayForm('in');
			},
			success: function(data) {
				
				var resp = JSON.parse(data);
				console.log(resp);
				if (resp.code == 200) {
					window.location.assign('<?php echo base_url() . $class_link ?>/view_box?dn_kd=<?php echo $id; ?>');
					notify(resp.status, resp.pesan, 'success');
				} else if (resp.code == 401) {
					$.each(resp.pesan, function(key, value) {
						$('#' + key).html(value);
					});
					generateToken(resp.csrf);
				} else if (resp.code == 400) {
					notify(resp.status, resp.pesan, 'error');
					generateToken(resp.csrf);
				} else {
					notify('Error', 'Error tidak Diketahui', 'error');
					generateToken(resp.csrf);
				}
			},
			complete: function() {
				$('#idbtnsubmit').attr('disabled', false);
				boxOverlayForm('out');
			}
		});
	}

	function boxOverlayForm(sts) {
		if (sts == 'in') {
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		} else if (sts == 'out') {
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

	function notify(title, text, type) {
		new PNotify({
			title: title,
			text: text,
			type: type,
			delay: 2500,
			styling: 'bootstrap3'
		});
	}
</script>