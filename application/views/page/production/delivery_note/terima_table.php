<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<?php 
echo form_open('', array('id' => 'idFormMaster', 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtdn_kd', 'name'=> 'txtdn_kd', 'value' => $id ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtdn_asal', 'name'=> 'txtdn_asal', 'value' => $master['dn_asal'] ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtdn_tujuan', 'name'=> 'txtdn_tujuan', 'value' => $master['dn_tujuan'] ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtdnstate_kd', 'name'=> 'txtdnstate_kd', 'value' => $master['dnstate_kd'] ));
?>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label for='idtxtterima_tanggal' class="col-md-1 control-label">Tgl Terima</label>
			<div class="col-sm-2 col-xs-12">
				<div class="input-group">
					<span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
					<?php echo form_input(array('type'=>'text', 'class'=> 'form-control datetimepicker', 'name'=> 'txtterima_tanggal', 'id'=> 'idtxtterima_tanggal', 'placeholder' =>'YYYY-MM-DD HH:ii:ss', 'value'=> isset($terima_tanggal) ? $terima_tanggal: date('Y-m-d') ));?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<table id="idtabledndetail" class="table table-bordered table-striped table-hover display responsive" style="font-size:100%;">
		<thead>
			<tr>
				<th style="text-align: center; width: 1%;">No.</th>
				<th style="text-align: center; width: 1%;"><input type="checkbox" id="idcheckall" class="icheck" checked></th>
				<th style="text-align: center; width: 5%;">No WO</th>
				<th style="text-align: center; width: 7%;">Product Code</th>
				<th style="text-align: center; width: 5%;">Jenis</th>
				<th style="text-align: center; width: 5%;">Qty</th>
				<th style="text-align: center; width: 5%;">Qty<br>Terima</th>
				<th style="text-align: center; width: 12%;">Remark</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$no = 1; 
			$sumDndetail_qty = 0;
			$sumDndetail_qtyreceived = 0;
			foreach ($result as $eResult) :
				$sumDndetail_qty += $eResult['dndetail_qty'];
			?>
			<tr>
				<td class="dt-right"><?php echo $no; ?></td>
				<td class="dt-center"> 
					<input type="checkbox" class="tt-check icheck" name="txtdndetail_kd[]" value="<?php echo $eResult['dndetail_kd']?>" checked> 
					<input type="hidden" name="txtkd_barang[<?php echo $eResult['dndetail_kd'];?>]" value="<?php echo $eResult['kd_barang']?>"> 
					<input type="hidden" name="txtwoitem_kds[<?php echo $eResult['dndetail_kd'];?>]" value="<?php echo $eResult['woitem_kd']?>"> 
					<?php
					/** Result child */
					foreach ($resultDetail as $eResultDetail) {
						if ($eResultDetail['dndetail_parent'] == $eResult['dndetail_kd']) {
							echo '<input type="hidden" name="txtdndetail_kdChild['.$eResult['dndetail_kd'].'][]" value="'.$eResultDetail['dndetail_kd'].'"> ';
							echo '<input type="hidden" name="txtdndetail_qtyChild['.$eResult['dndetail_kd'].'][]" value="'.$eResultDetail['dndetail_qty'].'"> ';
							echo '<input type="hidden" name="txtwoitem_kdsChild['.$eResult['dndetail_kd'].'][]" value="'.$eResultDetail['woitem_kd'].'"> ';
						}
					}
					?>
				</td>
				<td><?php echo $eResult['woitem_no_wo']?></td>
				<td><?php echo $eResult['woitem_itemcode']?></td>
				<td><?php echo ucwords($eResult['woitem_jenis'])?></td>
				<td class="dt-right"><?php echo $eResult['dndetail_qty']?></td>
				<?php
					if($eResult['dndetail_qty'] <= 0){
				?>
					<td><input type="number" class="form-control" name="txtdndetailreceived_qty[<?php echo $eResult['dndetail_kd'];?>]" value="<?php echo $eResult['dndetail_qty']?>" readonly/></td>
				<?php
					}else{
				?>
					<td><input type="number" class="form-control" name="txtdndetailreceived_qty[<?php echo $eResult['dndetail_kd'];?>]" value="<?php echo $eResult['dndetail_qty']?>"/></td>
				<?php
					}
				?>
				<td><input type="text" class="form-control" name="txtdndetailreceived_remark[<?php echo $eResult['dndetail_kd'];?>]" value="<?php echo $eResult['dndetail_remark']?>"/></td>
			</tr>
			<?php 
				$no++;
			endforeach; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="5" style="font-weight: bold; text-align: right;">Jumlah :</td>
				<td style="font-weight: bold; text-align: right;"><?php echo $sumDndetail_qty; ?></td>
				<td></td>
				<td></td>
			</tr>
		</tfoot>
	</table>
	<div class="form-group">
        <label for="idtxtdnlog_keterangan" class="col-md-1 control-label">Keterangan</label>
        <div class="col-md-11 col-xs-12">
            <div id="idErrdnlog_keterangan"></div>
            <?php echo form_textarea(array('name' => 'txtdnlog_keterangan', 'id' => 'idTxtdnlog_keterangan', 'rows'=>'2', 'class' => 'form-control', 'placeholder' => 'Katerangan')); ?>
        </div>
    </div>
	<button class="btn btn-sm btn-primary pull-right" id="idbtnsubmit" onclick="submitDataTerima('idFormMaster')"> <i class="fa fa-save"></i> Simpan</button>

</div>
<?php echo form_close(); ?>