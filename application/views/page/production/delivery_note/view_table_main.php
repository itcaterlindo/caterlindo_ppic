<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center {
		text-align: center;
	}

	td.dt-right {
		text-align: right;
	}

	td.dt-left {
		text-align: left;
	}
</style>
<div class="col-md-12">
	<div class="box-body table-responsive no-padding">
		<table id="idtabledndetail" class="table table-bordered table-striped table-hover display responsive" style="font-size:100%;">
			<thead>
				<tr>
					<th style="text-align: center; width: 1%;">No.</th>
					<th style="text-align: center; width: 5%;">No WO</th>
					<th style="text-align: center; width: 10%;">Product Code</th>
					<th style="text-align: center; width: 10%;">Deskripsi</th>
					<th style="text-align: center; width: 5%;">Jenis WO</th>
					<th colspan="2" style="text-align: center; width: 5%;">Qty</th>
					<th style="text-align: center; width: 8%;">Qty<br>Terima</th>
					<th style="text-align: center; width: 12%;">Remark</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$no = 1;
				$sumDndetail_qty = 0;
				foreach ($result as $eResult) :
					$sumDndetail_qty += $eResult['dndetail_qty'];
					/** Received */
					$htmlReceived = '<ul>';
					foreach ($resultReceived as $eReceived) {
						if ($eReceived['dndetail_kd'] == $eResult['dndetail_kd']) {
							$htmlReceived .= '<li> ' . $eReceived['dndetailreceived_tanggal'] . ' / ' . $eReceived['dndetailreceived_qty'] . ' </li>';
						}
					}
					$htmlReceived .= '</ul>';
				?>
					<tr style="font-weight: bold;">
						<td class="dt-right"><?php echo $no; ?></td>
						<td><?php echo $eResult['woitem_no_wo'] ?></td>
						<td><?php echo $eResult['woitem_itemcode'] ?></td>
						<td><?php echo "{$eResult['woitem_deskripsi']} {$eResult['woitem_dimensi']}" ?></td>
						<td><?php echo ucwords($eResult['woitem_jenis']) ?></td>
						<td></td>
						<td class="dt-right"><?php echo $eResult['dndetail_qty'] ?></td>

						<td class="dt-right"><?php echo $htmlReceived ?></td>
						<td><?php echo $eResult['dndetail_remark'] ?></td>
					</tr>
					<?php
					foreach ($resultDetail as $eResultDetail) :
						if ($eResultDetail['dndetail_parent'] == $eResult['dndetail_kd']) :
					?>
							<tr style="font-size: 85%;">
								<td class="dt-right"></td>
								<td><?php echo $eResultDetail['woitem_no_wo'] ?></td>
								<td><?php echo $eResultDetail['woitem_itemcode'] ?></td>
								<td></td>
								<td><?php echo ucwords($eResultDetail['woitem_jenis']) ?></td>
								<td class="dt-right"><?php echo $eResultDetail['dndetail_qty'] ?></td>
								<td></td>
								<td></td>
								<!-- <td class="dt-right"><?php //echo $eResultDetail['dndetail_qtyreceived']
															?></td> -->
								<td><?php echo $eResultDetail['dndetail_remark'] ?></td>
							</tr>
				<?php
						endif;
					endforeach;
					$no++;
				endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="5" style="font-weight: bold; text-align: right;">Jumlah :</td>
					<td style="font-weight: bold; text-align: right;"><?php echo $sumDndetail_qty; ?></td>
					<!-- <td style="font-weight: bold; text-align: right;"><?php //echo $sumDndetail_qtyreceived; 
																			?></td> -->
					<td></td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>