<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	view_table_main('<?php echo $id;?>');
	viewlog_main('<?php echo $id;?>');
    first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

    // $(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
	// 	$(this).slideUp();
	// 	open_form_box('form_add_master', '');
	// });

    // $(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
	// 	open_table();
    //     $('#<?php echo $btn_add_id; ?>').slideDown();
	// });

	 $(document).off('change', '#idtxtdnstate_kdOpsi').on('change', '#idtxtdnstate_kdOpsi', function() {
		$('#idtxtdnstate_kd').val($(this).val());
	});

	function view_table_main (id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link?>/view_table_main',
			data: {id:id},
			success:function(html){
				$('#<?php echo $box_content_id;?>').html(html);
			}
		});
	}

	function viewlog_main (id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link?>/viewlog_main',
			data: {id:id},
			success:function(html){
				$('#idlogmain').html(html);
			}
		});
	}

	function view_formstate_main (dn_kd, dnstate_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link?>/view_formstate_main',
			data: {dn_kd:dn_kd, dnstate_kd:dnstate_kd},
			success:function(html){
				toggle_modal('Rubah State', html)
			}
		});
	}

	function submitFormState(form_id) {
		event.preventDefault();
		var form = document.getElementById('idFormState');
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url().$class_link?>/action_changestatus',
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				var resp = JSON.parse(data);
				if (resp.code == 200) {
					notify (resp.status, resp.pesan, 'success')
					toggle_modal('', '');
					window.location.reload();
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			}
		});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function render_select2(element){
		$(element).select2({
			theme: 'bootstrap',
			width: '100%',
			placeholder: '-- Pilih Opsi --',
		});
	}

	function generateToken (csrf){
        $('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(csrf);
	}

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
			delay: 2500,
            styling: 'bootstrap3'
        });
    }

</script>