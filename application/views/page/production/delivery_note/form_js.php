<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	open_form_master('<?php echo $slug; ?>', '<?php echo $id;?>');
    first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

    $(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		$(this).slideUp();
		open_form_box('form_add_master', '');
	});

    $(document).off('click', '#<?php echo $btn_remove_id; ?>').on('click', '#<?php echo $btn_remove_id; ?>', function() {
		open_table();
        $('#<?php echo $btn_add_id; ?>').slideDown();
	});

	function open_form_master(slug, id) {
        $('#<?php echo $btn_add_id; ?>').slideDown();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link?>/form_master',
			data: {slug:slug, id:id},
			success:function(html){
				$('#<?php echo $box_content_id;?>').html(html);
				if(slug == 'add'){
					render_select2('.select2');
				}
				render_datetimepicker ('.datetimepicker');
				moveTo('idMainContent');
			}
		});
	}

	function open_form_main (slug, materialreq_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link?>/form_main',
			data: {slug:slug, materialreq_kd:materialreq_kd},
			success:function(html){
				$('#<?php echo $box_content_id;?>').html(html);
			}
		});
	}

	function form_item (slug, materialreq_kd, bagian_kd, id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link?>/form_item',
			data: {slug:slug, materialreq_kd:materialreq_kd, id:id},
			success:function(html){
				$('#idformItem').html(html);
				get_woitem (bagian_kd);
				render_select2('.select2');
			}
		});
	}

	function form_item_table (materialreq_kd) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link?>/form_item_table',
			data: {materialreq_kd:materialreq_kd},
			success:function(html){
				$('#idtableItem').html(html);
			}
		});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function submitFormMaster() {
		event.preventDefault();
		/** DN ASAL & DN TUJUAN tidak boleh sama */
		if( $('#idtxtdn_asal').val() == $('#idtxtdn_tujuan').val() ){		
			alert("DN ASAL & DN TUJUAN tidak boleh sama !!");
		}else{
			$('#idbtnsubmitMaster').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
			$('#idbtnsubmitMaster').attr('disabled', true);
			var form = document.getElementById('idFormMaster');
			var url = "<?php echo base_url().$class_link.'/action_insert_master'; ?>"; 		
			$.ajax({
				url: url,
				type: "POST",
				data:  new FormData(form),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){
					var resp = JSON.parse(data);
					console.log(resp);
					if(resp.code == 200){
						window.location.assign('<?php echo base_url().$class_link?>/form_main_box?dn_kd='+resp.data.dn_kd);
					}else if (resp.code == 401){
						$.each( resp.pesan, function( key, value ) {
							$('#'+key).html(value);
						});
						generateToken (resp.csrf);
						box_overlay('out');
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
					$('#idbtnsubmitMaster').html('<i class="fa fa-save"></i> Simpan');
					$('#idbtnsubmitMaster').attr('disabled', false);
				} 	        
			});
		}

	}

	function submitData () {
		box_overlay('in');
		event.preventDefault();
		var form = document.getElementById('idFormInput');
		var url = '<?php echo base_url().$class_link.'/action_insert_materialreq_item'; ?>';
		var slug = $('#idtxtwoitemdetailSlug').val();
		if (slug == 'edit'){
			url = '<?php echo base_url().$class_link.'/action_update_materialreq_item'; ?>';
		}

		/** Cek Qty sebelum submit */
		var max = $('#idtxtmaterialreqitem_qtymax').val();
		var qty = $('#idtxtmaterialreqitem_qty').val();

		if ( qty > max ) {
			notify ('Gagal', 'Qty lebih besar dari WO', 'error');
			box_overlay('out');
		}else {
			$.ajax({
				url: url,
				type: "POST",
				data:  new FormData(form),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){
					console.log(data);
					var resp = JSON.parse(data);
					if(resp.code == 200) {
						var materialreq_kd = $('#idtxtmaterialreq_kd').val();
						reset_form_item();
						form_item_table (materialreq_kd);
						notify (resp.status, resp.pesan, 'success');
						generateToken (resp.csrf);
					}else if (resp.code == 401){
						$.each( resp.pesan, function( key, value ) {
							$('#'+key).html(value);
						});
						generateToken (resp.csrf);
						box_overlay('out');
					}else if (resp.code == 400){
						notify (resp.status, resp.pesan, 'error');
						generateToken (resp.csrf);
					}else{
						notify ('Error', 'Error tidak Diketahui', 'error');
						generateToken (resp.csrf);
					}
					box_overlay('out');
				} 	        
			});
		}
	}

	function reset_form_item () {
		$('.tt-input').val('');
		var bagian_kd = $('#idtxtbagian_kd').val();
		get_woitem (bagian_kd);
	}

    function hapus_item (id){
        var conf  = confirm('Apakah anda yakin ?');
        if(conf){
			box_overlay('in');
            var url = '<?php echo base_url().$class_link?>/action_delete_materialreq_item';
            $.ajax({
                url: url,
                type: 'GET',
                data: 'id='+id,
                success: function(data) {
					console.log(data);
                    var resp = JSON.parse(data);
					if (resp.code == 200){
						notify( resp.pesan, 'Data Terhapus', '');
						var materialreq_kd = $('#idtxtmaterialreq_kd').val();
						reset_form_item();
						form_item_table (materialreq_kd);
					}else{
						notify('Gagal', resp.pesan, 'error');
					}
					box_overlay('out');
                }
            });
        }
    }

	// function edit_woitem(wo_kd, id) {
    //     var slug = 'edit';
	// 	open_form_item (slug, wo_kd, id);
	// 	moveTo('idMainContent');
    // }

	function render_datetimepicker (valParam) {
		$(valParam).datetimepicker({
            format: 'DD-MM-YYYY',
        });
	}

	function box_overlay(sts){
		if (sts == 'in'){
			$('#<?php echo $box_overlay_id; ?>').fadeIn();
		}else if (sts == 'out'){
			$('#<?php echo $box_overlay_id; ?>').fadeOut();
		}
	}

	
</script>