<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<div class="col-md-12">
    <table id="idtabledndetail" class="table table-bordered table-striped table-hover display responsive" style="font-size:100%;">
        <thead>
            <tr>
                <th style="text-align: center; width: 1%;">No.</th>
                <th style="text-align: center; width: 1%;">Aksi</th>
                <th style="text-align: center; width: 5%;">No WO</th>
                <th style="text-align: center; width: 10%;">Product Code</th>
                <th style="text-align: center; width: 5%;">Jenis WO</th>
                <th style="text-align: center; width: 5%;">Qty</th>
                <th style="text-align: center; width: 12%;">Remark</th>
                <th style="text-align: center; width: 7%;">UpdatedAt</th>
            </tr>
        </thead>
		<tbody>
			<?php 
			$no = 1;
			foreach ($result as $eResult) :	
			?>
			<tr>
				<td class="dt-right"><?php echo $no; ?></td>
				<td><?php
					$btns = [];
					if ($eResult['woitem_jenis'] == 'packing') {
						$btns[] = get_btn(array('title' => 'Tambah Detail Item', 'icon' => 'plus', 'onclick' => 'add_detail_item(\''.$eResult['dndetail_kd'].'\')'));
					}
					// $btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$eResult['dndetail_kd'].'\')'));
					$btns[] = get_btn_divider();
					$btns[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$eResult['dndetail_kd'].'\')'));
					
					$btn_group = group_btns($btns);
					echo $btn_group;
				?></td>
				<td><?php echo $eResult['woitem_no_wo']?></td>
				<td><?php echo $eResult['woitem_itemcode']?></td>
				<td><?php echo ucwords($eResult['woitem_jenis'])?></td>
				<td class="dt-right"><?php echo $eResult['dndetail_qty']?></td>
				<td><?php echo $eResult['dndetail_remark']?></td>
				<td><?php echo $eResult['dndetail_tgledit']?></td>
			</tr>
			<?php 
				foreach ($resultDetail as $eResultDetail):
					$btnsDetail = [];
					if ($eResultDetail['dndetail_parent'] == $eResult['dndetail_kd']) {
						?>
						<tr>
							<td></td>
							<td><?php
								$btnsDetail[] = get_btn(array('title' => 'Hapus Item', 'icon' => 'trash', 'onclick' => 'hapus_data(\''.$eResultDetail['dndetail_kd'].'\')'));
								
								$btn_groupDetail = group_btns($btnsDetail);
								echo $btn_groupDetail;
							?></td>
							<td style="font-style: italic;"><?php echo $eResultDetail['woitem_no_wo']?></td>
							<td style="font-style: italic;"><?php echo $eResultDetail['woitem_itemcode']?></td>
							<td><?php echo ucwords($eResultDetail['woitem_jenis'])?></td>
							<td class="dt-right"><?php echo $eResultDetail['dndetail_qty']?></td>
							<td><?php echo $eResultDetail['dndetail_remark']?></td>
							<td><?php echo $eResultDetail['dndetail_tgledit']?></td>
						</tr>
						<?php
					}
				endforeach; 
				$no++;
			endforeach; ?>
		</tbody>
    </table>
</div>
