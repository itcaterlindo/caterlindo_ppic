<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (isset($rowData)):
    extract($rowData);
endif;

echo form_open('', array('id' => 'idFormState', 'class' => 'form-horizontal'));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtdn_kd', 'name'=> 'txtdn_kd', 'value' => $id ));
echo form_input(array( 'type'=> 'hidden', 'id' => 'idtxtdnstate_kd', 'name'=> 'txtdnstate_kd', 'value' => $dnstate_kd ));
?>
<div class="box-body">
    
    <div class="form-group" >
        <label for="idtxtdnstate_kdOpsi" class="col-sm-2 control-label">State</label>
        <div class="col-sm-6">
        <div id="idErrdnstate_kdOpsi"></div>
            <?php echo form_dropdown('txtdnstate_kdOpsi', isset($opsiState) ? $opsiState : array() , isset($dnstate_kd)? $dnstate_kd : '', array('class'=> 'form-control', 'id'=> 'idtxtdnstate_kdOpsi', 'disabled' => 'true'));?>
        </div>
    </div>

    <div class="form-group">
        <label for="idtxtdnlog_keterangan" class="col-md-2 control-label">Keterangan</label>
        <div class="col-md-10 col-xs-12">
            <div id="idErrdnlog_keterangan"></div>
            <?php echo form_textarea(array('name' => 'txtdnlog_keterangan', 'id' => 'idTxtdnlog_keterangan', 'rows'=>'2', 'class' => 'form-control', 'placeholder' => 'Katerangan')); ?>
        </div>
    </div>
</div>

<div class="box-footer">
    <button id="idbtnsubmitMaster" onclick="submitFormState('idFormState')" type="submit" class="btn btn-primary btn-sm pull-right"><i class="fa fa-save"></i> Simpan</button>
</div>

<?php echo form_close(); ?>