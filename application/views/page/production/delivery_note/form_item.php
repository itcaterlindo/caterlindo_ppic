<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$form_id = 'idFormInput';

echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtdn_kd', 'name' => 'txtdn_kd', 'placeholder' => 'idtxtdn_kd', 'class' => 'tt-input', 'value' => isset($dn_kd) ? $dn_kd : null));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtdn_asal', 'name' => 'txtdn_asal', 'placeholder' => 'idtxtdn_asal', 'class' => 'tt-input', 'value' => isset($dn_asal) ? $dn_asal : null));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtdn_tujuan', 'name' => 'txtdn_tujuan', 'placeholder' => 'idtxtdn_tujuan', 'value' => isset($dn_tujuan) ? $dn_tujuan : null));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtslug', 'name' => 'txtslug', 'placeholder' => 'idtxtslug', 'value' => isset($slug) ? $slug : null));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtdndetail_kdParent', 'name' => 'txtdndetail_kdParent', 'placeholder' => 'idtxtdndetail_kdParent', 'class' => 'tt-input', 'value' => isset($dndetail_kd) ? $dndetail_kd : null));
?>

<?php if ($slug == 'add_detail') : ?>
	<div class="form-group">
		<label for='idtxtwoitem_itemcodeParent' class="col-md-2 control-label">WO Header</label>
		<div class="col-sm-3 col-xs-12">
			<?php echo form_input(array('type' => 'text', 'class' => 'form-control tt-input', 'name' => 'txtwoitem_itemcodeParent', 'id' => 'idtxtwoitem_itemcodeParent', 'placeholder' => 'Product Code', 'readonly' => 'readonly', 'value' => isset($woitem_itemcodeHeader) ? $woitem_itemcodeHeader : null)); ?>
		</div>
	</div>
<?php endif; ?>
<div class="form-group">
	<label for='idtxtidtxtwoitem_kdOpsi' class="col-md-2 control-label">WO</label>
	<div class="col-sm-5 col-xs-12">
		<div class="errInput" id="idErridtxtwoitem_kdOpsi"></div>
		<?php
		echo form_input(array('type' => 'hidden', 'id' => 'idtxtkd_barang', 'name' => 'txtkd_barang', 'placeholder' => 'idtxtkd_barang', 'value' => isset($kd_barang) ? $kd_barang : null));
		echo form_input(array('type' => 'hidden', 'id' => 'idtxtwoitem_kd', 'name' => 'txtwoitem_kd', 'placeholder' => 'idtxtwoitem_kd', 'value' => isset($woitem_kd) ? $woitem_kd : null));
		echo form_dropdown('txtwoitem_kdOpsi', isset($opsiWO) ? $opsiWO : [], isset($WOitemdetailSelected) ? $WOitemdetailSelected : '', array('class' => 'form-control select2', 'id' => 'idtxtwoitem_kdOpsi')); ?>
	</div>
</div>
<div class="form-group">
	<label for='idtxtwoitem_itemcode' class="col-md-2 control-label">Nama Part</label>
	<div class="col-sm-5 col-xs-12">
		<?php echo form_input(array('type' => 'text', 'class' => 'form-control tt-input', 'name' => 'txtwoitem_itemcode', 'id' => 'idtxtwoitem_itemcode', 'placeholder' => 'Product Code', 'readonly' => 'readonly', 'value' => isset($woitem_itemcode) ? $woitem_itemcode : null)); ?>
	</div>
	<div class="col-sm-3 col-xs-12">
		<?php echo form_input(array('type' => 'number', 'class' => 'form-control tt-input', 'name' => 'txtdndetail_qtymax', 'id' => 'idtxtdndetail_qtymax', 'placeholder' => 'Qty', 'readonly' => 'readonly')); ?>
	</div>
</div>
<div class="form-group">
	<label for='idtxtdndetail_qty' class="col-md-2 control-label">Qty</label>
	<div class="col-sm-5 col-xs-12">
		<div class="errInput" id="idErrdndetail_qty"></div>
		<?php echo form_input(array('type' => 'number', 'class' => 'form-control tt-input', 'name' => 'txtdndetail_qty', 'id' => 'idtxtdndetail_qty', 'placeholder' => 'Qty', 'value' => isset($dndetail_qty) ? $dndetail_qty : null)); ?>
	</div>
</div>
<div class="form-group">
	<label for="idtxtdndetail_remark" class="col-md-2 control-label">Remark</label>
	<div class="col-sm-9 col-xs-12">
		<div class="errInput" id="idErrdndetail_remark"></div>
		<?php echo form_textarea(array('type' => 'text', 'class' => 'form-control tt-input', 'name' => 'txtdndetail_remark', 'id' => 'idtxtdndetail_remark', 'placeholder' => 'Remark', 'rows' => '2', 'value' => isset($dndetail_remark) ? $dndetail_remark : null)); ?>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-4 col-sm-offset-2 col-xs-12">
		<button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData()" class="btn btn-primary btn-sm">
			<i class="fa fa-save"></i> Simpan
		</button>
		<button type="reset" name="btnReset" onclick="resetFormMain()" class="btn btn-default btn-sm">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
</div>

<?php echo form_close(); ?>