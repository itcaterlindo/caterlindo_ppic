<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<table id="idtablemain" cellspacing="0" cellpadding="1" style="font-size: 80%;" border="1" width="100%">
	<thead>
	<tr>
        <th width="5%" style="text-align: center; font-weight:bold;">No.</th>
        <th width="15%" style="text-align: center; font-weight:bold;">WO</th>
        <th width="40%" style="text-align: center; font-weight:bold;">Kode Item</th>
        <th colspan="2"  width="10%" style="text-align: center; font-weight:bold;">Jumlah</th>
        <th width="30%" style="text-align: center; font-weight:bold;">Keterangan</th>
    </tr>
	</thead>
    <tbody>
    <?php 
    $no = 1; 
    foreach ($result as $r):
    ?>
    <tr>
        <td width="5%" style="text-align: center;"><?php echo $no;?></td>
        <td width="15%"><?php echo $r['woitem_no_wo'];?></td>
        <td width="40%"><?php echo $r['woitem_itemcode'];?></td>
        <td width="5%" style="text-align: right;"><?php echo $r['dndetail_qty'];?></td>
        <td width="5%" style="text-align: right;"><?php echo $r['sum_received'];?></td>
        <td width="30%"><?php echo $r['dndetail_remark'];?></td>
    </tr>
    <?php 
        $no++;
    endforeach;?>
    </tbody>
</table>