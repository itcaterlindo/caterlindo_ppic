<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isset($rowData)) :
    extract($rowData);
endif;

echo form_open('', array('id' => 'idFormMaster', 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtmasterSlug', 'name' => 'txtmasterSlug', 'value' => $slug));
echo form_input(array('type' => 'hidden', 'id' => 'idtxtdn_kd', 'name' => 'txtdn_kd', 'value' => $id));
?>

<?php if ($slug != 'add') : ?>
    <div class="form-group">
        <label for="iddn_no" class="col-sm-1 control-label">No</label>
        <div class="col-md-3 col-xs-12">
            <div id="idErrBarcode"></div>
            <input type="text" name="dn_no" value="<?php echo isset($dn_no) ? $dn_no : null ?>" id="iddn_no" class="form-control" placeholder="No" readonly="true">
        </div>
    </div>
<?php endif; ?>
<div class="form-group">
    <label for="idtxtdn_tanggal" class="col-sm-1 control-label">Tanggal</label>
    <div class="col-md-3 col-xs-12">
        <div id="idErrdn_tanggal"></div>
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <input type="text" name="txtdn_tanggal" value="<?php echo date('d-m-Y') ?>" id="idtxtdn_tanggal" class="form-control datetimepicker" placeholder="Tanggal Batch">
        </div>
    </div>
</div>

<div class="form-group">
    <label for="idtxtdn_asal" class="col-sm-1 control-label">Asal</label>
    <div class="col-sm-4">
        <div id="idErrdn_asal"></div>
        <?php 
            $arrOption = [];
            if($slug == 'edit'){
                $arrOption = array('class' => 'form-control select2', 'id' => 'idtxtdn_asal', 'disabled' => 'disabled');
            }else{
                $arrOption = array('class' => 'form-control select2', 'id' => 'idtxtdn_asal');
            }
            echo form_dropdown('txtdn_asal', isset($opsiBagianAsal) ? $opsiBagianAsal : array(), isset($dn_asal) ? $dn_asal : '', $arrOption); 
        ?>
    </div>
    <label for="idtxtdn_tujuan" class="col-sm-1 control-label">Tujuan</label>
    <div class="col-sm-4">
        <div id="idErrdn_tujuan"></div>
        <?php 
            $arrOption = [];
            $arrOption = array('class' => 'form-control select2', 'id' => 'idtxtdn_tujuan');
            echo form_dropdown('txtdn_tujuan', isset($opsiBagianTujuan) ? $opsiBagianTujuan : array(), isset($dn_tujuan) ? $dn_tujuan : '', $arrOption); 
        
        ?>
    </div>
</div>

<div class="form-group">
	<label for="idtxtdn_note" class="col-sm-1 control-label">Note</label>
	<div class="col-sm-9 col-xs-12">
		<div class="errInput" id="idErrdn_note"></div>
		<?php echo form_textarea(array('type' => 'text', 'class' => 'form-control tt-input', 'name' => 'txtdn_note', 'id' => 'idtxtdn_note', 'placeholder' => 'Note', 'rows' => '2', 'value' => isset($dn_note) ? $dn_note : null)); ?>
	</div>
</div>

<div class="form-group">
	<label for="chkReturn" class="col-sm-1 control-label">Return</label>
	<div class="col-sm-9 col-xs-12">
		<div class="checkbox">
            <label>
                <?php echo form_checkbox('chkReturn', 1, $flag_return == 1 ? TRUE : FALSE); ?> * Silahkan centang ketika DN return
            </label>
        </div>
	</div>
</div>

<div class="box-footer">
    <button id="idbtnsubmitMaster" onclick="submitFormMaster()" type="submit" class="btn btn-primary btn-sm pull-right"><i class="fa fa-save"></i> Simpan</button>
</div>

<?php echo form_close(); ?>