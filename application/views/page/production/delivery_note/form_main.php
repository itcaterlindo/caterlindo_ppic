<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<div class="row invoice-info">
	<div class="col-sm-6 invoice-col">
		<b>Tanggal DN :</b> #<?php echo format_date($master['dn_tanggal'], 'Y-m-d') ?><br>
		<b>No DN :</b> <?php echo $master['dn_no']; ?>
	</div>
	<div class="col-sm-6 invoice-col">
		<b>DN Asal : </b> <?php echo $master['bagian_asal']; ?> <br>
		<b>DN Tujuan:</b> <?php echo $master['bagian_tujuan']; ?>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-12">
		<button class="btn btn-success btn-sm" onclick="window.location.assign('<?php echo base_url() . $class_link; ?>/view_box?dn_kd=<?php echo $master['dn_kd']; ?>');"> <i class="fa fa-check"></i> Selesai</button>
	</div>
</div>
<hr>
<div class="row">
	<div id="idformItem"></div>
</div>
<hr>
<div class="row">
	<div id="idtableItem"></div>
</div>

<script type="text/javascript">
	form_item('add', '<?php echo $master['dn_asal']; ?>', '<?php echo $master['dn_tujuan']; ?>', '<?php echo $master['dn_kd']; ?>', '');
	form_item_table('<?php echo $id; ?>');
</script>