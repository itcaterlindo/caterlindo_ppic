<?php
defined('BASEPATH') or exit('No direct script access allowed!');

echo form_open('', array('id' => 'idForm', 'class' => 'form-horizontal'));
?>
<div class="form-group">
	<label for="idTxtCodeSeparator" class="col-md-2 control-label">Code Separator</label>
	<div class="col-md-2">
		<div id="idErrSeparator"></div>
		<?php
		echo form_input(array('type' => 'hidden', 'name' => 'txtCodeFor', 'value' => $code_for));
		$pil_separator = array('' => '-- Pilih Separator --', 'n' => 'Tidak Ada', '.' => '.', ',' => ',', '-' => '-', '_' => '_', '/' => '/');
		$attr_separator = array('id' => 'idSelSeparator', 'class' => 'form-control');
		echo form_dropdown('selSeparator', $pil_separator, $code_separator, $attr_separator);
		?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtCodeReset" class="col-md-2 control-label">Code Reset</label>
	<div class="col-md-2">
		<div id="idErrCodeReset"></div>
		<?php
		$pil_resetter = array('' => '-- Pilih Resetter --', 'day' => 'Day', 'month' => 'Month', 'year' => 'Year');
		$attr_resetter = array('id' => 'idSelResetter', 'class' => 'form-control');
		echo form_dropdown('selResetter', $pil_resetter, $on_reset, $attr_resetter);
		?>
	</div>
</div>
<div class="form_format">
	<?php echo $code_form; ?>
</div>
<div class="box-footer">
	<div class="col-xs-4 pull-left">
		<button type="submit" name="btnSubmit" class="btn btn-info pull-right" style="margin-right: 5px;"">
			<i class="fa fa-save"></i> Simpan Format
		</button>
		<a href="javascript:void(0);" name="btnCancel" id="idBtnCancel" class="btn btn-danger pull-right" style="margin-right: 5px;"">
			<i class="fa fa-ban"></i> Cancel
		</a>
	</div>
</div>
<?php
$data['form_error'] = array('idErrSeparator', 'idErrCodeReset');
$this->load->view('script/'.$class_link.'/form_js', $data);
echo form_close();