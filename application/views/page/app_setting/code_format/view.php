<?php
defined('BASEPATH') or exit('No direct script access allowed!');

echo form_open('', array('id' => 'idForm', 'class' => 'form-horizontal'));
?>
<div class="form-group">
	<label for="idTxtCodeSeparator" class="col-md-2 control-label">Code Separator</label>
	<div class="col-md-2" style="margin-top: 7px;">
		<div id="idErrSeparator"></div>
		<?php echo form_input(array('type' => 'hidden', 'name' => 'txtCodeFor', 'value' => $code_for)); ?>
		<?php echo $code_separator; ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtCodeReset" class="col-md-2 control-label">Code Reset</label>
	<div class="col-md-2" style="margin-top: 7px;">
		<div id="idErrCodeReset"></div>
		<?php echo $on_reset; ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtCodeLength" class="col-md-2 control-label">Panjang Code</label>
	<div class="col-md-2" style="margin-top: 7px;">
		<div id="idErrCodeLength"></div>
		<?php echo $code_length; ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtCodeFormat" class="col-md-2 control-label">Format Code</label>
	<div class="col-md-3" style="margin-top: 7px;">
		<div id="idErrFormat"></div>
		<?php echo $code_format; ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtCodeFormat" class="col-md-2 control-label">Contoh Code</label>
	<div class="col-md-3" style="margin-top: 7px;">
		<div id="idErrFormat"></div>
		<?php echo $contoh_code; ?>
	</div>
</div>
<div class="box-footer">
	<div class="col-xs-4 pull-left">
		<a href="javascript:void(0);" name="btnUbah" id="idBtnUbah" class="btn btn-warning pull-right" style="margin-right: 100px;"">
			<i class="fa fa-pencil"></i> Ubah Format
		</a>
	</div>
</div>
<?php
$data['form_error'] = array('idErrSeparator', 'idErrCodeReset');
$this->load->view('script/'.$class_link.'/form_js', $data);
echo form_close();