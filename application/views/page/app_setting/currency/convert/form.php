<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<div class="box <?php echo $class; ?>" id="idBoxForm">
	<div class="box-header with-border">
		<h3 class="box-title">Form Input Currency Convert</h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool btn-collapse" id="idBtnBoxFormSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
				<i class="fa fa-minus"></i>
			</button>
			<button class="btn btn-box-tool btn-remove" id="idBtnBoxFormClose" data-widget="remove" data-toggle="tooltip" title="Close">
				<i class="fa fa-times"></i>
			</button>
		</div>
	</div>

	<div class="box-body">
		<div id="idBoxFormLoader" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="idBoxFormContent" style="display: none;">
			<?php
			echo form_open('', array('id' => 'idForm', 'class' => 'form-horizontal'));
			?>
			<div id="idErrForm"></div>
			<div class="form-group">
				<label for="idTxtPrimary" class="col-md-2 control-label">Primary Currency</label>
				<div class="col-md-3">
					<div id="idErrPrimary"></div>
					<?php
					echo form_input(array('type' => 'hidden', 'name' => 'txtKd', 'value' => $kd_currency_convert));
					echo form_input(array('name' => 'txtPrimary', 'id' => 'idTxtPrimary', 'class' => 'form-control', 'value' => $primary_name, 'placeholder' => 'Primary Currency', 'disabled' => 'disabled'));
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="idSelSecondary" class="col-md-2 control-label">Secondary Currency</label>
				<div class="col-md-2">
					<div id="idErrSecondary"></div>
					<?php
					$pil = array('' => '-- Pilih Secondary --');
					$result = $this->tm_currency->get_secondary();
					foreach ($result as $row) :
						$pil[$row->kd_currency] = $row->currency_nm;
					endforeach;
					$attr = array('id' => 'idSelSecondary', 'class' => 'form-control');
					echo form_dropdown('selSecondary', $pil, $secondary_kd, $attr);
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="idTxtConvert" class="col-md-2 control-label">Convert Value</label>
				<div class="col-md-2">
					<div id="idErrConvert"></div>
					<?php echo form_input(array('name' => 'txtConvert', 'id' => 'idTxtCOnvert', 'class' => 'form-control', 'value' => $convert_value, 'placeholder' => 'Convert Value')); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="idTxtTgl" class="col-md-2 control-label">Waktu Berlaku</label>
				<div class="col-md-2">
					<div id="idErrTgl"></div>
					<?php echo form_input(array('name' => 'txtTgl', 'id' => 'idTxtTgl', 'class' => 'form-control', 'value' => $tgl_berlaku, 'placeholder' => 'Waktu Berlaku')); ?>
				</div>
			</div>
			<div class="box-footer">
				<div class="col-xs-4 pull-left">
					<button type="submit" name="btnSubmit" id="idBtnSubmit" class="btn btn-info pull-right" style="margin-right: 5px;"">
						<i class="fa fa-save"></i> Simpan Format
					</button>
					<a href="javascript:void(0);" name="btnCancel" id="idBtnCancel" class="btn btn-danger pull-right" style="margin-right: 5px;"">
						<i class="fa fa-ban"></i> Cancel
					</a>
				</div>
			</div>
			<?php
			echo form_close();
			?>
		</div>
	</div>
	<div class="box-footer">
	</div>
	<div class="overlay" id="idBoxFormOverlay" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
</div>