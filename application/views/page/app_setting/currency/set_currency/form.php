<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<div class="box <?php echo $class; ?>" id="idBoxForm">
	<div class="box-header with-border">
		<h3 class="box-title">Form Input Set Currency</h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool btn-collapse" id="idBtnBoxFormSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
				<i class="fa fa-minus"></i>
			</button>
			<button class="btn btn-box-tool btn-remove" id="idBtnBoxFormClose" data-widget="remove" data-toggle="tooltip" title="Close">
				<i class="fa fa-times"></i>
			</button>
		</div>
	</div>

	<div class="box-body">
		<div id="idBoxFormLoader" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="idBoxFormContent" style="display: none;">
			<?php
			echo form_open('', array('id' => 'idForm', 'class' => 'form-horizontal'));
			?>
			<div id="idErrForm"></div>
			<div class="form-group">
				<label for="idTxtNama" class="col-md-2 control-label">Currency Name</label>
				<div class="col-md-3">
					<div id="idErrNama"></div>
					<?php
					echo form_input(array('type' => 'hidden', 'name' => 'txtKd', 'value' => $kd_currency));
					echo form_input(array('name' => 'txtNama', 'id' => 'idTxtNama', 'class' => 'form-control', 'value' => $currency_nm, 'placeholder' => 'Currency Name'));
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="idSelType" class="col-md-2 control-label">Currency Type</label>
				<div class="col-md-2">
					<div id="idErrType"></div>
					<?php
					$pil = array('' => '-- Pilih Currency Type --', 'primary' => 'Primary', 'secondary' => 'Secondary');
					$attr = array('id' => 'idSelType', 'class' => 'form-control');
					echo form_dropdown('selType', $pil, $currency_type, $attr);
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="idSelTipe" class="col-md-2 control-label">Icon Type</label>
				<div class="col-md-2">
					<div id="idErrTipe"></div>
					<?php
					$pil = array('' => '-- Pilih Icon Type --', 'icon' => 'Icon', 'text' => 'Text');
					$attr = array('id' => 'idSelTipe', 'class' => 'form-control');
					echo form_dropdown('selTipe', $pil, $icon_type, $attr);
					?>
				</div>
			</div>
			<div id="idFormIcon"><?php echo $currency_icon; ?></div>
			<div class="form-group">
				<label for="idTxtPemisah" class="col-md-2 control-label">Pemisah Angka</label>
				<div class="col-md-1">
					<div id="idErrPemisah"></div>
					<?php
					echo form_input(array('name' => 'txtPemisah', 'id' => 'idTxtPemisah', 'class' => 'form-control', 'value' => $pemisah_angka, 'placeholder' => 'Char'));
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="idTxtFormatAkhir" class="col-md-2 control-label">Format Akhir</label>
				<div class="col-md-1">
					<div id="idErrFormatAkhir"></div>
					<?php
					echo form_input(array('name' => 'txtFormatAkhir', 'id' => 'idTxtFormatAkhir', 'class' => 'form-control', 'value' => $format_akhir, 'placeholder' => 'Char'));
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="idSelDecimal" class="col-md-2 control-label">Decimal</label>
				<div class="col-md-2">
					<div id="idErrDecimal"></div>
					<?php
					echo form_dropdown('selDecimal', array('' => '--Pilih Setting--', '0' => 'Tidak', '1' => 'Ya'), $decimal, array('id' => 'idSelDecimal', 'class' => 'form-control'));
					?>
				</div>
			</div>
			<div class="box-footer">
				<div class="col-xs-4 pull-left">
					<button type="submit" name="btnSubmit" id="idBtnSubmit" class="btn btn-info pull-right" style="margin-right: 5px;"">
						<i class="fa fa-save"></i> Simpan Format
					</button>
					<a href="javascript:void(0);" name="btnCancel" id="idBtnCancel" class="btn btn-danger pull-right" style="margin-right: 5px;"">
						<i class="fa fa-ban"></i> Cancel
					</a>
				</div>
			</div>
			<?php
			echo form_close();
			?>
		</div>
	</div>
	<div class="box-footer">
	</div>
	<div class="overlay" id="idBoxFormOverlay" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
</div>