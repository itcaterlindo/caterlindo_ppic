<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	lihat_data();
	first_load('idBoxTableLoader', 'idBoxTableContent');

	$(document).off('click', '#idBtnBoxTableTambah').on('click', '#idBtnBoxTableTambah', function() {
		$('#idBtnBoxTableTambah').slideUp();
		open_form('');
	});

	$(document).off('click', '#idBtnBoxFormClose').on('click', '#idBtnBoxFormClose', function(){
		$('#idBtnBoxTableTambah').slideDown();
	});

	$(document).off('change', '#idSelType').on('change', '#idSelType', function() {
		check_type($(this).val());
	});

	$(document).off('change', '#idSelTipe').on('change', '#idSelTipe', function() {
		currency_form($(this).val(), '');
	});

	$(document).off('submit', '#idForm').on('submit', '#idForm', function(e) {
		e.preventDefault();
		submit_form(this);
	});

	$(document).off('click', '#idBtnCancel').on('click', '#idBtnCancel', function() {
		close_form();
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function lihat_data() {
		$('#idBoxTableContent').hide();
		$('#idBoxTableContent').html('');
		$('#idBoxTableLoader').show();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/open_table'; ?>',
			success: function(table) {
				$('#idBoxTableContent').html(table);
				$('#idBoxTableLoader').hide();
				moveTo('idMainContent');
			}
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function open_form(kode) {
		$('#idAlertTable').html('');
		$('#idBoxForm').remove();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/open_form'; ?>',
			data: 'primary_code='+kode,
			success: function(form) {
				$('#idMainContent').prepend(form);
				first_load('idBoxFormLoader', 'idBoxFormContent');
				moveTo('idMainContent');
			}
		});
	}

	function check_type(type) {
		$.ajax({
			url: '<?php echo base_url().$class_link.'/check_type'; ?>',
			data: 'type='+type,
			success: function(data) {
				if (data.confirm == 'success') {
					$('#idErrType').html(data.alert);
					$('#idBtnSubmit').removeAttr('disabled');
				} else if (data.confirm == 'error') {
					$('#idErrType').html(data.alert);
					$('#idBtnSubmit').attr('disabled', true);
				}
			}
		});
	}

	function currency_form(tipe, icon) {
		$.ajax({
			url: '<?php echo base_url().$class_link.'/form_simbol/tipe/icon'; ?>',
			data: 'tipe='+tipe+'&icon='+icon,
			success: function(form) {
				$('#idFormIcon').slideUp(function() {
					$('#idFormIcon').html(form);
					$('#idFormIcon').slideDown();
				});
			}
		});
	}

	function submit_form(form_id) {
		$('#idBoxFormOverlay').show();
		$.ajax({
			url: "<?php echo base_url().$class_link.'/submit_form/'; ?>",
			type: "POST",
			data:  new FormData(form_id),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idAlertTable').html(data.alert);
					$('#idBoxFormOverlay').hide();
					close_form();
				} else if (data.confirm == 'error') {
					<?php
					foreach ($form_error as $data) :
						echo '$(\'#'.$data.'\').html(data.'.$data.');'."\n";
					endforeach;
					?>
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idBoxFormOverlay').hide();
				}
			}
		});
	}

	function close_form() {
		$('#idBoxFormContent').slideUp(function(e) {
			$('#idBoxForm').slideUp(function() {
				$('#idBoxForm').remove();
			});
		});
		lihat_data();
		first_load('idBoxTableLoader', 'idBoxTableContent');
		$('#idBtnBoxTableTambah').slideDown();
		moveTo('idMainContent');
	}

	function edit_data(id) {
		open_form(id);
		$('#idBtnBoxTableTambah').slideDown();
	}

	function hapus_data(kode) {
		$('#idBoxTableOverlay').show();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/delete_data'; ?>',
			data: 'kode='+kode,
			success: function(data) {
				$('#idBoxTableOverlay').hide();
				$('#idAlertTable').html(data.alert);
				lihat_data();
			}
		});
	}
</script>