<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<table id="idTable" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
	<thead>
		<tr>
			<th style="width:5%; text-align:center;" class="all">No.</th>
			<th style="width:5%; text-align:center;" class="all">Opsi</th>
			<th style="width:50%; text-align:center;" class="never">Nama Harga</th>
			<th style="width:30%; text-align:center;" class="all">Currency</th>
			<th style="width:15%; text-align:center;" class="all">Flag</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($table_view as $tbl_row) :
			echo $tbl_row;
		endforeach;
		?>
	</tbody>
</table>