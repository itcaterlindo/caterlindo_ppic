<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<div class="box <?php echo $class; ?>" id="idBoxForm">
	<div class="box-header with-border">
		<h3 class="box-title">Form Input Currency Convert</h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool btn-collapse" id="idBtnBoxFormSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
				<i class="fa fa-minus"></i>
			</button>
			<button class="btn btn-box-tool btn-remove" id="idBtnBoxFormClose" data-widget="remove" data-toggle="tooltip" title="Close">
				<i class="fa fa-times"></i>
			</button>
		</div>
	</div>

	<div class="box-body">
		<div id="idBoxFormLoader" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="idBoxFormContent" style="display: none;">
			<?php
			echo form_open('', array('id' => 'idForm', 'class' => 'form-horizontal'));
			?>
			<div id="idErrForm"></div>
			<div class="form-group">
				<label for="idTxtNmTipe" class="col-md-2 control-label">Nama Tipe Harga</label>
				<div class="col-md-3">
					<div id="idErrTipe"></div>
					<?php
					echo form_input(array('name' => 'txtNmTipe', 'id' => 'idTxtNmTipe', 'class' => 'form-control', 'value' => $nm_harga, 'placeholder' => 'Nama Tipe Harga', 'readonly' => 'readonly'));
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="idSelCurrency" class="col-md-2 control-label">Nama Currency</label>
				<div class="col-md-3">
					<div id="idErrCurrency"></div>
					<?php
					$pil = array('' => '-- Pilih Currency --');
					$result = $this->tm_currency->get_all();
					foreach ($result as $row) :
						$pil[$row->kd_currency] = $row->currency_nm.' - '.$row->currency_type;
					endforeach;
					$attr = array('id' => 'idSelCurrency', 'class' => 'form-control');
					echo form_dropdown('selCurrency', $pil, $currency_kd, $attr);
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="idSelFlag" class="col-md-2 control-label">Flag</label>
				<div class="col-md-3">
					<div id="idErrFlag"></div>
					<?php
					$pil = array('' => '-- Pilih Flag --', 'active' => 'Active', 'inactive' => 'Inactive');
					$attr = array('id' => 'idSelFlag', 'class' => 'form-control');
					echo form_dropdown('selFlag', $pil, $row_flag, $attr);
					?>
				</div>
			</div>
			<div class="box-footer">
				<div class="col-xs-4 pull-left">
					<button type="submit" name="btnSubmit" id="idBtnSubmit" class="btn btn-info pull-right" style="margin-right: 5px;"">
						<i class="fa fa-save"></i> Simpan
					</button>
					<a href="javascript:void(0);" name="btnCancel" id="idBtnCancel" class="btn btn-danger pull-right" style="margin-right: 5px;"">
						<i class="fa fa-ban"></i> Cancel
					</a>
				</div>
			</div>
			<?php
			echo form_close();
			?>
		</div>
	</div>
	<div class="box-footer">
	</div>
	<div class="overlay" id="idBoxFormOverlay" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
</div>