<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
	lihat_data();
	first_load('idBoxTableLoader', 'idBoxTableContent');

	$(document).off('submit', '#idForm').on('submit', '#idForm', function(e) {
		e.preventDefault();
		submit_form(this);
	});

	$(document).off('click', '#idBtnCancel').on('click', '#idBtnCancel', function() {
		close_form();
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function lihat_data() {
		$('#idBoxTableContent').hide();
		$('#idBoxTableContent').html('');
		$('#idBoxTableLoader').show();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/open_table'; ?>',
			success: function(table) {
				$('#idBoxTableContent').html(table);
				$('#idBoxTableContent').slideDown();
				$('#idBoxTableLoader').hide();
				moveTo('idMainContent');
			}
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function open_form(kode) {
		$('#idAlertTable').html('');
		$('#idBoxForm').remove();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/open_form'; ?>',
			data: 'primary_code='+kode,
			success: function(form) {
				$('#idMainContent').prepend(form);
				first_load('idBoxFormLoader', 'idBoxFormContent');
				moveTo('idMainContent');
			}
		});
	}

	function submit_form(form_id) {
		$('#idBoxFormOverlay').show();
		$.ajax({
			url: "<?php echo base_url().$class_link.'/submit_form/'; ?>",
			type: "POST",
			data:  new FormData(form_id),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idAlertTable').html(data.alert);
					$('#idBoxFormOverlay').hide();
					close_form();
				} else if (data.confirm == 'error') {
					<?php
					foreach ($form_error as $data) :
						echo '$(\'#'.$data.'\').html(data.'.$data.');'."\n";
					endforeach;
					?>
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idBoxFormOverlay').hide();
				}
			}
		});
	}

	function close_form() {
		$('#idBoxFormContent').slideUp(function(e) {
			$('#idBoxForm').slideUp(function() {
				$('#idBoxForm').remove();
			});
		});
		lihat_data();
		first_load('idBoxTableLoader', 'idBoxTableContent');
		$('#idBtnBoxTableTambah').slideDown();
		moveTo('idMainContent');
	}

	function edit_data(id) {
		open_form(id);
		$('#idBtnBoxTableTambah').slideDown();
	}

	function hapus_data(kode, flag) {
		$('#idBoxTableOverlay').show();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/set_flag'; ?>',
			data: 'kode='+kode+'&flag='+flag,
			success: function(data) {
				$('#idBoxTableOverlay').hide();
				$('#idAlertTable').html(data.alert);
				lihat_data();
			}
		});
	}
</script>