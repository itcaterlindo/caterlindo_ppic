<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$no_btn_close = TRUE;
?>

<div class="box box-primary" id="idBoxTable">
	<div class="box-header with-border">
		<h3 class="box-title">Tabel Set Currency</h3>
		<div class="box-tools pull-right">
			<?php
			if ($this->session->create_access == '1') :
				?>
				<button class="btn btn-box-tool" id="idBtnBoxTableTambah" data-toggle="tooltip" title="Tambah">
					<i class="fa fa-plus"></i>
				</button>
				<?php
			endif;
			?>
			<button class="btn btn-box-tool btn-collapse" id="idBtnBoxTableSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
				<i class="fa fa-minus"></i>
			</button>
			<?php
			if ($no_btn_close != TRUE) :
				?>
				<button class="btn btn-box-tool btn-remove" id="idBtnBoxTableClose" data-widget="remove" data-toggle="tooltip" title="Close">
					<i class="fa fa-times"></i>
				</button>
				<?php
			endif;
			?>
		</div>
	</div>
	<div class="box-body">
		<div id="idBoxTableLoader" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="idAlertTable"></div>
		<div id="idBoxTableContent" style="display: none;"></div>
	</div>
	<div class="box-footer">
	</div>
	<div class="overlay" id="idBoxTableOverlay" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
</div>

<?php
$this->load->view('page/'.$class_link.'/script_js', $form_data);