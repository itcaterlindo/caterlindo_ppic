<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<div class="box <?php echo $class; ?>" id="idBoxForm">
	<div class="box-header with-border">
		<h3 class="box-title">Form Input Currency List</h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool btn-collapse" id="idBtnBoxFormSembunyi" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
				<i class="fa fa-minus"></i>
			</button>
			<button class="btn btn-box-tool btn-remove" id="idBtnBoxFormClose" data-widget="remove" data-toggle="tooltip" title="Close">
				<i class="fa fa-times"></i>
			</button>
		</div>
	</div>

	<div class="box-body">
		<div id="idBoxFormLoader" align="middle">
			<i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
		</div>
		<div id="idBoxFormContent" style="display: none;">
			<?php
			echo form_open('', array('id' => 'idForm', 'class' => 'form-horizontal'));
			?>
			<div id="idErrForm"></div>
			<div class="form-group">
				<label for="idTxtNama" class="col-md-2 control-label">Currency Name</label>
				<div class="col-md-3">
					<div id="idErrNama"></div>
					<?php
					echo form_input(array('type' => 'hidden', 'name' => 'txtKd', 'value' => $id));
					echo form_input(array('name' => 'txtNama', 'id' => 'idTxtNama', 'class' => 'form-control', 'value' => $nm_currency, 'placeholder' => 'Currency Name'));
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="idTxtIcon" class="col-md-2 control-label">Icon Currency</label>
				<div class="col-md-2">
					<div id="idErrIcon"></div>
					<?php echo form_input(array('name' => 'txtIcon', 'id' => 'idTxtIcon', 'class' => 'form-control', 'value' => $icon_currency, 'placeholder' => 'Currency Icon')); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="idTxtUnicode" class="col-md-2 control-label">Icon Unicode</label>
				<div class="col-md-2">
					<div id="idErrUnicode"></div>
					<?php echo form_input(array('name' => 'txtUnicode', 'id' => 'idTxtUnicode', 'class' => 'form-control', 'value' => $icon_unicode, 'placeholder' => 'Icon Unicode')); ?>
				</div>
			</div>
			<div class="box-footer">
				<div class="col-xs-4 pull-left">
					<button type="submit" name="btnSubmit" id="idBtnSubmit" class="btn btn-info pull-right" style="margin-right: 5px;"">
						<i class="fa fa-save"></i> Simpan Format
					</button>
					<a href="javascript:void(0);" name="btnCancel" id="idBtnCancel" class="btn btn-danger pull-right" style="margin-right: 5px;"">
						<i class="fa fa-ban"></i> Cancel
					</a>
				</div>
			</div>
			<?php
			echo form_close();
			?>
		</div>
	</div>
	<div class="box-footer">
	</div>
	<div class="overlay" id="idBoxFormOverlay" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>
</div>