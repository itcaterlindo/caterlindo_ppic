<?php
defined('BASEPATH') or exit('No direct script access allowed!');

echo form_open('', array('id' => 'idForm', 'class' => 'form-horizontal'));
?>
<div class="form-group">
	<label for="idTxtNoForm" class="col-md-2 control-label">No Form</label>
	<div class="col-md-4">
		<div id="idErrNoForm"></div>
		<?php echo form_input(array('type' => 'hidden', 'name' => 'txtLaporanFor', 'id' => 'idTxtLaporanFor', 'value' => $page_title)); ?>
		<?php echo form_input(array('name' => 'txtNoForm', 'id' => 'idTxtNoForm', 'class' => 'form-control', 'readonly' => 'true', 'value' => $no_form)); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtLaporanTitle" class="col-md-2 control-label">Laporan Title</label>
	<div class="col-md-8">
		<div id="idErrLaporanTitle"></div>
		<?php echo form_textarea(array('name' => 'txtLaporanTitle', 'id' => 'idTxtLaporanTitle', 'class' => 'form-control', 'placeholder' => 'Laporan Title', 'rows' => '3', 'readonly' => 'true', 'value' => $laporan_title)); ?>
	</div>
</div>
<div class="form-group">
	<label for="idTxtLaporanFooter" class="col-md-2 control-label">Laporan Footer</label>
	<div class="col-md-8">
		<div id="idErrLaporanFooter"></div>
		<?php echo form_textarea(array('name' => 'txtLaporanFooter', 'id' => 'idTxtLaporanFooter', 'class' => 'form-control', 'placeholder' => 'Laporan Footer', 'rows' => '3', 'readonly' => 'true', 'value' => $laporan_footer)); ?>
	</div>
</div>
<div class="box-footer">
	<div class="col-xs-4 pull-left">
		<div id="idBtnGroupOpen">
			<a href="javascript:void(0);" name="btnUbah" id="idBtnUbah" class="btn btn-warning pull-right" style="margin-right: 100px;"">
				<i class="fa fa-pencil"></i> Ubah Format
			</a>
		</div>
		<div id="idBtnGroupSubmit" style="display: none;">
			<button type="submit" name="btnSubmit" class="btn btn-info pull-right" style="margin-right: 5px;"">
				<i class="fa fa-save"></i> Simpan Format
			</button>
			<a href="javascript:void(0);" name="btnCancel" id="idBtnCancel" class="btn btn-danger pull-right" style="margin-right: 5px;"">
				<i class="fa fa-ban"></i> Cancel
			</a>
		</div>
	</div>
</div>
<?php echo form_close(); ?>