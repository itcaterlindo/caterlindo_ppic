<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
    statetable_main('<?php echo isset($wf_kd) ? $wf_kd : null; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		edit_data('<?php echo isset($wf_kd) ? $wf_kd : null;  ?>', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function statetable_main(wf_kd){
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/statetable_main'; ?>',
				data: {wf_kd: wf_kd},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
				}
			});
		});
	}

	function edit_data(wf_kd = null, id = null) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/stateform_main'; ?>',
			data: {id: id, wf_kd: wf_kd},
			success: function(html) {
				toggle_modal('Form', html);
				$('.my-colorpicker2').colorpicker();
			}
		});
	}

	function submitState(form_id) {
		event.preventDefault();
		var id_form = document.getElementById(form_id);
        let url = "<?php echo base_url().$class_link; ?>/action_submit_state";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(id_form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					$('.errInput').html('');
					toggle_modal('', '');
					statetable_main('<?php echo isset($wf_kd) ? $wf_kd : null; ?>');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function delete_data (id) {
		event.preventDefault();
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete_state'; ?>',
				data: {id: id},
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						statetable_main('<?php echo isset($wf_kd) ? $wf_kd : null; ?>');
					}else{
						notify (resp.status, resp.pesan, 'error');
					}
				},
				error: function (request, status, error) {
					notify ('Error', error, 'error');
				}
			});
		}
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }
</script>