<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
if (isset($row)){
    extract($row);
}
$form_id = 'idFormTransition';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtwftransition_kd', 'id' => 'idtxtwftransition_kd', 'placeholder' => 'Id', 'value' => isset($id) ? $id : null ));
echo form_input(array('type' => 'hidden', 'name' => 'txtwf_kd', 'placeholder' => 'wf_kd', 'value' => isset($wf_kd) ? $wf_kd : null ));
?>
<style type="text/css">
	.select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>
<div class="col-md-12">

    <div class="row">
        <div class="form-group">
            <label for='idtxtwftransition_active' class="col-md-2 control-label">Active</label>
            <div class="col-sm-3 col-xs-12">
                <div class="errInput" id="idErrwftransition_active"></div>
                <?php echo form_dropdown('txtwftransition_active', ['1' => 'Ya', '0' => 'Tidak'], isset($wftransition_active)? $wftransition_active : '', array('class'=> 'form-control', 'id'=> 'idtxtwftransition_active'));?>
            </div>
        </div>

        <div class="form-group">
            <label for='idtxtwftransition_nama' class="col-md-2 control-label">Transition</label>
            <div class="col-sm-8 col-xs-12">
                <div class="errInput" id="idErrwftransition_nama"></div>
                <?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtwftransition_nama', 'id'=> 'idtxtwftransition_nama', 'placeholder' =>'Transition', 'value'=> isset($wftransition_nama) ? $wftransition_nama: null ));?>
            </div>
        </div>

        <div class="form-group">
            <label for='idtxtwftransition_source' class="col-md-2 control-label">Dari</label>
            <div class="col-sm-4 col-xs-12">
                <div class="errInput" id="idErrwftransition_source"></div>
                <?php echo form_dropdown('txtwftransition_source', isset($opsiState) ? $opsiState : [], isset($wftransition_source)? $wftransition_source : '', array('class'=> 'form-control', 'id'=> 'idtxtwftransition_source'));?>
            </div>
        </div>

        <div class="form-group">
            <label for='idtxtwftransition_destination' class="col-md-2 control-label">Ke</label>
            <div class="col-sm-4 col-xs-12">
                <div class="errInput" id="idErrwftransition_destination"></div>
                <?php echo form_dropdown('txtwftransition_destination', isset($opsiState) ? $opsiState : [], isset($wftransition_destination)? $wftransition_destination : '', array('class'=> 'form-control', 'id'=> 'idtxtwftransition_destination'));?>
            </div>
        </div>

        <div class="form-group">
            <label for='idtxtwftransition_action' class="col-md-2 control-label">Action</label>
            <div class="col-sm-8 col-xs-12">
                <div class="errInput" id="idErrwftransition_action"></div>
                <?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtwftransition_action', 'id'=> 'idtxtwftransition_action', 'placeholder' =>'Action', 'value'=> isset($wftransition_action) ? $wftransition_action: null ));?>
            </div>
        </div>

        <hr>

        <div class="form-group">
            <label for='idtxtwftransition_email' class="col-md-2 control-label">Notif Email</label>
            <div class="col-sm-4 col-xs-12">
                <div class="errInput" id="idErrwftransition_email"></div>
                <?php echo form_dropdown('txtwftransition_email', ['0' => 'Tidak', '1' => 'Ya', ], isset($wftransition_email)? $wftransition_email : '', array('class'=> 'form-control', 'id'=> 'idtxtwftransition_email'));?>
            </div>
        </div>

        <div class="form-group">
            <label for='idtxtwftransitionnotification_to' class="col-md-2 control-label">Email To</label>
            <div class="col-sm-8 col-xs-12">
                <div class="errInput" id="idErrwftransitionnotification_to"></div>
                <?php echo form_input(array('type'=>'text', 'class'=> 'form-control mailClass', 'name'=> 'txtwftransitionnotification_to', 'id'=> 'idtxtwftransitionnotification_to', 'placeholder' =>'Email To', 'value'=> isset($wftransitionnotification_to) ? $wftransitionnotification_to: null ));?>
            </div>
        </div>

        <div class="form-group">
            <label for='idtxtwftransitionnotification_cc' class="col-md-2 control-label">Email CC</label>
            <div class="col-sm-8 col-xs-12">
                <div class="errInput" id="idErrwftransitionnotification_cc"></div>
                <?php echo form_input(array('type'=>'text', 'class'=> 'form-control mailClass', 'name'=> 'txtwftransitionnotification_cc', 'id'=> 'idtxtwftransitionnotification_cc', 'placeholder' =>'Email CC', 'value'=> isset($wftransitionnotification_cc) ? $wftransitionnotification_cc: null ));?>
            </div>
        </div>

        <div id="idpermissiontable"></div>
    </div>

    <hr>
    
    <div class="row">
        <div class="form-group pull-right">
            <div class="col-sm-1 col-xs-12">
                <button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitTransition('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
                    <i class="fa fa-save"></i> Simpan
                </button>
            </div>
        </div>
    </div>

</div>

<?php echo form_close(); ?>

<script type="text/javascript">
    mailClass ('<?php echo isset($wftransition_email) ? $wftransition_email : 0 ?>');
</script>