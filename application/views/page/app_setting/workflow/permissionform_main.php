<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
if (isset($row)){
    extract($row);
}
$form_id = 'idFormTransition';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtwftransition_kd', 'id' => 'idtxtwftransition_kd', 'placeholder' => 'Id', 'value' => isset($id) ? $id : null ));
echo form_input(array('type' => 'hidden', 'name' => 'txtwf_kd', 'placeholder' => 'wf_kd', 'value' => isset($wf_kd) ? $wf_kd : null ));
?>
<style type="text/css">
	.select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>
<div class="col-md-12">

    <div class="row">
        <div class="form-group">
            <label for='idtxtwftransitionpermission_adminkd' class="col-md-2 control-label">Permission User</label>
            <div class="col-sm-5 col-xs-12">
                <div class="errInput" id="idErrwftransitionpermission_adminkd"></div>
                <?php echo form_dropdown('txtwftransitionpermission_adminkd', isset($opsiUser) ? $opsiUser : [], isset($wftransitionpermission_adminkd)? $wftransitionpermission_adminkd : '', array('class'=> 'form-control select2', 'id'=> 'idtxtwftransitionpermission_adminkd'));?>
            </div>
        </div>
        <div class="form-group">
            <label for='idtxtkd_tipe_admin' class="col-md-2 control-label">Permission Tipe User</label>
            <div class="col-sm-5 col-xs-12">
                <div class="errInput" id="idErrkd_tipe_admin"></div>
                <?php echo form_dropdown('txtkd_tipe_admin', isset($opsiUsertype) ? $opsiUsertype : [], isset($kd_tipe_admin)? $kd_tipe_admin : '', array('class'=> 'form-control select2', 'id'=> 'idtxtkd_tipe_admin'));?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-1 col-sm-offset-10 col-xs-12">
                <button class="btn btn-sm btn-success" onclick="submitPermission()"> <i class="fa fa-plus"></i> Tambah </button>
            </div>
        </div>

        <div id="idpermissiontable"></div>
    </div>
</div>

<?php echo form_close(); ?>

<script type="text/javascript">
    mailClass ('<?php echo isset($wftransition_email) ? $wftransition_email : 0 ?>');
</script>