<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

<table id="idTable" class="table table-bordered table-striped table-hover display responsive" style="width:100%;">
	<thead>
	<tr>
		<th style="width:1%; text-align:center;" class="all">No.</th>
		<th style="width:4%; text-align:center;" class="all">Opsi</th>
		<th style="width:10%; text-align:center;">Admin</th>
		<th style="width:5%; text-align:center;">Tipe Admin</th>
		<th style="width:5%; text-align:center;">Last Update</th>
	</tr>
	</thead>
    <tbody>
    <?php 
    $no = 1;
    foreach ($result as $each) :?>
    <tr>
        <td><?php echo $no; ?></td>
        <td class="dt-center"> <button class="btn btn-xs btn-danger" title="Hapus Item" onclick="hapus_permission('<?php echo $each['wftransition_kd']?>','<?php echo $each['wftransitionpermission_kd']; ?>')"> <i class="fa fa-trash"></i> </button> </td>
        <td><?php echo $each['nm_admin']; ?></td>
        <td><?php echo $each['nm_tipe_admin']; ?></td>
        <td><?php echo $each['wftransitionpermission_tgledit']; ?></td>
    </tr>
    <?php 
    $no++;
    endforeach;?>
    </tbody>
</table>

<script type="text/javascript">
	
</script>