<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
if (isset($row)){
    extract($row);
}
$form_id = 'idFormState';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'hidden', 'name' => 'txtwfstate_kd', 'placeholder' => 'Id', 'value' => isset($id) ? $id : null ));
echo form_input(array('type' => 'hidden', 'name' => 'txtwf_kd', 'placeholder' => 'wf_kd', 'value' => isset($wf_kd) ? $wf_kd : null ));
?>
<div class="col-md-12">

    <div class="row">
        <div class="form-group">
            <label for='idtxtwfstate_nama' class="col-md-2 control-label">State</label>
            <div class="col-sm-8 col-xs-12">
                <div class="errInput" id="idErrwfstate_nama"></div>
                <?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtwfstate_nama', 'id'=> 'idtxtwfstate_nama', 'placeholder' =>'State', 'value'=> isset($wfstate_nama) ? $wfstate_nama: null ));?>
            </div>
        </div>

        <div class="form-group">
            <label for='idtxtwfstate_initial' class="col-md-2 control-label">Initial</label>
            <div class="col-sm-4 col-xs-12">
                <div class="errInput" id="idErrwfstate_initial"></div>
                <?php echo form_dropdown('txtwfstate_initial', ['0' => 'Tidak', '1' => 'Ya'], isset($wfstate_initial)? $wfstate_initial : '', array('class'=> 'form-control', 'id'=> 'idtxtwfstate_initial'));?>
            </div>
        </div>

        <div class="form-group">
            <label for='idtxtwfstate_badgecolor' class="col-md-2 control-label">Badge Color</label>
            <div class="col-sm-4 col-xs-12 colorpickeraddon">
                <div class="errInput" id="idErrwfstate_badgecolor"></div>
                    <div class="input-group my-colorpicker2">
                        <?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtwfstate_badgecolor', 'id'=> 'idtxtwfstate_badgecolor', 'placeholder' =>'Badge', 'value'=> isset($wfstate_badgecolor) ? $wfstate_badgecolor: null ));?>
                        <div class="input-group-addon">
                            <i></i>
                        </div>
                    </div>
            </div>
        </div>

        <div class="form-group">
            <label for='idtxtwfstate_active' class="col-md-2 control-label">Aktif</label>
            <div class="col-sm-4 col-xs-12">
                <div class="errInput" id="idErrwfstate_active"></div>
                <?php echo form_dropdown('txtwfstate_active', ['1' => 'Ya', '0' => 'Tidak'], isset($wfstate_active)? $wfstate_active : '', array('class'=> 'form-control', 'id'=> 'idtxtwfstate_active'));?>
            </div>
        </div>

        <div class="form-group">
            <label for='idtxtwfstate_onprintout' class="col-md-2 control-label">Display Printout</label>
            <div class="col-sm-4 col-xs-12">
                <div class="errInput" id="idErrwfstate_onprintout"></div>
                <?php echo form_dropdown('txtwfstate_onprintout', ['0' => 'Tidak', '1' => 'Ya'], isset($wfstate_onprintout)? $wfstate_onprintout : '', array('class'=> 'form-control', 'id'=> 'idtxtwfstate_onprintout'));?>
            </div>
        </div>

        <div class="form-group">
            <label for='idtxtwfstate_onprintoutseq' class="col-md-2 control-label">Printout Seq</label>
            <div class="col-sm-3 col-xs-12">
                <div class="errInput" id="idErrwfstate_onprintoutseq"></div>
                <?php echo form_input(array('type'=>'number', 'class'=> 'form-control', 'name'=> 'txtwfstate_onprintoutseq', 'id'=> 'idtxtwfstate_onprintoutseq', 'placeholder' =>'Printout Seq', 'value'=> isset($wfstate_onprintoutseq) ? $wfstate_onprintoutseq: null ));?>
            </div>
        </div>

        <div class="form-group">
            <label for='idtxtwfstate_action' class="col-md-2 control-label">Action</label>
            <div class="col-sm-8 col-xs-12">
                <div class="errInput" id="idErrwfstate_action"></div>
                <?php echo form_input(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtwfstate_action', 'id'=> 'idtxtwfstate_action', 'placeholder' =>'Action', 'value'=> isset($wfstate_action) ? $wfstate_action: null ));?>
            </div>
        </div>

    </div>

    <hr>

    <div class="row">
        <div class="form-group pull-right">
            <div class="col-sm-1 col-xs-12">
                <button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitState('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
                    <i class="fa fa-save"></i> Simpan
                </button>
            </div>
        </div>
    </div>

</div>

<?php echo form_close(); ?>
