<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>
<script type="text/javascript">
    transitiontable_main('<?php echo isset($wf_kd) ? $wf_kd : null; ?>');
	first_load('<?php echo $box_loader_id; ?>', '<?php echo $box_content_id; ?>');

	$(document).off('click', '#<?php echo $btn_add_id; ?>').on('click', '#<?php echo $btn_add_id; ?>', function() {
		edit_data('<?php echo isset($wf_kd) ? $wf_kd : null;  ?>', '');
	});

	$(document).off('change', '#idtxtwftransition_email').on('change', '#idtxtwftransition_email', function() {
		let val = $(this).val();
		mailClass (val);
	});

	function mailClass (val = 0) {
		if (val == 1){
			$('.mailClass').prop('disabled', false);
		}else{
			$('.mailClass').prop('disabled', true);
		}
	}

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function transitiontable_main(wf_kd){
		$('#<?php echo $btn_add_id; ?>').slideDown();
		$('#<?php echo $box_content_id; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/transitiontable_main'; ?>',
				data: {wf_kd: wf_kd},
				success: function(html) {
					$('#<?php echo $box_content_id; ?>').html(html);
					$('#<?php echo $box_content_id; ?>').slideDown();
				}
			});
		});
	}

	function hapus_permission (wftransition_kd, id){
		event.preventDefault();
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete_permission'; ?>',
				data: {id: id},
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						permissiontable_main (wftransition_kd);
					}else{
						notify (resp.status, resp.pesan, 'error');
					}
				},
				error: function (request, status, error) {
					notify ('Error', error, 'error');
				}
			});
		}
	}

	function edit_data(wf_kd = null, id = null) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/transitionform_main'; ?>',
			data: {id: id, wf_kd: wf_kd},
			success: function(html) {
				toggle_modal('Form', html);
			}
		});
	}

	function edit_permission(wf_kd = null, id = null) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/permissionform_main'; ?>',
			data: {id: id, wf_kd: wf_kd},
			success: function(html) {
				toggle_modal('Form', html);
				render_select2('select2');
				permissiontable_main (id);
			}
		});
	}

	function permissiontable_main (id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/permissiontable_main'; ?>',
			data: {id: id},
			success: function(html) {
				$('#idpermissiontable').html(html);
			}
		});
	}

	function action_submit_permission (wftransition_kd = null, wftransitionpermission_adminkd = null, kd_tipe_admin = null) {
		event.preventDefault();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/action_submit_permission'; ?>',
			data: {wftransition_kd: wftransition_kd, wftransitionpermission_adminkd: wftransitionpermission_adminkd, kd_tipe_admin: kd_tipe_admin},
			success: function(data) {
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					permissiontable_main (resp.data.wftransition_kd);
				}else{
					notify (resp.status, resp.pesan, 'success');
				}
				$('.select2').val('').trigger('change');
			}
		});
	}

	function submitPermission () {
		event.preventDefault();
		let wftransition_kd = $('#idtxtwftransition_kd').val(); 
		let wftransitionpermission_adminkd = $('#idtxtwftransitionpermission_adminkd').val(); 
		let kd_tipe_admin = $('#idtxtkd_tipe_admin').val();
		action_submit_permission(wftransition_kd, wftransitionpermission_adminkd, kd_tipe_admin);
	}

	function submitTransition(form_id) {
		event.preventDefault();
		var id_form = document.getElementById(form_id);
        let url = "<?php echo base_url().$class_link; ?>/action_submit_transition";

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(id_form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				var resp = JSON.parse(data);
				if(resp.code == 200){
					notify (resp.status, resp.pesan, 'success');
					$('.errInput').html('');
					toggle_modal('', '');
					transitiontable_main('<?php echo isset($wf_kd) ? $wf_kd : null; ?>');
				}else if (resp.code == 401){
					$.each( resp.pesan, function( key, value ) {
						$('#'+key).html(value);
					});
					generateToken (resp.csrf);
					box_overlay('out');
				}else if (resp.code == 400){
					notify (resp.status, resp.pesan, 'error');
					generateToken (resp.csrf);
				}else{
					notify ('Error', 'Error tidak Diketahui', 'error');
					generateToken (resp.csrf);
				}
			} 	        
		});
	}

	function delete_data (id) {
		event.preventDefault();
		var conf = confirm('Apakah anda yakin ?');
		if (conf) {
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/action_delete_transition'; ?>',
				data: {id: id},
				success: function(data) {
					var resp = JSON.parse(data);
					if(resp.code == 200){
						notify (resp.status, resp.pesan, 'success');
						transitiontable_main('<?php echo isset($wf_kd) ? $wf_kd : null; ?>');
					}else{
						notify (resp.status, resp.pesan, 'error');
					}
				},
				error: function (request, status, error) {
					notify ('Error', error, 'error');
				}
			});
		}
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#idmodal').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#<?php echo $content_modal_id; ?>').slideUp();
		$('#<?php echo $content_modal_id; ?>').html(htmlContent);
		$('#<?php echo $content_modal_id; ?>').slideDown();
	}

	function render_select2(valClass){
		$('.'+valClass).select2({
			theme: 'bootstrap',
			placeholder: '--Pilih Opsi--',
			allowClear : true,
		});
	}

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }
</script>