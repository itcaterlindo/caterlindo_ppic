<?php
defined('BASEPATH') or exit('No direct script script access allowed!');
/* --Masukkan setting properti untuk form-- */
if (isset($row)){
    extract($row);
}
$form_id = 'idForm';
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));
echo form_input(array('type' => 'text', 'name' => 'txtwf_kd', 'placeholder' => 'Id', 'value' => isset($id) ? $id : null ));
?>

<div class="row">
	<div class="form-group">
		<label for="idtxtwf_nama" class="col-md-2 control-label">Nama</label>
		<div class="col-sm-9 col-xs-12">
			<div class="errInput" id="idErrwf_nama"></div>
			<?php echo form_textarea(array('type'=>'text', 'class'=> 'form-control', 'name'=> 'txtwf_nama', 'id'=> 'idtxtwf_nama', 'placeholder' =>'Remark', 'rows' => '2', 'value'=> isset($wf_nama) ? $wf_nama: null ));?>
		</div>
	</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group pull-right">
            <div class="col-sm-1 col-xs-12">
                <button type="submit" name="btnSubmit" id="idbtnSubmitMain" onclick="submitData('<?php echo $form_id; ?>')" class="btn btn-primary btn-sm">
                    <i class="fa fa-save"></i> Simpan
                </button>
            </div>
        </div>
    </div>
</div>

<?php echo form_close(); ?>
