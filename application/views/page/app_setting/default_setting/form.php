<?php
defined('BASEPATH') OR exit('No direct script access allowed!');
$box_id = 'idBoxForm';
$box_title = 'Default App Setting';
// You can set it with 'TRUE/FALSE' or '1/0'
$btn_hide = TRUE;
// You can set it with 'TRUE/FALSE' or '1/0'
$btn_close = FALSE;
$form_id = 'idForm';
$form_err_id = 'idErrForm';
$form_overlay = 'idOverlayForm';
$data = array_combine(array('box_id', 'form_id', 'form_err_id', 'form_overlay'), array($box_id, $form_id, $form_err_id, $form_overlay));
?>


<div class="box box-primary" id="<?php echo $box_id; ?>">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $box_title; ?></h3>
		<div class="box-tools pull-right">
			<?php if ($btn_hide) : ?>
				<button class="btn btn-box-tool" id="idBtnCollapseForm" data-widget="collapse" data-toggle="tooltip" title="Sembunyikan">
					<i class="fa fa-minus"></i>
				</button>
			<?php endif; ?>
			<?php if ($btn_close) : ?>
				<button class="btn btn-box-tool" id="idBtnTutup" data-widget="remove" data-toggle="tooltip" title="Tutup">
					<i class="fa fa-times"></i>
				</button>
			<?php endif; ?>
		</div>
	</div>
	<div class="box-body">
		<div id="<?php echo $form_err_id; ?>"></div>
		<?php
		echo form_open('', array('id' => $form_id, 'class' => 'form-horizontal'));

		foreach ($app_forms as $app_form) :
			?>
			<div class="form-group">
				<label for="idTxt<?php echo $app_form->nm_setting; ?>" class="col-md-2 control-label"><?php echo ucwords(str_replace('_', ' ', $app_form->nm_setting)); ?></label>
				<div class="col-md-<?php echo $app_form->panjang_form; ?>">
					<div id="idErr<?php echo $app_form->nm_setting; ?>"></div>
					<?php echo form_input(array('type' => 'hidden', 'name' => 'txtSettingId[]', 'id' => 'idTxtSettingId', 'value' => $app_form->id)); ?>
					<?php echo form_input(array('name' => 'txtSettingVal[]', 'id' => 'idTxt'.$app_form->nm_setting, 'class' => 'form-control', 'placeholder' => ucwords(str_replace('_', ' ', $app_form->nm_setting)), 'disabled' => '', 'value' => $app_form->nilai_setting)); ?>
				</div>
				<div class="col-md-2">
					<?php echo form_dropdown('selTipeHarga[]', array('-- Pilih Tipe Harga --' => '', 'retail' => 'Harga Retail', 'reseller' => 'Harga Reseller', 'distributor' => 'Harga Distributor', 'ekspor' => 'Harga Ekspor', 'semua' => 'Semua Harga'), $app_form->tipe_harga, array('id' => 'idSel'.$app_form->nm_setting, 'class' => 'form-control', 'disabled' => '')); ?>
				</div>
			</div>
			<?php
		endforeach;
		?>
		<div class="col-xs-12">
			<div class="form-group">
				<div id="idBtnForm">
					<a href="javascript:void(0);" id="idBtnUbah" class="btn btn-warning pull-right">
						<i class="fa fa-cog"></i> Ubah Setting
					</a>
				</div>
				<div id="idGroupBtnAct" style="display: none;">
					<button type="submit" id="idBtnSimpan" class="btn btn-info pull-right">
						<i class="fa fa-save"></i> Simpan
					</button>
					<a href="javascript:void(0);" id="idBtnBatal" class="btn btn-danger pull-right" style="margin-right: 5px;">
						<i class="fa fa-ban"></i> Cancel
					</a>
				</div>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
	<div class="overlay" id="<?php echo $form_overlay; ?>" style="display: none;">
		<i class="fa fa-spinner fa-pulse" style="color:#31708f;"></i>
	</div>

	<?php $this->load->view('script/'.$class_link.'/form_js', $data); ?>
</div>