<?php
defined('BASEPATH') or exit('No direct script script access allowed!');

/* --Masukkan setting properti untuk form-- */
$master_var = 'SettingApp';
$form_id = 'idForm'.$master_var;
echo form_open_multipart('', array('id' => $form_id, 'class' => 'form-horizontal'));

foreach ($result as $row) :
	$jenis_input = $row->type_val == 'text'?'Txt':'File';
	$nm_setting = ucwords(str_replace('_', ' ', $row->nm_setting));
	?>
	<div class="form-group">
		<label for="id<?php echo $jenis_input.$row->nm_setting; ?>" class="col-md-2 control-label"><?php echo $nm_setting; ?></label>
		<div class="col-sm-4 col-xs-12">
			<div id="idErr<?php echo $row->nm_setting; ?>"></div>
			<?php
			if ($row->type_val == 'image' && !empty($row->val_setting)) :
				echo img(array('src' => 'assets/admin_assets/dist/img/setting_img/'.$row->val_setting, 'width' => '50%', 'style' => 'margin-bottom:5px;'));
			endif;
			?>
			<?php
			if ($row->type_val == 'text') :
				echo form_input(array('type' => 'hidden', 'name' => 'txtKdSetting[]', 'value' => $row->id));
				echo form_input(array('name' => 'txtAppSetting['.$row->id.']', 'id' => 'id'.$jenis_input.$row->nm_setting, 'class' => 'form-control', 'placeholder' => 'Nama Rak', 'value' => $row->val_setting));
			elseif ($row->type_val == 'image') :
				echo form_upload(array('name' => 'userfile[]', 'id' => 'id'.$jenis_input.$row->nm_setting, 'class' => 'form-control'));
				echo form_input(array('type' => 'hidden', 'name' => 'txtKdFile[]', 'value' => $row->id));
			endif;
			?>
		</div>
	</div>
	<?php
endforeach;
?>
<hr>
<div class="form-group">
	<div class="col-sm-1 col-sm-offset-2 col-xs-12">
		<button type="reset" name="btnReset" class="btn btn-default btn-flat">
			<i class="fa fa-refresh"></i> Reset
		</button>
	</div>
	<div class="col-sm-1 col-xs-12">
		<button type="submit" name="btnSubmit" class="btn btn-primary btn-flat">
			<i class="fa fa-save"></i> Submit
		</button>
	</div>
</div>
<?php echo form_close(); ?>