<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	$('#idFormInput').unbind().bind('submit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().$class_name.'/submit_form/input'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					formSuccess('idBoxForm', data.alert, 'barang', '');
				}
				if (data.confirm == 'errValidation') {
					$('#idTxtNm').focus();
					$('#idErrNm').html(data.idErrNm);
					$('#idErrPrice').html(data.idErrPrice);
					$('#idErrItem').html(data.idErrItem);
					$('#idErrSetPpn').html(data.idErrSetPpn);
					$('#idErrBarcode').html(data.idErrBarcode);
					$('#idErrTermFormat').html(data.idErrTermFormat);
					$('#idErrTermWaktu').html(data.idErrTermWaktu);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
				}
			} 	        
		});
		
		return false;
	});

	$('#idFormEdit').unbind().bind('submit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().$class_name.'/submit_form/edit'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					formSuccess('idBoxForm', data.alert, 'barang', '');
				}
				if (data.confirm == 'errValidation') {
					$('#idTxtNm').focus();
					$('#idErrNm').html(data.idErrNm);
					$('#idErrPrice').html(data.idErrPrice);
					$('#idErrItem').html(data.idErrItem);
					$('#idErrSetPpn').html(data.idErrSetPpn);
					$('#idErrBarcode').html(data.idErrBarcode);
					$('#idErrTermFormat').html(data.idErrTermFormat);
					$('#idErrTermWaktu').html(data.idErrTermWaktu);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
				}
			}
		});
		
		return false;
	});
</script>