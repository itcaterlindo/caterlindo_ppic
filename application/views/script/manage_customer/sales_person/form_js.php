<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	/* --start karyawan typeahead.js-- */
	var Karyawan = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		url: '<?php echo base_url(); ?>auto_complete/get_karyawan_aktif',
		remote: {
			url: '<?php echo base_url(); ?>auto_complete/get_karyawan_aktif',
			prepare: function (query, settings) {
				settings.type = 'GET';
				settings.contentType = 'application/json; charset=UTF-8';
				settings.data = 'nm_karyawan='+query;

				return settings;
			},
			wildcard: '%QUERY'
		}
	});

	$('#idTxtNmKaryawan').typeahead(null, {
		limit: 50,
		minLength: 1,
		name: 'karyawan_search',
		display: 'nm_karyawan',
		valueKey: 'get_karyawan',
		source: Karyawan.ttAdapter()
	});

	$('#idTxtNmKaryawan').bind('typeahead:select', function(obj, selected) {
		$('#idTxtKdKaryawan').val(selected.kd_karyawan);
		$('#idTxtEmail').val(selected.email);
		$('#idTxtTelp').val(selected.telp);
	});
	/* --end of karyawan typeahead.js-- */

	$('#idFormInput').unbind().bind('submit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().$class_name.'/submit_form/input'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					formSuccess('idBoxForm', data.alert, 'barang', '');
				}
				if (data.confirm == 'errValidation') {
					$('#idTxtNm').focus();
					$('#idErrNm').html(data.idErrNm);
					$('#idErrKaryawan').html(data.idErrKaryawan);
					$('#idErrTelp').html(data.idErrTelp);
					$('#idErrEmail').html(data.idErrEmail);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
					if(data.alert != null){
						notify ('Error API', data.alert, 'error');
					}
				}
			} 	        
		});
		
		return false;
	});

	$('#idFormEdit').unbind().bind('submit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().$class_name.'/submit_form/edit'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					formSuccess('idBoxForm', data.alert, 'barang', '');
				}
				if (data.confirm == 'errValidation') {
					$('#idTxtNm').focus();
					$('#idErrNm').html(data.idErrNm);
					$('#idErrKaryawan').html(data.idErrKaryawan);
					$('#idErrTelp').html(data.idErrTelp);
					$('#idErrEmail').html(data.idErrEmail);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
					if(data.alert != null){
						notify ('Error API', data.alert, 'error');
					}
				}
			}
		});
		
		return false;
	});

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
			delay: 2500,
            styling: 'bootstrap3'
        });	
    }

</script>