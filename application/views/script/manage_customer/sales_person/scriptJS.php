<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	lihatTabel();

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function lihatTabel() {
		$('#idBtnTambah').show();
		$('#idLoaderTable').show();
		$('#idTable').html('');
		$('#idTable').fadeOut('slow', function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_name.'/data_lihat/'; ?>',
				success:function(html){
					$('#idTable').html(html);
				}
			});
			$('#idTable').fadeIn();
			$('#idLoaderTable').hide();
			moveTo('idMainContent');
		});
	}

	function removeForm() {
		$('#idBoxForm').remove();
		$('#idAlertForm').fadeOut();
	}

	function formSuccess(idBox, msg, tabel, add) {
		if ($.isArray(idBox)) {
			$.each(idBox, function(index, value){
				$('#'+value).slideUp(500, function(){
					$('#'+value).remove();
				});
			});
		} else {
			$('#'+idBox).slideUp(500, function(){
				$('#'+idBox).remove();
			});
		}
		if (tabel == 'barang') {
			$('#idAlertTable').html(msg).fadeIn();
			lihatTabel();			
		}
	}

	function showForm(id, link) {
		removeForm();
		$('#idAlertTable').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_name.'/data_form/'; ?>',
			data: 'id='+id+'&link='+link,
			success:function(html){
				$('#idMainContent').prepend(html);
				$('#idTxtNm').focus();
				moveTo('idMainContent');
			}
		});
	}

	function editData(id) {
		showForm(id, 'data_form');
		$('#idAlertTable').fadeOut();
	}

	function showFormNamaKaryawan(check) {
		if (check.is(":checked")) {
			$('#idFormNmKaryawan').slideDown();
			$('#idTxtNmKaryawan').focus();
		} else {
			$('#idTxtKdKaryawan').val('');
			$('#idTxtNmKaryawan').val('');
			$('#idFormNmKaryawan').slideUp();
		}
	}

	$(document).on('click', '#idChkLink', function() {
		var check = $('#idChkLink');
		showFormNamaKaryawan(check);
	});

	function hapusData(id) {
		$('#idTableOverlay>').show();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_name.'/hapus_data'; ?>',
			data: 'id='+id,
			success:function(html){
				$('#idAlertTable').html(html.alert).fadeIn();
				lihatTabel();
				$('#idTableOverlay').hide();
			}
		});
	}

	$(document).on('click', '#idBtnTambah', function(){
		$('#idBtnTambah').hide();
		showForm('');
	});

	$(document).on('click', '#idBtnTutup', function(){
		$('#idBtnTambah').show();
		lihatTabel();
	});
</script>