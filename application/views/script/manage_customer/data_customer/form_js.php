<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	$('[data-mask]').inputmask();
	$('#idTxtNm').keyup(function() {
		var nama = $(this).val();
		var kode = $('[name="txtKd"]').val();
		var code = $('#idTxtKode').val();
		var formID = $('form').attr('id');
		// Jika proses insert maka buat code customer berdasarkan nama, jika proses edit customer code tetap
		if(formID == "idFormInput"){
			$.ajax({
				url: "<?php echo base_url().$class_name.'/buat_kode'; ?>",
				type: "GET",
				data: "nama="+nama+"&kode="+kode+"&code="+code,
				success: function(data){
					$('#idTxtKode').val(data.code_customer);
				}
			});
		}
	});

	$('#idFormInput').unbind().bind('submit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().$class_name.'/submit_form'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
				if (data.confirm == 'success') {
					formSuccess('idBoxForm', data.alert, 'barang', '');
				}
				if (data.confirm == 'errValidation') {
					$('#idSelJenis').focus();
					<?php
					foreach ($form_error as $data) :
						echo '$(\'#'.$data.'\').html(data.'.$data.');'."\n";
					endforeach;
					?>
					$('#idOverlayForm').hide();
					if(data.alert != null){
						notify ('Error API', data.alert, 'error');
					}
				}
			} 	        
		});
		
		return false;
	});

	$('#idFormEdit').unbind().bind('submit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().$class_name.'/submit_form'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
				if (data.confirm == 'success') {
					formSuccess('idBoxForm', data.alert, 'barang', '');
				}
				if (data.confirm == 'errValidation') {
					$('#idSelJenis').focus();
					<?php
					foreach ($form_error as $data) :
						echo '$(\'#'.$data.'\').html(data.'.$data.');'."\n";
					endforeach;
					?>
					$('#idOverlayForm').hide();
					if(data.alert != null){
						notify ('Error API', data.alert, 'error');
					}
				}
			}
		});
		
		return false;
	});

	$(document).on('change', '.idSelNegara', function(event){
		var kd_negara = $(this).val();
		$.ajax({
			url: "<?php echo base_url().$class_name.'/pilih_lokasi'; ?>",
			type: "GET",
			data: "type=negara&kd_negara="+kd_negara,
			success: function(data){
				if(kd_negara != ''){
					$(event.target).closest('.form_alamat').find('.idSelProvinsi').html(data.data_provinsi);
					$(event.target).closest('.form_alamat').find('.idSelKota').html('<option value="">-- Pilih Kota --</option>');
					$(event.target).closest('.form_alamat').find('.idSelKecamatan').html('<option value="">-- Pilih Kecamatan --</option>');
				} else {
					$(event.target).closest('.form_alamat').find('.idSelProvinsi').html('<option value="">-- Pilih Provinsi --</option>');
					$(event.target).closest('.form_alamat').find('.idSelKota').html('<option value="">-- Pilih Kota --</option>');
					$(event.target).closest('.form_alamat').find('.idSelKecamatan').html('<option value="">-- Pilih Kecamatan --</option>');
				}
				$(event.target).closest('.form_alamat').find('.idSelProvinsi').focus();
			}
		});
	});

	$(document).on('change', '.idSelProvinsi', function(event){
		var kd_negara = $(this).closest('.form_alamat').find('.idSelNegara').val();
		var kd_provinsi = $(this).val();
		$.ajax({
			url: "<?php echo base_url().$class_name.'/pilih_lokasi'; ?>",
			type: "GET",
			data: "type=provinsi&kd_negara="+kd_negara+"&kd_provinsi="+kd_provinsi,
			success: function(data){
				if(kd_provinsi != ''){
					$(event.target).closest('.form_alamat').find('.idSelKota').html(data.data_kota);
					$(event.target).closest('.form_alamat').find('.idSelKecamatan').html('<option value="">-- Pilih Kecamatan --</option>');
				} else {
					$(event.target).closest('.form_alamat').find('.idSelKota').html('<option value="">-- Pilih Kota --</option>');
					$(event.target).closest('.form_alamat').find('.idSelKecamatan').html('<option value="">-- Pilih Kecamatan --</option>');
				}
				$(event.target).closest('.form_alamat').find('.idSelKota').focus();
			}
		});
	});

	$(document).on('change', '.idSelKota', function(event){
		var kd_negara = $(this).closest('.form_alamat').find('.idSelNegara').val();
		var kd_provinsi = $(this).closest('.form_alamat').find('.idSelProvinsi').val();
		var kd_kota = $(this).val();
		$.ajax({
			url: "<?php echo base_url().$class_name.'/pilih_lokasi'; ?>",
			type: "GET",
			data: "type=kota&kd_negara="+kd_negara+"&kd_provinsi="+kd_provinsi+"&kd_kota="+kd_kota,
			success: function(data){
				if(kd_kota != ''){
					$(event.target).closest('.form_alamat').find('.idSelKecamatan').html(data.data_kecamatan);
				} else {
					$(event.target).closest('.form_alamat').find('.idSelKecamatan').html('<option value="">-- Pilih Kecamatan --</option>');
				}
				$(event.target).closest('.form_alamat').find('.idSelKecamatan').focus();
			}
		});
	});

	$(document).on('change', '.idSelKecamatan', function(event){
		$(this).closest('.form_alamat').find('.idTxtAlamat').focus();
	});
	
	$(document).off('click', '#idBtnTambahAlamat').on('click', '#idBtnTambahAlamat', function(){
		var jml = $('.form_alamat').length;
		$.ajax({
			url: "<?php echo base_url().$class_name.'/tambah_lokasi'; ?>",
			type: "GET",
			data: "jml="+jml,
			success: function(data){
				$(data.form_child).appendTo($('#form_alamat_child')).attr('style', 'display:none;').slideDown('fast');
				$('.select2').select2({
					theme: 'bootstrap'
				});
				$('[data-mask]').inputmask();
			}
		});
	});

	$(document).on('click', '.idBtnHapusAlamat', function(){
		$(this).closest('.form_alamat').slideUp('fast', function() {
			$(this).closest('.form_alamat').remove();
		});
	});

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
			delay: 2500,
            styling: 'bootstrap3'
        });	
    }
</script>