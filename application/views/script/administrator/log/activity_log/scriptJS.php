<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	lihatTabel();

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function lihatTabel() {
		var lebar = $(window).width();
		$('#idLoaderTable').show();
		$('#idTableLog').html('');
		$('#idTableLog').fadeOut('slow', function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url('administrator/log/activity_log/data_lihat'); ?>',
				data: 'lebar='+lebar,
				success:function(html){
					$('#idTableLog').html(html);
				}
			});
			$('#idTableLog').fadeIn();
			$('#idLoaderTable').hide();
			moveTo('idMainContent');
		});
	}
</script>