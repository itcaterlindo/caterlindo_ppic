<?php
defined('BASEPATH') or exit('No direct script allowed!');
?>
<script type="text/javascript">
	$('#idTxtNmKaryawan').autocomplete({
		source: function( request, response ) {
			$.ajax({
				url : '<?php echo base_url(); ?>auto_complete/fill_text',
				dataType: "json",
				data: {
					name_startsWith: request.term,
					db: 'sim_hrm',
					table: 'tb_karyawan',
					fields: 'nm_karyawan, kd_karyawan',
					search: {'like' : {'nm_karyawan' : request.term}},
				},
				 success: function( data ) {
					 response( $.map( data, function( item ) {
						var code = item.split("|");
						return {
							label: code[0],
							value: code[0],
							data : item
						}
					}));
				}
			});
		},
		autoFocus: true,
		minLength: 3,
		select: function(event, ui) {
			var names = ui.item.data.split("|");
			$('#idTxtKdKaryawan').val(names[1]);
		}
	});
</script>