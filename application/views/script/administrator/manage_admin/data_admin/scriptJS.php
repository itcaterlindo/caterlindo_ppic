<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	lihatTabel();

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function lihatTabel() {
		$('#<?php echo $table_loader; ?>').show();
		$('#<?php echo $table_name; ?>').html('');
		$('#<?php echo $table_name; ?>').fadeOut('slow', function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$view_url; ?>',
				success:function(html){
					$('#<?php echo $table_name; ?>').html(html);
				}
			});
			$('#<?php echo $table_name; ?>').fadeIn();
			$('#<?php echo $table_loader; ?>').hide();
			moveTo('idMainContent');
		});
	}

	function showForm(id) {
		$('#<?php echo $form_name; ?>').remove();
		$('#<?php echo $table_alert; ?>').fadeOut();
		$('#<?php echo $form_alert; ?>').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$form_url; ?>',
			data: 'id='+id,
			success:function(html){
				$('#idMainContent').prepend(html);
				$('#idTxtNmAdmin').focus();
				var check = $('#idChkLink');
				showFormNamaKaryawan(check);
				moveTo('idMainContent');
			}
		});
	}

	function showFormNamaKaryawan(check) {
		if (check.is(":checked")) {
			$('#idFormNmKaryawan').slideDown();
			$('#idTxtNmKaryawan').focus();
		} else {
			$('#idTxtKdKaryawan').val('');
			$('#idTxtNmKaryawan').val('');
			$('#idFormNmKaryawan').slideUp();
		}
	}

	$(document).on('click', '#idChkLink', function() {
		var check = $('#idChkLink');
		showFormNamaKaryawan(check);
	});

	$(document).on('submit', '#<?php echo $form_input; ?>', function(e) {
		$('#<?php echo $form_overlay; ?>').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().$form_input_url; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					$('#<?php echo $form_name; ?>').slideUp(500, function(){
						$('#<?php echo $form_name; ?>').remove();
					});
					$('#idBtnTambah').show();
					$('#<?php echo $table_alert; ?>').html(data.alert).fadeIn();
					lihatTabel();
				}
				if (data.confirm == 'errValidation') {
					$('#idUsername').focus();
					$('#<?php echo $form_alert; ?>').html(data.idErrForm);
					$('#idErrNmAdmin').html(data.idErrNmAdmin);
					$('#idErrUsername').html(data.idErrUsername);
					$('#idErrPassword').html(data.idErrPassword);
					$('#idErrPasswordConf').html(data.idErrPasswordConf);
					$('#idErrTipe').html(data.idErrTipe);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#<?php echo $form_overlay; ?>').hide();
				}
			} 	        
		});
		
		return false;
	});

	function editData(id) {
		showForm(id);
		$('#idBtnTambah').show();
	}

	function permissionData(kd_admin){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url(); ?>/administrator/manage_admin/permission/table_permissionadmin_box',
			data: 'kd_admin='+kd_admin,
			success:function(html){
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	$(document).on('submit', '#<?php echo $form_edit; ?>', function(e) {
		$('#<?php echo $form_overlay; ?>').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().$form_edit_url; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					$('#<?php echo $form_name; ?>').slideUp(500, function(){
						$('#<?php echo $form_name; ?>').remove();
					});
					$('#idBtnTambah').show();
					$('#<?php echo $table_alert; ?>').html(data.alert).fadeIn();
					lihatTabel();
				}
				if (data.confirm == 'errValidation') {
					$('#idUsername').focus();
					$('#<?php echo $form_alert; ?>').html(data.idErrForm);
					$('#idErrNmAdmin').html(data.idErrNmAdmin);
					$('#idErrUsername').html(data.idErrUsername);
					$('#idErrPassword').html(data.idErrPassword);
					$('#idErrPasswordConf').html(data.idErrPasswordConf);
					$('#idErrTipe').html(data.idErrTipe);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#<?php echo $form_overlay; ?>').hide();
				}
			} 	        
		});
		
		return false;
	});

	function hapusData(id) {
		$('#<?php echo $table_overlay; ?>').show();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$form_delete_url; ?>',
			data: 'id='+id,
			success:function(html){
				$('#<?php echo $table_alert; ?>').html(html.alert).fadeIn();
				lihatTabel();
				$('#<?php echo $table_overlay; ?>').hide();
			}
		});
	}

	$(document).on('click', '#idBtnTambah', function(){
		$('#idBtnTambah').hide();
		showForm('');
	});

	$(document).on('click', '#idBtnTutup', function(){
		$('#idBtnTambah').show();
		lihatTabel();
	});
</script>