<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	lihatTabel();

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function lihatTabel() {
		$('#<?php echo $table_loader; ?>').show();
		$('#<?php echo $table_name; ?>').html('');
		$('#<?php echo $table_name; ?>').fadeOut('slow', function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$view_url; ?>',
				success:function(html){
					$('#<?php echo $table_name; ?>').html(html);
				}
			});
			$('#<?php echo $table_name; ?>').fadeIn();
			$('#<?php echo $table_loader; ?>').hide();
			moveTo('idMainContent');
		});
	}

	function showForm(id) {
		$('#<?php echo $form_name; ?>').remove();
		$('#idBoxFormHakAkses').remove();
		$('#<?php echo $table_alert; ?>').fadeOut();
		$('#<?php echo $form_alert; ?>').fadeOut();
		$('#idAlertTableHakAkses').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$form_url; ?>',
			data: 'id='+id,
			success:function(html){
				$('#idMainContent').prepend(html);
				$('#idTxtNmTipe').focus();
				moveTo('idMainContent');
			}
		});
	}

	function showFormHakAkses(id) {
		$('#<?php echo $form_name; ?>').remove();
		$('#idBoxFormHakAkses').remove();
		$('#<?php echo $table_alert; ?>').fadeOut();
		$('#<?php echo $form_alert; ?>').fadeOut();
		$('#idAlertTableHakAkses').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url(); ?>administrator/manage_admin/hak_akses/hak_akses_form',
			data: 'id='+id,
			success:function(html){
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function showPermission(kd_tipe_admin) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url(); ?>administrator/manage_admin/permission/table_permissionhakakses_box',
			data: 'kd_tipe_admin='+kd_tipe_admin,
			success:function(html){
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function showItemGroupAdmin(kd_tipe_admin) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url(); ?>administrator/manage_admin/permission/table_itemgroup_admin_box',
			data: 'kd_tipe_admin='+kd_tipe_admin,
			success:function(html){
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	$(document).on('submit', '#<?php echo $form_input; ?>', function(e) {
		$('#<?php echo $form_overlay; ?>').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().$form_input_url; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					$('#<?php echo $form_name; ?>').slideUp(500, function(){
						$('#<?php echo $form_name; ?>').remove();
					});
					showFormHakAkses(data.kd_tipe_admin);
				}
				if (data.confirm == 'errValidation') {
					$('#idTxtNmTipe').focus();
					$('#<?php echo $form_alert; ?>').html(data.idErrForm);
					$('#idErrNmTipe').html(data.idErrNmTipe);
					$('#idErrManageItems').html(data.idErrManageItems);
					$('#idErrAccessImg').html(data.idErrAccessImg);
					$('#idErrHalaman').html(data.idErrHalaman);
					$('#idErrLog').html(data.idErrLog);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#<?php echo $form_overlay; ?>').hide();
				}
			} 	        
		});
		
		return false;
	});

	function editData(id) {
		showForm(id);
		$('#idBtnTambah').show();
	}

	$(document).on('submit', '#<?php echo $form_edit; ?>', function(e) {
		$('#<?php echo $form_overlay; ?>').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().$form_edit_url; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					$('#<?php echo $form_name; ?>').slideUp(500, function(){
						$('#<?php echo $form_name; ?>').remove();
					});
					showFormHakAkses(data.kd_tipe_admin);
				}
				if (data.confirm == 'errValidation') {
					$('#idTxtNmTipe').focus();
					$('#<?php echo $form_alert; ?>').html(data.idErrForm);
					$('#idErrNmTipe').html(data.idErrNmTipe);
					$('#idErrManageItems').html(data.idErrManageItems);
					$('#idErrAccessImg').html(data.idErrAccessImg);
					$('#idErrHalaman').html(data.idErrHalaman);
					$('#idErrLog').html(data.idErrLog);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#<?php echo $form_overlay; ?>').hide();
				}
			}
		});
		
		return false;
	});

	$(document).on('submit', '#idFormUpdateHakAkses', function(e) {
		$('#idOverlayTableHakAkses').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url(); ?>administrator/manage_admin/hak_akses/submit_hak_akses/submit",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					$('#idBoxFormHakAkses').slideUp(500, function(){
						$('#idBoxFormHakAkses').remove();
					});
					$('#idBtnTambah').show();
					$('#idAlertTable').html(data.alert).fadeIn();
					lihatTabel();
				}
				if (data.confirm == 'errValidation') {
					$('#idTxtNmTipe').focus();
					$('#idAlertTableHakAkses').html(data.idErrForm);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayTableHakAkses').hide();
				}
			}
		});
		
		return false;
	});

	function hapusData(id) {
		$('#<?php echo $table_overlay; ?>').show();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$form_delete_url; ?>',
			data: 'id='+id,
			success:function(html){
				$('#<?php echo $table_alert; ?>').html(html.alert).fadeIn();
				lihatTabel();
				$('#<?php echo $table_overlay; ?>').hide();
			}
		});
	}

	$(document).on('click', '#idBtnTambah', function(){
		$('#idBtnTambah').hide();
		showForm('');
	});

	$(document).on('click', '#idBtnTutup', function(){
		$('#idBtnTambah').show();
		lihatTabel();
	});
</script>