<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<script type="text/javascript">	
	$(document).ready(function(){
		CKEDITOR.config.customConfig = '<?php echo base_url(); ?>assets/admin_assets/plugins/ckeditor/config_asu.js';
		CKEDITOR.replaceAll({
			readonly: true,
		});
	})
	$('#idBtnUbah').on('click', function(e){
		show_form();
	});
	$('#idBtnCancel').on('click', function(e){
		hide_form();
	});

	function show_form() {
		CKEDITOR.instances['idTxtLaporanTitle'].setReadOnly(false);
		CKEDITOR.instances['idTxtLaporanFooter'].setReadOnly(false);
		$('#idTxtNoForm').prop('readonly', false);
		$('#idTxtNoForm').focus();
		$('#idBtnGroupOpen').slideUp();
		$('#idBtnGroupSubmit').slideDown();
		moveTo('<?php echo $box_id; ?>');
		$('#idErrForm').html('');
	}

	function hide_form() {
		CKEDITOR.instances['idTxtLaporanTitle'].setReadOnly(true);
		CKEDITOR.instances['idTxtLaporanFooter'].setReadOnly(true);
		$('#idTxtNoForm').prop('readonly', true);
		$('#idBtnGroupOpen').slideDown();
		$('#idBtnGroupSubmit').slideUp();
		moveTo('<?php echo $box_id; ?>');
	}

	$(document).off('submit', '#idForm').on('submit', '#idForm', function(e){
		$('#idBoxOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: '<?php echo base_url().$class_link; ?>/form_submit',
			type: 'POST',
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					<?php
					foreach ($form_error as $data) :
						echo '$(\'#'.$data.'\').html(\'\');'."\n";
					endforeach;
					?>
					$('#idErrForm').html(data.alert);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idBoxOverlayForm').hide();
					hide_form();
				}
				if (data.confirm == 'errValidation') {
					<?php
					foreach ($form_error as $data) :
						echo '$(\'#'.$data.'\').html(data.'.$data.');'."\n";
					endforeach;
					?>
					$('#idErrForm').html(data.alert);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idBoxOverlayForm').hide();
					hide_form();
				}
			}
		});
		
		return false;
	});
</script>