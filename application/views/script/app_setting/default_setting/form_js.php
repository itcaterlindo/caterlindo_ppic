<?php
defined('BASEPATH') OR exit('No direct script access allowed!');
?>

<script type="text/javascript">
	$(document).off('click', '#idBtnUbah').on('click', '#idBtnUbah', function(e){
		form_enable();
	});

	$(document).off('click', '#idBtnBatal').on('click', '#idBtnBatal', function(e){
		form_disable();
	});

	$(document).off('submit', '#<?php echo $form_id; ?>').on('submit', '#<?php echo $form_id; ?>', function(e){
		e.preventDefault();
		$('#<?php echo $form_overlay; ?>').show();
		submit_form(this);
	});

	function form_disable() {
		$('#idGroupBtnAct').slideUp(function() {
			$('#idBtnForm').slideDown();
		});
		$(':input').prop('disabled', true);
	}

	function form_enable() {
		$('#idBtnForm').slideUp(function() {
			$('#idGroupBtnAct').slideDown();
			$('#<?php echo $form_err_id; ?>').slideUp();
		});
		$(':input').prop('disabled', false);
		$('div.form-group input:eq(1)').focus();
	}

	function submit_form(form_id) {
		$.ajax({
			url: '<?php echo base_url().$class_link.'/submit_form'; ?>',
			type: 'POST',
			data: new FormData(form_id),
			contentType: false,
			cache: false,
			processData:false,
			encode:true,
			success: function(data){
				$('#<?php echo $form_err_id; ?>').slideUp().html(data.<?php echo $form_err_id; ?>).slideDown();
				$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
				$('#<?php echo $form_overlay; ?>').hide();
				form_disable();
			}
		});
	}
</script>