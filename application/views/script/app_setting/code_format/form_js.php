<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<script type="text/javascript">
	$('#<?php echo $idbox_loader; ?>').fadeOut(500, function(e){
		$('#<?php echo $idbox_content; ?>').slideDown();
	});

	$(document).off('click', '#idBtnUbah').on('click', '#idBtnUbah', function() {
		open_form('form');
	});

	$(document).off('click', '#idBtnCancel').on('click', '#idBtnCancel', function() {
		open_form('view');
	});

	function open_form(file) {
		$.ajax({
			url: '<?php echo base_url().$class_link.'/get_form'; ?>',
			type: 'GET',
			data: 'code_for=<?php echo $code_for; ?>&file_name='+file,
			success: function(html) {
				$('#idBoxFormat').slideUp(500, function() {
					$('#idBoxFormat').remove();
					$('#idMainContent').html(html);
					moveTo('idMainContent');
				});
			}
		});
	}

	$(document).off('change', '.idSelFormat').on('change', '.idSelFormat', function(event){
		$(this).closest('.text_format').html('');
		var nilai = $(this).val();
		var length = $('#idTxtCodeLength').val();
		var format_length = $('#idTxtCodeFormat').val();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/text_format/asd/asd'; ?>',
			data: 'nilai_format='+nilai+'&val_format=&length='+length+'&format_length='+format_length,
			success: function(data){
				if (data.nilai != '') {
					$(event.target).closest('.form-group').find('.text_format').html(data.text_format);
				} else {
					$(event.target).closest('.form-group').find('.text_format').html('');
				}
			}
		});
	});

	$(document).off('click', '.idBtnAdd').on('click', '.idBtnAdd', function(event){
		$.ajax({
			url: '<?php echo base_url().$class_link.'/structure_dropdown/val/form_anak'; ?>',
			success: function(data){
				$(event.target).closest('.form_format').append(data.text_format);
			}
		});
	});

	$(document).off('click', '.idBtnHapus').on('click', '.idBtnHapus', function(event){
		$(this).closest('.form-group').slideUp('slow', function(){
			$(this).closest('.form-group').remove();
		});
	});

	$(document).off('submit', '#idForm').on('submit', '#idForm', function(e) {
		$('#idBoxOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().$class_link.'/submit_form'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					open_form('view');
				}
				if (data.confirm == 'error') {
					$('#idErrForm').html(data.alert).fadeIn();
					$('#idBoxOverlayForm').hide();
					<?php
					foreach ($form_error as $data) :
						echo '$(\'#'.$data.'\').html(data.'.$data.');'."\n";
					endforeach;
					?>
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
				}
			}
		});
		
		return false;
	});
</script>