<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function show_form(method, code_name){
		$.ajax({
			url: '<?php echo base_url().$class_link.'/data_form/'; ?>'+method+'/'+code_name,
			success: function(data){
				$('#idBoxForm').slideUp('slow', function(){
					$(this).remove();
					$('#idMainContent').prepend(data).slideDown('slow');
				});
				moveTo('idMainContent');
			}
		});
	}

	$(document).on('click', '#idBtnUbah', function(){
		show_form('update', '<?php echo $nm_code; ?>');
	});

	$(document).on('click', '#idBtnCancel', function(){
		show_form('view', '<?php echo $nm_code; ?>');
	});

	$(document).on('change', '.idSelFormat', function(event){
		$(this).closest('.text_format').html('');
		var nilai = $(this).val();
		var length = $('#idTxtCodeLength').val();
		var format_length = $('#idTxtCodeFormat').val();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/text_format/asd/asd'; ?>',
			data: 'nilai_format='+nilai+'&val_format=&length='+length+'&format_length='+format_length,
			success: function(data){
				if (data.nilai != '') {
					$(event.target).closest('.form-group').find('.text_format').html(data.text_format);
				} else {
					$(event.target).closest('.form-group').find('.text_format').html('');
				}
			}
		});
	});

	$(document).on('click', '.idBtnAdd', function(event){
		$.ajax({
			url: '<?php echo base_url().$class_link.'/structure_dropdown/val/form_anak'; ?>',
			success: function(data){
				$(event.target).closest('.form_format').append(data.text_format);
			}
		});
	});

	$(document).on('click', '.idBtnHapus', function(event){
		$(this).closest('.form-group').slideUp('slow', function(){
			$(this).closest('.form-group').remove();
		});
	});

	$(document).on('submit', '#idFormInput', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().$class_link.'/submit_form'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					show_form('view', '<?php echo $nm_code; ?>');
				}
				if (data.confirm == 'errValidation') {
					$('#idAlertForm').html(data.alert).fadeIn();
					$('#idOverlayForm').hide();
					<?php
					foreach ($form_error as $data) :
						echo '$(\'#'.$data.'\').html(data.'.$data.');'."\n";
					endforeach;
					?>
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
				}
			}
		});
		
		return false;
	});
</script>