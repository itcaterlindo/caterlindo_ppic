<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	$('.btnAdd').click(function(){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/relasi_product/form_child'; ?>',
			success:function(data){
				$(data.form_child).appendTo($('#idBoxFormBody')).attr('style', 'display:none;').slideDown('fast');
				$('.select2').select2({
					theme: 'bootstrap'
				});
				$('.btnSub').click(function(){
					$(this).closest('.form-group').slideUp('fast', function() {
						$(this).closest('.form-group').remove();
					});
				});
			}
		});
	});

	$('.btnSub').click(function(){
		$(this).closest('.form-group').slideUp('fast', function() {
			$(this).closest('.form-group').remove();
		});
	});

	$('#idFormInput').unbind().bind('submit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().'manage_items/manage_product/relasi_product/submit_form/input'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					formSuccess('idBoxForm', data.alert, 'barang', '');
				}
				if (data.confirm == 'errValidation') {
					$('#idAlertForm').html(data.idErrForm);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
				}
			} 	        
		});
		
		return false;
	});

	$('#idFormEdit').unbind().bind('submit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().'manage_items/manage_product/relasi_product/submit_form/edit'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					formSuccess('idBoxForm', data.alert, 'barang', '');
				}
				if (data.confirm == 'errValidation') {
					$('#idAlertForm').html(data.idErrForm);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
				}
			}
		});
		
		return false;
	});
</script>