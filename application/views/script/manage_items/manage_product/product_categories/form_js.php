<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	$('#idTxtKdUser').keyup(function (){
		$('#idTxtKdUser').attr('readonly', 'TRUE');
		var kd_kat = $('#idTxtKdUser').val();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/product_categories/verify_kd_kat'; ?>',
			data: 'id='+kd_kat,
			success:function(data){
				if (data.confirm == 'success') {
					$('#idErrKdKat').html(data.idErrKdKat);
					$('#idBtnSubmit').slideDown();
					$('#idTxtKdUser').removeAttr('readonly');
				}
				if (data.confirm == 'errValidation') {
					$('#idErrKdKat').html(data.idErrKdKat);
					$('#idBtnSubmit').slideUp();
					$('#idTxtKdUser').removeAttr('readonly');
				}
			}
		});
	});

	$('#idFormInput').unbind().bind('submit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().'manage_items/manage_product/product_categories/submit_form/input'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					$('#idBoxForm').slideUp(500, function(){
						$('#idBoxForm').remove();
					});
					$('#idAlertTable').html(data.alert).fadeIn();
					$('#idBtnTambah').show();
					lihatTabel();
				}
				if (data.confirm == 'errValidation') {
					$('#idTxtNmKat').focus();
					$('#idAlertForm').html(data.idErrForm);
					$('#idErrNmKat').html(data.idErrNmKat);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
				}
			}
		});
		
		return false;
	});

	$('#idFormEdit').unbind().bind('submit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().'manage_items/manage_product/product_categories/submit_form/edit'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					$('#idBoxForm').slideUp(500, function(){
						$('#idBoxForm').remove();
					});
					$('#idAlertTable').html(data.alert).fadeIn();
					lihatTabel();
				}
				if (data.confirm == 'errValidation') {
					$('#idTxtNmKat').focus();
					$('#idAlertForm').html(data.idErrForm);
					$('#idErrNmKat').html(data.idErrNmKat);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
				}
			}
		});
		
		return false;
	});
</script>