<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	lihatTabel();

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function lihatTabel() {
		$('#idLoaderTable').show();
		$('#idTable').html('');
		$('#idTable').fadeOut('slow', function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().'manage_items/manage_product/product_categories/data_lihat'; ?>',
				success:function(html){
					$('#idTable').html(html);
				}
			});
			$('#idTable').fadeIn();
			$('#idLoaderTable').hide();
			moveTo('idMainContent');
		});
	}

	function showForm(id) {
		$('#idBoxForm').remove();
		$('#idAlertTable').fadeOut();
		$('#idAlertForm').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/product_categories/data_form'; ?>',
			data: 'id='+id,
			success:function(html){
				$('#idMainContent').prepend(html);
				$('#idTxtNmKat').focus();
				moveTo('idMainContent');
			}
		});
	}

	function editData(id) {
		showForm(id);
		$('#idBtnTambah').show();
	}

	function hapusData(id) {
		$('#idTableOverlay>').show();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/product_categories/hapus_data'; ?>',
			data: 'id='+id,
			success:function(html){
				$('#idAlertTable').html(html.alert).fadeIn();
				lihatTabel();
				$('#idTableOverlay').hide();
			}
		});
	}

	$(document).on('click', '#idBtnTambah', function(){
		$('#idBtnTambah').hide();
		showForm('');
	});

	$(document).on('click', '#idBtnTutup', function(){
		$('#idBtnTambah').show();
		lihatTabel();
	});
</script>