<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	$('#idFormInput').unbind().bind('submit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().'manage_items/manage_product/product_price/submit_form/input'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					formSuccess(['idBoxPriceForm', 'idBoxTablePrice'], data.alert, 'harga', data.kd_barang);
					$('#idBtnTambahPrice').show();
				}
				if (data.confirm == 'errValidation') {
					$('#idTxtHargaLokal').focus();
					$('#idAlertPriceForm').html(data.idErrForm);
					$('#idErrHargaDistributor').html(data.idErrHargaDistributor);
					$('#idErrHargaRetail').html(data.idErrHargaRetail);
					$('#idErrHargaResell').html(data.idErrHargaResell);
					$('#idErrHargaEkspor').html(data.idErrHargaEkspor);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
				}
			} 	        
		});
		
		return false;
	});

	$('#idFormEdit').unbind().bind('submit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().'manage_items/manage_product/product_price/submit_form/edit'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					formSuccess(['idBoxPriceForm', 'idBoxTablePrice'], data.alert, 'harga', data.kd_barang);
				}
				if (data.confirm == 'errValidation') {
					$('#idTxtHargaLokal').focus();
					$('#idAlertPriceForm').html(data.idErrForm);
					$('#idErrHargaDistributor').html(data.idErrHargaDistributor);
					$('#idErrHargaRetail').html(data.idErrHargaRetail);
					$('#idErrHargaResell').html(data.idErrHargaResell);
					$('#idErrHargaEkspor').html(data.idErrHargaEkspor);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
				}
			}
		});
		
		return false;
	});
</script>