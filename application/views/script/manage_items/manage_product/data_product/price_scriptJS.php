<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	lihatTabelPrice();

	function lihatTabelPrice() {
		$('#idLoaderTablePrice').show();
		$('#idTablePrice').html('');
		$('#idTablePrice').fadeOut('slow', function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().'manage_items/manage_product/product_price/data_lihat'; ?>',
				data: 'id=<?php echo $kd_barang; ?>',
				success:function(html){
					$('#idTablePrice').html(html);
				}
			});
			$('#idTablePrice').fadeIn();
			$('#idLoaderTablePrice').hide();
		});
	}

	function editDataHarga(id) {
		showPriceForm('edit', id);
		$('#idBtnTambahPrice').show();
	}

	function hapusDataHarga(id) {
		$('#idTablePriceOverlay').show();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/product_price/hapus_data'; ?>',
			data: 'id='+id,
			success:function(html){
				$('#idAlertTablePrice').html(html.alert).fadeIn();
				lihatTabelPrice();
				$('#idTablePriceOverlay').hide();
			}
		});
	}

	$('#idBtnTambahPrice').unbind().click(function(){
		showPriceForm('input', '<?php echo $kd_barang; ?>', '');
		$('#idBtnTambahPrice').hide();
	});

	$(document).on('click', '#idBtnClosePrice', function(){
		$('#idBtnTambahPrice').show();
		lihatTabel();
	});
</script>