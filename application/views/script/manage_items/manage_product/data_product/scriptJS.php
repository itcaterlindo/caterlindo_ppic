<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	lihatTabel();

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function lihatTabel() {
		$('#idLoaderTable').show();
		$('#idTable').html('');
		$('#idTable').fadeOut('slow', function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().'manage_items/manage_product/data_product/data_lihat/'; ?>',
				success:function(html){
					$('#idTable').html(html);
				}
			});
			$('#idTable').fadeIn();
			$('#idLoaderTable').hide();
			moveTo('idMainContent');
		});
	}

	function removeForm() {
		$('#idBoxForm').remove();
		$('#idBoxPriceForm').remove();
		$('#idBoxImgForm').remove();
		$('#idAlertForm').fadeOut();
		$('#idAlertPriceForm').fadeOut();
		$('#idAlertImgForm').fadeOut();
		$('#idFormBoxDataProduct').remove();
	}

	function formSuccess(idBox, msg, tabel, add) {
		if ($.isArray(idBox)) {
			$.each(idBox, function(index, value){
				$('#'+value).slideUp(500, function(){
					$('#'+value).remove();
				});
			});
		} else {
			$('#'+idBox).slideUp(500, function(){
				$('#'+idBox).remove();
			});
		}
		if (tabel == 'barang') {
			$('#idAlertTable').html(msg).fadeIn();
			$('#idBtnTambah').show();
			lihatTabel();			
		} else if (tabel == 'harga') {
			$('#idAlertTable').html(msg).fadeIn();
			lihatTabel();
		} else if (tabel == 'img') {
			$('#idAlertTable').html(msg).fadeIn();
			lihatTabel();
		}
	}

	function showForm(id, stts) {
		removeForm();
		$('#idAlertTable').fadeOut();
		$('#idBoxTablePrice').remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/data_product/data_form'; ?>',
			data: 'id='+id+'&stts='+stts,
			success:function(html){
				$('#idMainContent').prepend(html);
				$('#idTxtBarcode').focus();
				$('.select2').select2({
					theme: 'bootstrap'
				});
				moveTo('idMainContent');
			}
		});
	}

	function showPriceForm(act, id, kd) {
		removeForm();
		$('#idAlertTable').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/product_price/data_form'; ?>',
			data: 'act='+act+'&id='+id+'&kd='+kd,
			success:function(html){
				$('#idMainContent').prepend(html);
				$('#idTxtHargaLokal').focus();
				moveTo('idMainContent');
			}
		});
	}

	function hargaData(id) {
		removeForm();
		$('#idAlertTable').fadeOut();
		$('#idBoxTablePrice').remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/product_price/index'; ?>',
			data: 'id='+id,
			success:function(html){
				$('#idMainContent').prepend(html);
			}
		});
		$('#idBoxTableImg').remove();
		moveTo('idMainContent');
	}

	function showImgForm(act, id, kd) {
		removeForm();
		$('#idAlertTable').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/product_img/data_form'; ?>',
			data: 'act='+act+'&id='+id+'&kd='+kd,
			success:function(html){
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function imgData(id) {
		removeForm();
		$('#idAlertTable').fadeOut();
		$('#idBoxTableImg').remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/product_img/index'; ?>',
			data: 'id='+id,
			success:function(html){
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
		$('#idBoxTablePrice').remove();
	}

	function duplicate(id) {
		showForm(id, 'duplicate');
		$('#idAlertTable').fadeOut();
		$('#idBtnTambah').show();
		// $.ajax({
		// 	type: 'GET',
		// 	url: '<?php //echo base_url().'manage_items/manage_product/data_product/duplicate'; ?>',
		// 	data: 'kd='+id,
		// 	success:function(html){
		// 		console.log(html)
		// 	}
		// });
	}

	function editData(id) {
		showForm(id, 'edit');
		$('#idAlertTable').fadeOut();
		$('#idBtnTambah').show();
	}

	function hapusData(id) {
		$('#idTableOverlay>').show();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/data_product/hapus_data'; ?>',
			data: 'id='+id,
			success:function(html){
				$('#idAlertTable').html(html.alert).fadeIn();
				lihatTabel();
				$('#idTableOverlay').hide();
			}
		});
	}

	$(document).on('click', '#idBtnTambah', function(){
		$('#idBtnTambah').hide();
		showForm('', 'add');
	});

	$(document).on('click', '#idBtnTutup', function(){
		$('#idBtnTambahPrice').show();
		$('#idBtnTambah').show();
		lihatTabel();
	});

	$(document).off('click', '#idBtnGrid').on('click', '#idBtnGrid', function(){
		show_form_select_grid();
	});

	function show_form_select_grid() {
		removeForm();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/data_product/show_form_select_grid'; ?>',
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function show_form_grid(selected, group, kat) {
		removeForm();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/data_product/show_form_grid/'; ?>'+selected+'/'+group+'/'+kat,
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}
</script>