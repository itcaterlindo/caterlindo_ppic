<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	/** Jika mode edit maka txtCode di lock/readonly */
	var kd_barang = $('#idTxtKdBarang').val();
	var status = $('#stssx').val();
	if(kd_barang !== "" && status == "edit"){
		$('#idTxtCode').attr('readonly', 'TRUE');
		$('#idTxtCbm').attr('readonly', 'TRUE');
		$('#idTxtNetweight').attr('readonly', 'TRUE');
		
	}
	/** End mode edit */

	$('#idTxtBarcode').focusout(function (){
		$('#idTxtBarcode').attr('readonly', 'TRUE');
		var barcode = $('#idTxtBarcode').val();
		var kd_barang = $('#idTxtKdBarang').val();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/data_product/verify_barcode'; ?>',
			data: 'id='+barcode+'&kd_barang='+kd_barang,
			success:function(data){
				if (data.confirm == 'success') {
					$('#idErrBarcode').html(data.idErrBarcode);
					$('#idBtnSubmit').slideDown();
					$('#idTxtBarcode').removeAttr('readonly');
				}
				if (data.confirm == 'errValidation') {
					$('#idErrBarcode').html(data.idErrBarcode);
					$('#idBtnSubmit').slideUp();
					$('#idTxtBarcode').removeAttr('readonly');
				}
			}
		});
	});

	$('#idTxtCode').focusout(function (){
		var code = $('#idTxtCode').val();
		var kd_barang = $('#idTxtKdBarang').val();
		if(kd_barang == ""){
			// Jika kode barang tidak muncul maka mode insert, selain itu mode edit (field txtCode di readonly)
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().'manage_items/manage_product/data_product/verify_code'; ?>',
				data: 'id='+code+'&kd_barang='+kd_barang,
				success:function(data){
					if (data.confirm == 'success') {
						$('#idErrCode').html(data.idErrCode);
						$('#idBtnSubmit').slideDown();
						$('#idTxtCode').removeAttr('readonly');
					}
					if (data.confirm == 'errValidation') {
						$('#idErrCode').html(data.idErrCode);
						$('#idBtnSubmit').slideUp();
						$('#idTxtCode').removeAttr('readonly');
					}
				}
			});
		}
	});

	$(document).off('keyup', '.hitung_netweight').on('keyup', '.hitung_netweight', function() {
		hitung_net();
	});

	$(document).off('keyup', '.hitung_cbm').on('keyup', '.hitung_cbm', function() {
		hitung_cbm();
	});

	$('#idFormInput').unbind().bind('submit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().'manage_items/manage_product/data_product/submit_form/input'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					showPriceForm('input', data.kd_barang);
				}
				if (data.confirm == 'errValidation') {
					$('#idTxtBarcode').focus();
					$('#idAlertForm').html(data.idErrForm);
					$('#idErrBarcode').html(data.idErrBarcode);
					$('#idErrCode').html(data.idErrCode);
					$('#idErrHs').html(data.idErrHs);
					$('#idErrGroup').html(data.idErrGroup);
					$('#idErrKat').html(data.idErrKat);
					$('#idErrDesc').html(data.idErrDesc);
					$('#idErrDimension').html(data.idErrDimension);
					$('#idErrGrossweight').html(data.idErrGrossweight);
					$('#idErrBoxweight').html(data.idErrBoxweight);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
				}
			} 	        
		});
		
		return false;
	});

	$('#idFormEdit').unbind().bind('submit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().'manage_items/manage_product/data_product/submit_form/edit'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					formSuccess('idBoxForm', data.alert, 'barang', '');
				}
				if (data.confirm == 'errValidation') {
					$('#idTxtBarcode').focus();
					$('#idAlertForm').html(data.idErrForm);
					$('#idErrBarcode').html(data.idErrBarcode);
					$('#idErrCode').html(data.idErrCode);
					$('#idErrHs').html(data.idErrHs);
					$('#idErrGroup').html(data.idErrGroup);
					$('#idErrKat').html(data.idErrKat);
					$('#idErrDesc').html(data.idErrDesc);
					$('#idErrDimension').html(data.idErrDimension);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
				}
			}
		});
		
		return false;
	});

	function hitung_net() {
		var gross = $('#idTxtGrossweight').val();
		var box = $('#idTxtBoxweight').val();
		net = gross - box;
		$('#idTxtNetweight').val(net);
	}

	function hitung_cbm() {
		var length = $('#idTxtLength').val();
		var width = $('#idTxtWidth').val();
		var height = $('#idTxtHeight').val();
		cbm = (length * width * height) / 1000000;
		$('#idTxtCbm').val(cbm);
	}
</script>