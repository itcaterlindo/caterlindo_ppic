<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	$('#idFormInput').unbind().bind('submit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().'manage_items/manage_product/product_img/submit_form/input'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					formSuccess(['idBoxImgForm', 'idBoxTableImg'], data.alert, 'img', data.kd_barang);
					$('#idBtnTambahImg').show();
				}
				if (data.confirm == 'errValidation') {
					$('#idAlertPriceForm').html(data.idErrForm);
					$('#idErrFileProduct').html(data.idErrFileProduct);
					$('#idErrFileSketch').html(data.idErrFileSketch);
					$('#idErrFileRevet').html(data.idErrFileRevet);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
				}
			} 	        
		});
		
		return false;
	});

	$('#idFormEdit').unbind().bind('submit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().'manage_items/manage_product/product_img/submit_form/edit'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					formSuccess(['idBoxImgForm', 'idBoxTableImg'], data.alert, 'img', data.kd_barang);
				}
				if (data.confirm == 'errValidation') {
					$('#idAlertPriceForm').html(data.idErrForm);
					$('#idErrFileProduct').html(data.idErrFileProduct);
					$('#idErrFileSketch').html(data.idErrFileSketch);
					$('#idErrFileRevet').html(data.idErrFileRevet);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
				}
			}
		});
		
		return false;
	});
</script>