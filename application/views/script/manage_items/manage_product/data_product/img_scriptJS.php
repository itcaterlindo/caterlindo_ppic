<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	lihatTabelImg();

	function lihatTabelImg() {
		$('#idLoaderTableImg').show();
		$('#idTableImg').html('');
		$('#idTableImg').fadeOut('slow', function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().'manage_items/manage_product/product_img/data_lihat'; ?>',
				data: 'id=<?php echo $kd_barang; ?>',
				success:function(html){
					$('#idTableImg').html(html);
				}
			});
			$('#idTableImg').fadeIn();
			$('#idLoaderTableImg').hide();
		});
	}

	function editDataImg(id, kd) {
		showImgForm('edit', id, kd);
		$('#idBtnTambahImg').show();
	}

	function hapusDataImg(id) {
		$('#idTableImgOverlay').show();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/product_img/hapus_data'; ?>',
			data: 'id='+id,
			success:function(html){
				$('#idAlertTableImg').html(html.alert).fadeIn();
				lihatTabelImg();
				$('#idTableImgOverlay').hide();
			}
		});
	}

	$('#idBtnTambahImg').unbind().click(function(){
		showImgForm('input', '<?php echo $kd_barang; ?>', '');
		$('#idBtnTambahImg').hide();
	});

	$(document).on('click', '#idBtnCloseImg', function(){
		$('#idBtnTambahImg').show();
		lihatTabel();
	});
</script>