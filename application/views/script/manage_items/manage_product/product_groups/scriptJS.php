<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	lihatTabel();

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function lihatTabel() {
		$('#idLoaderTable').show();
		$('#idTable').html('');
		$('#idTable').fadeOut('slow', function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().'manage_items/manage_product/product_groups/data_lihat'; ?>',
				success:function(html){
					$('#idTable').html(html);
				}
			});
			$('#idTable').fadeIn();
			$('#idLoaderTable').hide();
			moveTo('idMainContent');
		});
	}

	function showForm(id) {
		$('#idBoxForm').remove();
		$('#idAlertTable').fadeOut();
		$('#idAlertForm').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/product_groups/data_form'; ?>',
			data: 'id='+id,
			success:function(html){
				$('#idMainContent').prepend(html);
				$('#idTxtNmGroup').focus();
				moveTo('idMainContent');
			}
		});
	}

	$(document).on('submit', '#idFormInput', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().'manage_items/manage_product/product_groups/submit_form/input'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					$('#idBoxForm').slideUp(500, function(){
						$('#idBoxForm').remove();
					});
					$('#idAlertTable').html(data.alert).fadeIn();
					$('#idBtnTambah').show();
					lihatTabel();
				}
				if (data.confirm == 'errValidation') {
					$('#idTxtNmGroup').focus();
					$('#idAlertForm').html(data.idErrForm);
					$('#idErrNmGroup').html(data.idErrNmGroup);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
				}
			} 	        
		});
		
		return false;
	});

	function editData(id) {
		showForm(id);
		$('#idBtnTambah').show();
	}

	$(document).on('submit', '#idFormEdit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().'manage_items/manage_product/product_groups/submit_form/edit'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					$('#idBoxForm').slideUp(500, function(){
						$('#idBoxForm').remove();
					});
					$('#idAlertTable').html(data.alert).fadeIn();
					lihatTabel();
				}
				if (data.confirm == 'errValidation') {
					$('#idTxtNmGroup').focus();
					$('#idAlertForm').html(data.idErrForm);
					$('#idErrNmGroup').html(data.idErrNmGroup);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
				}
			}
		});
		
		return false;
	});

	function hapusData(id) {
		$('#idTableOverlay>').show();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/product_groups/hapus_data'; ?>',
			data: 'id='+id,
			success:function(html){
				$('#idAlertTable').html(html.alert).fadeIn();
				lihatTabel();
				$('#idTableOverlay').hide();
			}
		});
	}

	$(document).on('click', '#idBtnTambah', function(){
		$('#idBtnTambah').hide();
		showForm('');
	});

	$(document).on('click', '#idBtnTutup', function(){
		$('#idBtnTambah').show();
		lihatTabel();
	});
</script>