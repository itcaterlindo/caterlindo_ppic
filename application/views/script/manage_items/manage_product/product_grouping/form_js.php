<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	$('.btnAdd').click(function(){
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/'.$class_name.'/form_child'; ?>',
			success:function(data){
				$(data.form_child).appendTo($('#idBoxFormBody')).attr('style', 'display:none;').slideDown('fast');
				$('.select2').select2({
					theme: 'bootstrap'
				});
				$('.btnSub').click(function(){
					$(this).closest('.form-group').slideUp('fast', function() {
						$(this).closest('.form-group').remove();
					});
				});
			}
		});
	});

	$('.btnSub').click(function(){
		$(this).closest('.form-group').slideUp('fast', function() {
			$(this).closest('.form-group').remove();
		});
	});

	$('#idTxtCode').focusout(function (){
		$('#idTxtCode').attr('readonly', 'TRUE');
		var kd_master = $('#idKdMGroup').val();
		var code = $('#idTxtCode').val();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_items/manage_product/'.$class_name.'/verify_code'; ?>',
			data: 'id='+code+'&kd_master='+kd_master,
			success:function(data){
				if (data.confirm == 'success') {
					$('#idErrCode').html(data.idErrCode);
					$('#idBtnSubmit').slideDown();
					$('#idTxtCode').removeAttr('readonly');
				}
				if (data.confirm == 'errValidation') {
					$('#idErrCode').html(data.idErrCode);
					$('#idBtnSubmit').slideUp();
					$('#idTxtCode').removeAttr('readonly');
				}
			}
		});
	});

	$('#idFormInput').unbind().bind('submit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().'manage_items/manage_product/'.$class_name.'/submit_form/input'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					formSuccess('idBoxForm', data.alert, 'barang', '');
				}
				if (data.confirm == 'errValidation') {
					$('#idAlertForm').html(data.idErrForm);
					$('#idErrCode').html(data.idErrCode);
					$('#idErrDesc').html(data.idErrDesc);
					$('#idErrDimension').html(data.idErrDimension);
					$('#idErrHargaDistributor').html(data.idErrHargaDistributor);
					$('#idErrHargaRetail').html(data.idErrHargaRetail);
					$('#idErrHargaResell').html(data.idErrHargaResell);
					$('#idErrHargaEkspor').html(data.idErrHargaEkspor);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
				}
			} 	        
		});
		
		return false;
	});

	$('#idFormEdit').unbind().bind('submit', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().'manage_items/manage_product/'.$class_name.'/submit_form/edit'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					formSuccess('idBoxForm', data.alert, 'barang', '');
				}
				if (data.confirm == 'errValidation') {
					$('#idAlertForm').html(data.idErrForm);
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
				}
			}
		});
		
		return false;
	});
</script>