<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<script type="text/javascript">
	get_default_disc_form('<?php echo $jenis_customer_kd; ?>', '', '<?php echo $kd_msalesorder; ?>');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	$('#idTxtProductCode').focus();
	item_detail('<?php echo $kd_msalesorder; ?>');

	/* --start product typeahead.js-- */
	var Product = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		url: '<?php echo base_url(); ?>auto_complete/quotation_detail',
		remote: {
			url: '<?php echo base_url(); ?>auto_complete/quotation_detail',
			prepare: function (query, settings) {
				settings.type = 'GET';
				settings.contentType = 'application/json; charset=UTF-8';
				settings.data = 'product_code='+query+'&kat_harga='+$('#idTxtPriceCategory').val()+'&kd_parent='+$('#idTxtKdParent').val();

				return settings;
			},
			wildcard: '%QUERY'
		}
	});

	$('#idTxtProductCode').typeahead(null, {
		limit: 50,
		minLength: 1,
		name: 'product_search',
		display: 'item_code',
		valueKey: 'get_product',
		source: Product.ttAdapter()
	});

	$('#idTxtProductCode').bind('typeahead:select', function(obj, selected) {
		$('#idTxtProductCodeStd').val(selected.item_code);
		$('#idTxtProductCodeCustom').val(selected.item_code);
		$('#idTxtKdBarang').val(selected.kd_barang);
		$('#idTextDesc').html(selected.deskripsi_barang);
		$('#idTxtItemDesc').html(selected.deskripsi_barang);
		$('#idTxtItemDescOri').val(selected.deskripsi_barang);
		$('#idTextDimension').html(selected.dimensi_barang);
		$('#idTxtItemDimension').html(selected.dimensi_barang);
		$('#idTxtItemDimensionOri').val(selected.dimensi_barang);
		$('#idTxtBarcodeStd').val(selected.item_barcode);
		$('#idTxtBarcode').val(selected.item_barcode);

		$('#idTxtNetweight').val(selected.netweight);
		$('#idTxtNetweightStd').val(selected.netweight);
		$('#idTxtNetweightCustom').val(selected.netweight);
		$('#idTxtGrossweight').val(selected.grossweight);
		$('#idTxtGrossweightStd').val(selected.grossweight);
		$('#idTxtGrossweightCustom').val(selected.grossweight);
		$('#idTxtBoxweight').val(selected.boxweight);
		$('#idTxtBoxweightStd').val(selected.boxweight);
		$('#idTxtBoxweightCustom').val(selected.boxweight);
		$('#idTxtLength').val(selected.length_cm);
		$('#idTxtLengthStd').val(selected.length_cm);
		$('#idTxtLengthCustom').val(selected.length_cm);
		$('#idTxtWidth').val(selected.width_cm);
		$('#idTxtWidthStd').val(selected.width_cm);
		$('#idTxtWidthCustom').val(selected.width_cm);
		$('#idTxtHeight').val(selected.height_cm);
		$('#idTxtHeightStd').val(selected.height_cm);
		$('#idTxtHeightCustom').val(selected.height_cm);
		$('#idTxtItemCbm').val(selected.item_cbm);
		$('#idTxtItemCbmStd').val(selected.item_cbm);
		$('#idTxtItemCbmCustom').val(selected.item_cbm);

		$('#idTxtProductPrice').val(selected.harga_barang);
		$('#idTxtPriceStd').val(selected.harga_barang);
		$('#idTxtPriceCustom').val(selected.harga_barang);
		$('#idTxtProductPriceRetail').val(selected.harga_retail);
		$('#idTxtKdBarangStandart').val(selected.kd_barang);
		$('#idTxtKdBarangCustom').val(selected.kd_custom);
		$('#idSelItemStatus').val('std');
		item_type('std');
		get_default_disc_form('<?php echo $jenis_customer_kd; ?>', '', '<?php echo $kd_msalesorder; ?>');
	});
	/* --end of product typeahead.js-- */

	$(document).off('change', '#idSelItemStatus').on('change', '#idSelItemStatus', function(e){
		var pilihan = $(this).val();
		item_type(pilihan);
	});

	$(document).off('keyup', '#idTxtProductCode').on('keyup', '#idTxtProductCode', function(){
		$('#idTxtProductCodeCustom').val($('#idTxtProductCode').val());
	});
	
	$(document).off('keyup change', '#idTxtNetweight').on('keyup change', '#idTxtNetweight', function(){
		$('#idTxtNetweightCustom').val($('#idTxtNetweight').val());
	});
	
	$(document).off('keyup change', '#idTxtGrossweight').on('keyup change', '#idTxtGrossweight', function(){
		$('#idTxtGrossweightCustom').val($('#idTxtGrossweight').val());
	});
	
	$(document).off('keyup change', '#idTxtBoxweight').on('keyup change', '#idTxtBoxweight', function(){
		$('#idTxtBoxweightCustom').val($('#idTxtBoxweight').val());
	});
	
	$(document).off('keyup change', '#idTxtLength').on('keyup change', '#idTxtLength', function(){
		$('#idTxtLengthCustom').val($('#idTxtLength').val());
	});
	
	$(document).off('keyup change', '#idTxtWidth').on('keyup change', '#idTxtWidth', function(){
		$('#idTxtWidthCustom').val($('#idTxtWidth').val());
	});
	
	$(document).off('keyup change', '#idTxtHeight').on('keyup change', '#idTxtHeight', function(){
		$('#idTxtHeightCustom').val($('#idTxtHeight').val());
	});
	
	$(document).off('keyup change', '#idTxtItemCbm').on('keyup change', '#idTxtItemCbm', function(){
		$('#idTxtItemCbmCustom').val($('#idTxtItemCbm').val());
	});

	function item_type(type) {
		if (type == 'custom') {
			$('#idTextDesc').slideUp();
			$('#idTxtItemDesc').slideDown();
			$('#idTextDimension').slideUp();
			$('#idTxtItemDimension').slideDown();
			$('#idTxtProductPrice').attr('readonly', false);
			$('#idTxtBarcode').attr('readonly', false);
			$('#idTxtProductPrice').val($('#idTxtPriceCustom').val());
			$('#idTxtNetweight').val($('#idTxtNetweightCustom').val());
			$('#idTxtGrossweight').val($('#idTxtGrossweightCustom').val());
			$('#idTxtBoxweight').val($('#idTxtBoxweightCustom').val());
			$('#idTxtLength').val($('#idTxtLengthCustom').val());
			$('#idTxtWidth').val($('#idTxtWidthCustom').val());
			$('#idTxtHeight').val($('#idTxtHeightCustom').val());
			$('#idTxtItemCbm').val($('#idTxtItemCbmCustom').val());
			$(document).off('keyup change', '#idTxtProductPrice').on('keyup change', '#idTxtProductPrice', function(){
				$('#idTxtPriceCustom').val($('#idTxtProductPrice').val());
			});
			$('#idTxtProductCode').val($('#idTxtProductCodeCustom').val());
			$('#idTxtBarcode').val($('#idTxtBarcodeCustom').val());
			if( $('#idTxtFormType').val() == 'input' ){
				$('#idTxtKdBarang').val($('#idTxtKdBarangCustom').val());
			} 
			if ($('#idTxtRelation').val() == 'child') {
				$('#idTxtProductCodeChild').slideDown();
			}
			$('.id-form-item-detail').slideDown();
		}
		else if (type == 'std') {
			$('#idTextDesc').slideDown();
			$('#idTxtItemDesc').slideUp();
			$('#idTextDimension').slideDown();
			$('#idTxtItemDimension').slideUp();
			$('#idTxtProductPrice').attr('readonly', true);
			$('#idTxtBarcode').attr('readonly', true);
			$('#idTxtProductPrice').val($('#idTxtPriceStd').val());
			$('#idTxtProductCode').val($('#idTxtProductCodeStd').val());
			$('#idTxtBarcode').val($('#idTxtBarcodeStd').val());
			$('#idTxtNetweight').val($('#idTxtNetweightStd').val());
			$('#idTxtGrossweight').val($('#idTxtGrossweightStd').val());
			$('#idTxtBoxweight').val($('#idTxtBoxweightStd').val());
			$('#idTxtLength').val($('#idTxtLengthStd').val());
			$('#idTxtWidth').val($('#idTxtWidthStd').val());
			$('#idTxtHeight').val($('#idTxtHeightStd').val());
			$('#idTxtItemCbm').val($('#idTxtItemCbmStd').val());
			$('#idTxtKdBarang').val($('#idTxtKdBarangStandart').val());
			if ($('#idTxtRelation').val() == 'child') {
				$('#idTxtProductCodeChild').slideUp();
			}
			$('.id-form-item-detail').slideUp();
		}
		$('#idTxtProductQty').focus();
		hitung();
	}

	$(document).off('keyup', '.hitung_netweight').on('keyup', '.hitung_netweight', function() {
		hitung_net();
	});

	$(document).off('keyup', '.hitung_cbm').on('keyup', '.hitung_cbm', function() {
		hitung_cbm();
	});

	function hitung_net() {
		var gross = $('#idTxtGrossweight').val();
		var box = $('#idTxtBoxweight').val();
		net = gross - box;
		$('#idTxtNetweight').val(net);
	}

	function hitung_cbm() {
		var length = $('#idTxtLength').val();
		var width = $('#idTxtWidth').val();
		var height = $('#idTxtHeight').val();
		cbm = (length * width * height) / 1000000;
		$('#idTxtItemCbm').val(cbm);
	}

	$(document).on('focus blur keyup change', '.hitung', function(e){
		hitung();
	});

	$(document).off('blur', '#idTxtProductPrice').on('blur', '#idTxtProductPrice', function() {
		decimal = $('#idTxtDecimal').val();
		harga = $(this).val();

		/* --start set nomor decimal untuk harga-- */
		if (decimal == '0') {
			/* --explode dulu angka dari harga, kemudian cek nilai decimal tsb, jika angka >= 5 maka angka awal akan +1, jika tidak maka nilai decimal akan dihilangkan-- */
			if (harga.indexOf('.') >= 0) {
				split = harga.split('.');
				if (undefined !== split[1] && split[1].length >= 1) {
					dec_pertama = split[1].substr(0, 1);
					dec_kedua = split[1].substr(1, 1);
					if (undefined !== dec_kedua && dec_kedua >= 5) {
						dec_pertama = parseInt(dec_pertama) + 1;
					}
					if (dec_pertama >= 5) {
						harga = parseInt(split[0]) + 1;
						$('#idTxtProductPrice').val(harga);
					}
					if (dec_pertama < 5) {
						$('#idTxtProductPrice').val(split[0]);
					}
				}
			}
		}
		/* --end set nomor decimal untuk harga-- */
	})

	function hitung() {
		var nilai;
		var dec_pertama;
		var dec_kedua;
		pilihan = $('#idSelDiscType').val();
		harga = $('#idTxtProductPrice').val();
		disc = $('#idTxtProductDisc').val();
		qty = $('#idTxtProductQty').val();
		decimal = $('#idTxtDecimal').val();

		/* --start set nomor decimal untuk harga-- */
		if (decimal == '0') {
			/* --explode dulu angka dari harga, kemudian cek nilai decimal tsb, jika angka >= 5 maka angka awal akan +1, jika tidak maka nilai decimal akan dihilangkan-- */
			if (harga.indexOf('.') >= 0) {
				split = harga.split('.');
				if (undefined !== split[1] && split[1].length == 2) {
					dec_pertama = split[1].substr(0, 1);
					dec_kedua = split[1].substr(1, 1);
					if (undefined !== dec_kedua && dec_kedua >= 5) {
						dec_pertama = parseInt(dec_pertama) + 1;
					}
					if (dec_pertama >= 5) {
						harga = parseInt(split[0]) + 1;
						$('#idTxtProductPrice').val(harga);
					}
					if (dec_pertama < 5) {
						$('#idTxtProductPrice').val(split[0]);
					}
				}
			}
		}
		/* --end set nomor decimal untuk harga-- */
		if (pilihan == 'percent') {
			nilai = (parseInt(harga) - ((parseInt(harga) * parseInt(disc)) / 100)) * parseInt(qty);
		}
		else if (pilihan == 'decimal') {
			nilai = (parseInt(harga) - parseInt(disc)) * parseInt(qty);
		}
		$('#idTxtProductTotPrice').val(nilai);
	}

	function resetForm() {
		$('#idTxtKdDSales, #idTxtProductCodeStd, #idTxtProductCodeCustom, #idTxtProductCodeChild, #idTxtBarcodeStd, #idTxtBarcode, #idTxtItemDescOri, #idTxtItemDimensionOri, #idTxtProductNote, #idTxtProductPrice, #idTxtPriceStd, #idTxtPriceCustom, #idTxtProductQty, #idTxtProductPriceRetail, #idTxtProductTotPrice, #idSelProductChild, #idTxtNetweight, #idTxtNetweightStd, #idTxtNetweightCustom, #idTxtGrossweight, #idTxtGrossweightStd, #idTxtGrossweightCustom, #idTxtBoxweight, #idTxtBoxweightStd, #idTxtBoxweightCustom, #idTxtLength, #idTxtLengthStd, #idTxtLengthCustom, #idTxtWidth, #idTxtWidthStd, #idTxtWidthCustom, #idTxtHeight, #idTxtHeightStd, #idTxtHeightCustom, #idTxtItemCbm, #idTxtItemCbmStd, #idTxtItemCbmCustom, #idTxtKdBarang, #idTxtKdBarangStandart, #idTxtKdBarangCustom').val('');
		$('#idTxtProductCode').typeahead('val', '');
		$('#idTextDesc, #idTextDimension').html('-');
		$('#idTxtItemDesc, #idTxtItemDimension').html('');
		$('#idSelItemStatus').val('std');
		$('#idSelDiscType').val('decimal');
		$('#idTxtProductDisc').val('0');
		item_type('std');
		$('#idTxtFormType').val('input');
		$('#idBtnCancelEdit').hide();
		get_default_disc_form('<?php echo $jenis_customer_kd; ?>', '', '<?php echo $kd_msalesorder; ?>');
	}

	$(document).off('submit', '#idForm').on('submit', '#idForm', function(e) {
		$('#idOverlayForm').show();
		e.preventDefault();
		var form_tipe = $('#idTxtFormType').val();
		<?php
		foreach ($form_error as $data) :
			echo '$(\'#'.$data.'\').html(\'\');'."\n";
		endforeach;
		?>
		$.ajax({
			url: "<?php echo base_url().$class_link.'/sales_orders_item/submit_form/'; ?>"+form_tipe,
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				if (data.confirm == 'success') {
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idErrFormItemSales').html(data.alert);
					$('#idOverlayForm').hide();
					resetForm();
					if (data.relation == 'child') {
						$('#idTxtProductCode').show();
						$('#idTxtProductCodeChild').slideUp();
						$('#idSelProductChild').val('');
					}
					$('#idTxtProductCode').focus();
				}
				if (data.confirm == 'error') {
					<?php
					foreach ($form_error as $data) :
						echo '$(\'#'.$data.'\').html(data.'.$data.');'."\n";
					endforeach;
					?>
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayForm').hide();
					$('#idTxtProductCode').focus();
				}
				item_detail('<?php echo $kd_msalesorder; ?>');
			}
		});
		
		return false;
	});

	function item_detail(kd_msalesorder) {
		$.ajax({
			url: '<?php echo base_url().$class_link.'/sales_orders_item/item_detail'; ?>',
			data: 'kd_msalesorder='+kd_msalesorder,
			success: function(data){
				$('#idDataDetail').html(data);
			}
		});
	}

	function deleteItemDetail(id) {
		$('#idOverlayForm').show();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/sales_orders_item/hapus_data'; ?>',
			data: 'id='+id+'&relation=parent',
			success:function(html){
				$('#idErrFormItemSales').html(html.alert);
				item_detail('<?php echo $kd_msalesorder; ?>');
				$('#idOverlayForm').hide();
				moveTo('idMainContent');
				resetForm();
				stopChildData();
			}
		});
	}

	function deleteItemDetailChild(id) {
		$('#idOverlayForm').show();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/sales_orders_item/hapus_data'; ?>',
			data: 'id='+id+'&relation=child',
			success:function(html){
				$('#idErrFormItemSales').html(html.alert);
				item_detail('<?php echo $kd_msalesorder; ?>');
				$('#idOverlayForm').hide();
				moveTo('idMainContent');
				resetForm();
				stopChildData();
			}
		});
	}

	function addChildData(id, kd_parent, parent_code) {
		var tipe_harga = $('#idTxtPriceCategory').val();
		$('#idOverlayForm').fadeIn(500,function(){
			$('#idTxtRelation').val('child');
			$('#idTxtKdParentSales').val(id);
			$('#idTxtKdParent').val(kd_parent);
			$('#idOverlayForm').fadeOut();
			$('#idBtnStopChild').slideDown();
			$('#idKeterangan').show();
			$('#idParentCode').html(parent_code);
			$('#idTxtProductCode').hide();
			$('#idSelProductChild').show();
			$('#idSelProductChild').val('');
			$('#idTxtKdDSales').val('');
			$('#scrollable-dropdown-menu').hide();
			moveTo('idMainContent');
			resetForm();
			$('#idSelProductChild').focus();
			$.ajax({
				url: "<?php echo base_url().'/sales/quotation_items/get_child'; ?>",
				type: "GET",
				data:  "kd_parent="+kd_parent+"&tipe_harga="+tipe_harga,
				success: function(data){
					$('#idSelProductChild').html(data);
				}
			});
		});
		<?php
		foreach ($form_error as $data) :
			echo '$(\'#'.$data.'\').html(\'\');'."\n";
		endforeach;
		?>
	}

	function stopChildData() {
		$('#idOverlayForm').fadeIn(500, function(){
			$('#idTxtRelation').val('parent');
			$('#idTxtKdParentQuotation').val('');
			$('#idTxtKdParent').val('');
			$('#idOverlayForm').fadeOut();
			$('#idBtnStopChild').slideUp();
			$('#idKeterangan').hide();
			$('#idTxtProductCode').show();
			$('#idSelProductChild').hide();
			$('#idSelProductChild').val('');
			$('#scrollable-dropdown-menu').show();
			$('#idTxtProductCodeChild').slideUp();
			moveTo('idMainContent');
			resetForm();
			$('#idTxtProductCode').focus();
		});
		<?php
		foreach ($form_error as $data) :
			echo '$(\'#'.$data.'\').html(\'\');'."\n";
		endforeach;
		?>
	}

	$(document).off('change', '#idSelProductChild').on('change', '#idSelProductChild', function() {
		var tipe_harga = $('#idTxtPriceCategory').val();
		item_type('std');
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url(); ?>/sales/quotation_items/sel_child',
			data: 'kd_child='+$(this).val()+'&tipe_harga='+tipe_harga,
			success:function(data) {
				$('#idTxtKdBarang').val(data.kd_barang);
				$('#idTxtProductCodeChild').val(data.item_code);
				$('#idTextDesc').html(data.deskripsi_barang);
				$('#idTxtItemDesc').html(data.deskripsi_barang);
				$('#idTxtItemDescOri').val(data.deskripsi_barang);
				$('#idTextDimension').html(data.dimensi_barang);
				$('#idTxtItemDimension').html(data.dimensi_barang);
				$('#idTxtItemDimensionOri').val(data.dimensi_barang);
				$('#idTxtProductPrice').val(data.harga);
				$('#idTxtPriceStd').val(data.harga);
				$('#idTxtPriceCustom').val(data.harga);
				$('#idTxtProductPriceRetail').val(data.harga_retail);
				$('#idTxtBarcodeStd').val(data.item_barcode);
				$('#idTxtBarcode').val(data.item_barcode);
				$('#idTxtKdBarangStandart').val(data.kd_barang);
				$('#idTxtKdBarangCustom').val(data.kd_custom);

				$('#idTxtNetweight').val(data.netweight);
				$('#idTxtNetweightStd').val(data.netweight);
				$('#idTxtNetweightCustom').val(data.netweight);
				$('#idTxtGrossweight').val(data.grossweight);
				$('#idTxtGrossweightStd').val(data.grossweight);
				$('#idTxtGrossweightCustom').val(data.grossweight);
				$('#idTxtBoxweight').val(data.boxweight);
				$('#idTxtBoxweightStd').val(data.boxweight);
				$('#idTxtBoxweightCustom').val(data.boxweight);
				$('#idTxtLength').val(data.length_cm);
				$('#idTxtLengthStd').val(data.length_cm);
				$('#idTxtLengthCustom').val(data.length_cm);
				$('#idTxtWidth').val(data.width_cm);
				$('#idTxtWidthStd').val(data.width_cm);
				$('#idTxtWidthCustom').val(data.width_cm);
				$('#idTxtHeight').val(data.height_cm);
				$('#idTxtHeightStd').val(data.height_cm);
				$('#idTxtHeightCustom').val(data.height_cm);
				$('#idTxtItemCbm').val(data.item_cbm);
				$('#idTxtItemCbmStd').val(data.item_cbm);
				$('#idTxtItemCbmCustom').val(data.item_cbm);

				get_default_disc_form('<?php echo $jenis_customer_kd; ?>', '', '<?php echo $kd_msalesorder; ?>');
				$('#idTxtProductQty').focus();
				$('#idSelItemStatus').val('std');
			}
		})
	});

	function cancelEdit() {
		$('#idTxtProductCode').typeahead('val', '');
		$('#idTxtFormType').val('input');
		$('#idTxtKdDSales, #idTxtKdParentSales, #idTxtKdParent, #idTxtKdBarang, #idTxtProductCodeStd, #idTxtBarcode, #idTxtBarcodeStd, #idTxtItemDescOri, #idTxtItemDimensionOri, #idTxtProductNote, #idTxtProductPriceRetail, #idTxtProductPrice, #idTxtPriceStd, #idTxtProductQty, #idTxtProductTotPrice, #idSelProductChild, #idTxtNetweight, #idTxtNetweightStd, #idTxtNetweightCustom, #idTxtGrossweight, #idTxtGrossweightStd, #idTxtGrossweightCustom, #idTxtBoxweight, #idTxtBoxweightStd, #idTxtBoxweightCustom, #idTxtLength, #idTxtLengthStd, #idTxtLengthCustom, #idTxtWidth, #idTxtWidthStd, #idTxtWidthCustom, #idTxtHeight, #idTxtHeightStd, #idTxtHeightCustom, #idTxtItemCbm, #idTxtItemCbmStd, #idTxtItemCbmCustom, #idTxtKdBarang, #idTxtKdBarangStandart, #idTxtKdBarangCustom').val('');
		$('#idTextDesc, #idTextDimension').html('-');
		$('#idTxtItemDesc, #idTxtItemDimension').html('');
		$('#idTxtRelation').val('parent');
		$('#idSelProductChild').val('');
		$('#idSelItemStatus').val('std');
		$('#idSelDiscType').val('decimal');
		$('#idTxtProductDisc').val('0');
		item_type('std');
		$('#idBtnCancelEdit').hide();
		$('#idTxtProductCode').focus();
		$('#scrollable-dropdown-menu').show();
		$('#idSelProductChild').hide();
		get_default_disc_form('<?php echo $jenis_customer_kd; ?>', '', '<?php echo $kd_msalesorder; ?>');
	}

	function editItemDetail(id) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link; ?>/sales_orders_item/get_item_detail/parent',
			data: 'kd_item='+id,
			success: function(data) {
				$('#idTxtKdDSales').val(data.kd_ditem_so);
				$('#idTxtProductCode').typeahead('val', data.item_code);
				$('#idTxtProductCodeStd').val(data.code_ori);
				$('#idTxtProductCodeCustom').val(data.item_code);
				$('#idTxtBarcodeStd').val(data.barcode_ori);
				$('#idTxtBarcode').val(data.item_barcode);
				$('#idTxtKdParent').val('');
				$('#idTxtProductCode').show();
				$('#idSelProductChild').hide();
				$('#idSelProductChild').val('');
				$('#idTxtRelation').val('parent');
				$('#idSelItemStatus').val(data.item_status);
				$('#idTextDesc').html(data.item_desc_ori);
				$('#idTxtItemDescOri').val(data.item_desc_ori);
				$('#idTxtItemDesc').html(data.item_desc);
				$('#idTextDimension').html(data.item_dimension_ori);
				$('#idTxtItemDimensionOri').val(data.item_dimension_ori);
				$('#idTxtItemDimension').html(data.item_dimension);
				$('#idTxtProductNote').val(data.item_note);
				$('#idTxtProductPrice').val(data.harga_barang);
				$('#idTxtPriceStd').val(data.harga_barang);
				$('#idTxtPriceCustom').val(data.harga_barang);
				$('#idTxtProductPriceRetail').val(data.harga_retail);
				$('#idSelDiscType').val(data.disc_type);
				$('#idTxtProductDisc').val(data.item_disc);
				$('#idTxtProductQty').val(data.item_qty);
				$('#idTxtProductTotPrice').val(data.total_harga);
				$('#idTxtFormType').val('edit');
				$('#idBtnCancelEdit').show();
				$('#scrollable-dropdown-menu').show();

				$('#idTxtNetweight').val(data.netweight);
				$('#idTxtNetweightStd').val(data.netweight_ori);
				$('#idTxtNetweightCustom').val(data.netweight);
				$('#idTxtGrossweight').val(data.grossweight);
				$('#idTxtGrossweightStd').val(data.grossweight_ori);
				$('#idTxtGrossweightCustom').val(data.grossweight);
				$('#idTxtBoxweight').val(data.boxweight);
				$('#idTxtBoxweightStd').val(data.boxweight_ori);
				$('#idTxtBoxweightCustom').val(data.boxweight);
				$('#idTxtLength').val(data.length_cm);
				$('#idTxtLengthStd').val(data.length_cm_ori);
				$('#idTxtLengthCustom').val(data.length_cm);
				$('#idTxtWidth').val(data.width_cm);
				$('#idTxtWidthStd').val(data.width_cm_ori);
				$('#idTxtWidthCustom').val(data.width_cm);
				$('#idTxtHeight').val(data.height_cm);
				$('#idTxtHeightStd').val(data.height_cm_ori);
				$('#idTxtHeightCustom').val(data.height_cm);
				$('#idTxtItemCbm').val(data.item_cbm);
				$('#idTxtItemCbmStd').val(data.item_cbm_ori);
				$('#idTxtItemCbmCustom').val(data.item_cbm);

				get_default_disc_form('<?php echo $jenis_customer_kd; ?>', data.kd_ditem_so, '<?php echo $kd_msalesorder; ?>');
				item_type(data.item_status);
				$('#idTxtProductCode').focus();
				$('#idTxtKdBarang').val(data.barang_kd);
			}
		});
	}

	function editItemDetailChild(id, kd_parent, code_parent, parent_detail) {
		var tipe_harga = $('#idTxtPriceCategory').val();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link; ?>/sales_orders_item/get_item_detail/child',
			data: 'kd_item='+id,
			success: function(data) {
				$('#idTxtProductCode').hide();
				$('#idSelProductChild').show();
				$.ajax({
					url: "<?php echo base_url().'/sales/quotation_items/get_child'; ?>",
					type: "GET",
					data:  "kd_parent="+data.kd_parent+"&tipe_harga="+tipe_harga+"&kd_child="+data.kd_child,
					success: function(data){
						$('#idSelProductChild').html(data);
					}
				});
				$('#idTxtKdDSales').val(data.kd_citem_so);
				$('#idTxtKdParentSales').val(data.ditem_so_kd);
				$('#idTxtKdParent').val(data.kd_parent);
				$('#idTxtRelation').val('child');
				$('#idTxtProductCodeChild').val(data.item_code);
				$('#idTxtBarcode').val(data.item_barcode);
				$('#idTxtBarcodeStd').val(data.barcode_ori);
				$('#idTxtProductCode').val('');
				$('#idSelItemStatus').val(data.item_status);
				$('#idTextDesc').html(data.item_desc_ori);
				$('#idTxtItemDescOri').val(data.item_desc_ori);
				$('#idTxtItemDesc').html(data.item_desc);
				$('#idTextDimension').html(data.item_dimension_ori);
				$('#idTxtItemDimensionOri').val(data.item_dimension_ori);
				$('#idTxtItemDimension').html(data.item_dimension);
				$('#idTxtProductNote').val(data.item_note);
				$('#idTxtProductPrice').val(data.harga_barang);
				$('#idTxtPriceStd').val(data.harga_barang);
				$('#idTxtPriceCustom').val(data.harga_barang);
				$('#idTxtProductPriceRetail').val(data.harga_retail);
				$('#idSelDiscType').val(data.disc_type);
				$('#idTxtProductDisc').val(data.item_disc);
				$('#idTxtProductQty').val(data.item_qty);
				$('#idTxtProductTotPrice').val(data.total_harga);
				$('#idTxtFormType').val('edit');
				$('#idBtnCancelEdit').show();
				$('#scrollable-dropdown-menu').hide();

				$('#idTxtNetweight').val(data.netweight);
				$('#idTxtNetweightStd').val(data.netweight_ori);
				$('#idTxtNetweightCustom').val(data.netweight);
				$('#idTxtGrossweight').val(data.grossweight);
				$('#idTxtGrossweightStd').val(data.grossweight_ori);
				$('#idTxtGrossweightCustom').val(data.grossweight);
				$('#idTxtBoxweight').val(data.boxweight);
				$('#idTxtBoxweightStd').val(data.boxweight_ori);
				$('#idTxtBoxweightCustom').val(data.boxweight);
				$('#idTxtLength').val(data.length_cm);
				$('#idTxtLengthStd').val(data.length_cm_ori);
				$('#idTxtLengthCustom').val(data.length_cm);
				$('#idTxtWidth').val(data.width_cm);
				$('#idTxtWidthStd').val(data.width_cm_ori);
				$('#idTxtWidthCustom').val(data.width_cm);
				$('#idTxtHeight').val(data.height_cm);
				$('#idTxtHeightStd').val(data.height_cm_ori);
				$('#idTxtHeightCustom').val(data.height_cm);
				$('#idTxtItemCbm').val(data.item_cbm);
				$('#idTxtItemCbmStd').val(data.item_cbm_ori);
				$('#idTxtItemCbmCustom').val(data.item_cbm);

				get_default_disc_form('<?php echo $jenis_customer_kd; ?>', data.kd_citem_so, '<?php echo $kd_msalesorder; ?>');
				item_type(data.item_status);
				$('#idTxtProductCode').focus();
				$('#idTxtKdBarang').val(data.kd_child);
			}
		});
	}

	function get_default_disc_form(jenis_customer_kd, so_item_kd, mso_kd) {
		$('#idFormDiscDefault').html('');
		$.ajax({
			url: '<?php echo base_url().$class_link.'/sales_orders_item/get_default_disc_form'; ?>',
			type: 'GET',
			data: 'jenis_customer_kd='+jenis_customer_kd+'&so_item_kd='+so_item_kd+'&mso_kd='+mso_kd,
			success: function(form) {
				$('#idFormDiscDefault').html(form);
			}
		});
	}

	function reset_form_price() {
		$('#idTxtNmKolomPrice').val('');
		$('#idSelKolomType').val('nilai');
		$('#idTxtTotalJumlah').val('');
	}

	$(document).off('submit', '#idFormTotalPrice').on('submit', '#idFormTotalPrice', function(e){
		$.ajax({
			url: "<?php echo base_url().$class_link.'/sales_orders_item/submit_disc/'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idTxtNmKolomPrice').focus();
					$('#idErrNmKolom').html(data.idErrNmKolom);
					$('#idErrTotalJumlah').html(data.idErrTotalJumlah);
					reset_form_price();
					item_detail(data.kd_master);
				}
				if (data.confirm == 'error') {
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idTxtNmKolomPrice').focus();
					$('#idErrNmKolom').html(data.idErrNmKolom);
					$('#idErrTotalJumlah').html(data.idErrTotalJumlah);
				}
			}
		});
		
		return false;
	});

	$(document).off('click', '#idTextOngkir').on('click', '#idTextOngkir', function(e){
		$(this).hide();
		$('#idFormTxtOngkir').show();
		$('#idTxtOngkir').focus();
	});

	$(document).off('focusout', '#idTxtOngkir').on('focusout', '#idTxtOngkir', function(e){
		var ongkir = $(this).val();
		var kd_master = $('#idTxtKdMaster').val();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/sales_orders_item/submit_detail/'; ?>',
			type: 'GET',
			data:  'act=submit_ongkir&kd_master='+kd_master+'&value='+ongkir,
			success: function(data){
				item_detail(data.kd_master);
				$('#idFormTxtOngkir').hide();
				$('#idTextOngkir').show();
			}
		});
	});

	$(document).off('click', '#idTextInstall').on('click', '#idTextInstall', function(e){
		$(this).hide();
		$('#idFormTxtInstall').show();
		$('#idTxtInstall').focus();
	});

	$(document).off('focusout', '#idTxtInstall').on('focusout', '#idTxtInstall', function(e){
		var install = $(this).val();
		var kd_master = $('#idTxtKdMaster').val();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/sales_orders_item/submit_detail/'; ?>',
			type: 'GET',
			data:  'act=submit_install&kd_master='+kd_master+'&value='+install,
			success: function(data){
				item_detail(data.kd_master);
				$('#idFormTxtInstall').hide();
				$('#idTextInstall').show();
			}
		});
	});

	$(document).off('click', '#idBtnTutupForm, #idBtnCloseBox').on('click', '#idBtnTutupForm, #idBtnCloseBox', function(e){
		var kd_msalesorder = $('#idTxtKdMSales').val();
		var no_salesorder = $('#idTxtNoSo').val();
		var no_master_so = $('#idTxtNoSalesOrder').val();
		var status_so = $('#idTxtStatusSo').val();
		var order_tipe = $('#idtxtorder_tipe').val();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/sales_orders_tbl/trans_items_old/'; ?>'+kd_msalesorder+'/'+no_salesorder+'/'+no_master_so+'/'+status_so+'/'+order_tipe,
			beforeSend: function(){
				if(order_tipe == 'Additional'){
					$('#idBtnTutupForm').attr('disabled', true);
				}
			},
			success: function(data) {
				if(order_tipe == 'Additional'){
					$('#idBtnTutupForm').attr('disabled', false);
					if(data.confirm == 'error'){
						notify ('Error', data.message, 'error');
					}else{
						$('#idTableSalesAlert').html(data.alert).fadeIn();
						tutupForm();
					}
				}else{
					$('#idTableSalesAlert').html(data.alert).fadeIn();
					tutupForm();
				}
			},
			error: function(err){
				if(order_tipe == 'Additional'){
					$('#idBtnTutupForm').attr('disabled', false);
					notify ('Error', err.responseText, 'error');
				}
			}
		});
	});
	
	function tutupForm()
	{
		// Ok value sudah didapat tinggal eksekusi perintah update item pada db_caterlindo xD
		$('#idBoxFormSalesOrder').find('.box-body').slideUp(function(){
			$('#idBoxFormSalesOrder').fadeOut(function(){
				$(this).remove();
			});
		});
	}

	function hapusSpecialDiscount(id, kd_master) {
		$('#idOverlayForm').show();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/sales_orders_item/hapus_special_disc/'; ?>',
			type: 'GET',
			data: 'id='+id+'&kd_master='+kd_master,
			success: function(data){
				item_detail(kd_master);
				$('#idErrFormItemSales').html(data.alert);
				$('#idOverlayForm').hide();
			}
		});
	}

	function addItemCustomModal(id, relation)
	{
		$.ajax({
			url: '<?php echo base_url().$class_link.'/sales_orders_item/form_input_master_custom/'; ?>',
			type: 'GET',
			data: { id: id, relation: relation },
			success: function(data){
				$('#modal-item-custom').find('.modal-body').html(data);
			}
		});
		$('#modal-item-custom').modal('show');
	}

	// Add Input Custom Item JS //
	$('.modal-body').on('submit', '.FormInputCustomItem', function(e) {
		e.preventDefault();
		if (confirm("Apakah anda yakin ingin menyimpan data tersebut ?")) {
			$(this).find('#idBtnSubmit').attr('disabled', true);
			$.ajax({
				url: "<?php echo base_url().$class_link.'/sales_orders_item/submit_form_input_master_custom'; ?>",
				type: "POST",
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				beforeSend: function(){
					$(this).find('#idBtnSubmit').attr('disabled', true);
				},
				success: function(data){
					if(data.confirm == 'errValidation'){
						$('#idErrCustomBarcode').html(data.idErrCustomBarcode);
						$('#idErrCustomCode').html(data.idErrCustomCode);
						$('#idErrCustomTarif').html(data.idErrCustomTarif);
						$('#idErrCustomGroup').html(data.idErrCustomGroup);
						$('#idErrCustomCategory').html(data.idErrCustomCategory);
						$('#idErrCustomItemGroup').html(data.idErrCustomItemGroup);
						$('#idErrCustomDesc').html(data.idErrCustomDesc);
						$('#idErrCustomDimension').html(data.idErrCustomDimension);
					}else{
						if(data.code == 200){
							notify (data.status, data.pesan, 'success');
							$('#modal-item-custom').modal('hide');
							item_detail('<?php echo $kd_msalesorder; ?>');
						}else{
							notify (data.status, data.pesan, 'error');
						}
					}
					$(this).find('#idBtnSubmit').attr('disabled', false);
				},
				error: function(err){
					$(this).find('#idBtnSubmit').attr('disabled', false);
					console.log(err);
				}
			});
		}
	});
	// End Add //

	function notify (title, text, type) {
        new PNotify({
            title: title,
            text: text,
			type: type,
            styling: 'bootstrap3'
        });	
    }

</script>