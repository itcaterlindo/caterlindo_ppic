<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<script type="text/javascript">
	$('#<?php echo $idbox_loader; ?>').fadeOut(500, function(e){
		$('#<?php echo $idbox_content; ?>').slideDown();
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	$(document).off('submit', '#idForm').on('submit', '#idForm', function(e){
		$('#<?php echo $idbox_overlay; ?>').show();
		e.preventDefault();
		$.ajax({
			url: '<?php echo base_url().$class_link; ?>/sales_orders_form/form_type',
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					$('#<?php echo $box_id; ?>').fadeOut(500, function(e){
						$('#<?php echo $box_id; ?>').remove();
						show_selected_form(data.selected);
					});
					$('#<?php echo $idbox_overlay; ?>').hide();
				}
				if (data.confirm == 'error') {
					$('#idSelSales').focus();
					<?php
					foreach ($form_error as $data) :
						echo '$(\'#'.$data.'\').html(data.'.$data.');'."\n";
					endforeach;
					?>
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#<?php echo $idbox_overlay; ?>').hide();
				}
			}
		});
	});

	function show_selected_form(sel) {
		var my_url = '';
		if (sel == 'form_po') {
			my_url = '<?php echo base_url().$class_link; ?>/sales_orders_form/form_po/select_sales';
		} else if (sel == 'form_quo') {
			my_url = '<?php echo base_url().$class_link; ?>/sales_orders_form/form_select';
		}
		$.ajax({
			url: my_url,
			type: 'GET',
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}
</script>