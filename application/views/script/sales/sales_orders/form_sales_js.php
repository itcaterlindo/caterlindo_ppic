<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<script type="text/javascript">
	$('#idTxtNmSales').focus();
	datepick('.datepickerbootstrap');
	$('.select2').select2({
		theme: 'bootstrap',
		width: '100%'
	});
	btn_customer($('#idTxtKdCustomer').val());

	/* --start salesperson typeahead.js-- */
	var Salesperson = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		url: '<?php echo base_url(); ?>auto_complete/fill_sales',
		remote: {
			url: '<?php echo base_url(); ?>auto_complete/fill_sales',
			prepare: function (query, settings) {
				settings.type = 'GET';
				settings.contentType = 'application/json; charset=UTF-8';
				settings.data = 'name_startsWith='+query;

				return settings;
			},
			wildcard: '%QUERY'
		}
	});

	$('#idTxtNmSales').typeahead(null, {
		limit: 20,
		minLength: 1,
		name: 'sales_search',
		display: 'Salesperson',
		valueKey: 'get_sales',
		source: Salesperson.ttAdapter()
	});

	$('#idTxtNmSales').bind('typeahead:select', function(obj, selected) {
		$('#idTxtKdSales').val(selected.Kode);
		$('#idTxtEmailSales').val(selected.Email);
		$('#idTxtMobilePhone').val(selected.Telp);
		$('#idTxtNmCustomer').focus();
	});
	/* --end of salesperson typeahead.js-- */

	/* --start customer typeahead.js-- */
	var Customer = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		url: '<?php echo base_url(); ?>auto_complete/fill_customer',
		remote: {
			url: '<?php echo base_url(); ?>auto_complete/fill_customer',
			prepare: function (query, settings) {
				settings.type = 'GET';
				settings.contentType = 'application/json; charset=UTF-8';
				settings.data = 'name_startsWith='+query;

				return settings;
			},
			wildcard: '%QUERY'
		}
	});

	$('#idTxtNmCustomer').typeahead(null, {
		limit: 20,
		minLength: 1,
		name: 'customer_search',
		display: 'Customer',
		valueKey: 'get_customer',
		source: Customer.ttAdapter()
	});

	$('#idTxtNmCustomer').bind('typeahead:select', function(obj, selected) {
		$('#idTxtKdCustomer').val(selected.Kode);
		$('#idTxtCustomerId').html(selected.Code);
		$('#idContactPerson').html(selected.Contact);
		$('#idTxtAlamatInstansi').html(selected.Alamat);
		alamat_customer(selected.Kode);
		btn_customer(selected.Kode);
	});
	/* --end of customer typeahead.js-- */

	$('#<?php echo $idbox_loader; ?>').fadeOut(500, function(e){
		$('#<?php echo $idbox_content; ?>').slideDown();
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function get_salesdetail() {
		var kd_sales = $('#idTxtKdSales').val();
		$.ajax({
			url: '<?php echo base_url().$class_link; ?>/sales_orders_form/get_salesdetail',
			type: 'GET',
			data: 'kd_sales='+kd_sales,
			success: function (data) {
				$('#idTxtNmSales').val(data.nm_salesperson);
				$('#idTxtEmailSales').val(data.email);
				$('#idTxtMobilePhone').val(data.telp);
			}
		});
	}

	function alamat_customer(kd_customer, kd_alamat_kirim) {
		$.ajax({
			url: '<?php echo base_url().$class_link.'/sales_orders_form/dropdown_alamat'; ?>',
			type: 'GET',
			data: 'kd_customer='+kd_customer+'&kd_alamat_kirim='+kd_alamat_kirim,
			success: function(data){
				$('#idFormAlamatCustomer').html(data);
				$('.select2').select2({
					theme: 'bootstrap'
				});
			}
		});
	}

	function btn_customer(kd_customer) {
		if (kd_customer != '') {
			$('#idGroupBtnEdit').slideDown();
			$('#idGroupBtnAdd').slideUp();
		} else if (kd_customer == '') {
			$('#idGroupBtnAdd').slideDown();
			$('#idGroupBtnEdit').slideUp();
		}
	}

	function showFormCustomer() {
		var id = $('#idTxtKdCustomer').val();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_customer/data_customer/data_form/'; ?>',
			data: 'id='+id,
			success:function(html){
				$('#idMainContent').prepend(html);
				$('#idSelJenis').focus();
				moveTo('idMainContent');
				$('.select2').select2({
					theme: 'bootstrap'
				});
			}
		});
	}

	function clearCustomerDetail() {
		$('#idTxtNmCustomer').val('');
		$('#idTxtKdCustomer').val('');
		$('#idTxtCustomerId').html('');
		$('#idContactPerson').html('');
		$('#idTxtAlamatInstansi').html('');
		$('#idTxtNmCustomer').focus();
		alamat_customer('');
		btn_customer('');
	}

	$(document).off('submit', '#idForm').on('submit', '#idForm', function(e){
		$('#<?php echo $idbox_overlay; ?>').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().$class_link.'/sales_orders_form/submit_form'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){console.log(data);
				if (data.confirm == 'success') {
					$('#<?php echo $box_id; ?>').fadeOut(500, function(e){
						$('#<?php echo $box_id; ?>').remove();
						if(data.status_so == "pending"){
							editItems(data.id, data.order_tipe);
						}
						$('#idTableSalesAlert').html(data.alert).fadeIn();
					});
					$('#<?php echo $idbox_overlay; ?>').hide();
					open_table();
				}
				if (data.confirm == 'error') {
					$('#idTxtNmSales').focus();
					$('#idErrForm').html(data.alert);
					<?php
					foreach ($form_error as $data) :
						echo '$(\'#'.$data.'\').html(data.'.$data.');'."\n";
					endforeach;
					?>
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#<?php echo $idbox_overlay; ?>').hide();
				}
			}
		});
		
		return false;
	});

	function show_item_form() {
		$.ajax({
			url: '<?php echo base_url().$class_link; ?>/sales_orders_item/form_item',
			type: 'GET',
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function get_container_data()
	{
		var id = $('#idTxtContainer').val();
		$.ajax({
			url: '<?php echo base_url().'sales/set_container/get_data_byid'; ?>',
			type: 'GET',
			dataType: 'JSON',
			data: 'id='+id,
			success: function(res) {
				var nama = res.container.nm_container;
				var konversi_20ft = res.container.konversi_20ft;							
				$('#idTxtTipeContainer').val(nama);
				$('#idTxtContainer20ft').val(konversi_20ft);
			}
		});
	}

</script>