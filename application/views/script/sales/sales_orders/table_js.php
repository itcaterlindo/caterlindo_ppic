<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<script type="text/javascript">
	open_table();

	$(document).off('click', '#idBtnAddData').on('click', '#idBtnAddData', function() {
		$('#idTableSalesAlert').fadeOut('slow', function() {
			$(this).html('');
		});
		$(this).slideUp();
		$('#idFormItemBoxDataDO').remove();
		$('#idFormBoxDataDO').remove();
		$('#idFormBoxDataSO').remove();
		$('#idFormBoxDataSOItem').remove();
		$('#idBoxFormSalesOrder').remove();
		$('#idBoxForm').remove();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/sales_orders_form'; ?>/get_form',
			type: 'GET',
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	});

	$(document).off('click', '#idBtnCloseBox').on('click', '#idBtnCloseBox', function(){
		$('#idBtnAddData').slideDown();
	});

	function formSuccess(idBox, msg, tabel, add) {
		if ($.isArray(idBox)) {
			$.each(idBox, function(index, value){
				$('#'+value).slideUp(500, function(){
					$('#'+value).remove();
				});
			});
		} else {
			$('#'+idBox).slideUp(500, function(){
				$('#'+idBox).remove();
			});
		}
		open_table();
	}

	function open_table() {
		$('#idBtnAddData').slideDown();
		$('#idBoxContentTable').fadeOut('slow', function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_link.'/sales_orders_tbl/get_table'; ?>',
				success:function(html){
					$('#idBoxContentTable').html(html);
					$('#idBoxContentTable').fadeIn();
					$('#idBoxLoaderTable').hide();
					moveTo('idMainContent');
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function datepick(element) {
		$(element).datetimepicker({
			format: 'DD-MM-YYYY'
		});
	}

	function editSales(id, order_tipe) {
		$('#idTableSalesAlert').fadeOut('slow', function() {
			$(this).html('');
		});
		$('#idFormItemBoxDataDO').remove();
		$('#idFormBoxDataDO').remove();
		$('#idFormBoxDataSO').remove();
		$('#idFormBoxDataSOItem').remove();
		$('#idBoxFormSalesOrder').remove();
		$('#idBoxForm').remove();
		$('#idBtnAddData').slideDown();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/sales_orders_form/sales_form/'; ?>'+id+'/'+order_tipe,
			success:function(html){
				$('#idMainContent').prepend(html);
				$('#idTxtNmSales').focus();
				$('#idTxtNoSalesOrder').removeAttr('readonly');
				moveTo('idMainContent');
				datepick('.datepickerbootstrap');
			}
		});
	}

	function editItems(id, order_tipe) {
		$('#idTableSalesAlert').fadeOut('slow', function() {
			$(this).html('');
		});
		$('#idFormItemBoxDataDO').remove();
		$('#idFormBoxDataDO').remove();
		$('#idFormBoxDataSO').remove();
		$('#idFormBoxDataSOItem').remove();
		$('#idBoxFormSalesOrder').remove();
		$('#idBoxForm').remove();
		$('#idBtnAddData').slideDown();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/sales_orders_item/item_form/'; ?>',
			data: {id:id, order_tipe:order_tipe},
			success:function(html){
				$('#idMainContent').prepend(html);
				$('#idTxtNmSales').focus();
				moveTo('idMainContent');
				datepick('.datepickerbootstrap');
			}
		});
	}

	function ubahStatus(kd_msales, status) {
		$('#idFormItemBoxDataDO').remove();
		$('#idFormBoxDataDO').remove();
		$('#idFormBoxDataSO').remove();
		$('#idFormBoxDataSOItem').remove();
		$('#idBoxFormSalesOrder').remove();
		$('#idBoxForm').remove();
		$('#idBoxOverlayTable').show();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/sales_orders_tbl/ubah_status'; ?>',
			data: 'id='+kd_msales+'&status='+status,
			success:function(html){
				$('#idTableSalesAlert').html(html.alert).fadeIn();
				open_table();
				$('#idBoxOverlayTable').hide();
			}
		});
	}

	function productionProcess(kd_msales, tipe) {
		$('#idFormItemBoxDataDO').remove();
		$('#idFormBoxDataDO').remove();
		$('#idFormBoxDataSO').remove();
		$('#idFormBoxDataSOItem').remove();
		$('#idBoxFormSalesOrder').remove();
		$('#idBoxForm').remove();
		$('#idBoxOverlayTable').show();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/sales_orders_tbl/process_production'; ?>',
			data: 'id='+kd_msales+'&tipe='+tipe,
			success:function(data){
				if (data.confirm == 'success') {
					ubahStatus(data.kd_msales, data.status);
				} else {
					$('#idTableSalesAlert').html(data.alert).fadeIn();
					open_table();
					$('#idBoxOverlayTable').hide();
				}
			}
		});
	}

	function hapusSO(kd_msales) {
		$('#idFormItemBoxDataDO').remove();
		$('#idFormBoxDataDO').remove();
		$('#idFormBoxDataSO').remove();
		$('#idFormBoxDataSOItem').remove();
		$('#idBoxFormSalesOrder').remove();
		$('#idBoxForm').remove();
		$('#idBoxOverlayTable').show();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/sales_orders_tbl/delete_so'; ?>',
			data: 'id='+kd_msales,
			success:function(html){
				$('#idTableSalesAlert').html(html.alert).fadeIn();
				open_table();
				$('#idBoxOverlayTable').hide();
			}
		});
	}

	function bacaSales(id, view_type, sales_tipe) {
		$('#idTableSalesAlert').fadeOut('slow', function() {
			$(this).html('');
		});
		$('#idFormItemBoxDataDO').remove();
		$('#idFormBoxDataDO').remove();
		$('#idFormBoxDataSO').remove();
		$('#idFormBoxDataSOItem').remove();
		$('#idBoxFormSalesOrder').remove();
		$('#idBoxForm').remove();
		$('#idBtnAddData').slideDown();
		$.ajax({
			url: '<?php echo base_url().$class_link; ?>/sales_order_view/detail',
			type: 'GET',
			data: 'kd_salesorder='+id+'&view_type='+view_type+'&sales_tipe='+sales_tipe,
			success:function(box) {
				$('#idMainContent').prepend(box);
				moveTo('idMainContent');
			}
		});
	}

	function bukaDp(id) {
		$('#idTableSalesAlert').fadeOut('slow', function() {
			$(this).html('');
		});
		$('#idFormItemBoxDataDO').remove();
		$('#idFormBoxDataDO').remove();
		$('#idFormBoxDataSO').remove();
		$('#idFormBoxDataSOItem').remove();
		$('#idBoxFormSalesOrder').remove();
		$('#idBoxForm').remove();
		$('#idBtnAddData').slideDown();
		// $('#idBoxOverlayTable').show();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/sales_order_view/form_dp'; ?>',
			type: 'GET',
			data: 'kd_msalesorder='+id,
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function bukaDpTermin(id) {
		$('#idTableSalesAlert').fadeOut('slow', function() {
			$(this).html('');
		});
		$('#idFormItemBoxDataDO').remove();
		$('#idFormBoxDataDO').remove();
		$('#idFormBoxDataSO').remove();
		$('#idFormBoxDataSOItem').remove();
		$('#idBoxFormSalesOrder').remove();
		$('#idBoxForm').remove();
		$('#idBtnAddData').slideDown();
		// $('#idBoxOverlayTable').show();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/sales_order_view/form_dp_termin'; ?>',
			type: 'GET',
			data: 'kd_msalesorder='+id,
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function bukaListDp(id) {
		$('#idTableSalesAlert').fadeOut('slow', function() {
			$(this).html('');
		});
		$('#idFormItemBoxDataDO').remove();
		$('#idFormBoxDataDO').remove();
		$('#idFormBoxDataSO').remove();
		$('#idFormBoxDataSOItem').remove();
		$('#idBoxFormSalesOrder').remove();
		$('#idBoxForm').remove();
		$('#idBtnAddData').slideDown();
		// $('#idBoxOverlayTable').show();
		$.ajax({
			url: '<?php echo base_url().$class_link.'/sales_order_view/list_dp'; ?>',
			type: 'GET',
			data: 'kd_msalesorder='+id,
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function check_items(id) {
		$('#idFormItemBoxDataDO').remove();
		$('#idFormBoxDataDO').remove();
		$('#idFormBoxDataSO').remove();
		$('#idFormBoxDataSOItem').remove();
		$('#idBoxFormSalesOrder').remove();
		$('#idBoxForm').remove();
		$('#idBtnAddData').slideDown();
		$('#idBoxOverlayTable').show();
		$.ajax({
			url: '<?php echo base_url().$class_link; ?>/sales_orders_tbl/check_items',
			type: 'GET',
			data: 'kd_msalesorder='+id,
			success:function(data) {
				switch (data.confirm) {
					case 'success' :
						open_table();
						break;
					case 'error' :
						$('#idTableSalesAlert').html(data.alert).fadeIn();

				}
				$('#idBoxOverlayTable').hide();
			}
		});
	}

	function prosesDo(id, kd_mso, tipe) {
		$('#idTableSalesAlert').fadeOut('slow', function() {
			$(this).html('');
		});
		$('#idFormItemBoxDataDO').remove();
		$('#idFormBoxDataDO').remove();
		$('#idFormBoxDataSO').remove();
		$('#idFormBoxDataSOItem').remove();
		$('#idBoxFormSalesOrder').remove();
		$('#idBoxForm').remove();
		$('#idBtnAddData').slideDown();
		$.ajax({
			url: '<?php echo base_url(); ?>sales/delivery_orders/data_do/get_form',
			type: 'GET',
			data: 'id='+id+'&kd_salesorder='+kd_mso+'&tipe='+tipe,
			success:function(box) {
				$('#idMainContent').prepend(box);
				moveTo('idMainContent');
			}
		});
	}

	function show_items(id) {
		$('#idTableSalesAlert').fadeOut('slow', function() {
			$(this).html('');
		});
		$('#idFormItemBoxDataDO').remove();
		$('#idFormBoxDataDO').remove();
		$('#idFormBoxDataSO').remove();
		$('#idFormBoxDataSOItem').remove();
		$('#idBoxFormSalesOrder').remove();
		$('#idBoxForm').remove();
		$('#idBtnAddData').slideDown();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'sales/delivery_orders/items_do/get_form'; ?>',
			data: 'id='+id,
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	function additionalOrder(id) {
		$('#idTableSalesAlert').fadeOut('slow', function() {
			$(this).html('');
		});
		$('#idFormItemBoxDataDO').remove();
		$('#idFormBoxDataDO').remove();
		$('#idFormBoxDataSO').remove();
		$('#idFormBoxDataSOItem').remove();
		$('#idBoxFormSalesOrder').remove();
		$('#idBoxForm').remove();
		$('#idBtnAddData').slideDown();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/sales_orders_form/sales_form/'; ?>'+id+'/additional',
			success:function(html){
				// $('#idMainContent').prepend(html);
				// $('#idTxtNmSales').focus();
				// moveTo('idMainContent');
				// datepick('.datepickerbootstrap');
				editItems(id, 'Additional');
			}
		});
	}

	function cancel_so(id) {
		$('#idTableSalesAlert').fadeOut('slow', function() {
			$(this).html('');
		});
		$('#idFormItemBoxDataDO').remove();
		$('#idFormBoxDataDO').remove();
		$('#idFormBoxDataSO').remove();
		$('#idFormBoxDataSOItem').remove();
		$('#idBoxFormSalesOrder').remove();
		$('#idBoxForm').remove();
		$('#idBtnAddData').slideDown();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/sales_orders_tbl/cancel_so/'; ?>'+id,
			success:function(html){
				$('#idTableSalesAlert').html(html).fadeIn();
				moveTo('idMainContent');
			}
		});
	}

	function get_select_header_form(kd_mquotation) {
		$('#idFormItemBoxDataDO').remove();
		$('#idFormBoxDataDO').remove();
		$('#idFormBoxDataSO').remove();
		$('#idFormBoxDataSOItem').remove();
		$('#idBoxFormSalesOrder').remove();
		$('#idBoxForm').remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'sales/sales_orders/sales_orders_form/get_select_header_form'; ?>',
			data: {kd_mquotation:kd_mquotation},
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function get_select_item_form(kd_mquotation) {
		$('#idFormItemBoxDataDO').remove();
		$('#idFormBoxDataDO').remove();
		$('#idFormBoxDataSO').remove();
		$('#idFormBoxDataSOItem').remove();
		$('#idBoxFormSalesOrder').remove();
		$('#idBoxForm').remove();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'sales/sales_orders/sales_orders_form/get_select_item_form'; ?>',
			data: 'kd_mquotation='+kd_mquotation,
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function push_sap(kd_msalesorder){
		if(confirm('Apakah anda yakin ingin push SAP ?')){
			$.ajax({
				url: "<?php echo base_url($class_link.'/sales_orders_tbl/submit_to_sap'); ?>",
				type: "POST",
				data:  {kd_msalesorder: kd_msalesorder},
				beforeSend: function(){
					$('.btn-push-sap').prop('disabled', true);
				},
				success: function(resp){
					if(resp.code == 200){
						notify(resp.status, resp.pesan, 'success');
					}else{
						notify(resp.status, resp.pesan, 'error');
					}
					$('.btn-push-sap').prop('disabled', false);
					open_table();
				},
				error: function(err){
					notify("Gagal", "Error proses ketika push ke SAP", 'error');
					$('.btn-push-sap').prop('disabled', false);
				}
			});
		}
	}

	function notify(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            type: type,
            styling: 'bootstrap3'
        });
    }

</script>