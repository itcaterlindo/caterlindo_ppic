<?php
defined('BASEPATH') or exit('No direct script access allowed!');
?>

<script type="text/javascript">
	$('#idSelQuo').select2({
		theme: 'bootstrap',
		width: '100%',
	});

	$('#<?php echo $idbox_loader; ?>').fadeOut(500, function(e){
		$('#<?php echo $idbox_content; ?>').slideDown();
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	/* --start customer typeahead.js-- */
	var Customer = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		url: '<?php echo base_url(); ?>auto_complete/customer_quotation',
		remote: {
			url: '<?php echo base_url(); ?>auto_complete/customer_quotation',
			prepare: function (query, settings) {
				settings.type = 'GET';
				settings.contentType = 'application/json; charset=UTF-8';
				settings.data = 'customer_name='+query;

				return settings;
			},
			wildcard: '%QUERY'
		}
	});

	$('#idTxtNamaCustomer').typeahead(null, {
		limit: 20,
		minLength: 1,
		name: 'customer_search',
		display: 'Customer',
		valueKey: 'get_customer',
		source: Customer.ttAdapter()
	});

	$('#idTxtNamaCustomer').bind('typeahead:select', function(obj, selected) {
		fill_quo_dropdown(selected.Kode);
		$('#idTxtKdCustomer').val(selected.Kode);
		$('#idSelQuo').focus();
	});
	/* --end of customer typeahead.js-- */

	function fill_quo_dropdown(kd_customer) {
		$.ajax({
			url: '<?php echo base_url().$class_link; ?>/sales_orders_form/get_quo_dropdown',
			type: 'GET',
			data: 'kd_customer='+kd_customer,
			success: function(data){
				$('#idSelQuo').html(data);
				$('.select2').select2({
					theme: 'bootstrap',
				});
			}
		});
	}

	function transferSales(form_data) {
		$('#idBoxOverlayForm').show();
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url().'sales/quotations/transfer_sales'; ?>',
			data: form_data,
			success:function(data){
				// open_table();
				$('#idBoxOverlayForm').hide();
				if (data.confirm == 'success') {
					// $('#idTableSalesAlert').html(html.alert).fadeIn();
					$('#<?php echo $box_id; ?>').fadeOut(500, function(e){
						$('#<?php echo $box_id; ?>').remove();
						// show_selected_form('form_sales', id);
						get_select_header_form(data.kd_mquotation);
					});
				}
				else if (data.confirm == 'error') {
					$('#idErrForm').html(data.alert).fadeIn();
				}
				$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
			}
		});
	}

	function show_selected_form(sel, kd_quo) {
		var my_url = '';
		if (sel == 'form_sales') {
			my_url = '<?php echo base_url().$class_link; ?>/sales_orders_form/sales_form/'+kd_quo;
		}
		$.ajax({
			url: my_url,
			type: 'GET',
			success: function(html) {
				$('#idMainContent').prepend(html);
			}
		});
	}

	$(document).off('click', '#btnTambahQuo').on('click', '#btnTambahQuo', function() {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_link.'/sales_orders_form/get_add_quo_dropdown'; ?>',
			data: 'kd_customer='+$('#idTxtKdCustomer').val(),
			success: function(html) {
				$('#idFormAddQuo').prepend(html);
				$('.select2').select2({
					theme: 'bootstrap',
				});
			}
		});
	});

	$(document).off('click', '.btn-remove-add-quo').on('click', '.btn-remove-add-quo', function() {
		$(this).closest('.form-group').slideUp('fast', function() {
			$(this).closest('.form-group').remove();
		});
	});
</script>