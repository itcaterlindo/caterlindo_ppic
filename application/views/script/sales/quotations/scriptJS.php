<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	lihatTabel();

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function lihatTabel() {
		$('#idBtnTambah').show();
		$('#idLoaderTable').show();
		$('#idTable').html('');
		$('#idTable').fadeOut('slow', function(){
			$.ajax({
				type: 'GET',
				url: '<?php echo base_url().$class_name.'/data_lihat/'; ?>',
				success:function(html){
					$('#idTable').html(html);
				}
			});
			$('#idTable').fadeIn();
			$('#idLoaderTable').hide();
			moveTo('idMainContent');
		});
	}

	function removeForm() {
		$('#idBoxForm').remove();
		$('#idAlertForm').fadeOut();
		$('#idBoxFormItemQuotation').remove();
		$('#idAlertTableItemQuotation').fadeOut();
		$('#idBoxDetailItemQuotation').remove();
	}

	function formSuccess(idBox, msg, tabel, add) {
		if ($.isArray(idBox)) {
			$.each(idBox, function(index, value){
				$('#'+value).slideUp(500, function(){
					$('#'+value).remove();
				});
			});
		} else {
			$('#'+idBox).slideUp(500, function(){
				$('#'+idBox).remove();
			});
		}
		if (tabel == 'barang') {
			$('#idAlertTable').html(msg).fadeIn();
			lihatTabel();
		}
	}

	function showForm(id, link) {
		removeForm();
		$('#idAlertTable').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_name.'/'; ?>'+link,
			data: 'id='+id+'&link='+link,
			success:function(html){
				$('#idMainContent').prepend(html);
				$('#idTxtNmSales').focus();
				moveTo('idMainContent');
				datepick('#idTxtDate');
			}
		});
	}

	function showFormCustomer(id, link) {
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'manage_customer/data_customer/data_form/'; ?>',
			data: 'id='+id+'&link='+link,
			success:function(html){
				$('#idMainContent').prepend(html);
				$('#idSelJenis').focus();
				moveTo('idMainContent');
				$('.select2').select2({
					theme: 'bootstrap'
				});
			}
		});
	}

	function show_form_item(id, link) {
		removeForm();
		$('#idAlertTable').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'sales/quotation_items/'; ?>'+link,
			data: 'id='+id+'&link='+link,
			success:function(html){
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	function editData(id) {
		showForm(id, 'data_form');
		$('#idAlertTable').fadeOut();
	}

	function editItems(id) {
		show_form_item(id, 'data_form');
		$('#idAlertTable').fadeOut();
	}

	function hapusData(id) {
		removeForm();
		$('#idTableOverlay').show();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_name.'/hapus_data'; ?>',
			data: 'id='+id,
			success:function(html){
				$('#idAlertTable').html(html.alert).fadeIn();
				lihatTabel();
				$('#idTableOverlay').hide();
			}
		});
	}

	function alamat_customer(kd_customer, kd_alamat_kirim) {
		$.ajax({
			url: '<?php echo base_url().$class_name.'/dropdown_alamat'; ?>',
			type: 'GET',
			data: 'kd_customer='+kd_customer+'&kd_alamat_kirim='+kd_alamat_kirim,
			success: function(data){
				$('#idFormAlamatCustomer').html(data.form_alamat);
				$('.select2').select2({
					theme: 'bootstrap'
				});
			}
		});
	}

	function btn_customer(kd_customer) {
		$.ajax({
			url: '<?php echo base_url().$class_name.'/btn_customer'; ?>',
			type: 'GET',
			data: 'kd_customer='+kd_customer,
			success: function(data){
				$('#idFormBtnCustomer').html(data.btn_form);
			}
		});
	}

	function datepick(element) {
		$(element).datetimepicker({
			format: 'DD-MM-YYYY'
		});
	}

	function ubahStatus(kd_mquo, status) {
		$('#idTableOverlay').show();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_name.'/ubah_status'; ?>',
			data: 'id='+kd_mquo+'&status='+status,
			success:function(html){
				$('#idAlertTable').html(html.alert).fadeIn();
				lihatTabel();
				$('#idTableOverlay').hide();
			}
		});
	}

	function transferSales(id) {
		$('#idTableOverlay').show();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().$class_name.'/transfer_quo_so'; ?>',
			data: 'id='+id,
			success:function(html){
				$('#idAlertTable').html(html.alert).fadeIn();
				lihatTabel();
				$('#idTableOverlay').hide();
			}
		});
	}

	function viewQuotations(id) {
		removeForm();
		$('#idAlertTable').fadeOut();
		$.ajax({
			type: 'GET',
			url: '<?php echo base_url().'sales/quotation_items/detail_form'; ?>',
			data: 'id='+id,
			success:function(html){
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');
			}
		});
	}

	$(document).on('click', '#idBtnTambah', function(){
		$('#idBtnTambah').hide();
		showForm('', 'data_form');
	});

	$(document).on('click', '#idBtnTutup', function(){
		$('#idBtnTambah').show();
		lihatTabel();
	});
</script>