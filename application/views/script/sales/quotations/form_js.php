<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	$('.select2').select2({
		theme: 'bootstrap'
	});
				
	$(document).off('submit', '#idFormInputQuo').on('submit', '#idFormInputQuo', function(e) {
		$('#idOverlayFormQuo').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().$class_name.'/submit_form/input'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					formSuccess('idBoxForm', data.alert, 'barang', '');
					editItems(data.kd_mquotation);
				}
				if (data.confirm == 'errValidation') {
					$('#idTxtNmSales').focus();
					<?php
					foreach ($form_error as $data) :
						echo '$(\'#'.$data.'\').html(data.'.$data.');'."\n";
					endforeach;
					?>
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayFormQuo').hide();
				}
			}
		});
		
		return false;
	});

	$(document).off('submit', '#idFormEditQuo').on('submit', '#idFormEditQuo', function(e) {
		$('#idOverlayFormQuo').show();
		e.preventDefault();
		$.ajax({
			url: "<?php echo base_url().$class_name.'/submit_form/edit'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if (data.confirm == 'success') {
					formSuccess('idBoxForm', data.alert, 'barang', '');
					editItems(data.kd_mquotation);
				}
				if (data.confirm == 'errValidation') {
					$('#idTxtNmSales').focus();
					<?php
					foreach ($form_error as $data) :
						echo '$(\'#'.$data.'\').html(data.'.$data.');'."\n";
					endforeach;
					?>
					$('input[name="<?php echo $this->config->item('csrf_token_name'); ?>"]').val(data.csrf);
					$('#idOverlayFormQuo').hide();
				}
			}
		});
		
		return false;
	});

	/* --start salesperson typeahead.js-- */
	var Salesperson = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		prefetch: '<?php echo base_url(); ?>auto_complete/fill_sales',
		remote: {
			url: '<?php echo base_url(); ?>auto_complete/fill_sales',
			prepare: function (query, settings) {
				settings.type = 'GET';
				settings.contentType = 'application/json; charset=UTF-8';
				settings.data = 'name_startsWith='+query;

				return settings;
			},
			wildcard: '%QUERY'
		}
	});

	$('#idTxtNmSales').typeahead(null, {
		limit: 20,
		minLength: 1,
		name: 'sales_search',
		display: 'Salesperson',
		valueKey: 'get_sales',
		source: Salesperson.ttAdapter()
	});

	$('#idTxtNmSales').bind('typeahead:select', function(obj, selected) {
		$('#idTxtKdSales').val(selected.Kode);
		$('#idTxtEmailSales').val(selected.Email);
		$('#idTxtMobilePhone').val(selected.Telp);
		$('#idTxtNmCustomer').focus();
	});
	/* --end of salesperson typeahead.js-- */

	/* --start customer typeahead.js-- */
	var Customer = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		prefetch: '<?php echo base_url(); ?>auto_complete/fill_customer',
		remote: {
			url: '<?php echo base_url(); ?>auto_complete/fill_customer',
			prepare: function (query, settings) {
				settings.type = 'GET';
				settings.contentType = 'application/json; charset=UTF-8';
				settings.data = 'name_startsWith='+query;

				return settings;
			},
			wildcard: '%QUERY'
		}
	});

	$('#idTxtNmCustomer').typeahead(null, {
		limit: 20,
		minLength: 1,
		name: 'customer_search',
		display: 'Customer',
		valueKey: 'get_customer',
		source: Customer.ttAdapter()
	});

	$('#idTxtNmCustomer').bind('typeahead:select', function(obj, selected) {
		$('#idTxtKdCustomer').val(selected.Kode);
		$('#idTxtCustomerId').html(selected.Code);
		$('#idContactPerson').html(selected.Contact);
		$('#idTxtAlamatInstansi').html(selected.Alamat);
		alamat_customer(selected.Kode);
		btn_customer(selected.Kode);
	});
	/* --end of customer typeahead.js-- */

	function clearCustomerDetail() {
		$('#idTxtNmCustomer').val('');
		$('#idTxtKdCustomer').val('');
		$('#idTxtCustomerId').html('');
		$('#idContactPerson').html('');
		$('#idTxtAlamatInstansi').html('');
		$('#idTxtNmCustomer').focus();
		alamat_customer('');
		btn_customer('');
	}
</script>