<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript">
	function auto_complete(search_id, form_id, search_db, search_tbl, search_fields, search_obj, return_value) {
		$(search_id).autocomplete({
			source: function( request, response ) {
				$.ajax({
					url : '<?php echo base_url(); ?>auto_complete/fill_text',
					dataType: "json",
					data: {
						db: search_db,
						table: search_tbl,
						fields: search_fields,
						search: search_obj,
					},
					 success: function( data ) {
						 response( $.map( data, function( item ) {
							var code = item.split("|");
							return {
								label: code[0],
								value: code[0],
								data : item
							}
						}));
					}
				});
			},
			autoFocus: true,
			minLength: 0,
			select: function(event, ui) {
				var names = ui.item.data.split("|");
				var count = return_value.length;
				var angka = 1;
				for (i = 0; i < count; i++) {
					var posisi = i + angka;
					$(search_id).closest(return_value[i]).val(names[posisi]);
				}
			}
		});
	}

	function cek_lokasi (search_tbl, result_column, data_obj, input_obj, text_result) {
		$.ajax({
			url: "<?php echo base_url().$class_name.'/cek_lokasi'; ?>",
			type: "GET",
			data: {
				table: search_tbl,
				result: result_column,
				search: data_obj,
				input: input_obj,
			},
			success: function(data){
				text_result.val(data.kd_lokasi);
			}
		});
	}
</script>