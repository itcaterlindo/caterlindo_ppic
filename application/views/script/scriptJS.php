<?php
defined('BASEPATH') or exit('No direct script access allowed!');
$js_data = (isset($js_data) and !empty($js_data))?$js_data:'';
$idbox_loader = (isset($idbox_loader) and !empty($idbox_loader))?$idbox_loader:'idBoxLoaderDefault';
$idbox_content = (isset($idbox_content) and !empty($idbox_content))?$idbox_content:'idBoxContentDefault';
?>

<script type="text/javascript">
	$('#<?php echo $idbox_loader; ?>').fadeOut(500, function(e){
		$('#<?php echo $idbox_content; ?>').slideDown();
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}
</script>

<?php
if (isset($adds_js) and !empty($adds_js)) :
	foreach ($adds_js as $add_js) :
		$this->load->view('script/'.$class_link.'/'.$add_js, $js_data);
	endforeach;
endif;