<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo $profile_img; ?>" class="img-circle" alt="Admin Image" style="max-width: 45px;max-height: 45px;">
      </div>
      <div class="pull-left info">
        <p><?php echo $nm_admin; ?></p>
        <a href="<?php echo base_url(); ?>profile"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <!-- <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form> -->
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header"><?php echo strtoupper($menu_title); ?></li>
      <?php
      foreach ($menus as $menu_item) :
        if ($menu_item->link_menu != '#') :
          if ((empty($this->uri->segment(1)) && $menu_item->link_menu == 'dashboard') || $this->uri->segment(1) == $menu_item->link_menu) :
            $active = 'active';
          else :
            $active = '';
          endif;
          ?>
          <li class="<?php echo $active; ?>">
            <a href="<?php echo site_url($menu_item->link_menu); ?>">
              <i class="<?php echo $menu_item->icon_menu; ?>"></i> <span><?php echo ucwords($menu_item->nm_menu); ?></span>
            </a>
          </li>
          <?php
        elseif ($menu_item->link_menu == '#') :
          ?>
          <li class="treeview <?php echo ($this->uri->segment(1) == $menu_item->alt_title)?'active':''; ?>">
            <a href="<?php echo site_url($menu_item->link_menu); ?>">
              <i class="<?php echo $menu_item->icon_menu; ?>"></i> 
              <span><?php echo ucwords($menu_item->nm_menu); ?></span>  <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php
              foreach ($sub_menus as $sub_menu) :
                $sub_icon = (empty($sub_menu->icon_menu))?'fa fa-circle-o':$sub_menu->icon_menu;
                if ($sub_menu->link_menu != '#' && $sub_menu->parent_menu == $menu_item->id) :
                  $nm_submenu = $sub_menu->nm_menu;
                  $nm_menu = $nm_submenu == 'set ppn'?'Set PPN':ucwords($nm_submenu);
                  ?>
                  <li class="<?php echo ($this->uri->segment(2) == $sub_menu->link_menu)?'active':''; ?>">
                    <a href="<?php echo site_url($menu_item->alt_title.'/'.$sub_menu->alt_title); ?>">
                      <i class="<?php echo $sub_icon; ?>"></i> <?php echo $nm_menu; ?>
                    </a>
                  </li>
                  <?php
                elseif ($sub_menu->link_menu == '#' && $sub_menu->parent_menu == $menu_item->id) :
                  ?>
                  <li class="<?php echo ($this->uri->segment(2) == $sub_menu->alt_title)?'active':''; ?>">
                    <a href="<?php echo site_url($sub_menu->link_menu); ?>">
                      <i class="<?php echo $sub_icon; ?>"></i> 
                      <span><?php echo ucwords($sub_menu->nm_menu); ?></span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                      <?php
                      foreach ($sub_sub_menus as $sub_sub_menu) :
                        if ($sub_sub_menu->parent_menu == $sub_menu->id) :
                          $sub_sub_icon = (empty($sub_sub_menu->icon_menu))?'fa fa-circle-o':$sub_sub_menu->icon_menu;
                          ?>
                          <li class="<?php echo ($this->uri->segment(3) == $sub_sub_menu->link_menu)?'active':''; ?>">
                            <a href="<?php echo site_url($menu_item->alt_title.'/'.$sub_menu->alt_title.'/'.$sub_sub_menu->link_menu); ?>">
                              <i class="<?php echo $sub_sub_icon; ?>"></i> <?php echo ucwords($sub_sub_menu->nm_menu); ?>
                            </a>
                          </li>
                          <?php
                        endif;
                      endforeach;
                      ?>
                    </ul>
                  </li>
                  <?php
                endif;
              endforeach;
              ?>
            </ul>
          </li>
          <?php
        endif;
      endforeach;
      ?>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>