<footer class="main-footer">
	<div class="pull-right hidden-xs">
	  	<b>Version</b> 1.0.0
	</div>
	<strong>Copyright &copy; <?php echo $thn_mulai; ?>-<?php echo date('Y'); ?> <a href="<?php echo base_url(); ?>"><?php echo $footer_title; ?></a>.</strong> All rights reserved.
	<?php
	if (ENVIRONMENT == 'development') {
		echo '<center">Page rendered in <strong>' . $this->benchmark->elapsed_time() .'</strong></center>';
	}
	?>
</footer>