<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$segments = $this->uri->segment_array();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title.' | '.ucwords(str_replace('_', ' ', end($segments))); ?></title>
    <?php echo $this->load->get_section('favicon'); ?>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_assets/bootstrap/css/bootstrap.min.css">
    <?php
    /** -- Copy from here -- */
    if(!empty($meta)) {
      foreach($meta as $name=>$content){
        echo "\n\t\t";
            ?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" /><?php
      }
      echo "\n";
    }
    if(!empty($canonical)) {
      echo "\n\t\t";
          ?><link rel="canonical" href="<?php echo $canonical?>" /><?php
    }
    echo "\n\t";
     
    foreach($css as $file) {
      echo "\n\t\t";
          ?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
    }
    echo "\n\t";
     
    /** -- to here -- */
    ?>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_assets/bootstrap/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_assets/dist/css/AdminLTE.min.css"">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_assets/dist/css/skins/skin-red.min.css">

    <script src="<?php echo base_url(); ?>assets/admin_assets/plugins/jQuery-3.2.1/jquery-3.2.1.min.js"></script>    
    <script src="<?php echo base_url(); ?>assets/admin_assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-red sidebar-mini fixed">
    <div class="wrapper">

      <?php echo $this->load->get_section('header'); ?>

      <?php echo $this->load->get_section('sidebar'); ?>

      <div class="content-wrapper">
        <section class="content-header">
          <?php echo $this->load->get_section('breadcrumb'); ?>
        </section>

        <section id="idMainContent" class="content" style="padding-bottom: 250px;">

          <?php echo $output; ?>

        </section>
      </div>

      <?php echo $this->load->get_section('footer'); ?>

      <?php echo $this->load->get_section('control_sidebar'); ?>
    </div>
    
    <?php echo $this->load->get_section('scriptJS'); ?>

    <?php
    foreach($js as $file) {
      echo "\n\t\t";
          ?><script src="<?php echo $file; ?>"></script><?php
    }
    echo "\n\t";
    ?>
    <script src="<?php echo base_url(); ?>assets/admin_assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin_assets/plugins/fastclick/fastclick.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin_assets/dist/js/app.min.js"></script>
    <script type="text/javascript">
      $(document).on('click', '#idBtnCollapseTable', function(){
        $('#idBtnCollapseTable').attr('id', 'idBtnExtendTable');
        $('#idBtnExtendTable').attr('data-original-title', 'Tampilkan');
      });
      $(document).on('click', '#idBtnExtendTable', function(){
        $('#idBtnExtendTable').attr('id', 'idBtnCollapseTable');
        $('#idBtnCollapseTable').attr('data-original-title', 'Sembunyikan');
      });
      $(document).on('click', '#idBtnCollapseForm', function(){
        $('#idBtnCollapseForm').attr('id', 'idBtnExtendForm');
        $('#idBtnExtendForm').attr('data-original-title', 'Tampilkan');
      });
      $(document).on('click', '#idBtnExtendForm', function(){
        $('#idBtnExtendForm').attr('id', 'idBtnCollapseForm');
        $('#idBtnCollapseForm').attr('data-original-title', 'Sembunyikan');
      });
    </script>

    <script type="text/javascript">
      $(document).on('click', '.btn-collapse', function(){
        $(this).attr('data-original-title', 'Tampilkan');
        $(this).addClass('btn-extend');
        $(this).removeClass('btn-collapse');
      });
      $(document).on('click', '.btn-extend', function(){
        $(this).attr('data-original-title', 'Sembunyikan');
        $(this).addClass('btn-collapse');
        $(this).removeClass('btn-extend');
      });
    </script>
  </body>
</html>