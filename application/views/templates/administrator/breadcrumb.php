<h1>
	<?php echo $home_title; ?>
	<small><?php echo $main_title; ?></small>
</h1>
<ol class="breadcrumb">
	<?php
	$lvl_one = (empty($this->uri->segment(1)))?'dashboard':$this->uri->segment(1);
	if (strpos($lvl_one, '_') !== FALSE ) :
		$lvl_one = str_replace('_', ' ', $lvl_one);
	endif;
	$lvl_one = ucwords($lvl_one);
	?>
	<li>
		<i class="fa fa-dashboard"></i> 
		<?php
		if (empty($this->uri->segment(2))) :
			echo '<a href="'. site_url($this->uri->segment(1)) .'">'. $lvl_one .'</a>';
		else :
			echo $lvl_one;
		endif;
		?>
	</li>
	<?php
	if (!empty($this->uri->segment(2))) :
		?>
		<li class="<?php echo (empty($this->uri->segment(3)))?'active':''; ?>">
			<?php
			$lvl_two = $this->uri->segment(2);
			if (strpos($lvl_two, '_') !== FALSE) :
				$lvl_two = str_replace('_', ' ', $lvl_two);
			endif;
			$lvl_two = ucwords($lvl_two);
			if (empty($this->uri->segment(3))) :
				echo '<a href="'. site_url($this->uri->segment(1).'/'.$this->uri->segment(2)) .'">'. $lvl_two .'</a>';
			else :
				echo $lvl_two;
			endif;
			?>
		</li>
		<?php
		if (!empty($this->uri->segment(3))) :
			echo '<li class="active">';
			$lvl_three = $this->uri->segment(3);
			if (strpos($lvl_three, '_') !== FALSE) :
				$lvl_three = str_replace('_', ' ', $lvl_three);
			endif;
			$lvl_three = ucwords($lvl_three);
			echo '<a href="'. site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3)) .'">'. $lvl_three .'</a>';
			echo '</li>';
		endif;
	endif;
	?>
</ol>