<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH."third_party/GuzzleHttp/autoload.php";
use GuzzleHttp\Client;

class MY_Controller extends CI_Controller {

	function __construct() {
		parent::__construct();
		
		$this->load->library(array('session'));
		$this->load->helper(array('url', 'array', 'html', 'my_helper'));
		// Cek Session
		if (!isset($this->session->kd_admin)) :
			$this->session->sess_destroy();
			$uri = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			?>
			<script>
				alert('Maaf Anda harus login dahulu!');
				window.location.replace('<?php echo base_url(); ?>login?uri=<?php echo $uri; ?>');
			</script>
			<?php
		endif;

		// $this->_init();
	}

	private function _init() {
		// $this->output->cache(60);
		
		// $this->load->section('ace_setting', 'templates/ace_setting');

		/* $this->load->js('assets/themes/default/js/jquery-1.9.1.min.js');
		$this->load->js('assets/themes/default/hero_files/bootstrap-transition.js');
		$this->load->js('assets/themes/default/hero_files/bootstrap-collapse.js'); */
	}

	public function administrator() {
		/* set content template */
		$this->output->set_template('administrator/admin_tpl');

		// Mengambil setting dari tabel setting
		$this->load->model('tb_setting/m_setting');
		$this->load->model('m_builder');
		$title = $this->m_setting->getSetting('header_title');
		$this->db->trans_start(); // Memulai query
		$data['main_title'] = $this->m_setting->getSetting('main_title');
		$data['home_title'] = $this->m_setting->getSetting('home_title');
		$data['favicon'] = $this->m_setting->getSetting('favicon');
		$data['thn_mulai'] = $this->m_setting->getSetting('thn_mulai');
		$data['footer_title'] = $this->m_setting->getSetting('footer_title');
		$data['main_bg_img'] = $this->m_setting->getSetting('main_bg_img');
		$data['menu_title'] = $this->m_setting->getSetting('menu_title');

		// Mengambil detail data admin.
		$data['nm_admin'] = $this->session->nm_admin;
		$data['tipe_admin'] = $this->session->tipe_admin;
		$data['profile_img'] = $this->session->profile_img;

		$this->m_setting->delUnusedImg('assets/admin_assets/dist/img/users/', 'tb_karyawan', 'pas_foto');
		$this->m_setting->delUnusedLocalImg('assets/admin_assets/dist/img/product_media/product_image/', 'td_barang_media', 'media_product');
		$this->m_setting->delUnusedLocalImg('assets/admin_assets/dist/img/product_media/sketch_welding/', 'td_barang_media', 'media_sketch_welding');
		$this->m_setting->delUnusedLocalImg('assets/admin_assets/dist/img/product_media/sketch_revet/', 'td_barang_media', 'media_sketch_revet');
		$this->m_setting->delUnusedLocalImg('assets/admin_assets/dist/img/attachment_po/', 'tm_salesorder', 'file_attach_po');
		$this->m_setting->delUnusedLocalImg('assets/admin_assets/dist/img/attachment_do/', 'tm_deliveryorder', 'do_attachment');
		$this->m_setting->delUnusedLocalImg('assets/admin_assets/dist/img/setting_img/', 'tb_setting', 'val_setting');
		$this->output->set_common_meta($title, '', '');

		// Mengambil data menu berdasarkan data admin
		// Untuk menampilkan sidebar dari aplikasi seuai dengan hak akses
		$this->db->from('tb_hakakses_menu');
		$this->db->join('tb_menu', 'tb_menu.id = tb_hakakses_menu.menu_kd');
		$this->db->where(array('tb_hakakses_menu.tipe_admin_kd' => $this->session->tipe_admin_kd, 'level_menu' => '0'));
		$this->db->where(array('tb_menu.aktif' => '1'));
		$this->db->order_by('tb_menu.urutan ASC');
		$q_menu = $this->db->get();
		$r_menu = $q_menu->result();
		$data['menus'] = $r_menu;
		// Menu level 1
		$this->db->from('tb_hakakses_menu');
		$this->db->join('tb_menu', 'tb_menu.id = tb_hakakses_menu.menu_kd');
		$this->db->where(array('tb_hakakses_menu.tipe_admin_kd' => $this->session->tipe_admin_kd, 'level_menu' => '1'));
		$this->db->where(array('tb_menu.aktif' => '1'));
		$this->db->order_by('tb_menu.urutan ASC');
		$q_menu = $this->db->get();
		$r_menu = $q_menu->result();
		$data['sub_menus'] = $r_menu;
		// Menu level 2
		$this->db->from('tb_hakakses_menu');
		$this->db->join('tb_menu', 'tb_menu.id = tb_hakakses_menu.menu_kd');
		$this->db->where(array('tb_hakakses_menu.tipe_admin_kd' => $this->session->tipe_admin_kd, 'level_menu' => '2'));
		$this->db->where(array('tb_menu.aktif' => '1'));
		$this->db->order_by('tb_menu.urutan ASC');
		$q_menu = $this->db->get();
		$r_menu = $q_menu->result();
		$data['sub_sub_menus'] = $r_menu;
		$this->db->trans_complete();  // Akhir query
		
		$this->load->section('favicon', 'templates/administrator/favicon', $data);
		$this->load->section('header', 'templates/administrator/header', $data);
		$this->load->section('sidebar', 'templates/administrator/sidebar', $data);
		$this->load->section('breadcrumb', 'templates/administrator/breadcrumb');
		$this->load->section('footer', 'templates/administrator/footer', $data);
		$this->load->css('assets/admin_assets/plugins/DataTables-1.10.15/media/css/dataTables.bootstrap.min.css');
		$this->load->css('assets/admin_assets/plugins/DataTables-1.10.15/extensions/FixedColumns/css/fixedColumns.bootstrap.min.css');
		$this->load->css('assets/admin_assets/plugins/DataTables-1.10.15/extensions/Responsive/css/responsive.bootstrap.min.css');
		$this->load->css('assets/admin_assets/plugins/DataTables-1.10.15/extensions/FixedHeader/css/fixedHeader.dataTables.min.css');

		$this->load->js('assets/admin_assets/plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js');
		$this->load->js('assets/admin_assets/plugins/DataTables-1.10.15/media/js/dataTables.bootstrap.min.js');
		$this->load->js('assets/admin_assets/plugins/DataTables-1.10.15/extensions/FixedColumns/js/dataTables.fixedColumns.min.js');
		$this->load->js('assets/admin_assets/plugins/DataTables-1.10.15/extensions/Responsive/js/dataTables.responsive.min.js');
		$this->load->js('assets/admin_assets/plugins/DataTables-1.10.15/extensions/Responsive/js/responsive.bootstrap.min.js');
		$this->load->js('assets/admin_assets/plugins/DataTables-1.10.15/extensions/FixedHeader/js/dataTables.fixedHeader.min.js');
		
		/** export button */
		$this->load->css('assets/admin_assets/plugins/DataTables-1.10.15/extensions/Buttons-1.5.6/css/buttons.bootstrap.min.css');
		$this->load->js('assets/admin_assets/plugins/DataTables-1.10.15/extensions/Buttons-1.5.6/js/dataTables.buttons.min.js');
		$this->load->js('assets/admin_assets/plugins/DataTables-1.10.15/extensions/Buttons-1.5.6/js/buttons.bootstrap4.min.js');
		$this->load->js('assets/admin_assets/plugins/DataTables-1.10.15/extensions/Buttons-1.5.6/js/buttons.html5.min.js');
		$this->load->js('assets/admin_assets/plugins/DataTables-1.10.15/extensions/Buttons-1.5.6/js/buttons.print.min.js');
		$this->load->js('assets/admin_assets/plugins/DataTables-1.10.15/extensions/JSZip-2.5.0/jszip.min.js');
		$this->load->js('https://malsup.github.io/jquery.blockUI.js');


		// $this->load->section('control_sidebar', 'templates/administrator/control_sidebar');
	}

	public function admin_print() {
		/* set content template */
		$this->output->set_template('administrator/admin_print_tpl');

		// Mengambil setting dari tabel setting
		$this->load->model('tb_setting/m_setting');
		$this->load->model('m_builder');
		$title = $this->m_setting->getSetting('header_title');
		$this->db->trans_start();
		$data['favicon'] = $this->m_setting->getSetting('favicon');
		$this->output->set_common_meta($title, '', '');
		
		$this->load->section('favicon', 'templates/administrator/favicon', $data);
	}

	public function admin_print_label() {
		/* set content template */
		$this->output->set_template('administrator/admin_print_label');

		// Mengambil setting dari tabel setting
		$this->load->model('tb_setting/m_setting');
		$this->load->model('m_builder');
		$title = $this->m_setting->getSetting('header_title');
		$this->db->trans_start();
		$data['favicon'] = $this->m_setting->getSetting('favicon');
		$this->output->set_common_meta($title, '', '');
		
		$this->load->section('favicon', 'templates/administrator/favicon', $data);
	}

	public function icheck_assets() {
		$this->load->css('assets/admin_assets/plugins/iCheck/all.css');
		$this->load->js('assets/admin_assets/plugins/iCheck/icheck.min.js');
	}

	public function select2_assets() {
		$this->load->css('assets/admin_assets/plugins/select2/select2.min.css');
		$this->load->css('assets/admin_assets/plugins/select2/select2-bootstrap.min.css');
		$this->load->js('assets/admin_assets/plugins/select2/select2.full.min.js');
	}

	public function typeahead_assets() {
		$this->load->js('assets/admin_assets/plugins/typeahead.js/typeahead.bundle.min.js');
		$this->load->css('assets/admin_assets/plugins/typeahead.js/typeahead.css');
	}

	public function datetimepicker_assets() {
		$this->load->js('assets/admin_assets/plugins/moment.js/moment.min.js');
		$this->load->js('assets/admin_assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js');
		$this->load->js('assets/admin_assets/plugins/timepicker/bootstrap-timepicker.min.js');
		$this->load->css('assets/admin_assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css');
		$this->load->css('assets/admin_assets/plugins/timepicker/bootstrap-timepicker.min.css');
	}

	public function ionicons_assets() {
		$this->load->css('assets/admin_assets/bootstrap/fonts/ionicons-2.0.1/css/ionicons.min.css');
	}

	public function pnotify_assets() {
		$this->load->css('assets/admin_assets/plugins/pnotify/dist/pnotify.css');
        $this->load->css('assets/admin_assets/plugins/pnotify/dist/pnotify.buttons.css');
        $this->load->css('assets/admin_assets/plugins/pnotify/dist/pnotify.nonblock.css');
        $this->load->js('assets/admin_assets/plugins/pnotify/dist/pnotify.js');
        $this->load->js('assets/admin_assets/plugins/pnotify/dist/pnotify.buttons.js');
        $this->load->js('assets/admin_assets/plugins/pnotify/dist/pnotify.nonblock.js');
	}

	public function input_mask() {
		$this->load->js('assets/admin_assets/plugins/input-mask/jquery.inputmask.js');
	}

	public function colorpicker_assets() {
		$this->load->css("assets/admin_assets/plugins/colorpicker/bootstrap-colorpicker.min.css");
		$this->load->js("assets/admin_assets/plugins/colorpicker/bootstrap-colorpicker.min.js");
	}
	
	public function fullcalendar_assets() {
		$this->load->css("assets/admin_assets/plugins/fullcalendar/fullcalendar.min.css");
		$this->load->js("assets/admin_assets/plugins/moment.js/moment.min.js");
		$this->load->js("assets/admin_assets/plugins/fullcalendar/fullcalendar.min.js");
	}

	public function fullcalendarv5_assets() {
		$this->load->css("assets/admin_assets/plugins/fullcalendarv5/main.min.css");
		$this->load->js("assets/admin_assets/plugins/fullcalendarv5/main.min.js");
	}

	public function chartjs_assets()
	{
		$this->load->js("assets/admin_assets/plugins/chartjs-3.7.1/chart.min.js");
	}

	public function handsonetable()
	{
		$this->load->js('https://cdn.jsdelivr.net/npm/handsontable/dist/handsontable.full.min.js');
		$this->load->css("https://cdn.jsdelivr.net/npm/handsontable/dist/handsontable.full.min.css");
	}

	public function fixedHeaderDataTable()
	{
		$this->load->js('assets/admin_assets/plugins/FixedHeader-3.3.1/js/dataTables.fixedHeader.min.js');
		$this->load->css('assets/admin_assets/plugins/FixedHeader-3.3.1/css/fixedHeader.dataTables.min.css');
	}

	// --- FOR API SAP -- //
	public function api_sap_get($path)
	{
		$resp_array = [];
		try{
			/** URL api disimpan di db tb_setting */
			$this->load->model('tb_setting/m_setting');
			$urlAPI = $this->m_setting->getSetting('api_sap');
			/** Variabel data = data yang akan disimpan/update (post) */
			$client = new Client([
				'base_uri' => $urlAPI,
			]);
			$response = $client->request('GET', $path);
			$body = $response->getBody();
			array_push($resp_array, json_decode($body));
			return $resp_array;
			/** Contoh pemanggilan respon : $array[0]->ErrorCode, $array[0]->Message  */
		}catch(Exception $e){
			$data_resp = [];
			/** Format respon disamakan dengan format respon SAP, code error 500 adalah dari exception ini */
			$data_resp[0] = [
				'ErrorCode' => 500,
				'Message' => $e->getMessage()
			];
			/** Convert array to object */
			$resp_array = json_decode(json_encode($data_resp));
			return $resp_array;
		}

	}

	public function api_sap_post($path, $data = [])
	{
		$resp_array = [];
		try{
			/** URL api disimpan di db tb_setting */
			$this->load->model('tb_setting/m_setting');
			$urlAPI = $this->m_setting->getSetting('api_sap');
			/** Variabel data = data yang akan disimpan/update (post) */
			$client = new Client([
				'base_uri' => $urlAPI,
			]);	
			$response = $client->request('POST', $path, [
				'headers' => ['User-Agent' => null],
				'json' => $data
			]);
			$body = $response->getBody();
			array_push($resp_array, json_decode($body));
			$resp_array[0]->Data = $data; 
			return $resp_array;
			/** Contoh pemanggilan respon : $array[0]->ErrorCode, $array[0]->Message  */
		}catch(Exception $e){
			$data_resp = [];
			/** Format respon disamakan dengan format respon SAP, code error 500 adalah dari exception ini */
			$data_resp[0] = [
				'ErrorCode' => 500,
				'Message' => $e->getMessage(),
				'Data' => $data
			];
			/** Convert array to object */
			$resp_array = json_decode(json_encode($data_resp));
			return $resp_array;
		}
	}
	// --- END FOR API SAP -- //

}