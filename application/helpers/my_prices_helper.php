<?php
defined('BASEPATH') or exit('No direct script access allowed!');

function prices_process($retail_curr_type = '', $this_curr_type = '', $retail_price = '',  $this_price = '', $retail_conv_val = '', $this_conv_val = '', $type_sales = '', $set_disc = '', $item_disc = '', $set_disc_type = '', $item_disc_type = '') {
	/* == Check Sales type == */
	$price_disc = 0;
	if ($type_sales == 'Lokal') :
		/* == Check retail currency type == */
		$this_conv_val = empty($this_conv_val)?'1':$this_conv_val;
		if ($retail_curr_type == 'primary') :
			$retail_price = $retail_price / $this_conv_val;
		elseif ($retail_curr_type == 'secondary') :
			 $retail_price = $retail_price * $this_conv_val;
		endif;
		/* == Check this currency type == */
		$retail_conv_val = empty($retail_conv_val)?'1':$retail_conv_val;
		if ($this_curr_type == 'primary') :
			$this_price = $this_price / $retail_conv_val;
		elseif ($this_curr_type == 'secondary') :
			 $this_price = $this_price * $retail_conv_val;
		endif;
		/* == Price disc value of difference between retail and this price == */
		$price_disc = $retail_price - $this_price;
	endif;
	/* == Item disc value == */
	$item_disc = count_disc($item_disc_type, $item_disc, $this_price);
	/* == Default disc value == */
	$default_disc = count_disc($set_disc_type, $set_disc, $this_price);

	$data['tot_disc'] = $price_disc + $item_disc + $default_disc;
	$data['pure_disc'] = $item_disc + $default_disc;
	$data['retail_price'] = $retail_price - $data['tot_disc'];
	$data['this_price'] = $this_price - $data['pure_disc'];
	return $data;
}