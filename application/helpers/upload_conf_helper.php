<?php
defined('BASEPATH') or exit('No direct script access allowed!');

function do_upload_config($path = '', $exts = '', $max_name = '50') {
	$config['upload_path'] = $path;
	$config['allowed_types'] = $exts;
	$config['encrypt_name'] = TRUE;
	$config['max_filename'] = $max_name;
	$config['detect_mime'] = TRUE;
	return $config;
}

function process_image_conf($path, $img_name) {
	$config['image_library'] = 'gd2';
	$config['source_image'] = $path.$img_name;
	$config['maintain_ratio'] = TRUE;
	$config['width'] = 150;
	return $config;
}