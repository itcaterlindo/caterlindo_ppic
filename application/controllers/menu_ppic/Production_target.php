<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Production_target extends MY_Controller
{
    private $class_link = 'menu_ppic/production_target';

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ssp', 'form_validation'));
        $this->load->model(['tb_production_target']);
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
    }

    public function index()
    {
        parent::administrator();
        parent::select2_assets();
        parent::pnotify_assets();
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function table_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }

    public function table_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data = $this->tb_production_target->ssp_table();
        echo json_encode(
            SSP::simple($_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'])
        );
    }

    public function form_box()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $id = $this->input->get('id', true);
        $slug = $this->input->get('slug', true);

        //get data target jika opsi edit
        if ($slug == 'edit') {
            $data['row'] = $this->tb_production_target->get_by_param_in('id_target', $id)->first_row();
            // $data['angka_target'] = $targets->angka_target;
        }

        // Initiate tahun
        $interval = 10;
        $year = date('Y');
        $data['year'] = [];
        for ($i = 0; $i < $interval; $i++) {
            array_push($data['year'], $year);
            $year = $year - 1;
        }

        $data['id_target'] = $id;
        $data['slug'] = $slug;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_box', $data);
    }

    public function action_insert()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtangka_target', 'AngkaTarget', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) :
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'iderrangka_target' => (!empty(form_error('txtangka_target'))) ? buildLabel('warning', form_error('txtangka_target', '"', '"')) : '',
            );

        else :
            $slug = $this->input->post('txtslug', true);
            $bulan_target = $this->input->post('txtbln_target', true);
            $tahun_target = $this->input->post('txttahun_target', true);
            $angka_target = $this->input->post('txtangka_target', true);
            $id_target = $this->input->post('txtid_target', true);

            $data = [
                'bulan_target' => !empty($bulan_target) ? $bulan_target : 1,
                'tahun_target' => !empty($tahun_target) ? $tahun_target : null,
                'angka_target' => !empty($angka_target) ? $angka_target : null,
                'admin_kd' => $this->session->userdata('kd_admin'),
            ];

            $act = false;
            if (empty($id_target)) {
                //verify duplicate datas
                $target = $this->tb_production_target->get_by_param(array('bulan_target'=> $bulan_target,'tahun_target'=> $tahun_target))->num_rows();
                if ($target == 0) {
                    $act = $this->tb_production_target->insert_data($data);
                }
            } else {
                $act = $this->tb_production_target->update_data(['id_target' => $id_target], $data);
            }

            if ($act) :
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            else :
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            endif;

        endif;

        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    function action_delete () {
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
        $id_target = $this->input->get('id');
        if (!empty($id_target)){
            $act = $this->tb_production_target->delete_data($id_target);
            if ($act) {
                $resp = array('code' => 200, 'pesan' => 'Sukses');
            }
            else{
                $resp = array('code' => 400, 'pesan' => 'Gagal Delete');
            }
        }
        else{
            $resp = array('code' => 400, 'pesan' => 'Id Tidak Ditemukan');
        }
        echo json_encode($resp);
    }
}
