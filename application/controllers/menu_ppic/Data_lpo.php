<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Data_lpo extends MY_Controller {
	private $p_key = '';
	private $class_link = 'menu_ppic/data_lpo';

	public function __construct() {
		parent::__construct();

		$this->load->library(array('ssp', 'form_validation'));
		$this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
		$this->load->model(array('m_builder', 'tm_salesorder', 'model_salesorder', 'm_salesorder', 'tb_tipe_harga', 'tm_currency', 'td_currency_convert', 'm_format_laporan', 'tm_customer', 'td_customer_alamat', 'td_salesorder_harga', 'tm_customer', 'td_salesorder_item_disc', 'td_salesorder_harga', 'td_salesorder_item', 'td_salesorder_item_detail'));
	}

	public function index() {
		parent::administrator();
		parent::datetimepicker_assets();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/box_table', $data);
	}

	public function open_table() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table', $data);
	}

	public function table_data() {
		$data = $this->tm_salesorder->ssp_table(array('process_lpo', 'process_wo'));
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function open_detail() {
		$data = $this->detail_data($this->input->get('kode'));
		$data['page_type'] = 'view';
		$this->load->view('page/'.$this->class_link.'/box_detail', $data);
	}

	private function detail_data($kd_msalesorder) {
		$data['title'] = 'Local Production Order';
		$data['class_link'] = $this->class_link;
		$data['master'] = $this->model_salesorder->read_masterdata($kd_msalesorder);
		$nm_laporan = $data['master']['tipe_customer'] == 'Lokal'?'localproductionorder':'eksporproductionorder';
		$data['format_laporan'] = $this->m_format_laporan->view_laporan($nm_laporan, $data['master']['term_payment_format']);
		$master_items = $this->m_salesorder->salesOrderItems($kd_msalesorder);
		foreach ($master_items as $master_item) :
			$kd_ditem_so[] = $master_item->kd_ditem_so;
		endforeach;
		$data['master_item'] = $master_items;
		$data['detail_item'] = $this->m_salesorder->item_child($kd_ditem_so);
		$data['kd_msalesorder'] = $kd_msalesorder;
		return $data;
	}

	public function cetak($kd_msales) {
		parent::admin_print();
		$data = $this->detail_data($kd_msales);
		$data['page_type'] = 'print';
		$this->load->view('page/'.$this->class_link.'/print_detail', $data);
	}

	public function ubah_status() {
		$this->load->model('db_caterlindo/tm_po');
		$kode['kd_msalesorder'] = $this->input->get('kode');
		$status['status_so'] = $this->input->get('status');
		$var = $this->input->get('no_so');
		$del['confirm'] = 'success';
		if ($status['status_so'] == 'pending') :
			$del = $this->tm_po->delete_data($var);
		endif;
		if ($del['confirm'] == 'success') :
			$act = $this->tm_salesorder->ubah_status($kode, $status, $var);
			echo $act;
		else :
			echo $del;
		endif;
	}

	public function get_form() {
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = array('idErrKdMso', 'idErrTglKirim', 'idErrTglStuff');
		$data['kd_mso'] = $this->input->get('kd_mso');
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function open_form() {
		$data['master'] = $this->tm_salesorder->get_row($this->input->get('kd_mso'));
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function send_data() {
		if ($this->input->is_ajax_request()) :
			$this->db->trans_begin();
			$this->form_validation->set_rules($this->tm_salesorder->form_rules_tgl_kirim());
			if ($this->form_validation->run() == FALSE) :
				$str = $this->tm_salesorder->build_warning_tgl_kirim(array('idErrKdMso', 'idErrTglKirim', 'idErrTglStuff'));
				$str['confirm'] = 'error';
			else :
				$row['kd_msalesorder'] = $this->input->post('txtKdMso');
				$row['tgl_kirim'] = format_date($this->input->post('txtTglKirim'), 'Y-m-d');
				$row['tgl_selesai'] = format_date($this->input->post('txtTglSelesai'), 'Y-m-d');
				$str = $this->tm_salesorder->update_tgl_kirim($row);

				if($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					$log_stat = 'gagal mengubah tanggal kirim';
					$str['confirm'] = 'error';
					$str['alert'] = buildAlert('danger', 'Gagal!', ' '.$log_stat.'!');
				}else{
					$api = $this->edit_push_to_sap($row['kd_msalesorder']);
					if ($api[0]->ErrorCode != 0) {
						$this->db->trans_rollback();
						$log_stat = 'gagal mengubah tanggal kirim';
						$str['confirm'] = 'error';
						$str['alert'] = buildAlert('danger', 'Gagal!', ' '.$log_stat.'!, API '.$api[0]->Message);
						$str['data_json'] = $api[0]->Data;
					}else{
						$this->db->trans_commit();
						$log_stat = 'berhasil mengubah tanggal kirim';
						$str['confirm'] = 'success';
						$str['alert'] = buildAlert('success', 'Berhasil!', ' '.$log_stat.'!, API '.$api[0]->Message);
						$str['data_json'] = $api[0]->Json;
					}
				}
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function edit_push_to_sap($kd_msalesorder)
	{
		/** Action edit to SAP */
		$so['so_master'] 		= $this->tm_salesorder->get_row($kd_msalesorder);
		$dataAPI = [
			'U_IDU_WEBID' => $so['so_master']->kd_msalesorder,
			'DocDueDate' => $so['so_master']->tgl_kirim,
			'U_IDU_WEBUSER' => $this->session->username,
		]; 
		$api = parent::api_sap_post('EditTglKirimSalesOrder', $dataAPI);
		$api[0]->Data = $dataAPI;
		return $api;
	}

}