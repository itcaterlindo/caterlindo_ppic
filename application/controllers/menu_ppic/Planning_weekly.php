<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Planning_weekly extends MY_Controller
{
    private $class_link = 'menu_ppic/planning_weekly';
    private $wf_kd = 4;

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('ssp', 'form_validation'));
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
        $this->load->model(array(
            'tm_planningweekly', 'tb_bagian', 'td_workflow_state', 'td_workorder_item', 'td_planningweekly_detail',
            'td_workflow_transition_notification', 'td_planningweekly_log', 'td_workflow_transition', 'tb_deliverynote_user', 'td_materialrequisition_detail'
        ));
    }

    public function index()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();
        parent::datetimepicker_assets();

        $data['class_link'] = $this->class_link;

        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function table_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }

    public function table_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data = $this->tm_planningweekly->ssp_table2();

        echo json_encode($data);
    }

    public function table_detail_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_detail_main', $data);
    }

    public function table_detail_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $data = $this->td_planningweekly_detail->ssp_table($data['id']);

        echo json_encode(
            SSP::simple($_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'])
        );
    }

    public function form_box()
    {
        parent::administrator();
        parent::pnotify_assets();

        if (!cek_permission('WEEKLYPLANNING_UPDATE')) {
            show_error('Anda tidak memiliki akses ! <br> <a href="' . base_url() . $this->class_link . '"> Weekly Planning </a>', '202', 'Error Unauthorized');
        }

        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $data['class_link'] = $this->class_link;
        $data['header'] = $this->tm_planningweekly->getRowPlanningWeeklyBagianState($data['id']);

        $this->load->view('page/' . $this->class_link . '/form_box', $data);
    }

    public function form_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_main', $data);
    }

    public function form_tablesuggestwo()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $data['class_link'] = $this->class_link;
        $qMaster = $this->tm_planningweekly->get_by_param(['planningweekly_kd' => $data['id']])->row_array();
        $qResults = $this->db->select('td_workorder_item.*, td_deliverynote_received.dndetailreceived_qty,
                tm_deliverynote.*')
            ->join('td_deliverynote_received', 'td_deliverynote_received.woitem_kd=td_workorder_item.woitem_kd', 'left')
            ->join('tm_deliverynote', 'tm_deliverynote.dn_kd=td_deliverynote_received.dn_kd', 'left')
            ->group_start()
            ->where('tm_deliverynote.dn_asal', $qMaster['bagian_kd'])
            ->or_where('tm_deliverynote.dn_tujuan', $qMaster['bagian_kd'])
            ->group_end()
            ->where('tm_deliverynote.dn_asal is null OR tm_deliverynote.dn_tujuan is null')     //revisi add filter = null
            ->where('td_workorder_item.woitem_prosesstatus', 'workorder')
            ->get('td_workorder_item')->result_array();

        $woSuggests = [];
        $woitem_kds = [];
        foreach ($qResults as $result) {
            $woSuggests[$result['woitem_kd']][] = $result;
            /** Get Woitem_kd untuk transaksi yang sudah masuk di dteail weekly planning
             * agar dapat digunakan unutk pengurangan qty
             */
            if (!in_array($result['woitem_kd'], $woitem_kds)) {
                $woitem_kds[] = $result['woitem_kd'];
            }
        }

        /** Search yang sudah input di planning weekly detail */
        $planningDetails = $this->td_planningweekly_detail->getItemInsertedGroupByWoitem($qMaster['bagian_kd'], $woitem_kds);
        $woSuggestResults = [];
        $i = 0;
        foreach ($woSuggests as $woitem_kd => $elements) {
            $qty = 0;
            foreach ($elements as $el) {
                if (is_null($el['dn_tujuan'])) {
                    $qty = $el['woitem_qty'];
                } else {
                    if ($el['dn_tujuan'] == $qMaster['bagian_kd']) {
                        $qty = $qty + $el['dndetailreceived_qty'];
                    } else {
                        $qty = $qty + $el['dndetailreceived_qty'];
                    }
                }
            }

            $i += 1;
            
            /** woitem Sudah masuk planningdetail karena sudah dipkai maka tdk dipakai hitung lagi */
            foreach ($planningDetails as $planningDetail) {
                if ($planningDetail['woitem_kd'] == $woitem_kd) {
                    $qty = $qty - $planningDetail['sum_planningweeklydetail_qty'];
                }
            }

            /** cek apakah part atau packing, jika packing memiliki part maka tidak akan ditampilkan */
            $woitem_detail = $this->db->where('woitem_kd', $el['woitem_kd'])
                ->where('woitemdetail_parent is not null')
                ->where("woitemdetail_parent != ''")
                ->get('td_workorder_item_detail')->num_rows();

            if ($woitem_detail == 0) {
                /** cek parent / ambil woitemdetail_kd untuk scan parent_kd di next logic*/
                $woitem_cek = $this->db->where('woitem_kd', $el['woitem_kd'])
                    ->where("(woitemdetail_parent is null or woitemdetail_parent = '')")
                    ->get('td_workorder_item_detail')->first_row();

                /** cek child where parent_kd = woitemdetail_kd */
                $woitem_n = $this->db->where('woitemdetail_parent', $woitem_cek->woitemdetail_kd)
                    ->where("woitem_kd is not null")   //punya woitem_kd
                    ->where("woitem_kd != ''")   //punya woitem_kd
                    ->get('td_workorder_item_detail')->num_rows();

                /** untuk mengakali agar ditampilkan */
                if ($woitem_n == 0) {
                    $woitem_detail = 1; //packing berdiri sendiri
                } else {
                    $woitem_detail = 0; //packing tidak ditampilkan
                }
            }

            /** cek jenis proses di td_workorder_item_so, jika jasa maka tdk ditampilkan */
            $jenis_proses = $this->db->where('woitemso_itemcode', $el['woitem_itemcode'])
            ->where("wo_kd",$el['wo_kd'])   //punya wo_kd
            ->where("woitemso_jenis_proses","jasa")   //jenis jasa
            ->get('td_workorder_item_so')->num_rows();

            /** Filter tampil data, qty diatas 0, divisi packing wo jenis non part,  */
            if ((!empty($el['woitem_no_wo']) and $qty > 0 and $qMaster['bagian_kd'] !== "60" and $woitem_detail > 0 and $jenis_proses == 0) or (!empty($el['woitem_no_wo']) and $qty > 0 and $el['woitem_jenis'] !== 'part' and $qMaster['bagian_kd'] == "60" and $qMaster['divisi_kd'] == "60" and $jenis_proses == 0)) {
                $woSuggestResults[] = [
                    'woitem_kd' => $woitem_kd,
                    'wo_kd' => $el['wo_kd'],
                    'woitem_itemcode' => $el['woitem_itemcode'],
                    'woitem_no_wo' => $el['woitem_no_wo'],
                    'woitem_deskripsi' => $el['woitem_deskripsi'],
                    'woitem_dimensi' => $el['woitem_dimensi'],
                    'woitem_jenis' => $el['woitem_jenis'],
                    'woitem_qty' => $qty,
                ];
            }
        }

        $data['results'] = $woSuggestResults;

        $this->load->view('page/' . $this->class_link . '/form_tablesuggestwo', $data);
    }

    public function form_tableallwo()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $qMaster = $this->tm_planningweekly->get_by_param(['planningweekly_kd' => $data['id']])->row_array();

        $woitems = $this->td_workorder_item->get_by_param(['woitem_prosesstatus' => 'workorder'])->result_array();
        $woitem_kds = [];
        foreach ($woitems as $woitem) {
            if (!in_array($woitem['woitem_kd'], $woitem_kds)) {
                $woitem_kds[] = $woitem['woitem_kd'];
            }
        }
        /** Search yang sudah input di planning weekly detail */
        $planningDetails = $this->td_planningweekly_detail->getItemInsertedGroupByWoitem($qMaster['bagian_kd'], $woitem_kds);
        $results = [];
        foreach ($woitems as $woitem2) {
            $qty = $woitem2['woitem_qty'];
            foreach ($planningDetails as $planningDetail) {
                if ($planningDetail['woitem_kd'] == $woitem2['woitem_kd']) {
                    $qty = $qty - $planningDetail['sum_planningweeklydetail_qty'];
                }
            }

            if (!empty($woitem2['woitem_no_wo'])) {
                $results[] = [
                    'woitem_kd' => $woitem2['woitem_kd'],
                    'wo_kd' => $woitem2['wo_kd'],
                    'woitem_itemcode' => $woitem2['woitem_itemcode'],
                    'woitem_no_wo' => $woitem2['woitem_no_wo'],
                    'woitem_deskripsi' => $woitem2['woitem_deskripsi'],
                    'woitem_dimensi' => $woitem2['woitem_dimensi'],
                    'woitem_jenis' => $woitem2['woitem_jenis'],
                    'woitem_qty' => $qty,
                ];
            }
        }

        $data['class_link'] = $this->class_link;
        $data['results'] = $results;
        $this->load->view('page/' . $this->class_link . '/form_tableallwo', $data);
    }

    public function form_master_box()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_master_box', $data);
    }

    public function form_master_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        if ($data['slug'] == 'edit') {
            $data['rowData'] = $this->tm_planningweekly->get_by_param(['planningweekly_kd' => $data['id']])->row_array();
        }

        $bagians = $this->tb_bagian->get_by_param(['bagian_lokasi' => 'WIP'])->result_array();
        $opsiBagian[null] = '-- Pilih Opsi --';
        foreach ($bagians as $bagian) {
            $opsiBagian[$bagian['bagian_kd']] = $bagian['bagian_nama'];
        }
        $data['opsiBagian'] = $opsiBagian;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_master_main', $data);
    }

    public function view_tablemain()
    {
        // if (!$this->input->is_ajax_request()) {
        //     exit('No direct script access allowed');
        // }
        $planningweekly_kd = $this->input->get('id', true);
        $data['results'] = $this->td_planningweekly_detail->getDetailPlanningWoitem($planningweekly_kd);
        $planningweeklydetail_kds = array_column($data['results'], 'planningweeklydetail_kd');
        $data['materialreqdetails'] = $this->td_materialrequisition_detail->get_where_in_detail('planningweeklydetail_kd', $planningweeklydetail_kds)->result_array();

        $this->load->view('page/' . $this->class_link . '/view_tablemain', $data);
    }

    public function pdf_tablemain()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $planningweekly_kd = $this->input->get('id', true);
        $data['results'] = $this->td_planningweekly_detail->getDetailPlanningWoitem($planningweekly_kd);

        $this->load->view('page/' . $this->class_link . '/pdf_tablemain', $data);
    }

    public function pdf_main()
    {
        $this->load->library('Pdf');
        $planningweekly_kd = $this->input->get('id', true);

        $dataDetail['results'] = $this->td_planningweekly_detail->getDetailPlanningWoitem($planningweekly_kd);

        $data['master'] = $this->tm_planningweekly->getRowPlanningWeeklyBagianState($planningweekly_kd);
        $data['konten'] = $this->load->view('page/' . $this->class_link . '/pdf_tablemain', $dataDetail, true);
        $data['logs'] = $this->td_planningweekly_log->get_log_printout($planningweekly_kd)->result_array();

        $this->load->view('page/' . $this->class_link . '/pdf_main', $data);
    }

    public function view_box()
    {
        parent::administrator();
        parent::pnotify_assets();
        $id = $this->input->get('id', true);

        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        $data['header'] = $this->tm_planningweekly->getRowPlanningWeeklyBagianState($id);
        $data['generate_button'] = $this->tm_planningweekly->generate_button($id, $this->wf_kd, $data['header']['wfstate_kd']);

        $this->load->view('page/' . $this->class_link . '/view_box', $data);
    }

    public function view_log()
    {
        $id = $this->input->get('id', true);

        $data['result'] = $this->db->select('td_planningweekly_log.*, td_workflow_transition.*, tb_admin.nm_admin, state_source.wfstate_nama as state_source, state_dst.wfstate_nama as state_dst')
            ->from('td_planningweekly_log')
            ->join('td_workflow_transition', 'td_planningweekly_log.wftransition_kd=td_workflow_transition.wftransition_kd', 'left')
            ->join('td_workflow_state as state_source', 'state_source.wfstate_kd=td_workflow_transition.wftransition_source', 'left')
            ->join('td_workflow_state as state_dst', 'state_dst.wfstate_kd=td_workflow_transition.wftransition_destination', 'left')
            ->join('tb_admin', 'td_planningweekly_log.admin_kd=tb_admin.kd_admin', 'left')
            ->where('td_planningweekly_log.planningweekly_kd', $id)
            ->order_by('td_planningweekly_log.planningweeklylog_tglinput', 'desc')
            ->get()->result_array();

        $this->load->view('page/' . $this->class_link . '/view_log', $data);
    }

    public function formubahstate_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $id = $this->input->get('id');
        $wftransition_kd = $this->input->get('wftransition_kd');

        $data['id'] = $id;
        $data['wftransition_kd'] = $wftransition_kd;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/formubahstate_main', $data);
    }

    private function action_special($action = null, $aParam = [])
    {
        $act = true;
        $admin_kd = $this->session->userdata('kd_admin');
        switch ($action) {
            case 'PLANNINGWEEKLY_CLOSED_0':
                $act = $this->tm_planningweekly->update_data(['planningweekly_kd' => $aParam['planningweekly_kd']], ['planningweekly_closed' => 0, 'planningweekly_tgledit' => date('Y-m-d H:i:s'), 'admin_kd' => $admin_kd]);
                break;
            case 'PLANNINGWEEKLY_CLOSED_1':
                $act = $this->tm_planningweekly->update_data(['planningweekly_kd' => $aParam['planningweekly_kd']], ['planningweekly_closed' => 1, 'planningweekly_tgledit' => date('Y-m-d H:i:s'), 'admin_kd' => $admin_kd]);
                break;
            default:
                $act = true;
        }

        return $act;
    }

    public function action_change_state()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtid', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtwftransition_kd', 'Transition', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrrm_kd' => (!empty(form_error('txtid'))) ? buildLabel('warning', form_error('txtid', '"', '"')) : '',
                'idErrrm_kd' => (!empty(form_error('txtwftransition_kd'))) ? buildLabel('warning', form_error('txtwftransition_kd', '"', '"')) : '',
            );
        } else {
            $planningweekly_kd = $this->input->post('txtid', true);
            $wftransition_kd = $this->input->post('txtwftransition_kd', true);
            $planningweeklylog_note = $this->input->post('txtplanningweeklylog_note', true);

            $data = [
                'planningweeklylog_kd' => $this->td_planningweekly_log->create_code(),
                'planningweekly_kd' => $planningweekly_kd,
                'wftransition_kd' => $wftransition_kd,
                'admin_kd' => $this->session->userdata('kd_admin'),
                'planningweeklylog_note' => $planningweeklylog_note,
                'planningweeklylog_tglinput' => date('Y-m-d H:i:s'),
            ];
            $this->db->trans_begin();
            $planningweekly = $this->tm_planningweekly->get_by_param(['planningweekly_kd' => $planningweekly_kd])->row_array();
            $wftransition = $this->td_workflow_transition->get_by_param(['wftransition_kd' => $wftransition_kd])->row_array();
            $dataUpdate = [
                'wfstate_kd' => $wftransition['wftransition_destination'],
                'planningweekly_tgledit' => date('Y-m-d H:i:s'),
            ];
            #email message notification
            //$subject = 'Planning Weekly - '.$planningweekly['planningweekly_no'];
            $dtMessage['subjcet'] = $planningweekly['planningweekly_no'];
			$dtMessage['text'] = 'Terdapat Planning Weekly yang perlu approval Anda :';
			$dtMessage['url'] = 'https://'.$_SERVER['HTTP_HOST'].base_url().$this->class_link.'/view_box?id='.$planningweekly_kd;
			$dtMessage['urltext'] = 'Detail Planning Weekly';
			$dtMessage['keterangan'] = !empty($planningweeklylog_note) ? $planningweeklylog_note : null;
			$message = $this->load->view('templates/email/email_notification', $dtMessage, true);

            #special action
            if (!empty($wftransition['wftransition_action'])) {
                $expAction = explode(';', $wftransition['wftransition_action']);
                for ($i = 0; $i < count($expAction); $i++) {
                    $actSpecial = $this->action_special($expAction[$i], $data);
                }
            }

            $act = $this->td_planningweekly_log->insert_data($data);
            $actUpdate = $this->tm_planningweekly->update_data(['planningweekly_kd' => $planningweekly_kd], $dataUpdate);
            //$actNotif = $this->td_workflow_transition_notification->generate_notification($wftransition_kd, $subject, $message);

            $SttsNotif =  $this->td_workflow_transition_notification->getStatus_generate_notification($wftransition_kd);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            } else {
                $this->db->trans_commit();
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'stts' =>  $SttsNotif, 'msg' => $dtMessage);
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_insert_master()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtbagian_kd', 'Bagian', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtplanningweekly_start', 'Start', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtplanningweekly_end', 'End', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrbagian_kd' => (!empty(form_error('txtbagian_kd'))) ? buildLabel('warning', form_error('txtbagian_kd', '"', '"')) : '',
                'idErrplanningweekly_start' => (!empty(form_error('txtplanningweekly_start'))) ? buildLabel('warning', form_error('txtplanningweekly_start', '"', '"')) : '',
                'idErrplanningweekly_end' => (!empty(form_error('txtplanningweekly_end'))) ? buildLabel('warning', form_error('txtplanningweekly_end', '"', '"')) : '',
            );
        } else {
            $slug = $this->input->post('slug', true);
            $id = $this->input->post('txtplanningweekly_kd', true);
            $divisi_kd = $this->input->post('txtdivisi_kd', true);
            $bagian_kd = $this->input->post('txtbagian_kd', true);
            $planningweekly_start = $this->input->post('txtplanningweekly_start', true);
            $planningweekly_end = $this->input->post('txtplanningweekly_end', true);

            $data = [
                'divisi_kd' => $divisi_kd,
                'bagian_kd' => $bagian_kd,
                'planningweekly_start' => $planningweekly_start,
                'planningweekly_end' => $planningweekly_end,
                'wf_kd' => $this->wf_kd,
                'admin_kd' => $this->session->userdata('kd_admin'),
            ];
            if (empty($id)) {
                /** Add */
                $planningweekly_kd = $this->tm_planningweekly->create_code();
                $data = array_merge($data, [
                    'planningweekly_kd' => $planningweekly_kd,
                    'planningweekly_no' => $this->tm_planningweekly->generate_no(),
                    'planningweekly_closed' => 1,
                    'wfstate_kd' => $this->td_workflow_state->getInitialState($this->wf_kd),
                    'planningweekly_originator' => $this->session->userdata('kd_admin'),
                    'planningweekly_tglinput' => date('Y-m-d H:i:s'),
                    'planningweekly_tgledit' => date('Y-m-d H:i:s')
                ]);
                try {
                    $this->tm_planningweekly->insert_data($data);
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['id' => $planningweekly_kd]);
                } catch (Exception $e) {
                    $resp = array('code' => 400, 'status' => 'Gagal;', 'pesan' => $e->getMessage());
                }
            } else {
                /** Edit */
                $data = array_merge($data, [
                    'planningweekly_tgledit' => date('Y-m-d H:i:s')
                ]);
                try {
                    $this->tm_planningweekly->update_data(['planningweekly_kd' => $id], $data);
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terupdate', 'data' => ['id' => $id]);
                } catch (Exception $e) {
                    $resp = array('code' => 400, 'status' => 'Gagal;', 'pesan' => $e->getMessage());
                }
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_insert_detail_batch()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtplanningweekly_kd', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrbagian_kd' => (!empty(form_error('txtplanningweekly_kd'))) ? buildLabel('warning', form_error('txtplanningweekly_kd', '"', '"')) : '',
            );
        } else {
            $planningweekly_kd = $this->input->post('txtplanningweekly_kd', true);
            $woitem_kds = $this->input->post('txtwoitem_kds', true);
            $planningweeklydetail_qty = $this->input->post('txtplanningweeklydetail_qty', true);
            $planningweeklydetail_notes = $this->input->post('txtplanningweeklydetail_notes', true);

            $planningweeklydetail_kd = $this->td_planningweekly_detail->create_code();
            for ($i = 0; $i < count($woitem_kds); $i++) {
                $data[] = [
                    'planningweeklydetail_kd' => $planningweeklydetail_kd,
                    'planningweekly_kd' => $planningweekly_kd,
                    'woitem_kd' => $woitem_kds[$i],
                    'planningweeklydetail_qty' => $planningweeklydetail_qty[$woitem_kds[$i]],
                    'planningweeklydetail_note' => !empty($planningweeklydetail_notes[$woitem_kds[$i]]) ? $planningweeklydetail_notes[$woitem_kds[$i]] : null,
                    'planningweeklydetail_tglinput' => date('Y-m-d H:i:s'),
                    'planningweeklydetail_tgledit' => date('Y-m-d H:i:s'),
                    'admin_kd' => $this->session->userdata('kd_admin')
                ];
                $planningweeklydetail_kd++;
            }
            try {
                $this->td_planningweekly_detail->insert_batch($data);
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            } catch (Exception $e) {
                $resp = array('code' => 400, 'status' => 'Gagal;', 'pesan' => $e->getMessage());
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    function action_delete_detail()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $planningweeklydetail_kd = $this->input->get('id');
        if (!empty($planningweeklydetail_kd)) {
            try {
                $this->td_planningweekly_detail->delete_data($planningweeklydetail_kd);
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
            } catch (Exception $e) {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal');
            }
        } else {
            $resp = array('code' => 400, 'pesan' => 'Id tidak ditemukan');
        }
        echo json_encode($resp);
    }

    function action_delete_master()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $planningweekly_kd = $this->input->get('id');
        if (!empty($planningweekly_kd)) {
            try {
                $actDetail = $this->td_planningweekly_detail->delete_by_param(['planningweekly_kd' => $planningweekly_kd]);
                if ($actDetail) {
                    $this->tm_planningweekly->delete_data($planningweekly_kd);
                }
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
            } catch (Exception $e) {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal');
            }
        } else {
            $resp = array('code' => 400, 'pesan' => 'Id tidak ditemukan');
        }
        echo json_encode($resp);
    }
}
