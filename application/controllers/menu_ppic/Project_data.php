<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Project_data extends MY_Controller
{
    private $class_link = 'menu_ppic/project_data';

    public function __construct(){
        parent::__construct();

        $this->load->library(array('ssp', 'form_validation'));
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
        $this->load->model(array('tm_project'));
        }

    public function index(){
        parent::administrator();
        parent::select2_assets();
        parent::pnotify_assets();
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function table_main () {
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }

    public function table_data () {
        if (!$this->input->is_ajax_request()) {
            //exit('No direct script access allowed');
        }
        $data = $this->tm_project->ssp_table();
        echo json_encode(
            SSP::simple($_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'])
        );

        //echo json_encode($data);
    }

    public function form_box () {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $id = $this->input->get('id', true);
        $slug = $this->input->get('slug', true);

        if ($slug == 'edit') {
            $data['row'] = $this->db->select('tm_project.*, tm_salesorder.no_salesorder, tm_salesorder.no_po, tm_salesorder.tipe_customer, tm_customer.nm_customer,
                        td_salesorder_item.item_code as parent_itemcode, td_salesorder_item.item_desc as parent_itemdesc, td_salesorder_item.item_dimension as parent_itemdimension,
                        td_salesorder_item_detail.item_code as child_itemcode, td_salesorder_item_detail.item_desc as child_itemdesc, td_salesorder_item_detail.item_dimension as child_itemdimension')
                    ->from('tm_project')
                    ->join('tm_salesorder', 'tm_project.kd_msalesorder=tm_salesorder.kd_msalesorder', 'left')
                    ->join('tm_customer', 'tm_salesorder.customer_kd=tm_customer.kd_customer', 'left')
                    ->join('td_salesorder_item', 'tm_project.kd_ditem_so=td_salesorder_item.kd_ditem_so', 'left')
                    ->join('td_salesorder_item_detail', 'tm_project.kd_citem_so=td_salesorder_item_detail.kd_citem_so', 'left')
                    ->where('tm_project.project_kd', $id)
                    ->get()->row_array();
        }

        $data['id'] = $id;
        $data['slug'] = $slug;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_box', $data);
    }

    public function findso_form () {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $id = $this->input->get('id', true);
        $slug = $this->input->get('slug', true);

        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/findso_form', $data);
    }
    
    public function findso_table () {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $id = $this->input->get('id', true);

        $resultParent = $this->db->select('td_salesorder_item.*, tm_salesorder.no_salesorder, tm_salesorder.no_po, tm_salesorder.tipe_customer,
                        tm_customer.nm_customer')
                    ->from('td_salesorder_item')
                    ->join('tm_salesorder', 'td_salesorder_item.msalesorder_kd=tm_salesorder.kd_msalesorder', 'left')
                    ->join('tm_customer', 'tm_salesorder.customer_kd=tm_customer.kd_customer', 'left')
                    ->where('td_salesorder_item.msalesorder_kd', $id)
                    ->where('td_salesorder_item.item_status', 'custom')
                    ->get()->result();
        $resultChild = $this->db->select('td_salesorder_item_detail.*, tm_salesorder.no_salesorder, tm_salesorder.no_po, tm_salesorder.tipe_customer,
                        tm_customer.nm_customer')
                    ->from('td_salesorder_item_detail')
                    ->join('tm_salesorder', 'td_salesorder_item_detail.msalesorder_kd=tm_salesorder.kd_msalesorder', 'left')
                    ->join('tm_customer', 'tm_salesorder.customer_kd=tm_customer.kd_customer', 'left')
                    ->where('td_salesorder_item_detail.msalesorder_kd', $id)
                    ->where('td_salesorder_item_detail.item_status', 'custom')
                    ->get()->result();

        $result = [];
        foreach ($resultParent as $rParent) {
            $result[] = [
                'kd_msalesorder' => $rParent->msalesorder_kd,
                'no_salesorder' => $rParent->no_salesorder,
                'no_po' => $rParent->no_po,
                'tipe_customer' => $rParent->tipe_customer,
                'nm_customer' => $rParent->nm_customer,
                'kd_ditem_so' => $rParent->kd_ditem_so,
                'kd_citem_so' => null,
                'item_code' => $rParent->item_code,
                'item_desc' => $rParent->item_desc,
                'item_dimension' => $rParent->item_dimension,
            ];
        }
        foreach ($resultChild as $rChild) {
            $result[] = [
                'kd_msalesorder' => $rChild->msalesorder_kd,
                'no_salesorder' => $rChild->no_salesorder,
                'no_po' => $rChild->no_po,
                'tipe_customer' => $rChild->tipe_customer,
                'nm_customer' => $rChild->nm_customer,
                'kd_ditem_so' => null,
                'kd_citem_so' => $rChild->kd_citem_so,
                'item_code' => $rChild->item_code,
                'item_desc' => $rChild->item_desc,
                'item_dimension' => $rChild->item_dimension,
            ];
        }

        $data['result'] = $result;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/findso_table', $data);
    }

    public function action_insert() {
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        
        $this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtproject_itemcode', 'ItemCode', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) :
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
                'iderrproject_itemcode' => (!empty(form_error('txtproject_itemcode')))?buildLabel('warning', form_error('txtproject_itemcode', '"', '"')):'',
			);
			
        else :
            $slug = $this->input->post('txtslug', true);
            $project_kd = $this->input->post('txtproject_kd', true);
            $project_nopj = $this->input->post('txtproject_nopj', true);
            $kd_ditem_so = $this->input->post('txtkd_ditem_so', true);
            $kd_citem_so = $this->input->post('txtkd_citem_so', true);
            $kd_msalesorder = $this->input->post('txtkd_msalesorder', true);
            $project_itemcode = $this->input->post('txtproject_itemcode', true);
            $project_itemdesc = $this->input->post('txtproject_itemdesc', true);
            $project_itemdimension = $this->input->post('txtproject_itemdimension', true);
            
            if (empty($project_kd)) {
                $project_nopj = $this->tm_project->generate_nopj('', '');
            }
            if (!empty($kd_citem_so)) {
                $kd_ditem_so = null;
            }

            $data = [
                'kd_msalesorder' => !empty($kd_msalesorder) ? $kd_msalesorder : null,
                'kd_ditem_so' => !empty($kd_ditem_so) ? $kd_ditem_so : null,
                'kd_citem_so' => !empty($kd_citem_so) ? $kd_citem_so : null,
                'project_nopj' => $project_nopj,
                'project_itemcode' => $project_itemcode,
                'project_itemdesc' => !empty($project_itemdesc) ? $project_itemdesc : null,
                'project_itemdimension' => !empty($project_itemdimension) ? $project_itemdimension : null,
                'project_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            ];
            $act = false;
            if (empty($project_kd)) {
                $project_kd = $this->tm_project->create_code('', '');
                $data = array_merge($data, ['project_kd' => $project_kd, 'project_status' => 'approved', 'project_tglinput' => date('Y-m-d H:i:s')]);
                $act = $this->tm_project->insert_data($data);
            }else {
                $act = $this->tm_project->update_data (['project_kd' => $project_kd], $data);
            }

            if ($act) :
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            else:
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            endif; 

        endif;

        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    function action_delete () {
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
        $project_kd = $this->input->get('id');
        if (!empty($project_kd)){
            $act = $this->tm_project->delete_data($project_kd);
            if ($act) {
                $resp = array('code' => 200, 'pesan' => 'Sukses');
            }
            else{
                $resp = array('code' => 400, 'pesan' => 'Gagal Delete');
            }
        }
        else{
            $resp = array('code' => 400, 'pesan' => 'Id Tidak Ditemukan');
        }
        echo json_encode($resp);
    }

    public function action_update_status() {
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
        $project_kd = $this->input->get('id');
        $project_status = $this->input->get('status');

        if (!empty($project_kd)){
            $project_nopj = null;
            if ($project_status == 'approved') {
                $master_project = $this->tm_project->get_by_param(['project_kd' => $project_kd])->row_array();
                if( $master_project['project_nopj'] != NULL ){
                    $project_nopj = $master_project['project_nopj'];
                }else{
                    $project_nopj = $this->tm_project->generate_nopj ('', '');
                }
            }
            $data = [
                'project_nopj' => $project_nopj, 
                'project_status' => $project_status, 
                'project_tgledit' => date('Y-m-d H:i:s'), 
                'admin_kd' => $this->session->userdata('kd_admin')
            ];
            $act = $this->tm_project->update_data (['project_kd' => $project_kd], $data);
            if ($act) {
                $resp = array('code' => 200, 'pesan' => 'Sukses');
            }
            else{
                $resp = array('code' => 400, 'pesan' => 'Gagal Delete');
            }
        }
        else{
            $resp = array('code' => 400, 'pesan' => 'Id Tidak Ditemukan');
        }
        echo json_encode($resp);
    }

}
