<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Wo_position extends MY_Controller
{
    private $class_link = 'menu_ppic/work_orders/wo_position';

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('ssp', 'form_validation'));
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
        $this->load->model(array('tm_wip', 'tb_bagian'));
    }

    public function index()
    {
        parent::administrator();
        parent::pnotify_assets();

        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function table_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        /** Bagian */
        // $data['bagians'] = $this->db->order_by('bagian_kd')->get('tb_bagian')->result_array();
        $data['bagians'] = $this->tb_bagian->get_by_param_in ('bagian_lokasi', ['WIP', 'WH'])->result_array();

        /** WIP */
        $data['wips'] = $this->db->join('tm_barang', 'tm_barang.kd_barang=tm_wip.kd_barang', 'left')
            ->where('tm_wip.wip_qty >', 0)
            ->where('tm_wip.kd_barang !=', 'PRD020817000573')
            ->order_by('tm_barang.item_code')
            ->get('tm_wip')->result_array();

        // WO complete
        $data['woCompletes'] = $this->db->join('td_workorder_item_detail', 'td_workorder_item_detail.woitem_kd=td_workorder_item.woitem_kd', 'left')
            ->where('td_workorder_item.woitem_prosesstatus', 'workorder')
            ->where('td_workorder_item.woitem_jenis', 'packing')
            ->get('td_workorder_item')->result_array();

        /** Detail bom untuk mencari per part */
        $barang_kds = array_column($data['wips'], 'kd_barang');
        $detailParts = $this->db->join('td_bom_detail', 'td_bom_detail.bom_kd=tm_bom.bom_kd', 'left')
            ->join('td_part', 'td_part.partmain_kd=td_bom_detail.partmain_kd', 'left')
            ->where_in('tm_bom.kd_barang', $barang_kds)
            ->get('tm_bom')->result_array();
        $arrDetailParts = [];
        foreach ($detailParts as $detailPart) {
            $arrDetailParts[$detailPart['kd_barang']][] = $detailPart['part_kd'];
        }
        $data['detailPartBarangs'] = $arrDetailParts;

        /* WO parts */
        $data['woParts'] = $this->db->join('td_workorder_item_detail', 'td_workorder_item_detail.woitem_kd=td_workorder_item.woitem_kd', 'left')
            ->where('td_workorder_item.woitem_prosesstatus', 'workorder')
            ->where('td_workorder_item.woitem_jenis', 'part')
            ->order_by('td_workorder_item.woitem_no_wo', 'asc')
            ->get('td_workorder_item')->result_array();

        $completeWoitem_kds = array_values(array_unique(array_column($data['woCompletes'], 'woitem_kd')));
        $partWoitem_kds = array_values(array_unique(array_column($data['woParts'], 'woitem_kd')));
        $woitem_kds = array_merge($completeWoitem_kds, $partWoitem_kds);

        /** Mapping location work order */
        $qDnReceived = $this->db->select('td_deliverynote_received.*, bagian_asal.bagian_nama as bagian_asal, bagian_tujuan.bagian_nama as bagian_tujuan, td_workorder_item.*, tm_deliverynote.dn_asal, tm_deliverynote.dn_tujuan')
            ->from('td_deliverynote_received')
            ->join('tm_deliverynote', 'td_deliverynote_received.dn_kd=tm_deliverynote.dn_kd', 'left')
            ->join('td_workorder_item', 'td_deliverynote_received.woitem_kd=td_workorder_item.woitem_kd', 'left')
            ->join('tb_bagian as bagian_asal', 'tm_deliverynote.dn_asal=bagian_asal.bagian_kd', 'left')
            ->join('tb_bagian as bagian_tujuan', 'tm_deliverynote.dn_tujuan=bagian_tujuan.bagian_kd', 'left')
            ->where_in('td_deliverynote_received.woitem_kd', $woitem_kds)
            ->order_by('td_deliverynote_received.dndetailreceived_tglinput', 'asc')
            ->get()->result_array();
        $arrRcv = [];
        foreach ($qDnReceived as $dnRcv) {
            $arrRcv[$dnRcv['woitem_kd']][] = [
                'woitem_qty' => $dnRcv['woitem_qty'],
                'dn_asal' => $dnRcv['dn_asal'],
                'dn_tujuan' => $dnRcv['dn_tujuan'],
                'dndetailreceived_qty' => $dnRcv['dndetailreceived_qty'],
            ];
        }
        $arrBagianCalc = [];
        foreach ($arrRcv as $woitem_kd => $elements) {
            $dtElements = [];
            // declare variable and reset to 0
            foreach ($data['bagians'] as $bagian1) {
                ${"s{$bagian1['bagian_kd']}"} = 0;
            }
            // set state asal pertamakali
            ${"s{$arrRcv[$woitem_kd][0]['dn_asal']}"} = $arrRcv[$woitem_kd][0]['woitem_qty'];
            foreach ($elements as $element) {
                isset(${"s{$element['dn_asal']}"}) ? ${"s{$element['dn_asal']}"} : 0;
                isset(${"s{$element['dn_tujuan']}"}) ? ${"s{$element['dn_tujuan']}"} : 0;

                ${"s{$element['dn_asal']}"} = ${"s{$element['dn_asal']}"} - $element['dndetailreceived_qty'];
                ${"s{$element['dn_tujuan']}"} = ${"s{$element['dn_tujuan']}"} + $element['dndetailreceived_qty'];
            }
            foreach ($data['bagians'] as $bagian2) {
                $dtElements[] = [
                    'bagian_kd' => $bagian2['bagian_kd'],
                    'qty' => isset(${"s{$bagian2['bagian_kd']}"}) ? ${"s{$bagian2['bagian_kd']}"} : 0
                ];
            }
            
            $arrBagianCalc[$woitem_kd] = $dtElements;
        }
        $data['woitemMappings'] = $arrBagianCalc;

        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }
}
