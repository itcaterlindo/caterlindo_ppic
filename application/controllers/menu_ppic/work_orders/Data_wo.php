<?php

use PhpOffice\PhpSpreadsheet\Writer\Ods\Thumbnails;

defined('BASEPATH') or exit('No direct script access allowed!');

class Data_wo extends MY_Controller
{
    private $class_link = 'menu_ppic/work_orders/data_wo';

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('ssp', 'form_validation'));
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
        $this->load->model(array(
            'm_builder', 'tm_salesorder', 'tm_workorder', 'td_workorder_item', 'td_salesorder_item',
            'td_salesorder_item_detail', 'tm_quotation', 'tb_relasi_sowo', 'td_product_grouping', 'tm_bom', 'td_workorder_item_detail',
            'td_bom_detail', 'tm_project', 'td_workorder_item_so',  'td_part_jenis',
            'td_workorder_item_pyr', 'td_workorder_item_so_detail', 'tm_barang', 'tm_inspection_rawmaterial'
        ));
    }

    public function index()
    {
        parent::administrator();
        parent::datetimepicker_assets();
        parent::select2_assets();
        parent::typeahead_assets();
        parent::pnotify_assets();
        parent::icheck_assets();
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function open_table()
    {
        $data['class_link'] = $this->class_link;
        $data['wos'] = $this->db->select('tm_workorder.*, 
                GROUP_CONCAT( IF (tm_salesorder.tipe_customer = "Lokal", tm_salesorder.no_salesorder, tm_salesorder.no_po) SEPARATOR ", ") AS no_salesorders')
            ->from('tm_workorder')
            ->join('tb_relasi_sowo', 'tb_relasi_sowo.wo_kd=tm_workorder.wo_kd', 'left')
            ->join('tm_salesorder', 'tm_salesorder.kd_msalesorder=tb_relasi_sowo.kd_msalesorder', 'left')
            ->group_by('tm_workorder.wo_kd')
            ->order_by('tm_workorder.wo_tgledit', 'desc')
            ->get()
            ->result_array();
        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }

    public function open_form_box()
    {
        if (!$this->input->is_ajax_request()){
        	exit('No direct script access allowed');
        }

        $slug = $this->input->get('slug');
        $id = $this->input->get('id');

        $url = '';
        switch ($slug) {
            case "form_add_master":
                $url = base_url() . $this->class_link . '/open_form_master';
                break;
            case "form_item_main":
                $url = base_url() . $this->class_link . '/form_item_main?wo_kd=' . $id;
                break;
            case "form_add_pyr":
                $url = base_url() . $this->class_link . '/form_add_pyr';
                break;
            default:
                echo "Jenis form tidak diketahui!";
        }

        $data['class_link'] = $this->class_link;
        $data['slug'] = $slug;
        $data['id'] = $id;
        $data['url'] = $url;
        $this->load->view('page/' . $this->class_link . '/form_box', $data);
    }

    public function open_form_master()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        // Get status statusnya sudah production order
        $select = 'a.kd_msalesorder, a.no_salesorder, b.nm_customer, a.tipe_customer, a.no_po';
        $where = 'a.status_so = "process_lpo"';
        $order_by = 'a.no_salesorder';
        $group_by = '';
        $join = array(
            array(
                'joinTable' => 'tm_customer as b',
                'joinOn' => 'a.customer_kd=b.kd_customer',
                'joinStatus' => 'left',
            ),
        );
        $msalesorders = $this->tm_salesorder->getResult($select, $where, $order_by, $group_by, $join);
        /** Opsi Msalesorder */
        foreach ($msalesorders as $eachMsalesorder) :
            $no_salesorder = $eachMsalesorder->no_salesorder;
            if ($eachMsalesorder->tipe_customer == 'Ekspor') :
                $no_salesorder = $eachMsalesorder->no_po;
            endif;
            $opsiMsalesorder[$eachMsalesorder->kd_msalesorder] = $no_salesorder . ' | ' . $eachMsalesorder->nm_customer;
        endforeach;

        $data['opsiMsalesorders'] = $opsiMsalesorder;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_master', $data);
    }

    public function form_add_pyr()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $slug = $this->input->get('slug');
        $wo_kd = $this->input->get('wo_kd');
        /** Dropdown Satuan */
        $opsiSatuan = [
            'unit' => 'Unit',
            'pcs' => 'Pcs',
        ];
        $data['class_link'] = $this->class_link;
        $data['wo_kd'] = $wo_kd;
        $data['slug'] = $slug;
        $data['opsiSatuan'] = $opsiSatuan;
        $this->load->view('page/' . $this->class_link . '/form_main_add_pyr', $data);

    }

    public function table_data_pyr()
    {
        try{
			$data = $this->td_workorder_item_pyr->ssp_table2();
			echo json_encode($data);
		}catch(\Throwable $e){
			echo $e->getMessage();
		}
    }

    public function form_item_step1_box()
    {
        parent::administrator();
        parent::datetimepicker_assets();
        parent::select2_assets();
        parent::typeahead_assets();
        parent::pnotify_assets();
        parent::icheck_assets();

        $wo_kd = $this->input->get('wo_kd', true);

        $data['wo_kd'] = $wo_kd;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_item_step1_box', $data);
    }

    private function generateheaderData($wo_kd)
    {
        $wo = $this->tm_workorder->get_by_param(['wo_kd' => $wo_kd])->row_array();
        $salesorders = $this->tb_relasi_sowo->get_by_param_detail(['tb_relasi_sowo.wo_kd' => $wo_kd]);

        $arrNoSO = [];
        $no_salesorders = '';
        if ($salesorders->num_rows == 1) :
            $salesorders = $salesorders->row();
            $no_salesorders = $salesorders->no_salesorder;
            if ($salesorders->tipe_customer == 'Ekspor') :
                $no_salesorders = $salesorders->no_po;
            endif;
        else :
            $salesorders = $salesorders->result();
            foreach ($salesorders as $each) :
                $no_so = $each->no_salesorder;
                if ($each->tipe_customer == 'Ekspor') :
                    $no_so = $each->no_po;
                endif;
                $arrNoSO[] = $no_so;
            endforeach;
            $no_salesorders = implode(', ', $arrNoSO);
        endif;

        $res['wo'] = $wo;
        $res['no_salesorders'] = $no_salesorders;

        return $res;
    }

    public function form_item_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $wo_kd = $this->input->get('wo_kd', true);

        $data['wo_kd'] = $wo_kd;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_item_main', $data);
    }

    public function form_item()
    {
        $slug = $this->input->get('slug');
        $wo_kd = $this->input->get('wo_kd');
        $woitemso_kd = $this->input->get('woitemso_kd');

        if ($slug == 'edit') {
            $qWOitem = $this->td_workorder_item_so->get_by_param(['woitemso_kd' => $woitemso_kd])->row_array();
            $data['rowData'] = $qWOitem;
        }

        $data['class_link'] = $this->class_link;
        $data['slug'] = $slug;
        $data['wo_kd'] = $wo_kd;

        $this->load->view('page/' . $this->class_link . '/form_item', $data);
    }

    public function form_item_step1_tablemain()
    {
        // if (!$this->input->is_ajax_request()) {
        //     exit('No direct script access allowed');
        // }

        $wo_kd = $this->input->get('wo_kd', true);

        $data['wo_kd'] = $wo_kd;
        $data['wo'] = $this->tm_workorder->get_by_param(['wo_kd' => $wo_kd])->row_array();
        $dtx = $this->db->where('td_workorder_item_so.wo_kd', $wo_kd)
            ->select('tm_barang.item_code as itemcode_real, td_workorder_item_so.wo_kd AS wo_kd_real, td_workorder_item_so.woitemso_kd AS woitemso_kd_real, SUM(td_workorder_item_so_detail.woitemsodetail_qty) AS real_qty, tm_bom.bom_kd AS bom_real, tbm.bom_kd AS bom_kd_real, td_workorder_item_so.*, tm_bom.*, tm_barang.item_code, td_workorder_item_so_detail.*')
            ->join('td_workorder_item_so_detail', 'td_workorder_item_so_detail.woitemso_kd=td_workorder_item_so.woitemso_kd', 'left')
            ->join('tm_barang', 'tm_barang.item_code=td_workorder_item_so_detail.woitemsodetail_itemcode', 'left')
            ->join('tm_barang AS tmb', 'tmb.item_code=td_workorder_item_so.woitemso_itemcode', 'left')
            ->join('tm_bom AS tbm', 'tmb.kd_barang=tbm.kd_barang', 'left')
            ->join('tm_bom', 'tm_barang.kd_barang=tm_bom.kd_barang', 'left')
            ->group_by('td_workorder_item_so.woitemso_itemcode')
            ->order_by('td_workorder_item_so.woitemso_tgledit', 'desc')
            
            ->get('td_workorder_item_so')->result_array();

            $nextstep = true;  
            foreach ($dtx as $item) { 
                if($item['woitemso_prosesstatus'] == 'null_bom'){
                    $nextstep = false; 
                } 
            }  
        $data['woitemsos'] = $dtx;
        $data['nextstep'] = $nextstep;


        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_item_step1_tablemain', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function tableplanning_box()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $wo_kd = $this->input->get('wo_kd');

        $data['class_link'] = $this->class_link;
        $data['wo_kd'] = $wo_kd;

        $this->load->view('page/' . $this->class_link . '/tableplanning_box', $data);
    }


    public function tableplanning_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $wo_kd = $this->input->get('wo_kd', true);

        $this->load->model(['tb_finishgood_user', 'tm_wip']);
        /** Cek Lokasi User */
        $userfg = $this->tb_finishgood_user->get_by_param(array('kd_tipe_admin' => $this->session->userdata('tipe_admin_kd')))->row_array();
        $set_gudang = $userfg['kd_gudang'];
        /** Stok FG */
        $stokFG = $this->db->select('tm_barang.kd_barang, tm_barang.item_code, tm_barang.deskripsi_barang, tm_barang.dimensi_barang, COALESCE(SUM(tm_finishgood.fg_qty), 0) AS fg_qty, tm_barang.barang_stock_min, tm_barang.barang_stock_max')
            ->from('tm_barang')
            ->join('tm_finishgood', 'tm_barang.kd_barang=tm_finishgood.barang_kd', 'left')
            ->where([
                'tm_finishgood.kd_gudang' => 'MGD071218001'
            ])
            ->group_by('tm_barang.kd_barang')
            ->order_by("tm_barang.item_code", "asc")
            ->get()
            ->result_array();
        /** Stok WIP */
        $stokWIP = $this->tm_wip->get_all()->result_array();
        /** Sudah terplan */
        /** Item detail tidak di ambil */
        // $qStokPlanned = $this->db->select('tm_salesorder.kd_msalesorder, tm_salesorder.no_salesorder, tm_salesorder.tipe_customer, tm_salesorder.no_po, 
        // tm_barang.kd_barang, SUM(td_salesorder_item.item_qty) as qtyplanned, tm_workorder.wo_noterbit')
        //     ->join('tm_salesorder', 'tm_salesorder.kd_msalesorder=tb_relasi_sowo.kd_msalesorder', 'left')
        //     ->join('td_salesorder_item', 'tm_salesorder.kd_msalesorder=td_salesorder_item.msalesorder_kd', 'left')
        //     ->join('tm_workorder', 'tm_workorder.wo_kd=tb_relasi_sowo.wo_kd', 'left')
        //     ->join('tm_barang', 'tm_barang.kd_barang=td_salesorder_item.barang_kd', 'left')
        //     ->where([
        //         'tm_salesorder.status_so' => 'process_wo',
        //         'tm_workorder.wo_status' => 'process_wo',
        //     ])
        //     ->group_by('tm_workorder.wo_kd, 
        //         tm_barang.kd_barang, tm_salesorder.kd_msalesorder')                     
        //     ->order_by('tm_salesorder.tgl_so', "asc")
        //     ->get('tb_relasi_sowo')
        //     ->result_array();

     $qStokPlanned = $this->db->query(
         "SELECT * FROM (
            -- item parent 
             SELECT * FROM (SELECT '' as wo_kd, tm_salesorder.kd_msalesorder, tm_salesorder.no_salesorder, tm_salesorder.tipe_customer, tm_salesorder.no_po, tm_barang.kd_barang, SUM(td_salesorder_item.item_qty) as qtyplanned, '' AS wo_noterbit, tm_salesorder.tgl_so
            FROM tm_salesorder 
            LEFT JOIN td_salesorder_item ON tm_salesorder.kd_msalesorder=td_salesorder_item.msalesorder_kd 
            LEFT JOIN tm_barang ON tm_barang.kd_barang=td_salesorder_item.barang_kd 
            WHERE tm_salesorder.status_so NOT IN ('finish', 'cancel')
            GROUP BY  tm_barang.kd_barang, tm_salesorder.kd_msalesorder 
            UNION ALL
            -- item child jika ada
            SELECT '' as wo_kd, tm_salesorder.kd_msalesorder, tm_salesorder.no_salesorder, tm_salesorder.tipe_customer, tm_salesorder.no_po, tm_barang.kd_barang, SUM(td_salesorder_item_detail.item_qty) as qtyplanned, '' AS wo_noterbit, tm_salesorder.tgl_so
            FROM tm_salesorder 
            LEFT JOIN td_salesorder_item_detail ON tm_salesorder.kd_msalesorder=td_salesorder_item_detail.msalesorder_kd
            LEFT JOIN tm_barang ON tm_barang.kd_barang=td_salesorder_item_detail.kd_child
            WHERE tm_salesorder.status_so NOT IN ('finish', 'cancel')
            GROUP BY tm_barang.kd_barang, tm_salesorder.kd_msalesorder) v WHERE kd_msalesorder NOT IN (SELECT p.kd_msalesorder FROM tb_relasi_sowo as p)
            
                                
                                 UNION ALL
            -- item parent 
            SELECT tm_workorder.wo_kd, tm_salesorder.kd_msalesorder, tm_salesorder.no_salesorder, tm_salesorder.tipe_customer, tm_salesorder.no_po, tm_barang.kd_barang, SUM(td_salesorder_item.item_qty) as qtyplanned, tm_workorder.wo_noterbit, tm_salesorder.tgl_so
            FROM tm_salesorder 
            LEFT JOIN tb_relasi_sowo ON tm_salesorder.kd_msalesorder=tb_relasi_sowo.kd_msalesorder 
            LEFT JOIN td_salesorder_item ON tm_salesorder.kd_msalesorder=td_salesorder_item.msalesorder_kd 
            LEFT JOIN tm_workorder ON tm_workorder.wo_kd=tb_relasi_sowo.wo_kd 
            LEFT JOIN tm_barang ON tm_barang.kd_barang=td_salesorder_item.barang_kd 
            WHERE tm_salesorder.status_so NOT IN ('finish', 'cancel') AND tm_workorder.wo_status NOT IN ('finish', 'cancel')
            GROUP BY tm_workorder.wo_kd, tm_barang.kd_barang, tm_salesorder.kd_msalesorder 
            UNION ALL
            -- item child jika ada
            SELECT tm_workorder.wo_kd, tm_salesorder.kd_msalesorder, tm_salesorder.no_salesorder, tm_salesorder.tipe_customer, tm_salesorder.no_po, tm_barang.kd_barang, SUM(td_salesorder_item_detail.item_qty) as qtyplanned, tm_workorder.wo_noterbit, tm_salesorder.tgl_so
            FROM tb_relasi_sowo 
            LEFT JOIN tm_salesorder ON tm_salesorder.kd_msalesorder=tb_relasi_sowo.kd_msalesorder 
            LEFT JOIN td_salesorder_item_detail ON tm_salesorder.kd_msalesorder=td_salesorder_item_detail.msalesorder_kd
            LEFT JOIN tm_workorder ON tm_workorder.wo_kd=tb_relasi_sowo.wo_kd 
            LEFT JOIN tm_barang ON tm_barang.kd_barang=td_salesorder_item_detail.kd_child
            WHERE tm_salesorder.status_so NOT IN ('finish', 'cancel') AND tm_workorder.wo_status NOT IN ('finish', 'cancel')
            GROUP BY tm_workorder.wo_kd, tm_barang.kd_barang, tm_salesorder.kd_msalesorder 
            ) x
             GROUP BY x.wo_kd, x.kd_barang, x.kd_msalesorder ORDER BY x.tgl_so ASC"
         )->result_array();

        
        // $qStokPlanned = $this->db->query(
        //         "SELECT * FROM (
        //             -- item parent 
        //             SELECT tm_salesorder.kd_msalesorder, tm_salesorder.no_salesorder, tm_salesorder.tipe_customer, tm_salesorder.no_po, tm_barang.kd_barang, SUM(td_salesorder_item.item_qty) as qtyplanned, tm_workorder.wo_noterbit, tm_salesorder.tgl_so
        //             FROM tm_salesorder 
        //             LEFT JOIN tb_relasi_sowo ON tm_salesorder.kd_msalesorder=tb_relasi_sowo.kd_msalesorder 
        //             LEFT JOIN td_salesorder_item ON tm_salesorder.kd_msalesorder=td_salesorder_item.msalesorder_kd 
        //             LEFT JOIN tm_workorder ON tm_workorder.wo_kd=tb_relasi_sowo.wo_kd 
        //             LEFT JOIN tm_barang ON tm_barang.kd_barang=td_salesorder_item.barang_kd 
        //             WHERE tm_salesorder.status_so NOT IN ('finish', 'cancel') AND tm_workorder.wo_status NOT IN ('finish', 'cancel')
        //             GROUP BY tm_workorder.wo_kd, tm_barang.kd_barang, tm_salesorder.kd_msalesorder 
        //             UNION ALL
        //             -- item child jika ada
        //             SELECT tm_salesorder.kd_msalesorder, tm_salesorder.no_salesorder, tm_salesorder.tipe_customer, tm_salesorder.no_po, tm_barang.kd_barang, SUM(td_salesorder_item_detail.item_qty) as qtyplanned, tm_workorder.wo_noterbit, tm_salesorder.tgl_so
        //             FROM tb_relasi_sowo 
        //             LEFT JOIN tm_salesorder ON tm_salesorder.kd_msalesorder=tb_relasi_sowo.kd_msalesorder 
        //             LEFT JOIN td_salesorder_item_detail ON tm_salesorder.kd_msalesorder=td_salesorder_item_detail.msalesorder_kd
        //             LEFT JOIN tm_workorder ON tm_workorder.wo_kd=tb_relasi_sowo.wo_kd 
        //             LEFT JOIN tm_barang ON tm_barang.kd_barang=td_salesorder_item_detail.kd_child
        //             WHERE tm_salesorder.status_so NOT IN ('finish', 'cancel') AND tm_workorder.wo_status NOT IN ('finish', 'cancel')
        //             GROUP BY tm_workorder.wo_kd, tm_barang.kd_barang, tm_salesorder.kd_msalesorder 
        //             ) x
        //             ORDER BY x.tgl_so ASC"
        // )->result_array();
        

        /** Qty barang keluar dari yg planned */
        $qStokPlannedOut = $this->db->select('tb_relasi_sowo.kd_msalesorder, tm_finishgood.barang_kd, SUM(td_finishgood_out.fgout_qty) as sum_out')
            ->join('tm_salesorder', 'tb_relasi_sowo.kd_msalesorder=tm_salesorder.kd_msalesorder', 'left')
            ->join('td_finishgood_out', 'td_finishgood_out.kd_msalesorder=tm_salesorder.kd_msalesorder', 'left')
            ->join('td_finishgood_in', 'td_finishgood_in.fgin_kd=td_finishgood_out.fgin_kd', 'left')
            ->join('tm_finishgood', 'tm_finishgood.fg_kd=td_finishgood_in.fg_kd', 'left')
            ->group_by('tb_relasi_sowo.kd_msalesorder, tm_finishgood.barang_kd')
            ->get('tb_relasi_sowo')
            ->result_array();
        $stokPlanned = [];
        foreach ($qStokPlanned as $ePlanned) {
            $qtyplanned = $ePlanned['qtyplanned'];
            foreach ($qStokPlannedOut as $eStokPlannedOut) {
                if ($eStokPlannedOut['kd_msalesorder'] == $ePlanned['kd_msalesorder'] && $eStokPlannedOut['barang_kd'] == $ePlanned['kd_barang']) {
                    $qtyplanned = $qtyplanned - $eStokPlannedOut['sum_out'];
                    if ($qtyplanned < 0) {
                        $qtyplanned = 0;
                    }
                }
            }
            $stokPlanned[] = [
                'kd_msalesorder' => $ePlanned['kd_msalesorder'],
                'no_salesorder' => $ePlanned['no_salesorder'],
                'tipe_customer' => $ePlanned['tipe_customer'],
                'no_po' => $ePlanned['no_po'],
                'kd_barang' => $ePlanned['kd_barang'],
                'wo_noterbit' => $ePlanned['wo_noterbit'],
                'qtyplanned' => $qtyplanned,
            ];
        }

        /** Akan diplan */
        $stokPlanning = $this->db->select('td_workorder_item_so_detail.kd_msalesorder, tm_salesorder.no_salesorder, tm_salesorder.tipe_customer, tm_salesorder.no_po, td_workorder_item_so.kd_barang, SUM(td_workorder_item_so_detail.woitemsodetail_qty) as qtyplanning, tm_workorder.wo_noterbit')
            ->from('td_workorder_item_so')
            ->join('td_workorder_item_so_detail', 'td_workorder_item_so_detail.woitemso_kd=td_workorder_item_so.woitemso_kd', 'left')
            ->join('tm_salesorder', 'tm_salesorder.kd_msalesorder=td_workorder_item_so_detail.kd_msalesorder', 'left')
            ->join('tm_workorder', 'td_workorder_item_so.wo_kd=tm_workorder.wo_kd', 'left')
            ->where(['td_workorder_item_so.wo_kd' => $wo_kd, 'tm_workorder.wo_status' => 'pending'])
            ->where('td_workorder_item_so_detail.kd_msalesorder IS NOT NULL')
            ->group_by('td_workorder_item_so_detail.kd_msalesorder, td_workorder_item_so.kd_barang')
            ->get()
            ->result_array();

        $woPlanning = $this->db->select('td_workorder_item_so.kd_barang, SUM(td_workorder_item_so.woitemso_qty) as qtywoplanning, tm_workorder.wo_noterbit')
            ->from('td_workorder_item_so')
            ->join('td_workorder_item_so_detail', 'td_workorder_item_so_detail.woitemso_kd=td_workorder_item_so.woitemso_kd', 'left')
            ->join('tm_salesorder', 'tm_salesorder.kd_msalesorder=td_workorder_item_so_detail.kd_msalesorder', 'left')
            ->join('tm_workorder', 'td_workorder_item_so.wo_kd=tm_workorder.wo_kd', 'left')
            ->where([
                'td_workorder_item_so.wo_kd' => $wo_kd,
                //'td_workorder_item_so.woitemso_status' => 'std',
                'td_workorder_item_so.woitemso_prosesstatus' => 'editing'
            ])
            ->group_by('td_workorder_item_so.kd_barang')
            ->get()
            ->result_array();

        $data['class_link'] = $this->class_link;
        $data['wo_kd'] = $wo_kd;
        $data['stokFG'] = $stokFG;
        $data['stokWIP'] = $stokWIP;
        $data['stokPlanned'] = $stokPlanned;
        $data['stokPlanning'] = $stokPlanning;
        $data['woPlanning'] = $woPlanning;
        $this->load->view('page/' . $this->class_link . '/tableplanning_main', $data);
    }

    public function itemdetail_box()
    {
        parent::administrator();
        parent::datetimepicker_assets();
        parent::select2_assets();
        parent::typeahead_assets();
        parent::pnotify_assets();
        parent::icheck_assets();

        $wo_kd = $this->input->get('wo_kd');

        /** information WO */
        $wo = $this->tm_workorder->get_by_param(['wo_kd' => $wo_kd])->row_array();
        $salesorders = $this->tb_relasi_sowo->get_by_param_detail(['tb_relasi_sowo.wo_kd' => $wo_kd]);
        $arrNoSO = [];
        if ($salesorders->num_rows == 1) :
            $salesorders = $salesorders->row();
            $no_salesorders = $salesorders->no_salesorder;
            if ($salesorders->tipe_customer == 'Ekspor') :
                $no_salesorders = $salesorders->no_po;
            endif;
        else :
            $salesorders = $salesorders->result();
            foreach ($salesorders as $each) :
                $no_so = $each->no_salesorder;
                if ($each->tipe_customer == 'Ekspor') :
                    $no_so = $each->no_po;
                endif;
                $arrNoSO[] = $no_so;
            endforeach;
            $no_salesorders = implode(',', $arrNoSO);
        endif;

        $data['no_salesorders'] = $no_salesorders;
        $data['wo'] = $wo;
        $data['class_link'] = $this->class_link;
        $data['wo_kd'] = $wo_kd;
        $this->load->view('page/' . $this->class_link . '/itemdetail_box', $data);
    }

    public function itemdetail_table()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $wo_kd = $this->input->get('wo_kd');

        $partjenis = $this->td_part_jenis->get_by_param(['partjenis_generatewo' => 'T'])->result_array();
        $items = $this->db->select('td_workorder_item.*, td_workorder_item_detail.kd_barang, td_workorder_item_so.woitemso_itemcode')
            ->from('td_workorder_item')
            ->join('td_workorder_item_detail', 'td_workorder_item_detail.woitem_kd=td_workorder_item.woitem_kd', 'left')
            ->join('td_workorder_item_so', 'td_workorder_item_detail.woitemso_kd=td_workorder_item_so.woitemso_kd', 'left')
            ->where('td_workorder_item.wo_kd', $wo_kd)
            ->group_by('td_workorder_item.woitem_kd')
            ->order_by('td_workorder_item.woitem_no_wo')
            ->get()->result_array();
        $itemdetailpart = $this->td_workorder_item->woitem_detail_partjenis($wo_kd)->result_array();
        $itemdetail = $this->td_workorder_item_detail->get_by_param_woitem(['td_workorder_item_detail.wo_kd' => $wo_kd])->result_array();

        $data['itemdetailpart'] = $itemdetailpart;
        $data['itemdetail'] = $itemdetail;
        $data['partjenis'] = $partjenis;
        $data['items'] = $items;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/itemdetail_table', $data);
    }

    public function itemdetail_formpyr()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $slug = $this->input->get('slug');
        $wo_kd = $this->input->get('wo_kd');

        /** Dropdown Satuan */
        $opsiSatuan = [
            'unit' => 'Unit',
            'pcs' => 'Pcs',
        ];

        $data['class_link'] = $this->class_link;
        $data['wo_kd'] = $wo_kd;
        $data['slug'] = $slug;
        $data['opsiSatuan'] = $opsiSatuan;
        $this->load->view('page/' . $this->class_link . '/itemdetail_formpyr', $data);
    }

    public function get_item_woitemdetail_packing(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $woitem_kd = $this->input->get('woitem_kd');
        $resp = $this->td_workorder_item_detail->get_itemchild_by_parent($woitem_kd);
        echo json_encode($resp);
    }

    public function itemdetail_form()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $slug = $this->input->get('slug');
        $woitem_kd = $this->input->get('id');

        if ($slug == 'edit') {
            $woitem = $this->db->select()->from('td_workorder_item')
                ->join('td_workorder_item_detail', 'td_workorder_item.woitem_kd=td_workorder_item_detail.woitem_kd', 'left')
                ->where(['td_workorder_item.woitem_kd' => $woitem_kd])
                ->get()->row_array();
            $data['rowData'] = $woitem;
        }

        /** Dropdown Satuan */
        $opsiSatuan = [
            'unit' => 'Unit',
            'pcs' => 'Pcs',
        ];

        $data['class_link'] = $this->class_link;
        $data['woitem_kd'] = $woitem_kd;
        $data['slug'] = $slug;
        $data['opsiSatuan'] = $opsiSatuan;
        $this->load->view('page/' . $this->class_link . '/itemdetail_form', $data);
    }

    public function itemunproseswo_box()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $wo_kd = $this->input->get('wo_kd');

        $data['class_link'] = $this->class_link;
        $data['wo_kd'] = $wo_kd;
        $this->load->view('page/' . $this->class_link . '/itemunproseswo_box', $data);
    }

    public function itemunproseswo_form()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $wo_kd = $this->input->get('wo_kd');

        $data['class_link'] = $this->class_link;
        $data['wo_kd'] = $wo_kd;
        $this->load->view('page/' . $this->class_link . '/itemunproseswo_form', $data);
    }

    public function itemunproseswo_table()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        parent::icheck_assets();
        $wo_kd = $this->input->get('wo_kd');

        $data['items'] = $this->db->join('tm_salesorder', 'tm_salesorder.kd_msalesorder=td_workorder_item_so_detail.kd_msalesorder', 'left')
            ->join('td_workorder_item_so', 'td_workorder_item_so.woitemso_kd=td_workorder_item_so_detail.woitemso_kd', 'left')
            ->join('tm_workorder', 'tm_workorder.wo_kd=td_workorder_item_so_detail.wo_kd', 'left')
            ->where('tm_workorder.wo_status', 'process_wo')
            ->where_in('td_workorder_item_so.woitemso_prosesstatus', ['editing', 'null_bom'])
            ->get('td_workorder_item_so_detail')->result_array();
        $data['wo_kd'] = $wo_kd;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/itemunproseswo_table', $data);
    }

    public function itemunplanwo_box()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $wo_kd = $this->input->get('wo_kd');

        $data['class_link'] = $this->class_link;
        $data['wo_kd'] = $wo_kd;
        $this->load->view('page/' . $this->class_link . '/itemunplanwo_box', $data);
    }

    public function itemunplanwo_getitemso()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $wo_kd = $this->input->get('wo_kd');
        $kd_msalesorder = $this->input->get('kd_msalesorder');
        $kd_barang = $this->input->get('kd_barang');

        if (!empty($wo_kd) && !empty($kd_msalesorder) && !empty($kd_barang)) {
            $itemsos = $this->td_workorder_item_so->get_by_param(['wo_kd' => $wo_kd, 'kd_msalesorder' => $kd_msalesorder, 'kd_barang' => $kd_barang, 'woitemso_status' => 'std']);
            if (!empty($itemsos->num_rows())) {
                $itemsos = $itemsos->row_array();
                $resp['code'] = 200;
                $resp['status'] = 'Sukses';
                $resp['pesan'] = 'Not Null';
                $resp['data'] = $itemsos;
            } else {
                $resp['code'] = 400;
                $resp['status'] = 'Gagal';
                $resp['pesan'] = 'Item tidak ditemukan';
                $resp['data'] = null;
            }
        } else {
            $resp['code'] = 400;
            $resp['status'] = 'Gagal';
            $resp['pesan'] = 'Input kosong';
            $resp['data'] = null;
        }
        echo json_encode($resp);
    }

    public function itemadditionalso_table()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        parent::icheck_assets();

        $wo_kd = $this->input->get('wo_kd');

        $salesorders = $this->tm_salesorder->get_by_status(['process_wo'])->result_array();

        $kd_msalesorders = [];
        $arraySOadditional = [];
        $kd_msalesorders = array_column($salesorders, 'kd_msalesorder');

        $itemwos = $this->db->select('*')
            ->from('tb_relasi_sowo')
            ->join('td_workorder_item_so_detail', 'tb_relasi_sowo.kd_msalesorder=td_workorder_item_so_detail.kd_msalesorder', 'left')
            ->where_in('tb_relasi_sowo.kd_msalesorder', $kd_msalesorders)
            ->get()->result_array();

        if (!empty($kd_msalesorders)) {
            $salesorders = $this->tm_salesorder->get_wherein($kd_msalesorders)->result_array();
            $items = $this->td_salesorder_item->get_items($kd_msalesorders);
            $itemdetails = $this->td_salesorder_item_detail->get_items($kd_msalesorders);

            $arraySOparent = [];
            $arraySOchild = [];
            foreach ($items as $r_item) {
                if ($r_item->order_tipe == 'additional') {
                    $arraySOparent[] = [
                        'parent_kd' => $r_item->kd_ditem_so,
                        'child_kd' => null,
                        'msalesorder_kd' => $r_item->msalesorder_kd,
                        'barang_kd' => $r_item->barang_kd,
                        'item_code' => $r_item->item_code,
                        'item_status' => $r_item->item_status,
                        'item_desc' => $r_item->item_desc,
                        'item_dimension' => $r_item->item_dimension,
                        'item_qty' => $r_item->item_qty,
                        'order_tipe' => $r_item->order_tipe,
                    ];
                }
            }
            foreach ($itemdetails as $r_itemdetail) {
                if ($r_itemdetail->order_tipe == 'additional') {
                    $arraySOchild[] = [
                        'parent_kd' => null,
                        'child_kd' => $r_itemdetail->kd_citem_so,
                        'msalesorder_kd' => $r_itemdetail->msalesorder_kd,
                        'barang_kd' => $r_itemdetail->kd_child,
                        'item_code' => $r_itemdetail->item_code,
                        'item_status' => $r_itemdetail->item_status,
                        'item_desc' => $r_itemdetail->item_desc,
                        'item_dimension' => $r_itemdetail->item_dimension,
                        'item_qty' => $r_itemdetail->item_qty,
                        'order_tipe' => $r_itemdetail->order_tipe,
                    ];
                }
            }

            if (!empty($itemwos)) {
                $itemwoParent = [];
                $itemwoChild = [];
                foreach ($itemwos as $r_itemwo) {
                    if (!empty($r_itemwo['parent_kd'])) {
                        $itemwoParent[] = $r_itemwo['parent_kd'];
                    } else {
                        $itemwoChild[] = $r_itemwo['child_kd'];
                    }
                }
                /** Parent */
                foreach ($arraySOparent as $r_arraySOparent) {
                    if (in_array($r_arraySOparent['parent_kd'], $itemwoParent)) {
                        continue;
                    }
                    $arraySOadditional[] = $r_arraySOparent;
                }
                /** Child */
                foreach ($arraySOchild as $r_arraySOchild) {
                    if (in_array($r_arraySOchild['child_kd'], $itemwoChild)) {
                        continue;
                    }
                    $arraySOadditional[] = $r_arraySOchild;
                }
            }
        }

        $data['class_link'] = $this->class_link;
        $data['wo_kd'] = $wo_kd;
        $data['salesorders'] = $salesorders;
        $data['itemsoadditional'] = $arraySOadditional;
        $this->load->view('page/' . $this->class_link . '/itemadditionalso_table', $data);
    }

    public function form_item_step0_box()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::icheck_assets();

        $wo_kd = $this->input->get('wo_kd');

        $data['class_link'] = $this->class_link;
        $data['wo_kd'] = $wo_kd;

        $this->load->view('page/' . $this->class_link . '/form_item_step0_box', $data);
    }

    public function form_item_step0_tablemain()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $wo_kd = $this->input->get('wo_kd', true);

        $data['wo'] = $this->tm_workorder->get_by_param(['wo_kd' => $wo_kd])->row_array();
        $data['woitemsodetails'] = $this->db->join('tb_relasi_sowo', 'tb_relasi_sowo.relasisowo_kd=td_workorder_item_so_detail.relasisowo_kd', 'left')
            ->join('tm_salesorder', 'tm_salesorder.kd_msalesorder=td_workorder_item_so_detail.kd_msalesorder', 'left')
            ->where('td_workorder_item_so_detail.wo_kd', $wo_kd)
            ->get('td_workorder_item_so_detail')->result_array();

        $data['wo_kd'] = $wo_kd;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_item_step0_tablemain', $data);
    }

    public function form_item_step2_box()
    {
        parent::administrator();
        parent::pnotify_assets();
        $wo_kd = $this->input->get('wo_kd');

        $data['wo'] = $this->tm_workorder->get_by_param(['wo_kd' => $wo_kd])->row_array();
        $data['class_link'] = $this->class_link;
        $data['wo_kd'] = $wo_kd;

        $this->load->view('page/' . $this->class_link . '/form_item_step2_box', $data);
    }

    public function form_item_step2_tablemain()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $wo_kd = $this->input->get('wo_kd');

        $data['class_link'] = $this->class_link;
        $data['wo_kd'] = $wo_kd;
        $data['wo'] = $this->tm_workorder->get_by_param(['wo_kd' => $wo_kd])->row_array();
        $data['woitemdetails'] = $this->db->join('td_workorder_item_so', 'td_workorder_item_so.woitemso_kd=td_workorder_item_detail.woitemso_kd', 'left')
            ->join('td_part', 'td_part.part_kd=td_workorder_item_detail.part_kd', 'left')
            ->join('tm_part_main', 'tm_part_main.partmain_kd=td_part.partmain_kd', 'left')
            ->join('td_part_jenis', 'td_part_jenis.partjenis_kd=tm_part_main.partjenis_kd', 'left')
            ->order_by('td_workorder_item_detail.woitemso_kd, td_workorder_item_detail.partjenis_kd, td_workorder_item_detail.woitemdetail_generatewo')
            ->where('td_workorder_item_detail.wo_kd', $wo_kd)
            ->get('td_workorder_item_detail')->result_array();

        $this->load->view('page/' . $this->class_link . '/form_item_step2_tablemain', $data);
    }

    public function form_item_step2_formmain()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $woitemdetail_kd = $this->input->get('id');

        $data['class_link'] = $this->class_link;
        $data['sts'] = 'edit';
        $data['woitemdetail'] = $this->db->join('td_part', 'td_part.part_kd=td_workorder_item_detail.part_kd', 'left')
            ->join('tm_part_main', 'tm_part_main.partmain_kd=td_part.partmain_kd', 'left')
            ->join('td_workorder_item_so', 'td_workorder_item_so.woitemso_kd=td_workorder_item_detail.woitemso_kd', 'left')
            ->join('tm_workorder', 'tm_workorder.wo_kd=td_workorder_item_detail.wo_kd', 'left')
            ->where('td_workorder_item_detail.woitemdetail_kd', $woitemdetail_kd)
            ->get('td_workorder_item_detail')
            ->row_array();

        $this->load->view('page/' . $this->class_link . '/form_item_step2_formmain', $data);
    }

    public function previewresultwo_table()
    {
        $wo_kd = $this->input->get('wo_kd');

        $woitemComplete = $this->td_workorder_item->get_by_param(['wo_kd' => $wo_kd, 'woitem_jenis' => 'packing'])->result_array();
        $woitempartdetails = $this->db->join('td_workorder_item_so', 'td_workorder_item_so.woitemso_kd=td_workorder_item_detail.woitemso_kd', 'left')
            ->join('td_part', 'td_part.part_kd=td_workorder_item_detail.part_kd', 'left')
            ->join('tm_part_main', 'tm_part_main.partmain_kd=td_part.partmain_kd', 'left')
            ->where('td_workorder_item_detail.wo_kd', $wo_kd)
            ->where('td_workorder_item_so.woitemso_status', 'std')
            ->where('td_workorder_item_detail.woitem_kd IS NULL')
            ->get('td_workorder_item_detail')->result_array();

        $arrayPartGroups = [];
        $arrayPartUngroups = [];
        $arrayPartUngenerateWOs = [];
        foreach ($woitempartdetails as $woitempartdetail) {
            if ($woitempartdetail['woitemdetail_generatewo'] == 'T') {
                if ($woitempartdetail['woitemdetail_wogrouping'] == 'T') {
                    $arrayPartGroups[$woitempartdetail['part_kd']][] = $woitempartdetail;
                } else {
                    $arrayPartUngroups[] = $woitempartdetail;
                }
            } else {
                // No genereate WO
                $arrayPartUngenerateWOs[] = $woitempartdetail;
            }
        }

        /** Group By part_kd */
        $woitem_kd = '';
        $arrayWorkorderitem['packing'] = $woitemComplete;
        $arrayBatchUpdateDetail = [];
        foreach ($arrayPartGroups as $part_kds => $elements) {
            $woitemso_qty = 0;
            foreach ($elements as $element) {
                $woitemso_qty += $element['woitemso_qty'];
            }
            $woitem_kd = $this->td_workorder_item->buat_kode($woitem_kd);
            $arrayWorkorderitem['part'][] = [
                'woitem_kd' => $woitem_kd,
                'wo_kd' => $wo_kd,
                'woitem_itemcode' => $elements[0]['partmain_nama'],
                'woitem_no_wo' => null,
                'woitem_qty' => $woitemso_qty,
                'woitem_jenis' => 'part',
                'woitem_note' => null,
                'woitem_satuan' => 'pcs',
                'woitem_status' => 'std',
                'woitem_tipe' => 'so',
                'woitem_tglselesai' => null,
                'woitem_prosesstatus' => 'workorder',
                'woitem_tglinput' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            ];

            /** Untuk update woitem_kd pada td_workorder_item_detail */
            foreach ($elements as $element2) {
                $arrayBatchUpdateDetail[] = [
                    'woitemdetail_kd' => $element2['woitemdetail_kd'],
                    'woitem_kd' => $woitem_kd,
                ];
            }
        }

        /** UnGroup part_kd */
        foreach ($arrayPartUngroups as $arrayPartUngroup) {
            $woitem_kd = $this->td_workorder_item->buat_kode($woitem_kd);
            $arrayWorkorderitem['part'][] = [
                'woitem_kd' => $woitem_kd,
                'wo_kd' => $wo_kd,
                'woitem_itemcode' => $arrayPartUngroup['partmain_nama'],
                'woitem_no_wo' => null,
                'woitem_qty' => $arrayPartUngroup['woitemso_qty'],
                'woitem_jenis' => 'part',
                'woitem_note' => null,
                'woitem_satuan' => 'pcs',
                'woitem_status' => 'std',
                'woitem_tipe' => 'so',
                'woitem_tglselesai' => null,
                'woitem_prosesstatus' => 'workorder',
                'woitem_tglinput' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            ];
        }

        $data['woitems'] = $arrayWorkorderitem;
        $data['wo_kd'] = $wo_kd;
        $data['class_link'] = $this->class_link;

        $this->load->view('page/' . $this->class_link . '/previewresultwo_table', $data);
    }

    public function action_insert_itemadditionalso()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('additionalsowo_kd', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrItemStatus' => (!empty(form_error('additionalsowo_kd'))) ? buildLabel('warning', form_error('additionalsowo_kd', '"', '"')) : '',
            );
        } else {
            $wo_kd = $this->input->post('additionalsowo_kd', true);
            $soitems = $this->input->post('soitems', true);

            $this->db->trans_start();

            $this->td_workorder_item_so_detail->insertBatch_additionalSalesOrder($wo_kd, $soitems);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            } else {
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'wo_kd' => $wo_kd);
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_submit_unproseswo()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtunproseswo_kd', 'Jenis', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrItemStatus' => (!empty(form_error('txtunproseswo_kd'))) ? buildLabel('warning', form_error('txtunproseswo_kd', '"', '"')) : '',
            );
        } else {
            $wo_kd = $this->input->post('txtunproseswo_kd', true);
            $woitemsodetailkds = $this->input->post('woitemsodetailkds', true);
            $woitemsokds = $this->input->post('woitemsokds', true);

            if (!empty($woitemsodetailkds)) {
                $woitemso_kds = [];
                foreach ($woitemsodetailkds as $woitemsodetail_kd) {
                    $data[] = array(
                        'woitemsodetail_kd' => $woitemsodetail_kd,
                        'wo_kd' => $wo_kd,
                        'woitemso_kd' => null,
                        'woitemsodetail_tgledit' => date('Y-m-d H:i:s'),
                        'admin_kd' => $this->session->userdata('kd_admin'),
                    );
                    $woitemso_kds[] = $woitemsokds[$woitemsodetail_kd];
                }
                $actUpdateBatch = $this->db->update_batch('td_workorder_item_so_detail', $data, 'woitemsodetail_kd');
                if ($actUpdateBatch) {
                    /** Del woitemso */
                    if (!empty($woitemso_kds)) {
                        $woitemso_kds = array_unique($woitemso_kds);
                        $this->db->where_in('woitemso_kd', $woitemso_kds)->delete('td_workorder_item_so');
                    }
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Item Ditambah');
                } else {
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
                }
            } else {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Item tidak ada yang dipiih');
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function get_bom()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $bom_jenis = $this->input->get('bom_jenis', true);

        $boms = $this->tm_bom->get_by_param_detail(['tm_bom.bom_jenis' => $bom_jenis])->result();

        $opsiBom = array();
        foreach ($boms as $bom) :
            if ($bom->bom_jenis == 'std') :
                $itemcode = $bom->item_code == NULL ? $bom->rm_kode : $bom->item_code;
            else :
                $itemcode = $bom->project_nopj;
            endif;

            $opsiBom[] = array(
                'bom_kd' => $bom->bom_kd,
                'bom_jenis' => $bom->bom_jenis,
                'itemcode' => $itemcode,
                'bom_ket' => $bom->bom_ket,
            );
        endforeach;

        $resp = array('code' => 200, 'status' => 'Sukses', 'data' => $opsiBom);

        header('Content-Type: application/json');
        echo json_encode($resp);
    }

    public function get_bomkd()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $kd_barang = $this->input->get('kd_barang', true);

        $rowBom_kd = $this->tm_bom->get_by_param(['kd_barang' => $kd_barang, 'bom_jenis' => 'std'])->row();
        $bom_kd = $rowBom_kd->bom_kd;

        $resp = array('code' => 200, 'status' => 'Sukses', 'data' => ['bom_kd' => $bom_kd]);

        header('Content-Type: application/json');
        echo json_encode($resp);
    }

    public function get_no_wo_inspection()
    {
        /** Fungsi untuk get data wo dan inspection berdasarkan refference (menu form add PYR) */
        $paramWO = $this->input->get('paramWO');
        $paramRefference = $this->input->get('paramRefference');
		$resultData = [];

        if($paramRefference == 'wo'){
            // WO
            $this->db->select('td_workorder_item.*, td_workorder_item_pyr.woitempyr_kd')
                ->from('td_workorder_item')
                ->join('td_workorder_item_pyr', 'td_workorder_item_pyr.woitemreject_kd=td_workorder_item.woitem_kd', 'left')
                ->like(['td_workorder_item.woitem_no_wo' => $paramWO])
                ->where('td_workorder_item.woitem_prosesstatus', 'workorder')
                ->where('td_workorder_item_pyr.woitempyr_kd', null)
                ->order_by('td_workorder_item.woitem_no_wo ASC');
            $query = $this->db->get();
        }else{
            // Param WO By Inspection
            /** WO PRODUCT */ 
            $this->db->select('tm_inspection_product.inspectionproduct_woitem_kd, 
                                tm_inspection_product.inspectionproduct_no, 
                                inspection_wo.woitem_kd, 
                                inspection_wo.woitem_no_wo, 
                                tm_inspection_product.inspectionproduct_qty, 
                                inspection_wo.woitem_qty, 
                                inspection_wo.woitem_status, 
                                inspection_wo.woitem_tglselesai, 
                                inspection_wo.woitem_jenis, 
                                tm_inspection_product.kd_inspectionproduct, 
                                inspection_wo.wo_kd, 
                                inspection_wo.woitem_itemcode, 
                                reject_pyr.woitempyr_kd')
                ->from('tm_inspection_product')
                ->join('td_workorder_item_pyr AS reject_pyr', 'reject_pyr.woitemreject_kd=tm_inspection_product.inspectionproduct_woitem_kd', 'left')
                ->join('td_workorder_item AS reject_wo ', 'reject_wo.woitem_kd=reject_pyr.woitemreject_kd', 'left')
                ->join('td_workorder_item AS inspection_wo ', 'inspection_wo.woitem_kd=tm_inspection_product.inspectionproduct_woitem_kd', 'left')
                ->where('inspection_wo.woitem_prosesstatus', 'workorder')
                ->where('tm_inspection_product.inspectionproduct_tindakan', 'reject')
                ->where('reject_pyr.woitempyr_kd', null)
                ->like(['tm_inspection_product.inspectionproduct_no' => $paramWO]);
            $queryProduct = $this->db->get_compiled_select();
            /** WO RAW MATERIAL */
            $this->db->select('"" AS inspectionproduct_woitem_kd,
                                "" AS inspectionrawmaterial_no,
                                td_workorder_item.woitem_kd,
                                td_workorder_item.woitem_no_wo,
                                "" AS inspectionrawmaterial_qty_reject,
                                td_workorder_item.woitem_qty,
                                td_workorder_item.woitem_status,
                                td_workorder_item.woitem_tglselesai,
                                td_workorder_item.woitem_jenis,
                                "" AS kd_inspectionrawmaterial,
                                td_workorder_item.wo_kd,
                                tm_rawmaterial.rm_kode,
                                "" AS woitempyr_kd')
            ->from('td_workorder_item')
            ->join('tm_rawmaterial', 'tm_rawmaterial.rm_kode = td_workorder_item.woitem_itemcode', 'left')
            ->join('(SELECT rm_kd FROM td_materialreceipt_detail_rawmaterial GROUP BY rm_kd) td_materialreceipt_detail_rawmaterial', 'td_materialreceipt_detail_rawmaterial.rm_kd = tm_rawmaterial.rm_kd', 'inner')
            ->join('td_workorder_item_pyr', 'td_workorder_item_pyr.woitemreject_kd=td_workorder_item.woitem_kd', 'left')
            ->where('td_workorder_item.woitem_prosesstatus', 'workorder')
            ->where('td_workorder_item_pyr.woitempyr_kd', null)
            ->like(['td_workorder_item.woitem_no_wo' => $paramWO]);
            $queryRawMaterial = $this->db->get_compiled_select();

            /** PROCESS UNION TABLE PRODUCT DAN RAWMATERIAL */
            $query = $this->db->query($queryProduct." UNION ".$queryRawMaterial." ORDER BY inspectionproduct_no ASC, woitem_no_wo ASC");
        }   
        
		if (!empty($query->num_rows())) {
			$result = $query->result_array();
			foreach ($result as $r) {
                /** ID DAN TEXT SELECT2 INITIATED KARENA NESTED TENNARY DI PHP 7 DISABLED */
                $id = "";
                $text = "";
                $jenis_item = ""; // product, rawmaterial
                if($paramRefference == 'wo'){
                    /** Refference by WO */
                    $id = $r['woitem_kd'];
                    $text = $r['woitem_no_wo'];
                    $jenis_item = "product";
                    $woitem_kd = $r['woitem_kd'];
                    $woitem_qty = $r['woitem_qty'];
                }else{
                    /** Refference by Inspection */
                    if(!empty($r['inspectionproduct_woitem_kd'])){
                        /** Jika wo yang dipilih product */
                        $id = $r['inspectionproduct_woitem_kd'];
                        $text = $r['woitem_no_wo']." | ".$r['inspectionproduct_no'];
                        $woitem_kd = $r['inspectionproduct_woitem_kd'];
                        $woitem_qty = $r['inspectionproduct_qty'];
                        $jenis_item = "product";
                    }else{
                        /** Jika wo yang dipilih rawmaterial */
                        $id = $r['woitem_kd'];
                        $text = $r['woitem_no_wo']." | ".$r['woitem_itemcode'];
                        $woitem_kd = $r['woitem_kd'];
                        $woitem_qty = $r['woitem_qty'];
                        $jenis_item = "rawmaterial";
                    }
                }
				$resultData[] = [
					'id'        => $id, 
					'text'      => $text,
                    'wo_kd'     => $r['wo_kd'],
                    'woitem_kd' => $woitem_kd,
                    'woitem_itemcode' => $r['woitem_itemcode'],
                    'woitem_qty' => $woitem_qty,
                    'woitem_status' => $r['woitem_status'],
                    'woitem_tglselesai' => $r['woitem_tglselesai'],
                    'woitem_jenis' => $r['woitem_jenis'],
                    'kd_inspectionproduct' => $paramRefference == 'inspection' ? $r['kd_inspectionproduct'] : '',
                    'jenis_item' => $jenis_item
                ];
			}
		}
		echo json_encode($resultData);
    }

    public function get_inspection_rawmaterial()
    {
        $param = $this->input->get('param');
        $rm_kode = $this->input->get('paramRMKode');
		$resultData = [];

        $this->db->select('*')
            ->from('tm_inspection_rawmaterial')
            ->where('inspectionrawmaterial_tindakan', 'reject')
            ->where('flag_pyr', '0')
            ->where('inspectionrawmaterial_rm_kode', $rm_kode)
            ->like(['inspectionrawmaterial_no' => $param]);

        $query = $this->db->get();

        if (!empty($query->num_rows())) {
			$result = $query->result_array();
			foreach ($result as $r) {
                $id = $r['kd_inspectionrawmaterial'];
                $text = $r['inspectionrawmaterial_no'];
				$resultData[] = [
					'id'        => $id, 
					'text'      => $text,
                    'inspectionrawmaterial_qty_reject' => $r['inspectionrawmaterial_qty_reject']
                ];
			}
		}
		echo json_encode($resultData);
    }

    public function cetak_pdf_wo()
    {
        $this->load->library('Pdf');
        $wo_kd = $this->uri->segment(4);
        if (!empty($wo_kd)) {
            $items = $this->td_workorder_item_detail->get_by_param_detail(['td_workorder_item_detail.wo_kd' => $wo_kd])->result();
            $rowWO = $this->tm_workorder->get_by_param(['wo_kd' => $wo_kd])->row();
            $data['class_link'] = $this->class_link;
            $data['rowWO'] = $rowWO;
            $data['items'] = $items;
            $this->load->view('page/' . $this->class_link . '/cetak_wo_pdf', $data);
        }
    }

    public function action_insert_master()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $kd_msalesorders = $this->input->post('txtkd_msalesorder');
        $wo_note = $this->input->post('txtwo_note');

        $wo_kd = $this->tm_workorder->create_code();
        $dataWO = array(
            'wo_kd' => $wo_kd,
            'wo_tanggal' => date('Y-m-d'),
            'wo_noterbit' => $this->tm_workorder->create_noterbit(),
            'wo_note' => !empty($wo_note) ? $wo_note : null,
            'wo_status' => 'pending',
            'wo_tglinput' => date('Y-m-d H:i:s'),
            'wo_tgledit' => date('Y-m-d H:i:s'),
            'admin_kd' => $this->session->userdata('kd_admin'),
        );

        if (!empty($kd_msalesorders)) {

            $this->db->trans_start();
            $this->tm_workorder->insert_data($dataWO);
            $this->tb_relasi_sowo->insert_batch_so($wo_kd, $kd_msalesorders);
            $this->td_workorder_item_so_detail->insertBatch_detailSalesOrderByWo($wo_kd);
            $this->action_update_status_so($kd_msalesorders, 'process_wo');
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            } else {
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'wo_kd' => $wo_kd);
            }
        } else {
            $actWO = $this->tm_workorder->insert_data($dataWO);
            $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terimpan SO Kosong', 'wo_kd' => $wo_kd);
        }

        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_insert_woitemso()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtItem_status', 'Item Status', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtProductCode', 'Product', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtbom_kd', 'BoM', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtwoitem_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrItemStatus' => (!empty(form_error('txtItem_status'))) ? buildLabel('warning', form_error('txtItem_status', '"', '"')) : '',
                'idErrItemJenisProses' => (!empty(form_error('txtItem_jenis_proses'))) ? buildLabel('warning', form_error('txtItem_jenis_proses', '"', '"')) : '',
                'idErrProductCode' => (!empty(form_error('txtProductCode'))) ? buildLabel('warning', form_error('txtProductCode', '"', '"')) : '',
                'idErrbom_kd' => (!empty(form_error('txtbom_kd'))) ? buildLabel('warning', form_error('txtbom_kd', '"', '"')) : '',
                'idErrwoitem_qty' => (!empty(form_error('txtwoitem_qty'))) ? buildLabel('warning', form_error('txtwoitem_qty', '"', '"')) : '',
            );
        } else {
            $wo_kd = $this->input->post('txtwo_kd', true);
            $kd_barang = $this->input->post('txtbarang_kd', true);
            $woitemso_itemcode = $this->input->post('txtProductCode', true);
            $woitemso_itemstatus = $this->input->post('txtItem_status', true);
            $woitemso_jenis_proses = $this->input->post('txtItem_jenis_proses', true);
            $woitemso_itemdeskripsi = $this->input->post('txtItem_desc', true);
            $woitemso_itemdimensi = $this->input->post('txtItemDimension', true);
            $woitemso_qty = $this->input->post('txtwoitem_qty', true);
            $bom_kd = $this->input->post('txtbom_kd', true);
            $woitemso_tglselesai = $this->input->post('txtwoitemso_tglselesai', true);

            /** QTY tidak boleh nol */
            if($woitemso_qty <= 0){
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan, Qty Tidak Boleh 0 (Nol)');
                $resp['csrf'] = $this->security->get_csrf_hash();
                echo json_encode($resp);
                die();
            }

            $woitemso_nopj = null;
            if ($woitemso_itemstatus == 'custom') {
                $bom = $this->tm_bom->get_by_param_detail(['tm_bom.bom_kd' => $bom_kd])->row();
                $woitemso_nopj = $bom->project_nopj;
            }

            /** Cek exiting itemcode in this workorde process 
             * Allow hanya untuk :
             * > item std yang tidak ada pada wo_kd ini
             * > item std with custom item
             * > item custom
             */
            define("CUSTOM_ITEM", "PRD020817000573");
            if ($woitemso_itemstatus == 'std') {
                if ($kd_barang != CUSTOM_ITEM) {
                    $woitemso = $this->td_workorder_item_so->get_by_param(['wo_kd' => $wo_kd, 'kd_barang' => $kd_barang])->num_rows();
                    if (!empty($woitemso)) {
                        $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Barang Sudah Ada');
                        $resp['csrf'] = $this->security->get_csrf_hash();
                        echo json_encode($resp);
                        die();
                    }
                }
            }

            $woitemso_kd = $this->td_workorder_item_so->buat_kode();
            $data = array(
                'woitemso_kd' => $woitemso_kd,
                'wo_kd' => $wo_kd,
                'kd_barang' => $kd_barang,
                'woitemso_itemcode' => !empty($woitemso_itemcode) ? $woitemso_itemcode : null,
                'woitemso_itemdeskripsi' => !empty($woitemso_itemdeskripsi) ? $woitemso_itemdeskripsi : null,
                'woitemso_itemdimensi' => !empty($woitemso_itemdimensi) ? $woitemso_itemdimensi : null,
                'woitemso_qty' => $woitemso_qty,
                'woitemso_status' => $woitemso_itemstatus,
                'woitemso_jenis_proses' => $woitemso_jenis_proses,
                'bom_kd' => $bom_kd,
                'woitemso_nopj' => $woitemso_nopj,
                'woitemso_tipe' => 'additional',
                'woitemso_prosesstatus' => 'editing',
                'woitemso_tglselesai' => format_date($woitemso_tglselesai, 'Y-m-d'),
                'woitemso_tglinput' => date('Y-m-d H:i:s'),
                'woitemso_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            );

            $this->db->trans_begin();

            $act = $this->td_workorder_item_so->insert_data($data);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            } else {
                $this->db->trans_commit();
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_update_woitemso()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtwoitemso_kd', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtbom_kd', 'Jenis', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrProductCode' => (!empty(form_error('txtwoitemso_kd'))) ? buildLabel('warning', form_error('txtwoitemso_kd', '"', '"')) : '',
                'idErrbom_kd' => (!empty(form_error('txtbom_kd'))) ? buildLabel('warning', form_error('txtbom_kd', '"', '"')) : '',
            );
        } else {
            $woitemso_kd = $this->input->post('txtwoitemso_kd', true);
            $bom_kd = $this->input->post('txtbom_kd', true);
            $woitemso_qty = $this->input->post('txtwoitem_qty', true);
            $woitem_qtyso = $this->input->post('txtwoitem_qtyso', true);
            $woitemso_tglselesai = $this->input->post('txtwoitemso_tglselesai', true);
            $woitemso_jenis_proses = $this->input->post('txtItem_jenis_proses', true);

            /** Find Nopj */
            $bom = $this->tm_bom->get_by_param_detail(['tm_bom.bom_kd' => $bom_kd])->row();
            $woitemso_nopj = $bom->project_nopj;

            $arraywoitemSO = array(
                'bom_kd' => $bom_kd,
                'woitemso_prosesstatus' => 'editing',
                'woitemso_qty' => !empty($woitemso_qty) ? $woitemso_qty : 0,
                'woitemso_jenis_proses' => $woitemso_jenis_proses,
                'woitemso_nopj' => $woitemso_nopj,
                'woitemso_tglselesai' => format_date($woitemso_tglselesai, 'Y-m-d'),
                'woitemso_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            );

            $act = $this->td_workorder_item_so->update_data(['woitemso_kd' => $woitemso_kd], $arraywoitemSO);
            if ($act) {
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
            } else {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_delete_wo_itemso()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $woitemso_kd = $this->input->get('woitemso_kd');
        if (!empty($woitemso_kd)) {
            $updateData = [
                'wo_kd' => null,
                'woitemso_tgledit' => date('Y-m-d H:i:s'),
            ];
            $act = $this->td_workorder_item_so->update_data(['woitemso_kd' => $woitemso_kd], $updateData);
            if ($act) {
                $resp = array('code' => 200, 'pesan' => 'Sukses');
            } else {
                $resp = array('code' => 400, 'pesan' => 'Gagal Delete');
            }
        } else {
            $resp = array('code' => 400, 'pesan' => 'Id Tidak Ditemukan');
        }
        echo json_encode($resp);
    }

    public function action_ubahstatus_woitemso()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $woitemso_kd = $this->input->get('woitemso_kd');
        $status = $this->input->get('status');

        if (!empty($woitemso_kd)) {
            if ($status == 'editing') {
                $woitemso = $this->td_workorder_item_so->get_by_param(['woitemso_kd' => $woitemso_kd])->row();
                $bom_kd = $woitemso->bom_kd;
                if (empty($bom_kd)) {
                    $status = 'null_bom';
                }
            }
            $updateData = [
                'woitemso_prosesstatus' => $status,
                'woitemso_tgledit' => date('Y-m-d H:i:s'),
            ];
            $act = $this->td_workorder_item_so->update_data(['woitemso_kd' => $woitemso_kd], $updateData);
            if ($act) {
                $resp = array('code' => 200, 'pesan' => 'Sukses');
            } else {
                $resp = array('code' => 400, 'pesan' => 'Gagal Update');
            }
        } else {
            $resp = array('code' => 400, 'pesan' => 'Id Tidak Ditemukan');
        }
        echo json_encode($resp);
    }

    public function action_update_woitemdetail()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtwoitemdetail_kd', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'iderrwoitemdetail_kd' => (!empty(form_error('txtwoitemdetail_kd'))) ? buildLabel('warning', form_error('txtItem_status', '"', '"')) : '',
            );
        } else {
            $woitemdetail_kd = $this->input->post('txtwoitemdetail_kd', true);
            $woitemdetail_generatewo = $this->input->post('txtwoitemdetail_generatewo', true);
            $woitemdetail_wogrouping = $this->input->post('txtwoitemdetail_wogrouping', true);

            $woitemdetail_generatewo = isset($woitemdetail_generatewo) ? 'T' : 'F';
            $woitemdetail_wogrouping = isset($woitemdetail_wogrouping) ? 'T' : 'F';


            $data = array(
                'woitemdetail_generatewo' => $woitemdetail_generatewo,
                'woitemdetail_wogrouping' => $woitemdetail_wogrouping,
                'woitemdetail_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            );

            $act = $this->td_workorder_item_detail->update_data(['woitemdetail_kd' => $woitemdetail_kd], $data);

            if ($act) {
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            } else {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_process_wo_step0()
    {
        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('wo_kd', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrItemStatus' => (!empty(form_error('wo_kd'))) ? buildLabel('warning', form_error('wo_kd', '"', '"')) : '',
            );
        } else {
            $wo_kd = $this->input->post('wo_kd');
            $slug = $this->input->post('slug');

            $act = true;
            $isEmptydetailSO = $this->tm_workorder->get_by_param(['wo_kd' => $wo_kd])->row();

            if (empty($isEmptydetailSO->wo_tglstep0)) {
                $act = $this->td_workorder_item_so->insertBatch_GroupByItemSO($wo_kd);
            }

            if ($act) {
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => $wo_kd);
            } else {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    /** Generate WO dengan penambahan step
     * step 1 -> generate so
     * step 2 -> generate mana yang akan di group dan mana yang tidak di generate wo
     * step 3 -> generate no wo
     * 
     * Slug : apabila reproses maka akan ada proses unttuk delete transaksi
     */
    public function action_process_wo_step1()
    {
        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('wo_kd', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrItemStatus' => (!empty(form_error('wo_kd'))) ? buildLabel('warning', form_error('wo_kd', '"', '"')) : '',
            );
        } else {
            $wo_kd = $this->input->post('wo_kd');
            $slug = $this->input->post('slug');

            if (!empty($wo_kd)) {
                $isEmptydetailSO = $this->tm_workorder->get_by_param(['wo_kd' => $wo_kd])->row();

                if (!empty($isEmptydetailSO->wo_tglstep1) && $slug != "reprocess") {
                    /** Sudah terproses */
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Sudah Terproses', 'data' => ['wo_kd' => $wo_kd]);
                    echo json_encode($resp);
                    die();
                }

                $this->db->trans_start();

                if ($slug == "reprocess") {
                    /** Delete item dan item detail  */
                    $this->td_workorder_item_detail->delete_by_param(['wo_kd' => $wo_kd]);
                    $this->td_workorder_item->delete_by_param(['wo_kd' => $wo_kd]);
                }

                $actPacking = $this->td_workorder_item->insertBatch_woComplete($wo_kd);
                if ($actPacking) {
                    $this->td_workorder_item_detail->insertBatch_woPart_step1($wo_kd);
                }
                /** Action WO custom */
                $wooo_kd = $this->td_workorder_item->insertBatch_woCustom($wo_kd);
                if($wooo_kd = TRUE){
                    $wooo_kd = $wo_kd;
                }

                /** Update informasi tgl step1  */
                $this->tm_workorder->update_data(['wo_kd' => $wo_kd], ['wo_tglstep1' => date('Y-m-d H:i:s')]);

                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal proses');
                } else {
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['wo_kd' =>  $wooo_kd]);
                }
            } else {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Kode Kosong');
            }
        }

        echo json_encode($resp);
    }

    public function action_process_generate_qtywo()
    {
        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('wo_kd', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrItemStatus' => (!empty(form_error('wo_kd'))) ? buildLabel('warning', form_error('wo_kd', '"', '"')) : '',
            );
        } else {
            $wo_kd = $this->input->post('wo_kd');
            $stts = $this->input->post('stts');
            if (!empty($wo_kd)) {
                $this->db->trans_start();

                $actPacking = $this->td_workorder_item_so->updateBatch_generateQtyWO_2($wo_kd, $stts);

                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal proses', 'data' => 'GGL');
                } else {
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => $actPacking);
                }
            } else {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Kode Kosong');
            }
        }
        //header('Content-Type: application/json; charset=utf-8');

        echo json_encode($resp);
    }

    /** 
     * 1. temp_process_wo_part_step2
     * 2. temp_process_generate_no_wo
     * 3. td_workorder_item_so->update_prosesstatus_batch
     * 4. Act update wo status
     * 5. aact insert wip
     */
    public function action_process_wo_step2()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $wo_kd = $this->input->get('wo_kd', true);
        if (!empty($wo_kd)) {
            $this->db->trans_start();
            // act step 2
            $actStep2 = $this->td_workorder_item_detail->insertBatch_woPart_step2($wo_kd);
            if ($actStep2) {
                /** Generate WO Number */
                $this->td_workorder_item->updateBatch_generateNoWO($wo_kd);
                /** Generate WIP */
                $this->temp_process_insert_wip($wo_kd, 'in');
                /** Act Update woitemso proses status */
                $this->td_workorder_item_so->updateBatch_prosesStatus($wo_kd, 'workorder');
                /** Act update wo status */
                $this->tm_workorder->update_data(['wo_kd' => $wo_kd], ['wo_status' => 'process_wo', 'wo_tgledit' => date('Y-m-d H:i:s'), 'admin_kd' => $this->session->userdata('kd_admin')]);
            }

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal proses');
            } else {
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['wo_kd' => $wo_kd]);
            }
        } else {
            $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Kode Kosong');
        }
        header('Content-Type: application/json');
        echo json_encode($resp);
    }

    public function action_insert_woitempyr()
    {
     
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $jenis_item = $this->input->post('jenis_item', true);

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('woitem_kd', 'WO', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('qty_pyr', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
        if($jenis_item == 'rawmaterial'){
            /** Validasi inspection rawmaterial harus diisi jika pilih WO rawmaterial */
            $this->form_validation->set_rules('txtInspectionRawMaterial', 'Inspection Raw Material', 'required', ['required' => '{field} tidak boleh kosong!']);
        }

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrwoitem_no_wo' => (!empty(form_error('woitem_kd'))) ? buildLabel('warning', form_error('woitem_kd', '"', '"')) : '',
                'idErrqty_pyr' => (!empty(form_error('qty_pyr'))) ? buildLabel('warning', form_error('qty_pyr', '"', '"')) : '',
            );
            if($jenis_item == 'rawmaterial'){
                /** Validasi inspection rawmaterial harus diisi jika pilih WO rawmaterial */
                $resp['pesan'] = [
                    'idErrInspectionRawMaterial' => (!empty(form_error('txtInspectionRawMaterial'))) ? buildLabel('warning', form_error('txtInspectionRawMaterial', '"', '"')) : ''
                ];
                // array_push($resp['pesan'],['idErrInspectionRawMaterial' => (!empty(form_error('txtInspectionRawMaterial'))) ? buildLabel('warning', form_error('txtInspectionRawMaterial', '"', '"')) : '']);
            }
        } else {
            /** Iniate jika input berdasarkan inspection atau WO (menu form add pyr) */
            $refference = $this->input->post('txtRefference', true);
            /** Jika refference empty inputnya dari WO (bukan dari menu form add pyr) */
            if( empty($refference) ){
                $refference = 'wo';
            }
            $inspectionproduct_kd = $this->input->post('inspectionproduct_kd', true);

            $wo_kd = $this->input->post('txtwo_kd', true);
            $woitem_kd = $this->input->post('woitem_kd', true);
            $woitem_qty = $this->input->post('woitem_qty', true);
            $qty_pyr = $this->input->post('qty_pyr', true);
            $woitem_satuan = $this->input->post('txtwoitem_satuan', true);
            $woitem_tglselesai = $this->input->post('txtwoitem_tglselesai', true);
            $woitem_tglselesai = $this->input->post('txtwoitem_tglselesai', true);
            $woitem_note = $this->input->post('txtwoitem_note', true);
            $woitem_status = $this->input->post('woitem_status', true);
            $woitemdetail_kds = $this->input->post('txtwoitemdetail_kds', true);

            $woitemReject = $this->td_workorder_item->get_by_param(['woitem_kd' => $woitem_kd])->row_array();

            $woitem_kdpyr = $this->td_workorder_item->buat_kode('');
            $woitem_no_wopyr = $this->td_workorder_item->buat_no_wo($woitem_status, '');
            $arrayWorkorderitemNPyr = [
                'woitem_kd' => $woitem_kdpyr,
                'wo_kd' => $wo_kd,
                'woitem_itemcode' => $woitemReject['woitem_itemcode'],
                'woitem_no_wo' => $woitem_no_wopyr,
                'woitem_qty' => $qty_pyr,
                'woitem_jenis' => $woitemReject['woitem_jenis'],
                'woitem_note' => !empty($woitem_note) ? $woitem_note : null,
                'woitem_satuan' => $woitem_satuan,
                'woitem_status' => $woitemReject['woitem_status'],
                'woitem_tipe' => $woitemReject['woitem_tipe'],
                'woitem_tglselesai' => format_date($woitem_tglselesai, 'Y-m-d'),
                'woitem_prosesstatus' => 'workorder',
                'woitem_tglinput' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            ];

            $woitemdetailReject = $this->td_workorder_item_detail->get_by_param(['woitem_kd' => $woitem_kd])->result_array();
            $arrayWorkorderitemdetail = [];
            $woitemdetail_kdpyr = '';
            foreach ($woitemdetailReject as $r_woitemdetailReject) {
                $woitemdetail_kdpyr = $this->td_workorder_item_detail->buat_kode($woitemdetail_kdpyr);
                $arrayWorkorderitemdetail[] = [
                    'woitemdetail_kd' => $woitemdetail_kdpyr,
                    'wo_kd' => $wo_kd,
                    'woitem_kd' => $woitem_kdpyr,
                    'woitemso_kd' => $r_woitemdetailReject['woitemso_kd'],
                    'part_kd' => $r_woitemdetailReject['part_kd'],
                    'partjenis_kd' => $r_woitemdetailReject['partjenis_kd'],
                    'bom_kd' => $r_woitemdetailReject['bom_kd'],
                    'woitemdetail_qty' => $qty_pyr,
                    'woitemdetail_generatewo' => $r_woitemdetailReject['woitemdetail_generatewo'],
                    'woitemdetail_wogrouping' => $r_woitemdetailReject['woitemdetail_wogrouping'],
                    'woitemdetail_parent' => null,
                    'woitemdetail_tglinput' => date('Y-m-d H:i:s'),
                    'admin_kd' => $this->session->userdata('kd_admin'),
                ];
            }

            /** WO item detail child for WOpacking */
            if (!empty($woitemdetail_kds)) {
                $woitemdetail_parent = $woitemdetail_kdpyr;
                $woitemdetailChilds = $this->td_workorder_item_detail->get_where_in('woitemdetail_kd', $woitemdetail_kds)->result_array();
                foreach ($woitemdetailChilds as $woitemdetailChild) {
                    $woitemdetail_kdpyr = $this->td_workorder_item_detail->buat_kode($woitemdetail_kdpyr);
                    $arrayWorkorderitemdetail[] = [
                        'woitemdetail_kd' => $woitemdetail_kdpyr,
                        'wo_kd' => $wo_kd,
                        'woitem_kd' => null,
                        'woitemso_kd' => $woitemdetailChild['woitemso_kd'],
                        'part_kd' => $woitemdetailChild['part_kd'],
                        'partjenis_kd' => $woitemdetailChild['partjenis_kd'],
                        'bom_kd' => $woitemdetailChild['bom_kd'],
                        'woitemdetail_qty' => $qty_pyr,
                        'woitemdetail_generatewo' => $woitemdetailChild['woitemdetail_generatewo'],
                        'woitemdetail_wogrouping' => $woitemdetailChild['woitemdetail_wogrouping'],
                        'woitemdetail_parent' => $woitemdetail_parent,
                        'woitemdetail_tglinput' => date('Y-m-d H:i:s'),
                        'admin_kd' => $this->session->userdata('kd_admin'),
                    ];
                }
            }

            $woitempyr_kd = $this->td_workorder_item_pyr->create_code();
            $arrayWoitemPyr = [
                'woitempyr_kd' => $woitempyr_kd,
                'woitem_kd' => $woitem_kdpyr,
                'woitempyr_qty' => $qty_pyr,
                'woitemreject_kd' => $woitem_kd,
                'inspectionproduct_kd' => $refference == 'inspection' ? $inspectionproduct_kd : null,
                'woitempyr_tglinput' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            ];

            if ($qty_pyr <= $woitem_qty) {
                $this->db->trans_begin();

                $actWOitem = $this->td_workorder_item->insert_data($arrayWorkorderitemNPyr);
                $actWOitemdetail = $this->td_workorder_item_detail->insert_batch_data($arrayWorkorderitemdetail);
                $actWOitempyr = $this->td_workorder_item_pyr->insert_data($arrayWoitemPyr);
                /** Jika item code yang di input rawmaterial */
                if($jenis_item == 'rawmaterial'){
                    $actFlagPyr = $this->tm_inspection_rawmaterial->update(['flag_pyr' => '1'], $inspectionproduct_kd);
                }

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
                } else {
                    $this->db->trans_commit();
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
                }
            } else {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Qty Pyr lebih besar');
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_update_woitem()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);

        $this->form_validation->set_rules('woitem_kd', 'WO', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrwoitem_no_wo' => (!empty(form_error('woitem_kd'))) ? buildLabel('warning', form_error('woitem_kd', '"', '"')) : '',
            );
        } else {
            $woitem_kd = $this->input->post('woitem_kd', true);
            $woitem_satuan = $this->input->post('txtwoitem_satuan', true);
            $woitem_tglselesai = $this->input->post('txtwoitem_tglselesai', true);
            $woitem_note = $this->input->post('txtwoitem_note', true);
            $woitem_deskripsi = $this->input->post('txtwoitem_deskripsi', true);
            $woitem_dimensi = $this->input->post('txtwoitem_dimensi', true);

            $arrayWorkorderitem = [
                'woitem_satuan' => $woitem_satuan,
                'woitem_tglselesai' => format_date($woitem_tglselesai, 'Y-m-d'),
                'woitem_note' => !empty($woitem_note) ? $woitem_note : null,
                'woitem_deskripsi' => !empty($woitem_deskripsi) ? $woitem_deskripsi : null,
                'woitem_dimensi' => !empty($woitem_dimensi) ? $woitem_dimensi : null,
                'woitem_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            ];

            $act = $this->td_workorder_item->update_data(['woitem_kd' => $woitem_kd], $arrayWorkorderitem);
            if ($act) :
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
            else :
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Update');
            endif;
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_cancel_wo()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $wo_kd = $this->input->get('id', true);

        $dataWO = [
            'wo_status' => 'cancel',
            'wo_tgledit' => date('Y-m-d H:i:s'),
            'admin_kd' => $this->session->userdata('kd_admin'),
        ];

        $kd_msalesorders = [];
        $salesorders = $this->tb_relasi_sowo->get_by_param(['wo_kd' => $wo_kd])->result();
        foreach ($salesorders as $salesorder) :
            $kd_msalesorders[] = $salesorder->kd_msalesorder;
        endforeach;

        $actSO = true;
        $actWO = $this->tm_workorder->update_data(['wo_kd' => $wo_kd], $dataWO);
        if (!empty($kd_msalesorders)) :
            $actSO = $this->action_update_status_so($kd_msalesorders, 'process_lpo');
        endif;

        if ($actWO && $actSO) {
            $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
        } else {
            $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Update');
        }

        echo json_encode($resp);
    }

    public function action_cancel_woitem()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $woitem_kd = $this->input->get('id', true);

        $arrayWoitem = [
            'woitem_prosesstatus' => 'cancel',
            'woitem_tgledit' => date('Y-m-d H:i:s'),
            'admin_kd' => $this->session->userdata('kd_admin'),
        ];

        $actWoitem = $this->td_workorder_item->update_data(['woitem_kd' => $woitem_kd], $arrayWoitem);
        if ($actWoitem) {
            $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
        } else {
            $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Update');
        }

        echo json_encode($resp);
    }

    private function action_update_status_so($kd_msalesorders = [], $status)
    {
        $dataUpdateSO = [
            'status_so' => $status,
            'tgl_edit' => date('Y-m-d H:i:s'),
            'admin_kd' => $this->session->userdata('kd_admin'),
        ];

        $act = $this->tm_salesorder->update_wherein($kd_msalesorders, $dataUpdateSO);

        return $act;
    }

    /** -------------------------------------------------------------------------------- */
    private function temp_process_insert_wip($wo_kd, $stsTransaksiWip): bool
    {
        $this->load->model(['tm_wip']);
        $woitemparts = $this->td_workorder_item_so->get_detail_part(['td_workorder_item_so.wo_kd' => $wo_kd])->result();
        /** Group by kdbarang dan item std */
        $arrayGroupStd = [];
        $groupResult = [];
        $woitemsos = [];
        foreach ($woitemparts as $woitempart) {
            /** Cek group by woitemso */
            if (in_array($woitempart->woitemso_kd, $woitemsos)) {
                continue;
            }
            if ($woitempart->woitemso_prosesstatus == 'editing') {
                $woitemsos[] = $woitempart->woitemso_kd;
                $key = $woitempart->kd_barang;
                if (!array_key_exists($key, $arrayGroupStd)) {
                    $arrayGroupStd[$key] = array(
                        'kd_barang' => $woitempart->kd_barang,
                        'woitemso_itemcode' => $woitempart->woitemso_itemcode,
                        'woitemso_qty' => $woitempart->woitemso_qty,
                    );
                } else {
                    $arrayGroupStd[$key]['woitemso_qty'] = $arrayGroupStd[$key]['woitemso_qty'] + $woitempart->woitemso_qty;
                }
            }
        }
        foreach ($arrayGroupStd as $eachGroupStd) {
            $groupResult[] = [
                'kd_barang' => $eachGroupStd['kd_barang'],
                'qty' => $eachGroupStd['woitemso_qty']
            ];
        }
        $actWIP = $this->tm_wip->change_wip_stock($groupResult, $stsTransaksiWip);

        return $actWIP;
    }
}
