<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Wo_list extends MY_Controller
{
    private $class_link = 'menu_ppic/work_orders/wo_list';

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('ssp', 'form_validation'));
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
        $this->load->model(array(
            'tm_workorder', 'td_workorder_item_detail', 'td_workorder_item',
            'td_workorder_item_so', 'td_workorder_item_so_detail', 'td_deliverynote_received', 'td_workorder_item_pyr', 'tm_wip', 'Td_materialreceipt_detail', 'tm_materialreceipt'
        ));
    }

    public function index()
    {
        parent::administrator();
        parent::pnotify_assets();

        #opsi bulan
        $months = bulanIndo();
        $months = array_flip($months);
        $months['ALL'] = 'ALL';
        $data['months'] = $months;

        // opsi woitem_status
        $data['woitem_prosesstatuss'] = ['ALL' => 'ALL', 'workorder' => 'Workorder', 'finish' => 'Finish'];

        // bulan selected
        $month =  $this->input->get('m');
        $data['month'] = !empty($month) ? $month : date('m');
        // tahun selected
        $year =  $this->input->get('Y');
        $data['year'] = !empty($year) ? $year : date('Y');
        // woitem_status selected
        $woitem_prosesstatus =  $this->input->get('woitem_prosesstatus');
        $data['woitem_prosesstatus'] = !empty($woitem_prosesstatus) ? $woitem_prosesstatus : 'ALL';


        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function table_main()
    {
        $month =  $this->input->get('m');
        $year =  $this->input->get('Y');
        $woitem_prosesstatus =  $this->input->get('woitem_prosesstatus');

        $month = !empty($month) ? $month : date('m');
        $year = !empty($year) ? $year : date('Y');
        $woitem_prosesstatus = !empty($woitem_prosesstatus) ? $woitem_prosesstatus : 'ALL';
        $woitems = $this->db->select('`td_workorder_item`.*,GROUP_CONCAT(DISTINCT tm_materialreceipt.materialreceipt_no SEPARATOR \', \') AS mrc_no, tm_workorder.wo_noterbit')
            ->join('td_materialreceipt_detail', 'td_materialreceipt_detail.woitem_kd=td_workorder_item.woitem_kd', 'left')
            ->join('tm_materialreceipt', 'tm_materialreceipt.materialreceipt_kd=td_materialreceipt_detail.materialreceipt_kd', 'left')
            ->join('tm_workorder', 'tm_workorder.wo_kd=td_workorder_item.wo_kd', 'left')
            ->where('td_workorder_item.woitem_no_wo IS NOT NULL')
            ->where('YEAR(tm_workorder.wo_tanggal)', $year);
        if ($month != 'ALL') {
            $woitems = $woitems->where('MONTH(tm_workorder.wo_tanggal)', $month);
        }
        if ($woitem_prosesstatus != 'ALL') {
            $woitems = $woitems->where('td_workorder_item.woitem_prosesstatus', $woitem_prosesstatus);
        }
        $woitems = $woitems->order_by('td_workorder_item.woitem_no_wo');
        $woitems = $woitems->group_by('woitem_no_wo')
            ->get('td_workorder_item');

        $data['woitems'] = $woitems->result_array();
        $data['class_link'] = $this->class_link;

        // header('Content-Type: application/json');
        // echo json_encode($data);
        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }

    public function historywoitem_table()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $woitem_kd = $this->input->get('woitem_kd');

        $data['master'] = $this->db->where('td_workorder_item.woitem_kd', $woitem_kd)
            ->join('tm_workorder', 'tm_workorder.wo_kd=td_workorder_item.wo_kd', 'left')
            ->get('td_workorder_item')->row_array();
        $data['result'] = $this->td_deliverynote_received->get_by_param_detail(['td_deliverynote_received.woitem_kd' => $woitem_kd])->result_array();
        $data['class_link'] = $this->class_link;

        /** Calc WO position */
        $arrBagianCalc = [];
        $qDnReceived = $this->td_deliverynote_received->get_by_param_detail(['td_deliverynote_received.woitem_kd' => $woitem_kd])->result_array();
        if (!empty($qDnReceived)) {
            ${"s{$qDnReceived[0]['bagian_asal']}"} = $data['master']['woitem_qty'];
            foreach ($qDnReceived as $dt) {
                isset(${"s{$dt['bagian_asal']}"}) ?: ${"s{$dt['bagian_asal']}"} = 0;
                isset(${"s{$dt['bagian_tujuan']}"}) ?: ${"s{$dt['bagian_tujuan']}"} = 0;
    
                ${"s{$dt['bagian_asal']}"} = ${"s{$dt['bagian_asal']}"} - $dt['dndetailreceived_qty'];
                ${"s{$dt['bagian_tujuan']}"} = ${"s{$dt['bagian_tujuan']}"} + $dt['dndetailreceived_qty'];
            }
            $bagians = array_values(array_unique(array_merge(array_column($qDnReceived, 'bagian_asal'), array_column($qDnReceived, 'bagian_tujuan'))));
            for ($i = 0; $i < count($bagians); $i++) {
                $arrBagianCalc[] = ['bagian_nm' => $bagians[$i], 'qty' => ${"s$bagians[$i]"}];
            }
        }
        
        $data['bagianQtys'] = $arrBagianCalc;
        
        /** PYR History */

        $data['pyrdatas'] = $this->td_workorder_item_pyr->get_woreject_bywoitem($woitem_kd);
// echo json_encode($data['pyrdatas']);die();
        $this->load->view('page/' . $this->class_link . '/historywoitem_table', $data);
    }

    public function ubah_status()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $woitem_kd = $this->input->get('woitem_kd');
        $status = $this->input->get('status');
        $data = [
            'woitem_prosesstatus' => $status,
            'woitem_tgledit' => date('Y-m-d H:i:s'),
            'admin_kd' => $this->session->userdata('kd_admin'),
        ];
       
        $updateData = $this->td_workorder_item->update_data(['woitem_kd' => $woitem_kd], $data);
        //$updateData =  true;
        
        $alo = $this->db->where('td_workorder_item.woitem_kd', $woitem_kd)
        ->join('tm_barang', 'td_workorder_item.woitem_itemcode=tm_barang.item_code', 'left')
        ->get('td_workorder_item')->result_array();

        $wip = $this->tm_wip->get_by_param(["kd_barang" => $alo[0]['kd_barang']])->result_array();

        $itm_upd = [
            'wip_qty' => (int) $wip[0]['wip_qty'] - $alo[0]['woitem_qty'],
            'wip_tgledit' => date('Y-m-d H:i:s'),
            'admin_kd' => $this->session->userdata('kd_admin'),
        ];
        $upd_wip = $this->tm_wip->update_data(['wip_kd' => $wip[0]['wip_kd']], $itm_upd);

       
        if ($updateData) {
            $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Berhasil mengubah status', 'data' => $itm_upd);  
        } else {
            $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Ubah Status');
        }

        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);

    }

}
