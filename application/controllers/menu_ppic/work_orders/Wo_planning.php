<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Wo_planning extends MY_Controller
{
    private $class_link = 'menu_ppic/work_orders/wo_planning';

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('ssp', 'form_validation'));
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
        $this->load->model(array());
    }

    public function index()
    {
        parent::administrator();
        parent::pnotify_assets();

        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }
}
