<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Outstand_wo extends MY_Controller
{
    private $class_link = 'menu_ppic/work_orders/outstand_wo';

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('ssp', 'form_validation'));
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
        $this->load->model(array('tm_wip', 'tb_bagian', 'Td_rawmaterial_stok_master', 'Tm_purchaseorder', 'Tm_purchaserequisition','Tm_workorder'));
    }

    public function index()
    {
        parent::administrator();
        parent::pnotify_assets();
        
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function table_main()
    {        
        // if (!$this->input->is_ajax_request()) {
        //     exit('No direct script access allowed');
        // }
        $range =  $this->input->get('range');
        /** Bagian */
        // $data['bagians'] = $this->db->order_by('bagian_kd')->get('tb_bagian')->result_array();
        $sql="SELECT rm_kd, rm_kode, rm_nama, rm_deskripsi, SUM(ROUND(all_material_by_woitem, 1)) AS all_material_by_woitem, SUM(ROUND(MRC, 1)) AS MRC, SUM(ROUND(Final_outstanding, 1)) AS Final_outstanding  FROM(

            SELECT rm_kd, rm_kode, rm_nama, rm_deskripsi, SUM(ROUND((woitem_qty * partdetail_qty), 1)) AS all_material_by_woitem, outstanding AS MRC, (SUM(ROUND((woitem_qty * partdetail_qty), 1))- outstanding ) AS Final_outstanding FROM (SELECT * FROM 
                    (SELECT 
                            tdw.woitem_kd,
                            tmr.rm_kode,
                            tmr.rm_kd,
                            tdw.woitem_qty, 
                            tdpd.partdetail_qty, 
                            tmr.rm_spesifikasi as rm_nama,
                            tdw.woitem_tglinput,
                            tmr.rm_deskripsi, 
                            tmb.item_code,
                            tdw.woitem_prosesstatus,
                            (SELECT need FROM 
                                (SELECT tdmdr.rm_kd, ROUND(SUM(tdmdr.materialreceiptdetailrm_qty), 1) AS need
                                        FROM (SELECT * FROM td_workorder_item) AS tdw
                                        INNER JOIN td_materialreceipt_detail AS tdmd ON tdmd.woitem_kd = tdw.woitem_kd
                                        INNER JOIN td_materialreceipt_detail_rawmaterial AS tdmdr ON tdmdr.materialreceiptdetail_kd = tdmd.materialreceiptdetail_kd
                                        INNER JOIN tm_rawmaterial AS tmr ON tmr.rm_kd = tdmdr.rm_kd WHERE tdw.woitem_prosesstatus = 'workorder' GROUP BY tdmdr.rm_kd
                            ) mrc_detail WHERE rm_kd = tmr.rm_kd ) AS outstanding
 
                            FROM (SELECT * FROM td_workorder_item) AS tdw 
                            INNER JOIN td_workorder_item_detail AS tdwd ON tdw.woitem_kd = tdwd.woitem_kd
                            INNER JOIN tm_barang AS tmb ON tmb.item_code = tdw.woitem_itemcode  
                            INNER JOIN tm_bom AS tmbo ON tmbo.kd_barang = tmb.kd_barang
                            INNER JOIN (SELECT * FROM td_bom_detail WHERE bomdetail_generatewo = 'F') AS tdbo ON tdbo.bom_kd = tmbo.bom_kd
                            INNER JOIN tm_part_main AS tmp ON tdbo.partmain_kd = tmp.partmain_kd
                            INNER JOIN tb_part_lastversion ON tmp.partmain_kd = tb_part_lastversion.partmain_kd
                            INNER JOIN td_part AS tdp ON tdp.part_kd = tb_part_lastversion.part_kd
                            INNER JOIN td_part_detail AS tdpd ON tdp.part_kd = tdpd.part_kd
                            INNER JOIN tm_rawmaterial AS tmr ON tmr.rm_kd = tdpd.rm_kd) outstand_wo
            
                            WHERE item_code IN (SELECT
                                                tmb.item_code
                                                FROM tm_workorder tmw
                                                INNER JOIN (SELECT * FROM td_workorder_item WHERE woitem_no_wo NOT LIKE 'P%') AS tdw ON tdw.wo_kd = tmw.wo_kd
                                                INNER JOIN td_workorder_item_detail AS tdwd ON tdw.woitem_kd = tdwd.woitem_kd
                                                INNER JOIN tm_barang AS tmb ON tmb.kd_barang = tdwd.kd_barang
                                                WHERE tdw.woitem_prosesstatus = 'workorder' AND tdw.woitem_tglinput >= now()- interval $range month GROUP BY item_code) AND woitem_prosesstatus = 'workorder' AND woitem_tglinput >= now()- interval $range month) AS outstand
             GROUP BY rm_kd
            
            UNION
            
            SELECT rm_kd, rm_kode, rm_nama, rm_deskripsi, SUM(ROUND((woitem_qty * partdetail_qty), 1)) AS all_material_by_woitem, outstanding AS MRC, (SUM(ROUND((woitem_qty * partdetail_qty), 1))- outstanding ) AS Final_outstanding  FROM 
                    (SELECT tdw.woitem_kd,
                            tmr.rm_kode,
                            tmr.rm_kd,
                            tdw.woitem_qty, 
                            tdpd.partdetail_qty, 
                            tmr.rm_spesifikasi as rm_nama,
                            tmw.wo_tglinput,
                            tmr.rm_deskripsi, 
                            ROUND((tdw.woitem_qty * tdpd.partdetail_qty), 1) AS many_needed,
                            
                            (SELECT need FROM 
                                (SELECT tdmdr.rm_kd, ROUND(SUM(tdmdr.materialreceiptdetailrm_qty), 1) AS need
                                        FROM (SELECT * FROM td_workorder_item) AS tdw
                                        INNER JOIN td_materialreceipt_detail AS tdmd ON tdmd.woitem_kd = tdw.woitem_kd
                                        INNER JOIN td_materialreceipt_detail_rawmaterial AS tdmdr ON tdmdr.materialreceiptdetail_kd = tdmd.materialreceiptdetail_kd
                                        INNER JOIN tm_rawmaterial AS tmr ON tmr.rm_kd = tdmdr.rm_kd WHERE tdw.woitem_prosesstatus = 'workorder' GROUP BY tdmdr.rm_kd
                            ) mrc_detail WHERE rm_kd = tmr.rm_kd ) AS outstanding
                
                
                
                            FROM tm_workorder tmw
                            INNER JOIN (SELECT * FROM td_workorder_item) AS tdw ON tdw.wo_kd = tmw.wo_kd
                            INNER JOIN td_workorder_item_detail AS tdwd ON tdw.woitem_kd = tdwd.woitem_kd
                            INNER JOIN tm_barang AS tmb ON tmb.item_code = tdw.woitem_itemcode
                            INNER JOIN tm_bom AS tmbo ON tmbo.kd_barang = tmb.kd_barang
                            INNER JOIN td_bom_detail AS tdbo ON tdbo.bom_kd = tmbo.bom_kd
                            INNER JOIN tm_part_main AS tmp ON tdbo.partmain_kd = tmp.partmain_kd
                            INNER JOIN tb_part_lastversion ON tmp.partmain_kd = tb_part_lastversion.partmain_kd
                            INNER JOIN td_part AS tdp ON tdp.part_kd = tb_part_lastversion.part_kd
                            INNER JOIN td_part_detail AS tdpd ON tdp.part_kd = tdpd.part_kd
                            INNER JOIN tm_rawmaterial AS tmr ON tmr.rm_kd = tdpd.rm_kd
                            WHERE tdw.woitem_prosesstatus = 'workorder' AND tmw.wo_tglinput >= now()-interval $range month GROUP BY tdw.woitem_kd, tmr.rm_kd) outstand_wo GROUP BY rm_kd
            ) AS REAL_OUTSTAND GROUP BY rm_kd
            
                                    
    ";    

        $query = $this->db->query($sql);
        $outstand_mrc1 =  $query->result_array();

        $sql2 = "SELECT   
                    rm_kd,   
                    rm_kode,   
                    rm_nama,   
                    rm_deskripsi,   
                    SUM(ROUND(all_material_by_woitem, 1)) AS all_material_by_woitem,   
                    SUM(ROUND(MRC, 1)) AS MRC,   
                    SUM(ROUND(Final_outstanding, 1)) AS Final_outstanding  
                FROM (  
                    SELECT   
                        rm_kd,   
                        rm_kode,   
                        rm_nama,   
                        rm_deskripsi,   
                        SUM(ROUND((woitem_qty * partdetail_qty), 1)) AS all_material_by_woitem,   
                        outstanding AS MRC,   
                        (SUM(ROUND((woitem_qty * partdetail_qty), 1)) - outstanding) AS Final_outstanding   
                    FROM (  
                        SELECT   
                            tdw.woitem_kd,  
                            tmr.rm_kode,  
                            tmr.rm_kd,  
                            tdw.woitem_qty,   
                            tdpd.partdetail_qty,   
                            tmr.rm_spesifikasi AS rm_nama,  
                            tdw.woitem_tglinput,  
                            tmr.rm_deskripsi,   
                            tmb.item_code,  
                            tdw.woitem_prosesstatus,  
                            (SELECT need FROM (  
                                SELECT   
                                    tdmdr.rm_kd,   
                                    ROUND(SUM(tdmdr.materialreceiptdetailrm_qty), 1) AS need  
                                FROM   
                                    td_workorder_item AS tdw  
                                INNER JOIN   
                                    td_materialreceipt_detail AS tdmd ON tdmd.woitem_kd = tdw.woitem_kd  
                                INNER JOIN   
                                    td_materialreceipt_detail_rawmaterial AS tdmdr ON tdmdr.materialreceiptdetail_kd = tdmd.materialreceiptdetail_kd  
                                                        INNER JOIN (select tm_bom.kd_barang, tm_rawmaterial.*, td_part_detail.partdetail_qty FROM tm_bom 
                            LEFT JOIN td_bom_detail ON tm_bom.bom_kd = td_bom_detail.bom_kd
                            LEFT JOIN tm_part_main ON tm_part_main.partmain_kd = td_bom_detail.partmain_kd
                            LEFT JOIN (SELECT td_part.partmain_kd, MAX(td_part.part_versi), MAX(td_part.part_kd) as xxi FROM td_part GROUP BY td_part.partmain_kd) AS cust_part ON tm_part_main.partmain_kd = cust_part.partmain_kd
                            LEFT JOIN td_part_detail ON td_part_detail.part_kd = cust_part.xxi
                            LEFT JOIN tm_rawmaterial ON tm_rawmaterial.rm_kd = td_part_detail.rm_kd WHERE tm_bom.kd_barang LIKE '%RM%') AS tmr ON tmr.kd_barang=tdmdr.rm_kd
                                WHERE   
                                    tdw.woitem_prosesstatus = 'workorder'   
                                GROUP BY   
                                    tdmdr.rm_kd  
                            ) mrc_detail WHERE rm_kd = tmr.rm_kd) AS outstanding  
                        FROM   
                            td_workorder_item AS tdw   
                        INNER JOIN   
                            td_workorder_item_detail AS tdwd ON tdw.woitem_kd = tdwd.woitem_kd  
                        INNER JOIN   
                            tm_barang AS tmb ON tmb.item_code = tdw.woitem_itemcode  
                        INNER JOIN   
                            tm_bom AS tmbo ON tmbo.kd_barang = tmb.kd_barang  
                        INNER JOIN   
                            (SELECT * FROM td_bom_detail WHERE bomdetail_generatewo = 'F') AS tdbo ON tdbo.bom_kd = tmbo.bom_kd  
                        INNER JOIN   
                            tm_part_main AS tmp ON tdbo.partmain_kd = tmp.partmain_kd  
                        INNER JOIN   
                            tb_part_lastversion ON tmp.partmain_kd = tb_part_lastversion.partmain_kd  
                        INNER JOIN   
                            td_part AS tdp ON tdp.part_kd = tb_part_lastversion.part_kd  
                        INNER JOIN   
                            td_part_detail AS tdpd ON tdp.part_kd = tdpd.part_kd  
                                        
                            INNER JOIN (select tm_bom.kd_barang, tm_rawmaterial.*, td_part_detail.partdetail_qty FROM tm_bom 
                            LEFT JOIN td_bom_detail ON tm_bom.bom_kd = td_bom_detail.bom_kd
                            LEFT JOIN tm_part_main ON tm_part_main.partmain_kd = td_bom_detail.partmain_kd
                            LEFT JOIN (SELECT td_part.partmain_kd, MAX(td_part.part_versi), MAX(td_part.part_kd) as xxi FROM td_part GROUP BY td_part.partmain_kd) AS cust_part ON tm_part_main.partmain_kd = cust_part.partmain_kd
                            LEFT JOIN td_part_detail ON td_part_detail.part_kd = cust_part.xxi
                            LEFT JOIN tm_rawmaterial ON tm_rawmaterial.rm_kd = td_part_detail.rm_kd WHERE tm_bom.kd_barang LIKE '%RM%') AS tmr ON tmr.kd_barang=tdpd.rm_kd
                        WHERE   
                            item_code IN (  
                                SELECT  
                                    tmb.item_code  
                                FROM   
                                    tm_workorder AS tmw  
                                INNER JOIN   
                                    (SELECT * FROM td_workorder_item WHERE woitem_no_wo NOT LIKE 'P%') AS tdw ON tdw.wo_kd = tmw.wo_kd  
                                INNER JOIN   
                                    td_workorder_item_detail AS tdwd ON tdw.woitem_kd = tdwd.woitem_kd  
                                INNER JOIN   
                                    tm_barang AS tmb ON tmb.kd_barang = tdwd.kd_barang  
                                WHERE   
                                    tdw.woitem_prosesstatus = 'workorder'   
                                    AND tdw.woitem_tglinput >= NOW() - INTERVAL $range MONTH   
                                GROUP BY   
                                    item_code  
                            )   
                            AND woitem_prosesstatus = 'workorder'   
                            AND woitem_tglinput >= NOW() - INTERVAL $range MONTH  
                    ) AS outstand   
                    GROUP BY   
                        rm_kd  
                ) AS final_results  
                GROUP BY   
                    rm_kd, rm_kode, rm_nama, rm_deskripsi
                ORDER BY rm_kode;";

        $query2 = $this->db->query($sql2);
        $outstand_mrc2 =  $query2->result_array();

        $outstand_mrc = array_merge($outstand_mrc1, $outstand_mrc2);



        $rm_kd = array_column($outstand_mrc, 'rm_kd');
        $rm_kd_qty = $this->Td_rawmaterial_stok_master->getLastStokResult($rm_kd, date("Y-m-d H:i:s"));
        

        //get qty kartu stok
        $new_result = [];

        foreach ($outstand_mrc as $key => $item) {
            $other = array_filter($rm_kd_qty, function ($item2) use ($item) { 
                return $item2['rm_kd'] == $item['rm_kd'];
            });
            if (!empty($other)) {
                     array_push($new_result, (object)[
                            'rm_kd' => $item['rm_kd'],
                            'rm_kode' => $item['rm_kode'],
                            'rm_nama' => $item['rm_nama'],
                            'rm_deskripsi' => $item['rm_deskripsi'],
                            'all_material_by_woitem' => $item['all_material_by_woitem'],
                            'MRC' => $item['MRC'],
                            'Final_outstanding' => $item['Final_outstanding'],
                            'qty_aft_mutasi' => current($other)['qty_aft_mutasi'],
                    ]);
                // $new_result['rm_kd'] = current($other)['rm_kd'];
                // $new_result['qty_aft_mutasi'] = current($other)['qty_aft_mutasi'];
            }
        }

        $po_sggs = $this->Tm_purchaseorder->outstanding_po_purchasesuggest($rm_kd);
        $pr_sggs = $this->Tm_purchaserequisition->outstanding_pr_purchasesuggest($rm_kd);
        $wo_sggs = $this->Tm_workorder->outstanding_wo_suggest($rm_kd);       
        
        $new_result_po = [];
        //get PO suggest
        foreach ($new_result as $key => $item) {
            $other = array_filter($po_sggs['resultPO'], function ($item2) use ($item) { 
                return $item2['rm_kd'] == $item->rm_kd;
            });
           
            if (!empty($other)) {
                     array_push($new_result_po, (object)[
                            'rm_kd' => $item->rm_kd,
                            'rm_kode' => $item->rm_kode,
                            'rm_nama' => $item->rm_nama,
                            'rm_deskripsi' => $item->rm_deskripsi,
                            'all_material_by_woitem' => $item->all_material_by_woitem,
                            'MRC' => $item->MRC,
                            'Final_outstanding' => $item->Final_outstanding,
                            'qty_aft_mutasi' => $item->qty_aft_mutasi,
                            'sisa_po' => current($other)['sisa_po']
                    ]);
                // $new_result['rm_kd'] = current($other)['rm_kd'];
                // $new_result['qty_aft_mutasi'] = current($other)['qty_aft_mutasi'];
            }else{
                array_push($new_result_po, (object)[
                    'rm_kd' => $item->rm_kd,
                            'rm_kode' => $item->rm_kode,
                            'rm_nama' => $item->rm_nama,
                            'rm_deskripsi' => $item->rm_deskripsi,
                            'all_material_by_woitem' => $item->all_material_by_woitem,
                            'MRC' => $item->MRC,
                            'Final_outstanding' => $item->Final_outstanding,
                            'qty_aft_mutasi' => $item->qty_aft_mutasi,
                            'sisa_po' => '0'
                    ]);
            }
        }

        $new_result_pr = [];
        
        //get PR suggest
        foreach ($new_result_po as $key => $item) {
            $sisa_wo = 0;
            $other = array_filter($pr_sggs['resultPR'], function ($item2) use ($item) { 
                return $item2['rm_kd'] == $item->rm_kd;
            });
           
            foreach($wo_sggs as $w){
                // echo $item->rm_kode . '-' . $w['woitem_itemcode'] .'<br>';
                if($item->rm_kode == $w['woitem_itemcode']){
                    // if($w['dn_tujuan'] == '80'){
                    //     $sisa_wo = $w['qty_wo'] - $w['qty_dn'];
                    // }else{
                    //     $sisa_wo = $w['qty_wo'];
                    // }
                    $sisa_wo = $w['sisa_wo']; 
                    break;
                }
            }

            if (!empty($other)) {
                     array_push($new_result_pr, (object)[
                            'rm_kd' => $item->rm_kd,
                            'rm_kode' => $item->rm_kode,
                            'rm_nama' => $item->rm_nama,
                            'rm_deskripsi' => $item->rm_deskripsi,
                            'all_material_by_woitem' => $item->all_material_by_woitem,
                            'MRC' => $item->MRC,
                            'Final_outstanding' => $item->Final_outstanding,
                            'qty_aft_mutasi' => $item->qty_aft_mutasi,
                            'sisa_po' =>  $item->sisa_po,
                            'sisa_pr' => current($other)['sisa_pr'],
                            'sisa_wo' => $sisa_wo
                    ]);
                // $new_result['rm_kd'] = current($other)['rm_kd'];
                // $new_result['qty_aft_mutasi'] = current($other)['qty_aft_mutasi'];
            }else{
                array_push($new_result_pr, (object)[
                    'rm_kd' => $item->rm_kd,
                            'rm_kode' => $item->rm_kode,
                            'rm_nama' => $item->rm_nama,
                            'rm_deskripsi' => $item->rm_deskripsi,
                            'all_material_by_woitem' => $item->all_material_by_woitem,
                            'MRC' => $item->MRC,
                            'Final_outstanding' => $item->Final_outstanding,
                            'qty_aft_mutasi' => $item->qty_aft_mutasi,
                            'sisa_po' =>  $item->sisa_po,
                            'sisa_pr' => '0',
                            'sisa_wo' => $sisa_wo
                    ]);
            }
        }
        
        $data['items'] = $new_result_pr;
        $data['po_sggs'] = $po_sggs['resultPO'];
        $data['pr_sggs'] = $pr_sggs['resultPR'];


        // header('Content-Type: application/json');
        // echo json_encode($data);

        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }
}
