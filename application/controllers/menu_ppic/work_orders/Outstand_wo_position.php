<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Outstand_wo_position extends MY_Controller
{
    private $class_link = 'menu_ppic/work_orders/outstand_wo_position';

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ssp', 'form_validation'));
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
        $this->load->model(array(
            'tm_workorder', 'td_workorder_item_detail', 'td_workorder_item',
            'td_workorder_item_so', 'td_workorder_item_so_detail', 'td_deliverynote_received', 'td_workorder_item_pyr', 'Td_materialreceipt_detail', 'tm_materialreceipt', 'tb_bagian'
        ));
    }

    public function index()
    {
        parent::administrator();
        parent::pnotify_assets();
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function table_main()
    {
        // if (!$this->input->is_ajax_request()) {
        //     exit('No direct script access allowed');
        // }

        $data['master'] = $this->db->where('td_workorder_item.woitem_prosesstatus', 'workorder')
            ->join('tm_workorder', 'tm_workorder.wo_kd=td_workorder_item.wo_kd', 'left')
            ->group_by('td_workorder_item.woitem_no_wo')
            ->order_by('td_workorder_item.woitem_no_wo', 'ASC')
            ->get('td_workorder_item')->result_array();
        $data['class_link'] = $this->class_link;
        /** Declare bagian */
        $data['bagian'] = $this->tb_bagian->get_by_param_in ('bagian_kd', ['10', '20', '30', '40', '50', '60'])->result_array();
        /** Recap master */
        $data['master_recap'] = [];
        foreach($data['master'] as $key => $val){
            /** Calc WO position */
            $arrBagianCalc = [];
            $sql = "SELECT `td_deliverynote_received`.*, SUM(dndetailreceived_qty) as totalx, `bagian_asal`.`bagian_nama` as `bagian_asal`, `bagian_tujuan`.`bagian_nama` as `bagian_tujuan`, `td_workorder_item`.*, `tm_deliverynote`.`dn_no`, `tm_deliverynote`.`dn_asal`, `tm_deliverynote`.`dn_tujuan` FROM `td_deliverynote_received` LEFT JOIN `tm_deliverynote` ON `td_deliverynote_received`.`dn_kd`=`tm_deliverynote`.`dn_kd` LEFT JOIN `td_workorder_item` ON `td_deliverynote_received`.`woitem_kd`=`td_workorder_item`.`woitem_kd` LEFT JOIN `tb_bagian` as `bagian_asal` ON `tm_deliverynote`.`dn_asal`=`bagian_asal`.`bagian_kd` LEFT JOIN `tb_bagian` as `bagian_tujuan` ON `tm_deliverynote`.`dn_tujuan`=`bagian_tujuan`.`bagian_kd` WHERE `td_deliverynote_received`.`woitem_kd` = '".$val['woitem_kd']."' AND `td_workorder_item`.`woitem_prosesstatus` = 'workorder' GROUP BY bagian_asal.bagian_nama, bagian_tujuan.bagian_nama";    
            $query = $this->db->query($sql);
            $qDnReceived =  $query->result_array();
            if (!empty($qDnReceived)) {
                ${"s{$qDnReceived[0]['bagian_asal']}"} = $val['woitem_qty'];
                foreach ($qDnReceived as $dt) {
                    ${"s{$dt['bagian_asal']}"} = 0;
                    ${"s{$dt['bagian_tujuan']}"} = 0;
                    
                    isset(${"s{$dt['bagian_asal']}"}) ?: ${"s{$dt['bagian_asal']}"} = 0;
                    isset(${"s{$dt['bagian_tujuan']}"}) ?: ${"s{$dt['bagian_tujuan']}"} = 0;
        
                    ${"s{$dt['bagian_asal']}"} = abs(${"s{$dt['bagian_asal']}"} - $dt['totalx']);
                    ${"s{$dt['bagian_tujuan']}"} = abs(${"s{$dt['bagian_tujuan']}"} + $dt['totalx']);
                }

                $bagians = array_values(array_unique(array_merge(array_column($qDnReceived, 'bagian_asal'), array_column($qDnReceived, 'bagian_tujuan'))));
                for ($i = 0; $i < count($bagians); $i++) {
                    $arrBagianCalc[$bagians[$i]] = ${"s$bagians[$i]"};
                }
            }

            /** get PYR by woitem_kd */
            $pyr = $this->td_workorder_item_pyr->get_woreject_bywoitem($val['woitem_kd']);
            $qtyPyr = array_sum(array_column($pyr['woitem'], 'woitempyr_qty'));

            $data['master_recap'][$val['woitem_no_wo']]['header'] = $val;
            $data['master_recap'][$val['woitem_no_wo']]['header']['qty_pyr'] = $qtyPyr;
            $data['master_recap'][$val['woitem_no_wo']]['qty_bagian'] = $arrBagianCalc;
            
            $sqlx = "SELECT SUM(td_deliverynote_detail.dndetail_qty) as total_x FROM td_deliverynote_detail
                    INNER JOIN tm_deliverynote ON tm_deliverynote.dn_kd = td_deliverynote_detail.dn_kd
                    WHERE td_deliverynote_detail.woitem_kd = '".$val['woitem_kd']."' AND tm_deliverynote.dn_asal = '60' GROUP BY kd_barang";
            $queryx = $this->db->query($sqlx);
            $qDnReceivedx =  $queryx->result_array();
            $bxc = $qDnReceivedx ? $qDnReceivedx[0]['total_x'] : 0;


            
            $data['master_recap'][$val['woitem_no_wo']]['header']['qty_outstanding'] = $arrBagianCalc['PACKING'] - $bxc - $qtyPyr;
        }
        // $qDnReceived = $this->td_deliverynote_received->get_by_param_detail(['td_deliverynote_received.woitem_kd' => 'DWO241113000051', 'td_workorder_item.woitem_prosesstatus' => 'workorder'])->result_array();
        //   header('Content-Type: application/json');
        //   $data['x1'] = $qDnReceived;
        // echo json_encode($data);
        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }

}
