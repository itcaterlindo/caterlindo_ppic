<?php
defined('BASEPATH') or exit('No direct script access allowed!');
require_once APPPATH . "third_party/PhpSpreadsheets/autoload.php";

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/** copas from report_dn */
class Report_category extends MY_Controller
{
	private $class_link = 'menu_ppic/ppic_report/report_category';

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['tb_bagian', 'tm_project']);
		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper', 'my_helper2']);
	}

	public function index()
	{
		parent::administrator();
		$this->table_box();
	}

	public function table_box()
	{

		// Initiate bulan dan tahun ambil dari database
		$interval = 10;
		$year = date('Y');
		$arrYear = [];
		for ($i = 0; $i < $interval; $i++) {
			array_push($arrYear, $year);
			$year = $year - 1;
		}
		$arrMonth = [];
		for ($i = 1; $i <= 12; $i++) {
			array_push($arrMonth, $i);
		}

		$data['optionYear'] = $arrYear;
		$data['optionMonth'] = $arrMonth;
		$data['class_link'] = $this->class_link;

		$this->load->view('page/' . $this->class_link . '/table_box', $data);
	}

	public function table_main()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$yearAwal = $this->input->get('yearAwal', true);
		$monthAwal = $this->input->get('monthAwal', true);
		$yearAkhir = $this->input->get('yearAkhir', true);
		$monthAkhir = $this->input->get('monthAkhir', true);
		$KonversiDollar = $this->input->get('KonversiDollar', true);

		// echo $yearAkhir;exit(); 	
		$data['result'] = $this->data_report($yearAwal, $monthAwal, $yearAkhir, $monthAkhir, $KonversiDollar);
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/table_main', $data);
	}

	public function data_report($yearAwal, $monthAwal, $yearAkhir, $monthAkhir, $KonversiDollar)
	{
		/** Get kat barang */
		$kat_barang = $this->db->query(
			"SELECT d.kat_barang_kd, e.nm_kat_barang 
			FROM tm_deliveryorder AS a
			LEFT JOIN td_do_item AS b ON a.kd_mdo=b.mdo_kd 
			LEFT JOIN td_salesorder_item AS c ON b.parent_kd=c.kd_ditem_so 
			LEFT JOIN td_salesorder_item_disc AS cc ON c.kd_ditem_so=cc.so_item_kd AND cc.nm_pihak = 'Distributor' 
			LEFT JOIN tm_barang AS d ON c.barang_kd=d.kd_barang
			LEFT JOIN tm_kat_barang AS e ON d.kat_barang_kd =e.kd_kat_barang 
			WHERE a.tgl_do between '" . $yearAwal . "-" . $monthAwal . "-1' AND '" . $yearAkhir . "-" . $monthAkhir . "-31' 
			AND (b.child_kd IS NULL or b.child_kd IS NOT NULL)
			AND a.status_do = 'finish'
			group by kat_barang_kd;"
		)->result_array();

		$reportData = [];
		foreach ($kat_barang as $key => $val) {
			$reportData[$key] = $val;
		}
		// echo json_encode($reportData);exit();

		$dataOriso = $this->db->query(
			"SELECT a.tgl_do, a.tipe_do,  c.barang_kd, c.item_code,d.kat_barang_kd,d.grossweight, c.harga_barang, c.total_harga, sum(b.stuffing_item_qty) as item_qty
			, cc.jml_disc, cc.nm_pihak
			FROM tm_deliveryorder AS a
			LEFT JOIN td_do_item AS b ON a.kd_mdo=b.mdo_kd 
			LEFT JOIN td_salesorder_item AS c ON b.parent_kd=c.kd_ditem_so 
			LEFT JOIN td_salesorder_item_disc AS cc ON c.kd_ditem_so=cc.so_item_kd AND cc.nm_pihak = 'Distributor' 
			LEFT JOIN tm_barang AS d ON c.barang_kd=d.kd_barang
			WHERE a.tgl_do between '" . $yearAwal . "-" . $monthAwal . "-1' AND '" . $yearAkhir . "-" . $monthAkhir . "-31' 
			AND b.child_kd IS NULL 
			AND a.status_do = 'finish'
			group by item_code 
			UNION ALL
			SELECT a.tgl_do, a.tipe_do, c.kd_child AS barang_kd, c.item_code,d.kat_barang_kd,d.grossweight, c.harga_barang,  c.total_harga, sum(b.stuffing_item_qty) as item_qty
			, cc.jml_disc, cc.nm_pihak
			FROM tm_deliveryorder AS a
			LEFT JOIN td_do_item AS b ON a.kd_mdo=b.mdo_kd
			LEFT JOIN td_salesorder_item_detail AS c ON b.child_kd=c.kd_citem_so 
			LEFT JOIN td_salesorder_item_disc AS cc ON c.kd_citem_so=cc.so_item_kd AND cc.nm_pihak = 'Distributor' 
			LEFT JOIN tm_barang AS d ON c.kd_child=d.kd_barang
			WHERE a.tgl_do between '" . $yearAwal . "-" . $monthAwal . "-1' AND '" . $yearAkhir . "-" . $monthAkhir . "-31' 
			AND b.child_kd IS NOT NULL 
			AND a.status_do = 'finish'
			group by item_code "
		)->result();
		// echo json_encode($dataOriso);

		$dataPacking = $this->db->query(
			"SELECT td_deliverynote_detail.kd_barang,tm_barang.item_code as woitem_itemcode,sum(td_deliverynote_detail.dndetail_qty) as sum_dndetail_qty
			FROM td_deliverynote_detail
			LEFT JOIN tm_deliverynote ON tm_deliverynote.dn_kd = td_deliverynote_detail.dn_kd
			LEFT JOIN td_workorder_item ON td_workorder_item.woitem_kd = td_deliverynote_detail.woitem_kd
			LEFT JOIN tb_bagian ON tb_bagian.bagian_kd = tm_deliverynote.dn_asal 
			LEFT JOIN tm_barang ON tm_barang.kd_barang = td_deliverynote_detail.kd_barang
			LEFT JOIN tm_rawmaterial ON tm_rawmaterial.rm_kd = td_deliverynote_detail.kd_barang
			WHERE tm_deliverynote.dn_tanggal between '" . $yearAwal . "-" . $monthAwal . "-1' AND '" . $yearAkhir . "-" . $monthAkhir . "-31'  AND tm_deliverynote.dn_asal IN ('60') AND td_workorder_item.woitem_jenis = 'packing'
			AND tm_barang.kat_barang_kd NOT IN ('KTB020817000046','KTB020817000047') OR (left(td_deliverynote_detail.kd_barang,2) = 'RM' AND left(td_workorder_item.woitem_itemcode,4) = 'SINK' AND tm_deliverynote.dn_tanggal between '" . $yearAwal . "-" . $monthAwal . "-1' AND '" . $yearAkhir . "-" . $monthAkhir . "-31') 
			GROUP BY td_deliverynote_detail.kd_barang"
		)->result();
		// echo json_encode($dataPacking);

		/** Get data part khusus */
		$dataPartTop = $this->db->query(
			"SELECT td_deliverynote_detail.kd_barang,tm_barang.item_code as woitem_itemcode,sum(td_deliverynote_detail.dndetail_qty) as sum_dndetail_qty
			FROM td_deliverynote_detail
			LEFT JOIN tm_deliverynote ON tm_deliverynote.dn_kd = td_deliverynote_detail.dn_kd
			LEFT JOIN td_workorder_item ON td_workorder_item.woitem_kd = td_deliverynote_detail.woitem_kd
			LEFT JOIN tb_bagian ON tb_bagian.bagian_kd = tm_deliverynote.dn_asal 
			LEFT JOIN tm_barang ON tm_barang.kd_barang = td_deliverynote_detail.kd_barang 
			LEFT JOIN tm_rawmaterial ON tm_rawmaterial.rm_kd = td_deliverynote_detail.kd_barang 
			WHERE tm_deliverynote.dn_tanggal between '" . $yearAwal . "-" . $monthAwal . "-1' AND '" . $yearAkhir . "-" . $monthAkhir . "-31' AND tm_deliverynote.dn_asal IN ('60') AND td_workorder_item.woitem_jenis = 'part'
			AND (td_workorder_item.woitem_itemcode LIKE '%PART TOP%' 
			OR left(td_workorder_item.woitem_itemcode,2) = 'PJ') 
			GROUP BY td_deliverynote_detail.kd_barang"
		)->result();
		// echo json_encode($dataPartTop);
		// exit();

		foreach ($dataOriso as $v) {
			$bypass_pt = 0;
			foreach ($dataPacking as $p) {
				if ($p->kd_barang == $v->barang_kd) {
					//cek dan add kategori didalam reportdata
					foreach ($reportData as $keyReportData => $valReportData) {
						if ($valReportData['kat_barang_kd'] == $v->kat_barang_kd) {
							$reportData[$keyReportData]['jumlah'] += $p->sum_dndetail_qty;
							$reportData[$keyReportData]['kg'] += ($v->item_qty * $v->grossweight);
							// echo $v->barang_kd . " || " . $v->kat_barang_kd . ' = ' . $v->item_qty . " * " . $v->grossweight . '<br>';
							$bypass_pt = 1;

							if ($v->tipe_do == "Ekspor") {
								$reportData[$keyReportData]['jumlah_ekspor'] += $v->item_qty;
								$reportData[$keyReportData]['rp_ekspor'] += $v->item_qty * ($v->harga_barang * $KonversiDollar);
							} elseif ($v->tipe_do == "Lokal") {
								$reportData[$keyReportData]['jumlah_lokal'] += $v->item_qty;
								$reportData[$keyReportData]['rp_lokal'] += $v->item_qty * $v->harga_barang;
							}
						}
					}
				}
			}

			if ($bypass_pt == 0) {
				foreach ($dataPartTop as $t) {
					if ($t->kd_barang == $v->barang_kd) {
						//cek dan add kategori didalam reportdata
						foreach ($reportData as $keyReportData => $valReportData) {
							if ($valReportData['kat_barang_kd'] == $v->kat_barang_kd) {
								$reportData[$keyReportData]['jumlah'] += $t->sum_dndetail_qty;
								$reportData[$keyReportData]['kg'] += ($v->item_qty * $v->grossweight);
								if ($v->tipe_do == "Ekspor") {
									$reportData[$keyReportData]['jumlah_ekspor'] += $v->item_qty;
									$reportData[$keyReportData]['rp_ekspor'] += $v->item_qty * ($v->harga_barang * $KonversiDollar);
								} elseif ($v->tipe_do == "Lokal") {
									$reportData[$keyReportData]['jumlah_lokal'] += $v->item_qty;
									$reportData[$keyReportData]['rp_lokal'] += $v->item_qty * $v->harga_barang;
								}
							}
						}
					}
				}
			}
		}
		// echo json_encode($reportData);exit();
		$result = $reportData;
		return $result;
	}

	/**
	 * Function that groups an array of associative arrays by some key.
	 * 
	 * @param {String} $key Property to sort by.
	 * @param {Array} $data Array that stores multiple associative arrays.
	 */
	private function group_by($key, $data)
	{
		$result = array();
		foreach ($data as $val) {
			if (array_key_exists($key, $val)) {
				$result[$val[$key]][] = $val;
			} else {
				$result[""][] = $val;
			}
		}
		return $result;
	}
}
