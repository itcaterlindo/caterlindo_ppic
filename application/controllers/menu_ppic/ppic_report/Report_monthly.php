<?php
defined('BASEPATH') or exit('No direct script access allowed!');
require_once APPPATH . "third_party/PhpSpreadsheets/autoload.php";

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Report_monthly extends MY_Controller
{
	private $class_link = 'menu_ppic/ppic_report/report_monthly';

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['tb_bagian', 'tm_project', 'db_hrm/tb_hari_libur']);
		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
	}

	public function index()
	{
		parent::administrator();
		$this->table_box();
	}

	public function table_box()
	{

		// Initiate bulan dan tahun ambil dari database
		$interval = 10;
		$year = date('Y');
		$arrYear = [];
		for ($i = 0; $i < $interval; $i++) {
			array_push($arrYear, $year);
			$year = $year - 1;
		}
		$arrMonth = [];
		for ($i = 1; $i <= 12; $i++) {
			array_push($arrMonth, $i);
		}

		$data['optionYear'] = $arrYear;
		$data['optionMonth'] = $arrMonth;
		$data['class_link'] = $this->class_link;

		$this->load->view('page/' . $this->class_link . '/table_box', $data);
	}

	public function table_main()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$year = $this->input->get('year', true);
		$month = $this->input->get('month', true);
		$data['bulanthn'] = $month . '-' . $year;
		$data['libur'] = $this->tb_hari_libur->get_all_where_range($year . '-' . $month . '-01', $year . '-' . $month . '-31');
		$data['lastdate'] = date("t", strtotime($year . '-' . $month . '-01')); //date("Y-m-t", Last date of current month.
		$data['result'] = $this->data_report($year, $month);
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/table_main', $data);
		// echo json_encode($data_report2);
	}

	public function data_report($year, $month)
	{
		/** Get master barang by wo packing ALL BAGIAN filter by dn date, barang dengan kategori ACCESORIES dan MERCHANDISE tidak di tampilkan */
		$barang = $this->db->query(
			"SELECT td_deliverynote_detail.kd_barang, td_workorder_item.woitem_itemcode, tm_barang.deskripsi_barang, td_workorder_item.woitem_status, GROUP_CONCAT(td_workorder_item.woitem_no_wo) woitem_no_wo
			FROM td_deliverynote_detail
			LEFT JOIN tm_deliverynote ON tm_deliverynote.dn_kd = td_deliverynote_detail.dn_kd
			LEFT JOIN td_workorder_item ON td_workorder_item.woitem_kd = td_deliverynote_detail.woitem_kd
			LEFT JOIN tb_bagian ON tb_bagian.bagian_kd = tm_deliverynote.dn_asal 
			LEFT JOIN tm_barang ON tm_barang.kd_barang = td_deliverynote_detail.kd_barang
			WHERE td_workorder_item.woitem_jenis = 'packing' AND tm_deliverynote.dn_asal IN ('10', '20', '30', '40', '50', '60', '90') AND
			tm_barang.kat_barang_kd NOT IN ('KTB020817000046','KTB020817000047')
			GROUP BY td_deliverynote_detail.kd_barang
			ORDER BY td_workorder_item.woitem_itemcode ASC"
		)->result_array();

		/** Get material receipt group by rawmaterial untuk validasi */
		$materialReceipt = $this->db->query(
			"SELECT td_materialreceipt_detail_rawmaterial.rm_kd, tm_rawmaterial.rm_kode 
			FROM td_materialreceipt_detail_rawmaterial
			LEFT JOIN tm_rawmaterial ON tm_rawmaterial.rm_kd = td_materialreceipt_detail_rawmaterial.rm_kd
			GROUP BY td_materialreceipt_detail_rawmaterial.rm_kd"
		)->result_array();

		/** Variable untuk output report */
		/** Kode bagian */
		$whereBagian = ['10', '20', '30', '40', '50', '60', '90'];
		$arrBagian = $this->tb_bagian->get_by_param_in('bagian_kd', $whereBagian)->result_array();
		$reportData = [];
		foreach ($barang as $key => $val) {
			/** Cek jika pada WO ITEM yang dari rawmaterial tidak ada di MRC maka jangan tampilkan di report DN, (field deskripsi barang hanya untuk identitas pembeda barang atau rawmaterial) */
			if (empty($val['deskripsi_barang']) && !in_array($val['kd_barang'], array_column($materialReceipt, 'rm_kd'))) {
				continue;
			}
			$reportData[$key] = $val;
			foreach ($arrBagian as $keyBagian => $valBagian) {
				$reportData[$key][$valBagian['bagian_nama']] = 0;
			}
		}

		/** Get resep part by master barang */
		$resepDataPart = [];
		foreach ($barang as $key => $val) {

			/** STD = where diambil dari kd_barang, custom = where diambil dari project_kd */
			if ($val['woitem_status'] == 'std') {
				/** standart */
				$where = "bom.kd_barang = '" . $val['kd_barang'] . "'";
			} else {
				/** custom */
				$project = $this->tm_project->get_by_param(['project_nopj' => $val['woitem_no_wo']])->row_array();
				$where = "bom.project_kd='" . $project['project_kd'] . "'";
			}

			$part = $this->db->query(
				"SELECT bom.bom_kd, bom.kd_barang, bom.project_kd, bom.bom_jenis, barang.item_code, part_main.partmain_nama, part_jenis.partjenis_nama, part_jenis.partjenis_kd, part_jenis.partjenis_priorityreportdn
				FROM tm_bom AS bom
				LEFT JOIN td_bom_detail AS bom_detail ON bom_detail.bom_kd = bom.bom_kd
				LEFT JOIN tm_part_main AS part_main ON part_main.partmain_kd = bom_detail.partmain_kd
				LEFT JOIN td_part_jenis AS part_jenis ON part_jenis.partjenis_kd = part_main.partjenis_kd
				LEFT JOIN tm_barang AS barang ON barang.kd_barang = bom.kd_barang
				WHERE $where AND part_jenis.partjenis_priorityreportdn IS NOT NULL 
				AND part_jenis.partjenis_priorityreportdn = ( 
						SELECT MIN(part_jenis.partjenis_priorityreportdn) FROM tm_bom AS bom
						LEFT JOIN td_bom_detail AS bom_detail ON bom_detail.bom_kd = bom.bom_kd
						LEFT JOIN tm_part_main AS part_main ON part_main.partmain_kd = bom_detail.partmain_kd
						LEFT JOIN td_part_jenis AS part_jenis ON part_jenis.partjenis_kd = part_main.partjenis_kd
						WHERE $where )"
			)->result_array();
			if (!empty($part)) {
				foreach ($part as $keyPart => $valPart) {
					$resepDataPart[$val['kd_barang']][$keyPart] = $valPart;
				}
			} else {
				continue;
			}
		}

		/** Get data packing saja, semua bagian */
		$dataPacking = $this->db->query(
			"SELECT td_deliverynote_detail.dndetail_tglinput as dn_tanggal,td_deliverynote_detail.dndetail_kd, td_deliverynote_detail.kd_barang, td_workorder_item.woitem_itemcode, td_deliverynote_detail.dndetail_qty as sum_dndetail_qty, td_workorder_item.woitem_jenis, tm_deliverynote.dn_asal,tb_bagian.bagian_nama
			FROM td_deliverynote_detail
			LEFT JOIN tm_deliverynote ON tm_deliverynote.dn_kd = td_deliverynote_detail.dn_kd
			LEFT JOIN td_workorder_item ON td_workorder_item.woitem_kd = td_deliverynote_detail.woitem_kd
			LEFT JOIN tb_bagian ON tb_bagian.bagian_kd = tm_deliverynote.dn_asal 
			LEFT JOIN tm_barang ON tm_barang.kd_barang = td_deliverynote_detail.kd_barang
			WHERE YEAR(tm_deliverynote.dn_tanggal) = '" . $year . "' AND MONTH(tm_deliverynote.dn_tanggal) = '" . $month . "' AND tm_deliverynote.dn_asal IN ('10', '20', '30', '40', '50', '60', '90') AND td_workorder_item.woitem_jenis = 'packing'
			AND tm_barang.kat_barang_kd NOT IN ('KTB020817000046','KTB020817000047') OR (left(td_deliverynote_detail.kd_barang,2) = 'RM' AND left(td_workorder_item.woitem_itemcode,4) = 'SINK' AND  YEAR(tm_deliverynote.dn_tanggal) = '" . $year . "' AND MONTH(tm_deliverynote.dn_tanggal) = '" . $month . "') 
			ORDER BY tm_deliverynote.dn_asal ASC"
		)->result_array();
		// echo json_encode($dataPacking);

		/** Get data part saja, semua bagian selain packing dan selain part top*/
		// $dataPart = $this->db->query(
		// 	"SELECT td_deliverynote_detail.dndetail_tglinput as dn_tanggal,td_deliverynote_detail.dndetail_kd, td_deliverynote_detail.kd_barang, td_workorder_item.woitem_itemcode, td_deliverynote_detail.dndetail_qty as sum_dndetail_qty, td_workorder_item.woitem_jenis, tm_deliverynote.dn_asal,tb_bagian.bagian_nama
		// 	FROM td_deliverynote_detail
		// 	LEFT JOIN tm_deliverynote ON tm_deliverynote.dn_kd = td_deliverynote_detail.dn_kd
		// 	LEFT JOIN td_workorder_item ON td_workorder_item.woitem_kd = td_deliverynote_detail.woitem_kd
		// 	LEFT JOIN tb_bagian ON tb_bagian.bagian_kd = tm_deliverynote.dn_asal 
		// 	WHERE YEAR(tm_deliverynote.dn_tanggal) = '" . $year . "' AND MONTH(tm_deliverynote.dn_tanggal) = '" . $month . "' AND tm_deliverynote.dn_asal IN ('10', '20', '30', '40', '50', '90') AND td_workorder_item.woitem_jenis = 'part'
		// 	AND td_workorder_item.woitem_itemcode NOT LIKE '%PART TOP%' 
		// 	ORDER BY tm_deliverynote.dn_asal ASC"
		// )->result_array();
		// echo json_encode($dataPart);

		/** Get data part khusus PART TOP, PJ */
		$dataPartTop = $this->db->query(
			"SELECT td_deliverynote_detail.dndetail_tglinput as dn_tanggal,td_deliverynote_detail.dndetail_kd, td_deliverynote_detail.kd_barang, td_workorder_item.woitem_itemcode, td_deliverynote_detail.dndetail_qty as sum_dndetail_qty, td_workorder_item.woitem_jenis, tm_deliverynote.dn_asal,tb_bagian.bagian_nama
				FROM td_deliverynote_detail
				LEFT JOIN tm_deliverynote ON tm_deliverynote.dn_kd = td_deliverynote_detail.dn_kd
				LEFT JOIN td_workorder_item ON td_workorder_item.woitem_kd = td_deliverynote_detail.woitem_kd
				LEFT JOIN tb_bagian ON tb_bagian.bagian_kd = tm_deliverynote.dn_asal 
				WHERE YEAR(tm_deliverynote.dn_tanggal) = '" . $year . "' AND MONTH(tm_deliverynote.dn_tanggal) = '" . $month . "' AND tm_deliverynote.dn_asal IN ('10', '20', '30', '40', '50', '90') AND td_workorder_item.woitem_jenis = 'part'
				AND (td_workorder_item.woitem_itemcode LIKE '%PART TOP%' 
				OR left(td_workorder_item.woitem_itemcode,2) = 'PJ') 
				ORDER BY tm_deliverynote.dn_asal ASC"
		)->result_array();
		// echo json_encode($dataPartTop);

		/** Part */
		/** add tanggal di part */
		foreach ($dataPart as $keyPdataPart => $valdataPart) {
			/** Jika datanya part */
			if ($valdataPart['woitem_jenis'] == 'part') {
				foreach ($reportData as $keyReportData => $valReportData) {
					if ($valReportData['kd_barang'] == $valdataPart['kd_barang']) {
						/** Tambahkan ke array report data */
						$reportData[$keyReportData]['tgl_dn'] = date('d-m-Y', strtotime($valdataPart['dn_tanggal']));
					}
				}
			}
		}
		/** Group by bagian */
		$groupByBagianPartData = $this->group_by('dn_asal', $dataPart);
		/** Bandingkan group by PartData dengan resep part */
		$partHabis = false;
		/** Perulangan berdasarkan bagian */
		foreach ($arrBagian as $keyBagian => $valBagian) {
			/** Group berdasarkan kd_barang untuk dihitung qtynya */
			$groupByPartData = $this->group_by('kd_barang', $groupByBagianPartData[$valBagian['bagian_kd']]);
			/** Bandingkan data resep dengan data part */
			foreach ($resepDataPart as $keyResepDataPart => $valResepDataPart) {
				$qtyBarang = 0;
				/** Jika data resep dan part bertemu (berdasarkan kd_barang) */
				if (array_key_exists($keyResepDataPart, $groupByPartData)) {
					$countResepPart = count($valResepDataPart);
					for ($iResep = 0; $iResep < $countResepPart; $iResep++) {
						/** Cek apakah part ada di resep, jika ada maka hitung, jika tidak ada maka langsung lewati next ke barang selanjutnya */
						if (in_array($valResepDataPart[$iResep]['partmain_nama'], array_column($groupByPartData[$keyResepDataPart], 'woitem_itemcode'))) {
							/** Hitung sampai qty dari salah satu bahan menjadi 0 */
							for ($iGroupBy = 0; $iGroupBy < count($groupByPartData[$keyResepDataPart]); $iGroupBy++) {
								if ($valResepDataPart[$iResep]['partmain_nama'] == $groupByPartData[$keyResepDataPart][$iGroupBy]['woitem_itemcode']) {
									/** Jika qty dari part tersebut sudah habis maka perulangan untuk mengurangi array part berhenti */
									if ($groupByPartData[$keyResepDataPart][$iGroupBy]['sum_dndetail_qty'] <= 0) {
										$partHabis = true;
									} else {
										$groupByPartData[$keyResepDataPart][$iGroupBy]['sum_dndetail_qty'] = $groupByPartData[$keyResepDataPart][$iGroupBy]['sum_dndetail_qty'] - 1;
										$partHabis = false;
									}
								}
							}
						} else {
							/** Part tidak ada di resep, langsung skip lanjut ke barang selanjutnya */
							break;
						}
						/** Jika part sudah habis perulangan selesai */
						if ($partHabis == true) {
							break;
						}
						/** Jika perulangan qty data part belum habis maka kembali lakukan perulangan */
						if ($iResep == $countResepPart - 1) {
							$qtyBarang = $qtyBarang + 1;
							$iResep = -1;
						}
					}

					/** Merger part ke array report data untuk di tampilkan ke front end */
					foreach ($reportData as $keyReportData => $valReportData) {
						if ($valReportData['kd_barang'] == $keyResepDataPart) {
							/** Tambahkan ke array report data */
							$reportData[$keyReportData][$valBagian['bagian_nama']] = $qtyBarang;
							$reportData[$keyReportData]['sum_dndetail_qty'] = $qtyBarang;
							$reportData[$keyReportData]['bagian_nama'] = $valBagian['bagian_nama'];
						}
					}
				}
			}
		}
		/** End part */

		$result = array('Part' => $reportData, 'Packing' => $dataPacking, 'PartTop' => $dataPartTop);
		return $result;
	}

	/**
	 * Function that groups an array of associative arrays by some key.
	 * 
	 * @param {String} $key Property to sort by.
	 * @param {Array} $data Array that stores multiple associative arrays.
	 */
	private function group_by($key, $data)
	{
		$result = array();
		foreach ($data as $val) {
			if (array_key_exists($key, $val)) {
				$result[$val[$key]][] = $val;
			} else {
				$result[""][] = $val;
			}
		}
		return $result;
	}

	private function get_tgl_part($year, $month, $kd_barang, $woitem_itemcode)
	{
		$dataPart = $this->db->query(
			"SELECT td_deliverynote_detail.dndetail_tglinput as dn_tanggal,td_deliverynote_detail.dndetail_kd, td_deliverynote_detail.kd_barang, td_workorder_item.woitem_itemcode, SUM(td_deliverynote_detail.dndetail_qty) sum_dndetail_qty, td_workorder_item.woitem_jenis, tm_deliverynote.dn_asal,tb_bagian.bagian_nama
			FROM td_deliverynote_detail
			LEFT JOIN tm_deliverynote ON tm_deliverynote.dn_kd = td_deliverynote_detail.dn_kd
			LEFT JOIN td_workorder_item ON td_workorder_item.woitem_kd = td_deliverynote_detail.woitem_kd
			LEFT JOIN tb_bagian ON tb_bagian.bagian_kd = tm_deliverynote.dn_asal 
			WHERE YEAR(tm_deliverynote.dn_tanggal) = '" . $year . "' AND MONTH(tm_deliverynote.dn_tanggal) = '" . $month . "' AND tm_deliverynote.dn_asal IN ('10', '20', '30', '40', '50', '90') AND td_workorder_item.woitem_jenis = 'part'
			td_deliverynote_detail.kd_barang = '" . $kd_barang . "' AND td_workorder_item.woitem_itemcode = '" . $woitem_itemcode . "' 
			GROUP BY td_workorder_item.woitem_itemcode, tm_deliverynote.dn_asal"
		)->first_row();

		return $dataPart->dn_tanggal;
	}
}
