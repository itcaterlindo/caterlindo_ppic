<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Purchase_suggestion_main extends MY_Controller
{
    private $class_link = 'menu_ppic/purchase_suggestion/purchase_suggestion_main';

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('ssp', 'form_validation'));
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
        $this->load->model(array('db_hrm/tb_hari_libur', 'td_purchase_suggestion_so', 'td_purchase_suggestion_so_barang', 'td_purchase_suggestion_so_material', 'td_purchaserequisition_detail'));
    }

    public function index()
    {
        parent::administrator();
        parent::fullcalendarv5_assets();
        parent::pnotify_assets();

        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function view_calendarmain()
    {
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/view_calendarmain', $data);
    }

    /** Calculate holidat between, saturday and sunday is holiday 
     * not useed
    */
    public function getWorkingDays($startDate, $endDate, $holidays = array())
    {
        // do strtotime calculations just once
        $endDate = strtotime($endDate);
        $startDate = strtotime($startDate);


        //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
        //We add one to inlude both dates in the interval.
        $days = ($endDate - $startDate) / 86400 + 1;

        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);

        //It will return 1 if it's Monday,.. ,7 for Sunday
        $the_first_day_of_week = date("N", $startDate);
        $the_last_day_of_week = date("N", $endDate);

        //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
        //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
        } else {
            // (edit by Tokes to fix an edge case where the start day was a Sunday
            // and the end day was NOT a Saturday)

            // the day of the week for start is later than the day of the week for end
            if ($the_first_day_of_week == 7) {
                // if the start date is a Sunday, then we definitely subtract 1 day
                $no_remaining_days--;

                if ($the_last_day_of_week == 6) {
                    // if the end date is a Saturday, then we subtract another day
                    $no_remaining_days--;
                }
            } else {
                // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
                // so we skip an entire weekend and subtract 2 days
                $no_remaining_days -= 2;
            }
        }

        //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
        //---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
        $workingDays = $no_full_weeks * 5;
        if ($no_remaining_days > 0) {
            $workingDays += $no_remaining_days;
        }

        //We subtract the holidays
        foreach ($holidays as $holiday) {
            $time_stamp = strtotime($holiday);
            //If the holiday doesn't fall in weekend
            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N", $time_stamp) != 6 && date("N", $time_stamp) != 7)
                $workingDays--;
        }

        return $workingDays;
    }

    public function renderGRleadtime($rm_kd)
    {
        $row = $this->db->where('td_suplier_katalog.rm_kd', $rm_kd)
            ->join('tm_suplier', 'tm_suplier.suplier_kd=td_suplier_katalog.suplier_kd', 'left')
            ->order_by('tm_suplier.suplier_leadtime', 'asc')
            ->get('td_suplier_katalog')->row();
        return !empty($row->suplier_leadtime) ? (int)$row->suplier_leadtime : 2; //default masih 2 hari
    }

    public function renderPRPOleadtime($purchasesuggestion_kd)
    {
        $row = $this->db->where('purchasesuggestion_kd', $purchasesuggestion_kd)
            ->get('tm_purchase_suggestion')->row();
        return !empty($row->purchasesuggestion_leadtime_prpo) ? (int)$row->purchasesuggestion_leadtime_prpo : 1;  // hardcode
    }

    public function get_events()
    {
        $start = $this->input->get('start');
        $end = $this->input->get('end');

        if (!empty($start) && !empty($end)) {
            $start = date('Y-m-d', strtotime($start));
            $end = date('Y-m-d', strtotime($end));

            /** Hari Libur */
            $arrayEvents = [];
            $arrayHolidayDates = [];
            $offs = $this->tb_hari_libur->getByDateBetween($start, $end)->result();
            foreach ($offs as $off) {
                $arrayEvents[] = [
                    'title' => $off->deskripsi,
                    'start' => $off->tgl_libur,
                    'backgroundColor' => '#f56954',
                    'borderColor' => '#f56954'
                ];
                $arrayHolidayDates[] = $off->tgl_libur;
            }

            /** SO */
            $sos = $this->td_purchase_suggestion_so->getTglSelesaiPeriode($start, $end)->result();
            foreach ($sos as $so) {
                $arrayEvents[] = [
                    'title' => $so->no_salesorder . ' (Start Produksi)',
                    'start' => $so->purchasesuggestionso_tglmulai_produksi,
                    'end' => null,
                    'backgroundColor' => '#3f52cd',
                    'borderColor' => '#3f52cd'
                ];
                $arrayEvents[] = [
                    'title' => $so->no_salesorder . ' (Selesai Produksi)',
                    'start' => $so->purchasesuggestionso_tglselesai_produksi,
                    'backgroundColor' => '#cdbf3f',
                    'borderColor' => '#cdbf3f'
                ];
            }

            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($arrayEvents);
        }
    }

    public function viewsosuggestedtable_main()
    {
        $data['class_link'] = $this->class_link;
        $data['result'] = $this->td_purchase_suggestion_so->getSOProcessed()->result_array();
        
        $this->load->view('page/' . $this->class_link . '/viewsosuggestedtable_main', $data);
    }

    /** Not used */
    public function get_events_old()
    {
        $purchasesuggestion_kd = 14;  // hardcode

        $start = $this->input->get('start');
        $end = $this->input->get('end');
        $rm_kd = $this->input->get('rm_kd');

        if (!empty($start) && !empty($end)) {
            $start = date('Y-m-d', strtotime($start));
            $end = date('Y-m-d', strtotime($end));

            /** Hari Libur */
            $arrayEvents = [];
            $arrayHolidayDates = [];
            $offs = $this->tb_hari_libur->getByDateBetween($start, $end)->result();
            foreach ($offs as $off) {
                $arrayEvents[] = [
                    'title' => $off->deskripsi,
                    'start' => $off->tgl_libur,
                    'backgroundColor' => '#f56954',
                    'borderColor' => '#f56954'
                ];
                $arrayHolidayDates[] = $off->tgl_libur;
            }

            /** SO */
            $sos = $this->td_purchase_suggestion_so->getTglKirimStuffing($purchasesuggestion_kd)->result();
            $grLeadTime = $this->renderGRleadtime($rm_kd);
            $prpoLeadTime = $this->renderPRPOleadtime($purchasesuggestion_kd);
            foreach ($sos as $so) {
                $startProdDays = $so->purchasesuggestionso_leadtime_produksi;
                $startDateEstimate = date('Y-m-d', strtotime($so->tgl_selesai . '-' . (int)$startProdDays . ' days'));
                $offs = $this->tb_hari_libur->getByDateBetween($start, $end)->result_array();
                $arrayHolidayDates = !empty($offs) ? array_column($offs, 'tgl_libur') : [];
                $calcLeadtimeProdEstimate = $this->getWorkingDays($startDateEstimate, $so->tgl_selesai, $arrayHolidayDates);
                $diffEndStart = (int)$startProdDays - $calcLeadtimeProdEstimate;
                $startDateAfterHoliday = $startProdDays + $diffEndStart;
                $startDate = date('Y-m-d', strtotime($so->tgl_selesai . '-' . $startDateAfterHoliday . ' days'));

                /** Leadtime supplier */
                $startDateGREstimate = date('Y-m-d', strtotime($startDate . '-' . (int)$grLeadTime . ' days'));
                $calcLeadtimeGREstimate = $this->getWorkingDays($startDateGREstimate, $startDate, $arrayHolidayDates);
                $startDateAfterHolidayGR = $grLeadTime + $calcLeadtimeGREstimate;
                $startDateGR = date('Y-m-d', strtotime($startDate . '-' . $startDateAfterHolidayGR . ' days'));

                /** Leadtime PRPO */
                $startDatePRPOEstimate = date('Y-m-d', strtotime($startDateGR . '-' . (int)$prpoLeadTime . ' days'));
                $calcLeadtimePRPOEstimate = $this->getWorkingDays($startDatePRPOEstimate, $startDateGR, $arrayHolidayDates);
                // echo $calcLeadtimePRPOEstimate;die();
                $startDateAfterHolidayPRPO = $prpoLeadTime + $calcLeadtimePRPOEstimate;
                $startDatePRPO = date('Y-m-d', strtotime($startDateGR . '-' . $startDateAfterHolidayPRPO . ' days'));


                $arrayEvents[] = [
                    'title' => $so->no_salesorder . ' (Start Leadtime Produksi)',
                    'start' => $startDate,
                    'end' => null,
                    'backgroundColor' => '#f39c12',
                    'borderColor' => '#f39c12'
                ];
                $arrayEvents[] = [
                    'title' => $so->no_salesorder . ' (Selesai Leadtime Produksi)',
                    'start' => $so->tgl_selesai,
                    'backgroundColor' => '#f39c12',
                    'borderColor' => '#f39c12'
                ];

                /** Leadtime GR */
                $arrayEvents[] = [
                    'title' => $rm_kd . ' (GR Supplier start)',
                    'start' => $startDateGR,
                    'backgroundColor' => '#00c0ef',
                    'borderColor' => '#00c0ef'
                ];

                /** Leadtime PRPO */
                $arrayEvents[] = [
                    'title' => $rm_kd . ' (PRPO Supplier start)',
                    'start' => $startDatePRPO,
                    'backgroundColor' => '#00a65a',
                    'borderColor' => '#00a65a'
                ];
            }

            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($arrayEvents);
        }
    }

    public function viewsuggestpr_box()
    {
        $pr_kd = $this->input->get('pr_kd', true);

        $data['headerPR'] = $this->db->select()->from('tm_purchaserequisition as a')
            ->join('td_workflow_state as b', 'a.wfstate_kd=b.wfstate_kd', 'left')
            ->join('tb_admin as c', 'a.admin_kd=c.kd_admin', 'left')
            ->where('a.pr_kd', $pr_kd)
            ->get()->row_array();
        $data['pr_kd'] = $pr_kd;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/viewsuggestpr_box', $data);
    }

    public function viewsuggestpr_tablemain()
    {
        $periodestart = $this->input->get('periodestart');   
        $periodeend = $this->input->get('periodeend');

        $data['results'] = $this->td_purchase_suggestion_so_material->get_suggestpr_periode($periodestart, $periodeend);

        $this->load->view('page/' . $this->class_link . '/viewsuggestpr_tablemain', $data);
    }

    public function action_insert_prsuggest_batch()
    {
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpr_kd', 'PR', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idtxtpr_kdsuggest' => (!empty(form_error('txtpr_kd')))?buildLabel('warning', form_error('txtpr_kd', '"', '"')):'',
			);
			
		}else {
            $pr_kd = $this->input->post('txtpr_kd', true);
            $rm_kds = $this->input->post('txtrm_kds', true);
            $prdetail_nama = $this->input->post('txtprdetail_nama', true);
            $prdetail_deskripsi = $this->input->post('txtprdetail_deskripsi', true);
            $prdetail_spesifikasi = $this->input->post('txtprdetail_spesifikasi', true);
            $prdetail_qty = $this->input->post('txtprdetail_qty', true);
            $rmsatuan_kd = $this->input->post('txtrmsatuan_kd', true);
            $prdetail_konversi = $this->input->post('txtprdetail_konversi', true);
            $prdetail_qtykonversi = $this->input->post('txtprdetail_qtykonversi', true);
            $prdetail_duedate = $this->input->post('txtprdetail_duedate', true);
            $prdetail_remarks = $this->input->post('txtprdetail_remarks', true);
			
            $prdetail_kd = null;
            for($i=0; $i<count($rm_kds); $i++){
                $prdetail_kd = $this->td_purchaserequisition_detail->create_code($prdetail_kd);
                $data[] = array(
                    'prdetail_kd' => $prdetail_kd,
                    'pr_kd' => $pr_kd,
                    'rm_kd' => !empty($rm_kds[$i]) ? $rm_kds[$i]:null,
                    'prdetail_nama' => !empty($prdetail_nama[$rm_kds[$i]]) ? $prdetail_nama[$rm_kds[$i]]:null,
                    'prdetail_deskripsi' => !empty($prdetail_deskripsi[$rm_kds[$i]]) ? $prdetail_deskripsi[$rm_kds[$i]]:null,
                    'prdetail_spesifikasi' => !empty($prdetail_spesifikasi[$rm_kds[$i]]) ? $prdetail_spesifikasi[$rm_kds[$i]]:null,
                    'prdetail_qty' => !empty($prdetail_qty[$rm_kds[$i]]) ? $prdetail_qty[$rm_kds[$i]]:null,
                    'prdetail_konversi' => !empty($prdetail_konversi[$rm_kds[$i]]) ? $prdetail_konversi[$rm_kds[$i]]:1,
                    'prdetail_qtykonversi' => !empty($prdetail_qtykonversi[$rm_kds[$i]]) ? $prdetail_qtykonversi[$rm_kds[$i]]:null,
                    'rmsatuan_kd' => !empty($rmsatuan_kd[$rm_kds[$i]]) ? $rmsatuan_kd[$rm_kds[$i]]:'-',
                    'prdetail_duedate' => !empty($prdetail_duedate[$rm_kds[$i]]) ? $prdetail_duedate[$rm_kds[$i]] : date('Y-m-d'),
                    'prdetail_remarks' => !empty($prdetail_remarks[$rm_kds[$i]]) ? $prdetail_remarks[$rm_kds[$i]]:null,
                    'prdetail_tglinput' => date('Y-m-d H:i:s'),
                    'admin_kd' => $this->session->userdata('kd_admin'),
                );
            }
            $act = $this->td_purchaserequisition_detail->insert_batch($data);
            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();

        header('Content-Type: application/json; charset=utf-8');
		echo json_encode($resp);
    }
}
