<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Purchase_suggestion_process extends MY_Controller
{
    private $class_link = 'menu_ppic/purchase_suggestion/purchase_suggestion_process';

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('ssp', 'form_validation'));
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
        $this->load->model(array('tm_purchase_suggestion', 'td_purchase_suggestion_so', 'td_purchase_suggestion_so_barang', 'td_purchase_suggestion_so_material', 'td_rawmaterial_stok_master', 'td_purchaseorder_detail', 'td_purchaserequisition_detail', 'tm_purchaseorder', 'tm_purchaserequisition', 'tm_barang'));
    }

    public function index()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();

        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function table_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;

        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }

    public function table_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->load->library(['ssp']);

        $data = $this->tm_purchase_suggestion->ssp_table();

        echo json_encode(
            SSP::simple($_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'])
        );
    }

    public function formso_box()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::datetimepicker_assets();

        $purchasesuggestion_kd = $this->input->get('purchasesuggestion_kd');

        $data['purchasesuggestion_kd'] = $purchasesuggestion_kd;
        $data['class_link'] = $this->class_link;
        $data['row'] = $this->db->where('tm_purchase_suggestion.purchasesuggestion_kd', $purchasesuggestion_kd)
            ->get('tm_purchase_suggestion')->row_array();

            $this->load->view('page/' . $this->class_link . '/formso_box', $data);
    }

    public function tableso_main()
    {
        $data['purchasesuggestion_kd'] = $this->input->get('purchasesuggestion_kd');
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/tableso_main', $data);
    }

    public function tableso_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->load->library(['ssp']);

        $purchasesuggestion_kd = $this->input->get('purchasesuggestion_kd');
        $data = $this->td_purchase_suggestion_so->ssp_table($purchasesuggestion_kd);

        echo json_encode(
            SSP::simple($_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'])
        );
    }

    public function formso_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['purchasesuggestionso_kd'] = $this->input->get('purchasesuggestionso_kd');
        $data['class_link'] = $this->class_link;
        $data['row'] = $this->td_purchase_suggestion_so->get_by_param(['purchasesuggestionso_kd' => $this->input->get('purchasesuggestionso_kd')])->row_array();

        $this->load->view('page/' . $this->class_link . '/formso_main', $data);
    }

    public function formbarang_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['action'] = $this->input->get('action');
        $data['purchasesuggestion_kd'] = $this->input->get('purchasesuggestion_kd');
        $data['purchasesuggestionsobarang_kd'] = $this->input->get('purchasesuggestionsobarang_kd');
        if($data['action'] == 'edit'){
            $data['class_link'] = $this->class_link;
            $data['row'] = $this->db->select('td_purchase_suggestion_so_barang.*, barangbom.item_code as itemcode_bom, tm_barang.item_code')
                ->where(['purchasesuggestionsobarang_kd' => $this->input->get('purchasesuggestionsobarang_kd')])
                ->join('tm_barang', 'tm_barang.kd_barang=td_purchase_suggestion_so_barang.kd_barang', 'left')
                ->join('tm_bom', 'tm_bom.kd_barang=td_purchase_suggestion_so_barang.kd_barang', 'left')
                ->join('tm_barang as barangbom', 'barangbom.kd_barang=tm_bom.kd_barang', 'left')
                ->get('td_purchase_suggestion_so_barang')->row_array();
        }
        // echo json_encode($data);
        $this->load->view('page/' . $this->class_link . '/formbarang_main', $data);
       
    }

    public function formmaterial_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['purchasesuggestionmaterial_kd'] = $this->input->get('purchasesuggestionmaterial_kd');
        $data['class_link'] = $this->class_link;
        $data['row'] = $this->db->select('td_purchase_suggestion_so_material.*, tm_rawmaterial.rm_kode')
            ->where(['td_purchase_suggestion_so_material.purchasesuggestionmaterial_kd' => $this->input->get('purchasesuggestionmaterial_kd')])
            ->join('tm_rawmaterial', 'tm_rawmaterial.rm_kd=td_purchase_suggestion_so_material.rm_kd', 'left')
            ->get('td_purchase_suggestion_so_material')->row_array();

        $this->load->view('page/' . $this->class_link . '/formmaterial_main', $data);
    }

    private function get_salesorder()
    {
        $resultData[null] = '';
        $query = $this->db->select('tm_salesorder.kd_msalesorder, tm_salesorder.no_salesorder, tm_salesorder.no_po, tm_salesorder.tipe_customer, tm_customer.nm_customer')
            ->from('tm_salesorder')
            ->join('tm_customer', 'tm_salesorder.customer_kd=tm_customer.kd_customer', 'left')
            ->join('td_purchase_suggestion_so', 'td_purchase_suggestion_so.kd_msalesorder=tm_salesorder.kd_msalesorder', 'left')
            ->where_in('tm_salesorder.status_so', ['process_lpo', 'process_wo'])
            ->where('td_purchase_suggestion_so.kd_msalesorder IS NULL')
            ->order_by('tm_salesorder.tgl_so', 'DESC')
            ->get();
            if (!empty($query->num_rows())) {
            $result = $query->result_array();
            foreach ($result as $r) {
                $no_salesorder = $r['no_salesorder'];
                if ($r['tipe_customer'] == 'Ekspor') {
                    $no_salesorder = $r['no_po'];
                }
                $resultData[$r['kd_msalesorder']] = $no_salesorder . ' | ' . $r['nm_customer'];
            }
        }
        
        return $resultData;
    }

    public function formbarang_box()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();

        $purchasesuggestion_kd = $this->input->get('purchasesuggestion_kd');

        $data['purchasesuggestion_kd'] = $purchasesuggestion_kd;
        $data['class_link'] = $this->class_link;
        $data['row'] = $this->db->where('tm_purchase_suggestion.purchasesuggestion_kd', $purchasesuggestion_kd)
            ->get('tm_purchase_suggestion')->row_array();

            $this->load->view('page/' . $this->class_link . '/formbarang_box', $data);
    }

    public function tablebarang_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['purchasesuggestion_kd'] = $this->input->get('purchasesuggestion_kd');
        $data['class_link'] = $this->class_link;

        $this->load->view('page/' . $this->class_link . '/tablebarang_main', $data);

    }

    public function tablebarang_data()
    {
        // if (!$this->input->is_ajax_request()) {
        //     exit('No direct script access allowed');
        // }
        $this->load->library(['ssp']);
        $purchasesuggestion_kd = $this->input->get('purchasesuggestion_kd');
        //$data = $this->td_purchase_suggestion_so_barang->get_by_param(['purchasesuggestion_kd' => $purchasesuggestion_kd])->result_array();

        $data = $this->db->select('td_purchase_suggestion_so_barang.*, barangbom.item_code as itemcode_bom, tm_barang.item_code')
        ->where(['purchasesuggestion_kd' => $purchasesuggestion_kd])
        ->join('tm_barang', 'tm_barang.kd_barang=td_purchase_suggestion_so_barang.kd_barang', 'left')
        ->join('tm_bom', 'tm_bom.kd_barang=td_purchase_suggestion_so_barang.kd_barang', 'left')
        ->join('tm_barang as barangbom', 'barangbom.kd_barang=tm_bom.kd_barang', 'left')
        ->get('td_purchase_suggestion_so_barang')->result_array();

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($data);
        // echo json_encode(
        //     SSP::simple($_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'])
        // );
    }

    public function tablematerial_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['purchasesuggestion_kd'] = $this->input->get('purchasesuggestion_kd');
        $data['class_link'] = $this->class_link;

        $this->load->view('page/' . $this->class_link . '/tablematerial_main', $data);

    }

    public function tablematerial_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->load->library(['ssp']);
        $purchasesuggestion_kd = $this->input->get('purchasesuggestion_kd');
        $data = $this->td_purchase_suggestion_so_material->ssp_table($purchasesuggestion_kd);

        echo json_encode(
            SSP::simple($_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'])
        );
    }

    public function tablematerialbyso_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['purchasesuggestion_kd'] = $this->input->get('purchasesuggestion_kd');
        $data['class_link'] = $this->class_link;
        $data['result'] = $this->td_purchase_suggestion_so_material->getMaterialGroupBySO($this->input->get('purchasesuggestion_kd'))->result_array();

        $this->load->view('page/' . $this->class_link . '/tablematerialbyso_main', $data);
    }

    public function tablematerialbytglsuggest_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['purchasesuggestion_kd'] = $this->input->get('purchasesuggestion_kd');
        $data['class_link'] = $this->class_link;
        $data['result'] = $this->td_purchase_suggestion_so_material->getMaterialGroupByTglSuggest($this->input->get('purchasesuggestion_kd'))->result_array();

        $this->load->view('page/' . $this->class_link . '/tablematerialbytglsuggest_main', $data);
    }

    public function formmaster_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $purchasesuggestion_kd = $this->input->get('purchasesuggestion_kd');

        $data['purchasesuggestion_kd'] = $purchasesuggestion_kd;
        $data['opsiSO'] = $this->get_salesorder();
        $data['class_link'] = $this->class_link;
        $data['rowData'] = $this->tm_purchase_suggestion->get_by_param(['purchasesuggestion_kd' => $purchasesuggestion_kd])->row_array();
        $data['soSelected'] = array_column($this->td_purchase_suggestion_so->get_by_param(['purchasesuggestion_kd' => $purchasesuggestion_kd])->result_array(), 'kd_msalesorder');

        $this->load->view('page/' . $this->class_link . '/formmaster_main', $data);
    }

    public function form_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['class_link'] = $this->class_link;

        $this->load->view('page/' . $this->class_link . '/form_main', $data);
    }   
    
	// public function texa(){
	// 	// if (!$this->input->is_ajax_request()){
	// 	// 	exit('No direct script access allowed');
	// 	// }

    //     $purchasesuggestion_kd = $this->input->get('purchasesuggestion_kd');
	// 		$rawQuery = "SELECT `td_purchase_suggestion_so_material`.`purchasesuggestionso_kd`, `td_purchase_suggestion_so_material`.`rm_kd`, `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_rm_deskripsi`, `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_rm_spesifikasi`, `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_leadtimesupplier_hari`, `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_suggestpr_tanggal`, `tm_salesorder`.`kd_msalesorder`, `tm_salesorder`.`no_salesorder`, `tm_salesorder`.`no_po`, `tm_salesorder`.`tipe_customer`, SUM(td_purchase_suggestion_so_material.purchasesuggestionmaterial_qty) as sum_purchasesuggestionmaterial_qty, `tm_rawmaterial`.`rm_kode`, `td_rawmaterial_satuan`.`rmsatuan_nama`, `tm_rawmaterial`.`rm_stock_min` 
    //         FROM `td_purchase_suggestion_so_material` 
    //         LEFT JOIN `td_purchase_suggestion_so` ON `td_purchase_suggestion_so`.`purchasesuggestionso_kd`=`td_purchase_suggestion_so_material`.`purchasesuggestionso_kd` 
    //         LEFT JOIN `tm_salesorder` ON `tm_salesorder`.`kd_msalesorder`=`td_purchase_suggestion_so`.`kd_msalesorder` 
    //         LEFT JOIN `tm_rawmaterial` ON `tm_rawmaterial`.`rm_kd`=`td_purchase_suggestion_so_material`.`rm_kd` 
    //         LEFT JOIN `td_rawmaterial_satuan` ON `td_rawmaterial_satuan`.`rmsatuan_kd`=`td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_rm_satuan` 
    //         WHERE `td_purchase_suggestion_so_material`.`purchasesuggestion_kd` = ".$this->input->get('purchasesuggestion_kd')."
    //         GROUP BY `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_suggestpr_tanggal`, `td_purchase_suggestion_so_material`.`rm_kd` 
          
						
	// 		UNION				
    //         SELECT `td_purchase_suggestion_so_material`.`purchasesuggestionso_kd`, `td_purchase_suggestion_so_material`.`rm_kd`, `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_rm_deskripsi`, `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_rm_spesifikasi`, `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_leadtimesupplier_hari`, `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_suggestpr_tanggal`, `tm_salesorder`.`kd_msalesorder`, `tm_salesorder`.`no_salesorder`, `tm_salesorder`.`no_po`, `tm_salesorder`.`tipe_customer`, SUM(td_purchase_suggestion_so_material.purchasesuggestionmaterial_qty) as sum_purchasesuggestionmaterial_qty, `tm_rawmaterialx`.`rm_kode`, `td_rawmaterial_satuan`.`rmsatuan_nama`, `tm_rawmaterialx`.`rm_stock_min` 
    //             FROM `td_purchase_suggestion_so_material` 
    //             INNER JOIN `td_purchase_suggestion_so` ON `td_purchase_suggestion_so`.`purchasesuggestionso_kd`=`td_purchase_suggestion_so_material`.`purchasesuggestionso_kd` 
    //             INNER JOIN `tm_salesorder` ON `tm_salesorder`.`kd_msalesorder`=`td_purchase_suggestion_so`.`kd_msalesorder` 
    //             INNER JOIN (select tm_bom.kd_barang, tm_rawmaterial.* FROM tm_bom 
    //                                                 LEFT JOIN td_bom_detail ON tm_bom.bom_kd = td_bom_detail.bom_kd
    //                                                 LEFT JOIN tm_part_main ON tm_part_main.partmain_kd = td_bom_detail.partmain_kd
    //                                                 LEFT JOIN (SELECT td_part.partmain_kd, MAX(td_part.part_versi), MAX(td_part.part_kd) as xxi FROM td_part GROUP BY td_part.partmain_kd) AS cust_part ON tm_part_main.partmain_kd = cust_part.partmain_kd
    //                                                 LEFT JOIN td_part_detail ON td_part_detail.part_kd = cust_part.xxi
    //                                                 LEFT JOIN tm_rawmaterial ON tm_rawmaterial.rm_kd = td_part_detail.rm_kd
    //                                                 WHERE tm_bom.kd_barang LIKE '%RM%') AS tm_rawmaterialx ON `tm_rawmaterialx`.`kd_barang`=`td_purchase_suggestion_so_material`.`rm_kd` 
    //             INNER JOIN `td_rawmaterial_satuan` ON `td_rawmaterial_satuan`.`rmsatuan_kd`=`td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_rm_satuan` 
    //             WHERE `td_purchase_suggestion_so_material`.`purchasesuggestion_kd` = ".$this->input->get('purchasesuggestion_kd')."
    //             GROUP BY `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_suggestpr_tanggal`, `td_purchase_suggestion_so_material`.`rm_kd` 
						
    //       ";


							

	// 		$query = $this->db->query($rawQuery)->result_array();

			
	// 	// $resp['code'] = 200;
	// 	// $resp['status'] = 'Sukses';
	// 	// $resp['csrf'] = $this->security->get_csrf_hash();
	// 	 $resp['datax'] = $query;
	// 	// $resp['htmlxx'] = 
	// 	//$this->load->view('page/'.$this->class_link.'/table_main', $resp);

	// 	header('Content-Type: application/json');
    //     echo json_encode($resp);
	// }
    

    public function formmaterial_box()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::datetimepicker_assets();

        $purchasesuggestion_kd = $this->input->get('purchasesuggestion_kd');

        $data['purchasesuggestion_kd'] = $purchasesuggestion_kd;
        $data['class_link'] = $this->class_link;
        $data['row'] = $this->db->where('tm_purchase_suggestion.purchasesuggestion_kd', $purchasesuggestion_kd)
            ->get('tm_purchase_suggestion')->row_array();

            $this->load->view('page/' . $this->class_link . '/formmaterial_box', $data);
    }

    public function summarymaterial_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['purchasesuggestion_kd'] = $this->input->get('purchasesuggestion_kd');
        $data['class_link'] = $this->class_link;
        // $sumMaterial = $this->td_purchase_suggestion_so_material->summaryMaterialGroupByTglSuggest($this->input->get('purchasesuggestion_kd'))->result_array();
        $rawQuery1 = "SELECT `td_purchase_suggestion_so_material`.`purchasesuggestionso_kd`, `td_purchase_suggestion_so_material`.`rm_kd`, '1' as partdetail_qty, `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_rm_deskripsi`, `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_rm_spesifikasi`, `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_leadtimesupplier_hari`, `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_suggestpr_tanggal`, `tm_salesorder`.`kd_msalesorder`, `tm_salesorder`.`no_salesorder`, `tm_salesorder`.`no_po`, `tm_salesorder`.`tipe_customer`, SUM(td_purchase_suggestion_so_material.purchasesuggestionmaterial_qty) as sum_purchasesuggestionmaterial_qty, `tm_rawmaterial`.`rm_kode`, `td_rawmaterial_satuan`.`rmsatuan_nama`, `tm_rawmaterial`.`rm_stock_min` 
            FROM `td_purchase_suggestion_so_material` 
            LEFT JOIN `td_purchase_suggestion_so` ON `td_purchase_suggestion_so`.`purchasesuggestionso_kd`=`td_purchase_suggestion_so_material`.`purchasesuggestionso_kd` 
            LEFT JOIN `tm_salesorder` ON `tm_salesorder`.`kd_msalesorder`=`td_purchase_suggestion_so`.`kd_msalesorder` 
            LEFT JOIN `tm_rawmaterial` ON `tm_rawmaterial`.`rm_kd`=`td_purchase_suggestion_so_material`.`rm_kd` 
            LEFT JOIN `td_rawmaterial_satuan` ON `td_rawmaterial_satuan`.`rmsatuan_kd`=`td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_rm_satuan` 
            WHERE `td_purchase_suggestion_so_material`.`purchasesuggestion_kd` = ".$this->input->get('purchasesuggestion_kd')."
            GROUP BY `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_suggestpr_tanggal`, `td_purchase_suggestion_so_material`.`rm_kd`";		

		$sumMaterial1 = $this->db->query($rawQuery1)->result_array();


        $rawQuery2 = "SELECT 
                        `td_purchase_suggestion_so_material`.`purchasesuggestionso_kd`, 
                        `td_purchase_suggestion_so_material`.`rm_kd`, 
                        `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_rm_deskripsi`, 
                        `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_rm_spesifikasi`, 
                        `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_leadtimesupplier_hari`, 
                        `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_suggestpr_tanggal`, 
                        `tm_salesorder`.`kd_msalesorder`, 
                        `tm_salesorder`.`no_salesorder`, 
                        `tm_salesorder`.`no_po`, 
                        `tm_salesorder`.`tipe_customer`, 
                        SUM(td_purchase_suggestion_so_material.purchasesuggestionmaterial_qty) as sum_purchasesuggestionmaterial_qty, 
                        `tm_rawmaterialx`.`rm_kode`, 
                        `td_rawmaterial_satuan`.`rmsatuan_nama`, 
                        `tm_rawmaterialx`.`rm_stock_min`,
                        `tm_rawmaterialx`.`partdetail_qty`
                        
                            FROM `td_purchase_suggestion_so_material` 
                                        INNER JOIN `td_purchase_suggestion_so` ON `td_purchase_suggestion_so`.`purchasesuggestionso_kd`=`td_purchase_suggestion_so_material`.`purchasesuggestionso_kd` 
                                        INNER JOIN `tm_salesorder` ON `tm_salesorder`.`kd_msalesorder`=`td_purchase_suggestion_so`.`kd_msalesorder` 
                                        INNER JOIN (select tm_bom.kd_barang, tm_rawmaterial.*, td_part_detail.partdetail_qty FROM tm_bom 
                                                                LEFT JOIN td_bom_detail ON tm_bom.bom_kd = td_bom_detail.bom_kd
                                                                LEFT JOIN tm_part_main ON tm_part_main.partmain_kd = td_bom_detail.partmain_kd
                                                                LEFT JOIN (SELECT td_part.partmain_kd, MAX(td_part.part_versi), MAX(td_part.part_kd) as xxi FROM td_part GROUP BY td_part.partmain_kd) AS cust_part ON tm_part_main.partmain_kd = cust_part.partmain_kd
                                                                LEFT JOIN td_part_detail ON td_part_detail.part_kd = cust_part.xxi
                                                                LEFT JOIN tm_rawmaterial ON tm_rawmaterial.rm_kd = td_part_detail.rm_kd WHERE tm_bom.kd_barang LIKE '%RM%') AS tm_rawmaterialx ON `tm_rawmaterialx`.`kd_barang`=`td_purchase_suggestion_so_material`.`rm_kd` 
                                        INNER JOIN `td_rawmaterial_satuan` ON `td_rawmaterial_satuan`.`rmsatuan_kd`=`tm_rawmaterialx`.`rmsatuan_kd` 
                                        WHERE `td_purchase_suggestion_so_material`.`purchasesuggestion_kd` = ".$this->input->get('purchasesuggestion_kd')."
                                        GROUP BY `td_purchase_suggestion_so_material`.`purchasesuggestionmaterial_suggestpr_tanggal`, `td_purchase_suggestion_so_material`.`rm_kd`, `tm_rawmaterialx`.`rm_kode";		

    $sumMaterial2 = $this->db->query($rawQuery2)->result_array();

    $sumMaterial = array_merge($sumMaterial1, $sumMaterial2);
        
        $rm_kd = [];
        foreach($sumMaterial as $row => $val){
            $rm_kd[] = $val['rm_kd'];
        }
        $arr = [
            'kartu_stok' => $this->td_rawmaterial_stok_master->getLastStokResult($rm_kd, date('Y-m-d')),
            'outstanding_pr' => $this->outstanding_pr($rm_kd)['resultPR'],
            'outstanding_po' => $this->outstanding_po($rm_kd)['resultPO'],
        ];

        /** Recap */
        foreach($sumMaterial as $row => $val){
            $data['result'][$row] = $val;
            
            /** Check jika rawmaterial kode ditemukan maka get value dari array */
            if( in_array($val['rm_kd'], array_column($arr['kartu_stok'], "rm_kd")) ){
                foreach($arr['kartu_stok'] as $kartu => $kartu_val){
                    if($kartu_val['rm_kd'] == $val['rm_kd']){
                        $data['result'][$row]['rm_stock_awal'] = $kartu_val['qty_aft_mutasi'];
                    }
                }
            }else{
                $data['result'][$row]['rm_stock_awal'] = 0;
            }
            
            /** Check jika rawmaterial kode ditemukan maka get value dari array */
            if( in_array($val['rm_kd'], array_column($arr['outstanding_pr'], "rm_kd")) ){
                foreach($arr['outstanding_pr'] as $pr => $pr_val){
                    if($pr_val['rm_kd'] == $val['rm_kd']){
                        $data['result'][$row]['sisa_pr'] = !empty($pr_val['sisa_pr']) ? $pr_val['sisa_pr'] : 0;
                    }
                }
            }else{
                $data['result'][$row]['sisa_pr'] = 0;
            }

            /** Check jika rawmaterial kode ditemukan maka get value dari array */
            if( in_array($val['rm_kd'], array_column($arr['outstanding_po'], "rm_kd")) ){
                foreach($arr['outstanding_po'] as $po => $po_val){
                    if($po_val['rm_kd'] == $val['rm_kd']){
                        $data['result'][$row]['sisa_po'] = !empty($po_val['sisa_po']) ? $po_val['sisa_po'] : 0;
                    }
                }
            }else{
                $data['result'][$row]['sisa_po'] = 0;
            }

            $data['result'][$row]['qty_sisa'] =  $data['result'][$row]['rm_stock_awal'] - $data['result'][$row]['rm_stock_min'] + $data['result'][$row]['sisa_pr'] + $data['result'][$row]['sisa_po'] - $data['result'][$row]['sum_purchasesuggestionmaterial_qty'];
            $data['result'][$row]['qty_suggest'] =  $data['result'][$row]['qty_sisa'] < 0 ? abs($data['result'][$row]['qty_sisa']) : 0; // Jika nilainya negatif tampilkan qty sisa, jika positif maka tampilkan 0 
        }
        $this->load->view('page/' . $this->class_link . '/summarymaterial_main', $data);
    }

    public function summarymaterial_box()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::datetimepicker_assets();

        $purchasesuggestion_kd = $this->input->get('purchasesuggestion_kd');

        $data['purchasesuggestion_kd'] = $purchasesuggestion_kd;
        $data['class_link'] = $this->class_link;
        $data['row'] = $this->db->where('tm_purchase_suggestion.purchasesuggestion_kd', $purchasesuggestion_kd)->get('tm_purchase_suggestion')->row_array();
        $this->load->view('page/' . $this->class_link . '/summarymaterial_box', $data);
    }

    public function action_insert_master()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtpurchasesuggestion_leadtime_prpo', 'Leadtime', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrpurchasesuggestion_leadtime_prpo' => (!empty(form_error('txtpurchasesuggestion_leadtime_prpo'))) ? buildLabel('warning', form_error('txtpurchasesuggestion_leadtime_prpo', '"', '"')) : '',
            );
        } else {
            $purchasesuggestion_kd = $this->input->post('txtpurchasesuggestion_kd');
            $kd_msalsorders = $this->input->post('txtkd_msalsorders');

            if (empty($purchasesuggestion_kd)) {
                $purchasesuggestion_kd = $this->tm_purchase_suggestion->create_code();
                $purchasesuggestion_kode = $this->tm_purchase_suggestion->generate_kode();
                // ADD
                $arrayPurchaseSuggest = [
                    'purchasesuggestion_kd' => $purchasesuggestion_kd,
                    'purchasesuggestion_kode' => $purchasesuggestion_kode,
                    'purchasesuggestion_leadtime_prpo' => $this->input->post('txtpurchasesuggestion_leadtime_prpo'),
                    'purchasesuggestion_note' => !empty($this->input->post('txtpurchasesuggestion_note')) ? $this->input->post('txtpurchasesuggestion_note') : null,
                    'purchasesuggestion_state' => 'process',
                    'purchasesuggestion_tglinput' => date('Y-m-d H:i:s'),
                    'purchasesuggestion_tgledit' => date('Y-m-d H:i:s'),
                    'admin_kd' => $this->session->userdata('kd_admin')
                ];

                /** Insert prsuggest SO */
                $msos = $this->db->where_in('kd_msalesorder', $kd_msalsorders)
                    ->get('tm_salesorder')
                    ->result_array();
                $arrayMsos = array();
                foreach ($msos as $mso) {
                    $arrayMsos[$mso['kd_msalesorder']] = $mso['tgl_selesai'];
                }
                $arrarPRSO = array();
                $purchasesuggestionso_kd = $this->td_purchase_suggestion_so->create_code();
                for ($iSo = 0; $iSo < count($kd_msalsorders); $iSo++){
                    $arrarPRSO[] = [
                        'purchasesuggestionso_kd' => $purchasesuggestionso_kd,
                        'purchasesuggestion_kd' => $purchasesuggestion_kd,
                        'kd_msalesorder' => $kd_msalsorders[$iSo],
                        'purchasesuggestionso_leadtime_produksi' => null,
                        'purchasesuggestionso_tglmulai_produksi' => null,
                        'purchasesuggestionso_tglselesai_produksi' => isset($arrayMsos[$kd_msalsorders[$iSo]]) ? $arrayMsos[$kd_msalsorders[$iSo]] : null,
                        'purchasesuggestionso_tglinput' => date('Y-m-d H:i:s'),
                        'purchasesuggestionso_tgledit' => date('Y-m-d H:i:s'),
                        'admin_kd' => $this->session->userdata('kd_admin')
                    ];
                    $purchasesuggestionso_kd++;
                }

                $this->tm_purchase_suggestion->insert_data($arrayPurchaseSuggest);
                $this->td_purchase_suggestion_so->insert_batch($arrarPRSO);

                $resp['code'] = 200;
                $resp['status'] = 'Sukses';
                $resp['pesan'] = 'Tersimpan';
                $resp['data'] = $arrayPurchaseSuggest;
            } else {
                // EDIT
                $arrayUpdatePurchaseSuggest = [
                    'purchasesuggestion_kd' => $purchasesuggestion_kd,
                    'purchasesuggestion_leadtime_prpo' => $this->input->post('txtpurchasesuggestion_leadtime_prpo'),
                    'purchasesuggestion_note' => !empty($this->input->post('txtpurchasesuggestion_note')) ? $this->input->post('txtpurchasesuggestion_note') : null,
                    'purchasesuggestion_tgledit' => date('Y-m-d H:i:s'),
                    'admin_kd' => $this->session->userdata('kd_admin')
                ];
                $this->tm_purchase_suggestion->update_data(['purchasesuggestion_kd' => $purchasesuggestion_kd], $arrayUpdatePurchaseSuggest);
                $resp['code'] = 200;
                $resp['status'] = 'Sukses';
                $resp['pesan'] = 'Terupdate';
                $resp['data'] = array_merge($arrayUpdatePurchaseSuggest, ['purchasesuggestion_kd' => $purchasesuggestion_kd]);
            }
        }

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($resp);
    }

    public function action_insert_prsuggestso ()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtpurchasesuggestionso_kd', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idtxtpurchasesuggestionso_kd' => (!empty(form_error('txtpurchasesuggestionso_kd'))) ? buildLabel('warning', form_error('txtpurchasesuggestionso_kd', '"', '"')) : '',
            );
        } else {
            $purchasesuggestionso_kd = $this->input->post('txtpurchasesuggestionso_kd');
            $arrayPRSuggestSO = [
                'purchasesuggestionso_leadtime_produksi' => $this->input->post('txtpurchasesuggestionso_leadtime_produksi'),
                'purchasesuggestionso_tglselesai_produksi' => $this->input->post('txtpurchasesuggestionso_tglselesai_produksi'),
                'purchasesuggestionso_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin')
            ];

            $this->td_purchase_suggestion_so->update_data(['purchasesuggestionso_kd' => $purchasesuggestionso_kd], $arrayPRSuggestSO);
            $resp['code'] = 200;
            $resp['status'] = 'Sukses'; 
            $resp['pesan'] = 'Terupdate';
            $resp['data'] = array_merge($arrayPRSuggestSO, ['purchasesuggestionso_kd' => $purchasesuggestionso_kd]);
        }
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($resp);
    }

    public function action_insert_prsuggestsobarang()
    {

        // $masterBarang = $this->tm_barang->get_item(['kd_barang' => $this->input->post('txtitemcode')]);
        // echo json_encode($this->input->post());

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $action = $this->input->post('txtaction');
        if( $action == 'add' ){
            $this->form_validation->set_rules('txtitemcode', 'Item Code / Nama Barang', 'required', ['required' => '{field} tidak boleh kosong!']);
        }
        $this->form_validation->set_rules('txtbom_kd', 'BOM', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtpurchasesuggestionsobarang_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idtxtpurchasesuggestionsobarang_kd' => (!empty(form_error('txtpurchasesuggestionsobarang_kd'))) ? buildLabel('warning', form_error('txtpurchasesuggestionsobarang_kd', '"', '"')) : '',
                'idErrItemCode' => (!empty(form_error('txtitemcode'))) ? buildLabel('warning', form_error('txtitemcode', '"', '"')) : '',
                'idErrbom_kd' => (!empty(form_error('txtbom_kd'))) ? buildLabel('warning', form_error('txtbom_kd', '"', '"')) : '',
                'idErrQty' => (!empty(form_error('txtpurchasesuggestionsobarang_qty'))) ? buildLabel('warning', form_error('txtpurchasesuggestionsobarang_qty', '"', '"')) : '',
            );
        } else {

            if($action == 'add'){
                /** Add */
                $purchasesuggestionsobarang_kd = $this->input->post('txtpurchasesuggestionsobarang_kd');
                $bom_kd = $this->input->post('txtbom_kd');
                $qty = $this->input->post('txtpurchasesuggestionsobarang_qty');
                $purchasesuggestion_kd = $this->input->post('txtpurchasesuggestion_kd');
                $masterBarang = $this->tm_barang->get_item(['kd_barang' => $this->input->post('txtitemcode')]);
                $deskripsiBarang = $this->input->post('txtdeskripsi');
                $arrayAdd = [
                    'purchasesuggestionsobarang_kd' => $this->td_purchase_suggestion_so_barang->create_code(),
                    'purchasesuggestion_kd' => $purchasesuggestion_kd,
                    'kd_barang' => $masterBarang->kd_barang,
                    'bom_kd' => $bom_kd,
                    'purchasesuggestionsobarang_itemstatus' => $masterBarang->item_group_kd == '2' ? 'custom' : 'std',
                    'purchasesuggestionsobarang_deskripsi' => $deskripsiBarang,
                    'purchasesuggestionsobarang_dimensi' => $masterBarang->dimensi_barang,
                    'purchasesuggestionsobarang_qty' => $qty,
                    'purchasesuggestionsobarang_tglinput' => date('Y-m-d H:i:s'),
                    'purchasesuggestionsobarang_tgledit' => date('Y-m-d H:i:s'),
                    'admin_kd' =>$this->session->userdata('kd_admin')
                ];
                $this->td_purchase_suggestion_so_barang->insert_data($arrayAdd);
                $resp['code'] = 200;
                $resp['status'] = 'Sukses'; 
                $resp['pesan'] = 'Berhasil menambahkan data';

            }else{
                /** Edit */
                $purchasesuggestionsobarang_kd = $this->input->post('txtpurchasesuggestionsobarang_kd');
                $bom_kd = $this->input->post('txtbom_kd');
                $qty = $this->input->post('txtpurchasesuggestionsobarang_qty');
                $arrayUpdate = [
                    'bom_kd' => $bom_kd,
                    'purchasesuggestionsobarang_qty' => $qty,
                    'purchasesuggestionsobarang_tgledit' => date('Y-m-d H:i:s'),
                    'admin_kd' => $this->session->userdata('kd_admin')
                ];
                $this->td_purchase_suggestion_so_barang->update_data(['purchasesuggestionsobarang_kd' => $purchasesuggestionsobarang_kd], $arrayUpdate);
                $resp['code'] = 200;
                $resp['status'] = 'Sukses'; 
                $resp['pesan'] = 'Terupdate';
            }
        }

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($resp);
    }

    public function action_insert_prsuggestsomaterial()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtpurchasesuggestionmaterial_kd', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtpurchasesuggestionmaterial_leadtimesupplier_hari', 'hari', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtpurchasesuggestionmaterial_suggestpr_tanggal', 'tanggal', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idtxtpurchasesuggestionmaterial_kd' => (!empty(form_error('txtpurchasesuggestionmaterial_kd'))) ? buildLabel('warning', form_error('txtpurchasesuggestionmaterial_kd', '"', '"')) : '',
                'idErrpurchasesuggestionmaterial_leadtimesupplier_hari' => (!empty(form_error('txtpurchasesuggestionmaterial_leadtimesupplier_hari'))) ? buildLabel('warning', form_error('txtpurchasesuggestionmaterial_leadtimesupplier_hari', '"', '"')) : '',
                'idErrpurchasesuggestionmaterial_suggestpr_tanggal' => (!empty(form_error('txtpurchasesuggestionmaterial_suggestpr_tanggal'))) ? buildLabel('warning', form_error('txtpurchasesuggestionmaterial_suggestpr_tanggal', '"', '"')) : '',
            );
        } else {
            $purchasesuggestionmaterial_kd = $this->input->post('txtpurchasesuggestionmaterial_kd');
            $purchasesuggestionmaterial_leadtimesupplier_hari = $this->input->post('txtpurchasesuggestionmaterial_leadtimesupplier_hari');
            $purchasesuggestionmaterial_suggestpr_tanggal = $this->input->post('txtpurchasesuggestionmaterial_suggestpr_tanggal');

            $arrayUpdate = [
                'purchasesuggestionmaterial_leadtimesupplier_hari' => $purchasesuggestionmaterial_leadtimesupplier_hari,
                'purchasesuggestionmaterial_suggestpr_tanggal' => $purchasesuggestionmaterial_suggestpr_tanggal,
                'purchasesuggestionmaterial_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin')
            ];
            $this->td_purchase_suggestion_so_material->update_data(['purchasesuggestionmaterial_kd' => $purchasesuggestionmaterial_kd], $arrayUpdate);
            
            $resp['code'] = 200;
            $resp['status'] = 'Sukses'; 
            $resp['pesan'] = 'Terupdate';
        }

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($resp);
    }

    public function action_delete_suggest()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $purchasesuggestion_kd = $this->input->get('purchasesuggestion_kd');
        if (!empty($purchasesuggestion_kd)){
            $this->tm_purchase_suggestion->delete_data($purchasesuggestion_kd);
            $resp['code'] = 200;
            $resp['status'] = 'Sukses'; 
            $resp['pesan'] = 'Terhapus';
        }else{
            $resp['code'] = 400;
            $resp['status'] = 'Gagal'; 
            $resp['pesan'] = 'Gagal Hapus';
        }

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($resp);
    }

    public function action_delete_barang()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $purchasesuggestionsobarang_kd = $this->input->get('purchasesuggestionsobarang_kd');
        if (!empty($purchasesuggestionsobarang_kd)){
            $this->td_purchase_suggestion_so_material->delete_by_param(['purchasesuggestionsobarang_kd' => $purchasesuggestionsobarang_kd]);
            $this->td_purchase_suggestion_so_barang->delete_by_param(['purchasesuggestionsobarang_kd' => $purchasesuggestionsobarang_kd]);
           
            $resp['code'] = 200;
            $resp['status'] = 'Sukses'; 
            $resp['pesan'] = 'Terhapus';
        }else{
            $resp['code'] = 400;
            $resp['status'] = 'Gagal'; 
            $resp['pesan'] = 'Gagal Hapus';
        }

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($resp);
    }

    public function action_delete_suggest_so()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $purchasesuggestionso_kd = $this->input->get('purchasesuggestionso_kd');
        if (!empty($purchasesuggestionso_kd)){
            $this->td_purchase_suggestion_so->delete_data($purchasesuggestionso_kd);
            $resp['code'] = 200;
            $resp['status'] = 'Sukses'; 
            $resp['pesan'] = 'Terhapus';
        }else{
            $resp['code'] = 400;
            $resp['status'] = 'Gagal'; 
            $resp['pesan'] = 'Gagal Hapus';
        }

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($resp);
    }

    public function action_generate_startproduksi()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $purchasesuggestion_kd = $this->input->get('purchasesuggestion_kd');
        $act = $this->td_purchase_suggestion_so->generateTglMulaiProduksi($purchasesuggestion_kd);
        
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($act);
    }

    public function action_generate_itemso()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $purchasesuggestion_kd = $this->input->get('purchasesuggestion_kd');
        $act = $this->td_purchase_suggestion_so_barang->generate_so_barangs($purchasesuggestion_kd);
        
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($act);
    }

    public function action_generate_material()
    {
        $purchasesuggestion_kd = $this->input->get('purchasesuggestion_kd');
        $act = $this->td_purchase_suggestion_so_material->generate_so_materials($purchasesuggestion_kd);
        
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($act);
    }

    public function action_generate_material_leadtimesupplier()
    {
        $purchasesuggestion_kd = $this->input->get('purchasesuggestion_kd');
        $act = $this->td_purchase_suggestion_so_material->generate_rm_leadtimesupplier($purchasesuggestion_kd);
        
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($act);
    }

    public function action_generate_material_suggestpr()
    {
        $purchasesuggestion_kd = $this->input->get('purchasesuggestion_kd');
        $act = $this->td_purchase_suggestion_so_material->generate_suggest_pr_material($purchasesuggestion_kd);
        
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($act);
    }

    /** Data outstanding PR */
    public function outstanding_pr($rm_kd = [])
    {
        $data = $this->tm_purchaserequisition->outstanding_pr_purchasesuggest($rm_kd);
        return $data;
    }

    /** Data outstanding PO */
    public function outstanding_po($rm_kd = [])
    {
        $data = $this->tm_purchaseorder->outstanding_po_purchasesuggest($rm_kd);
        return $data;
    }

    
}
