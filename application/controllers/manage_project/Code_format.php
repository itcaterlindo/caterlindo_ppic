<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Code_format extends MY_Controller {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'tb_code_format';
	private $p_key = 'id';
	private $class_link = 'manage_project/code_format';
	private $title = 'Format Code';

	function __construct() {
		parent::__construct();
		
		$this->load->model('m_builder');
		$this->load->helper(array('form', 'html', 'my_helper'));
	}

	function index() {
		parent::administrator();

		$script['class_link'] = $this->class_link;
		$nm = $this->uri->segment(3);
		if ($nm == 'project_code') :
			$script['nm_code'] = 'Project';
		endif;
		$script['form_error'] = array('idErrResetter', 'idErrSeparator');
		
		$this->data_form('view', $script['nm_code']);
		$this->load->section('scriptJS', 'script/'.$this->class_link.'/scriptJS', $script);
	}

	function data_form($act, $nm_halaman){
		// Form data
		$btn_form = '';
		$id_form = 'idFormInput';
		$data['form_title'] = $this->title.' '.$nm_halaman;
		$data['btn_close'] = FALSE;
		if ($act == 'view') :
			$data['f_box_class'] = 'box-success';
			$btn_form .= '<a href="javascript:void(0);" id="idBtnUbah" class="btn btn-warning pull-right">';
			$btn_form .= '<i class="fa fa-pencil"></i> Ubah Data';
			$btn_form .= '</a>';
		elseif ($act == 'update') :
			$data['f_box_class'] = 'box-primary';
			$btn_form .= '<button type="submit" id="idBtnSubmit" class="btn btn-primary pull-right">';
			$btn_form .= '<i class="fa fa-save"></i> Simpan';
			$btn_form .= '</button>';
			$btn_form .= '<a href="javascript:void(0);" id="idBtnCancel" class="btn btn-danger pull-right" style="margin-right:1%;">';
			$btn_form .= '<i class="fa fa-ban"></i> Batal';
			$btn_form .= '</a>';
		endif;
		$check = $this->m_builder->getRow($this->tbl_name, array('code_for' => $nm_halaman), 'num_rows');
		$det = $this->m_builder->getRow($this->tbl_name, array('code_for' => $nm_halaman), 'row');
		$id_code = ($check > 0)?$this->security->xss_clean($det->id):'';
		$code_length = ($check > 0)?$this->security->xss_clean($det->code_length):'';
		$code_format = ($check > 0)?$this->security->xss_clean($det->code_format):'';
		$code_for = ($check > 0)?$this->security->xss_clean($det->code_for):$nm_halaman;
		$code_separator = ($check > 0)?$this->security->xss_clean($det->code_separator):'';
		$on_reset = ($check > 0)?$this->security->xss_clean($det->on_reset):'';

		// Pilihan untuk pemisah format kode
		$pil_separator = array('' => '-- Pilih Separator --', 'n' => 'Tidak Ada', '.' => '.', ',' => ',', '-' => '-', '_' => '_');
		$sel_separator = $code_separator;
		$attr_separator = array('id' => 'idSelSeparator', 'class' => 'form-control');

		// Pilihan untuk pemisah reset kode
		$pil_resetter = array('' => '-- Pilih Resetter --', 'day' => 'Day', 'month' => 'Month', 'year' => 'Year');
		$sel_resetter = $on_reset;
		$attr_resetter = array('id' => 'idSelResetter', 'class' => 'form-control');

		$dropdown = '';
		$form_view = '';
		if ($act == 'update') :
			$dropdown .= form_input(array('type' => 'hidden', 'name' => 'txtKd', 'value' => $id_code));
			$dropdown .= form_input(array('type' => 'hidden', 'name' => 'txtCodeFor', 'value' => $code_for));
			$dropdown .= '<div class="form-group">';
			$dropdown .= '<label for="idSelSeparator" class="col-md-2 control-label">Code Separator</label>';
			$dropdown .= '<div class="col-md-2">';
			$dropdown .= '<div id="idErrSeparator"></div>';
			$dropdown .= form_dropdown('selSeparator', $pil_separator, $sel_separator, $attr_separator);
			$dropdown .= '</div>';
			$dropdown .= '</div>';
			$dropdown .= '<div class="form-group">';
			$dropdown .= '<label for="idSelResetter" class="col-md-2 control-label">Code Reset</label>';
			$dropdown .= '<div class="col-md-2">';
			$dropdown .= '<div id="idErrResetter"></div>';
			$dropdown .= form_dropdown('selResetter', $pil_resetter, $sel_resetter, $attr_resetter);
			$dropdown .= '</div>';
			$dropdown .= '</div>';
			if (!empty($code_format)) :
				$pecah_format = explode('-', $code_format);
				for ($i=0; $i < count($pecah_format); $i++) {
					$type = $i > 0?'form_anak':'form_inti';
					$val = $pecah_format[$i];
					if (stripos($val, 'urutan_angka') !== FALSE) :
						$jml = strlen('urutan_angka');
						$kata = substr($val, 0, $jml);
						$nilai = substr($val, $jml);
						$nilai = substr($nilai, 1);
						$nilai = substr($nilai, 0, -1);
					elseif (stripos($val, 'kode_huruf') !== FALSE) :
						$jml = strlen('kode_huruf');
						$kata = substr($val, 0, $jml);
						$nilai = substr($val, $jml);
						$nilai = substr($nilai, 1);
						$nilai = substr($nilai, 0, -1);
					else :
						$kata = $val;
						$nilai = '';
					endif;

					$dropdown .= $this->structure_dropdown($pecah_format[$i], $type);
				}
			else :
				$dropdown .= $this->structure_dropdown('', 'form_inti');
			endif;
		else :
			$form_view .= '<div class="form-group">';
			$form_view .= '<label for="idTxtCodeSeparator" class="col-md-2 control-label">Code Separator</label>';
			$form_view .= '<div class="col-md-4">';
			$form_view .= '<div id="idErrCodeSeparator"></div>';
			$form_view .= form_input(array('type' => 'hidden', 'name' => 'txtKd', 'value' => $id_code));
			$form_view .= form_input(array('type' => 'hidden', 'name' => 'txtCodeFor', 'value' => $code_for));
			$form_view .= form_input(array('name' => 'txtKdCodeSeparator', 'id' => 'idTxtCodeSeparator', 'class' => 'form-control', 'placeholder' => 'Code Separator', 'value' => $code_separator, 'readonly' => 'TRUE'));
			$form_view .= '</div>';
			$form_view .= '</div>';
			$form_view .= '<div class="form-group">';
			$form_view .= '<label for="idTxtCodeReset" class="col-md-2 control-label">Code Reset</label>';
			$form_view .= '<div class="col-md-4">';
			$form_view .= '<div id="idErrCodeReset"></div>';
			$form_view .= form_input(array('name' => 'txtKdCodeReset', 'id' => 'idTxtCodeReset', 'class' => 'form-control', 'placeholder' => 'Code Reset', 'value' => $on_reset, 'readonly' => 'TRUE'));
			$form_view .= '</div>';
			$form_view .= '</div>';
			$form_view .= '<div class="form-group">';
			$form_view .= '<label for="idTxtCodeLength" class="col-md-2 control-label">Panjang Code</label>';
			$form_view .= '<div class="col-md-4">';
			$form_view .= '<div id="idErrCodeLength"></div>';
			$form_view .= form_input(array('name' => 'txtKdCodeLength', 'id' => 'idTxtCodeLength', 'class' => 'form-control', 'placeholder' => 'Panjang Code', 'value' => $code_length, 'readonly' => 'TRUE'));
			$form_view .= '</div>';
			$form_view .= '</div>';
			$form_view .= '<div class="form-group">';
			$form_view .= '<label for="idTxtCodeFormat" class="col-md-2 control-label">Format Code</label>';
			$form_view .= '<div class="col-md-4">';
			$form_view .= '<div id="idErrNmKat"></div>';
			$form_view .= form_input(array('name' => 'txtCodeFormat', 'id' => 'idTxtCodeFormat', 'class' => 'form-control', 'placeholder' => 'Format Code', 'value' => $code_format, 'readonly' => 'TRUE'));
			$form_view .= '</div>';
			$form_view .= '</div>';
		endif;

		$data['f_attr'] = 'id="idBoxForm"';
		$data['form_url'] = '';
		$data['form_att'] = array('id' => $id_form, 'class' => 'form-horizontal');
		$data['form_hidden'] = '';
		$data['form_close_att'] = '';
		$data['form_field'] = array(
			'code_length' => array(
				$form_view,
			),
			'code_structure' => array(
				'<div class="form_format">',
				$dropdown,
				'</div>',
			),
		);

		$data['form_btn'] = array(
			'btn_submit' => array(
				$btn_form,
			),
		);
		$data['form_box'] = 'idBoxFormBody';
		$data['form_alert'] = 'idAlertForm';
		$data['form_overlay'] = 'idOverlayForm';
		
		$this->load->view('page/admin/v_form', $data);
	}

	function structure_dropdown($val, $type){
		$selected = $val == 'val'?'':$val;

		if ($val != 'val') :
			$kata = '';
			$nilai = '';
			if (stripos($val, 'urutan_angka') !== FALSE) :
				$jml = strlen('urutan_angka');
				$selected = substr($val, 0, $jml);
				$nilai = substr($val, $jml);
				$nilai = substr($nilai, 1);
				$nilai = substr($nilai, 0, -1);
			elseif (stripos($val, 'kode_huruf') !== FALSE) :
				$jml = strlen('kode_huruf');
				$selected = substr($val, 0, $jml);
				$nilai = substr($val, $jml);
				$nilai = substr($nilai, 1);
				$nilai = substr($nilai, 0, -1);
			endif;
		endif;

		$this->db->where(array('jenis_select' => 'code_format'));
		$this->db->from('tb_set_dropdown');
		$query = $this->db->get();
		$result = $query->result();
		$pil_format = array('' => '-- Pilih Format --');
		foreach ($result as $row) :
			$nm_select = $this->security->xss_clean($row->nm_select);
			$val_select = str_replace(' ', '_', $nm_select);
			$pil_format[$val_select] = $nm_select;
		endforeach;
		$sel_format = $selected;
		$attr_format = array('id' => 'idSelFormat', 'class' => 'form-control idSelFormat', 'style' => 'width:100%;');
		$form_child = '';
		$text_child = '';

		if ($val != 'val') :
			if ($selected == 'urutan_angka') :
				$panjang = strlen($nilai);
				$text_child .= '<div class="col-md-1">';
				$text_child .= form_input(array('name' => 'txtTextLength['.$selected.']', 'class' => 'form-control', 'value' => $panjang, 'placeholder' => 'Panjang'));
				$text_child .= '</div>';
				$text_child .= '<div class="col-md-1">';
				$text_child .= form_input(array('name' => 'txtTextStart['.$selected.']', 'class' => 'form-control', 'value' => $nilai, 'placeholder' => 'Mulai'));
				$text_child .= '</div>';
			elseif ($selected == 'kode_huruf') :
				$text_child .= '<div class="col-md-2">';
				$text_child .= form_input(array('name' => 'txtTextFormat['.$selected.']', 'class' => 'form-control', 'value' => $nilai, 'placeholder' => 'Format Huruf'));
				$text_child .= '</div>';
			endif;
		endif;

		$form_child .= '<div class="form-group">';
		$form_child .= '<label for="idSelFormat" class="col-md-2 control-label">Pilih Format</label>';
		$form_child .= '<div class="col-md-3">';
		$form_child .= form_dropdown('selFormat[]', $pil_format, $sel_format, $attr_format);
		$form_child .= '</div>';
		$form_child .= '<div class="text_format">'.$text_child.'</div>';
		if ($type == 'form_anak') :
			$form_child .= '<div class="col-md-1">';
			$form_child .= '<a href="javascript:void(0);" class="btn btn-sm btn-danger idBtnHapus" title="Hapus"><i class="fa fa-trash"></i></a>';
			$form_child .= '</div>';
		elseif ($type == 'form_inti') :
			$form_child .= '<div class="col-md-1">';
			$form_child .= '<a href="javascript:void(0);" class="btn btn-sm btn-success idBtnAdd" title="Tambah"><i class="fa fa-plus"></i></a>';
			$form_child .= '</div>';
		endif;
		$form_child .= '</div>';

		if ($val == 'val') :
			$str['text_format'] = $form_child;
			header('Content-Type: application/json');
			echo json_encode($str);
		else :
			return $form_child;
		endif;
	}

	function text_format($nilai_format, $val_format){
		$nilai_format = $this->input->get('nilai_format');
		$val_format = $this->input->get('val_format');

		$form_child = '';
		if ($nilai_format == 'urutan_angka' || $nilai_format == 'kode_huruf') :
			if ($nilai_format == 'urutan_angka') :
				$form_child .= '<div class="col-md-1">';
				$form_child .= form_input(array('name' => 'txtTextLength['.$nilai_format.']', 'class' => 'form-control', 'value' => $val_format, 'placeholder' => 'Panjang'));
				$form_child .= '</div>';
				$form_child .= '<div class="col-md-1">';
				$form_child .= form_input(array('name' => 'txtTextStart['.$nilai_format.']', 'class' => 'form-control', 'value' => $val_format, 'placeholder' => 'Mulai'));
				$form_child .= '</div>';
			elseif ($nilai_format == 'kode_huruf') :
				$form_child .= '<div class="col-md-2">';
				$form_child .= form_input(array('name' => 'txtTextFormat['.$nilai_format.']', 'class' => 'form-control', 'value' => $val_format, 'placeholder' => 'Format Huruf'));
				$form_child .= '</div>';
			endif;

		endif;

		if ($this->input->is_ajax_request()) :
			$str['text_format'] = $form_child;
			header('Content-Type: application/json');
			echo json_encode($str);
		else :
			return $form_child;
		endif;
	}

	function submit_form(){
		if ($this->input->is_ajax_request()) :
			$this->load->library(array('form_validation'));
			$str['csrf'] = $this->security->get_csrf_hash();

			$str['id_code'] = $this->input->post('txtKd');
			$str['code_for'] = $this->input->post('txtCodeFor');
			$str['sel_format'] = $this->input->post('selFormat');
			$str['txt_length'] = $this->input->post('txtTextLength');
			$str['txt_start'] = $this->input->post('txtTextStart');
			$str['txt_format'] = $this->input->post('txtTextFormat');

			// Cek Code For
			$id_code = $this->input->post('txtKd');
			$code_for = $this->input->post('txtCodeFor');
			$sel_format = $this->input->post('selFormat');
			$txt_length = $this->input->post('txtTextLength');
			$txt_start = $this->input->post('txtTextStart');
			$txt_format = $this->input->post('txtTextFormat');
			$check = $this->m_builder->getRow($this->tbl_name, array('code_for' => $code_for), 'num_rows');

			$this->form_validation->set_rules('selSeparator', 'Code Separator', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selResetter', 'Code Reset', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'errValidation';
				$str['idErrSeparator'] = (!empty(form_error('selSeparator')))?buildLabel('warning', form_error('selSeparator', '"', '"')):'';
				$str['idErrResetter'] = (!empty(form_error('selResetter')))?buildLabel('warning', form_error('selResetter', '"', '"')):'';
			else :
				// Hitung berapa select yang digunakan
				$err = 0;
				$success = 0;
				$no = 0;
				$proto_code = '';
				$code_format = '';
				foreach ($sel_format as $format) :
					$no++;
					$hubung = $no == count($sel_format)?'':'-';
					if ($format == 'urutan_angka') :
						foreach ($txt_length as $key => $length) :
							$panjang = strlen($txt_start[$key]);
							($panjang > $length || $panjang < $length)?$err++:$success++;
						endforeach;
						if ($err < 1) :
							foreach ($txt_start as $key => $start) :
								$proto_code .= $start;
								$code_format .= 'urutan_angka['.$start.']'.$hubung;
							endforeach;
						endif;
					elseif ($format == 'kode_huruf') :
						foreach ($txt_format as $key => $huruf) :
							$proto_code .= $huruf;
							$code_format .= 'urutan_angka['.$huruf.']'.$hubung;
						endforeach;
					else :
						$proto_code .= $format;
						$code_format .= $format.$hubung;
					endif;
				endforeach;

				$code_length = strlen($proto_code);
				if ($err < 1) :
					if ($check > 0) :
						$data = array(
							'code_length' => $code_length,
							'code_format' => $code_format,
							'code_for' => $code_for,
							'tgl_edit' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin'),
						);
						$where = array('id' => $id_code);
						$aksi = $this->db->update($this->tbl_name, $data, $where);
						$label_err = 'mengubah';
					else :
						$data = array(
							'code_length' => $code_length,
							'code_format' => $code_format,
							'code_for' => $code_for,
							'tgl_input' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin'),
						);
						$aksi = $this->db->insert($this->tbl_name, $data);
						$label_err = 'menambahkan';
					endif;

					if ($aksi) :
						$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' '.$code_for.' dengan code_length \''.$code_length.'\' code_format \''.$code_format.'\' code_for \''.$code_for.'\'');
						$str['confirm'] = 'success';
						$str['alert'] = buildLabel('success', 'Berhasil!', 'Berhasil '.$label_err.' data category barang!');
					else :
						$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' '.$this->title.' '.$code_for.' dengan code_length \''.$code_length.'\' code_format \''.$code_format.'\' code_for \''.$code_for.'\'');
						$str['confirm'] = 'errValidation';
						$str['idErrForm'] = buildLabel('danger', 'Gagal '.$label_err.' data '.$this->title.' '.$code_for.', Kesalahan sistem!');
					endif;
				else :
					$str['confirm'] = 'errValidation';
					$str['idErrForm'] = buildLabel('danger', 'Gagal '.$label_err.' data '.$this->title.' '.$code_for.', Kesalahan pada pengisian form!');
				endif;
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}