<?php
defined('BASEPATH') OR exit('No direct script access allowed!');
 
require_once APPPATH."third_party/PhpSpreadsheets/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Home extends MY_Controller {
	private $class_link = 'dashboard/home';
	private $form_errs = array('idErr');

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'html', 'my_helper', 'my_prices_helper'));
		$this->load->model(array('model_dashboard', 'tb_jenis_customer', 'tb_default_disc', 'm_salesperson', 'model_do', 'model_salesorder', 'model_quotation'));
		$this->load->library(array('form_validation'));
	}

	public function index() {
		parent::administrator();
		parent::ionicons_assets();
		parent::icheck_assets();
		parent::datetimepicker_assets();
		parent::chartjs_assets();
		$this->get_boxes();
		// Sales Lokal
		if($this->session->tipe_admin == "Admin" || $this->session->tipe_admin == "Sales Lokal" || $this->session->tipe_admin == "Admin Sales"):
			$this->chart_sales_box();
		endif;
		// Sales Ekspor
		if($this->session->tipe_admin == "Admin" || $this->session->tipe_admin == "Eksport"):
			$this->chartekspor_sales_box();
		endif;
		// $this->get_graph();
		if ($_SESSION['view_detail_report'] == 1) :
			$this->get_report();
		endif;
	}

	public function get_boxes() {
		$data = $this->model_dashboard->get_count();
		$this->load->view('page/'.$this->class_link.'/boxes_view', $data);
	}

	public function chart_sales_box()
	{
		$data['class_link'] = $this->class_link;
		$data['opt_tahun'] = pil_tahun(date('Y'), 25, 5);
		$data['pilihan_tahun'] = date('Y');
		$this->load->view('page/'.$this->class_link.'/chart_sales_box', $data);
	}

	public function chart_sales_main()
	{
		$tahunAwal = $this->input->get('tahun_awal');
		$tahunAkhir = $this->input->get('tahun_akhir');
		$data['salesOrder'] = $this->model_salesorder->report_perbandingan_pertahun($tahunAwal, $tahunAkhir);
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = $this->form_errs;
		$this->load->view('page/'.$this->class_link.'/chart_sales_main', $data);
	}

	public function chartekspor_sales_box()
	{
		$data['class_link'] = $this->class_link;
		$data['opt_tahun'] = pil_tahun(date('Y'), 25, 5);
		$data['pilihan_tahun'] = date('Y');
		$this->load->view('page/'.$this->class_link.'/chartekspor_sales_box', $data);
	}

	public function chartekspor_sales_main()
	{
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$tahunAwal = $this->input->get('tahun_awal');
		$tahunAkhir = $this->input->get('tahun_akhir');
		$tahun = [];
		$tipeCustomer = "Ekspor";
		$ammountDistributor = 0;
		$ammount20ft = 0;

		$selisihTahun = $tahunAkhir - $tahunAwal;
		for($i = 0; $i <= $selisihTahun; $i++):
			$ammountDistributor = 0;
			$ammount20ft = 0;
			// Get all teritory untuk di bandingkan dengan hasil group by
			for($bulan = 1; $bulan <= 12; $bulan++ ):
				// Get RAW Invoice
				$inv = $this->model_salesorder->report_sales_ekspor_invoice($tahunAwal, $bulan, $tipeCustomer);
				if(empty($inv)):
					break;
				else:
					$ammountDistributor += array_sum(array_column($inv, 'ammount_distributor'));
					$ammount20ft += array_sum(array_column($inv, 'container_20ft'));
				endif;
			endfor;
			$recapInv[$tahunAwal] = [
				'ammount_distributor' => ROUND($ammountDistributor, 2),
				'container_20ft' => ROUND($ammount20ft, 2)
			];
			$tahun[] = (int)$tahunAwal;
			$tahunAwal++;
		endfor;
		$data['tahun'] = $tahun;
		$data['recapInv'] = $recapInv;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/chartekspor_sales_main', $data);
	}

	public function get_graph() {
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = $this->form_errs;
		$this->load->view('page/'.$this->class_link.'/graph_box', $data);
	}

	public function open_graph() {
		$data['class_link'] = $this->class_link;
		$data['opt_tahun'] = pil_tahun(date('Y'), 25, 5);
		$data['pilihan_tahun'] = date('Y');
		$data['opt_bulan'] = pil_bulan();
		$data['pilihan_bulan'] = date('m');
		$this->load->view('page/'.$this->class_link.'/graph_main', $data);
	}

	public function get_report() {
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = $this->form_errs;
		$this->load->view('page/'.$this->class_link.'/report_box', $data);
	}

	public function open_report() {
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = $this->form_errs;
		$data['opt_sales'] = $this->m_salesperson->generate_dropdown();
		$data['pilihan_sales'] = '';
		$this->load->view('page/'.$this->class_link.'/report_main', $data);
	}

	public function get_tipe_customer() {
		$tipe_penjualan = $this->input->get('tipe_penjualan');
		$data = $this->tb_jenis_customer->sel_tipe_penjualan($tipe_penjualan);
		echo $data;
	}

	public function get_tipe_disc() {
		$tipe_customer = $this->input->get('tipe_customer');
		$data = '<option value="">-- Pilih Tipe Discount --</option>';
		if ($tipe_customer == 'semua') :
			$data .= '<option value="Customer">Customer</option>';
			$data .= '<option value="Distributor">Distributor</option>';
		else :
			$result = $this->tb_default_disc->get_all($tipe_customer);
			if (!empty($result)) :
				foreach ($result as $row) :
					$data .= '<option value="'.$row->nm_pihak.'">'.$row->nm_pihak.'</option>';
				endforeach;
			endif;
		endif;
		echo $data;
	}

	public function generate_report() {
		if ($this->input->is_ajax_request()) :
			$form_errs = array('idErrSelTipePenjualan', 'idErrSelTipeCustomer', 'idErrSelDiscount', 'idErrSelSales', 'idErrSelStatusItem', 'idErrTxtTglAwal', 'idErrTxtTglAkhir');
			$this->form_validation->set_rules($this->model_dashboard->report_rules());
			if ($this->form_validation->run() == FALSE) :
				$str = $this->model_dashboard->report_warning($form_errs);
				$str['confirm'] = 'error';
			else :
				$data['tipe_report'] = 'invoice';
				$data['tipe_penjualan'] = $this->input->post('selTipePenjualan');
				$data['kd_jenis_customer'] = $this->input->post('selTipeCustomer');
				$data['nm_disc'] = $this->input->post('selTipeDiscount');
				$data['kd_salesperson'] = $this->input->post('selSales');
				$data['status_item'] = $this->input->post('selStatusItem');
				$data['tgl_awal'] = $this->input->post('txtTglAwal');
				$data['tgl_akhir'] = $this->input->post('txtTglAkhir');

				$table = $this->model_do->get_sales_report($data);
				$str = $this->generate_sales_report($table);
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function test_excel() {
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Dtulis dari sini!');

		$writer = new Xlsx($spreadsheet);
		$writer->save('assets/admin_assets/dist/laporan/report_sales.xlsx');
	}

	public function generate_sales_report($data = '') {
		$this->load->helper(array('my_helper'));

		$no = 0;
		$nrow = 1;
		$arr_tot_disc = array('0');
		$tot_harga = array('0');
		$tot_stuff = array('0');
		$style = array(
			'font' => array('bold' => true),
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,)
		);
		$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()
			->setCreator($_SESSION['nm_admin'])
			->setLastModifiedBy($_SESSION['nm_admin'])
			->setTitle('Report '.ucwords($data['tipe_report']).' '.ucwords($data['tipe_penjualan']).' Sales Periode '.$data['tgl_awal'].' / '.$data['tgl_akhir'])
			->setSubject('Report '.ucwords($data['tipe_report']).' '.ucwords($data['tipe_penjualan']).' Sales')
			->setDescription('Report data '.ucwords($data['tipe_report']).' '.ucwords($data['tipe_penjualan']).' Sales yang dihasilkan dari Sistem ERP')
			->setKeywords("office 2007 openxml php")
			->setCategory("Report");
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->getStyle('A1:P1')->applyFromArray($style);
		// $sheet->mergeCells('A1:L1')->setCellValue('A1', 'Report '.ucwords($data['tipe_report']).' '.ucwords($data['tipe_penjualan']).' Sales Periode '.$data['tgl_awal'].' / '.$data['tgl_akhir']);
		$sheet->setCellValue('A1', 'No');
		$sheet->setCellValue('B1', 'Tgl_invoice')->getColumnDimension('B')->setAutoSize(true);
		$sheet->setCellValue('C1', 'No Invoice')->getColumnDimension('C')->setAutoSize(true);
		$sheet->setCellValue('D1', 'Nama Customer')->getColumnDimension('D')->setAutoSize(true);
		$sheet->setCellValue('E1', 'Alamat Customer')->getColumnDimension('E')->setAutoSize(true);
		$sheet->setCellValue('F1', 'NPWP')->getColumnDimension('F')->setAutoSize(true);
		$sheet->setCellValue('G1', 'No PO')->getColumnDimension('G')->setAutoSize(true);
		$sheet->setCellValue('H1', 'Nama Salesperson')->getColumnDimension('H')->setAutoSize(true);
		$sheet->setCellValue('I1', 'Code Item')->getColumnDimension('I')->setAutoSize(true);
		$sheet->setCellValue('J1', 'Status Item')->getColumnDimension('J')->setAutoSize(true);
		$sheet->setCellValue('K1', 'Item_desc')->getColumnDimension('K')->setAutoSize(true);
		$sheet->setCellValue('L1', 'Qty')->getColumnDimension('L')->setAutoSize(true);
		$sheet->setCellValue('M1', 'Harga Barang')->getColumnDimension('M')->setAutoSize(true);
		$sheet->setCellValue('N1', 'Disc')->getColumnDimension('N')->setAutoSize(true);
		$sheet->setCellValue('O1', 'Total Disc')->getColumnDimension('O')->setAutoSize(true);
		$sheet->setCellValue('P1', 'Subtotal')->getColumnDimension('P')->setAutoSize(true);
		if (!empty($data['master'])) :
			foreach ($data['master'] as $row) :
				$currency_icon = $row->icon_type.' '.$row->currency_icon.' '.$row->pemisah_angka.' '.$row->format_akhir;

				if (!empty($data['parent_items'])) :
					foreach ($data['parent_items'] as $parent) :
						if ($row->kd_mdo == $parent->mdo_kd) :
							$no++;
							$nrow++;
							$harga_barang = $parent->harga_barang;

							/* == set default discount jika default discount pada item kosong == */
							if ($row->nm_pihak == 'Distributor') :
								$default_disc_tipe = empty($parent->disc_distributor)?$row->jml_disc:$parent->disc_distributor;
							elseif ($row->nm_pihak == 'Customer') :
								$default_disc_tipe = empty($parent->disc_customer)?$row->jml_disc:$parent->disc_customer;
							endif;

							$pro_price = prices_process($row->currency_type_retail, $row->currency_type, $parent->harga_retail, $harga_barang, $row->convert_value_retail, $row->convert_value, $data['tipe_penjualan'], $row->jml_disc, $parent->item_disc, 'percent', $parent->disc_type);
							$tot_disc = $pro_price['tot_disc'];
							$retail_price = $pro_price['retail_price'];
							$pure_disc = $pro_price['pure_disc'];
							$real_price = $pro_price['this_price'];

							$tots_disc = $pure_disc * $parent->stuffing_item_qty;
							$sub_total = $real_price * $parent->stuffing_item_qty;
							if ($data['tipe_penjualan'] == 'Ekspor') :
								$harga_barang = ($harga_barang < 0)?0:number_format($harga_barang, 2, '.', '');
								$pure_disc = ($pure_disc < 0)?0:number_format($pure_disc, 2, '.', '');
								$sub_total = ($sub_total < 0)?0:number_format($sub_total, 2, '.', '');
								$tots_disc = ($tots_disc < 0)?0:number_format($tots_disc, 2, '.', '');
							endif;

							$arr_tot_disc[] = $tots_disc;
							$tot_harga[] = $sub_total;
							$tot_harga_so[$row->msalesorder_kd][] = $sub_total;
							$tot_stuff[] = $parent->stuffing_item_qty;

							$sheet->setCellValue('A'.$nrow, $no);
							$sheet->setCellValue('B'.$nrow, $row->tgl_do);
							$sheet->setCellValue('C'.$nrow, $row->no_invoice);
							$sheet->setCellValue('D'.$nrow, $row->nm_customer);
							$sheet->setCellValue('E'.$nrow, $row->alamat);
							$sheet->setCellValue('F'.$nrow, $row->npwp_customer);
							$sheet->setCellValue('G'.$nrow, $row->no_po);
							$sheet->setCellValue('H'.$nrow, $row->nm_salesperson);
							$sheet->setCellValue('I'.$nrow, $parent->item_code);
							$sheet->setCellValue('J'.$nrow, $parent->item_status);
							$sheet->setCellValue('K'.$nrow, $parent->item_desc);
							$sheet->setCellValue('L'.$nrow, $parent->stuffing_item_qty);
							$sheet->setCellValue('M'.$nrow, $harga_barang);
							$sheet->setCellValue('N'.$nrow, $pure_disc);
							$sheet->setCellValue('O'.$nrow, $tots_disc);
							$sheet->setCellValue('P'.$nrow, $sub_total);
						endif;
					endforeach;
				endif;

				if (!empty($data['child_items'])) :
					foreach ($data['child_items'] as $child) :
						if ($row->kd_mdo == $child->mdo_kd) :
							$no++;
							$nrow++;
							$harga_barang = $child->harga_barang;

							/* == set default discount jika default discount pada item kosong == */
							if ($row->nm_pihak == 'Distributor') :
								$default_disc_tipe = empty($child->disc_distributor)?$row->jml_disc:$child->disc_distributor;
							elseif ($row->nm_pihak == 'Customer') :
								$default_disc_tipe = empty($child->disc_customer)?$row->jml_disc:$child->disc_customer;
							endif;

							$pro_price = prices_process($row->currency_type_retail, $row->currency_type, $child->harga_retail, $harga_barang, $row->convert_value_retail, $row->convert_value, $data['tipe_penjualan'], $row->jml_disc, $child->item_disc, 'percent', $child->disc_type);
							$tot_disc = $pro_price['tot_disc'];
							$retail_price = $pro_price['retail_price'];
							$pure_disc = $pro_price['pure_disc'];
							$real_price = $pro_price['this_price'];

							$tots_disc = $pure_disc * $child->stuffing_item_qty;
							$sub_total = $real_price * $child->stuffing_item_qty;
							if ($data['tipe_penjualan'] == 'Ekspor') :
								$harga_barang = ($harga_barang < 0)?0:number_format($harga_barang, 2, '.', '');
								$pure_disc = ($pure_disc < 0)?0:number_format($pure_disc, 2, '.', '');
								$sub_total = ($sub_total < 0)?0:number_format($sub_total, 2, '.', '');
								$tots_disc = ($tots_disc < 0)?0:number_format($tots_disc, 2, '.', '');
							endif;

							$arr_tot_disc[] = $tots_disc;
							$tot_harga[] = $sub_total;
							$tot_harga_so[$row->msalesorder_kd][] = $sub_total;
							$tot_stuff[] = $child->stuffing_item_qty;

							$sheet->setCellValue('A'.$nrow, $no);
							$sheet->setCellValue('B'.$nrow, $row->tgl_do);
							$sheet->setCellValue('C'.$nrow, $row->no_invoice);
							$sheet->setCellValue('D'.$nrow, $row->nm_customer);
							$sheet->setCellValue('E'.$nrow, $row->alamat);
							$sheet->setCellValue('F'.$nrow, $row->npwp_customer);
							$sheet->setCellValue('G'.$nrow, $row->no_po);
							$sheet->setCellValue('H'.$nrow, $row->nm_salesperson);
							$sheet->setCellValue('I'.$nrow, $child->item_code);
							$sheet->setCellValue('J'.$nrow, $child->item_status);
							$sheet->setCellValue('K'.$nrow, $child->item_desc);
							$sheet->setCellValue('L'.$nrow, $child->stuffing_item_qty);
							$sheet->setCellValue('M'.$nrow, $harga_barang);
							$sheet->setCellValue('N'.$nrow, $pure_disc);
							$sheet->setCellValue('O'.$nrow, $tots_disc);
							$sheet->setCellValue('P'.$nrow, $sub_total);
						endif;
					endforeach;
				endif;

				if (!empty($data['disc_lists'])) :
					foreach ($data['disc_lists'] as $tot) :
						if ($row->msalesorder_kd == $tot->msalesorder_kd) :
							$no++;
							$nrow++;

							if ($tot->type_kolom == 'nilai') :
								$jml_potongan = $tot->total_nilai;
							elseif ($tot->type_kolom == 'persen') :
								$jml_potongan = count_disc('percent', $tot->total_nilai, array_sum($tot_harga_so[$tot->msalesorder_kd]));
							
								if (empty($decimal) || $decimal == '0') :
									$jml_potongan = pembulatan_decimal($jml_potongan);
								elseif ($decimal == '1') :
									$jml_potongan = $jml_potongan;
								endif;
							endif;

							$sub_total = 0 - $jml_potongan;
							$arr_tot_disc[] = $jml_potongan;
							$tot_harga[] = $sub_total;

							$sheet->setCellValue('A'.$nrow, $no);
							$sheet->setCellValue('B'.$nrow, $row->tgl_do);
							$sheet->setCellValue('C'.$nrow, $row->no_invoice);
							$sheet->setCellValue('D'.$nrow, $row->nm_customer);
							$sheet->setCellValue('E'.$nrow, $row->alamat);
							$sheet->setCellValue('F'.$nrow, $row->npwp_customer);
							$sheet->setCellValue('G'.$nrow, $row->no_po);
							$sheet->setCellValue('H'.$nrow, $row->nm_salesperson);
							$sheet->setCellValue('I'.$nrow, $tot->nm_kolom);
							$sheet->setCellValue('J'.$nrow, '-');
							$sheet->setCellValue('K'.$nrow, 'Diskon');
							$sheet->setCellValue('L'.$nrow, '0');
							$sheet->setCellValue('M'.$nrow, '0');
							$sheet->setCellValue('N'.$nrow, $jml_potongan);
							$sheet->setCellValue('O'.$nrow, $jml_potongan);
							$sheet->setCellValue('P'.$nrow, $sub_total);
						endif;
					endforeach;
				endif;
			endforeach;
		else :
			$sheet->mergeCells('A4:K4')->setCellValue('A4', 'Lho kok kosong?!?');
		endif;
		// $lrow = $nrow + 1;
		// $tots_disc = array_sum($arr_tot_disc);
		// $total_harga = array_sum($tot_harga);
		// $total_stuff = array_sum($tot_stuff);
		// if ($data['tipe_penjualan'] == 'Ekspor') :
		// 	$total_harga = ($total_harga < 0)?0:number_format($total_harga, 2, '.', '');
		// endif;
		// if (!empty($data['master'])) :
		// 	$sheet->mergeCells('A'.$lrow.':F'.$lrow)->setCellValue('A'.$lrow, 'Total');
		// 	$sheet->setCellValue('H'.$lrow, $total_stuff);
		// 	$sheet->setCellValue('K'.$lrow, $tots_disc);
		// 	$sheet->setCellValue('L'.$lrow, $total_harga);
		// endif;

		$judul = 'Report '.ucwords($data['tipe_report']).' '.ucwords($data['tipe_penjualan']).' Sales Periode '.$data['tgl_awal'].' sd '.$data['tgl_akhir'];
		$url = 'assets/admin_assets/dist/laporan/'.$judul.'.xlsx';
		$writer = new Xlsx($spreadsheet);
		$writer->save($url);
		$return['tables'] = $url;
		$return['confirm'] = 'success';
		return $return;
	}
}