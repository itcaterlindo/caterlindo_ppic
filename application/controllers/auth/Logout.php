<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends MY_Controller {
	public function index() {
		$this->load->model('m_builder');
		$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' logout dari sistem');
		$this->session->sess_destroy();
		?>
		<script>
			window.location.replace('<?php echo base_url(); ?>');
		</script>
		<?php
	}
}