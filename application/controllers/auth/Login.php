<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'html', 'my_helper'));
		$this->load->library(array('session', 'form_validation'));
	}
	
	public function index() {
		// Cek Session
		$uri = $this->input->get('uri');
		$uri = !empty($uri) ? $uri : base_url().'dashboard';
		if (isset($this->session->kd_admin)) :
			?>
			<script type="text/javascript">
				// window.location.replace("<?php //echo base_url(); ?>dashboard");
				window.location.replace("<?php echo $uri; ?>");
			</script>
			<?php
		else :
			$this->load->model('tb_setting/m_setting');
			$data['header_title'] = $this->m_setting->getSetting('header_title');
			$data['main_title'] = $this->m_setting->getSetting('main_title');
			$data['home_title'] = $this->m_setting->getSetting('home_title');
			$data['favicon'] = $this->m_setting->getSetting('favicon');
			$data['login_title'] = $this->m_setting->getSetting('login_title');
			$data['thn_mulai'] = $this->m_setting->getSetting('thn_mulai');
			$data['footer_title'] = $this->m_setting->getSetting('footer_title');
			$data['login_bg_img'] = $this->m_setting->getSetting('login_bg_img');
			$data['uri'] = $uri;
			$this->session->sess_destroy();
			$this->load->view('page/login/v_login', $data);
		endif;
	}
	
	public function verifyLogin() {
		$this->load->model('m_builder');
		$this->load->model('tb_setting/m_setting');
		$ip_address = get_client_ip();
		$username = $this->input->post('txtUser');
		$password = $this->input->post('txtPass');

		$input_log = $this->m_builder->insert_log($ip_address, $ip_address.' mencoba akses login dengan username \''.$username.'\'');

		$this->form_validation->set_rules('txtUser', 'Username', 'required',
			array(
				'required' => '{field} tidak boleh kosong!'
			)
		);
		$this->form_validation->set_rules('txtPass', 'Password', 'required',
			array(
				'required' => '{field} tidak boleh kosong!'
			)
		);

		if ($this->form_validation->run() == FALSE) :
			$input_log = $this->m_builder->insert_log($ip_address, $ip_address.' gagal login dengan username \''.$username.'\'');

			$str['confirm'] = 'errValidation';
			$str['csrf'] = $this->security->get_csrf_hash();
			$str['idErrUsername'] = (!empty(form_error('txtUser')))?buildLabel('warning', form_error('txtUser', '"', '"')):'';
			$str['idErrPassword'] = (!empty(form_error('txtPass')))?buildLabel('warning', form_error('txtPass', '"', '"')):'';
		else :
			$this->db->where('username', $username);
			$this->db->where('admin_stsaktif', 1);
			$this->db->from('tb_admin');
			$q_cek = $this->db->get();
			$j_cek = $q_cek->num_rows();
			if ($j_cek > 0) :
				$r_cek = $q_cek->row();
				$pass_admin = $r_cek->password;
				if (getHash($password, $pass_admin)) :
					$input_log = $this->m_builder->insert_log($r_cek->kd_admin, $r_cek->kd_admin.' berhasil login kesistem');

            		$str['confirm'] = 'success';
					$q_tipe_admin = $this->m_builder->getRow(
						'td_admin_tipe', array('kd_tipe_admin' => $r_cek->tipe_admin_kd), 'row'
					);

					// Mengambil data detail karyawan di db_sim_hrm
					$db_hrm = $this->load->database('sim_hrm', TRUE);
					$kd_karyawan = $r_cek->kd_karyawan;
					$user_no_img = $this->m_setting->getSetting('no_profile_img');
					$no_pic = base_url('assets/admin_assets/dist/img/setting_img/'.$user_no_img);
					if ($kd_karyawan != '-') :
						$db_hrm->where('kd_karyawan', $kd_karyawan);
						$db_hrm->from('tb_karyawan');
						$q_karyawan = $db_hrm->get();
						$j_karyawan = $q_karyawan->num_rows();
						$r_karyawan = $q_karyawan->row();
						if ($j_karyawan > 0) :
							// Check apakah file sudah ada disistem
							$path = FCPATH . 'assets/admin_assets/dist/img/users/';
							$img_path = $path.$r_karyawan->pas_foto;
							if (!empty($r_karyawan->pas_foto) && is_file($img_path)
								&& getimagesize($img_path)
								&& ($this->security->xss_clean($r_karyawan->pas_foto, TRUE) == TRUE)
							) :
								$img_path = base_url('assets/admin_assets/dist/img/users/' . $r_karyawan->pas_foto);
								$profile_img = $img_path;
							else :
								$img_path_ori = FCPATH . '../caterlindo_hrm/assets/img/karyawan/'.$r_karyawan->pas_foto;

								if (!empty($r_karyawan->pas_foto) && is_file($img_path_ori)
									&& getimagesize($img_path_ori)
									&& ($this->security->xss_clean($r_karyawan->pas_foto, TRUE) == TRUE)
								) :
									$this->m_setting->delUnusedImg('assets/admin_assets/dist/img/users/', 'tb_karyawan', 'pas_foto');
									copy($img_path_ori, $img_path);
									$img_path = base_url('assets/admin_assets/dist/img/users/' . $r_karyawan->pas_foto);
									$profile_img = $img_path;
								else :
									$profile_img = $no_pic;
								endif;
							endif;
						else :
							$profile_img = $no_pic;
						endif;
					else :
						$profile_img = $no_pic;
					endif;

					$userdata = array(
						'kd_admin' => $r_cek->kd_admin,
						'tipe_admin_kd' => $r_cek->tipe_admin_kd,
						'kd_karyawan' => $r_cek->kd_karyawan,
						'username' => $r_cek->username,
						'nm_admin' => $r_cek->nm_admin,
						'tipe_admin' => $q_tipe_admin->nm_tipe_admin,
						'kd_manage_items' => $q_tipe_admin->kd_manage_items,
						'kd_access_img' => $q_tipe_admin->kd_access_img,
						'kd_halaman' => $q_tipe_admin->kd_halaman,
						'kd_view_logs' => $q_tipe_admin->kd_view_logs,
						'create_access' => $q_tipe_admin->create,
						'read_access' => $q_tipe_admin->read,
						'update_access' => $q_tipe_admin->update,
						'delete_access' => $q_tipe_admin->delete,
						'upload_product' => $q_tipe_admin->upload_product,
						'upload_sketch' => $q_tipe_admin->upload_sketch,
						'view_diskon_distributor' => $q_tipe_admin->view_diskon_distributor,
						'view_diskon_customer' => $q_tipe_admin->view_diskon_customer,
						'view_detail_report' => $q_tipe_admin->view_detail_report,
						'profile_img' => $profile_img,
						'session_id' => session_id(),
					);
					$this->session->set_userdata($userdata);
            		$str['msg'] = 'Selamat Datang '.$this->session->userdata('nm_admin').'!';
				else :
					$input_log = $this->m_builder->insert_log($ip_address, $ip_address.' gagal login kesistem dengan username \''.$username.'\', karena password tidak cocok');
					$str['confirm'] = 'errValidation';
					$str['csrf'] = $this->security->get_csrf_hash();
					$str['idErrUsername'] = '';
					$str['idErrPassword'] = buildLabel('danger', 'Password tidak cocok dengan username!');
				endif;
			else :
				$input_log = $this->m_builder->insert_log($ip_address, $ip_address.' gagal login kesistem dengan username \''.$username.'\', karena username tidak cocok');
				$str['confirm'] = 'errValidation';
				$str['csrf'] = $this->security->get_csrf_hash();
				$str['idErrUsername'] = buildLabel('danger', 'Username anda tidak cocok dengan akun manapun!');
				$str['idErrPassword'] = '';
			endif;
		endif;

	 	header('Content-Type: application/json');
		echo json_encode($str);
	}
}