<?php
defined('BASEPATH') or exit('No direct script access allowed!');
 
require_once APPPATH."third_party/phpspreadsheets/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Stock_report extends MY_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->helper(array('my_helper', 'url'));
		$this->load->model(array('db_caterlindo/tm_stock', 'tm_finishgood'));
	}

	public function report_today() {
		$report = $this->tm_stock->get_report_stock();

		$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()
			->setCreator($_SESSION['nm_admin'])
			->setLastModifiedBy($_SESSION['nm_admin'])
			->setTitle('Report Stock '.date('d-M-Y'))
			->setSubject('Report Stock')
			->setDescription('Report Stock ini dihasilkan oleh controller \'report/stock_report\'')
			->setKeywords('office 2007 xlsx php spreadsheet stock_report inventory')
			->setCategory('Report');
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		$sheet = $spreadsheet->getActiveSheet();
		$thStyle = array(
			'font' => array('bold' => true),
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,)
		);
		$tbodyStyle = array(
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,)
		);
		$headStyles = array(
			'font' => array('bold' => true, 'size' => '16'),
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER),
		);
		$headerStyles = array(
			'font' => array('bold' => true)
		);
		$mulai_header = 1;
		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->mergeCells('A'.$mulai_header.':G'.$mulai_header)->setCellValue('A'.$mulai_header, 'REPORT STOCK FINISH GOOD '.date('d-M-Y'))->getStyle('A'.$mulai_header)->applyFromArray($headStyles);

		$no_cell = $mulai_header + 2;
		$mulai_cell = $no_cell;
		$no = 0;
		$sheet->setCellValue('A'.$mulai_cell, 'No')->getStyle('A'.$mulai_cell)->applyFromArray($thStyle);
		$sheet->setCellValue('B'.$mulai_cell, 'Kode Barang')->getStyle('B'.$mulai_cell)->applyFromArray($thStyle);
		$sheet->setCellValue('C'.$mulai_cell, 'Nama Stock')->getStyle('C'.$mulai_cell)->applyFromArray($thStyle);
		$sheet->setCellValue('D'.$mulai_cell, 'No Batch')->getStyle('D'.$mulai_cell)->applyFromArray($thStyle);
		$sheet->setCellValue('E'.$mulai_cell, 'Qty Data')->getStyle('E'.$mulai_cell)->applyFromArray($thStyle);
		$sheet->setCellValue('F'.$mulai_cell, 'Qty Kartu Stock')->getStyle('F'.$mulai_cell)->applyFromArray($thStyle);
		$sheet->setCellValue('G'.$mulai_cell, 'Selisih')->getStyle('G'.$mulai_cell)->applyFromArray($thStyle);
		foreach ($report as $row) :
			$no++;
			$mulai_cell++;
			$qty_data = !empty($row->fn_qty)?$row->fn_qty:'0';
			$qty_sisa = !empty($row->fn_sisa)?$row->fn_sisa:'0';
			$selisih = $qty_data - $qty_sisa;
			$sheet->setCellValue('A'.$mulai_cell, $no)->getStyle('A'.$mulai_cell)->applyFromArray($tbodyStyle);
			$sheet->setCellValue('B'.$mulai_cell, $row->fc_kdstock)->getStyle('B'.$mulai_cell)->applyFromArray(array('alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)));
			$sheet->setCellValue('C'.$mulai_cell, $row->fv_namastock);
			$sheet->setCellValue('D'.$mulai_cell, $row->fc_barcode)->getStyle('D'.$mulai_cell)->applyFromArray($tbodyStyle);
			$sheet->setCellValue('E'.$mulai_cell, $qty_data)->getStyle('E'.$mulai_cell)->applyFromArray($tbodyStyle);
			$sheet->setCellValue('F'.$mulai_cell, $qty_sisa)->getStyle('F'.$mulai_cell)->applyFromArray($tbodyStyle);
			if ($selisih > 0) :
				$sheet->setCellValue('G'.$mulai_cell, '=E'.$mulai_cell.'-F'.$mulai_cell)->getStyle('G'.$mulai_cell)->applyFromArray($tbodyStyle)->getFill()
					->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
					->getStartColor()->setARGB('FF0000');
			else :
				$sheet->setCellValue('G'.$mulai_cell, '=E'.$mulai_cell.'-F'.$mulai_cell)->getStyle('G'.$mulai_cell)->applyFromArray($tbodyStyle);
			endif;
		endforeach;
		$akhir_cell = $mulai_cell + 1;
		$sheet->mergeCells('A'.$akhir_cell.':D'.$akhir_cell)->setCellValue('A'.$akhir_cell, 'Total Qty')->getStyle('A'.$akhir_cell)->applyFromArray($tbodyStyle);
		$sheet->setCellValue('E'.$akhir_cell, '=SUM(E'.$no_cell.':E'.$mulai_cell.')')->getStyle('E'.$akhir_cell)->applyFromArray($tbodyStyle);
		$sheet->setCellValue('F'.$akhir_cell, '=SUM(F'.$no_cell.':F'.$mulai_cell.')')->getStyle('F'.$akhir_cell)->applyFromArray($tbodyStyle);
		$sheet->setCellValue('G'.$akhir_cell, '=SUM(G'.$no_cell.':G'.$mulai_cell.')')->getStyle('G'.$akhir_cell)->applyFromArray($tbodyStyle);

		$styleArray = [
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
				],
			],
		];
		$sheet->getStyle('A'.$no_cell.':G'.$akhir_cell)->applyFromArray($styleArray);

		$filename = 'report_stock_'.date('d-M-Y').'.xlsx';
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='. $filename .'');
		header('Cache-Control: max-age=0');
		$writer->save('php://output');
	}

	public function report_today_baru () {
		$act = $this->tm_finishgood->get_all()->result_array();
		
		$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()
			->setCreator($_SESSION['nm_admin'])
			->setLastModifiedBy($_SESSION['nm_admin'])
			->setTitle('Report Stock '.date('d-M-Y'))
			->setSubject('Report Stock')
			->setCategory('Report');
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		$sheet = $spreadsheet->getActiveSheet();
		$sheet = $spreadsheet->getActiveSheet();
		$header_cell = 1;
		$sheet->setCellValue('A'.$header_cell, 'No')->getStyle('A'.$header_cell);
		$sheet->setCellValue('B'.$header_cell, 'Product Code')->getStyle('B'.$header_cell);
		$sheet->setCellValue('C'.$header_cell, 'Nama Barang')->getStyle('C'.$header_cell);
		$sheet->setCellValue('D'.$header_cell, 'Qty')->getStyle('D'.$header_cell);

		$dataCell = 2;
		$no = 1;

		foreach ($act as $each):
			$sheet->setCellValue('A'.$dataCell, $no)->getStyle('A'.$dataCell);
			$sheet->setCellValue('B'.$dataCell, $each['item_code'])->getStyle('B'.$dataCell);
			$sheet->setCellValue('C'.$dataCell, $each['deskripsi_barang'].' '.$each['dimensi_barang'])->getStyle('C'.$dataCell);
			$sheet->setCellValue('D'.$dataCell, $each['fg_qty'])->getStyle('D'.$dataCell);

			$dataCell++;
			$no++;
		endforeach;


		$filename = 'report_stock_baru_'.date('d-M-Y').'.xlsx';
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='. $filename .'');
		header('Cache-Control: max-age=0');
		$writer->save('php://output');

	}
}