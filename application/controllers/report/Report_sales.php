<?php
defined('BASEPATH') or exit('No direct script access allowed!');
 
require_once APPPATH."third_party/phpspreadsheets/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Report_sales extends MY_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->helper(array('my_helper'));
		$this->load->model(array('tm_deliveryorder', 'model_do', 'model_salesorder', 'm_format_laporan', 'm_salesorder', 'tb_default_disc', 'td_salesorder_item_disc'));
	}

	private function get_invoice_kd($kd_mdo = '') {
		$no_invoice = $this->tm_deliveryorder->get_no_invoice($kd_mdo);
		$lists_kd = $this->tm_deliveryorder->get_same_kd($no_invoice);
		if (!empty($lists_kd)) :
			$no = 0;
			foreach ($lists_kd as $list) :
				$no++;
				if ($no == 1) :
					$first_kd = $list->kd_mdo;
				else :
					$first_kd = $first_kd;
				endif;
				$list_kd[] = $list->kd_mdo;
				$list_kdso[] = $list->msalesorder_kd;
			endforeach;
		else :
			$list_kd = array('');
			$list_kdso = array('');
			$first_kd = '';
		endif;
		$data = array('first_kd' => $first_kd, 'list_kd' => $list_kd, 'list_kdso' => $list_kdso);
		return $data;
	}

	private function get_invoice_header($first_kd = '') {
		$headers = $this->model_do->get_header_detail($first_kd);
		return $headers;
	}

	private function get_invoice_file($tipe_do = '') {
		if ($tipe_do == 'Ekspor') :
			$invoice_tipe = 'invoiceekspor';
			$invoice_file = 'inv_do/ekspor/v_inv_ekspor';
		elseif ($tipe_do == 'Lokal') :
			$invoice_tipe = 'invoicelocal';
			$invoice_file = 'inv_do/lokal/v_inv_lokal';
		endif;
		$data = array('invoice_tipe' => $invoice_tipe, 'invoice_file' => $invoice_file);
		return $data;
	}

	private function get_so_masterdata($kd_msalesorder = '') {
		$data = $this->model_salesorder->read_masterdata($kd_msalesorder);
		return $data;
	}

	public function invoice_report($tipe_invoice = '', $kd_mdo = '') {
		/* ==All of invoice variables will be writen below this comment== */
		$invoice_kd = $this->get_invoice_kd($kd_mdo);
		$headers = $this->get_invoice_header($invoice_kd['first_kd']);
		$invoice_file = $this->get_invoice_file($headers['tipe_do']);
		$so_master = $this->get_so_masterdata($headers['msalesorder_kd']);

		$format_laporan = $this->m_format_laporan->view_laporan($invoice_file['invoice_tipe'], $so_master['term_payment_format']);
		$master_head = $headers;
		$parent_items = $this->model_do->list_parent_items($invoice_kd['list_kd'], '', $headers['tipe_do']);
		$child_items = $this->model_do->list_child_items($invoice_kd['list_kd'], '', $headers['tipe_do']);
		$tot_potongan = $this->m_salesorder->get_price($invoice_kd['list_kdso']);
		$tipe_invoice = $tipe_invoice;
		$kd_mdo = $kd_mdo;
		$def_discs = $this->td_salesorder_item_disc->get_where_array_so($invoice_kd['list_kdso'], array('nm_pihak' => $tipe_invoice));

		/* ==This is where all the datas going, they will be here being processed etc etc== */
		$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()
			->setCreator($_SESSION['nm_admin'])
			->setLastModifiedBy($_SESSION['nm_admin'])
			->setTitle('Report Invoice '.ucwords($tipe_invoice).' '.$headers['no_invoice'])
			->setSubject('Report Invoice')
			->setDescription('Report Invoice ini dihasilkan oleh controller \'report_sales/invoice_report\'')
			->setKeywords('office 2007 xlsx php spreadsheet invoice report sales')
			->setCategory('Report');
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		$sheet = $spreadsheet->getActiveSheet();
		$style = array(
			'font' => array('bold' => true),
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,)
		);
		$headerStyles = array(
			'font' => array('bold' => true)
		);
		$mulai_header = 1;
		$sheet->mergeCells('A'.$mulai_header.':B'.$mulai_header)->setCellValue('A'.$mulai_header, 'Invoice To :')->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
		$sheet->mergeCells('C'.$mulai_header.':D'.$mulai_header)->setCellValue('C'.$mulai_header, $headers['nm_cust'])->getStyle('C'.$mulai_header)->applyFromArray($headerStyles);
		if (!empty($headers['nm_contact_cust'])) :
			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('C'.$mulai_header.':D'.$mulai_header)->setCellValue('C'.$mulai_header, $headers['nm_contact_cust']);
		endif;
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('C'.$mulai_header.':D'.$mulai_header)->setCellValue('C'.$mulai_header, str_replace('<br>', ' ', $headers['alamat_cust']));
		$spreadsheet->getActiveSheet()->getStyle('C'.$mulai_header)->getAlignment()->setWrapText(true);
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('A'.$mulai_header.':B'.$mulai_header)->setCellValue('A'.$mulai_header, 'Notify Party :')->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
		$sheet->mergeCells('C'.$mulai_header.':D'.$mulai_header)->setCellValue('C'.$mulai_header, $headers['notify_name'])->getStyle('C'.$mulai_header)->applyFromArray($headerStyles);
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('C'.$mulai_header.':D'.$mulai_header)->setCellValue('C'.$mulai_header, str_replace('<br>', ' ', $headers['notify_address']));
		$spreadsheet->getActiveSheet()->getStyle('C'.$mulai_header)->getAlignment()->setWrapText(true);
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('A'.$mulai_header.':B'.$mulai_header)->setCellValue('A'.$mulai_header, 'Delivery Address :')->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
		$sheet->mergeCells('C'.$mulai_header.':D'.$mulai_header)->setCellValue('C'.$mulai_header, $headers['nm_penerima'])->getStyle('C'.$mulai_header)->applyFromArray($headerStyles);
		$sheet->setCellValue('E'.$mulai_header, 'Date :');
		$sheet->setCellValue('G'.$mulai_header, format_date($headers['tgl_do'], 'd M Y'));
		if (!empty($headers['nm_contact_cust'])) :
			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('C'.$mulai_header.':D'.$mulai_header)->setCellValue('C'.$mulai_header, $headers['nm_contact_penerima']);
		endif;
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('C'.$mulai_header.':D'.$mulai_header)->setCellValue('C'.$mulai_header, str_replace('<br>', ' ', $headers['alamat_penerima']));
		$spreadsheet->getActiveSheet()->getStyle('C'.$mulai_header)->getAlignment()->setWrapText(true);
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('A'.$mulai_header.':B'.$mulai_header)->setCellValue('A'.$mulai_header, 'Freight By :')->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
		$sheet->mergeCells('C'.$mulai_header.':D'.$mulai_header)->setCellValue('C'.$mulai_header, $headers['nm_jasakirim'])->getStyle('C'.$mulai_header)->applyFromArray($headerStyles);
		if (!empty($headers['no_po'])) :
			$sheet->setCellValue('E'.$mulai_header, 'Order No :');
			$sheet->setCellValue('G'.$mulai_header, $headers['no_po']);
		endif;
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('C'.$mulai_header.':D'.$mulai_header)->setCellValue('C'.$mulai_header, $headers['alamat_jasakirim']);
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('A'.$mulai_header.':B'.$mulai_header)->setCellValue('A'.$mulai_header, 'Description of goods :')->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
		$sheet->setCellValue('C'.$mulai_header, 'Stainless Steel Goods');
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('A'.$mulai_header.':B'.$mulai_header)->setCellValue('A'.$mulai_header, 'Number of boxs :')->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
		$sheet->setCellValue('C'.$mulai_header, $headers['jml_koli'])->getStyle('C'.$mulai_header)
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

		$mulai_tbheader = $mulai_header + 2;
		$sheet->getStyle('A'.$mulai_tbheader.':G'.$mulai_tbheader)->applyFromArray($style);
		/* ==SET EXCEL HEADER== */
		$sheet->setCellValue('A'.$mulai_tbheader, 'No')->getColumnDimension('A')->setAutoSize(true);
		$sheet->setCellValue('B'.$mulai_tbheader, 'Prod Code')->getColumnDimension('B')->setAutoSize(true);
		$sheet->setCellValue('C'.$mulai_tbheader, 'Description')->getColumnDimension('C')->setAutoSize(true);
		$sheet->setCellValue('D'.$mulai_tbheader, 'Dimension')->getColumnDimension('D')->setAutoSize(true);
		$sheet->setCellValue('E'.$mulai_tbheader, 'Qty')->getColumnDimension('E')->setAutoSize(true);
		$sheet->setCellValue('F'.$mulai_tbheader, 'Price/Unit')->getColumnDimension('F')->setAutoSize(true);
		$sheet->setCellValue('G'.$mulai_tbheader, 'Subtotal')->getColumnDimension('G')->setAutoSize(true);

		/* ==SET EXCEL ITEM DETAIL== */
		if (!empty($def_discs)) :
			foreach ($def_discs as $row) :
				$item_discs[$row->so_item_kd] = array('jml_disc' => $row->jml_disc, 'type_individual_disc' => $row->type_individual_disc, 'jml_individual_disc' => $row->jml_individual_disc);	
			endforeach;
		endif;
		$def_disc_set = $this->tb_default_disc->select_where(array('jenis_customer_kd' => $headers['kd_jenis_customer'], 'nm_pihak' => $tipe_invoice));

		$mulai_isi = $mulai_tbheader + 1;
		$no = 0;
		$no_cell_child = 0;
		$no_cell_child_alone = 0;
		foreach($parent_items as $parent) :
			$no_child = 0;
			$rows = '';
			foreach ($child_items as $child) :
				if ($parent->parent_kd == $child->parent_kd) :
					$no_child++;
				endif;
				$rowspan = $no_cell + $no_child;
			endforeach;
			$no_cell = $mulai_isi + $no;
			$no++;

			if ($tipe_invoice == 'customer') :
				$disc_tipe_invoice = $parent->disc_customer != 'kosong'?$parent->disc_customer:$def_disc_set->jml_disc;
			elseif ($tipe_invoice == 'distributor') :
				$disc_tipe_invoice = $parent->disc_distributor != 'kosong'?$parent->disc_distributor:$def_disc_set->jml_disc;
			endif;
			$jml_disc = isset($item_discs[$parent->parent_kd]['jml_disc'])?$item_discs[$parent->parent_kd]['jml_disc']:$disc_tipe_invoice;
			$type_individual_disc = isset($item_discs[$parent->parent_kd]['type_individual_disc'])?$item_discs[$parent->parent_kd]['type_individual_disc']:'decimal';
			$jml_individual_disc = isset($item_discs[$parent->parent_kd]['jml_individual_disc'])?$item_discs[$parent->parent_kd]['jml_individual_disc']:'0';
			$item_price_retail = $parent->harga_barang;

			$jml_default_disc = count_disc('percent', $jml_disc, $item_price_retail);
			$item_price = $item_price_retail - $jml_default_disc;
			$val_individual_disc = count_disc($type_individual_disc, $jml_individual_disc, $item_price);
			$item_price = $item_price - $val_individual_disc;
					
			$disc_type = $parent->disc_type;
			$disc_val = $parent->item_disc;
			$item_disc = count_disc($disc_type, $disc_val, $item_price);
			$item_price = $item_price - $item_disc;
			$total_disc = $item_price_retail - $item_price;

			if ($tipe_invoice == 'customer') :
				$item_disc_customer = empty($parent->disc_customer)?'0':$parent->disc_customer;
				if ($jml_disc > $def_disc_set->jml_disc) :
					$item_price_display = $item_price;
					$total_disc = 0;
				else :
					$item_price_display = $item_price_retail;
				endif;
				$item_price_display = $item_price_display;
				$total_harga = $item_price_display * $parent->stuffing_item_qty;
				$tot_harga[] = round($total_harga, 2);
			else :
				$item_price = round($item_price, 2);
				$total_harga = $item_price * $parent->stuffing_item_qty;
				$tot_harga[] = round($total_harga, 2);
			endif;
			$tot_disc[] = $total_disc * $parent->stuffing_item_qty;
			$tot_qty[] = $parent->stuffing_item_qty;

			$spreadsheet->getActiveSheet()->getStyle('A'.$no_cell.':B'.$no_cell)
				->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
			$spreadsheet->getActiveSheet()->getStyle('D'.$no_cell.':G'.$no_cell)
				->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
			if (isset($rowspan)) :
				$sheet->mergeCells('A'.$no_cell.':A'.$rowspan)->setCellValue('A'.$no_cell, $no);
				unset($rowspan);
			else :
				$sheet->setCellValue('A'.$no_cell, $no);
			endif;
			$sheet->setCellValue('B'.$no_cell, $parent->item_code);
			$sheet->setCellValue('C'.$no_cell, $parent->item_desc);
			$sheet->setCellValue('D'.$no_cell, $parent->item_dimension);
			$sheet->setCellValue('E'.$no_cell, $parent->stuffing_item_qty);
			if ($tipe_invoice == 'customer') :
				$sheet->setCellValue('F'.$no_cell, round($item_price_display, 2));
				$sheet->setCellValue('G'.$no_cell, round($total_harga, 2));
			else :
				$sheet->setCellValue('F'.$no_cell, round($item_price, 2));
				$sheet->setCellValue('G'.$no_cell, round($total_harga, 2));
			endif;

			foreach ($child_items as $child) :
				if (!empty($child->parent_kd) && $parent->parent_kd == $child->parent_kd) :
					$no_cell_child++;
					$no_cell = $no_cell + $no_cell_child;

					if ($tipe_invoice == 'customer') :
						$disc_tipe_invoice = $child->disc_customer != 'kosong'?$child->disc_customer:$def_disc_set->jml_disc;
					elseif ($tipe_invoice == 'distributor') :
						$disc_tipe_invoice = $child->disc_distributor != 'kosong'?$child->disc_distributor:$def_disc_set->jml_disc;
					endif;
					$jml_disc = isset($item_discs[$child->child_kd]['jml_disc'])?$item_discs[$child->child_kd]['jml_disc']:$disc_tipe_invoice;
					$type_individual_disc = isset($item_discs[$child->child_kd]['type_individual_disc'])?$item_discs[$child->child_kd]['type_individual_disc']:'decimal';
					$jml_individual_disc = isset($item_discs[$child->child_kd]['jml_individual_disc'])?$item_discs[$child->child_kd]['jml_individual_disc']:'0';
					$item_price_retail = $child->harga_barang;

					$jml_default_disc = count_disc('percent', $jml_disc, $item_price_retail);
					$item_price = $item_price_retail - $jml_default_disc;
					$val_individual_disc = count_disc($type_individual_disc, $jml_individual_disc, $item_price);
					$item_price = $item_price - $val_individual_disc;
							
					$disc_type = $child->disc_type;
					$disc_val = $child->item_disc;
					$item_disc = count_disc($disc_type, $disc_val, $item_price);
					$item_price = $item_price - $item_disc;
					$total_disc = $item_price_retail - $item_price;
			
					if ($tipe_invoice == 'customer') :
						$item_disc_customer = empty($child->disc_customer)?'0':$child->disc_customer;
						if ($jml_disc > $def_disc_set->jml_disc) :
							$item_price_display = $item_price;
							$total_disc = 0;
						else :
							$item_price_display = $item_price_retail;
						endif;
						$item_price_display = $item_price_display;
						$total_harga = $item_price_display * $child->stuffing_item_qty;
						$tot_harga[] = round($total_harga, 2);
					else :
						$item_price = round($item_price, 2);
						$total_harga = $item_price * $child->stuffing_item_qty;
						$tot_harga[] = round($total_harga, 2);
					endif;
					$tot_disc[] = $total_disc * $child->stuffing_item_qty;
					$tot_qty[] = $child->stuffing_item_qty;

					$sheet->setCellValue('B'.$no_cell, $child->item_code);
					$sheet->setCellValue('C'.$no_cell, $child->item_desc);
					$sheet->setCellValue('D'.$no_cell, $child->item_dimension);
					$sheet->setCellValue('E'.$no_cell, $child->stuffing_item_qty);
					if ($tipe_invoice == 'customer') :
						$sheet->setCellValue('F'.$no_cell, round($item_price_display, 2));
						$sheet->setCellValue('G'.$no_cell, round($total_harga, 2));
					else :
						$sheet->setCellValue('F'.$no_cell, round($item_price, 2));
						$sheet->setCellValue('G'.$no_cell, round($total_harga, 2));
					endif;
				else :
					$child_data['arr_child']['parent_kd'][] = $child->parent_kd;
					$child_data['arr_child']['child_kd'][] = $child->child_kd;
					$child_data['arr_child']['harga_retail'][] = $child->harga_retail;
					$child_data['arr_child']['harga_barang'][] = $child->harga_barang;
					$child_data['arr_child']['disc_type'][] = $child->disc_type;
					$child_data['arr_child']['item_disc'][] = $child->item_disc;
					$child_data['arr_child']['item_status'][] = $child->item_status;
					$child_data['arr_child']['stuffing_item_qty'][] = $child->stuffing_item_qty;
					$child_data['arr_child']['item_code'][] = $child->item_code;
					$child_data['arr_child']['item_desc'][] = $child->item_desc;
					$child_data['arr_child']['item_dimension'][] = $child->item_dimension;
					$child_data['arr_child']['disc_customer'][] = $child->disc_customer;
					$child_data['arr_child']['disc_distributor'][] = $child->disc_distributor;
				endif;
			endforeach;
		endforeach;
		if (isset($child_data)) :
			for ($i = 0; $i < count($child_data); $i++) :
				if (empty($child_data['arr_child']['parent_kd'][$i])) :
					$no++;
					$no_cell_child_alone++;
					$no_cell = $no_cell + $no_cell_child_alone;

					if ($tipe_invoice == 'customer') :
						$disc_tipe_invoice = $child_data['arr_child']['disc_customer'][$i] != 'kosong'?$child_data['arr_child']['disc_customer'][$i]:$def_disc_set->jml_disc;
					elseif ($tipe_invoice == 'distributor') :
						$disc_tipe_invoice = $child_data['arr_child']['disc_distributor'][$i] != 'kosong'?$child_data['arr_child']['disc_distributor'][$i]:$def_disc_set->jml_disc;
					endif;
					$jml_disc = isset($item_discs[$child_data['arr_child']['child_kd'][$i]]['jml_disc'])?$item_discs[$child_data['arr_child']['child_kd'][$i]]['jml_disc']:$disc_tipe_invoice;
					$type_individual_disc = isset($item_discs[$child_data['arr_child']['child_kd'][$i]]['type_individual_disc'])?$item_discs[$child_data['arr_child']['child_kd'][$i]]['type_individual_disc']:'decimal';
					$jml_individual_disc = isset($item_discs[$child_data['arr_child']['child_kd'][$i]]['jml_individual_disc'])?$item_discs[$child_data['arr_child']['child_kd'][$i]]['jml_individual_disc']:'0';
					$item_price_retail = $child_data['arr_child']['harga_barang'][$i];

					$jml_default_disc = count_disc('percent', $jml_disc, $item_price_retail);
					$item_price = $item_price_retail - $jml_default_disc;
					$val_individual_disc = count_disc($type_individual_disc, $jml_individual_disc, $item_price);
					$item_price = $item_price - $val_individual_disc;
							
					$disc_type = $child_data['arr_child']['disc_type'][$i];
					$disc_val = $child_data['arr_child']['item_disc'][$i];
					$item_disc = count_disc($disc_type, $disc_val, $item_price);
					$item_price = $item_price - $item_disc;
					$total_disc = $item_price_retail - $item_price;
			
					if ($tipe_invoice == 'customer') :
						$item_disc_customer = empty($child_data['arr_child']['disc_customer'][$i])?'0':$child_data['arr_child']['disc_customer'][$i];
						if ($jml_disc > $def_disc_set->jml_disc) :
							$item_price_display = $item_price;
							$total_disc = 0;
						else :
							$item_price_display = $item_price_retail;
						endif;
						$item_price_display = $item_price_display;
						$total_harga = $item_price_display * $child->stuffing_item_qty;
						$tot_harga[] = round($total_harga, 2);
					else :
						$item_price = round($item_price, 2);
						$total_harga = $item_price * $child->stuffing_item_qty;
						$tot_harga[] = round($total_harga, 2);
					endif;
					$tot_disc[] = $total_disc * $child_data['arr_child']['stuffing_item_qty'][$i];
					$tot_qty[] = $child_data['arr_child']['stuffing_item_qty'][$i];

					$sheet->setCellValue('A'.$no_cell, $no);
					$sheet->setCellValue('B'.$no_cell, $child_data['arr_child']['item_code'][$i]);
					$sheet->setCellValue('C'.$no_cell, $child_data['arr_child']['item_desc'][$i]);
					$sheet->setCellValue('D'.$no_cell, $child_data['arr_child']['item_dimension'][$i]);
					$sheet->setCellValue('E'.$no_cell, $child_data['arr_child']['stuffing_item_qty'][$i]);
					if ($tipe_invoice == 'customer') :
						$sheet->setCellValue('F'.$no_cell, round($item_price_display, 2));
						$sheet->setCellValue('G'.$no_cell, round($total_harga, 2));
					else :
						$sheet->setCellValue('F'.$no_cell, round($item_price, 2));
						$sheet->setCellValue('G'.$no_cell, round($total_harga, 2));
					endif;
				endif;
			endfor;
		endif;

		$last_cell = $no_cell;
		$tot_pots = 0;
		$tots_qty = array_sum($tot_qty);
		$tots_disc = array_sum($tot_disc);
		$tots_harga = array_sum($tot_harga);
		$jml_pot = count($tot_potongan);
		$jml_add = $jml_pot > 0?1:0;
		$jml_rowspan = 7 + $jml_pot + $jml_add;
		$jml_rowspan_det = ($jml_pot > 0)?0:7 + $jml_pot;
		$no = 0;
		foreach ($tot_potongan as $tot) :
			if ($tot->type_kolom == 'nilai') :
				$jml_potongan = $tot->total_nilai;
			elseif ($tot->type_kolom == 'persen') :
				$jml_potongan = count_disc('percent', $tot->total_nilai, $tots_harga);
			
				if (empty($decimal) || $decimal == '0') :
					$jml_potongan = pembulatan_decimal($jml_potongan);
				elseif ($decimal == '1') :
					$jml_potongan = $jml_potongan;
				endif;
			endif;
			$tots_pot[] = $jml_potongan;
		endforeach;
		if (isset($tots_pot)) :
			$tot_pots = array_sum($tots_pot);
		endif;
		$total_pot = $tots_disc + $tot_pots;
		if ($tipe_invoice == 'customer') :
			$grand_tot = $tots_harga - $total_pot;
		else :
			$grand_tot = $tots_harga;
		endif;
		$no_cell = $no_cell + 1;

		if ($tipe_invoice == 'customer') :
			$spreadsheet->getActiveSheet()->getStyle('E'.$no_cell.':G'.$no_cell)
				->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
			$sheet->mergeCells('A'.$no_cell.':C'.$no_cell);
			$sheet->setCellValue('D'.$no_cell, 'Sub Total')->getStyle('D'.$no_cell)->applyFromArray($headerStyles);
			$sheet->setCellValue('G'.$no_cell, $tots_harga)->getStyle('G'.$no_cell)->applyFromArray($headerStyles);
			$no_cell = $no_cell + 1;
			$spreadsheet->getActiveSheet()->getStyle('E'.$no_cell.':G'.$no_cell)
				->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
			$sheet->mergeCells('A'.$no_cell.':C'.$no_cell);
			$sheet->setCellValue('D'.$no_cell, 'Total Discount')->getStyle('D'.$no_cell)->applyFromArray($headerStyles);
			$sheet->setCellValue('G'.$no_cell, $total_pot)->getStyle('G'.$no_cell)->applyFromArray($headerStyles);
			$no_cell = $no_cell + 1;
			$spreadsheet->getActiveSheet()->getStyle('E'.$no_cell.':G'.$no_cell)
				->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
			$sheet->mergeCells('A'.$no_cell.':C'.$no_cell);
			$sheet->setCellValue('D'.$no_cell, 'Total Cost (Ex Works)')->getStyle('D'.$no_cell)->applyFromArray($headerStyles);
			$sheet->setCellValue('E'.$no_cell, '=SUM(E4:E'.$last_cell.')')->getStyle('E'.$no_cell)->applyFromArray($headerStyles);
			$sheet->setCellValue('G'.$no_cell, $grand_tot)->getStyle('G'.$no_cell)->applyFromArray($headerStyles);
		else :
			$spreadsheet->getActiveSheet()->getStyle('D'.$no_cell.':G'.$no_cell)
				->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
			$sheet->mergeCells('A'.$no_cell.':C'.$no_cell);
			$sheet->setCellValue('D'.$no_cell, 'Total Cost (Ex Works)')->getStyle('D'.$no_cell)->applyFromArray($headerStyles);
			$sheet->setCellValue('E'.$no_cell, '=SUM(E4:E'.$last_cell.')')->getStyle('E'.$no_cell)->applyFromArray($headerStyles);
			$sheet->setCellValue('G'.$no_cell, $grand_tot)->getStyle('G'.$no_cell)->applyFromArray($headerStyles);
		endif;

		$no_cell_isi = $no_cell;
		$no_cell = $no_cell + 2;
		if ($so_master['ket_note_so'] == 'available') :
			$sheet->mergeCells('A'.$no_cell.':D'.$no_cell)->setCellValue('A'.$no_cell, 'Catatan :');
			$no_cell = $no_cell + 1;
			$sheet->mergeCells('A'.$no_cell.':D'.$no_cell)->setCellValue('A'.$no_cell, $so_master['note_so']);
			foreach ($parent_items as $parent) :
				if (!empty($parent->item_note)) :
					$no_cell++;
					$sheet->setCellValue('A'.$no_cell, '> '.$parent->item_code.' - '.$parent->item_note);
				endif;
			endforeach;
			foreach ($child_items as $child) :
				if (!empty($child->item_note)) :
					$no_cell++;
					$sheet->setCellValue('A'.$no_cell, '> '.$child->item_code.' - '.$child->item_note);
				endif;
			endforeach;
		endif;

		$no_cell = $no_cell + 2;
		if ($tipe_invoice == 'customer') :
			$sheet->mergeCells('A'.$no_cell.':C'.$no_cell)->setCellValue('A'.$no_cell, 'For Payment made within trading terms - Total net invoice value : '.$grand_tot.'');
			$no_cell = $no_cell + 1;
			$sheet->setCellValue('A'.$no_cell, 'CONTAINER/SEAL');
			$sheet->setCellValue('B'.$no_cell, $headers['container_number'].'/'.$headers['seal_number']);
			$no_cell = $no_cell + 1;
			$sheet->setCellValue('A'.$no_cell, 'MADE IN INDONESIA');
			$no_cell = $no_cell + 1;
			$sheet->mergeCells('A'.$no_cell.':C'.$no_cell)->setCellValue('A'.$no_cell, 'WE HEREBY CERTIFY THAT CARGO IS INDONESIA ORIGIN');
			$no_cell = $no_cell + 1;
			$sheet->setCellValue('A'.$no_cell, 'Bank Details :');
			$no_cell = $no_cell + 1;
			$sheet->setCellValue('A'.$no_cell, 'Account Name :');
			$sheet->setCellValue('B'.$no_cell, 'CI Group Pty Ltd US Account');
			$no_cell = $no_cell + 1;
			$sheet->setCellValue('A'.$no_cell, 'BSB :');
			$sheet->setCellValue('B'.$no_cell, '34702');
			$no_cell = $no_cell + 1;
			$sheet->setCellValue('A'.$no_cell, 'Account No :');
			$sheet->setCellValue('B'.$no_cell, '695930');
			$no_cell = $no_cell + 1;
			$sheet->setCellValue('A'.$no_cell, 'Swift Code :');
			$sheet->setCellValue('B'.$no_cell, 'WPACAU25XXX');
			$no_cell = $no_cell + 1;
			$sheet->setCellValue('A'.$no_cell, 'Payment Terms :');
			$sheet->setCellValue('B'.$no_cell, $headers['term_payment_waktu'].' Days After BL');
		else :
			$sheet->mergeCells('A'.$no_cell.':C'.$no_cell)->setCellValue('A'.$no_cell, 'Container Number : '.$headers['container_number']);
			$no_cell = $no_cell + 1;
			$sheet->mergeCells('A'.$no_cell.':C'.$no_cell)->setCellValue('A'.$no_cell, 'Seal Number : '.$headers['seal_number']);
			$no_cell = $no_cell + 2;
			$sheet->mergeCells('E'.$no_cell.':G'.$no_cell)->setCellValue('E'.$no_cell, 'Prepared by,')
				->getStyle('E'.$no_cell.':G'.$no_cell)->getAlignment()
					->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
					->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
			$no_cell = $no_cell + 3;
			$sheet->mergeCells('E'.$no_cell.':G'.$no_cell)->setCellValue('E'.$no_cell, 'Exim')
				->getStyle('E'.$no_cell.':G'.$no_cell)->getAlignment()
					->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
					->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		endif;

		$styleArray = [
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
				],
			],
		];
		$sheet->getStyle('A'.$mulai_tbheader.':G'.$no_cell_isi)->applyFromArray($styleArray);

		/* ==This is where the output will going, hope they will do it well== */
		$filename = 'report_invoice_'.$tipe_invoice.'_'.$headers['no_invoice'].'.xlsx';
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='. $filename .'');
		header('Cache-Control: max-age=0');
		$writer->save('php://output');
	}
}