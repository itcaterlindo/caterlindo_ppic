<?php
defined('BASEPATH') or exit('No direct script access allowed!');

require_once APPPATH."third_party/phpspreadsheets/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Picking_slip extends MY_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->helper(array('my_helper', 'url'));
		$this->load->model(array('tm_deliveryorder', 'model_do', 'model_salesorder', 'm_format_laporan', 'm_salesorder', 'tb_default_disc'));
	}

	public function data_cetak($kd_mdo = '') {
		/* --Detail Data, fetch from model-- */
		$no_invoice = $this->tm_deliveryorder->get_no_invoice($kd_mdo);
		$lists_kd = $this->tm_deliveryorder->get_same_kd($no_invoice);
		if (!empty($lists_kd)) :
			$no = 0;
			foreach ($lists_kd as $list) :
				$no++;
				if ($no == 1) :
					$first_kd = $list->kd_mdo;
				else :
					$first_kd = $first_kd;
				endif;
				$list_kd[] = $list->kd_mdo;
				$list_kdso[] = $list->msalesorder_kd;
			endforeach;
		else :
			$list_kd = array('');
			$list_kdso = array('');
			$first_kd = '';
		endif;

		$data = $this->model_do->get_header_detail($first_kd);
		$m_detail = $this->model_salesorder->read_masterdata($data['msalesorder_kd']);
		if ($data['tipe_do'] == 'Ekspor') :
			$invoice_tipe = 'invoiceekspor';
		elseif ($data['tipe_do'] == 'Lokal') :
			$invoice_tipe = 'invoicelocal';
		endif;
		$data['format_laporan'] = $this->m_format_laporan->view_laporan($invoice_tipe, $m_detail['term_payment_format']);
		$data['parent_items'] = $this->model_do->list_parent_items($list_kd, '', $data['tipe_do']);
		$data['child_items'] = $this->model_do->list_child_items($list_kd, '', $data['tipe_do']);
		/* --Load main view and return it to browser-- */
		return $data;
	}

	public function report($kd_mdo = '') {
		$data = $this->data_cetak($kd_mdo);

		$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()
			->setCreator($_SESSION['nm_admin'])
			->setLastModifiedBy($_SESSION['nm_admin'])
			->setTitle('Report Picking Slip '.$data['no_invoice'])
			->setSubject('Report Picking Slip')
			->setDescription('Report Picking Slip ini dihasilkan oleh controller \'report/picking_slip\'')
			->setKeywords('office 2007 xlsx php spreadsheet picking_slip report sales')
			->setCategory('Report');
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		$sheet = $spreadsheet->getActiveSheet();
		$style = array(
			'font' => array('bold' => true),
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,)
		);
		$headStyles = array(
			'font' => array('bold' => true, 'size' => '16'),
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER),
		);
		$headerStyles = array(
			'font' => array('bold' => true)
		);
		$footstyle = array(
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER),
		);
		$mulai_header = 1;
		$sheet->getColumnDimension('A')->setWidth(5);
		$sheet->getColumnDimension('B')->setWidth(20);
		$sheet->getColumnDimension('C')->setWidth(20);
		$sheet->getColumnDimension('D')->setWidth(20);
		$sheet->getColumnDimension('E')->setWidth(20);
		$sheet->getColumnDimension('F')->setWidth(20);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setWidth(10);
		$sheet->mergeCells('A'.$mulai_header.':H'.$mulai_header)->setCellValue('A'.$mulai_header, 'Picking Slip')->getStyle('A'.$mulai_header)->applyFromArray($headStyles);
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('A'.$mulai_header.':H'.$mulai_header)->setCellValue('A'.$mulai_header, $data['no_invoice'])->getStyle('A'.$mulai_header)->applyFromArray($headStyles);
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'Invoice To : '.$data['nm_cust'])->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
		if (!empty($data['nm_contact_cust'])) :
			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, $data['nm_contact_cust']);
		endif;
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, str_replace('<br>', ' ', $data['alamat_cust']));

		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'Notify Party : '.$data['notify_name'])->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, $data['notify_address']);

		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'Delivery Address : '.$data['nm_penerima'])->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
		$sheet->mergeCells('E'.$mulai_header.':H'.$mulai_header)->setCellValue('E'.$mulai_header, 'Date : '.format_date($data['tgl_do'], 'd M Y'));
		if (!empty($data['nm_contact_penerima'])) :
			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, $data['nm_contact_penerima']);
		endif;
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, str_replace('<br>', ' ', $data['alamat_penerima']));

		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'Freight By : '.$data['nm_jasakirim'])->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
		if (!empty($data['no_po'])) :
			$sheet->mergeCells('E'.$mulai_header.':H'.$mulai_header)->setCellValue('E'.$mulai_header, 'Order No : '.$data['no_po']);
		endif;
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, str_replace('<br>', ' ', $data['alamat_jasakirim']));

		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'Description of goods : Stainless Steel Goods')->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'Number of boxs : '.$data['jml_koli'])->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);

		$cell_start = $mulai_header + 2;
		$cell_isi = $cell_start;
		$no = 0;
		$tots_stuff = array('');
		$tots_nw = array('');
		$tots_gw = array('');
		$sheet->setCellValue('A'.$cell_isi, 'No')->getStyle('A'.$cell_isi)->applyFromArray($style);
		$sheet->mergeCells('B'.$cell_isi.':C'.$cell_isi)->setCellValue('B'.$cell_isi, 'Code')->getStyle('B'.$cell_isi)->applyFromArray($style);
		$sheet->mergeCells('D'.$cell_isi.':F'.$cell_isi)->setCellValue('D'.$cell_isi, 'Description')->getStyle('D'.$cell_isi)->applyFromArray($style);
		$sheet->setCellValue('G'.$cell_isi, 'Size')->getStyle('G'.$cell_isi)->applyFromArray($style);
		$sheet->setCellValue('H'.$cell_isi, 'Qty')->getStyle('H'.$cell_isi)->applyFromArray($style);
		foreach ($data['parent_items'] as $parent) :
			$item_data[] = array('item_code' => $parent->item_code, 'item_desc' => $parent->item_desc, 'item_dimension' => $parent->item_dimension, 'stuffing_item_qty' => $parent->stuffing_item_qty);
			$tots_stuff[] = $parent->stuffing_item_qty;
			$tots_nw[] = $parent->netweight;
			$tots_gw[] = $parent->grossweight;
		endforeach;
		foreach ($data['child_items'] as $child) :
			$item_data[] = array('item_code' => $child->item_code, 'item_desc' => $child->item_desc, 'item_dimension' => $child->item_dimension, 'stuffing_item_qty' => $child->stuffing_item_qty);
			$tots_stuff[] = $child->stuffing_item_qty;
			$tots_nw[] = $child->netweight;
			$tots_gw[] = $child->grossweight;
		endforeach;
		sort($item_data);
		foreach ($item_data as $datas) :
			$no++;
			$cell_isi++;
			$sheet->setCellValue('A'.$cell_isi, $no);
			$sheet->mergeCells('B'.$cell_isi.':C'.$cell_isi)->setCellValue('B'.$cell_isi, $datas['item_code']);
			$sheet->mergeCells('D'.$cell_isi.':F'.$cell_isi)->setCellValue('D'.$cell_isi, $datas['item_desc']);
			$sheet->setCellValue('G'.$cell_isi, $datas['item_dimension']);
			$sheet->setCellValue('H'.$cell_isi, $datas['stuffing_item_qty']);
		endforeach;
		$cell_footer = $cell_isi + 1;
		$sheet->setCellValue('A'.$cell_footer, '');
		$sheet->mergeCells('B'.$cell_footer.':C'.$cell_footer)->setCellValue('B'.$cell_footer, '');
		$sheet->mergeCells('D'.$cell_footer.':F'.$cell_footer)->setCellValue('D'.$cell_footer, 'Grand Total');
		$sheet->setCellValue('G'.$cell_footer, 'Size')->getStyle('G'.$cell_footer, '');
		$sheet->setCellValue('H'.$cell_footer, '=SUM(H'.$cell_start.':H'.$cell_isi.')');

		$report_foot = $cell_footer + 2;
		$sheet->mergeCells('A'.$report_foot.':H'.$report_foot)->setCellValue('A'.$report_foot, 'Container Number : '.$data['container_number']);
		$report_foot = $report_foot + 1;
		$sheet->mergeCells('A'.$report_foot.':H'.$report_foot)->setCellValue('A'.$report_foot, 'Seal Number : '.$data['seal_number']);
		$report_foot = $report_foot + 2;
		$sheet->mergeCells('A'.$report_foot.':C'.$report_foot)->setCellValue('A'.$report_foot, 'Prepared by,')->getStyle('A'.$report_foot)->applyFromArray($footstyle);
		$sheet->mergeCells('F'.$report_foot.':H'.$report_foot)->setCellValue('F'.$report_foot, 'Reported by,')->getStyle('F'.$report_foot)->applyFromArray($footstyle);
		$report_foot = $report_foot + 3;
		$sheet->mergeCells('A'.$report_foot.':C'.$report_foot)->setCellValue('A'.$report_foot, 'Exim')->getStyle('A'.$report_foot)->applyFromArray($footstyle);
		$sheet->mergeCells('F'.$report_foot.':H'.$report_foot)->setCellValue('F'.$report_foot, 'Finished Goods')->getStyle('F'.$report_foot)->applyFromArray($footstyle);
		$report_foot = $report_foot + 2;
		$sheet->mergeCells('A'.$report_foot.':C'.$report_foot)->setCellValue('A'.$report_foot, 'Monitored by,')->getStyle('A'.$report_foot)->applyFromArray($footstyle);
		$sheet->mergeCells('F'.$report_foot.':H'.$report_foot)->setCellValue('F'.$report_foot, 'Driver,')->getStyle('F'.$report_foot)->applyFromArray($footstyle);
		$report_foot = $report_foot + 3;
		$sheet->mergeCells('A'.$report_foot.':C'.$report_foot)->setCellValue('A'.$report_foot, 'Security')->getStyle('A'.$report_foot)->applyFromArray($footstyle);
		$sheet->mergeCells('F'.$report_foot.':H'.$report_foot)->setCellValue('F'.$report_foot, '')->getStyle('F'.$report_foot)->applyFromArray($footstyle);

		$styleArray = [
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
				],
			],
		];
		$sheet->getStyle('A'.$cell_start.':H'.$cell_footer)->applyFromArray($styleArray);

		$filename = 'report_picking_slip_'.$data['no_invoice'].'.xlsx';
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='. $filename .'');
		header('Cache-Control: max-age=0');
		$writer->save('php://output');
		// var_dump($data);
	}
}