<?php
defined('BASEPATH') or exit('No direct script access allowed!');
 
require_once APPPATH."third_party/phpspreadsheets/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Report_po extends MY_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->helper(array('my_helper', 'url'));
		$this->load->model(array('db_caterlindo/tm_stock', 'tm_salesorder', 'td_salesorder_item', 'td_salesorder_item_detail'));
	}

	public function report_today() {
		$report = $this->tm_stock->get_report_stock();
		$data_mso = $this->tm_salesorder->get_unfinish_mso();
		$arr_kdmso = extract_column($data_mso, 'kd_msalesorder');
		$item_parent = $this->td_salesorder_item->get_items($arr_kdmso);
		$item_child = $this->td_salesorder_item_detail->get_items($arr_kdmso);

		$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()
			->setCreator($_SESSION['nm_admin'])
			->setLastModifiedBy($_SESSION['nm_admin'])
			->setTitle('Report PO '.date('d-M-Y'))
			->setSubject('Report PO')
			->setDescription('Report PO ini dihasilkan oleh controller \'report/report_po\'')
			->setKeywords('office 2007 xlsx php spreadsheet report_po inventory')
			->setCategory('Report');
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		$sheet = $spreadsheet->getActiveSheet();
		$thStyle = array(
			'font' => array('bold' => true),
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,)
		);
		$tbodyStyle = array(
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,)
		);
		$headStyles = array(
			'font' => array('bold' => true, 'size' => '16'),
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER),
		);
		$headerStyles = array(
			'font' => array('bold' => true)
		);
		$mulai_header = 1;
		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'REPORT PO '.date('d-M-Y'))->getStyle('A'.$mulai_header)->applyFromArray($headStyles);

		$no_cell = $mulai_header + 2;
		$mulai_cell = $no_cell;
		$no = 0;
		$sheet->setCellValue('A'.$mulai_cell, 'No')->getStyle('A'.$mulai_cell)->applyFromArray($thStyle);
		$sheet->setCellValue('B'.$mulai_cell, 'Kode Barang')->getStyle('B'.$mulai_cell)->applyFromArray($thStyle);
		$sheet->setCellValue('C'.$mulai_cell, 'No Batch')->getStyle('C'.$mulai_cell)->applyFromArray($thStyle);
		$sheet->setCellValue('D'.$mulai_cell, 'Qty Data')->getStyle('D'.$mulai_cell)->applyFromArray($thStyle);
		foreach ($data_mso as $row) :
			$tgl_view = !empty($row->tgl_stuffing)?$row->tgl_stuffing:$row->tgl_kirim;
			$tgl_view = !empty($tgl_view) && $tgl_view != '0000-00-00'?format_date($tgl_view, 'd-m-y'):'-';
			if ($row->tipe_customer == 'Lokal') :
				$cell_header[] = $row->no_salesorder."\n".$tgl_view;
			elseif ($row->tipe_customer == 'Ekspor') :
				$cell_header[] = $row->no_po."\n".$tgl_view;
			endif;
			$cell_header[] = 'Sisa';
		endforeach;
		$cell_header[] = 'Selisih';
		$sheet->fromArray($cell_header, NULL, 'E'.$mulai_cell);

		$part_satu = $mulai_cell;
		foreach ($report as $row) :
			$no++;
			$part_satu++;
			$qty_data = !empty($row->fn_qty)?$row->fn_qty:'0';
			$qty_sisa = !empty($row->fn_sisa)?$row->fn_sisa:'0';
			$selisih = $qty_data;
			$sheet->setCellValue('A'.$part_satu, $no)->getStyle('A'.$part_satu)->applyFromArray($tbodyStyle);
			$sheet->setCellValue('B'.$part_satu, $row->fc_kdstock)->getStyle('B'.$part_satu)->applyFromArray(array('alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)));
			$sheet->setCellValue('C'.$part_satu, $row->fc_barcode)->getStyle('C'.$part_satu)->applyFromArray($tbodyStyle);
			$sheet->setCellValue('D'.$part_satu, $qty_data)->getStyle('D'.$part_satu)->applyFromArray($tbodyStyle);
		endforeach;
		foreach ($item_parent as $item) :
			$parent_item_qty[$item->msalesorder_kd][$item->item_barcode][] = $item->item_qty;
		endforeach;
		foreach ($item_child as $item) :
			$child_item_qty[$item->msalesorder_kd][$item->item_barcode][] = $item->item_qty;
		endforeach;
		$part_dua = $mulai_cell;
		/*foreach ($report as $row) :
			$part_dua++;
			foreach ($data_mso as $mso) :
				if (isset($parent_item_qty[$mso->kd_msalesorder][$row->fc_barcode])) :
					$tot_item_parent = array_sum($parent_item_qty[$mso->kd_msalesorder][$row->fc_barcode]);
				else :
					$tot_item_parent = 0;
				endif;
				if (isset($child_item_qty[$mso->kd_msalesorder][$row->fc_barcode])) :
					$tot_item_child = array_sum($child_item_qty[$mso->kd_msalesorder][$row->fc_barcode]);
				else :
					$tot_item_child = 0;
				endif;
				$tot_item = $tot_item_parent + $tot_item_child;
				$selisih = $selisih - $tot_item;
				$cell_isi[] = $tot_item;
				$cell_isi[] = $selisih;
			endforeach;
			$cell_isi[] = $selisih;
			$sheet->fromArray($cell_isi, NULL, 'E'.$part_dua);
		endforeach;*/

		$filename = 'report_po_'.date('d-M-Y').'.xlsx';
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='. $filename .'');
		header('Cache-Control: max-age=0');
		$writer->save('php://output');
	}
}