<?php
defined('BASEPATH') or exit('No direct script access allowed!');

require_once APPPATH."third_party/phpspreadsheets/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Shipping_instruction extends MY_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->helper(array('my_helper', 'url'));
		$this->load->model(array('tm_deliveryorder', 'model_do', 'model_salesorder', 'm_format_laporan', 'm_salesorder', 'tb_default_disc'));
	}

	public function data_cetak($kd_mdo = '') {
		/* --Detail Data, fetch from model-- */
		$no_invoice = $this->tm_deliveryorder->get_no_invoice($kd_mdo);
		$lists_kd = $this->tm_deliveryorder->get_same_kd($no_invoice);
		if (!empty($lists_kd)) :
			$no = 0;
			foreach ($lists_kd as $list) :
				$no++;
				if ($no == 1) :
					$first_kd = $list->kd_mdo;
				else :
					$first_kd = $first_kd;
				endif;
				$list_kd[] = $list->kd_mdo;
				$list_kdso[] = $list->msalesorder_kd;
			endforeach;
		else :
			$list_kd = array('');
			$list_kdso = array('');
			$first_kd = '';
		endif;

		$data = $this->model_do->get_header_detail($first_kd);
		$m_detail = $this->model_salesorder->read_masterdata($data['msalesorder_kd']);
		if ($data['tipe_do'] == 'Ekspor') :
			$invoice_tipe = 'invoiceekspor';
		elseif ($data['tipe_do'] == 'Lokal') :
			$invoice_tipe = 'invoicelocal';
		endif;
		$data['format_laporan'] = $this->m_format_laporan->view_laporan($invoice_tipe, $m_detail['term_payment_format']);
		$data['parent_items'] = $this->model_do->list_parent_items($list_kd, '', $data['tipe_do']);
		$data['child_items'] = $this->model_do->list_child_items($list_kd, '', $data['tipe_do']);
		/* --Load main view and return it to browser-- */
		return $data;
	}

	public function report($kd_mdo = '') {
		$data = $this->data_cetak($kd_mdo);

		$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()
			->setCreator($_SESSION['nm_admin'])
			->setLastModifiedBy($_SESSION['nm_admin'])
			->setTitle('Report Shipping Instruction '.$data['no_invoice'])
			->setSubject('Report Shipping Instruction')
			->setDescription('Report Shipping Instruction ini dihasilkan oleh controller \'report/shipping_instruction\'')
			->setKeywords('office 2007 xlsx php spreadsheet shipping_instruction report sales')
			->setCategory('Report');
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		$sheet = $spreadsheet->getActiveSheet();
		$style = array(
			'font' => array('bold' => true),
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,)
		);
		$headStyles = array(
			'font' => array('bold' => true, 'size' => '16'),
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER),
		);
		$headerStyles = array(
			'font' => array('bold' => true)
		);
		$mulai_header = 1;
		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'Shipping Instruction')->getStyle('A'.$mulai_header)->applyFromArray($headStyles);
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, $data['no_invoice'])->getStyle('A'.$mulai_header)->applyFromArray($headStyles);
		$mulai_header = $mulai_header + 2;
		$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, '1. SHIPPER : '.$data['nm_jasakirim'])->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
		if (!empty($data['alamat_jasakirim'])) :
			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, str_replace('<br>', ' ', $data['alamat_jasakirim']));
		endif;
		$mulai_header = $mulai_header + 2;
		$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, '2. CONSIGNEE : '.$data['nm_cust'])->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
		if (!empty($data['nm_contact_cust'])) :
			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, $data['nm_contact_cust']);
		endif;
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, str_replace('<br>', ' ', $data['alamat_cust']));
		$mulai_header = $mulai_header + 2;
		$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'NOTIFY PARTY : '.$data['notify_name'])->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, str_replace('<br>', ' ', $data['notify_address']));
		$mulai_header = $mulai_header + 2;
		$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'NUMBER OF BOXS : '.$data['jml_koli'])->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
		$mulai_header = $mulai_header + 2;
		$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, '3. DESCRIPTION OF GOODS :')->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);

		$no_cell = $mulai_header + 1;
		$tots_stuff = array('');
		$tots_nw = array('');
		$tots_gw = array('');
		foreach ($data['parent_items'] as $parent) :
			$item_desc[] = $parent->item_desc;
			$stuffing_item_qty[$parent->item_desc][] = $parent->stuffing_item_qty;
			$hs_code[$parent->item_desc] = $parent->hs_code;

			$tots_stuff[] = $parent->stuffing_item_qty;
			$tots_nw[] = $parent->stuffing_item_qty * $parent->netweight;
			$tots_gw[] = $parent->stuffing_item_qty * $parent->grossweight;
		endforeach;
		foreach ($data['child_items'] as $child) :
			$item_desc[] = $child->item_desc;
			$stuffing_item_qty[$child->item_desc] = $child->stuffing_item_qty;
			$hs_code[$child->item_desc] = $child->hs_code;

			$tots_stuff[] = $child->stuffing_item_qty;
			$tots_nw[] = $child->stuffing_item_qty * $child->netweight;
			$tots_gw[] = $child->stuffing_item_qty * $child->grossweight;
		endforeach;
		$item_desc_unique = array_unique($item_desc);
		foreach ($item_desc_unique as $item_description) :
			$data_items[] = array('stuffing' => array_sum($stuffing_item_qty[$item_description]), 'hs' => $hs_code[$item_description], 'item_description' => $item_description);
		endforeach;
		asort($data_items);
		foreach ($data_items as $key => $value) :
			$sheet->setCellValue('A'.$no_cell, $value['stuffing']);
			$sheet->setCellValue('B'.$no_cell, 'Pcs');
			$sheet->setCellValue('C'.$no_cell, $value['hs']);
			$sheet->setCellValue('D'.$no_cell, $value['item_description']);
			$no_cell++;
		endforeach;
		$tot_stuff = array_sum($tots_stuff);
		$tot_nw = array_sum($tots_nw);
		$tot_gw = array_sum($tots_gw);

		$cell_footer = $no_cell + 1;
		$sheet->mergeCells('A'.$cell_footer.':D'.$cell_footer)->setCellValue('A'.$cell_footer, '4. NETT / GROSS WEIGHT : '.$tot_nw.' / '.$tot_gw.' KGS');
		$cell_footer = $cell_footer + 1;
		$sheet->mergeCells('A'.$cell_footer.':D'.$cell_footer)->setCellValue('A'.$cell_footer, '5. MEASUREMENT :');
		$cell_footer = $cell_footer + 1;
		$sheet->mergeCells('A'.$cell_footer.':D'.$cell_footer)->setCellValue('A'.$cell_footer, '6. LC/NON LC : NON LC');
		$cell_footer = $cell_footer + 1;
		$sheet->mergeCells('A'.$cell_footer.':D'.$cell_footer)->setCellValue('A'.$cell_footer, '7. SHIPPED ON BOARD : '.$data['tgl_do']);

		$filename = 'report_shipping_instruction_'.$data['no_invoice'].'.xlsx';
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='. $filename .'');
		header('Cache-Control: max-age=0');
		$writer->save('php://output');
	}
}