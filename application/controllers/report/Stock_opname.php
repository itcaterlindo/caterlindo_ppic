<?php
defined('BASEPATH') or exit('No direct script access allowed!');
 
require_once APPPATH."third_party/phpspreadsheets/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Stock_opname extends MY_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->helper(array('my_helper', 'url'));
		$this->load->model(array('db_caterlindo/tm_stock'));
	}

	public function report($kd_opname = '', $tgl_opname = '') {
		$report = $this->tm_stock->get_report_opname($kd_opname);

		$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()
			->setCreator($_SESSION['nm_admin'])
			->setLastModifiedBy($_SESSION['nm_admin'])
			->setTitle('Report Stock Opname '.$kd_opname)
			->setSubject('Report Stock Opname')
			->setDescription('Report Stock Opname ini dihasilkan oleh controller \'report/stock_opname\'')
			->setKeywords('office 2007 xlsx php spreadsheet stock_opname report inventory')
			->setCategory('Report');
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		$sheet = $spreadsheet->getActiveSheet();
		$thStyle = array(
			'font' => array('bold' => true),
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,)
		);
		$tbodyStyle = array(
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,)
		);
		$headStyles = array(
			'font' => array('bold' => true, 'size' => '16'),
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER),
		);
		$headerStyles = array(
			'font' => array('bold' => true)
		);
		$mulai_header = 1;
		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->mergeCells('A'.$mulai_header.':G'.$mulai_header)->setCellValue('A'.$mulai_header, 'STOCK OPNAME FINISH GOOD '.format_date($tgl_opname, 'd-M-Y'))->getStyle('A'.$mulai_header)->applyFromArray($headStyles);

		$no_cell = $mulai_header + 2;
		$mulai_cell = $no_cell;
		$no = 0;
		$sheet->setCellValue('A'.$mulai_cell, 'No')->getStyle('A'.$mulai_cell)->applyFromArray($thStyle);
		$sheet->setCellValue('B'.$mulai_cell, 'Kode Barang')->getStyle('B'.$mulai_cell)->applyFromArray($thStyle);
		$sheet->setCellValue('C'.$mulai_cell, 'Nama Stock')->getStyle('C'.$mulai_cell)->applyFromArray($thStyle);
		$sheet->setCellValue('D'.$mulai_cell, 'No Batch')->getStyle('D'.$mulai_cell)->applyFromArray($thStyle);
		$sheet->setCellValue('E'.$mulai_cell, 'Qty Data')->getStyle('E'.$mulai_cell)->applyFromArray($thStyle);
		$sheet->setCellValue('F'.$mulai_cell, 'Qty Opname')->getStyle('F'.$mulai_cell)->applyFromArray($thStyle);
		$sheet->setCellValue('G'.$mulai_cell, 'Selisih')->getStyle('G'.$mulai_cell)->applyFromArray($thStyle);
		foreach ($report as $row) :
			$no++;
			$mulai_cell++;
			$sheet->setCellValue('A'.$mulai_cell, $no)->getStyle('A'.$mulai_cell)->applyFromArray($tbodyStyle);
			$sheet->setCellValue('B'.$mulai_cell, $row->fc_kdstock)->getStyle('B'.$mulai_cell)->applyFromArray(array('alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)));
			$sheet->setCellValue('C'.$mulai_cell, $row->fv_namastock);
			$sheet->setCellValue('D'.$mulai_cell, $row->fc_barcode)->getStyle('D'.$mulai_cell)->applyFromArray($tbodyStyle);
			$sheet->setCellValue('E'.$mulai_cell, $row->fn_qty)->getStyle('E'.$mulai_cell)->applyFromArray($tbodyStyle);
			$sheet->setCellValue('F'.$mulai_cell, $row->qty_opname)->getStyle('F'.$mulai_cell)->applyFromArray($tbodyStyle);
			if ($row->fn_qty - $row->qty_opname > 0) :
				$sheet->setCellValue('G'.$mulai_cell, '=E'.$mulai_cell.'-F'.$mulai_cell)->getStyle('G'.$mulai_cell)->applyFromArray($tbodyStyle)->getFill()
					->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
					->getStartColor()->setARGB('FF0000');
			else :
				$sheet->setCellValue('G'.$mulai_cell, '=E'.$mulai_cell.'-F'.$mulai_cell)->getStyle('G'.$mulai_cell)->applyFromArray($tbodyStyle);
			endif;
		endforeach;
		$akhir_cell = $mulai_cell + 1;
		$sheet->mergeCells('A'.$akhir_cell.':D'.$akhir_cell)->setCellValue('A'.$akhir_cell, 'Total Qty')->getStyle('A'.$akhir_cell)->applyFromArray($tbodyStyle);
		$sheet->setCellValue('E'.$akhir_cell, '=SUM(E'.$no_cell.':E'.$mulai_cell.')')->getStyle('E'.$akhir_cell)->applyFromArray($tbodyStyle);
		$sheet->setCellValue('F'.$akhir_cell, '=SUM(F'.$no_cell.':F'.$mulai_cell.')')->getStyle('F'.$akhir_cell)->applyFromArray($tbodyStyle);
		$sheet->setCellValue('G'.$akhir_cell, '=SUM(G'.$no_cell.':G'.$mulai_cell.')')->getStyle('G'.$akhir_cell)->applyFromArray($tbodyStyle);

		$styleArray = [
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
				],
			],
		];
		$sheet->getStyle('A'.$no_cell.':G'.$akhir_cell)->applyFromArray($styleArray);

		$filename = 'report_stock_opname_'.$kd_opname.'.xlsx';
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='. $filename .'');
		header('Cache-Control: max-age=0');
		$writer->save('php://output');
	}
}