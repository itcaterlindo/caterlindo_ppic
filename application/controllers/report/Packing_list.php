<?php
defined('BASEPATH') or exit('No direct script access allowed!');

require_once APPPATH."third_party/phpspreadsheets/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Packing_list extends MY_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->helper(array('my_helper', 'url'));
		$this->load->model(array('tm_deliveryorder', 'model_do', 'model_salesorder', 'm_format_laporan', 'm_salesorder', 'tb_default_disc'));
	}

	public function data_cetak_relation($kd_mdo = '') {
		/* --Detail Data, fetch from model-- */
		$no_invoice = $this->tm_deliveryorder->get_no_invoice($kd_mdo);
		$lists_kd = $this->tm_deliveryorder->get_same_kd($no_invoice);
		if (!empty($lists_kd)) :
			$no = 0;
			foreach ($lists_kd as $list) :
				$no++;
				if ($no == 1) :
					$first_kd = $list->kd_mdo;
				else :
					$first_kd = $first_kd;
				endif;
				$list_kd[] = $list->kd_mdo;
				$list_kdso[] = $list->msalesorder_kd;
			endforeach;
		else :
			$list_kd = array('');
			$list_kdso = array('');
			$first_kd = '';
		endif;

		$data = $this->model_do->get_header_detail($first_kd);
		$m_detail = $this->model_salesorder->read_masterdata($data['msalesorder_kd']);
		if ($data['tipe_do'] == 'Ekspor') :
			$invoice_tipe = 'invoiceekspor';
		elseif ($data['tipe_do'] == 'Lokal') :
			$invoice_tipe = 'invoicelocal';
		endif;
		$data['format_laporan'] = $this->m_format_laporan->view_laporan($invoice_tipe, $m_detail['term_payment_format']);
		$data['parent_items'] = $this->model_do->list_parent_items($list_kd, '', $data['tipe_do']);
		$data['child_items'] = $this->model_do->list_child_items($list_kd, '', $data['tipe_do']);
		/* --Load main view and return it to browser-- */
		return $data;
	}

	public function report($tipe_report = '', $kd_mdo = '') {
		$data = $this->data_cetak_relation($kd_mdo);

		$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()
			->setCreator($_SESSION['nm_admin'])
			->setLastModifiedBy($_SESSION['nm_admin'])
			->setTitle('Report Packing List '.$data['no_invoice'])
			->setSubject('Report Packing List')
			->setDescription('Report Packing List ini dihasilkan oleh controller \'report/packing_list\'')
			->setKeywords('office 2007 xlsx php spreadsheet packing_list report sales')
			->setCategory('Report');
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		$sheet = $spreadsheet->getActiveSheet();
		$style = array(
			'font' => array('bold' => true),
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,)
		);
		$headStyles = array(
			'font' => array('bold' => true, 'size' => '16'),
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER),
		);
		$headerStyles = array(
			'font' => array('bold' => true)
		);
		$footstyle = array(
			'alignment' => array('horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER),
		);
		$mulai_header = 1;
		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->mergeCells('A'.$mulai_header.':I'.$mulai_header)->setCellValue('A'.$mulai_header, 'Packing List')->getStyle('A'.$mulai_header)->applyFromArray($headStyles);
		$mulai_header = $mulai_header + 1;
		$sheet->mergeCells('A'.$mulai_header.':I'.$mulai_header)->setCellValue('A'.$mulai_header, $data['no_invoice'])->getStyle('A'.$mulai_header)->applyFromArray($headStyles);
		if ($tipe_report == 'distributor') :
			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'Invoice To : '.$data['nm_cust'])->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
			if (!empty($data['nm_contact_cust'])) :
				$mulai_header = $mulai_header + 1;
				$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, $data['nm_contact_cust']);
			endif;
			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, str_replace('<br>', ' ', $data['alamat_cust']));

			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'Notify Party : '.$data['notify_name'])->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, $data['notify_address']);

			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'Delivery Address : '.$data['nm_penerima'])->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
			$sheet->mergeCells('E'.$mulai_header.':I'.$mulai_header)->setCellValue('E'.$mulai_header, 'Date : '.format_date($data['tgl_do'], 'd M Y'));
			if (!empty($data['nm_contact_penerima'])) :
				$mulai_header = $mulai_header + 1;
				$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, $data['nm_contact_penerima']);
			endif;
			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, str_replace('<br>', ' ', $data['alamat_penerima']));

			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'Freight By : '.$data['nm_jasakirim'])->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
			if (!empty($data['no_po'])) :
				$sheet->mergeCells('E'.$mulai_header.':I'.$mulai_header)->setCellValue('E'.$mulai_header, 'Order No : '.$data['no_po']);
			endif;
			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, str_replace('<br>', ' ', $data['alamat_jasakirim']));

			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'Description of goods : Stainless Steel Goods')->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'Number of boxs : '.$data['jml_koli'])->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
		elseif ($tipe_report == 'customer') :
			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'Invoice To : '.$data['nm_cust'])->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
			if (!empty($data['nm_contact_cust'])) :
				$mulai_header = $mulai_header + 1;
				$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, $data['nm_contact_cust']);
			endif;
			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, str_replace('<br>', ' ', $data['alamat_cust']));

			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'Delivery Address : '.$data['nm_penerima'])->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
			$sheet->mergeCells('E'.$mulai_header.':I'.$mulai_header)->setCellValue('E'.$mulai_header, 'Date : '.format_date($data['tgl_do'], 'd M Y'));
			if (!empty($data['nm_contact_penerima'])) :
				$mulai_header = $mulai_header + 1;
				$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, $data['nm_contact_penerima']);
			endif;
			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, str_replace('<br>', ' ', $data['alamat_penerima']));

			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'Freight By : '.$data['nm_jasakirim'])->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
			if (!empty($data['no_po'])) :
				$sheet->mergeCells('E'.$mulai_header.':I'.$mulai_header)->setCellValue('E'.$mulai_header, 'Order No : '.$data['no_po']);
			endif;
			if (!empty($data['alamat_jasakirim'])) :
				$mulai_header = $mulai_header + 1;
				$sheet->mergeCells('B'.$mulai_header.':D'.$mulai_header)->setCellValue('B'.$mulai_header, str_replace('<br>', ' ', $data['alamat_jasakirim']));
			endif;

			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'Description of goods : Stainless Steel Goods')->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
			$mulai_header = $mulai_header + 1;
			$sheet->mergeCells('A'.$mulai_header.':D'.$mulai_header)->setCellValue('A'.$mulai_header, 'Number of boxs : '.$data['jml_koli'])->getStyle('A'.$mulai_header)->applyFromArray($headerStyles);
		endif;

		$cell_start = $mulai_header + 2;
		$cell_isi = $cell_start;
		$no = 0;
		$tots_stuff = array('');
		$tots_nw = array('');
		$tots_gw = array('');
		$sheet->setCellValue('A'.$cell_isi, 'No')->getStyle('A'.$cell_isi)->applyFromArray($style);
		$sheet->setCellValue('B'.$cell_isi, 'Product Code')->getStyle('B'.$cell_isi)->applyFromArray($style);
		$sheet->setCellValue('C'.$cell_isi, 'Desc')->getStyle('C'.$cell_isi)->applyFromArray($style);
		$sheet->setCellValue('D'.$cell_isi, 'Size')->getStyle('D'.$cell_isi)->applyFromArray($style);
		$sheet->setCellValue('E'.$cell_isi, 'Tariff')->getStyle('E'.$cell_isi)->applyFromArray($style);
		$sheet->setCellValue('F'.$cell_isi, 'Qty')->getStyle('F'.$cell_isi)->applyFromArray($style);
		$sheet->setCellValue('G'.$cell_isi, 'Netweight')->getStyle('G'.$cell_isi)->applyFromArray($style);
		$sheet->setCellValue('H'.$cell_isi, 'Grossweight')->getStyle('H'.$cell_isi)->applyFromArray($style);
		$sheet->setCellValue('I'.$cell_isi, 'Meas / (CBM)')->getStyle('I'.$cell_isi)->applyFromArray($style);
		foreach ($data['parent_items'] as $parent) :
			$no++;
			$cell_isi++;
			$tots_stuff[] = $parent->stuffing_item_qty;
			$tots_nw[] = $parent->stuffing_item_qty * $parent->netweight;
			$tots_gw[] = $parent->stuffing_item_qty * $parent->grossweight;
			$sheet->setCellValue('A'.$cell_isi, $no);
			$sheet->setCellValue('B'.$cell_isi, $parent->item_code);
			$sheet->setCellValue('C'.$cell_isi, $parent->item_desc);
			$sheet->setCellValue('D'.$cell_isi, $parent->item_dimension);
			$sheet->setCellValue('E'.$cell_isi, $parent->hs_code);
			$sheet->setCellValue('F'.$cell_isi, $parent->stuffing_item_qty);
			$sheet->setCellValue('G'.$cell_isi, $parent->stuffing_item_qty * $parent->netweight);
			$sheet->setCellValue('H'.$cell_isi, $parent->stuffing_item_qty * $parent->grossweight);
			$bg = $parent->stuffing_item_qty * $parent->item_cbm;
			$sheet->setCellValue('I'.$cell_isi, sprintf('%.4f', floor($bg*10000*($bg>0?1:-1))/10000*($bg>0?1:-1)));
		endforeach;
		foreach ($data['child_items'] as $child) :
			$no++;
			$cell_isi++;
			$tots_stuff[] = $child->stuffing_item_qty;
			$tots_nw[] = $child->stuffing_item_qty * $child->netweight;
			$tots_gw[] = $child->stuffing_item_qty * $child->grossweight;
			$sheet->setCellValue('A'.$cell_isi, $no);
			$sheet->setCellValue('B'.$cell_isi, $child->item_code);
			$sheet->setCellValue('C'.$cell_isi, $child->item_desc);
			$sheet->setCellValue('D'.$cell_isi, $child->item_dimension);
			$sheet->setCellValue('E'.$cell_isi, $child->hs_code);
			$sheet->setCellValue('F'.$cell_isi, $child->stuffing_item_qty);
			$sheet->setCellValue('G'.$cell_isi, $child->stuffing_item_qty * $child->netweight);
			$sheet->setCellValue('H'.$cell_isi, $child->stuffing_item_qty * $child->grossweight);
			$bg = $child->stuffing_item_qty * $child->item_cbm;
			$sheet->setCellValue('I'.$cell_isi, sprintf('%.4f', floor($bg*10000*($bg>0?1:-1))/10000*($bg>0?1:-1)));
		endforeach;
		$tot_stuff = array_sum($tots_stuff);
		$tot_nw = array_sum($tots_nw);
		$tot_gw = array_sum($tots_gw);
		$cell_footer = $cell_isi + 1;
		$sheet->setCellValue('A'.$cell_footer, '');
		$sheet->setCellValue('B'.$cell_footer, '');
		$sheet->setCellValue('C'.$cell_footer, 'Grand Total');
		$sheet->setCellValue('D'.$cell_footer, '');
		$sheet->setCellValue('E'.$cell_footer, '');
		$sheet->setCellValue('F'.$cell_footer, '=SUM(F'.$cell_start.':F'.$cell_isi.')');
		$sheet->setCellValue('G'.$cell_footer, '=SUM(G'.$cell_start.':G'.$cell_isi.')');
		$sheet->setCellValue('H'.$cell_footer, '=SUM(H'.$cell_start.':H'.$cell_isi.')');
		$sheet->setCellValue('I'.$cell_footer, '=SUM(I'.$cell_start.':I'.$cell_isi.')');

		$report_foot = $cell_footer + 2;
		if ($tipe_report == 'distributor') :
			$sheet->mergeCells('A'.$report_foot.':I'.$report_foot)->setCellValue('A'.$report_foot, 'NOTE : Gross Weight exclude tare weight container');
			$report_foot = $report_foot + 1;
			$sheet->mergeCells('A'.$report_foot.':I'.$report_foot)->setCellValue('A'.$report_foot, 'Container Number : '.$data['container_number']);
			$report_foot = $report_foot + 1;
			$sheet->mergeCells('A'.$report_foot.':I'.$report_foot)->setCellValue('A'.$report_foot, 'Seal Number : '.$data['seal_number']);
			$report_foot = $report_foot + 1;
			$sheet->mergeCells('F'.$report_foot.':I'.$report_foot)->setCellValue('F'.$report_foot, 'Prepared by,')->getStyle('F'.$report_foot)->applyFromArray($footstyle);
			$report_foot = $report_foot + 3;
			$sheet->mergeCells('F'.$report_foot.':I'.$report_foot)->setCellValue('F'.$report_foot, 'Exim')->getStyle('F'.$report_foot)->applyFromArray($footstyle);
		elseif ($tipe_report == 'customer') :
			$sheet->mergeCells('A'.$report_foot.':I'.$report_foot)->setCellValue('A'.$report_foot, '1. SHIPPER : '.$data['nm_jasakirim']);
			$report_foot = $report_foot + 1;
			$sheet->mergeCells('A'.$report_foot.':I'.$report_foot)->setCellValue('A'.$report_foot, '2. CONSIGNEE/NOTIFY PARTY : '.$data['notify_name']);
			$report_foot = $report_foot + 1;
			$sheet->mergeCells('B'.$report_foot.':I'.$report_foot)->setCellValue('B'.$report_foot, $data['notify_address']);
			$report_foot = $report_foot + 1;
			$sheet->mergeCells('A'.$report_foot.':I'.$report_foot)->setCellValue('A'.$report_foot, '3. DESCRIPTION OF GOODS : Stainless Steel Goods');
			$report_foot = $report_foot + 1;
			$sheet->mergeCells('A'.$report_foot.':I'.$report_foot)->setCellValue('A'.$report_foot, '4. NETT / GROSS WEIGHT : '.$tot_nw.' / '.$tot_gw.' KGS');
			$report_foot = $report_foot + 1;
			$sheet->mergeCells('A'.$report_foot.':I'.$report_foot)->setCellValue('A'.$report_foot, '5. MEASUREMENT : ');
			$report_foot = $report_foot + 1;
			$sheet->mergeCells('A'.$report_foot.':I'.$report_foot)->setCellValue('A'.$report_foot, '6. LC/NON LC : NON LC');
			$report_foot = $report_foot + 1;
			$sheet->mergeCells('A'.$report_foot.':I'.$report_foot)->setCellValue('A'.$report_foot, '7. SHIPPED ON BOARD : '.$data['tgl_do']);
		endif;

		$styleArray = [
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
				],
			],
		];
		$sheet->getStyle('A'.$cell_start.':I'.$cell_footer)->applyFromArray($styleArray);

		$filename = 'report_packing_list_'.$tipe_report.'_'.$data['no_invoice'].'.xlsx';
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='. $filename .'');
		header('Cache-Control: max-age=0');
		$writer->save('php://output');
		// var_dump($data);
	}
}