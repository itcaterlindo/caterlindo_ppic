<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Set_teritory extends MY_Controller {
	private $class_link = 'sales/set_teritory';
	private $form_errs = array('idErrNama', 'idErrDeskripsi');

	public function __construct() {
		parent::__construct();

		$this->load->library(array('ssp', 'form_validation'));
		$this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
		$this->load->model(array('m_builder', 'tb_teritory', 'tb_tipe_teritory'));
	}

	public function index() {
		parent::administrator();
		parent::select2_assets();
		$this->get_table();
	}

	public function get_table() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function open_table() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		$data = $this->tb_teritory->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function get_form() {
		$id = $this->input->get('id');
		$data = $this->tb_teritory->get_row($id);
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = $this->form_errs;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function open_form() {
		$id = $this->input->get('id');
		$data['class_link'] = $this->class_link;
		$data['teritory'] = $this->tb_teritory->get_row($id);
		
		$arrTipeTeritory = $this->tb_tipe_teritory->get_all();
		// UNTUK PARSING TIPE TERITORY KE VIEW FORM
		$tipeTeritory[''] = "-- Silahkan Pilih Tipe Teritory --";
		foreach($arrTipeTeritory as $ter):
			$tipeTeritory[$ter->id] = $ter->kd_tipe_teritory." - ".$ter->nm_tipe_teritory;
		endforeach;
		$data['tipe_teritory'] = $tipeTeritory;

		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function send_data() {
		if ($this->input->is_ajax_request()) :
			$id = $this->input->post('txtId');
			$opt = !empty($id) ? "edit" : "new";
			$this->form_validation->set_rules($this->tb_teritory->form_rules($opt));
			if ($this->form_validation->run() == FALSE) :
				$str = $this->tb_teritory->build_warning($this->form_errs);
				$str['confirm'] = 'error';
			else :
				if($opt == "new"):
					$data['nm_teritory'] =  $this->input->post('txtNama');
				else:
					$data['id'] =  $id;
				endif;
				$data['deskripsi'] = $this->input->post('txtDeskripsi');
				$data['tipe_teritory_id'] = $this->input->post('txtTipe');
				$str = $this->tb_teritory->submit_data($data, $opt);
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function delete_data() {
		if ($this->input->is_ajax_request()) :
			$id = $this->input->get('id');
			$str = $this->tb_teritory->delete_data($id);
			
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}