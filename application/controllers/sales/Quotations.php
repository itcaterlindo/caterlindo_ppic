<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotations extends MY_Controller {
    private $db_name = 'db_caterlindo_ppic';
    private $tbl_name = 'tm_quotation';
    private $p_key = 'kd_mquotation';
    private $class_link = 'sales/quotations';
    private $title = 'Data Quotations';

    function __construct() {
        parent::__construct();

        $this->load->model(array('m_builder', 'm_quotation', 'm_app_setting', 'm_master_quotation', 'm_format_laporan', 'm_msalesorder', 'm_salesorder', 'm_icon_currency', 'tb_tipe_harga', 'tm_currency', 'td_currency_convert', 'model_jenis_customer', 'tm_quotation'));
        $this->load->helper(array('form', 'html', 'url', 'my_helper'));
    }

    function index() {
        parent::administrator();

        $data['t_box_class'] = 'box-primary';
        $title = $this->uri->segment_array();
        $page_title = get_title($title);
        $data['table_title'] = $page_title;
        $data['table_alert'] = 'idAlertTable';
        $data['table_loader'] = 'idLoaderTable';
        $data['table_name'] = 'idTable';
        $data['table_overlay'] = 'idTableOverlay';
        $data['btn_add'] = cek_permission('QUOTATION_CREATE');
        
        $this->page_assets();
        $this->load->view('page/admin/v_table', $data);
        // $this->data_admin_form();
    }
    
    function page_assets() {
        $script['class_name'] = $this->class_link;
        $this->load->js('assets/admin_assets/plugins/typeahead.js/typeahead.bundle.min.js');
        $this->load->css('assets/admin_assets/plugins/typeahead.js/typeahead.css');
        $this->load->css('assets/admin_assets/plugins/select2/select2.min.css');
        $this->load->css('assets/admin_assets/plugins/select2/select2-bootstrap.min.css');
        $this->load->js('assets/admin_assets/plugins/input-mask/jquery.inputmask.js');
        $this->load->js('assets/admin_assets/plugins/select2/select2.full.min.js');
        $this->load->js('assets/admin_assets/plugins/moment.js/moment.min.js');
        $this->load->js('assets/admin_assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js');
        $this->load->css('assets/admin_assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css');
        $this->load->section('scriptJS', 'script/'.$this->class_link.'/scriptJS', $script);
    }

    function data_lihat() {
        if ($this->input->is_ajax_request()) :
            /* tabel property */
            $t_data['btn'] = TRUE;
            $t_data['t_header'] = array(
                '<th style="width:5%;">No. Quotation</th>',
                '<th style="width:10%;">Customer</th>',
                '<th style="width:1%;">Tipe Customer</th>',
                '<th style="width:10%;">Salesperson</th>',
                '<th style="width:1%;">Tgl Quotation</th>',
                '<th style="width:3%;">Status</th>',
            );
            $t_data['t_uri'] = base_url().$this->class_link.'/data_table/';
            $t_data['t_order'] = '[6, "desc"]';

            $this->load->view('page/admin/table_ssp', $t_data);
        endif;
    }

    function btn_table($id, $status, $no_quo) {
        $btn_read = '';
        $btn_edit = '';
        $btn_status = '';
        $btn_delete = '';
        if (cek_permission('QUOTATION_VIEW')) :
            $btn_read = '<li><a id="detail" title="Quotation Detail" href="javascript:void(0);" onclick="viewQuotations(\''.$id.'\')"><i class="fa fa-search"></i> Quotation Detail</a></li>';
        endif;
        if (cek_permission('QUOTATION_UPDATE') && $status == 'pending') :
            $btn_edit = '<li><a id="edit" title="Edit Quotation" href="javascript:void(0);" onclick="editData(\''.$id.'\')"><i class="fa fa-pencil"></i> Quotation</a></li>';
            $btn_edit .= '<li><a id="edit_item" title="Edit Items" href="javascript:void(0);" onclick="editItems(\''.$id.'\')"><i class="fa fa-list"></i> Edit Items</a></li>';
            $btn_edit .= '<li class="divider"></li>';
            $btn_status = '<li><a id="cancel" title="Cancel Quotation" href="javascript:void(0);" onclick="return confirm(\'Anda akan yakin cancel Quotation No. '.$no_quo.'?\')?ubahStatus(\''.$id.'\', \'cancel\'):false;"><i class="fa fa-ban"></i> Cancel</a></li>';
            // $btn_status .= '<li><a id="process" title="Process SO" href="javascript:void(0);" onclick="return confirm(\'Quotation No. '.$no_quo.' akan diproses ke Sales Order, Anda yakin?\')?transferSales(\''.$id.'\'):false;"><i class="fa fa-check"></i> Process SO</a></li>';
            $btn_status .= '<li class="divider"></li>';
        endif;
        if (cek_permission('QUOTATION_DELETE') == '1' && $status == 'pending') :
            $btn_delete = '<li><a id="delete" title="Hapus Data" href="javascript:void(0);" onclick="return confirm(\'Apakah anda yakin?\')?hapusData(\''.$id.'\'):false;"><i class="fa fa-trash"></i> Hapus Data</a></li>';
        endif;
        $all_btn = $btn_read.$btn_edit.$btn_status.$btn_delete;

        if (!empty($all_btn)) :
            $btn = '<div align="center"><div class="btn-group"><button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">Opsi <span class="caret"></span></button><ul class="dropdown-menu">'.$all_btn.'</ul></div></div>';
        else :
            $btn = '<center>-</center>';
        endif;

        return $btn;
    }

    function data_table() {
        if (!$this->input->is_ajax_request()) :
            exit('No direct script access allowed');
        else :
            $this->load->library('ssp');

            $table = $this->tbl_name;

            $primaryKey = $this->p_key;

            $columns = array(
                array( 'db' => 'a.'.$this->p_key,
                        'dt' => 1, 'field' => $this->p_key,
                        'formatter' => function($d, $row){
                            
                            return $this->btn_table($d, $row[6], $row[1]);
                        } ),
                array( 'db' => 'a.no_quotation',
                        'dt' => 2, 'field' => 'no_quotation',
                        'formatter' => function($d){
                            $d = $this->security->xss_clean($d);

                            return $d;
                        } ),
                array( 'db' => 'b.nm_customer',
                        'dt' => 3, 'field' => 'nm_customer',
                        'formatter' => function($d){
                            $d = $this->security->xss_clean($d);

                            return $d;
                        } ),
                array( 'db' => 'a.tipe_customer',
                        'dt' => 4, 'field' => 'tipe_customer',
                        'formatter' => function($d){
                            $d = $this->security->xss_clean($d);

                            return $d;
                        } ),
                array( 'db' => 'c.nm_salesperson',
                        'dt' => 5, 'field' => 'nm_salesperson',
                        'formatter' => function($d){
                            $d = $this->security->xss_clean($d);

                            return $d;
                        } ),
                array( 'db' => 'a.tgl_quotation',
                        'dt' => 6, 'field' => 'tgl_quotation',
                        'formatter' => function($d){
                            $d = $this->security->xss_clean($d);
                            $d = format_date($d, 'd-m-Y');

                            return $d;
                        } ),
                array( 'db' => 'a.status_quotation',
                        'dt' => 7, 'field' => 'status_quotation',
                        'formatter' => function($d){
                            $d = $this->security->xss_clean($d);
                            $word = process_status($d);
                            $color = color_status($d);
                            $d = bg_label($word, $color);

                            return $d;
                        } ),
            );

            $sql_details = sql_connect();

            $joinQuery = "
                FROM
                    ".$this->tbl_name." a
                LEFT JOIN tm_customer b ON a.customer_kd = b.kd_customer
                LEFT JOIN tb_salesperson c ON a.salesperson_kd = c.kd_salesperson
            ";
            $where = "";

            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $where )
            );
        endif;
    }

    function data_form(){
        if ($this->input->is_ajax_request()) :
            // Form data
            $id = $this->input->get('id');

            if (!empty($id)) :
                $det = $this->m_builder->getRow($this->tbl_name, array($this->p_key => $id), 'row');
                $id_form = 'idFormEditQuo';
                $data['f_box_class'] = 'box-warning';
                $data['form_title'] = 'Edit '.$this->title;
                $btn_form = 'Edit & Pilih Items <i class="fa fa-arrow-right"></i>';
                $btn_class = 'info';

                // Data dari tm_quotation
                $kd_mquotation = $this->security->xss_clean($det->kd_mquotation);
                $salesperson_kd = $this->security->xss_clean($det->salesperson_kd);
                $email_sales = $this->security->xss_clean($det->email_sales);
                $mobile_sales = $this->security->xss_clean($det->mobile_sales);
                $no_quotation = $this->security->xss_clean($det->no_quotation);
                $tgl_quotation = format_date($this->security->xss_clean($det->tgl_quotation), 'd-m-Y');
                $customer_kd = $this->security->xss_clean($det->customer_kd);
                $alamat_kirim_kd = $this->security->xss_clean($det->alamat_kirim_kd);
            else :
                $id_form = 'idFormInputQuo';
                $data['f_box_class'] = 'box-info';
                $data['form_title'] = 'Input '.$this->title;
                $btn_form = 'Simpan & Pilih Items <i class="fa fa-arrow-right"></i>';
                $btn_class = 'primary';
                $kd_mquotation = '';
                $salesperson_kd = '';
                $email_sales = '';
                $mobile_sales = '';
                $no_quotation = '';
                $tgl_quotation = date('d-m-Y');
                $customer_kd = '';
                $alamat_kirim_kd = '';
            endif;

            // Cek kd admin yang login, untuk mendapatkan kode karyawan->kode sales
            if (empty($salesperson_kd)) :
                $this->db->from('tb_salesperson');
                $this->db->where(array('karyawan_kd' => $this->session->kd_karyawan));
                $query = $this->db->get();
                $num = $query->num_rows();
                $result = $query->row();
                if ($num > 0) :
                    $salesperson_kd = $this->security->xss_clean($result->kd_salesperson);
                    $nm_salesperson = $this->security->xss_clean($result->nm_salesperson);

                    $db_hrm = $this->load->database('sim_hrm', TRUE);
                    $db_hrm->from('tb_karyawan');
                    $db_hrm->where(array('kd_karyawan' => $this->session->kd_karyawan));
                    $query = $db_hrm->get();
                    $num = $query->num_rows();
                    $result = $query->row();

                    if ($num > 0) :
                        $telp_rumah = $result->telp_rumah;
                        $telp_mobile = $result->telp_mobile;
                        $telp_kantor = $result->telp_kantor;
                        $email_utama = $result->email_utama;
                        $email_lain = $result->email_lain;

                        $telp = '';
                        if (!empty($telp_kantor) || $telp_kantor != '-') :
                            $telp = $telp_kantor;
                        elseif (!empty($telp_mobile) || $telp_mobile != '-') :
                            $telp = $telp_mobile;
                        endif;

                        // Buat variable untuk menentukan mana email tidak kosong
                        $email = '';
                        if (!empty($email_utama) || $email_utama != '-') :
                            $email = $email_utama;
                        elseif (!empty($email_lain) || $email_lain != '-') :
                            $email = $email_lain;
                        endif;
                    endif;

                    $email_sales = $email;
                    $mobile_sales = $telp;
                    $salesperson_readonly = 'readonly';
                else :
                    $salesperson_readonly = '';
                endif;
            else :
                $salesperson_readonly = 'readonly';
            endif;

            // Data dari tb_salesperson
            if (!empty($salesperson_kd)) :
                $d_sales = $this->m_builder->getRow('tb_salesperson', array('kd_salesperson' => $salesperson_kd), 'row');
                $nm_salesperson = $this->security->xss_clean($d_sales->nm_salesperson);
                $karyawan_kd = $this->security->xss_clean($d_sales->karyawan_kd);
            else :
                $nm_salesperson = '';
                $karyawan_kd = '';
            endif;

            $form_salesperson = form_input(array('type' => 'hidden', 'name' => 'txtKdSales', 'id' => 'idTxtKdSales', 'value' => $salesperson_kd));
            $form_salesperson .= form_input(array('name' => 'txtNmSales', 'id' => 'idTxtNmSales', 'class' => 'form-control', 'placeholder' => 'Sales Person', 'value' => $nm_salesperson, $salesperson_readonly => ''));

            // Data dari tm_customer
            if (!empty($customer_kd)) :
                $this->db->select('a.nm_customer, a.code_customer, a.contact_person, a.alamat, b.nm_negara, c.nm_provinsi, d.nm_kota, e.nm_kecamatan');
                $this->db->from('tm_customer a');
                $this->db->join('tb_negara b', 'b.kd_negara = a.negara_kd', 'left outer');
                $this->db->join('tb_provinsi c', 'c.kd_provinsi = a.provinsi_kd AND c.negara_kd = b.kd_negara', 'left outer');
                $this->db->join('tb_kota d', 'd.kd_kota = a.kota_kd AND d.provinsi_kd = c.kd_provinsi AND d.negara_kd = b.kd_negara', 'left outer');
                $this->db->join('tb_kecamatan e', 'e.kd_kecamatan = a.kecamatan_kd AND e.kota_kd = d.kd_kota AND e.provinsi_kd = c.kd_provinsi AND e.negara_kd = b.kd_negara', 'left outer');
                $this->db->where(array('a.kd_customer' => $customer_kd));
                $query = $this->db->get();
                $d_customer = $query->row();
                $nm_customer = $this->security->xss_clean($d_customer->nm_customer);
                $code_customer = $this->security->xss_clean($d_customer->code_customer);
                $contact_person = $this->security->xss_clean($d_customer->contact_person);
                $nm_negara = $d_customer->nm_negara.after_before_char($d_customer->nm_negara, $d_customer->nm_negara, '.', '.');
                $nm_provinsi = $d_customer->nm_provinsi.after_before_char($d_customer->nm_provinsi, $d_customer->nm_negara, ', ', '.');
                $nm_kota = $d_customer->nm_kota.after_before_char($d_customer->nm_kota, array($d_customer->nm_provinsi, $d_customer->nm_kota), ', ', '.');
                $nm_kecamatan = $d_customer->nm_kecamatan.after_before_char($d_customer->nm_kecamatan, array($d_customer->nm_negara, $d_customer->nm_provinsi, $d_customer->nm_kota), ', ', '.');
                $alamat = ucwords(strtolower($this->security->xss_clean($d_customer->alamat))).after_before_char($d_customer->alamat, array($d_customer->nm_kecamatan, $d_customer->nm_negara, $d_customer->nm_provinsi, $d_customer->nm_kota), '. ', '.');
                $alamat_lengkap = $alamat.$nm_kecamatan.$nm_kota.$nm_provinsi.$nm_negara;
            else :
                $nm_customer = '';
                $code_customer = '';
                $contact_person = '';
                $alamat_lengkap = '';
            endif;

            // Button untuk customer
            if (empty($customer_kd)) :
                $btn = '<a onclick="return showFormCustomer(\'\', \'\');" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Customer</a>';
            else :
                $btn = '<a onclick="return clearCustomerDetail();" class="btn btn-sm btn-danger" style="margin-right:5px;"><i class="fa fa-trash"></i> Clear Values</a>';
                $btn .= '<a onclick="return showFormCustomer(\''.$customer_kd.'\', \'\');" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i> Customer</a>';
            endif;

            // Data dari td_customer_alamat
            $form_dropdown = '';
            if (!empty($customer_kd)) :
                // dapatkan jumlah alamat kirim customer ditabel td_customer_alamat
                $this->db->select('a.kd_alamat_kirim, a.kd_badan_usaha, a.nm_customer, a.alamat, b.nm_negara, c.nm_provinsi, d.nm_kota, e.nm_kecamatan');
                $this->db->from('td_customer_alamat a');
                $this->db->join('tb_negara b', 'a.negara_kd = b.kd_negara', 'left');
                $this->db->join('tb_provinsi c', 'a.provinsi_kd = c.kd_provinsi AND a.negara_kd = c.negara_kd', 'left');
                $this->db->join('tb_kota d', 'a.kota_kd = d.kd_kota AND a.provinsi_kd = d.provinsi_kd AND a.negara_kd = d.negara_kd', 'left');
                $this->db->join('tb_kecamatan e', 'a.kecamatan_kd = e.kd_kecamatan AND a.kota_kd = e.kota_kd AND a.provinsi_kd = e.provinsi_kd AND a.negara_kd = e.negara_kd', 'left');
                $this->db->where(array('a.customer_kd' => $customer_kd));
                $query = $this->db->get();
                $num = $query->num_rows();

                if ($num > 0) :
                    $result = $query->result();
                    $pil_alamat = array('' => '-- Pilih Alamat Kirim --');
                    foreach ($result as $row) :
                        $kd_alamat_kirim = $this->security->xss_clean($row->kd_alamat_kirim);
                        $nm_negara = $row->nm_negara.after_before_char($row->nm_negara, $row->nm_negara, ', ', '.');
                        $nm_provinsi = $row->nm_provinsi.after_before_char($row->nm_provinsi, $row->nm_negara, ', ', '.');
                        $nm_kota = $row->nm_kota.after_before_char($row->nm_kota, array($row->nm_provinsi, $row->nm_kota), ', ', '.');
                        $nm_kecamatan = $row->nm_kecamatan.after_before_char($row->nm_kecamatan, array($row->nm_negara, $row->nm_provinsi, $row->nm_kota), ', ', '.');
                        $alamat = ucwords(strtolower($this->security->xss_clean($row->alamat))).after_before_char($row->alamat, array($row->nm_kecamatan, $row->nm_negara, $row->nm_provinsi, $row->nm_kota), '. ', '.');
                        $kd_badan_usaha = $this->security->xss_clean($row->kd_badan_usaha);
                        $nm_customer_alamat = $this->security->xss_clean($row->nm_customer);
                        $this->db->select('nm_select');
                        $this->db->from('tb_set_dropdown');
                        $this->db->where(array('id' => $kd_badan_usaha));
                        $query = $this->db->get();
                        $result = $query->row();
                        $nm_select = $this->security->xss_clean($result->nm_select);
                        $nm_badan_usaha = ($nm_select == 'Lainnya')?''.$nm_customer_alamat.' - ':$nm_select.' '.$nm_customer_alamat.' - ';
                        $alamat = $nm_badan_usaha.$alamat.$nm_kecamatan.$nm_kota.$nm_provinsi.$nm_negara;
                        $pil_alamat[$kd_alamat_kirim] = $alamat;
                    endforeach;
                    $sel_alamat = $alamat_kirim_kd;
                    $attr_alamat = array('id' => 'idSelAlamatKirim', 'class' => 'form-control select2');
                    $form_alamat = form_dropdown('selAlamatKirim', $pil_alamat, $sel_alamat, $attr_alamat);
                elseif ($num == 1) :
                    $result = $query->row();
                    $kd_alamat_kirim = $this->security->xss_clean($result->kd_alamat_kirim);
                    $nm_negara = $row->nm_negara.after_before_char($row->nm_negara, $row->nm_negara, ', ', '.');
                    $nm_provinsi = $row->nm_provinsi.after_before_char($row->nm_provinsi, $row->nm_negara, ', ', '.');
                    $nm_kota = $row->nm_kota.after_before_char($row->nm_kota, array($row->nm_provinsi, $row->nm_kota), ', ', '.');
                    $nm_kecamatan = $row->nm_kecamatan.after_before_char($row->nm_kecamatan, array($row->nm_negara, $row->nm_provinsi, $row->nm_kota), ', ', '.');
                    $alamat = ucwords(strtolower($this->security->xss_clean($result->alamat))).after_before_char($result->alamat, $row->nm_kecamatan, '. ', ', ');
                    $kd_badan_usaha = $this->security->xss_clean($row->kd_badan_usaha);
                    $nm_customer_alamat = $this->security->xss_clean($row->nm_customer);
                    $this->db->select('nm_select');
                    $this->db->from('tb_set_dropdown');
                    $this->db->where(array('id' => $kd_badan_usaha));
                    $query = $this->db->get();
                    $result = $query->row();
                    $nm_select = $this->security->xss_clean($result->nm_select);
                    $nm_badan_usaha = ($nm_select == 'Lainnya')?''.$nm_customer_alamat.' - ':$nm_select.' '.$nm_customer_alamat.' - ';
                    $alamat = $nm_badan_usaha.$alamat.$nm_kecamatan.$nm_kota.$nm_provinsi.$nm_negara;

                    $form_alamat = '';
                    $form_alamat .= form_input(array('type' => 'hidden', 'name' => 'selAlamatKirim', 'id' => 'idSelAlamatKirim', 'value' => $alamat_kirim_kd));
                    $form_alamat .= $alamat;
                endif;

                if (!empty($form_alamat)) :
                    $form_dropdown .= '<div class="form-group">';
                    $form_dropdown .= '<label for="idSelAlamatKirim" class="col-md-2 control-label">Alamat Pengiriman</label>';
                    $form_dropdown .= '<div class="col-md-4">';
                    $form_dropdown .= '<div id="idErrAlamatKirim"></div>';
                    $form_dropdown .= $form_alamat;
                    $form_dropdown .= '</div>';
                    $form_dropdown .= '</div>';
                endif;
            endif;

            $data['f_attr'] = 'id="idBoxForm"';
            $data['form_url'] = '';
            $data['form_att'] = array('id' => $id_form, 'name' => 'nm_form_quotation', 'class' => 'form-horizontal');
            $data['form_hidden'] = '';
            $data['form_close_att'] = '';
            $data['form_field'] = array(
                'nm_sales_dan_no_quo' => array(
                    '<div class="form-group">',
                    '<label for="idTxtNmSales" class="col-md-2 control-label">Sales Person</label>',
                    '<div class="col-md-4">',
                    '<div id="idErrSales"></div>',
                    form_input(array('type' => 'hidden', 'name' => 'txtKdQuotation', 'id' => 'idTxtKdQuotation', 'value' => $kd_mquotation)),
                    form_input(array('type' => 'hidden', 'name' => 'txtKdCustomer', 'id' => 'idTxtKdCustomer', 'value' => $customer_kd)),
                    '<div id="scrollable-dropdown-menu">',
                    $form_salesperson,
                    '</div>',
                    '</div>',
                    '<label for="idTxtNmSales" class="col-md-1" style="padding-top:7px;">No Quo</label>',
                    '<div class="col-md-2">',
                    '<div id="idErrNoQuo"></div>',
                    form_input(array('name' => 'txtNoQuotation', 'id' => 'idTxtNoQuotation', 'class' => 'form-control', 'placeholder' => 'No Quotation', 'value' => $no_quotation, 'readonly' => '')),
                    '</div>',
                    '</div>',
                ),
                'email_dan_tanggal' => array(
                    '<div class="form-group">',
                    '<label for="idTxtEmailSales" class="col-md-2 control-label">Email Address</label>',
                    '<div class="col-md-3">',
                    '<div id="idErrEmailSales"></div>',
                    form_input(array('name' => 'txtEmailSales', 'id' => 'idTxtEmailSales', 'class' => 'form-control', 'placeholder' => 'Email Address', 'value' => $email_sales)),
                    '</div>',
                    '<div class="col-md-1"></div>',
                    '<label for="idTxtDate" class="col-md-1" style="padding-top:7px;">Date</label>',
                    '<div class="col-md-2">',
                    '<div id="idErrDate"></div>',
                    form_input(array('name' => 'txtDate', 'id' => 'idTxtDate', 'class' => 'form-control', 'placeholder' => 'Date', 'value' => $tgl_quotation)),
                    '</div>',
                    '</div>',
                ),
                'mobile_phone_dan_customer_id' => array(
                    '<div class="form-group">',
                    '<label for="idTxtMobilePhone" class="col-md-2 control-label">Mobile Phone</label>',
                    '<div class="col-md-2">',
                    '<div id="idErrMobilePhone"></div>',
                    form_input(array('name' => 'txtMobilePhone', 'id' => 'idTxtMobilePhone', 'class' => 'form-control', 'placeholder' => 'Mobile Phone', 'value' => $mobile_sales)),
                    '</div>',
                    '<div class="col-md-2"></div>',
                    '<label for="idTxtCustomerId" class="col-md-1" style="padding-top:7px;">Customer ID</label>',
                    '<div class="col-md-2">',
                    '<div id="idErrCustomerId"></div>',
                    '<div id="idTxtCustomerId" style="padding-top:7px;">'.$code_customer.'</div>',
                    '</div>',
                    '</div>',
                    '<hr />',
                ),
                'customer' => array(
                    '<div class="form-group">',
                    '<label for="idTxtNmCustomer" class="col-md-2 control-label">Customer</label>',
                    '<div class="col-md-4">',
                    '<div id="idErrNmCustomer"></div>',
                    '<div id="scrollable-dropdown-menu">',
                    form_input(array('name' => 'txtNmCustomer', 'id' => 'idTxtNmCustomer', 'class' => 'form-control', 'placeholder' => 'Customer', 'value' => $nm_customer)),
                    '</div>',
                    '</div>',
                    '<div class="col-md-2 col-md-offset-2">',
                    '<div id="idFormBtnCustomer">',
                    $btn,
                    '</div>',
                    '</div>',
                    '</div>',
                ),
                'contact_person' => array(
                    '<div class="form-group">',
                    '<label for="idContactPerson" class="col-md-2 control-label">Contact Person</label>',
                    '<div class="col-md-3">',
                    '<div id="idErrContactPerson"></div>',
                    '<div id="idContactPerson" style="padding-top:7px;">'.$contact_person.'</div>',
                    '</div>',
                    '</div>',
                ),
                'alamat_instansi' => array(
                    '<div class="form-group">',
                    '<label for="idTxtAlamatInstansi" class="col-md-2 control-label">Alamat Instansi</label>',
                    '<div class="col-md-4">',
                    '<div id="idErrAlamatInstansi"></div>',
                    '<div id="idTxtAlamatInstansi" style="padding-top:7px;">'.$alamat_lengkap.'</div>',
                    '</div>',
                    '</div>',
                ),
                'alamat_pengiriman' => array(
                    '<div id="idFormAlamatCustomer">'.$form_dropdown.'</div>',
                ),
            );

            $data['form_btn'] = array(
                'btn_submit' => array(
                    '<button type="submit" name="btnSubmit" id="idBtnSubmit" class="btn btn-'.$btn_class.' pull-right" style="margin-right:5px;">',
                    $btn_form,
                    '</button>',
                ),
            );
            $data['form_box'] = 'idBoxFormBody';
            $data['form_alert'] = 'idAlertForm';
            $data['form_overlay'] = 'idOverlayFormQuo';
            $script['class_name'] = $this->class_link;
            $script['form_error'] = array('idErrSales', 'idErrNoQuo', 'idErrEmailSales', 'idErrDate', 'idErrMobilePhone', 'idErrCustomerId', 'idErrNmCustomer', 'idErrContactPerson', 'idErrAlamatInstansi', 'idErrAlamatKirim',);

            $this->load->section('form_js', 'script/'.$this->class_link.'/form_js', $script);
            $this->load->view('page/admin/v_form', $data);
        endif;
    }

    function submit_form($act){
        if ($this->input->is_ajax_request()) :
            $this->load->helper('email');
            $this->load->library(array('form_validation'));
            $str['csrf'] = $this->security->get_csrf_hash();

            $kd_mquotation = $this->input->post('txtKdQuotation');
            $tgl_quotation = $this->input->post('txtDate');
            $tgl_quotation = !empty($tgl_quotation)?format_date($tgl_quotation, 'Y-m-d'):$tgl_quotation;
            $salesperson_kd = $this->input->post('txtKdSales');
            $no_quotation = empty($this->input->post('txtNoQuotation'))?$this->tm_quotation->create_noquotation():$this->input->post('txtNoQuotation');
            $email_sales = $this->input->post('txtEmailSales');
            $mobile_sales = $this->input->post('txtMobilePhone');
            $customer_kd = $this->input->post('txtKdCustomer');
            $alamat_kirim_kd = $this->input->post('selAlamatKirim');
            $tipe_customer = $this->model_jenis_customer->get_tipe($customer_kd, 'tipe_customer');
            $tipe_harga = $this->model_jenis_customer->get_tipe($customer_kd, 'tipe_harga');
            $jml_ppn = $this->model_jenis_customer->get_col($customer_kd, 'set_ppn');
            if (!empty($jml_ppn)) :
                $jml_ppns = explode('.', $jml_ppn);
                $akhir_ppn = $jml_ppns[1] > 0?substr($jml_ppns[1], 0, 1):'';
                $pemisah = !empty($akhir_ppn)?'.':'';
                $set_ppn = $jml_ppns[0].$pemisah.$akhir_ppn;
            else :
                $set_ppn = '0';
            endif;
            $nm_kolom_ppn = 'PPN '.$set_ppn.'%';

            $this->form_validation->set_rules('txtKdSales', 'Nama Salesperson', 'required',
                array(
                    'required' => '{field} tidak boleh kosong!',
                )
            );
            $this->form_validation->set_rules('txtNmSales', 'Nama Salesperson', 'required',
                array(
                    'required' => '{field} tidak boleh kosong!',
                )
            );
            $this->form_validation->set_rules('txtEmailSales', 'Email Address', 'required|valid_email',
                array(
                    'required' => '{field} tidak boleh kosong!',
                    'valid_email' => '{field} harus mengunakan format email!',
                )
            );
            $this->form_validation->set_rules('txtMobilePhone', 'Mobile Phone', 'required',
                array(
                    'required' => '{field} tidak boleh kosong!',
                )
            );
            $this->form_validation->set_rules('txtDate', 'Date', 'required',
                array(
                    'required' => '{field} tidak boleh kosong!',
                )
            );
            $this->form_validation->set_rules('txtKdCustomer', 'Customer Name', 'required',
                array(
                    'required' => '{field} tidak boleh kosong!',
                )
            );
            $this->form_validation->set_rules('txtNmCustomer', 'Customer Name', 'required',
                array(
                    'required' => '{field} tidak boleh kosong!',
                )
            );

            if ($this->form_validation->run() == FALSE) :
                $str['confirm'] = 'errValidation';
                $str['idErrSales'] = (!empty(form_error('txtNmSales')))?buildLabel('warning', form_error('txtNmSales', '"', '"')):'';
                $str['idErrSales'] = (!empty(form_error('txtKdSales')))?buildLabel('warning', form_error('txtKdSales', '"', '"')):'';
                $str['idErrEmailSales'] = (!empty(form_error('txtEmailSales')))?buildLabel('warning', form_error('txtEmailSales', '"', '"')):'';
                $str['idErrMobilePhone'] = (!empty(form_error('txtMobilePhone')))?buildLabel('warning', form_error('txtMobilePhone', '"', '"')):'';
                $str['idErrDate'] = (!empty(form_error('txtDate')))?buildLabel('warning', form_error('txtDate', '"', '"')):'';
                $str['idErrNmCustomer'] = (!empty(form_error('txtNmCustomer')))?buildLabel('warning', form_error('txtNmCustomer', '"', '"')):'';
                $str['idErrNmCustomer'] = (!empty(form_error('txtKdCustomer')))?buildLabel('warning', form_error('txtKdCustomer', '"', '"')):'';
            else :
                if ($act == 'input') :
                    $conds = array(
                        'select' => 'tgl_input, '.$this->p_key,
                        'order_by' => 'tgl_input DESC',
                        'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
                    );
                    $kd_mquotation = $this->m_builder->buat_kode($this->tbl_name, $conds, 6, 'MQO');

                    // Insert into db_caterlindo_ppic
                    $data = array(
                        'kd_mquotation' => $kd_mquotation,
                        'salesperson_kd' => $salesperson_kd,
                        'email_sales' => $email_sales,
                        'mobile_sales' => $mobile_sales,
                        'no_quotation' => $no_quotation,
                        'tgl_quotation' => $tgl_quotation,
                        'customer_kd' => $customer_kd,
                        'alamat_kirim_kd' => $alamat_kirim_kd,
                        'status_quotation' => 'pending',
                        'tipe_customer' => $tipe_customer,
                        'tipe_harga' => $tipe_harga,
                        'nm_kolom_ppn' => $nm_kolom_ppn,
                        'jml_ppn' => $set_ppn,
                        'tgl_input' => date('Y-m-d H:i:s'),
                        'admin_kd' => $this->session->userdata('kd_admin'),
                    );
                    $aksi = $this->db->insert($this->tbl_name, $data);
                    $label_err = 'menambahkan';
                elseif ($act == 'edit') :

                    // Update db_caterlindo_ppic
                    $data = array(
                        'salesperson_kd' => $salesperson_kd,
                        'email_sales' => $email_sales,
                        'mobile_sales' => $mobile_sales,
                        'no_quotation' => $no_quotation,
                        'tgl_quotation' => $tgl_quotation,
                        'customer_kd' => $customer_kd,
                        'alamat_kirim_kd' => $alamat_kirim_kd,
                        'tipe_customer' => $tipe_customer,
                        'tipe_harga' => $tipe_harga,
                        'nm_kolom_ppn' => $nm_kolom_ppn,
                        'jml_ppn' => $set_ppn,
                        'tgl_edit' => date('Y-m-d H:i:s'),
                        'admin_kd' => $this->session->userdata('kd_admin'),
                    );
                    $where = array($this->p_key => $kd_mquotation);
                    $aksi = $this->db->update($this->tbl_name, $data, $where);
                    $label_err = 'mengubah';
                endif;

                if ($aksi) :
                    $input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' dengan kd_mquotation \''.$kd_mquotation.'\' salesperson_kd \''.$salesperson_kd.'\' email_sales \''.$email_sales.'\' mobile_sales \''.$mobile_sales.'\' no_quotation \''.$no_quotation.'\' tgl_quotation \''.$tgl_quotation.'\' customer_kd \''.$customer_kd.'\' alamat_kirim_kd \''.$alamat_kirim_kd.'\'');
                    $str['confirm'] = 'success';
                    $str['kd_mquotation'] = $kd_mquotation;
                    $str['no_quotation'] = $no_quotation;
                    $str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
                else :
                    $input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' '.$this->title.' dengan kd_mquotation \''.$kd_mquotation.'\' salesperson_kd \''.$salesperson_kd.'\' email_sales \''.$email_sales.'\' mobile_sales \''.$mobile_sales.'\' no_quotation \''.$no_quotation.'\' tgl_quotation \''.$tgl_quotation.'\' customer_kd \''.$customer_kd.'\' alamat_kirim_kd \''.$alamat_kirim_kd.'\'');
                    $str['confirm'] = 'errValidation';
                    $str['idErrForm'] = buildAlert('danger', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
                endif;
            endif;

            header('Content-Type: application/json');
            echo json_encode($str);
        endif;
    }

    function hapus_data() {
        if ($this->input->is_ajax_request()) :
            $kd_mquotation = $this->input->get('id');
            $label_err = 'menghapus';
            $aksi['master'] = $this->db->delete($this->tbl_name, array($this->p_key => $kd_mquotation));
            $aksi['item'] = $this->db->delete('td_quotation_item', array('mquotation_kd' => $kd_mquotation));
            $aksi['item_detail'] = $this->db->delete('td_quotation_item_detail', array('mquotation_kd' => $kd_mquotation));
            $aksi['harga'] = $this->db->delete('td_quotation_harga', array('mquotation_kd' => $kd_mquotation));

            if ($aksi) :
                $input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' dengan kd_mquotation \''.$kd_mquotation.'\', data yang mempunyai kd_mquotation \''.$kd_mquotation.'\' juga dihapus dari td_quotation_item, td_quotation_harga');
                $str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
            else :
                $input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' '.$this->title.' dengan kd_mquotation \''.$kd_mquotation.'\'');
                $str['alert'] = buildAlert('danger', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
            endif;

            header('Content-Type: application/json');
            echo json_encode($str);
        endif;
    }

    function dropdown_alamat(){
        if ($this->input->is_ajax_request()) :
            $kd_customer = $this->input->get('kd_customer');
            $kd_alamat = $this->input->get('kd_alamat_kirim');

            // dapatkan jumlah alamat kirim customer ditabel td_customer_alamat
            $this->db->select('a.kd_alamat_kirim, a.kd_badan_usaha, a.nm_customer, a.alamat, b.nm_negara, c.nm_provinsi, d.nm_kota, e.nm_kecamatan');
            $this->db->from('td_customer_alamat a');
            $this->db->join('tb_negara b', 'a.negara_kd = b.kd_negara', 'left');
            $this->db->join('tb_provinsi c', 'a.provinsi_kd = c.kd_provinsi AND a.negara_kd = c.negara_kd', 'left');
            $this->db->join('tb_kota d', 'a.kota_kd = d.kd_kota AND a.provinsi_kd = d.provinsi_kd AND a.negara_kd = d.negara_kd', 'left');
            $this->db->join('tb_kecamatan e', 'a.kecamatan_kd = e.kd_kecamatan AND a.kota_kd = e.kota_kd AND a.provinsi_kd = e.provinsi_kd AND a.negara_kd = e.negara_kd', 'left');
            $this->db->where(array('a.customer_kd' => $kd_customer));
            $query = $this->db->get();
            $num = $query->num_rows();

            if ($num > 1) :
                $result = $query->result();
                $pil_alamat = array('' => '-- Pilih Alamat Kirim --');
                foreach ($result as $row) :
                    $kd_alamat_kirim = $this->security->xss_clean($row->kd_alamat_kirim);
                    $nm_negara = $row->nm_negara.after_before_char($row->nm_negara, $row->nm_negara, ', ', '.');
                    $nm_provinsi = $row->nm_provinsi.after_before_char($row->nm_provinsi, $row->nm_negara, ', ', '.');
                    $nm_kota = $row->nm_kota.after_before_char($row->nm_kota, array($row->nm_provinsi, $row->nm_kota), ', ', '.');
                    $nm_kecamatan = $row->nm_kecamatan.after_before_char($row->nm_kecamatan, array($row->nm_negara, $row->nm_provinsi, $row->nm_kota), ', ', '.');
                    $alamat = ucwords(strtolower($this->security->xss_clean($result->alamat))).after_before_char($result->alamat, array($row->nm_kecamatan, $row->nm_negara, $row->nm_provinsi, $row->nm_kota), '. ', ', ');
                    $kd_badan_usaha = $this->security->xss_clean($row->kd_badan_usaha);
                    $nm_customer = $this->security->xss_clean($row->nm_customer);
                    $this->db->select('nm_select');
                    $this->db->from('tb_set_dropdown');
                    $this->db->where(array('id' => $kd_badan_usaha));
                    $query = $this->db->get();
                    $result = $query->row();
                    $nm_select = $this->security->xss_clean($result->nm_select);
                    $nm_badan_usaha = ($nm_select == 'Lainnya')?''.$nm_customer.' - ':$nm_select.' '.$nm_customer.' - ';
                    $alamat = $nm_badan_usaha.$alamat.$nm_kecamatan.$nm_kota.$nm_provinsi.$nm_negara;
                    $pil_alamat[$kd_alamat_kirim] = $alamat;
                endforeach;
                $sel_alamat = $kd_alamat;
                $attr_alamat = array('id' => 'idSelAlamatKirim', 'class' => 'form-control select2');
                $form_alamat = form_dropdown('selAlamatKirim', $pil_alamat, $sel_alamat, $attr_alamat);
            elseif ($num == 1) :
                $result = $query->row();
                $kd_alamat_kirim = $this->security->xss_clean($result->kd_alamat_kirim);
                $nm_negara = $result->nm_negara.after_before_char($result->nm_negara, $result->nm_negara, ', ', '.');
                $nm_provinsi = $result->nm_provinsi.after_before_char($result->nm_provinsi, $result->nm_negara, ', ', '.');
                $nm_kota = $result->nm_kota.after_before_char($result->nm_kota, array($result->nm_provinsi, $result->nm_kota), ', ', '.');
                $nm_kecamatan = $result->nm_kecamatan.after_before_char($result->nm_kecamatan, array($result->nm_negara, $result->nm_provinsi, $result->nm_kota), ', ', '.');
                $alamat = ucwords(strtolower($this->security->xss_clean($result->alamat))).after_before_char($result->alamat, array($result->nm_kecamatan, $result->nm_negara, $result->nm_provinsi, $result->nm_kota), '. ', ', ');
                $alamat = $alamat.$nm_kecamatan.$nm_kota.$nm_provinsi.$nm_negara;

                $form_alamat = '';
                $form_alamat .= form_input(array('type' => 'hidden', 'name' => 'selAlamatKirim', 'id' => 'idSelAlamatKirim', 'value' => $kd_alamat_kirim));
                $form_alamat .= $alamat;
            else :
                $form_alamat = '';
            endif;

            $form_dropdown = '';
            if (!empty($form_alamat)) :
                $form_dropdown .= '<div class="form-group">';
                $form_dropdown .= '<label for="idSelAlamatKirim" class="col-md-2 control-label">Alamat Pengiriman</label>';
                $form_dropdown .= '<div class="col-md-4">';
                $form_dropdown .= '<div id="idErrAlamatKirim"></div>';
                $form_dropdown .= $form_alamat;
                $form_dropdown .= '</div>';
                $form_dropdown .= '</div>';
            endif;

            $str['form_alamat'] = $form_dropdown;
            header('Content-Type: application/json');
            echo json_encode($str);
        endif;
    }

    function btn_customer() {
        if ($this->input->is_ajax_request()) :
            $kd_customer = $this->input->get('kd_customer');

            if (empty($kd_customer)) :
                $btn = '<a onclick="return showFormCustomer(\'\', \'\');" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Customer</a>';
            else :
                $btn = '<a onclick="return clearCustomerDetail();" class="btn btn-sm btn-danger" style="margin-right:5px;"><i class="fa fa-trash"></i> Clear Values</a>';
                $btn .= '<a onclick="return showFormCustomer(\''.$kd_customer.'\', \'\');" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i> Customer</a>';
            endif;

            $str['btn_form'] = $btn;
            header('Content-Type: application/json');
            echo json_encode($str);
        endif;
    }

    function ubah_status() {
        if ($this->input->is_ajax_request()) :
            $id = $this->input->get('id');
            $status = $this->input->get('status');

            $data = array(
                'status_quotation' => $status,
            );
            $where = array($this->p_key => $id);
            $aksi = $this->db->update($this->tbl_name, $data, $where);
            $label_err = 'mengubah status';

            if ($aksi) :
                $input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' dengan kd_mquotation \''.$id.'\' menjadi status_quotation \''.$status.'\'');
                $str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
            else :
                $input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' '.$this->title.' dengan kd_mquotation \''.$id.'\' menjadi status_quotation \''.$status.'\'');
                $str['alert'] = buildAlert('danger', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
            endif;

            header('Content-Type: application/json');
            echo json_encode($str);
        endif;
    }

    function cetak_quotation($kd_mquotation = '', $default_disc = '') {
        parent::admin_print();
        $this->load->model(array('td_quotation_item_disc'));

        $data = array();
        $m_detail = $this->m_quotation->quotation_master_detail($kd_mquotation);
        $retail_curr = $this->tb_tipe_harga->get_data('Harga Retail');
        $detail_retail = $this->tm_currency->get_data($retail_curr->currency_kd);
        $data['val_retail'] = '';
        $data['val_detail'] = '';
        $data['type_retail'] = $detail_retail->currency_type;
        if ($detail_retail->currency_type == 'secondary') :
            $data['val_retail'] = $this->td_currency_convert->get_value($retail_curr->currency_kd, $m_detail->tgl_quotation);
        endif;
        if ($m_detail->currency_type == 'secondary') :
            $data['val_detail'] = $this->td_currency_convert->get_value($m_detail->currency_kd, $m_detail->tgl_quotation);
        endif;
        if ($m_detail) :
            $kecamatan = $m_detail->nm_kecamatan.after_before_char($m_detail->nm_kecamatan, array($m_detail->nm_kota, $m_detail->nm_provinsi, $m_detail->nm_negara), ', ', '.');
            $kota = $m_detail->nm_kota.after_before_char($m_detail->nm_kota, array($m_detail->nm_provinsi, $m_detail->nm_negara), ', ', '.');
            $provinsi = $m_detail->nm_provinsi.after_before_char($m_detail->nm_provinsi, $m_detail->nm_negara, ', ', '.');
            $kode_pos = $m_detail->kode_pos.after_before_char($m_detail->kode_pos, $m_detail->kode_pos, ', ', '.');
            $negara = $m_detail->nm_negara.after_before_char($m_detail->nm_negara, $m_detail->nm_negara, '.', '.');
            $data['icon_type'] = $m_detail->icon_type;
            $data['currency_type'] = $m_detail->currency_type;
            $data['currency_icon'] = $m_detail->currency_icon;
            $data['pemisah_angka'] = $m_detail->pemisah_angka;
            $data['format_akhir'] = $m_detail->format_akhir;
            
            $tgl_quo = format_date($m_detail->tgl_quotation, 'd-m-Y');
            $data['tgl_quotation'] = $tgl_quo;
            $data['nm_salesperson'] = $m_detail->nm_salesperson;
            $data['telp_salesperson'] = $m_detail->mobile_sales;
            $data['email_salesperson'] = $m_detail->email_sales;
            $data['tipe_customer'] = $m_detail->tipe_customer;
            $data['header_customer'] = $m_detail->header_customer;
            $data['nm_customer'] = $m_detail->nm_customer;
            $data['code_customer'] = $m_detail->code_customer;
            $data['alamat_satu'] = ucwords(strtolower($m_detail->alamat));
            $data['alamat_dua'] = $kecamatan.$kota.$provinsi.$kode_pos.$negara;
            $data['telp_customer'] = !empty($m_detail->no_telp_utama)?$m_detail->no_telp_utama:$m_detail->no_telp_lain;
            $data['email_customer'] = $m_detail->email;
            $data['term_payment_format'] = $m_detail->term_payment_format;
            $data['no_quotation'] = $m_detail->no_quotation;
            $data['status_quotation'] = $m_detail->status_quotation;
        endif;

        $items_data = $this->m_quotation->quotatationItems($kd_mquotation);
        if (!empty($items_data)) :
            foreach ($items_data as $item_data) :
                $kd_ditem_quotation[] = $item_data->kd_ditem_quotation;
            endforeach;
            $details_data = $this->m_quotation->item_child($kd_ditem_quotation);
            $data['items_data'] = $items_data;
            $data['details_data'] = $details_data;
            $data['data_discs'] = $this->td_quotation_item_disc->get_all_where(array('mquotation_kd' => $kd_mquotation));
            $data['nm_kolom_ppn'] = $m_detail->nm_kolom_ppn;
            $data['jml_ppn'] = $m_detail->jml_ppn;
            $data['ongkir'] = $this->m_master_quotation->detail_price('ongkir', $kd_mquotation);
            $data['install'] = $this->m_master_quotation->detail_price('install', $kd_mquotation);
            $data['total_price_data'] = $this->table_total_price($kd_mquotation);
            $data['tot_potongan'] = $this->m_master_quotation->total_potongan($kd_mquotation);
        endif;

        $row_format = $this->m_format_laporan->read_data('quotation');
        if ($row_format) :
            $data['title'] = $row_format->laporan_title;
            $data['footer'] = $row_format->laporan_footer;

            // Coba pecah term payment format
            if (!empty($m_detail)) :
                $term_payment_format = $m_detail->term_payment_format;
                $footer = $row_format->laporan_footer;
                if (strpos($footer, '{term_payment}') !== FALSE) :
                    $rep_footer = str_replace('{term_payment}', $term_payment_format, $footer);
                    $data['footer'] = $rep_footer;
                    $data['term_payment_format'] = '';
                endif; 
            endif;
        endif;

        $this->load->view('page/sales/quotation_items/print', $data);
    }

    function get_ppn() {
        $var = 'set_ppn';
        $result = $this->m_app_setting->get_data($var);
        $value = $result->nilai_setting;

        return $result?$value:FALSE;
    }

    function create_no_so($tipe_customer = '') {
        $this->load->model(array('tm_salesorder'));
        // Dapatkan format no quotation
        if ($tipe_customer == 'Ekspor') :
            $code = 'salesorderekspor';
        elseif ($tipe_customer == 'Lokal' || empty($tipe_customer)) :
            $code = 'salesorderlocal';
            $tipe_customer = 'Lokal';
        endif;
        $no_salesorder = $this->tm_salesorder->create_nosalesorder($code, $tipe_customer);

        return $no_salesorder;
    }

    function transfer_sales() {
        if ($this->input->is_ajax_request()) :
            $kd_customer = $this->input->post('txtKdCustomer');
            $nm_customer = $this->input->post('txtNamaCustomer');
            $kd_mquotation = $this->input->post('selQuo');
            if (empty($kd_customer) || empty($nm_customer) || empty($kd_mquotation)) :
                $str['confirm'] = 'error';
                $str['alert'] = buildAlert('danger', 'Gagal!', 'Gagal mengubah status '.$this->title.' Kode Quotation kosong boss!');
            
                $str['csrf'] = $this->security->get_csrf_hash();
                header('Content-Type: application/json');
                echo json_encode($str);
                exit();
            endif;
            if (is_array($kd_mquotation)) :
                // Jika kode master quotation adalah array maka check apakah ada array yang nilainya sama?
                $same = 0;
                $same_val = array_count_values($kd_mquotation);
                foreach ($kd_mquotation as $kd) :
                    if (!empty($kd)) :
                        if ($same_val[$kd] > 1) :
                            $same++;
                        endif;
                    endif;
                endforeach;
                if ($same == 0) :
                    $str['confirm'] = 'success';
                    $str['kd_mquotation'] = $kd_mquotation;
                else :
                    $str['confirm'] = 'error';
                    $str['alert'] = buildAlert('danger', 'Gagal!', 'Gagal mengubah status '.$this->title.' Kode Quotation tidak boleh sama!');
                endif;
            else :
                $str['confirm'] = 'success';
                $str['kd_mquotation'] = $kd_mquotation;
            endif;
            
            $str['csrf'] = $this->security->get_csrf_hash();
            header('Content-Type: application/json');
            echo json_encode($str);
            exit();
        endif;
    }

    function table_total_price($kd_mquotation) {
        $price_data = $this->m_quotation->get_price($kd_mquotation);
        return $price_data;
    }

    function write_log($stat, $var, $data = array()){
        $nm_kolom = '';
        $no = 0;
        $jml = count($data);
        foreach ($data as $key) :
            $no++;
            $koma = $no == $jml?'':',';
            if (is_array($key)) :
                foreach ($key as $val) :
                    $nm_kolom .= ' '.$key.' = '.$val.$koma;
                endforeach;
            else :
                $nm_kolom .= ' '.$key.$koma;
            endif;
        endforeach;
        $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' '.$stat.' '.$var.' '.$this->title.' dengan'.$nm_kolom);
    }

    public function transfer_quo_so() {
        $id = $this->input->get('id');
        $str = $this->m_quotation->trans_quo_so($id);
        header('Content-Type: application/json');
        echo json_encode($str);
    }
}