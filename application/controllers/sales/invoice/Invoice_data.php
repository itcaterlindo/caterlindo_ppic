<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Invoice_data extends MY_Controller {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'tm_salesorder';
	private $p_key = 'kd_msalesorder';
	private $class_link = 'sales/invoice';
	private $title = 'Invoice';

	function __construct() {
		parent::__construct();

		$this->load->library(array('form_validation'));
		$this->load->model(array('m_builder', 'model_salesorder', 'm_salesorder', 'm_format_laporan', 'tb_tipe_harga', 'tm_currency', 'td_currency_convert', 'tm_salesorder', 'm_master_quotation', 'tb_default_disc', 'model_do', 'tm_deliveryorder'));
	}

	function tabel_data($kd_msalesorder) {
		$items_data = $this->m_salesorder->salesOrderItems($kd_msalesorder);
		if (!empty($items_data)) :
			foreach ($items_data as $item_data) :
				$kd_ditem_so[] = $item_data->kd_ditem_so;
			endforeach;
			$details_data = $this->m_salesorder->item_child($kd_ditem_so);
			$data['items_data'] = $items_data;
			$data['details_data'] = $details_data;
			$data['tot_potongan'] = $this->m_salesorder->get_price($kd_msalesorder);
			return $data;
		endif;
	}

	public function cetak($tipe_cust = '', $invoice_stat = '', $kd_msales = '', $proforma = '') {
		$this->load->model(array('td_so_termin'));
		parent::admin_print();

		$m_detail = $this->model_salesorder->read_masterdata($kd_msales);
		if ($tipe_cust == 'ekspor') :
			$invoice_tipe =  'invoiceekspor';
			$invoice_file =  'ekspor/view_invoice_ekspor';
		elseif ($tipe_cust == 'lokal') :
			$invoice_tipe =  'invoicelocal';
			//$invoice_tipe =  'salesorderlocal';
			$invoice_file =  'local/view_invoice_local';
		endif;
		$data['format_laporan'] = $this->m_format_laporan->view_laporan($invoice_tipe, $m_detail['term_payment_format']);
		$m_detail['tgl_so'] = format_date($m_detail['tgl_dp'], 'd M, Y');
		if (!empty($proforma) && $proforma == 'proforma') :
			$data['format'] = 'proforma';
			// $data['format_laporan']['laporan_title'] = '<p style="margin-left:40px"><img alt="" src="http://202.148.25.50/caterlindo_ppic/assets/admin_assets/dist/img/invoice_header.png" style="width:450px" /></p><p style="margin-left:40px; text-align:center"><strong><span style="font-size:20px">PROFORMA INVOICE</span></strong></p>';
			// $data['format_laporan']['laporan_title'] = '<p style="margin-left:40px"><img alt="" src="'.my_baseurl().'assets/admin_assets/dist/img/invoice_header.png" style="width:450px" /></p><p style="margin-left:40px; text-align:center"><strong><span style="font-size:20px">PROFORMA INVOICE</span></strong></p>';
			$data['format_laporan']['laporan_title'] = '
			<table border="0">
				<tr>
					<td style="width: 40%;">
						<img alt="" src="'.my_baseurl().'assets/admin_assets/dist/img/LogoSS2021.jpg" />
					</td>
					
					<td style="width: 60%;">

						<table border="0" style="width: 100%;"> 
							<tr>
								<td colspan="3" style="text-align: right; font-size: 36px; font-weight: bold;">PROFORMA INVOICE</td>
							</tr>
							<tr>
								<td width="30%"></td>
								<td width="35%" style="font-size: 12px; text-align: left; border-bottom: 1px solid black;">No. Dokumen</td>
								<td width="35%" style="font-size: 12px; text-align: left; border-bottom: 1px solid black;">: CAT4-SLS-012</td>
							</tr>
							<tr>
								<td></td>
								<td style="font-size: 12px; text-align: left; border-bottom: 1px solid black;">Tanggal</td>
								<td style="font-size: 12px; text-align: left; border-bottom: 1px solid black;">: 12 Agustus 2021</td>
							</tr>
							<tr>
								<td></td>
								<td style="font-size: 12px; text-align: left; border-bottom: 1px solid black;">Revisi</td>
								<td style="font-size: 12px; text-align: left; border-bottom: 1px solid black;">: 00</td>
							</tr>
						</table>

					</td>
				</tr>
			</table>';
		else :
			$data['format'] = 'final';
			//$data['format_laporan']['laporan_title'] = '<p style="margin-left:40px"><img alt="" src="http://202.148.25.50/caterlindo_ppic/assets/admin_assets/dist/img/invoice_header.png" style="width:450px" /></p><p style="margin-left:40px; text-align:center"><strong><span style="font-size:20px">INVOICE LOCAL</span></strong></p>';
			if (!empty($proforma) && $proforma != 'proforma') :
				$data_termin = $this->td_so_termin->read_data($proforma);
				$m_detail['no_invoice_dp'] = $data_termin->no_invoice;
				$m_detail['jml_dp'] = $data_termin->jml_termin;
				$m_detail['jml_ppn_dp'] = $data_termin->jml_termin_ppn;
				$m_detail['tgl_so'] = format_date($data_termin->tgl_termin, 'd M, Y');
			endif;
		endif;
		$data['master_head'] = $m_detail;
		if ($invoice_stat == 'dp') :
			$data['master_head']['no_invoice'] = $m_detail['no_invoice_dp'];
		elseif ($invoice_stat == 'final') :
			$data['master_head']['no_invoice'] = '';
		endif;
		$data['item_detail'] = $this->tabel_data($kd_msales);
		$data['item_detail']['page_type'] = 'print';
		$data['item_detail']['invoice_stat'] = $invoice_stat;
		$data['class_link'] = $this->class_link;
		$data['kd_msalesorder'] = $kd_msales;
		$def_discs = $this->tb_default_disc->get_all($m_detail['jenis_customer_kd']);
		foreach ($def_discs as $def_disc) :
			if ($def_disc->nm_pihak == 'Distributor' && $def_disc->status == '1' && $this->session->view_diskon_distributor == '1') :
				$data['default_disc'] = $def_disc->jml_disc;
				$this->load->view('page/'.$this->class_link.'/'.$invoice_file, $data);
			endif;
			if ($def_disc->nm_pihak == 'Customer' && $def_disc->status == '1' && $this->session->view_diskon_customer == '1') :
				$data['default_disc'] = $def_disc->jml_disc;
				$this->load->view('page/'.$this->class_link.'/'.$invoice_file, $data);
			endif;
		endforeach;
	}

	public function cetak_inv_do($dp = '0', $tipe_invoice = '', $kd_mdo = '') {
		parent::admin_print();

		$this->load->model(array('td_salesorder_item_disc', 'td_so_termin'));
		$no_invoice = $this->tm_deliveryorder->get_no_invoice($kd_mdo);
		$lists_kd = $this->tm_deliveryorder->get_same_kd($no_invoice);
		if (!empty($lists_kd)) :
			$no = 0;
			foreach ($lists_kd as $list) :
				$no++;
				if ($no == 1) :
					$first_kd = $list->kd_mdo;
				else :
					$first_kd = $first_kd;
				endif;
				$list_kd[] = $list->kd_mdo;
				$list_kdso[] = $list->msalesorder_kd;
				$list_nopo[] = $list->no_po;
			endforeach;
		else :
			$list_kd = array('');
			$list_kdso = array('');
			$list_nopo = array('');
			$first_kd = '';
		endif;
		$headers = $this->model_do->get_header_detail($first_kd);
		$m_detail = $this->model_salesorder->read_masterdata($headers['msalesorder_kd']);
		if ($headers['tipe_do'] == 'Ekspor') :
			//$invoice_tipe = 'invoiceekspor';
			$invoice_tipe = 'invoiceekspor_do';
			$invoice_file = 'inv_do/ekspor/v_inv_ekspor';
		elseif ($headers['tipe_do'] == 'Lokal') :
			//$invoice_tipe = 'invoicelocal';
			$invoice_tipe = 'invoicelocal_do';
			$invoice_file = 'inv_do/lokal/v_inv_lokal';
		endif;
		$data['format_laporan'] = $this->m_format_laporan->view_laporan($invoice_tipe, $m_detail['term_payment_format']);
		$data['master_head'] = $headers;
		$data['item_detail']['so_termin'] = $this->td_so_termin->read_all($list_kdso);
		$data['item_detail']['parent_items'] = $this->model_do->list_parent_items($list_kd, '', $headers['tipe_do']);
		$data['item_detail']['child_items'] = $this->model_do->list_child_items($list_kd, '', $headers['tipe_do']);
		$data['item_detail']['tot_potongan'] = $this->m_salesorder->get_price($list_kdso);
		$data['class_link'] = $this->class_link;
		$data['tipe_invoice'] = $tipe_invoice;
		$data['kd_mdo'] = $kd_mdo;
		$data['list_nopo'] = $list_nopo;
		$data['item_detail']['is_dp'] = $dp?TRUE:FALSE;
		$data['item_detail']['default_disc'] = $this->td_salesorder_item_disc->get_where_array_so($list_kdso, array('nm_pihak' => $tipe_invoice));
		$this->load->view('page/'.$this->class_link.'/'.$invoice_file, $data);
	}
}