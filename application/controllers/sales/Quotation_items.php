<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotation_items extends MY_Controller {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'td_quotation_item';
	private $p_key = 'kd_ditem_quotation';
	private $class_link = 'sales/quotation_items';
	private $title = 'Quotation Items';

	function __construct() {
		parent::__construct();

		$this->load->model(array('m_builder', 'm_quotation', 'm_app_setting', 'm_master_quotation', 'm_icon_currency', 'tb_tipe_harga', 'tm_currency', 'td_currency_convert', 'tm_barang'));
		$this->load->helper(array('form', 'html', 'my_helper'));
		$this->load->library('table');
	}

	function get_desc($kd_mquotation) {
		$this->db->select('kd_mquotation, tipe_harga, tipe_customer');
		$this->db->from('tm_quotation');
		$this->db->where(array('kd_mquotation' => $kd_mquotation));
		$query = $this->db->get();
		$row = $query->row();

		return $row;
	}

	function data_form(){
		if ($this->input->is_ajax_request()) :
			$this->load->model(array('tb_default_disc'));
			$id = $this->input->get('id');
			$m_detail = $this->m_quotation->quotation_master_detail($id);
			$kecamatan = $m_detail->nm_kecamatan.after_before_char($m_detail->nm_kecamatan, array($m_detail->nm_kota, $m_detail->nm_provinsi, $m_detail->nm_negara), ', ', '.');
			$kota = $m_detail->nm_kota.after_before_char($m_detail->nm_kota, array($m_detail->nm_provinsi, $m_detail->nm_negara), ', ', '.');
			$provinsi = $m_detail->nm_provinsi.after_before_char($m_detail->nm_provinsi, array($m_detail->nm_negara), ', ', '.');
			$negara = $m_detail->nm_negara.after_before_char($m_detail->nm_negara, array($m_detail->nm_negara), '.', '.');

			$data['kd_mquotation'] = $id;
			$data['tgl_quotation'] = $m_detail->tgl_quotation;
			$data['kd_jenis_customer'] = $m_detail->kd_jenis_customer;
			$data['nm_salesperson'] = $m_detail->nm_salesperson;
			$data['telp_salesperson'] = $m_detail->mobile_sales;
			$data['email_salesperson'] = $m_detail->email_sales;
			$data['nm_customer'] = $m_detail->nm_customer;
			$data['alamat_satu'] = ucwords(strtolower($m_detail->alamat));
			$data['alamat_dua'] = $kecamatan.$kota.$provinsi.$negara;
			$data['telp_customer'] = !empty($m_detail->no_telp_utama)?$m_detail->no_telp_utama:$m_detail->no_telp_lain;
			$data['email_customer'] = $m_detail->email;
			$data['no_quotation'] = $m_detail->no_quotation;
			$data['nm_kolom_ppn'] = $m_detail->nm_kolom_ppn;
			$data['jml_ppn'] = $m_detail->jml_ppn;
			$data['custom_barcode'] = $m_detail->custom_barcode;
			$data['status_quotation'] = $m_detail->status_quotation;
			$data['decimal'] = !empty($m_detail->decimal)?$m_detail->decimal:'0';
			$data['detail'] = $this->get_desc($id);
            $data['class_name'] = $this->class_link;

			$this->load->view('page/sales/quotation_items/form', $data);
		endif;
	}

	function detail_form() {
		if ($this->input->is_ajax_request()) :
			$id = $this->input->get('id');
			$data = $this->get_master($id);
			$data['detail'] = $this->get_desc($id);
			$data['class_name'] = $this->class_link;

			$this->load->view('page/sales/quotation_items/detail', $data);
		endif;
	}

	function get_master($id = '') {
		$this->load->model(array('td_quotation_item_disc'));

		$m_detail = $this->m_quotation->quotation_master_detail($id);
		$retail_curr = $this->tb_tipe_harga->get_data('Harga Retail');
		$detail_retail = $this->tm_currency->get_data($retail_curr->currency_kd);
		$data['val_retail'] = '';
		$data['val_detail'] = '';
		$data['type_retail'] = $detail_retail->currency_type;
		if ($detail_retail->currency_type == 'secondary') :
			$data['val_retail'] = $this->td_currency_convert->get_value($retail_curr->currency_kd, $m_detail->tgl_quotation);
		endif;
		if ($m_detail->currency_type == 'secondary') :
			$data['val_detail'] = $this->td_currency_convert->get_value($m_detail->currency_kd, $m_detail->tgl_quotation);
		endif;
		$kecamatan = $m_detail->nm_kecamatan.after_before_char($m_detail->nm_kecamatan, array($m_detail->nm_kota, $m_detail->nm_provinsi, $m_detail->nm_negara), ', ', '.');
		$kota = $m_detail->nm_kota.after_before_char($m_detail->nm_kota, array($m_detail->nm_provinsi, $m_detail->nm_negara), ', ', '.');
		$provinsi = $m_detail->nm_provinsi.after_before_char($m_detail->nm_provinsi, array($m_detail->nm_negara), ', ', '.');
		$kode_pos = $m_detail->kode_pos.after_before_char($m_detail->kode_pos, array($m_detail->kode_pos), ', ', '.');
		$negara = $m_detail->nm_negara.after_before_char($m_detail->nm_negara, array($m_detail->nm_negara), '.', '.');

		$items_data = $this->m_quotation->quotatationItems($id);
		if (!empty($items_data)) :
			foreach ($items_data as $item_data) :
				$kd_ditem_quotation[] = $item_data->kd_ditem_quotation;
			endforeach;
			$details_data = $this->m_quotation->item_child($kd_ditem_quotation);
			$data['items_data'] = $items_data;
			$data['data_discs'] = $this->td_quotation_item_disc->get_all_where(array('mquotation_kd' => $id));
			$data['details_data'] = $details_data;
			$data['nm_kolom_ppn'] = $m_detail->nm_kolom_ppn;
			$data['jml_ppn'] = $m_detail->jml_ppn;
			$data['ongkir'] = $this->m_master_quotation->detail_price('ongkir', $id);
			$data['install'] = $this->m_master_quotation->detail_price('install', $id);
			$data['total_price_data'] = $this->table_total_price($id);
			$data['tot_potongan'] = $this->m_master_quotation->total_potongan($id);
			$data['tot_default_disc'] = $this->td_quotation_item_disc->default_disc_list(array('mquotation_kd' => $id));
		endif;

		$data['kd_mquotation'] = $id;
		$data['icon_type'] = $m_detail->icon_type;
		$data['currency_type'] = $m_detail->currency_type;
		$data['currency_icon'] = $m_detail->icon_type.' '.$m_detail->currency_icon.' '.$m_detail->pemisah_angka.' '.$m_detail->format_akhir;
		$data['tgl_quotation'] = $m_detail->tgl_quotation;
		$data['nm_salesperson'] = $m_detail->nm_salesperson;
		$data['telp_salesperson'] = $m_detail->mobile_sales;
		$data['email_salesperson'] = $m_detail->email_sales;
		$data['nm_customer'] = $m_detail->nm_customer;
		$data['tipe_customer'] = $m_detail->tipe_customer;
		$data['alamat_satu'] = ucwords(strtolower($m_detail->alamat));
		$data['alamat_dua'] = $kecamatan.$kota.$provinsi.$kode_pos.$negara;
		$data['telp_customer'] = !empty($m_detail->no_telp_utama)?$m_detail->no_telp_utama:$m_detail->no_telp_lain;
		$data['email_customer'] = $m_detail->email;
		$data['no_quotation'] = $m_detail->no_quotation;
		$data['status_quotation'] = $m_detail->status_quotation;
		$data['header_customer'] = $m_detail->header_customer;
		return $data;
	}

	function submit_form($act){
		if ($this->input->is_ajax_request()) :
			$this->db->trans_begin();
			$this->load->library(array('form_validation'));
			$str['csrf'] = $this->security->get_csrf_hash();
			$set_row = $this->m_app_setting->get_data('custom_barcode');

			$kd_ditem_quotation = $this->input->post('txtKdDQuotation');
			$mquotation_kd = $this->input->post('txtKdMQuotation');
			$item_type = $this->input->post('txtItemType');
			$item_status = $this->input->post('selItemStatus');
			/** Jika status custom maka kode barang set ke custom, jika status standart kode barang set ke standart */
			if($item_status == 'std'):
				$barang_kd = $this->input->post('txtKdBarangStandart');
			else:
				/** Hardcode code barang custom */
				$barang_kd = 'PRD020817000573';
			endif;
			$item_desc = $this->input->post('txtItemDescOri');
			$item_dimension = $this->input->post('txtItemDimensionOri');
			$harga_retail = $this->input->post('txtProductPriceRetail');
			$harga_barang = $this->input->post('txtProductPrice');
			$item_barcode = $this->input->post('txtBarcode');
			if ($item_status == 'custom') :
				$item_desc = $this->input->post('txtItemDesc');
				$item_dimension = $this->input->post('txtItemDimension');
				$harga_retail = $this->input->post('txtProductPrice');
				$harga_barang = $this->input->post('txtProductPrice');
			endif;
			$item_qty = $this->input->post('txtProductQty');
			$disc_type = $this->input->post('selDiscType');
			$item_disc = $this->input->post('txtProductDisc');
			$total_harga = $this->input->post('txtProductTotPrice');
			$relation_status = $this->input->post('txtRelation');
			$item_note = $this->input->post('txtProductNote');
			$kd_parent_quo = $this->input->post('txtKdParentQuotation');
			$kd_parent_item = $this->input->post('txtKdParent');
			$view_access = $this->input->post('txtDiscViewAccess');
			$nm_pihak = $this->input->post('txtNmPihakDisc');
			$jml_disc = $this->input->post('txtDefaultDisc');
			$type_individual_disc = $this->input->post('selIndividualDisc');
			$jml_individual_disc = $this->input->post('txtIndividualDiscVal');

			if (empty($kd_parent_item)) :
				if($item_status == 'std'):
					$this->form_validation->set_rules('txtKdBarangStandart', 'Text Product Code (Kode Barang)', 'required',
					array(
						'required' => '{field} tidak boleh kosong!',
					)
				);
				endif;

				$this->form_validation->set_rules('txtProductCode', 'Text Product Code', 'required',
					array(
						'required' => '{field} tidak boleh kosong!',
					)
				);
			else :
				$this->form_validation->set_rules('selProductChild', 'Pilihan Product Code', 'required',
					array(
						'required' => '{field} tidak boleh kosong!',
					)
				);
			endif;
			$this->form_validation->set_rules('txtProductQty', 'Qty', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtBarcode', 'Barcode', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'errValidation';
				if (empty($kd_parent_item)) :
					$str['idErrProductCode'] = (!empty(form_error('txtProductCode')))?buildLabel('warning', form_error('txtProductCode', '"', '"')):'';
					$str['idErrProductCode'] = (!empty(form_error('txtKdBarangStandart')))?buildLabel('warning', form_error('txtKdBarangStandart', '"', '"')):'';
				else :
					$str['idErrProductCode'] = (!empty(form_error('selProductChild')))?buildLabel('warning', form_error('selProductChild', '"', '"')):'';
				endif;
				$str['idErrProductQty'] = (!empty(form_error('txtProductQty')))?buildLabel('warning', form_error('txtProductQty', '"', '"')):'';
				$str['idErrBarcode'] = (!empty(form_error('txtBarcode')))?buildLabel('warning', form_error('txtBarcode', '"', '"')):'';
				$str['product_code'] = $this->input->post('txtProductCode');
			else :
				if ($act == 'input') :
					if ($relation_status == 'parent') :
						$kd_ditem_quotation = $this->create_code('DQI', $this->p_key, $this->tbl_name);

						$data = array(
							$this->p_key => $kd_ditem_quotation,
							'mquotation_kd' => $mquotation_kd,
							'barang_kd' => $barang_kd,
							'item_code' => $this->input->post('txtProductCode'),
							'item_barcode' => $item_barcode,
							'item_status' => $item_status,
							'item_desc' => $item_desc,
							'item_dimension' => $item_dimension,
							'netweight' => $this->input->post('txtNetweight'),
							'grossweight' => $this->input->post('txtGrossweight'),
							'boxweight' => $this->input->post('txtBoxweight'),
							'length_cm' => $this->input->post('txtLength'),
							'width_cm' => $this->input->post('txtWidth'),
							'height_cm' => $this->input->post('txtHeight'),
							'item_cbm' => $this->input->post('txtItemCbm'),
							'item_qty' => $item_qty,
							'harga_barang' => $harga_barang,
							'harga_retail' => $harga_retail,
							'disc_type' => $disc_type,
							'item_disc' => $item_disc,
							'total_harga' => $total_harga,
							'item_note' => $item_note,
							'tgl_input' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin'),
						);
						$aksi['item'] = $this->db->insert($this->tbl_name, $data);

						$kd_item_disc = $this->create_code('PQO', 'kd_item_disc', 'td_quotation_item_disc');
						for ($i = 0; $i < count($nm_pihak); $i++) :
							$kd_item_disc = $i == 0?$kd_item_disc:$this->create_inc_code($kd_item_disc, 'PQO');
							$data_disc[] = array(
								'kd_item_disc' => $kd_item_disc,
								'quo_item_kd' => $kd_ditem_quotation,
								'mquotation_kd' => $mquotation_kd,
								'item_relation' => $relation_status,
								'nm_pihak' => $nm_pihak[$i],
								'jml_disc' => $jml_disc[$i],
								'type_individual_disc' => $type_individual_disc[$i],
								'jml_individual_disc' => $jml_individual_disc[$i],
								'view_access' => $view_access[$i],
								'tgl_input' => date('Y-m-d H:i:s')
							);
						endfor;
						if (isset($data_disc)) :
							$aksi['disc_default'] = $this->db->insert_batch('td_quotation_item_disc', $data_disc);
						endif;
						$str['relation'] = 'parent';
					elseif ($relation_status == 'child') :
						$kd_citem_quotation = $this->create_code('DQC', 'kd_citem_quotation', 'td_quotation_item_detail');

						$data = array(
							'kd_citem_quotation' => $kd_citem_quotation,
							'ditem_quotation_kd' => $kd_parent_quo,
							'mquotation_kd' => $mquotation_kd,
							'kd_parent' => $kd_parent_item,
							'kd_child' => $barang_kd,
							'item_code' => $this->input->post('txtProductCodeChild'),
							'item_barcode' => $item_barcode,
							'item_status' => $item_status,
							'item_desc' => $item_desc,
							'item_dimension' => $item_dimension,
							'netweight' => $this->input->post('txtNetweight'),
							'grossweight' => $this->input->post('txtGrossweight'),
							'boxweight' => $this->input->post('txtBoxweight'),
							'length_cm' => $this->input->post('txtLength'),
							'width_cm' => $this->input->post('txtWidth'),
							'height_cm' => $this->input->post('txtHeight'),
							'item_cbm' => $this->input->post('txtItemCbm'),
							'item_qty' => $item_qty,
							'harga_barang' => $harga_barang,
							'harga_retail' => $harga_retail,
							'disc_type' => $disc_type,
							'item_disc' => $item_disc,
							'total_harga' => $total_harga,
							'item_note' => $item_note,
							'tgl_input' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin'),
						);
						$aksi['item'] = $this->db->insert('td_quotation_item_detail', $data);

						$kd_item_disc = $this->create_code('PQO', 'kd_item_disc', 'td_quotation_item_disc');
						for ($i = 0; $i < count($nm_pihak); $i++) :
							$kd_item_disc = $i == 0?$kd_item_disc:$this->create_inc_code($kd_item_disc, 'PQO');
							$data_disc[] = array(
								'kd_item_disc' => $kd_item_disc,
								'quo_item_kd' => $kd_citem_quotation,
								'mquotation_kd' => $mquotation_kd,
								'item_relation' => $relation_status,
								'nm_pihak' => $nm_pihak[$i],
								'jml_disc' => $jml_disc[$i],
								'type_individual_disc' => $type_individual_disc[$i],
								'jml_individual_disc' => $jml_individual_disc[$i],
								'view_access' => $view_access[$i],
								'tgl_input' => date('Y-m-d H:i:s')
							);
						endfor;
						if (isset($data_disc)) :
							$aksi['disc_default'] = $this->db->insert_batch('td_quotation_item_disc', $data_disc);
						endif;
						$str['relation'] = 'child';
					endif;
					$label_err = 'menambahkan';
				elseif ($act == 'edit') :
					if ($relation_status == 'parent') :
	                    $data = array(
							'barang_kd' => $barang_kd,
							'item_code' => $this->input->post('txtProductCode'),
							'item_barcode' => $item_barcode,
							'item_status' => $item_status,
							'item_desc' => $item_desc,
							'item_dimension' => $item_dimension,
							'netweight' => $this->input->post('txtNetweight'),
							'grossweight' => $this->input->post('txtGrossweight'),
							'boxweight' => $this->input->post('txtBoxweight'),
							'length_cm' => $this->input->post('txtLength'),
							'width_cm' => $this->input->post('txtWidth'),
							'height_cm' => $this->input->post('txtHeight'),
							'item_cbm' => $this->input->post('txtItemCbm'),
							'item_qty' => $item_qty,
							'harga_barang' => $harga_barang,
							'harga_retail' => $harga_retail,
							'disc_type' => $disc_type,
							'item_disc' => $item_disc,
							'total_harga' => $total_harga,
							'item_note' => $item_note,
							'tgl_edit' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin'),
						);
						$where = array($this->p_key => $kd_ditem_quotation, 'mquotation_kd' => $mquotation_kd);
						$aksi['item'] = $this->db->update($this->tbl_name, $data, $where);

						$this->db->delete('td_quotation_item_disc', array('quo_item_kd' => $kd_ditem_quotation, 'mquotation_kd' => $mquotation_kd));
						$kd_item_disc = $this->create_code('PQO', 'kd_item_disc', 'td_quotation_item_disc');
						for ($i = 0; $i < count($nm_pihak); $i++) :
							$kd_item_disc = $i == 0?$kd_item_disc:$this->create_inc_code($kd_item_disc, 'PQO');
							$data_disc[] = array(
								'kd_item_disc' => $kd_item_disc,
								'quo_item_kd' => $kd_ditem_quotation,
								'mquotation_kd' => $mquotation_kd,
								'item_relation' => $relation_status,
								'nm_pihak' => $nm_pihak[$i],
								'jml_disc' => $jml_disc[$i],
								'type_individual_disc' => $type_individual_disc[$i],
								'jml_individual_disc' => $jml_individual_disc[$i],
								'view_access' => $view_access[$i],
								'tgl_input' => date('Y-m-d H:i:s')
							);
						endfor;
						if (isset($data_disc)) :
							$aksi['disc_default'] = $this->db->insert_batch('td_quotation_item_disc', $data_disc);
						endif;
						$str['relation'] = 'parent';
					elseif ($relation_status == 'child') :
						$data = array(
							'kd_child' => $barang_kd,
							'item_code' => $this->input->post('txtProductCodeChild'),
							'item_barcode' => $item_barcode,
							'item_status' => $item_status,
							'item_desc' => $item_desc,
							'item_dimension' => $item_dimension,
							'netweight' => $this->input->post('txtNetweight'),
							'grossweight' => $this->input->post('txtGrossweight'),
							'boxweight' => $this->input->post('txtBoxweight'),
							'length_cm' => $this->input->post('txtLength'),
							'width_cm' => $this->input->post('txtWidth'),
							'height_cm' => $this->input->post('txtHeight'),
							'item_cbm' => $this->input->post('txtItemCbm'),
							'item_qty' => $item_qty,
							'harga_barang' => $harga_barang,
							'harga_retail' => $harga_retail,
							'disc_type' => $disc_type,
							'item_disc' => $item_disc,
							'total_harga' => $total_harga,
							'item_note' => $item_note,
							'tgl_edit' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin'),
						);
						$where = array('kd_citem_quotation' => $kd_ditem_quotation, 'ditem_quotation_kd' => $kd_parent_quo, 'kd_parent' => $kd_parent_item, 'mquotation_kd' => $mquotation_kd);
						$aksi['item'] = $this->db->update('td_quotation_item_detail', $data, $where);

						$this->db->delete('td_quotation_item_disc', array('quo_item_kd' => $kd_ditem_quotation, 'mquotation_kd' => $mquotation_kd));
						$kd_item_disc = $this->create_code('PQO', 'kd_item_disc', 'td_quotation_item_disc');
						for ($i = 0; $i < count($nm_pihak); $i++) :
							$kd_item_disc = $i == 0?$kd_item_disc:$this->create_inc_code($kd_item_disc, 'PQO');
							$data_disc[] = array(
								'kd_item_disc' => $kd_item_disc,
								'quo_item_kd' => $kd_ditem_quotation,
								'mquotation_kd' => $mquotation_kd,
								'item_relation' => $relation_status,
								'nm_pihak' => $nm_pihak[$i],
								'jml_disc' => $jml_disc[$i],
								'type_individual_disc' => $type_individual_disc[$i],
								'jml_individual_disc' => $jml_individual_disc[$i],
								'view_access' => $view_access[$i],
								'tgl_input' => date('Y-m-d H:i:s')
							);
						endfor;
						if (isset($data_disc)) :
							$aksi['disc_default'] = $this->db->insert_batch('td_quotation_item_disc', $data_disc);
						endif;
						$str['relation'] = 'child';
					endif;
                    $label_err = 'mengubah';
				endif;


				if($this->db->trans_status() === FALSE):
					$this->db->trans_rollback();
					if ($relation_status == 'parent') :
                    	$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' '.$this->title.' dengan kd_ditem_quotation \''.$kd_ditem_quotation.'\' mquotation_kd \''.$mquotation_kd.'\' item_type \''.$item_type.'\' barang_kd \''.$barang_kd.'\' item_status \''.$item_status.'\' item_desc \''.$item_desc.'\' item_dimension \''.$item_dimension.'\' item_qty \''.$item_qty.'\' harga_barang \''.$harga_barang.'\' disc_type \''.$disc_type.'\' item_disc \''.$item_disc.'\' total_harga \''.$total_harga.'\' relation_status \''.$relation_status.'\' item_note \''.$item_note.'\'');
                    elseif ($relation_status == 'child') :
						$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' pada td_quotation_item_detail dengan kd_citem_quotation \''.$kd_ditem_quotation.'\' ditem_quotation_kd \''.$kd_parent_quo.'\' kd_parent \''.$kd_parent_item.'\' kd_child \''.$barang_kd.'\' item_status \''.$item_status.'\' item_desc \''.$item_desc.'\' item_dimension \''.$item_dimension.'\' item_qty \''.$item_qty.'\' harga_barang \''.$harga_barang.'\' disc_type \''.$disc_type.'\' item_disc \''.$item_disc.'\' total_harga \''.$total_harga.'\' item_note \''.$item_note.'\'');
                    endif;
                    $str['confirm'] = 'errValidation';
                    $str['idErrForm'] = buildAlert('danger', 'Gagal!', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
				else:
					$this->db->trans_commit();
					if ($relation_status == 'parent') :
                    	$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' dengan kd_ditem_quotation \''.$kd_ditem_quotation.'\' mquotation_kd \''.$mquotation_kd.'\' item_type \''.$item_type.'\' barang_kd \''.$barang_kd.'\' item_status \''.$item_status.'\' item_desc \''.$item_desc.'\' item_dimension \''.$item_dimension.'\' item_qty \''.$item_qty.'\' harga_barang \''.$harga_barang.'\' disc_type \''.$disc_type.'\' item_disc \''.$item_disc.'\' total_harga \''.$total_harga.'\' relation_status \''.$relation_status.'\' item_note \''.$item_note.'\'');
                    elseif ($relation_status == 'child') :
						$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' pada td_quotation_item_detail dengan kd_citem_quotation \''.$kd_ditem_quotation.'\' ditem_quotation_kd \''.$kd_parent_quo.'\' kd_parent \''.$kd_parent_item.'\' kd_child \''.$barang_kd.'\' item_status \''.$item_status.'\' item_desc \''.$item_desc.'\' item_dimension \''.$item_dimension.'\' item_qty \''.$item_qty.'\' harga_barang \''.$harga_barang.'\' disc_type \''.$disc_type.'\' item_disc \''.$item_disc.'\' total_harga \''.$total_harga.'\' item_note \''.$item_note.'\'');
                    endif;
                    $str['confirm'] = 'success';
                    $str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
				endif;

                // if ($aksi) :
                // 	if ($relation_status == 'parent') :
                //     	$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' dengan kd_ditem_quotation \''.$kd_ditem_quotation.'\' mquotation_kd \''.$mquotation_kd.'\' item_type \''.$item_type.'\' barang_kd \''.$barang_kd.'\' item_status \''.$item_status.'\' item_desc \''.$item_desc.'\' item_dimension \''.$item_dimension.'\' item_qty \''.$item_qty.'\' harga_barang \''.$harga_barang.'\' disc_type \''.$disc_type.'\' item_disc \''.$item_disc.'\' total_harga \''.$total_harga.'\' relation_status \''.$relation_status.'\' item_note \''.$item_note.'\'');
                //     elseif ($relation_status == 'child') :
				// 		$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' pada td_quotation_item_detail dengan kd_citem_quotation \''.$kd_ditem_quotation.'\' ditem_quotation_kd \''.$kd_parent_quo.'\' kd_parent \''.$kd_parent_item.'\' kd_child \''.$barang_kd.'\' item_status \''.$item_status.'\' item_desc \''.$item_desc.'\' item_dimension \''.$item_dimension.'\' item_qty \''.$item_qty.'\' harga_barang \''.$harga_barang.'\' disc_type \''.$disc_type.'\' item_disc \''.$item_disc.'\' total_harga \''.$total_harga.'\' item_note \''.$item_note.'\'');
                //     endif;
                //     $str['confirm'] = 'success';
                //     $str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
                // else :
                // 	if ($relation_status == 'parent') :
                //     	$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' '.$this->title.' dengan kd_ditem_quotation \''.$kd_ditem_quotation.'\' mquotation_kd \''.$mquotation_kd.'\' item_type \''.$item_type.'\' barang_kd \''.$barang_kd.'\' item_status \''.$item_status.'\' item_desc \''.$item_desc.'\' item_dimension \''.$item_dimension.'\' item_qty \''.$item_qty.'\' harga_barang \''.$harga_barang.'\' disc_type \''.$disc_type.'\' item_disc \''.$item_disc.'\' total_harga \''.$total_harga.'\' relation_status \''.$relation_status.'\' item_note \''.$item_note.'\'');
                //     elseif ($relation_status == 'child') :
				// 		$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' pada td_quotation_item_detail dengan kd_citem_quotation \''.$kd_ditem_quotation.'\' ditem_quotation_kd \''.$kd_parent_quo.'\' kd_parent \''.$kd_parent_item.'\' kd_child \''.$barang_kd.'\' item_status \''.$item_status.'\' item_desc \''.$item_desc.'\' item_dimension \''.$item_dimension.'\' item_qty \''.$item_qty.'\' harga_barang \''.$harga_barang.'\' disc_type \''.$disc_type.'\' item_disc \''.$item_disc.'\' total_harga \''.$total_harga.'\' item_note \''.$item_note.'\'');
                //     endif;
                //     $str['confirm'] = 'errValidation';
                //     $str['idErrForm'] = buildAlert('danger', 'Gagal!', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
                // endif;
			endif;

            header('Content-Type: application/json');
            echo json_encode($str);
		endif;
	}

	private function create_code($unique, $p_key, $key_tbl) {
		$this->db->select($p_key.' AS code')
			->from($key_tbl)
			->where(array('DATE(tgl_input)' => date('Y-m-d')))
			->order_by($p_key.' DESC, tgl_input DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -6);
		endif;
		$angka = $urutan + 1;
		$primary = $unique.date('dmy').str_pad($angka, 6, '000', STR_PAD_LEFT);
		return $primary;
	}

	private function create_inc_code($last_code = '', $unique = '') {
		if (!empty($last_code)) :
			$urutan = substr($last_code, -6);
		else :
			$urutan = 0;
		endif;
		$angka = $urutan + 1;
		$primary = $unique.date('dmy').str_pad($angka, 6, '000', STR_PAD_LEFT);
		return $primary;
	}

	function hapus_data() {
		if ($this->input->is_ajax_request()) :
			$this->db->trans_begin();
			$p_code = $this->input->get('id');
			$this->db->select('mquotation_kd')->from('td_quotation_item')->where(array('kd_ditem_quotation' => $p_code));
			$q_kdmaster = $this->db->get();
			$row = $q_kdmaster->row();
			$kd_mquotation = $row->mquotation_kd;
			$label_err = 'menghapus';
			$aksi['master'] = $this->db->delete($this->tbl_name, array($this->p_key => $p_code));
			$aksi['detail'] = $this->db->delete('td_quotation_item_detail', array('ditem_quotation_kd' => $p_code));
			$aksi['default_disc'] = $this->db->delete('td_quotation_item_disc', array('mquotation_kd' => $kd_mquotation, 'quo_item_kd' => $p_code));

			if($this->db->trans_status() === FALSE):
				$this->db->trans_rollback();
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' '.$this->title.' dengan kd_ditem_quotation \''.$p_code.'\'');
				$str['alert'] = buildAlert('danger', 'Gagal!', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
			else:
				$this->db->trans_commit();
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' dengan kd_ditem_quotation \''.$p_code.'\'');
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function hapus_data_child() {
		if ($this->input->is_ajax_request()) :
			$p_code = $this->input->get('id');
			$this->db->select('mquotation_kd')->from('td_quotation_item_detail')->where(array('kd_citem_quotation' => $p_code));
			$q_kdmaster = $this->db->get();
			$row = $q_kdmaster->row();
			$kd_mquotation = $row->mquotation_kd;
			$label_err = 'menghapus';
			$aksi['detail'] = $this->db->delete('td_quotation_item_detail', array('kd_citem_quotation' => $p_code));
			$aksi['default_disc'] = $this->db->delete('td_quotation_item_disc', array('mquotation_kd' => $kd_mquotation, 'quo_item_kd' => $p_code));

			if($this->db->trans_status() === FALSE):
				$this->db->trans_rollback();
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' '.$this->title.' pada td_quotation_item_detail dengan kd_citem_quotation \''.$p_code.'\'');
				$str['alert'] = buildAlert('danger', 'Gagal!', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
			else:
				$this->db->trans_commit();
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' pada td_quotation_item_detail dengan kd_citem_quotation \''.$p_code.'\'');
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
			endif;
						
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function price_category($cat) {
		$price = '';
		if ($cat == 'harga_retail') :
			$price = 'harga_lokal_retail';
		elseif ($cat == 'harga_distributor') :
			$price = 'harga_lokal_distributor';
		elseif ($cat == 'harga_reseller') :
			$price = 'harga_lokal_reseller';
		elseif ($cat == 'harga_ekspor') :
			$price = 'harga_ekspor';
		endif;
		return $price;
	}

	function get_child() {
		$kd_parent = $this->input->get('kd_parent');
		$tipe_harga = $this->price_category($this->input->get('tipe_harga'));
		$kd_child = $this->input->get('kd_child');

		$this->db->select('a.item_code, a.kd_barang, a.deskripsi_barang, a.dimensi_barang, b.'.$tipe_harga.', b.harga_lokal_retail');
		$this->db->from('tm_barang a');
		$this->db->join('td_barang_harga b', 'b.kd_harga_barang = a.harga_barang_kd', 'left');
		$this->db->join('td_barang_relasi c', 'c.barang_kd = a.kd_barang', 'left');
		$this->db->join('tm_barang_relasi d', 'd.kd_mrelasi_barang = c.mrelasi_barang_kd', 'left');
		$this->db->where(array('d.barang_kd' => $kd_parent));
		$this->db->group_by('a.kd_barang, a.item_code');
		$this->db->order_by('a.item_code ASC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$result = $query->result();

		echo '<option value="">-- Pilih Child --</option>';
		foreach ($result as $data) :
			$sel = $data->kd_barang == $kd_child?'selected':'';
			echo '<option value="'.$data->kd_barang.'" '.$sel.'>'.$data->item_code.'</option>';
		endforeach;
	}

	function item_detail() {
		$kd_mquotation = $this->input->get('kd_mquotation');
		$items_data = $this->m_quotation->quotatationItems($kd_mquotation);
		if (!empty($items_data)) :
			foreach ($items_data as $item_data) :
				$kd_ditem_quotation[] = $item_data->kd_ditem_quotation;
			endforeach;
			$details_data = $this->m_quotation->item_child($kd_ditem_quotation);
			$data['master'] = $this->get_master($kd_mquotation);
			$data['items_data'] = $items_data;
			$data['details_data'] = $details_data;
			$data['nm_kolom_ppn'] = $this->input->get('nm_kolom_ppn');
			$data['get_ppn'] = $this->input->get('ppn');
			$data['ongkir'] = $this->m_master_quotation->detail_price('ongkir', $kd_mquotation);
			$data['install'] = $this->m_master_quotation->detail_price('install', $kd_mquotation);
			$data['total_price_data'] = $this->table_total_price($kd_mquotation);
			$data['tot_potongan'] = $this->m_master_quotation->total_potongan($kd_mquotation);
			$data['kd_mquotation'] = $kd_mquotation;
			$data['csrf'] = $this->security->get_csrf_hash();
			$this->load->view('page/'.$this->class_link.'/table', $data);
			$this->load->view('page/'.$this->class_link.'/form_total', $data);
		endif;
	}

	function table_total_price($kd_mquotation) {
		$price_data = $this->m_quotation->get_price($kd_mquotation);
		return $price_data;
	}

	function get_ppn() {
		$var = 'set_ppn';
		$result = $this->m_app_setting->get_data($var);
		$value = $result->nilai_setting;

		return $result?$value:FALSE;
	}

	function sel_child() {
		$kd_child = $this->input->get('kd_child');
		$tipe_harga = $this->price_category($this->input->get('tipe_harga'));

		$this->db->select('a.item_code, a.kd_barang, a.deskripsi_barang, a.dimensi_barang, a.item_barcode, a.netweight, a.grossweight, a.boxweight, a.length_cm, a.width_cm, a.height_cm, a.item_cbm, b.'.$tipe_harga.' AS tipe_harga, b.harga_lokal_retail AS harga_retail');
		$this->db->from('tm_barang a');
		$this->db->join('td_barang_harga b', 'b.kd_harga_barang = a.harga_barang_kd', 'left');
		$this->db->where(array('a.kd_barang' => $kd_child));
		$this->db->group_by('a.item_code');
		$this->db->order_by('a.item_code ASC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$result = $query->row();

		$data['kd_barang'] = $result->kd_barang;
		$data['item_code'] = $result->item_code;
		$data['item_barcode'] = $result->item_barcode;
		$data['deskripsi_barang'] = $result->deskripsi_barang;
		$data['dimensi_barang'] = $result->dimensi_barang;
		$data['netweight'] = $result->netweight;
		$data['grossweight'] = $result->grossweight;
		$data['boxweight'] = $result->boxweight;
		$data['length_cm'] = $result->length_cm;
		$data['width_cm'] = $result->width_cm;
		$data['height_cm'] = $result->height_cm;
		$data['item_cbm'] = $result->item_cbm;
		$data['harga'] = $result->tipe_harga;
		$data['harga_retail'] = $result->harga_retail;
		$data['kd_custom'] = 'PRD020817000573';

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	function get_item_detail($type) {
		$kd_item = $this->input->get('kd_item');

		if ($type == 'parent') :
			$this->db->select('a.kd_ditem_quotation, a.barang_kd, a.item_status, a.item_desc, a.item_code, a.item_barcode, a.item_dimension, a.item_qty, a.harga_barang, a.harga_retail, a.disc_type, a.item_disc, a.total_harga, a.item_note, a.netweight, a.grossweight, a.boxweight, a.length_cm, a.width_cm, a.height_cm, a.item_cbm, b.item_code AS code_ori, b.item_barcode AS barcode_ori, b.deskripsi_barang AS desc_ori, b.dimensi_barang AS dimensi_ori, b.netweight AS netweight_ori, b.grossweight AS grossweight_ori, b.boxweight AS boxweight_ori, b.length_cm AS length_cm_ori, b.width_cm AS width_cm_ori, b.height_cm AS height_cm_ori, b.item_cbm AS item_cbm_ori');
			$this->db->from($this->tbl_name.' a');
			$this->db->join('tm_barang b', 'b.kd_barang = a.barang_kd', 'left');
			$this->db->where(array('a.kd_ditem_quotation' => $kd_item));
		elseif ($type == 'child') :
			$this->db->select('a.kd_citem_quotation, a.ditem_quotation_kd, a.kd_parent, a.kd_child, a.item_status, a.item_desc, a.item_code, a.item_barcode, a.item_dimension, a.item_qty, a.harga_barang, a.harga_retail, a.disc_type, a.item_disc, a.total_harga, a.item_note, a.netweight, a.grossweight, a.boxweight, a.length_cm, a.width_cm, a.height_cm, a.item_cbm, b.item_code AS parent_code, b.deskripsi_barang AS parent_desc, c.item_code AS code_ori, c.item_barcode AS barcode_ori, c.deskripsi_barang AS desc_ori, c.dimensi_barang AS dimensi_ori, c.netweight AS netweight_ori, c.grossweight AS grossweight_ori, c.boxweight AS boxweight_ori, c.length_cm AS length_cm_ori, c.width_cm AS width_cm_ori, c.height_cm AS height_cm_ori, c.item_cbm AS item_cbm_ori');
			$this->db->from('td_quotation_item_detail a');
			$this->db->join('tm_barang b', 'b.kd_barang = a.kd_parent', 'left');
			$this->db->join('tm_barang c', 'c.kd_barang = a.kd_child', 'left');
			$this->db->where(array('a.kd_citem_quotation' => $kd_item));
		endif;
		$query = $this->db->get();
		$result = $query->row();

		if ($type == 'parent') :
			$data['kd_ditem_quotation'] = $result->kd_ditem_quotation;
			$data['barang_kd'] = $result->barang_kd;
			$data['item_code'] = $result->item_code;
			$data['code_ori'] = $result->code_ori;
			$data['item_barcode'] = $result->item_barcode;
			$data['barcode_ori'] = $result->barcode_ori;
		elseif ($type == 'child') :
			$data['kd_citem_quotation'] = $result->kd_citem_quotation;
			$data['ditem_quotation_kd'] = $result->ditem_quotation_kd;
			$data['kd_parent'] = $result->kd_parent;
			$data['parent_code'] = $result->parent_code.'/'.$result->parent_desc;
			$data['kd_child'] = $result->kd_child;
			$data['item_code'] = $result->item_code;
			$data['code_ori'] = $result->code_ori;
			$data['item_barcode'] = $result->item_barcode;
			$data['barcode_ori'] = $result->barcode_ori;
		endif;
		$data['item_status'] = $result->item_status;
		$data['item_desc'] = $result->item_desc;
		$data['item_desc_ori'] = $result->desc_ori;
		$data['item_dimension'] = $result->item_dimension;
		$data['item_dimension_ori'] = $result->dimensi_ori;
		$data['netweight'] = $result->netweight;
		$data['netweight_ori'] = $result->netweight_ori;
		$data['grossweight'] = $result->grossweight;
		$data['grossweight_ori'] = $result->grossweight_ori;
		$data['boxweight'] = $result->boxweight;
		$data['boxweight_ori'] = $result->boxweight_ori;
		$data['length_cm'] = $result->length_cm;
		$data['length_cm_ori'] = $result->length_cm_ori;
		$data['width_cm'] = $result->width_cm;
		$data['width_cm_ori'] = $result->width_cm_ori;
		$data['height_cm'] = $result->height_cm;
		$data['height_cm_ori'] = $result->height_cm_ori;
		$data['item_cbm'] = $result->item_cbm;
		$data['item_cbm_ori'] = $result->item_cbm_ori;
		$data['item_qty'] = $result->item_qty;
		$data['harga_barang'] = $result->harga_barang;
		$data['harga_retail'] = $result->harga_retail;
		$data['disc_type'] = $result->disc_type;
		$data['item_disc'] = $result->item_disc;
		$data['total_harga'] = $result->total_harga;
		$data['item_note'] = $result->item_note;

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	function submit_detail() {
		if ($this->input->is_ajax_request()) :
			$act = $this->input->get('act');
			$kd_master = $this->input->get('kd_master');
			$value = $this->input->get('value');
			$fix_value = $value;
			$ppn = $this->input->get('ppn');
			if ($this->m_master_quotation->check_val($act, $value, $kd_master)) :
				$fix_value = $this->before_ppn($value, $ppn);
			endif;
			if ($act == 'submit_ongkir') :
				$ongkir = $this->m_master_quotation->update_price('ongkir', $kd_master, $fix_value);
				$install = $this->m_master_quotation->detail_price('install', $kd_master);
			elseif ($act == 'submit_install') :
				$install = $this->m_master_quotation->update_price('install', $kd_master, $fix_value);
				$ongkir = $this->m_master_quotation->detail_price('ongkir', $kd_master);
			endif;
			$data['kd_master'] = $kd_master;

			header('Content-Type: application/json');
			echo json_encode($data);
		endif;
	}

	function before_ppn($value, $ppn) {
		$iki = $value / (($ppn + 100) / 100);
		return $iki;
	}

	private function create_pkey($unique = '', $column = '', $table = '') {
		$this->db->select($column.' AS code')
			->from($table)
			->where(array('DATE(tgl_input)' => date('Y-m-d')))
			->order_by('tgl_input DESC, '.$column.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -6);
		endif;
		$angka = $urutan + 1;
		$primary = $unique.date('dmy').str_pad($angka, 6, '000', STR_PAD_LEFT);
		return $primary;
	}

	function submit_disc() {
		if ($this->input->is_ajax_request()) :
			$this->load->library(array('form_validation'));
			$str['csrf'] = $this->security->get_csrf_hash();

			$mquotation_kd = $this->input->post('txtKdMQuotationPrice');
			$nm_kolom = $this->input->post('txtNmKolomPrice');
			$type_kolom = $this->input->post('selKolomType');
			$total_nilai = $this->input->post('txtTotalJumlah');

			$this->form_validation->set_rules('txtNmKolomPrice', 'Nama Discount', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtTotalJumlah', 'Total Jumlah', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'errValidation';
				$str['idErrNmKolom'] = (!empty(form_error('txtNmKolomPrice')))?buildLabel('warning', form_error('txtNmKolomPrice', '"', '"')):'';
				$str['idErrTotalJumlah'] = (!empty(form_error('txtTotalJumlah')))?buildLabel('warning', form_error('txtTotalJumlah', '"', '"')):'';
			else :
				$kd_dharga_quotation = $this->create_pkey('DQH', 'kd_dharga_quotation', 'td_quotation_harga');
				$data = array(
					'kd_dharga_quotation' => $kd_dharga_quotation,
					'mquotation_kd' => $mquotation_kd,
					'nm_kolom' => $nm_kolom,
					'type_kolom' => $type_kolom,
					'total_nilai' => $total_nilai,
					'tgl_input' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				);
				$aksi = $this->db->insert('td_quotation_harga', $data);
				$label_err = 'menambahkan';

				if ($aksi) :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' discount special dengan kd_dharga_quotation \''.$kd_dharga_quotation.'\' mquotation_kd \''.$mquotation_kd.'\' nm_kolom \''.$nm_kolom.'\' type_kolom \''.$type_kolom.'\' total_nilai \''.$total_nilai.'\'');
					$str['confirm'] = 'success';
					$str['kd_master'] = $mquotation_kd;
					$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
				else :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' discount special dengan kd_dharga_quotation \''.$kd_dharga_quotation.'\' mquotation_kd \''.$mquotation_kd.'\' nm_kolom \''.$nm_kolom.'\' type_kolom \''.$type_kolom.'\' total_nilai \''.$total_nilai.'\'');
					$str['confirm'] = 'errValidation';
					$str['idErrForm'] = buildAlert('danger', 'Gagal!', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
				endif;
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function hapus_special_disc() {
		if ($this->input->is_ajax_request()) :
			$id = $this->input->get('id');
            $label_err = 'menghapus';
            $this->db->select('mquotation_kd');
            $this->db->from('td_quotation_harga');
            $this->db->where(array('kd_dharga_quotation' => $id));
            $query = $this->db->get();
            $row = $query->row();
            $str['kd_master'] = $row->mquotation_kd;
            $aksi = $this->db->delete('td_quotation_harga', array('kd_dharga_quotation' => $id));

            if ($aksi) :
                $input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' special discount dengan kd_dharga_quotation \''.$id.'\'');
                $str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' special discount!');
            else :
                $input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' special discount dengan kd_dharga_quotation \''.$id.'\'');
                $str['alert'] = buildAlert('danger', 'Gagal '.$label_err.' special discount, Kesalahan sistem!');
            endif;

            header('Content-Type: application/json');
            echo json_encode($str);
		endif;
	}

	function insert_ppn() {
		$where['kd_mquotation'] = $this->input->get('kd_master');
		$data['nm_kolom_ppn'] = $this->input->get('nm_kolom_ppn');
		$data['jml_ppn'] = $this->get_ppn();
		$update = $this->m_master_quotation->insert_ppn($data, $where);
	}
}