<?php
defined('BASEPATH') or exit('No direct script access allowed!');

// Modul container yang berguna untuk tabel pembantu ketika input container

class Set_container extends MY_Controller {
	private $class_link = 'sales/set_container';
	private $form_errs = array('idErrNama', 'idErrDeskripsi', 'idErr20Ft');

	public function __construct() {
		parent::__construct();

		$this->load->library(array('ssp', 'form_validation'));
		$this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
		$this->load->model(array('m_builder', 'tb_container'));
	}

	public function index() {
		parent::administrator();
		parent::select2_assets();
		$this->get_table();
	}

	public function get_table() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function open_table() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function get_data_byid()
	{
		$id = $this->input->get('id', true);
		$data['container'] = $this->tb_container->get_row($id);
		echo json_encode($data);
	}

	public function table_data() {
		$data = $this->tb_container->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function get_form() {
		$id = $this->input->get('id');
		$data = $this->tb_container->get_row($id);
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = $this->form_errs;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function open_form() {
		$id = $this->input->get('id');
		$data['class_link'] = $this->class_link;
		$data = $this->tb_container->get_row($id);
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function send_data() {
		if ($this->input->is_ajax_request()) :
			$id = $this->input->post('txtId');
			$opt = !empty($id) ? "edit" : "new";
			$this->form_validation->set_rules($this->tb_container->form_rules($opt));
			if ($this->form_validation->run() == FALSE) :
				$str = $this->tb_container->build_warning($this->form_errs);
				$str['confirm'] = 'error';
			else :
				if($opt == "new"):
					$data['nm_container'] =  $this->input->post('txtNama');
				else:
					$data['id'] =  $id;
					$data['nm_container'] =  $this->input->post('txtNama');
				endif;
				$data['deskripsi'] = $this->input->post('txtDeskripsi');
				$data['konversi_20ft'] = $this->input->post('txt20Ft');
				$str = $this->tb_container->submit_data($data, $opt);
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function delete_data() {
		if ($this->input->is_ajax_request()) :
			$id = $this->input->get('id');
			$str = $this->tb_container->delete_data($id);
			
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}