<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Sales_order_view extends MY_Controller {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'tm_salesorder';
	private $p_key = 'kd_msalesorder';
	private $class_link = 'sales/sales_orders';
	private $title = 'Sales Orders';

	function __construct() {
		parent::__construct();

		$this->load->helper(array('my_helper'));
		$this->load->library(array('form_validation'));
		$this->load->model(array('m_builder', 'model_salesorder', 'm_salesorder', 'm_format_laporan', 'tb_tipe_harga', 'tm_currency', 'td_currency_convert', 'tm_salesorder', 'm_master_quotation', 'm_app_setting', 'tb_default_disc', 'td_so_termin'));
	}

	function detail() {
		$this->load->model(array('td_salesorder_item_disc'));
		$kd_msales = $this->input->get('kd_salesorder');
		$data['view_type'] = $this->input->get('view_type');
		$data['sales_tipe'] = $this->input->get('sales_tipe');
		
		$data['invoice_datas'] = $this->model_salesorder->read_no_dps($kd_msales);
		$data['master_head'] = $this->model_salesorder->read_masterdata($kd_msales);
		$data['item_detail'] = $this->tabel_data($kd_msales);
		$data['item_detail']['page_type'] = 'detail';
		$data['pihak_default_disc'] = $this->td_salesorder_item_disc->select_distinct($kd_msales, $data['master_head']['kd_jenis_customer']);
		$data['class_link'] = $this->class_link;
		$data['kd_msalesorder'] = $kd_msales;
		$data['disc_view'] = FALSE;
		$this->load->view('page/'.$this->class_link.'/view_detail', $data);
	}

	function tabel_data($kd_msalesorder) {
		$this->load->model(array('td_salesorder_item_disc'));
		$items_data = $this->m_salesorder->salesOrderItems($kd_msalesorder);
		if (!empty($items_data)) :
			foreach ($items_data as $item_data) :
				$kd_ditem_so[] = $item_data->kd_ditem_so;
			endforeach;
			$details_data = $this->m_salesorder->item_child($kd_ditem_so);
			$data['items_data'] = $items_data;
			$data['details_data'] = $details_data;
			$data['tot_potongan'] = $this->m_salesorder->get_price($kd_msalesorder);
			$data['data_discs'] = $this->td_salesorder_item_disc->get_all_where(array('mso_kd' => $kd_msalesorder));
			return $data;
		endif;
	}

	function cetak($type_disc = '', $kd_msales = '') {
		parent::admin_print();

		$m_detail = $this->model_salesorder->read_masterdata($kd_msales);
		$tipe_view = $this->tipe_view($m_detail['tipe_customer']);
		$data['format_laporan'] = $this->m_format_laporan->view_laporan($tipe_view, $m_detail['term_payment_format']);
		$data['master_head'] = $m_detail;
		$data['item_detail'] = $this->tabel_data($kd_msales);
		$data['item_detail']['page_type'] = 'print';
		$data['item_detail']['default_disc'] = $this->td_salesorder_item_disc->get_all_where(array('mso_kd' => $kd_msales, 'nm_pihak' => $type_disc));
		$data['class_link'] = $this->class_link;
		$data['kd_msalesorder'] = $kd_msales;
		$data['disc_view'] = TRUE;

		$this->load->view('page/'.$this->class_link.'/part/view_print', $data);
	}

	public function tipe_view($tipe_customer = '') {
		if ($tipe_customer == 'Ekspor') :
			$code = 'salesorderekspor';
		elseif ($tipe_customer == 'Lokal' || empty($tipe_customer)) :
			$code = 'salesorderlocal';
			$tipe_customer = 'Lokal';
		endif;
		return $code;
	}

	public function form_dp() {
		$data['master_data'] = $this->model_salesorder->read_masterdata($this->input->get('kd_msalesorder'));
		$data['no_dps'] = $this->model_salesorder->read_no_dps($this->input->get('kd_msalesorder'));
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_dp', $data);
	}

	public function form_dp_termin() {
		$data['master_data'] = $this->model_salesorder->read_masterdata($this->input->get('kd_msalesorder'));
		$data['no_dps'] = $this->model_salesorder->read_no_dps($this->input->get('kd_msalesorder'));
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_dp_termin', $data);
	}

	public function list_dp() {
		$data['master_data'] = $this->model_salesorder->read_masterdata($this->input->get('kd_msalesorder'));
		$data['no_dps'] = $this->model_salesorder->read_no_dps($this->input->get('kd_msalesorder'));
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/list_dp', $data);
	}

	public function get_additional_dp($no_invoice = '', $jml_dp = '') {
		$form = '<div class="form_additional_dp" style="display: none;">';
			if (!empty($no_invoice)) :
				$form .= $this->get_additional_invoice($no_invoice);
			endif;
				/** No invoice dari front end pemisahnya '-' sedangkan masuk ke backend pemisahnya '/' */
				$inv = $this->td_so_termin->get_by_param(['no_invoice' => str_replace('-', '/', $no_invoice)])->row();
			$form .= '<div class="form-group">';
				$form .= '<label for="idtxtTglDp" class="col-md-1 control-label">Tanggal</label>';
				$form .= '<div class="col-md-2">';
					$form .= '<input type="text" name="txtTglAddDp[]" value="'.date('d-m-Y').'" class="form-control datetimepicker">';
				$form .= '</div>';
				$form .= '<label for="idTxtJmlDp" class="col-md-1 control-label">Jumlah DP</label>';
				$form .= '<div class="col-md-3">';
					$form .= form_input(array('name' => 'txtJmlAddDp[]', 'class' => 'form-control', 'value' => $jml_dp, 'placeholder' => 'Jumlah DP'));
				$form .= '</div>';
				$form .= '<div class="col-md-2">';
					$form .= form_button(array('type' => 'button', 'id' => 'idBtnAddDp', 'class' => 'btn btn-primary', 'title' => 'Tambah Data DP', 'content' => '<i class="fa fa-plus"></i>'));
					$form .= form_button(array('type' => 'button', 'class' => 'btn btn-danger btn_remove_dp', 'title' => 'Hapus Data DP', 'content' => '<i class="fa fa-trash"></i>'));
				$form .= '</div>';
			$form .= '</div>';
		$form .= '</div>';
		if ($this->input->is_ajax_request()) :
			echo $form;
		else :
			return $form;
		endif;
	}

	public function get_additional_invoice($no_invoice = '') {
		$no_invoice = !empty($no_invoice)?str_replace('-', '/', $no_invoice):$no_invoice;
		if (empty($no_invoice)) :
			$form = form_input(array('type' => 'hidden', 'name' => 'txtAddNoInvoice[]'));
		else :
			$form = '<div class="form-group">';
				$form .= '<label class="col-md-2 control-label">No. Invoice</label>';
				$form .= '<div class="col-md-3">';
					$form .= form_input(array('name' => 'txtAddNoInvoice[]', 'class' => 'form-control', 'value' => $no_invoice, 'placeholder' => 'No. Invoice'));
				$form .= '</div>';
			$form .= '</div>';
		endif;
		return $form;
	}

	public function create_no_invoice() {
		// Dapatkan format no quotation
		$code = 'invoicelocal';
		$tipe_customer = 'Lokal';
		$no_invoice = $this->tm_salesorder->create_noinvoicedp($code, $tipe_customer);

		return $no_invoice;
	}

	public function submit_form() {
		if ($this->input->is_ajax_request()) :
			$this->db->trans_begin();
			$this->form_validation->set_rules('txtJmlDp', 'Jumlah DP', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'error';
				$str['idErrDp'] = (!empty(form_error('txtJmlDp')))?buildLabel('warning', form_error('txtJmlDp', '"', '"')):'';
			else :
				$app_setting = $this->m_app_setting->get_data('set_ppn');
				$jml_ppn = $app_setting->nilai_setting;
				$where['kd_msalesorder'] = $this->input->post('txtKd');
				$data['tgl_dp'] = format_date($this->input->post('txtTglDp'), 'Y-m-d');
				$data['jml_dp'] = value_ppn($this->input->post('txtJmlDp'), $jml_ppn);
				$data['jml_ppn_dp'] = hitung_ppn($this->input->post('txtJmlDp'), $jml_ppn);
				if ($data['jml_dp'] > 0) :
					$data['no_invoice_dp'] = empty($this->input->post('txtNoInvoiceDp'))?$this->create_no_invoice():$this->input->post('txtNoInvoiceDp');
				endif;
				$str = $this->tm_salesorder->insert_dp($data, $where);	
				/** Process Transaction & Send to SAP */
				if($this->db->trans_status() == FALSE):
					$this->db->trans_rollback();
					$str['confirm'] = 'error';
					$str['alert'] = buildAlert('danger', 'Gagal!', 'Gagal menambahkan DP!');
				else:
					$this->db->trans_commit();
					$stat = 'menambahkan DP';
					$str['confirm'] = 'success';
					$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$stat.'!');
				endif;
				/** END Process Transaction */
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
	
	public function submit_form_dp_termin()
	{
		if ($this->input->is_ajax_request()) :
			$app_setting = $this->m_app_setting->get_data('set_ppn');
			$jml_ppn = $app_setting->nilai_setting;
			$where['kd_msalesorder'] = $this->input->post('txtKd');
			$this->db->delete('td_so_termin', array('msalesorder_kd' => $where['kd_msalesorder']));
			$jml_dps = $this->input->post('txtJmlAddDp');
			$no_invoices = $this->input->post('txtAddNoInvoice');
			$tgl_termins = $this->input->post('txtTglAddDp');
			$kd_so_termin = $this->td_so_termin->create_code();
			$act = true;
			if (is_array($jml_dps)) :
				for ($i = 0; $i < count($jml_dps); $i++) :
					$this->db->trans_begin();
					$jml_dp = value_ppn($jml_dps[$i], $jml_ppn);
					$jml_ppn_dp = hitung_ppn($jml_dps[$i], $jml_ppn);
					if (!empty($no_invoices[$i])){
						$no_invoice = $no_invoices[$i];
					}else{
						$no_invoice = $this->create_no_invoice();
					}
					if (!empty($tgl_termins[$i])){
						$tgl_termin = format_date($tgl_termins[$i], 'Y-m-d');
					}else{
						$tgl_termin = date('Y-m-d');
					}
					$submit_termin = array('kd_so_termin' => $kd_so_termin, 'msalesorder_kd' => $where['kd_msalesorder'], 'jml_termin' => $jml_dp, 'jml_termin_ppn' => $jml_ppn_dp, 'no_invoice' => $no_invoice, 'tgl_termin' => $tgl_termin, 'tgl_input' => date('Y-m-d H:i:s'), 'admin_kd' => $this->session->kd_admin);
					$act = $this->db->insert('td_so_termin', $submit_termin);
					if($this->db->trans_status() == FALSE):
						$this->db->trans_rollback();
						$act = false;
						$log_status = "Transaction error";
						break;
					else:
					$this->db->trans_commit();
					$act = true;
					$log_status = "Success";
					endif;
					$kd_so_termin = $this->td_so_termin->create_code($i + 1, $kd_so_termin);
				endfor;
			endif;

			if ($act == false) :
				$message = 'Gagal menambahkan DP termin! '.$log_status;
				$str['confirm'] = 'error';
				$str['alert'] = buildAlert('danger', 'Gagal!', $message);
				$str['message'] = $message;
			else:
				$message = 'Berhasil menambahkan DP termin! '.$log_status;
				$str['confirm'] = 'success';
				$str['alert'] = buildAlert('success', 'Berhasil!', $message);
				$str['message'] = $message;
			endif;

			$str['csrf'] = $this->security->get_csrf_hash();
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function submit_dp_to_sap()
	{
		if ($this->input->is_ajax_request()) :
			$this->db->trans_begin();
			$id = $this->input->post('id');
			$jenis_dp = $this->input->post('jenis_dp');
			$dataAPI = [];
			if($jenis_dp == 'utama'):
				$SO = $this->tm_salesorder->get_row($id);
				$dataAPI = [
					'KdSalesOrder' => $SO->kd_msalesorder,
					'U_IDU_ARDP_INTNUM' => $SO->no_invoice_dp,
					'NumAtCard' => $SO->no_invoice_dp,
					'DocDate' => $SO->tgl_dp,
					'Amount' => $SO->jml_dp + $SO->jml_ppn_dp,
					'U_IDU_WEBUSER' => $this->session->username
				];
				$update = $this->tm_salesorder->update_data(['status_dp' => 'Y'], ['kd_msalesorder' => $id]);
			else:
				$termin = $this->td_so_termin->read_data($id);
				$dataAPI = [
					'KdSalesOrder' => $termin->msalesorder_kd,
					'U_IDU_ARDP_INTNUM' => $termin->no_invoice,
					'NumAtCard' => $termin->no_invoice,
					'DocDate' => $termin->tgl_termin,
					'Amount' => $termin->jml_termin + $termin->jml_termin_ppn,
					'U_IDU_WEBUSER' => $this->session->username
				];
				$update = $this->td_so_termin->update_data(['kd_so_termin' => $id], ['status_dp' => 'Y']);
			endif;

			if($this->db->trans_status() == FALSE):
				$this->db->trans_rollback();
				$str['code'] = 400;
				$str['status'] = "Gagal";
				$str['pesan'] = "AR DP gagal post ke SAP";
			else:
				$api = $this->send_to_sap($dataAPI);
				if($api[0]->ErrorCode == 0):
					$this->db->trans_commit();
					$str['code'] = 200;
					$str['status'] = "Sukses";
					$str['pesan'] = "AR DP berhasil post ke SAP, API ".$api[0]->Message;
				else:
					$this->db->trans_rollback();
					$str['code'] = 400;
					$str['status'] = "Gagal";
					$str['pesan'] = "AR DP gagal post ke SAP, API ".$api[0]->Message;
				endif;
			endif;

			$str['csrf'] = $this->security->get_csrf_hash();
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function cetak_invoice($invoice_stat, $kd_msales) {
		parent::admin_print();

		$m_detail = $this->model_salesorder->read_masterdata($kd_msales);
		$data['format_laporan'] = $this->m_format_laporan->view_laporan('invoice', $m_detail['term_payment_format']);
		$data['master_head'] = $m_detail;
		$data['item_detail'] = $this->tabel_data($kd_msales);
		$data['item_detail']['page_type'] = 'print';
		$data['class_link'] = $this->class_link;
		$data['kd_msalesorder'] = $kd_msales;
		$this->load->view('page/'.$this->class_link.'/part/view_invoice', $data);
	}

	public function hapus_dp() {
		if ($this->input->is_ajax_request()) :
			$kd_msalesorder = $this->input->get('kd_msalesorder');
			$str = $this->tm_salesorder->hapus_dp($kd_msalesorder);

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	/** Send add DP to SAP */
	public function send_to_sap($data)
	{
		$api = parent::api_sap_post('AddARDP', $data);
		return $api;
	}


}