<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Sales_orders_item extends MY_Controller {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'tm_salesorder';
	private $p_key = 'kd_msalesorder';
	private $class_link = 'sales/sales_orders';
	private $title = 'Sales Order Item';

	function __construct() {
		parent::__construct();
		
		$this->load->helper(array('form', 'html', 'my_helper'));
		$this->load->library(array('form_validation'));
		$this->load->model(array('m_builder', 'm_msalesorder', 'm_salesorder', 'm_app_setting', 'tm_finishgood', 'td_currency_convert', 'tb_tipe_harga', 'tm_currency', 'model_salesorder', 'tb_default_disc', 'tm_customer', 'td_customer_alamat', 'tm_salesorder', 'tm_project', 'tm_barang', 'td_barang_harga', 'td_salesorder_item_detail', 'td_salesorder_item', 'tb_relasi_sowo', 'td_workorder_item_so_detail', 'td_workorder_item_so'));
	}

	function index() {
		parent::administrator();

		$this->get_form();
	}

	function define_page($url_array_num) {
		$ttl = $this->uri->segment($url_array_num);
		if (strpos($ttl, '_') !== FALSE) :
			$rpl_title = str_replace('_', ' ', $ttl);
			$title = ucwords($rpl_title);
		else :
			$title = ucwords($this->uri->segment($url_array_num));
		endif;
		return $title;
	}

	function item_form() {
		$kd_msales = $this->input->get('id');
		$order_tipe = $this->input->get('order_tipe');
		$data = $this->get_masterdata($kd_msales);
		$data['discounts'] = $this->tb_default_disc->get_all($data['kd_jenis_customer']);
		// Variabel $data['class_link'] untuk mendapatakkan uri dari controller
		$data['class_link'] = $this->class_link;
		$data['order_tipe'] = $order_tipe;
		$this->load->view('page/'.$this->class_link.'/form_item', $data);
	}

	function get_masterdata($var) {
		$row = $this->model_salesorder->read_masterdata($var);
		return $row;
	}

	function item_detail() {
		$kd_msalesorder = $this->input->get('kd_msalesorder');
		$kd_ditem_so = array();
		$items_data = $this->m_salesorder->salesOrderItems($kd_msalesorder);
		foreach ($items_data as $item_data) :
			$kd_ditem_so[] = $item_data->kd_ditem_so;
		endforeach;
		$details_data = $this->m_salesorder->item_child($kd_ditem_so);
		$data['master'] = $this->get_masterdata($kd_msalesorder);
		$data['items_data'] = $items_data;
		$data['details_data'] = $details_data;
		$data['tot_potongan'] = $this->m_msalesorder->total_potongan($kd_msalesorder);
		$data['csrf'] = $this->security->get_csrf_hash();
		$this->load->view('page/'.$this->class_link.'/table', $data);
		$this->load->view('page/'.$this->class_link.'/form_total', $data);
	}

	function form_input_master_custom()
	{
		/** ID Detail SO */
		$id = $this->input->get('id');
		$relation = $this->input->get('relation');
		
		if($relation == 'parent'):
			// Get data parent dari tm_project
			$data['data_project'] = $this->tm_project->get_by_param(['kd_ditem_so' => $id])->row(); 
			$data['data_detail'] = $this->td_salesorder_item->get_by_param(['kd_ditem_so' => $id])->row();
		else:
			// Get data child dari tm_project
			$data['data_project'] = $this->tm_project->get_by_param(['kd_citem_so' => $id])->row(); 
			$data['data_detail'] = $this->td_salesorder_item_detail->get_by_param(['kd_citem_so' => $id])->row();
		endif;

		/** Group barang */
		$this->db->from('tm_group_barang');
		$q_group = $this->db->get();
		$data['group_barang'] = $q_group->result();

		/** Category product */
		$this->db->from('tm_kat_barang');
		$q_kat = $this->db->get();
		$data['category_barang'] = $q_kat->result();

		/** Item group SAP */
		$this->db->from('tm_item_group');
		$q_item_group = $this->db->get();
		$data['item_group_sap'] = $q_item_group->result();

		$data['id'] = $id;
		$data['relation'] = $relation;

		$this->load->view('page/'.$this->class_link.'/form_item_input_custom', $data);
	}

	function submit_form_input_master_custom()
	{
		if ($this->input->is_ajax_request()):
			$this->form_validation->set_rules('txtCustomBarcode', 'Product Barcode', 'trim|required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtCustomCode', 'Product Code', 'trim|required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtCustomTarif', 'Tarif (HS Code)', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selCustomGroup', 'Product Group', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selCustomCategory', 'Product Category', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selCustomInventItem', 'Inventory Item', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selCustomItemGroup', 'Item Group SAP', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtCustomDesc', 'Product Description', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtCustomProductDimension', 'Product Dimension', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtCustomGrossweight', 'Product Grossweight', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtCustomBoxweight', 'Product Boxweight', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);

			if ($this->form_validation->run() == FALSE) :
				// $str['confirm'] = 'errValidation';
				$resp['idErrCustomBarcode'] = (!empty(form_error('txtCustomBarcode')))?buildLabel('warning', form_error('txtCustpmBarcode', '"', '"')):'';
				$resp['idErrCustomCode'] = (!empty(form_error('txtCustomCode')))?buildLabel('warning', form_error('txtCustomCode', '"', '"')):'';
				$resp['idErrCustomGroup'] = (!empty(form_error('selCustomGroup')))?buildLabel('warning', form_error('selCustomGroup', '"', '"')):'';
				$resp['idErrCustomTarif'] = (!empty(form_error('txtCustomTarif')))?buildLabel('warning', form_error('txtCustomTarif', '"', '"')):'';
				$resp['idErrCustomCategory'] = (!empty(form_error('selCustomCategory')))?buildLabel('warning', form_error('selCustomCategory', '"', '"')):'';
				$resp['idErrCustomItemGroup'] = (!empty(form_error('selCustomItemGroup')))?buildLabel('warning', form_error('selCustomItemGroup', '"', '"')):'';
				$resp['idErrCustomDesc'] = (!empty(form_error('txtCustomDesc')))?buildLabel('warning', form_error('txtCustomDesc', '"', '"')):'';
				$resp['idErrCustomDimension'] = (!empty(form_error('txtCustomDimension')))?buildLabel('warning', form_error('txtCustomDimension', '"', '"')):'';
				$resp['idErrCustomGrossweight'] = (!empty(form_error('txtCustomGrossweight')))?buildLabel('warning', form_error('txtCustomGrossweight', '"', '"')):'';
				$resp['idErrCustomBoxweight'] = (!empty(form_error('txtCustomBoxweight')))?buildLabel('warning', form_error('txtCustomBoxweight', '"', '"')):'';
				$resp['confirm'] = 'errValidation';
			else :
				$this->db->trans_begin();
				// Buat kode barang
				$conds = array(
					'select' => 'tgl_input, kd_barang',
					'order_by' => 'tgl_input DESC',
					'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
				);
				$kd_barang = $this->m_builder->buat_kode('tm_barang', $conds, 6, 'PRD');
				$item_relation = $this->input->post('txtCustomRelation');
				$kd_detail_barang = $this->input->post('txtCustomKdDetail');
				$no_project = $this->input->post('txtCustomNoProject');
				// Barang
				$item_barcode = $this->input->post('txtCustomBarcode');
				$item_code = $this->input->post('txtCustomCode');
				$hs_code = $this->input->post('txtCustomTarif');
				$group_barang_kd = $this->input->post('selCustomGroup');
				$kat_barang_kd = $this->input->post('selCustomCategory');
				$item_group_kd = $this->input->post('selCustomItemGroup');
				$deskripsi_barang = $this->input->post('txtCustomDesc');
				$dimensi_barang = $this->input->post('txtCustomProductDimension');
				$netweight = $this->input->post('txtCustomNettweight');
				$grossweight = $this->input->post('txtCustomGrossweight');
				$boxweight = $this->input->post('txtCustomBoxweight');
				$length_cm = $this->input->post('txtCustomLength');
				$width_cm = $this->input->post('txtCustomwidth');
				$height_cm = $this->input->post('txtCustomHeight');
				$item_cbm = $this->input->post('txtCustomCBM');
				$barang_stock_max = $this->input->post('txtCustomStockMax');
				$barang_stock_min = $this->input->post('txtCustomStockMin');
				$barang_stock_safety = $this->input->post('txtCustomStockSafety');
				$inventory_item = $this->input->post('selCustomInventItem');
				// Harga
				$kd_harga_barang = $this->td_barang_harga->create_code();
				$harga_distributor = $this->input->post('txtCustomHargaDistributor');
				$harga_retail = $this->input->post('txtCustomHargaRetail');
				$harga_reseller = $this->input->post('txtCustomHargaReseller');
				$harga_ekspor = $this->input->post('txtCustomHargaEkspor');
				$data['master'] = array(
					'kd_barang' => $kd_barang,
					'item_barcode' => $item_barcode,
					'item_code' => $no_project." ".$item_code,
					'hs_code' => $hs_code,
					'group_barang_kd' => $group_barang_kd,
					'kat_barang_kd' => $kat_barang_kd,
					'item_group_kd' => $item_group_kd,
					'deskripsi_barang' => $deskripsi_barang,
					'dimensi_barang' => $dimensi_barang,
					'netweight' => $netweight,
					'grossweight' => $grossweight,
					'boxweight' => $boxweight,
					'length_cm' => $length_cm,
					'width_cm' => $width_cm,
					'height_cm' => $height_cm,
					'item_cbm' => $item_cbm,
					'barang_stock_max' => $barang_stock_max,
					'barang_stock_min' => $barang_stock_min,
					'barang_stock_safety' => $barang_stock_safety,
					'inventory_item' => $inventory_item,
					'harga_barang_kd' => $kd_harga_barang,
					'tgl_input' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				);
				$data['harga'] = [
					'kd_harga_barang' => $kd_harga_barang,
					'barang_kd' => $kd_barang,
					'harga_lokal_distributor' => $harga_distributor,
					'harga_lokal_retail' => $harga_retail,
					'harga_lokal_reseller' => $harga_reseller,
					'harga_ekspor' => $harga_ekspor,
					'tgl_input' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				];
				/** Insert to DB Caterlindo PPIC */
				$this->db->insert('tm_barang', $data['master']);
				$this->db->insert('td_barang_harga', $data['harga']);
				$master_barang = $this->tm_barang->get_item(['kd_barang' => $kd_barang]);

				$fg_kd = $this->tm_finishgood->buat_kode();
				$dataFG = array(
					'fg_kd' => $fg_kd,
					'kd_gudang' => 'MGD071218001',
					'barang_kd' => $kd_barang,
					'item_code' => $no_project." ".$item_code,
					'item_barcode' => $item_group_kd == '2' ? '899000' : $item_barcode, /** Jika barang custom atau PJ barcodenya '899000' */
					'fg_qty' => 0,
					'fg_tglinput' => date('Y-m-d H:i:s'),
					'fg_tgledit' => null,
					'admin_kd' => $this->session->userdata('kd_admin'),
				);
				$actInsertFG = $this->tm_finishgood->insert_data($dataFG);

				/** Adjust/replace data barang_kd di detail SO berdasarkan kd detail SO */
				if($item_relation == 'parent'):
					// Parent
					$act_detail = $this->td_salesorder_item->update_data($kd_detail_barang, [
						'barang_kd' => $kd_barang,
						'item_code' => $master_barang->item_code,
						'item_desc' => $master_barang->deskripsi_barang,
						'item_dimension' => $master_barang->dimensi_barang,
						'netweight' => $master_barang->netweight,
						'grossweight' => $master_barang->grossweight,
						'boxweight' => $master_barang->boxweight,
						'length_cm' => $master_barang->length_cm,
						'width_cm' => $master_barang->width_cm,
						'height_cm' => $master_barang->height_cm,
						'item_cbm' => $master_barang->item_cbm
					]);
					// Cek apakah item parent mempunyai child
					$parent = $this->td_salesorder_item_detail->get_by_param(['ditem_so_kd' => $kd_detail_barang])->row();
					if( !empty($parent) ):
						$this->db->where('ditem_so_kd', $kd_detail_barang);
						$this->db->update('td_salesorder_item_detail', [
							'kd_parent' => $kd_barang,
						]);
					endif;
				else: 					
					// Child
					$act_detail = $this->td_salesorder_item_detail->update_data($kd_detail_barang, [
						'kd_child' => $kd_barang,
						'item_code' => $master_barang->item_code,
						'item_desc' => $master_barang->deskripsi_barang,
						'item_dimension' => $master_barang->dimensi_barang,
						'netweight' => $master_barang->netweight,
						'grossweight' => $master_barang->grossweight,
						'boxweight' => $master_barang->boxweight,
						'length_cm' => $master_barang->length_cm,
						'width_cm' => $master_barang->width_cm,
						'height_cm' => $master_barang->height_cm,
						'item_cbm' => $master_barang->item_cbm
					]);
				endif;
				if($this->db->trans_status() === FALSE):
					$this->db->trans_rollback();
					$resp = ['code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan'];
				else:
					// Push ke SAP
					$api = $this->push_custom_item_to_sap($kd_barang, "add");
					if($api[0]->ErrorCode == 0):
						$this->db->trans_commit();
						$resp = ['code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan, API '.$api[0]->Message];
					else:
						$this->db->trans_rollback();
						$resp = ['code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan, API '.$api[0]->Message];
					endif;
					// End push ke SAP
					$this->db->trans_commit();
					$resp = ['code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan'];

				endif;
			endif;
			header('Content-Type: application/json');
			echo json_encode($resp);
		endif;
	}

	function table_total_price($kd_msalesorder) {
		$price_data = $this->m_salesorder->get_price($kd_msalesorder);
		return $price_data;
	}

	function get_ppn() {
		$var = 'set_ppn';
		$result = $this->m_app_setting->get_data($var);
		$value = $result->nilai_setting;

		return $result?$value:FALSE;
	}

	private function create_pkey($table, $primary, $unique) {
		$this->db->select($primary.' AS code')
			->from($table)
			->where(array('DATE(tgl_input)' => date('Y-m-d')))
			->order_by('tgl_input DESC, '.$primary.' DESC');
		$query = $this->db->get();
		$num = $query->num_rows();
		$urutan = 0;
		if ($num > 0) :
			$row = $query->row();
			$code = $row->code;
			$urutan = substr($code, -6);
		endif;
		$angka = $urutan + 1;
		$primary = $unique.date('dmy').str_pad($angka, 6, '000', STR_PAD_LEFT);
		return $primary;
	}

	private function create_inc_code($last_code = '', $unique = '') {
		if (!empty($last_code)) :
			$urutan = substr($last_code, -6);
		else :
			$urutan = 0;
		endif;
		$angka = $urutan + 1;
		$primary = $unique.date('dmy').str_pad($angka, 6, '000', STR_PAD_LEFT);
		return $primary;
	}

	function submit_form() {
		if ($this->input->is_ajax_request()) :
			$relation = $this->input->post('txtRelation');
			$set_row = $this->m_app_setting->get_data('custom_barcode');

			if ($relation == 'parent') :
				$this->form_validation->set_rules('txtKdBarang', 'Product Code', 'required',
					array(
						'required' => '{field} tidak boleh kosong',
					)
				);
				$this->form_validation->set_rules('txtProductCode', 'Product Code', 'required',
					array(
						'required' => '{field} tidak boleh kosong',
					)
				);
			elseif ($relation == 'child') :
				$this->form_validation->set_rules('selProductChild', 'Product Code', 'required',
					array(
						'required' => '{field} tidak boleh kosong',
					)
				);
			endif;
			$this->form_validation->set_rules('txtProductQty', 'Qty Product', 'required',
				array(
					'required' => '{field} tidak boleh kosong',
				)
			);
			if ($this->form_validation->run() == FALSE) :
				$err = '';
				$str['idErrProductCode'] = '';
				$str['confirm'] = 'error';
				if ($relation == 'parent') :
					if (empty(form_error('txtProductCode'))) :
						$err = form_error('txtKdBarang', '"', '"');
					endif;
					if (empty(form_error('txtKdBarang'))) :
						$err = form_error('txtProductCode', '"', '"');
					endif;
					if (!empty($err)) :
						$str['idErrProductCode'] = buildLabel('warning', $err);
					endif;
				elseif ($relation == 'child') :
					$str['idErrProductCode'] = (!empty(form_error('selProductChild')))?buildLabel('warning', form_error('selProductChild', '"', '"')):'';
				endif;
				$str['idErrProductQty'] = (!empty(form_error('txtProductQty')))?buildLabel('warning', form_error('txtProductQty', '"', '"')):'';
			else :
				$act = $this->input->post('txtFormType');
				$barang_kd = $this->input->post('txtKdBarang');
				$item_status = $this->input->post('selItemStatus');
				$kd_ditem = $this->input->post('txtKdDSales');
				if ($item_status == 'std') :
					$data['item_desc'] = $this->input->post('txtItemDescOri');
					$data['item_dimension'] = $this->input->post('txtItemDimensionOri');
					$data['harga_retail'] = $this->input->post('txtProductPriceRetail');
					$data['item_barcode'] = $this->input->post('txtBarcode');
				elseif ($item_status == 'custom') :
					$data['item_desc'] = $this->input->post('txtItemDesc');
					$data['item_dimension']= $this->input->post('txtItemDimension');
					$data['harga_retail'] = $this->input->post('txtProductPrice');
					$data['item_barcode'] = $this->input->post('txtBarcode');
				endif;
				$data['msalesorder_kd'] = $this->input->post('txtKdMSales');
				$data['item_status'] = $item_status;
				$data['item_qty'] = $this->input->post('txtProductQty');
				$data['harga_barang'] = $this->input->post('txtProductPrice');
				$data['disc_type'] = $this->input->post('selDiscType');
				$data['item_disc'] = $this->input->post('txtProductDisc');
				$data['total_harga'] = $this->input->post('txtProductTotPrice');
				$data['item_note'] = $this->input->post('txtProductNote');
				$data['netweight'] = $this->input->post('txtNetweight');
				$data['grossweight'] = $this->input->post('txtGrossweight');
				$data['boxweight'] = $this->input->post('txtBoxweight');
				$data['length_cm'] = $this->input->post('txtLength');
				$data['width_cm'] = $this->input->post('txtWidth');
				$data['height_cm'] = $this->input->post('txtHeight');
				$data['item_cbm'] = $this->input->post('txtItemCbm');

				
				// /** Jika input barang custom maka langsung insert data item ke master dengan nomor projectnya, insert ke tm_project status approved dan push ke SAP */
				// if($item_status == 'custom'):
				// 	$barang_kd = ;
				// endif;
				// /** End */

				if ($relation == 'parent') :
					$data['kd_ditem_so'] = !empty($kd_ditem)?$kd_ditem:$this->create_pkey('td_salesorder_item', 'kd_ditem_so', 'DSI');
					$data['barang_kd'] = $barang_kd;
					$data['item_code'] = $this->input->post('txtProductCode');
				elseif ($relation == 'child') :
					$data['kd_citem_so'] = !empty($kd_ditem)?$kd_ditem:$this->create_pkey('td_salesorder_item_detail', 'kd_citem_so', 'DSC');
					$data['ditem_so_kd'] = $this->input->post('txtKdParentSales');
					$data['kd_parent'] = $this->input->post('txtKdParent');
					$data['kd_child'] = $barang_kd;
					$data['item_code'] = $this->input->post('txtProductCodeChild');
				endif;
				$data['order_tipe'] = $this->input->post('txtorder_tipe');

				$str = $this->send_data($act, $relation, $data);
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function send_data($act, $relation, $data = array()) {
		$this->db->trans_begin();
		if ($relation == 'parent') :
			$where['kd_ditem_so'] = $data['kd_ditem_so'];
			$so_item_kd = $data['kd_ditem_so'];
		elseif ($relation == 'child') :
			$where['kd_citem_so'] = $data['kd_citem_so'];
			$so_item_kd = $data['kd_citem_so'];
		endif;

		if ($act == 'input') :
			$label_err = 'menambahkan';
			$function = 'insert_data_'.$relation;
			$act = $this->m_salesorder->{$function}($data);
			$data_sowo = $this->send_data_so_wo($data['msalesorder_kd'], $data['kd_ditem_so']);
		elseif ($act == 'edit') :
			$label_err = 'mengubah';
			$function = 'update_data_'.$relation;
			$act = $this->m_salesorder->{$function}($data, $where);
		endif;
		
		if ($act) :
			if ($act == 'edit') :
				$this->db->delete('td_salesorder_item_disc', array('so_item_kd' => $so_item_kd, 'mso_kd' => $data['msalesorder_kd']));
			endif;
			$nm_pihak = $this->input->post('txtNmPihakDisc');
			$jml_disc = $this->input->post('txtDefaultDisc');
			$type_individual_disc = $this->input->post('selIndividualDisc');
			$jml_individual_disc = $this->input->post('txtIndividualDiscVal');
			$flag_individual_disc = $this->input->post('txtFlagIndividualDisc');
			$view_access = $this->input->post('txtDiscViewAccess');
			$kd_item_disc = $this->create_pkey('td_salesorder_item_disc', 'kd_item_disc', 'PSO');
			for ($i = 0; $i < count($nm_pihak); $i++) :
				$kd_item_disc = $i == 0?$kd_item_disc:$this->create_inc_code($kd_item_disc, 'PSO');
				$data_disc[] = array(
					'kd_item_disc' => $kd_item_disc,
					'so_item_kd' => $so_item_kd,
					'mso_kd' => $data['msalesorder_kd'],
					'item_relation' => $relation,
					'nm_pihak' => $nm_pihak[$i],
					'jml_disc' => $jml_disc[$i],
					'type_individual_disc' => $type_individual_disc[$i],
					'jml_individual_disc' => $jml_individual_disc[$i],
					'flag_individual_disc' => $flag_individual_disc[$i],
					'view_access' => $view_access[$i],
					'tgl_input' => date('Y-m-d H:i:s')
				);
			endfor;
			if (isset($data_disc)) :
				$aksi['disc_default'] = $this->db->insert_batch('td_salesorder_item_disc', $data_disc);
			endif;

			if($this->db->trans_status() === FALSE):
				$this->db->trans_rollback();
				$log_stat = 'gagal';
				$str['confirm'] = 'error';
				$str['idErrForm'] = buildAlert('danger', 'Gagal!', 'Gagal '.$label_err.' '.$this->title.', kesalahan sistem!');
				$str['data'] = $data;
			else:
				$this->db->trans_commit();
				$log_stat = 'berhasil';
				$str['confirm'] = 'success';
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
				$str['data'] = $data;
				$str['data_sowo'] = $data_sowo;
			endif;

		else :
			$log_stat = 'gagal';
			$str['confirm'] = 'error';
			$str['idErrForm'] = buildAlert('danger', 'Gagal!', 'Gagal '.$label_err.' '.$this->title.', kesalahan sistem!');
			$str['data'] = $data;
			$str['data_sowo'] = $data_sowo;
			
		endif;
		$this->write_log($log_stat, $label_err, $data);

		return $str;
	}

	public function send_data_so_wo($msalesorder_kd, $so_item_kd)
	{
		if (!empty($so_item_kd)) {
		$items = $this->td_salesorder_item->get_where_in_project($so_item_kd)->result_array();

		$data_wo = $this->tb_relasi_sowo->get_by_param(["kd_msalesorder" => $msalesorder_kd]);
		$wo = $data_wo->result_array();
		//add workorder item so

		$woitemso_kd = $this->td_workorder_item_so->buat_kode();
		$data = array(
			'woitemso_kd' => $woitemso_kd,
			'wo_kd' => $wo[0]['wo_kd'],
			'kd_barang' => $items[0]['barang_kd'],
			'woitemso_itemcode' => !empty($items[0]['item_code']) ? $items[0]['item_code'] : null,
			'woitemso_itemdeskripsi' => !empty($items[0]['item_desc']) ? $items[0]['item_desc'] : null,
			'woitemso_itemdimensi' => !empty($items[0]['item_dimension']) ? $items[0]['item_dimension'] : null,
			'woitemso_qty' => $items[0]['item_qty'],
			'woitemso_status' => $items[0]['item_status'],
			'bom_kd' => '',
			'woitemso_nopj' => $items[0]['project_nopj'],
			'woitemso_tipe' => 'additional',
			'woitemso_prosesstatus' => 'editing',
			'woitemso_tglselesai' => format_date($items[0]['tgl_input'], 'Y-m-d'),
			'woitemso_tglinput' => date('Y-m-d H:i:s'),
			'woitemso_tgledit' => date('Y-m-d H:i:s'),
			'admin_kd' => $this->session->userdata('kd_admin'),
		);
		$act = $this->td_workorder_item_so->insert_data($data);
		//add workorder item so detail 
		

			$arrayWoitemsodetails = [];
            $woitemsodetail_kd = $this->td_workorder_item_so_detail->generate_kd();
            foreach ($items as $item) {
                $arrayWoitemsodetails[] = [
                    'woitemsodetail_kd' => $woitemsodetail_kd,
                    'wo_kd' => $wo[0]['wo_kd'],
                    'relasisowo_kd' => $wo[0]['relasisowo_kd'],
                    'kd_msalesorder' => $item['msalesorder_kd'],
                    'kd_barang' => $item['barang_kd'],
                    'parent_kd' => $item['kd_ditem_so'],
                    'child_kd' => null,
                    'woitemso_kd' => $woitemso_kd,
                    'woitemsodetail_itemcode' => $item['item_code'],
                    'woitemsodetail_itemdeskripsi' => $item['item_desc'],
                    'woitemsodetail_itemdimensi' => $item['item_dimension'],
                    'woitemsodetail_qty' => $item['item_qty'],
                    'woitemsodetail_status' => $item['item_status'],
                    'woitemsodetail_tipe' => 'so',
                    'woitemsodetail_prosesstatus' => 'workorder',
                    'woitemsodetail_tglinput' => date('Y-m-d H:i:s'),
                    'woitemsodetail_tgledit' => date('Y-m-d H:i:s'),
                    'admin_kd' => $this->session->userdata('kd_admin'),
                ];
                $woitemsodetail_kd++;
            }

			return $this->td_workorder_item_so_detail->insert_batch_data($arrayWoitemsodetails);
			/** Transaction db function dipasang di function send_data */
		}else{
			// return 'Tidak Berhasil';
			return false;
		}
	}

	function hapus_data() {
		if ($this->input->is_ajax_request()) :
			$this->db->trans_begin();
			$p_code = $this->input->get('id');
			$relation = $this->input->get('relation');
			$label_err = 'menghapus';
			if ($relation == 'parent') :
				$aksi['master'] = $this->db->delete('td_salesorder_item', array('kd_ditem_so' => $p_code));
				$aksi['detail'] = $this->db->delete('td_salesorder_item_detail', array('ditem_so_kd' => $p_code));
			elseif ($relation == 'child') :
				$aksi['detail'] = $this->db->delete('td_salesorder_item_detail', array('kd_citem_so' => $p_code));
			endif;
			$this->db->delete('td_salesorder_item_disc', array('so_item_kd' => $p_code));
			$data = array('kd_ditem_so' => $p_code.' pada tabel td_salesorder_item dan td_salesorder_item_detail');
			
			if($this->db->trans_status() === FALSE):
				$this->db->trans_rollback();
				$stat = 'gagal';
				$str['alert'] = buildAlert('danger', 'Gagal!', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
			else:
				$this->db->trans_commit();
				$stat = 'berhasil';
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
			endif;
			$this->write_log($stat, $label_err, $data);

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function get_item_detail($type) {
		$kd_item = $this->input->get('kd_item');

		if ($type == 'parent') :
			$result = $this->m_salesorder->get_parent($kd_item);
		elseif ($type == 'child') :
			$result = $this->m_salesorder->get_child($kd_item);
		endif;

		if ($type == 'parent') :
			$data['kd_ditem_so'] = $result->kd_ditem_so;
			$data['barang_kd'] = $result->barang_kd;
			$data['item_code'] = $result->item_code;
			$data['code_ori'] = $result->code_ori;
			$data['item_barcode'] = $result->item_barcode;
			$data['barcode_ori'] = $result->barcode_ori;
		elseif ($type == 'child') :
			$data['kd_citem_so'] = $result->kd_citem_so;
			$data['ditem_so_kd'] = $result->ditem_so_kd;
			$data['kd_parent'] = $result->kd_parent;
			$data['parent_code'] = $result->parent_code.'/'.$result->parent_desc;
			$data['kd_child'] = $result->kd_child;
			$data['item_code'] = $result->child_code;
			$data['item_barcode'] = $result->item_barcode;
			$data['barcode_ori'] = $result->barcode_ori;
		endif;
		$data['item_status'] = $result->item_status;
		$data['item_desc'] = $result->item_desc;
		$data['item_desc_ori'] = $result->desc_ori;
		$data['item_dimension'] = $result->item_dimension;
		$data['item_dimension_ori'] = $result->dimensi_ori;
		$data['netweight'] = $result->netweight;
		$data['netweight_ori'] = $result->netweight_ori;
		$data['grossweight'] = $result->grossweight;
		$data['grossweight_ori'] = $result->grossweight_ori;
		$data['boxweight'] = $result->boxweight;
		$data['boxweight_ori'] = $result->boxweight_ori;
		$data['length_cm'] = $result->length_cm;
		$data['length_cm_ori'] = $result->length_cm_ori;
		$data['width_cm'] = $result->width_cm;
		$data['width_cm_ori'] = $result->width_cm_ori;
		$data['height_cm'] = $result->height_cm;
		$data['height_cm_ori'] = $result->height_cm_ori;
		$data['item_cbm'] = $result->item_cbm;
		$data['item_cbm_ori'] = $result->item_cbm_ori;
		$data['item_qty'] = $result->item_qty;
		$data['harga_barang'] = $result->harga_barang;
		$data['harga_retail'] = $result->harga_retail;
		$data['disc_type'] = $result->disc_type;
		$data['item_disc'] = $result->item_disc;
		$data['total_harga'] = $result->total_harga;
		$data['item_note'] = $result->item_note;

		$data['barang_kd_custom'] = 'PRD020817000573';
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	function get_default_disc_form() {
		$this->load->model(array('td_salesorder_item_disc'));
		if ($this->input->is_ajax_request()) :
			$kd_jenis_customer = $this->input->get('jenis_customer_kd');
			$so_item_kd = $this->input->get('so_item_kd');
			$mso_kd = $this->input->get('mso_kd');
			$form_default_disc = $this->td_salesorder_item_disc->render_form_default_disc($so_item_kd, $mso_kd, $kd_jenis_customer);
			echo $form_default_disc;
		endif;
	}

	function submit_detail() {
		if ($this->input->is_ajax_request()) :
			$act = $this->input->get('act');
			$kd_master = $this->input->get('kd_master');
			$value = $this->input->get('value');
			$fix_value = $value;
			$ppn = $this->get_ppn();
			if ($this->m_msalesorder->check_val($act, $value, $kd_master)) :
				$fix_value = $this->before_ppn($value, $ppn);
			endif;
			if ($act == 'submit_ongkir') :
				$ongkir = $this->m_msalesorder->update_price('ongkir', $kd_master, $fix_value);
				$install = $this->m_msalesorder->detail_price('install', $kd_master);
			elseif ($act == 'submit_install') :
				$install = $this->m_msalesorder->update_price('install', $kd_master, $fix_value);
				$ongkir = $this->m_msalesorder->detail_price('ongkir', $kd_master);
			endif;
			$data['kd_master'] = $kd_master;

			header('Content-Type: application/json');
			echo json_encode($data);
		endif;
	}

	function before_ppn($value, $ppn) {
		$iki = $value / (($ppn + 100) / 100);
		return $iki;
	}

	function submit_disc() {
		if ($this->input->is_ajax_request()) :
			$str['csrf'] = $this->security->get_csrf_hash();
			$label_err = 'menambahkan diskon special';

			$this->form_validation->set_rules('txtNmKolomPrice', 'Nama Discount', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtTotalJumlah', 'Total Jumlah', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'error';
				$stat = 'gagal';
				$str['idErrNmKolom'] = (!empty(form_error('txtNmKolomPrice')))?buildLabel('warning', form_error('txtNmKolomPrice', '"', '"')):'';
				$str['idErrTotalJumlah'] = (!empty(form_error('txtTotalJumlah')))?buildLabel('warning', form_error('txtTotalJumlah', '"', '"')):'';
			else :
				$data['kd_dharga_so'] = $this->create_pkey('td_salesorder_harga', 'kd_dharga_so', 'DSH');
				$data['msalesorder_kd'] = $this->input->post('txtKdMSalesPrice');
				$data['nm_kolom'] = $this->input->post('txtNmKolomPrice');
				$data['type_kolom'] = $this->input->post('selKolomType');
				$data['total_nilai'] = $this->input->post('txtTotalJumlah');
				$aksi = $this->m_salesorder->insert_data_harga($data);

				if ($aksi) :
					$stat = 'berhasil';
					$str['confirm'] = 'success';
					$str['kd_master'] = $data['msalesorder_kd'];
					$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
				else :
					$stat = 'gagal';
					$str['confirm'] = 'error';
					$str['idErrForm'] = buildAlert('danger', 'Gagal!', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
				endif;
				$this->write_log($stat, $label_err, $data);
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function hapus_special_disc() {
		if ($this->input->is_ajax_request()) :
			$id = $this->input->get('id');
			$label_err = 'menghapus diskon special';
			$data = array('kd_dharga_so' => $id);
			$aksi = $this->db->delete('td_salesorder_harga', $data);
			if ($aksi) :
				$stat = 'berhasil';
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.'!');
			else :
				$stat = 'gagal';
				$str['alert'] = buildAlert('danger', 'Gagal '.$label_err.', Kesalahan sistem!');
			endif;
			$this->write_log($stat, $label_err, $data);

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function write_log($stat, $var, $data = array()){
		$nm_kolom = '';
		$no = 0;
		$jml = count($data);
		foreach ($data as $key => $val) :
			$no++;
			$koma = $no == $jml?'':',';
			$nm_kolom .= ' '.$key.' = '.$val.$koma;
		endforeach;
		$this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' '.$stat.' '.$var.' '.$this->title.' dengan'.$nm_kolom);
	}

	function push_custom_item_to_sap($kd, $act)
	{
		/** Custom item di insert dan di concat dengan no_project */
		$data = $this->tm_barang->get_item(['kd_barang' => $kd]);
		/** API FILL VARIABLE */
		$dataAPI = [
			'ItemCode' => $data->item_code,
			'ItemName' => $data->deskripsi_barang,
			'PurchaseItem' => "N",
			'SalesItem' => "Y",
			'InventoryItem' => $data->inventory_item,
			'KdItemGroup' => $data->item_group_kd, /** kodenya diambil dari modul item group */
			'Dimensi' => $data->dimensi_barang,
			'NetWeight' => $data->netweight,
			'GrossWeight' => $data->grossweight,
			'BoxWeight' => $data->boxweight,
			'Length' => $data->length_cm,
			'Width' => $data->width_cm,
			'Height' => $data->height_cm,
			'Volume' => $data->item_cbm,
			'PrchseItem' => "N",
			'SellItem' => "Y",
			'InvntItem' => $data->inventory_item,
			'StockSafety' => $data->barang_stock_safety,
			'U_IDU_WEBID' => $data->kd_barang,
		];
		if ($act == "add"):
			$api = parent::api_sap_post('AddItem', $dataAPI);
		else:
			$api = parent::api_sap_post('EditItem', $dataAPI);
		endif;
		return $api;
	}

}