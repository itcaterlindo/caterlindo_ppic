<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Sales_orders_form extends MY_Controller {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'tm_salesorder';
	private $p_key = 'kd_msalesorder';
	private $class_link = 'sales/sales_orders';
	private $title = 'Sales Orders';

	function __construct() {
		parent::__construct();
		
		$this->load->helper(array('form', 'html', 'my_helper'));
		$this->load->library(array('form_validation'));
		$this->load->model(array('m_builder', 'm_msalesorder', 'm_salesperson', 'm_karyawan', 'm_customer', 'm_master_quotation', 'm_app_setting', 'model_jenis_customer', 'db_caterlindo/tm_po', 'tm_salesorder', 'tm_quotation', 'm_quotation', 'model_salesorder', 'tb_teritory', 'tb_container', 'tm_customer', 'td_customer_alamat'));
	}

	function index() {
		parent::administrator();
		parent::pnotify_assets();

		$this->get_form();
	}

	function define_page($url_array_num) {
		$ttl = $this->uri->segment($url_array_num);
		if (strpos($ttl, '_') !== FALSE) :
			$rpl_title = str_replace('_', ' ', $ttl);
			$title = ucwords($rpl_title);
		else :
			$title = ucwords($this->uri->segment($url_array_num));
		endif;
		return $title;
	}

	function get_form() {
		// Buat judul halaman dengan mengambil data dari URL yang didapatkan pada browser
		$page = $this->define_page(2);

		// Box property header_color/box_status, box id, box title, dan box_overlay 
		$data['box_id'] = 'idBoxFormSalesOrder';
		$data['box_status'] = 'info';
		$data['box_title'] = 'Form '.ucwords($page);
		$data['idbox_overlay'] = 'idBoxOverlayForm';
		$data['idbox_loader'] = 'idBoxLoaderForm';
		$data['idbox_content'] = 'idBoxContentForm';
		$data['tbl_alertid'] = 'idErrForm';

		// btn array btn_add, btn_hide, btn_close dengan menambahkan id kesetiap buttonnya
		$data['btns'] = array('btn_hide' => 'idBtnHideBoxForm', 'btn_close' => 'idBtnCloseBox');
		// Gunakan $data['box_body'] = 'box_body_url' untuk memanggil isi dari box
		$data['box_body'] = 'page/'.$this->class_link.'/form_select';
		// Variabel $data['form_data'] untuk mendapatakkan isi dari form
		$data['form_data'] = '';
		// Variabel $data['class_link'] untuk mendapatakkan uri dari controller
		$data['class_link'] = $this->class_link;
		$data['form_error'] = array('idErrSales');
		// Variabel $data['adds_js'] untuk mendapatakkan memanggil file js
		// $data['adds_js'] = array('form_js');
		// $data['js_data'] digunakan untuk penulisan form_error dan mendapatkan box_id
		// $data['js_data'] = array('form_error' => array('idErrProductCode', 'idErrLaporanFooter'), 'box_id' => 'idBoxFormat', 'class_link' => $this->class_link);

		$this->load->view('containers/box', $data);
	}

	function form_type() {
		if ($this->input->is_ajax_request()) :
            $str['csrf'] = $this->security->get_csrf_hash();
			$pilihan = $this->input->post('selSales');

			$this->form_validation->set_rules('selSales', 'Pilihan SO', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'error';
				$str['idErrSales'] = (!empty(form_error('selSales')))?buildLabel('warning', form_error('selSales', '"', '"')):'';
			else :
				$str['confirm'] = 'success';
				$str['selected'] = $this->check_selected($pilihan);
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function check_selected($sel) {
		if ($sel == 'po') :
			// Keluarkan form baru seperti form quotation
			$pil = 'form_po';
		elseif ($sel == 'quo') :
			// Keluarkan tabel quo
			$pil = 'form_quo';
		endif;
		return $pil;
	}

	function form_po($form_type) {
		if ($form_type == 'select_sales') :
			$this->sales_form('', 'new');
		elseif ($form_type == 'select_items') :
			$this->items_form();
		endif;
	}

	function sales_form($kd_msales, $order_tipe = '') {
		// Buat judul halaman dengan mengambil data dari URL yang didapatkan pada browser
		$page = $this->define_page(2);

		// Box property header_color/box_status, box id, box title, dan box_overlay 
		$data['box_id'] = 'idBoxFormSalesOrder';
		$data['box_status'] = 'info';
		$data['box_title'] = 'Form '.ucwords($page);
		$data['idbox_overlay'] = 'idBoxOverlayForm';
		$data['idbox_loader'] = 'idBoxLoaderForm';
		$data['idbox_content'] = 'idBoxContentForm';
		$data['tbl_alertid'] = 'idErrForm';

		$arrContainer = $this->tb_container->get_all();
		// UNTUK PARSING CONTAINER KE VIEW FORM
		$container[''] = "-- Silahkan Pilih Container --";
		foreach($arrContainer as $con):
			$container[$con->id] = $con->nm_container." - ".$con->deskripsi;
		endforeach;

		$arrTeritory = $this->tb_teritory->get_all();
		// UNTUK PARSING TERITORY KE VIEW FORM
		$teritory[''] = "-- Silahkan Pilih Teritory --";
		foreach($arrTeritory as $ter):
			$teritory[$ter->id] = $ter->nm_teritory." - ".$ter->deskripsi;
		endforeach;

		// btn array btn_add, btn_hide, btn_close dengan menambahkan id kesetiap buttonnya
		$data['btns'] = array('btn_hide' => 'idBtnHideBoxForm', 'btn_close' => 'idBtnCloseBox');
		// Gunakan $data['box_body'] = 'box_body_url' untuk memanggil isi dari box
		$data['box_body'] = 'page/'.$this->class_link.'/form_sales';
		// Variabel $data['form_data'] untuk mendapatakkan isi dari form
		$data['form_data'] = $this->get_masterdata($kd_msales);
		$data['form_data']['container'] = $container;
		$data['form_data']['teritory'] = $teritory;		
		$data['order_tipe'] = $order_tipe;
		if (!empty($order_tipe) && empty($data['form_data']['kd_msalesorder'])) :
			$data['form_data']['kd_msalesorder'] = '';
			$data['form_data']['status_so'] = 'pending';
		endif;
		// Variabel $data['class_link'] untuk mendapatakkan uri dari controller
		$data['class_link'] = $this->class_link;
		$data['form_error'] = array('idErrForm', 'idErrSales', 'idErrNoSo', 'idErrEmailSales', 'idErrDate', 'idErrMobilePhone', 'idErrCustomerId', 'idErrNmCustomer', 'idErrAlamatKirim', 'idErrFileAttach');
		// Variabel $data['adds_js'] untuk mendapatkan/memanggil file js
		// $data['adds_js'] = array('form_js');
		// $data['js_data'] digunakan untuk penulisan form_error dan mendapatkan box_id
		// $data['js_data'] = array('form_error' => array('idErrProductCode', 'idErrLaporanFooter'), 'box_id' => 'idBoxFormat', 'class_link' => $this->class_link);

		$this->load->view('containers/box', $data);
	}

	function get_masterdata($var) {
		$row = $this->m_msalesorder->read_masterdata($var);
		if ($row) :
			$salesperson_kd = $row->salesperson_kd;
			$customer_kd = $row->customer_kd;
			$alamat_kirim_kd = $row->alamat_kirim_kd;
			$teritory_id = !empty($row->teritory_id) ? $row->teritory_id : "";
			$tgl_so = format_date($row->tgl_so, 'd-m-Y');
			$tgl_kirim = empty($row->tgl_kirim) || $row->tgl_kirim == '0000-00-00'?'-':format_date($row->tgl_kirim, 'd-m-Y');
			$tgl_po = format_date($row->tgl_po, 'd-m-Y');
			$readonly = !empty($salesperson_kd)?'readonly':'';
			$data_master = array('readonly' => $readonly, 'kd_msalesorder' => $row->kd_msalesorder, 'email_sales' => $row->email_sales, 'mobile_sales' => $row->mobile_sales, 'mquotation_kd' => $row->mquotation_kd, 'salesperson_kd' => $salesperson_kd, 'teritory_id' => $teritory_id, 'tgl_so' => $tgl_so, 'tgl_kirim' => $tgl_kirim, 'note_so' => $row->note_so, 'no_po' => $row->no_po, 'tgl_po' => $tgl_po, 'file_attach_po' => $row->file_attach_po, 'tipe_container' => $row->tipe_container, 'container_20ft' => $row->container_20ft, 
			'customer_kd' => $customer_kd, 'no_salesorder' => $row->no_salesorder, 'alamat_form' => $this->get_alamat($customer_kd, $alamat_kirim_kd), 'status_so' => $row->status_so);
		else :
			// Jika data tidak ditemukan, maka sistem akan cek kd_session salesperson jika kd_session ditemukan maka set sales person kd
			$kd_karyawan = $this->session->kd_karyawan;
			$salesperson_kd = $this->m_salesperson->check_sales($kd_karyawan);
			$det_salesperson = $this->m_salesperson->get_row($salesperson_kd);
			if (!empty($det_salesperson)) :
				$email = $det_salesperson->email_address;
				$telp = $det_salesperson->no_telp;
			else :
				$email = '';
				$telp = '';
			endif;
			$customer_kd = '';
			$teritory_id = '';
			$readonly = !empty($salesperson_kd)?'readonly':'';
			$data_master = array('readonly' => $readonly, 'kd_msalesorder' => '', 'mquotation_kd' => '', 'salesperson_kd' => $salesperson_kd, 'teritory_id' => $teritory_id, 'email_sales' => $email, 'mobile_sales' => $telp, 'tgl_so' => date('d-m-Y'), 'note_so' => '', 'no_po' => '', 'tgl_po' => date('d-m-Y'), 'tgl_kirim' => date('d-m-Y'), 'file_attach_po' => '', 'tipe_container' => '', 'container_20ft' => '','customer_kd' => '', 'no_salesorder' => '', 'alamat_form' => '', 'status_so' => 'pending');
		endif;
		$data_detail_salesperson = array();
		$data_detail_customer = array();
		$data_detail_salesperson = $this->get_salesdetail($salesperson_kd);
		$data_detail_customer = $this->get_customerdetail($customer_kd);
		$data = array_merge($data_detail_salesperson, $data_detail_customer, $data_master);

		return $data;
	}

	function get_ppn() {
		$var = 'set_ppn';
		$result = $this->m_app_setting->get_data($var);
		$value = $result->nilai_setting;

		return $result?$value:FALSE;
	}

	function get_salesdetail($kd_salesperson) {
		$row_sales = $this->m_salesperson->get_detail($kd_salesperson);
		$kd_karyawan = '';
		if (!empty($row_sales)) :
			$nm_salesperson = $row_sales->nm_salesperson;
			$kd_karyawan = $row_sales->karyawan_kd;
		endif;
		$row_contact = $this->m_karyawan->get_contact($kd_karyawan);
		if (!empty($row_contact)) :
			// Buat variable untuk menentukan mana telp tidak kosong
			$telp = '';
			if (!empty($row_contact->telp_kantor) || $row_contact->telp_kantor != '-') :
				$telp = $row_contact->telp_kantor;
			elseif (!empty($row_contact->telp_mobile) || $row_contact->telp_mobile != '-') :
				$telp = $row_contact->telp_mobile;
			endif;

			// Buat variable untuk menentukan mana email tidak kosong
			$email = '';
			if (!empty($row_contact->email_utama) || $row_contact->email_utama != '-') :
				$email = $row_contact->email_utama;
			elseif (!empty($row_contact->email_lain) || $row_contact->email_lain != '-') :
				$email = $row_contact->email_lain;
			endif;
		endif;
		$data['nm_salesperson'] = isset($nm_salesperson)?$nm_salesperson:'';
		$data['telp'] = isset($telp)?$telp:'';
		$data['email'] = isset($email)?$email:'';

		return $data;
	}

	function get_customerdetail($kd_customer) {
		$row = $this->m_customer->detail_customer($kd_customer);
		if (!empty($row)) :
			$nm_customer = $row->nm_customer;
			$nm_badanusaha = $row->nm_select;
			$contact_person = $row->contact_person;
			$nm_negara = $row->nm_negara.after_before_char($row->nm_negara, array($row->nm_negara), '.', '.');
			$nm_provinsi = $row->nm_provinsi.after_before_char($row->nm_provinsi, array($row->nm_negara), ', ', '.');
			$nm_kota = $row->nm_kota.after_before_char($row->nm_kota, array($row->nm_provinsi, $row->nm_negara), ', ', '.');
			$nm_kecamatan = $row->nm_kecamatan.after_before_char($row->nm_kecamatan, array($row->nm_kota, $row->nm_provinsi, $row->nm_negara), ', ', '.');
			$alamat = ucwords(strtolower($row->alamat)).after_before_char($row->alamat, array($row->nm_kecamatan, $row->nm_kota, $row->nm_provinsi, $row->nm_negara), '. ', '. ');
			$alamat_instansi = $alamat.$nm_kecamatan.$nm_kota.$nm_provinsi.$nm_negara;
			$code_customer = $row->code_customer;
		endif;
		$data['nm_customer'] = isset($nm_customer)?$nm_customer:'';
		$data['contact'] = isset($contact_person)?$contact_person:'';
		$data['alamat_instansi'] = isset($alamat_instansi)?$alamat_instansi:'';
		$data['code_customer'] = isset($code_customer)?$code_customer:'';

		return $data;
	}

	function dropdown_alamat() {
		$kd_customer = $this->input->get('kd_customer');
		$kd_alamat = $this->input->get('kd_alamat_kirim');

		echo $this->get_alamat($kd_customer, $kd_alamat);
	}

	function get_alamat($kd_customer, $kd_alamat) {
		$rows = $this->m_customer->alamat_customer($kd_customer);

		if (!empty($rows)) :
			$no = 0;
			foreach ($rows as $row) :
				$no++;
				$kd_alamat_kirim = $row->kd_alamat_kirim;
				$nm_negara = $row->nm_negara.after_before_char($row->nm_negara, array($row->nm_negara), '.', '.');
				$nm_provinsi = $row->nm_provinsi.after_before_char($row->nm_provinsi, array($row->nm_negara), ', ', '.');
				$nm_kota = $row->nm_kota.after_before_char($row->nm_kota, array($row->nm_provinsi, $row->nm_negara), ', ', '.');
				$nm_kecamatan = $row->nm_kecamatan.after_before_char($row->nm_kecamatan, array($row->nm_kota, $row->nm_provinsi, $row->nm_negara), ', ', '.');
				$alamat = ucwords(strtolower($row->alamat)).after_before_char($row->alamat, array($row->nm_kecamatan, $row->nm_kota, $row->nm_provinsi, $row->nm_negara), '. ', '. ');
				$kd_badan_usaha = $row->kd_badan_usaha;
				$nm_customer = $row->nm_customer;
				$nm_select = $row->nm_select;
				$nm_badan_usaha = ($nm_select == 'Lainnya')?''.$nm_customer.' - ':$nm_select.' '.$nm_customer.' - ';
				$alamat = $nm_badan_usaha.$alamat.$nm_kecamatan.$nm_kota.$nm_provinsi.$nm_negara;
				$pil_alamat[$kd_alamat_kirim] = $alamat;
			endforeach;
			if ($no > 1) :
				$sel_alamat = $kd_alamat;
				$attr_alamat = array('id' => 'idSelAlamatKirim', 'class' => 'form-control select2');
				$form_alamat = form_dropdown('selAlamatKirim', $pil_alamat, $sel_alamat, $attr_alamat);
			else :
				$form_alamat = '';
				$form_alamat .= form_input(array('type' => 'hidden', 'name' => 'selAlamatKirim', 'id' => 'idSelAlamatKirim', 'value' => $kd_alamat_kirim));
				$form_alamat .= $alamat;
			endif;

			return $form_alamat;
		endif;
	}

	function create_no_so($tipe_customer = '') {
		// Dapatkan format no quotation
		if ($tipe_customer == 'Ekspor') :
			$code = 'salesorderekspor';
		elseif ($tipe_customer == 'Lokal' || empty($tipe_customer)) :
			$code = 'salesorderlocal';
			$tipe_customer = 'Lokal';
		endif;
		$no_salesorder = $this->tm_salesorder->create_nosalesorder($code, $tipe_customer);

		return $no_salesorder;
	}

	function create_pkey() {
		$conds = array(
			'select' => 'tgl_input, '.$this->p_key,
			'order_by' => 'tgl_input DESC',
			'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
		);
		$primary_key = $this->m_builder->buat_kode($this->tbl_name, $conds, 6, 'MSO');

		return $primary_key;
	}

	function upload_file($file_name) {
		$config['upload_path'] = './assets/admin_assets/dist/img/attachment_po/';
		$config['allowed_types'] = 'gif|jpg|png|pdf|doc|docx|xls|xlsx|';
		$config['max_size'] = 10240;
		$config['max_filename_increment'] = 50;
		$config['encrypt_name'] = TRUE;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload($file_name)) :
			$data = array('error' => $this->upload->display_errors('', ''));
		else :
			$data = array('file_upload' => $this->upload->data());
		endif;

		return $data;
	}

	function compress_img($img_name) {
		$config['image_library'] = 'gd2';
		$config['source_image'] = './assets/admin_assets/dist/img/attachment_po/'.$img_name;
		$config['maintain_ratio'] = TRUE;
		$config['width'] = 600;

		$this->load->library('image_lib', $config);

		$this->image_lib->resize();
	}

	function submit_form() {
		if ($this->input->is_ajax_request()) :
			$upload = array();
			$file_user = $_FILES['fileAttach']['error'];
			if ($_FILES['fileAttach']['error'] != 4) :
				$upload = $this->upload_file('fileAttach');
				if (!empty($upload['file_upload'])) :
					$file_name = $upload['file_upload']['file_name'];
					if ($upload['file_upload']['is_image']) :
						$this->compress_img($file_name);
					endif;
				endif;
			else :
				$file_name = $this->input->post('txtFileLama');
			endif;

			$this->form_validation->set_rules('txtKdSales', 'Nama Salesperson', 'required');
			$this->form_validation->set_rules('txtNmSales', 'Nama Salesperson', 'required');
			$this->form_validation->set_rules('txtEmailSales', 'Email Salesperson', 'required');
			$this->form_validation->set_rules('txtMobilePhone', 'Telp Salesperson', 'required');
			$this->form_validation->set_rules('txtDate', 'Tgl SO', 'required');
			$this->form_validation->set_rules('txtNmCustomer', 'Nama Customer', 'required');
			$this->form_validation->set_rules('txtKdCustomer', 'Nama Customer', 'required');
			$this->form_validation->set_rules('selAlamatKirim', 'Alamat Kirim', 'required');
			if ($this->form_validation->run() == FALSE || !empty($upload['error'])) :
                $str['confirm'] = 'error';
                $str['idErrSales'] = (!empty(form_error('txtNmSales')))?buildLabel('warning', form_error('txtNmSales', '"', '"')):'';
                $str['idErrSales'] = (!empty(form_error('txtKdSales')))?buildLabel('warning', form_error('txtKdSales', '"', '"')):'';
                $str['idErrEmailSales'] = (!empty(form_error('txtEmailSales')))?buildLabel('warning', form_error('txtEmailSales', '"', '"')):'';
                $str['idErrMobilePhone'] = (!empty(form_error('txtMobilePhone')))?buildLabel('warning', form_error('txtMobilePhone', '"', '"')):'';
                $str['idErrDate'] = (!empty(form_error('txtDate')))?buildLabel('warning', form_error('txtDate', '"', '"')):'';
                $str['idErrNmCustomer'] = (!empty(form_error('txtNmCustomer')))?buildLabel('warning', form_error('txtNmCustomer', '"', '"')):'';
                $str['idErrNmCustomer'] = (!empty(form_error('txtKdCustomer')))?buildLabel('warning', form_error('txtKdCustomer', '"', '"')):'';
                $str['idErrAlamatKirim'] = (!empty(form_error('selAlamatKirim')))?buildLabel('warning', form_error('selAlamatKirim', '"', '"')):'';
				if (!empty($upload['error'])) :
					$error = $upload['error'];
                	$str['idErrFileAttach'] = buildLabel('warning', $error);
				endif;
			else :
				$kd_msalesorder = $this->input->post('txtKdSalesOrder');
				$mquotation_kd = $this->input->post('txtKdQuotation');
				$tgl_so = $this->input->post('txtDate');
				$act = empty($kd_msalesorder)?'input':'edit';
				$data['kd_msalesorder'] = empty($kd_msalesorder)?$this->create_pkey():$kd_msalesorder;
				$data['mquotation_kd'] = empty($mquotation_kd)?'-':$mquotation_kd;
				$data['no_salesorder'] = $this->input->post('txtNoSalesOrder');
				$data['order_tipe'] = $this->input->post('txtOrderTipe');
				$data['salesperson_kd'] = $this->input->post('txtKdSales');
				$data['email_sales'] = $this->input->post('txtEmailSales');
				$data['mobile_sales'] = $this->input->post('txtMobilePhone');
				$data['tgl_so'] = format_date($tgl_so, 'Y-m-d');
				$data['note_so'] = $this->input->post('txtNotes');
				$data['no_po'] = $this->input->post('txtNoPo');
				$data['tgl_po'] = format_date($this->input->post('txtTglPo'), 'Y-m-d');
				$data['file_attach_po'] = $file_name;
				$data['tipe_container'] = $this->input->post('txtTipeContainer');
				$data['container_20ft'] = $this->input->post('txtContainer20ft');
				$data['customer_kd'] = $this->input->post('txtKdCustomer');
				$data['alamat_kirim_kd'] = $this->input->post('selAlamatKirim');
				$data['teritory_id'] = $this->input->post('txtTeritory');
				if (empty($this->input->post('txtStatusSo'))) :
					$data['status_so'] = 'pending';
				else :
					$data['status_so'] = sales_status($this->input->post('txtStatusSo'));
				endif;

				$str = $this->send_data($act, $data);
				// jika variabel diatas mengembalikan nilai success dan status sekarang bukan pending atau cancel, maka sistem akan mulai mengubah data yang ada pada db disistem lama :)
				if ($str['confirm'] == 'success') :
					if ($data['status_so'] != 'pending' && 'cancel' && empty($data['order_tipe'])) :
						if ($str['tipe_customer'] == 'Ekspor') :
							$detail['fc_nopo'] = $data['no_po'];
						elseif ($str['tipe_customer'] == 'Lokal') :
							$detail['fc_nopo'] = $data['no_salesorder'];
						endif;
						$detail['fd_tglpo'] = $data['tgl_po'];
						$detail['fc_userid'] = $this->session->username;
						$detail['fv_customer'] = $this->input->post('txtNmCustomer');
						$str = $this->tm_po->submit_data($detail);
						$str['status_so'] = $data['status_so'];
						if ($str['confirm'] == 'error') :
							$del_data = array('kd_msalesorder' => $data['kd_msalesorder']);
							$del_detail = array('msalesorder_kd' => $data['kd_msalesorder']);
							$aksi['master'] = $this->db->delete('tm_salesorder', $del_data);
							$aksi['item'] = $this->db->delete('td_salesorder_item', $del_detail);
							$aksi['item_detail'] = $this->db->delete('td_salesorder_item_detail', $del_detail);
							$aksi['harga'] = $this->db->delete('td_salesorder_harga', $del_detail);
						endif;
					endif;
				endif;
				$str['upload'] = $upload;
				$str['id'] = $data['kd_msalesorder'];
				$str['order_tipe'] = $data['order_tipe'];
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function send_data($act, $data = array()) {
		if ($act == 'input') :
			$label_err = 'menambahkan';
			$jml_ppn = $this->model_jenis_customer->get_col($data['customer_kd'], 'set_ppn');
			if (!empty($jml_ppn)) :
				$jml_ppns = explode('.', $jml_ppn);
				$akhir_ppn = $jml_ppns[1] > 0?substr($jml_ppns[1], 0, 1):'';
				$pemisah = !empty($akhir_ppn)?'.':'';
				$set_ppn = $jml_ppns[0].$pemisah.$akhir_ppn;
			else :
				$set_ppn = '0';
			endif;
			if (empty($data['order_tipe'])) :
				$data['order_tipe'] = 'new';
			endif;
			$data['jml_ppn'] = $set_ppn;
			$data['nm_kolom_ppn'] = 'PPN '.$set_ppn.'%';
			$data['tipe_customer'] = $this->model_jenis_customer->get_tipe($data['customer_kd'], 'tipe_customer');
			if ($data['order_tipe'] != 'additional') :
				$data['no_salesorder'] = $this->create_no_so($data['tipe_customer']);
			endif;
			$data['tipe_harga'] = $this->model_jenis_customer->get_tipe($data['customer_kd'], 'tipe_harga');
			$act = $this->m_msalesorder->insert_data($data);
		elseif ($act == 'edit') :
			/** Jika proses sudah state LPO dan data di edit maka push ke SAP */
			if($data['status_so'] == "process_lpo"):
				$this->db->trans_begin();
				$dataAPI = [
					'U_IDU_WEBID' => $data['kd_msalesorder'],
					'KdSalesPerson' => $data['salesperson_kd'],
					'U_IDU_SO_INTNUM' => $data['no_salesorder'],
					'DocDate' => $data['tgl_so'],
					'DocDueDate' => $data['tgl_kirim'] == null ? $data['tgl_so'] : $data['tgl_kirim'], // Untuk inputan awal defaultnya disamakan dengan tgl SO, tgl kirim actual diubah melalui production order PPIC
					'Comments' => $data['note_so'] == null ? "" : $data['note_so'],
					'U_IDU_PO_INTNUM' => $data['no_po'] == null ? "" : $data['no_po'],
					'U_IDU_PO_INTDate' => $data['tgl_po'] == null ? "" : $data['tgl_po'],
					'CardCode' =>  $this->tm_customer->get_by_param(['kd_customer' => $data['customer_kd']])->row()->code_customer,
					'Street' => $data['alamat_kirim_kd'] == null || $data['alamat_kirim_kd'] == "" ? 
					$this->tm_customer->get_by_param(['kd_customer' => $data['customer_kd']])->row()->alamat : 
					$this->td_customer_alamat->get_by_param(['kd_alamat_kirim' => $data['alamat_kirim_kd']])->row()->alamat,
					'U_IDU_WEBUSER' => $this->session->username,
				];
				$where['kd_msalesorder'] = $data['kd_msalesorder'];
				$act = $this->m_msalesorder->update_data($data, $where);
				$api = parent::api_sap_post('EditSalesOrder', $dataAPI);
				if($this->db->trans_status() == FALSE || $api[0]->ErrorCode != 0 || $act == FALSE):
					$this->db->trans_rollback();
					$act = false;
				else:
					$this->db->trans_commit();
					$act = true;
				endif;
				$label_err = 'mengubah API['.$api[0]->Message.']';
				/** End push to SAP */
			else:
				$label_err = 'mengubah';
				$where['kd_msalesorder'] = $data['kd_msalesorder'];
				$act = $this->m_msalesorder->update_data($data, $where);
			endif;
		
		endif;

		if ($act) :
			$log_stat = 'berhasil';
			$str['id'] = $data['kd_msalesorder'];
			$str['no_salesorder'] = $data['no_salesorder'];
			$str['tipe_customer'] = $this->model_jenis_customer->get_tipe($data['customer_kd'], 'tipe_customer');
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
			$str['status_so'] = $data['status_so'];
			$this->write_log($log_stat, $label_err, $data);
		else :
			$log_stat = 'gagal';
			$str['confirm'] = 'error';
			$str['idErrForm'] = buildAlert('danger', 'Gagal!', 'Gagal '.$label_err.' '.$this->title.', kesalahan sistem!');
			$str['status_so'] = $data['status_so'];
			$this->write_log($log_stat, $label_err, $data);
		endif;

		return $str;
	}

	function form_select() {
		// Buat judul halaman dengan mengambil data dari URL yang didapatkan pada browser
		$page = $this->define_page(2);

		// Box property header_color/box_status, box id, box title, dan box_overlay 
		$data['box_id'] = 'idBoxFormSalesOrder';
		$data['box_status'] = 'info';
		$data['box_title'] = 'Form '.ucwords($page);
		$data['idbox_overlay'] = 'idBoxOverlayForm';
		$data['idbox_loader'] = 'idBoxLoaderForm';
		$data['idbox_content'] = 'idBoxContentForm';
		$data['tbl_alertid'] = 'idErrForm';

		// btn array btn_add, btn_hide, btn_close dengan menambahkan id kesetiap buttonnya
		$data['btns'] = array('btn_hide' => 'idBtnHideBoxForm', 'btn_close' => 'idBtnCloseBox');
		// Gunakan $data['box_body'] = 'box_body_url' untuk memanggil isi dari box
		$data['box_body'] = 'page/'.$this->class_link.'/form_pilih';
		// Variabel $data['form_data'] untuk mendapatakkan isi dari form
		$data['form_data'] = '';
		// Variabel $data['class_link'] untuk mendapatakkan uri dari controller
		$data['class_link'] = $this->class_link;
		$data['form_error'] = array('idErrNamaCustomer', 'idErrNoQuo');
		// Variabel $data['adds_js'] untuk mendapatakkan memanggil file js
		// $data['adds_js'] = array('form_js');
		// $data['js_data'] digunakan untuk penulisan form_error dan mendapatkan box_id
		// $data['js_data'] = array('form_error' => array('idErrProductCode', 'idErrLaporanFooter'), 'box_id' => 'idBoxFormat', 'class_link' => $this->class_link);

		$this->load->view('containers/box', $data);
	}

	function get_quo_dropdown() {
		$kd_customer = $this->input->get('kd_customer');
		$dropdown = '<option value="">-- Pilih No Quotation --</option>';
		$dropdown .= $this->m_master_quotation->get_quo_bycustomer($kd_customer);
		echo $dropdown;
	}

	function write_log($stat, $var, $data = array()){
		$nm_kolom = '';
		$no = 0;
		$jml = count($data);
		foreach ($data as $key => $val) :
			$no++;
			$koma = $no == $jml?'':',';
			$nm_kolom .= ' '.$key.' = '.$val.$koma;
		endforeach;
		$this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' '.$stat.' '.$var.' '.$this->title.' dengan'.$nm_kolom);
	}

	function get_add_quo_dropdown() {
		$data = '<div class="form-group">';
		$data .= '<label for="idSelQuo" class="col-md-2 control-label">No Quotation</label>';
		$data .= '<div class="col-md-3 col-xs-11">';
		$data .= '<div id="idErrNoQuo"></div>';
		$data .= '<select name="selQuo[]" id="idSelQuo" class="form-control select2">';
		$data .= '<option value="">-- Pilih No Quotation --</option>';;
		$data .= $this->m_master_quotation->get_quo_bycustomer($this->input->get('kd_customer'));
		$data .= '</select>';
		$data .= '</div>';
		$data .= '<div class="col-md-1 col-xs-1">';
		$data .= '<button type="button" class="btn btn-warning btn-flat btn-remove-add-quo" title="Hapus Quotation"><i class="fa fa-minus"></i></button>';
		$data .= '</div>';
		$data .= '</div>';
		echo $data;
	}

	function get_select_header_form() {
		$data['kd_mquotation'] = $this->input->get('kd_mquotation');
		$data['class_link'] = 'sales/sales_orders/sales_orders_form';
		$data['form_errs'] = array('idErrNm');
		$this->load->view('page/'.$data['class_link'].'/selheader_form_box', $data);
	}

	function open_select_header_form() {
		$kd_mquotation = json_decode($this->input->get('kd_mquotation'));
		/*
		** Disini kd_mquotation yang berupa array maupun tidak akan diperiksa dan didapatkan headernya.
		*/
		for ($i = 0; $i < count($kd_mquotation); $i++) :
			$dheader[] = $kd_mquotation[$i];
		endfor;
		$data['headers'] = $this->tm_quotation->get_header($dheader);
		$data['class_link'] = 'sales/sales_orders/sales_orders_form';
		$this->load->view('page/'.$data['class_link'].'/selheader_form_main', $data);
	}

	function send_header_data() {
		/*
		** Disini controller menerima data berupa array kd_mquotation yang nantinya akan digunakan untuk mendapatkan data list item dari detail quotation
		** sedangkan master SO akan menggunakan data header pilihan
		** array data kd_mquotation juga digunakan untuk mengubah status quotation yang terlibat
		*/
		if ($this->input->is_ajax_request()) :
			$arr_kdmaster = $this->input->post('txtKdMaster');
			$kd_header = $this->input->post('radioHeader');
			$r_master = $this->m_master_quotation->get_row($kd_header);
			if (!empty($r_master)) :
				$_SESSION['sales_order']['salesperson_kd'] = $r_master->salesperson_kd;
				$_SESSION['sales_order']['email_sales'] = $r_master->email_sales;
				$_SESSION['sales_order']['mobile_sales'] = $r_master->mobile_sales;
				$_SESSION['sales_order']['customer_kd'] = $r_master->customer_kd;
				$_SESSION['sales_order']['tipe_customer'] = $r_master->tipe_customer;
				$_SESSION['sales_order']['tipe_harga'] = $r_master->tipe_harga;
				$_SESSION['sales_order']['alamat_kirim_kd'] = $r_master->alamat_kirim_kd;
				$_SESSION['sales_order']['nm_kolom_ppn'] = $r_master->nm_kolom_ppn;
				$_SESSION['sales_order']['jml_ppn'] = $r_master->jml_ppn;
				$_SESSION['sales_order']['jml_ongkir'] = $r_master->jml_ongkir;
				$_SESSION['sales_order']['jml_install'] = $r_master->jml_install;

				/*
				** Setelah session berhasil dinisialisasi maka langkah selanjutnya adalah mengirim data array kd_mquotation kedalam form child
				** untuk dipilih data item mana yang akan dijadikan SO
				*/
				$str['kd_mquotation'] = '';
				for ($i = 0; $i < count($arr_kdmaster); $i++) :
					$penghubung = $i > 0?'-':'';
					$str['kd_mquotation'] .= $penghubung.$arr_kdmaster[$i];
				endfor;
				$_SESSION['sales_order']['kd_mquotation'] = $str['kd_mquotation'];
				$str['confirm'] = 'success';
			else :
				$str['confirm'] = 'error';
				$str['alert'] = buildAlert('danger', 'Gagal!', 'Gagal memilih header sales order, master quotation tidak ditemukan!');
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function get_select_item_form() {
		$data['kd_mquotation'] = $this->input->get('kd_mquotation');
		$data['class_link'] = 'sales/sales_orders/sales_orders_form';
		$data['form_errs'] = array('idErrNm');
		$this->load->view('page/'.$data['class_link'].'/selitem_form_box', $data);
	}

	function open_select_items_form() {
		$kd_mquotation = '';
		$kd_mquotations = $this->input->get('kd_mquotation');
		if (stripos($kd_mquotations, '-') !== FALSE) :
			$kd_masters = explode('-', $kd_mquotations);
			for ($i = 0; $i < count($kd_masters); $i++) :
				$kd_mquotation[] = $kd_masters[$i];
			endfor;
		else :
			$kd_mquotation = $kd_mquotations;
		endif;
		$kd_parents = array('');
		$data['item_parent'] = $this->m_quotation->quotation_item_list($kd_mquotation);
		if (!empty($data['item_parent'])) :
			foreach ($data['item_parent'] as $parent) :
				$kd_parents[] = $parent->kd_ditem_quotation;
			endforeach;
		endif;
		$data['item_child'] = $this->m_quotation->item_child($kd_parents);
		$data['class_link'] = 'sales/sales_orders/sales_orders_form';
		$data['form_errs'] = array('idErrNm');
		$this->load->view('page/'.$data['class_link'].'/selitem_form_main', $data);
	}

	function send_item_data() {
		if ($this->input->is_ajax_request()) :
			$data['kd_parent'] = $this->input->post('chkItemParent');
			$data['kd_child'] = $this->input->post('chkItemChild');
			$txt_kdparent = $this->input->post('txtKdParent');

			$str = $this->model_salesorder->send_item_data($data);
			$str['csrf'] = $this->security->get_csrf_hash();
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}