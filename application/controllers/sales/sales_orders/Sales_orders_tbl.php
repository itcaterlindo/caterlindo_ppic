<?php

use PhpOffice\PhpSpreadsheet\Writer\Ods\Thumbnails;

defined('BASEPATH') or exit('No direct script access allowed!');

class Sales_orders_tbl extends MY_Controller {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'tm_salesorder';
	private $p_key = 'kd_msalesorder';
	private $class_link = 'sales/sales_orders';
	private $title = 'Sales Orders';

	function __construct() {
		parent::__construct();
		
		$this->load->helper(array('my_btn_access_helper', 'my_helper'));
		$this->load->library(array('ssp'));
		$this->load->model(array('m_builder', 'tm_salesorder', 'model_salesorder', 'm_salesorder', 'db_caterlindo/tm_po', 'db_caterlindo/td_po', 'tb_tipe_harga', 'tm_currency', 'td_currency_convert', 'td_salesorder_item', 'td_salesorder_item_detail', 'tm_deliveryorder', 'td_product_grouping', 'm_msalesorder', 'tm_customer', 'td_customer_alamat', 'td_salesorder_item_disc', 'tm_item_group', 'tm_barang', 'td_salesorder_harga'));
	}

	function index() {
		parent::administrator();
		parent::icheck_assets();
		parent::datetimepicker_assets();
		parent::pnotify_assets();

		$this->box_container();
	}

	function box_container() {
		// Buat judul halaman dengan mengambil data dari URL yang didapatkan pada browser
		$page = $this->define_page(2);

		// Box property header_color/box_status, box id, box title, dan box_overlay 
		$data['box_id'] = 'idBoxSalesOrder';
		$data['box_status'] = 'primary';
		$data['box_title'] = 'Tabel '.ucwords($page);
		$data['idbox_overlay'] = 'idBoxOverlayTable';
		$data['idbox_loader'] = 'idBoxLoaderTable';
		$data['idbox_content'] = 'idBoxContentTable';

		// btn array btn_add, btn_hide, btn_close dengan menambahkan id kesetiap buttonnya
		$btn_add = 'btn_add_hide';
		if (cek_permission('SALESORDER_CREATE')) {
			$btn_add = 'btn_add';
		}
		$data['btns'] = array($btn_add => 'idBtnAddData', 'btn_hide' => 'idBtnHideBoxTable');
		// Gunakan $data['box_body'] = 'box_body_url' untuk memanggil isi dari box
		// $data['box_body'] = 'containers/table_ssp';
		// Variabel $data['form_data'] untuk mendapatakkan isi dari form
		// $data['form_data'] = $this->tbl_properties();
		// Variabel $data['class_link'] untuk mendapatakkan uri dari controller
		$data['class_link'] = $this->class_link;
		// Variabel $data['adds_js'] untuk mendapatakkan memanggil file js
		$data['adds_js'] = array('table_js');
		// $data['js_data'] digunakan untuk penulisan form_error dan mendapatkan box_id
		$data['js_data'] = array('class_link' => $this->class_link);
		$data['tbl_alertid'] = 'idTableSalesAlert';

		$this->load->view('containers/box', $data);
        $this->load->js('assets/admin_assets/plugins/typeahead.js/typeahead.bundle.min.js');
        $this->load->css('assets/admin_assets/plugins/typeahead.js/typeahead.css');
		$this->load->js('assets/admin_assets/plugins/moment.js/moment.min.js');
		$this->load->js('assets/admin_assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js');
		$this->load->css('assets/admin_assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css');
        $this->load->css('assets/admin_assets/plugins/select2/select2.min.css');
        $this->load->js('assets/admin_assets/plugins/input-mask/jquery.inputmask.js');
        $this->load->css('assets/admin_assets/plugins/select2/select2-bootstrap.min.css');
        $this->load->js('assets/admin_assets/plugins/select2/select2.full.min.js');
        $this->load->section('scriptJS', 'script/scriptJS', $data);
	}

	function get_table() {
		$data = $this->tbl_properties();
		$this->load->view('containers/table_ssp', $data);
	}

	function define_page($url_array_num) {
		$ttl = $this->uri->segment($url_array_num);
		if (strpos($ttl, '_') !== FALSE) :
			$rpl_title = str_replace('_', ' ', $ttl);
			$title = ucwords($rpl_title);
		else :
			$title = ucwords($this->uri->segment($url_array_num));
		endif;
		return $title;
	}

	function tbl_properties() {
		$data['tbl_id'] = 'idTableSalesOrders';
		$data['tbl_uri'] = $this->class_link.'/sales_orders_tbl/tbl_data';
		// $data['col_def'] = array('searchable' => 'TRUE/FALSE', 'orderable' => 'TRUE/FALSE', 'target' => 'column_number');
		$data['tbl_order'] = '[[7, "desc"], [2, "desc"]]';
		$data['tbl_header'] = array(
			'<th style="width:3%; text-align:center;">No. Sales Order</th>',
			'<th style="width:3%; text-align:center;">No. PO</th>',
			'<th style="width:5%; text-align:center;">Customer</th>',
			'<th style="width:5%; text-align:center;">Tipe Customer</th>',
			'<th style="width:5%; text-align:center;">Salesperson</th>',
			'<th style="width:3%; text-align:center;">Tgl Sales Order</th>',
			'<th style="width:3%; text-align:center;" class="never">Tgl Kirim</th>',
			'<th style="width:1%; text-align:center;" class="always">Status</th>',
			'<th style="width:1%; text-align:center;" class="never">Order Tipe</th>',
			'<th style="width:1%; text-align:center;" class="never">Push SAP</th>',
		);

		return $data;
	}

	function tbl_data() {
		$data = $this->tm_salesorder->ssp_table_so();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	function ubah_status() {
		if ($this->input->is_ajax_request()) :
			$id = $this->input->get('id');
			$status = $this->input->get('status');
			/** Jika process_lpo maka dia insert ke SAP untuk transaksi ekspor */
			if($status == "finish" || $status == "process_lpo"):
				$this->db->trans_begin();
			endif;
			if ($status == 'pending') :
				$stat_quo = 'process_so';
			else :
				$stat_quo = $status;
			endif;
			
			$master_det = $this->m_msalesorder->read_masterdata($id);
			$kd_mquo = $master_det->mquotation_kd;
			$kd_msalesorder = $master_det->kd_msalesorder;
			$aksi['ubah_status_quo'] = $this->model_salesorder->ubah_status_quo($kd_mquo, $stat_quo);

			$data = array(
				'status_so' => $status,
			);
			$where = array('kd_msalesorder' => $kd_msalesorder);
			$aksi['so'] = $this->db->update($this->tbl_name, $data, $where);
			$label_err = 'mengubah status';
			$data = array('kd_msalesorder' => $kd_msalesorder.' menjadi', 'status_so' => $status);
			/** Khusus untuk ubah status ke finish */
			if($status == "finish"):
				if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					$stat = 'gagal';
					$str['alert'] = buildAlert('danger', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
				}else{
					$this->db->trans_commit();
					$stat = 'berhasil';
					$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
				}
			elseif ($status == "process_lpo"):
				/** Khusus untuk ubah status ke process_lpo ekspor */
				if($this->db->trans_status() === FALSE):
					$this->db->trans_rollback();
					$stat = 'gagal';
					$str['alert'] = buildAlert('danger', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem! transaksi');
				else:
					$api = $this->insert_push_to_sap($kd_msalesorder);
					if($api[0]->ErrorCode == 0):
						$this->db->trans_commit();
						$stat = 'berhasil';
						$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'! API '.$api[0]->Message);
						$str['data'] = $api[0]->Data;
					else:
						$this->db->trans_rollback();
						$stat = 'gagal';
						$str['alert'] = buildAlert('danger', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem! API '.$api[0]->Message);
						$str['data'] = $api[0]->Data;
					endif;
				endif;
			else:
			/** Selain finish */
				if ($aksi) :
					$stat = 'berhasil';
					$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
				else :
					$stat = 'gagal';
					$str['alert'] = buildAlert('danger', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
				endif;
			/** end Selain finish */
			endif;
			$this->write_log($stat, $label_err, $data);

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function delete_so() {
		if ($this->input->is_ajax_request()) :
			$id = $this->input->get('id');
			$master_det = $this->m_msalesorder->read_masterdata($id);
			$kd_mquo = $master_det->mquotation_kd;
			$aksi['ubah_status_quo'] = $this->model_salesorder->ubah_status_quo($kd_mquo, 'pending');
			$label_err = 'menghapus';
			$data = array('kd_msalesorder' => $id);
			$data_detail = array('msalesorder_kd' => $id);
			$data_disc = array('mso_kd' => $id);
			$aksi['master'] = $this->db->delete('tm_salesorder', $data);
			$aksi['item'] = $this->db->delete('td_salesorder_item', $data_detail);
			$aksi['item_detail'] = $this->db->delete('td_salesorder_item_detail', $data_detail);
			$aksi['harga'] = $this->db->delete('td_salesorder_harga', $data_detail);
			$aksi['disc_default'] = $this->db->delete('td_salesorder_item_disc', $data_disc);
			if ($aksi) :
				$stat = 'berhasil';
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.'!');
			else :
				$stat = 'gagal';
				$str['alert'] = buildAlert('danger', 'Gagal '.$label_err.', Kesalahan sistem!');
			endif;
			$this->write_log($stat, $label_err, $data);

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function write_log($stat, $var, $data = array()){
		$nm_kolom = '';
		$no = 0;
		$jml = count($data);
		foreach ($data as $key => $val) :
			$no++;
			$koma = $no == $jml?'':',';
			$nm_kolom .= ' '.$key.' = '.$val.$koma;
		endforeach;
		$this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' '.$stat.' '.$var.' '.$this->title.' dengan'.$nm_kolom);
	}

	function process_production() {
		if ($this->input->is_ajax_request()) :
			$kd_msalesorder = $this->input->get('id');
			$tipe = $this->input->get('tipe');
			$row_so = $this->model_salesorder->read_masterdata($kd_msalesorder);

			$no_salesorder = $this->tm_salesorder->get_nosalesorder($kd_msalesorder);
			$master_so = $this->tm_salesorder->get_master_so($no_salesorder);
			$data = $this->model_salesorder->read_masterdata($master_so);
			if ($row_so['status_so'] != 'cancel') :
				$tot_qty = $this->td_salesorder_item->count_from_noso($no_salesorder) + $this->td_salesorder_item_detail->count_from_noso($no_salesorder);
				if ($tipe == 'ekspor') :
					$master['fc_nopo'] = substr($row_so['no_po'], 0, 20);
				elseif ($tipe == 'lokal') :
					$master['fc_nopo'] = substr($row_so['no_salesorder'], 0, 20);
				endif;
				$master['fd_tglpo'] = format_date($row_so['tgl_po'], 'Y-m-d');
				$master['fn_totalqty'] = $tot_qty;
				$master['fv_customer'] = $row_so['nm_customer'];
				$act = $this->tm_po->submit_data($master);
				if ($act['confirm'] == 'success') :
					if ($tot_qty > 0) :
						$act_detail = $this->trans_items_old($kd_msalesorder, $no_salesorder, $master['fc_nopo'], 'process_lpo', $row_so['order_tipe']);
					else :
						$act_detail = $this->tm_po->delete_data($master['fc_nopo']);
						$act_detail['confirm'] = 'error';
						$act_detail['alert'] = buildAlert('danger', 'Gagal!', 'Mengubah data SO "'.$no_salesorder.'" ke Production Order, Total quantity Barang di SO = '.$tot_qty.'!');
						header('Content-Type: application/json');
						echo json_encode($act_detail);
					endif;
				else :
					header('Content-Type: application/json');
					echo json_encode($act);
				endif;
			else :
				$word = process_status($row_so['status_so']);
				$color = color_status($row_so['status_so']);
				$d = bg_label($word, $color);
				$act = $this->tm_po->report(0, 'No SO = '.$no_salesorder.' dengan status = '.$d.' tidak bisa diproses');
				header('Content-Type: application/json');
				echo json_encode($act);
			endif;
		endif;
	}

	function trans_items_old($kd_msalesorder = '', $no_salesorder = '', $no_master_sales = '', $status_so = 'process_lpo', $order_tipe = '') {
		if ($status_so != 'pending' || $status_so != 'cancel') :
			$tot_qty = $this->td_salesorder_item->count_from_noso($no_salesorder) + $this->td_salesorder_item_detail->count_from_noso($no_salesorder);
			$act = $this->tm_po->submit_data(array('fc_nopo' => $no_master_sales, 'fn_totalqty' => $tot_qty));
			if ($tot_qty > 0) :
				$item_master = $this->td_salesorder_item->get_item($no_salesorder);
				foreach ($item_master as $row_master) :
					if (!empty($row_master->kd_mgrouping)) :
						$kd_mgrouping[] = $row_master->kd_mgrouping;
						$stuffing_item_qty[$row_master->kd_mgrouping] = $row_master->item_qty;
					elseif (!isset($data['item_barcode'][$row_master->item_barcode])) :
						$data['item_barcode'][$row_master->item_barcode] = $row_master->item_barcode;
						$data['stuffing_item_qty'][$row_master->item_barcode] = $row_master->item_qty;
					elseif (isset($data['item_barcode'][$row_master->item_barcode]) && empty($row_master->kd_mgrouping)) :
						$data['stuffing_item_qty'][$row_master->item_barcode] = $data['stuffing_item_qty'][$row_master->item_barcode] + $row_master->item_qty;
					endif;
				endforeach;
				if (isset($kd_mgrouping)) :
					$item_group = $this->td_product_grouping->get_item($kd_mgrouping);
					foreach ($item_group as $group) :
						if (in_array($group->mgrouping_kd, $kd_mgrouping)) :
							if (isset($data['item_barcode'][$group->item_barcode])) :
								$data['stuffing_item_qty'][$group->item_barcode] = $data['stuffing_item_qty'][$group->item_barcode] + $stuffing_item_qty[$group->mgrouping_kd];
							else :
								$data['item_barcode'][$group->item_barcode] = $group->item_barcode;
								$data['stuffing_item_qty'][$group->item_barcode] = $stuffing_item_qty[$group->mgrouping_kd];
							endif;
						endif;
					endforeach;
				endif;
				$item_detail = $this->td_salesorder_item_detail->get_item($no_salesorder);
				foreach($item_detail as $row_detail) :
					if (!isset($data['item_barcode'][$row_detail->item_barcode])) :
						$data['item_barcode'][$row_detail->item_barcode] = $row_detail->item_barcode;
						$data['stuffing_item_qty'][$row_detail->item_barcode] = $row_detail->item_qty;
					else :
						$data['stuffing_item_qty'][$row_detail->item_barcode] = $data['stuffing_item_qty'][$row_detail->item_barcode] + $row_detail->item_qty;
					endif;
				endforeach;
				$act_detail = $this->td_po->submit_data($data, $no_master_sales);
				$act_detail['kd_msales'] = $kd_msalesorder;
				$act_detail['status'] = $status_so;
			else :
				$act_detail = $this->tm_po->delete_data($no_master_sales);
				$act_detail['confirm'] = 'error';
				$act_detail['alert'] = buildAlert('danger', 'Gagal!', 'Mengubah data SO "'.$no_salesorder.'" ke Production Order, Total quantity Barang di SO = '.$tot_qty.'!');
			endif;

			// Additional item to SAP ketika simpan sales order
			if($order_tipe == 'Additional' && $act_detail['confirm'] == 'success'):
				$this->db->trans_begin();
				$label = "Menambahkan additional item";
				$this->tm_salesorder->update_data(['order_tipe' => 'Additional'], ['kd_msalesorder' => $kd_msalesorder]);
				if($this->db->trans_status() === FALSE):
					$this->db->trans_rollback();
					$act_detail['confirm'] = 'error';
					$act_detail['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem additional transaction!');
					$act_detail['message'] = 'Error transaction !';
				else:
					$api = $this->additional_push_sap($kd_msalesorder);
					if($api[0]->ErrorCode == 0):
						$this->db->trans_commit();
						$act_detail['confirm'] = 'success';
						$act_detail['alert'] = buildAlert('success', 'Berhasil!', $label.'! API '.$api[0]->Message);
						$act_detail['data'] = $api[0]->Data;
					else:
						$this->db->trans_rollback();
						$act_detail['confirm'] = 'error';
						$act_detail['alert'] = buildAlert('danger', 'Gagal!', $label.' kesalahan sistem! API '.$api[0]->Message);
						$act_detail['data'] = $api[0]->Data;
						$act_detail['message'] = 'Error API '.$api[0]->Message;
					endif;
				endif;
			endif;
			// END Additional item to SAP
		endif;
		if ($this->input->is_ajax_request()) :
			header('Content-Type: application/json');
			echo json_encode($act_detail);
		else :
			return $act_detail;
		endif;
	}

	public function check_items() {
		if ($this->input->is_ajax_request()) :
			$this->db->trans_begin();
			/** Jika process_lpo maka dia insert ke SAP untuk transaksi lokal */
			$kd_msalesorder = $this->input->get('kd_msalesorder');
			$no_salesorder = $this->tm_salesorder->get_nosalesorder($kd_msalesorder);
			$master_so = $this->tm_salesorder->get_master_so($no_salesorder);
			$data = $this->model_salesorder->read_masterdata($master_so);
			$tot_qty = $this->td_salesorder_item->count_from_noso($no_salesorder) + $this->td_salesorder_item_detail->count_from_noso($no_salesorder);
			if ($tot_qty > 0) :
				/** Action Update SO */
				$actUpdate = $this->tm_salesorder->update_data(['status_so' => 'process_lpo'], ['kd_msalesorder' => $kd_msalesorder]);
				if($this->db->trans_status() === FALSE):
					$this->db->trans_rollback();
					$str['confirm'] = 'error';
					$str['kd_msalesorder'] = $kd_msalesorder;
					$str['alert'] = buildAlert('warning', 'Peringatan!', 'SO "'.$no_salesorder.'" tidak bisa diproses!');
				else:
					$api = $this->insert_push_to_sap($kd_msalesorder);
					if($api[0]->ErrorCode == 0):
						$this->db->trans_commit();
						$str['confirm'] = 'success';
						$str['kd_msalesorder'] = $kd_msalesorder;
						$str['alert'] = buildAlert('success', 'Passed!', 'SO "'.$no_salesorder.'" bisa diproses!, '.$api[0]->Message.'');	
						$str['data'] = $api[0]->Data;
					else:
						$this->db->trans_rollback();
						$str['confirm'] = 'error';
						$str['kd_msalesorder'] = $kd_msalesorder;
						$str['alert'] = buildAlert('warning', 'Peringatan!', 'SO "'.$no_salesorder.'" tidak bisa diproses!, '.$api[0]->Message.'');	
						$str['data'] = $api[0]->Data;
					endif;
				endif;
			else :
				$this->db->trans_rollback();
				$str['confirm'] = 'error';
				$str['alert'] = buildAlert('warning', 'Peringatan!', 'SO "'.$no_salesorder.'" tidak bisa diproses, Total quantity Barang di SO = "'.$tot_qty.'"!');
			endif;
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	/*
	** Fungsi ini digunakan untuk membatalkan salesorder
	** cara kerja fungsi ini adalah, pertama cek pada tabel delivery order,
	** jika ada data dan status so/do bukan finish maka data tsb akan dihapus
	** setelah itu data so tsb akan diubah statusnya menjadi cancel.
	** Fungsi ini ada di modul sales order dan ppic
	*/
	public function cancel_so($kd_msalesorder = '') {
		if ($this->input->is_ajax_request()) :
			$this->db->trans_begin();
			$master_det = $this->m_msalesorder->read_masterdata($kd_msalesorder);
			$kd_mquo = $master_det->mquotation_kd;
			$aksi['ubah_status_quo'] = $this->model_salesorder->ubah_status_quo($kd_mquo, 'pending');

			$no_so = $this->tm_salesorder->get_nosalesorder($kd_msalesorder);
			$stat_do = $this->tm_deliveryorder->chk_stat_so($kd_msalesorder, 'finish');
			$stat_so = $this->tm_salesorder->chk_stat_so($kd_msalesorder, 'finish');
			if (!$stat_do) :
				$del_do = $this->tm_deliveryorder->del_do_so($kd_msalesorder);
				if ($del_do['confirm'] == 'success') :
					$act = $this->tm_salesorder->ubah_status(array('kd_msalesorder' => $kd_msalesorder), array('status_so' => 'cancel'), $no_so);
					/** Action to SAP */
					$dataAPI = [
						'U_IDU_WEBID' => $kd_msalesorder
					];
					$api = parent::api_sap_post('CancelSalesOrder', $dataAPI);
					if ($this->db->trans_status() === FALSE || $api[0]->ErrorCode != 0) {
						$this->db->trans_rollback();					
						echo buildAlert('danger', 'Gagal!', 'Gagal cancel SO karena error, API '.$api[0]->Message);
					}else{
						$this->db->trans_commit();
						echo buildAlert('danger', 'Gagal!', 'Berhasil cancel SO, API '.$api[0]->Message);
					}
					/** End Action to SAP */
				else :
					echo buildAlert('danger', 'Gagal!', 'Gagal mengubah status No Salesorder = '.$no_so.', gagal hapus delivery order!');
				endif;
			else :
				echo buildAlert('danger', 'Gagal!', 'Gagal mengubah status No Salesorder = '.$no_so.', dikarenakan status delivery order = "finish/cancel"!');
			endif; 
		endif;
	}

	/** Function untuk push SAP dari button push to SAP di salesorder */
	function submit_to_sap(){
		if ($this->input->is_ajax_request()) :
			$this->db->trans_begin();
			$kd = $this->input->post('kd_msalesorder');
			$api = $this->insert_push_to_sap($kd);
			if($api[0]->ErrorCode == 0):
				$this->db->trans_commit();
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan, API['.$api[0]->Message.']', 'data' => $api[0]->Data);
			else:
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan, API['.$api[0]->Message.']', 'data' => $api[0]->Data);
			endif;
			header('Content-Type: application/json');
			echo json_encode($resp);
		endif;
	}

	/** Function Additional Item to SAP */
	function additional_push_sap($kd_msalesorder)
	{
		/** Action edit to SAP */
		$so['so_master'] 		= $this->tm_salesorder->get_row($kd_msalesorder);
		$so['so_detail'] 		= $this->td_salesorder_item->get_items($kd_msalesorder);
		$so['so_sub_detail'] 	= $this->td_salesorder_item_detail->get_items($kd_msalesorder);
		/** Variabel doc total untuk menampung total nilai dari transaksi */
		$docTotal = 0;
		
		$detail = [];
		$sub_detail = [];
		$kdCurrency = "";

		/** Menampung transaksi spesial discount (td_salesorder_harga) */
		/** Di inputan transaksi namanya "special discount" */
		$getSpecialDisc = $this->td_salesorder_harga->get_by_param(['msalesorder_kd' => $kd_msalesorder])->result_array();
		$specialDisc = 0;
		if( !empty($getSpecialDisc) ):
			$specialDisc = array_sum( array_column($getSpecialDisc, "total_nilai") );
		endif;

		/** Jika tipe SO ekspor maka currency yang dikirim ke SAP adalah dollar jika lokal maka rupiah */
		if( $so['so_master']->tipe_customer == "Lokal" ):
			/** IDR */
			$kdCurrency = "MCN271017001";
		else:
			/** USD */
			$kdCurrency = "MCN271017002";
		endif;

		if(!empty($so['so_detail'])):
			foreach($so['so_detail'] as $row):
				$harga_satuan = 0;
				$total_harga = 0;
				// Discount Distributor (Untuk ekspor)
				$discDistributor = $this->td_salesorder_item_disc->get_where([
					'so_item_kd' => $row->kd_ditem_so,
					'mso_kd' => $so['so_master']->kd_msalesorder,
					'nm_pihak' => "Distributor"
				])->jml_disc;
				if(empty($discDistributor)):
					$harga_satuan = $row->harga_barang;
					$total_harga = $row->total_harga;
				else:
					$harga_satuan = ROUND($row->harga_barang - $row->harga_barang * $discDistributor / 100, 2);
					$total_harga =  $harga_satuan * $row->item_qty;
				endif;
				/** Jika status barang belum push ke SAP, masukkan variabel untuk push ke SAP */
				if($row->push_sap == "T"):
					$detail[] = [
						'U_IDU_WEBID' => $row->kd_ditem_so,
						'ItemCode' => $row->item_code,
						'Dscription' => $row->item_desc,
						'KdWarehouse' => $this->tm_item_group->get_by_param([
							'item_group_kd' => $this->tm_barang->get_item(['kd_barang' => $row->barang_kd])->item_group_kd ])
							->row()->gudang_kd, 
						'KdCurrency' => $kdCurrency,
						'Quantity' => $row->item_qty,
						'KdSatuan' => "MRS000006", // Hardcode UOM dari td_rawmaterial_satuan
						'UnitPrice' => $harga_satuan,
						'DiscType' => $row->disc_type,
						'ItemDisc' => 0,
						'DiscPrcnt' => 0,
						'LineTotal' => $total_harga,
						'U_IDU_FreeText' => $row->item_note == null ? "" : $row->item_note,
						'U_IDU_OrderType' => $row->order_tipe == null ? "" : $row->order_tipe,
					];
					$act = $this->td_salesorder_item->update_data($row->kd_ditem_so, ['push_sap' => 'Y']);
					$docTotal += $total_harga;
				else:
					$docTotal += $total_harga;
				endif;
			endforeach;
		endif;
		if(!empty($so['so_sub_detail'])):
			foreach($so['so_sub_detail'] as $row):
				$harga_satuan = 0;
				$total_harga = 0;
				// Discount Distributor (Untuk ekspor)
				$discDistributor = $this->td_salesorder_item_disc->get_where([
					'so_item_kd' => $row->kd_citem_so,
					'mso_kd' => $so['so_master']->kd_msalesorder,
					'nm_pihak' => "Distributor"
				])->jml_disc;
				if(empty($discDistributor)):
					$harga_satuan = $row->harga_barang;
					$total_harga = $row->total_harga;
				else:
					$harga_satuan = ROUND($row->harga_barang - $row->harga_barang * $discDistributor / 100, 2);
					$total_harga =  $harga_satuan * $row->item_qty;
				endif;
				/** Jika status barang belum push ke SAP, masukkan variabel untuk push ke SAP */
				if($row->push_sap == "T"):
					$sub_detail[] = [
						'U_IDU_WEBID' => $row->kd_citem_so,
						'ItemCode' => $row->item_code,
						'Dscription' => $row->item_desc,
						'KdWarehouse' => $this->tm_item_group->get_by_param([
							'item_group_kd' => $this->tm_barang->get_item(['kd_barang' => $row->kd_child])->item_group_kd ])
							->row()->gudang_kd, 
						'KdCurrency' => $kdCurrency,
						'Quantity' => $row->item_qty,
						'KdSatuan' => "MRS000006", // Hardcode UOM dari td_rawmaterial_satuan
						'UnitPrice' => $harga_satuan,
						'DiscType' => $row->disc_type,
						'ItemDisc' => 0,
						'DiscPrcnt' => 0,
						'LineTotal' => $total_harga,
						'U_IDU_FreeText' => $row->item_note == null ? "" : $row->item_note,
						'U_IDU_OrderType' => $row->order_tipe == null ? "" : $row->order_tipe,
					];
					$act = $this->td_salesorder_item_detail->update_data($row->kd_citem_so, ['push_sap' => 'Y']);
					$docTotal += $total_harga;
				else:
					$docTotal += $total_harga;
				endif;
			endforeach;
		endif;
		$dataAPI = [
			'U_IDU_WEBID' => $so['so_master']->kd_msalesorder,
			'KdSalesPerson' => $so['so_master']->salesperson_kd,
			'U_IDU_SO_INTNUM' => $so['so_master']->no_salesorder,
			'DocDate' => $so['so_master']->tgl_so,
			'DocDueDate' => $so['so_master']->tgl_kirim == null ? $so['so_master']->tgl_so : $so['so_master']->tgl_kirim, // Untuk inputan awal defaultnya disamakan dengan tgl SO, tgl kirim actual diubah mealui production order PPIC
			'NumAtCard' => $so['so_master']->tipe_customer == "Lokal" ? $so['so_master']->no_salesorder : $so['so_master']->no_po, // Lokal push no_salesorder, Ekspor push no_po
			'Comments' => $so['so_master']->note_so == null ? "" : $so['so_master']->note_so,
			'U_IDU_PO_INTNUM' => $so['so_master']->no_po == null ? "" : $so['so_master']->no_po,
			'U_IDU_PO_INTDate' => $so['so_master']->tgl_po == null ? "" : $so['so_master']->tgl_po,
			'CardCode' =>  $this->tm_customer->get_by_param(['kd_customer' => $so['so_master']->customer_kd ])->row()->code_customer,
			'Street' => $so['so_master']->alamat_kirim_kd == null || $so['so_master']->alamat_kirim_kd == "" ? 
			$this->tm_customer->get_by_param(['kd_customer' => $so['so_master']->customer_kd])->row()->alamat : 
			$this->td_customer_alamat->get_by_param(['kd_alamat_kirim' => $so['so_master']->alamat_kirim_kd])->row()->alamat,
			'OngkosKirim' => $so['so_master']->jml_ongkir == null ? 0 : $so['so_master']->jml_ongkir,
			'BiayaInstall' => $so['so_master']->jml_install == null ? 0 : $so['so_master']->jml_install,
			'DiscPrcnt' => 0,
			'DocTotal' => ($docTotal - $specialDisc) + ( ($docTotal - $specialDisc) * (int)$so['so_master']->jml_ppn / 100 ), 
			'U_IDU_WEBUSER' => $this->session->username,
			'Lines_Detail_Item' => $detail,
			'Lines_Sub_Detail_Item' => $sub_detail
		];

		/** Check jika ada field yang kosong */
		if($dataAPI['NumAtCard'] == "" || $dataAPI['NumAtCard'] == null){
			/** Respon API disamakan dengan resp API utama SAP (Function ada di class MY_CONTROLLER->api_sap_post) */
			$data_resp[0] = [
				'ErrorCode' => 500,
				'Message' => "Gagal, Data NumAtCard / No. sales order atau No. PO tidak boleh kosong !!",
				'Data' => null
			];
			$resp_array = json_decode(json_encode($data_resp));
			$api = $resp_array;
		}else{
			$api = parent::api_sap_post("EditSalesOrder", $dataAPI);
		}
		
		return $api;
		/** End action edit to SAP */
	}

	function insert_push_to_sap($kd_msalesorder)
	{
		/** Action insert to SAP */
		$so['so_master'] 		= $this->tm_salesorder->get_row($kd_msalesorder);
		$so['so_detail'] 		= $this->td_salesorder_item->get_items($kd_msalesorder);
		$so['so_sub_detail'] 	= $this->td_salesorder_item_detail->get_items($kd_msalesorder);
		$detail = [];
		$sub_detail = [];
		/** Variabel doc total untuk menampung total nilai dari transaksi */
		$docTotal = 0;

		/** Menampung transaksi spesial discount (td_salesorder_harga) */
		/** Di inputan transaksi namanya "special discount" */
		$getSpecialDisc = $this->td_salesorder_harga->get_by_param(['msalesorder_kd' => $kd_msalesorder])->result_array();
		$specialDisc = 0;
		if( !empty($getSpecialDisc) ):
			$specialDisc = array_sum( array_column($getSpecialDisc, "total_nilai") );
		endif;

		/** Jika tipe SO ekspor maka currency yang dikirim ke SAP adalah dollar jika lokal maka rupiah */
		if( $so['so_master']->tipe_customer == "Lokal" ):
			/** IDR */
			$kdCurrency = "MCN271017001";
		else:
			/** USD */
			$kdCurrency = "MCN271017002";
		endif;
						
		if(!empty($so['so_detail'])):
			foreach($so['so_detail'] as $row):
				$harga_satuan = 0;
				$total_harga = 0;
				// Discount Distributor (Untuk ekspor)
				$discDistributor = $this->td_salesorder_item_disc->get_where([
					'so_item_kd' => $row->kd_ditem_so,
					'mso_kd' => $so['so_master']->kd_msalesorder,
					'nm_pihak' => "Distributor"
				])->jml_disc;
				if(empty($discDistributor)):
					$harga_satuan = $row->harga_barang;
					$total_harga = $row->total_harga;
				else:
					$harga_satuan = ROUND($row->harga_barang - $row->harga_barang * $discDistributor / 100, 2);
					$total_harga =  $harga_satuan * $row->item_qty;
				endif;

				$detail[] = [
					'U_IDU_WEBID' => $row->kd_ditem_so,
					'ItemCode' => $row->item_code,
					'Dscription' => $row->item_desc,
					'KdWarehouse' => $this->tm_item_group->get_by_param([
						'item_group_kd' => $this->tm_barang->get_item(['kd_barang' => $row->barang_kd])->item_group_kd ])
						->row()->gudang_kd, 
					'KdCurrency' => $kdCurrency,
					'Quantity' => $row->item_qty,
					'KdSatuan' => "MRS000006", // Hardcode UOM dari td_rawmaterial_satuan
					'UnitPrice' => $harga_satuan,
					'DiscType' => $row->disc_type,
					'ItemDisc' => 0,
					'DiscPrcnt' => 0,
					'LineTotal' => $total_harga,
					'U_IDU_FreeText' => $row->item_note == null ? "" : $row->item_note,
					'U_IDU_OrderType' => $row->order_tipe == null ? "" : $row->order_tipe,
				];
				$act = $this->td_salesorder_item->update_data($row->kd_ditem_so, ['push_sap' => 'Y']);
				$docTotal += $total_harga;
			endforeach;
		endif;
		if(!empty($so['so_sub_detail'])):
			foreach($so['so_sub_detail'] as $row):
				$harga_satuan = 0;
				$total_harga = 0;
				// Discount Distributor (Untuk ekspor)
				$discDistributor = $this->td_salesorder_item_disc->get_where([
					'so_item_kd' => $row->kd_citem_so,
					'mso_kd' => $so['so_master']->kd_msalesorder,
					'nm_pihak' => "Distributor"
				])->jml_disc;
				if(empty($discDistributor)):
					$harga_satuan = $row->harga_barang;
					$total_harga = $row->total_harga;
				else:
					$harga_satuan = ROUND($row->harga_barang - $row->harga_barang * $discDistributor / 100, 2);
					$total_harga =  $harga_satuan * $row->item_qty;				
				endif;

				$sub_detail[] = [
					'U_IDU_WEBID' => $row->kd_citem_so,
					'ItemCode' => $row->item_code,
					'Dscription' => $row->item_desc,
					'KdWarehouse' => $this->tm_item_group->get_by_param([
						'item_group_kd' => $this->tm_barang->get_item(['kd_barang' => $row->kd_child])->item_group_kd ])
						->row()->gudang_kd, 
					'KdCurrency' => $kdCurrency,
					'Quantity' => $row->item_qty,
					'KdSatuan' => "MRS000006", // Hardcode UOM dari td_rawmaterial_satuan
					'UnitPrice' => $harga_satuan,
					'DiscType' => $row->disc_type,
					'ItemDisc' => 0,
					'DiscPrcnt' => 0,
					'LineTotal' => $total_harga,
					'U_IDU_FreeText' => $row->item_note == null ? "" : $row->item_note,
					'U_IDU_OrderType' => $row->order_tipe == null ? "" : $row->order_tipe,
				];
				$act = $this->td_salesorder_item_detail->update_data($row->kd_citem_so, ['push_sap' => 'Y']);
				$docTotal += $total_harga;
			endforeach;
		endif;
		$dataAPI = [
			'U_IDU_WEBID' => $so['so_master']->kd_msalesorder,
			'KdSalesPerson' => $so['so_master']->salesperson_kd,
			'U_IDU_SO_INTNUM' => $so['so_master']->no_salesorder,
			'DocDate' => $so['so_master']->tgl_so,
			'DocDueDate' => $so['so_master']->tgl_kirim == null ? $so['so_master']->tgl_so : $so['so_master']->tgl_kirim, // Untuk inputan awal defaultnya disamakan dengan tgl SO, tgl kirim actual diubah mealui production order PPIC
			'NumAtCard' => $so['so_master']->tipe_customer == "Lokal" ? $so['so_master']->no_salesorder : $so['so_master']->no_po, // Lokal push no_salesorder, Ekspor push no_po
			'Comments' => $so['so_master']->note_so == null ? "" : $so['so_master']->note_so,
			'U_IDU_PO_INTNUM' => $so['so_master']->no_po == null ? "" : $so['so_master']->no_po,
			'U_IDU_PO_INTDate' => $so['so_master']->tgl_po == null ? "" : $so['so_master']->tgl_po,
			'CardCode' =>  $this->tm_customer->get_by_param(['kd_customer' => $so['so_master']->customer_kd ])->row()->code_customer,
			'Street' => $so['so_master']->alamat_kirim_kd == null || $so['so_master']->alamat_kirim_kd == "" ? 
			$this->tm_customer->get_by_param(['kd_customer' => $so['so_master']->customer_kd])->row()->alamat : 
			$this->td_customer_alamat->get_by_param(['kd_alamat_kirim' => $so['so_master']->alamat_kirim_kd])->row()->alamat,
			'OngkosKirim' => $so['so_master']->jml_ongkir == null ? 0 : $so['so_master']->jml_ongkir,
			'BiayaInstall' => $so['so_master']->jml_install == null ? 0 : $so['so_master']->jml_install,
			'DiscPrcnt' => 0,
			'DocTotal' => ($docTotal - $specialDisc) + ( ($docTotal - $specialDisc) * (int)$so['so_master']->jml_ppn / 100 ), 
			'U_IDU_WEBUSER' => $this->session->username,
			'Lines_Detail_Item' => $detail,
			'Lines_Sub_Detail_Item' => $sub_detail
		];
		
		/** Check jika ada field yang kosong */
		if($dataAPI['NumAtCard'] == "" || $dataAPI['NumAtCard'] == null){
			/** Respon API disamakan dengan resp API utama SAP (Function ada di class MY_CONTROLLER->api_sap_post) */
			$data_resp[0] = [
				'ErrorCode' => 500,
				'Message' => "Gagal, Data NumAtCard / No. sales order atau No. PO tidak boleh kosong !!",
				'Data' => null
			];
			$resp_array = json_decode(json_encode($data_resp));
			$api = $resp_array;
		}else{
			$act = $this->tm_salesorder->update_data(['push_sap' => 'Y'], ['kd_msalesorder' => $kd_msalesorder]);
			/** End action insert to SAP */
			$api = parent::api_sap_post("AddSalesOrder", $dataAPI);
		}

		return $api;

	}

}
