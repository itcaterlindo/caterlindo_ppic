<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Set_notify extends MY_Controller {
	private $class_link = 'sales/set_notify';
	private $form_errs = array('idErrNama', 'idErrAlamat', 'idErrKodePos');

	public function __construct() {
		parent::__construct();

		$this->load->library(array('ssp', 'form_validation'));
		$this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
		$this->load->model(array('m_builder', 'tb_notifyparty', 'lokasi/tb_negara', 'lokasi/tb_provinsi', 'lokasi/tb_kota', 'lokasi/tb_kecamatan'));
	}

	public function index() {
		parent::administrator();
		parent::select2_assets();
		$this->get_table();
	}

	public function get_table() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function open_table() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		$data = $this->tb_notifyparty->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function get_form() {
		$id = $this->input->get('id');
		$data = $this->tb_notifyparty->get_row($id);
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = $this->form_errs;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function open_form() {
		$data['class_link'] = $this->class_link;
		$data = $this->tb_notifyparty->get_row($this->input->get('id'));
		$data['data_negara'] = $this->tb_negara->get_all_dropdown();
		$data['data_provinsi'] = $this->tb_provinsi->get_all_dropdown($data['negara_kd']);
		$data['data_kota'] = $this->tb_kota->get_all_dropdown($data['negara_kd'], $data['provinsi_kd']);
		$data['data_kecamatan'] = $this->tb_kecamatan->get_all_dropdown($data['negara_kd'], $data['provinsi_kd'], $data['kota_kd']);
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function send_data() {
		if ($this->input->is_ajax_request()) :
			$this->form_validation->set_rules($this->tb_notifyparty->form_rules());
			if ($this->form_validation->run() == FALSE) :
				$str = $this->tb_notifyparty->build_warning($this->form_errs);
				$str['confirm'] = 'error';
			else :
				$data['kd_notifyparty'] = $this->input->post('txtKd');
				$data['nm_notifyparty'] = $this->input->post('txtNama');
				$data['alamat'] = $this->input->post('txtAlamat');
				$data['kecamatan_kd'] = $this->input->post('selKecamatan');
				$data['kota_kd'] = $this->input->post('selKota');
				$data['provinsi_kd'] = $this->input->post('selProvinsi');
				$data['negara_kd'] = $this->input->post('selNegara');
				$data['kode_pos'] = $this->input->post('txtKodePos');
				$str = $this->tb_notifyparty->submit_data($data);
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function delete_data() {
		if ($this->input->is_ajax_request()) :
			$id = $this->input->get('id');
			$str = $this->tb_notifyparty->delete_data($id);
			
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}