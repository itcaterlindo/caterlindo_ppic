<?php
defined('BASEPATH') or exit('No direct script access allowed!');

require_once APPPATH."third_party/PhpSpreadsheets/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Generate_packvol extends MY_Controller {
	private $class_link = 'sales/generate_packvol';

	public function __construct() {
		parent::__construct();
		$this->load->model(['m_quotation', 'model_quotation', 'tm_salesorder', 'td_salesorder_item']);
		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
    }

    public function index() {
        parent::administrator();
        parent::select2_assets();
        $this->form_box();
    }
	 
	public function form_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function get_value_PO()
	{
		$search = $this->input->post('search');
		// Untuk get di select2 menggunakan ajax
		$param = [
			'tipe_customer' => 'Ekspor',
			'no_po LIKE' => '%'.$search.'%'
		];
		$list = array();
		$data = $this->tm_salesorder->get_all_where($param);
		if(count($data) > 0):
			$key=0;
			foreach($data as $key => $value):
				$list[$key]['id'] = $value->no_po;
				$list[$key]['text'] = $value->no_po; 
			$key++;
			endforeach;
		else:
			$list = null;
		endif;
		echo json_encode($list);
	}

	public function generateExcel () {

		$no_po = count($this->input->post('no_po')) > 0 ? $this->input->post('no_po') : null;
		$res = [];
		$data = $this->td_salesorder_item->get_where_in_po($no_po);
		if( count($data) > 0 ):
			$spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setCellValue('A1', 'item_qty');
			$sheet->setCellValue('B1', 'length_cm');
			$sheet->setCellValue('C1', 'width_cm');
			$sheet->setCellValue('D1', 'height_cm');
			$sheet->setCellValue('E1', 'item_code'); 
			$sheet->setCellValue('F1', 'grossweight');

			$noROW = 2;
			foreach($data as $po):
				$sheet->setCellValue('A'.$noROW, $po->item_qty);
				$sheet->setCellValue('B'.$noROW, $po->length_cm);
				$sheet->setCellValue('C'.$noROW, $po->width_cm);
				$sheet->setCellValue('D'.$noROW, $po->height_cm);
				$sheet->setCellValue('E'.$noROW, $po->item_code);
				$sheet->setCellValue('F'.$noROW, $po->grossweight);
				$noROW++;
			endforeach;

			$judul = "generate_for_packvol";
			$writer = new Xlsx($spreadsheet);
			$writer->save('assets/admin_assets/dist/laporan/'.$judul.'.xlsx');
			$url = base_url()."assets/admin_assets/dist/laporan/".$judul.".xlsx";
			$res['status'] = "OK";
			$res['url'] = $url;
		else:
			$res['status'] = "Error";
		endif;
		echo json_encode($res);
    }
}