<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Salesreportekspor_comparesimplypret extends MY_Controller
{
	private $class_link = 'sales/sales_report/salesreportekspor_comparesimplypret';

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(array('model_salesorder', 'm_salesperson', 'tb_default_disc', 'db_hrm/tb_hari_libur', 'db_hrm/tb_hari_efektif', 'model_do', 'tm_deliveryorder', 'td_salesorder_item_disc', 'tm_salesorder', 'm_customer', 'tb_teritory', 'tb_tipe_teritory'));
	}

	public function index()
	{
		parent::administrator();
		parent::chartjs_assets();
		$this->table_box();
	}

	public function table_box()
	{

		// Tahun ambil dari database
		$tahunInit = $this->db->select('MIN(YEAR(tgl_so)) as min_tahun, MAX(YEAR(tgl_so)) as max_tahun')
			->from('tm_salesorder')
			->get()->row_array();
		$selisihTahun = $tahunInit['max_tahun'] - $tahunInit['min_tahun'];
		$data['tahun'][] = $tahunInit['min_tahun'];
		for ($i = 0; $i < $selisihTahun; $i++) {
			$data['tahun'][] = $data['tahun'][$i] + 1;
		}
		$data['tipe_penjualan'] = 'Ekspor';
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/table_box', $data);
	}

	public function table_main()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$tahunAwal = $this->input->get('tahunawal', true);
		$tahun = $tahunAwal;
		$tahunAkhir = $this->input->get('tahunakhir', true);
		$tipeCustomer = $this->input->get('tipe_customer', true);
		$selisihTahun = $tahunAkhir - $tahunAwal;
		$data = [];
		for ($iTahun = 0; $iTahun <= $selisihTahun; $iTahun++) :
			// Perulangan untuk recap per tahun tari tahun awal sampai akhir
			$recapInv = [];
			// Get all teritory untuk di bandingkan dengan hasil group by
			$tipeTeritory = $this->tb_tipe_teritory->get_all();
			for ($bulan = 1; $bulan <= 12; $bulan++) :
				// Get RAW Invoice
				$inv = $this->model_salesorder->report_sales_ekspor_invoice($tahunAwal, $bulan, $tipeCustomer);
				if (empty($inv)) {
					// break;
				} else {
					// Group BY Teritory
					$groupByTipeTeritory = $this->group_by("tipe_teritory", $inv);
					$sumTipeTeritory = [];
					// Perbandingan teritory dengan group by
					$i = 0;
					foreach ($tipeTeritory as $ter) :
						if (array_key_exists($ter->kd_tipe_teritory, $groupByTipeTeritory)) :
							$sumTipeTeritory[$i]['tipe_teritory'] = $ter->kd_tipe_teritory;
							$sumTipeTeritory[$i]['ammount_distributor'] = ROUND(array_sum(array_column($groupByTipeTeritory[$ter->kd_tipe_teritory], "ammount_distributor")), 2);
							$sumTipeTeritory[$i]['container_20ft'] = array_sum(array_column($groupByTipeTeritory[$ter->kd_tipe_teritory], "container_20ft"));
						else :
							$sumTipeTeritory[$i]['tipe_teritory'] = $ter->kd_tipe_teritory;
							$sumTipeTeritory[$i]['ammount_distributor'] = 0;
							$sumTipeTeritory[$i]['container_20ft'] = 0;
						endif;
						$i++;
					endforeach;
					$recapInv[$bulan] = $sumTipeTeritory;
				}
			endfor;
			// RECAP PER BULAN
			$arrBulan = [
				1 => 'januari',
				2 => 'februari',
				3 => 'maret',
				4 => 'april',
				5 => 'mei',
				6 => 'juni',
				7 => 'juli',
				8 => 'agustus',
				9 => 'september',
				10 => 'oktober',
				11 => 'november',
				12 => 'desember',
			];
			$resultTipeTeritory = [];
			$iHeader = 0;
			foreach ($tipeTeritory as $ter) :
				$resultTipeTeritory[$iHeader]['tipe_teritory'] = $ter->kd_tipe_teritory;
				$resultTipeTeritory[$iHeader]['total']['ammount_distributor'] = 0;
				$resultTipeTeritory[$iHeader]['total']['container_20ft'] = 0;
				for ($i = 1; $i <= 12; $i++) :
					// Cek jika data tidak ada maka perulangan for untuk get data child berhenti
					if (!isset($recapInv[$i])) :
						// break;
						$recapInv[$i] = null;
					endif;
					foreach ($recapInv[$i] as $recap) :
						if ($recap['tipe_teritory'] == $ter->kd_tipe_teritory) :
							$resultTipeTeritory[$iHeader][$arrBulan[$i]]['ammount_distributor'] = $recap['ammount_distributor'];
							$resultTipeTeritory[$iHeader]['total']['ammount_distributor'] += $recap['ammount_distributor'];
							$resultTipeTeritory[$iHeader][$arrBulan[$i]]['container_20ft'] = $recap['container_20ft'];
							$resultTipeTeritory[$iHeader]['total']['container_20ft'] += $recap['container_20ft'];
						endif;
					endforeach;
				endfor;
				$iHeader++;
			endforeach; //echo json_encode($resultTipeTeritory);exit();
			// END RECAP PER BULAN
			$data['resultTipeTeritory'][$iTahun] = $resultTipeTeritory;
			$tahunAwal++;
			$this->db->reset_query();
		// End perulangan untuk recap per tahun tari tahun awal sampai akhir
		endfor;
		$data['class_link'] = $this->class_link;
		$data['tahunAmmount'] = $tahun;
		$data['tahun20ft'] = $tahun;
		$this->load->view('page/' . $this->class_link . '/table_main', $data);
		// echo json_encode($data['resultTipeTeritory']);
		// Output struktur arraynyaj
		// "resultTipeTeritory": {
		// 	"2022": [{
		// 		"tipe_teritory": "simply",
		// 		"januari": {
		// 			"ammount_distributor": 274201.3,
		// 			"container_20ft": 12
		// 		},
		// 		"februari": {
		// 			"ammount_distributor": 340818.79,
		// 			"container_20ft": 12
		// 		}

	}

	/**
	 * Function that groups an array of associative arrays by some key.
	 * 
	 * @param {String} $key Property to sort by.
	 * @param {Array} $data Array that stores multiple associative arrays.
	 */
	private function group_by($key, $data)
	{
		$result = array();
		foreach ($data as $val) {
			if (array_key_exists($key, $val)) {
				$result[$val[$key]][] = $val;
			} else {
				$result[""][] = $val;
			}
		}
		return $result;
	}
}
