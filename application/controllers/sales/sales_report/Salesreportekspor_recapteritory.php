<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Salesreportekspor_recapteritory extends MY_Controller {
	private $class_link = 'sales/sales_report/salesreportekspor_recapteritory';

	public function __construct() {
		parent::__construct();
		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(array('model_salesorder', 'm_salesperson', 'tb_default_disc', 'db_hrm/tb_hari_libur', 'db_hrm/tb_hari_efektif', 'model_do', 'tm_deliveryorder', 'td_salesorder_item_disc', 'tm_salesorder', 'm_customer', 'tb_teritory'));
    }

    public function index() {
        parent::administrator();
		parent::chartjs_assets();
        $this->table_box();
    }
	 
	public function table_box(){

		// Initiate bulan dan tahun ambil dari database
		$data['bulan'] = [1,2,3,4,5,6,7,8,9,10,11,12];
		$tahunInit = $this->db->select('MIN(YEAR(tgl_so)) as min_tahun, MAX(YEAR(tgl_so)) as max_tahun')
					->from('tm_salesorder')
					->get()->row_array();
		$selisihTahun = $tahunInit['max_tahun'] - $tahunInit['min_tahun'];
		$data['tahun'][] = $tahunInit['min_tahun']; 
		for($i = 0; $i < $selisihTahun; $i++){
			$data['tahun'][] = $data['tahun'][$i] + 1;
		}

		$data['sales_person'] = $this->m_salesperson->get_all();
		$data['tipe_penjualan'] = 'Ekspor';
		$data['status_item'] = array(
			'std' => 'Standart',
			'custom' => 'Custom'
		);		
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main()
	{
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$tahun = $this->input->get('tahun', true);
		$bulan = $this->input->get('bulan', true);
		$tipeCustomer = $this->input->get('tipe_customer', true);
		// Get RAW Invoice
		$inv = $this->model_salesorder->report_sales_ekspor_invoice($tahun, $bulan, $tipeCustomer);
		// Group BY Teritory
		$groupByTeritory = $this->group_by("teritory", $inv);
		// Get all teritory untuk di bandingkan dengan hasil group by
		$teritory = $this->tb_teritory->get_all();
		$sumTeritory = [];
		// Perbandingan teritory dengan group by
		$i = 0;
		foreach($teritory as $ter):
			if(array_key_exists($ter->nm_teritory, $groupByTeritory)):
				$sumTeritory[$i]['teritory'] = $ter->nm_teritory;
				$sumTeritory[$i]['ammount_distributor'] = ROUND(array_sum(array_column($groupByTeritory[$ter->nm_teritory], "ammount_distributor")), 2);
				$sumTeritory[$i]['container_20ft'] = array_sum(array_column($groupByTeritory[$ter->nm_teritory], "container_20ft"));
				$i++;
			endif;
		endforeach;
		$data['sumTeritory'] = $sumTeritory;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	/**
	 * Function that groups an array of associative arrays by some key.
	 * 
	 * @param {String} $key Property to sort by.
	 * @param {Array} $data Array that stores multiple associative arrays.
	 */
	private function group_by($key, $data) {
		$result = array();
		foreach($data as $val) {
			if(array_key_exists($key, $val)){
				$result[$val[$key]][] = $val;
			}else{
				$result[""][] = $val;
			}
		}
		return $result;
	}

}