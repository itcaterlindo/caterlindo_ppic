<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Salesreport_soexclude extends MY_Controller {
	private $class_link = 'sales/sales_report/salesreport_soexclude';

	public function __construct() {
		parent::__construct();
		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(array('model_salesorder'));
    }

    public function index() {
        parent::administrator();
        $this->table_box();
    }
	 
	public function table_box(){

		// Initiate bulan dan tahun ambil dari database
		$data['bulan'] = [1,2,3,4,5,6,7,8,9,10,11,12];
		$tahunInit = $this->db->select('MIN(YEAR(tgl_so)) as min_tahun, MAX(YEAR(tgl_so)) as max_tahun')
					->from('tm_salesorder')
					->get()->row_array();
		$selisihTahun = $tahunInit['max_tahun'] - $tahunInit['min_tahun'];
		$data['tahun'][] = $tahunInit['min_tahun']; 
		for($i = 0; $i < $selisihTahun; $i++){
			$data['tahun'][] = $data['tahun'][$i] + 1;
		}
		
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {

		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
		$tahun = $this->input->get('tahun', true);
      
		$data['retail'] = $this->model_salesorder->report_exclude_by_tahun_jeniscust($tahun, "Retail");
		$data['distributor'] = $this->model_salesorder->report_exclude_by_tahun_jeniscust($tahun, "Distributor");
		$data['reseller'] = $this->model_salesorder->report_exclude_by_tahun_jeniscust($tahun, "Reseller");
		
		$data['tahun'] = $tahun;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);

    }

}
