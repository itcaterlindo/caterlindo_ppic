<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Salesreportekspor_leaveitem extends MY_Controller {
	private $class_link = 'sales/sales_report/salesreportekspor_leaveitem';

	public function __construct() {
		parent::__construct();
		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(array('model_salesorder', 'm_salesperson', 'tb_default_disc', 'db_hrm/tb_hari_libur', 'db_hrm/tb_hari_efektif', 'model_do', 'tm_deliveryorder', 'td_salesorder_item_disc', 'tm_salesorder', 'm_customer'));
    }

    public function index() {
        parent::administrator();
        $this->table_box();
    }
	 
	public function table_box(){
		// Initiate tahun
		$tahunInit = $this->db->select('MIN(YEAR(tgl_so)) as min_tahun, MAX(YEAR(tgl_so)) as max_tahun')
					->from('tm_salesorder')
					->get()->row_array();
		$selisihTahun = $tahunInit['max_tahun'] - $tahunInit['min_tahun'];
		$data['tahun'][] = $tahunInit['min_tahun']; 
		for($i = 0; $i < $selisihTahun; $i++){
			$data['tahun'][] = $data['tahun'][$i] + 1;
		}

		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main()
	{
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$tahun = $this->input->get('tahun', true);
		$leave = $this->model_salesorder->report_sales_leave_item_ekspor($tahun);
		$data['resultLeave'] = $leave;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	/**
	 * Function that groups an array of associative arrays by some key.
	 * 
	 * @param {String} $key Property to sort by.
	 * @param {Array} $data Array that stores multiple associative arrays.
	 */
	private function group_by($key, $data) {
		$result = array();
		foreach($data as $val) {
			if(array_key_exists($key, $val)){
				$result[ $val[$key]][] = $val;
			}else{
				$result[""][] = $val;
			}
		}
		return $result;
	}

}