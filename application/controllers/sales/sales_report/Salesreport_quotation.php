<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Salesreport_quotation extends MY_Controller {
	private $class_link = 'sales/sales_report/salesreport_quotation';

	public function __construct() {
		parent::__construct();
		$this->load->model(['m_quotation', 'model_quotation']);
		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
    }

    public function index() {
        parent::administrator();
        $this->table_box();
    }
	 
	public function table_box(){
		// Initiate bulan dan tahun ambil dari database
		$data['bulan'] = [1,2,3,4,5,6,7,8,9,10,11,12];
		$tahunInit = $this->db->select('MIN(YEAR(tgl_quotation)) as min_tahun, MAX(YEAR(tgl_quotation)) as max_tahun')
					->from('tm_quotation')
					->get()->row_array();
		$selisihTahun = $tahunInit['max_tahun'] - $tahunInit['min_tahun'];
		$data['tahun'][] = $tahunInit['min_tahun']; 
		for($i = 0; $i < $selisihTahun; $i++){
			$data['tahun'][] = $data['tahun'][$i] + 1;
		}
		
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
		// Report sales by jenis customer khusus lokal (Retail, Reseller, Distributor) selain eksport
		// Report sales by kode sales selain kantor
		$bulan = $this->input->get('bulan', true);
        $tahun = $this->input->get('tahun', true);
		$quotation = $this->model_quotation->report_quotation($tahun, $bulan);
		// Cek quotation yang sudah jadi SO dan belum jadi SO, yang sudah jadi PO ditandai dengan nomer SO yang ada datanya / tidak null
		$totalQO = count($quotation);
		$jadiSO = 0;
		$tidakJadiSO = 0;
		$totalPriceExc = 0;
		$totalJadiPriceExc = 0;
		$totalTidakJadiPriceExc = 0;
		$totalQty = 0;
		$totalJadiQty = 0;
		$totalTidakJadiQty = 0;
		foreach($quotation as $qo):
			if( $qo['no_salesorder'] == null ):
				$tidakJadiSO++;
				$totalTidakJadiPriceExc += $qo['total_harga_exc'];
				$totalTidakJadiQty += $qo['total_qty'];
			endif;
			
			if( $qo['no_salesorder'] !== null ):
				$jadiSO++;
				$totalJadiPriceExc += $qo['total_harga_exc'];
				$totalJadiQty += $qo['total_qty'];
			endif;
			$totalPriceExc += $qo['total_harga_exc'];
			$totalQty += $qo['total_qty'];
		endforeach;
		$data['resultQO'] = $quotation;
		// Quotation VS SO
		$data['totalQO'] = $totalQO;
		$data['jadiSO'] = $jadiSO;
		$data['tidakJadiSO'] = $tidakJadiSO;
		$data['persentaseSO'] = round((float)$jadiSO/(float)$totalQO * 100);
		// Price
		$data['totalPriceExc'] = $totalPriceExc;
		$data['totalJadiPriceExc'] = $totalJadiPriceExc;
		$data['totalTidakJadiPriceExc'] = $totalTidakJadiPriceExc;
		$data['persentasePrice'] = round((float)$totalJadiPriceExc/(float)$totalPriceExc * 100);
		// Qty
		$data['totalQty'] = $totalQty;
		$data['totalJadiQty'] = $totalJadiQty;
		$data['totalTidakJadiQty'] = $totalTidakJadiQty;
		$data['persentaseQty'] = round((float)$totalJadiQty/(float)$totalQty * 100);
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }

}
