<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Salesreport_recapinvteritory extends MY_Controller {
	private $class_link = 'sales/sales_report/salesreport_recapinvteritory';

	public function __construct() {
		parent::__construct();
		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(array('model_salesorder', 'm_salesperson', 'tb_default_disc'));
    }

    public function index() {
        parent::administrator();
        $this->table_box();
    }
	 
	public function table_box(){

        // Initiate bulan dan tahun ambil dari database
        $data['bulan'] = [1,2,3,4,5,6,7,8,9,10,11,12];
        $tahunInit = $this->db->select('MIN(YEAR(tgl_so)) as min_tahun, MAX(YEAR(tgl_so)) as max_tahun')
                    ->from('tm_salesorder')
                    ->get()->row_array();
        $selisihTahun = $tahunInit['max_tahun'] - $tahunInit['min_tahun'];
        $data['tahun'][] = $tahunInit['min_tahun']; 
        for($i = 0; $i < $selisihTahun; $i++){
            $data['tahun'][] = $data['tahun'][$i] + 1;
        }
        $data['class_link'] = $this->class_link;
        $this->load->view('page/'.$this->class_link.'/table_box', $data);

	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
		$bulan = "";
        $tahun = $this->input->get('tahun', true);
        $salesPerson = "";
		$tipeCustomer = $this->input->get('tipe_customer', true);
        if($tipeCustomer == "Lokal"):
            $salesInvoice = $this->model_salesorder->report_sales_invoice($tahun, $bulan, $salesPerson, $tipeCustomer);
        elseif($tipeCustomer == "Ekspor"):
            $salesInvoice = $this->model_salesorder->report_sales_ekspor_invoice($tahun, $bulan, $tipeCustomer);
        endif;
 
        // GET NEGARA PROVINSI BY SALES INVOICE
        $lokasi = array();
        foreach($salesInvoice as $key => $value):
            $lokasi[] = array(
                'negara' => $value['nm_negara'],
                'provinsi' => $value['nm_provinsi']
            );
        endforeach;
        $temp = array_unique(array_column($lokasi, 'provinsi') );
        $arr = array_intersect_key($lokasi, $temp);
        $arrLokasi = array();
        foreach($arr as $key => $value):
            $arrLokasi[] = $value;
        endforeach;
        // END GET NEGARA DAN PROVINSI

        // HITUNG BERDASARKAN NEGARA DAN PROVINSI
        // Array Januari
		$arrJanuari = array_filter($salesInvoice, function ($var) {
            $bulan = "01";
            // Lokal menggunakan field tgl invoice
            if( isset($var['tgl_invoice']) ):
                if( format_date( $var['tgl_invoice'], "m" ) == $bulan && $var['status_invoice'] != "DP" ):
                    return $var;
                endif;
            // Ekspor menggunakan field tgl_do tidak ada DP	
            else:
                if( format_date( $var['tgl_do'], "m" ) == $bulan ):
                    return $var;
                endif;	
            endif;
		});
        // Array Februari
		$arrFebruari = array_filter($salesInvoice, function ($var) {
            $bulan = "02";
            // Lokal menggunakan field tgl invoice
            if( isset($var['tgl_invoice']) ):
                if( format_date( $var['tgl_invoice'], "m" ) == $bulan && $var['status_invoice'] != "DP" ):
                    return $var;
                endif;
            // Ekspor menggunakan field tgl_do tidak ada DP	
            else:
                if( format_date( $var['tgl_do'], "m" ) == $bulan ):
                    return $var;
                endif;	
            endif;
		});
        // Array Maret
		$arrMaret = array_filter($salesInvoice, function ($var) {
            $bulan = "03";
            // Lokal menggunakan field tgl invoice
            if( isset($var['tgl_invoice']) ):
                if( format_date( $var['tgl_invoice'], "m" ) == $bulan && $var['status_invoice'] != "DP" ):
                    return $var;
                endif;
            // Ekspor menggunakan field tgl_do tidak ada DP	
            else:
                if( format_date( $var['tgl_do'], "m" ) == $bulan ):
                    return $var;
                endif;	
            endif;
		});
        // Array April
		$arrApril = array_filter($salesInvoice, function ($var) {
            $bulan = "04";
            // Lokal menggunakan field tgl invoice
            if( isset($var['tgl_invoice']) ):
                if( format_date( $var['tgl_invoice'], "m" ) == $bulan && $var['status_invoice'] != "DP" ):
                    return $var;
                endif;
            // Ekspor menggunakan field tgl_do tidak ada DP	
            else:
                if( format_date( $var['tgl_do'], "m" ) == $bulan ):
                    return $var;
                endif;	
            endif;
		});
        // Array Mei
		$arrMei = array_filter($salesInvoice, function ($var) {
            $bulan = "05";
            // Lokal menggunakan field tgl invoice
            if( isset($var['tgl_invoice']) ):
                if( format_date( $var['tgl_invoice'], "m" ) == $bulan && $var['status_invoice'] != "DP" ):
                    return $var;
                endif;
            // Ekspor menggunakan field tgl_do tidak ada DP	
            else:
                if( format_date( $var['tgl_do'], "m" ) == $bulan ):
                    return $var;
                endif;	
            endif;
		});
        // Array Juni
		$arrJuni = array_filter($salesInvoice, function ($var) {
            $bulan = "06";
            // Lokal menggunakan field tgl invoice
            if( isset($var['tgl_invoice']) ):
                if( format_date( $var['tgl_invoice'], "m" ) == $bulan && $var['status_invoice'] != "DP" ):
                    return $var;
                endif;
            // Ekspor menggunakan field tgl_do tidak ada DP	
            else:
                if( format_date( $var['tgl_do'], "m" ) == $bulan ):
                    return $var;
                endif;	
            endif;
		});
        // Array Juli
		$arrJuli = array_filter($salesInvoice, function ($var) {
            $bulan = "07";
            // Lokal menggunakan field tgl invoice
            if( isset($var['tgl_invoice']) ):
                if( format_date( $var['tgl_invoice'], "m" ) == $bulan && $var['status_invoice'] != "DP" ):
                    return $var;
                endif;
            // Ekspor menggunakan field tgl_do tidak ada DP	
            else:
                if( format_date( $var['tgl_do'], "m" ) == $bulan ):
                    return $var;
                endif;	
            endif;
		});
        // Array Agustus
		$arrAgustus = array_filter($salesInvoice, function ($var) {
            $bulan = "08";
            // Lokal menggunakan field tgl invoice
            if( isset($var['tgl_invoice']) ):
                if( format_date( $var['tgl_invoice'], "m" ) == $bulan && $var['status_invoice'] != "DP" ):
                    return $var;
                endif;
            // Ekspor menggunakan field tgl_do tidak ada DP	
            else:
                if( format_date( $var['tgl_do'], "m" ) == $bulan ):
                    return $var;
                endif;	
            endif;
		});
        // Array September
		$arrSeptember = array_filter($salesInvoice, function ($var) {
            $bulan = "09";
            // Lokal menggunakan field tgl invoice
            if( isset($var['tgl_invoice']) ):
                if( format_date( $var['tgl_invoice'], "m" ) == $bulan && $var['status_invoice'] != "DP" ):
                    return $var;
                endif;
            // Ekspor menggunakan field tgl_do tidak ada DP	
            else:
                if( format_date( $var['tgl_do'], "m" ) == $bulan ):
                    return $var;
                endif;	
            endif;
		});
        // Array Oktober
		$arrOktober = array_filter($salesInvoice, function ($var) {
            $bulan = "10";
            // Lokal menggunakan field tgl invoice
            if( isset($var['tgl_invoice']) ):
                if( format_date( $var['tgl_invoice'], "m" ) == $bulan && $var['status_invoice'] != "DP" ):
                    return $var;
                endif;
            // Ekspor menggunakan field tgl_do tidak ada DP	
            else:
                if( format_date( $var['tgl_do'], "m" ) == $bulan ):
                    return $var;
                endif;	
            endif;
		});
        // Array November
		$arrNovember = array_filter($salesInvoice, function ($var) {
            $bulan = "11";
            // Lokal menggunakan field tgl invoice
            if( isset($var['tgl_invoice']) ):
                if( format_date( $var['tgl_invoice'], "m" ) == $bulan && $var['status_invoice'] != "DP" ):
                    return $var;
                endif;
            // Ekspor menggunakan field tgl_do tidak ada DP	
            else:
                if( format_date( $var['tgl_do'], "m" ) == $bulan ):
                    return $var;
                endif;	
            endif;
		});
        // Array Desember
		$arrDesember = array_filter($salesInvoice, function ($var) {
            $bulan = "12";
            // Lokal menggunakan field tgl invoice
            if( isset($var['tgl_invoice']) ):
                if( format_date( $var['tgl_invoice'], "m" ) == $bulan && $var['status_invoice'] != "DP" ):
                    return $var;
                endif;
            // Ekspor menggunakan field tgl_do tidak ada DP	
            else:
                if( format_date( $var['tgl_do'], "m" ) == $bulan ):
                    return $var;
                endif;	
            endif;
		});
        $recapInv = array();
        $ammountRecap = 0;
        foreach($arrLokasi as $lok):
            // Recap Januari
            $ammountRecap = 0;
            foreach($arrJanuari as $inv):
                if( ( $inv['nm_negara'] == $lok['negara'] && $inv['nm_provinsi'] == $lok['provinsi'] ) ):
                    // LOKAL
                    if($tipeCustomer == "Lokal"):
                        $AmmountExc = $inv['item_standart_harga'] + $inv['item_custom_harga'] - $inv['total_nilai_disc'] + $inv['jml_ongkir'] + $inv['jml_install'];
                        $ppn = $AmmountExc * $inv['jml_ppn'] / 100;
                        $ammountRecap +=  $AmmountExc + $ppn;    
                    // EKSPOR                
                    else:
                        $ammountRecap += $inv['ammount_distributor'];
                    endif;
                    $recapInv[1][ $inv['nm_negara']."-".$inv['nm_provinsi'] ] = $ammountRecap;
                endif; 
            endforeach;
            // Recap Februari
            $ammountRecap = 0;
            foreach($arrFebruari as $inv):
                if( ( $inv['nm_negara'] == $lok['negara'] && $inv['nm_provinsi'] == $lok['provinsi'] ) ):
                    // LOKAL
                    if($tipeCustomer == "Lokal"):
                        $AmmountExc = $inv['item_standart_harga'] + $inv['item_custom_harga'] - $inv['total_nilai_disc'] + $inv['jml_ongkir'] + $inv['jml_install'];
                        $ppn = $AmmountExc * $inv['jml_ppn'] / 100;
                        $ammountRecap +=  $AmmountExc + $ppn;    
                    // EKSPOR                
                    else:
                        $ammountRecap += $inv['ammount_distributor'];
                    endif;
                    $recapInv[2][ $inv['nm_negara']."-".$inv['nm_provinsi'] ] = $ammountRecap;
                endif; 
            endforeach;   
            // Recap Maret
            $ammountRecap = 0;
            foreach($arrMaret as $inv):
                if( ( $inv['nm_negara'] == $lok['negara'] && $inv['nm_provinsi'] == $lok['provinsi'] ) ):
                    // LOKAL
                    if($tipeCustomer == "Lokal"):
                        $AmmountExc = $inv['item_standart_harga'] + $inv['item_custom_harga'] - $inv['total_nilai_disc'] + $inv['jml_ongkir'] + $inv['jml_install'];
                        $ppn = $AmmountExc * $inv['jml_ppn'] / 100;
                        $ammountRecap +=  $AmmountExc + $ppn;    
                    // EKSPOR                
                    else:
                        $ammountRecap += $inv['ammount_distributor'];
                    endif;
                    $recapInv[3][ $inv['nm_negara']."-".$inv['nm_provinsi'] ] = $ammountRecap;
                endif; 
            endforeach;
            // Recap April
            $ammountRecap = 0;
            foreach($arrApril as $inv):
                if( ( $inv['nm_negara'] == $lok['negara'] && $inv['nm_provinsi'] == $lok['provinsi'] ) ):
                    // LOKAL
                    if($tipeCustomer == "Lokal"):
                        $AmmountExc = $inv['item_standart_harga'] + $inv['item_custom_harga'] - $inv['total_nilai_disc'] + $inv['jml_ongkir'] + $inv['jml_install'];
                        $ppn = $AmmountExc * $inv['jml_ppn'] / 100;
                        $ammountRecap +=  $AmmountExc + $ppn;    
                    // EKSPOR                
                    else:
                        $ammountRecap += $inv['ammount_distributor'];
                    endif;
                    $recapInv[4][ $inv['nm_negara']."-".$inv['nm_provinsi'] ] = $ammountRecap;
                endif; 
            endforeach;
            // Recap Mei
            $ammountRecap = 0;
            foreach($arrMei as $inv):
                if( ( $inv['nm_negara'] == $lok['negara'] && $inv['nm_provinsi'] == $lok['provinsi'] ) ):
                    // LOKAL
                    if($tipeCustomer == "Lokal"):
                        $AmmountExc = $inv['item_standart_harga'] + $inv['item_custom_harga'] - $inv['total_nilai_disc'] + $inv['jml_ongkir'] + $inv['jml_install'];
                        $ppn = $AmmountExc * $inv['jml_ppn'] / 100;
                        $ammountRecap +=  $AmmountExc + $ppn;    
                    // EKSPOR                
                    else:
                        $ammountRecap += $inv['ammount_distributor'];
                    endif;
                    $recapInv[5][ $inv['nm_negara']."-".$inv['nm_provinsi'] ] = $ammountRecap;
                endif; 
            endforeach;   
            // Recap Juni
            $ammountRecap = 0;
            foreach($arrJuni as $inv):
                if( ( $inv['nm_negara'] == $lok['negara'] && $inv['nm_provinsi'] == $lok['provinsi'] ) ):
                    // LOKAL
                    if($tipeCustomer == "Lokal"):
                        $AmmountExc = $inv['item_standart_harga'] + $inv['item_custom_harga'] - $inv['total_nilai_disc'] + $inv['jml_ongkir'] + $inv['jml_install'];
                        $ppn = $AmmountExc * $inv['jml_ppn'] / 100;
                        $ammountRecap +=  $AmmountExc + $ppn;    
                    // EKSPOR                
                    else:
                        $ammountRecap += $inv['ammount_distributor'];
                    endif;
                    $recapInv[6][ $inv['nm_negara']."-".$inv['nm_provinsi'] ] = $ammountRecap;
                endif; 
            endforeach; 
            // Recap Juli
            $ammountRecap = 0;
            foreach($arrJuli as $inv):
                if( ( $inv['nm_negara'] == $lok['negara'] && $inv['nm_provinsi'] == $lok['provinsi'] ) ):
                    // LOKAL
                    if($tipeCustomer == "Lokal"):
                        $AmmountExc = $inv['item_standart_harga'] + $inv['item_custom_harga'] - $inv['total_nilai_disc'] + $inv['jml_ongkir'] + $inv['jml_install'];
                        $ppn = $AmmountExc * $inv['jml_ppn'] / 100;
                        $ammountRecap +=  $AmmountExc + $ppn;    
                    // EKSPOR                
                    else:
                        $ammountRecap += $inv['ammount_distributor'];
                    endif;
                    $recapInv[7][ $inv['nm_negara']."-".$inv['nm_provinsi'] ] = $ammountRecap;
                endif; 
            endforeach;   
            // Recap Agustus
            $ammountRecap = 0;
            foreach($arrAgustus as $inv):
                if( ( $inv['nm_negara'] == $lok['negara'] && $inv['nm_provinsi'] == $lok['provinsi'] ) ):
                    // LOKAL
                    if($tipeCustomer == "Lokal"):
                        $AmmountExc = $inv['item_standart_harga'] + $inv['item_custom_harga'] - $inv['total_nilai_disc'] + $inv['jml_ongkir'] + $inv['jml_install'];
                        $ppn = $AmmountExc * $inv['jml_ppn'] / 100;
                        $ammountRecap +=  $AmmountExc + $ppn;    
                    // EKSPOR                
                    else:
                        $ammountRecap += $inv['ammount_distributor'];
                    endif;
                    $recapInv[8][ $inv['nm_negara']."-".$inv['nm_provinsi'] ] = $ammountRecap;
                endif; 
            endforeach;    
            // Recap September
            $ammountRecap = 0;
            foreach($arrSeptember as $inv):
                if( ( $inv['nm_negara'] == $lok['negara'] && $inv['nm_provinsi'] == $lok['provinsi'] ) ):
                    // LOKAL
                    if($tipeCustomer == "Lokal"):
                        $AmmountExc = $inv['item_standart_harga'] + $inv['item_custom_harga'] - $inv['total_nilai_disc'] + $inv['jml_ongkir'] + $inv['jml_install'];
                        $ppn = $AmmountExc * $inv['jml_ppn'] / 100;
                        $ammountRecap +=  $AmmountExc + $ppn;    
                    // EKSPOR                
                    else:
                        $ammountRecap += $inv['ammount_distributor'];
                    endif;
                    $recapInv[9][ $inv['nm_negara']."-".$inv['nm_provinsi'] ] = $ammountRecap;
                endif; 
            endforeach; 
            // Recap Oktober
            $ammountRecap = 0;
            foreach($arrOktober as $inv):
                if( ( $inv['nm_negara'] == $lok['negara'] && $inv['nm_provinsi'] == $lok['provinsi'] ) ):
                    // LOKAL
                    if($tipeCustomer == "Lokal"):
                        $AmmountExc = $inv['item_standart_harga'] + $inv['item_custom_harga'] - $inv['total_nilai_disc'] + $inv['jml_ongkir'] + $inv['jml_install'];
                        $ppn = $AmmountExc * $inv['jml_ppn'] / 100;
                        $ammountRecap +=  $AmmountExc + $ppn;    
                    // EKSPOR                
                    else:
                        $ammountRecap += $inv['ammount_distributor'];
                    endif;
                    $recapInv[10][ $inv['nm_negara']."-".$inv['nm_provinsi'] ] = $ammountRecap;
                endif; 
            endforeach; 
            // Recap November
            $ammountRecap = 0;
            foreach($arrNovember as $inv):
                if( ( $inv['nm_negara'] == $lok['negara'] && $inv['nm_provinsi'] == $lok['provinsi'] ) ):
                    // LOKAL
                    if($tipeCustomer == "Lokal"):
                        $AmmountExc = $inv['item_standart_harga'] + $inv['item_custom_harga'] - $inv['total_nilai_disc'] + $inv['jml_ongkir'] + $inv['jml_install'];
                        $ppn = $AmmountExc * $inv['jml_ppn'] / 100;
                        $ammountRecap +=  $AmmountExc + $ppn;    
                    // EKSPOR                
                    else:
                        $ammountRecap += $inv['ammount_distributor'];
                    endif;
                    $recapInv[11][ $inv['nm_negara']."-".$inv['nm_provinsi'] ] = $ammountRecap;
                endif; 
            endforeach; 
            // Recap Desember
            $ammountRecap = 0;
            foreach($arrDesember as $inv):
                if( ( $inv['nm_negara'] == $lok['negara'] && $inv['nm_provinsi'] == $lok['provinsi'] ) ):
                    // LOKAL
                    if($tipeCustomer == "Lokal"):
                        $AmmountExc = $inv['item_standart_harga'] + $inv['item_custom_harga'] - $inv['total_nilai_disc'] + $inv['jml_ongkir'] + $inv['jml_install'];
                        $ppn = $AmmountExc * $inv['jml_ppn'] / 100;
                        $ammountRecap +=  $AmmountExc + $ppn;    
                    // EKSPOR                
                    else:
                        $ammountRecap += $inv['ammount_distributor'];
                    endif;
                    $recapInv[12][ $inv['nm_negara']."-".$inv['nm_provinsi'] ] = $ammountRecap;
                endif; 
            endforeach; 
        endforeach;
        // Adjust array to parsing view
        $recapSUM = array();
        foreach($arrLokasi as $lok):
            $recapSUM[] = [
                'lokasi' => $lok['negara']."-".$lok['provinsi'],
                1 => isset($recapInv[1][$lok['negara']."-".$lok['provinsi']]) ? $recapInv[1][$lok['negara']."-".$lok['provinsi']] : 0,
                2 => isset($recapInv[2][$lok['negara']."-".$lok['provinsi']]) ? $recapInv[2][$lok['negara']."-".$lok['provinsi']] : 0,
                3 => isset($recapInv[3][$lok['negara']."-".$lok['provinsi']]) ? $recapInv[3][$lok['negara']."-".$lok['provinsi']] : 0,
                4 => isset($recapInv[4][$lok['negara']."-".$lok['provinsi']]) ? $recapInv[4][$lok['negara']."-".$lok['provinsi']] : 0,
                5 => isset($recapInv[5][$lok['negara']."-".$lok['provinsi']]) ? $recapInv[5][$lok['negara']."-".$lok['provinsi']] : 0,
                6 => isset($recapInv[6][$lok['negara']."-".$lok['provinsi']]) ? $recapInv[6][$lok['negara']."-".$lok['provinsi']] : 0,
                7 => isset($recapInv[7][$lok['negara']."-".$lok['provinsi']]) ? $recapInv[7][$lok['negara']."-".$lok['provinsi']] : 0,
                8 => isset($recapInv[8][$lok['negara']."-".$lok['provinsi']]) ? $recapInv[8][$lok['negara']."-".$lok['provinsi']] : 0,
                9 => isset($recapInv[9][$lok['negara']."-".$lok['provinsi']]) ? $recapInv[9][$lok['negara']."-".$lok['provinsi']] : 0,
                10 => isset($recapInv[10][$lok['negara']."-".$lok['provinsi']]) ? $recapInv[10][$lok['negara']."-".$lok['provinsi']] : 0,
                11 => isset($recapInv[11][$lok['negara']."-".$lok['provinsi']]) ? $recapInv[11][$lok['negara']."-".$lok['provinsi']] : 0,
                12 => isset($recapInv[12][$lok['negara']."-".$lok['provinsi']]) ? $recapInv[12][$lok['negara']."-".$lok['provinsi']] : 0,
            ];
        endforeach;
        $data['lokasi'] = $arrLokasi;
        $data['recap'] = $recapSUM;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }
}