<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Salesreportekspor_comparesimplyezy extends MY_Controller
{
	private $class_link = 'sales/sales_report/salesreportekspor_comparesimplyezy';

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(array('model_salesorder', 'm_salesperson', 'tb_default_disc', 'db_hrm/tb_hari_libur', 'db_hrm/tb_hari_efektif', 'model_do', 'tm_deliveryorder', 'td_salesorder_item_disc', 'tm_salesorder', 'm_customer', 'tb_teritory', 'tb_tipe_teritory'));
	}

	public function index()
	{
		parent::administrator();
		parent::chartjs_assets();
		$this->table_box();
	}

	public function table_box()
	{

		// Tahun ambil dari database
		$tahunInit = $this->db->select('MIN(YEAR(tgl_so)) as min_tahun, MAX(YEAR(tgl_so)) as max_tahun')
			->from('tm_salesorder')
			->get()->row_array();
		$selisihTahun = $tahunInit['max_tahun'] - $tahunInit['min_tahun'];
		$data['tahun'][] = $tahunInit['min_tahun'];
		for ($i = 0; $i < $selisihTahun; $i++) {
			$data['tahun'][] = $data['tahun'][$i] + 1;
		}
		$data['tipe_penjualan'] = 'Ekspor';
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/table_box', $data);
	}

	public function table_main()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$tahunAwal = $this->input->get('tahunawal', true);
		$tahun = $tahunAwal;
		$tahunAkhir = $this->input->get('tahunakhir', true);
		$tipe_penjualan = $this->input->get('tipe_penjualan', true);
		$selisihTahun = $tahunAkhir - $tahunAwal;
		$result = [];
		$listGroup = [];
		for ($iTahun = 0; $iTahun <= $selisihTahun; $iTahun++) :
			// array_push($result, array('tahun' => $tahunAwal));
			for ($bulan = 1; $bulan <= 12; $bulan++) :
				// Get per tahun dan per bulan
				// echo json_encode($data_so) . '<br>'; 
				$data_so = $this->report_sales($tahunAwal, $bulan, $tipe_penjualan);
				
				if (empty($data_so)) {
					// break;
				}else{
					foreach ($data_so as $v) {
						$result[$tahunAwal][$bulan][$v->nm_group]['item_qty'] = ($v->item_qty ?: 0);
						$result[$tahunAwal][$bulan][$v->nm_group]['total_harga'] = ($v->total_harga ?: 0);
	
						if (in_array($v->nm_group, $listGroup)) {
							// continue;
						} else {
							array_push($listGroup, $v->nm_group);
						}
						// array_push($result[$tahunAwal], array( 'bulan' => $bulan, 'nm_group' => $v->nm_group, 'total_harga' => $v->total_harga, 'item_qty' => $v->item_qty));
					}
				}

				
				

			endfor;
			$tahunAwal++;
		endfor;

		$data['class_link'] = $this->class_link;
		$data['result'] = $result;
		$data['listGroup'] = $listGroup;
		$data['tahunAwal'] = $tahun;
		$data['selisihTahun'] = $selisihTahun;
		$this->load->view('page/' . $this->class_link . '/table_main', $data);
		// echo json_encode($result);
		// Output struktur arraynyaj
		// "resultTipeTeritory": {
		// 	"2022": [{
		// 		"tipe_teritory": "simply",
		// 		"januari": {
		// 			"ammount_distributor": 274201.3,
		// 			"container_20ft": 12
		// 		},
		// 		"februari": {
		// 			"ammount_distributor": 340818.79,
		// 			"container_20ft": 12
		// 		}

	}

	private function report_sales($tahun, $bulan, $tipe_penjualan)
	{
		$q = $this->db->query("select e.nm_group, sum(((100 - cc.jml_disc) / 100) * c.total_harga) as total_harga, sum(b.stuffing_item_qty) as item_qty 
		FROM tm_deliveryorder AS a
		LEFT JOIN td_do_item AS b ON a.kd_mdo=b.mdo_kd 
		LEFT JOIN td_salesorder_item AS c ON b.parent_kd=c.kd_ditem_so 
		LEFT JOIN td_salesorder_item_disc AS cc ON c.kd_ditem_so=cc.so_item_kd AND cc.nm_pihak = 'Distributor' 
		LEFT JOIN tm_barang AS d ON c.barang_kd=d.kd_barang
		LEFT JOIN tm_group_barang AS e ON d.group_barang_kd =e.kd_group_barang
		WHERE YEAR(a.tgl_do) = " . $tahun . " 
		AND MONTH(a.tgl_do) = " . $bulan . "
		AND (b.child_kd IS NULL OR b.child_kd IS NOT NULL )  
		AND a.status_do = 'finish'
		and a.tipe_do = '" . $tipe_penjualan . "'
		group by e.nm_group")->result();

		return $q;
	}

	/**
	 * Function that groups an array of associative arrays by some key.
	 * 
	 * @param {String} $key Property to sort by.
	 * @param {Array} $data Array that stores multiple associative arrays.
	 */
	private function group_by($key, $data)
	{
		$result = array();
		foreach ($data as $val) {
			if (array_key_exists($key, $val)) {
				$result[$val[$key]][] = $val;
			} else {
				$result[""][] = $val;
			}
		}
		return $result;
	}

	private function get_value($tahun, $bulan, $group, $data)
	{
		$result['item_qty'] = 0;
		$result['total_harga'] = 0;
		foreach ($data as $v) {
			if (($v['tahun'] == $tahun) && ($v['bulan'] == $bulan) && ($v['group'] == $group)) {
				$result['item_qty'] = $v['item_qty'];
				$result['total_harga'] = $v['total_harga'];
				break;
			}
		}
		return $result;
	}
}
