<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Salesreport_salesorder extends MY_Controller {
	private $class_link = 'sales/sales_report/salesreport_salesorder';

	public function __construct() {
		parent::__construct();
		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(array('model_salesorder'));
    }

    public function index() {
        parent::administrator();
        $this->table_box();
    }
	 
	public function table_box(){
		// Initiate bulan dan tahun ambil dari database
		$data['bulan'] = [1,2,3,4,5,6,7,8,9,10,11,12];
		$tahunInit = $this->db->select('MIN(YEAR(tgl_so)) as min_tahun, MAX(YEAR(tgl_so)) as max_tahun')
					->from('tm_salesorder')
					->get()->row_array();
		$selisihTahun = $tahunInit['max_tahun'] - $tahunInit['min_tahun'];
		$data['tahun'][] = $tahunInit['min_tahun']; 
		for($i = 0; $i < $selisihTahun; $i++){
			$data['tahun'][] = $data['tahun'][$i] + 1;
		}
		
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
		$bulan = $this->input->get('bulan', true);
        $tahun = $this->input->get('tahun', true);
		$salesOrder = $this->model_salesorder->report_sales($tahun, $bulan);
		// Disc proposional
		$discItemStandart = 0;
		$discItemCustom = 0;
		$i = 0;
		foreach($salesOrder as $so):
			$data['resultSO'][$i]['no_salesorder'] = $so['no_salesorder'];
			$data['resultSO'][$i]['nm_salesperson'] = $so['nm_salesperson'];
			$data['resultSO'][$i]['tgl_so'] = $so['tgl_so'];
			$data['resultSO'][$i]['tgl_kirim'] = $so['tgl_kirim'];
			$data['resultSO'][$i]['nm_customer'] = $so['nm_customer'];
			$data['resultSO'][$i]['nm_jenis_customer'] = $so['nm_jenis_customer'];
			$data['resultSO'][$i]['no_invoice'] = $so['no_invoice'];
			$data['resultSO'][$i]['jml_ongkir'] = $so['jml_ongkir'];
			$data['resultSO'][$i]['jml_install'] = $so['jml_install'];
			$data['resultSO'][$i]['total_nilai_disc'] = $so['total_nilai_disc'];
			// Menampung total ammount harga std dan custom
			$totalStdCustomHarga = $so['item_standart_harga'] + $so['item_custom_harga'];
			// Proposional diskon standart dan custom jika terdapat diskon di SO
			// IF untuk antisipasi divide by zero (pembagian nol)
			if($totalStdCustomHarga != 0 ):
			$discItemStandart = $so['item_standart_harga'] / ($totalStdCustomHarga) * $so['total_nilai_disc'];
			$discItemCustom = $so['item_custom_harga'] / ($totalStdCustomHarga) * $so['total_nilai_disc'];
			endif;
			$data['resultSO'][$i]['item_standart_harga'] = $so['item_standart_harga'] - $discItemStandart;
			$data['resultSO'][$i]['item_custom_harga'] = $so['item_custom_harga'] - $discItemCustom;
			// Total exc di ambil dari total item standart dan custom sudah di kurangi discount proposional masing2 status item
			$data['resultSO'][$i]['total_harga_exc'] = $data['resultSO'][$i]['item_standart_harga'] + $data['resultSO'][$i]['item_custom_harga'] + $so['jml_ongkir'] + $so['jml_install'];
			$data['resultSO'][$i]['total_harga_inc'] = $data['resultSO'][$i]['total_harga_exc'] + (($data['resultSO'][$i]['total_harga_exc'] * $so['jml_ppn'])/100);
			$data['resultSO'][$i]['item_standart_qty'] = $so['item_standart_qty'];
			$data['resultSO'][$i]['item_custom_qty'] = $so['item_custom_qty'];
		$i++;
		endforeach;
		// Array Retail
		$arrRetail = array_filter($data['resultSO'], function ($var) {
			if($var['nm_jenis_customer'] == "Retail"):
				return $var;
			endif;	
		});
		// Array Distributor
		$arrDistributor = array_filter($data['resultSO'], function ($var) {
			if($var['nm_jenis_customer'] == "Distributor"):
				return $var;
			endif;	
		});
		// Array Reseller
		$arrReseller = array_filter($data['resultSO'], function ($var) {
			if($var['nm_jenis_customer'] == "Reseller"):
				return $var;
			endif;	
		});
		// Retail
		// Inc Exc
		$data['totalRetailInc'] = array_sum(array_column($arrRetail, 'total_harga_inc'));
		$data['totalRetailExc'] = array_sum(array_column($arrRetail, 'total_harga_exc'));
		// Qty
		$data['totalQtyRetail'] = array_sum(array_column($arrRetail, 'item_standart_qty')) + array_sum(array_column($arrRetail, 'item_custom_qty'));
		$data['totalQtyStandartRetail'] = array_sum(array_column($arrRetail, 'item_standart_qty'));
		$data['totalQtyCustomRetail'] = array_sum(array_column($arrRetail, 'item_custom_qty'));
		// Ammount (Exc)
		$data['totalAmmountRetail'] = array_sum(array_column($arrRetail, 'item_standart_harga')) + array_sum(array_column($arrRetail, 'item_custom_harga'));
		$data['totalAmmountStandartRetail'] = array_sum(array_column($arrRetail, 'item_standart_harga'));
		$data['totalAmmountCustomRetail'] = array_sum(array_column($arrRetail, 'item_custom_harga'));
		// Distributor
		// Inc Exc
		$data['totalDistributorInc'] = array_sum(array_column($arrDistributor, 'total_harga_inc'));
		$data['totalDistributorExc'] = array_sum(array_column($arrDistributor, 'total_harga_exc'));
		// Qty
		$data['totalQtyDistributor'] = array_sum(array_column($arrDistributor, 'item_standart_qty')) + array_sum(array_column($arrDistributor, 'item_custom_qty'));
		$data['totalQtyStandartDistributor'] = array_sum(array_column($arrDistributor, 'item_standart_qty'));
		$data['totalQtyCustomDistributor'] = array_sum(array_column($arrDistributor, 'item_custom_qty'));
		// Ammount (Exc)
		$data['totalAmmountDistributor'] = array_sum(array_column($arrDistributor, 'item_standart_harga')) + array_sum(array_column($arrDistributor, 'item_custom_harga'));
		$data['totalAmmountStandartDistributor'] = array_sum(array_column($arrDistributor, 'item_standart_harga'));
		$data['totalAmmountCustomDistributor'] = array_sum(array_column($arrDistributor, 'item_custom_harga'));
		// Reseller
		// Inc Exc
		$data['totalResellerInc'] = array_sum(array_column($arrReseller, 'total_harga_inc'));
		$data['totalResellerExc'] = array_sum(array_column($arrReseller, 'total_harga_exc'));
		// Qty
		$data['totalQtyReseller'] = array_sum(array_column($arrReseller, 'item_standart_qty')) + array_sum(array_column($arrReseller, 'item_custom_qty'));
		$data['totalQtyStandartReseller'] = array_sum(array_column($arrReseller, 'item_standart_qty'));
		$data['totalQtyCustomReseller'] = array_sum(array_column($arrReseller, 'item_custom_qty'));
		// Ammount (Exc)
		$data['totalAmmountReseller'] = array_sum(array_column($arrReseller, 'item_standart_harga')) + array_sum(array_column($arrReseller, 'item_custom_harga'));
		$data['totalAmmountStandartReseller'] = array_sum(array_column($arrReseller, 'item_standart_harga'));
		$data['totalAmmountCustomReseller'] = array_sum(array_column($arrReseller, 'item_custom_harga'));
		// Summary all standart & custom
		$data['totalQty'] = $data['totalQtyRetail'] + $data['totalQtyDistributor'] + $data['totalQtyReseller'];
		$data['totalAmmount'] = $data['totalAmmountRetail'] + $data['totalAmmountDistributor'] + $data['totalAmmountReseller'];
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }

}
