<?php
defined('BASEPATH') or exit('No direct script access allowed!');

require_once APPPATH."third_party/PhpSpreadsheets/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Salesreport_deliveryorder extends MY_Controller {
	private $class_link = 'sales/sales_report/salesreport_deliveryorder';

	public function __construct() {
		parent::__construct();
		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
    }

    public function index() {
        parent::administrator();
        $this->table_box();
    }
	 
	public function table_box(){

		// Initiate bulan dan tahun ambil dari database
		$data['bulan'] = [1,2,3,4,5,6,7,8,9,10,11,12];
		$tahunInit = $this->db->select('MIN(YEAR(tgl_do)) as min_tahun, MAX(YEAR(tgl_do)) as max_tahun')
					->from('tm_deliveryorder')
					->get()->row_array();
		$selisihTahun = $tahunInit['max_tahun'] - $tahunInit['min_tahun'];
		$data['tahun'][] = $tahunInit['min_tahun']; 
		for($i = 0; $i < $selisihTahun; $i++){
			$data['tahun'][] = $data['tahun'][$i] + 1;
		}
		
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
		// Report sales by jenis customer khusus lokal (Retail, Reseller, Distributor) selain eksport
		// Report sales lokal selain sales kantor
		$bulan = $this->input->get('bulan', true);
        $tahun = $this->input->get('tahun', true);
		$deliveryOrder = $this->db->select('b.no_salesorder,c.nm_customer,e.nm_salesperson,a.tgl_do,b.tgl_so,b.tgl_kirim,a.tgl_stuffing,IF(a.tgl_stuffing <= b.tgl_kirim,"ontime","late") as evaluation_delivery,d.alamat,a.nm_jasakirim,a.do_ket,f.tgl_fg_out')
						->from('tm_deliveryorder a')
						->join('tm_salesorder b', 'a.msalesorder_kd = b.kd_msalesorder')
						->join('tm_customer c', 'b.customer_kd = c.kd_customer', 'LEFT')
						->join('td_customer_alamat d', 'b.alamat_kirim_kd = d.kd_alamat_kirim', 'LEFT')
						->join('tb_salesperson e', 'b.salesperson_kd = e.kd_salesperson', 'LEFT')
						->join('( 	SELECT a.kd_msalesorder, GROUP_CONCAT(DISTINCT DATE_FORMAT(a.fgout_tglinput,"%d-%m-%Y") SEPARATOR ";") tgl_fg_out
									FROM td_finishgood_out a
									GROUP BY a.kd_msalesorder
									ORDER BY a.fgout_tglinput ASC ) f', 'b.kd_msalesorder = f.kd_msalesorder', 'LEFT')
						->where('a.tipe_do', 'Lokal')
						->where('e.nm_salesperson !=', 'Kantor')
						->where('YEAR(a.tgl_stuffing)', $tahun)
						->where('MONTH(a.tgl_stuffing)', $bulan)
						->order_by('a.tgl_stuffing ASC')
						->get()->result_array();
		$data['resultDO'] = $deliveryOrder;
		// Count ontime & late
		$arrOntimeLate = array_count_values(array_column($deliveryOrder, "evaluation_delivery"));
		$data['countOntime'] = $arrOntimeLate['ontime'];
		$data['countLate'] = $arrOntimeLate['late'];
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }

	public function save_to_excel()
	{
		
	}
	
}
