<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Salesreportekspor_invoice extends MY_Controller {
	private $class_link = 'sales/sales_report/salesreportekspor_invoice';

	public function __construct() {
		parent::__construct();
		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(array('model_salesorder', 'm_salesperson', 'tb_default_disc', 'db_hrm/tb_hari_libur', 'db_hrm/tb_hari_efektif', 'model_do', 'tm_deliveryorder', 'td_salesorder_item_disc', 'tm_salesorder', 'm_customer'));
    }

    public function index() {
        parent::administrator();
        $this->table_box();
    }
	 
	public function table_box(){

		// Initiate bulan dan tahun ambil dari database
		$data['bulan'] = [1,2,3,4,5,6,7,8,9,10,11,12];
		$tahunInit = $this->db->select('MIN(YEAR(tgl_so)) as min_tahun, MAX(YEAR(tgl_so)) as max_tahun')
					->from('tm_salesorder')
					->get()->row_array();
		$selisihTahun = $tahunInit['max_tahun'] - $tahunInit['min_tahun'];
		$data['tahun'][] = $tahunInit['min_tahun']; 
		for($i = 0; $i < $selisihTahun; $i++){
			$data['tahun'][] = $data['tahun'][$i] + 1;
		}

		$data['sales_person'] = $this->m_salesperson->get_all();
		$data['tipe_penjualan'] = 'Ekspor';
		$data['status_item'] = array(
			'std' => 'Standart',
			'custom' => 'Custom'
		);		
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	/* 
	* Untuk menampilkan report invoice eksport
	*/
	public function table_main()
	{
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$tahun = $this->input->get('tahun', true);
		$bulan = $this->input->get('bulan', true);
		$tipeCustomer = $this->input->get('tipe_customer', true);
		$inv = $this->model_salesorder->report_sales_ekspor_invoice($tahun, $bulan, $tipeCustomer);
		$data['resultInvoice'] = $inv;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}
}