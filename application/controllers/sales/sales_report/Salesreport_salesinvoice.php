<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Salesreport_salesinvoice extends MY_Controller {
	private $class_link = 'sales/sales_report/salesreport_salesinvoice';

	public function __construct() {
		parent::__construct();
		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(array('model_salesorder', 'm_salesperson'));
    }

    public function index() {
        parent::administrator();
        $this->table_box();
    }
	 
	public function table_box(){

		// Initiate bulan dan tahun ambil dari database
		$data['bulan'] = [1,2,3,4,5,6,7,8,9,10,11,12];
		$tahunInit = $this->db->select('MIN(YEAR(tgl_so)) as min_tahun, MAX(YEAR(tgl_so)) as max_tahun')
					->from('tm_salesorder')
					->get()->row_array();
		$selisihTahun = $tahunInit['max_tahun'] - $tahunInit['min_tahun'];
		$data['tahun'][] = $tahunInit['min_tahun']; 
		for($i = 0; $i < $selisihTahun; $i++){
			$data['tahun'][] = $data['tahun'][$i] + 1;
		}

		$data['sales_person'] = $this->m_salesperson->get_all();
		$data['tipe_penjualan'] = 'Lokal';
		$data['status_item'] = array(
			'std' => 'Standart',
			'custom' => 'Custom'
		);		
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
		$bulan = $this->input->get('bulan', true);
        $tahun = $this->input->get('tahun', true);
		$salesPerson = $this->input->get('sales_person', true);
        $itemStatus = $this->input->get('item_status', true);
		$tipeCustomer = $this->input->get('tipe_customer', true);
		$salesInvoice = $this->model_salesorder->report_sales_invoice($tahun, $bulan, $salesPerson, $tipeCustomer);
		$i = 0;
		$discItemStandart = 0;
		$discItemCustom = 0;
		$totalDisc = 0;
		$dpItemStandart = 0;
		$dpItemCustom = 0;
		$totalDP = 0;
		// Note ... variabel $data adalah yang akan tampil di report, inv adalah data dari database ... filter std dan custom by foreach di php
		foreach($salesInvoice as $inv):
			$data['resultInvoice'][$i]['no_salesorder'] = $inv['no_salesorder'];
			$data['resultInvoice'][$i]['nm_salesperson'] = $inv['nm_salesperson'];
			$data['resultInvoice'][$i]['tgl_so'] = $inv['tgl_so'];
			$data['resultInvoice'][$i]['tgl_kirim'] = $inv['tgl_kirim'];
			$data['resultInvoice'][$i]['tgl_invoice'] = $inv['tgl_invoice'];
			$data['resultInvoice'][$i]['tgl_termin'] = $inv['tgl_termin'];
			$data['resultInvoice'][$i]['tgl_dp'] = $inv['tgl_dp'];
			$data['resultInvoice'][$i]['code_customer'] = $inv['code_customer'];
			$data['resultInvoice'][$i]['nm_customer'] = $inv['nm_customer'];
			$data['resultInvoice'][$i]['no_invoice'] = $inv['no_invoice'];
			$data['resultInvoice'][$i]['do_ket'] = $inv['do_ket'];
			// Menampung total ammount harga std dan custom
			$totalStdCustomHarga = $inv['item_standart_harga'] + $inv['item_custom_harga'];
			// Filter item standart dan custom berasal dari inputan get user
			if($itemStatus == "std" ):
				$inv['item_custom_harga'] = 0;
			elseif($itemStatus == "custom"):
				$inv['item_standart_harga'] = 0;
			endif;
			// IF untuk antisipasi divide by zero (pembagian nol)
			if(($totalStdCustomHarga) != 0 ):
				// Proposional disc
				// Filter item standart dan custom berasal dari inputan get user
				if($itemStatus == "std" ):
					$discItemStandart = $inv['item_standart_harga'] / ($totalStdCustomHarga) * $inv['total_nilai_disc'];
				elseif($itemStatus == "custom"):
					$discItemCustom = $inv['item_custom_harga'] / ($totalStdCustomHarga) * $inv['total_nilai_disc'];
				else:
					$discItemStandart = $inv['item_standart_harga'] / ($totalStdCustomHarga) * $inv['total_nilai_disc'];
					$discItemCustom = $inv['item_custom_harga'] / ($totalStdCustomHarga) * $inv['total_nilai_disc'];
				endif;
				$totalDisc = $discItemStandart + $discItemCustom;
			endif;
			// IF untuk antisipasi divide by zero (pembagian nol)
			if(($totalStdCustomHarga) != 0 ):
				// Proposional DP
				if($itemStatus == "std"):
					if($inv['status_invoice'] == "DP"):
						$dpItemStandart = $inv['item_standart_harga'] / ($totalStdCustomHarga) * $inv['jml_dp'];
					elseif ($inv['status_invoice'] == "Termin"):
						$dpItemStandart = $inv['item_standart_harga'] / ($totalStdCustomHarga) * $inv['jml_termin'];
					endif;
				elseif($itemStatus == "custom"):
					if($inv['status_invoice'] == "DP"):
						$dpItemCustom = $inv['item_custom_harga'] / ($totalStdCustomHarga) * $inv['jml_dp'];
					elseif($inv['status_invoice'] == "Termin"):
						$dpItemCustom = $inv['item_custom_harga'] / ($totalStdCustomHarga) * $inv['jml_termin'];
					endif;
				else:
					if($inv['status_invoice'] == "DP"):
						$dpItemStandart = $inv['item_standart_harga'] / ($totalStdCustomHarga) * $inv['jml_dp'];
						$dpItemCustom = $inv['item_custom_harga'] / ($totalStdCustomHarga) * $inv['jml_dp'];
					elseif($inv['status_invoice'] == "Termin"):
						$dpItemStandart = $inv['item_standart_harga'] / ($totalStdCustomHarga) * $inv['jml_termin'];
						$dpItemCustom = $inv['item_custom_harga'] / ($totalStdCustomHarga) * $inv['jml_termin'];
					endif;
				endif;
				$totalDP = $dpItemStandart + $dpItemCustom;
			endif;
			// Jika status invoice DP maka total harga exc diisi field jml_dp
			if($inv['status_invoice'] == "DP" || $inv['status_invoice'] == "Termin"):
				$data['resultInvoice'][$i]['total_harga_exc'] = $totalDP;
			else:
				$data['resultInvoice'][$i]['total_harga_exc'] = $inv['item_standart_harga'] + $inv['item_custom_harga'] - $totalDisc + $inv['jml_ongkir'] + $inv['jml_install'] - ($inv['jml_dp'] + $inv['jml_termin']);
				if($data['resultInvoice'][$i]['total_harga_exc'] < 0 ):
					$data['resultInvoice'][$i]['total_harga_exc'] = 0;
				endif;
			endif;
			$data['resultInvoice'][$i]['total_harga_inc'] = $data['resultInvoice'][$i]['total_harga_exc'] + (($data['resultInvoice'][$i]['total_harga_exc'] * $inv['jml_ppn'])/100);
			$data['resultInvoice'][$i]['status_invoice'] = $inv['status_invoice'];
			$i++;
		endforeach;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }

	// public function test()
	// {
	// 	$bulan = $this->input->get('bulan', true);
    //     $tahun = $this->input->get('tahun', true);
	// 	$salesPerson = $this->input->get('sales_person', true);
    //     $itemStatus = $this->input->get('item_status', true);
	// 	$tipeCustomer = $this->input->get('tipe_customer', true);
	// 	$salesInvoice = $this->model_salesorder->report_sales_invoice($tahun, $bulan, $salesPerson, $tipeCustomer);
	// 	echo json_encode($salesInvoice);
	// }

}
