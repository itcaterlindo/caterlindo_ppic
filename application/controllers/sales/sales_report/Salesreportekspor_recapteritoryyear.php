<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Salesreportekspor_recapteritoryyear extends MY_Controller {
	private $class_link = 'sales/sales_report/salesreportekspor_recapteritoryyear';

	public function __construct() {
		parent::__construct();
		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(array('model_salesorder', 'm_salesperson', 'tb_default_disc', 'db_hrm/tb_hari_libur', 'db_hrm/tb_hari_efektif', 'model_do', 'tm_deliveryorder', 'td_salesorder_item_disc', 'tm_salesorder', 'm_customer', 'tb_teritory', 'tm_deliveryorder'));
    }

    public function index() {
        parent::administrator();
		parent::chartjs_assets();
        $this->table_box();
    }
	 
	public function table_box(){

		// Tahun ambil dari database
		$tahunInit = $this->db->select('MIN(YEAR(tgl_so)) as min_tahun, MAX(YEAR(tgl_so)) as max_tahun')
					->from('tm_salesorder')
					->get()->row_array();
		$selisihTahun = $tahunInit['max_tahun'] - $tahunInit['min_tahun'];
		$data['tahun'][] = $tahunInit['min_tahun']; 
		for($i = 0; $i < $selisihTahun; $i++){
			$data['tahun'][] = $data['tahun'][$i] + 1;
		}
		$data['tipe_penjualan'] = 'Ekspor';	
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main()
	{
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$tahun = $this->input->get('tahun', true);
		$tipeCustomer = $this->input->get('tipe_customer', true);
		$recapInv = [];
		// Get all teritory untuk di bandingkan dengan hasil group by
		$teritory = $this->tb_teritory->get_all();
		for($bulan = 1; $bulan <= 12; $bulan++ ):
			// Get RAW Invoice
			$inv = $this->model_salesorder->report_sales_ekspor_invoice($tahun, $bulan, $tipeCustomer);
			if(empty($inv)):
				break;
			endif;
			// Group BY Teritory
			$groupByTeritory = $this->group_by("teritory", $inv);
			$sumTeritory = [];
			// Perbandingan teritory dengan group by
			$i = 0;
			foreach($teritory as $ter):
				if(array_key_exists($ter->nm_teritory, $groupByTeritory)):
					$sumTeritory[$i]['teritory'] = $ter->nm_teritory;
					$sumTeritory[$i]['ammount_distributor'] = ROUND(array_sum(array_column($groupByTeritory[$ter->nm_teritory], "ammount_distributor")), 2);
					$sumTeritory[$i]['container_20ft'] = array_sum(array_column($groupByTeritory[$ter->nm_teritory], "container_20ft"));
				else:
					$sumTeritory[$i]['teritory'] = $ter->nm_teritory;
					$sumTeritory[$i]['ammount_distributor'] = 0;
					$sumTeritory[$i]['container_20ft'] = 0;
				endif;
				$i++;
			endforeach;
			$recapInv[$bulan] = $sumTeritory;
		endfor;
		// RECAP PER BULAN
		$arrBulan = [
			1 => 'januari',
			2 => 'februari',
			3 => 'maret',
			4 => 'april',
			5 => 'mei',
			6 => 'juni',
			7 => 'juli',
			8 => 'agustus',
			9 => 'september',
			10 => 'oktober',
			11 => 'november',
			12 => 'desember',
		];
		$resultTeritory = [];
		$iHeader = 0;
		foreach($teritory as $ter):
			$resultTeritory[$iHeader]['teritory'] = $ter->nm_teritory;
			for($i = 1; $i <= 12; $i++ ):
				// Cek jika data tidak ada maka perulangan for untuk get data child berhenti
				if(!isset($recapInv[$i])):
					break;
				endif;
				foreach($recapInv[$i] as $recap):
					if($recap['teritory'] == $ter->nm_teritory):
						$resultTeritory[$iHeader][$arrBulan[$i]]['ammount_distributor'] = $recap['ammount_distributor'];
						$resultTeritory[$iHeader][$arrBulan[$i]]['container_20ft'] = $recap['container_20ft'];
					endif;
				endforeach;
			endfor;
			$iHeader++;
		endforeach;
		// END RECAP PER BULAN
		$data['resultTeritory'] = $resultTeritory;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
		// echo json_encode($data['resultTeritory']);
	}

	/**
	 * Function that groups an array of associative arrays by some key.
	 * 
	 * @param {String} $key Property to sort by.
	 * @param {Array} $data Array that stores multiple associative arrays.
	 */
	private function group_by($key, $data) {
		$result = array();
		foreach($data as $val) {
			if(array_key_exists($key, $val)){
				$result[$val[$key]][] = $val;
			}else{
				$result[""][] = $val;
			}
		}
		return $result;
	}

}