<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Data_do extends MY_Controller
{
	private $class_link = 'sales/data_do';
	private $form_errs = array('idErrTglDo', 'idErrJmlKoli');

	public function __construct()
	{
		parent::__construct();

		$this->load->library(array('ssp', 'form_validation'));
		$this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper', 'upload_conf_helper'));
		$this->load->model(array(
			'm_builder', 'tm_deliveryorder', 'model_salesorder', 'db_caterlindo/tm_po', 'tb_notifyparty', 'model_do', 'tb_default_disc', 'm_format_laporan',
			'tm_salesorder', 'td_customer_alamat', 'td_do_item_reportunit', 'tb_container', 'td_do_item', 'tm_gudang', 'td_salesorder_item', 'td_salesorder_item_detail', 'tm_item_group', 'tm_barang'
		));
	}

	public function index()
	{
		parent::administrator();
		parent::select2_assets();
		parent::icheck_assets();
		parent::typeahead_assets();
		parent::datetimepicker_assets();
		parent::pnotify_assets();
		$this->get_table();
	}

	public function get_table()
	{
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/table_box', $data);
	}

	public function open_table()
	{
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/table_main', $data);
	}

	public function table_data()
	{
		$data = $this->tm_deliveryorder->ssp_table();
		echo json_encode(
			SSP::simple($_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'])
		);
	}

	public function get_form()
	{
		$data['id'] = $this->input->get('id');
		$data['msalesorder_kd'] = $this->input->get('kd_salesorder');
		$data['tipe_do'] = $this->input->get('tipe');
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = $this->form_errs;
		$this->load->view('page/' . $this->class_link . '/form_box', $data);
	}

	public function open_form()
	{
		$id = $this->input->get('id');
		$kd_msalesorder = $this->input->get('kd_msalesorder');

		$data = $this->tm_deliveryorder->get_row($id);
		$so = $this->tm_salesorder->get_row($kd_msalesorder);
		/** Opsi Detail */
		$actopsiAlamatKirim = $this->td_customer_alamat->get_by_param(['customer_kd' => $so->customer_kd])->result_array();
		$opsiAlamatKirim[''] = '-- Pilih Opsi --';
		foreach ($actopsiAlamatKirim as $eachAlamatKirim) :
			$opsiAlamatKirim[$eachAlamatKirim['kd_alamat_kirim']] = $eachAlamatKirim['alamat'];
		endforeach;

		/** UNTUK PARSING CONTAINER KE VIEW FORM */
		$arrContainer = $this->tb_container->get_all();
		$container[''] = "-- Silahkan Pilih Container --";
		foreach ($arrContainer as $con) :
			$container[$con->id] = $con->nm_container . " - " . $con->deskripsi;
		endforeach;

		/** UNTUK PARSING WAREHOUSE/GUDANG KE VIEW FORM */
		$arrGudang = $this->tm_gudang->get_all()->result();
		foreach ($arrGudang as $gud) :
			$gudang[$gud->kd_gudang] = $gud->nm_gudang;
		endforeach;

		$data['class_link'] = $this->class_link;
		$data['msalesorder_kd'] = empty($data['msalesorder_kd']) ? $this->input->get('kd_msalesorder') : $data['msalesorder_kd'];
		$data['tipe_do'] = empty($data['tipe_do']) ? $this->input->get('tipe') : $data['tipe_do'];
		$data['notify_opts'] = render_dropdown('Notify Name', $this->tb_notifyparty->get_all(), 'nm_notifyparty', 'nm_notifyparty');
		$data['so'] = $this->tm_salesorder->get_row($kd_msalesorder);
		$data['opsiAlamatKirim'] = $opsiAlamatKirim;
		$data['container'] = $container;
		$data['gudang'] = $gudang;


		$this->load->view('page/' . $this->class_link . '/form_main', $data);
	}

	public function get_notify_address()
	{
		$notify_name = $this->input->get('notify_name');
		$str['notify_address'] = $this->tb_notifyparty->get_notify_address($notify_name);

		header('Content-Type: application/json');
		echo json_encode($str);
	}

	public function send_data()
	{
		if ($this->input->is_ajax_request()) :
			$this->form_validation->set_rules($this->tm_deliveryorder->form_rules());
			if ($this->form_validation->run() == FALSE) :
				$str = $this->tm_deliveryorder->build_warning($this->form_errs);
				$str['confirm'] = 'error';
			else :
				/* --Start variabel untuk input kedalam db_caterlindo-- */
				$row_mso = $this->model_salesorder->read_masterdata($this->input->post('txtKdMSo'));
				$detail['fc_nopo'] = $row_mso['no_master_so'];
				$detail['fd_tglstaffing'] = format_date($this->input->post('txtTglDo'), 'Y-m-d');
				$detail['fc_userid'] = $this->session->usename;
				$detail['fv_ket'] = $this->input->post('txtNoKendaraan');
				/* --End variabel untuk input kedalam db_caterlindo-- */

				/* --Start variabel untuk input kedalam db_caterlindo_ppic-- */
				$data['kd_mdo'] = $this->input->post('txtKdMDo');
				$data['msalesorder_kd'] = $this->input->post('txtKdMSo');
				$data['tipe_do'] = $this->input->post('txtTipeDo');
				$data['notify_name'] = $this->input->post('selNotify');
				$data['notify_address'] = $this->input->post('txtNotifyAddress');
				$data['nm_jasakirim'] = $this->input->post('txtFreight');
				$data['alamat_jasakirim'] = $this->input->post('txtAlamatJasaKirim');
				$data['do_trucknumber'] = $this->input->post('txtNoKendaraan');
				$data['seal_number'] = $this->input->post('txtSealNumber');
				$data['container_number'] = $this->input->post('txtContainerNumber');
				$data['do_ket'] = $this->input->post('txtKeterangan');
				$data['gudang_kd'] = $this->input->post('selGudang');
				$data['goods_desc'] = $this->input->post('txtGoodDesc');
				$data['vessel'] = $this->input->post('txtVessel');
				$data['etd_sub'] = format_date($this->input->post('txtEtdSub'), 'Y-m-d');
				$data['etd_sin'] = format_date($this->input->post('txtEtdSin'), 'Y-m-d');
				$data['eta'] = format_date($this->input->post('txtEta'), 'Y-m-d');
				$data['container_arrived'] = format_date($this->input->post('txtContainerArrived'), 'H:i:s');
				$data['no_bl'] = $this->input->post('txtNoBl');
				$data['jml_koli'] = $this->input->post('txtJmlKoli');
				$data['tipe_container'] = $this->input->post('txtTipeContainer');
				$data['container_20ft'] = $this->input->post('txtContainer20ft');
				$alamat_kirim_kd = $this->input->post('selAlamatKirim');
				if (!empty($_FILES['fileAttachment']['name'])) :
					$upload = $this->upload_file('fileAttachment');
					if ($upload['confirm'] == 'error') :
						header('Content-Type: application/json');
						echo json_encode($upload);
						exit();
					else :
						$data['do_attachment'] = $upload['upload_data']['file_name'];
					endif;
				else :
					$data['do_attachment'] = $this->input->post('txtFileAttachment');
				endif;
				$data['tgl_do'] = format_date($this->input->post('txtTglDo'), 'Y-m-d');
				$data['tgl_stuffing'] = format_date($this->input->post('txtTglStuffing'), 'Y-m-d');
				$data['status_do'] = 'pending';
				/* --End variabel untuk input kedalam db_caterlindo_ppic-- */

				/* --Action submit dan report-- */
				$act = $this->tm_po->submit_data($detail);
				if ($act['confirm'] == 'success') :
					/** Update Alamat Kirim */
					$actUpdateAlmKirim = $this->tm_salesorder->update_data(['alamat_kirim_kd' => !empty($alamat_kirim_kd) ? $alamat_kirim_kd : null], ['kd_msalesorder' => $data['msalesorder_kd']]);

					if (empty($data['kd_mdo'])) :
						$str = $this->tm_deliveryorder->store_in_session($data);
						$str['open_item'] = 'yes';
					else :
						$str = $this->tm_deliveryorder->submit_data($data, $data['tipe_do']);
						$str['open_item'] = 'yes';
					endif;
				elseif ($act['confirm'] == 'error') :
					header('Content-Type: application/json');
					echo json_encode($act);
				endif;
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function delete_data()
	{
		if ($this->input->is_ajax_request()) :
			$str = $this->tm_deliveryorder->delete_data($this->input->get('id'));
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function upload_file($file_upload = '')
	{
		$this->load->library('upload', do_upload_config('./assets/admin_assets/dist/img/attachment_do/', 'docx|xlsx|doc|xls|pdf'));
		if (!$this->upload->do_upload($file_upload)) :
			$data = array('confirm' => 'error', 'idErrAttachment' => buildLabel('warning', $this->upload->display_errors('', '')), 'csrf' => $this->security->get_csrf_hash());
		else :
			$data = array('upload_data' => $this->upload->data());
		endif;
		return $data;
	}

	public function get_detail()
	{
		$data['kd_mdo'] = $this->input->get('id');
		/* --Get data detail-- */
		$row = $this->tm_deliveryorder->get_row($data['kd_mdo']);
		$data['no_invoice'] = $row['no_invoice'];
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/detail_box', $data);
	}

	public function open_detail()
	{
		$this->load->model(array('td_salesorder_item_disc'));
		$kd_mdo = $this->input->get('id');
		/* --Detail Data, fetch from model-- */
		$data = $this->item_data($kd_mdo);
		/* --Load main view and return it to browser-- */
		$data['kd_mdo_ori'] = $kd_mdo;
		$this->load->view('page/' . $this->class_link . '/detail_main', $data);
	}

	public function get_invoice_form()
	{
		$data['id'] = $this->input->get('id');
		$data['no_invoice'] = $this->tm_deliveryorder->get_no_invoice($data['id']);
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = array('idErrInvoice');
		$this->load->view('page/' . $this->class_link . '/invoice_box', $data);
	}

	public function open_invoice_form()
	{
		$data['class_link'] = $this->class_link;
		$data = $this->tm_deliveryorder->get_row($this->input->get('id'));
		$this->load->view('page/' . $this->class_link . '/invoice_main', $data);
	}

	public function send_invoice_data()
	{
		if ($this->input->is_ajax_request()) :
			$this->form_validation->set_rules($this->tm_deliveryorder->invoice_form_rules());
			if ($this->form_validation->run() == FALSE) :
				$str = $this->tm_deliveryorder->invoice_build_warning(array('idErrInvoice'));
				$str['confirm'] = 'error';
			else :
				$data['kd_mdo'] = $this->input->post('txtKdMDo');
				$data['no_invoice'] = $this->input->post('txtInvoice');
				/** Jika no invoice yang di input tidak ada maka generate baru code ar untuk SAP */
				$cekInv = $this->tm_deliveryorder->get_by_param(['no_invoice' => $data['no_invoice']])->row_array();
				if (empty($cekInv)) :
					$data['invoicearsap_kd'] = $this->tm_deliveryorder->create_code_ar_sap();
				else :
					/** Jika no invoice yang di input ada maka ambil code ar SAP yang sama dengan no invoice tersebut */
					$data['invoicearsap_kd'] = $cekInv['invoicearsap_kd'];
				endif;
				$str = $this->tm_deliveryorder->submit_invoice_data($data);
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function data_cetak($kd_mdo = '')
	{
		parent::admin_print();
		return $this->item_data($kd_mdo);
	}

	public function item_data($kd_mdo = '')
	{
		/* --Detail Data, fetch from model-- */
		$no_invoice = $this->tm_deliveryorder->get_no_invoice($kd_mdo);
		$lists_kd = $this->tm_deliveryorder->get_same_kd($no_invoice);
		if (!empty($lists_kd)) :
			$no = 0;
			foreach ($lists_kd as $list) :
				$no++;
				if ($no == 1) :
					$first_kd = $list->kd_mdo;
				else :
					$first_kd = $first_kd;
				endif;
				$list_kd[] = $list->kd_mdo;
				$list_kdso[] = $list->msalesorder_kd;
				$list_nopo[] = $list->no_po;
			endforeach;
		else :
			$list_kd = array('');
			$list_kdso = array('');
			$list_nopo = array('');
			$first_kd = '';
		endif;

		$data = $this->model_do->get_header_detail($first_kd);
		$m_detail = $this->model_salesorder->read_masterdata($data['msalesorder_kd']);
		if ($data['tipe_do'] == 'Ekspor') :
			$invoice_tipe = 'invoiceekspor';
		elseif ($data['tipe_do'] == 'Lokal') :
			$invoice_tipe = 'invoicelocal';
		endif;
		$data['format_laporan'] = $this->m_format_laporan->view_laporan($invoice_tipe, $m_detail['term_payment_format']);
		$data['parent_items'] = $this->model_do->list_parent_items($list_kd, '', $data['tipe_do']);
		$data['child_items'] = $this->model_do->list_child_items($list_kd, '', $data['tipe_do']);
		$data['list_nopo'] = $list_nopo;
		/* --Load main view and return it to browser-- */
		return $data;
	}

	public function cetak_packing_list($tipe = '', $kd_mdo = '')
	{
		$data = $this->data_cetak($kd_mdo);
		$data['tipe_laporan'] = $tipe;
		$this->load->view('page/' . $this->class_link . '/report/packing_list', $data);
	}

	public function cetak_shipping_instruction($kd_mdo = '')
	{
		$data = $this->data_cetak($kd_mdo);
		$this->load->view('page/' . $this->class_link . '/report/shipping_instruction', $data);
	}

	public function cetak_picking_slip($kd_mdo = '')
	{
		$data = $this->data_cetak($kd_mdo);
		$this->load->view('page/' . $this->class_link . '/report/picking_slip', $data);
	}

	public function cetak_do($kd_mdo = '')
	{
		$data = $this->data_cetak($kd_mdo);

		$no_invoice = $this->tm_deliveryorder->get_no_invoice($kd_mdo);
		$lists_kd = $this->tm_deliveryorder->get_same_kd($no_invoice);
		if (!empty($lists_kd)) :
			$no = 0;
			foreach ($lists_kd as $list) :
				$no++;
				$list_kd[] = $list->kd_mdo;
			endforeach;
		else :
			$list_kd = array('');
		endif;
		$data['parent_items'] = $this->model_do->list_parent_items($list_kd);
		$data['child_items'] = $this->model_do->list_child_items($list_kd);

		$this->load->view('page/' . $this->class_link . '/report/delivery_order', $data);
	}

	public function cetak_do_xml($kd_mdo = '')
	{
		// $data = $this->data_cetak($kd_mdo);
		$data = $this->item_data($kd_mdo);
		$row = $this->tm_deliveryorder->get_row($data['kd_mdo']);

		$no_invoice = $this->tm_deliveryorder->get_no_invoice($kd_mdo);
		$lists_kd = $this->tm_deliveryorder->get_same_kd($no_invoice);
		if (!empty($lists_kd)) :
			$no = 0;
			foreach ($lists_kd as $list) :
				$no++;
				$list_kd[] = $list->kd_mdo;
			endforeach;
		else :
			$list_kd = array('');
		endif;
		// $data['parent_items'] = $this->model_do->list_parent_items($list_kd);
		// $data['child_items'] = $this->model_do->list_child_items($list_kd);

		// echo json_encode($data['npwp_customer'] );
		// echo json_encode($data['parent_items'] );
		// echo json_encode($data );

		header('Content-Type: text/xml');
		// header('Content-Type: application/xml');
		$this->load->view('page/' . $this->class_link . '/report/delivery_order_xml', $data);
		
		// $output = "<Bikes><Name>Ducati</Name></Bikes>";
		// echo $output;
	}

	public function ubah_status()
	{
		if ($this->input->is_ajax_request()) :
			$this->db->trans_begin();
			$id = $this->input->get('id');
			$status = $this->input->get('status');
			$str = $this->tm_deliveryorder->ubah_status($id, $status);
			$label = "Mengubah status delivery order";
			if ($this->db->trans_status() == FALSE) :
				$this->db->trans_rollback();
				$str['confirm'] = 'error';
				$str['alert'] = buildAlert('danger', 'Gagal!', $label . ' kesalahan sistem!');
			else :
				if ($status == "finish") :
					/** Push to SAP */
					$api = $this->send_ar_invoice_to_sap($id);
					if ($api[0]->ErrorCode == 0) :
						$this->db->trans_commit();
						$str['confirm'] = 'success';
						$str['alert'] = buildAlert('success', 'Berhasil!', $label . '! API ' . $api[0]->Message);
						$str['data'] = $api[0]->Data;
					else :
						$this->db->trans_rollback();
						$str['confirm'] = 'error';
						$str['alert'] = buildAlert('danger', 'Gagal!', $label . ' kesalahan sistem! API ' . $api[0]->Message);
						$str['data'] = $api[0]->Data;
					endif;
					/** End push to SAP */
				else :
					$this->db->trans_commit();
					$str['confirm'] = 'success';
					$str['alert'] = buildAlert('success', 'Berhasil!', $label . '!');
				endif;
			endif;
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function strukturbiayaunit_box()
	{
		parent::administrator();
		parent::datetimepicker_assets();
		parent::pnotify_assets();
		$kd_mdo = $this->input->get('kd_mdo', true);

		$data['kd_mdo'] = $kd_mdo;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/strukturbiayaunit_box', $data);
	}

	public function strukturbiayaunit_read()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$kd_mdo = $this->input->get('kd_mdo', true);
		$slug = $this->input->get('slug', true);
		$slug = !empty($slug) ? $slug : 'read';

		if ($slug == 'read') {
			$data['do'] = $this->db->select()
				->from('tm_deliveryorder as a')
				->join('tm_salesorder as b', 'b.kd_msalesorder=a.msalesorder_kd', 'left')
				->where('a.kd_mdo', $kd_mdo)
				->get()->row_array();
		}
		$data['slug'] = $slug;
		$data['kd_mdo'] = $kd_mdo;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/strukturbiayaunit_read', $data);
	}

	function strukturbiayaunit_form()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$kd_mdo = $this->input->get('kd_mdo', true);
		$data['do'] = $this->db->select()
			->from('tm_deliveryorder as a')
			->join('tm_salesorder as b', 'b.kd_msalesorder=a.msalesorder_kd', 'left')
			->where('a.kd_mdo', $kd_mdo)
			->get()->row_array();
		$data['kd_mdo'] = $kd_mdo;
		$this->load->view('page/' . $this->class_link . '/strukturbiayaunit_form', $data);
	}

	public function strukturbiayaunit_view()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$kd_mdo = $this->input->get('kd_mdo');

		$data['url'] = base_url() . $this->class_link . '/struktur_biaya_pdf?kd_mdo=' . $kd_mdo;
		$data['class_link'] = $this->class_link;

		$this->load->view('page/' . $this->class_link . '/strukturbiayaunit_view', $data);
	}

	public function strukturbiayaunit_group_hscode_view()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$kd_mdo = $this->input->get('kd_mdo');

		$data['url'] = base_url() . $this->class_link . '/struktur_biaya_group_hscode_pdf?kd_mdo=' . $kd_mdo;
		$data['class_link'] = $this->class_link;

		$this->load->view('page/' . $this->class_link . '/strukturbiayaunitgrouphscode_view', $data);
	}

	public function strukturbiayaunitpartstatus_box()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$kd_mdo = $this->input->get('kd_mdo');
		$data['class_link'] = $this->class_link;
		$data['kd_mdo'] = $kd_mdo;
		$this->load->view('page/' . $this->class_link . '/strukturbiayaunitpartstatus_box', $data);
	}

	public function strukturbiayaunitpartstatus_table()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$kd_mdo = $this->input->get('kd_mdo');

		/** Check DO banyak SO */
		$DOkode = $this->tm_deliveryorder->get_by_param(['kd_mdo' => $kd_mdo])->row_array();
		$DOinv = $this->tm_deliveryorder->get_by_param(['no_invoice' => $DOkode['no_invoice']])->result_array();

		$data['class_link'] = $this->class_link;
		$data['results'] = $this->db->select('f.kd_barang, f.item_code, f.deskripsi_barang, f.dimensi_barang, g.kd_kat_barang, g.nm_kat_barang, i.partmain_nama, h.*, j.partstate_nama, j.partstate_label, g.nm_kat_barang')
			->from('td_do_item_reportunit as a')
			->join('tm_barang as f', 'f.kd_barang=a.kd_barang', 'left')
			->join('tm_kat_barang as g', 'g.kd_kat_barang=f.kat_barang_kd', 'left')
			->join('td_part as h', 'h.part_kd=a.part_kd', 'left')
			->join('tm_part_main as i', 'i.partmain_kd=h.partmain_kd', 'left')
			->join('tb_part_state as j', 'j.partstate_kd=h.partstate_kd', 'left')
			->where_in('a.kd_mdo', array_column($DOinv, 'kd_mdo'))
			->group_by('g.kd_kat_barang, a.kd_barang, h.part_kd')
			->get()->result_array();
		$this->load->view('page/' . $this->class_link . '/strukturbiayaunitpartstatus_table', $data);
	}

	public function get_currency()
	{
		$this->load->model(['_kursfiskal_configuration']);
		$tgl_currency = $this->input->get('tgl_currency', true);

		$currency = $this->db->select()
			->where('currency_nm', 'US Dollar')
			->get('tm_currency')->row_array();
		$row = $this->_kursfiskal_configuration->getby_kdcurrency($currency['kd_currency'], $tgl_currency);

		header('Content-Type: application/json');
		echo json_encode($row);
	}

	public function action_submit_biayastrukturunit()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtkd_mdo', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtcurrency_rp', 'Currency', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtbiaya_angkut', 'Biaya', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrcurrency_rp' => (!empty(form_error('txtkd_mdo'))) ? buildLabel('warning', form_error('txtkd_mdo', '"', '"')) : '',
				'idErrcurrency_rp' => (!empty(form_error('txtcurrency_rp'))) ? buildLabel('warning', form_error('txtcurrency_rp', '"', '"')) : '',
				'idErrbiaya_angkut' => (!empty(form_error('txtbiaya_angkut'))) ? buildLabel('warning', form_error('txtbiaya_angkut', '"', '"')) : '',
			);
		} else {
			$kd_mdo = $this->input->post('txtkd_mdo', true);
			$currency_rp = $this->input->post('txtcurrency_rp', true);
			$biaya_angkut = $this->input->post('txtbiaya_angkut', true);

			$data = array(
				'currency_rp' => $currency_rp,
				'biaya_angkut' => $biaya_angkut,
				'tgl_edit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);
			$this->db->trans_begin();
			$DOkode = $this->tm_deliveryorder->get_by_param(['kd_mdo' => $kd_mdo])->row_array();

			$act = $this->tm_deliveryorder->update_data(['no_invoice' => $DOkode['no_invoice']], $data);

			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			} else {
				$this->db->trans_commit();
				$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_generatereport_strukturbiaya()
	{
		$kd_mdo = $this->input->get('kd_mdo', true);

		$qItems = 'SELECT a.kd_ditem_do, d.kd_kat_barang, b.barang_kd, b.item_status, b.item_code, b.item_desc, b.item_dimension, d.nm_kat_barang, c.hs_code, "parent" AS sts,
			g.partmain_kd, g.part_kd
			FROM td_do_item AS a
			LEFT JOIN td_salesorder_item AS b ON b.kd_ditem_so=a.parent_kd
			LEFT JOIN tm_barang AS c ON c.kd_barang=b.barang_kd
			LEFT JOIN tm_kat_barang AS d ON d.kd_kat_barang=c.kat_barang_kd

			LEFT JOIN tm_bom AS e ON e.kd_barang=c.kd_barang
			LEFT JOIN td_bom_detail AS f ON f.bom_kd=e.bom_kd

			LEFT JOIN tb_part_lastversion AS g ON g.partmain_kd=f.partmain_kd

			WHERE a.mdo_kd = "' . $kd_mdo . '"
			AND a.parent_kd IS NOT NULL AND a.child_kd IS NULL

			UNION ALL
			
			SELECT a.kd_ditem_do, d.kd_kat_barang, b.kd_child AS barang_kd, b.item_status, b.item_code, b.item_desc, b.item_dimension, d.nm_kat_barang, c.hs_code, "child" AS sts,
			g.partmain_kd, g.part_kd
			FROM td_do_item AS a
			LEFT JOIN td_salesorder_item_detail AS b ON b.kd_citem_so=a.child_kd
			LEFT JOIN tm_barang AS c ON c.kd_barang=b.kd_child
			LEFT JOIN tm_kat_barang AS d ON d.kd_kat_barang=c.kat_barang_kd

			LEFT JOIN tm_bom AS e ON e.kd_barang=c.kd_barang
			LEFT JOIN td_bom_detail AS f ON f.bom_kd=e.bom_kd

			LEFT JOIN tb_part_lastversion AS g ON g.partmain_kd=f.partmain_kd

			WHERE a.mdo_kd = "' . $kd_mdo . '"
			AND a.child_kd IS NOT NULL';

		$items = $this->db->query($qItems)->result_array();

		if (!empty($items)) {
			$doitemrepunit_kd = $this->td_do_item_reportunit->create_code();
			foreach ($items as $item) {
				$data[] = [
					'doitemrepunit_kd' => $doitemrepunit_kd,
					'kd_mdo' => $kd_mdo,
					'kd_ditem_do' => $item['kd_ditem_do'],
					'kd_barang' => $item['barang_kd'],
					'partmain_kd' => $item['partmain_kd'],
					'part_kd' => $item['part_kd'],
					'doitemrepunit_tglinput' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				];
				$doitemrepunit_kd++;
			}
			$actDel = $this->td_do_item_reportunit->delete_by_param(['kd_mdo' => $kd_mdo]);
			$act = $this->td_do_item_reportunit->insert_batch($data);
			$actDO = $this->tm_deliveryorder->update_data(['kd_mdo' => $kd_mdo], ['tgl_generatebiayaunit' => date('Y-m-d H:i:s')]);
		}
		$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');

		header('Content-Type: application/json');
		echo json_encode($resp);
	}

	public function struktur_biaya_pdf()
	{
		$this->load->library('Pdf');

		$kd_mdo = $this->input->get('kd_mdo', true);

		$doitemreports = $this->db->select('b.kat_barang_kd')
			->from('td_do_item_reportunit as a')
			->join('tm_barang as b', 'b.kd_barang=a.kd_barang', 'left')
			->where('a.kd_mdo', $kd_mdo)
			->group_by('b.kat_barang_kd')
			->get()->result_array();
		$data['doitemreports'] = $doitemreports;
		foreach ($doitemreports as $doitemreport) {
			$dataKonten = $this->td_do_item_reportunit->generatereport_strukturbiaya($kd_mdo, $doitemreport['kat_barang_kd']);
			$data['kontens'][$doitemreport['kat_barang_kd']] = $this->load->view('page/' . $this->class_link . '/report/struktur_biaya', $dataKonten, true);
		}
		$this->load->view('page/' . $this->class_link . '/report/struktur_biaya_pdf', $data);
	}

	public function struktur_biaya_group_hscode_pdf()
	{
		$this->load->library('Pdf');
		$kd_mdo = $this->input->get('kd_mdo', true);
		$konten = $this->td_do_item_reportunit->generatereport_strukturbiaya_group_hscode($kd_mdo);
		$data['kontens'] = $this->load->view('page/' . $this->class_link . '/report/struktur_biaya_group_hscode', $konten, true);
		$this->load->view('page/' . $this->class_link . '/report/struktur_biaya_group_hscode_pdf', $data);
	}

	/** AR PUSH TO SAP */
	public function send_ar_invoice_to_sap($id)
	{
		$header = $this->tm_deliveryorder->get_row($id);
		$detail = [];
		/** Cek jika SO terdapat banyak DO berdasarkan kd_msalesorder */
		$detail = $this->td_do_item->get_item_do($id);
		$barangKd = "";
		$gudangKd = "";
		$linesDetail = [];
		// Detail jika parent dan child terisi semua maka yang di ambil adalah value child
		foreach ($detail as $row) :
			if ($row['parent_kd'] != null && $row['child_kd'] != null) :
				// Child item
				$kdSOItem = $row['child_kd'];
				$barangKd = $this->td_salesorder_item_detail->get_by_param([
					'kd_citem_so' => $kdSOItem
				])->row()->kd_child;
			else :
				// Parent item
				$kdSOItem = $row['parent_kd'];
				$barangKd = $this->td_salesorder_item->get_by_param([
					'kd_ditem_so' => $kdSOItem
				])->row()->barang_kd;
			endif;

			// Get data gudang berdasarkan barang -> item group
			$gudangKd = $this->tm_item_group->get_by_param([
				'item_group_kd' => $this->tm_barang->get_item(['kd_barang' => $barangKd])->item_group_kd
			])
				->row()->gudang_kd;

			$linesDetail[] = [
				'U_IDU_WEBID' => $row['kd_ditem_do'],
				'KdSalesOrderDtl' => $kdSOItem,
				'Quantity' => $row['stuffing_item_qty'],
				'KdWarehouse' => $gudangKd
			];
		endforeach;
		$dataAPI = [
			'U_IDU_WEBID' 			=> $header['invoicearsap_kd'],
			'KdSalesOrder' 			=> $header['msalesorder_kd'],
			'U_IDU_AR_INTNUM' 		=> $header['no_invoice'],
			'DocDate'				=> format_date($header['tgl_do'], "Y-m-d"),
			'U_IDU_Stuffing_Date' 	=> format_date($header['tgl_stuffing'], "Y-m-d"),
			'NumAtCard'				=> $header['no_invoice'],
			'Comments'				=> $header['do_ket'],
			'U_IDU_WEBUSER' 		=> $this->session->username,
			'Lines_Detail_Item' 	=> $linesDetail
		];
		/** Ubah status push SAP ke Y */
		$where = ['kd_mdo' => $id];
		$dataUpdate = ['push_sap' => 'Y'];
		$act = $this->tm_deliveryorder->update_data($where, $dataUpdate);
		/** End Ubah status push SAP ke Y */
		$api = parent::api_sap_post('AddARInvoice', $dataAPI);
		return $api;
	}
}
