<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Items_do extends MY_Controller {
	private $class_link = 'sales/items_do';
	private $form_errs = array('idErrForm', 'idErrForm');

	public function __construct() {
		parent::__construct();

		$this->load->library(array('form_validation'));
		$this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
		$this->load->model(array('m_builder', 'tm_deliveryorder', 'm_salesorder', 'td_do_item', 'tm_salesorder'));
	}

	public function index() {
		parent::administrator();
		parent::pnotify_assets();
		$this->get_table();
	}

	public function get_form() {
		$data['id'] = $this->input->get('id');
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = array('idErrForm');
		$this->load->view('page/'.$this->class_link.'/items_box', $data);
	}

	public function open_form() {
		$data['class_link'] = $this->class_link;
		$data = $this->tm_deliveryorder->get_row($this->input->get('id'));
		$data['msalesorder_kd'] = empty($data['msalesorder_kd'])?$_SESSION['master_do']['msalesorder_kd']:$data['msalesorder_kd'];
		$items_data = $this->m_salesorder->salesOrderItems($data['msalesorder_kd']);
		if (!empty($items_data)) :
			foreach ($items_data as $item_data) :
				$kd_ditem_so[] = $item_data->kd_ditem_so;
			endforeach;
			$details_data = $this->m_salesorder->item_child($kd_ditem_so);
		endif;
		/** Untuk cek jenis insert; Edit / Add */
		$slug = 'add';
		if (!empty($data['kd_mdo'])){
			$slug = 'edit';
			$data['slug'] = $slug;
		}
		$data['so_data_parent'] = $items_data;
		$data['so_data_child'] = $details_data;
		$data['items_do'] = $this->td_do_item->get_from_so($data['msalesorder_kd']);
		$this->load->view('page/'.$this->class_link.'/items_main', $data);
	}
	
	public function tabledetail_main () {
		$kd_mdo = $this->input->get('id');
		
		$data['master'] = $this->db->select()
						->from('tm_deliveryorder')
						->join('tm_salesorder', 'tm_deliveryorder.msalesorder_kd=tm_salesorder.kd_msalesorder', 'left')
						->where('tm_deliveryorder.kd_mdo', $kd_mdo)
						->get()->row();
		$data['kd_mdo'] = $kd_mdo;
		$data['resultDOitem'] = $this->td_do_item->get_item_do($kd_mdo);
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/tabledetail_main', $data);
	}
	
	public function formdetail_main () {
		$kd_ditem_do = $this->input->get('kd_ditem_do');
		
		$qRow = $this->td_do_item->get_by_param(['kd_ditem_do' => $kd_ditem_do])->row();
		$this->db->select()->from('td_do_item');
		if (empty($qRow->child_kd)) {
			$this->db->join('td_salesorder_item', 'td_do_item.parent_kd=td_salesorder_item.kd_ditem_so', 'left');
		}else{
			$this->db->join('td_salesorder_item_detail', 'td_do_item.child_kd=td_salesorder_item_detail.kd_citem_so', 'left');
		}
		$this->db->where('kd_ditem_do', $kd_ditem_do);
		$qRowdetail = $this->db->get()->row();
		$data['row'] = $qRowdetail;
		$data['kd_ditem_do'] = $kd_ditem_do;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/formdetail_main', $data);
	}

	public function send_data() {
		if ($this->input->is_ajax_request()) :
			$this->form_validation->set_rules($this->td_do_item->form_rules());
			if ($this->form_validation->run() == FALSE) :
				$str = $this->td_do_item->build_warning($this->form_errs);
				$str['confirm'] = 'error';
			else :
				$this->db->trans_begin();
				$slug = $this->input->post('txtSlug');
				$msalesorder_kd = $this->input->post('txtKdMSo');;
				$jml_master = $this->m_salesorder->count_so_master_items($this->input->post('txtKdMSo'));
				$jml_detail = $this->m_salesorder->count_so_detail_items($this->input->post('txtKdMSo'));
				$detail['so_count'] = $jml_master + $jml_detail;
				$detail['kd_ditem_do'] = $this->input->post('txtKdDDo');
				$detail['kd_mdo'] = $this->input->post('txtKdMDo');
				$detail['msalesorder_kd'] = $msalesorder_kd;
				$detail['parent_kd'] = $this->input->post('chkItemParent');
				$detail['child_parent'] = $this->input->post('txtKdParent');
				$detail['child_kd'] = $this->input->post('chkItemChild');
				$detail['item_barcode'] = $this->input->post('txtItemBarcode');
				$detail['stuffing_item_qty'] = $this->input->post('txtStuffingQty');
				$detail['item_note'] = $this->input->post('txtItemNote');
				if ($slug == 'add') :
					foreach ($_SESSION['master_do'] as $col => $val) :
						$data[$col] = $val;
					endforeach;
					$act = $this->tm_deliveryorder->submit_data($data, $data['tipe_do']);
					if ($act) :
						$detail['kd_mdo'] = $act['kd_mdo'];
					else :
						header('Content-Type: application/json');
						echo json_encode($act);
						exit();
					endif;
				endif;
				$str = $this->td_do_item->submit_data($detail);
				/** Cek status SO */
				$cekSOdetail = $this->td_do_item->check_itemdoso_complete($msalesorder_kd);
				if ($cekSOdetail) {
					$actStatus = $this->tm_salesorder->ubah_status_so ($msalesorder_kd, 'finish');
				}
				if ($this->db->trans_status() == TRUE):
					$this->db->trans_commit();
					$str['confirm'] = 'success';
					$str['alert'] = buildAlert('success', 'Berhasil!', 'Data Item DO!');
				else:
					$this->db->trans_rollback();
					$str['confirm'] = 'error';
					$str['alert'] = buildAlert('success', 'Gagal!', 'Kesalahan sistem!');
				endif;
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();
			$str['slug'] = $slug;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function action_submitDatadetail () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtkd_ditem_do', 'Item', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtstuffing_item_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErritem_code' => (!empty(form_error('txtkd_ditem_do')))?buildLabel('warning', form_error('txtkd_ditem_do', '"', '"')):'',
				'idErrstuffing_item_qty' => (!empty(form_error('txtstuffing_item_qty')))?buildLabel('warning', form_error('txtstuffing_item_qty', '"', '"')):'',
			);
			
		}else {
            $kd_ditem_do = $this->input->post('txtkd_ditem_do', true);
            $stuffing_item_qty = $this->input->post('txtstuffing_item_qty', true);

			$data = array(
				'stuffing_item_qty' => $stuffing_item_qty,
				'tgl_edit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

            $act = $this->td_do_item->update_data (['kd_ditem_do' => $kd_ditem_do], $data);
            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_deletedetail () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $kd_ditem_do = $this->input->get('id', TRUE);

        if (!empty($kd_ditem_do)) {          
            $act = $this->td_do_item->delete_data($kd_ditem_do);            
            if ($act) {
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
            }else {
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
            }
        }else {
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal ID kosong');
        }

		echo json_encode($resp);
	}


	/** Close SO Push to SAP ketika SO di DO penuh */
	public function close_so_sap($kd_msalesorder){
		$dataAPI = [
			'U_IDU_WEBID' => $kd_msalesorder
		];
		$api = parent::api_sap_post('CloseSalesOrder', $dataAPI);
		return $api;
	}

}