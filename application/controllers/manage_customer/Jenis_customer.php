<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenis_customer extends MY_Controller {
	private $class_link = 'manage_customer/jenis_customer';
	private $form_errs = array('idErrNm', 'idErrPrice', 'idErrItem', 'idErrSetPpn', 'idErrBarcode', 'idErrTermFormat', 'idErrTermWaktu');

	public function __construct() {
		parent::__construct();

		$this->load->library(array('ssp', 'form_validation'));
		$this->load->helper(array('form', 'my_btn_access_helper', 'my_helper'));
		$this->load->model(array('tb_jenis_customer', 'tb_default_disc', 'tb_set_dropdown'));
	}

	public function index() {
		parent::administrator();
		parent::pnotify_assets();
		$this->get_table();
	}

	public function get_table() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function open_table() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		$data = $this->tb_jenis_customer->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function get_form() {
		$data['id'] = $this->input->get('id');
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = $this->form_errs;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function open_form() {
		$id = $this->input->get('id');
		$data['master'] = $this->tb_jenis_customer->get_data($id);
		$data['form_diskon'] = $this->tb_default_disc->create_form_diskon($id);
		/** Get Term Payment dan PPN dari SAP lalu push ke input select */
		$data['term_payment'] 	= parent::api_sap_post('Search', ['CustomQuery' => 'GetPaymentTerms'])[0];
		$data['ppn'] 			= parent::api_sap_post('Search', ['CustomQuery' => 'GetOutputTax'])[0];
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function get_form_diskon() {
		echo $this->tb_default_disc->get_form_diskon(2, 'nothing', '', '', '', '');
	}

	public function form_masterbom_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id');

		$data['rowData'] = $this->tb_jenis_customer->get_row($id);
		$data['class_link'] = $this->class_link;
		$data['id'] = $id;
		$this->load->view('page/'.$this->class_link.'/form_masterbom_main', $data);
	}

	public function send_data() {
		if ($this->input->is_ajax_request()) :
			$this->db->trans_begin();
			$this->form_validation->set_rules($this->tb_jenis_customer->form_rules());
			if ($this->form_validation->run() == FALSE) :
				$str = $this->tb_jenis_customer->form_warning($this->form_errs);
				$str['confirm'] = 'error';
			else :
				$data['kd_jenis_customer'] = $this->input->post('txtKd');
				$data['kd_sap'] = $this->input->post('txtKdSap');
				$data['nm_jenis_customer'] = $this->input->post('txtNm');
				$data['kd_price_category'] = $this->input->post('selPrice');
				$data['kd_manage_items'] = $this->input->post('selItem');
				$data['tipe_penjualan'] = $this->tb_set_dropdown->get_by_param(['id' => $data['kd_manage_items']])->row()->nm_select;
				/** Data get from SAP trim by character "|", array 0 untuk push ke SAP, array 0 untuk push ke web ERP */
				$ppn = explode("|", $this->input->post('txtSetPpn'));
				$data['kd_ppn_sap'] = $ppn[0];
				$data['set_ppn'] = $ppn[1];
				/** End data get from SAP trim */
				$data['custom_barcode'] = $this->input->post('txtBarcode');
				$data['term_payment_format'] = $this->input->post('txtTermFormat');
				/** Data get from SAP trim by character "|", array 0 untuk push ke SAP, array 0 untuk push ke web ERP */
				$term = explode("|", $this->input->post('txtTermWaktu'));
				$data['kd_term_payment_sap'] = $term[0];
				$data['term_payment_waktu'] = $term[1];
				/** End data get from SAP trim */
				$m_act = $this->tb_jenis_customer->submit_data($data);
				$code = $m_act['data']['kd_jenis_customer'];
				if ($m_act['confirm'] == 'success') :
					$detail['jenis_customer_kd'] = $m_act['kd_jenis_customer'];
					$detail['nm_pihak'] = $this->input->post('txtNmPihak');
					$detail['status'] = $this->input->post('selStatus');
					$detail['jml_disc'] = $this->input->post('txtJmlDiskon');
					$detail['individual_disc'] = $this->input->post('selIndividual');
					$detail['view_access'] = $this->input->post('selDiscAccess');
					$d_act = $this->tb_default_disc->submit_data($detail);
					if ($d_act['confirm'] == 'error') :
						$str = $this->tb_jenis_customer->delete_data($m_act['kd_jenis_customer']);
					else :
						$str = $d_act;
					endif;
				endif;
				
				if ($this->db->trans_status() === FALSE || $str['confirm'] != 'success') :
					$this->db->trans_rollback();
					$str['stat'] = 'Gagal';
					$str['confirm'] = 'error';
					$str['alert'] = buildAlert('danger', 'Gagal!', 'Error transaction !');
				else :
					if(empty($data['kd_jenis_customer'])):
						/** API INSERT */
						$api = $this->push_to_sap($m_act['data']['kd_jenis_customer'], "add");
						$method = "Menambahkan";
					else:
						/** API EDIT */
						$api = $this->push_to_sap($m_act['data']['kd_jenis_customer'], "edit");
						$method = "Mengubah";
					endif;
					if($api[0]->ErrorCode == 0):
						$this->db->trans_commit();
						$str['stat'] = 'Berhasil';
						$str['confirm'] = 'success';
						$str['alert'] = buildAlert('success', 'Berhasil!', ''.$method.' data jenis customer !');
					else:
						$this->db->trans_rollback();
						$str['stat'] = 'Gagal';
						$str['confirm'] = 'error';
						$str['alert'] = buildAlert('danger', 'Gagal!', ''.$method.' data jenis customer, error API ['.$api[0]->Message.'] !');
					endif;
				endif;

			endif;
			$str['csrf'] = $this->security->get_csrf_hash();
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function action_submitMasterBom () {
		if ($this->input->is_ajax_request()) :
			$this->form_validation->set_rules($this->tb_jenis_customer->form_rules());
			$data['kd_jenis_customer'] = $this->input->post('txtkd_jenis_customer');

			$data['jeniscustomer_marginbom'] = $this->input->post('txtjeniscustomer_marginbom');
			$data['jeniscustomer_discbom'] = $this->input->post('txtjeniscustomer_discbom');

			$m_act = $this->tb_jenis_customer->submit_data($data);
			$str = $m_act;
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function delete_data() {
		if ($this->input->is_ajax_request()) :
			$str = $this->tb_jenis_customer->delete_data($this->input->get('id'));
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function push_to_sap($kd, $act){
		$data = $this->tb_jenis_customer->get_row($kd);
		/** Fill variabel to SAP */
		$dataAPI = [
			'Code' 			 => $data->kd_sap, /** Diambil dari model tb_jenis_Customer karena generate codenya dari model tersebut */
			'Name' 			 => $data->nm_jenis_customer,
			'U_IDU_Tipe' 	 => $data->tipe_penjualan,
			'U_IDU_GroupNum' => $data->kd_term_payment_sap,
			'U_IDU_VatGroup' => $data->kd_ppn_sap
		];
		if($act == "add"):
			$api = parent::api_sap_post('AddJenisCustomer', $dataAPI);
		else:
			$api = parent::api_sap_post('EditJenisCustomer', $dataAPI);
		endif;
		return $api;
	}

}