<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Alamat extends MY_Controller {
	private $class_link = 'manage_customer/alamat';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_cust_group', 'lokasi/tb_negara', 'lokasi/tb_provinsi', 'lokasi/tb_kota']);
	}

	public function index() {
        parent::administrator();
		parent::pnotify_assets();
        $data['class_link'] = $this->class_link;

        $this->load->view('page/'.$this->class_link.'/table_box', $data);
	}
    
    public function table_main(){
    	if (!$this->input->is_ajax_request()){
    		exit('No direct script access allowed');
    	}
    	$data['class_link'] = $this->class_link;
    	$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }
	public function table_mainp(){
    	if (!$this->input->is_ajax_request()){
    		exit('No direct script access allowed');
    	}
    	$data['class_link'] = $this->class_link;
    	$this->load->view('page/'.$this->class_link.'/table_main_prov', $data);
    }
	public function table_maink(){
    	if (!$this->input->is_ajax_request()){
    		exit('No direct script access allowed');
    	}
    	$data['class_link'] = $this->class_link;
    	$this->load->view('page/'.$this->class_link.'/table_main_kota', $data);
    }

    public function table_data(){
    	if (!$this->input->is_ajax_request()){
    		exit('No direct script access allowed');
    	}
    	$this->load->library(['ssp']);

		$data = $this->tb_negara->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
    }

	
    public function table_data_prov(){
    	// if (!$this->input->is_ajax_request()){
    	// 	exit('No direct script access allowed');
    	// }
    	$this->load->library(['ssp']);

		$data = $this->tb_provinsi->ssp_table_p();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
    }

	public function table_data_kota(){
    	// if (!$this->input->is_ajax_request()){
    	// 	exit('No direct script access allowed');
    	// }
    	$this->load->library(['ssp']);

		$data = $this->tb_kota->ssp_table_k();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
    }


	function form_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', true);
		if (!empty($id)) {
			$data['rowData'] = $this->tb_negara->get_by_param (['id' => $id])->row_array();
			$sts = 'edit';
		}else{
			$sts = 'add';
		}
		$data['sts'] = $sts;
		$data['id'] = $id;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	function form_main_prov(){
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }
		$id = $this->input->get('id', true);
		if (!empty($id)) {
			$data['rowData'] = $this->tb_provinsi->get_by_param (['id' => $id])->row_array();
			$sts = 'edit';
		}else{
			$sts = 'add';
		}
		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['negara'] = $this->db->query("SELECT * FROM tb_negara")->result();

		// header('Content-Type: application/json');
		// echo json_encode($data);
		$this->load->view('page/'.$this->class_link.'/form_main_prov', $data);
	}

	function form_main_kota(){
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }
		$id = $this->input->get('id', true);
		if (!empty($id)) {
			$data['rowData'] = $this->tb_kota->get_by_param (['id' => $id])->row_array();
			$sts = 'edit';
		}else{
			$sts = 'add';
		}
		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['negara'] = $this->db->query("SELECT * FROM tb_negara")->result();
		$data['prov'] = $this->db->query("SELECT * FROM tb_provinsi")->result();

		// header('Content-Type: application/json');
		// echo json_encode($data);
		$this->load->view('page/'.$this->class_link.'/form_main_kota', $data);
	}
    
    public function action_submit () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		//$this->db->trans_begin();
		$cust_group_kd = $this->input->post('txt_kd', true);	
		$id_negara = $this->input->post('id_negara', true);	
		$nm_negara = $this->input->post('nm_negara', true);	
		$sts = $this->input->post('txtSts', true);			
		$data = [
			'kd_negara' => $id_negara,
			'nm_negara' => $nm_negara,
		];
		$act = false;
		if($sts == 'edit'){
			$xdata = [
				'kd_negara' => $cust_group_kd,
				'nm_negara' => $nm_negara,
			];
			$act = $this->tb_negara->update_data(['kd_negara' => $cust_group_kd], $xdata);
		}else if($sts == "add"){
			$act = $this->tb_negara->insert_data($data);
		}

		// $act = $this->tb_purchaseorder_user->insert_data($data);
		if ($act){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		}

		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_submit_prov() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		//$this->db->trans_begin();
		$id_prov = $this->input->post('id_prov', true);	
		$negara_kd = $this->input->post('negara', true);	
		$nm_prov = $this->input->post('nm_prov', true);	
		$sts = $this->input->post('txtSts', true);		
		$txt_kd = $this->input->post('txt_kd', true);			
		
		$data = [
			'kd_provinsi' => $id_prov,
			'negara_kd' => $negara_kd,
			'nm_provinsi' => $nm_prov,
		];
		$act = false;
		if($sts == 'edit'){
			$xdata = [
				'kd_provinsi' => $id_prov,
				'negara_kd' => $negara_kd,
				'nm_provinsi' => $nm_prov,
			];
			$act = $this->tb_provinsi->update_data(['id' => $txt_kd], $xdata);
		}else if($sts == "add"){
			$act = $this->tb_provinsi->insert_data($data);
		}

		// $act = $this->tb_purchaseorder_user->insert_data($data);
		if ($act){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		}

		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_submit_kota() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		//$this->db->trans_begin();
		$id_kota = $this->input->post('id_kota', true);	
		$negara_k = $this->input->post('negara_k', true);	
		$prov_k = $this->input->post('prov_k', true);	
		$nm_kota = $this->input->post('nm_kota', true);	
		$sts = $this->input->post('txtSts', true);		
		$txt_kd = $this->input->post('txt_kd', true);			
		
		$data = [
			'kd_kota' => $id_kota,
			'provinsi_kd' => $prov_k,
			'negara_kd' => $negara_k,
			'nm_kota' => $nm_kota,
		];
		$act = false;
		if($sts == 'edit'){
			$xdata = [
			'kd_kota' => $id_kota,
			'provinsi_kd' => $prov_k,
			'negara_kd' => $negara_k,
			'nm_kota' => $nm_kota,
			];
			$act = $this->tb_kota->update_data(['id' => $txt_kd], $xdata);
		}else if($sts == "add"){
			$act = $this->tb_kota->insert_data($data);
		}

		// $act = $this->tb_purchaseorder_user->insert_data($data);
		if ($act){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		}

		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_hapus () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->db->trans_begin();
		$id = $this->input->get('id', TRUE);
		$act = $this->tm_cust_group->delete_data($id);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Terhapus, error transaction !');
		}else{
			$api = $this->push_to_sap($id, "delete");
			if($api[0]->ErrorCode == 0){
				$this->db->trans_commit();
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus, API['.$api[0]->Message.']');
			}else{
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Terhapus, API['.$api[0]->Message.']');
			}
		}
		echo json_encode($resp);
	}

	public function get_last_code () {
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }
		// $this->db->trans_begin();
		// $id = $this->input->get('id', TRUE);
		//$act = $this->tm_cust_group->delete_data($id);
		$newquery = $this->db->query("SELECT MAX(kd_negara) as q FROM tb_negara");
		// if ($this->db->trans_status() === FALSE) {
		// 	$this->db->trans_rollback();
		// 	$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Terhapus, error transaction !');
		// }else{
		// 	$api = $this->push_to_sap($id, "delete");
		// 	if($api[0]->ErrorCode == 0){
		// 		$this->db->trans_commit();
		// 		$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus, API['.$api[0]->Message.']');
		// 	}else{
		// 		$this->db->trans_rollback();
		// 		$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Terhapus, API['.$api[0]->Message.']');
		// 	}
		// }

		$data = $newquery->result();
                    
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function get_last_code_prov () {
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }
		// $this->db->trans_begin();
		$id = $this->input->get('id', TRUE);
		//$act = $this->tm_cust_group->delete_data($id);
		$newquery = $this->db->query("SELECT MAX(kd_provinsi) as q FROM tb_provinsi where negara_kd = $id");
		// if ($this->db->trans_status() === FALSE) {
		// 	$this->db->trans_rollback();
		// 	$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Terhapus, error transaction !');
		// }else{
		// 	$api = $this->push_to_sap($id, "delete");
		// 	if($api[0]->ErrorCode == 0){
		// 		$this->db->trans_commit();
		// 		$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus, API['.$api[0]->Message.']');
		// 	}else{
		// 		$this->db->trans_rollback();
		// 		$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Terhapus, API['.$api[0]->Message.']');
		// 	}
		// }
		$data = $newquery->result();
                    
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function get_last_code_kota () {
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }
		// $this->db->trans_begin();
		$negara_kd = $this->input->get('negara_kd', TRUE);
		$prov_kd = $this->input->get('prov_kd', TRUE);
		//$act = $this->tm_cust_group->delete_data($id);
		$newquery = $this->db->query("SELECT MAX(kd_kota) as q FROM tb_kota where negara_kd = $negara_kd and provinsi_kd = $prov_kd");
		// if ($this->db->trans_status() === FALSE) {
		// 	$this->db->trans_rollback();
		// 	$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Terhapus, error transaction !');
		// }else{
		// 	$api = $this->push_to_sap($id, "delete");
		// 	if($api[0]->ErrorCode == 0){
		// 		$this->db->trans_commit();
		// 		$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus, API['.$api[0]->Message.']');
		// 	}else{
		// 		$this->db->trans_rollback();
		// 		$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Terhapus, API['.$api[0]->Message.']');
		// 	}
		// }
		$data = $newquery->result();
                    
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	
}
