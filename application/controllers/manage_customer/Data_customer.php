<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_customer extends MY_Controller {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'tm_customer';
	private $p_key = 'kd_customer';
	private $class_link = 'manage_customer/data_customer';
	private $title = 'Data Customer';

	function __construct() {
		parent::__construct();
		$this->load->model(array('m_builder', 'tm_customer', 'td_customer_alamat', 'tb_set_dropdown', 'model_jenis_customer'));
		$this->load->helper(array('form', 'html', 'my_helper'));
	}

	function index() {
		parent::administrator();
		parent::pnotify_assets();

		$data['t_box_class'] = 'box-primary';
		$title = $this->uri->segment_array();
		$title = end($title);
		if (strpos($title, '_') !== FALSE ) :
			$title = str_replace('_', ' ', $title);
		endif;
		$title = ucwords($title);
		$data['table_title'] = $title;
		$data['table_alert'] = 'idAlertTable';
		$data['table_loader'] = 'idLoaderTable';
		$data['table_name'] = 'idTable';
		$data['table_overlay'] = 'idTableOverlay';
		$script['class_name'] = $this->class_link;
		$data['btn_add'] = cek_permission('DATACUSTOMER_CREATE');

		$this->load->css('assets/admin_assets/plugins/select2/select2.min.css');
		$this->load->css('assets/admin_assets/plugins/select2/select2-bootstrap.min.css');
		$this->load->js('assets/admin_assets/plugins/input-mask/jquery.inputmask.js');
		$this->load->js('assets/admin_assets/plugins/select2/select2.full.min.js');
		$this->load->section('scriptJS', 'script/'.$this->class_link.'/scriptJS', $script);
		// $this->data_admin_form();
		$this->load->view('page/admin/v_table', $data);
	}

	function data_lihat() {
		if ($this->input->is_ajax_request()) :
			/* tabel property */
			$t_data['btn'] = TRUE;
			$t_data['t_header'] = array(
				'<th style="width:5%;">Code Customer</th>',
				'<th style="width:5%;">Nama Customer</th>',
				'<th style="width:1%;">Jenis Customer</th>',
				'<th style="width:10%;">Alamat</th>',
				'<th style="width:10%;">Kode Pos</th>',
				'<th style="width:1%;">Telp Utama</th>',
				'<th style="width:1%;">Telp Lain</th>',
				'<th style="width:1%;">Email</th>',
				'<th style="width:1%;">Fax</th>',
				'<th style="width:1%;" class="never">Badan Usaha</th>',
				'<th style="width:1%;" class="never">Negara</th>',
				'<th style="width:1%;" class="never">Provinsi</th>',
				'<th style="width:1%;" class="never">Kota</th>',
				'<th style="width:1%;" class="never">Kecamatan</th>',
			);
			$t_data['t_uri'] = base_url().$this->class_link.'/data_table';
			$t_data['t_order'] = '[2, "asc"]';
			$t_data['datatable_properties'] = ['\'scrollX\': true,', '\'scrollY\': \'600px\','];

			$this->load->view('page/admin/table_ssp', $t_data);
		endif;
	}

	function data_table() {
		if (!$this->input->is_ajax_request()) :
            exit('No direct script access allowed');
        else :

            $this->load->library('ssp');

			$table = $this->tbl_name;

			$primaryKey = $this->p_key;

			$columns = array(
			    array( 'db' => 'a.'.$this->p_key,
						'dt' => 1, 'field' => $this->p_key,
						'formatter' => function($d){
							$btn_edit = '';
							$divider = '';
							$btn_delete = '';
							if (cek_permission('DATACUSTOMER_UPDATE')) :
								$btn_edit = '<a id="edit" title="Edit Data" href="javascript:void(0);" onclick="editData(\''.$d.'\')"><i class="fa fa-pencil"></i> Edit Data</a>';
								$divider = '<li class="divider"></li>';
							endif;
							if (cek_permission('DATACUSTOMER_DELETE')) :
								$btn_delete = '<a id="delete" title="Hapus Data" href="javascript:void(0);" onclick="return confirm(\'Apakah anda yakin?\')?hapusData(\''.$d.'\'):false;"><i class="fa fa-trash"></i> Hapus Data</a>';
							endif;

							$btn = '<div align="center"><div class="btn-group"><button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">Opsi <span class="caret"></span></button><ul class="dropdown-menu"><li>'.$btn_edit.'</li>'.$divider.'<li>'.$btn_delete.'</li></ul></div></div>';
							
							return $btn;
						} ),
				array( 'db' => 'a.code_customer', 'dt' => 2, 'field' => 'code_customer' ),
			    array( 'db' => 'a.nm_customer',
			    		'dt' => 3, 'field' => 'nm_customer',
			    		'formatter' => function($d){
							$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'b.nm_jenis_customer',
			    		'dt' => 4, 'field' => 'nm_jenis_customer',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'a.alamat',
			    		'dt' => 5, 'field' => 'alamat',
			    		'formatter' => function($d, $row){
			    			$d = $this->security->xss_clean($d).', '.$row[13].', '.$row[12].', '.$row[11];

			    			return $d;
			    		} ),
				array( 'db' => 'a.kode_pos',
			    		'dt' => 6, 'field' => 'kode_pos',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'a.no_telp_utama',
			    		'dt' => 7, 'field' => 'no_telp_utama',
			    		'formatter' => function($d){
			    			$d = replace_empty($this->security->xss_clean($d), '-');

			    			return $d;
			    		} ),
			    array( 'db' => 'a.no_telp_lain',
			    		'dt' => 8, 'field' => 'no_telp_lain',
			    		'formatter' => function($d){
			    			$d = replace_empty($this->security->xss_clean($d), '-');

			    			return $d;
			    		} ),
			    array( 'db' => 'a.email',
			    		'dt' => 9, 'field' => 'email',
			    		'formatter' => function($d){
			    			$d = replace_empty($this->security->xss_clean($d), '-');

			    			return $d;
			    		} ),
			    array( 'db' => 'a.fax',
			    		'dt' => 10, 'field' => 'fax',
			    		'formatter' => function($d){
			    			$d = replace_empty($this->security->xss_clean($d), '-');

			    			return $d;
			    		} ),
			    array( 'db' => 'c.nm_select',
			    		'dt' => 11, 'field' => 'nm_select',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'd.nm_negara',
			    		'dt' => 12, 'field' => 'nm_negara',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'e.nm_provinsi',
			    		'dt' => 13, 'field' => 'nm_provinsi',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'f.nm_kota',
			    		'dt' => 14, 'field' => 'nm_kota',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'g.nm_kecamatan',
			    		'dt' => 15, 'field' => 'nm_kecamatan',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			);

			$sql_details = sql_connect();

			$joinQuery = "
				FROM
					".$this->tbl_name." a
				LEFT OUTER JOIN tb_jenis_customer b ON b.kd_jenis_customer = a.jenis_customer_kd
				LEFT OUTER JOIN tb_set_dropdown c ON c.id = a.kd_badan_usaha
				LEFT OUTER JOIN tb_negara d ON d.kd_negara = a.negara_kd
				LEFT OUTER JOIN tb_provinsi e ON e.kd_provinsi = a.provinsi_kd AND e.negara_kd = d.kd_negara
				LEFT OUTER JOIN tb_kota f ON f.kd_kota = a.kota_kd AND f.provinsi_kd = e.kd_provinsi AND f.negara_kd = d.kd_negara
				LEFT OUTER JOIN tb_kecamatan g ON g.kd_kecamatan = a.kecamatan_kd AND g.kota_kd = f.kd_kota AND g.provinsi_kd = e.kd_provinsi AND g.negara_kd = d.kd_negara
			";
			$where = "";

			echo json_encode(
			    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $where )
			);
		endif;
	}

	function data_form(){
		if ($this->input->is_ajax_request()) :
			// Form data
			$id = $this->input->get('id');
			if (!empty($id)) :
				$det = $this->m_builder->detail_customer($id);
				$det_alamat = $this->m_builder->detail_alamat($id, 'row');
				$id_form = 'idFormEdit';
				$data['f_box_class'] = 'box-warning';
				$data['form_title'] = 'Edit '.$this->title;
				$btn_form = '<i class="fa fa-edit"></i> Edit Data';
				$kd_customer = $this->security->xss_clean($det->kd_customer);
				$kd_alamat_kirim_single = $this->security->xss_clean($det_alamat->kd_alamat_kirim);
				$jenis_customer_kd = $this->security->xss_clean($det->jenis_customer_kd);
				$kd_badan_usaha = $this->security->xss_clean($det->kd_badan_usaha);
				$nm_customer = $this->security->xss_clean($det->nm_customer);
				$contact_person = $this->security->xss_clean($det->contact_person);
				$code_customer = $this->security->xss_clean($det->code_customer);
				$npwp_customer = $this->security->xss_clean($det->npwp_customer);
				$negara_kd = $this->security->xss_clean($det->negara_kd);
				$provinsi_kd = $this->security->xss_clean($det->provinsi_kd);
				$kota_kd = $this->security->xss_clean($det->kota_kd);
				$kecamatan_kd = $this->security->xss_clean($det->kecamatan_kd);
				$alamat = $this->security->xss_clean($det->alamat);
				$kode_pos = $this->security->xss_clean($det->kode_pos);
				$no_telp_utama = $this->security->xss_clean($det->no_telp_utama);
				$no_telp_lain = $this->security->xss_clean($det->no_telp_lain);
				$email = $this->security->xss_clean($det->email);
				$fax = $this->security->xss_clean($det->fax);
				$cust_group_kd = $this->security->xss_clean($det->cust_group_kd);
				$debpay_acc_kd = $this->security->xss_clean($det->debpay_acc_kd);
			else :
				$id_form = 'idFormInput';
				$data['f_box_class'] = 'box-info';
				$data['form_title'] = 'Input '.$this->title;
				$btn_form = '<i class="fa fa-save"></i> Input Data';
				$kd_customer = '';
				$kd_alamat_kirim_single = '';
				$jenis_customer_kd = '';
				$kd_badan_usaha = '';
				$nm_customer = '';
				$contact_person = '';
				$code_customer = '';
				$npwp_customer = '';
				$negara_kd = '';
				$nm_negara = '';
				$provinsi_kd = '';
				$nm_provinsi = '';
				$kota_kd = '';
				$nm_kota = '';
				$kecamatan_kd = '';
				$nm_kecamatan = '';
				$alamat = '';
				$kode_pos = '';
				$no_telp_utama = '';
				$no_telp_lain = '';
				$email = '';
				$fax = '';
				$cust_group_kd = '';
				$debpay_acc_kd = '';
			endif;
			
			// select option untuk jenis customer
			$this->db->from('tb_jenis_customer');
			$query = $this->db->get();
			$result = $query->result();
			$pil_jenis = array('' => '-- Pilih Jenis Customer --');
			foreach ($result as $row) :
				$kd_jenis_customer = $this->security->xss_clean($row->kd_jenis_customer);
				$nm_jenis_customer = $this->security->xss_clean($row->nm_jenis_customer);
				$pil_jenis[$kd_jenis_customer] = $nm_jenis_customer;
			endforeach;
			$sel_jenis = $jenis_customer_kd;
			$attr_jenis = array('id' => 'idSelJenis', 'class' => 'form-control');
			
			// select option untuk badan usaha
			$this->db->from('tb_set_dropdown')->where(array('jenis_select' => 'customer_property'));
			$query = $this->db->get();
			$result = $query->result();
			$pil_badan_usaha = array('' => '-- Pilih Badan Usaha --');
			foreach ($result as $row) :
				$id = $this->security->xss_clean($row->id);
				$nm_select = $this->security->xss_clean($row->nm_select);
				$pil_badan_usaha[$id] = $nm_select;
			endforeach;
			$sel_badan_usaha = $kd_badan_usaha;
			$attr_badan_usaha = array('id' => 'idSelBadanUsaha', 'class' => 'form-control');

			// select option untuk negara
			$this->db->from('tb_negara');
			$query = $this->db->get();
			$result = $query->result();
			$pil_negara = array('' => '-- Pilih Negara --');
			foreach ($result as $row) :
				$kd_negara = $this->security->xss_clean($row->kd_negara);
				$nm_negara = $this->security->xss_clean($row->nm_negara);
				$pil_negara[$kd_negara] = $nm_negara;
			endforeach;
			$sel_negara = $negara_kd;
			$attr_negara = array('id' => 'idSelNegara0', 'class' => 'form-control select2 idSelNegara');

			// select option untuk provinsi
			$this->db->from('tb_provinsi');
			$this->db->where(array('negara_kd' => $negara_kd));
			$query = $this->db->get();
			$result = $query->result();
			$pil_provinsi = array('' => '-- Pilih Provinsi --');
			foreach ($result as $row) :
				$kd_provinsi = $this->security->xss_clean($row->kd_provinsi);
				$nm_provinsi = $this->security->xss_clean($row->nm_provinsi);
				$pil_provinsi[$kd_provinsi] = $nm_provinsi;
			endforeach;
			$sel_provinsi = $provinsi_kd;
			$attr_provinsi = array('id' => 'idSelProvinsi0', 'class' => 'form-control select2 idSelProvinsi');

			// select option untuk kota
			$this->db->from('tb_kota');
			$this->db->where(array('negara_kd' => $negara_kd, 'provinsi_kd' => $provinsi_kd));
			$query = $this->db->get();
			$result = $query->result();
			$pil_kota = array('' => '-- Pilih Kota --');
			foreach ($result as $row) :
				$kd_kota = $this->security->xss_clean($row->kd_kota);
				$nm_kota = $this->security->xss_clean($row->nm_kota);
				$pil_kota[$kd_kota] = $nm_kota;
			endforeach;
			$sel_kota = $kota_kd;
			$attr_kota = array('id' => 'idSelKota0', 'class' => 'form-control select2 idSelKota');

			// select option untuk kecamatan
			$this->db->from('tb_kecamatan');
			$this->db->where(array('negara_kd' => $negara_kd, 'provinsi_kd' => $provinsi_kd, 'kota_kd' => $kota_kd));
			$query = $this->db->get();
			$result = $query->result();
			$pil_kecamatan = array('' => '-- Pilih Kecamatan --');
			foreach ($result as $row) :
				$kd_kecamatan = $this->security->xss_clean($row->kd_kecamatan);
				$nm_kecamatan = $this->security->xss_clean($row->nm_kecamatan);
				$pil_kecamatan[$kd_kecamatan] = $nm_kecamatan;
			endforeach;
			$sel_kecamatan = $kecamatan_kd;
			$attr_kecamatan = array('id' => 'idSelKecamatan0', 'class' => 'form-control select2 idSelKecamatan');

			// select option untuk customer group SAP
			$this->db->from('tm_cust_group');
			$query = $this->db->get();
			$result = $query->result();
			$pil_cust_group = array('' => '-- Pilih Customer Group --');
			foreach ($result as $row) :
				$kd_cust_group = $this->security->xss_clean($row->cust_group_kd);
				$nm_cust_group = $this->security->xss_clean($row->cust_group_name);
				$pil_cust_group[$kd_cust_group] = $nm_cust_group;
			endforeach;
			$sel_cust_group = $cust_group_kd;
			$attr_cust_group = array('id' => 'idSelCustGroup', 'class' => 'form-control');

			// select option untuk deb_pay account group SAP
			$pil_acc = array('' => '-- Pilih Account --');
			$acc_list = parent::api_sap_post('Search', ['CustomQuery' => 'GetDebPayAcct'])[0];
			foreach ($acc_list as $row) :
				$kd_acc = $row->AcctCode;
				$nm_acc = $row->AcctName;
				$pil_acc[$kd_acc] = $nm_acc;
			endforeach;
			$sel_acc = $debpay_acc_kd;
			$attr_acc = array('id' => 'idSelAccDebPay', 'class' => 'form-control');

			// Ambil detail alamat kirim
			$no = 0;
			$form_child = '';
			$det_kirim = $this->m_builder->detail_alamat($kd_customer, 'result');
			$jml_det_kirim = $this->m_builder->detail_alamat($kd_customer, 'num_rows');
			foreach ($det_kirim as $d_kirim) :
				$no++;
				if ($no > 1) :
					$kd_alamat_kirim = $d_kirim->kd_alamat_kirim;
					$nm_customer_child = $d_kirim->nm_customer;
					$contact_person_child = $d_kirim->contact_person;
					$npwp_customer_child = $d_kirim->npwp_customer;
					$sel_badan_usaha_child = $d_kirim->kd_badan_usaha;
					$attr_badan_usaha_child = array('id' => 'idSelBadanUsaha'.$no, 'class' => 'form-control idSelBadanUsaha');
					$sel_negara_child = $d_kirim->negara_kd;
					$attr_negara_child = array('id' => 'idSelNegara'.$no, 'class' => 'form-control select2 idSelNegara');
					$sel_provinsi_child = $d_kirim->provinsi_kd;
					$attr_provinsi_child = array('id' => 'idSelProvinsi'.$no, 'class' => 'form-control select2 idSelProvinsi');
					$sel_kota_child = $d_kirim->kota_kd;
					$attr_kota_child = array('id' => 'idSelKota'.$no, 'class' => 'form-control select2 idSelKota');
					$sel_kecamatan_child = $d_kirim->kecamatan_kd;
					$attr_kecamatan_child = array('id' => 'idSelKecamatan'.$no, 'class' => 'form-control select2 idSelKecamatan');
					$alamat_child = $d_kirim->alamat;
					$kode_pos_child = $d_kirim->kode_pos;
					$no_telp_utama_child = $d_kirim->no_telp_utama;
					$no_telp_lain_child = $d_kirim->no_telp_lain;
					$email_child = $d_kirim->email;
					$fax_child = $d_kirim->fax;


					$form_child .= '<div class="form_alamat">';

					$form_child .= '<div class="form-group">';
					$form_child .= '<label for="idSelBadanUsaha'.$no.'" class="col-md-2 control-label">Badan Usaha</label>';
					$form_child .= '<div class="col-md-3">';
					$form_child .= form_input(array('type' => 'hidden', 'name' => 'txtKdAlamatKirim[]', 'value' => $kd_alamat_kirim));
					$form_child .= form_dropdown('selBadanUsaha[]', $pil_badan_usaha, $sel_badan_usaha_child, $attr_badan_usaha_child);
					$form_child .= '</div>';
					$form_child .= '<label for="idTxtNm'.$no.'" class="col-md-2 control-label">Nama Customer</label>';
					$form_child .= '<div class="col-md-3">';
					$form_child .= form_input(array('name' => 'txtNm[]', 'id' => 'idTxtNm'.$no, 'class' => 'form-control', 'placeholder' => 'Nama Customer', 'value' => $nm_customer_child));
					$form_child .= '</div>';
					$form_child .= '</div>';
					
					$form_child .= '<div class="form-group">';
					$form_child .= '<label for="idTxtContactPerson'.$no.'" class="col-md-2 control-label">Contact Person</label>';
					$form_child .= '<div class="col-md-3">';
					$form_child .= form_input(array('name' => 'txtContactPerson[]', 'id' => 'idTxtContactPerson'.$no, 'class' => 'form-control', 'placeholder' => 'Contact Person', 'value' => $contact_person_child));
					$form_child .= '</div>';
					$form_child .= '<label for="idTxtNpwp'.$no.'" class="col-md-2 control-label">No. NPWP/NIK</label>';
					$form_child .= '<div class="col-md-2">';
					$form_child .= form_input(array('name' => 'txtNpwp[]', 'id' => 'idTxtNpwp'.$no, 'class' => 'form-control', 'placeholder' => 'No. NPWP/NIK', 'value' => $npwp_customer_child));
					$form_child .= '</div>';
					$form_child .= '</div>';

					$form_child .= '<div class="form-group">
						<label for="idSelNegara'.$no.'" class="col-md-2 control-label">Negara</label>
						<div class="col-md-3">';
					$form_child .= form_dropdown('selNegara[]', $pil_negara, $sel_negara_child, $attr_negara_child);
					$form_child .= '</div>';
					$form_child .= '<label for="idSelProvinsi'.$no.'" class="col-md-2 control-label">Provinsi</label>
						<div class="col-md-3">';
					$form_child .= form_dropdown('selProvinsi[]', $pil_provinsi, $sel_provinsi_child, $attr_provinsi_child);
					$form_child .= '</div></div>';

					$form_child .= '<div class="form-group">
						<label for="idSelKota'.$no.'" class="col-md-2 control-label">Kota</label>
						<div class="col-md-3">';
					$form_child .= form_dropdown('selKota[]', $pil_kota, $sel_kota_child, $attr_kota_child);
					$form_child .= '</div>';
					$form_child .= '<label for="idSelKecamatan'.$no.'" class="col-md-2 control-label">Kecamatan</label>
						<div class="col-md-3">';
					$form_child .= form_dropdown('selKecamatan[]', $pil_kecamatan, $sel_kecamatan_child, $attr_kecamatan_child);
					$form_child .= '</div></div>';

					$form_child .= '<div class="form-group">
						<label for="idTxtAlamat'.$no.'" class="col-md-2 control-label">Alamat</label>
						<div class="col-md-3">';
					$form_child .= form_textarea(array('name' => 'txtAlamat[]', 'id' => 'idTxtAlamat'.$no, 'class' => 'form-control idTxtAlamat', 'placeholder' => 'Alamat', 'rows' => '5', 'value' => $alamat_child));
					$form_child .= '</div>';
		
					$form_child .= '<label for="idTxtKodePos'.$no.'" class="col-md-2 control-label">Kode Pos</label>
						<div class="col-md-3">';
					$form_child .= form_input(array('name' => 'txtKodePos[]', 'id' => 'idTxtKodePos', 'class' => 'form-control', 'placeholder' => 'Kode Pos', 'maxlength' => 5, 'value' => $kode_pos_child));
					$form_child .= '</div></div>';

					$form_child .= '<div class="form-group">';
					$form_child .= '<label for="idTxtTelpUtama'.$no.'" class="col-md-2 control-label">Telp Utama</label>';
					$form_child .= '<div class="col-md-3">';
					$form_child .= form_input(array('name' => 'txtTelpUtama[]', 'id' => 'idTxtTelpUtama'.$no, 'class' => 'form-control', 'placeholder' => 'Telp Utama', 'value' => $no_telp_utama_child, 'data-inputmask' => '\'mask\': [\'+99-999-999-999-99\']', 'data-mask' => ''));
					$form_child .= '</div>';
					$form_child .= '<label for="idTxtTelpLain'.$no.'" class="col-md-2 control-label">Telp Lain</label>';
					$form_child .= '<div class="col-md-3">';
					$form_child .= form_input(array('name' => 'txtTelpLain[]', 'id' => 'idTxtTelpLain'.$no, 'class' => 'form-control', 'placeholder' => 'Telp Lain', 'value' => $no_telp_lain_child, 'data-inputmask' => '\'mask\': [\'+99-999-999-999-99\']', 'data-mask' => ''));
					$form_child .= '</div>';
					$form_child .= '</div>';

					$form_child .= '<div class="form-group">';
					$form_child .= '<label for="idTxtEmail'.$no.'" class="col-md-2 control-label">Email</label>';
					$form_child .= '<div class="col-md-3">';
					$form_child .= form_input(array('name' => 'txtEmail[]', 'id' => 'idTxtEmail'.$no, 'class' => 'form-control', 'placeholder' => 'Email', 'value' => $email_child));
					$form_child .= '</div>';
					$form_child .= '<label for="idTxtFax'.$no.'" class="col-md-2 control-label">Fax</label>';
					$form_child .= '<div class="col-md-3">';
					$form_child .= form_input(array('name' => 'txtFax[]', 'id' => 'idTxtFax'.$no, 'class' => 'form-control', 'placeholder' => 'Fax', 'value' => $fax_child, 'data-inputmask' => '\'mask\': [\'+99-9999-9999999\']', 'data-mask' => ''));
					$form_child .= '</div>';
					$form_child .= '</div>';

					$form_child .= '<a href="javascript:void(0);" name="btnHapusAlamat" class="btn btn-danger idBtnHapusAlamat">';
					$form_child .= '<i class="fa fa-trash"></i> Hapus Alamat Kirim';
					$form_child .= '</a>';
					$form_child .= '<div class="form-group col-xs-12"><hr /></div></div>';
				endif;
			endforeach;

			$data['f_attr'] = 'id="idBoxForm"';
			$data['form_url'] = '';
			$data['form_att'] = array('id' => $id_form, 'class' => 'form-horizontal');
			$data['form_hidden'] = '';
			$data['form_close_att'] = '';
			$data['form_field'] = array(
				'jenis_customer_kd' => array(
					'<div class="form-group">',
					'<label for="idSelJenis" class="col-md-2 control-label">Jenis Customer</label>',
					'<div class="col-md-3">',
					'<div id="idErrJenis"></div>',
					form_input(array('type' => 'hidden', 'name' => 'txtKd', 'value' => $kd_customer)),
					form_dropdown('selJenis', $pil_jenis, $sel_jenis, $attr_jenis),
					'</div>',
					'<label for="idSelBadanUsaha" class="col-md-2 control-label">Badan Usaha</label>',
					'<div class="col-md-3">',
					'<div id="idErrBadanUsaha"></div>',
					form_input(array('type' => 'hidden', 'name' => 'txtKdAlamatKirim[]', 'value' => $kd_alamat_kirim_single)),
					form_dropdown('selBadanUsaha[]', $pil_badan_usaha, $sel_badan_usaha, $attr_badan_usaha),
					'</div>',
					'</div>',
				),
				'nm_customer' => array(
					'<div class="form-group">',
					'<label for="idTxtNm" class="col-md-2 control-label">Nama Customer</label>',
					'<div class="col-md-3">',
					'<div id="idErrNm"></div>',
					form_input(array('name' => 'txtNm[]', 'id' => 'idTxtNm', 'class' => 'form-control', 'placeholder' => 'Nama Customer', 'value' => $nm_customer)),
					'</div>',
					'<label for="idTxtKode" class="col-md-2 control-label">Kode Customer</label>',
					'<div class="col-md-2">',
					'<div id="idErrKode"></div>',
					form_input(array('name' => 'txtKode', 'id' => 'idTxtKode', 'class' => 'form-control', 'placeholder' => 'Kode Customer', 'value' => $code_customer, 'readonly' => '')),
					'</div>',
					'</div>',
				),
				'npwp_customer' => array(
					'<div class="form-group">',
					'<label for="idTxtContactPerson" class="col-md-2 control-label">Contact Person</label>',
					'<div class="col-md-3">',
					form_input(array('name' => 'txtContactPerson[]', 'id' => 'idTxtContactPerson', 'class' => 'form-control', 'placeholder' => 'Contact Person', 'value' => $contact_person)),
					'</div>',
					'<label for="idTxtNpwp" class="col-md-2 control-label">No. NPWP/NIK</label>',
					'<div class="col-md-2">',
					'<div id="idErrNpwp"></div>',
					form_input(array('name' => 'txtNpwp[]', 'id' => 'idTxtNpwp', 'class' => 'form-control', 'placeholder' => 'No. NPWP/NIK', 'value' => $npwp_customer)),
					'</div>',
					'</div>',
				),
				'negara_kd' => array(
					'<div class="form_alamat"><div class="form-group">',
					'<label for="idTxtNmNegara0" class="col-md-2 control-label">Negara</label>',
					'<div class="col-md-3">',
					'<div id="idErrNegara"></div>',
					form_dropdown('selNegara[]', $pil_negara, $sel_negara, $attr_negara),
					'</div>',
					'<label for="idTxtNmProvinsi0" class="col-md-2 control-label">Provinsi</label>',
					'<div class="col-md-3">',
					'<div id="idErrProvinsi"></div>',
					form_dropdown('selProvinsi[]', $pil_provinsi, $sel_provinsi, $attr_provinsi),
					'</div>',
					'</div>',
				),
				'kota_kd' => array(
					'<div class="form-group">',
					'<label for="idTxtNmKota0" class="col-md-2 control-label">Kota/Kab.</label>',
					'<div class="col-md-3">',
					'<div id="idErrKota"></div>',
					form_dropdown('selKota[]', $pil_kota, $sel_kota, $attr_kota),
					'</div>',
					'<label for="idTxtNmKecamatan0" class="col-md-2 control-label">Kecamatan</label>',
					'<div class="col-md-3">',
					'<div id="idErrKecamatan"></div>',
					form_dropdown('selKecamatan[]', $pil_kecamatan, $sel_kecamatan, $attr_kecamatan),
					'</div>',
					'</div>',
				),
				'alamat' => array(
					'<div class="form-group">',
					'<label for="idTxtAlamat0" class="col-md-2 control-label">Alamat</label>',
					'<div class="col-md-3">',
					'<div id="idErrAlamat"></div>',
					form_textarea(array('name' => 'txtAlamat[]', 'id' => 'idTxtAlamat0', 'class' => 'form-control', 'placeholder' => 'Alamat', 'value' => $alamat, 'rows' => '5')),
					'</div>',
					'<label for="idTxtKodePos" class="col-md-2 control-label">Kode Pos</label>',
					'<div class="col-md-3">',
					'<div id="idErrKodePos0"></div>',
					form_input(array('name' => 'txtKodePos[]', 'id' => 'idTxtKodePos', 'class' => 'form-control', 'placeholder' => 'Kode Pos', 'value' => $kode_pos, 'maxlength' => 5)),
					'</div></div>',
				),
				'no_telp_utama' => array(
					'<div class="form-group">',
					'<label for="idTxtTelpUtama" class="col-md-2 control-label">Telp Utama</label>',
					'<div class="col-md-3">',
					'<div id="idErrTelpUtama"></div>',
					form_input(array('name' => 'txtTelpUtama[]', 'id' => 'idTxtTelpUtama', 'class' => 'form-control', 'placeholder' => 'Telp Utama', 'value' => $no_telp_utama, 'data-inputmask' => '\'mask\': [\'+99-999-999-999-99\']', 'data-mask' => '')),
					'</div>',
					'<label for="idTxtTelpLain" class="col-md-2 control-label">Telp Lain</label>',
					'<div class="col-md-3">',
					'<div id="idErrTelpLain"></div>',
					form_input(array('name' => 'txtTelpLain[]', 'id' => 'idTxtTelpLain', 'class' => 'form-control', 'placeholder' => 'Telp Lain', 'value' => $no_telp_lain, 'data-inputmask' => '\'mask\': [\'+99-999-999-999-99\']', 'data-mask' => '')),
					'</div>',
					'</div>',
				),
				'email' => array(
					'<div class="form-group">',
					'<label for="idTxtEmail" class="col-md-2 control-label">Email</label>',
					'<div class="col-md-3">',
					'<div id="idErrEmail"></div>',
					form_input(array('name' => 'txtEmail[]', 'id' => 'idTxtEmail', 'class' => 'form-control', 'placeholder' => 'Email', 'value' => $email)),
					'</div>',
					'<label for="idTxtFax" class="col-md-2 control-label">Fax</label>',
					'<div class="col-md-3">',
					'<div id="idErrFax"></div>',
					form_input(array('name' => 'txtFax[]', 'id' => 'idTxtFax', 'class' => 'form-control', 'placeholder' => 'Fax', 'value' => $fax, 'data-inputmask' => '\'mask\': [\'+99-9999-9999999\']', 'data-mask' => '')),
					'</div>',
					'</div>',
				),
				'customer_group' => array(
					'<div class="form-group">',
					'<label for="idTxtEmail" class="col-md-2 control-label">Customer Group (SAP)</label>',
					'<div class="col-md-3">',
					'<div id="idErrCustGroup"></div>',
					form_dropdown('selCustGroup', $pil_cust_group, $sel_cust_group, $attr_cust_group),
					'</div>',
					'<label for="" class="col-md-2 control-label">&nbsp;</label>',
					'<div class="col-md-3">',
					'<div id=""></div>',
					'&nbsp;',
					'</div>',
					'</div>',
				),
				'acc' => array(
					'<div class="form-group">',
					'<label for="idTxtAcc" class="col-md-2 control-label">Account (SAP)</label>',
					'<div class="col-md-3">',
					'<div id="idErrAcc"></div>',
					form_dropdown('selAccDebPay', $pil_acc, $sel_acc, $attr_acc),
					'</div>',
					'<label for="" class="col-md-2 control-label">&nbsp;</label>',
					'<div class="col-md-3">',
					'<div id=""></div>',
					'&nbsp;',
					'</div>',
					'</div>',
				),
				'batas' => array(
					'<a href="javascript:void(0);" name="btnTambahAlamat" id="idBtnTambahAlamat" class="btn btn-success">',
					'<i class="fa fa-address-book"></i> Tambah Alamat Kirim',
					'</a>',
					'<div class="form-group col-xs-12"><hr /></div>',
				),
				'form_child' => array(
					'<div id="form_alamat_child">',
					$form_child,
					'</div>',
				),
			);

			$data['form_btn'] = array(
				'btn_submit' => array(
					'<button type="submit" name="btnSubmit" class="btn btn-primary pull-right">',
					$btn_form,
					'</button>',
				),
			);
			$data['form_box'] = 'idBoxFormBody';
			$data['form_alert'] = 'idAlertForm';
			$data['form_overlay'] = 'idOverlayForm';
			$script['class_name'] = $this->class_link;
			$script['form_error'] = array('idErrJenis', 'idErrBadanUsaha', 'idErrNm', 'idErrKode', 'idErrNpwp', 'idErrNegara', 'idErrProvinsi', 'idErrKota', 'idErrKecamatan', 'idErrAlamat', 'idErrTelpUtama', 'idErrTelpLain', 'idErrEmail', 'idErrFax');

			$this->load->section('form_js', 'script/'.$this->class_link.'/form_js', $script);
			$this->load->view('page/admin/v_form', $data);
		endif;
	}

	function submit_form(){
		if ($this->input->is_ajax_request()) :
			$this->load->library(array('form_validation'));
			$kd_badan_usaha = $this->input->post('selBadanUsaha');
			$nm_customer = $this->input->post('txtNm');
			$negara_kd = $this->input->post('selNegara');
			$nm_negara = $this->input->post('txtNmNegara');
			$provinsi_kd = $this->input->post('selProvinsi');
			$nm_provinsi = $this->input->post('txtNmProvinsi');
			$cust_group_kd = $this->input->post('selCustGroup');
			$debpay_acc_kd = $this->input->post('selAccDebPay');
			$dataAPI = [];

			$this->form_validation->set_rules('selJenis', 'Jenis Customer', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);

			if (empty($kd_badan_usaha[0])) :
				$this->form_validation->set_rules('selBadanUsaha', 'Badan Usaha', 'required',
					array(
						'required' => '{field} tidak boleh kosong!',
					)
				);
			endif;

			if (empty($nm_customer[0])) :
				$this->form_validation->set_rules('txtNm', 'Nama Customer', 'required',
					array(
						'required' => '{field} tidak boleh kosong!',
					)
				);
			endif;

			if (empty($negara_kd[0])) :
				$this->form_validation->set_rules('selNegara', 'Nama Negara', 'required',
					array(
						'required' => '{field} tidak boleh kosong!',
					)
				);
			endif;

			if (empty($provinsi_kd[0])) :
				$this->form_validation->set_rules('selProvinsi', 'Nama Provinsi', 'required',
					array(
						'required' => '{field} tidak boleh kosong!',
					)
				);
			endif;

			if (empty($cust_group_kd[0])) :
				$this->form_validation->set_rules('selCustGroup', 'Customer Group', 'required',
					array(
						'required' => '{field} tidak boleh kosong!',
					)
				);
			endif;

			if (empty($debpay_acc_kd[0])) :
				$this->form_validation->set_rules('selAccDebPay', 'Account', 'required',
					array(
						'required' => '{field} tidak boleh kosong!',
					)
				);
			endif;

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'errValidation';
				$str['idErrJenis'] = (!empty(form_error('selJenis')))?buildLabel('warning', form_error('selJenis', '"', '"')):'';
				$str['idErrBadanUsaha'] = (!empty(form_error('selBadanUsaha')))?buildLabel('warning', form_error('selBadanUsaha', '"', '"')):'';
				$str['idErrNm'] = (!empty(form_error('txtNm')))?buildLabel('warning', form_error('txtNm', '"', '"')):'';
				$str['idErrNegara'] = (!empty(form_error('selNegara')))?buildLabel('warning', form_error('selNegara', '"', '"')):'';
				$str['idErrProvinsi'] = (!empty(form_error('selProvinsi')))?buildLabel('warning', form_error('selProvinsi', '"', '"')):'';
				$str['idErrCustGroup'] = (!empty(form_error('selCustGroup')))?buildLabel('warning', form_error('selCustGroup', '"', '"')):'';
				$str['idErrAcc'] = (!empty(form_error('selAccDebPay')))?buildLabel('warning', form_error('selAccDebPay', '"', '"')):'';
			else :
				/*
				** Jika validasi berhasil dan tidak ada error
				** Data dari form akan diinputkan kedalam tabel
				** Jika query input return true
				** alert success akan ditampilakan, selain itu error (kesalahan sistem!)
				*/
				$kd_customer = $this->input->post('txtKd');
				$kd_alamat_kirim = $this->input->post('txtKdAlamatKirim');
				$jenis_customer_kd = $this->input->post('selJenis');
				$contact_person = $this->input->post('txtContactPerson');
				$code_customer = $this->input->post('txtKode');
				$npwp_customer = $this->input->post('txtNpwp');
				$kota_kd = $this->input->post('selKota');
				$nm_kota = $this->input->post('txtNmKota');
				$kecamatan_kd = $this->input->post('selKecamatan');
				$nm_kecamatan = $this->input->post('txtNmKecamatan');
				$alamat = $this->input->post('txtAlamat');
				$kode_pos = $this->input->post('txtKodePos');
				$no_telp_utama = $this->input->post('txtTelpUtama');
				$no_telp_lain = $this->input->post('txtTelpLain');
				$email = $this->input->post('txtEmail');
				$fax = $this->input->post('txtFax');
				$cust_group_kd = $this->input->post('selCustGroup');
				$debpay_acc_kd = $this->input->post('selAccDebPay');

				$this->db->trans_begin();
				if (empty($kd_customer)) :
					$conds = array(
						'select' => 'tgl_input, '.$this->p_key,
						'order_by' => 'tgl_input DESC',
						'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
					);
					$kd_customer = $this->m_builder->buat_kode($this->tbl_name, $conds, 6, 'CST');
					$data_generate = ['tgl_input' => date('Y-m-d H:i:s')];
					$act = 'create';
				else :
					$data_generate = ['tgl_edit' => date('Y-m-d H:i:s')];
					$act = 'update';
				endif;

				$data_submit = array(
					$this->p_key => $kd_customer,
					'jenis_customer_kd' => $jenis_customer_kd,
					'kd_badan_usaha' => $kd_badan_usaha[0],
					'nm_customer' => $nm_customer[0],
					'contact_person' => $contact_person[0],
					'code_customer' => $code_customer,
					'npwp_customer' => $npwp_customer[0],
					'negara_kd' => $negara_kd[0],
					'provinsi_kd' => $provinsi_kd[0],
					'kota_kd' => $kota_kd[0],
					'kecamatan_kd' => $kecamatan_kd[0],
					'alamat' => $alamat[0],
					'kode_pos' => $kode_pos[0],
					'no_telp_utama' => $no_telp_utama[0],
					'no_telp_lain' => $no_telp_lain[0],
					'email' => $email[0],
					'fax' => $fax[0],
					'cust_group_kd' => $cust_group_kd,
					'debpay_acc_kd' => $debpay_acc_kd,
					'admin_kd' => $this->session->userdata('kd_admin'),
				);

				/** Action ketika insert ke DB ERP */
				$data = array_merge($data_submit, $data_generate);
				if ($act == 'create') :
					$aksi = $this->db->insert($this->tbl_name, $data);
				elseif ($act == 'update') :
					$aksi = $this->db->update($this->tbl_name, $data, [$this->p_key => $kd_customer]);
				endif;

				if ($aksi) :
					$jml_alamat = count($kd_alamat_kirim);
					// $kd_alamat_kirim_new = $this->m_builder->create_pkey('td_customer_alamat', 'kd_alamat_kirim', 'ALC', 'dmy', 6, 'tgl_input DESC, kd_alamat_kirim DESC', '', 0);
					$kd_alamat = $this->td_customer_alamat->create_code();
					$aksi = $this->db->delete('td_customer_alamat', array('customer_kd' => $kd_customer));
					for ($i=0; $i < $jml_alamat; $i++) {
						// $kd_alamat = empty($kd_alamat_kirim[$i])?$kd_alamat_kirim_new:$kd_alamat_kirim[$i];
						// $kd_alamat = $kd_alamat_kirim_new;
						$tgl_edit = empty($kd_alamat_kirim[$i])?NULL:date('Y-m-d H:i:s');
						$data_detail[] = array(
							'kd_alamat_kirim' => $kd_alamat,
							'customer_kd' => $kd_customer,
							'kd_badan_usaha' => $kd_badan_usaha[$i],
							'nm_customer' => $nm_customer[$i],
							'contact_person' => $contact_person[$i],
							'npwp_customer' => $npwp_customer[$i],
							'negara_kd' => $negara_kd[$i],
							'provinsi_kd' => $provinsi_kd[$i],
							'kota_kd' => $kota_kd[$i],
							'kecamatan_kd' => $kecamatan_kd[$i],
							'alamat' => $alamat[$i],
							'kode_pos' => $kode_pos[$i],
							'no_telp_utama' => $no_telp_utama[$i],
							'no_telp_lain' => $no_telp_lain[$i],
							'email' => $email[$i],
							'fax' => $fax[$i],
							'tgl_input' => date('Y-m-d H:i:s'),
							'tgl_edit' => $tgl_edit,
							'admin_kd' => $this->session->userdata('kd_admin'),
						);
						$num = substr($kd_alamat, -6);
						$angka = $num + 1;
						$kd_alamat = 'ALC'.date('dmy').str_pad($angka, 6, '0', STR_PAD_LEFT);
						// if (empty($kd_alamat_kirim[$i])) :
						// 	$kd_alamat_kirim_new = $this->m_builder->create_pkey('td_customer_alamat', 'kd_alamat_kirim', 'ALC', 'dmy', 6, 'tgl_input DESC, kd_alamat_kirim DESC', $kd_alamat_kirim_new, 1);
						// endif;
					}

					$aksi = $this->db->insert_batch('td_customer_alamat', $data_detail);
				endif;
				if ($this->db->trans_status() === FALSE) :
					$this->db->trans_rollback();
					$log_stat = 'gagal';
					$str['confirm'] = 'errValidation';
					$str['idErrForm'] = buildAlert('danger', 'Gagal error transaction, Kesalahan sistem!');
					$str['alert'] = 'Error transaction';
				else :
					/** Action API SAP */
					if ($act == 'create') :
						$api = $this->push_to_sap($kd_customer, "add");
						$label_err = 'API ['.$api[0]->Message.'] menambahkan';
					elseif ($act == 'update') :
						$api = $this->push_to_sap($kd_customer, "edit");
						$label_err = 'API ['.$api[0]->Message.'] mengubah';
					endif;
					/** END Action API SAP */
					if($api[0]->ErrorCode == 0):
						$this->db->trans_commit();
						$log_stat = 'berhasil';
						$str['confirm'] = 'success';
						$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
					else:
						$this->db->trans_rollback();
						$log_stat = 'gagal';
						$str['confirm'] = 'errValidation';
						$str['idErrForm'] = buildAlert('danger', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
						$str['alert'] = $label_err;
					endif;
				endif;
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' '.$log_stat.' '.$label_err.' '.$this->title.' dengan '.$this->p_key.' \''.$kd_customer.'\', jenis_customer_kd \''.$jenis_customer_kd.'\', kd_badan_usaha \''.$kd_badan_usaha[0].'\', nm_customer \''.$nm_customer[0].'\', code_customer \''.$code_customer.'\', npwp_customer \''.$npwp_customer[0].'\', negara_kd \''.$negara_kd[0].'\', provinsi_kd \''.$provinsi_kd[0].'\', kota_kd \''.$kota_kd[0].'\', kecamatan_kd \''.$kecamatan_kd[0].'\', alamat \''.$alamat[0].'\', no_telp_utama \''.$no_telp_utama[0].'\', no_telp_lain \''.$no_telp_lain[0].'\', email \''.$email[0].'\', fax \''.$fax[0].'\'');
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function hapus_data() {
		if ($this->input->is_ajax_request()) :
			$kd_customer = $this->input->get('id');
			$label_err = 'menghapus';
			$aksi = $this->db->delete($this->tbl_name, array($this->p_key => $kd_customer));
			$aksi = $this->db->delete('td_customer_alamat', array('customer_kd' => $kd_customer));

			if ($aksi) :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' dengan '.$this->p_key.' \''.$kd_customer.'\'!');
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
			else :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' '.$this->title.' dengan '.$this->p_key.' \''.$kd_customer.'\'');
				$str['alert'] = buildAlert('danger', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function buat_kode() {
		if ($this->input->is_ajax_request()) :
			$nm_customer = $this->input->get('nama');
			$kd_customer = $this->input->get('kode');
			$code_customer = $this->input->get('code');
			// Cek kode customer
			$this->db->from('tm_customer');
			$this->db->where(array('kd_customer !=' => $kd_customer, 'code_customer' => $code_customer));
			$query = $this->db->get();
			$num = $query->num_rows();
			$nm_customer = strtoupper(substr(str_replace(' ', '', $nm_customer), 0, 4));
			$str['code_customer'] = $this->tm_customer->create_customer_code($nm_customer);

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function cek_lokasi() {
		if ($this->input->is_ajax_request()) :
			$table = $this->input->get('table', TRUE);
			$res = $this->input->get('result', TRUE);
			$search = $this->input->get('search', TRUE);
			$key_maker = $this->input->get('key', TRUE);
			$input = $this->input->get('input', TRUE);

			if (!empty($search)) :
				foreach ($search as $key => $value) :
					foreach ($value as $string => $param) :
						$this->db->{$key}($string, $param);
					endforeach;
				endforeach;
			endif;
			$this->db->from($table);
			$query = $this->db->get();
			$jumlah = $query->num_rows();
			if ($jumlah > 0) :
				$result = $query->row();
				$str['kd_lokasi'] = $result->{$res};
			else :
				$no_kosong = 0;
				$conds = array(
					'select' => 'tgl_input, '.$res,
					'order_by' => 'tgl_input DESC, '.$res.' DESC',
				);
				if (isset($search['where'])) :
					$conds = array_merge($conds, array('where' => $search['where']));
				endif;
				$kd_lokasi = $this->m_builder->buat_kode($table, $conds, 3, '');

				foreach ($input as $key => $value) :
					if (!empty($value)) :
						$data[$key] = strtoupper($value);
					else :
						$no_kosong++;
					endif;
				endforeach;
				if ($no_kosong < 1) :
					$data = array_merge($data, array($res => $kd_lokasi, 'tgl_input' => date('Y-m-d H:i:s'), 'admin_kd' => $this->session->userdata('kd_admin')));
					$aksi = $this->db->insert($table, $data);

					$str['kd_lokasi'] = $aksi?$kd_lokasi:'';
				else :
					$str['kd_lokasi'] = '';
				endif;
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function pilih_lokasi() {
		if ($this->input->is_ajax_request()) :
			$type = $this->input->get('type');
			if ($type == 'negara') :
				$kd_negara = $this->input->get('kd_negara');
				$this->db->where(array('negara_kd' => $kd_negara));
				$this->db->from('tb_provinsi');
				$query = $this->db->get();
				$result = $query->result();
				$str['data_provinsi'] = '<option value="">-- Pilih Provinsi --</option>';
				foreach ($result as $row) :
					$str['data_provinsi'] .= '<option value="'.$row->kd_provinsi.'">'.$row->nm_provinsi.'</option>';
				endforeach;
			elseif ($type == 'provinsi') :
				$kd_negara = $this->input->get('kd_negara');
				$kd_provinsi = $this->input->get('kd_provinsi');
				$this->db->where(array('negara_kd' => $kd_negara, 'provinsi_kd' => $kd_provinsi));
				$this->db->from('tb_kota');
				$query = $this->db->get();
				$result = $query->result();
				$str['data_kota'] = '<option value="">-- Pilih Kota --</option>';
				foreach ($result as $row) :
					$str['data_kota'] .= '<option value="'.$row->kd_kota.'">'.$row->nm_kota.'</option>';
				endforeach;
			elseif ($type == 'kota') :
				$kd_negara = $this->input->get('kd_negara');
				$kd_provinsi = $this->input->get('kd_provinsi');
				$kd_kota = $this->input->get('kd_kota');
				$this->db->where(array('negara_kd' => $kd_negara, 'provinsi_kd' => $kd_provinsi, 'kota_kd' => $kd_kota));
				$this->db->from('tb_kecamatan');
				$query = $this->db->get();
				$result = $query->result();
				$str['data_kecamatan'] = '<option value="">-- Pilih Kecamatan --</option>';
				foreach ($result as $row) :
					$str['data_kecamatan'] .= '<option value="'.$row->kd_kecamatan.'">'.$row->nm_kecamatan.'</option>';
				endforeach;
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function tambah_lokasi() {
		if ($this->input->is_ajax_request()) :
			// select option untuk badan usaha
			$this->db->from('tb_set_dropdown')->where(array('jenis_select' => 'customer_property'));
			$query = $this->db->get();
			$result = $query->result();
			$pil_badan_usaha = array('' => '-- Pilih Badan Usaha --');
			foreach ($result as $row) :
				$id = $this->security->xss_clean($row->id);
				$nm_select = $this->security->xss_clean($row->nm_select);
				$pil_badan_usaha[$id] = $nm_select;
			endforeach;

			// select option untuk negara
			$this->db->from('tb_negara');
			$query = $this->db->get();
			$result = $query->result();
			$pil_negara = array('' => '-- Pilih Negara --');
			foreach ($result as $row) :
				$kd_negara = $this->security->xss_clean($row->kd_negara);
				$nm_negara = $this->security->xss_clean($row->nm_negara);
				$pil_negara[$kd_negara] = $nm_negara;
			endforeach;

			// select option untuk provinsi
			$pil_provinsi = array('' => '-- Pilih Provinsi --');

			// select option untuk kota
			$pil_kota = array('' => '-- Pilih Kota --');

			// select option untuk kecamatan
			$pil_kecamatan = array('' => '-- Pilih Kecamatan --');

			// Ambil detail alamat kirim
			$no = $this->input->get('jml');
			$form_child = '';

			$sel_badan_usaha_child = '';
			$nm_customer_child = '';
			$contact_person_child = '';
			$npwp_customer_child = '';
			$attr_badan_usaha_child = array('id' => 'idSelBadanUsaha'.$no, 'class' => 'form-control');
			$sel_negara_child = '';
			$attr_negara_child = array('id' => 'idSelNegara'.$no, 'class' => 'form-control select2 idSelNegara', 'style' => 'width:100%;');
			$sel_provinsi_child = '';
			$attr_provinsi_child = array('id' => 'idSelProvinsi'.$no, 'class' => 'form-control select2 idSelProvinsi', 'style' => 'width:100%;');
			$sel_kota_child = '';
			$attr_kota_child = array('id' => 'idSelKota'.$no, 'class' => 'form-control select2 idSelKota', 'style' => 'width:100%;');
			$sel_kecamatan_child = '';
			$attr_kecamatan_child = array('id' => 'idSelKecamatan'.$no, 'class' => 'form-control select2 idSelKecamatan', 'style' => 'width:100%;');
			$no_telp_utama_child = '';
			$no_telp_lain_child = '';
			$email_child = '';
			$fax_child = '';

			$form_child .= '<div class="form_alamat">';
			$form_child .= '<div class="form-group">';
			$form_child .= '<label for="idSelBadanUsaha'.$no.'" class="col-md-2 control-label">Badan Usaha</label>';
			$form_child .= '<div class="col-md-3">';
			$form_child .= form_input(array('type' => 'hidden', 'name' => 'txtKdAlamatKirim[]'));
			$form_child .= form_dropdown('selBadanUsaha[]', $pil_badan_usaha, $sel_badan_usaha_child, $attr_badan_usaha_child);
			$form_child .= '</div>';
			$form_child .= '<label for="idTxtNm'.$no.'" class="col-md-2 control-label">Nama Customer</label>';
			$form_child .= '<div class="col-md-3">';
			$form_child .= form_input(array('name' => 'txtNm[]', 'class' => 'form-control', 'placeholder' => 'Nama Customer', 'value' => $nm_customer_child));
			$form_child .= '</div>';
			$form_child .= '</div>';
					
			$form_child .= '<div class="form-group">';
			$form_child .= '<label for="idTxtContactPerson'.$no.'" class="col-md-2 control-label">Contact Person</label>';
			$form_child .= '<div class="col-md-3">';
			$form_child .= form_input(array('name' => 'txtContactPerson[]', 'id' => 'idTxtContactPerson'.$no, 'class' => 'form-control', 'placeholder' => 'Contact Person', 'value' => $contact_person_child));
			$form_child .= '</div>';
			$form_child .= '<label for="idTxtNpwp'.$no.'" class="col-md-2 control-label">No. NPWP</label>';
			$form_child .= '<div class="col-md-2">';
			$form_child .= form_input(array('name' => 'txtNpwp[]', 'id' => 'idTxtNpwp'.$no, 'class' => 'form-control', 'placeholder' => 'No. NPWP', 'value' => $npwp_customer_child, 'data-inputmask' => '\'mask\': [\'99.999.999.9-999.999\']', 'data-mask' => ''));
			$form_child .= '</div>';
			$form_child .= '</div>';

			$form_child .= '<div class="form-group">
				<label for="idSelNegara'.$no.'" class="col-md-2 control-label">Negara</label>
				<div class="col-md-3">';
			$form_child .= form_dropdown('selNegara[]', $pil_negara, $sel_negara_child, $attr_negara_child);
			$form_child .= '</div>';

			$form_child .= '<label for="idSelProvinsi'.$no.'" class="col-md-2 control-label">Provinsi</label>
				<div class="col-md-3">';
			$form_child .= form_dropdown('selProvinsi[]', $pil_provinsi, $sel_provinsi_child, $attr_provinsi_child);
			$form_child .= '</div></div>';

			$form_child .= '<div class="form-group">
				<label for="idSelKota'.$no.'" class="col-md-2 control-label">Kota</label>
				<div class="col-md-3">';
			$form_child .= form_dropdown('selKota[]', $pil_kota, $sel_kota_child, $attr_kota_child);
			$form_child .= '</div>';

			$form_child .= '<label for="idSelKecamatan'.$no.'" class="col-md-2 control-label">Kecamatan</label>
				<div class="col-md-3">';
			$form_child .= form_dropdown('selKecamatan[]', $pil_kecamatan, $sel_kecamatan_child, $attr_kecamatan_child);
			$form_child .= '</div></div>';


			$form_child .= '<div class="form-group">
				<label for="idTxtAlamat'.$no.'" class="col-md-2 control-label">Alamat</label>
				<div class="col-md-3">';
			$form_child .= form_textarea(array('name' => 'txtAlamat[]', 'id' => 'idTxtAlamat'.$no, 'class' => 'form-control idTxtAlamat', 'placeholder' => 'Alamat', 'rows' => '5'));
			$form_child .= '</div>';

			$form_child .= '<label for="idTxtKodePos'.$no.'" class="col-md-2 control-label">Kode Pos</label>
				<div class="col-md-3">';
			$form_child .= form_input(array('name' => 'txtKodePos[]', 'id' => 'idTxtKodePos', 'class' => 'form-control', 'placeholder' => 'Kode Pos', 'maxlength' => 5));
			$form_child .= '</div></div>';
			
			$form_child .= '<div class="form-group">';
			$form_child .= '<label for="idTxtTelpUtama'.$no.'" class="col-md-2 control-label">Telp Utama</label>';
			$form_child .= '<div class="col-md-3">';
			$form_child .= form_input(array('name' => 'txtTelpUtama[]', 'id' => 'idTxtTelpUtama'.$no, 'class' => 'form-control', 'placeholder' => 'Telp Utama', 'value' => $no_telp_utama_child, 'data-inputmask' => '\'mask\': [\'+99-999-999-999-99\']', 'data-mask' => ''));
			$form_child .= '</div>';
			$form_child .= '<label for="idTxtTelpLain'.$no.'" class="col-md-2 control-label">Telp Lain</label>';
			$form_child .= '<div class="col-md-3">';
			$form_child .= form_input(array('name' => 'txtTelpLain[]', 'id' => 'idTxtTelpLain'.$no, 'class' => 'form-control', 'placeholder' => 'Telp Lain', 'value' => $no_telp_lain_child, 'data-inputmask' => '\'mask\': [\'+99-999-999-999-99\']', 'data-mask' => ''));
			$form_child .= '</div>';
			$form_child .= '</div>';

			$form_child .= '<div class="form-group">';
			$form_child .= '<label for="idTxtEmail'.$no.'" class="col-md-2 control-label">Email</label>';
			$form_child .= '<div class="col-md-3">';
			$form_child .= form_input(array('name' => 'txtEmail[]', 'id' => 'idTxtEmail'.$no, 'class' => 'form-control', 'placeholder' => 'Email', 'value' => $email_child));
			$form_child .= '</div>';
			$form_child .= '<label for="idTxtFax'.$no.'" class="col-md-2 control-label">Fax</label>';
			$form_child .= '<div class="col-md-3">';
			$form_child .= form_input(array('name' => 'txtFax[]', 'id' => 'idTxtFax'.$no, 'class' => 'form-control', 'placeholder' => 'Fax', 'value' => $fax_child, 'data-inputmask' => '\'mask\': [\'+99-9999-9999999\']', 'data-mask' => ''));
			$form_child .= '</div>';
			$form_child .= '</div>';
			$form_child .= '<a href="javascript:void(0);" name="btnHapusAlamat" class="btn btn-danger idBtnHapusAlamat">';
			$form_child .= '<i class="fa fa-trash"></i> Hapus Alamat Kirim';
			$form_child .= '</a>';
			$form_child .= '<div class="form-group col-xs-12"><hr /></div></div>';

			$str['form_child'] = $form_child;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function push_to_sap($kd, $act){
		$data_header = $this->tm_customer->get_row($kd);
		$data_detail_alamat = $this->td_customer_alamat->get_by_param(['customer_kd' => $data_header->kd_customer])->result();
		
		/** Check jenis currency jika Lokal = IDR , Ekspor = USD */
		$tipe_customer = $this->model_jenis_customer->get_tipe($data_header->kd_customer, 'tipe_customer');
		if($tipe_customer == 'Ekspor'){
			$currency = "USD";
		}else{
			$currency = "IDR";
		}

		/** Fill header data submit to variabel API SAP */
		$dataAPI = [
			'CardCode' => $data_header->code_customer,
			'CardName' => $data_header->nm_customer,
			'CardType' => "C", // C di SAP penanda untuk customer
			'KdGroup' => $data_header->cust_group_kd, // Customer/BP Group
			'CntctPrsn' => $data_header->contact_person != null || $data_header->contact_person != "" ? $data_header->contact_person : "",
			'LicTradNum' => $data_header->npwp_customer,
			'DebPayAcct' => $data_header->debpay_acc_kd,
			'Phone1' => $data_header->no_telp_utama != null || $data_header->no_telp_utama != "" ? str_replace(['+', '-'], '', $data_header->no_telp_utama) : "",
			'Phone2' => $data_header->no_telp_lain != null || $data_header->no_telp_lain != "" ? str_replace(['+', '-'], '', $data_header->no_telp_lain) : "",
			'Email' => $data_header->email != null || $data_header->email != "" ? $data_header->email : "",
			'Fax' => $data_header->fax != null || $data_header->fax != "" ? str_replace(['+', '-'], '', $data_header->fax) : "",
			'Currency' => $currency,
			'Street' => substr($data_header->alamat, 0, 100), // Masuk SAP max 100 karakter, jika lebih dari 100 sisanya tidak masuk SAP
			'ZipCode' => $data_header->kode_pos != null || $data_header->kode_pos != "" ? $data_header->kode_pos : "",
			'U_IDU_WEBID' => $data_header->kd_customer,
			'U_IDU_WEBUSER' => $this->session->userdata('username'),
			'U_IDU_KodeJenisCustomer' => $data_header->jenis_customer_kd,
			'U_IDU_BadanUsaha' => $this->tb_set_dropdown->get_by_param(['id' => $data_header->kd_badan_usaha])->row()->nm_select,
			'Lines_ShipTo' => []
		];
		/** Fill variabel for submit detail to SAP */
		/** Get alamat master push to SAP */
		$data_detail_sap = [];
		$data_detail_sap = [
			'Street' => $dataAPI['Street'],
			'ZipCode' => $dataAPI['ZipCode'],
			'Phone1' => $dataAPI['Phone1'],
			'Phone2' => $dataAPI['Phone2'],
			'Email' => $dataAPI['Email'],
			'Fax' => $dataAPI['Fax'],
			'U_IDU_BadanUsaha' => $dataAPI['U_IDU_BadanUsaha'],
			'U_IDU_NamaCustomer' => $dataAPI['CardName'],
			'U_IDU_CP' => $dataAPI['CntctPrsn'],
		];
		array_push($dataAPI['Lines_ShipTo'], $data_detail_sap); 
		if ($act == "add"):
			$api = parent::api_sap_post('AddBP', $dataAPI);
		else:
			$api = parent::api_sap_post('EditBP', $dataAPI);
		endif;
		return $api;
	}
}