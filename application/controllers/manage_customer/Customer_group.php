<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Customer_group extends MY_Controller {
	private $class_link = 'manage_customer/customer_group';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_cust_group']);
	}

	public function index() {
        parent::administrator();
		parent::pnotify_assets();
        $data['class_link'] = $this->class_link;

        $this->load->view('page/'.$this->class_link.'/table_box', $data);
	}
    
    public function table_main(){
    	if (!$this->input->is_ajax_request()){
    		exit('No direct script access allowed');
    	}
    	$data['class_link'] = $this->class_link;
    	$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }

    public function table_data (){
    	if (!$this->input->is_ajax_request()){
    		exit('No direct script access allowed');
    	}
    	$this->load->library(['ssp']);

		$data = $this->tm_cust_group->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
    }

	function form_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', true);
		if (!empty($id)) {
			$data['rowData'] = $this->tm_cust_group->get_by_param (['cust_group_kd' => $id])->row_array();
			$sts = 'edit';
		}else{
			$sts = 'add';
		}
		$data['sts'] = $sts;
		$data['id'] = $id;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}
    
    public function action_submit () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->db->trans_begin();
		$cust_group_kd = $this->input->post('txt_kd', true);	
		$cust_group_name = $this->input->post('nmcust_group_name', true);	
		$sts = $this->input->post('txtSts', true);			
		$data = [
			'cust_group_name' => $cust_group_name,
		];
		$act = false;
		if($sts == 'edit'){
			$act = $this->tm_cust_group->update_data(['cust_group_kd' => $cust_group_kd], $data);
		}else if($sts == "add"){
			$act = $this->tm_cust_group->insert_data($data);
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan, error transaction !');
		}else{
			if($sts == 'edit'){
				$api = $this->push_to_sap($cust_group_kd, "edit");
			}else if($sts == "add"){
				$api = $this->push_to_sap($this->db->insert_id(), "add");
			}
			if($api[0]->ErrorCode == 0){
				$this->db->trans_commit();
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan, API['.$api[0]->Message.']');
			}else{
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan, API['.$api[0]->Message.']');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_hapus () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->db->trans_begin();
		$id = $this->input->get('id', TRUE);
		$act = $this->tm_cust_group->delete_data($id);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Terhapus, error transaction !');
		}else{
			$api = $this->push_to_sap($id, "delete");
			if($api[0]->ErrorCode == 0){
				$this->db->trans_commit();
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus, API['.$api[0]->Message.']');
			}else{
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Terhapus, API['.$api[0]->Message.']');
			}
		}
		echo json_encode($resp);
	}

	public function push_to_sap($kd, $act){
		$data = $this->tm_cust_group->get_row($kd);
		if ($act == "add"):
			$dataAPI = [
				'U_IDU_WEBID' => $data->cust_group_kd,
				'GroupName' => $data->cust_group_name,
				'GroupType' => "C", /** C adalah customer */
			];
			$api = parent::api_sap_post('AddBPGroup', $dataAPI);
		elseif ($act == "edit"):
			$dataAPI = [
				'U_IDU_WEBID' => $data->cust_group_kd,
				'GroupName' => $data->cust_group_name,
			];
			$api = parent::api_sap_post('EditBPGroup', $dataAPI);
		else:
			$dataAPI = [
				'U_IDU_WEBID' => $kd
			];
			$api = parent::api_sap_post('DeleteBPGroup', $dataAPI);
		endif;
		return $api;
	}
	
}
