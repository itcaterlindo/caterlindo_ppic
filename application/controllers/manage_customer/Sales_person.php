<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_person extends MY_Controller {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'tb_salesperson';
	private $p_key = 'kd_salesperson';
	private $class_link = 'manage_customer/sales_person';
	private $title = 'Data Sales Person';
	
	function __construct() {
		parent::__construct();
		
		$this->load->model(array('m_builder', 'm_salesperson'));
		$this->load->helper(array('form', 'html', 'my_helper'));
	}

	function index() {
		parent::administrator();
		parent::pnotify_assets();

		$data['t_box_class'] = 'box-primary';
		$title = $this->uri->segment_array();
		$title = end($title);
		if (strpos($title, '_') !== FALSE ) :
			$title = str_replace('_', ' ', $title);
		endif;
		$title = ucwords($title);
		$data['table_title'] = $title;
		$data['table_alert'] = 'idAlertTable';
		$data['table_loader'] = 'idLoaderTable';
		$data['table_name'] = 'idTable';
		$data['table_overlay'] = 'idTableOverlay';
		$script['class_name'] = $this->class_link;
		$data['btn_add'] = cek_permission('SALESPERSON_CREATE');

        $this->load->js('assets/admin_assets/plugins/typeahead.js/typeahead.bundle.min.js');
        $this->load->css('assets/admin_assets/plugins/typeahead.js/typeahead.css');
		$this->load->section('scriptJS', 'script/'.$this->class_link.'/scriptJS', $script);
		// $this->data_admin_form();
		$this->load->view('page/admin/v_table', $data);
	}

	function data_lihat() {
		if ($this->input->is_ajax_request()) :
			/* tabel property */
			$t_data['btn'] = TRUE;
			$t_data['t_header'] = array(
				'<th style="width:1%;">Kode Karyawan</th>',
				'<th style="width:10%;">Nama Sales Person</th>',
				'<th style="width:5%;">Email</th>',
				'<th style="width:5%;">No Telp</th>',
			);
			$t_data['t_uri'] = base_url().$this->class_link.'/data_table';
			$t_data['t_order'] = '[2, "asc"]';

			$this->load->view('page/admin/table_ssp', $t_data);
		endif;
	}

	function data_table() {
		if (!$this->input->is_ajax_request()) :
            exit('No direct script access allowed');
        else :

            $this->load->library('ssp');

			$table = $this->tbl_name;

			$primaryKey = $this->p_key;

			$columns = array(
			    array( 'db' => $this->p_key,
						'dt' => 1, 'field' => $this->p_key,
						'formatter' => function($d){
							$btn_edit = '';
							$divider = '';
							$btn_delete = '';
							if (cek_permission('SALESPERSON_UPDATE')) :
								$btn_edit = '<a id="edit" title="Edit Data" href="javascript:void(0);" onclick="editData(\''.$d.'\')"><i class="fa fa-pencil"></i> Edit Data</a>';
								$divider = '<li class="divider"></li>';
							endif;
							if (cek_permission('SALESPERSON_DELETE')) :
								$btn_delete = '<a id="delete" title="Hapus Data" href="javascript:void(0);" onclick="return confirm(\'Apakah anda yakin?\')?hapusData(\''.$d.'\'):false;"><i class="fa fa-trash"></i> Hapus Data</a>';
							endif;

							$btn = '<div align="center"><div class="btn-group"><button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">Opsi <span class="caret"></span></button><ul class="dropdown-menu"><li>'.$btn_edit.'</li>'.$divider.'<li>'.$btn_delete.'</li></ul></div></div>';
							
							return $btn;
						} ),
			    array( 'db' => 'karyawan_kd',
			    		'dt' => 2, 'field' => 'karyawan_kd',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'nm_salesperson',
			    		'dt' => 3, 'field' => 'nm_salesperson',
			    		'formatter' => function($d){
			    			$d = replace_empty($this->security->xss_clean($d), '-');

			    			return $d;
			    		} ),
			    array( 'db' => 'email_address',
			    		'dt' => 4, 'field' => 'email_address',
			    		'formatter' => function($d){
			    			$d = replace_empty($this->security->xss_clean($d), '-');

			    			return $d;
			    		} ),
			    array( 'db' => 'no_telp',
			    		'dt' => 5, 'field' => 'no_telp',
			    		'formatter' => function($d){
			    			$d = replace_empty($this->security->xss_clean($d), '-');

			    			return $d;
			    		} ),
			);

			$sql_details = sql_connect();

			$joinQuery = "";
			$where = "";

			echo json_encode(
			    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $where )
			);
		endif;
	}

	function data_form(){
		if ($this->input->is_ajax_request()) :
			// Form data
			$id = $this->input->get('id');
			if (!empty($id)) :
				$det = $this->m_builder->getRow($this->tbl_name, $data = array($this->p_key => $id), 'row');
				$id_form = 'idFormEdit';
				$data['f_box_class'] = 'box-warning';
				$data['form_title'] = 'Edit '.$this->title;
				$btn_form = '<i class="fa fa-edit"></i> Edit Data';
				$kd_salesperson = $this->security->xss_clean($det->kd_salesperson);
				$nm_salesperson = $this->security->xss_clean($det->nm_salesperson);
				$email_address = $this->security->xss_clean($det->email_address);
				$no_telp = $this->security->xss_clean($det->no_telp);
				$karyawan_kd = $this->security->xss_clean($det->karyawan_kd);
				if ($karyawan_kd != '-') :
					$db_hrm = $this->load->database('sim_hrm', TRUE);
					$db_hrm->select('nm_karyawan');
					$db_hrm->where(array('kd_karyawan' => $karyawan_kd));
					$db_hrm->from('tb_karyawan');
					$q_hrm = $db_hrm->get();
					$j_hrm = $q_hrm->num_rows();
					$r_hrm = $q_hrm->row();
					$nm_karyawan = $this->security->xss_clean($r_hrm->nm_karyawan);
					$check = TRUE;
					$view_karyawan = '';
				else :
					$nm_karyawan = '';
					$check = FALSE;
					$view_karyawan = 'style="display:none;"';
				endif;
			else :
				$id_form = 'idFormInput';
				$data['f_box_class'] = 'box-info';
				$data['form_title'] = 'Input '.$this->title;
				$btn_form = '<i class="fa fa-save"></i> Input Data';
				$kd_salesperson = '';
				$nm_salesperson = '';
				$karyawan_kd = '';
				$nm_karyawan = '';
				$email_address = '';
				$no_telp = '';
				$check = FALSE;
				$view_karyawan = 'style="display:none;"';
			endif;

			// Check untuk link data karyawan
			$chkLinkAttr = array(
				'name' => 'chkLink',
				'id' => 'idChkLink',
				'value' => '1',
				'checked' => $check,
			);

			$data['f_attr'] = 'id="idBoxForm"';
			$data['form_url'] = '';
			$data['form_att'] = array('id' => $id_form, 'class' => 'form-horizontal');
			$data['form_hidden'] = '';
			$data['form_close_att'] = '';
			$data['form_field'] = array(
				'nm_tipe_admin' => array(
					'<div class="form-group">',
					'<label for="idTxtNm" class="col-md-2 control-label">Nama Sales Person</label>',
					'<div class="col-md-4">',
					'<div id="idErrNm"></div>',
					form_input(array('type' => 'hidden', 'name' => 'txtKd', 'id' => 'idTxtKd', 'value' => $kd_salesperson)),
					form_input(array('name' => 'txtNm', 'id' => 'idTxtNm', 'class' => 'form-control', 'placeholder' => 'Nama Sales Person', 'value' => $nm_salesperson)),
					'</div>',
					'</div>',
				),
				'check_link_karyawan' => array(
					'<div class="form-group">',
					'<div class="col-sm-offset-2 col-sm-10"><div class="checkbox">',
					'<label>',
					form_checkbox($chkLinkAttr),
					'Link Data Karyawan',
					'</label></div></div></div>',
				),
				'nama_karyawan' => array(
					'<div id="idFormNmKaryawan" '.$view_karyawan.'><div class="form-group">',
					'<label for="idTxtNmKaryawan" class="col-md-2 control-label">Nama Karyawan</label>',
					'<div class="col-md-4">',
					'<div id="idErrKaryawan"></div>',
					form_input(array('type' => 'hidden', 'name' => 'txtKdKaryawan', 'id' => 'idTxtKdKaryawan', 'value' => $karyawan_kd)),
                    '<div id="scrollable-dropdown-menu">',
						form_input(array('name' => 'txtNamaKaryawan', 'id' => 'idTxtNmKaryawan', 'class' => 'form-control', 'placeholder' => 'Nama Karyawan', 'value' => $nm_karyawan)),
					'</div></div></div></div>',
				),
				'email_address' => array(
					'<div class="form-group">',
					'<label for="idTxtEmail" class="col-md-2 control-label">Email Address</label>',
					'<div class="col-md-3">',
					'<div id="idErrEmail"></div>',
					form_input(array('name' => 'txtEmail', 'id' => 'idTxtEmail', 'class' => 'form-control', 'placeholder' => 'Email Address', 'value' => $email_address)),
					'</div>',
					'</div>',
				),
				'no_telp' => array(
					'<div class="form-group">',
					'<label for="idTxtTelp" class="col-md-2 control-label">Telp Number</label>',
					'<div class="col-md-2">',
					'<div id="idErrTelp"></div>',
					form_input(array('name' => 'txtTelp', 'id' => 'idTxtTelp', 'class' => 'form-control', 'placeholder' => 'Telp Number', 'value' => $no_telp)),
					'</div>',
					'</div>',
				),
			);

			$data['form_btn'] = array(
				'btn_submit' => array(
					'<button type="submit" name="btnSubmit" class="btn btn-primary pull-right">',
					$btn_form,
					'</button>',
				),
			);
			$data['form_box'] = 'idBoxFormBody';
			$data['form_alert'] = 'idAlertForm';
			$data['form_overlay'] = 'idOverlayForm';
			$script['class_name'] = $this->class_link;

			$this->load->section('form_css', 'css/auto_complete/form_css');
			$this->load->section('form_js', 'script/'.$this->class_link.'/form_js', $script);
			$this->load->view('page/admin/v_form', $data);
		endif;
	}

	function submit_form($act){
		if ($this->input->is_ajax_request()) :
			$this->load->library(array('form_validation'));
			$str['csrf'] = $this->security->get_csrf_hash();
			$kd_salesperson = $this->input->post('txtKd');
			$nm_salesperson = $this->input->post('txtNm');
			$email_address = $this->input->post('txtEmail');
			$no_telp = $this->input->post('txtTelp');
			$check = $this->input->post('chkLink');
			$karyawan_kd = $this->input->post('txtKdKaryawan');
			$karyawan_kd = isset($check)?$karyawan_kd:'-';
			$nm_karyawan = $this->input->post('txtNamaKaryawan');

			$this->form_validation->set_rules('txtNm', 'Nama Sales Person', 'required');
			$this->form_validation->set_rules('txtEmail', 'Email Address', 'required|valid_email');
			$this->form_validation->set_rules('txtTelp', 'Telp Number', 'required');
			if (isset($check)) :
				$this->form_validation->set_rules('txtNamaKaryawan', 'Nama Karyawan', 'required');
			endif;

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'errValidation';
				$str['idErrNm'] = (!empty(form_error('txtNm')))?buildLabel('warning', form_error('txtNm', '"', '"')):'';
				$str['idErrKaryawan'] = (!empty(form_error('txtNamaKaryawan')))?buildLabel('warning', form_error('txtNamaKaryawan', '"', '"')):'';
				$str['idErrEmail'] = (!empty(form_error('txtEmail')))?buildLabel('warning', form_error('txtEmail', '"', '"')):'';
				$str['idErrTelp'] = (!empty(form_error('txtTelp')))?buildLabel('warning', form_error('txtTelp', '"', '"')):'';
			else :
				/*
				** Jika validasi berhasil dan tidak ada error
				** Data dari form akan diinputkan kedalam tabel
				** Jika query input return true
				** alert success akan ditampilakan, selain itu error (kesalahan sistem!)
				*/
				if ($act == 'input') :
					$conds = array(
						'select' => 'tgl_input, '.$this->p_key,
						'order_by' => 'tgl_input DESC',
						'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
					);
					$kd_salesperson = $this->m_builder->buat_kode($this->tbl_name, $conds, 3, 'SLS');

					$data = array(
						$this->p_key => $kd_salesperson,
						'nm_salesperson' => $nm_salesperson,
						'karyawan_kd' => $karyawan_kd,
						'email_address' => $email_address,
						'no_telp' => $no_telp,
						'tgl_input' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					$this->db->trans_begin();
					$this->db->insert($this->tbl_name, $data);
					if ($this->db->trans_status() === FALSE){
						$this->db->trans_rollback();
						$label_err = 'Error transaction';
						$aksi = false;
					}
					else{
						$api = $this->push_to_sap($kd_salesperson, "add");
						if($api[0]->ErrorCode == 0){
							$this->db->trans_commit();
							$label_err = 'API ['.$api[0]->Message.'] menambahkan';
							$aksi = true;
						}else{
							$this->db->trans_rollback();
							$label_err = 'API ['.$api[0]->Message.']';
							$aksi = false;
						}
					}
				elseif ($act == 'edit') :
					$data = array(
						'nm_salesperson' => $nm_salesperson,
						'karyawan_kd' => $karyawan_kd,
						'email_address' => $email_address,
						'no_telp' => $no_telp,
						'tgl_edit' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					$this->db->trans_begin();
					$where = array($this->p_key => $kd_salesperson);
					$this->db->update($this->tbl_name, $data, $where);
					if ($this->db->trans_status() === FALSE){
						$this->db->trans_rollback();
						$label_err = 'Error transaction';
						$aksi = false;
					}
					else{
						$api = $this->push_to_sap($kd_salesperson, "edit");
						if($api[0]->ErrorCode == 0){
							$this->db->trans_commit();
							$label_err = 'API ['.$api[0]->Message.'] mengubah';
							$aksi = true;
						}else{
							$this->db->trans_rollback();
							$label_err = 'API ['.$api[0]->Message.']';
							$aksi = false;
						}
					}
				endif;
				if ($aksi) :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' dengan '.$this->p_key.' \''.$kd_salesperson.'\' nm_salesperson \''.$nm_salesperson.'\' karyawan_kd \''.$karyawan_kd.'\' email_address \''.$email_address.'\' no_telp \''.$no_telp.'\'');
					$str['confirm'] = 'success';
					$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
				else :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' '.$this->title.' dengan '.$this->p_key.' \''.$kd_salesperson.'\' nm_salesperson \''.$nm_salesperson.'\' karyawan_kd \''.$karyawan_kd.'\' email_address \''.$email_address.'\' no_telp \''.$no_telp.'\'');
					$str['confirm'] = 'errValidation';
					$str['idErrForm'] = buildAlert('danger', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
					$str['alert'] = $label_err;
				endif;
			endif;
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function hapus_data() {
		if ($this->input->is_ajax_request()) :
			$kd_salesperson = $this->input->get('id');
			$label_err = 'menghapus';
			$aksi = $this->db->delete($this->tbl_name, array($this->p_key => $kd_salesperson));

			if ($aksi) :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' dengan '.$this->p_key.' \''.$kd_salesperson.'\'!');
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
			else :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' '.$this->title.' dengan '.$this->p_key.' \''.$kd_salesperson.'\'');
				$str['alert'] = buildAlert('danger', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function push_to_sap($kd, $act){
		$data = $this->m_salesperson->get_row($kd);
		/** API TO SAP */
		$dataAPI = [
			'U_IDU_WEBID' => $data->kd_salesperson,
			'SlpName' => $data->nm_salesperson,
			'Email' => $data->email_address,
			'Telp' => $data->no_telp
		];
		if($act == "add"):
			$api = parent::api_sap_post('AddSalesPerson', $dataAPI);
		else:
			$api = parent::api_sap_post('EditSalesPerson', $dataAPI);
		endif;
		/** END API TO SAP */
		return $api;
	}

}