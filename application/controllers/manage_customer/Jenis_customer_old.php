<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_customer extends MY_Controller {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'tb_jenis_customer';
	private $p_key = 'kd_jenis_customer';
	private $class_link = 'manage_customer/jenis_customer';
	private $title = 'Data Jenis Customer';

	function __construct() {
		parent::__construct();
		
		$this->load->model(array('m_builder', 'm_icon_currency', 'tb_default_disc'));
		$this->load->helper(array('form', 'html', 'my_helper'));
	}

	function index() {
		parent::administrator();

		$data['t_box_class'] = 'box-primary';
		$title = $this->uri->segment_array();
		$title = end($title);
		if (strpos($title, '_') !== FALSE ) :
			$title = str_replace('_', ' ', $title);
		endif;
		$title = ucwords($title);
		$data['table_title'] = $title;
		$data['table_alert'] = 'idAlertTable';
		$data['table_loader'] = 'idLoaderTable';
		$data['table_name'] = 'idTable';
		$data['table_overlay'] = 'idTableOverlay';
		$script['class_name'] = $this->class_link;

		$this->load->section('scriptJS', 'script/'.$this->class_link.'/scriptJS', $script);
		// $this->data_admin_form();
		$this->load->view('page/admin/v_table', $data);
	}

	function data_lihat() {
		if ($this->input->is_ajax_request()) :
			/* tabel property */
			$t_data['btn'] = TRUE;
			$t_data['t_header'] = array(
				'<th style="width:15%;">Nama Jenis Customer</th>',
				'<th style="width:10%;">Kategori Harga</th>',
				'<th style="width:10%;">Lama Term Payment</th>',
				'<th style="width:10%;">Tipe Customer</th>',
			);
			$t_data['t_uri'] = base_url().$this->class_link.'/data_table';
			$t_data['t_order'] = '[2, "asc"]';

			$this->load->view('page/admin/table_ssp', $t_data);
		endif;
	}

	function data_table() {
		if (!$this->input->is_ajax_request()) :
            exit('No direct script access allowed');
        else :

            $this->load->library('ssp');

			$table = $this->tbl_name;

			$primaryKey = $this->p_key;

			$columns = array(
			    array( 'db' => 'a.'.$this->p_key,
						'dt' => 1, 'field' => $this->p_key,
						'formatter' => function($d){
							if ($this->session->update_access == '1') :
								$btn_edit = '<a id="edit" title="Edit Data" href="javascript:void(0);" onclick="editData(\''.$d.'\')"><i class="fa fa-pencil"></i> Edit Data</a>';
								$divider = '<li class="divider"></li>';
							else :
								$btn_edit = '';
								$divider = '';
							endif;
							if ($this->session->delete_access == '1') :
								$btn_delete = '<a id="delete" title="Hapus Data" href="javascript:void(0);" onclick="return confirm(\'Apakah anda yakin?\')?hapusData(\''.$d.'\'):false;"><i class="fa fa-trash"></i> Hapus Data</a>';
							else :
								$btn_delete = '';
							endif;

							$btn = '<div align="center"><div class="btn-group"><button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">Opsi <span class="caret"></span></button><ul class="dropdown-menu"><li>'.$btn_edit.'</li>'.$divider.'<li>'.$btn_delete.'</li></ul></div></div>';
							
							return $btn;
						} ),
			    array( 'db' => 'a.nm_jenis_customer',
			    		'dt' => 2, 'field' => 'nm_jenis_customer',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'b.nm_select AS kat_harga',
			    		'dt' => 3, 'field' => 'kat_harga',
			    		'formatter' => function($d){
			    			$d = replace_empty($this->security->xss_clean($d), '-');

			    			return $d;
			    		} ),
			    array( 'db' => 'a.term_payment_waktu',
			    		'dt' => 4, 'field' => 'term_payment_waktu',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);
			    			$data = !empty($d)?$d:'0';

			    			return $data.' Hari';
			    		} ),
			    array( 'db' => 'c.nm_select AS tipe_customer',
			    		'dt' => 5, 'field' => 'tipe_customer',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);
			    			$data = !empty($d)?$d:'-';

			    			return $data;
			    		} ),
			);

			$sql_details = array(
			    'user' => 'root',
			    'pass' => '',
			    'db'   => $this->db_name,
			    'host' => 'localhost'
			);

			$joinQuery = "
				FROM
					".$this->tbl_name." a
				LEFT JOIN tb_set_dropdown b ON b.id = a.kd_price_category
				LEFT JOIN tb_set_dropdown c ON c.id = a.kd_manage_items
			";
			$where = "";

			echo json_encode(
			    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $where )
			);
		endif;
	}

	function data_form(){
		if ($this->input->is_ajax_request()) :
			// Form data
			$id = $this->input->get('id');
			if (!empty($id)) :
				$det = $this->m_builder->getRow($this->tbl_name, $data = array($this->p_key => $id), 'row');
				$id_form = 'idFormEdit';
				$data['f_box_class'] = 'box-warning';
				$data['form_title'] = 'Edit '.$this->title;
				$btn_form = '<i class="fa fa-edit"></i> Edit Data';
				$kd_jenis_customer = $this->security->xss_clean($det->kd_jenis_customer);
				$nm_jenis_customer = $this->security->xss_clean($det->nm_jenis_customer);
				$kd_price_category = $this->security->xss_clean($det->kd_price_category);
				$kd_manage_items = $this->security->xss_clean($det->kd_manage_items);
				$set_ppn = $this->security->xss_clean($det->set_ppn);
				$custom_barcode = $this->security->xss_clean($det->custom_barcode);
				$term_payment_format = $this->security->xss_clean($det->term_payment_format);
				$term_payment_waktu = $this->security->xss_clean($det->term_payment_waktu);
			else :
				$id_form = 'idFormInput';
				$data['f_box_class'] = 'box-info';
				$data['form_title'] = 'Input '.$this->title;
				$btn_form = '<i class="fa fa-save"></i> Input Data';
				$kd_jenis_customer = '';
				$nm_jenis_customer = '';
				$kd_price_category = '';
				$kd_manage_items = '';
				$set_ppn = '';
				$custom_barcode = '';
				$term_payment_format = '';
				$term_payment_waktu = '';
			endif;
			
			// select option untuk price category
			$this->db->from('tb_set_dropdown')->where(array('jenis_select' => 'price_category'));
			$query = $this->db->get();
			$result = $query->result();
			$pil_price = array('' => '-- Pilih Price Category --');
			foreach ($result as $row) :
				$id = $this->security->xss_clean($row->id);
				$nm_select = $this->security->xss_clean($row->nm_select);
				$pil_price[$id] = $this->security->xss_clean($nm_select);
			endforeach;
			$sel_price = $kd_price_category;
			$attr_price = array('id' => 'idSelPrice', 'class' => 'form-control');

			// select option untuk manage item customer
			$this->db->from('tb_set_dropdown')->where(array('jenis_select' => 'manage_items'))->where_not_in('nm_select', array('Semua', 'Tidak Ada'));
			$query = $this->db->get();
			$result = $query->result();
			$pil_item = array('' => '-- Pilih Manage Item --');
			foreach ($result as $row) :
				$id = $this->security->xss_clean($row->id);
				$nm_select = $this->security->xss_clean($row->nm_select);
				$pil_item[$id] = $this->security->xss_clean($nm_select);
			endforeach;
			$sel_item = $kd_manage_items;
			$attr_item = array('id' => 'idSelItem', 'class' => 'form-control');

			// Set value untuk default diskon
			$no = 0;
			$kd_default_disc = array(0 => '', 1 => '');
			$nm_pihak = array(0 => 'Customer', 1 => 'Distributor');
			$status = array(0 => '', 1 => '');
			$jml_disc = array(0 => '', 1 => '');
			$r_default_disc = $this->tb_default_disc->get_all($kd_jenis_customer);
			foreach ($r_default_disc as $row_def_disc) :
				$kd_default_disc[$no] = $row_def_disc->kd_default_disc;
				$nm_pihak[$no] = $row_def_disc->nm_pihak;
				$status[$no] = $row_def_disc->status;
				$jml_disc[$no] = $row_def_disc->jml_disc;
				$no++;
			endforeach;

			$data['f_attr'] = 'id="idBoxForm"';
			$data['form_url'] = '';
			$data['form_att'] = array('id' => $id_form, 'class' => 'form-horizontal');
			$data['form_hidden'] = '';
			$data['form_close_att'] = '';
			$data['form_field'] = array(
				'nm_tipe_admin' => array(
					'<div class="form-group">',
					'<label for="idTxtNm" class="col-md-2 control-label">Nama Jenis Customer</label>',
					'<div class="col-md-4">',
					'<div id="idErrNm"></div>',
					form_input(array('type' => 'hidden', 'name' => 'txtKd', 'id' => 'idTxtKd', 'value' => $kd_jenis_customer)),
					form_input(array('name' => 'txtNm', 'id' => 'idTxtNm', 'class' => 'form-control', 'placeholder' => 'Nama Jenis Customer', 'value' => $nm_jenis_customer)),
					'</div>',
					'</div>',
				),
				'item_group' => array(
					'<div class="form-group">',
					'<label for="idSelPrice" class="col-md-2 control-label">Price Category</label>',
					'<div class="col-md-3">',
					'<div id="idErrPrice"></div>',
					form_dropdown('selPrice', $pil_price, $sel_price, $attr_price),
					'</div>',
					'</div>',
				),
				'manage_item' => array(
					'<div class="form-group">',
					'<label for="idSelItem" class="col-md-2 control-label">Tipe Customer</label>',
					'<div class="col-md-2">',
					'<div id="idErrItem"></div>',
					form_dropdown('selItem', $pil_item, $sel_item, $attr_item),
					'</div>',
					'</div>',
					'<hr>',
				),
				'set_ppn' => array(
					'<div class="form-group">',
					'<label for="idTxtSetPpn" class="col-md-2 control-label">Set Default PPN (%)</label>',
					'<div class="col-md-2">',
					'<div id="idErrSetPpn"></div>',
					form_input(array('name' => 'txtSetPpn', 'id' => 'idTxtSetPpn', 'class' => 'form-control', 'placeholder' => 'Set Default PPN (%)', 'value' => $set_ppn)),
					'</div>',
					'<label for="idTxtBarcode" class="col-md-2 control-label">Custom Barcode</label>',
					'<div class="col-md-2">',
					'<div id="idErrBarcode"></div>',
					form_input(array('name' => 'txtBarcode', 'id' => 'idTxtBarcode', 'class' => 'form-control', 'placeholder' => 'Custom Barcode', 'value' => $custom_barcode)),
					'</div>',
					'</div>',
				),
				'term_payment_format' => array(
					'<div class="form-group">',
					'<label for="idTxtTermFormat" class="col-md-2 control-label">Format Term Payment</label>',
					'<div class="col-md-8">',
					'<div id="idErrTermFormat"></div>',
					form_textarea(array('name' => 'txtTermFormat', 'id' => 'idTxtTermFormat', 'class' => 'form-control', 'placeholder' => 'Term Payment Format', 'value' => $term_payment_format, 'rows' => '3')),
					'</div>',
					'</div>',
				),
				'term_payment_waktu' => array(
					'<div class="form-group">',
					'<label for="idTxtTermWaktu" class="col-md-2 control-label">Masa Tenggang Term Payment (Hari)</label>',
					'<div class="col-md-1">',
					'<div id="idErrTermWaktu"></div>',
					form_input(array('name' => 'txtTermWaktu', 'id' => 'idTxtTermWaktu', 'class' => 'form-control', 'placeholder' => 'Term Payment Waktu', 'value' => $term_payment_waktu)),
					'</div>',
					'</div>',
					'<hr>',
				),
				'default_diskon_kedua' => array(
					'<div class="box-header" style="margin-top:-15px;"><h3 class="box-title">Setting Default Discount</h3></div>',
					'<div class="form-group">',
					'<label for="idTxtNamaPihakDua" class="col-md-2 control-label">Nama Pihak Ke-2</label>',
					'<div class="col-md-3">',
					'<div id="idErrNamaPihakDua"></div>',
					form_input(array('type' => 'hidden', 'name' => 'txtKdPihakKedua', 'id' => 'idTxtKdPihakKedua', 'value' => $kd_default_disc[0])),
					form_input(array('name' => 'txtNamaPihakDua', 'id' => 'idTxtNamaPihakDua', 'class' => 'form-control', 'placeholder' => 'Nama Pihak Ke-2', 'value' => $nm_pihak[0], 'readonly' => 'readonly')),
					'</div>',
					'<label for="idSelStatusDua" class="col-md-2 control-label">Status Diskon</label>',
					'<div class="col-md-3">',
					'<div id="idErrStatusDua"></div>',
					form_dropdown('selStatusDua', array('' => '-- Pilih Status --', '0' => 'Tidak Aktif', '1' => 'Aktif'), $status[0], array('id' => 'idSelStatusDua', 'class' => 'form-control')),
					'</div>',
					'</div>',
				),
				'jml_diskon_kedua' => array(
					'<div class="form-group">',
					'<label for="idTxtJmlDiskonKedua" class="col-md-2 control-label">Jumlah Diskon (%)</label>',
					'<div class="col-md-1">',
					'<div id="idErrDiskonKedua"></div>',
					form_input(array('name' => 'txtJmlDiskonKedua', 'id' => 'idTxtJmlDiskonKedua', 'class' => 'form-control', 'placeholder' => 'Jumlah', 'value' => $jml_disc[0])),
					'</div>',
					'</div>',
				),
				'default_diskon_ketiga' => array(
					'<div class="form-group">',
					'<label for="idTxtNamaPihakTiga" class="col-md-2 control-label">Nama Pihak Ke-3</label>',
					'<div class="col-md-3">',
					'<div id="idErrNamaPihakTiga"></div>',
					form_input(array('type' => 'hidden', 'name' => 'txtKdPihakKetiga', 'id' => 'idTxtKdPihakKetiga', 'value' => $kd_default_disc[1])),
					form_input(array('name' => 'txtNamaPihakTiga', 'id' => 'idTxtNamaPihakTiga', 'class' => 'form-control', 'placeholder' => 'Nama Pihak Ke-3', 'value' => $nm_pihak[1], 'readonly' => 'readonly')),
					'</div>',
					'<label for="idSelStatusTiga" class="col-md-2 control-label">Status Diskon</label>',
					'<div class="col-md-3">',
					'<div id="idErrStatusTiga"></div>',
					form_dropdown('selStatusTiga', array('' => '-- Pilih Status --', '0' => 'Tidak Aktif', '1' => 'Aktif'), $status[1], array('id' => 'idSelStatusTiga', 'class' => 'form-control')),
					'</div>',
					'</div>',
				),
				'jml_diskon_ketiga' => array(
					'<div class="form-group">',
					'<label for="idTxtJmlDiskonKetiga" class="col-md-2 control-label">Jumlah Diskon (%)</label>',
					'<div class="col-md-1">',
					'<div id="idErrDiskonKetiga"></div>',
					form_input(array('name' => 'txtJmlDiskonKetiga', 'id' => 'idTxtJmlDiskonKetiga', 'class' => 'form-control', 'placeholder' => 'Jumlah', 'value' => $jml_disc[1])),
					'</div>',
					'</div>',
				),
			);

			$data['form_btn'] = array(
				'btn_submit' => array(
					'<button type="submit" name="btnSubmit" class="btn btn-primary pull-right">',
					$btn_form,
					'</button>',
				),
			);
			$data['form_box'] = 'idBoxFormBody';
			$data['form_alert'] = 'idAlertForm';
			$data['form_overlay'] = 'idOverlayForm';
			$script['class_name'] = $this->class_link;

			$this->load->section('form_js', 'script/'.$this->class_link.'/form_js', $script);
			$this->load->view('page/admin/v_form', $data);
		endif;
	}

	function submit_form($act){
		if ($this->input->is_ajax_request()) :
			$this->load->library(array('form_validation'));
			$str['csrf'] = $this->security->get_csrf_hash();
			$kd_jenis_customer = $this->input->post('txtKd');
			$nm_jenis_customer = $this->input->post('txtNm');
			$kd_price_category = $this->input->post('selPrice');
			$kd_manage_items = $this->input->post('selItem');
			$set_ppn = $this->input->post('txtSetPpn');
			$custom_barcode = $this->input->post('txtBarcode');
			$term_payment_format = $this->input->post('txtTermFormat');
			$term_payment_waktu = $this->input->post('txtTermWaktu');
			$kd_pihak_kedua = $this->input->post('txtKdPihakKedua');
			$kd_pihak_ketiga = $this->input->post('txtKdPihakKetiga');

			$this->form_validation->set_rules('txtNm', 'Nama Jenis Customer', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selPrice', 'Price Category', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selItem', 'Tipe Customer', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtTermFormat', 'Format Term Payment', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtSetPpn', 'Set PPN', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtBarcode', 'Custom Barcode', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtTermWaktu', 'Masa Tenggang Term Payment', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'errValidation';
				$str['idErrNm'] = (!empty(form_error('txtNm')))?buildLabel('warning', form_error('txtNm', '"', '"')):'';
				$str['idErrPrice'] = (!empty(form_error('selPrice')))?buildLabel('warning', form_error('selPrice', '"', '"')):'';
				$str['idErrItem'] = (!empty(form_error('selItem')))?buildLabel('warning', form_error('selItem', '"', '"')):'';
				$str['idErrSetPpn'] = (!empty(form_error('txtSetPpn')))?buildLabel('warning', form_error('txtSetPpn', '"', '"')):'';
				$str['idErrBarcode'] = (!empty(form_error('txtBarcode')))?buildLabel('warning', form_error('txtBarcode', '"', '"')):'';
				$str['idErrTermFormat'] = (!empty(form_error('txtTermFormat')))?buildLabel('warning', form_error('txtTermFormat', '"', '"')):'';
				$str['idErrTermWaktu'] = (!empty(form_error('txtTermWaktu')))?buildLabel('warning', form_error('txtTermWaktu', '"', '"')):'';
			else :
				/*
				** Jika validasi berhasil dan tidak ada error
				** Data dari form akan diinputkan kedalam tabel
				** Jika query input return true
				** alert success akan ditampilakan, selain itu error (kesalahan sistem!)
				*/
				$key_disc_kedua['kd_default_disc'] = $kd_pihak_kedua;
				$key_disc_ketiga['kd_default_disc'] = $kd_pihak_ketiga;
				$data_disc_kedua = array(
					'jenis_customer_kd' => $kd_jenis_customer,
					'nm_pihak' => $this->input->post('txtNamaPihakDua'),
					'status' => $this->input->post('selStatusDua'),
					'jml_disc' => $this->input->post('txtJmlDiskonKedua'),
				);
				$data_disc_ketiga = array(
					'jenis_customer_kd' => $kd_jenis_customer,
					'nm_pihak' => $this->input->post('txtNamaPihakTiga'),
					'status' => $this->input->post('selStatusTiga'),
					'jml_disc' => $this->input->post('txtJmlDiskonKetiga'),
				);
				$this->db->trans_begin();
				$this->tb_default_disc->submit_data($data_disc_kedua, $key_disc_kedua);
				$this->tb_default_disc->submit_data($data_disc_ketiga, $key_disc_ketiga);
				if ($act == 'input') :
					$conds = array(
						'select' => 'tgl_input, '.$this->p_key,
						'order_by' => 'tgl_input DESC',
					);
					$kd_jenis_customer = $this->m_builder->buat_kode($this->tbl_name, $conds, 3, '');

					$data = array(
						$this->p_key => $kd_jenis_customer,
						'nm_jenis_customer' => $nm_jenis_customer,
						'kd_price_category' => $kd_price_category,
						'kd_manage_items' => $kd_manage_items,
						'set_ppn' => $set_ppn,
						'custom_barcode' => $custom_barcode,
						'term_payment_format' => $term_payment_format,
						'term_payment_waktu' => $term_payment_waktu,
						'tgl_input' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					$aksi['jenis_customer'] = $this->db->insert($this->tbl_name, $data);
					$label_err = 'menambahkan';
				elseif ($act == 'edit') :

					$data = array(
						'nm_jenis_customer' => $nm_jenis_customer,
						'kd_price_category' => $kd_price_category,
						'kd_manage_items' => $kd_manage_items,
						'set_ppn' => $set_ppn,
						'custom_barcode' => $custom_barcode,
						'term_payment_format' => $term_payment_format,
						'term_payment_waktu' => $term_payment_waktu,
						'tgl_edit' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					$where = array($this->p_key => $kd_jenis_customer);
					$aksi['jenis_customer'] = $this->db->update($this->tbl_name, $data, $where);
					$label_err = 'mengubah';
				endif;

				if ($this->db->trans_status() === FALSE) :
					$this->db->trans_rollback();
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' '.$this->title.' dengan '.$this->p_key.' = \''.$kd_jenis_customer.'\', nm_jenis_customer = \''.$nm_jenis_customer.'\', kd_price_category = \''.$kd_price_category.'\', set_ppn = \''.$set_ppn.'\', custom_barcode = \''.$custom_barcode.'\', term_payment_format = \''.$term_payment_format.'\', term_payment_waktu = \''.$term_payment_waktu.'\'');
					$str['confirm'] = 'errValidation';
					$str['idErrForm'] = buildAlert('danger', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
				else :
					$this->db->trans_commit();
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' dengan '.$this->p_key.' = \''.$kd_jenis_customer.'\', nm_jenis_customer = \''.$nm_jenis_customer.'\', kd_price_category = \''.$kd_price_category.'\', set_ppn = \''.$set_ppn.'\', custom_barcode = \''.$custom_barcode.'\', term_payment_format = \''.$term_payment_format.'\', term_payment_waktu = \''.$term_payment_waktu.'\'');
					$str['confirm'] = 'success';
					$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
				endif;
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function hapus_data() {
		if ($this->input->is_ajax_request()) :
			$kd_jenis_customer = $this->input->get('id');
			$label_err = 'menghapus';
			$aksi = $this->db->delete($this->tbl_name, array($this->p_key => $kd_jenis_customer));

			if ($aksi) :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' dengan '.$this->p_key.' \''.$kd_jenis_customer.'\'!');
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
			else :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' '.$this->title.' dengan '.$this->p_key.' \''.$kd_jenis_customer.'\'');
				$str['alert'] = buildAlert('danger', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}