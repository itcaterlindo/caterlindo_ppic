<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auto_complete extends CI_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->helper(array('my_helper'));
		$this->load->model(array('m_customer'));
		$this->load->library(array('session'));
	}

	public function fill_text() {
		if ($this->input->is_ajax_request()) :
			$db = $this->input->get('db', TRUE);
			$table = $this->input->get('table', TRUE);
			$fields = $this->input->get('fields', TRUE);
			$search = $this->input->get('search', TRUE);
			$columns = $this->input->get('columns', TRUE);

			if ($db == 'sim_hrm') :
				$db_autocomplete = $this->load->database('sim_hrm', TRUE);
			elseif ($db == 'sim_ppic') :
				$db_autocomplete = $this->load->database('default', TRUE);
			endif;

			$db_autocomplete->select($fields);
			if (!empty($search)) :
				foreach ($search as $key => $value) :
					foreach ($value as $string => $param) :
						$db_autocomplete->{$key}($string, $param);
					endforeach;
				endforeach;
			endif;
			$db_autocomplete->from($table);
			$q_autocomplete = $db_autocomplete->get();
			$j_autocomplete = $q_autocomplete->num_rows();
			$r_autocomplete = $q_autocomplete->result();
			
			if ($j_autocomplete > 0) :
				$data = array();
				$name = '';
				$field = explode(', ', $fields);
				$j_field = count($field);
				foreach ($r_autocomplete as $d_autocomplete) :
					$no = 0;
					foreach ($field as $f_data) :
						$no++;
						$separator = ($no == $j_field)?'':'|';
						$name .= $d_autocomplete->$f_data.$separator;
					endforeach;
					array_push($data, $name);
					$name = '';
				endforeach;
				echo json_encode($data);
			endif;
		endif;
	}

	public function get_karyawan_aktif() {
		if ($this->input->is_ajax_request()) :
			$db_hrm = $this->load->database('sim_hrm', TRUE);
			$db_hrm->select('kd_karyawan, nm_karyawan, telp_rumah, telp_mobile, telp_kantor, email_utama, email_lain')
				->from('tb_karyawan')
				->like(array('nm_karyawan' => $this->input->get('nm_karyawan')))
				->group_start()
				->where(array('DATE(tgl_masuk) <=' => date('Y-m-d')))
				->or_where(array('tgl_masuk !=' => ''))
				->or_where('tgl_masuk IS NOT NULL', NULL, FALSE)
				->group_end()
				->group_start()
				->where(array('DATE(tgl_keluar) >=' => date('Y-m-d')))
				->or_where(array('tgl_keluar' => ''))
				->or_where('tgl_keluar IS NULL', NULL, FALSE)
				->group_end()
				->order_by('length(nm_karyawan) ASC');
			$query = $db_hrm->get();
			$result = $query->result();
			if (!empty($result)) :
				foreach ($result as $row) :
					$telp = '';
					if (!empty($row->telp_rumah)) :
						$telp = $row->telp_rumah;
					elseif (!empty($row->telp_mobile)) :
						$telp = $row->telp_mobile;
					elseif (!empty($row->telp_kantor)) :
						$telp = $row->telp_kantor;
					endif;
					$email = '';
					if (!empty($row->email_lain)) :
						$email = $row->email_lain;
					elseif (!empty($row->email_utama)) :
						$email = $row->email_utama;
					endif;
					$dataset[] = array('kd_karyawan' => $row->kd_karyawan, 'nm_karyawan' => $row->nm_karyawan, 'telp' => $telp, 'email' => $email, 'query' => $query);
				endforeach;
				echo json_encode($dataset);
			endif;
		endif;
	}

	public function fill_text_new() {
		if ($this->input->is_ajax_request()) :
			$db = $this->input->get('db', TRUE);
			$table = $this->input->get('table', TRUE);
			$fields = $this->input->get('fields', TRUE);
			$search = $this->input->get('search', TRUE);
			$columns = $this->input->get('columns', TRUE);

			if ($db == 'sim_hrm') :
				$db_autocomplete = $this->load->database('sim_hrm', TRUE);
			elseif ($db == 'sim_ppic') :
				$db_autocomplete = $this->load->database('default', TRUE);
			endif;

			$db_autocomplete->select($fields);
			if (!empty($search)) :
				foreach ($search as $key => $value) :
					foreach ($value as $string => $param) :
						$db_autocomplete->{$key}($string, $param);
					endforeach;
				endforeach;
			endif;
			$db_autocomplete->from($table);
			$q_autocomplete = $db_autocomplete->get();
			$j_autocomplete = $q_autocomplete->num_rows();
			$r_autocomplete = $q_autocomplete->result();
			
			if ($j_autocomplete > 0) :
				$no = 0;
				$field = explode(', ', $fields);
				foreach ($r_autocomplete as $d_autocomplete) :
					$no++;
					foreach ($field as $f_data) :
						$dataset[$no][$f_data] = $d_autocomplete->$f_data;
					endforeach;
				endforeach;
				echo json_encode($dataset);
			endif;
		endif;
	}

	public function fill_sales() {
		if ($this->input->is_ajax_request()) :
			$db_hrm = $this->load->database('sim_hrm', TRUE);
			$nm_salesperson = $this->input->get('name_startsWith');

			$this->db->from('tb_salesperson');
			$this->db->like(array('nm_salesperson' => $nm_salesperson));
			$this->db->order_by('length(nm_salesperson) ASC');
			$query = $this->db->get();
			$result = $query->result();
			foreach ($result as $res) :
				$dataset[] = array('Salesperson' => $res->nm_salesperson, 'Kode' => $res->kd_salesperson, 'Email' => $res->email_address, 'Telp' => $res->no_telp);
			endforeach;
			echo json_encode($dataset);
		endif;
	}

	public function fill_customer() {
		if ($this->input->is_ajax_request()) :
			$nm_customer = $this->input->get('name_startsWith');
			$kd_manage_items = $this->session->kd_manage_items;

			$this->db->select('a.kd_customer, a.nm_customer, a.code_customer, a.contact_person, a.alamat, b.nm_negara, c.nm_provinsi, d.nm_kota, e.nm_kecamatan');
			$this->db->from('tm_customer a');
			$this->db->join('tb_negara b', 'a.negara_kd = b.kd_negara', 'left');
			$this->db->join('tb_provinsi c', 'a.provinsi_kd = c.kd_provinsi AND a.negara_kd = c.negara_kd', 'left');
			$this->db->join('tb_kota d', 'a.kota_kd = d.kd_kota AND a.provinsi_kd = d.provinsi_kd AND a.negara_kd = d.negara_kd', 'left');
			$this->db->join('tb_kecamatan e', 'a.kecamatan_kd = e.kd_kecamatan AND a.kota_kd = e.kota_kd AND a.provinsi_kd = e.provinsi_kd AND a.negara_kd = e.negara_kd', 'left');
			$this->db->like(array('a.nm_customer' => $nm_customer));
			if ($kd_manage_items == '2' || $kd_manage_items == '3') :
				$this->db->join('tb_jenis_customer f', 'f.kd_jenis_customer = a.jenis_customer_kd', 'left');
				$this->db->where(array('f.kd_manage_items' => $kd_manage_items));
			endif;
			$query = $this->db->get();
			$num = $query->num_rows();
			$result = $query->result();
			$data = array();

			if ($num > 0) :
				foreach ($result as $row) :
					$nm_customer = $row->nm_customer;
					$kd_customer = $row->kd_customer;
					$code_customer = $row->code_customer;
					$contact_person = $row->contact_person;
					$nm_negara = $row->nm_negara.after_before_char($row->nm_negara, $row->nm_negara, '.', '.');
					$nm_provinsi = $row->nm_provinsi.after_before_char($row->nm_provinsi, $row->nm_negara, ', ', '.');
					$nm_kota = $row->nm_kota.after_before_char($row->nm_kota, array($row->nm_provinsi, $row->nm_negara), ', ', '.');
					$nm_kecamatan = $row->nm_kecamatan.after_before_char($row->nm_kecamatan, array($row->nm_kota, $row->nm_provinsi, $row->nm_negara), ', ', '.');
					$alamat = ucwords(strtolower($row->alamat)).after_before_char($row->alamat, array($row->nm_kecamatan, $row->nm_kota, $row->nm_provinsi, $row->nm_negara), ', ', ', ').$nm_kecamatan.$nm_kota.$nm_provinsi.$nm_negara;

					$dataset[] = array('Customer' => $nm_customer, 'Kode' => $kd_customer, 'Code' => $code_customer, 'Contact' => $contact_person, 'Alamat' => $alamat);
				endforeach;
				echo json_encode($dataset);
			endif;
		endif;
	}

	public function quotation_detail() {
		if ($this->input->is_ajax_request()) :
			$product_code = $this->input->get('product_code');
			$kat_harga = $this->input->get('kat_harga');
			$kd_parent = $this->input->get('kd_parent');
			$price_column = $this->price_category($kat_harga);

			$this->db->select('a.item_code, a.kd_barang, a.deskripsi_barang, a.dimensi_barang, a.item_barcode, a.netweight, a.grossweight, a.boxweight, a.length_cm, a.width_cm, a.height_cm, a.item_cbm, b.'.$price_column.', b.harga_lokal_retail');
			$this->db->from('tm_barang a');
			$this->db->join('td_barang_harga b', 'b.kd_harga_barang = a.harga_barang_kd', 'left');
			$this->db->like(array('a.item_code' => $product_code));
			if (!empty($kd_parent)) :
				$this->db->join('td_barang_relasi c', 'c.barang_kd = a.kd_barang', 'left');
				$this->db->join('tm_barang_relasi d', 'd.kd_mrelasi_barang = c.mrelasi_barang_kd', 'left');
				$this->db->where(array('d.barang_kd' => $kd_parent));
				$this->db->group_by('a.item_code');
			endif;
			$this->db->order_by('length(a.item_code) ASC, a.item_code ASC');
			$query = $this->db->get();
			$num = $query->num_rows();
			$result = $query->result();
			
			if ($num > 0) :
				$data = array();
				foreach ($result as $row) :
					$item_code = $row->item_code;
					$kd_barang = $row->kd_barang;
					$deskripsi_barang = $row->deskripsi_barang;
					$dimensi_barang = $row->dimensi_barang;
					$item_barcode = $row->item_barcode;
					$harga_barang = $row->{$price_column};
					$harga_retail = empty($row->harga_lokal_retail) || $row->harga_lokal_retail < 1?$harga_barang:$row->harga_lokal_retail;

					$dataset[] = array('item_code' => $item_code, 'kd_barang' => $kd_barang, 'deskripsi_barang' => $deskripsi_barang, 'dimensi_barang' => $dimensi_barang, 'item_barcode' => $item_barcode, 'harga_barang' => $harga_barang, 'harga_retail' => $harga_retail, 'netweight' => $row->netweight, 'grossweight' => $row->grossweight, 'boxweight' => $row->boxweight, 'length_cm' => $row->length_cm, 'width_cm' => $row->width_cm, 'height_cm' => $row->height_cm, 'item_cbm' => $row->item_cbm, 'kd_custom' => 'PRD020817000573');
				endforeach;
				echo json_encode($dataset);
			endif;
		endif;
	}

	public function jasakirim_detail() {
		if ($this->input->is_ajax_request()) :
			$nm_jasakirim = $this->input->get('nm_jasakirim');

			$this->db->select('a.nm_jasakirim, a.alamat, a.kode_pos, b.nm_negara, c.nm_provinsi, d.nm_kota, e.nm_kecamatan')
				->from('tb_jasakirim a')
				->join('tb_negara b', 'b.kd_negara = a.negara_kd', 'left')
				->join('tb_provinsi c', 'c.kd_provinsi = a.provinsi_kd AND b.kd_negara = c.negara_kd', 'left')
				->join('tb_kota d', 'd.kd_kota = a.kota_kd AND b.kd_negara = d.negara_kd AND c.kd_provinsi = d.provinsi_kd', 'left')
				->join('tb_kecamatan e', 'e.kd_kecamatan = a.kecamatan_kd AND b.kd_negara = e.negara_kd AND c.kd_provinsi = e.provinsi_kd AND d.kd_kota = e.kota_kd', 'left')
				->like(array('a.nm_jasakirim' => $nm_jasakirim))
				->order_by('length(a.nm_jasakirim) ASC');
			$query = $this->db->get();
			$result = $query->result();

			if (!empty($result)) :
				foreach ($result as $row) :
					$nm_jasakirim = $row->nm_jasakirim;
					$kode_pos = $row->kode_pos.after_before_char($row->kode_pos, $row->kode_pos, '.', '');
					$alamat = $row->alamat.after_before_char($row->alamat, $kode_pos, ', ', '.');
					$negara = $row->nm_negara.after_before_char($row->nm_negara, $row->nm_negara, '.', '.');
					$provinsi = $row->nm_provinsi.after_before_char($row->nm_provinsi, array($row->nm_provinsi, $negara), ', ', '.');
					$kota = $row->nm_kota.after_before_char($row->nm_kota, array($row->nm_kota, $provinsi, $negara), ', ', '.');
					$kecamatan = $row->nm_kecamatan.after_before_char($row->nm_kecamatan, array($row->nm_kecamatan, $kota, $provinsi, $negara), ', ', '.');
					$detail_alamat = $alamat.$kode_pos.'<br>'.$kecamatan.$kota.$provinsi.$negara;

					$dataset[] = array('nm_jasakirim' => $nm_jasakirim, 'detail_alamat' => $detail_alamat);
				endforeach;
				echo json_encode($dataset);
			else :
				echo 'Maaf kami sudah melakukan semaksimal mungkin, tapi datanya nggak ada :(';
			endif;
		endif;
	}

	public function price_category($cat) {
		$price = '';
		if ($cat == 'harga_retail') :
			$price = 'harga_lokal_retail';
		elseif ($cat == 'harga_distributor') :
			$price = 'harga_lokal_distributor';
		elseif ($cat == 'harga_reseller') :
			$price = 'harga_lokal_reseller';
		elseif ($cat == 'harga_ekspor') :
			$price = 'harga_ekspor';
		endif;
		return $price;
	}

	public function customer_quotation() {
		$nm_customer = $this->input->get('customer_name');
		$result = $this->m_customer->get_byname($nm_customer);
		$data = array();
		foreach ($result as $row) :
			$nm_customer = $row->nm_customer;
			$kd_customer = $row->kd_customer;
			$dataset[] = array('Customer' => $nm_customer, 'Kode' => $kd_customer);
		endforeach;
		echo json_encode($dataset);
	}

	public function get_item_detail() {
		$item_code = $this->input->get('item_code');

		$this->db->from('tm_barang')
			->like(['item_code' => $item_code])
			->order_by('length(item_code) ASC, item_code ASC');
		$query = $this->db->get();
		$result = $query->result();
		if (!empty($result)) :
			foreach ($result as $row) :
				$dataset[] = ['kd_barang' => $row->kd_barang, 'item_code' => $row->item_code.' - '.$row->deskripsi_barang];
			endforeach;
			echo json_encode($dataset);
		endif;
	}
	
	// cal
	public function get_barang () {
		$item_code = $this->input->get('item_code');

		$this->db->from('tm_barang')
			->like(['item_code' => $item_code])
			->order_by('length(item_code) ASC, item_code ASC');
		$query = $this->db->get();
		$result = $query->result();
		if (!empty($result)) :
			foreach ($result as $row) :
				$dataset[] = array (
					// 'item_code' => $row->item_code.' - '.$row->deskripsi_barang,
					'item_code' => $row->item_code,
					'kd_barang' => $row->kd_barang, 
					'item_barcode' => $row->item_barcode,
					'deskripsi_barang' => $row->deskripsi_barang,
					'dimensi_barang' => $row->dimensi_barang,
					);
			endforeach;
			echo json_encode($dataset);
		endif;
	}

	public function get_barang_rm_wip () {
		$item_code = $this->input->get('item_code');

		$queryBarang = $this->db->select('kd_barang, item_code, item_barcode, deskripsi_barang, dimensi_barang')
			->from('tm_barang')
			->like('item_code', $item_code)->get_compiled_select();
		$queryRM = $this->db->select('rm_kd, rm_kode, "NULL", rm_deskripsi, rm_spesifikasi')
			->from('tm_rawmaterial')
			->like('rm_kode', $item_code)
			->where('itemgroup_kd', '14')->get_compiled_select();
		$query = $this->db->query($queryBarang . ' UNION ' . $queryRM);
		$result = $query->result();
		
		if (!empty($result)) :
			foreach ($result as $row) :
				$dataset[] = array (
					'item_code' => $row->item_code,
					'kd_barang' => $row->kd_barang, 
					'item_barcode' => $row->item_barcode,
					'deskripsi_barang' => $row->deskripsi_barang,
					'dimensi_barang' => $row->dimensi_barang,
					);
			endforeach;
			echo json_encode($dataset);
		endif;
	}

	public function get_barang_byid() {
		$kd_barang = $this->input->get('kd_barang');
		$this->db->from('tm_barang')
			->where('kd_barang', $kd_barang);
		$query = $this->db->get();
		$result = $query->row_array();
		echo json_encode($result);
	}

	public function get_barang_select2() {
		$item_code = $this->input->get('paramItemCode');
		$this->db->from('tm_barang')
			->like(['item_code' => $item_code])
			->order_by('length(item_code) ASC, item_code ASC');
		$query = $this->db->get();
		if (!empty($query->num_rows())) {
			$result = $query->result_array();
			foreach ($result as $r) {
				$resultData[] = ['id' => $r['kd_barang'], 'text' => $r['item_code'] ];
			}
		}
		echo json_encode($resultData);
	}

	public function get_no_wo () {
		$woitem_no_wo = $this->input->get('woitem_no_wo');

		$this->db->from('td_workorder_item')
			->like(['woitem_no_wo' => $woitem_no_wo])
			->where('woitem_prosesstatus', 'workorder')
			->order_by('woitem_no_wo ASC');
		$query = $this->db->get();
		if (!empty($query->num_rows())) :
			$result = $query->result();
			foreach ($result as $row) :
				$dataset[] = $row;
			endforeach;
			echo json_encode($dataset);
		endif;
	}

	public function get_rm_satuan () {
		$paramSatuan = $this->input->get('paramSatuan');
		$resultData = [];
		$query = $this->db->from('td_rawmaterial_satuan')
			->select('rmsatuan_kd, rmsatuan_nama')
			->like('rmsatuan_nama', $paramSatuan, 'match')
			->order_by('rmsatuan_nama', 'ASC')
			->get();
		if (!empty($query->num_rows())) {
			$result = $query->result_array();
			foreach ($result as $r) {
				$resultData[] = ['id' => $r['rmsatuan_kd'], 'text' => $r['rmsatuan_nama'] ];
			}
		}
		echo json_encode($resultData);
	}

	public function get_raw_material () {
		$paramRM = $this->input->get('paramRM');
		$resultData = [];
		if (!empty($paramRM)) {
			$query = $this->db->from('tm_rawmaterial')
				->join('td_rawmaterial_satuan', 'tm_rawmaterial.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd','left')
				->join('td_rawmaterial_satuan as satuan_secondary', 'tm_rawmaterial.rmsatuansecondary_kd=satuan_secondary.rmsatuan_kd','left')
				->select('rm_kd, rm_kode, rm_nama, rm_deskripsi, rm_spesifikasi, tm_rawmaterial.rmsatuan_kd, td_rawmaterial_satuan.rmsatuan_nama,
				satuan_secondary.rmsatuan_kd as rmsatuan_secodary_kd, satuan_secondary.rmsatuan_nama as rmsatuan_secodary_nama, tm_rawmaterial.rm_konversiqty')
				->like('rm_kode', $paramRM, 'match')
				->or_like('rm_nama', $paramRM, 'match')
				->or_like('rm_deskripsi', $paramRM, 'match')
				->or_like('rm_spesifikasi', $paramRM, 'match')
				->order_by('rm_nama', 'ASC')
				->get();
			if (!empty($query->num_rows())) {
				$result = $query->result_array();
				foreach ($result as $r) {
					$resultData[] = [
						'id' => $r['rm_kd'], 
						'text' => $r['rm_kode'].' | '.$r['rm_nama'].'/'.$r['rm_deskripsi'].'/'.$r['rm_spesifikasi'],
						'rm_kode' => $r['rm_kode'],
						'rm_nama' => $r['rm_nama'],
						'rm_deskripsi' => $r['rm_deskripsi'],
						'rm_spesifikasi' => $r['rm_spesifikasi'],
						'rmsatuan_kd' => $r['rmsatuan_kd'],
						'rmsatuan_nama' => $r['rmsatuan_nama'],
						'rm_kd' => $r['rm_kd'],
					];
				}
			}
		}
		echo json_encode($resultData);
	}

	public function get_raw_material_stock () {
		$paramRM = $this->input->get('paramRM');
		$resultData = [];
		if (!empty($paramRM)) {
			$query = $this->db->from('tm_rawmaterial')
				->join('td_rawmaterial_satuan', 'tm_rawmaterial.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd','left')
				->join('td_rawmaterial_satuan as satuan_secondary', 'tm_rawmaterial.rmsatuansecondary_kd=satuan_secondary.rmsatuan_kd','left')
				->join('td_rawmaterial_stok_master as stock', 'tm_rawmaterial.rm_kd=stock.rm_kd','left')
				->select('tm_rawmaterial.rm_kd, tm_rawmaterial.rm_kode, tm_rawmaterial.rm_nama, tm_rawmaterial.rm_deskripsi, tm_rawmaterial.rm_spesifikasi, tm_rawmaterial.rmsatuan_kd, td_rawmaterial_satuan.rmsatuan_nama,
				satuan_secondary.rmsatuan_kd as rmsatuan_secodary_kd, satuan_secondary.rmsatuan_nama as rmsatuan_secodary_nama, tm_rawmaterial.rm_konversiqty, stock.rmstokmaster_qty')
				->like('rm_kode', $paramRM, 'match')
				->or_like('rm_nama', $paramRM, 'match')
				->or_like('rm_deskripsi', $paramRM, 'match')
				->or_like('rm_spesifikasi', $paramRM, 'match')
				->order_by('rm_nama', 'ASC')
				->get();
			if (!empty($query->num_rows())) {
				$result = $query->result_array();
				foreach ($result as $r) {
					$resultData[] = [
						'id' => $r['rm_kd'], 
						'text' => $r['rm_kode'].' | '.$r['rm_nama'].'/'.$r['rm_deskripsi'].'/'.$r['rm_spesifikasi'].'/'.$r['rmstokmaster_qty']." ".$r['rmsatuan_nama'],
						'rm_kode' => $r['rm_kode'],
						'rm_nama' => $r['rm_nama'],
						'rm_deskripsi' => $r['rm_deskripsi'],
						'rm_spesifikasi' => $r['rm_spesifikasi'],
						'rmsatuan_kd' => $r['rmsatuan_kd'],
						'rmsatuan_nama' => $r['rmsatuan_nama'],
						'rm_kd' => $r['rm_kd'],
						'stock_qty' => $r['rmstokmaster_qty'],
					];
				}
			}
		}
		echo json_encode($resultData);
	}

	public function get_salesorder () {
		$paramSO = $this->input->get('paramSO');
		$resultData = [];
		if (!empty($paramSO)) {
			$query = $this->db->select('tm_salesorder.kd_msalesorder, tm_salesorder.no_salesorder, tm_salesorder.no_po, tm_salesorder.tipe_customer, tm_customer.nm_customer')
				->from('tm_salesorder')
				->join('tm_customer', 'tm_salesorder.customer_kd=tm_customer.kd_customer', 'left')
				->like('tm_salesorder.no_salesorder', $paramSO, 'match')
				->or_like('tm_salesorder.no_po', $paramSO, 'match')
				->or_like('tm_customer.nm_customer', $paramSO, 'match')
				->order_by('tm_salesorder.tgl_so', 'DESC')
				->get();
			if (!empty($query->num_rows())) {
				$result = $query->result_array();
				foreach ($result as $r) {
					$no_salesorder = $r['no_salesorder'];
					if ($r['tipe_customer'] == 'Ekspor') {
						$no_salesorder = $r['no_po'];
					}
					$resultData[] = [
						'id' => $r['kd_msalesorder'], 
						'text' => $no_salesorder.' | '.$r['nm_customer'],
					];
				}
			}
		}
		echo json_encode($resultData);
	}

	public function get_suplier () {
		$paramSuplier = $this->input->get('paramSuplier');
		$resultData = [];
		$query = $this->db->from('tm_suplier')
			->select('tm_suplier.*, tb_set_dropdown.nm_select')
			->join('tb_set_dropdown', 'tm_suplier.badanusaha_kd=tb_set_dropdown.id', 'left')
			->like('tm_suplier.suplier_nama', $paramSuplier, 'match')
			->or_like('tm_suplier.suplier_kode', $paramSuplier, 'match')
			->order_by('tm_suplier.suplier_nama', 'ASC')
			->get();
		if (!empty($query->num_rows())) {
			$result = $query->result_array();
			foreach ($result as $r) {
				$resultData[] = [
					'id' => $r['suplier_kd'], 
					'text' => $r['suplier_kode'].' | '.$r['suplier_nama'].' '.$r['nm_select'],
					'suplier_termpayment' => $r['suplier_termpayment'],
					'suplier_includeppn' => $r['suplier_includeppn'],
				];
			}
		}
		echo json_encode($resultData);
	}
	public function get_payment () {
		$paramSuplier = $this->input->get('paramSuplier');
		$resultData = [];
		$query = $this->db->from('tm_m_payment')
			->select('tm_m_payment.*')
			->like('tm_m_payment.nm_bank', $paramSuplier, 'match')
			->order_by('tm_m_payment.nm_bank', 'ASC')
			->get();
		if (!empty($query->num_rows())) {
			$result = $query->result_array();
			foreach ($result as $r) {
				$resultData[] = [
					'id' => $r['m_payment_kd'], 
					'text' => $r['nm_bank'].' | '.$r['no_rek'] 
				];
			}
		}
		echo json_encode($resultData);
	}

	public function get_bank () {
		$paramBank = $this->input->get('paramBank');
		$resultData = [];
		$query = $this->db->from('tb_bank')
			->like('bank_nama', $paramBank, 'match')
			->order_by('bank_nama', 'ASC')
			->get();
		if (!empty($query->num_rows())) {
			$result = $query->result_array();
			foreach ($result as $r) {
				$resultData[] = [
					'id' => $r['bank_kd'], 
					'text' => $r['bank_nama'],
				];
			}
		}
		echo json_encode($resultData);
	}

	public function get_karyawan () {
		$this->db = $this->load->database('sim_hrm', TRUE);

		$paramKaryawan = $this->input->get('paramKaryawan');
		$resultData = [];
		$query = $this->db->from('tb_karyawan')
			->select('tb_karyawan.*, tb_bagian.nm_bagian')
			->join('tb_bagian', 'tb_bagian.kd_bagian=tb_karyawan.kd_bagian', 'left')
			->like('tb_karyawan.nm_karyawan', $paramKaryawan, 'match')
			->or_like('tb_karyawan.nik_karyawan', $paramKaryawan, 'match')
			->order_by('tb_karyawan.nm_karyawan', 'ASC')
			->get();
		if (!empty($query->num_rows())) {
			$result = $query->result_array();
			foreach ($result as $r) {
				$resultData[] = [
					'id' => $r['kd_karyawan'], 
					'text' => $r['nik_karyawan'].' | '.$r['nm_karyawan'].' / '.$r['nm_bagian'],
				];
			}
		}
		echo json_encode($resultData);
	}

	public function get_bagian () {
		$paramBank = $this->input->get('paramBagian');
		$resultData = [];
		$query = $this->db->from('tb_bagian')
			->like('bagian_nama', $paramBank, 'match')
			->order_by('bagian_nama', 'ASC')
			->get();
		if (!empty($query->num_rows())) {
			$result = $query->result_array();
			foreach ($result as $r) {
				$resultData[] = [
					'id' => $r['bagian_kd'], 
					'text' => $r['bagian_nama'],
				];
			}
		}
		echo json_encode($resultData);
	}

	public function get_gudang () {
		$paramBank = $this->input->get('paramGudang');
		$resultData = [];
		$query = $this->db->from('tm_gudang')
			->like('nm_gudang', $paramBank, 'match')
			->order_by('nm_gudang', 'ASC')
			->get();
		if (!empty($query->num_rows())) {
			$result = $query->result_array();
			foreach ($result as $r) {
				$resultData[] = [
					'id' => $r['kd_gudang'], 
					'text' => $r['nm_gudang'],
				];
			}
		}
		echo json_encode($resultData);
	}

	public function get_rm_satuan_konversi () {
		/** Dari rmsatuankonversi_form / _to
		 * jika _form maka ikuti qty konversinya
		 * jika _to maka qty = 1/konversinyas
		 */
		$rmsatuan_kd_from = $this->input->get('rmsatuan_kd_from'); 
		$rmsatuan_kd_to = $this->input->get('rmsatuan_kd_to'); 
		$paramSatuan = $this->input->get('paramSatuan');
		$resultData = [];
		$query = $this->db->from('td_rawmaterial_satuan_konversi')
			->select('td_rawmaterial_satuan_konversi.*, fr_satuan.rmsatuan_nama as fr_satuan, to_satuan.rmsatuan_nama as to_satuan')
			->join('td_rawmaterial_satuan as fr_satuan', 'fr_satuan.rmsatuan_kd=td_rawmaterial_satuan_konversi.rmsatuankonversi_from', 'left')
			->join('td_rawmaterial_satuan as to_satuan', 'to_satuan.rmsatuan_kd=td_rawmaterial_satuan_konversi.rmsatuankonversi_to', 'left');
		if (!empty($rmsatuan_kd_from)){
			$query = $query->where('td_rawmaterial_satuan_konversi.rmsatuankonversi_from', $rmsatuan_kd_from);
		}
		if (!empty($rmsatuan_kd_to)){
			$query = $query->where('td_rawmaterial_satuan_konversi.rmsatuankonversi_to', $rmsatuan_kd_to);
		}
		if (!empty($paramSatuan)){
			$query = $query->like('to_satuan.rmsatuan_nama', $paramSatuan, 'match');
		}
			$query = $query->order_by('to_satuan.rmsatuan_nama', 'ASC')
			->get();
		if (!empty($query->num_rows())) {
			$result = $query->result_array();
			foreach ($result as $r) {
				$resultData[] = [
					'id' => $r['rmsatuankonversi_kd'], 
					'text' => "{$r['fr_satuan']} to {$r['to_satuan']} = {$r['rmsatuankonversi_konversi']}",
					'rmsatuankonversi_from' => $r['rmsatuankonversi_from'],
					'rmsatuankonversi_to' => $r['rmsatuankonversi_to'],
					'rmsatuankonversi_konversi' => $r['rmsatuankonversi_konversi'],
				];
			}
		}
		echo json_encode($resultData);
	}

	public function get_bom_std()
	{
		$paramBOM = $this->input->get('paramBOM');
		$result = $this->db->join('tm_barang', 'tm_barang.kd_barang=tm_bom.kd_barang', 'left');
		if (!empty($paramBOM)){
			$result = $result->like('tm_barang.item_code', $paramBOM, 'match');
		}
		$result = $result->where('tm_bom.bom_jenis', 'std')
			->get('tm_bom')->result_array();
		$arrayResult = array();
		foreach ($result as $r){
			$arrayResult[] = [
				'id' => $r['bom_kd'],
				'text' => $r['item_code']
			];
		}
		echo json_encode($arrayResult);
	}

	/** Get data code srj from goodsreceive */
	public function get_kdsrj()
	{
		$paramTerm = $this->input->get('paramTerm');
		$resultData = [];
		$query = $this->db->from('td_rawmaterial_goodsreceive')
			->like('rmgr_code_srj', $paramTerm, 'match')
			->group_by('rmgr_code_srj')
			->order_by('rmgr_code_srj', 'ASC')
			->get();
		if (!empty($query->num_rows())) {
			$result = $query->result_array();
			foreach ($result as $r) {
				$resultData[] = [
					'id' => $r['rmgr_code_srj'], 
					'text' => $r['rmgr_code_srj'],
				];
			}
		}
		echo json_encode($resultData);
	}

	/** Get data from goods receive by rmgr_code_srj rawmaterial goodsreceive */
	public function get_batch_from_goodsreceive()
	{
		$paramSRJ = $this->input->get('paramSRJ');
		$paramTerm = $this->input->get('paramTerm');
		$resultData = [];
		$query = $this->db->from('td_rawmaterial_goodsreceive')
			->where('rmgr_code_srj', $paramSRJ)
			->like('batch', $paramTerm, 'match')
			->order_by('batch', 'ASC')
			->get();
		if (!empty($query->num_rows())) {
			$result = $query->result_array();
			foreach ($result as $r) {
				$resultData[] = [
					'id' => $r['batch'], 
					'text' => $r['batch'],
				];
			}
		}
		echo json_encode($resultData);
	}

	/** Get data from goods receive by rawmaterial kode */
	public function get_kdsrj_from_goodsreceive_by_rm()
	{
		$paramRMKD = $this->input->get('paramRMKD');
		$paramTerm = $this->input->get('paramTerm');
		$resultData = [];
		$query = $this->db->from('td_rawmaterial_goodsreceive')
			->where('rm_kd', $paramRMKD)
			->like('rmgr_code_srj', $paramTerm, 'match')
			->group_by('rmgr_code_srj')
			->order_by('rmgr_code_srj', 'ASC')
			->get();
		if (!empty($query->num_rows())) {
			$result = $query->result_array();
			foreach ($result as $r) {
				$resultData[] = [
					'id' => $r['rmgr_code_srj'], 
					'text' => $r['rmgr_code_srj'],
					'rmgr_code_srj' => $r['rmgr_code_srj']
				];
			}
		}
		echo json_encode($resultData);
	}

	public function get_delivery_note_packing () {
		$paramDN = $this->input->get('paramDN');
		$resultData = [];
		$query = $this->db->from('tm_deliverynote')
			->where('dn_tujuan', '70')
			->like('dn_no', $paramDN, 'match')
			->order_by('dn_kd', 'DESC')
			->get();
		if (!empty($query->num_rows())) {
			$result = $query->result_array();
			foreach ($result as $r) {
				$resultData[] = [
					'id' => $r['dn_kd'], 
					'text' => $r['dn_no'],
				];
			}
		}
		echo json_encode($resultData);
	}

}