<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Cli_request extends CI_Controller {
	function __construct() {
		parent::__construct();
	}

	public function index() {
		echo 'Dude!';
	}

	public function cli_test($id = '') {
		$test = !empty($id)?$id:'Lho Namanya Kosong!';
		echo 'Hello CLI! My Name is : '.$test;
	}

	public function cli_again() {
		echo 'This must be working!';
	}
}