<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Profile extends MY_Controller {
	private $class_link = 'profile';
	private $form_errs = array('idErrNm', 'idErrUsername');

	public function __construct() {
		parent::__construct();

		$this->load->helper(array('my_helper'));
		$this->load->library(array('form_validation'));
		$this->load->model(array('tb_admin'));
	}

	public function index() {
		parent::administrator();
		$this->get_form();
	}

	public function get_form() {
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = $this->form_errs;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function open_form() {
		$data['row'] = $this->tb_admin->get_row($_SESSION['kd_admin']);
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function send_data() {
		if ($this->input->is_ajax_request()) :
			$this->form_validation->set_rules($this->tb_admin->form_rules());
			if ($this->form_validation->run() == FALSE) :
				$str = $this->tb_admin->form_warning($this->form_errs);
				$str['alert'] = buildAlert('danger', 'Peringatan!', 'Ada kesalahan pada proses input!');
				$str['confirm'] = 'error';
			else :
				$data['kd_admin'] = $_SESSION['kd_admin'];
				$data['username'] = $this->input->post('txtUsername');
				$data['nm_admin'] = $this->input->post('txtNm');
				$data['password'] = hashText($this->input->post('txtPassword'));
				$str = $this->tb_admin->update_profile($data);
			endif;
			$str['username'] = $_SESSION['username'];
			$str['nm_admin'] = $_SESSION['nm_admin'];
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}