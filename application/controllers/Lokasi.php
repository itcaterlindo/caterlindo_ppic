<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Lokasi extends MY_Controller {
	private $class_link = 'lokasi';
	private $form_errs = array('idErrIdk');

	public function __construct() {
		parent::__construct();

		$this->load->helper(array('my_helper'));
		$this->load->model(array('m_builder', 'lokasi/tb_negara', 'lokasi/tb_provinsi', 'lokasi/tb_kota', 'lokasi/tb_kecamatan'));
	}

	public function dropdown_provinsi() {
		$kd_negara = $this->input->get('kd_negara');
		/* --Query untuk pemanggilan data provinsi berdasarkan Negara yang sudah dipilih-- */
		$dropdown = $this->tb_provinsi->dropdown_on_negara($kd_negara);
		echo $dropdown;
	}

	public function dropdown_kota() {
		$kd_negara = $this->input->get('kd_negara');
		$kd_provinsi = $this->input->get('kd_provinsi');
		/* --Query untuk pemanggilan data provinsi berdasarkan Negara yang sudah dipilih-- */
		$dropdown = $this->tb_kota->dropdown_on_provinsi($kd_negara, $kd_provinsi);
		echo $dropdown;
	}

	public function dropdown_kecamatan() {
		$kd_negara = $this->input->get('kd_negara');
		$kd_provinsi = $this->input->get('kd_provinsi');
		$kd_kota = $this->input->get('kd_kota');
		/* --Query untuk pemanggilan data provinsi berdasarkan Negara yang sudah dipilih-- */
		$dropdown = $this->tb_kecamatan->dropdown_on_kota($kd_negara, $kd_provinsi, $kd_kota);
		echo $dropdown;
	}
}