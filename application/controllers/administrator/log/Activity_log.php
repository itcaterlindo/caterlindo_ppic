<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity_log extends MY_Controller {
	private $my_db = 'db_caterlindo_ppic';
	private $my_tbl = 'tb_log_activity';
	private $p_key = 'kd_log_activity';

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'html', 'my_helper'));
	}

	function index() {
		parent::administrator();

		$data['t_box_class'] = 'box-success';
		$data['t_attr'] = 'id="idBoxTableImg"';
		$data['btn_add'] = FALSE;
		$data['btn_close'] = TRUE;
		$data['id_btn_close'] = 'idBtnCloseImg';
		$data['id_btn_tambah'] = 'idBtnTambahImg';

		$data['table_title'] = 'Logs Activity';
		$data['table_alert'] = 'idAlertTable';
		$data['table_loader'] = 'idLoaderTable';
		$data['table_name'] = 'idTableLog';
		$data['table_overlay'] = 'idTableOverlay';

		$this->load->section('scriptJS', 'script/administrator/log/activity_log/scriptJS');
		$this->load->view('page/admin/v_table', $data);
	}

	function data_lihat() {
		if ($this->input->is_ajax_request()) :
			/* tabel property */
			$lebar = $this->input->get('lebar');
			$id = $this->session->kd_view_logs;
			$id = empty($id)?'12':$id;
			$kd_user = $this->session->kd_admin;
			$t_data['table_id'] = 'dataTable';
			if ($lebar < '600') :
				$t_data['t_header'] = array(
					'<th style="width:1%;" class="none">Kode Log</th>',
					'<th style="width:10%;" class="never">Activity Log</th>',
					'<th style="width:10%;" class="always">User</th>',
					'<th style="width:10%;" class="always">Activity Time</th>',
				);
			else :
				$t_data['t_header'] = array(
					'<th style="width:1%;">Kode Log</th>',
					'<th style="width:1%;" class="none">Activity Log</th>',
					'<th style="width:10%;">User</th>',
					'<th style="width:10%;">Activity Time</th>',
				);
			endif;
			$t_data['t_uri'] = base_url().'administrator/log/activity_log/data_table/'.$id.'/'.$kd_user;
			$t_data['t_order'] = '[5, "DESC"]';
			$t_data['t_col_def'] = '{"targets": 1, "visible": false}';

			$this->load->view('page/admin/table_ssp', $t_data);
		endif;
	}

	function data_table($id, $kd_user) {
		if (!$this->input->is_ajax_request()) :
            exit('No direct script access allowed');
        else :
            $this->load->library('ssp');

			$this->db->where(array('id' => $id));
			$this->db->from('tb_set_dropdown');
			$q_view = $this->db->get();
			$r_view = $q_view->row();
			$view_access = $r_view->nm_select;

			$columns = array(
			    array( 'db' => 'kd_log_activity',
			    		'dt' => 1, 'field' => 'kd_log_activity',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'kd_log_activity',
			    		'dt' => 2, 'field' => 'kd_log_activity',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'activity_log',
			    		'dt' => 3, 'field' => 'activity_log',
			    		'formatter' => function($d){
			    			$d = word_break($this->security->xss_clean($d), 10);

			    			return $d;
			    		} ),
			    array( 'db' => 'user',
			    		'dt' => 4, 'field' => 'user',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'activity_time',
			    		'dt' => 5, 'field' => 'activity_time',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			);

			$sql_details = sql_connect();

			$joinQuery = "";
			$where = "";
			if ($view_access == 'Semua') :
				$where = "";
			elseif ($view_access == 'Hanya Saya') :
				$where = "user = '".$kd_user."'";
			endif;

			echo json_encode(
			    SSP::simple( $_GET, $sql_details, $this->my_tbl, $this->p_key, $columns, $joinQuery, $where )
			);
		endif;
	}
}