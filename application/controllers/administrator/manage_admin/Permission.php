<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permission extends MY_Controller {
    private $class_link = 'administrator/manage_admin/permission';

	function __construct() {
		parent::__construct();
		
		$this->load->model('m_builder');
        $this->load->helper(array('form', 'html', 'my_helper'));
        $this->load->model(['td_admin_tipe', 'td_tipeadmin_permission', 'tb_admin', 'td_admin_permission', 'tm_item_group', 'td_itemgroup_admin']);
	}

	public function index () {
        
    }

    public function table_permissionhakakses_box () {
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }

        $kd_tipe_admin = $this->input->get('kd_tipe_admin');

        $q_admintipe = $this->td_admin_tipe->get_by_param(['kd_tipe_admin' => $kd_tipe_admin])->row_array();

        $data['class_link'] = $this->class_link;
        $data['kd_tipe_admin'] = $kd_tipe_admin;
        $data['admintipe'] = $q_admintipe;

		$this->load->view('page/'.$this->class_link.'/table_permissionhakakses_box', $data);
    }

    public function table_permissionhakakses_main () {
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }

        $kd_tipe_admin = $this->input->get('kd_tipe_admin');

        $q_menu = $this->db->order_by('urutan')->get('tb_menu')->result_array();
        $q_permission = $this->db->get('tb_permission')->result_array();
        $q_hakmenu = $this->db->where(['tipe_admin_kd' => $kd_tipe_admin])->get('tb_hakakses_menu')->result_array();
        $q_permissionTipeAdmin = $this->db->where(['kd_tipe_admin' => $kd_tipe_admin])->get('td_tipeadmin_permission')->result_array();

        $arrayMenu = [];
        $arrayPermissionTipeAdmin = [];
        foreach ($q_hakmenu as $eachMenu) {
            $arrayMenu[] = $eachMenu['menu_kd'];
        }
        foreach ($q_permissionTipeAdmin as $eachPermissionTipeAdmin) {
            $arrayPermissionTipeAdmin[] =  $eachPermissionTipeAdmin['permission_kd'];
        }

        $data['menu'] = $q_menu;
        $data['hak_menu'] = $arrayMenu;
        $data['permission'] = $q_permission;
        $data['permissionTipeAdmin'] = $arrayPermissionTipeAdmin;
        $data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_permissionhakakses_main', $data);
    }

    public function table_permissionadmin_box () {
        // if (!$this->input->is_ajax_request()){
        //     exit('No direct script access allowed');
        // }
        // parent::administrator();

        $kd_admin = $this->input->get('kd_admin');

        $q_admin = $this->tb_admin->get_by_param_detail(['tb_admin.kd_admin' => $kd_admin])->row_array();

        $data['class_link'] = $this->class_link;
        $data['kd_admin'] = $kd_admin;
        $data['admin'] = $q_admin;

		$this->load->view('page/'.$this->class_link.'/table_permissionadmin_box', $data);
    }

    public function table_permissionadmin_main () {
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }

        $kd_admin = $this->input->get('kd_admin');

        $q_admin = $this->tb_admin->get_row($kd_admin);

        $q_menu = $this->db->where(['aktif' => '1'])->order_by('urutan')->get('tb_menu')->result_array();
        $q_permission = $this->db->get('tb_permission')->result_array();
        $q_hakmenu = $this->db->where(['tipe_admin_kd' => $q_admin->tipe_admin_kd])->get('tb_hakakses_menu')->result_array();
        $q_permissionTipeAdmin = $this->db->where(['kd_tipe_admin' => $q_admin->tipe_admin_kd])->get('td_tipeadmin_permission')->result_array();
        $q_permissionAdmin = $this->td_admin_permission->get_by_param(['kd_admin' => $kd_admin])->result_array();

        $arrayMenu = [];
        $arrayPermissionTipeAdmin = [];
        $arrayPermissionAdmin = [];
        foreach ($q_hakmenu as $eachMenu) {
            $arrayMenu[] = $eachMenu['menu_kd'];
        }
        foreach ($q_permissionTipeAdmin as $eachPermissionTipeAdmin) {
            $arrayPermissionTipeAdmin[] =  $eachPermissionTipeAdmin['permission_kd'];
        }
        foreach ($q_permissionAdmin as $eachPermissionAdmin) {
            $arrayPermissionAdmin[] =  $eachPermissionAdmin['permission_kd'];
        }

        $data['menu'] = $q_menu;
        $data['hak_menu'] = $arrayMenu;
        $data['permission'] = $q_permission;
        $data['permissionTipeAdmin'] = $arrayPermissionTipeAdmin;
        $data['permissionAdmin'] = $arrayPermissionAdmin;
        $data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_permissionadmin_main', $data);
    }

    public function table_itemgroup_admin_box () {
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
        $kd_admin = $this->input->get('kd_tipe_admin');
        $q_admin = $this->tb_admin->get_by_param_detail(['td_admin_tipe.kd_tipe_admin' => $kd_admin])->row_array();
        $data['class_link'] = $this->class_link;
        $data['kd_admin'] = $kd_admin;
        $data['admin'] = $q_admin;
		$this->load->view('page/'.$this->class_link.'/table_itemgroupadmin_box', $data);
    }

    public function table_itemgroup_admin_main () {
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
        $kd_tipe_admin = $this->input->get('kd_admin');
        $data['item_group'] = $this->tm_item_group->get_all()->result_array();
        $data['item_group_admin'] = $this->td_itemgroup_admin->get_by_param(['kd_tipe_admin' => $kd_tipe_admin])->result_array();
        $data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_itemgroupadmin_main', $data);
    }

    public function action_hakaksespermission_insertbatch () {
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtkd_tipe_admin', 'Part', 'required', ['required' => '{field} tidak boleh kosong!']);	

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrkd_tipe_admin' => (!empty(form_error('txtkd_tipe_admin')))?buildLabel('warning', form_error('txtkd_tipe_admin', '"', '"')):'',
            );
            
        }else {
            $kd_tipe_admin = $this->input->post('txtkd_tipe_admin');
            $permissions = $this->input->post('chkPermission');
            
            $actDel = $this->td_tipeadmin_permission->delete_by_param(['kd_tipe_admin' => $kd_tipe_admin]);
            
            $data = [];
            if (!empty($permissions)){
                $tipeadminpermission_kd = $this->td_tipeadmin_permission->create_code();
                foreach ($permissions as $eachPermission) {
                    $data[] = [
                        'tipeadminpermission_kd' => $tipeadminpermission_kd,
                        'kd_tipe_admin' => $kd_tipe_admin,
                        'permission_kd' => $eachPermission
                    ];
                    $tipeadminpermission_kd++;
                }

                $act = $this->td_tipeadmin_permission->insert_batch_data($data);
                if ($act) {
                    $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
                } else {
                    $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
                }
            }else{
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }

        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_adminpermission_insertbatch () {
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtkd_admin', 'Part', 'required', ['required' => '{field} tidak boleh kosong!']);	

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrkd_admin' => (!empty(form_error('txtkd_admin')))?buildLabel('warning', form_error('txtkd_admin', '"', '"')):'',
            );
            
        }else {
            $kd_admin = $this->input->post('txtkd_admin');
            $permissions = $this->input->post('chkPermission');
            
            $actDel = $this->td_admin_permission->delete_by_param(['kd_admin' => $kd_admin]);
            
            $data = [];
            if (!empty($permissions)){
                $adminpermission_kd = $this->td_admin_permission->create_code();
                foreach ($permissions as $eachPermission) {
                    $data[] = [
                        'adminpermission_kd' => $adminpermission_kd,
                        'kd_admin' => $kd_admin,
                        'permission_kd' => $eachPermission
                    ];
                    $adminpermission_kd++;
                }

                $act = $this->td_admin_permission->insert_batch_data($data);
                if ($act) {
                    $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => $data);
                } else {
                    $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
                }
            }else{
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }

        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_itemgroupadmin_insertbatch () {
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
            $this->db->trans_begin();
            $kd_tipe_admin = $this->input->post('txtkd_tipe_admin');
            $arrAccess = $this->input->post('checkAccess');
            $actDel = $this->td_itemgroup_admin->delete_by_param(['kd_tipe_admin' => $kd_tipe_admin]);
            $data = [];
            if (!empty($arrAccess)){
                $itemgroupadmin_kd = $this->td_itemgroup_admin->create_code();
                foreach ($arrAccess as $eachAccess) {
                    $data[] = [
                        'itemgroupadmin_kd' => $itemgroupadmin_kd,
                        'kd_tipe_admin' => $kd_tipe_admin,
                        'item_group_kd' => $eachAccess
                    ];
                    $itemgroupadmin_kd++;
                }

                $act = $this->td_itemgroup_admin->insert_batch_data($data);
                if ($this->db->trans_status() === TRUE) {
                    $this->db->trans_commit();
                    $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
                } else {
                    $this->db->trans_rollback();					
                    $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
                }
            }else{
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

	
}