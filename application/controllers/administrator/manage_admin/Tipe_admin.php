<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipe_admin extends MY_Controller {

	function __construct() {
		parent::__construct();
		
		$this->load->model('m_builder');
		$this->load->helper(array('form', 'html', 'my_helper'));
	}

	function index() {
		parent::administrator();
		parent::pnotify_assets();

		$data['t_box_class'] = 'box-primary';
		$title = $this->uri->segment_array();
		$title = end($title);
		if (strpos($title, '_') !== FALSE ) :
			$title = str_replace('_', ' ', $title);
		endif;
		$title = ucwords($title);
		$data['table_title'] = $title;
		$data['table_alert'] = 'idAlertTable';
		$data['table_loader'] = 'idLoaderTable';
		$data['table_name'] = 'idTable';
		$data['table_overlay'] = 'idTableOverlay';
		$data['view_url'] = 'administrator/manage_admin/tipe_admin/data_tipe_lihat';
		$data['form_name'] = 'idBoxForm';
		$data['form_alert'] = 'idAlertForm';
		$data['form_url'] = 'administrator/manage_admin/tipe_admin/data_tipe_form';
		$data['form_overlay'] = 'idOverlayForm';
		$data['form_input'] = 'idFormTipeInput';
		$data['form_input_url'] = 'administrator/manage_admin/tipe_admin/submit_tipe_form/input';
		$data['form_edit'] = 'idFormTipeEdit';
		$data['form_edit_url'] = 'administrator/manage_admin/tipe_admin/submit_tipe_form/edit';
		$data['form_delete_url'] = 'administrator/manage_admin/tipe_admin/hapus_data_tipe';

		$this->load->css('assets/admin_assets/plugins/jquery-ui-1.12.1/jquery-ui.min.css');
		$this->load->js('assets/admin_assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js');
		$this->load->section('scriptJS', 'script/administrator/manage_admin/tipe_admin/scriptJS', $data);
		// $this->data_admin_form();
		$this->load->view('page/admin/v_table', $data);
	}

	function data_tipe_lihat() {
		if ($this->input->is_ajax_request()) :
			/* tabel property */
			$t_data['btn'] = TRUE;
			$t_data['t_header'] = array(
				'<th data-priority="1">Nama Tipe</th>',
				'<th>Management Barang (Sales)</th>',
				'<th>Akses Image (Sales)</th>',
				'<th>Halaman</th>',
				'<th>Access Log</th>',
			);
			$t_data['t_uri'] = base_url().'administrator/manage_admin/tipe_admin/data_tipe_table/';
			$t_data['t_order'] = '[2, "asc"]';

			$this->load->view('page/admin/table_ssp', $t_data);
		endif;
	}

	function data_tipe_table2() {
		if (!$this->input->is_ajax_request()) :
            exit('No direct script access allowed');
        else :

            $this->load->library('ssp');

			$table = 'td_admin_tipe';

			$primaryKey = 'kd_tipe_admin';

			$columns = array(
			    array( 'db' => 'a.kd_tipe_admin',
						'dt' => 1, 'field' => 'kd_tipe_admin',
						'formatter' => function($d){
							$btn_access = '';
							if ($this->session->update_access == '1') :
								$btn_edit = '<a id="edit" title="Edit Data" href="javascript:void(0);" onclick="editData(\''.$d.'\')"><i class="fa fa-pencil"></i> Edit Data</a>';
								$btn_access = '<a id="hakakses" title="Hak Akses" href="javascript:void(0);" onclick="showFormHakAkses(\''.$d.'\')"><i class="fa fa-users"></i> Hak Akses</a>';
								$btn_access .= '<a id="permission" title="Permission" href="javascript:void(0);" onclick="showPermission(\''.$d.'\')"><i class="fa fa-cog"></i> Permission</a>';
								$divider = '<li class="divider"></li>';
							else :
								$btn_edit = '';
								$btn_access = '';
								$divider = '';
							endif;
							if ($this->session->delete_access == '1') :
								$btn_delete = '<a id="delete" title="Hapus Data" href="javascript:void(0);" onclick="return confirm(\'Apakah anda yakin?\')?hapusData(\''.$d.'\'):false;"><i class="fa fa-trash"></i> Hapus Data</a>';
							else :
								$btn_delete = '';
							endif;

							$btn = '<div align="center"><div class="btn-group"><button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">Opsi <span class="caret"></span></button><ul class="dropdown-menu"><li>'.$btn_edit.'</li><li>'.$btn_access.'</li>'.$divider.'<li>'.$btn_delete.'</li></ul></div></div>';
							
							return $btn;
						} ),
			    array( 'db' => 'a.nm_tipe_admin',
			    		'dt' => 2, 'field' => 'nm_tipe_admin',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'b.nm_select AS manage_items',
			    		'dt' => 3, 'field' => 'manage_items',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'c.nm_select AS access_img',
			    		'dt' => 4, 'field' => 'access_img',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'd.nm_select AS halaman',
			    		'dt' => 5, 'field' => 'halaman',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'e.nm_select AS access_log',
			    		'dt' => 6, 'field' => 'access_log',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			);

			$sql_details = sql_connect();

			$joinQuery = "
				FROM
					td_admin_tipe a
				LEFT JOIN tb_set_dropdown b ON a.kd_manage_items = b.id 
				LEFT JOIN tb_set_dropdown c ON a.kd_access_img = c.id
				LEFT JOIN tb_set_dropdown d ON a.kd_halaman = d.id
				LEFT JOIN tb_set_dropdown e ON a.kd_view_logs = e.id
			";
			$where = "";

			echo json_encode(
			    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $where )
			);
		endif;
	}
	
	function data_tipe_table() {
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));

		$data = [];
		$qData = $this->db->select('a.kd_tipe_admin, a.nm_tipe_admin, b.nm_select AS manage_items, c.nm_select AS access_img, d.nm_select AS halaman,
					e.nm_select AS access_log')
				->from('td_admin_tipe as a')
				->join('tb_set_dropdown as b', 'a.kd_manage_items=b.id', 'left')
				->join('tb_set_dropdown as c', 'a.kd_manage_items=c.id', 'left')
				->join('tb_set_dropdown as d', 'a.kd_manage_items=d.id', 'left')
				->join('tb_set_dropdown as e', 'a.kd_manage_items=e.id', 'left')
				->get();
		foreach ($qData->result_array() as $r) {
			$data[] = [
				'1' => $this->tbl_btn($r['kd_tipe_admin']),
				'2' => $r['nm_tipe_admin'],
				'3' => $r['manage_items'],
				'4' => $r['access_img'],
				'5' => $r['halaman'],
				'6' => $r['access_log'],
			];
		}
		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		
		echo json_encode($output);
	}

	function tbl_btn ($id) {
		$btn_access = '';
		if ($this->session->update_access == '1') :
			$btn_edit = '<a id="edit" title="Edit Data" href="javascript:void(0);" onclick="editData(\''.$id.'\')"><i class="fa fa-pencil"></i> Edit Data</a>';
			$btn_access = '<a id="hakakses" title="Hak Akses" href="javascript:void(0);" onclick="showFormHakAkses(\''.$id.'\')"><i class="fa fa-users"></i> Hak Akses</a>';
			$btn_access .= '<a id="permission" title="Permission" href="javascript:void(0);" onclick="showPermission(\''.$id.'\')"><i class="fa fa-cog"></i> Permission</a>';
			$btn_access .= '<a id="item_group" title="Item Group Admin" href="javascript:void(0);" onclick="showItemGroupAdmin(\''.$id.'\')"><i class="fa fa-database"></i> Item Group Admin</a>';
			$divider = '<li class="divider"></li>';
		else :
			$btn_edit = '';
			$btn_access = '';
			$divider = '';
		endif;
		if ($this->session->delete_access == '1') :
			$btn_delete = '<a id="delete" title="Hapus Data" href="javascript:void(0);" onclick="return confirm(\'Apakah anda yakin?\')?hapusData(\''.$id.'\'):false;"><i class="fa fa-trash"></i> Hapus Data</a>';
		else :
			$btn_delete = '';
		endif;

		$btn = '<div align="center"><div class="btn-group"><button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">Opsi <span class="caret"></span></button><ul class="dropdown-menu"><li>'.$btn_edit.'</li><li>'.$btn_access.'</li>'.$divider.'<li>'.$btn_delete.'</li></ul></div></div>';
		
		return $btn;
	}

	function data_tipe_form(){
		if ($this->input->is_ajax_request()) :
			// Form data
			$id = $this->input->get('id');
			if (!empty($id)) :
				$det = $this->m_builder->getRow('td_admin_tipe', $data = array('kd_tipe_admin' => $id), 'row');
				$id_form = 'idFormTipeEdit';
				$data['f_box_class'] = 'box-warning';
				$data['form_title'] = 'Edit Data Tipe Admin';
				$btn_form = 'Hak Akses Menu <i class="fa fa-arrow-right"></i>';
				$kd_tipe_admin = $this->security->xss_clean($det->kd_tipe_admin);
				$nm_tipe_admin = $this->security->xss_clean($det->nm_tipe_admin);
				$kd_manage_items = $this->security->xss_clean($det->kd_manage_items);
				$kd_access_img = $this->security->xss_clean($det->kd_access_img);
				$kd_halaman = $this->security->xss_clean($det->kd_halaman);
				$kd_view_logs = $this->security->xss_clean($det->kd_view_logs);
				$create = ($this->security->xss_clean($det->create) == '1')?TRUE:FALSE;
				$read = ($this->security->xss_clean($det->read) == '1')?TRUE:FALSE;
				$update = ($this->security->xss_clean($det->update) == '1')?TRUE:FALSE;
				$delete = ($this->security->xss_clean($det->delete) == '1')?TRUE:FALSE;
				$upload_product = ($this->security->xss_clean($det->upload_product) == '1')?TRUE:FALSE;
				$upload_sketch = ($this->security->xss_clean($det->upload_sketch) == '1')?TRUE:FALSE;
				$view_diskon_distributor = ($this->security->xss_clean($det->view_diskon_distributor) == '1')?TRUE:FALSE;
				$view_diskon_customer = ($this->security->xss_clean($det->view_diskon_customer) == '1')?TRUE:FALSE;
				$view_detail_report = ($this->security->xss_clean($det->view_detail_report) == '1')?TRUE:FALSE;
			else :
				$id_form = 'idFormTipeInput';
				$data['f_box_class'] = 'box-info';
				$data['form_title'] = 'Input Data Tipe Admin';
				$btn_form = 'Hak Akses Menu <i class="fa fa-arrow-right"></i>';
				$kd_tipe_admin = '';
				$nm_tipe_admin = '';
				$kd_manage_items = '';
				$kd_access_img = '';
				$kd_halaman = '';
				$kd_view_logs = '';
				$create = FALSE;
				$read = FALSE;
				$update = FALSE;
				$delete = FALSE;
				$upload_product = FALSE;
				$upload_sketch = FALSE;
				$view_diskon_distributor = FALSE;
				$view_diskon_customer = FALSE;
				$view_detail_report = FALSE;
			endif;
			
			// Dropdown untuk management items
			$this->db->from('tb_set_dropdown');
			$this->db->where(array('jenis_select' => 'manage_items'));
			$q_manage_item = $this->db->get();
			$r_manage_item = $q_manage_item->result();
			$pil_manage_item = array('' => '-- Pilih Manage Items --');
			foreach ($r_manage_item as $d_manage_item) :
				$pil_manage_item[$this->security->xss_clean($d_manage_item->id)] = $this->security->xss_clean($d_manage_item->nm_select);
			endforeach;
			$sel_manage_item = $kd_manage_items;
			$attr_manage_item = array('id' => 'idSelManageItems', 'class' => 'form-control');
			
			// Dropdown untuk access image
			$this->db->from('tb_set_dropdown');
			$this->db->where(array('jenis_select' => 'access_img'));
			$q_access_img = $this->db->get();
			$r_access_img = $q_access_img->result();
			$pil_access_img = array('' => '-- Pilih Access Image --');
			foreach ($r_access_img as $d_access_img) :
				$pil_access_img[$this->security->xss_clean($d_access_img->id)] = $this->security->xss_clean($d_access_img->nm_select);
			endforeach;
			$sel_access_img = $kd_access_img;
			$attr_access_img = array('id' => 'idSelAccessImg', 'class' => 'form-control');
			
			// Dropdown untuk pilihan halaman
			$this->db->from('tb_set_dropdown');
			$this->db->where(array('jenis_select' => 'manage_pages'));
			$q_halaman = $this->db->get();
			$r_halaman = $q_halaman->result();
			$pil_halaman = array('' => '-- Pilih Halaman --');
			foreach ($r_halaman as $d_halaman) :
				$pil_halaman[$this->security->xss_clean($d_halaman->id)] = $this->security->xss_clean($d_halaman->nm_select);
			endforeach;
			$sel_halaman = $kd_halaman;
			$attr_halaman = array('id' => 'idSelHalaman', 'class' => 'form-control');
			
			// Dropdown untuk pilihan view logs
			$this->db->from('tb_set_dropdown');
			$this->db->where(array('jenis_select' => 'view_logs'));
			$q_log = $this->db->get();
			$r_log = $q_log->result();
			$pil_log = array('' => '-- Pilih Akses Logs --');
			foreach ($r_log as $d_log) :
				$pil_log[$this->security->xss_clean($d_log->id)] = $this->security->xss_clean($d_log->nm_select);
			endforeach;
			$sel_log = $kd_view_logs;
			$attr_log = array('id' => 'idSelLog', 'class' => 'form-control');

			$data['f_attr'] = 'id="idBoxForm"';
			$data['form_url'] = '';
			$data['form_att'] = array('id' => $id_form, 'class' => 'form-horizontal');
			$data['form_hidden'] = '';
			$data['form_close_att'] = '';
			$data['form_field'] = array(
				'nm_tipe_admin' => array(
					'<div class="form-group">',
					'<label for="idTxtNmTipe" class="col-md-2 control-label">Nama Tipe Admin</label>',
					'<div class="col-md-4">',
					'<div id="idErrNmTipe"></div>',
					form_input(array('type' => 'hidden', 'name' => 'txtKdTipe', 'id' => 'idTxtKdTipe', 'value' => $kd_tipe_admin)),
					form_input(array('name' => 'txtNmTipe', 'id' => 'idTxtNmTipe', 'class' => 'form-control', 'placeholder' => 'Nama Tipe Admin', 'value' => $nm_tipe_admin)),
					'</div>',
					'</div>',
				),
				'manage_items' => array(
					'<div class="form-group">',
					'<label for="idSelManageItems" class="col-md-2 control-label">Management Items</label>',
					'<div class="col-md-3">',
					'<div id="idErrManageItems"></div>',
					form_dropdown('selManageItems', $pil_manage_item, $sel_manage_item, $attr_manage_item),
					'</div>',
					'</div>',
				),
				'access_img' => array(
					'<div class="form-group">',
					'<label for="idSelAccessImg" class="col-md-2 control-label">Access Image</label>',
					'<div class="col-md-3">',
					'<div id="idErrAccessImg"></div>',
					form_dropdown('selAccessImg', $pil_access_img, $sel_access_img, $attr_access_img),
					'</div>',
					'</div>',
				),
				'halaman' => array(
					'<div class="form-group">',
					'<label for="idSelHalaman" class="col-md-2 control-label">Halaman</label>',
					'<div class="col-md-3">',
					'<div id="idErrHalaman"></div>',
					form_dropdown('selHalaman', $pil_halaman, $sel_halaman, $attr_halaman),
					'</div>',
					'</div>',
				),
				'log' => array(
					'<div class="form-group">',
					'<label for="idSelHalaman" class="col-md-2 control-label">Access Logs</label>',
					'<div class="col-md-3">',
					'<div id="idErrLog"></div>',
					form_dropdown('selLog', $pil_log, $sel_log, $attr_log),
					'</div>',
					'</div>',
				),
				'create' => array(
					'<div class="col-xs-3 col-xs-offset-1">',
					'<div class="form-group">',
					'<div class="col-sm-offset-2 col-sm-10"><div class="checkbox">',
					'<label>',
					form_checkbox(array('name' => 'chkCreate', 'id' => 'idChkCreate', 'value' => '1', 'checked' => $create,)),
					'Create Access',
					'</label></div></div></div>',
				),
				'read' => array(
					'<div class="form-group">',
					'<div class="col-sm-offset-2 col-sm-10"><div class="checkbox">',
					'<label>',
					form_checkbox(array('name' => 'chkRead', 'id' => 'idChkRead', 'value' => '1', 'checked' => $read,)),
					'Read Access',
					'</label></div></div></div>',
				),
				'update' => array(
					'<div class="form-group">',
					'<div class="col-sm-offset-2 col-sm-10"><div class="checkbox">',
					'<label>',
					form_checkbox(array('name' => 'chkUpdate', 'id' => 'idChkUpdate', 'value' => '1', 'checked' => $update,)),
					'Update Access',
					'</label></div></div></div>',
				),
				'delete' => array(
					'<div class="form-group">',
					'<div class="col-sm-offset-2 col-sm-10"><div class="checkbox">',
					'<label>',
					form_checkbox(array('name' => 'chkDelete', 'id' => 'idChkDelete', 'value' => '1', 'checked' => $delete,)),
					'Delete Access',
					'</label></div></div></div></div>',
				),
				'upload_product' => array(
					'<div class="col-xs-3">',
					'<div class="form-group">',
					'<div class="col-sm-offset-2 col-sm-10"><div class="checkbox">',
					'<label>',
					form_checkbox(array('name' => 'chkProduct', 'id' => 'idChkProduct', 'value' => '1', 'checked' => $upload_product,)),
					'Upload Product Image',
					'</label></div></div></div>',
				),
				'upload_sketch' => array(
					'<div class="form-group">',
					'<div class="col-sm-offset-2 col-sm-10"><div class="checkbox">',
					'<label>',
					form_checkbox(array('name' => 'chkSketch', 'id' => 'idChkSketch', 'value' => '1', 'checked' => $upload_sketch,)),
					'Upload Sketch Image',
					'</label></div></div></div>',
				),
				'view_diskon_distributor' => array(
					'<div class="form-group">',
					'<div class="col-sm-offset-2 col-sm-10"><div class="checkbox">',
					'<label>',
					form_checkbox(array('name' => 'chkDiscDistributor', 'id' => 'idChkDiscDistributor', 'value' => '1', 'checked' => $view_diskon_distributor,)),
					'View Distributor Invoice',
					'</label></div></div></div>',
				),
				'view_diskon_customer' => array(
					'<div class="form-group">',
					'<div class="col-sm-offset-2 col-sm-10"><div class="checkbox">',
					'<label>',
					form_checkbox(array('name' => 'chkDiscCustomer', 'id' => 'chkDiscCustomer', 'value' => '1', 'checked' => $view_diskon_customer,)),
					'View Customer Invoice',
					'</label></div></div></div></div>',
				),
				'view_detail_report' => array(
					'<div class="col-xs-3">',
					'<div class="form-group">',
					'<div class="col-sm-offset-2 col-sm-10"><div class="checkbox">',
					'<label>',
					form_checkbox(array('name' => 'chkDetailReport', 'id' => 'chkDetailReport', 'value' => '1', 'checked' => $view_detail_report,)),
					'View Detail Report',
					'</label></div></div></div></div>',
				),
			);

			$data['form_btn'] = array(
				'btn_submit' => array(
					'<button type="submit" name="btnSubmit" class="btn btn-primary pull-right">',
					$btn_form,
					'</button>',
				),
			);
			$data['form_box'] = 'idBoxFormBody';
			$data['form_alert'] = 'idAlertForm';
			$data['form_overlay'] = 'idOverlayForm';

			$this->load->view('page/admin/v_form', $data);
		endif;
	}

	function submit_tipe_form($act){
		if ($this->input->is_ajax_request()) :
			$this->load->library(array('form_validation'));
			$str['csrf'] = $this->security->get_csrf_hash();

			$this->form_validation->set_rules('txtNmTipe', 'Nama Tipe Admin', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selManageItems', 'Management Items', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selAccessImg', 'Access Image', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selHalaman', 'Halaman', 'required',
				array(
					'required' => '{field} tidak boleh kosong!'
				)
			);
			$this->form_validation->set_rules('selLog', 'Access Logs', 'required',
				array(
					'required' => '{field} tidak boleh kosong!'
				)
			);

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'errValidation';
				$str['idErrNmTipe'] = (!empty(form_error('txtNmTipe')))?buildLabel('warning', form_error('txtNmTipe', '"', '"')):'';
				$str['idErrManageItems'] = (!empty(form_error('selManageItems')))?buildLabel('warning', form_error('selManageItems', '"', '"')):'';
				$str['idErrAccessImg'] = (!empty(form_error('selAccessImg')))?buildLabel('warning', form_error('selAccessImg', '"', '"')):'';
				$str['idErrHalaman'] = (!empty(form_error('selHalaman')))?buildLabel('warning', form_error('selHalaman', '"', '"')):'';
				$str['idErrLog'] = (!empty(form_error('selLog')))?buildLabel('warning', form_error('selLog', '"', '"')):'';
			else :
				/*
				** Jika validasi berhasil dan tidak ada error
				** Data dari form akan diinputkan kedalam tabel
				** Jika query input return true
				** alert success akan ditampilakan, selain itu error (kesalahan sistem!)
				*/
				$nm_tipe_admin = $this->input->post('txtNmTipe');
				$kd_manage_items = $this->input->post('selManageItems');
				$kd_access_img = $this->input->post('selAccessImg');
				$kd_halaman = $this->input->post('selHalaman');
				$kd_view_logs = $this->input->post('selLog');
				$create = !empty($this->input->post('chkCreate'))?'1':'0';
				$read = !empty($this->input->post('chkRead'))?'1':'0';
				$update = !empty($this->input->post('chkUpdate'))?'1':'0';
				$delete = !empty($this->input->post('chkDelete'))?'1':'0';
				$upload_product = !empty($this->input->post('chkProduct'))?'1':'0';
				$upload_sketch = !empty($this->input->post('chkSketch'))?'1':'0';
				$view_diskon_distributor = !empty($this->input->post('chkDiscDistributor'))?'1':'0';
				$view_diskon_customer = !empty($this->input->post('chkDiscCustomer'))?'1':'0';
				$view_detail_report = !empty($this->input->post('chkDetailReport'))?'1':'0';
				if ($act == 'input') :
					$conds = array(
						'select' => 'id, kd_tipe_admin',
						'order_by' => 'id DESC',
						'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
					);
					$kd_tipe_admin = $this->m_builder->buat_kode('td_admin_tipe', $conds, 3, 'TPA');

					$data = array(
						'kd_tipe_admin' => $kd_tipe_admin,
						'nm_tipe_admin' => $nm_tipe_admin,
						'kd_manage_items' => $kd_manage_items,
						'kd_access_img' => $kd_access_img,
						'kd_halaman' => $kd_halaman,
						'kd_view_logs' => $kd_view_logs,
						'create' => $create,
						'read' => $read,
						'update' => $update,
						'delete' => $delete,
						'upload_product' => $upload_product,
						'upload_sketch' => $upload_sketch,
						'view_diskon_distributor' => $view_diskon_distributor,
						'view_diskon_customer' => $view_diskon_customer,
						'view_detail_report' => $view_detail_report,
					);
					$aksi = $this->db->insert('td_admin_tipe', $data);
					$label_err = 'menambahkan';
				elseif ($act == 'edit') :
					$kd_tipe_admin = $this->input->post('txtKdTipe');
					$data = array(
						'nm_tipe_admin' => $nm_tipe_admin,
						'kd_manage_items' => $kd_manage_items,
						'kd_access_img' => $kd_access_img,
						'kd_halaman' => $kd_halaman,
						'kd_view_logs' => $kd_view_logs,
						'create' => $create,
						'read' => $read,
						'update' => $update,
						'delete' => $delete,
						'upload_product' => $upload_product,
						'upload_sketch' => $upload_sketch,
						'view_diskon_distributor' => $view_diskon_distributor,
						'view_diskon_customer' => $view_diskon_customer,
						'view_detail_report' => $view_detail_report,
					);
					$where = array('kd_tipe_admin' => $kd_tipe_admin);
					$aksi = $this->db->update('td_admin_tipe', $data, $where);
					if ($kd_tipe_admin == $this->session->tipe_admin_kd AND $aksi) :
						$userdata = array(
							'kd_manage_items' => $kd_manage_items,
							'kd_access_img' => $kd_access_img,
							'kd_halaman' => $kd_halaman,
							'kd_view_logs' => $kd_view_logs,
							'tipe_admin' => $nm_tipe_admin,
							'create_access' => $create,
							'read_access' => $read,
							'update_access' => $update,
							'delete_access' => $delete,
							'upload_product' => $upload_product,
							'upload_sketch' => $upload_sketch,
							'view_diskon_distributor' => $view_diskon_distributor,
							'view_diskon_customer' => $view_diskon_customer,
							'view_detail_report' => $view_detail_report,
						);
						$this->session->set_userdata($userdata);
					endif;
					$label_err = 'mengubah';
				endif;

				$str['kd_tipe_admin'] = $kd_tipe_admin;
				if ($aksi) :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' tipe admin dengan kd_tipe_admin \''.$kd_tipe_admin.'\' nm_tipe_admin \''.$nm_tipe_admin.'\' kd_manage_items \''.$kd_manage_items.'\' kd_access_img \''.$kd_access_img.'\' kd_halaman \''.$kd_halaman.'\' kd_view_logs \''.$kd_view_logs.'\' create \''.$create.'\' read \''.$read.'\' update \''.$update.'\' delete \''.$delete.'\' upload_product \''.$upload_product.'\' upload_sketch \''.$upload_sketch.'\' view_diskon_distributor \''.$view_diskon_distributor.'\' view_diskon_customer \''.$view_diskon_customer.'\' view_detail_report \''.$view_detail_report.'\'');
					$str['confirm'] = 'success';
					$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' data tipe admin!');
				else :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' tipe admin dengan kd_tipe_admin \''.$kd_tipe_admin.'\' nm_tipe_admin \''.$nm_tipe_admin.'\' kd_manage_items \''.$kd_manage_items.'\' kd_access_img \''.$kd_access_img.'\' kd_halaman \''.$kd_halaman.'\' kd_view_logs \''.$kd_view_logs.'\' create \''.$create.'\' read \''.$read.'\' update \''.$update.'\' delete \''.$delete.'\' upload_product \''.$upload_product.'\' upload_sketch \''.$upload_sketch.'\' view_diskon_distributor \''.$view_diskon_distributor.'\' view_diskon_customer \''.$view_diskon_customer.'\' view_detail_report \''.$view_detail_report.'\'');
					$str['confirm'] = 'errValidation';
					$str['idErrForm'] = buildLabel('danger', 'Gagal '.$label_err.' data tipe admin, Kesalahan sistem!');
				endif;
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function hapus_data_tipe() {
		if ($this->input->is_ajax_request()) :
			$kd_tipe_admin = $this->input->get('id');
			$label_err = 'menghapus';
			$aksi = $this->db->delete('td_admin_tipe', array('kd_tipe_admin' => $kd_tipe_admin));

			$data = array(
				'tipe_admin_kd' => '',
			);
			$where = array('tipe_admin_kd' => $kd_tipe_admin);
			$aksi = $this->db->update('tb_admin', $data, $where);

			if ($aksi) :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' tipe admin dengan kd_tipe_admin \''.$kd_tipe_admin.'\', kd_tipe_admin tsb juga dihilangkan di tb_admin!');
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' data tipe admin!');
			else :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' tipe admin dengan kd_tipe_admin \''.$kd_tipe_admin.'\'');
				$str['alert'] = buildLabel('danger', 'Gagal '.$label_err.' data tipe admin, Kesalahan sistem!');
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}