<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_admin extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->model('m_builder');
		$this->load->helper(array('form', 'html', 'my_helper'));
	}

	function index() {
		parent::administrator();
		parent::pnotify_assets();

		$data['t_box_class'] = 'box-primary';
		$title = $this->uri->segment_array(); // Untuk mengambil url
		$title = end($title);   // Untuk mengambil data terakhir dari array url
		if (strpos($title, '_') !== FALSE ) :
			$title = str_replace('_', ' ', $title);
		endif;
		$title = ucwords($title);   // Untuk mengubah huruf awal kata menjadi Upper
		$data['table_title'] = $title;
		$data['table_alert'] = 'idAlertTable';
		$data['table_loader'] = 'idLoaderTable';
		$data['table_name'] = 'idTable';
		$data['table_overlay'] = 'idTableOverlay';
		$data['view_url'] = 'administrator/manage_admin/data_admin/data_admin_lihat';
		$data['form_name'] = 'idBoxForm';
		$data['form_alert'] = 'idAlertForm';
		$data['form_url'] = 'administrator/manage_admin/data_admin/data_admin_form';
		$data['form_overlay'] = 'idOverlayForm';
		$data['form_input'] = 'idFormAdminInput';
		$data['form_input_url'] = 'administrator/manage_admin/data_admin/submit_admin_form/input';
		$data['form_edit'] = 'idFormAdminEdit';
		$data['form_edit_url'] = 'administrator/manage_admin/data_admin/submit_admin_form/edit';
		$data['form_delete_url'] = 'administrator/manage_admin/data_admin/hapus_data_admin';

		$this->load->css('assets/admin_assets/plugins/jquery-ui-1.12.1/jquery-ui.min.css');
		$this->load->js('assets/admin_assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js');
		$this->load->section('scriptJS', 'script/administrator/manage_admin/data_admin/scriptJS', $data);
		// $this->data_admin_form();
		$this->load->view('page/admin/v_table', $data);
	}

	function data_admin_lihat() {
		if ($this->input->is_ajax_request()) :
			/* tabel property */
			$t_data['btn'] = TRUE;
			$t_data['t_header'] = array(
				'<th data-priority="1">Username</th>',
				'<th>Tipe Admin</th>',
				'<th>Kode Karyawan</th>',
				'<th>Email Karyawan</th>',
				'<th>Status Aktif</th>',
			);
			$t_data['t_uri'] = base_url().'administrator/manage_admin/data_admin/data_admin_table/'.$this->session->kd_admin;
			$t_data['t_order'] = '[2, "asc"]';

			$this->load->view('page/admin/table_ssp', $t_data);
		endif;
	}

	function data_admin_table($kd_admin) {
		if (!$this->input->is_ajax_request()) :
            exit('No direct script access allowed');
        else :
            $this->load->library('ssp');

			$table = 'tb_admin';

			$primaryKey = 'kd_admin';

			$columns = array(
			    array( 'db' => 'a.kd_admin',
						'dt' => 1, 'field' => 'kd_admin',
						'formatter' => function($d){
							$btn_edit = '';
							if ($this->session->update_access == '1') :
								$btn_edit .= '<a id="edit" title="Edit Data" href="javascript:void(0);" onclick="editData(\''.$d.'\')"><i class="fa fa-pencil"></i> Edit Data</a>';
								$btn_edit .= '<a id="edit" title="Permission" href="javascript:void(0);" onclick="permissionData(\''.$d.'\')"><i class="fa fa-cog"></i> Permission</a>';
								$divider = '<li class="divider"></li>';
							else :
								$btn_edit = '';
								$divider = '';
							endif;
							if ($this->session->delete_access == '1') :
								$btn_delete = '<a id="delete" title="Hapus Data" href="javascript:void(0);" onclick="return confirm(\'Apakah anda yakin?\')?hapusData(\''.$d.'\'):false;"><i class="fa fa-trash"></i> Hapus Data</a>';
							else :
								$btn_delete = '';
							endif;

							$btn = '<div align="center"><div class="btn-group"><button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">Opsi <span class="caret"></span></button><ul class="dropdown-menu"><li>'.$btn_edit.'</li>'.$divider.'<li>'.$btn_delete.'</li></ul></div></div>';
							
							return $btn;
						} ),
			    array( 'db' => 'a.username',
			    		'dt' => 2, 'field' => 'username',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'b.nm_tipe_admin',
			    		'dt' => 3, 'field' => 'nm_tipe_admin',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'a.kd_karyawan',
			    		'dt' => 4, 'field' => 'kd_karyawan',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'a.admin_email',
			    		'dt' => 5, 'field' => 'admin_email',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'a.admin_stsaktif',
			    		'dt' => 6, 'field' => 'admin_stsaktif',
			    		'formatter' => function($d){
							if ($d == 1) {
								$d = build_span ('success', 'Aktif');
							}else {
								$d = build_span ('danger', 'Tidak Aktif');
							}
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			);

			$sql_details = sql_connect();

			$joinQuery = "FROM
							tb_admin a
						LEFT JOIN td_admin_tipe b ON b.kd_tipe_admin = a.tipe_admin_kd
						";
			// $where = "kd_admin != '".$kd_admin."'";
			$where = "";

			echo json_encode(
			    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $where )
			);
		endif;
	}

	function data_admin_form(){
		if ($this->input->is_ajax_request()) :
			// Form data
			$id = $this->input->get('id');
			if (!empty($id)) :
				$det_admin = $this->m_builder->getRow('tb_admin', $data = array('kd_admin' => $id), 'row');
				$id_form = 'idFormAdminEdit';
				$data['f_box_class'] = 'box-warning';
				$data['form_title'] = 'Edit Data Admin';
				$btn_form = '<i class="fa fa-pencil"></i> Edit Data';
				$kd_admin = $this->security->xss_clean($det_admin->kd_admin);
				$nm_admin = $this->security->xss_clean($det_admin->nm_admin);
				$username = $this->security->xss_clean($det_admin->username);
				$tipe = $this->security->xss_clean($det_admin->tipe_admin_kd);
				$kd_karyawan = $this->security->xss_clean($det_admin->kd_karyawan);
				$admin_email = $this->security->xss_clean($det_admin->admin_email);
				$admin_stsaktif = $this->security->xss_clean($det_admin->admin_stsaktif);
				if (!empty($kd_karyawan)) :
					$db_hrm = $this->load->database('sim_hrm', TRUE);
					$db_hrm->select('nm_karyawan');
					$db_hrm->where(array('kd_karyawan' => $kd_karyawan));
					$db_hrm->from('tb_karyawan');
					$q_hrm = $db_hrm->get();
					$j_hrm = $q_hrm->num_rows();
					$r_hrm = $q_hrm->row();
					$nm_karyawan = $this->security->xss_clean($r_hrm->nm_karyawan);
					$check = TRUE;
				else :
					$nm_karyawan = '';
					$check = FALSE;
				endif;
				$username_lama = form_input(array('type' => 'hidden', 'name' => 'txtUsernameLama', 'id' => 'idTxtUsernameLama', 'value' => $username));
			else :
				$id_form = 'idFormAdminInput';
				$data['f_box_class'] = 'box-info';
				$data['form_title'] = 'Input Data Admin';
				$btn_form = '<i class="fa fa-save"></i> Input Data';
				$kd_admin = '';
				$nm_admin = '';
				$username = '';
				$tipe = '';
				$kd_karyawan = '';
				$nm_karyawan = '';
				$check = FALSE;
				$username_lama = '';
				$admin_email = '';
				$admin_stsaktif = '';
			endif;
		
			$this->db->from('td_admin_tipe');
			$q_tipe = $this->db->get();
			$r_tipe = $q_tipe->result();
			$pil_tipe = array('' => '-- Pilih Tipe Admin --');
			foreach ($r_tipe as $d_tipe) :
				$pil_tipe[$d_tipe->kd_tipe_admin] = $d_tipe->nm_tipe_admin;
			endforeach;
			$sel_tipe = $tipe;
			$attr_tipe = array('id' => 'idSelTipe', 'class' => 'form-control');
			$chkLinkAttr = array(
				'name' => 'chkLink',
				'id' => 'idChkLink',
				'value' => '1',
				'checked' => $check,
			);
			$sel_stsaktif = $admin_stsaktif;
			$attr_stsaktif = array('id' => 'idSelstsaktif', 'class' => 'form-control');

			$data['f_attr'] = 'id="idBoxForm"';
			$data['form_url'] = '';
			$data['form_att'] = array('id' => $id_form, 'class' => 'form-horizontal');
			$data['form_hidden'] = '';
			$data['form_close_att'] = '';
			$data['form_field'] = array(
				'nm_admin' => array(
					'<div class="form-group">',
					'<label for="idTxtNmAdmin" class="col-md-2 control-label">Nama Admin</label>',
					'<div class="col-md-4">',
					'<div id="idErrNmAdmin"></div>',
					form_input(array('type' => 'hidden', 'name' => 'txtKdAdmin', 'id' => 'idTxtKdAdmin', 'value' => $kd_admin)),
					form_input(array('name' => 'txtNmAdmin', 'id' => 'idTxtNmAdmin', 'class' => 'form-control', 'placeholder' => 'Nama Admin', 'value' => $nm_admin)),
					'</div>',
					'</div>',
				),
				'admin_email' => array(
					'<div class="form-group">',
					'<label for="idTxtadmin_email" class="col-md-2 control-label">Email Admin</label>',
					'<div class="col-md-4">',
					'<div id="idErradmin_email"></div>',
					form_input(array('name' => 'txtadmin_email', 'id' => 'idTxtadmin_email', 'class' => 'form-control', 'placeholder' => 'Email Admin', 'value' => $admin_email)),
					'</div>',
					'</div>',
				),
				'username' => array(
					'<div class="form-group">',
					'<label for="idTxtUsername" class="col-md-2 control-label">Username</label>',
					'<div class="col-md-3">',
					'<div id="idErrUsername"></div>',
					form_input(array('name' => 'txtUsername', 'id' => 'idTxtUsername', 'class' => 'form-control', 'placeholder' => 'Username', 'value' => $username)),
					$username_lama,
					'</div>',
					'</div>',
				),
				'password' => array(
					'<div class="form-group">',
					'<label for="idTxtPassword" class="col-md-2 control-label">Password</label>',
					'<div class="col-md-3">',
					'<div id="idErrPassword"></div>',
					form_password(array('name' => 'txtPassword', 'id' => 'idTxtPassword', 'class' => 'form-control', 'placeholder' => 'Password')),
					'</div>',
					'</div>',
				),
				'password_conf' => array(
					'<div class="form-group">',
					'<label for="idTxtPasswordConf" class="col-md-2 control-label">Konfirmasi Password</label>',
					'<div class="col-md-3">',
					'<div id="idErrPasswordConf"></div>',
					form_password(array('name' => 'txtPasswordConf', 'id' => 'idTxtPasswordConf', 'class' => 'form-control', 'placeholder' => 'Konfirmasi Password')),
					'</div>',
					'</div>',
				),
				'tipe_admin' => array(
					'<div class="form-group">',
					'<label for="idSelTipe" class="col-md-2 control-label">Tipe Admin</label>',
					'<div class="col-md-3">',
					'<div id="idErrTipe"></div>',
					form_dropdown('selTipe', $pil_tipe, $sel_tipe, $attr_tipe),
					'</div>',
					'</div>',
				),
				'check_link_karyawan' => array(
					'<div class="form-group">',
					'<div class="col-sm-offset-2 col-sm-10"><div class="checkbox">',
					'<label>',
					form_checkbox($chkLinkAttr),
					'Link Data Karyawan',
					'</label></div></div></div>',
				),
				'nama_karyawan' => array(
					'<div id="idFormNmKaryawan" style="display:none;"><div class="form-group">',
					'<label for="idTxtNmKaryawan" class="col-md-2 control-label">Nama Karyawan</label>',
					'<div class="col-md-4">',
					form_input(array('type' => 'hidden', 'name' => 'txtKdKaryawan', 'id' => 'idTxtKdKaryawan', 'value' => $kd_karyawan)),
					form_input(array('name' => 'txtNamaKaryawan', 'id' => 'idTxtNmKaryawan', 'class' => 'form-control', 'placeholder' => 'Nama Karyawan', 'value' => $nm_karyawan)),
					'</div></div></div>',
				),
				'admin_stsaktif' => array(
					'<div class="form-group">',
					'<label for="idSelstsaktif" class="col-md-2 control-label">Status Admin</label>',
					'<div class="col-md-3">',
					'<div id="idErrStatusAktif"></div>',
					form_dropdown('admin_stsaktif', ['1' => 'Aktif', '0' => 'Tidak Aktif'], $sel_stsaktif, $attr_stsaktif),
					'</div>',
					'</div>',
				),
			);

			$data['form_btn'] = array(
				'btn_submit' => array(
					'<button type="submit" name="btnSubmit" class="btn btn-primary pull-right">',
					$btn_form,
					'</button>',
				),
			);
			$data['form_box'] = 'idBoxForm';
			$data['form_alert'] = 'idAlertForm';
			$data['form_overlay'] = 'idOverlayForm';

			$this->load->section('form_css', 'css/auto_complete/form_css');
			$this->load->section('form_js', 'script/administrator/manage_admin/data_admin/form_js');
			$this->load->view('page/admin/v_form', $data);
		endif;
	}

	function submit_admin_form($act){
		if ($this->input->is_ajax_request()) :
			$this->load->library(array('form_validation'));
			$str['csrf'] = $this->security->get_csrf_hash();

			$this->form_validation->set_rules('txtNmAdmin', 'Nama Admin', 'required',
				array(
					'required' => '{field} tidak boleh kosong!'
				)
			);
			if (($act == 'input') || ($act == 'edit' && $this->input->post('txtUsername') != $this->input->post('txtUsernameLama'))) :
				$this->form_validation->set_rules('txtUsername', 'Username', 'required|is_unique[tb_admin.username]',
					array(
						'required' => '{field} tidak boleh kosong!',
						'is_unique' => '{field} tidak boleh sama dengan yang sudah ada!',
					)
				);
			endif;
			if (($act == 'input') || ($act == 'edit' && !empty($this->input->post('txtPassword')))) :
				$this->form_validation->set_rules('txtPassword', 'Password', 'required',
					array(
						'required' => '{field} tidak boleh kosong!'
					)
				);
				$this->form_validation->set_rules('txtPasswordConf', 'Konfirmasi Password', 'required|matches[txtPassword]',
					array(
						'required' => '{field} tidak boleh kosong!',
						'matches' => '{field} harus sama dengan password!',
					)
				);
			endif;
			$this->form_validation->set_rules('selTipe', 'Tipe Admin', 'required',
				array(
					'required' => '{field} tidak boleh kosong!'
				)
			);

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'errValidation';
				$str['idErrNmAdmin'] = (!empty(form_error('txtNmAdmin')))?buildLabel('warning', form_error('txtNmAdmin', '"', '"')):'';
				$str['idErrUsername'] = (!empty(form_error('txtUsername')))?buildLabel('warning', form_error('txtUsername', '"', '"')):'';
				if ($act == 'input') :
					$str['idErrPassword'] = (!empty(form_error('txtPassword')))?buildLabel('warning', form_error('txtPassword', '"', '"')):'';
					$str['idErrPasswordConf'] = (!empty(form_error('txtPasswordConf')))?buildLabel('warning', form_error('txtPasswordConf', '"', '"')):'';
				elseif ($act == 'edit') :
					$str['idErrPasswordConf'] = (!empty(form_error('txtPasswordConf')))?buildLabel('warning', form_error('txtPasswordConf', '"', '"')):'';
				endif;
				$str['idErrTipe'] = (!empty(form_error('selTipe')))?buildLabel('warning', form_error('selTipe', '"', '"')):'';
			else :
				/*
				** Jika validasi berhasil dan tidak ada error
				** Data dari form akan diinputkan kedalam tabel
				** Jika query input return true
				** alert success akan ditampilakan, selain itu error (kesalahan sistem!)
				*/
				$nm_admin = $this->input->post('txtNmAdmin');
				$username = $this->input->post('txtUsername');
				$password = $this->input->post('txtPassword');
				$tipe = $this->input->post('selTipe');
				$kd_karyawan = $this->input->post('txtKdKaryawan');
				$admin_email = $this->input->post('txtadmin_email');
				$admin_stsaktif = $this->input->post('admin_stsaktif');
				if ($act == 'input') :
					$conds = array(
						'select' => 'id, kd_admin',
						'order_by' => 'id DESC',
						'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
					);
					$kd_admin = $this->m_builder->buat_kode('tb_admin', $conds, 3, 'ADM');

					$data = array(
						'kd_admin' => $kd_admin,
						'nm_admin' => $nm_admin,
						'username' => $username,
						'password' => hashText($password),
						'tipe_admin_kd' => $tipe,
						'kd_karyawan' => $kd_karyawan,
						'admin_email' => !empty($admin_email) ? $admin_email : null,
						'admin_stsaktif' => $admin_stsaktif,
					);
					$aksi = $this->db->insert('tb_admin', $data);
					$label_err = 'menambahkan';
				elseif ($act == 'edit') :
					$kd_admin = $this->input->post('txtKdAdmin');

					$data = array(						
						'nm_admin' => $nm_admin,
						'username' => $username,						
						'tipe_admin_kd' => $tipe,
						'kd_karyawan' => $kd_karyawan,
						'admin_email' => !empty($admin_email) ? $admin_email : null,
						'admin_stsaktif' => $admin_stsaktif,
					);
					if (!empty($password)) :
						$data = array_merge($data, array('password' => hashText($password)));
					endif;
					$where = array('kd_admin' => $kd_admin,);
					$aksi = $this->db->update('tb_admin', $data, $where);
					$label_err = 'mengubah';
				endif;

				if ($aksi) :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' data admin dengan kd_admin \''.$kd_admin.'\' nm_admin \''.$nm_admin.'\' username \''.$username.'\' tipe_admin_kd \''.$tipe.'\' kd_karyawan \''.$kd_karyawan.'\'');
					$str['confirm'] = 'success';
					$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' data admin!');
				else :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' data admin dengan kd_admin \''.$kd_admin.'\' nm_admin \''.$nm_admin.'\' username \''.$username.'\' tipe_admin_kd \''.$tipe.'\' kd_karyawan \''.$kd_karyawan.'\'');
					$str['confirm'] = 'errValidation';
					$str['idErrForm'] = buildLabel('danger', 'Gagal '.$label_err.' data admin, Kesalahan sistem!');
				endif;
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function hapus_data_admin() {
		if ($this->input->is_ajax_request()) :
			$kd_admin = $this->input->get('id');
			$label_err = 'menghapus';
			$aksi = $this->db->delete('tb_admin', array('kd_admin' => $kd_admin));

			if ($aksi) :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' data admin dengan kd_admin \''.$kd_admin.'\' nm_admin \''.$nm_admin.'\' username \''.$username.'\' tipe_admin_kd \''.$tipe.'\' kd_karyawan \''.$kd_karyawan.'\'');
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' data admin!');
			else :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' data admin dengan kd_admin \''.$kd_admin.'\' nm_admin \''.$nm_admin.'\' username \''.$username.'\' tipe_admin_kd \''.$tipe.'\' kd_karyawan \''.$kd_karyawan.'\'');
				$str['alert'] = buildLabel('danger', 'Gagal '.$label_err.' data admin, Kesalahan sistem!');
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}