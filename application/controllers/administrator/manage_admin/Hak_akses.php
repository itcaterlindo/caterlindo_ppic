<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hak_akses extends MY_Controller {

	function __construct() {
		parent::__construct();
		
		$this->load->model('m_builder');
		$this->load->helper(array('form', 'html', 'my_helper'));
	}

	function hak_akses_form(){
		if ($this->input->is_ajax_request()) :
			// Detail tipe admin
			$kd_tipe_admin = $this->input->get('id');
			$this->db->where(array('kd_tipe_admin' => $kd_tipe_admin));
			$this->db->from('td_admin_tipe');
			$q_tipe_admin = $this->db->get();
			$r_tipe_admin = $q_tipe_admin->row();
			$kd_halaman = $r_tipe_admin->kd_halaman;

			$btn_form = '<i class="fa fa-edit"></i> Ubah Hak Akses';
			$data['t_box_class'] = 'box-danger';
			$data['f_attr'] = 'id="idBoxFormHakAkses"';
			$data['table_title'] = 'Form Ubah Hak Akses';
			$data['table_alert'] = 'idAlertTableHakAkses';
			$data['table_loader'] = 'idLoaderTableHakAkses';
			$data['table_overlay'] = 'idOverlayTableHakAkses';
			$data['form_url'] = '';
			$data['form_att'] = array('id' => 'idFormUpdateHakAkses');
			$data['form_hidden'] = array('txtKdTipe' => $kd_tipe_admin);
			$data['form_close_att'] = '';

			// Buat table
			$data['t_header'] = array(
				'<th>Nama Menu </th>',
				'<th style="width: 10%;">Pilih</th>',
			);
			// Data menu
			$no = 0;
			$this->db->where(array('aktif' => '1', 'level_menu' => '0', 'kd_halaman' => $kd_halaman));
			$this->db->order_by('urutan ASC');
			$this->db->from('tb_menu');
			$q_menu = $this->db->get();
			$r_menu = $q_menu->result();
			foreach ($r_menu as $d_menu) :
				$no++;
				$font_weight = $d_menu->tipe_menu == 'single'?'style="font-weight:500;"':'';

				// Check menu id di hakakses menu
				$this->db->where(array('tipe_admin_kd' => $kd_tipe_admin, 'menu_kd' => $d_menu->id));
				$this->db->from('tb_hakakses_menu');
				$q_check = $this->db->get();
				$j_check = $q_check->num_rows();
				$check = ($j_check > 0)?TRUE:FALSE;

				if ($d_menu->tipe_menu == 'single') :
					$data['t_body'][$d_menu->id] = array(
						'<tr>',
						'<td><label for="idChk'.$no.'" class="col-xs-12" '.$font_weight.'>'.ucwords($d_menu->nm_menu).'</label></td>',
						'<td align="center">'.form_checkbox(array('name' => 'chkMenu[]', 'id' => 'idChk'.$no, 'value' => $d_menu->id, 'checked' => $check)).'</td>',
						'</tr>',
					);
				elseif ($d_menu->tipe_menu == 'tree') :
					$noSub = 0;
					$data['t_body'][$d_menu->id] = array(
						'<tr>',
						'<td><label for="idChk'.$no.'" class="col-xs-12" '.$font_weight.'>'.ucwords($d_menu->nm_menu).'</label></td>',
						'<th align="center">Sub Menu</th>',
						'</tr>',
					);
					$this->db->where(array('aktif' => '1', 'level_menu' => '1', 'kd_halaman' => $kd_halaman, 'parent_menu' => $d_menu->id));
					$this->db->order_by('urutan ASC');
					$this->db->from('tb_menu');
					$q_submenu = $this->db->get();
					$r_submenu = $q_submenu->result();
					foreach ($r_submenu as $d_submenu) :
						$noSub++;
						$no_sub = $no.$noSub;
						$font_weight_sub = $d_submenu->tipe_menu == 'single'?'style="font-weight:500;"':'';

						// Check menu id di hakakses menu
						$this->db->where(array('tipe_admin_kd' => $kd_tipe_admin, 'menu_kd' => $d_submenu->id));
						$this->db->from('tb_hakakses_menu');
						$q_check = $this->db->get();
						$j_check = $q_check->num_rows();
						$check = ($j_check > 0)?TRUE:FALSE;

						if ($d_submenu->tipe_menu == 'single') :
							$data['t_body'][$d_submenu->id] = array(
								'<tr>',
								'<td><label for="idChk'.$no_sub.'" class="col-xs-11 col-xs-offset-1" '.$font_weight_sub.'>'.ucwords($d_submenu->nm_menu).'</label></td>',
								'<td align="center">'.form_checkbox(array('name' => 'chkMenu[]', 'id' => 'idChk'.$no_sub, 'value' => $d_menu->id.'-'.$d_submenu->id, 'checked' => $check)).'</td>',
								'</tr>',
							);
						elseif ($d_submenu->tipe_menu == 'tree') :
							$noSubSub = 0;
							$data['t_body'][$d_submenu->id] = array(
								'<tr>',
								'<td><label for="idChk'.$no_sub.'" class="col-xs-11 col-xs-offset-1" '.$font_weight_sub.'>'.ucwords($d_submenu->nm_menu).'</label></td>',
								'<th align="center">Sub Menu</th>',
								'</tr>',
							);
							$this->db->where(array('aktif' => '1', 'level_menu' => '2', 'kd_halaman' => $kd_halaman, 'parent_menu' => $d_submenu->id));
							$this->db->order_by('urutan ASC');
							$this->db->from('tb_menu');
							$q_subsubmenu = $this->db->get();
							$r_subsubmenu = $q_subsubmenu->result();
							foreach ($r_subsubmenu as $d_subsubmenu) :
								$noSubSub++;
								$no_sub_sub = $no.$noSub.$noSubSub;
								$font_weight_sub_sub = 'style="font-weight:500;"';

								// Check menu id di hakakses menu
								$this->db->where(array('tipe_admin_kd' => $kd_tipe_admin, 'menu_kd' => $d_subsubmenu->id));
								$this->db->from('tb_hakakses_menu');
								$q_check = $this->db->get();
								$j_check = $q_check->num_rows();
								$check = ($j_check > 0)?TRUE:FALSE;
								
								$data['t_body'][$d_subsubmenu->id] = array(
									'<tr>',
									'<td><label for="idChk'.$no_sub_sub.'" class="col-xs-10 col-xs-offset-2" '.$font_weight_sub_sub.'>'.ucwords($d_subsubmenu->nm_menu).'</label></td>',
									'<td align="center">'.form_checkbox(array('name' => 'chkMenu[]', 'id' => 'idChk'.$no_sub_sub, 'value' => $d_menu->id.'-'.$d_submenu->id.'-'.$d_subsubmenu->id, 'checked' => $check)).'</td>',
									'</tr>',
								);
							endforeach;
						endif;
					endforeach;
				endif;
			endforeach;

			$data['form_btn'] = array(
				'btn_submit' => array(
					'<button type="submit" name="btnSubmit" class="btn btn-primary pull-right">',
					$btn_form,
					'</button>',
				),
			);

			$this->load->view('page/admin/table_form', $data);
		endif;
	}

	function submit_hak_akses($act){
		if ($this->input->is_ajax_request()) :
			$this->load->library(array('form_validation'));
			$str['csrf'] = $this->security->get_csrf_hash();

			$label_err = 'mengubah';
			$kd_tipe_admin = $this->input->post('txtKdTipe');
			$check = $this->input->post('chkMenu');
			$this->db->trans_start();
			$aksi = $this->db->delete('tb_hakakses_menu', array('tipe_admin_kd' => $kd_tipe_admin));
			foreach ($check as $key) :
				if (strrpos($key, '-')) :
					$arMenu = explode('-', $key);
					$jmlArMenu = count($arMenu);
					for ($x = 0;$x < $jmlArMenu;$x++) {
						$menu[] = $arMenu[$x];
					}
				else :
					$menu[] = $key;
				endif;
			endforeach;
			$menu = array_unique($menu);
			$jmlMenu = count($menu);

			foreach ($menu as $r_menu) :
				$data = array(
					'tipe_admin_kd' => $kd_tipe_admin,
					'menu_kd' => $r_menu,
				);
				$aksi = $this->db->insert('tb_hakakses_menu', $data);
			endforeach;
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE) :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' hak akses untuk kd_tipe_admin \''.$kd_tipe_admin.'\'');
				$str['confirm'] = 'errValidation';
				$str['idErrForm'] = buildLabel('danger', 'Gagal '.$label_err.' hakakses admin, Kesalahan sistem!');
			else :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' hak akses untuk kd_tipe_admin \''.$kd_tipe_admin.'\'');
				$str['confirm'] = 'success';
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' hakakses admin!');
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}