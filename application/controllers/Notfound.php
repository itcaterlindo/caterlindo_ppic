<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notfound extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->_init();
	}

	private function _init() {
		// $this->load->section('ace_setting', 'templates/ace_setting');

		/* $this->load->js('assets/themes/default/js/jquery-1.9.1.min.js');
		$this->load->js('assets/themes/default/hero_files/bootstrap-transition.js');
		$this->load->js('assets/themes/default/hero_files/bootstrap-collapse.js'); */
	}

	public function index() {
		parent::administrator();
		$this->load->view('page/errors/404');
	}
}