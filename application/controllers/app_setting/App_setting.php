<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class App_setting extends MY_Controller {
	private $class_link = 'app_setting/app_setting';
	private $form_errs = array('idErr');

	public function __construct() {
		parent::__construct();

		$this->load->helper(array('my_helper', 'form', 'html'));
		$this->load->model(array('m_builder', 'tb_setting'));
		$this->load->library(array('upload'));
	}

	public function index() {
		parent::administrator();
		$this->get_form();
	}

	public function get_form() {
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = $this->form_errs;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function open_form() {
		$data['result'] = $this->tb_setting->get_all();
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function send_data() {
		if ($this->input->is_ajax_request()) :
			/*
			** Cek file upload, apakah ada file yang diupload atau tidak
			** jika tidak maka bisa langsung update data kedalam database
			** jika ada file upload maka ambil data file upload atau munculkan error jika gagal upload file
			*/
			$alert = '';
			$confirm = 'success';
			$data['upload_files'] = $this->multiple_upload();
			if (!empty($data['upload_files'])) :
				for ($i = 0; $i < count($data['upload_files']); $i++) :
					if (isset($data['upload_files'][$i]['error']) || array_key_exists('error', $data['upload_files'][$i])) :
						$alert .= $data['upload_files'][$i]['error']; 
						$confirm = 'error';
					else :
						$data_file['id'][] = $data['upload_files'][$i]['id'];
						$data_file['val_setting'][$data['upload_files'][$i]['id']] = $data['upload_files'][$i]['file_name'];
					endif;
					$str['alert'] = buildAlert('danger', 'Gagal!', $alert);
				endfor;
			endif;
			if ($confirm == 'success') :
				$data_set['id'] = $this->input->post('txtKdSetting');
				$data_set['val_setting'] = $this->input->post('txtAppSetting');
				$str = $this->tb_setting->submit_data($data_set);
				if (!empty($data['upload_files'])) :
					$str = $this->tb_setting->submit_data($data_file);
				endif;
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function multiple_upload() {
		$jml_upload = count($_FILES['userfile']['name']);
		$str = '';
		for ($i = 0; $i < $jml_upload; $i++) :
			if (!empty($_FILES['userfile']['name'][$i])) :
				$_FILES['upl_file']['name'] = $_FILES['userfile']['name'][$i];
				$_FILES['upl_file']['type'] = $_FILES['userfile']['type'][$i];
				$_FILES['upl_file']['tmp_name'] = $_FILES['userfile']['tmp_name'][$i];
				$_FILES['upl_file']['error'] = $_FILES['userfile']['error'][$i];
				$_FILES['upl_file']['size'] = $_FILES['userfile']['size'][$i];

				$config = array(
					'encrypt_name' => TRUE,
					'allowed_types' => 'jpg|jpeg|png|gif',
					'max_size' => 10000,
					'upload_path' => './assets/admin_assets/dist/img/setting_img'
				);

				$this->upload->initialize($config);
				if (!$this->upload->do_upload('upl_file')) :
					$str[] = array('error' => $this->upload->display_errors(), 'file_name' => '', 'id' => $_POST['txtKdFile'][$i]);
				else :
					$str[] = array_merge($this->upload->data(), array('id' => $_POST['txtKdFile'][$i]));
				endif;
			endif;
		endfor;
		return $str;
	}
}