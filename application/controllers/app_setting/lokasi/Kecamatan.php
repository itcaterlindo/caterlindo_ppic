<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Kecamatan extends MY_Controller {
	private $class_link = 'app_setting/lokasi/kecamatan';
	private $form_errs = array('idErrNm');

	public function __construct() {
		parent::__construct();

		$this->load->helper(array('my_helper', 'my_btn_access_helper', 'form'));
		$this->load->model(array('lokasi/tb_kecamatan'));
		$this->load->library(array('form_validation', 'ssp'));
	}

	public function index() {
		parent::administrator();
		$this->get_table();
	}

	public function get_table() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function open_table() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		$data = $this->tb_kecamatan->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function get_form() {
		$data['id'] = $this->input->get('id');
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = $this->form_errs;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function open_form() {
		$id = $this->input->get('id');
		$data = $this->tb_kecamatan->get_data($id);
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function send_data() {
		if ($this->input->is_ajax_request()) :
			$this->form_validation->set_rules($this->tb_kecamatan->form_rules());
			if ($this->form_validation->run() == FALSE) :
				$str = $this->tb_kecamatan->form_warning($this->form_errs);
				$str['confirm'] = 'error';
			else :
				$data['negara_kd'] = $_SESSION['modul_lokasi']['kd_negara'];
				$data['provinsi_kd'] = $_SESSION['modul_lokasi']['kd_provinsi'];
				$data['kota_kd'] = $_SESSION['modul_lokasi']['kd_kota'];
				$data['kd_kecamatan'] = $this->input->post('txtKd');
				$data['nm_kecamatan'] = $this->input->post('txtNm');
				$str = $this->tb_kecamatan->submit_data($data);
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function delete_data() {
		if ($this->input->is_ajax_request()) :
			$id = $this->input->get('id');
			$str = $this->tb_kecamatan->delete_data($id);
			
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}