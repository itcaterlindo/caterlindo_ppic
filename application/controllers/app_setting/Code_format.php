<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Code_format extends MY_Controller {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'tb_code_format';
	private $p_key = 'id';
	private $class_link = 'app_setting/code_format';
	private $title = 'Format Code';

	function __construct() {
		parent::__construct();
		
		$this->load->model(array('m_builder', 'm_code_format'));
		$this->load->helper(array('form', 'html', 'my_helper'));
	}

	function index() {
		parent::administrator();
		$page = $this->define_page();
		$for = str_replace(' ', '', $page);
		$data['box_id'] = 'idBoxFormat';
		$data['box_status'] = 'primary';
		$data['box_title'] = 'Format Laporan '.ucwords($page);
		$data['btns'] = array('btn_hide' => 'idBtnHideBox');
		$data['box_body'] = 'page/'.$this->class_link.'/view';
		$data['idbox_overlay'] = 'idBoxOverlayForm';
		$data['idbox_loader'] = 'idBoxLoader';
		$data['idbox_content'] = 'idBoxContent';
		$data['tbl_alertid'] = 'idErrForm';
		$data['form_data'] = $this->get_data($for);

		$this->load->view('containers/box', $data);
		$this->load->section('scriptJS', 'script/scriptJS', $data);
	}

	function define_page() {
		$segs = $this->uri->segment_array();
		$title = str_replace('_', ' ', $this->uri->segment(2));
		return $title;
	}

	function type_list($var) {
		$list = array('project', 'quotation', 'salesorder');
		if (in_array($var, $list)) :
			return TRUE;
		else :
			return FALSE;
		endif;
	}

	function get_data($var) {
		$row = $this->m_code_format->read_data($var);
		if ($row) :
			$data = array('code_length' => $row->code_length, 'code_format' => $row->code_format, 'code_for' => $row->code_for, 'code_separator' => $row->code_separator, 'on_reset' => $row->on_reset, 'contoh_code' => code_maker($row->code_format, $row->code_length, '-', $row->code_separator, '0', '0'), 'code_form' => $this->code_form($row->code_format));
		else :
			$data = array('code_length' => '', 'code_format' => '', 'code_for' => $var, 'code_separator' => '', 'on_reset' => '', 'contoh_code' => '', 'code_form' => $this->code_form(''));
		endif;
		$data['class_link'] = $this->class_link;

		return $data;
	}

	function get_form() {
		$page = $this->input->get('code_for');
		$file = $this->input->get('file_name');
		$for = str_replace(' ', '', $page);
		$data['box_id'] = 'idBoxFormat';
		if ($file == 'form') :
			$data['box_status'] = 'info';
			$data['box_title'] = 'Ubah Format Laporan '.ucwords($page);
		elseif ($file == 'view') :
			$data['box_status'] = 'primary';
			$data['box_title'] = 'Format Laporan '.ucwords($page);
		endif;
		$data['btns'] = array('btn_hide' => 'idBtnHideBox');
		$data['box_body'] = 'page/'.$this->class_link.'/'.$file;
		$data['idbox_overlay'] = 'idBoxOverlayForm';
		$data['idbox_loader'] = 'idBoxLoader';
		$data['idbox_content'] = 'idBoxContent';
		$data['tbl_alertid'] = 'idErrForm';
		$data['form_data'] = $this->get_data($for);

		$this->load->view('containers/box', $data);
	}

	function code_form($code_format) {
		$dropdown = '';
		if (!empty($code_format)) :
			$pecah_format = explode('-', $code_format);
			for ($i=0; $i < count($pecah_format); $i++) :
				$type = $i > 0?'form_anak':'form_inti';
				$val = $pecah_format[$i];
				if (stripos($val, 'urutan_angka') !== FALSE) :
					$jml = strlen('urutan_angka');
					$kata = substr($val, 0, $jml);
					$nilai = substr($val, $jml);
					$nilai = substr($nilai, 1);
					$nilai = substr($nilai, 0, -1);
				elseif (stripos($val, 'kode_huruf') !== FALSE) :
					$jml = strlen('kode_huruf');
					$kata = substr($val, 0, $jml);
					$nilai = substr($val, $jml);
					$nilai = substr($nilai, 1);
					$nilai = substr($nilai, 0, -1);
				else :
					$kata = $val;
					$nilai = '';
				endif;

				$dropdown .= $this->structure_dropdown($pecah_format[$i], $type);
			endfor;
		else :
			$dropdown .= $this->structure_dropdown('', 'form_inti');
		endif;
		return $dropdown;
	}

	function structure_dropdown($val, $type){
		$selected = $val == 'val'?'':$val;

		if ($val != 'val') :
			$kata = '';
			$nilai = '';
			if (stripos($val, 'urutan_angka') !== FALSE) :
				$jml = strlen('urutan_angka');
				$selected = substr($val, 0, $jml);
				$nilai = substr($val, $jml);
				$nilai = substr($nilai, 1);
				$nilai = substr($nilai, 0, -1);
			elseif (stripos($val, 'kode_huruf') !== FALSE) :
				$jml = strlen('kode_huruf');
				$selected = substr($val, 0, $jml);
				$nilai = substr($val, $jml);
				$nilai = substr($nilai, 1);
				$nilai = substr($nilai, 0, -1);
			endif;
		endif;

		$this->db->where(array('jenis_select' => 'code_format'));
		$this->db->from('tb_set_dropdown');
		$query = $this->db->get();
		$result = $query->result();
		$pil_format = array('' => '-- Pilih Format --');
		foreach ($result as $row) :
			$nm_select = $this->security->xss_clean($row->nm_select);
			$val_select = str_replace(' ', '_', $nm_select);
			$pil_format[$val_select] = $nm_select;
		endforeach;
		$sel_format = $selected;
		$attr_format = array('id' => 'idSelFormat', 'class' => 'form-control idSelFormat', 'style' => 'width:100%;');
		$form_child = '';
		$text_child = '';

		if ($val != 'val') :
			if ($selected == 'urutan_angka') :
				$panjang = strlen($nilai);
				$text_child .= '<div class="col-md-1">';
				$text_child .= form_input(array('name' => 'txtTextLength['.$selected.']', 'class' => 'form-control', 'value' => $panjang, 'placeholder' => 'Panjang'));
				$text_child .= '</div>';
				$text_child .= '<div class="col-md-1">';
				$text_child .= form_input(array('name' => 'txtTextStart['.$selected.']', 'class' => 'form-control', 'value' => $nilai, 'placeholder' => 'Mulai'));
				$text_child .= '</div>';
			elseif ($selected == 'kode_huruf') :
				$text_child .= '<div class="col-md-2">';
				$text_child .= form_input(array('name' => 'txtTextFormat['.$selected.']', 'class' => 'form-control', 'value' => $nilai, 'placeholder' => 'Format Huruf'));
				$text_child .= '</div>';
			endif;
		endif;

		$form_child .= '<div class="form-group">';
		$form_child .= '<label for="idSelFormat" class="col-md-2 control-label">Pilih Format</label>';
		$form_child .= '<div class="col-md-3">';
		$form_child .= form_dropdown('selFormat[]', $pil_format, $sel_format, $attr_format);
		$form_child .= '</div>';
		$form_child .= '<div class="text_format">'.$text_child.'</div>';
		if ($type == 'form_anak') :
			$form_child .= '<div class="col-md-1">';
			$form_child .= '<a href="javascript:void(0);" class="btn btn-sm btn-danger idBtnHapus" title="Hapus"><i class="fa fa-trash"></i></a>';
			$form_child .= '</div>';
		elseif ($type == 'form_inti') :
			$form_child .= '<div class="col-md-1">';
			$form_child .= '<a href="javascript:void(0);" class="btn btn-sm btn-success idBtnAdd" title="Tambah"><i class="fa fa-plus"></i></a>';
			$form_child .= '</div>';
		endif;
		$form_child .= '</div>';

		if ($val == 'val') :
			$str['text_format'] = $form_child;
			header('Content-Type: application/json');
			echo json_encode($str);
		else :
			return $form_child;
		endif;
	}

	function text_format($nilai_format, $val_format){
		$nilai_format = $this->input->get('nilai_format');
		$val_format = $this->input->get('val_format');

		$form_child = '';
		if ($nilai_format == 'urutan_angka' || $nilai_format == 'kode_huruf') :
			if ($nilai_format == 'urutan_angka') :
				$form_child .= '<div class="col-md-1">';
				$form_child .= form_input(array('name' => 'txtTextLength['.$nilai_format.']', 'class' => 'form-control', 'value' => $val_format, 'placeholder' => 'Panjang'));
				$form_child .= '</div>';
				$form_child .= '<div class="col-md-1">';
				$form_child .= form_input(array('name' => 'txtTextStart['.$nilai_format.']', 'class' => 'form-control', 'value' => $val_format, 'placeholder' => 'Mulai'));
				$form_child .= '</div>';
			elseif ($nilai_format == 'kode_huruf') :
				$form_child .= '<div class="col-md-2">';
				$form_child .= form_input(array('name' => 'txtTextFormat['.$nilai_format.']', 'class' => 'form-control', 'value' => $val_format, 'placeholder' => 'Format Huruf'));
				$form_child .= '</div>';
			endif;

		endif;

		if ($this->input->is_ajax_request()) :
			$str['text_format'] = $form_child;
			header('Content-Type: application/json');
			echo json_encode($str);
		else :
			return $form_child;
		endif;
	}

	function submit_form(){
		if ($this->input->is_ajax_request()) :
			$this->load->library(array('form_validation'));
			$str['csrf'] = $this->security->get_csrf_hash();

			$str['code_for'] = $this->input->post('txtCodeFor');
			$str['sel_format'] = $this->input->post('selFormat');
			$str['txt_length'] = $this->input->post('txtTextLength');
			$str['txt_start'] = $this->input->post('txtTextStart');
			$str['txt_format'] = $this->input->post('txtTextFormat');

			// Cek Code For
			$code_separator = $this->input->post('selSeparator');
			$on_reset = $this->input->post('selResetter');
			$code_for = $this->input->post('txtCodeFor');
			$sel_format = $this->input->post('selFormat');
			$txt_length = $this->input->post('txtTextLength');
			$txt_start = $this->input->post('txtTextStart');
			$txt_format = $this->input->post('txtTextFormat');
			$check = $this->m_builder->getRow($this->tbl_name, array('code_for' => $code_for), 'num_rows');

			$this->form_validation->set_rules('selSeparator', 'Code Separator', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selResetter', 'Code Reset', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'error';
				$str['idErrSeparator'] = (!empty(form_error('selSeparator')))?buildLabel('warning', form_error('selSeparator', '"', '"')):'';
				$str['idErrResetter'] = (!empty(form_error('selResetter')))?buildLabel('warning', form_error('selResetter', '"', '"')):'';
			else :
				// Hitung berapa select yang digunakan
				$err = 0;
				$success = 0;
				$no = 0;
				$proto_code = '';
				$code_format = '';
				foreach ($sel_format as $format) :
					$no++;
					$hubung = $no == count($sel_format)?'':'-';
					if ($format == 'urutan_angka') :
						foreach ($txt_length as $key => $length) :
							$panjang = strlen($txt_start[$key]);
							($panjang > $length || $panjang < $length)?$err++:$success++;
						endforeach;
						if ($err < 1) :
							foreach ($txt_start as $key => $start) :
								$proto_code .= $start;
								$code_format .= 'urutan_angka['.$start.']'.$hubung;
							endforeach;
						endif;
					elseif ($format == 'kode_huruf') :
						foreach ($txt_format as $key => $huruf) :
							$proto_code .= $huruf;
							$code_format .= 'kode_huruf['.$huruf.']'.$hubung;
						endforeach;
					else :
						$proto_code .= $format;
						$code_format .= $format.$hubung;
					endif;
				endforeach;

				$code_length = strlen($proto_code);
				if ($err < 1) :
					if ($check > 0) :
						$data = array(
							'code_length' => $code_length,
							'code_format' => $code_format,
							'code_separator' => $code_separator,
							'on_reset' => $on_reset,
							'tgl_edit' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin'),
						);
						$where = array('code_for' => $code_for);
						$aksi = $this->db->update($this->tbl_name, $data, $where);
						$label_err = 'mengubah';
					else :
						$data = array(
							'code_length' => $code_length,
							'code_format' => $code_format,
							'code_separator' => $code_separator,
							'on_reset' => $on_reset,
							'code_for' => $code_for,
							'tgl_input' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin'),
						);
						$aksi = $this->db->insert($this->tbl_name, $data);
						$label_err = 'menambahkan';
					endif;

					if ($aksi) :
						$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' '.$code_for.' dengan code_length \''.$code_length.'\' code_format \''.$code_format.'\' code_for \''.$code_for.'\'');
						$str['confirm'] = 'success';
						$str['alert'] = buildLabel('success', 'Berhasil!', 'Berhasil '.$label_err.' data category barang!');
					else :
						$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' '.$this->title.' '.$code_for.' dengan code_length \''.$code_length.'\' code_format \''.$code_format.'\' code_for \''.$code_for.'\'');
						$str['confirm'] = 'error';
						$str['idErrForm'] = buildLabel('danger', 'Gagal!', 'Gagal '.$label_err.' data '.$this->title.' '.$code_for.', Kesalahan sistem!');
					endif;
				else :
					$str['confirm'] = 'error';
					$str['idErrForm'] = buildLabel('danger', 'Gagal!', 'Gagal '.$label_err.' data '.$this->title.' '.$code_for.', Kesalahan pada pengisian form!');
				endif;
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}