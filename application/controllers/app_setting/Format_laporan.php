<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Format_laporan extends MY_Controller {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'tb_format_laporan';
	private $p_key = 'id';
	private $class_link = 'app_setting/format_laporan';
	private $title = 'Format Laporan';

	function __construct() {
		parent::__construct();
		
		$this->load->helper(array('form', 'html', 'my_helper'));
		$this->load->library(array('form_validation'));
		$this->load->model(array('m_format_laporan', 'm_builder'));
	}

	function index() {
		$this->load_page();
	}

	function load_page() {
		parent::administrator();

		$page = $this->define_page();
		$for = str_replace(' ', '', $page);
		$data['box_id'] = 'idBoxFormat';
		$data['box_status'] = 'primary';
		$data['box_title'] = 'Format Laporan '.ucwords($page);
		$data['btns'] = array('btn_hide' => 'idBtnHideBox');
		$data['box_body'] = 'page/'.$this->class_link.'/form';
		$data['idbox_overlay'] = 'idBoxOverlayForm';
		$data['idbox_loader'] = 'idBoxLoader';
		$data['idbox_content'] = 'idBoxContent';
		$data['tbl_alertid'] = 'idErrForm';

		$data['class_link'] = $this->class_link;
		$data['form_data'] = $this->get_data($for);
		$data['js_data'] = array('form_error' => array('idErrProductCode', 'idErrLaporanFooter'), 'box_id' => 'idBoxFormat');
		$data['adds_js'] = array('form_js');
		
		$this->load->js('assets/admin_assets/plugins/ckeditor/ckeditor.js');
		$this->load->view('containers/box', $data);
		$this->load->section('scriptJS', 'script/scriptJS', $data);
	}

	function define_page() {
		$segs = $this->uri->segment_array();
		$title = str_replace('_', ' ', $this->uri->segment(2));
		return $title;
	}

	function get_data($var) {
		$row = $this->m_format_laporan->read_data($var);
		if ($row) :
			$data = array('page_title' => $var, 'no_form' => $row->no_form, 'laporan_title' => $row->laporan_title, 'laporan_footer' => $row->laporan_footer);
		else :
			$data = array('page_title' => $var, 'no_form' => '', 'laporan_title' => '', 'laporan_footer' => '');
		endif;

		return $data;
	}

	function form_submit() {
		if ($this->input->is_ajax_request()) :
			$label_err = 'mengubah';

			$data['no_form'] = $this->input->post('txtNoForm');
			// Laporan title formatnya berubah ketika di update lewat program, BUG ON PROGRESS
			// $data['laporan_title'] = $this->input->post('txtLaporanTitle');
			$data['laporan_footer'] = $this->input->post('txtLaporanFooter');
			$data['laporan_for'] = str_replace(' ', '', $this->input->post('txtLaporanFor'));
			$var = $this->check_submit($data['laporan_for']);

			if ($this->laporan_type($data['laporan_for'])) :
				$str = $this->laporan_submit($var, $data);
			else :
				$log_stat = 'gagal';
				$str['csrf'] = $this->security->get_csrf_hash();
				$str['confirm'] = 'errValidation';
				$str['alert'] = buildAlert('danger', 'Gagal!', 'Gagal '.$label_err.' '.$this->title.', format laporan tidak sesuai!');
				$this->write_log($log_stat, $var, $data);
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function laporan_type($type) {
		$list_type = array('quotation', 'salesorderlocal', 'salesorderekspor', 'localproductionorder', 'eksporproductionorder', 'invoicelocal', 'invoiceekspor');
		if (in_array($type, $list_type)) :
			return TRUE;
		else :
			return FALSE;
		endif;
	}

	function check_submit($var){
		$var = $this->m_format_laporan->check_laporan($var)?'mengubah':'menambahkan';
		return $var;
	}

	function laporan_submit($var, $data = array()) {
		$str['csrf'] = $this->security->get_csrf_hash();
		// Submit data to table, with 'input' or 'edit' variable
		$act = $this->m_format_laporan->submit_laporan($var, $data);
		if ($act) :
			$log_stat = 'berhasil';
			$str['confirm'] = 'success';
			$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$var.' '.$this->title.'!');
		else :
			$log_stat = 'gagal';
			$str['confirm'] = 'errValidation';
			$str['alert'] = buildAlert('danger', 'Gagal!', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
		endif;
		$this->write_log($log_stat, $var, $data);

		return $str;
	}

	function write_log($stat, $var, $data = array()){
		$this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' '.$stat.' '.$var.' '.$this->title.' dengan no_form \''.$data['no_form'].'\' laporan_title \''.$data['laporan_title'].'\' laporan_footer \''.$data['laporan_footer'].'\' laporan_for \''.$data['laporan_for'].'\'');
	}
}