<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Default_setting extends MY_Controller {
    private $class_link = 'app_setting/default_setting';
    private $title = 'Set PPN';

	function __construct() {
		parent::__construct();

		$this->load->model(array('m_app_setting', 'm_builder'));
		$this->load->helper(array('form', 'html', 'my_helper'));
		$this->load->library(array('form_validation'));
	}

	public function index() {
		parent::administrator();
		$data = $this->get_prop();
		$data['class_link'] = $this->class_link;

		$this->load->view('page/'.$this->class_link.'/form', $data);
	}

	private function get_prop() {
		$tbls = $this->m_app_setting->get_all();
		foreach ($tbls as $row) :
			$data['form_error'][] = 'idErr'.$row->nm_setting;
		endforeach;
		$data['app_forms'] = $tbls;
		return $data;
	}

	public function submit_form() {
		if ($this->input->is_ajax_request()) :
			$this->form_validation->set_rules('txtSettingVal[]', 'Nilai Setting', 'trim|required', array('required' => '{field} Tidak Boleh Kosong!'));
			$this->form_validation->set_rules('selTipeHarga[]', 'Tipe Harga', 'trim|required', array('required' => '{field} Tidak Boleh Kosong!'));
			if ($this->form_validation->run() == FALSE) :
				$data['confirm'] = 'error';
				$data['idErrForm'] = buildAlert('danger', 'Gagal!', 'Form harus diisi sesuai format!');
			else :
				$data_array = array();
				for ($i=0;$i<count($this->input->post('txtSettingId'));$i++) :
					$data_array[] = array('id' => $this->input->post('txtSettingId')[$i], 'nilai_setting' => $this->input->post('txtSettingVal')[$i], 'tipe_harga' => $this->input->post('selTipeHarga')[$i]);
				endfor;
				$data = $this->m_app_setting->submit_data($data_array);
			endif;
			$data['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($data);
		else :
			echo '==HACKED --- YOUR BRAINS ,.||., ==';
		endif;
	}

	public function submit_form_lama() {
		if ($this->input->is_ajax_request()) :
			$str['csrf'] = $this->security->get_csrf_hash();

			$nm_setting = $this->input->post('txtSettingVar');
			$nilai_setting = $this->input->post('txtSettingVal');

			$this->form_validation->set_rules('txtSettingVal', 'Jumlah PPn', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'errValidation';
				$str['idErrSettingVal'] = (!empty(form_error('txtSettingVal')))?buildLabel('warning', form_error('txtSettingVal', '"', '"')):'';
			else :
				$data = array('nilai_setting' => $nilai_setting);
				$where = array('nm_setting' => $nm_setting);
				$aksi = $this->m_app_setting->update_data($data, $where);
				$label_err = 'mengubah';

				if ($aksi) :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' dengan nm_setting \''.$nm_setting.'\' nilai_setting \''.$nilai_setting.'\'');
					$str['confirm'] = 'success';
					$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' '.$this->title.'!');
				else :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' '.$this->title.' dengan nm_setting \''.$nm_setting.'\' nilai_setting \''.$nilai_setting.'\'');
					$str['confirm'] = 'errValidation';
					$str['idErrForm'] = buildAlert('danger', 'Gagal '.$label_err.' '.$this->title.', Kesalahan sistem!');
				endif;
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}