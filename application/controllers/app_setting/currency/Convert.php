<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Convert extends MY_Controller {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'td_currency_convert';
	private $p_key = 'kd_currency_convert';
	private $class_link = 'app_setting/currency/convert';
	private $title = 'Currency Convert';

	function __construct() {
		parent::__construct();
		
		$this->load->library(array('ssp', 'form_validation'));
		$this->load->model(array('m_builder', 'td_currency_convert', 'tm_currency'));
		$this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
	}

	function index() {
		parent::administrator();
		$this->load->js('assets/admin_assets/plugins/moment.js/moment.min.js');
		$this->load->js('assets/admin_assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js');
		$this->load->css('assets/admin_assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css');
		$data['class_link'] = $this->class_link;
		$data['form_data'] = array('form_error' => array('idErrForm', 'idErrPrimary', 'idErrSecondary', 'idErrConvert', 'idErrTgl'));
		$this->load->view('page/'.$this->class_link.'/box_table', $data);
	}

	function open_table() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table', $data);
	}

	function get_data() {
		$data = $this->td_currency_convert->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	function open_form() {
		$primary_key = $this->input->get('primary_code');
		$row = $this->td_currency_convert->get_data($primary_key);
		$primary = $this->tm_currency->get_primary();
		if (!empty($row)) :
			$value = array($this->p_key => $row->kd_currency_convert, 'primary_name' => $primary->currency_nm, 'secondary_kd' => $row->secondary_kd, 'convert_value' => $row->convert_value, 'tgl_berlaku' => format_date($row->tgl_berlaku, 'd-m-Y H:i:s'), 'class' => 'box-warning');
		else :
			$value = array($this->p_key => '', 'primary_name' => $primary->currency_nm, 'secondary_kd' => '', 'convert_value' => '', 'tgl_berlaku' => '', 'class' => 'box-info');
		endif;
		$data = $value;
		$this->load->view('page/'.$this->class_link.'/form', $data);
	}

	function submit_form() {
		if ($this->input->is_ajax_request()) :
			$this->form_validation->set_rules('selSecondary', 'Secondary Currency', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtConvert', 'Convert Value', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtTgl', 'Waktu Berlaku', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'error';
				$str['idErrSecondary'] = (!empty(form_error('selSecondary')))?buildLabel('warning', form_error('selSecondary', '"', '"')):'';
				$str['idErrConvert'] = (!empty(form_error('txtConvert')))?buildLabel('warning', form_error('txtConvert', '"', '"')):'';
				$str['idErrTgl'] = (!empty(form_error('txtTgl')))?buildLabel('warning', form_error('txtTgl', '"', '"')):'';
			else :
				$primary = $this->tm_currency->get_primary();
				$data[$this->p_key] = $this->input->post('txtKd');
				$data['primary_kd'] = $primary->kd_currency;
				$data['secondary_kd'] = $this->input->post('selSecondary');
				$data['convert_value'] = $this->input->post('txtConvert');
				$data['tgl_berlaku'] = format_date($this->input->post('txtTgl'), 'Y-m-d H:i:s');
				$str = $this->td_currency_convert->send_data($data);
			endif;

			$str['csrf'] = $this->security->get_csrf_hash();
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function delete_data() {
		if ($this->input->is_ajax_request()) :
			$data[$this->p_key] = $this->input->get('kode');
			$data['row_flag'] = 'inactive';
			$str = $this->td_currency_convert->send_data($data);
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}