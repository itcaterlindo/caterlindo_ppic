<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Currency_list extends MY_Controller {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'tb_icon_currency';
	private $p_key = 'id';
	private $class_link = 'app_setting/currency/currency_list';
	private $title = 'Currency List';

	function __construct() {
		parent::__construct();
		
		$this->load->library(array('ssp', 'form_validation'));
		$this->load->model(array('m_builder', 'tb_icon_currency'));
		$this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
	}

	function index() {
		parent::administrator();
		$data['class_link'] = $this->class_link;
		$data['form_data'] = array('form_error' => array('idErrForm', 'idErrNama', 'idErrIcon', 'idErrUnicode'));
		$this->load->view('page/'.$this->class_link.'/box_table', $data);
	}

	function open_table() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table', $data);
	}

	function get_data() {
		$data = $this->tb_icon_currency->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	function open_form() {
		$primary_key = $this->input->get('primary_code');
		$row = $this->tb_icon_currency->get_data($primary_key);
		if (!empty($row)) :
			$value = array('id' => $row->id, 'nm_currency' => $row->nm_currency, 'icon_currency' => $row->icon_currency, 'icon_unicode' => $row->icon_unicode, 'class' => 'box-warning');
		else :
			$value = array('id' => '', 'nm_currency' => '', 'icon_currency' => '', 'icon_unicode' => '', 'class' => 'box-info');
		endif;
		$data = $value;
		$this->load->view('page/'.$this->class_link.'/form', $data);
	}

	function submit_form() {
		if ($this->input->is_ajax_request()) :
			$this->form_validation->set_rules('txtNama', 'Currency Name', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtIcon', 'Icon Currency', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtUnicode', 'Icon Unicode', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'error';
				$str['idErrNama'] = (!empty(form_error('txtNama')))?buildLabel('warning', form_error('txtNama', '"', '"')):'';
				$str['idErrIcon'] = (!empty(form_error('txtIcon')))?buildLabel('warning', form_error('txtIcon', '"', '"')):'';
				$str['idErrUnicode'] = (!empty(form_error('txtUnicode')))?buildLabel('warning', form_error('txtUnicode', '"', '"')):'';
			else :
				$data['id'] = $this->input->post('txtKd');
				$data['nm_currency'] = $this->input->post('txtNama');
				$data['icon_currency'] = $this->input->post('txtIcon');
				$data['icon_unicode'] = $this->input->post('txtUnicode');
				$str = $this->tb_icon_currency->send_data($data);
			endif;

			$str['csrf'] = $this->security->get_csrf_hash();
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function delete_data() {
		if ($this->input->is_ajax_request()) :
			$data['id'] = $this->input->get('kode');
			$data['row_flag'] = 'inactive';
			$str = $this->tb_icon_currency->send_data($data);
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}