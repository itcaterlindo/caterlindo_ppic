<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Set_currency extends MY_Controller {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'tm_currency';
	private $p_key = 'kd_currency_convert';
	private $class_link = 'app_setting/currency/set_currency';
	private $title = 'Set Currency';

	function __construct() {
		parent::__construct();
		
		$this->load->library(array('ssp', 'form_validation'));
		$this->load->model(array('m_builder', 'tm_currency', 'tb_icon_currency'));
		$this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
	}

	function index() {
		parent::administrator();
		$data['class_link'] = $this->class_link;
		$data['form_data'] = array('form_error' => array('idErrForm', 'idErrNama', 'idErrType', 'idErrTipe', 'idErrPemisah', 'idErrFormatAkhir', 'idErrDecimal'));
		$this->load->view('page/'.$this->class_link.'/box_table', $data);
	}

	function open_table() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table', $data);
	}

	function get_data() {
		$data = $this->tm_currency->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	function open_form() {
		$primary_key = $this->input->get('primary_code');
		$row = $this->tm_currency->get_data($primary_key);
		if (!empty($row)) :
			$value = array('kd_currency' => $row->kd_currency, 'currency_nm' => $row->currency_nm, 'currency_type' => $row->currency_type, 'icon_type' => $row->icon_type, 'currency_icon' => $this->tm_currency->form_simbol($row->icon_type, $row->currency_icon), 'pemisah_angka' => $row->pemisah_angka, 'format_akhir' => $row->format_akhir, 'decimal' => $row->decimal, 'class' => 'box-warning');
		else :
			$value = array('kd_currency' => '', 'currency_nm' => '', 'currency_type' => '', 'icon_type' => '', 'currency_icon' => $this->tm_currency->form_simbol('', ''), 'pemisah_angka' => '', 'format_akhir' => '', 'decimal' => '', 'class' => 'box-info');
		endif;
		$data = $value;
		$this->load->view('page/'.$this->class_link.'/form', $data);
	}

	function form_simbol($tipe, $icon) {
		if ($this->input->is_ajax_request()) :
			echo $this->tm_currency->form_simbol($this->input->get('tipe'), $this->input->get('icon'));
		endif;
	}

	function check_type() {
		if ($this->input->is_ajax_request()) :
			$type = $this->input->get('type');
			$str = $this->tm_currency->check_type($type);

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function submit_form() {
		if ($this->input->is_ajax_request()) :
			$this->form_validation->set_rules('txtNama', 'Currency Name', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selType', 'Currency Type', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selTipe', 'Icon Type', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtPemisah', 'Pemisah Angka', 'required|trim',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtFormatAkhir', 'Format Akhir', 'required|trim',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selDecimal', 'Pilihan Decimal', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'error';
				$str['idErrNama'] = (!empty(form_error('txtNama')))?buildLabel('warning', form_error('txtNama', '"', '"')):'';
				$str['idErrType'] = (!empty(form_error('selType')))?buildLabel('warning', form_error('selType', '"', '"')):'';
				$str['idErrTipe'] = (!empty(form_error('selTipe')))?buildLabel('warning', form_error('selTipe', '"', '"')):'';
				$str['idErrPemisah'] = (!empty(form_error('txtPemisah')))?buildLabel('warning', form_error('txtPemisah', '"', '"')):'';
				$str['idErrFormatAkhir'] = (!empty(form_error('txtFormatAkhir')))?buildLabel('warning', form_error('txtFormatAkhir', '"', '"')):'';
				$str['idErrDecimal'] = (!empty(form_error('selDecimal')))?buildLabel('warning', form_error('selDecimal', '"', '"')):'';
			else :
				$data['kd_currency'] = $this->input->post('txtKd');
				$data['currency_nm'] = $this->input->post('txtNama');
				$data['currency_type'] = $this->input->post('selType');
				$data['icon_type'] = $this->input->post('selTipe');
				$data['currency_icon'] = $this->input->post('txtSimbol');
				$data['pemisah_angka'] = $this->input->post('txtPemisah');
				$data['format_akhir'] = $this->input->post('txtFormatAkhir');
				$data['decimal'] = $this->input->post('selDecimal');
				$str = $this->tm_currency->send_data($data);
			endif;

			$str['csrf'] = $this->security->get_csrf_hash();
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function delete_data() {
		if ($this->input->is_ajax_request()) :
			$data['kd_currency'] = $this->input->get('kode');
			$data['currency_flag'] = 'inactive';
			$str = $this->tm_currency->send_data($data);
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}