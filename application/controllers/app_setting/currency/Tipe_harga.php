<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Tipe_harga extends MY_Controller {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'tb_tipe_harga';
	private $p_key = 'nm_harga';
	private $class_link = 'app_setting/currency/tipe_harga';
	private $title = 'Tipe Harga';

	function __construct() {
		parent::__construct();
		
		$this->load->library(array('form_validation', 'table'));
		$this->load->model(array('m_builder', 'tb_tipe_harga', 'tm_currency'));
		$this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
	}

	function index() {
		parent::administrator();
		$data['class_link'] = $this->class_link;
		$data['page_title'] = $this->title;
		$data['form_data'] = array('form_error' => array('idErrTipe', 'idErrCurrency', 'idErrFlag'));
		$this->load->view('page/'.$this->class_link.'/box_table', $data);
	}

	function open_table() {
		$data['class_link'] = $this->class_link;
		$data['table_view'] = $this->tb_tipe_harga->get_table();
		$this->load->view('page/'.$this->class_link.'/table', $data);
	}

	function open_form() {
		$primary_key = $this->input->get('primary_code');
		$row = $this->tb_tipe_harga->get_data($primary_key);
		if (!empty($row)) :
			$value = array($this->p_key => $row->nm_harga, 'currency_kd' => $row->currency_kd, 'row_flag' => $row->row_flag, 'class' => 'box-warning');
		else :
			$value = array($this->p_key => '', 'currency_kd' => '', 'row_flag' => $row->row_flag, 'class' => 'box-info');
		endif;
		$this->load->view('page/'.$this->class_link.'/form', $value);
	}

	function submit_form() {
		if ($this->input->is_ajax_request()) :
			$this->form_validation->set_rules('txtNmTipe', 'Nama Tipe Harga', 'required|trim',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selCurrency', 'Nama Currency', 'required|trim',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selFlag', 'Flag', 'required|trim',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'error';
				$str['idErrTipe'] = (!empty(form_error('txtNmTipe')))?buildLabel('warning', form_error('txtNmTipe', '"', '"')):'';
				$str['idErrCurrency'] = (!empty(form_error('selCurrency')))?buildLabel('warning', form_error('selCurrency', '"', '"')):'';
				$str['idErrFlag'] = (!empty(form_error('selFlag')))?buildLabel('warning', form_error('selFlag', '"', '"')):'';
			else :
				$data[$this->p_key] = $this->input->post('txtNmTipe');
				$data['currency_kd'] = $this->input->post('selCurrency');
				$data['row_flag'] = $this->input->post('selFlag');
				$str = $this->tb_tipe_harga->send_data($data);
			endif;

			$str['csrf'] = $this->security->get_csrf_hash();
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function set_flag() {
		if ($this->input->is_ajax_request()) :
			$data[$this->p_key] = $this->input->get('kode');
			$data['row_flag'] = $this->input->get('flag');
			$str = $this->tb_tipe_harga->send_data($data);
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}