<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Workflow extends MY_Controller {
	private $class_link = 'app_setting/workflow';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
        $this->load->model(['tm_workflow', 'td_workflow_state', 'td_workflow_transition', 'td_workflow_transition_notification', 'td_workflow_transition_permission']);
	}

	public function index() {
        parent::administrator();
		parent::pnotify_assets();
		$this->workflow_box();
	}
	
	public function workflow_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/workflow_box', $data);
	}

	public function workflowtable_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;

		$this->load->view('page/'.$this->class_link.'/workflowtable_main', $data);
	}

	public function workflowtable_data (){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->tm_workflow->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function workflowform_main (){
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $id = $this->input->get('id', true);

        if (!empty($id)){
            $data['row'] = $this->tm_workflow->get_by_param(['wf_kd' => $id])->row_array();
        }

        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/workflowform_main', $data);
    }

	public function state(){
		parent::administrator();
		parent::pnotify_assets();
		parent::colorpicker_assets();

		$wf_kd = $this->input->get('wf_kd');
		$data['class_link'] = $this->class_link;
		$data['wf'] = $this->tm_workflow->get_by_param(['wf_kd' => $wf_kd])->row_array();
		$data['wf_kd'] = $wf_kd;
		$this->load->view('page/'.$this->class_link.'/state_box', $data);
	}

	public function statetable_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$wf_kd = $this->input->get('wf_kd');
		$data['class_link'] = $this->class_link;
		$data['wf_kd'] = $wf_kd;

		$this->load->view('page/'.$this->class_link.'/statetable_main', $data);
	}

	public function statetable_data () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
	
		$this->load->library(['ssp']);
		$wf_kd = $this->input->get('wf_kd');

		$data = $this->td_workflow_state->ssp_table($wf_kd);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function stateform_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', true);
		$wf_kd = $this->input->get('wf_kd', true);

        if (!empty($id)){
            $data['row'] = $this->td_workflow_state->get_by_param(['wfstate_kd' => $id])->row_array();
        }
        $data['id'] = $id;
        $data['wf_kd'] = $wf_kd;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/stateform_main', $data);
	}

	public function action_submit_state() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtwfstate_nama', 'State', 'required', ['required' => '{field} tidak boleh kosong!']);		

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrwfstate_nama' => (!empty(form_error('txtwfstate_nama')))?buildLabel('warning', form_error('txtwfstate_nama', '"', '"')):'',
			);
			
		}else {
			$wfstate_kd = $this->input->post('txtwfstate_kd', true);
			$wf_kd = $this->input->post('txtwf_kd', true);
			$wfstate_nama = $this->input->post('txtwfstate_nama', true);
			$wfstate_initial = $this->input->post('txtwfstate_initial', true);
			$wfstate_spancolor = $this->input->post('txtwfstate_spancolor', true);
			$wfstate_active = $this->input->post('txtwfstate_active', true);
			$wfstate_onprintout = $this->input->post('txtwfstate_onprintout', true);
			$wfstate_onprintoutseq = $this->input->post('txtwfstate_onprintoutseq', true);
			$wfstate_action = $this->input->post('txtwfstate_action', true);
			$wfstate_badgecolor = $this->input->post('txtwfstate_badgecolor', true);

			$data = [
                'wf_kd' => $wf_kd,
				'wfstate_nama' => $wfstate_nama,
				'wfstate_initial' => $wfstate_initial,
				'wfstate_spancolor' => !empty($wfstate_spancolor) ? $wfstate_spancolor : null,
				'wfstate_badgecolor' => $wfstate_badgecolor,
				'wfstate_active' => $wfstate_active,
				'wfstate_onprintout' => $wfstate_onprintout,
				'wfstate_onprintoutseq' => !empty($wfstate_onprintoutseq) ? $wfstate_onprintoutseq : null,
				'wfstate_action' => $wfstate_action,
				'wfstate_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
            ];
            $act = false;
            if (empty($wfstate_kd)){
                #add
                $data = array_merge($data,
                    [
                        'wfstate_kd' => $this->td_workflow_state->create_code(),
                        'wfstate_tglinput' => date('Y-m-d H:i:s'),
                    ]);
                $act = $this->td_workflow_state->insert_data($data);
            }else{
                #update
                $act = $this->td_workflow_state->update_data (['wfstate_kd' => $wfstate_kd], $data);
				$data = array_merge($data, ['wfstate_kd' => $wfstate_kd]);
            }

            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => $data);
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete_state() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		if (!empty($id)){
			$actDel = $this->td_workflow_state->delete_data($id);
			if ($actDel == true){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
			}
		}
		
		echo json_encode($resp);
	}

	public function transition(){
		parent::administrator();
		parent::select2_assets();
		parent::pnotify_assets();

		$wf_kd = $this->input->get('wf_kd');
		$data['class_link'] = $this->class_link;
		$data['wf'] = $this->tm_workflow->get_by_param(['wf_kd' => $wf_kd])->row_array();
		$data['wf_kd'] = $wf_kd;
		$this->load->view('page/'.$this->class_link.'/transition_box', $data);
	}

	public function transitiontable_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$wf_kd = $this->input->get('wf_kd');
		$data['class_link'] = $this->class_link;
		$data['wf_kd'] = $wf_kd;

		$this->load->view('page/'.$this->class_link.'/transitiontable_main', $data);
	}

	public function transitiontable_data () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
	
		$this->load->library(['ssp']);
		$wf_kd = $this->input->get('wf_kd');

		$data = $this->td_workflow_transition->ssp_table($wf_kd);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function transitionform_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', true);
		$wf_kd = $this->input->get('wf_kd', true);

        if (!empty($id)){
            $data['row'] = $this->db->join('td_workflow_transition_notification', 'td_workflow_transition.wftransition_kd=td_workflow_transition_notification.wftransition_kd', 'left')
					->where('td_workflow_transition.wftransition_kd', $id)
					->get('td_workflow_transition')->row_array();
        }

		 /** State */
		 $states = $this->td_workflow_state->get_by_param(['wf_kd' => $wf_kd])->result_array();
		 $opsiState[''] = '-- Pilih Opsi --';
		 foreach ($states as $each):
			 $opsiState[$each['wfstate_kd']] = $each['wfstate_nama'];
		 endforeach;
		
        $data['id'] = $id;
        $data['wf_kd'] = $wf_kd;
        $data['opsiState'] = $opsiState;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/transitionform_main', $data);
	}

	public function permissionform_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', true);
		$wf_kd = $this->input->get('wf_kd', true);

		 /** User */
		 $users = $this->db->where('admin_stsaktif', 1)->order_by('nm_admin')->get('tb_admin')->result_array();
		 $opsiUser[''] = '-- Pilih Opsi --';
		 foreach ($users as $each):
			 $opsiUser[$each['kd_admin']] = $each['nm_admin'];
		 endforeach;

		 /** Tipe User */
		 $usertypes = $this->db->order_by('nm_tipe_admin')->get('td_admin_tipe')->result_array();
		 $opsiUsertype[''] = '-- Pilih Opsi --';
		 foreach ($usertypes as $each):
			 $opsiUsertype[$each['kd_tipe_admin']] = $each['nm_tipe_admin'];
		 endforeach;
		
        $data['id'] = $id;
        $data['wf_kd'] = $wf_kd;
        $data['opsiUser'] = $opsiUser;
        $data['opsiUsertype'] = $opsiUsertype;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/permissionform_main', $data);
	}

	public function permissiontable_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', true);

		$permissions = $this->db->where('td_workflow_transition_permission.wftransition_kd', $id)
				->join('tb_admin', 'td_workflow_transition_permission.wftransitionpermission_adminkd=tb_admin.kd_admin', 'left')
				->join('td_admin_tipe', 'td_workflow_transition_permission.kd_tipe_admin=td_admin_tipe.kd_tipe_admin', 'left')
				->order_by('td_workflow_transition_permission.wftransitionpermission_tgledit', 'desc')
				->get('td_workflow_transition_permission')->result_array();
		$data['result'] = $permissions;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/permissiontable_main', $data);
	}

	public function action_submit_transition() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtwftransition_nama', 'Transition', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtwftransition_source', 'Dari', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtwftransition_destination', 'Ke', 'required', ['required' => '{field} tidak boleh kosong!']);		

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrwftransition_nama' => (!empty(form_error('txtwftransition_nama')))?buildLabel('warning', form_error('txtwftransition_nama', '"', '"')):'',
				'idErrwftransition_source' => (!empty(form_error('txtwftransition_source')))?buildLabel('warning', form_error('txtwftransition_source', '"', '"')):'',
				'idErrwftransition_destination' => (!empty(form_error('txtwftransition_destination')))?buildLabel('warning', form_error('txtwftransition_destination', '"', '"')):'',
			);
			
		}else {
			$wftransition_kd = $this->input->post('txtwftransition_kd', true);
			$wf_kd = $this->input->post('txtwf_kd', true);
			$wftransition_nama = $this->input->post('txtwftransition_nama', true);
			$wftransition_source = $this->input->post('txtwftransition_source', true);
			$wftransition_destination = $this->input->post('txtwftransition_destination', true);
			$wftransition_email = $this->input->post('txtwftransition_email', true);
			$wftransitionnotification_to = $this->input->post('txtwftransitionnotification_to', true);
			$wftransitionnotification_cc = $this->input->post('txtwftransitionnotification_cc', true);
			$wftransition_active = $this->input->post('txtwftransition_active', true);
			$wftransition_action = $this->input->post('txtwftransition_action', true);

			$data = [
                'wf_kd' => $wf_kd,
				'wftransition_nama' => $wftransition_nama,
				'wftransition_source' => $wftransition_source,
				'wftransition_destination' => $wftransition_destination,
				'wftransition_email' => $wftransition_email,
				'wftransition_action' => !empty($wftransition_action) ? $wftransition_action : null,
				'wftransition_active' => $wftransition_active,
				'wftransition_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
            ];
            $act = false;
            if (empty($wftransition_kd)){
                #add
				$wftransition_kd = $this->td_workflow_transition->create_code();
                $data = array_merge($data,
                    [
                        'wftransition_kd' => $wftransition_kd,
                        'wftransition_tglinput' => date('Y-m-d H:i:s'),
                    ]);
                $act = $this->td_workflow_transition->insert_data($data);		
            }else{
                #update
                $act = $this->td_workflow_transition->update_data (['wftransition_kd' => $wftransition_kd], $data);
				$data = array_merge($data, ['wftransition_kd' => $wftransition_kd]);
            }

			#transition notification
			if (!empty($wftransition_email)){
				$actDelNotif = $this->td_workflow_transition_notification->delete_by_param(['wftransition_kd' => $wftransition_kd]);
				$dataNotif = [
					'wftransitionnotification_kd' => $this->td_workflow_transition_notification->create_code(),
					'wftransition_kd' => $wftransition_kd,
					'wftransitionnotification_to' => $wftransitionnotification_to,
					'wftransitionnotification_cc' => $wftransitionnotification_cc,
					'wftransitionnotification_tglinput' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				];
				$actNotif = $this->td_workflow_transition_notification->insert_data($dataNotif);
			}		

            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => $data);
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete_transition() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		if (!empty($id)){
			$actDel = $this->td_workflow_transition->delete_data($id);
			if ($actDel == true){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
			}
		}
		
		echo json_encode($resp);
	}

	function action_submit_permission () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$wftransition_kd = $this->input->get('wftransition_kd', true);
		$wftransitionpermission_adminkd = $this->input->get('wftransitionpermission_adminkd', true);
		$kd_tipe_admin = $this->input->get('kd_tipe_admin', true);

		if (!empty($wftransition_kd) && (!empty($wftransitionpermission_adminkd) || !empty($kd_tipe_admin) )){
			$data = [
				'wftransitionpermission_kd' => $this->td_workflow_transition_permission->create_code(),
				'wftransition_kd' => $wftransition_kd,
				'wftransitionpermission_adminkd' => $wftransitionpermission_adminkd,
				'kd_tipe_admin' => $kd_tipe_admin,
				'wftransitionpermission_tglinput' => date('Y-m-d H:i:s'),
				'wftransitionpermission_tgledit' => date('Y-m-d H:i:s'),
			];
			$act = $this->td_workflow_transition_permission->insert_data($data);
			if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => $data);
			}else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Tidak Sesuai');
		}
		echo json_encode($resp);
	}

	function action_delete_permission () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		if (!empty($id)){
			$actDel = $this->td_workflow_transition_permission->delete_data($id);
			if ($actDel == true){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
			}
		}
		
		echo json_encode($resp);
	}	
}
