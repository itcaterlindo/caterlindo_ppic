<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Delivery_note extends MY_Controller
{
    private $class_link = 'production/delivery_note';
    private $wf_kd = 4;

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('ssp', 'form_validation'));
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
        $this->load->model(array(
            'tm_deliverynote', 'tb_bagian', 'td_deliverynote_detail', 'tb_deliverynote_log', 'tb_deliverynote_state',
            'td_deliverynote_received', 'tm_wip', 'td_bom_detail', 'tb_deliverynote_user', 'td_workorder_item'
        ));
    }

    public function index()
    {
        parent::administrator();
        parent::datetimepicker_assets();
        parent::select2_assets();
        parent::pnotify_assets();
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function open_table()
    {
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }

    public function table_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data = $this->tm_deliverynote->ssp_table2();

        echo json_encode($data);
    }

    public function open_form_box()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $slug = $this->input->get('slug');
        $id = $this->input->get('id');

        $data['class_link'] = $this->class_link;
        $data['slug'] = $slug;
        $data['id'] = $id;
        $this->load->view('page/' . $this->class_link . '/form_box', $data);
    }

    public function form_master()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $slug = $this->input->get('slug', true);
        $id = $this->input->get('id', true);

        if ($slug == 'edit') :
            $data['rowData'] = $this->tm_deliverynote->get_by_param(['dn_kd' => $id])->row_array();
        endif;

        $kd_admin = $this->session->userdata('kd_admin');
        $tipe_admin_kd = $this->session->userdata('tipe_admin_kd');
        $qBagians = $this->tb_deliverynote_user->get_bagianAllowed($kd_admin, $tipe_admin_kd);
        $bagians = $this->tb_bagian->get_by_param_in('bagian_kd', $qBagians)->result();
        $opsiBagianAsal[null] = '-- Pilih Opsi --';
        foreach ($bagians as $bagian) :
            $opsiBagianAsal[$bagian->bagian_kd] = $bagian->bagian_kd . ' | ' . $bagian->bagian_nama;
        endforeach;

        /** Opsi Bagian Tujuan*/
        $bagianTujuans = $this->tb_bagian->get_by_param_in('bagian_lokasi', ['WIP', 'WH'])->result();
        $opsiBagianTujuan[null] = '-- Pilih Opsi --';
        foreach ($bagianTujuans as $bagianTujuan) :
            if($data['rowData']['dn_asal'] != $bagianTujuan->bagian_kd){
                $opsiBagianTujuan[$bagianTujuan->bagian_kd] = $bagianTujuan->bagian_kd . ' | ' . $bagianTujuan->bagian_nama;
            }
        endforeach;

        $data['class_link'] = $this->class_link;
        $data['slug'] = $slug;
        $data['id'] = $id;
        $data['opsiBagianAsal'] = $opsiBagianAsal;
        $data['opsiBagianTujuan'] = $opsiBagianTujuan;
        $this->load->view('page/' . $this->class_link . '/form_master', $data);
    }

    public function form_main_box()
    {
        $dn_kd = $this->input->get('dn_kd');
        parent::administrator();
        parent::select2_assets();
        parent::pnotify_assets();

        $data['class_link'] = $this->class_link;
        $data['id'] = $dn_kd;
        $this->load->view('page/' . $this->class_link . '/form_main_box', $data);
    }

    public function form_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $dn_kd = $this->input->get('id', true);

        $data['class_link'] = $this->class_link;
        $data['master'] = $this->tm_deliverynote->get_by_param_detail(['tm_deliverynote.dn_kd' => $dn_kd])->row_array();
        $data['id'] = $dn_kd;
        $this->load->view('page/' . $this->class_link . '/form_main', $data);
    }

    public function form_item()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $slug = $this->input->get('slug', true);
        $dn_asal = $this->input->get('dn_asal', true);
        $dn_tujuan = $this->input->get('dn_tujuan', true);
        $dn_kd = $this->input->get('dn_kd', true);
        $dndetail_kd = $this->input->get('dndetail_kd', true);

        if ($slug == 'add_detail') {
            $qDndetail = $this->db->select()
                ->from('td_deliverynote_detail')
                ->join('td_workorder_item', 'td_deliverynote_detail.woitem_kd=td_workorder_item.woitem_kd', 'left')
                ->where('td_deliverynote_detail.dndetail_kd', $dndetail_kd)
                ->get()->row_array();

            $data['woitem_itemcodeHeader'] = $qDndetail['woitem_itemcode'];
        }

        $data['slug'] = $slug;
        $data['dn_asal'] = $dn_asal;
        $data['dn_tujuan'] = $dn_tujuan;
        $data['dn_kd'] = $dn_kd;
        $data['dndetail_kd'] = $dndetail_kd;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_item', $data);
    }

    public function form_item_table()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $dn_kd = $this->input->get('dn_kd', true);

        $qData = $this->db->select()
            ->from('td_deliverynote_detail')
            ->join('td_workorder_item', 'td_deliverynote_detail.woitem_kd=td_workorder_item.woitem_kd', 'left')
            ->where('td_deliverynote_detail.dn_kd', $dn_kd)
            ->get()->result_array();
        $result = [];
        $resultDetail = [];
        foreach ($qData as $eData) {
            if (empty($eData['dndetail_parent'])) {
                $result[] = $eData;
            } else {
                $resultDetail[] = $eData;
            }
        }

        $data['class_link'] = $this->class_link;
        $data['dn_kd'] = $dn_kd;
        $data['result'] = $result;
        $data['resultDetail'] = $resultDetail;
        $this->load->view('page/' . $this->class_link . '/form_item_table', $data);
    }

    public function view_box()
    {
        parent::administrator();
        parent::select2_assets();
        parent::pnotify_assets();

        $dn_kd = $this->input->get('dn_kd');

        $kd_admin = $this->session->userdata('kd_admin');
        $tipe_admin_kd = $this->session->userdata('tipe_admin_kd');

        $data['master'] = $this->tm_deliverynote->get_by_param_detail(['tm_deliverynote.dn_kd' => $dn_kd])->row_array();
        $data['masterStatus'] = build_span($data['master']['dnstate_span'], $data['master']['dnstate_nama']);
        $data['class_link'] = $this->class_link;
        $data['id'] = $dn_kd;

        $data['permission']['permission_sendItem'] = $this->tb_deliverynote_user->cek_permission($data['master']['dn_asal'], $kd_admin, $tipe_admin_kd);
        $data['permission']['permission_rcvItem'] = $this->tb_deliverynote_user->cek_permission($data['master']['dn_tujuan'], $kd_admin, $tipe_admin_kd);
        
        /** Tombol aprrove dan not approve hanya muncul/bisa diakses oleh hak akses SPV PRODUKSI */
        if($this->session->tipe_admin == "Admin" || $this->session->tipe_admin == "Supervisor"){
            $data['permission']['permission_approveItem'] = true;
        }else{
            $data['permission']['permission_approveItem'] = false;
        }
        $this->load->view('page/' . $this->class_link . '/view_box', $data);
    }

    public function view_table_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $dn_kd = $this->input->get('id');

        $data['class_link'] = $this->class_link;
        $data['dn_kd'] = $dn_kd;
        $qResult = $this->db->select()
            ->from('td_deliverynote_detail')
            ->join('td_workorder_item', 'td_deliverynote_detail.woitem_kd=td_workorder_item.woitem_kd', 'left')
            ->where('td_deliverynote_detail.dn_kd', $dn_kd)
            ->get()->result_array();
        $qReceived = $this->td_deliverynote_received->get_by_param(['dn_kd' => $dn_kd])->result_array();

        $result = [];
        $resultDetail = [];
        foreach ($qResult as $eData) {
            if (empty($eData['dndetail_parent'])) {
                $result[] = $eData;
            } else {
                $resultDetail[] = $eData;
            }
        }
        $data['result'] = $result;
        $data['resultDetail'] = $resultDetail;
        $data['resultReceived'] = $qReceived;

        $this->load->view('page/' . $this->class_link . '/view_table_main', $data);
    }

    public function view_formstate_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $dn_kd = $this->input->get('dn_kd', true);
        $dnstate_kd = $this->input->get('dnstate_kd', true);

        /** Opsi State */
        $rState = $this->tb_deliverynote_state->get_all()->result();
        $opsiState[null] = '-- Pilih Opsi --';
        foreach ($rState as $r) :
            $opsiState[$r->dnstate_kd] = ucwords($r->dnstate_nama);
        endforeach;

        $data['class_link'] = $this->class_link;
        $data['id'] = $dn_kd;
        $data['opsiState'] = $opsiState;
        $data['dnstate_kd'] = $dnstate_kd;
        $this->load->view('page/' . $this->class_link . '/view_formstate_main', $data);
    }

    public function terima_box()
    {
        parent::administrator();
        parent::datetimepicker_assets();
        parent::select2_assets();
        parent::icheck_assets();
        parent::pnotify_assets();

        $dn_kd = $this->input->get('dn_kd');

        $data['master'] = $this->tm_deliverynote->get_by_param_detail(['tm_deliverynote.dn_kd' => $dn_kd])->row_array();
        /** Check Authorization */
        $kd_admin = $this->session->userdata('kd_admin');
        $tipe_admin_kd = $this->session->userdata('tipe_admin_kd');
        if (
            $data['master']['dnstate_kd'] != 5 ||
            $this->tb_deliverynote_user->cek_permission($data['master']['dn_tujuan'], $kd_admin, $tipe_admin_kd) == false
        ) {
            show_error('Anda tidak diperbolehkan melakukan transaksi ini <br> <a href="' . base_url() . $this->class_link . '"> Delivery Note </a>', '202', 'Unauthorize');
        }

        $data['masterStatus'] = build_span($data['master']['dnstate_span'], $data['master']['dnstate_nama']);
        $data['class_link'] = $this->class_link;
        $data['id'] = $dn_kd;

        /** Permissino check */
        $kd_admin = $this->session->userdata('kd_admin');
        $tipe_admin_kd = $this->session->userdata('tipe_admin_kd');
        $permission_rcvItem = $this->tb_deliverynote_user->cek_permission($data['master']['dn_tujuan'], $kd_admin, $tipe_admin_kd);
        if (!$permission_rcvItem) {
            show_error("Anda tidak memiliki akses ! <a href='" . base_url() . "'> Home </a>", '403', 'Unauthorized User');
        }

        $this->load->view('page/' . $this->class_link . '/terima_box', $data);
    }

    public function terima_table()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $dn_kd = $this->input->get('id');

        /** Qty Sent dikurangi Qty yang sudah received */
        $qDetail = $this->db->select()
            ->from('td_deliverynote_detail')
            ->join('td_workorder_item', 'td_deliverynote_detail.woitem_kd=td_workorder_item.woitem_kd', 'left')
            ->where('td_deliverynote_detail.dn_kd', $dn_kd)
            ->get()->result_array();
        $qReceived = $this->db->select('dndetail_kd, SUM(dndetailreceived_qty) as sumQtyReceived')
            ->from('td_deliverynote_received')
            ->where('dn_kd', $dn_kd)
            ->group_by('dndetail_kd')
            ->get()->result_array();

        $arrResult = [];
        foreach ($qDetail as $eDetail) {
            $dndetail_qty = $eDetail['dndetail_qty'];
            foreach ($qReceived as $eReceived) {
                if ($eReceived['dndetail_kd'] == $eDetail['dndetail_kd']) {
                    $dndetail_qty = $dndetail_qty - $eReceived['sumQtyReceived'];
                }
            }
            $arrResult[] = [
                'dndetail_kd' => $eDetail['dndetail_kd'],
                'woitem_kd' => $eDetail['woitem_kd'],
                'dn_kd' => $eDetail['dn_kd'],
                'woitem_no_wo' => $eDetail['woitem_no_wo'],
                'kd_barang' => $eDetail['kd_barang'],
                'woitem_itemcode' => $eDetail['woitem_itemcode'],
                'woitem_jenis' => $eDetail['woitem_jenis'],
                'dndetail_remark' => $eDetail['dndetail_remark'],
                'dndetail_parent' => $eDetail['dndetail_parent'],
                'dndetail_qty' => $dndetail_qty,
            ];
        }

        $resultDetail = [];
        foreach ($arrResult as $eData) {
            if (empty($eData['dndetail_parent'])) {
                $result[] = $eData;
            } else {
                $resultDetail[] = $eData;
            }
        }
        $data['master'] = $this->tm_deliverynote->get_by_param_detail(['tm_deliverynote.dn_kd' => $dn_kd])->row_array();
        $data['result'] = $result;
        $data['resultDetail'] = $resultDetail;
        $data['id'] = $dn_kd;

        $this->load->view('page/' . $this->class_link . '/terima_table', $data);
    }

    public function viewlog_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $id = $this->input->get('id', true);

        if (!empty($id)) {
            $resultData = $this->tb_deliverynote_log->get_by_param_detail(['tb_deliverynote_log.dn_kd' => $id]);

            $data['id'] = $id;
            $data['resultData'] = $resultData->result_array();

            $this->load->view('page/' . $this->class_link . '/viewlog_main', $data);
        }
    }

    // public function get_woitem()
    // {
    //     // if (!$this->input->is_ajax_request()) {
    //     //     exit('No direct script access allowed');
    //     // }
    //     $dn_asal = $this->input->get('dn_asal', true);
    //     $dn_tujuan = $this->input->get('dn_tujuan', true);
    //     $dndetail_kd = $this->input->get('dndetail_kd', true);
    //     $paramWO = $this->input->get('paramWO', true);
    //     /** param search */

    //     $result = [];
    //     $qSent = $this->db->select('td_deliverynote_detail.woitem_kd, SUM(td_deliverynote_detail.dndetail_qty) as dndetail_qty')
    //         ->from('td_deliverynote_detail')
    //         ->join('tm_deliverynote', 'td_deliverynote_detail.dn_kd=tm_deliverynote.dn_kd', 'left')
    //         ->where('tm_deliverynote.dn_asal', $dn_asal)
    //         ->group_by('td_deliverynote_detail.woitem_kd')
    //         ->get()->result_array();
        
    //     /** QUERY UNTUK CHECK DAN GET DN YANG RETURN */
    //     $qReturn = $this->db->select('td_deliverynote_received.woitem_kd, SUM(td_deliverynote_received.dndetailreceived_qty) as dndetailreceived_qty, tm_deliverynote.flag_return')
    //         ->from('td_deliverynote_received')
    //         ->join('tm_deliverynote', 'td_deliverynote_received.dn_kd=tm_deliverynote.dn_kd', 'left')
    //         ->where('tm_deliverynote.flag_return', 1)
    //         ->group_by('td_deliverynote_received.woitem_kd')
    //         ->get()->result_array();

    //     if (empty($dndetail_kd)) {
    //         $qData = $this->db->select()
    //             ->from('td_workorder_item')
    //             ->join('td_workorder_item_detail', 'td_workorder_item.woitem_kd=td_workorder_item_detail.woitem_kd', 'left')
    //             // ->where('woitem_jenis !=', 'packing')
    //             ->where('woitem_prosesstatus', 'workorder')
    //             ->group_by('td_workorder_item.woitem_kd')
    //             ->get()->result_array();

    //         /** Packing ke finished goods atau ke WH RM */
    //         if (($dn_asal == '60' && $dn_tujuan == '70') || ($dn_asal == '60' && $dn_tujuan == '80')) {
    //             $qData = $this->db->select()
    //                 ->from('td_workorder_item')
    //                 ->join('td_workorder_item_detail', 'td_workorder_item.woitem_kd=td_workorder_item_detail.woitem_kd', 'left')
    //                 ->where('woitem_jenis', 'packing')
    //                 ->or_where('woitem_jenis', 'project')
    //                 ->where('woitem_prosesstatus', 'workorder')
    //                 ->group_by('td_workorder_item.woitem_kd')
    //                 ->get()->result_array();
    //         }

    //         if (!empty($qSent)) {
    //             foreach ($qData as $eData) {
    //                 $qtySisa = $eData['woitem_qty'];
    //                 foreach ($qSent as $eSent) {
    //                     if ($eSent['woitem_kd'] == $eData['woitem_kd']) {
    //                         $qtySisa = $qtySisa - $eSent['dndetail_qty'];
    //                     }
    //                 }

    //                 /** Cek jika ada return */
    //                 if(!empty($qReturn)){
    //                     foreach ($qReturn as $eReturn){
    //                         if ($eReturn['woitem_kd'] == $eData['woitem_kd']) {
    //                             $qtySisa = $qtySisa + $eReturn['dndetailreceived_qty'];
    //                         }
    //                     }    
    //                 }

    //                 if (empty($qtySisa)) {
    //                     continue;
    //                 }
    //                 $result[] = [
    //                     'woitem_kd' => $eData['woitem_kd'],
    //                     'woitem_itemcode' => $eData['woitem_itemcode'],
    //                     'woitem_deskripsi' => $eData['woitem_deskripsi'],
    //                     'woitem_dimensi' => $eData['woitem_dimensi'],
    //                     'woitem_no_wo' => $eData['woitem_no_wo'],
    //                     'woitem_qty' => $qtySisa > $eData['woitem_qty'] ? $eData['woitem_qty'] : $qtySisa,
    //                     'woitem_jenis' => $eData['woitem_jenis'],
    //                     'kd_barang' => $eData['kd_barang'],
    //                 ];
    //             }
    //         } else {
    //             $result = $qData;
    //         }
    //     } else {
    //         $rowResult = $this->db->select()
    //             ->from('td_deliverynote_detail')
    //             ->join('td_workorder_item', 'td_deliverynote_detail.woitem_kd=td_workorder_item.woitem_kd', 'left')
    //             ->join('td_workorder_item_detail', 'td_workorder_item.woitem_kd=td_workorder_item_detail.woitem_kd', 'left')
    //             ->where('td_deliverynote_detail.dndetail_kd', $dndetail_kd)
    //             ->get()->row_array();
    //         $qBom = $this->db->select()
    //             ->from('td_workorder_item_detail')
    //             ->where('td_workorder_item_detail.woitemdetail_parent', $rowResult['woitemdetail_kd'])
    //             ->get()->result_array();
    //         $arrPart = array_column($qBom, 'part_kd');

    //         $qData = $this->db->select('td_workorder_item.*, td_workorder_item_detail.kd_barang')
    //             ->from('td_workorder_item')
    //             ->join('td_workorder_item_detail', 'td_workorder_item.woitem_kd=td_workorder_item_detail.woitem_kd', 'left')
    //             ->where_in('td_workorder_item_detail.part_kd', $arrPart)
    //             ->where('td_workorder_item.woitem_jenis != ', 'packing')
    //             ->where('td_workorder_item.woitem_prosesstatus', 'workorder')
    //             ->group_by('td_workorder_item.woitem_kd')
    //             ->get()->result_array();
    //         if (!empty($qSent)) {
    //             foreach ($qData as $eData2) {
    //                 $qtySisa = $eData2['woitem_qty'];
    //                 foreach ($qSent as $eSent2) {
    //                     if ($eSent2['woitem_kd'] == $eData2['woitem_kd']) {
    //                         $qtySisa = $qtySisa - $eSent2['dndetail_qty'];
    //                     }
    //                 }
    //                 if (empty($qtySisa)) {
    //                     continue;
    //                 }
    //                 $result[] = [
    //                     'woitem_kd' => $eData2['woitem_kd'],
    //                     'woitem_itemcode' => $eData2['woitem_itemcode'],
    //                     'woitem_deskripsi' => $eData2['woitem_deskripsi'],
    //                     'woitem_dimensi' => $eData2['woitem_dimensi'],
    //                     'woitem_no_wo' => $eData2['woitem_no_wo'],
    //                     'woitem_qty' => $qtySisa > $eData2['woitem_qty'] ? $eData2['woitem_qty'] : $qtySisa,
    //                     'woitem_jenis' => $eData2['woitem_jenis'],
    //                     'kd_barang' => $eData2['kd_barang'],
    //                 ];
    //             }
    //         } else {
    //             $result = $qData;
    //         }
    //     }

    //     /** Sesuaikan dengan select2 */
    //     $arrResult = [];
    //     $paramWO = strtolower($paramWO);
    //     foreach ($result as $e) {
    //         $text = "{$e['woitem_no_wo']} | {$e['woitem_itemcode']} / {$e['woitem_deskripsi']} {$e['woitem_dimensi']}";
    //         /** Cek array search with param from input select2 params */
    //         if (!empty($paramWO) && strpos(strtolower($text), $paramWO) === false) {
    //             continue;
    //         }
    //         $e['id'] = $e['woitem_kd'];
    //         $e['text'] = $text;
    //         $arrResult[] = $e;
    //     }

    //     echo json_encode($arrResult);
    // }

    public function get_woitem()
    {
        // if (!$this->input->is_ajax_request()) {
        //     exit('No direct script access allowed');
        // }
        $dn_asal = $this->input->get('dn_asal', true);
        $dn_tujuan = $this->input->get('dn_tujuan', true);
        $dndetail_kd = $this->input->get('dndetail_kd', true);
        $paramWO = $this->input->get('paramWO', true);
        /** param search */
        $result = [];

        /** Jika DN asal packing get data wo packing saja */
        if($dn_asal == '60'){
            $qSent = $this->db->select('td_deliverynote_detail.woitem_kd, SUM(td_deliverynote_detail.dndetail_qty) as dndetail_qty')
                ->from('td_deliverynote_detail')
                ->join('tm_deliverynote', 'td_deliverynote_detail.dn_kd=tm_deliverynote.dn_kd', 'left')
                ->join('td_workorder_item', 'td_workorder_item.woitem_kd=td_deliverynote_detail.woitem_kd', 'left')
                ->where('tm_deliverynote.dn_asal', $dn_asal)
                ->where('td_workorder_item.woitem_prosesstatus', 'workorder')
                //->where('td_workorder_item.woitem_jenis', 'packing')
                ->group_by('td_deliverynote_detail.woitem_kd')
                ->get()->result_array();
        }else{
                $qSent = $this->db->select('td_deliverynote_detail.woitem_kd, SUM(td_deliverynote_detail.dndetail_qty) as dndetail_qty')  
                  ->from('td_deliverynote_detail')  
                  ->join('tm_deliverynote', 'td_deliverynote_detail.dn_kd = tm_deliverynote.dn_kd', 'left')  
                  ->join('td_workorder_item', 'td_workorder_item.woitem_kd = td_deliverynote_detail.woitem_kd', 'left')  
                  ->where('tm_deliverynote.dn_asal', $dn_asal) // Ganti '50' dengan nilai yang sesuai jika diperlukan  
                  //->where('tm_deliverynote.dnstate_kd', '3')  
                  ->where('td_workorder_item.woitem_prosesstatus', 'workorder')  
                  ->group_by('td_deliverynote_detail.woitem_kd')  
                  ->get()  
                  ->result_array();  
        }
        
        /** QUERY UNTUK CHECK DAN GET DN YANG RETURN */
        $qReturn = $this->db->select('td_deliverynote_received.woitem_kd, SUM(td_deliverynote_received.dndetailreceived_qty) as dndetailreceived_qty, tm_deliverynote.flag_return')
            ->from('td_deliverynote_received')
            ->join('tm_deliverynote', 'td_deliverynote_received.dn_kd=tm_deliverynote.dn_kd', 'left')
            ->where('tm_deliverynote.flag_return', 1)
            ->group_by('td_deliverynote_received.woitem_kd')
            ->get()->result_array();

        if (empty($dndetail_kd)) {
            
            /** Jika DN asal packing get data wo packing saja */
            if($dn_asal == '60'){   
                $qData = $this->db->select('td_workorder_item.woitem_kd as prim_key, td_workorder_item.*, td_workorder_item_detail.kd_barang as kd_barang_key, td_workorder_item_detail.*')  
                    ->from('td_workorder_item')
                    ->join('td_workorder_item_detail', 'td_workorder_item.woitem_kd=td_workorder_item_detail.woitem_kd', 'left')
                    ->where('td_workorder_item.woitem_prosesstatus', 'workorder')
                    ->where('td_workorder_item.woitem_jenis', 'packing')
                    ->group_by('td_workorder_item.woitem_kd')
                    ->get()->result_array();
            }else{
                if($dn_asal == '10' || $dn_asal == '90' || $dn_asal == '81'){   
                    $qData = $this->db->select('td_workorder_item.woitem_kd as prim_key, td_workorder_item.*, td_workorder_item_detail.kd_barang as kd_barang_key, td_workorder_item_detail.*')  
                    ->from('td_workorder_item')
                    ->join('td_workorder_item_detail', 'td_workorder_item.woitem_kd=td_workorder_item_detail.woitem_kd', 'left')
                    ->where('td_workorder_item.woitem_prosesstatus', 'workorder')
                    ->group_by('td_workorder_item.woitem_kd')
                    ->get()->result_array();
                }else{
                    $x1 = $this->db->select('td_workorder_item.woitem_kd as prim_key, td_workorder_item_detail.kd_barang as kd_barang_key, td_workorder_item.*, td_workorder_item_detail.*, td_deliverynote_detail.*, tm_barang.*, tm_deliverynote.*')  
                        ->from('td_workorder_item')  
                        ->join('td_workorder_item_detail', 'td_workorder_item.woitem_kd = td_workorder_item_detail.woitem_kd', 'left')  
                        ->join('td_deliverynote_detail', 'td_workorder_item.woitem_kd = td_deliverynote_detail.woitem_kd', 'left')
                        ->join('tm_barang', 'tm_barang.kd_barang = td_workorder_item_detail.kd_barang', 'left')    
                        ->join('tm_deliverynote', 'td_deliverynote_detail.dn_kd = tm_deliverynote.dn_kd', 'left')  
                        ->where_in('tm_deliverynote.dnstate_kd', ['2', '3', '5'])
                        ->where('td_workorder_item.woitem_prosesstatus', 'workorder')  
                        ->where('tm_deliverynote.dn_tujuan', $dn_asal)// Ganti '50' dengan nilai yang sesuai jika diperlukan 
                        ->group_by('td_workorder_item.woitem_kd')  
                        ->get()  
                        ->result_array(); 

                    $x2 = $this->db->select('td_workorder_item.woitem_kd as prim_key, td_workorder_item_detail.kd_barang as kd_barang_key, td_workorder_item.*, td_workorder_item_detail.*, td_deliverynote_detail.*, tm_barang.*, tm_deliverynote.*')  
                    ->from('td_workorder_item')  
                    ->join('td_workorder_item_detail', 'td_workorder_item.woitem_kd = td_workorder_item_detail.woitem_kd', 'left')  
                    ->join('td_deliverynote_detail', 'td_workorder_item.woitem_kd = td_deliverynote_detail.woitem_kd', 'left')  
                    ->join('tm_barang', 'tm_barang.kd_barang = td_workorder_item_detail.kd_barang', 'left')  
                    ->join('tm_deliverynote', 'td_deliverynote_detail.dn_kd = tm_deliverynote.dn_kd', 'left')  
                    ->where_in('tm_barang.kat_barang_kd', ['KTB020817000046', 'KTB020817000047', 'KTB250324000001'])  
                    ->where('td_workorder_item.woitem_prosesstatus', 'workorder')  
                    ->group_by('td_workorder_item.woitem_kd')  
                    ->get()  
                    ->result_array();  

                        $qData = array_merge($x1, $x2);  
                }
            }

            /** Packing ke finished goods atau ke WH RM */
            if (($dn_asal == '60' && $dn_tujuan == '70') || ($dn_asal == '60' && $dn_tujuan == '80')) {
               $qData = $this->db->select('td_workorder_item.woitem_kd as prim_key, td_workorder_item.*, td_workorder_item_detail.kd_barang as kd_barang_key, td_workorder_item_detail.*')  
                ->from('td_workorder_item')
                    ->join('td_workorder_item_detail', 'td_workorder_item.woitem_kd=td_workorder_item_detail.woitem_kd', 'left')
                    ->where('td_workorder_item.woitem_prosesstatus', 'workorder')
                    ->where('td_workorder_item.woitem_jenis', 'packing')
                    ->or_where('td_workorder_item.woitem_jenis', 'project')
                    ->group_by('td_workorder_item.woitem_kd')
                    ->get()->result_array();
            }

            if (!empty($qSent)) {
                foreach ($qData as $eData) {
                    $qtySisa = $eData['woitem_qty'];
                    $dndetail_qty = 0;
                    foreach ($qSent as $eSent) {
                        if ($eSent['woitem_kd'] == $eData['woitem_kd']) {
                            $qtySisa = $qtySisa - $eSent['dndetail_qty'];
                            $dndetail_qty = $eSent['dndetail_qty'];
                        }
                    }

                    if($dn_asal != '60'){  
                    /** Cek jika ada return */
                    if(!empty($qReturn)){
                        foreach ($qReturn as $eReturn){
                            if ($eReturn['woitem_kd'] == $eData['woitem_kd']) {
                                $qtySisa = $qtySisa + $eReturn['dndetailreceived_qty'];
                            }
                        }    
                    }
                }

                    if (empty($qtySisa)) {
                        continue;
                    }
                    // if(intval($dndetail_qty) == 0){
                        if($qtySisa != 0){
                            $result[] = [
                                'woitem_kd' => $eData['prim_key'],
                                'woitem_itemcode' => $eData['woitem_itemcode'],
                                'woitem_deskripsi' => $eData['woitem_deskripsi'],
                                'woitem_dimensi' => $eData['woitem_dimensi'],
                                'woitem_no_wo' => $eData['woitem_no_wo'],
                                'woitem_qty' => $qtySisa > $eData['woitem_qty'] ? $eData['woitem_qty'] : $qtySisa,
                                'woitem_jenis' => $eData['woitem_jenis'],
                                'kd_barang' => $eData['kd_barang_key'],
                                'sisa' => $qtySisa,
                                'qty_utuh' => $eData['woitem_qty'],
                                'qty_terkirim' => intval($dndetail_qty),
                            ];
                        }
                      
                    // }
                
                    // $result['A'] = 'A';
                    //$result['F'] = $qData;
                  
                    // $result['X'] = $qtySisa;
                }
            } else {
                $result = $qData;
            }
        } 
        else {
            $rowResult = $this->db->select()
                ->from('td_deliverynote_detail')
                ->join('td_workorder_item', 'td_deliverynote_detail.woitem_kd=td_workorder_item.woitem_kd', 'left')
                ->join('td_workorder_item_detail', 'td_workorder_item.woitem_kd=td_workorder_item_detail.woitem_kd', 'left')
                ->where('td_deliverynote_detail.dndetail_kd', $dndetail_kd)
                ->get()->row_array();
            $qBom = $this->db->select()
                ->from('td_workorder_item_detail')
                ->where('td_workorder_item_detail.woitemdetail_parent', $rowResult['woitemdetail_kd'])
                ->get()->result_array();
            $arrPart = array_column($qBom, 'part_kd');

            $qData = $this->db->select('td_workorder_item.*, td_workorder_item_detail.kd_barang')
                ->from('td_workorder_item')
                ->join('td_workorder_item_detail', 'td_workorder_item.woitem_kd=td_workorder_item_detail.woitem_kd', 'left')
                ->where_in('td_workorder_item_detail.part_kd', $arrPart)
                ->where('td_workorder_item.woitem_jenis != ', 'packing')
                ->where('td_workorder_item.woitem_prosesstatus', 'workorder')
                ->group_by('td_workorder_item.woitem_kd')
                ->get()->result_array();
            if (!empty($qSent)) {
                foreach ($qData as $eData2) {
                    $qtySisa = $eData2['woitem_qty'];
                    $dndetail_qty = 0;
                    foreach ($qSent as $eSent2) {
                        if ($eSent2['woitem_kd'] == $eData2['woitem_kd']) {
                            $qtySisa = $qtySisa - $eSent2['dndetail_qty'];
                            $dndetail_qty = $eSent2['dndetail_qty'];
                        }
                    }
                    if (empty($qtySisa)) {
                        continue;
                    }
                    $result[] = [
                        'woitem_kd' => $eData2['woitem_kd'],
                        'woitem_itemcode' => $eData2['woitem_itemcode'],
                        'woitem_deskripsi' => $eData2['woitem_deskripsi'],
                        'woitem_dimensi' => $eData2['woitem_dimensi'],
                        'woitem_no_wo' => $eData2['woitem_no_wo'],
                        'woitem_qty' => $qtySisa > $eData2['woitem_qty'] ? $eData2['woitem_qty'] : $qtySisa,
                        'woitem_jenis' => $eData2['woitem_jenis'],
                        'kd_barang' => $eData2['kd_barang'],
                        'sisa' => $qtySisa,
                        'qty_utuh' => $eData2['woitem_qty'],
                        'qty_terkirim' => intval($dndetail_qty),
                    ];
                }
            } else {
                $result = $qData;
            }
        }

        /** Sesuaikan dengan select2 */
        $arrResult = [];
        $paramWO = strtolower($paramWO);
        foreach ($result as $e) {
            $text = "{$e['woitem_no_wo']} | {$e['woitem_itemcode']} / {$e['woitem_deskripsi']} {$e['woitem_dimensi']}";
            /** Cek array search with param from input select2 params */
            if (!empty($paramWO) && strpos(strtolower($text), $paramWO) === false) {
                continue;
            }
            $e['id'] = $e['woitem_kd'];
            $e['text'] = $text;
            $arrResult[] = $e;
        }
        header('Content-Type: application/json');
        echo json_encode($arrResult);
    }


    public function pdf_tablemain()
    {
        $this->load->library('Pdf');
        $dn_kd = $this->input->get('id', true);

        $qMaster = $this->tm_deliverynote->get_by_param_detail(['tm_deliverynote.dn_kd' => $dn_kd])->row_array();
        $qReceived = $this->db->join('tb_admin', 'tb_admin.kd_admin=td_deliverynote_received.admin_kd', 'left')->where('td_deliverynote_received.dn_kd', $dn_kd)
            ->get('td_deliverynote_received')->row_array();
        $qResult = $this->db->select('td_deliverynote_detail.*, SUM(td_deliverynote_received.dndetailreceived_qty) as sum_received, td_workorder_item.woitem_no_wo, td_workorder_item.woitem_itemcode')
            ->from('td_deliverynote_detail')
            ->join('td_deliverynote_received', 'td_deliverynote_detail.dndetail_kd=td_deliverynote_received.dndetail_kd', 'left')
            ->join('td_workorder_item', 'td_deliverynote_detail.woitem_kd=td_workorder_item.woitem_kd', 'left');
        if ($qMaster['dn_tujuan'] == '70') {
            $qResult = $qResult->group_start()
                ->where('td_workorder_item.woitem_jenis', 'packing')
                ->or_where('td_workorder_item.woitem_jenis', 'project')
                ->group_end();
        }
        $qResult = $qResult->where('td_deliverynote_detail.dn_kd', $dn_kd)
            ->group_by('td_deliverynote_detail.dndetail_kd')
            ->get();
        $qLogDNTerkirim = $this->tb_deliverynote_log->get_by_param_detail(['dn_kd' => $qMaster['dn_kd'], 'tb_deliverynote_log.dnstate_kd' => '2'])->row_array();
        $qLogDNDiterima = $this->tb_deliverynote_log->get_by_param_detail(['dn_kd' => $qMaster['dn_kd'], 'tb_deliverynote_log.dnstate_kd' => '3'])->result_array();
        $qLogDNApprove = $this->tb_deliverynote_log->get_by_param_detail(['dn_kd' => $qMaster['dn_kd'], 'tb_deliverynote_log.dnstate_kd' => '5'])->row_array();
        
        $data['result'] =  $qResult->result_array();
        $data['master'] = $qMaster;
        $data['master']['pengirim'] = $qLogDNTerkirim;
        $data['master']['penerima'] = $qLogDNDiterima;
        $data['master']['approve'] = $qLogDNApprove;
        $data['class_link'] = $this->class_link;
        $data['konten'] = $this->load->view('page/' . $this->class_link . '/pdf_tablemain', $data, true);
        $this->load->view('page/' . $this->class_link . '/pdf_main', $data);
    }

    public function action_insert_master()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $slug = $this->input->post('txtmasterSlug', true);
        $dn_asal = $this->input->post('txtdn_asal', true);
        $dn_tujuan = $this->input->post('txtdn_tujuan', true);
        $dn_tanggal = $this->input->post('txtdn_tanggal', true);
        $dn_kd = $this->input->post('txtdn_kd', true);
        $dn_note = $this->input->post('txtdn_note', true);
        $return = $this->input->post('chkReturn', true) ? $this->input->post('chkReturn', true) : 0;
        $act = false;

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtmasterSlug', 'Slug', 'required', ['required' => '{field} tidak boleh kosong!']);
        if($slug == 'add'){
            $this->form_validation->set_rules('txtdn_asal', 'Asal', 'required', ['required' => '{field} tidak boleh kosong!']);
            $this->form_validation->set_rules('txtdn_tujuan', 'Tujuan', 'required', ['required' => '{field} tidak boleh kosong!']);
        }else{
            $this->form_validation->set_rules('txtdn_tujuan', 'Tujuan', 'required', ['required' => '{field} tidak boleh kosong!']);
        }
        $this->form_validation->set_rules('txtdn_tanggal', 'Tanggal', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) :
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idtxtmasterSlug' => (!empty(form_error('txtmasterSlug'))) ? buildLabel('warning', form_error('txtmasterSlug', '"', '"')) : '',
                'idErrdn_asal' => (!empty(form_error('txtdn_asal'))) ? buildLabel('warning', form_error('txtdn_asal', '"', '"')) : '',
                'idErrdn_tujuan' => (!empty(form_error('txtdn_tujuan'))) ? buildLabel('warning', form_error('txtdn_tujuan', '"', '"')) : '',
                'idErrdn_tanggal' => (!empty(form_error('txtdn_tanggal'))) ? buildLabel('warning', form_error('txtdn_tanggal', '"', '"')) : '',
            );

        else :
            if ($slug == 'add') {
                $dn_kd = $this->tm_deliverynote->create_code();
                $dn_no = $this->tm_deliverynote->create_no($dn_asal, $dn_tanggal);
                $data = [
                    'dn_kd' => $dn_kd,
                    'dn_no' => $dn_no,
                    'dn_originator' => $this->session->userdata('kd_admin'),
                    'dn_tanggal' => format_date($dn_tanggal, 'Y-m-d'),
                    'dn_asal' => $dn_asal,
                    'dn_tujuan' => $dn_tujuan,
                    'dnstate_kd' => 1,
                    'dn_note' => $dn_note,
                    'flag_return' => $return,
                    'dn_tglinput' => date('Y-m-d H:i:s'),
                    'dn_tgledit' => date('Y-m-d H:i:s'),
                    'admin_kd' => $this->session->userdata('kd_admin'),
                ];
                $act = $this->tm_deliverynote->insert_data($data);
            } elseif ($slug == 'edit') {
                $data = [
                    'dn_tanggal' => format_date($dn_tanggal, 'Y-m-d'),
                    'dn_tujuan' => $dn_tujuan,
                    'dn_note' => $dn_note,
                    'flag_return' => $return,
                    'dn_tgledit' => date('Y-m-d H:i:s'),
                    'admin_kd' => $this->session->userdata('kd_admin'),
                ];
                $act = $this->tm_deliverynote->update_data(['dn_kd' => $dn_kd], $data);
            }

            if ($act) :
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['dn_kd' => $dn_kd]);
            else :
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            endif;

        endif;

        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_insert_dndetail()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtwoitem_kd', 'WO', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtdndetail_qty', 'Jenis', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErridtxtwoitem_kdOpsi' => (!empty(form_error('txtwoitem_kd'))) ? buildLabel('warning', form_error('txtwoitem_kd', '"', '"')) : '',
                'idErrdndetail_qty' => (!empty(form_error('txtdndetail_qty'))) ? buildLabel('warning', form_error('txtdndetail_qty', '"', '"')) : '',
            );
        } else {
            $dn_kd = $this->input->post('txtdn_kd', true);
            $woitem_kd = $this->input->post('txtwoitem_kd', true);
            $dndetail_qty = $this->input->post('txtdndetail_qty', true);
            $dndetail_remark = $this->input->post('txtdndetail_remark', true);
            $slug = $this->input->post('txtslug', true);
            $dndetail_kdParent = $this->input->post('txtdndetail_kdParent', true);
            $kd_barang = $this->input->post('txtkd_barang', true);
            $dndetail_qtymax = $this->input->post('txtdndetail_qtymax', true);

            /** Blocking action condition */
            if ($dndetail_qty > $dndetail_qtymax) {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Qty lebih dari Qty Max');
                $resp['csrf'] = $this->security->get_csrf_hash();
                echo json_encode($resp);
                die();
            }

            /** Untuk insert dengan parent */
            $dndetail_parent = null;
            if ($slug == 'add_detail') {
                $dndetail_parent = $dndetail_kdParent;
            }

            $data = array(
                'dndetail_kd' => $this->td_deliverynote_detail->create_code(),
                'dn_kd' => $dn_kd,
                'woitem_kd' => $woitem_kd,
                'kd_barang' => $kd_barang,
                'dndetail_qty' => $dndetail_qty,
                'dndetail_parent' => $dndetail_parent,
                'dndetail_remark' => $dndetail_remark,
                'dndetail_tglinput' => date('Y-m-d H:i:s'),
                'dndetail_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            );

            $act = $this->td_deliverynote_detail->insert_data($data);
            if ($act) :
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            else :
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            endif;
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_insert_dnreceived()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtdn_kd', 'WO', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErridtxtdn_kd' => (!empty(form_error('txtdn_kd'))) ? buildLabel('warning', form_error('txtdn_kd', '"', '"')) : '',
            );
        } else {
            $dn_kd = $this->input->post('txtdn_kd', true);
            $dn_asal = $this->input->post('txtdn_asal', true);
            $dn_tujuan = $this->input->post('txtdn_tujuan', true);
            $dndetailreceived = $this->input->post('txtdndetailreceived', true);
            $rDndetail_kd = $this->input->post('txtdndetail_kd', true);
            $dndetailreceived_tanggal = $this->input->post('txtterima_tanggal', true);
            $rDndetailreceived_qty = $this->input->post('txtdndetailreceived_qty', true);
            $rDndetailreceived_remark = $this->input->post('txtdndetailreceived_remark', true);
            $dnlog_keterangan = $this->input->post('txtdnlog_keterangan', true);
            $dnstate_kd = $this->input->post('txtdnstate_kd', true);
            $dndetail_kdChild = $this->input->post('txtdndetail_kdChild', true);
            $dndetail_qtyChild = $this->input->post('txtdndetail_qtyChild', true);
            $arrKd_barang = $this->input->post('txtkd_barang', true);
            $woitem_kds = $this->input->post('txtwoitem_kds', true);
            $woitem_kdsChild = $this->input->post('txtwoitem_kdsChild', true);

            $dn = $this->tm_deliverynote->get_by_param(['dn_kd' => $dn_kd])->row_array();
            $data = [];
            $dndetailreceived_kd = $this->td_deliverynote_received->create_code();
            foreach ($rDndetail_kd as $r) {
                /** Jangan di proses apabila Qty = 0 */
                if (empty($rDndetailreceived_qty[$r])) {
                    continue;
                }
                $data[] = [
                    'dndetailreceived_kd' => $dndetailreceived_kd,
                    'dn_kd' => $dn_kd,
                    'dndetail_kd' => $r,
                    'woitem_kd' => $woitem_kds[$r],
                    'dndetailreceived_tanggal' => $dndetailreceived_tanggal,
                    'dndetailreceived_qty' => $rDndetailreceived_qty[$r],
                    'dndetailreceived_remark' => !empty($rDndetailreceived_remark[$r]) ? $rDndetailreceived_remark[$r] : null,
                    'dndetailreceived_tglinput' => date('Y-m-d H:i:s'),
                    'dndetailreceived_tgledit' => date('Y-m-d H:i:s'),
                    'admin_kd' => $this->session->userdata('kd_admin'),
                ];
                $dndetailreceived_kd++;
                /** Detail dn */
                /** Child */
                if (!empty($dndetail_kdChild)) {
                    $countChild = count($dndetail_kdChild[$r]);
                    for ($i = 0; $i < $countChild; $i++) {
                        $data[] = [
                            'dndetailreceived_kd' => $dndetailreceived_kd,
                            'dn_kd' => $dn_kd,
                            'dndetail_kd' => $dndetail_kdChild[$r][$i],
                            'woitem_kd' => $woitem_kdsChild[$r][$i],
                            'dndetailreceived_tanggal' => $dndetailreceived_tanggal,
                            'dndetailreceived_qty' => $dndetail_qtyChild[$r][$i],
                            'dndetailreceived_remark' => !empty($rDndetailreceived_remark[$r]) ? $rDndetailreceived_remark[$r] : null,
                            'dndetailreceived_tglinput' => date('Y-m-d H:i:s'),
                            'dndetailreceived_tgledit' => date('Y-m-d H:i:s'),
                            'admin_kd' => $this->session->userdata('kd_admin'),
                        ];
                        $dndetailreceived_kd++;
                    }
                }

                /** Untuk update WIP */
                $dataWIP[] = ['kd_barang' => $arrKd_barang[$r], 'qty' => $rDndetailreceived_qty[$r]];
            }
                $this->db->trans_start();
                $act = $this->td_deliverynote_received->insert_batch_data($data);

                if ($act) {
                    $cekReceived = $this->tm_deliverynote->all_received($dn_kd, $dn_asal, $dn_tujuan);
                    // Kode terima DN
                    $terimaDnstate_kd = 3;
                    if ($cekReceived) {
                        /** Update otomatis ke Terima */
                        $actUpdateDn = $this->tm_deliverynote->update_data(['dn_kd' => $dn_kd], ['dnstate_kd' => $terimaDnstate_kd, 'dn_tgledit' => date('Y-m-d H:i:s')]);
                    }
                    $dataLog = [
                        'dnlog_kd' => $this->tb_deliverynote_log->create_code(),
                        'dn_kd' => $dn_kd,
                        'dnstate_kd' => $terimaDnstate_kd,
                        'dnlog_keterangan' => $dnlog_keterangan,
                        'dnlog_tglinput' => date('Y-m-d H:i:s'),
                        'admin_kd' => $this->session->userdata('kd_admin'),
                    ];
                    $actLog = $this->tb_deliverynote_log->insert_data($dataLog);
                    /** Clear cache query database */
                    $this->db->reset_query();
                    if ($dn['dn_tujuan'] == '70') {
                        /** Update WIP 
                        * Jika terjadi deliverynote ke FG
                        */
                        $actUpdateWip = $this->tm_wip->change_wip_stock($dataWIP, 'out');
                        /** Update WO to finish jika sudah selesai */
                        $actUpdateWO = $this->tm_deliverynote->update_woitem_prosesstatus($dn_kd); 
                    } elseif ($dn['dn_tujuan'] == '80'){
                        /** Update WIP 
                        * Jika terjadi deliverynote ke RM dan WO item langsung di close/finish
                        */
                        $actUpdateWip = $this->tm_wip->change_wip_stock($dataWIP, 'out');
                        /** Update WO item to finish from dn_kd */
                        $qWOItemReceived = $this->db->select('t0.woitem_kd, SUM(t0.dndetailreceived_qty) AS dn_qty, t1.woitem_qty, SUM(t0.dndetailreceived_qty) - t1.woitem_qty AS total')
                            ->from('td_deliverynote_received t0')
                            ->join('td_workorder_item t1', 't0.woitem_kd = t1.woitem_kd', 'INNER')
                            ->where('t0.dn_kd', $dn_kd)
                            ->group_by('t0.woitem_kd')
                            ->get()->result_array();
                        $dataUpdateToWO = [];
                        foreach ($qWOItemReceived as $q) {
                            /** Jika (Qty DN - Qty WO) hasilnya 0 berarti nilainya penuh atau habis / status WO di close */ 
                            if( $q['total'] <= 0 ){
                                $dataUpdateToWO[] = $q['woitem_kd'];
                            }
                        }
                        if (!empty($dataUpdateToWO)) {
                            /** Jika qty WO dan received sudah penuh, WO di close */
                            $actUpdateWO = $this->td_workorder_item->update_prosesstatus($dataUpdateToWO, 'finish');
                        }
                    }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
                } else {
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
                }
            } else {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_delete_dndetail()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            $dndetail_kd = $this->input->get('id');
            if (!empty($dndetail_kd)) {

                $this->db->trans_start();
                $this->td_deliverynote_detail->delete_data($dndetail_kd);
                /** Del child */
                $this->td_deliverynote_detail->delete_by_param(['dndetail_parent' => $dndetail_kd]);
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE) {
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Delete');
                } else {
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Sukses');
                }
            } else {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Id Tidak Ditemukan');
            }
            echo json_encode($resp);
        }
    }

    public function action_changestatus()
    {
        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtdnstate_kd', 'State', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrdnstate_kd' => (!empty(form_error('txtdnstate_kd'))) ? buildLabel('warning', form_error('txtdnstate_kd', '"', '"')) : '',
            );
        } else {
            $dn_kd = $this->input->post('txtdn_kd', true);
            $dnstate_kd = $this->input->post('txtdnstate_kd', true);
            $dnlog_keterangan = $this->input->post('txtdnlog_keterangan', true);

            $data = [
                'dnstate_kd' => $dnstate_kd,
                'dn_tgledit' => date('Y-m-d H:i:s'),
            ];
            $dataLog = [
                'dnlog_kd' => $this->tb_deliverynote_log->create_code(),
                'dn_kd' => $dn_kd,
                'dnstate_kd' => $dnstate_kd,
                'dnlog_keterangan' => !empty($dnlog_keterangan) ? $dnlog_keterangan : null,
                'dnlog_tglinput' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            ];
            $act = $this->tm_deliverynote->update_data(['dn_kd' => $dn_kd], $data);
            if ($act) {
                $actLog = $this->tb_deliverynote_log->insert_data($dataLog);
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Sukses');
            } else {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Delete');
            }
        }

        echo json_encode($resp);
    }

    public function action_delete()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            $dn_kd = $this->input->get('id');
            if (!empty($dn_kd)) {
                $act = $this->td_deliverynote_detail->delete_by_param(['dn_kd' => $dn_kd]);
                if ($act) {
                    $actM = $this->tm_deliverynote->delete_data($dn_kd);
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Sukses');
                } else {
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Delete');
                }
            } else {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Id Tidak Ditemukan');
            }
            echo json_encode($resp);
        }
    }
}
