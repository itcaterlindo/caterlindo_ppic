<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Material_requisition extends MY_Controller
{
    private $class_link = 'production/material_requisition';
    private $wf_kd = 5;

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('ssp', 'form_validation'));
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
        $this->load->model(array(
            'tm_materialrequisition', 'tb_bagian', 'td_materialrequisition_detail', 'td_workorder_item_detail', 'td_workorder_item', 'tb_admin', 'td_workflow_state', 'td_materialrequisition_log', 'td_workflow_transition',
            'td_workflow_transition_notification', 'tb_deliverynote_user', 'tm_planningweekly', 'tb_materialrequisition_detail_planningweekly'
        ));
    }

    public function index()
    {
        parent::administrator();
        parent::datetimepicker_assets();
        parent::select2_assets();
        parent::pnotify_assets();
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function open_table()
    {
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }

    public function table_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data = $this->tm_materialrequisition->ssp_table2();
        echo json_encode($data);
    }

    public function open_form_box()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $slug = $this->input->get('slug');
        $id = $this->input->get('id');

        $data['class_link'] = $this->class_link;
        $data['slug'] = $slug;
        $data['id'] = $id;
        $this->load->view('page/' . $this->class_link . '/form_box', $data);
    }

    public function form_master()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $slug = $this->input->get('slug', true);
        $id = $this->input->get('id', true);

        if ($slug == 'edit') :
            $data['rowData'] = $this->tm_materialrequisition->get_by_param(['materialreq_kd' => $id])->row_array();
            $data['planningWeeklys'] = $this->tb_materialrequisition_detail_planningweekly->get_by_param_detail(['tb_materialrequisition_detail_planningweekly.materialreq_kd' => $id])->result_array();
        endif;

        $bagians = $this->tb_bagian->get_by_param(['bagian_lokasi' => 'WIP'])->result();
        /** Opsi Bagian */
        $opsiBagian = array();
        $opsiBagian[null] = '-- Pilih Opsi --';
        foreach ($bagians as $bagian) :
            $opsiBagian[$bagian->bagian_kd] = $bagian->bagian_kd . ' | ' . $bagian->bagian_nama;
        endforeach;

        $data['class_link'] = $this->class_link;
        $data['slug'] = $slug;
        $data['id'] = $id;
        $data['opsiBagian'] = $opsiBagian;
        $this->load->view('page/' . $this->class_link . '/form_master', $data);
    }

    public function form_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $slug = $this->input->get('slug', true);
        $id = $this->input->get('id', true);
        $materialreq_kd = $this->input->get('materialreq_kd', true);

        $reqplannings = $this->tb_materialrequisition_detail_planningweekly->get_by_param_detail(['tb_materialrequisition_detail_planningweekly.materialreq_kd' => $materialreq_kd])->result_array();

        $data['materialreq'] = $this->tm_materialrequisition->getRowMaterialReqBagianState($materialreq_kd);
        $data['planningweekly_nos'] = implode(', ', array_column($reqplannings, 'planningweekly_no'));
        $data['class_link'] = $this->class_link;
        $data['slug'] = $slug;
        $data['id'] = $id;
        $data['materialreq_kd'] = $materialreq_kd;
        $this->load->view('page/' . $this->class_link . '/form_main', $data);
    }

    public function form_item()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $slug = $this->input->get('slug', true);
        $materialreq_kd = $this->input->get('materialreq_kd', true);

        $data['materialreq_kd'] = $materialreq_kd;
        $data['slug'] = $slug;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_item', $data);
    }

    public function form_item_table()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $materialreq_kd = $this->input->get('materialreq_kd', true);

        $data['materialreq_kd'] = $materialreq_kd;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_item_table', $data);
    }

    public function form_item_table_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $materialreq_kd = $this->input->get('materialreq_kd', true);

        $data = $this->td_materialrequisition_detail->ssp_table($materialreq_kd);
        echo json_encode(
            SSP::simple($_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'])
        );
    }

    public function form_item_tablewoplanning()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $materialreq_kd = $this->input->get('materialreq_kd', true);

        $data['results'] = $this->db->select('td_planningweekly_detail.planningweeklydetail_kd, td_planningweekly_detail.woitem_kd, td_planningweekly_detail.planningweeklydetail_qty,
                td_workorder_item.woitem_no_wo, td_workorder_item.woitem_itemcode, td_workorder_item.woitem_deskripsi, td_workorder_item.woitem_dimensi,
                tm_planningweekly.planningweekly_no,
                tb_materialrequisition_detail_planningweekly.materialreqdetailweekly_kd,
                SUM(td_materialrequisition_detail.materialreqdetail_qty) as sum_materialreqdetail_qty')
            ->where('tb_materialrequisition_detail_planningweekly.materialreq_kd', $materialreq_kd)
            ->join('td_planningweekly_detail', 'td_planningweekly_detail.planningweekly_kd=tb_materialrequisition_detail_planningweekly.planningweekly_kd', 'left')
            ->join('td_workorder_item', 'td_workorder_item.woitem_kd=td_planningweekly_detail.woitem_kd', 'left')
            ->join('td_materialrequisition_detail', 'td_materialrequisition_detail.planningweeklydetail_kd=td_planningweekly_detail.planningweeklydetail_kd', 'left')
            ->join('tm_planningweekly', 'tm_planningweekly.planningweekly_kd=tb_materialrequisition_detail_planningweekly.planningweekly_kd', 'left')
            ->group_by('td_planningweekly_detail.planningweeklydetail_kd')
            ->get('tb_materialrequisition_detail_planningweekly')->result_array();
        $data['id'] = $materialreq_kd;
        $data['class_link'] = $this->class_link;

        $this->load->view('page/' . $this->class_link . '/form_item_tablewoplanning', $data);
    }

    public function view_box()
    {
        parent::administrator();
        parent::pnotify_assets();
        $id = $this->input->get('id', true);

        $reqplannings = $this->tb_materialrequisition_detail_planningweekly->get_by_param_detail(['tb_materialrequisition_detail_planningweekly.materialreq_kd' => $id])->result_array();

        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        $data['header'] = $this->tm_materialrequisition->getRowMaterialReqBagianState($id);
        $data['planningweekly_nos'] = implode(', ', array_column($reqplannings, 'planningweekly_no'));
        $data['generate_button'] = $this->tm_materialrequisition->generate_button($id, $this->wf_kd, $data['header']['wfstate_kd']);

        $this->load->view('page/' . $this->class_link . '/view_box', $data);
    }

    public function view_log()
    {
        $id = $this->input->get('id', true);

        $data['result'] = $this->db->select('td_materialrequisition_log.*, td_workflow_transition.*, tb_admin.nm_admin, state_source.wfstate_nama as state_source, state_dst.wfstate_nama as state_dst')
            ->from('td_materialrequisition_log')
            ->join('td_workflow_transition', 'td_materialrequisition_log.wftransition_kd=td_workflow_transition.wftransition_kd', 'left')
            ->join('td_workflow_state as state_source', 'state_source.wfstate_kd=td_workflow_transition.wftransition_source', 'left')
            ->join('td_workflow_state as state_dst', 'state_dst.wfstate_kd=td_workflow_transition.wftransition_destination', 'left')
            ->join('tb_admin', 'td_materialrequisition_log.admin_kd=tb_admin.kd_admin', 'left')
            ->where('td_materialrequisition_log.materialreq_kd', $id)
            ->order_by('td_materialrequisition_log.materialreqlog_tglinput', 'desc')
            ->get()->result_array();

        $this->load->view('page/' . $this->class_link . '/view_log', $data);
    }

    public function pdf_tablemain()
    {
        $materialreq_kd = $this->input->get('id', true);
        $data['results'] = $this->td_materialrequisition_detail->getDetailMaterialReqWoitem($materialreq_kd);

        $this->load->view('page/' . $this->class_link . '/pdf_tablemain', $data);
    }

    public function formubahstate_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $id = $this->input->get('id');
        $wftransition_kd = $this->input->get('wftransition_kd');

        $data['id'] = $id;
        $data['wftransition_kd'] = $wftransition_kd;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/formubahstate_main', $data);
    }

    public function get_planningweekly()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $paramPlanningweekly = $this->input->get('paramPlanningweekly');
        $divisi_kd = $this->input->get('divisi_kd');
        $planWeeklys = $this->db->select('tm_planningweekly.*, tb_bagian.bagian_nama, div.bagian_nama as divisi_nama')
            ->where('tm_planningweekly.divisi_kd', $divisi_kd)
            ->where('tm_planningweekly.planningweekly_closed', 0)
            ->join('tb_bagian', 'tb_bagian.bagian_kd=tm_planningweekly.bagian_kd', 'left')
            ->join('tb_bagian as div', 'div.bagian_kd=tm_planningweekly.divisi_kd', 'left');
        if (!empty($paramPlanningweekly)) {
            $planWeeklys = $planWeeklys->like('tm_planningweekly.planningweekly_no', $paramPlanningweekly);
        }
        $planWeeklys = $planWeeklys->get('tm_planningweekly')->result_array();

        $arrayResults = [];
        foreach ($planWeeklys as $planWeekly) {
            $arrayResults[] = [
                'id' => $planWeekly['planningweekly_kd'],
                'text' => "{$planWeekly['planningweekly_no']} | {$planWeekly['divisi_nama']}({$planWeekly['bagian_nama']})",
            ];
        }
        echo json_encode($arrayResults);
    }

    private function action_special($action = null, $aParam = [])
    {
        $act = true;
        $admin_kd = $this->session->userdata('kd_admin');
        switch ($action) {
            case 'MATERIALREQ_CLOSED_0':
                $act = $this->tm_materialrequisition->update_data(['materialreq_kd' => $aParam['materialreq_kd']], ['materialreq_receiptclosed' => 0]);
                break;
            case 'MATERIALREQ_CLOSED_1':
                $act = $this->tm_materialrequisition->update_data(['materialreq_kd' => $aParam['materialreq_kd']], ['materialreq_receiptclosed' => 1]);
                break;
            case 'PLANNINGWEEKLY_CHECK_CLOSED':
                $reqplannings = $this->tb_materialrequisition_detail_planningweekly->get_by_param(['materialreq_kd' => $aParam['materialreq_kd']])->result_array();
                $act = $this->tm_planningweekly->checkRequisitionPlanning(array_column($reqplannings, 'planningweekly_kd'));
                break;
            default:
                $act = true;
        }

        return $act;
    }

    public function action_change_state()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtid', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtwftransition_kd', 'Transition', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrrm_kd' => (!empty(form_error('txtid'))) ? buildLabel('warning', form_error('txtid', '"', '"')) : '',
                'idErrrm_kd' => (!empty(form_error('txtwftransition_kd'))) ? buildLabel('warning', form_error('txtwftransition_kd', '"', '"')) : '',
            );
        } else {
            $materialreq_kd = $this->input->post('txtid', true);
            $wftransition_kd = $this->input->post('txtwftransition_kd', true);
            $materialreqlog_note = $this->input->post('txtmaterialreqlog_note', true);

            $data = [
                'materialreqlog_kd' => $this->td_materialrequisition_log->create_code(),
                'materialreq_kd' => $materialreq_kd,
                'wftransition_kd' => $wftransition_kd,
                'admin_kd' => $this->session->userdata('kd_admin'),
                'materialreqlog_note' => $materialreqlog_note,
                'materialreqlog_tglinput' => date('Y-m-d H:i:s'),
            ];
            $this->db->trans_begin();
            $materialreq = $this->tm_materialrequisition->get_by_param(['materialreq_kd' => $materialreq_kd])->row_array();
            $wftransition = $this->td_workflow_transition->get_by_param(['wftransition_kd' => $wftransition_kd])->row_array();
            $dataUpdate = [
                'wfstate_kd' => $wftransition['wftransition_destination'],
                'materialreq_tgledit' => date('Y-m-d H:i:s'),
            ];
            #email message notification
            $subject = 'Material Requisition - ' . $materialreq['materialreq_no'];

            $dtMessage['text'] = 'Terdapat Material Requisition yang perlu approval Anda :';
            $dtMessage['url'] = 'http://' . $_SERVER['HTTP_HOST'] . base_url() . $this->class_link . '/view_box?id=' . $materialreq_kd;
            $dtMessage['urltext'] = 'Detail Material Requisition';
            $dtMessage['keterangan'] = !empty($materialreqlog_note) ? $materialreqlog_note : null;
            $message = $this->load->view('templates/email/email_notification', $dtMessage, true);

            #special action
            if (!empty($wftransition['wftransition_action'])) {
                $expAction = explode(';', $wftransition['wftransition_action']);
                for ($i = 0; $i < count($expAction); $i++) {
                    $actSpecial = $this->action_special($expAction[$i], $data);
                }
            }

            $act = $this->td_materialrequisition_log->insert_data($data);
            $actUpdate = $this->tm_materialrequisition->update_data(['materialreq_kd' => $materialreq_kd], $dataUpdate);
            $actNotif = $this->td_workflow_transition_notification->generate_notification($wftransition_kd, $subject, $message);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            } else {
                $this->db->trans_commit();
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function get_woitem()
    {
        $paramWO = $this->input->get('paramWO', true);
        $bagian_kd = $this->input->get('bagian_kd', true);

        $planningWeeklyDetails = $this->db->where('tm_planningweekly.bagian_kd', $bagian_kd);
        if (!empty($paramWO)) {
            $planningWeeklyDetails = $planningWeeklyDetails->like('td_workorder_item.woitem_no_wo', $paramWO);
        }
        $planningWeeklyDetails = $planningWeeklyDetails->join('td_workorder_item', 'td_workorder_item.woitem_kd=td_planningweekly_detail.woitem_kd', 'left')
            ->join('tm_planningweekly', 'tm_planningweekly.planningweekly_kd=td_planningweekly_detail.planningweekly_kd', 'left')
            ->get('td_planningweekly_detail')
            ->result_array();

        $materialReqs = $this->db->select('td_materialrequisition_detail.planningweeklydetail_kd, SUM(td_materialrequisition_detail.materialreqdetail_qty) as sumQty')
            ->where('tm_materialrequisition.bagian_kd', $bagian_kd)
            ->join('tm_materialrequisition', 'tm_materialrequisition.materialreq_kd=td_materialrequisition_detail.materialreq_kd', 'left')
            ->group_by('td_materialrequisition_detail.planningweeklydetail_kd')
            ->get('td_materialrequisition_detail')->result_array();

        $arrResult = [];
        foreach ($planningWeeklyDetails as $planningWeeklyDetail) {
            $qty = $planningWeeklyDetail['planningweeklydetail_qty'];
            foreach ($materialReqs as $materialReq) {
                if ($materialReq['planningweeklydetail_kd'] == $planningWeeklyDetail['planningweeklydetail_kd']) {
                    $qty = $qty - $materialReq['sumQty'];
                }
            }
            if ($qty == 0) {
                continue;
            }
            $arrResult[] = [
                'planningweekly_no' => $planningWeeklyDetail['planningweekly_no'],
                'planningweeklydetail_kd' => $planningWeeklyDetail['planningweeklydetail_kd'],
                'woitem_kd' => $planningWeeklyDetail['woitem_kd'],
                'woitem_no_wo' => $planningWeeklyDetail['woitem_no_wo'],
                'woitem_itemcode' => $planningWeeklyDetail['woitem_itemcode'],
                'woitem_jenis' => $planningWeeklyDetail['woitem_jenis'],
                'woitem_deskripsi' => $planningWeeklyDetail['woitem_deskripsi'],
                'woitem_dimensi' => $planningWeeklyDetail['woitem_dimensi'],
                'qty' => $qty,
                'id' => $planningWeeklyDetail['planningweeklydetail_kd'],
                'text' => "{$planningWeeklyDetail['woitem_no_wo']} | {$planningWeeklyDetail['woitem_itemcode']}"
            ];
        }

        echo json_encode($arrResult);
    }

    public function action_insert_master()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtdivisi_kd', 'Divisi', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtbagian_kd', 'Bagian', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) :
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrdivisi_kd' => (!empty(form_error('txtdivisi_kd'))) ? buildLabel('warning', form_error('txtdivisi_kd', '"', '"')) : '',
                'idErrbagian_kd' => (!empty(form_error('txtbagian_kd'))) ? buildLabel('warning', form_error('txtbagian_kd', '"', '"')) : '',
            );

        else :
            $materialreq_kd = $this->input->post('txtmaterialreq_kd', true);
            $divisi_kd = $this->input->post('txtdivisi_kd', true);
            $bagian_kd = $this->input->post('txtbagian_kd', true);
            $planningweekly_kds = $this->input->post('txtplanningweekly_kds', true);

            $data = [
                'divisi_kd' => $divisi_kd,
                'bagian_kd' => $bagian_kd,
                'materialreq_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            ];

            if (empty($materialreq_kd)) {
                # Add
                $materialreq_kd = $this->tm_materialrequisition->create_code();
                $data = array_merge($data, [
                    'materialreq_kd' => $materialreq_kd,
                    'materialreq_no' => $this->tm_materialrequisition->create_no($divisi_kd, date('Y-m-d')),
                    'materialreq_tanggal' => date('Y-m-d'),
                    'materialreq_originator' => $this->session->userdata('kd_admin'),
                    'materialreq_receiptclosed' => 1,
                    'wfstate_kd' => $this->td_workflow_state->getInitialState($this->wf_kd),
                    'materialreq_tglinput' => date('Y-m-d H:i:s'),
                ]);
                $act = $this->tm_materialrequisition->insert_data($data);
                if ($act) :
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['materialreq_kd' => $materialreq_kd]);
                else :
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
                endif;
            } else {
                # Edit
                $act = $this->tm_materialrequisition->update_data(['materialreq_kd' => $materialreq_kd], $data);
                if ($act) :
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['materialreq_kd' => $materialreq_kd]);
                else :
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
                endif;
            }
            /** Detail planning weekly master */
            if (!empty($planningweekly_kds)) {
                $plannignweekly = $this->tb_materialrequisition_detail_planningweekly->get_by_param(['materialreq_kd' => $materialreq_kd])->result_array();
                $materialreq_kdExist = array_column($plannignweekly, 'planningweekly_kd');

                $arrInsertDetails = array_values(array_diff($planningweekly_kds, $materialreq_kdExist));
                $arrDeleteDetails = array_values(array_diff($materialreq_kdExist, $planningweekly_kds));
                if (!empty($arrInsertDetails)) {
                    for ($i = 0; $i < count($arrInsertDetails); $i++) {
                        $dataDetailInsert[] = [
                            'materialreq_kd' => $materialreq_kd,
                            'planningweekly_kd' => $arrInsertDetails[$i],
                            'materialreqdetailweekly_tglinput' =>  date('Y-m-d H:i:s'),
                            'admin_kd' => $this->session->userdata('kd_admin')
                        ];
                    }
                    try {
                        $this->tb_materialrequisition_detail_planningweekly->insert_batch($dataDetailInsert);
                    } catch (Exception $e) {
                        $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan PlanningWeekly');
                    }
                }
                if (!empty($arrDeleteDetails)) {
                    try {
                        $this->db->where_in('planningweekly_kd', $arrDeleteDetails)
                            ->delete('tb_materialrequisition_detail_planningweekly');
                    } catch (Exception $e) {
                        $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan PlanningWeekly');
                    }
                }
            }
        endif;

        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_insert_materialreqdetail()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtplanningweeklydetail_kd', 'WO', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtmaterialreqdetail_qty', 'Jenis', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErridtxtplanningweeklydetail_kd' => (!empty(form_error('txtplanningweeklydetail_kd'))) ? buildLabel('warning', form_error('txtplanningweeklydetail_kd', '"', '"')) : '',
                'idErrmaterialreqdetail_qty' => (!empty(form_error('txtmaterialreqdetail_qty'))) ? buildLabel('warning', form_error('txtmaterialreqdetail_qty', '"', '"')) : '',
            );
        } else {
            $materialreqdetail_kd = $this->input->post('txtmaterialreq_kd', true);
            $materialreq_kd = $this->input->post('txtmaterialreq_kd', true);
            $woitem_kd = $this->input->post('txtwoitem_kd', true);
            $planningweeklydetail_kd = $this->input->post('txtplanningweeklydetail_kd', true);
            $materialreqdetail_qty = $this->input->post('txtmaterialreqdetail_qty', true);
            $materialreqdetail_remark = $this->input->post('txtmaterialreqdetail_remark', true);

            $data = array(
                'materialreq_kd' => $materialreq_kd,
                'woitem_kd' => $woitem_kd,
                'planningweeklydetail_kd' => $planningweeklydetail_kd,
                'materialreqdetail_qty' => $materialreqdetail_qty,
                'materialreqdetail_remark' => !empty($materialreqdetail_remark) ? $materialreqdetail_remark : null,
                'materialreqdetail_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            );

            if (!empty($materialreqdetail_kd)) {
                /** Add */
                $materialreqdetail_kd = $this->td_materialrequisition_detail->create_code();
                $data = array_merge($data, [
                    'materialreqdetail_kd' => $materialreqdetail_kd,
                    'materialreqdetail_tglinput' => date('Y-m-d H:i:s'),
                ]);
                $act = $this->td_materialrequisition_detail->insert_data($data);
                if ($act) :
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
                else :
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
                endif;
            } else {
                /** Edit */
                $act = $this->td_materialrequisition_detail->update_data(['materialreqdetail_kd' => $materialreqdetail_kd], $data);
                if ($act) :
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
                else :
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Update');
                endif;
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_insert_materialreqdetail_batch()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtmaterialreq_kd', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrmaterialreq_kd' => (!empty(form_error('txtmaterialreq_kd'))) ? buildLabel('warning', form_error('txtmaterialreq_kd', '"', '"')) : '',
            );
        } else {
            $materialreq_kd = $this->input->post('txtmaterialreq_kd', true);
            $planningweeklydetail_kds = $this->input->post('txtplanningweeklydetail_kds', true);
            $woitem_kds = $this->input->post('txtwoitem_kds', true);
            $materialreqdetailweekly_kds = $this->input->post('txtmaterialreqdetailweekly_kds', true);
            $materialreqdetail_qtys = $this->input->post('txtmaterialreqdetail_qtys', true);
            $materialreqdetail_remarks = $this->input->post('txtmaterialreqdetail_remarks', true);

            if (!empty($planningweeklydetail_kds)) {
                $materialreqdetail_kd = $this->td_materialrequisition_detail->create_code();
                for ($i = 0; $i < count($planningweeklydetail_kds); $i++) {
                    if ($materialreqdetail_qtys[$planningweeklydetail_kds[$i]] <= 0) {
                        continue;
                    }
                    $data[] = [
                        'materialreqdetail_kd' => $materialreqdetail_kd,
                        'materialreq_kd' => $materialreq_kd,
                        'woitem_kd' => $woitem_kds[$planningweeklydetail_kds[$i]],
                        'materialreqdetailweekly_kd' => $materialreqdetailweekly_kds[$planningweeklydetail_kds[$i]],
                        'planningweeklydetail_kd' => $planningweeklydetail_kds[$i],
                        'materialreqdetail_qty' => $materialreqdetail_qtys[$planningweeklydetail_kds[$i]],
                        'materialreqdetail_remark' => !empty($materialreqdetail_remarks[$planningweeklydetail_kds[$i]]) ? $materialreqdetail_remarks[$planningweeklydetail_kds[$i]] : null,
                        'materialreqdetail_tglinput' => date('Y-m-d H:i:s'),
                        'materialreqdetail_tgledit' => date('Y-m-d H:i:s'),
                        'admin_kd' => $this->session->userdata('kd_admin'),
                    ];
                    $materialreqdetail_kd++;
                }
                try {
                    $this->td_materialrequisition_detail->insert_batch_data($data);
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['materialreq_kd' => $materialreq_kd]);
                } catch (Exception $e) {
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
                }
            } else {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Data Tidak ada yg dipilih');
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_delete_materialreq_detail()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            $materialreqdetail_kd = $this->input->get('id');
            if (isset($materialreqdetail_kd)) {
                $act = $this->td_materialrequisition_detail->delete_data($materialreqdetail_kd);
                if ($act) {
                    $resp = array('code' => 200, 'pesan' => 'Sukses');
                } else {
                    $resp = array('code' => 400, 'pesan' => 'Gagal Delete');
                }
            } else {
                $resp = array('code' => 400, 'pesan' => 'Id Tidak Ditemukan');
            }
            echo json_encode($resp);
        }
    }

    public function action_delete_master()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            $materialreq_kd = $this->input->get('id');
            if (isset($materialreq_kd)) {
                $act = $this->td_materialrequisition_detail->delete_by_param(['materialreq_kd' => $materialreq_kd]);
                if ($act) {
                    $this->tm_materialrequisition->delete_data($materialreq_kd);
                    $resp = array('code' => 200, 'pesan' => 'Sukses');
                } else {
                    $resp = array('code' => 400, 'pesan' => 'Gagal Delete');
                }
            } else {
                $resp = array('code' => 400, 'pesan' => 'Id Tidak Ditemukan');
            }
            echo json_encode($resp);
        }
    }

    public function pdf_main()
    {
        $this->load->library('Pdf');
        $materialreq_kd = $this->input->get('id', true);

        $dataDetail['results'] = $this->td_materialrequisition_detail->getDetailMaterialReqWoitem($materialreq_kd);

        $data['master'] = $this->tm_materialrequisition->getRowMaterialReqBagianState($materialreq_kd);
        $data['konten'] = $this->load->view('page/' . $this->class_link . '/pdf_tablemain', $dataDetail, true);
        $data['logs'] = $this->td_materialrequisition_log->get_log_printout($materialreq_kd)->result_array();

        $this->load->view('page/' . $this->class_link . '/pdf_main', $data);
    }

    public function pdf_detail_woitem()
    {
        $this->load->library('Pdf');
        $materialreq_kd = $this->input->get('id', true);

        $dataDetail['results'] = $this->td_materialrequisition_detail->getDetailMaterialReqWoitem($materialreq_kd);

        $data['master'] = $this->tm_materialrequisition->getRowMaterialReqBagianState($materialreq_kd);
        $data['konten'] = $this->load->view('page/' . $this->class_link . '/pdf_tablewoitem', $dataDetail, true);
        $data['logs'] = $this->td_materialrequisition_log->get_log_printout($materialreq_kd)->result_array();

        $this->load->view('page/' . $this->class_link . '/pdf_main', $data);
    }
}
