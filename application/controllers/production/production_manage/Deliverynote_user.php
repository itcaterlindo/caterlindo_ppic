<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Deliverynote_user extends MY_Controller
{
    private $class_link = 'production/production_manage/deliverynote_user';

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('ssp', 'form_validation'));
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
        $this->load->model(array('tb_deliverynote_user', 'tb_bagian', 'tb_admin', 'td_admin_tipe'));
    }

    public function index()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function table_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }

    public function table_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data = $this->tb_deliverynote_user->ssp_table();
        echo json_encode(
            SSP::simple($_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'])
        );
    }

    public function form_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $dn_kd = $this->input->get('id', true);

        /** Opsi Admin */
        $admins = $this->tb_admin->get_all()->result_array();
        $opsiUser[null] = '-- Pilih Opsi --';
        foreach ($admins as $admin) {
            $opsiUser[$admin['kd_admin']] = $admin['nm_admin'];
        }

        /** Opsi tipe admin */
        $adminTipes = $this->td_admin_tipe->get_all()->result_array();
        $opsiTipeUser[null] = '-- Pilih Opsi --';
        foreach ($adminTipes as $adminTipe) {
            $opsiTipeUser[$adminTipe['kd_tipe_admin']] = $adminTipe['nm_tipe_admin'];
        }

        /** Opsi Bagian */
        $bagians = $this->tb_bagian->get_all()->result_array();
        $opsiBagian[null] = '-- Pilih Opsi --';
        foreach ($bagians as $bagian) {
            $opsiBagian[$bagian['bagian_kd']] = $bagian['bagian_nama'];
        }

        $data['opsiUser'] = $opsiUser;
        $data['opsiTipeUser'] = $opsiTipeUser;
        $data['opsiBagian'] = $opsiBagian;
        $data['class_link'] = $this->class_link;
        $data['id'] = $dn_kd;
        $this->load->view('page/' . $this->class_link . '/form_main', $data);
    }

    public function action_insert()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtkd_bagian', 'Bagian', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErridtxtkd_bagian' => (!empty(form_error('txtkd_bagian'))) ? buildLabel('warning', form_error('txtkd_bagian', '"', '"')) : '',
            );
        } else {
            $dnuser_kd = $this->input->post('txtdnuser_kd', true);
            $admin_kd = $this->input->post('txtadmin_kd', true);
            $kd_tipe_admin = $this->input->post('txtkd_tipe_admin', true);
            $kd_bagian = $this->input->post('txtkd_bagian', true);

            $data = array(
                'admin_kd' => !empty($admin_kd) ? $admin_kd : null,
                'kd_tipe_admin' => !empty($kd_tipe_admin) ? $kd_tipe_admin : null,
                'kd_bagian' => $kd_bagian,
                'dnuser_tgledit' => date('Y-m-d H:i:s'),
            );
            if (empty($dnuser_kd)) {
                /** Add */
                $data = array_merge($data, [
                    'dnuser_kd' => $this->tb_deliverynote_user->create_code(),
                    'dnuser_tglinput' => date('Y-m-d H:i:s'),
                ]);
                $act = $this->tb_deliverynote_user->insert_data($data);
            } else {
                /** Edit */
                $act = $this->tb_deliverynote_user->update_data(['dnuser_kd' => $dnuser_kd], $data);
            }

            if ($act) :
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            else :
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            endif;
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_delete()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $dnuser_kd = $this->input->get('id');
        if (!empty($dnuser_kd)) {
            $this->db->trans_start();
            $this->tb_deliverynote_user->delete_data($dnuser_kd);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Delete');
            } else {
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Sukses');
            }
        } else {
            $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Id Tidak Ditemukan');
        }
        echo json_encode($resp);
    }
}
