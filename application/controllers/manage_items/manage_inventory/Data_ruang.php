<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Data_ruang extends MY_Controller {
	private $class_link = 'manage_items/manage_inventory/data_ruang';
	private $form_errs = array('idErrNmRuang');

	public function __construct() {
		parent::__construct();

		$this->load->library(array('ssp', 'form_validation'));
		$this->load->helper(array('form', 'my_btn_access_helper', 'my_helper'));
		$this->load->model(array('m_builder', 'td_rak_ruang'));
	}

	public function index() {
		parent::administrator();
		$this->get_table();
	}

	public function get_table($id = '') {
		$data['nm_gudang'] = $_SESSION['modul_inventory']['nm_gudang'];
		$data['nm_rak'] = $_SESSION['modul_inventory']['nm_rak'];
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function open_table() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		$data = $this->td_rak_ruang->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function get_form() {
		$data['id'] = $this->input->get('id');
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = $this->form_errs;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function open_form() {
		$id = $this->input->get('id');
		$data = $this->td_rak_ruang->get_data($id);
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function send_data() {
		if ($this->input->is_ajax_request()) :
			$this->form_validation->set_rules($this->td_rak_ruang->form_rules());
			if ($this->form_validation->run() == FALSE) :
				$str = $this->td_rak_ruang->form_warning($this->form_errs);
				$str['confirm'] = 'error';
			else :
				$data['kd_rak_ruang'] = $this->input->post('txtKd');
				$data['rak_kd'] = $this->input->post('txtKdRak');
				$data['gudang_kd'] = $this->input->post('txtKdGudang');
				$data['nm_rak_ruang'] = $this->input->post('txtNmRuang');
				$str = $this->td_rak_ruang->submit_data($data);
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function delete_data() {
		if ($this->input->is_ajax_request()) :
			$id = $this->input->get('id');
			$str = $this->td_rak_ruang->delete_data($id);
			
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}