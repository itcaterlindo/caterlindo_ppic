<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Data_warehouse extends MY_Controller {
	private $class_link = 'manage_items/manage_inventory/data_warehouse';
	private $form_errs = array('idErrNmGudang');

	public function __construct() {
		parent::__construct();

		$this->load->library(array('ssp', 'form_validation'));
		$this->load->helper(array('form', 'my_btn_access_helper', 'my_helper'));
		$this->load->model(array('m_builder', 'tm_gudang'));
	}

	public function index() {
		parent::administrator();
		$this->get_table();
	}

	public function get_table() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function open_table() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		$data = $this->tm_gudang->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function get_form() {
		$data['id'] = $this->input->get('id');
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = $this->form_errs;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function open_form() {
		$id = $this->input->get('id');
		$data = $this->tm_gudang->get_data($id);
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function send_data() {
		if ($this->input->is_ajax_request()) :
			$this->form_validation->set_rules($this->tm_gudang->form_rules());
			if ($this->form_validation->run() == FALSE) :
				$str = $this->tm_gudang->form_warning($this->form_errs);
				$str['confirm'] = 'error';
			else :
				$data['kd_gudang'] = $this->input->post('txtKd');
				$data['nm_gudang'] = $this->input->post('txtNmGudang');
				$str = $this->tm_gudang->submit_data($data);
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function delete_data() {
		if ($this->input->is_ajax_request()) :
			$id = $this->input->get('id');
			$str = $this->tm_gudang->delete_data($id);
			
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function data_rak() {
		$id = $this->input->get('id');
		$data = $this->tm_gudang->get_data($id);
		$_SESSION['modul_inventory']['kd_gudang'] = $data['kd_gudang'];
		$_SESSION['modul_inventory']['nm_gudang'] = $data['nm_gudang'];
	}
}