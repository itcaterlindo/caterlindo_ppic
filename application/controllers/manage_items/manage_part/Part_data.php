<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Part_data extends MY_Controller {
	private $class_link = 'manage_items/manage_part/part_data';
	private $class_linkLabourcost = 'manage_items/manage_part/part_labourcost';
	private $class_linkDetailPlate = 'manage_items/manage_part/part_detailplate';
	private $class_linkVersi = 'manage_items/manage_part/part_versi';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_rawmaterial','td_part', 'td_part_detail', 'tb_bagian', 'td_part_labourcost', 'td_part_detailplate', 'td_part_jenis', 'tm_part_main', 'tb_part_lastversion',
			'tb_admin', 'tb_part_state', 'td_part_state_user', 'td_part_overhead', 'td_part_state_log']);
	}

	public function index() {
        parent::administrator();
		parent::pnotify_assets();
        parent::select2_assets();
        parent::datetimepicker_assets();
		$this->table_box();
    }
    
	public function form_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$data['class_linkLabourcost'] = $this->class_linkLabourcost;
		$data['class_linkDetailPlate'] = $this->class_linkDetailPlate;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}
	
	// public function versi_box() {
	// 	if (!$this->input->is_ajax_request()){
	// 		exit('No direct script access allowed');
	// 	}
	// 	$sts = $this->input->get('sts');
	// 	$id = $this->input->get('id');

	// 	$data['sts'] = $sts;
	// 	$data['id'] = $id;
	// 	$data['class_link'] = $this->class_link;
	// 	$this->load->view('page/'.$this->class_link.'/versi_box', $data);
	// }
    
    public function form_master_main(){
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        $sts = $this->input->get('sts');
        $id = $this->input->get('id');

        if ($sts == 'edit' || $sts == 'duplicate'){
			$rowData = $this->tm_part_main->get_by_param (array('partmain_kd'=> $id))->row_array();			
			$data['rowData'] = $rowData;
			$data['rowVersi'] = $this->td_part->get_max_versi($id);
		}
		
		/** Opsi PartJenis */
		$opsiPartJenis = [];
		$actopsiPartJenis = $this->td_part_jenis->get_all()->result_array();
		foreach ($actopsiPartJenis as $eachPartJenis):
			$opsiPartJenis[$eachPartJenis['partjenis_kd']] = $eachPartJenis['partjenis_nama'];
		endforeach;

        $data['sts'] = $sts;
        $data['class_link'] = $this->class_link;
        $data['opsiPartJenis'] = $opsiPartJenis;
		$this->load->view('page/'.$this->class_link.'/form_master_main', $data);
    }
	
	public function table_box(){
		$partjenis_kd = $this->input->get('partjenis_kd', true);
		$partstate_kd = $this->input->get('partstate_kd', true);

		/** Opsi StatusJenis */
		$opsipartState = [];
		$actopsiPartSatate = $this->tb_part_state->get_all()->result_array();
		$opsipartState[''] = 'ALL';
		foreach ($actopsiPartSatate as $eachPartState):
			$opsipartState[$eachPartState['partstate_kd']] = ucwords(str_replace('_', ' ', $eachPartState['partstate_nama']));
		endforeach;

		/** Opsi PartJenis */
		$opsipartJenis = [];
		$actopsiPartJenis = $this->td_part_jenis->get_all()->result_array();
		$opsipartJenis[''] = 'ALL';
		foreach ($actopsiPartJenis as $eachPartJenis):
			$opsipartJenis[$eachPartJenis['partjenis_kd']] = $eachPartJenis['partjenis_nama'];
		endforeach;

		$data['opsiJenispencarian'] = [
			'' => 'ALL', 'jnsPart' => 'Jenis Part', 'statusPart' => 'Status Part'
		];
		$data['opsipartState'] = $opsipartState;
		$data['opsipartJenis'] = $opsipartJenis;
		$data['partjenis_kd'] = $partjenis_kd;
		$data['partstate_kd'] = $partstate_kd;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
		$partjenis_kd = $this->input->get('partjenis_kd', true);
		$partstate_kd = $this->input->get('partstate_kd', true);
        
		$data['class_link'] = $this->class_link;
		$data['partjenis_kd'] = $partjenis_kd;
		$data['partstate_kd'] = $partstate_kd;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $this->load->library(['ssp']);

		$partjenis_kd = $this->input->get('partjenis_kd', true);
		$partstate_kd = $this->input->get('partstate_kd', true);
        
		$data = $this->tm_part_main->ssp_table($partjenis_kd, $partstate_kd);
		
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
    }
    
	public function view_detail_box(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		
		$partmain_kd = $this->input->get('partmain_kd');
		$part_kd = $this->input->get('part_kd');

		if (empty($part_kd)) {
			$rowPart = $this->tb_part_lastversion->get_by_param (['partmain_kd' => $partmain_kd])->row();
			$part_kd = $rowPart->part_kd;
		}
		$data['header'] = $this->td_part->get_by_param_detail (['td_part.part_kd' => $part_kd])->row_array();
		$data['url'] = base_url().$this->class_link.'/view_detailtable_pdf?part_kd='.$part_kd;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/view_detail_box', $data);
	}

	public function view_detailtable_box() {
		parent::administrator();
		$part_kd = $this->input->get('part_kd');
		$partmain_kd = $this->input->get('partmain_kd');
		
		if (empty($part_kd)) {
			$rowPart = $this->tb_part_lastversion->get_by_param (['partmain_kd' => $partmain_kd])->row();
			$part_kd = $rowPart->part_kd;
			if (empty($part_kd)) {
				show_error('Belum ada versi default, masuk ke <strong> Detail Versi </strong> dahulu <br> <a href="'.base_url().$this->class_link.'"> Part Data </a>', '202', 'Error Default Versi');
			}
		}

		/** SpecalAction */
		$specialAction = false;
		$admin_kd = $this->session->userdata('kd_admin');
		$isApprover = $this->td_part_state_user->isApprover($admin_kd);
		$isAdmin = $this->tb_admin->isAdmin($admin_kd);
		if ($isApprover || $isAdmin) {
			$specialAction = true;
		}
		
		$data['header'] = $this->td_part->get_by_param_detail (['td_part.part_kd' => $part_kd])->row_array();
		$data['part_kd'] = $part_kd;
		$data['buttonState'] = $this->td_part->generateButtonState ($part_kd);
		$data['specialAction'] = $specialAction;
		$data['class_link'] = $this->class_link;
		$data['class_linkVersi'] = $this->class_linkVersi;
		
		$this->load->view('page/'.$this->class_link.'/view_detailtable_box', $data);
	}

	public function view_detailtable_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$part_kd = $this->input->get('part_kd', true);

		$data = $this->td_part->partcosting_data ($part_kd);
		$data['part_kd'] = $part_kd;
		$this->load->view('page/'.$this->class_link.'/view_detailtable_main', $data);
	}
	
	public function view_detailtable_pdf () {
		$this->load->library('Pdf');

		$part_kd = $this->input->get('part_kd', true);
		
		$dataPart = $this->td_part->partcosting_data ($part_kd);
		$data['keterangan'] = $this->td_part->get_by_param_detail(['td_part.part_kd' => $part_kd])->row_array();
		$data['konten'] = $this->load->view('page/'.$this->class_link.'/view_detailtable_main', $dataPart, true);
		$data['states'] = $this->td_part_state_log->get_last_log($part_kd)->result_array();
		
		$this->load->view('page/'.$this->class_link.'/view_detailtable_pdf', $data);
	}

	/** Table BOM dan Costing */
	public function view_detailtable_costing_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$part_kd = $this->input->get('part_kd', true);

		$data = $this->td_part->partcosting_data ($part_kd);
		$data['part_kd'] = $part_kd;
		$this->load->view('page/'.$this->class_link.'/view_detailtable_costing_main', $data);
	}
	
	public function view_detailtable_costing_pdf () {
		$this->load->library('Pdf');

		$part_kd = $this->input->get('part_kd', true);
		
		$dataPart = $this->td_part->partcosting_data ($part_kd);
		$data['keterangan'] = $this->td_part->get_by_param_detail(['td_part.part_kd' => $part_kd])->row_array();
		$data['konten'] = $this->load->view('page/'.$this->class_link.'/view_detailtable_main', $dataPart, true);
		$data['states'] = $this->td_part_state_log->get_last_log($part_kd)->result_array();
		
		$this->load->view('page/'.$this->class_link.'/view_detailtable_costing_pdf', $data);
	}

	public function form_ubahstate_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$part_kd = $this->input->get('part_kd', true);
		$partstate_kd = $this->input->get('partstate_kd', true);
		$specialAction = $this->input->get('specialAction', true);

		$partstate = $this->tb_part_state->get_all();

		/** Opsi PartJenis */
		$opsiPartstate = [];
		$partstates = $this->tb_part_state->get_all()->result_array();
		foreach ($partstates as $partstate):
			$opsiPartstate[$partstate['partstate_kd']] = $partstate['partstate_nama'];
		endforeach;

		/** Cek special state */
		$specialState = false;		
		if ($specialAction == 1) {
			$specialState = true;
		}

		$data['class_link'] = $this->class_link;
		$data['part_kd'] = $part_kd;
		$data['partstate_kd'] = $partstate_kd;
		$data['opsiPartstate'] = $opsiPartstate;
		$data['specialState'] = $specialState;
		$this->load->view('page/'.$this->class_link.'/form_ubahstate_main', $data);
	}

	public function view_detaillog_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$part_kd = $this->input->get('part_kd', true);

		$resultPartstateLog = $this->td_part_state_log->get_byparam_detail(['td_part_state_log.part_kd' => $part_kd])->result_array();
		$data['resultPartstateLog'] = $resultPartstateLog;
		$this->load->view('page/'.$this->class_link.'/view_detaillog_main', $data);		
	}

	public function action_ubahstate () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$part_kd = $this->input->post('txtpart_kd', true);
		$partstate_kd = $this->input->post('txtpartstate_kd', true);
		$partstate_note = $this->input->post('txtpartstate_note', true);
				
		$actUbah = $this->td_part->ubah_state ($part_kd, $partstate_kd, $partstate_note);
		if ($actUbah['status']) {
			$resp = array('code'=> 200, 'status' => 'Sukses', 'data_message'=>$actUbah);
		}else {
			$resp = array('code'=> 400, 'status' => 'Gagal', 'data_message'=>$actUbah, 'pesan' => 'Data tdk ditemukan');
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}
    
    public function action_master_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpartmain_nama', 'Part', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpartjenis_kd', 'Jenis Part', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpartmain_nama' => (!empty(form_error('txtpartmain_nama')))?buildLabel('warning', form_error('txtpartmain_nama', '"', '"')):'',
				'idErrpartjenis_kd' => (!empty(form_error('txtpartjenis_kd')))?buildLabel('warning', form_error('txtpartjenis_kd', '"', '"')):'',
			);
			
		}else { 
            $partmain_nama = $this->input->post('txtpartmain_nama', true);
			$partjenis_kd = $this->input->post('txtpartjenis_kd', true);
            $partmain_note = $this->input->post('txtpartmain_note', true);

			$part_name = !empty($partmain_nama) ? $partmain_nama:null;

			if(!empty($partmain_nama)){
				$name = substr($partmain_nama,0, 4);

				if($name != "PART"){
					$part_name = 'PART '. $part_name;
				}
			}

			
			$partmain_kd = $this->tm_part_main->create_code();
			$dataMain = array(
				'partmain_kd' => $partmain_kd,
				'partjenis_kd' => !empty($partjenis_kd) ? $partjenis_kd:null,
				'partmain_nama' => $part_name,
				'partmain_note' => !empty($partmain_note) ? $partmain_note:null,
				'partmain_tglinput' => date('Y-m-d H:i:s'),
				'partmain_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);
			
			$part_kd = $this->td_part->create_code();
			$data = array(
				'part_kd' => $part_kd,
				'partmain_kd' => $partmain_kd,
				'part_versi' => 1,
				'part_tglinput' => date('Y-m-d H:i:s'),
				'part_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$dataLastversion = [
				'partmain_kd' => $partmain_kd,
				'part_kd' => null,
			];

            $actMain = $this->tm_part_main->insert_data($dataMain);
			$act = $this->td_part->insert_data($data);
			$actVersion = $this->tb_part_lastversion->insert_data($dataLastversion);
			
            if ($actMain && $act && $actVersion){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => $data);
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}
	
	public function action_master_duplicate() {
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
		}
        
		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpartmain_nama', 'Part', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpartjenis_kd', 'Jenis Part', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpartmain_nama' => (!empty(form_error('txtpartmain_nama')))?buildLabel('warning', form_error('txtpartmain_nama', '"', '"')):'',
				'idErrpartjenis_kd' => (!empty(form_error('txtpartjenis_kd')))?buildLabel('warning', form_error('txtpartjenis_kd', '"', '"')):'',
			);
			
		}else {
            $partmain_kd = $this->input->post('txtpartmain_kd', true);
            $part_kd = $this->input->post('txtpart_kd', true);
			$partmain_nama = $this->input->post('txtpartmain_nama', true);
			$partjenis_kd = $this->input->post('txtpartjenis_kd', true);
			$partmain_note = $this->input->post('txtpartmain_note', true);
			
			$duplicatePartmainKd = $this->tm_part_main->create_code();
			$part_name = !empty($partmain_nama) ? $partmain_nama:null;

			if(!empty($partmain_nama)){
				$name = substr($partmain_nama,0, 4);

				if($name != "PART"){
					$part_name = 'PART '. $part_name;
				}
			}

			$dataMain = [
				'partmain_kd' => $duplicatePartmainKd,
				'partmain_nama' => $part_name,
                'partjenis_kd' => !empty($partjenis_kd) ? $partjenis_kd:null,
                'partmain_note' => !empty($partmain_note) ? $partmain_note:null,
				'partmain_tglinput' => date('Y-m-d H:i:s'),
				'partmain_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			];

			$dataLastversion = [
				'partmain_kd' => $duplicatePartmainKd,
				'part_kd' => null,
			];

			$actMain = $this->tm_part_main->insert_data($dataMain);
			$actDuplicate = $this->td_part->duplicate_part ($duplicatePartmainKd, $part_kd, 1);
			$actVersion = $this->tb_part_lastversion->insert_data($dataLastversion);

            if ($actMain && $actDuplicate && $actVersion){
                $resp = array('code'=> 201, 'status' => 'Sukses', 'pesan' => 'Terduplicate');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Duplicate');
            }
            
		}
        $resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}
    
    public function action_master_update() {
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
		}
        
		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpartmain_nama', 'Part', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpartjenis_kd', 'Jenis Part', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpartmain_nama' => (!empty(form_error('txtpartmain_nama')))?buildLabel('warning', form_error('txtpartmain_nama', '"', '"')):'',
				'idErrpartjenis_kd' => (!empty(form_error('txtpartjenis_kd')))?buildLabel('warning', form_error('txtpartjenis_kd', '"', '"')):'',
			);
			
		}else {
            $partmain_kd = $this->input->post('txtpartmain_kd', true);
            $part_kd = $this->input->post('txtpart_kd', true);
			$partmain_nama = $this->input->post('txtpartmain_nama', true);
			$partjenis_kd = $this->input->post('txtpartjenis_kd', true);
			$partmain_note = $this->input->post('txtpartmain_note', true);

			$lastversi = $this->td_part->generate_versi($partmain_kd);

			$part_name = !empty($partmain_nama) ? $partmain_nama:null;

			if(!empty($partmain_nama)){
				$name = substr($partmain_nama,0, 4);

				if($name != "PART"){
					$part_name = 'PART '. $part_name;
				}
			}
			
			$data = array(
                'partmain_nama' => $part_name,
				'partjenis_kd' => !empty($partjenis_kd) ? $partjenis_kd:null,
				'partmain_note' => !empty($partmain_note) ? $partmain_note:null,
				'partmain_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->tm_part_main->update_data(array('partmain_kd' => $partmain_kd),$data);
			
            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
            
		}
        $resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}
	    
	public function action_ubah_status() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$partmain_kd = $this->input->get('partmain_kd', TRUE);
		$partmain_status = $this->input->get('partmain_status', TRUE);
		
		if (!empty($partmain_kd)){
			$data = [
				'partmain_status' => $partmain_status,
				'partmain_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			];
			$act = $this->tm_part_main->update_data (['partmain_kd' => $partmain_kd], $data);
			if ( $act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terupdate');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Update');
			}
		}
		
		echo json_encode($resp);
    }

	public function push_sap()
	{
		$act = "";
			$part_main_kd = $this->input->get('id', true);

			$stts = $this->input->get('stts', true);

		if ($stts == 'bom') {
			$dataMain = array(
				'status_sap' => '1',
				'partmain_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);
			$act = $this->tm_part_main->update_data(array('partmain_kd' => $part_main_kd), $dataMain);
		}else{
			$dataMain = array(
				'status_sap_add_item' => '1',
				'partmain_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);
			$act = $this->tm_part_main->update_data(array('partmain_kd' => $part_main_kd), $dataMain);
		}

			

		if ($act) {
			$sql="SELECT * FROM (SELECT 
						tmp.partmain_kd,
						tmp.partmain_nama,
						tmp.partmain_note,
						tbb.bagian_kd, 
						tdpd.partdetail_kd,
						tmr.rm_kode, 
						tmr.rm_platepanjang,
						tdpd.partdetail_qty, 
						tdpdp.partdetailplate_kd,
						tdpdp.partdetailplate_jenis,
						tdpdp.partdetailplate_panjang AS P, 
						tdpdp.partdetailplate_lebar AS L, 
						tdpdp.partdetailplate_tebal AS T, 
						tdpdp.partdetailplate_massajenis AS MJ,
						tdpdp.partdetailplate_qty AS qty,
						ROUND(((tmr.rm_platepanjang * tdpdp.partdetailplate_lebar * tdpdp.partdetailplate_tebal) / 1000000 * tdpdp.partdetailplate_massajenis), 4) AS CUT_SIZE
						FROM tm_part_main AS tmp 
										LEFT JOIN tb_part_lastversion ON tmp.partmain_kd = tb_part_lastversion.partmain_kd
										LEFT JOIN td_part ON td_part.part_kd = tb_part_lastversion.part_kd
										LEFT JOIN td_part_detail AS tdpd ON td_part.part_kd = tdpd.part_kd
										LEFT JOIN td_part_detailplate AS tdpdp ON tdpd.partdetail_kd = tdpdp.partdetail_kd
										LEFT JOIN tm_rawmaterial AS tmr ON tmr.rm_kd = tdpd.rm_kd
										LEFT JOIN tb_bagian AS tbb ON tbb.bagian_kd = tdpd.bagian_kd
									WHERE tmp.partmain_kd = '$part_main_kd' AND tdpdp.partdetailplate_jenis = 'cut_size' GROUP BY tdpdp.partdetailplate_kd) AS pdpp
						UNION
						SELECT * FROM (SELECT 
						tmp.partmain_kd,
						tmp.partmain_nama,
						tmp.partmain_note,
						tbb.bagian_kd, 
						tdpd.partdetail_kd,
						tmr.rm_kode, 
						tmr.rm_platepanjang,
						tdpd.partdetail_qty, 
						tdpdp.partdetailplate_kd,
						tdpdp.partdetailplate_jenis,
						tdpdp.partdetailplate_panjang AS P, 
						tdpdp.partdetailplate_lebar AS L, 
						tdpdp.partdetailplate_tebal AS T, 
						tdpdp.partdetailplate_massajenis AS MJ,
						tdpdp.partdetailplate_qty AS qty,
						ROUND(SUM((((tdpdp.partdetailplate_panjang * tdpdp.partdetailplate_lebar * tdpdp.partdetailplate_tebal) / 1000000 * tdpdp.partdetailplate_massajenis) * tdpdp.partdetailplate_qty)), 4) AS CUT_SIZE
						FROM tm_part_main AS tmp 
										LEFT JOIN tb_part_lastversion ON tmp.partmain_kd = tb_part_lastversion.partmain_kd
										LEFT JOIN td_part ON td_part.part_kd = tb_part_lastversion.part_kd
										LEFT JOIN td_part_detail AS tdpd ON td_part.part_kd = tdpd.part_kd
										LEFT JOIN td_part_detailplate AS tdpdp ON tdpd.partdetail_kd = tdpdp.partdetail_kd
										LEFT JOIN tm_rawmaterial AS tmr ON tmr.rm_kd = tdpd.rm_kd
										LEFT JOIN tb_bagian AS tbb ON tbb.bagian_kd = tdpd.bagian_kd
									WHERE tmp.partmain_kd = '$part_main_kd' AND tdpdp.partdetailplate_jenis = 'main' GROUP BY tdpdp.partdetailplate_kd) AS pdpp
						UNION

						SELECT 
							tmp.partmain_kd,
							tmp.partmain_nama,
							tmp.partmain_note,
						tbb.bagian_kd, 
						tdpd.partdetail_kd,
						tmr.rm_kode, 
						tmr.rm_platepanjang,
						tdpd.partdetail_qty, 
						'' AS partdetailplate_kd,
						tdpdp.rm_kd AS partdetailplate_jenis,
						'' AS P, 
						'' AS L, 
						'' AS T, 
						'' AS MJ,
						'' AS qty,
						'' AS CUT_SIZE
						FROM tm_part_main AS tmp 
										LEFT JOIN tb_part_lastversion ON tmp.partmain_kd = tb_part_lastversion.partmain_kd
										LEFT JOIN td_part ON td_part.part_kd = tb_part_lastversion.part_kd
										LEFT JOIN td_part_detail AS tdpd ON td_part.part_kd = tdpd.part_kd
										LEFT JOIN tm_rawmaterial AS tmr ON tmr.rm_kd = tdpd.rm_kd
										LEFT JOIN td_part_detailplate AS tdpdp ON tdpd.partdetail_kd = tdpdp.partdetail_kd
										LEFT JOIN tb_bagian AS tbb ON tbb.bagian_kd = tdpd.bagian_kd
						WHERE tmp.partmain_kd = '$part_main_kd'";    

			$query = $this->db->query($sql);
			$res = $query->result_array();

			$qLabourcost = $this->td_part_labourcost->get_lc_by_partmain('tm_part_main.partmain_kd', $part_main_kd);
			$qOverhead = $this->td_part_overhead->get_ovh_by_partmain($part_main_kd);

			$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terupdate', 'data' => $res, 'data_labourcost' => $qLabourcost->result_array(), 'data_ovh' => $qOverhead->result_array() );
		} else {
			$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		}
		header('Content-Type: application/json');
		echo json_encode($resp);
	}

	public function rollback_sap()
	{
			$bom_kd = $this->input->get('id', true);
			$stts = $this->input->get('stts', true);
			$act = "";
		if ($stts == 'bom') {
			$dataMain = array(
				'status_sap' => '0',
				'partmain_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);
			$act = $this->tm_part_main->update_data(array('partmain_kd' => $bom_kd), $dataMain);
		}else{
			$dataMain = array(
				'status_sap_add_item' => '0',
				'partmain_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);
			$act = $this->tm_part_main->update_data(array('partmain_kd' => $bom_kd), $dataMain);
		}
			

		if ($act) {
			$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Gagal Push, Rollback Success!');
		} else {
			$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Rollback, Hub Admin!');
		}
		header('Content-Type: application/json');
		echo json_encode($resp);
	}

}
