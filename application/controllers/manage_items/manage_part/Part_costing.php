<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Part_costing extends MY_Controller {
	private $class_link = 'manage_items/manage_part/part_costing';
	private $class_link_partdata = 'manage_items/manage_part/part_data';
	private $class_linkLabourcost = 'manage_items/manage_part/part_labourcost';
	private $class_linkDetailPlate = 'manage_items/manage_part/part_detailplate';
	private $class_linkOverhead = 'manage_items/manage_part/part_overhead';
	private $class_linkVersi = 'manage_items/manage_part/part_versi';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_helper', 'my_btn_access_helper']);
        $this->load->model(['td_part', 'tb_part_lastversion', 'tm_part_main', 'tb_bagian', 'tm_rawmaterial', 'td_part_detail', 'tb_admin', 'td_part_detailplate', 'td_part_state_log']);
	}

	public function index() {
		parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
		parent::datetimepicker_assets();
        $partmain_kd = $this->input->get('partmain_kd');
        
		$data['partmain_kd'] = $partmain_kd;
		$data['class_link'] = $this->class_link;
		$data['class_link_partdata'] = $this->class_link_partdata;
		$data['header'] = $this->tm_part_main->get_by_param_detail (['tm_part_main.partmain_kd' => $partmain_kd])->row_array();
		$this->load->view('page/'.$this->class_link.'/partversi_box', $data);
    }
    
    public function partversi_table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		
		$partmain_kd = $this->input->get('partmain_kd');

		$data['resultData'] = $this->td_part->get_by_param_detail(['td_part.partmain_kd' => $partmain_kd])->result();
		$data['versi'] = $this->tb_part_lastversion->get_by_param(['partmain_kd' => $partmain_kd])->row();

        $data['partmain_kd'] = $partmain_kd;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/partversi_table_main', $data);
	}

	public function form_box() {
		parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
		parent::datetimepicker_assets();
		$sts = $this->input->get('sts');
        $id = $this->input->get('id');
        $id = url_decrypt($id);

		$data['sts'] = $sts;
        $data['id'] = $id;
        $data['rowDataMasterPart'] = $this->td_part->get_by_param_detail (array('td_part.part_kd'=> $id)) ->row_array();
		$data['class_link'] = $this->class_link;
		$data['class_linkLabourcost'] = $this->class_linkLabourcost;
		$data['class_linkDetailPlate'] = $this->class_linkDetailPlate;
		$data['class_linkOverhead'] = $this->class_linkOverhead;
		
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
        $partdetail_kd = $this->input->get('id');

		if ($sts == 'edit'){
			$act = $this->td_part_detail->get_by_id_detail ($partdetail_kd)->row_array();			
			$data['rowData'] = $act;
        }
        
		$data['partdetail_kd'] = $partdetail_kd;
		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function form_tabledetail_main() {
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        $sts = $this->input->get('sts');
		$id = $this->input->get('id');

        $data['id'] = $id;
        $data['sts'] = $sts ;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_tabledetail_main', $data);
	}

	public function form_tabledetail_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $this->load->library(['ssp']);

		$id = $this->input->get('id');

		$data = $this->td_part_detail->ssp_table_costing($id);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function partcosting_box () {
		parent::administrator();
        parent::pnotify_assets();
		$part_kd = $this->input->get('part_kd');
		$partmain_kd = $this->input->get('partmain_kd');

		if (empty($part_kd) && empty($partmain_kd)) {
			show_error('Part tidak ditemukan', '202', 'Error Default Part');
		}
		
		if (empty($part_kd)) {
			$rowPart = $this->tb_part_lastversion->get_by_param (['partmain_kd' => $partmain_kd])->row();
			$part_kd = $rowPart->part_kd;
		}
		
		$data['header'] = $this->td_part->get_by_param_detail (['td_part.part_kd' => $part_kd])->row_array();
		$data['part_kd'] = $part_kd;
		// $data['buttonState'] = $this->td_part->generateButtonState ($part_kd);
		$data['class_link'] = $this->class_link;
		$data['class_link_partdata'] = $this->class_link_partdata;
		$data['class_linkVersi'] = $this->class_linkVersi;
		
		$this->load->view('page/'.$this->class_link.'/partcosting_box', $data);	
	}

	public function view_detailtablecosting_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$part_kd = $this->input->get('part_kd', true);

		$data = $this->td_part->partcosting_data ($part_kd);
		$data['part_kd'] = $part_kd;
		$this->load->view('page/'.$this->class_link.'/view_detailtablecosting_main', $data);
	}

	function getDetailPart(){
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
        $id = $this->input->get('id');
        if (!empty($id)){
            $act = $this->td_part_detail->get_by_id_detail($id);
            if ($act->num_rows() > 0 ){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => '', 'data' => $act->row_array());
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Tidak Ditemukan');
            }
        }else{
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Kosong');
        }

        echo json_encode($resp);
	}

	public function getMaterialData(){
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $id = $this->input->get('id');
        if (!empty($id)){
            $act = $this->tm_rawmaterial->get_detail_material(array('tm_rawmaterial.rm_kd'=>$id))->row_array();
            $resp = array('code'=> 200, 'status' => 'Sukses', 'data' => $act);
        }else{
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Data tdk ditemukan');
        }
        
        echo json_encode($resp);
	}

	public function action_detail_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		if (!empty($id)){
			$actDel = $this->td_part_detail->delete_data($id);
			if ($actDel == true){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
			}
		}
		echo json_encode($resp);
	}	

	public function action_duplicatepart () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$part_kd = $this->input->get('part_kd');
		if (!empty($part_kd)) {
			$rowPart = $this->td_part->get_by_param (['part_kd' => $part_kd])->row_array();
			$versi = $this->td_part->generate_versi ($rowPart['partmain_kd']);
			$act = $this->td_part->duplicate_part ($rowPart['partmain_kd'], $part_kd, $versi);
			if ($act) {
				$resp = array('code'=> 200, 'status' => 'Sukses');
			}else {
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Duplikasi');
			}
		}else {
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Part Kosong');
		}
		echo json_encode($resp);
	}

	public function action_deletepart () {
		$this->load->model(['td_part_detailplate', 'td_part_overhead', 'td_part_labourcost']);
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$part_kd = $this->input->get('part_kd');
		if (!empty($part_kd)) {
			$this->db->trans_start();
			$delDetailPlate = $this->td_part_detailplate->delete_by_param(['part_kd' => $part_kd]);
			$delOverhead = $this->td_part_overhead->delete_by_param(['part_kd' => $part_kd]);
			$delLabourcost = $this->td_part_labourcost->delete_by_param(['part_kd' => $part_kd]);
			$delDetail = $this->td_part_detail->delete_by_param(['part_kd' => $part_kd]);
			$delPart = $this->td_part->delete_data($part_kd);
			$this->db->trans_complete();
			
			if ($this->db->trans_status() === FALSE){
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Duplikasi');
			}else {
				$resp = array('code'=> 200, 'status' => 'Sukses');
			}
		}else {
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Id Kosong');
		}
		echo json_encode($resp);
	}
	
	public function action_updateharga_partdetail () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpartdetail_hargaunit', 'Nama Material', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpartdetail_hargaunit' => (!empty(form_error('txtpartdetail_hargaunit')))?buildLabel('warning', form_error('txtpartdetail_hargaunit', '"', '"')):'',
			);
			
		}else {
			$partdetail_kd = $this->input->post('txtpartdetail_kd');
			$partdetail_hargaunit = $this->input->post('txtpartdetail_hargaunit');
			$partdetail_qty = $this->input->post('txtpartdetail_qty');

			$partdetail_hargatotal = (float) $partdetail_hargaunit * $partdetail_qty;
			$arrayUpdate = [
				'partdetail_hargaunit' => $partdetail_hargaunit,
				'partdetail_hargatotal' => $partdetail_hargatotal,
				'partdetail_hargaupdate' => date('Y-m-d H:i:s'),
			];
			$act = $this->td_part_detail->update_data(['partdetail_kd' => $partdetail_kd], $arrayUpdate);
			if ($act) {
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
			}else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

    public function action_updateharga_rm () {
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        $part_kd = $this->input->get('part_kd');

        if (!empty($part_kd)) {
			$act = $this->td_part_detail->update_harga ($part_kd); 
			if ($act) {
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'ID kosong');
			}
        }else {
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'ID kosong');
        }
		echo json_encode($resp);
	}

	public function cetak_costing()
	{
		parent::admin_print();
		$part_kd = $this->input->get('part_kd', true);
		$url = base_url().$this->class_link.'/view_detailtablecosting_main?part_kd='.$part_kd;
		$data['header'] = $this->td_part->get_by_param_detail (['td_part.part_kd' => $part_kd])->row_array();
		$data['state'] = $this->td_part_state_log->get_last_log($part_kd)->result_array();
		$data['class_link'] = $this->class_link;
		$data['url'] = $url;
		$this->load->view('page/'.$this->class_link.'/print_detail', $data);
	}
	
}
