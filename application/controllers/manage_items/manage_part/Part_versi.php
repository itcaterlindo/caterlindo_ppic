<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Part_versi extends MY_Controller {
	private $class_link = 'manage_items/manage_part/part_versi';
	private $class_link_partdata = 'manage_items/manage_part/part_data';
	private $class_linkLabourcost = 'manage_items/manage_part/part_labourcost';
	private $class_linkDetailPlate = 'manage_items/manage_part/part_detailplate';
	private $class_linkOverhead = 'manage_items/manage_part/part_overhead';
	private $class_link_costing = 'manage_items/manage_part/part_costing';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_helper', 'my_btn_access_helper']);
        $this->load->model(['td_part', 'tb_part_lastversion', 'tm_part_main', 'tb_bagian', 'tm_rawmaterial', 'td_part_detailpipe', 'td_part_detailplate', 'td_part_detail', 'tb_admin', 'tm_rawmaterial', 'td_admin_permission']);
	}

	public function index() {
		parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
		parent::datetimepicker_assets();
        $partmain_kd = $this->input->get('partmain_kd');
        
		$data['partmain_kd'] = $partmain_kd;
		$data['class_link'] = $this->class_link;
		$data['class_link_partdata'] = $this->class_link_partdata;
		$data['class_link_costing'] = $this->class_link_costing;
		$data['header'] = $this->tm_part_main->get_by_param_detail (['tm_part_main.partmain_kd' => $partmain_kd])->row_array();
		$this->load->view('page/'.$this->class_link.'/partversi_box', $data);
    }
    
    public function partversi_table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		
		$partmain_kd = $this->input->get('partmain_kd');

		$data['resultData'] = $this->td_part->get_by_param_detail(['td_part.partmain_kd' => $partmain_kd])->result();
		$data['versi'] = $this->tb_part_lastversion->get_by_param(['partmain_kd' => $partmain_kd])->row();

        $data['partmain_kd'] = $partmain_kd;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/partversi_table_main', $data);
	}

	public function form_box() {
		parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
		parent::datetimepicker_assets();
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');
		$id = url_decrypt($id);
		if ($id == false) {
			show_error('ID tidak ditemukan...!!! <br> <a href="'.base_url().'"> Home </a>', '202', 'Error ID');
		}

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$data['class_linkLabourcost'] = $this->class_linkLabourcost;
		$data['class_linkDetailPlate'] = $this->class_linkDetailPlate;
		$data['class_linkOverhead'] = $this->class_linkOverhead;
		$data['rowDataMasterPart'] = $this->td_part->get_by_param_detail (array('td_part.part_kd'=> $id)) ->row_array();
		
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
        $id = $this->input->get('id');

		if ($sts == 'edit'){
			$act = $this->td_part->get_by_param (array('part_kd'=> $id))->row_array();			
			$data['rowData'] = $act;
        }

        /** Master Part */
        $actPart = $this->td_part->get_by_param_detail (array('td_part.part_kd'=> $id)) ->row_array();

		/** Opsi Material */
		$actopsiMaterial = $this->tm_rawmaterial->get_by_param(['rm_filterbom' => 1])->result_array();
		$opsiMaterial[''] = '--Pilih Opsi--';
		foreach ($actopsiMaterial as $eachMaterial):
			$opsiMaterial[$eachMaterial['rm_kd']] = $eachMaterial['rm_kode'].' | '.$eachMaterial['rm_deskripsi'].'/'.$eachMaterial['rm_spesifikasi'];
		endforeach;
		
		/** Opsi Bagian New */
		$actopsiBagian = $this->tb_bagian->get_by_param(['bagian_lokasi' => 'WIP'])->result_array();
		foreach ($actopsiBagian as $eachBagian):
			$opsiBagian[$eachBagian['bagian_kd']] = $eachBagian['bagian_nama'];
		endforeach;

		/** Opsi jenis material */
		$opsiJenisMaterial = ['std' => 'Standart'];
		/** Check jenis part, apabila termasuk Project maka bisa input custom */
		if ($actPart['partjenis_kd'] == 'JNP0001') {
			$opsiJenisMaterial = ['std' => 'Standart', 'special' => 'Special'];
		}
        
        $data['opsiJenisMaterial'] = $opsiJenisMaterial;
        $data['opsiMaterial'] = $opsiMaterial;
        $data['opsiBagian'] = $opsiBagian;
		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function form_tabledetail_main() {
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        $sts = $this->input->get('sts');
		$id = $this->input->get('id');

        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_tabledetail_main', $data);
	}

	public function form_tabledetail_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $this->load->library(['ssp']);

		$id = $this->input->get('id');

		$data = $this->td_part_detail->ssp_table($id);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	function getDetailPart(){
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
        $id = $this->input->get('id');
        if (!empty($id)){
            $act = $this->td_part_detail->get_by_id_detail($id);
            if ($act->num_rows() > 0 ){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => '', 'data' => $act->row_array());
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Tidak Ditemukan');
            }
        }else{
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Kosong');
        }

        echo json_encode($resp);
	}

	public function getMaterialData(){
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $id = $this->input->get('id');
        if (!empty($id)){
            $act = $this->tm_rawmaterial->get_detail_material(array('tm_rawmaterial.rm_kd'=>$id))->row_array();
            $resp = array('code'=> 200, 'status' => 'Sukses', 'data' => $act);
        }else{
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Data tdk ditemukan');
        }
        
        echo json_encode($resp);
	}

	public function action_detail_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		if (!empty($id)){
			$actDel = $this->td_part_detail->delete_data($id);
			if ($actDel == true){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
			}
		}
		echo json_encode($resp);
	}	

	public function action_duplicatepart () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$part_kd = $this->input->get('part_kd');
		if (!empty($part_kd)) {
			$rowPart = $this->td_part->get_by_param (['part_kd' => $part_kd])->row_array();
			$versi = $this->td_part->generate_versi ($rowPart['partmain_kd']);
			$act = $this->td_part->duplicate_part ($rowPart['partmain_kd'], $part_kd, $versi);
			if ($act) {
				$resp = array('code'=> 200, 'status' => 'Sukses');
			}else {
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Duplikasi');
			}
		}else {
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Part Kosong');
		}
		echo json_encode($resp);
	}

	public function action_deletepart () {
		$this->load->model(['td_part_detailplate', 'td_part_overhead', 'td_part_labourcost']);
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$part_kd = $this->input->get('part_kd');
		if (!empty($part_kd)) {
			$this->db->trans_start();
			$delDetailPlate = $this->td_part_detailplate->delete_by_param(['part_kd' => $part_kd]);
			$delOverhead = $this->td_part_overhead->delete_by_param(['part_kd' => $part_kd]);
			$delLabourcost = $this->td_part_labourcost->delete_by_param(['part_kd' => $part_kd]);
			$delDetail = $this->td_part_detail->delete_by_param(['part_kd' => $part_kd]);
			$delPart = $this->td_part->delete_data($part_kd);
			$this->db->trans_complete();
			
			if ($this->db->trans_status() === FALSE){
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Duplikasi');
			}else {
				$resp = array('code'=> 200, 'status' => 'Sukses');
			}
		}else {
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Id Kosong');
		}
		echo json_encode($resp);
	}

	public function action_insertpart () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$partmain_kd = $this->input->get('partmain_kd');

		$part_kd = $this->td_part->create_code(); 
		$versi = $this->td_part->generate_versi($partmain_kd);
		$dataPart = array(
			'part_kd' => $part_kd,
			'partmain_kd' => $partmain_kd,
			'part_versi' => $versi,
			'part_tglinput' => date('Y-m-d H:i:s'),
			'part_tgledit' => date('Y-m-d H:i:s'),
			'admin_kd' => $this->session->userdata('kd_admin'),
		);
		$act = $this->td_part->insert_data($dataPart);
		if ($act){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'part_kd' => $part_kd);
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		}
		echo json_encode($resp);
	}

	function action_defaultpart () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$partmain_kd = $this->input->get('partmain_kd');
		$part_kd = $this->input->get('part_kd');

		$dataUpdate = [
			'part_kd' => $part_kd
		];
		$act = $this->tb_part_lastversion->update_data (['partmain_kd' => $partmain_kd], $dataUpdate);
		if ($act){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'part_kd' => $part_kd);
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		}
		echo json_encode($resp);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrm_kd', 'Nama Material', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpartdetail_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtbagian_kd', 'Bagian', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrrm_kd' => (!empty(form_error('txtrm_kd')))?buildLabel('warning', form_error('txtrm_kd', '"', '"')):'',
				'idErrpartdetail_qty' => (!empty(form_error('txtpartdetail_qty')))?buildLabel('warning', form_error('txtpartdetail_qty', '"', '"')):'',
				'idErrbagian_kd' => (!empty(form_error('txtbagian_kd')))?buildLabel('warning', form_error('txtbagian_kd', '"', '"')):'',
			);
			
		}else {
            $part_kd = $this->input->post('txtpart_kd', true);
            $rm_kd = $this->input->post('txtrm_kd', true);
            $partdetail_qty = $this->input->post('txtpartdetail_qty', true);
			$bagian_kd = $this->input->post('txtbagian_kd', true);
			$partdetail_jenis = $this->input->post('txtjenisMaterial', true);
			$partdetail_nama = $this->input->post('txtrm_nama', true);
			$partdetail_deskripsi = $this->input->post('txtrm_deskripsi', true);
			$partdetail_spesifikasi = $this->input->post('txtrm_spesifikasi', true);
			$rmsatuan_kd = $this->input->post('txtrmsatuan_kd', true);
			
			$data = array(
                'partdetail_kd' => $this->td_part_detail->create_code(),
                'part_kd' => $part_kd,
				'rm_kd' => $rm_kd,
				'partdetail_nama' => !empty($partdetail_nama) ? $partdetail_nama : null,
				'partdetail_deskripsi' => !empty($partdetail_deskripsi) ? $partdetail_deskripsi : null,
				'partdetail_spesifikasi' => !empty($partdetail_spesifikasi) ? $partdetail_spesifikasi : null,
                'bagian_kd' => $bagian_kd,
                'partdetail_jenis' => $partdetail_jenis,
                'partdetail_qty' => !empty($partdetail_qty) ? $partdetail_qty:null,
                'rmsatuan_kd' => !empty($rmsatuan_kd) ? $rmsatuan_kd:null,
				'partdetail_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

            $act = $this->td_part_detail->insert_data($data);
            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
    }

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrm_kd', 'Nama Material', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpartdetail_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtbagian_kd', 'Bagian', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrrm_kd' => (!empty(form_error('txtrm_kd')))?buildLabel('warning', form_error('txtrm_kd', '"', '"')):'',
				'idErrpartdetail_qty' => (!empty(form_error('txtpartdetail_qty')))?buildLabel('warning', form_error('txtpartdetail_qty', '"', '"')):'',
				'idErrbagian_kd' => (!empty(form_error('txtbagian_kd')))?buildLabel('warning', form_error('txtbagian_kd', '"', '"')):'',
			);
			
		}else {
            $partdetail_kd = $this->input->post('txtpartdetail_kd', true);
            $rm_kd = $this->input->post('txtrm_kd', true);
            $partdetail_qty = $this->input->post('txtpartdetail_qty', true);
			$bagian_kd = $this->input->post('txtbagian_kd', true);
			$partdetail_nama = $this->input->post('txtrm_nama', true);
			$partdetail_deskripsi = $this->input->post('txtrm_deskripsi', true);
			$partdetail_spesifikasi = $this->input->post('txtrm_spesifikasi', true);
			$rmsatuan_kd = $this->input->post('txtrmsatuan_kd', true);
			
			$data = array(
				'rm_kd' => $rm_kd,
				'partdetail_nama' => !empty($partdetail_nama) ? $partdetail_nama : null,
				'partdetail_deskripsi' => !empty($partdetail_deskripsi) ? $partdetail_deskripsi : null,
				'partdetail_spesifikasi' => !empty($partdetail_spesifikasi) ? $partdetail_spesifikasi : null,
                'bagian_kd' => $bagian_kd,
				'partdetail_qty' => !empty($partdetail_qty) ? $partdetail_qty:null,
				'rmsatuan_kd' => !empty($rmsatuan_kd) ? $rmsatuan_kd:null,
				'partdetail_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);
			$act = $this->td_part_detail->update_data(array('partdetail_kd' => $partdetail_kd), $data);
			
			$plate = $this->td_part_detailplate->get_by_param(['partdetail_kd' => $partdetail_kd])->row();
			$pipe = $this->td_part_detailpipe->get_by_param(['partdetail_kd' => $partdetail_kd])->row();

			$material = $this->tm_rawmaterial->get_by_param(['rm_kd' => $rm_kd])->row();

			$pesan = '';

			if($plate){
				$data_update_plate = array(
					'rm_kd' => $rm_kd,
					'partdetailplate_tebal' => $material->rm_platetebal,
					'partdetailplate_tgledit' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				);

				$act_part_plate = $this->td_part_detailplate->update_data(array('partdetail_kd' => $partdetail_kd), $data_update_plate);
				if($act_part_plate){
					$pesan = 'Header & Detail(Part Detail Plate) Berhasil Update.';
				}else{
					$pesan = 'Header Berhasil Update. & Detail(Part Detail Plate) Gagal Update ';
				}
			}else if($pipe){
				$data_update_pipe = array(
					'rm_kd' => $rm_kd,
					'partdetailplate_tgledit' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				);
				$act_part_pipe = $this->td_part_detailplate->update_data(array('partdetail_kd' => $partdetail_kd), $data_update_pipe);
				if($act_part_pipe){
					$pesan = 'Header & Detail(Part Detail Pipe) Berhasil Update.';
				}else{
					$pesan = 'Header Berhasil Update. & Detail(Part Detail Pipe) Gagal Update ';
				}
			}	

            
            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => $pesan, 'mess' => $material ? $material->rm_platetebal : 'Tidak ada Dua dua nya');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan Header -- '+$pesan);
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
    }
}
