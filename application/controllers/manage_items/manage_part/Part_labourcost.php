<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Part_labourcost extends MY_Controller {
	private $class_link = 'manage_items/manage_part/part_labourcost';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_rawmaterial','td_part', 'td_part_labourcost', 'tb_bagian']);
	}
    
	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
        $id = $this->input->get('id');
        
		if ($sts == 'edit'){
			$act = $this->td_part->get_by_param (array('part_kd'=> $id))->row_array();			
			$data['rowData'] = $act;
        }

        /** Opsi Bagian New */
		$actopsiBagian = $this->tb_bagian->get_all()->result_array();
		foreach ($actopsiBagian as $eachBagian):
			$opsiBagian[$eachBagian['bagian_kd']] = $eachBagian['bagian_nama'];
        endforeach;
        
		/** Opsi Satuan New */
        $opsiSatuan = array(
            'Persons' => 'Persons',
            'Team' => 'Team',
        );

        $data['opsiSatuan'] = $opsiSatuan;
        $data['opsiBagian'] = $opsiBagian;
		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
    }
    
	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        
        $sts = $this->input->get('sts');
		$id = $this->input->get('id');
		
        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $this->load->library(['ssp']);
        $id = $this->input->get('id');
        
		$data = $this->td_part_labourcost->ssp_table($id);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function tablecosting_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');
		
        $data['id'] = $id;
        $data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/tablecosting_main', $data);
	}

	public function tablecosting_data () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);
        $id = $this->input->get('id');
        
		$data = $this->td_part_labourcost->ssp_tablecosting($id);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function formharga_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
        $id = $this->input->get('id');
		
		$data['rowData'] = $this->td_part_labourcost->get_by_id_detail($id)->row_array();
		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/formharga_main', $data);
    }
	
	function getDetailLabourcost(){
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
        $id = $this->input->get('id');
        if (!empty($id)){
            $act = $this->td_part_labourcost->get_by_id_detail($id);
            if ($act->num_rows() > 0 ){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => '', 'data' => $act->row_array());
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Tidak Ditemukan');
            }
        }else{
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Kosong');
        }

        echo json_encode($resp);
    }

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpartlabourcost_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtbagian_kd', 'Bagian', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpartlabourcost_durasi', 'Durasi', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpartlabourcost_qty' => (!empty(form_error('txtpartlabourcost_qty')))?buildLabel('warning', form_error('txtpartlabourcost_qty', '"', '"')):'',
				'idErrbagian_kd' => (!empty(form_error('txtbagian_kd')))?buildLabel('warning', form_error('txtbagian_kd', '"', '"')):'',
				'idErrpartlabourcost_durasi' => (!empty(form_error('txtpartlabourcost_durasi')))?buildLabel('warning', form_error('txtpartlabourcost_durasi', '"', '"')):'',
			);
			
		}else {
            $part_kd = $this->input->post('txtpart_kd', true);
            $partlabourcost_qty = $this->input->post('txtpartlabourcost_qty', true);
            $partlabourcost_satuan = $this->input->post('txtlabourcost_satuan', true);
            $partlabourcost_durasi = $this->input->post('txtpartlabourcost_durasi', true);            
            $bagian_kd = $this->input->post('txtbagian_kd', true);
			
			$data = array(
                'partlabourcost_kd' => $this->td_part_labourcost->create_code(),
                'part_kd' => $part_kd,
                'bagian_kd' => $bagian_kd,
                'partlabourcost_qty' => !empty($partlabourcost_qty) ? $partlabourcost_qty:null,
                'partlabourcost_satuan' => !empty($partlabourcost_satuan) ? $partlabourcost_satuan:null,
                'partlabourcost_durasi' => !empty($partlabourcost_durasi) ? $partlabourcost_durasi:null,
				'partlabourcost_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

            $act = $this->td_part_labourcost->insert_data($data);
            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
    }

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpartlabourcost_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtbagian_kd', 'Bagian', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpartlabourcost_qty' => (!empty(form_error('txtpartlabourcost_qty')))?buildLabel('warning', form_error('txtpartlabourcost_qty', '"', '"')):'',
				'idErrbagian_kd' => (!empty(form_error('txtbagian_kd')))?buildLabel('warning', form_error('txtbagian_kd', '"', '"')):'',
			);
			
		}else {
			$partlabourcost_kd = $this->input->post('txtpartlaburcost_kd', true);
            $partlabourcost_qty = $this->input->post('txtpartlabourcost_qty', true);
            $partlabourcost_satuan = $this->input->post('txtlabourcost_satuan', true);
            $partlabourcost_durasi = $this->input->post('txtpartlabourcost_durasi', true);            
            $bagian_kd = $this->input->post('txtbagian_kd', true);
			
			$data = array(
                'bagian_kd' => $bagian_kd,
                'partlabourcost_qty' => !empty($partlabourcost_qty) ? $partlabourcost_qty:null,
                'partlabourcost_satuan' => !empty($partlabourcost_satuan) ? $partlabourcost_satuan:null,
                'partlabourcost_durasi' => !empty($partlabourcost_durasi) ? $partlabourcost_durasi:null,
				'partlabourcost_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

            $act = $this->td_part_labourcost->update_data(array('partlabourcost_kd' => $partlabourcost_kd), $data);
            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}
	
	public function action_updateharga() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpartlabourcost_hargaunit', 'Harga', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpartlabourcost_hargaunit' => (!empty(form_error('txtpartlabourcost_hargaunit')))?buildLabel('warning', form_error('txtpartlabourcost_hargaunit', '"', '"')):'',
			);
			
		}else {
			$partlabourcost_kd = $this->input->post('txtpartlabourcost_kd', true);
			$partlabourcost_hargaunit = $this->input->post('txtpartlabourcost_hargaunit', true);
			$partlabourcost_durasi = $this->input->post('txtpartlabourcost_durasi', true);
			$partlabourcost_qty = $this->input->post('txtpartlabourcost_qty', true);

			$partlabourcost_hargatotal = (float) $partlabourcost_hargaunit * $partlabourcost_durasi * $partlabourcost_qty;
			
			$data = array(
                'partlabourcost_hargaunit' => !empty($partlabourcost_hargaunit) ? $partlabourcost_hargaunit:null,
                'partlabourcost_hargatotal' => $partlabourcost_hargatotal,
				'partlabourcost_hargaupdate' => date('Y-m-d H:i:s'),
			);

            $act = $this->td_part_labourcost->update_data(array('partlabourcost_kd' => $partlabourcost_kd), $data);
            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
    }
	    
	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		if (!empty($id)){
			$actDelDetail = $this->td_part_labourcost->delete_data($id);
			$actDel = $this->td_part->delete_data($id);
			if ($actDel == true){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
			}
		}
		
		echo json_encode($resp);
	}

	public function action_updateharga_batch () {
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        $part_kd = $this->input->get('id');

        if (!empty($part_kd)) {
			$act = $this->td_part_labourcost->update_harga ($part_kd); 
			if ($act) {
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'ID kosong');
			}
        }else {
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'ID kosong');
        }
		echo json_encode($resp);
	}
	
	function temp() {
		$part_kd = 'PART0000000095';
		$aa = $this->td_part_labourcost->update_harga($part_kd);

	}
	
}
