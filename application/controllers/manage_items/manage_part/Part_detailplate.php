<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Part_detailplate extends MY_Controller {
	private $class_link = 'manage_items/manage_part/part_detailplate';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_part_detailplate', 'td_part_detail']);
	}
    
	public function form_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		
		$sts = $this->input->get('sts', true);
		$partdetail_kd = $this->input->get('partdetail_kd', true);
		$partdetailplate_kd = $this->input->get('partdetailplate_kd', true);

		if ($sts == 'edit'){
			$data['rowPartDetailPlate'] = $this->td_part_detailplate->get_by_param (array('partdetailplate_kd' => $partdetailplate_kd))->row_array();
		}
		
		$opsiJenis = array(
			'main' => 'Main',
			'additional' => 'Additional',
			'cut_size' => 'Cut Size',
		);
		
		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$data['partdetail_kd'] = $partdetail_kd;
		$data['rowPartDetail'] = $this->td_part_detail->get_by_param_detail(array('partdetail_kd' => $partdetail_kd))->row_array();
		$data['opsiJenis'] = $opsiJenis;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}
    
	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		
		$partdetail_kd = $this->input->get('partdetail_kd');
		
		$data['class_link'] = $this->class_link;
		$data['partdetail_kd'] = $partdetail_kd;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);
		
		$partdetail_kd = $this->input->get('partdetail_kd');

		$data = $this->td_part_detailplate->ssp_table($partdetail_kd);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}
	
	public function tablecosting_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		
		$partdetail_kd = $this->input->get('partdetail_kd');
		
		$data['class_link'] = $this->class_link;
		$data['partdetail_kd'] = $partdetail_kd;
		$this->load->view('page/'.$this->class_link.'/tablecosting_main', $data);
	}

	public function tablecosting_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);
		
		$partdetail_kd = $this->input->get('partdetail_kd');

		$data = $this->td_part_detailplate->ssp_tablecosting($partdetail_kd);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}
	
	public function get_partDetailPlate(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$partdetailplate_kd = $this->input->get('partdetailplate_kd');
		
		if (!empty($partdetailplate_kd)){
            $row_partdetailplate = $this->db->select('td_part_detailplate.*, tm_rawmaterial.*')
								->from('td_part_detailplate')
								->join('td_part_detail', 'td_part_detailplate.partdetail_kd=td_part_detail.partdetail_kd', 'left')
								->join('tm_rawmaterial', 'td_part_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
								->where('td_part_detailplate.partdetailplate_kd', $partdetailplate_kd)
								->get();
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => '', 'data' => $row_partdetailplate->row_array());
        }else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Tidak Ditemukan');
        }

        echo json_encode($resp);
	}

	public function get_cutsizeplate() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$partdetail_kd = $this->input->get('partdetail_kd');

		if (!empty($partdetail_kd)){
            $partdetail = $this->td_part_detail->get_by_param_detail(['td_part_detail.partdetail_kd' => $partdetail_kd]);
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => '', 'data' => $partdetail->row_array());
        }else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Tidak Ditemukan');
        }

        echo json_encode($resp);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpartdetailplate_jenis', 'Jenis', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpartdetailplate_panjang', 'Panjang', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpartdetailplate_lebar', 'Lebar', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpartdetailplate_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpartdetailplate_jenis' => (!empty(form_error('txtpartdetailplate_jenis')))?buildLabel('warning', form_error('txtpartdetailplate_jenis', '"', '"')):'',
				'idErrpartdetailplate_panjang' => (!empty(form_error('txtpartdetailplate_panjang')))?buildLabel('warning', form_error('txtpartdetailplate_panjang', '"', '"')):'',
				'idErrpartdetailplate_lebar' => (!empty(form_error('txtpartdetailplate_lebar')))?buildLabel('warning', form_error('txtpartdetailplate_lebar', '"', '"')):'',
				'idErrpartdetailplate_qty' => (!empty(form_error('txtpartdetailplate_qty')))?buildLabel('warning', form_error('txtpartdetailplate_qty', '"', '"')):'',
			);
			
		}else {
			
            $partdetail_kd = $this->input->post('txtpartdetail_kd', true);
            $partdetailplate_panjang = $this->input->post('txtpartdetailplate_panjang', true);
            $partdetailplate_lebar = $this->input->post('txtpartdetailplate_lebar', true);
            $partdetailplate_qty = $this->input->post('txtpartdetailplate_qty', true);
            $partdetailplate_jenis = $this->input->post('txtpartdetailplate_jenis', true);
			$partdetailplate_keterangan = $this->input->post('txtpartdetailplate_keterangan', true);    
			$part_kd = $this->input->post('txtplatepart_kd', true);
			$rm_kd = $this->input->post('txtplaterm_kd', true);
			$partdetailplate_tebal = $this->input->post('txtrm_platetebal', true);
			$partdetailplate_massajenis = $this->input->post('txtrm_platemassajenis', true);
			
			if ($partdetailplate_jenis == 'cut_size') {
				$cekCutsize = $this->td_part_detailplate->get_by_param (['partdetail_kd' => $partdetail_kd, 'partdetailplate_jenis' => $partdetailplate_jenis])->num_rows();
				if (!empty($cekCutsize)) {
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Cutsize sudah ada');
					$resp['csrf'] = $this->security->get_csrf_hash();
					echo json_encode($resp);die();
				}
			}
			$data = array(
                'partdetailplate_kd' => $this->td_part_detailplate->create_code(),
                'part_kd' => !empty($part_kd) ? $part_kd:null,
                'partdetail_kd' => $partdetail_kd,
                'rm_kd' => !empty($rm_kd) ? $rm_kd:null,
                'partdetailplate_panjang' => !empty($partdetailplate_panjang) ? $partdetailplate_panjang:null,
                'partdetailplate_lebar' => !empty($partdetailplate_lebar) ? $partdetailplate_lebar:null,
                'partdetailplate_qty' => !empty($partdetailplate_qty) ? $partdetailplate_qty:null,
                'partdetailplate_tebal' => !empty($partdetailplate_tebal) ? $partdetailplate_tebal:null,
                'partdetailplate_massajenis' => !empty($partdetailplate_massajenis) ? $partdetailplate_massajenis:null,
                'partdetailplate_jenis' => !empty($partdetailplate_jenis) ? $partdetailplate_jenis:null,
                'partdetailplate_keterangan' => !empty($partdetailplate_keterangan) ? $partdetailplate_keterangan:null,
				'partdetailplate_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

            $act = $this->td_part_detailplate->insert_data($data);
            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}
	
	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpartdetailplate_jenis', 'Jenis', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpartdetailplate_panjang', 'Panjang', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpartdetailplate_lebar', 'Lebar', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpartdetailplate_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpartdetailplate_jenis' => (!empty(form_error('txtpartdetailplate_jenis')))?buildLabel('warning', form_error('txtpartdetailplate_jenis', '"', '"')):'',
				'idErrpartdetailplate_panjang' => (!empty(form_error('txtpartdetailplate_panjang')))?buildLabel('warning', form_error('txtpartdetailplate_panjang', '"', '"')):'',
				'idErrpartdetailplate_lebar' => (!empty(form_error('txtpartdetailplate_lebar')))?buildLabel('warning', form_error('txtpartdetailplate_lebar', '"', '"')):'',
				'idErrpartdetailplate_qty' => (!empty(form_error('txtpartdetailplate_qty')))?buildLabel('warning', form_error('txtpartdetailplate_qty', '"', '"')):'',
			);
			
		}else {
			
            $partdetail_kd = $this->input->post('txtpartdetail_kd', true);
            $partdetailplate_panjang = $this->input->post('txtpartdetailplate_panjang', true);
            $partdetailplate_lebar = $this->input->post('txtpartdetailplate_lebar', true);
            $partdetailplate_qty = $this->input->post('txtpartdetailplate_qty', true);
            $partdetailplate_jenis = $this->input->post('txtpartdetailplate_jenis', true);
			$partdetailplate_keterangan = $this->input->post('txtpartdetailplate_keterangan', true);    
			$part_kd = $this->input->post('txtplatepart_kd', true);
			$rm_kd = $this->input->post('txtplaterm_kd', true);
			$partdetailplate_kd = $this->input->post('txtpartdetailplate_kd', true);
			$partdetailplate_tebal = $this->input->post('txtrm_platetebal', true);
			$partdetailplate_massajenis = $this->input->post('txtrm_platemassajenis', true);
			
			$data = array(
                'part_kd' => !empty($part_kd) ? $part_kd:null,
                'partdetail_kd' => $partdetail_kd,
                'rm_kd' => !empty($rm_kd) ? $rm_kd:null,
                'partdetailplate_panjang' => !empty($partdetailplate_panjang) ? $partdetailplate_panjang:null,
                'partdetailplate_lebar' => !empty($partdetailplate_lebar) ? $partdetailplate_lebar:null,
				'partdetailplate_qty' => !empty($partdetailplate_qty) ? $partdetailplate_qty:null,
				'partdetailplate_tebal' => !empty($partdetailplate_tebal) ? $partdetailplate_tebal:null,
                'partdetailplate_massajenis' => !empty($partdetailplate_massajenis) ? $partdetailplate_massajenis:null,
                'partdetailplate_jenis' => !empty($partdetailplate_jenis) ? $partdetailplate_jenis:null,
                'partdetailplate_keterangan' => !empty($partdetailplate_keterangan) ? $partdetailplate_keterangan:null,
				'partdetailplate_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

            $act = $this->td_part_detailplate->update_data(array('partdetailplate_kd' => $partdetailplate_kd), $data);
            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Update');
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
    }
	    
	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		if (!empty($id)){
			$actDel = $this->td_part_detailplate->delete_data($id);
			if ($actDel == true){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
			}
		}
		
		echo json_encode($resp);
	}

	public function action_updateharga () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        $partdetail_kd = $this->input->get('partdetail_kd');

        if (!empty($partdetail_kd)) {
			$act = $this->td_part_detailplate->update_harga ($partdetail_kd); 
			if ($act) {
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Update');
			}
        }else {
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'ID kosong');
        }
		echo json_encode($resp);
	}

	function temp () {
		$act = $this->td_part_detailplate->update_harga ('DPRT0000000461'); 
		echo json_encode($act);
	}

	
}
