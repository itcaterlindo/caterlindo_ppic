<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Part_overhead extends MY_Controller {
	private $class_link = 'manage_items/manage_part/part_overhead';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_part', 'tb_part_overhead', 'tb_bagian', 'td_part_overhead', 'td_part_detailplate']);
	}
    
	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
        $id = $this->input->get('id');
		$stsOverhead = $this->input->get('stsoverhead');
        $partoverhead_kd = $this->input->get('partoverhead_kd');
        
		if ($stsOverhead == 'edit'){
			$act = $this->td_part_overhead->get_by_param(array('partoverhead_kd'=> $partoverhead_kd))->row_array();
			$data['rowData'] = $act;
        }

        /** Master Part */
        $actPart = $this->td_part->get_by_param_detail (array('td_part.part_kd'=> $id)) ->row_array();
        
        /** Opsi Bagian New */
        $actopsiOverhead = $this->tb_part_overhead->get_all()->result_array();
        $opsiOverhead = [];
		foreach ($actopsiOverhead as $eachOverhead):
			$opsiOverhead[$eachOverhead['overhead_kd']] = $eachOverhead['overhead_nama'];
        endforeach;

		$data['stsOverhead'] = $stsOverhead;
        $data['opsiOverhead'] = $opsiOverhead;
        $data['rowDataMasterPart'] = $actPart;
		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
    }
    
	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        
        $sts = $this->input->get('sts');
		$id = $this->input->get('id');
		
        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $this->load->library(['ssp']);
        $id = $this->input->get('id');
        
		$data = $this->td_part_overhead->ssp_table($id);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}
	
	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtoverhead_kd', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpartoverhead_qty', 'Bagian', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErroverhead_kd' => (!empty(form_error('txtoverhead_kd')))?buildLabel('warning', form_error('txtoverhead_kd', '"', '"')):'',
				'idErrpartoverhead_qty' => (!empty(form_error('txtpartoverhead_qty')))?buildLabel('warning', form_error('txtpartoverhead_qty', '"', '"')):'',
			);
			
		}else {
            $part_kd = $this->input->post('txtpart_kd', true);
            $overhead_kd = $this->input->post('txtoverhead_kd', true);      
			$partoverhead_qty = $this->input->post('txtpartoverhead_qty', true);
			$pilihsemua = $this->input->post('txtpilihSemua', true);
			
			if ($pilihsemua) {
				$actDelAll = $this->td_part_overhead->delete_by_param(['part_kd' => $part_kd]);
				$overheads = $this->tb_part_overhead->get_all()->result_array();
				$partoverhead_kd = $this->td_part_overhead->create_code();
				foreach ($overheads as $overhead) {
					$partoverhead_hargaunit = $overhead['overhead_harga'];
					$partoverhead_hargatotal = $partoverhead_hargaunit * $partoverhead_qty;
					$dataBatch[] = [
						'partoverhead_kd' => $partoverhead_kd,
						'part_kd' => $part_kd,
						'overhead_kd' => $overhead['overhead_kd'],
						'partoverhead_qty' => !empty($partoverhead_qty) ? $partoverhead_qty:null,
						'partoverhead_hargaunit' => $partoverhead_hargaunit,
						'partoverhead_hargatotal' => $partoverhead_hargatotal,
						'partoverhead_tglinput' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					];
					$partoverhead_kd++;
				}
				
				$act = $this->td_part_overhead->insert_batch($dataBatch);
			}else {
				$qHargaoverhead = $this->tb_part_overhead->get_by_param(['overhead_kd' => $overhead_kd])->row_array();
				$hargaUnit = $qHargaoverhead['overhead_harga'];
				$hargaTotal = (float) $hargaUnit * $partoverhead_qty;

				$data = array(
					'partoverhead_kd' => $this->td_part_overhead->create_code(),
					'part_kd' => $part_kd,
					'overhead_kd' => !empty($overhead_kd) ? $overhead_kd:null,
					'partoverhead_qty' => !empty($partoverhead_qty) ? $partoverhead_qty:null,
					'partoverhead_hargaunit' => !empty($hargaUnit) ? $hargaUnit:0,
					'partoverhead_hargatotal' => !empty($hargaTotal) ? $hargaTotal:0,
					'partoverhead_tglinput' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				);
	
				$act = $this->td_part_overhead->insert_data($data);
			}
			
            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
    }

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtoverhead_kd', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpartoverhead_qty', 'Bagian', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErroverhead_kd' => (!empty(form_error('txtoverhead_kd')))?buildLabel('warning', form_error('txtoverhead_kd', '"', '"')):'',
				'idErrpartoverhead_qty' => (!empty(form_error('txtpartoverhead_qty')))?buildLabel('warning', form_error('txtpartoverhead_qty', '"', '"')):'',
			);
			
		}else {
			$partoverhead_kd = $this->input->post('txtpartoverhead_kd', true);
			$part_kd = $this->input->post('txtpart_kd', true);
            $overhead_kd = $this->input->post('txtoverhead_kd', true);      
			$partoverhead_qty = $this->input->post('txtpartoverhead_qty', true);
			
			$qHargaoverhead = $this->tb_part_overhead->get_by_param(['overhead_kd' => $overhead_kd])->row_array();
			$hargaUnit = $qHargaoverhead['overhead_harga'];
			$hargaTotal = (float) $hargaUnit * $partoverhead_qty;

			$data = array(
                'part_kd' => $part_kd,
                'overhead_kd' => !empty($overhead_kd) ? $overhead_kd:null,
                'partoverhead_qty' => !empty($partoverhead_qty) ? $partoverhead_qty:null,
                'partoverhead_hargaunit' => !empty($hargaUnit) ? $hargaUnit:0,
                'partoverhead_hargatotal' => !empty($hargaTotal) ? $hargaTotal:0,
				'partoverhead_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

            $act = $this->td_part_overhead->update_data(array('partoverhead_kd' => $partoverhead_kd), $data);
            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
    }
	    
	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		if (!empty($id)){
			$actDelDetail = $this->td_part_overhead->delete_data($id);
			$actDel = $this->td_part->delete_data($id);
			if ($actDel == true){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
			}
		}
		
		echo json_encode($resp);
	}
	
}
