<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Part_bagian extends MY_Controller {
	private $class_link = 'manage_items/manage_part/part_bagian';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tb_bagian']);
	}

	public function index() {
		parent::administrator();
		parent::pnotify_assets();
		$this->table_box();
	}

	public function form_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		if ($sts == 'edit'){
			$actFGuser = $this->tb_bagian->get_by_param (array('bagian_kd'=> $id))->row_array();	
			$data['id'] = $actFGuser['bagian_kd'];
			$data['bagian_nama'] = $actFGuser['bagian_nama'];
			$data['bagian_lokasi'] = $actFGuser['bagian_lokasi'];
			$data['pic'] = $actFGuser['pic'];
		}

		$data['opsiBagianLokasi'] = ['WIP' => 'WIP', 'WH' => 'WH', 'GENERAL' => 'General'];
		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->tb_bagian->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtbagian_kd', 'Kode Bagian', 'required', ['required' => '{field} tidak boleh kosong!']);	
		$this->form_validation->set_rules('txtbagian_nama', 'Nama Bagian', 'required', ['required' => '{field} tidak boleh kosong!']);	
		$this->form_validation->set_rules('txtpic', 'Nama PIC', 'required', ['required' => '{field} tidak boleh kosong!']);	

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrbagian_kd' => (!empty(form_error('txtbagian_kd')))?buildLabel('warning', form_error('txtbagian_kd', '"', '"')):'',
				'idErrbagian_nama' => (!empty(form_error('txtbagian_nama')))?buildLabel('warning', form_error('txtbagian_nama', '"', '"')):'',
				'idErrpic' => (!empty(form_error('txtpic')))?buildLabel('warning', form_error('txtpic', '"', '"')):'',
			);
			
		}else {
			$bagian_kd = $this->input->post('txtbagian_kd');
			$bagian_nama = $this->input->post('txtbagian_nama');
			$bagian_lokasi = $this->input->post('txtbagian_lokasi');
			$pic = $this->input->post('txtpic');
			
			$data = array(
				'bagian_kd' => $bagian_kd,
				'bagian_nama' => strtoupper($bagian_nama),
				'bagian_lokasi' => $bagian_lokasi,
				'pic' => $pic,
				'bagian_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

            /** Cek kode */
            $actCek = $this->tb_bagian->get_by_param (array('bagian_kd' => $bagian_kd));
            if ($actCek->num_rows() == 0){
                $act = $this->tb_bagian->insert_data($data);
                if ($act){
                    $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
                }else{
                    $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
                }
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Kode Sudah Ada');
            }
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
    }
    
	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtbagian_nama', 'Tipe Admin', 'required', ['required' => '{field} tidak boleh kosong!']);	
		$this->form_validation->set_rules('txtpic', 'Nama PIC', 'required', ['required' => '{field} tidak boleh kosong!']);	

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrRmgroup_nama' => (!empty(form_error('txtbagian_nama')))?buildLabel('warning', form_error('txtbagian_nama', '"', '"')):'',
				'idErrpic' => (!empty(form_error('txtpic')))?buildLabel('warning', form_error('txtpic', '"', '"')):'',
			);
			
		}else {
			$bagian_kd = $this->input->post('txtbagian_kd');
			$bagian_nama = $this->input->post('txtbagian_nama');
			$bagian_lokasi = $this->input->post('txtbagian_lokasi');
			$pic = $this->input->post('txtpic');
			
			$data = array(
				'bagian_nama' => strtoupper($bagian_nama),
				'bagian_lokasi' => $bagian_lokasi,
				'pic' => $pic,
				'bagian_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->tb_bagian->update_data (array('bagian_kd' => $bagian_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$bagian_kd = $this->input->get('id', TRUE);
		
		$actDel = $this->tb_bagian->delete_data($bagian_kd);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
