<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Part_detailpipe extends MY_Controller {
	private $class_link = 'manage_items/manage_part/part_detailpipe';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_part_detail', 'td_part', 'tm_rawmaterial', 'td_part_detailpipe']);
	}
    
	public function form_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		
		$sts = $this->input->get('sts', true);
		$part_kd = $this->input->get('part_kd', true);
		$partdetail_kd = $this->input->get('partdetail_kd', true);
		$rm_kd = $this->input->get('rm_kd', true);
		$bagian_kd = $this->input->get('bagian_kd', true);
		$jenismaterial = $this->input->get('jenismaterial', true);

		if(!empty($partdetail_kd)) {
			$partdetail = $this->td_part_detail->get_by_param(['partdetail_kd' => $partdetail_kd])->row_array();
			$rm_kd = $partdetail['rm_kd'];
		}
		
        $data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
        $data['part_kd'] = $part_kd;
        $data['bagian_kd'] = $bagian_kd;
        $data['jenismaterial'] = $jenismaterial;
        $data['partdetail_kd'] = $partdetail_kd;
		$data['rowPart'] = $this->td_part->get_by_param_detail(['part_kd' => $part_kd])->row_array();
		$data['rowRM'] = $this->tm_rawmaterial->get_detail_material(array('rm_kd' => $rm_kd))->row_array();
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}
    
	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		
		$partdetail_kd = $this->input->get('partdetail_kd');
		
		$data['class_link'] = $this->class_link;
		$data['partdetail_kd'] = $partdetail_kd;
		$data['resultDetailPlate'] = $this->td_part_detailpipe->get_by_param_detail(['partdetail_kd' => $partdetail_kd])->result_array();
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}
	
	public function get_partDetailPipe(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$partdetailpipe_kd = $this->input->get('partdetailpipe_kd');
		
		if (!empty($partdetailpipe_kd)){
            $row_partdetailpipe = $this->td_part_detailpipe->get_by_param(array('partdetailpipe_kd' => $partdetailpipe_kd));
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => '', 'data' => $row_partdetailpipe->row_array());
        }else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Tidak Ditemukan');
        }

        echo json_encode($resp);
	}

	private function action_insert_partdetail ($part_kd, $bagian_kd, $rm_kd, $jenismaterial, $qty) {
		$partdetail_kd = $this->td_part_detail->create_code();
		$rowRM = $this->tm_rawmaterial->get_by_param(['rm_kd' => $rm_kd])->row_array();
		
		$data = array(
			'partdetail_kd' => $partdetail_kd,
			'part_kd' => $part_kd,
			'rm_kd' => $rm_kd,
			'partdetail_nama' => !empty($rowRM['rm_nama']) ? $rowRM['rm_nama'] : null,
			'partdetail_deskripsi' => !empty($rowRM['rm_deskripsi']) ? $rowRM['rm_deskripsi'] : null,
			'partdetail_spesifikasi' => !empty($rowRM['rm_spesifikasi']) ? $rowRM['rm_spesifikasi'] : null,
			'bagian_kd' => $bagian_kd,
			'partdetail_jenis' => $jenismaterial,
			'partdetail_qty' => !empty($qty) ? $qty:null,
			'rmsatuan_kd' => !empty($rowRM['rmsatuan_kd']) ? $rowRM['rmsatuan_kd']:null,
			'partdetail_tglinput' => date('Y-m-d H:i:s'),
			'admin_kd' => $this->session->userdata('kd_admin'),
		);
		$act = $this->td_part_detail->insert_data($data);
		return $partdetail_kd;
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpartdetailpipe_panjang', 'Panjang', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpartdetailpipe_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpartdetailpipe_panjang' => (!empty(form_error('txtpartdetailpipe_panjang')))?buildLabel('warning', form_error('txtpartdetailpipe_panjang', '"', '"')):'',
				'idErrpartdetailpipe_qty' => (!empty(form_error('txtpartdetailpipe_qty')))?buildLabel('warning', form_error('txtpartdetailpipe_qty', '"', '"')):'',
			);
			
		}else {
			$part_kd = $this->input->post('txtpipepart_kd', true);
			$partdetail_kd = $this->input->post('txtpartdetail_kd', true);
			$rm_kd = $this->input->post('txtpiperm_kd', true);
            $partdetailpipe_panjang = $this->input->post('txtpartdetailpipe_panjang', true);
            $partdetailpipe_qty = $this->input->post('txtpartdetailpipe_qty', true);
			$partdetailpipe_keterangan = $this->input->post('txtpartdetailpipe_keterangan', true);
			$bagian_kd = $this->input->post('txtpipebagian_kd', true);
			$jenismaterial = $this->input->post('txtpipejenismaterial', true); 
			
			if (empty($partdetail_kd)) {
				$partdetail_kd = $this->action_insert_partdetail ($part_kd, $bagian_kd, $rm_kd, $jenismaterial, $partdetailpipe_qty);
			}

			$data = array(
                'partdetailpipe_kd' => $this->td_part_detailpipe->create_code(),
                'part_kd' => !empty($part_kd) ? $part_kd:null,
                'partdetail_kd' => $partdetail_kd,
                'rm_kd' => !empty($rm_kd) ? $rm_kd:null,
                'partdetailpipe_panjang' => !empty($partdetailpipe_panjang) ? $partdetailpipe_panjang:null,
                'partdetailpipe_qty' => !empty($partdetailpipe_qty) ? $partdetailpipe_qty:null,
                'partdetailpipe_keterangan' => !empty($partdetailpipe_keterangan) ? $partdetailpipe_keterangan:null,
				'partdetailpipe_tglinput' => date('Y-m-d H:i:s'),
				'partdetailpipe_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

            $act = $this->td_part_detailpipe->insert_data($data);
            if ($act){
				$actUpdateQty = $this->td_part_detailpipe->update_qty_detail($partdetail_kd);
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['partdetail_kd' => $partdetail_kd]);
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}
	
	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpartdetailpipe_panjang', 'Panjang', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpartdetailpipe_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpartdetailpipe_panjang' => (!empty(form_error('txtpartdetailpipe_panjang')))?buildLabel('warning', form_error('txtpartdetailpipe_panjang', '"', '"')):'',
				'idErrpartdetailpipe_qty' => (!empty(form_error('txtpartdetailpipe_qty')))?buildLabel('warning', form_error('txtpartdetailpipe_qty', '"', '"')):'',
			);
			
		}else {
            $partdetail_kd = $this->input->post('txtpartdetail_kd', true);
            $partdetailpipe_kd = $this->input->post('txtpartdetailpipe_kd', true);
            $rm_kd = $this->input->post('txtpiperm_kd', true);
            $partdetailpipe_panjang = $this->input->post('txtpartdetailpipe_panjang', true);
			$partdetailpipe_qty = $this->input->post('txtpartdetailpipe_qty', true);
			$partdetailpipe_keterangan = $this->input->post('txtpartdetailpipe_keterangan', true);
			
			$data = array(
                'rm_kd' => !empty($rm_kd) ? $rm_kd:null,
                'partdetailpipe_panjang' => !empty($partdetailpipe_panjang) ? $partdetailpipe_panjang:null,
                'partdetailpipe_qty' => !empty($partdetailpipe_qty) ? $partdetailpipe_qty:null,
                'partdetailpipe_keterangan' => !empty($partdetailpipe_keterangan) ? $partdetailpipe_keterangan:null,
				'partdetailpipe_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

            $act = $this->td_part_detailpipe->update_data(array('partdetailpipe_kd' => $partdetailpipe_kd), $data);
            if ($act){
				$actUpdateQty = $this->td_part_detailpipe->update_qty_detail($partdetail_kd);
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Update');
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
    }
	    
	public function action_delete () {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$partdetailpipe_kd = $this->input->get('id');
		if (!empty($partdetailpipe_kd)) {
			$rowPartdetailpipe = $this->td_part_detailpipe->get_by_param (['partdetailpipe_kd' => $partdetailpipe_kd])->row_array();
			$actDel = $this->td_part_detailpipe->delete_data($partdetailpipe_kd);

			if ($actDel) {
				$actUpdateQty = $this->td_part_detailpipe->update_qty_detail($rowPartdetailpipe['partdetail_kd']);
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
			} else {
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
			}
		}else {
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Id Kosong');
		}
		echo json_encode($resp);
	}
	
}
