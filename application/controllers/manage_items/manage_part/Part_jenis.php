<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Part_jenis extends MY_Controller {
	private $class_link = 'manage_items/manage_part/part_jenis';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_part_jenis']);
	}

	public function index() {
		parent::administrator();
		parent::pnotify_assets();
		$this->table_box();
	}

	public function form_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		if ($sts == 'edit'){
			$actFGuser = $this->td_part_jenis->get_by_param (array('partjenis_kd'=> $id))->row_array();			
			$data['id'] = $actFGuser['partjenis_kd'];
			$data['partjenis_nama'] = $actFGuser['partjenis_nama'];
			$data['partjenis_generatewo'] = $actFGuser['partjenis_generatewo'];
		}

		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->td_part_jenis->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpartjenis_nama', 'Nama Jenis Part', 'required', ['required' => '{field} tidak boleh kosong!']);	

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpartjenis_nama' => (!empty(form_error('txtpartjenis_nama')))?buildLabel('warning', form_error('txtpartjenis_nama', '"', '"')):'',
			);
			
		}else {
			$partjenis_nama = $this->input->post('txtpartjenis_nama');
			$partjenis_generatewo = $this->input->post('txtpartjenis_generatewo');
			
			$data = array(
				'partjenis_kd' => $this->td_part_jenis->create_code(),
				'partjenis_nama' => strtoupper($partjenis_nama),
				'partjenis_generatewo' => $partjenis_generatewo,
				'partjenis_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->td_part_jenis->insert_data($data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
    }
 
	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpartjenis_nama', 'Nama Jenis Part', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpartjenis_nama' => (!empty(form_error('txtpartjenis_nama')))?buildLabel('warning', form_error('txtpartjenis_nama', '"', '"')):'',
			);
			
		}else {
			$partjenis_kd = $this->input->post('txtpartjenis_kd');
			$partjenis_nama = $this->input->post('txtpartjenis_nama');
			$partjenis_generatewo = $this->input->post('txtpartjenis_generatewo');
			
			$data = array(
				'partjenis_nama' => strtoupper($partjenis_nama),
				'partjenis_generatewo' => $partjenis_generatewo,
				'partjenis_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->td_part_jenis->update_data (array('partjenis_kd' => $partjenis_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$partjenis_kd = $this->input->get('id', TRUE);
		
		$actDel = $this->td_part_jenis->delete_data($partjenis_kd);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
