<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relasi_product extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->model('m_builder');
		$this->load->helper(array('form', 'html', 'my_helper'));
	}

	function index() {
		parent::administrator();

		$data['t_box_class'] = 'box-primary';
		$title = $this->uri->segment_array();
		$title = end($title);
		if (strpos($title, '_') !== FALSE ) :
			$title = str_replace('_', ' ', $title);
		endif;
		$title = ucwords($title);
		$data['table_title'] = $title;
		$data['table_alert'] = 'idAlertTable';
		$data['table_loader'] = 'idLoaderTable';
		$data['table_name'] = 'idTable';
		$data['table_overlay'] = 'idTableOverlay';
		$data['btn_add'] = FALSE;

		$this->load->css('assets/admin_assets/plugins/select2/select2.min.css');
		$this->load->css('assets/admin_assets/plugins/select2/select2-bootstrap.min.css');
		$this->load->js('assets/admin_assets/plugins/select2/select2.full.min.js');
		$this->load->section('scriptJS', 'script/manage_items/manage_product/relasi_product/scriptJS');
		// $this->data_admin_form();
		$this->load->view('page/admin/v_table', $data);
	}

	function data_lihat() {
		if ($this->input->is_ajax_request()) :
			/* tabel property */
			$t_data['btn'] = TRUE;
			$t_data['t_header'] = array(
				'<th style="width:1%;">Barcode</th>',
				'<th style="width:5%;">Code</th>',
				'<th style="width:5%;" class="always">Desc</th>',
				'<th style="width:5%;" class="always">Dimension</th>',
				'<th style="width:5%;">Group</th>',
				'<th style="width:1%;">Category</th>',
			);
			$t_data['t_uri'] = base_url().'manage_items/manage_product/relasi_product/data_table/';
			$t_data['t_order'] = '[2, "asc"]';
			// $t_data['datatable_properties'] = array('"responsive": true,');
			$t_data['t_col_def'] = '{"data": 0.4, target: 4}';

			$this->load->view('page/admin/table_ssp', $t_data);
		endif;
	}

	function data_table() {
		if (!$this->input->is_ajax_request()) :
            exit('No direct script access allowed');
        else :
            $this->load->library('ssp');

			$table = 'tm_barang';

			$primaryKey = 'kd_barang';

			$columns = array(
			    array( 'db' => 'a.kd_barang',
						'dt' => 1, 'field' => 'kd_barang',
						'formatter' => function($d){
							$btn_relasi = '';
							$btn_edit = '';
							$divider = '';
							$btn_delete = '';
							if (cek_permission('PRODUCTRELASI_VIEW')) :
								$btn_relasi = '<a id="lihat" title="Lihat Relasi" href="javascript:void(0);" onclick="relasiData(\''.$d.'\')"><i class="fa fa-code-fork"></i> Lihat Relasi</a>';
							endif;
							if (cek_permission('PRODUCTRELASI_UPDATE')) :
								$btn_edit = '<a id="edit" title="Edit Data" href="javascript:void(0);" onclick="editData(\''.$d.'\')"><i class="fa fa-pencil"></i> Edit Relasi</a>';
								$divider = '<li class="divider"></li>';
							endif;
							if (cek_permission('PRODUCTRELASI_DELETE')) :
								$btn_delete = '<a id="delete" title="Hapus Data" href="javascript:void(0);" onclick="return confirm(\'Apakah anda yakin?\')?hapusData(\''.$d.'\'):false;"><i class="fa fa-trash"></i> Hapus Data</a>';
							endif;

							$btn = '<div align="center"><div class="btn-group"><button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">Opsi <span class="caret"></span></button><ul class="dropdown-menu"><li>'.$btn_relasi.'</li><li>'.$btn_edit.'</li>'.$divider.'<li>'.$btn_delete.'</li></ul></div></div>';
							
							return $btn;
						} ),
			    array( 'db' => 'a.item_barcode',
			    		'dt' => 2, 'field' => 'item_barcode',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'a.item_code',
			    		'dt' => 3, 'field' => 'item_code',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'a.deskripsi_barang',
			    		'dt' => 4, 'field' => 'deskripsi_barang',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);
			    			$d = word_break($d, 3);

			    			return $d;
			    		} ),
			    array( 'db' => 'a.dimensi_barang',
			    		'dt' => 5, 'field' => 'dimensi_barang',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);
			    			$d = word_break($d, 3);

			    			return $d;
			    		} ),
			    array( 'db' => 'b.nm_group',
			    		'dt' => 6, 'field' => 'nm_group',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'c.nm_kat_barang',
			    		'dt' => 7, 'field' => 'nm_kat_barang',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			);

			$sql_details = sql_connect();

			$joinQuery = "
				FROM
					tm_barang a
				LEFT JOIN tm_group_barang b ON a.group_barang_kd = b.kd_group_barang
				LEFT JOIN tm_kat_barang c ON a.kat_barang_kd = c.kd_kat_barang
			";
			$where = "";

			echo json_encode(
			    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $where )
			);
		endif;
	}

	function data_form(){
		if ($this->input->is_ajax_request()) :
			// Form data
			$id = $this->input->get('id');
			$link = $this->input->get('link');
			$jml = $this->m_builder->getRow('tm_barang_relasi', $data = array('barang_kd' => $id), 'num_rows');
			if ($jml > 0) :
				$kata = ($link == 'data_form')?'Edit':'Detail';
				$det = $this->m_builder->getRow('tm_barang_relasi', $data = array('barang_kd' => $id), 'row');
				$id_form = 'idFormEdit';
				$btn_form = '<i class="fa fa-edit"></i> Edit Data';
				$kd_mrelasi_barang = $this->security->xss_clean($det->kd_mrelasi_barang);
				$kd_parent = $this->security->xss_clean($det->barang_kd);
				$barang = $this->m_builder->getRow('tm_barang', $data = array('kd_barang' => $kd_parent), 'row');
				$item_barcode = $this->security->xss_clean($barang->item_barcode);
				$item_code_parent = $this->security->xss_clean($barang->item_code);
				$data['f_box_class'] = 'box-warning';
				$data['form_title'] = $kata.' Relasi Barang : '.$item_barcode.' / '.$item_code_parent;
				// ambil data child
				$child = $this->m_builder->getRow('td_barang_relasi', array('mrelasi_barang_kd' => $kd_mrelasi_barang), 'result');
				foreach ($child as $detail) :
					$kd_child[$detail->kd_drelasi_barang] = $this->security->xss_clean($detail->barang_kd);
				endforeach;
			else :
				$kata = ($link == 'data_form')?'Input':'Detail';
				$id_form = 'idFormInput';
				$btn_form = '<i class="fa fa-save"></i> Simpan Data';
				$kd_mrelasi_barang = '';
				$kd_parent = $id;
				$barang = $this->m_builder->getRow('tm_barang', array('kd_barang' => $kd_parent), 'row');
				$item_barcode = $this->security->xss_clean($barang->item_barcode);
				$item_code_parent = $this->security->xss_clean($barang->item_code);
				$kd_child['kosong'] = '';
				$data['f_box_class'] = 'box-info';
				$data['form_title'] = $kata.' Relasi Barang : '.$item_barcode.' / '.$item_code_parent;
			endif;

			// buat form untuk child
			$this->db->trans_start();
			$this->db->from('tm_barang');
			$q_barang = $this->db->get();
			$r_barang = $q_barang->result();
			$pil_barang = array('' => '-- Pilih Child Product --');
			foreach ($r_barang as $d_barang) :
				$kd_barang = $this->security->xss_clean($d_barang->kd_barang);
				$item_code = $this->security->xss_clean($d_barang->item_code);
				$deskripsi_barang = $this->security->xss_clean($d_barang->deskripsi_barang);
				$pil_barang[$kd_barang] = $item_code.' - '.$deskripsi_barang;
			endforeach;
			$no_child = 0;
			$form_child = '';
			foreach ($kd_child as $child => $value) :
				// select option untuk pilih child
				$no_child++;
				$sel_barang = $value;
				$attr_barang = array('id' => 'idSelChild'.$no_child, 'class' => 'form-control select2');

				// Isi dari detail child
				if (!empty($sel_barang)) :
					$child_det = $this->m_builder->getRow('tm_barang', array('kd_barang' => $sel_barang), 'row');
					$child_barcode = $child_det->item_barcode;
					$child_code = $child_det->item_code;
					$child_nm = $child_det->deskripsi_barang;
					$child_dimension = $child_det->dimensi_barang;
				else :
					$child_barcode = '[Tidak Ada Data]';
					$child_code = '[Tidak Ada Data]';
					$child_nm = '[Tidak Ada Data]';
					$child_dimension = '[Tidak Ada Data]';
				endif;

				// js button untuk add child
				if ($no_child <= 1) :
					$btn_js = '<a name="btnAdd" id="idBtnAdd" title="Tambah Child" class="btn btn-sm btn-primary btnAdd"><i class="fa fa-plus"></i></a>';
				else :
					$btn_js = '<a name="btnSub" id="idBtnSub" title="Hapus Child" class="btn btn-sm btn-danger btnSub"><i class="fa fa-minus"></i></a>';
				endif;

				// label
				$label_child = $no_child <= 1?'Relasi Child':'';

				if ($link == 'data_form') :
					$form_child .= '<div class="form-group">
						<label for="idSelChild'.$no_child.'" class="col-md-2 control-label">'.$label_child.'</label>
						<div class="col-md-3">'
						.form_dropdown('selChild[]', $pil_barang, $sel_barang, $attr_barang).
						'</div><div class="col-md-1">'.$btn_js.'</div></div>';
				elseif ($link == 'data_detail') :
					$form_child .= '<div class="form-group">
						<label for="idSelChild'.$no_child.'" class="col-md-2 control-label">'.$label_child.'</label>
						<div class="col-md-6">'
						.$no_child.'.) '.$child_barcode.' - '.$child_code.' - '.$child_nm.' - '.$child_dimension.
						'</div></div>';
				endif;
			endforeach;
			$this->db->trans_complete();

			$data['f_attr'] = 'id="idBoxForm"';
			$data['form_url'] = '';
			$data['form_att'] = array('id' => $id_form, 'class' => 'form-horizontal');
			$data['form_hidden'] = '';
			$data['form_close_att'] = '';
			$data['form_field'] = array(
				'item_barcode' => array(
					'<div class="form-group">',
					form_input(array('type' => 'hidden', 'name' => 'txtKdMRelasi', 'value' => $kd_mrelasi_barang)),
					form_input(array('type' => 'hidden', 'name' => 'txtKdParent', 'id' => 'idTxtKdParent', 'value' => $kd_parent)),
					form_input(array('type' => 'hidden', 'name' => 'txtBarcode', 'id' => 'idTxtKdParent', 'value' => $item_barcode)),
					form_input(array('type' => 'hidden', 'name' => 'txtCode', 'id' => 'idTxtKdParent', 'value' => $item_code_parent)),
					'</div>',
					$form_child,
				),
			);

			if ($link == 'data_form') :
				$data['form_btn'] = array(
					'btn_submit' => array(
						'<button type="submit" name="btnSubmit" id="idBtnSubmit" class="btn btn-warning pull-right">',
						$btn_form,
						'</button>',
					),
				);
			endif;
			$data['form_box'] = 'idBoxFormBody';
			$data['form_alert'] = 'idAlertForm';
			$data['form_overlay'] = 'idOverlayForm';

			$this->load->section('form_js', 'script/manage_items/manage_product/relasi_product/form_js');
			$this->load->view('page/admin/v_form', $data);
		endif;
	}

	function form_child() {
		if ($this->input->is_ajax_request()) :
			$this->db->from('tm_barang');
			$q_barang = $this->db->get();
			$r_barang = $q_barang->result();
			$pil_barang = array('' => '-- Pilih Child Product --');
			foreach ($r_barang as $d_barang) :
				$kd_barang = $this->security->xss_clean($d_barang->kd_barang);
				$item_code = $this->security->xss_clean($d_barang->item_code);
				$deskripsi_barang = $this->security->xss_clean($d_barang->deskripsi_barang);
				$pil_barang[$kd_barang] = $item_code.' - '.$deskripsi_barang;
			endforeach;
			$sel_barang = '';
			$attr_barang = array('id' => 'idSelChild', 'class' => 'form-control select2');

			// js button untuk add child			
			$btn_js = '<a name="btnSub" id="idBtnSub" title="Hapus Child" class="btn btn-sm btn-danger btnSub"><i class="fa fa-minus"></i></a>';

			$data['form_child'] = '<div class="form-group">
				<label for="idSelChild" class="col-md-2 control-label"></label>
				<div class="col-md-3">'.
				form_dropdown('selChild[]', $pil_barang, $sel_barang, $attr_barang).
				'</div><div class="col-md-1">'.$btn_js.'</div></div>';

			header('Content-Type: application/json');
			echo json_encode($data);
		endif;
	}

	function submit_form($act){
		if ($this->input->is_ajax_request()) :
			$this->load->library(array('form_validation'));
			$str['csrf'] = $this->security->get_csrf_hash();

			if (!empty($_POST['selChild'])) :
				$this->form_validation->set_rules('selChild[]', 'Pilihan Child', 'required',
					array(
						'required' => '{field} tidak boleh kosong!',
					)
				);
			endif;

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'errValidation';
				$str['idErrForm'] = (!empty(form_error('selChild[]')))?buildLabel('warning', form_error('selChild[]', '"', '"')):'';
			else :
				/*
				** Jika validasi berhasil dan tidak ada error
				** Data dari form akan diinputkan kedalam tabel
				** Jika query input return true
				** alert success akan ditampilakan, selain itu error (kesalahan sistem!)
				*/
				$item_barcode = $this->input->post('txtBarcode');
				$item_code = $this->input->post('txtCode');
				$kd_parent = $this->input->post('txtKdParent');
				$kd_child = $this->input->post('selChild');
				if ($act == 'input') :
					$conds = array(
						'select' => 'tgl_input, kd_mrelasi_barang',
						'order_by' => 'tgl_input DESC, kd_mrelasi_barang DESC',
						'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
					);
					$kd_mrelasi_barang = $this->m_builder->buat_kode('tm_barang_relasi', $conds, 6, 'MRL');

					$this->db->trans_start();
					// Insert into master relasi
					$data = array(
						'kd_mrelasi_barang' => $kd_mrelasi_barang,
						'barang_kd' => $kd_parent,
						'tgl_input' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					$aksi = $this->db->insert('tm_barang_relasi', $data);

					// Insert into detail relasi
					foreach ($kd_child as $child) :
						$c_conds = array(
							'select' => 'tgl_input, kd_drelasi_barang',
							'order_by' => 'tgl_input DESC, kd_drelasi_barang DESC',
							'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
						);
						$kd_drelasi_barang = $this->m_builder->buat_kode('td_barang_relasi', $c_conds, 6, 'DRL');
						$d_detail = array(
							'kd_drelasi_barang' => $kd_drelasi_barang,
							'mrelasi_barang_kd' => $kd_mrelasi_barang,
							'barang_kd' => $child,
							'tgl_input' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin'),
						);
						$aksi = $this->db->insert('td_barang_relasi', $d_detail);
					endforeach;
					$this->db->trans_complete();

					$label_err = 'menambahkan';
				elseif ($act == 'edit') :
					$kd_mrelasi_barang = $this->input->post('txtKdMRelasi');

					$this->db->trans_start();
					// Update master relasi
					$data = array(
						'barang_kd' => $kd_parent,
						'tgl_edit' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					$where = array('kd_mrelasi_barang' => $kd_mrelasi_barang);
					$aksi = $this->db->update('tm_barang_relasi', $data, $where);

					// Delete detail relasi
					$del_detail = $this->db->delete('td_barang_relasi', array('mrelasi_barang_kd' => $kd_mrelasi_barang));

					// Update detail relasi
					foreach ($kd_child as $child) :
						$c_conds = array(
							'select' => 'tgl_input, kd_drelasi_barang',
							'order_by' => 'tgl_input DESC, kd_drelasi_barang DESC',
							'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
						);
						$kd_drelasi_barang = $this->m_builder->buat_kode('td_barang_relasi', $c_conds, 6, 'DRL');
						$d_detail = array(
							'kd_drelasi_barang' => $kd_drelasi_barang,
							'mrelasi_barang_kd' => $kd_mrelasi_barang,
							'barang_kd' => $child,
							'tgl_input' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin'),
						);
						$aksi = $this->db->insert('td_barang_relasi', $d_detail);
					endforeach;
					$this->db->trans_complete();

					$label_err = 'mengubah';
				endif;

				if ($this->db->trans_status() === FALSE) :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' relasi product dengan kd_barang \''.$kd_parent.'\' item_barcode \''.$item_barcode.'\' item_code \''.$item_code.'\' kd_mrelasi_barang \''.$kd_mrelasi_barang.'\'');
					$str['confirm'] = 'errValidation';
					$str['idErrForm'] = buildLabel('danger', 'Gagal '.$label_err.' relasi product, Kesalahan sistem!');
				else :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' relasi product dengan kd_barang \''.$kd_parent.'\' item_barcode \''.$item_barcode.'\' item_code \''.$item_code.'\' kd_mrelasi_barang \''.$kd_mrelasi_barang.'\'');
					$str['confirm'] = 'success';
					$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' relasi product!');
				endif;
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function hapus_data() {
		if ($this->input->is_ajax_request()) :
			$kd_mrelasi_barang = $this->input->get('id');
			$label_err = 'menghapus';
			$aksi['master'] = $this->db->delete('tm_barang_relasi', array('kd_mrelasi_barang' => $kd_mrelasi_barang));
			$aksi['detail'] = $this->db->delete('td_barang_relasi', array('mrelasi_barang_kd' => $kd_mrelasi_barang));

			if ($aksi) :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' relasi product dengan kd_mrelasi_barang \''.$kd_mrelasi_barang.'\'');
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' data product!');
			else :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' relasi product dengan kd_mrelasi_barang \''.$kd_mrelasi_barang.'\'');
				$str['alert'] = buildLabel('danger', 'Gagal '.$label_err.' relasi product, Kesalahan sistem!');
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}