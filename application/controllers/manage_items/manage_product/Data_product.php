<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_product extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->library(array('form_validation'));
		$this->load->model(array('m_builder', 'model_product', 'tm_barang', 'tm_project', 'tm_finishgood'));
		$this->load->helper(array('form', 'html', 'my_helper'));
	}

	function index() {
		parent::administrator();
		parent::pnotify_assets();

		$data['t_box_class'] = 'box-primary';
		$title = $this->uri->segment_array();
		$title = end($title);
		if (strpos($title, '_') !== FALSE ) :
			$title = str_replace('_', ' ', $title);
		endif;
		$title = ucwords($title);
		$data['table_title'] = $title;
		$data['btn_grid_edit'] = TRUE;
		$data['table_alert'] = 'idAlertTable';
		$data['table_loader'] = 'idLoaderTable';
		$data['table_name'] = 'idTable';
		$data['table_overlay'] = 'idTableOverlay';

		$this->load->css('assets/admin_assets/plugins/select2/select2.min.css');
		$this->load->css('assets/admin_assets/plugins/select2/select2-bootstrap.min.css');
		$this->load->js('assets/admin_assets/plugins/select2/select2.full.min.js');
		$this->load->section('scriptJS', 'script/manage_items/manage_product/data_product/scriptJS');
		// $this->data_admin_form();
		$this->load->view('page/admin/v_table', $data);
	}

	function data_lihat() {
		if ($this->input->is_ajax_request()) :
			/* tabel property */
			$this->db->where(array('id' => $this->session->kd_manage_items));
			$this->db->from('tb_set_dropdown');
			$q_set = $this->db->get();
			$r_set = $q_set->row();
			$manage_items = $r_set->nm_select;
			$t_data['btn'] = TRUE;
			if ($manage_items == 'Ekspor') :
				$harga_lokal = 'class="never"';
				$harga_ekspor = '';
			elseif ($manage_items == 'Lokal') :
				$harga_lokal = '';
				$harga_ekspor = 'class="never"';
			elseif ($manage_items == 'Semua') :
				$harga_lokal = '';
				$harga_ekspor = '';
			elseif ($manage_items == 'Tidak Ada') :
				$harga_lokal = 'class="never"';
				$harga_ekspor = 'class="never"';
			endif;
			$t_data['btn'] = TRUE;
			$t_data['t_header'] = array(
				'<th style="width:1%;">Barcode</th>',
				'<th style="width:5%;">Code</th>',
				'<th style="width:5%;" class="always">Desc</th>',
				'<th style="width:5%;" class="always">Dimension</th>',
				'<th style="width:5%;" class="always">Hs Code</th>',
				'<th style="width:5%;" '.$harga_lokal.'>Harga Retail</th>',
				'<th style="width:5%;" '.$harga_lokal.'>Harga Reseller</th>',
				'<th style="width:5%;" '.$harga_lokal.'>Harga Distributor</th>',
				'<th style="width:5%;" '.$harga_ekspor.'>Harga Ekspor</th>',
				'<th style="width:5%;">Group</th>',
				'<th style="width:1%;" class="never">Format Retail</th>',
				'<th style="width:1%;" class="never">Format Reseller</th>',
				'<th style="width:1%;" class="never">Format Distributor</th>',
				'<th style="width:1%;" class="never">Format Ekspor</th>',
				// '<th style="width:1%;">Category</th>',
			);
			$t_data['t_uri'] = base_url().'manage_items/manage_product/data_product/data_table/';
			$t_data['t_order'] = '[2, "asc"]';
			$t_data['datatable_properties'] = array('"responsive": true,');
			$t_data['t_col_def'] = '{"searchable": false, "orderable": false, "targets": 7}, {"searchable": false, "orderable": false, "targets": 8}, {"searchable": false, "orderable": false, "targets": 9}, {"searchable": false, "orderable": false, "targets": 11}, {"searchable": false, "orderable": false, "targets": 12}, {"searchable": false, "orderable": false, "targets": 13}, {"searchable": false, "orderable": false, "targets": 14}, {"searchable": false, "orderable": false, "targets": 15}';

			$this->load->view('page/admin/table_ssp', $t_data);
		endif;
	}

	function data_table() {
		if (!$this->input->is_ajax_request()) :
            exit('No direct script access allowed');
        else :
            $this->load->library('ssp');

			$table = 'tm_barang';

			$primaryKey = 'kd_barang';

			$columns = array(
			    array( 'db' => 'a.kd_barang',
						'dt' => 1, 'field' => 'kd_barang',
						'formatter' => function($d){
							if ($this->session->update_access == '1') :
								$btn_edit = '<a id="edit" title="Edit Data" href="javascript:void(0);" onclick="editData(\''.$d.'\')"><i class="fa fa-pencil"></i> Edit Data</a>';
								$btn_price = '<a id="price" title="Data Harga" href="javascript:void(0);" onclick="hargaData(\''.$d.'\')"><i class="fa fa-usd"></i> Data Harga</a>';
								$btn_img = '<a id="image" title="Data Gambar" href="javascript:void(0);" onclick="imgData(\''.$d.'\')"><i class="fa fa-picture-o"></i> Data Gambar</a>';
								$btn_dup = '<a id="duplicate" title="Duplicate Items" href="javascript:void(0);" onclick="duplicate(\''.$d.'\')"><i class="fa fa-clipboard"></i> Duplicate Items</a>';
								$divider = '<li class="divider"></li>';
							else :
								$btn_edit = '';
								$btn_price = '';
								$btn_img = '';
								$btn_dup = '';
								$divider = '';
							endif;
							if ($this->session->delete_access == '1') :
								$btn_delete = '<a id="delete" title="Hapus Data" href="javascript:void(0);" onclick="return confirm(\'Apakah anda yakin?\')?hapusData(\''.$d.'\'):false;"><i class="fa fa-trash"></i> Hapus Data</a>';
							else :
								$btn_delete = '';
							endif;

							$btn = '<div align="center"><div class="btn-group"><button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">Opsi <span class="caret"></span></button><ul class="dropdown-menu"><li>'.$btn_edit.'</li><li>'.$btn_price.'</li><li>'.$btn_img.'</li><li>'.$btn_dup.'</li>'.$divider.'<li>'.$btn_delete.'</li></ul></div></div>';
							
							return $btn;
						} ),
			    array( 'db' => 'a.item_barcode',
			    		'dt' => 2, 'field' => 'item_barcode',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'a.item_code',
			    		'dt' => 3, 'field' => 'item_code',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'a.deskripsi_barang',
			    		'dt' => 4, 'field' => 'deskripsi_barang',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);
			    			$d = word_break($d, 3);

			    			return $d;
			    		} ),
			    array( 'db' => 'a.dimensi_barang',
			    		'dt' => 5, 'field' => 'dimensi_barang',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);
			    			$d = word_break($d, 3);

			    			return $d;
			    		} ),

				array( 'db' => 'a.hs_code',
							'dt' => 6, 'field' => 'hs_code',
							'formatter' => function($d){
								$d = $this->security->xss_clean($d);

							return $d;
						} ),
			    array( 'db' => 'd.harga_lokal_retail',
			    		'dt' => 7, 'field' => 'harga_lokal_retail',
			    		'formatter' => function($d, $row){
			    			$d = !empty($d)?$d:'0';
			    			$formats = format_currency($d, $row[11]);

			    			return $formats;
			    		} ),
			    array( 'db' => 'd.harga_lokal_reseller',
			    		'dt' => 8, 'field' => 'harga_lokal_reseller',
			    		'formatter' => function($d, $row){
			    			$d = !empty($d)?$d:'0';
			    			$formats = format_currency($d, $row[12]);

			    			return $formats;
			    		} ),
			    array( 'db' => 'd.harga_lokal_distributor',
			    		'dt' => 9, 'field' => 'harga_lokal_distributor',
			    		'formatter' => function($d, $row){
			    			$d = !empty($d)?$d:'0';
			    			$formats = format_currency($d, $row[13]);

			    			return $formats;
			    		} ),
			    array( 'db' => 'd.harga_ekspor',
			    		'dt' => 10, 'field' => 'harga_ekspor',
			    		'formatter' => function($d, $row){
			    			$d = !empty($d)?$d:'0';
			    			$formats = format_currency($d, $row[14]);

			    			return $formats;
			    		} ),
			    array( 'db' => 'b.nm_group',
			    		'dt' => 11, 'field' => 'nm_group',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => '(SELECT CONCAT(b.icon_type, \' \', b.currency_icon, \' \', b.pemisah_angka, \' \', b.format_akhir) FROM tb_tipe_harga a LEFT JOIN tm_currency b ON b.kd_currency = a.currency_kd WHERE a.nm_harga = \'Harga Retail\') as format_harga_retail',
			    		'dt' => 12, 'field' => 'format_harga_retail',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => '(SELECT CONCAT(b.icon_type, \' \', b.currency_icon, \' \', b.pemisah_angka, \' \', b.format_akhir) FROM tb_tipe_harga a LEFT JOIN tm_currency b ON b.kd_currency = a.currency_kd WHERE a.nm_harga = \'Harga Reseller\') as format_harga_reseller',
			    		'dt' => 13, 'field' => 'format_harga_reseller',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => '(SELECT CONCAT(b.icon_type, \' \', b.currency_icon, \' \', b.pemisah_angka, \' \', b.format_akhir) FROM tb_tipe_harga a LEFT JOIN tm_currency b ON b.kd_currency = a.currency_kd WHERE a.nm_harga = \'Harga Distributor\') as format_harga_distributor',
			    		'dt' => 14, 'field' => 'format_harga_distributor',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => '(SELECT CONCAT(b.icon_type, \' \', b.currency_icon, \' \', b.pemisah_angka, \' \', b.format_akhir) FROM tb_tipe_harga a LEFT JOIN tm_currency b ON b.kd_currency = a.currency_kd WHERE a.nm_harga = \'Harga Ekspor\') as format_harga_ekspor',
			    		'dt' => 15, 'field' => 'format_harga_ekspor',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    /*array( 'db' => 'c.nm_kat_barang',
			    		'dt' => 10, 'field' => 'nm_kat_barang',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),*/
			);

			$sql_details = sql_connect();

			$joinQuery = "
				FROM
					tm_barang a
				LEFT JOIN tm_group_barang b ON a.group_barang_kd = b.kd_group_barang
				LEFT JOIN tm_kat_barang c ON a.kat_barang_kd = c.kd_kat_barang
				LEFT JOIN td_barang_harga d ON a.harga_barang_kd = d.kd_harga_barang
			";
			$where = "";

			echo json_encode(
			    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $where )
			);
		endif;
	}

	function data_form(){
		if ($this->input->is_ajax_request()) :
			// Form data
			$id = $this->input->get('id');
			$sttsxx = $this->input->get('stts');
			if (!empty($id) && $sttsxx == 'edit'){
				$det = $this->m_builder->getRow('tm_barang', $data = array('kd_barang' => $id), 'row');
				$id_form = 'idFormEdit';
				$data['f_box_class'] = 'box-warning';
				$data['form_title'] = 'Edit Data Barang';
				$btn_form = '<i class="fa fa-edit"></i> Edit Data';
				$kd_barang = $this->security->xss_clean($det->kd_barang);
				$kat_barang_kd = $this->security->xss_clean($det->kat_barang_kd);
				$group_barang_kd = $this->security->xss_clean($det->group_barang_kd);
				$item_group_kd = $this->security->xss_clean($det->item_group_kd);
				$item_code = $this->security->xss_clean($det->item_code);
				$hs_code = $this->security->xss_clean($det->hs_code);
				$item_barcode = $this->security->xss_clean($det->item_barcode);
				$deskripsi_barang = $this->security->xss_clean($det->deskripsi_barang);
				$dimensi_barang = $this->security->xss_clean($det->dimensi_barang);
				$netweight = $this->security->xss_clean($det->netweight);
				$grossweight = $this->security->xss_clean($det->grossweight);
				$boxweight = $this->security->xss_clean($det->boxweight);
				$length_cm = $this->security->xss_clean($det->length_cm);
				$width_cm = $this->security->xss_clean($det->width_cm);
				$height_cm = $this->security->xss_clean($det->height_cm);
				$item_cbm = $this->security->xss_clean($det->item_cbm);
				$harga_barang_kd = $this->security->xss_clean($det->harga_barang_kd);
				$barang_stock_max = $this->security->xss_clean($det->barang_stock_max);
				$barang_stock_min = $this->security->xss_clean($det->barang_stock_min);
				$barang_stock_safety = $this->security->xss_clean($det->barang_stock_safety);
				$inventory_item = $this->security->xss_clean($det->inventory_item);
				// $stok_barang_kd = $this->security->xss_clean($det->stok_barang_kd);
				$item_code_old = form_input(array('type' => 'hidden', 'name' => 'txtCodeOld', 'id' => 'idTxtCodeOld', 'value' => $item_code));
			}else if(!empty($id) && $sttsxx == 'duplicate'){
				// Generate barcode untuk input barang
				$det = $this->m_builder->getRow('tm_barang', $data = array('kd_barang' => $id), 'row');
				$id_form = 'idFormInput';
				$data['f_box_class'] = 'box-info';
				$data['form_title'] = 'Input Data Barang';
				$btn_form = 'Product Prices <i class="fa fa-arrow-right"></i>';
				$kd_barang = '';
				$kat_barang_kd = $this->security->xss_clean($det->kat_barang_kd);
				$group_barang_kd = $this->security->xss_clean($det->group_barang_kd);
				$item_group_kd = $this->security->xss_clean($det->item_group_kd);
				$item_code = '';
				$hs_code = $this->security->xss_clean($det->hs_code);
				$item_barcode = $det->item_barcode;
				$deskripsi_barang = $this->security->xss_clean($det->deskripsi_barang);
				$dimensi_barang = $this->security->xss_clean($det->dimensi_barang);
				$netweight = $this->security->xss_clean($det->netweight);
				$grossweight = $this->security->xss_clean($det->grossweight);
				$boxweight = $this->security->xss_clean($det->boxweight);
				$length_cm = $this->security->xss_clean($det->length_cm);
				$width_cm = $this->security->xss_clean($det->width_cm);
				$height_cm = $this->security->xss_clean($det->height_cm);
				$item_cbm = $this->security->xss_clean($det->item_cbm);
				$harga_barang_kd = $this->security->xss_clean($det->harga_barang_kd);
				$barang_stock_max = $this->security->xss_clean($det->barang_stock_max);
				$barang_stock_min = $this->security->xss_clean($det->barang_stock_min);
				$barang_stock_safety = $this->security->xss_clean($det->barang_stock_safety);
				$inventory_item = $this->security->xss_clean($det->inventory_item);
				$item_code_old = form_input(array('type' => 'hidden', 'name' => 'txtCodeOld', 'id' => 'idTxtCodeOld', 'value' => ''));
				
			}else{
				$conds = array(
					'select' => 'item_barcode',
					'order_by' => 'item_barcode DESC',
				);
				$item_barcode = $this->m_builder->buat_kode('tm_barang', $conds, 4, '89');

				$id_form = 'idFormInput';
				$data['f_box_class'] = 'box-info';
				$data['form_title'] = 'Input Data Barang';
				$btn_form = 'Product Prices <i class="fa fa-arrow-right"></i>';
				$kd_barang = '';
				$kat_barang_kd = '';
				$group_barang_kd = '';
				$item_group_kd = '';
				$item_code = '';
				$hs_code = '';
				$item_barcode = $item_barcode;
				$deskripsi_barang = '';
				$dimensi_barang = '';
				$netweight = '';
				$grossweight = '';
				$boxweight = '';
				$length_cm = '';
				$width_cm = '';
				$height_cm = '';
				$item_cbm = '';
				$harga_barang_kd = '';
				$stok_barang_kd = '';
				$item_code_old = '';
				$barang_stock_max = '';
				$barang_stock_min = '';
				$barang_stock_safety = '';
				$inventory_item = '';
			}
			
			// select option untuk group product
			$this->db->from('tm_group_barang');
			$q_group = $this->db->get();
			$r_group = $q_group->result();
			$pil_group = array('' => '-- Pilih Group Product --');
			foreach ($r_group as $d_group) :
				$kd_group_barang = $this->security->xss_clean($d_group->kd_group_barang);
				$nm_group = $this->security->xss_clean($d_group->nm_group);
				$pil_group[$kd_group_barang] = $this->security->xss_clean($nm_group);
			endforeach;
			$sel_group = $group_barang_kd;
			$attr_group = array('id' => 'idSelGroup', 'class' => 'form-control select2');
			
			// select option untuk kategori product
			$this->db->from('tm_kat_barang');
			$q_kat = $this->db->get();
			$r_kat = $q_kat->result();
			$pil_kat = array('' => '-- Pilih Category Product --');
			foreach ($r_kat as $d_kat) :
				$kd_kat_barang = $this->security->xss_clean($d_kat->kd_kat_barang);
				$kd_user_kat = $this->security->xss_clean($d_kat->kd_user_kat);
				$nm_kat_barang = $this->security->xss_clean($d_kat->nm_kat_barang);
				$pil_kat[$kd_kat_barang] = $kd_user_kat.' - '.$nm_kat_barang;
			endforeach;
			$sel_kat = $kat_barang_kd;
			$attr_kat = array('id' => 'idSelKat', 'class' => 'form-control select2');

			// select option untuk inventory item
			$pil_invent = array('Y' => 'Ya', 'N' => 'Tidak');
			$sel_invent = $inventory_item;
			$attr_invent = array('id' => 'idSelInventItem', 'class' => 'form-control');

			// select option untuk item group SA
			$this->db->from('tm_item_group');
			$q_item_group = $this->db->get();
			$r_item_group = $q_item_group->result();
			$pil_item_group = array('' => '-- Pilih Item Group --');
			foreach ($r_item_group as $d_item_group) :
				$kd_item_group = $this->security->xss_clean($d_item_group->item_group_kd);
				$nm_item_group = $this->security->xss_clean($d_item_group->item_group_name);
				$pil_item_group[$kd_item_group] = $nm_item_group;
			endforeach;
			$sel_item_group = $item_group_kd;
			$attr_item_group = array('id' => 'idSelItemGroup', 'class' => 'form-control select2');

			$data['f_attr'] = 'id="idBoxForm"';
			$data['form_url'] = '';
			$data['form_att'] = array('id' => $id_form, 'class' => 'form-horizontal');
			$data['form_hidden'] = '';
			$data['form_close_att'] = '';
			$data['form_field'] = array(
				'item_barcode' => array(
					'<div class="form-group">',
					'<label for="idTxtBarcode" class="col-md-2 control-label">Barcode Product</label>',
					'<div class="col-md-4">',
					'<div id="idErrBarcode"></div>',
					form_input(array('type' => 'hidden', 'name' => 'stssx', 'id' => 'stssx', 'value' => $sttsxx)),
					form_input(array('type' => 'hidden', 'name' => 'txtKdBarang', 'id' => 'idTxtKdBarang', 'value' => $kd_barang)),
					form_input(array('type' => 'hidden', 'name' => 'txtOldKd', 'id' => 'idTxtOldKd', 'value' => $item_code)),
					form_input(array('name' => 'txtBarcode', 'id' => 'idTxtBarcode', 'class' => 'form-control', 'placeholder' => 'Barcode Product', 'value' => $item_barcode)),
					'</div>',
					'</div>',
				),
				'item_code' => array(
					'<div class="form-group">',
					'<label for="idTxtCode" class="col-md-2 control-label">Code Product</label>',
					'<div class="col-md-4">',
					'<div id="idErrCode"></div>',
					$item_code_old,
					form_input(array('name' => 'txtCode', 'id' => 'idTxtCode', 'class' => 'form-control', 'placeholder' => 'Code Product', 'value' => $item_code)),
					'</div>',
					'</div>',
				),
				'hs_code' => array(
					'<div class="form-group">',
					'<label for="idTxtHs" class="col-md-2 control-label">Tariff (HS Code)</label>',
					'<div class="col-md-4">',
					'<div id="idErrHs"></div>',
					$item_code_old,
					form_input(array('name' => 'txtHs', 'id' => 'idTxtHs', 'class' => 'form-control', 'placeholder' => 'Tariff (HS Code)', 'value' => $hs_code)),
					'</div>',
					'</div>',
				),
				'item_group' => array(
					'<div class="form-group">',
					'<label for="idSelGroup" class="col-md-2 control-label">Group Product</label>',
					'<div class="col-md-3">',
					'<div id="idErrGroup"></div>',
					form_dropdown('selGroup', $pil_group, $sel_group, $attr_group),
					'</div>',
					'</div>',
				),
				'item_kat' => array(
					'<div class="form-group">',
					'<label for="idSelKat" class="col-md-2 control-label">Category Product</label>',
					'<div class="col-md-3">',
					'<div id="idErrKat"></div>',
					form_dropdown('selKat', $pil_kat, $sel_kat, $attr_kat),
					'</div>',
					'</div>',
					'<hr>',
				),
				'item_group_sap' => array(
					'<div class="form-group">',
					'<label for="idSelItemGroup" class="col-md-2 control-label">Item Group (SAP)</label>',
					'<div class="col-md-3">',
					'<div id="idErrItemGroup"></div>',
					form_dropdown('selItemGroup', $pil_item_group, $sel_item_group, $attr_item_group),
					'</div>',
					'</div>',
					'<hr>',
				),
				'grossweight' => array(
					'<div class="form-group">',
					'<label for="idTxtGrossweight" class="col-md-2 control-label">Product Grossweight</label>',
					'<div class="col-md-3">',
					'<div id="idErrGrossweight"></div>',
					form_input(array('name' => 'txtGrossweight', 'id' => 'idTxtGrossweight', 'class' => 'form-control hitung_netweight', 'placeholder' => 'Product Grossweight', 'value' => $grossweight)),
					'</div>',
					'<label for="idTxtBoxweight" class="col-md-2 control-label">Product Box Weight</label>',
					'<div class="col-md-3">',
					'<div id="idErrBoxweight"></div>',
					form_input(array('name' => 'txtBoxweight', 'id' => 'idTxtBoxweight', 'class' => 'form-control hitung_netweight', 'placeholder' => 'Product Box Weight', 'value' => $boxweight)),
					'</div>',
					'</div>',
				),
				'inventory_item' => array(
					'<div class="form-group">',
					'<label for="idSelInventItem" class="col-md-2 control-label">Inventory Item</label>',
					'<div class="col-md-3">',
					'<div id="idErrInvent"></div>',
					form_dropdown('selInventItem', $pil_invent, $sel_invent, $attr_invent),
					'</div>',
					'</div>',
					'<hr>',
				),
				'netweight' => array(
					'<div class="form-group">',
					'<label for="idTxtNetweight" class="col-md-2 control-label">Product Netweight</label>',
					'<div class="col-md-3">',
					'<div id="idErrNetweight"></div>',
					form_input(array('name' => 'txtNetweight', 'id' => 'idTxtNetweight', 'class' => 'form-control', 'placeholder' => 'Product Netweight', 'value' => $netweight)),
					'</div>',
					'</div>',
					'<hr>',
				),
				'length_cm' => array(
					'<div class="form-group">',
					'<label for="idTxtLength" class="col-md-2 control-label">Product Length (cm)</label>',
					'<div class="col-md-3">',
					'<div id="idErrLength"></div>',
					form_input(array('name' => 'txtLength', 'id' => 'idTxtLength', 'class' => 'form-control hitung_cbm', 'placeholder' => 'Product Length (cm)', 'value' => $length_cm)),
					'</div>',
					'<label for="idTxtWidth" class="col-md-2 control-label">Product Width (cm)</label>',
					'<div class="col-md-3">',
					'<div id="idErrWidth"></div>',
					form_input(array('name' => 'txtWidth', 'id' => 'idTxtWidth', 'class' => 'form-control hitung_cbm', 'placeholder' => 'Product Width (cm)', 'value' => $width_cm)),
					'</div>',
					'</div>',
				),
				'height_cm' => array(
					'<div class="form-group">',
					'<label for="idTxtHeight" class="col-md-2 control-label">Product Height (cm)</label>',
					'<div class="col-md-3">',
					'<div id="idErrHeight"></div>',
					form_input(array('name' => 'txtHeight', 'id' => 'idTxtHeight', 'class' => 'form-control hitung_cbm', 'placeholder' => 'Product Height (cm)', 'value' => $height_cm)),
					'</div>',
					'<label for="idTxtCbm" class="col-md-2 control-label">Product CBM (Cubic Metres)</label>',
					'<div class="col-md-3">',
					'<div id="idErrCbm"></div>',
					form_input(array('name' => 'txtCbm', 'id' => 'idTxtCbm', 'class' => 'form-control', 'placeholder' => 'Product CBM (Cubic Metres)', 'value' => $item_cbm)),
					'</div>',
					'</div>',
					'<hr>',
				),
				'stock_max' => array(
					'<div class="form-group">',
					'<label for="idTxtStockMax" class="col-md-2 control-label">Stock Max </label>',
					'<div class="col-md-3">',
					'<div id="idErrStockMax"></div>',
					form_input(array('name' => 'txtStockMax', 'type' => 'number', 'id' => 'idTxtStockMax', 'class' => 'form-control', 'placeholder' => 'Stock Max', 'value' => $barang_stock_max)),
					'</div>',
					'<label for="idTxtStockMin" class="col-md-2 control-label">Stock Min</label>',
					'<div class="col-md-3">',
					'<div id="idErrStockMin"></div>',
					form_input(array('name' => 'txtStockMin', 'type' => 'number', 'id' => 'idTxtStockMin', 'class' => 'form-control', 'placeholder' => 'Stock Min', 'value' => $barang_stock_min)),
					'</div>',
					'</div>',
				),
				'stock_safety' => array(
					'<div class="form-group">',
					'<label for="idTxtStockSafety" class="col-md-2 control-label">Stock Safety</label>',
					'<div class="col-md-3">',
					'<div id="idErrStockSafety"></div>',
					form_input(array('name' => 'txtStockSafety', 'type' => 'number', 'id' => 'idTxtStockSafety', 'class' => 'form-control', 'placeholder' => 'Stock Safety', 'value' => $barang_stock_safety)),
					'</div>',
					'</div>',
					'<hr>',
				),
				'item_desc' => array(
					'<div class="form-group">',
					'<label for="idTxtDesc" class="col-md-2 control-label">Product Description</label>',
					'<div class="col-md-5">',
					'<div id="idErrDesc"></div>',
					form_textarea(array('name' => 'txtDesc', 'id' => 'idTxtDesc', 'class' => 'form-control', 'placeholder' => 'Product Description', 'value' => $deskripsi_barang, 'rows' => '5')),
					'</div>',
					'</div>',
				),
				'item_dimension' => array(
					'<div class="form-group">',
					'<label for="idTxtDimension" class="col-md-2 control-label">Product Dimension</label>',
					'<div class="col-md-5">',
					'<div id="idErrDimension"></div>',
					form_textarea(array('name' => 'txtDimension', 'id' => 'idTxtDimension', 'class' => 'form-control', 'placeholder' => 'Product Dimension', 'value' => $dimensi_barang, 'rows' => '5')),
					'</div>',
					'</div>',
				),
			);

			$data['form_btn'] = array(
				'btn_submit' => array(
					'<button type="submit" name="btnSubmit" id="idBtnSubmit" class="btn btn-warning pull-right">',
					$btn_form,
					'</button>',
				),
			);
			$data['form_box'] = 'idBoxFormBody';
			$data['form_alert'] = 'idAlertForm';
			$data['form_overlay'] = 'idOverlayForm';

			$this->load->section('form_js', 'script/manage_items/manage_product/data_product/form_js');
			$this->load->view('page/admin/v_form', $data);
		endif;
	}

	function verify_barcode() {
		if ($this->input->is_ajax_request()) :
			$barcode = $this->input->get('id');
			$kd_barang = $this->input->get('kd_barang');
			if (empty($barcode)) :
				$str['confirm'] = 'errValidation';
				$str['idErrBarcode'] = buildLabel('danger', 'Barcode tidak boleh kosong!');
			else :
				$check_code = $this->m_builder->check_code($barcode, 'tm_barang', array('item_barcode' => $barcode, 'kd_barang !=' => $kd_barang));
				if ($check_code) :
					$str['confirm'] = 'success';
					$str['idErrBarcode'] = buildLabel('success', 'Barcode bisa dipakai!');
				else :
					$str['confirm'] = 'errValidation';
					$str['idErrBarcode'] = buildLabel('warning', 'Barcode sudah dipakai!');
				endif;
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function verify_code() {
		if ($this->input->is_ajax_request()) :
			$code = $this->input->get('id');
			$kd_barang = $this->input->get('kd_barang');
			if (empty($code)) :
				$str['confirm'] = 'errValidation';
				$str['idErrCode'] = buildLabel('danger', 'Code tidak boleh kosong!');
			else :
				$check_code = $this->m_builder->check_code($code, 'tm_barang', array('item_code' => $code, 'kd_barang !=' => $kd_barang));
				if ($check_code) :
					$str['confirm'] = 'success';
					$str['idErrCode'] = buildLabel('success', 'Code bisa dipakai!');
				else :
					$str['confirm'] = 'errValidation';
					$str['idErrCode'] = buildLabel('warning', 'Code sudah dipakai!');
				endif;
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function submit_form($act){
		if ($this->input->is_ajax_request()) :
			$this->db->trans_begin();
			$db_caterlindo = $this->load->database('db_caterlindo', TRUE);
			$str['csrf'] = $this->security->get_csrf_hash();

			$this->form_validation->set_rules('txtBarcode', 'Product Barcode', 'trim|required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			if (($act == 'input') || ($act == 'edit' && $this->input->post('txtCode') != $this->input->post('txtCodeOld'))) :
				$this->form_validation->set_rules('txtCode', 'Product Code', 'trim|required|is_unique[tm_barang.item_code]',
					array(
						'required' => '{field} tidak boleh kosong!',
						'is_unique' => '{field} sudah digunakan, gunakan code lainnya!',
					)
				);
			endif;
			$this->form_validation->set_rules('txtHs', 'Tariff (HS Code)', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selGroup', 'Product Group', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selKat', 'Product Category', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selInventItem', 'Inventory Item', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('selItemGroup', 'Item Group SAP', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtDesc', 'Product Description', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtDimension', 'Product Dimension', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtGrossweight', 'Product Grossweight', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtBoxweight', 'Product Boxweight', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'errValidation';
				$str['idErrBarcode'] = (!empty(form_error('txtBarcode')))?buildLabel('warning', form_error('txtBarcode', '"', '"')):'';
				$str['idErrCode'] = (!empty(form_error('txtCode')))?buildLabel('warning', form_error('txtCode', '"', '"')):'';
				$str['idErrGroup'] = (!empty(form_error('selGroup')))?buildLabel('warning', form_error('selGroup', '"', '"')):'';
				$str['idErrHs'] = (!empty(form_error('txtHs')))?buildLabel('warning', form_error('txtHs', '"', '"')):'';
				$str['idErrKat'] = (!empty(form_error('selKat')))?buildLabel('warning', form_error('selKat', '"', '"')):'';
				$str['idErrItemGroup'] = (!empty(form_error('selItemGroup')))?buildLabel('warning', form_error('selItemGroup', '"', '"')):'';
				$str['idErrDesc'] = (!empty(form_error('txtDesc')))?buildLabel('warning', form_error('txtDesc', '"', '"')):'';
				$str['idErrDimension'] = (!empty(form_error('txtDimension')))?buildLabel('warning', form_error('txtDimension', '"', '"')):'';
				$str['idErrGrossweight'] = (!empty(form_error('txtGrossweight')))?buildLabel('warning', form_error('txtGrossweight', '"', '"')):'';
				$str['idErrBoxweight'] = (!empty(form_error('txtBoxweight')))?buildLabel('warning', form_error('txtBoxweight', '"', '"')):'';
			else :
				/*
				** Jika validasi berhasil dan tidak ada error
				** Data dari form akan diinputkan kedalam tabel
				** Jika query input return true
				** alert success akan ditampilakan, selain itu error (kesalahan sistem!)
				*/
				$item_barcode = $this->input->post('txtBarcode');
				$item_code = $this->input->post('txtCode');
				$hs_code = $this->input->post('txtHs');
				$old_item_code = $this->input->post('txtOldKd');
				$group_barang_kd = $this->input->post('selGroup');
				$kat_barang_kd = $this->input->post('selKat');
				$item_group_kd = $this->input->post('selItemGroup');
				$deskripsi_barang = $this->input->post('txtDesc');
				$dimensi_barang = $this->input->post('txtDimension');
				$netweight = $this->input->post('txtNetweight');
				$grossweight = $this->input->post('txtGrossweight');
				$boxweight = $this->input->post('txtBoxweight');
				$length_cm = $this->input->post('txtLength');
				$width_cm = $this->input->post('txtWidth');
				$height_cm = $this->input->post('txtHeight');
				$item_cbm = $this->input->post('txtCbm');
				$barang_stock_max = $this->input->post('txtStockMax');
				$barang_stock_min = $this->input->post('txtStockMin');
				$barang_stock_safety = $this->input->post('txtStockSafety');
				$inventory_item = $this->input->post('selInventItem');
				if ($act == 'input') :
					$conds = array(
						'select' => 'tgl_input, kd_barang',
						'order_by' => 'tgl_input DESC',
						'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
					);
					$kd_barang = $this->m_builder->buat_kode('tm_barang', $conds, 6, 'PRD');

					// Insert into db_caterlindo_ppic
					$data = array(
						'kd_barang' => $kd_barang,
						'item_barcode' => $item_barcode,
						'item_code' => $item_code,
						'hs_code' => $hs_code,
						'group_barang_kd' => $group_barang_kd,
						'kat_barang_kd' => $kat_barang_kd,
						'item_group_kd' => $item_group_kd,
						'deskripsi_barang' => $deskripsi_barang,
						'dimensi_barang' => $dimensi_barang,
						'netweight' => $netweight,
						'grossweight' => $grossweight,
						'boxweight' => $boxweight,
						'length_cm' => $length_cm,
						'width_cm' => $width_cm,
						'height_cm' => $height_cm,
						'item_cbm' => $item_cbm,
						'barang_stock_max' => $barang_stock_max,
						'barang_stock_min' => $barang_stock_min,
						'barang_stock_safety' => $barang_stock_safety,
						'inventory_item' => $inventory_item,
						'tgl_input' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					
					/** Jika barang yang di input custom maka insert ke tm_project */
					if($item_group_kd == '2'){
						$projectKd = $this->tm_project->create_code ('', '');
						$projectNo = $this->tm_project->generate_nopj('', '');
						$data['item_code'] = $projectNo." ".$item_code;
						$data['item_barcode'] = $projectNo;
						$dataProject = [
							'project_kd' => $projectKd,
							'kd_msalesorder' => null,
							'kd_ditem_so' => null,	
							'kd_citem_so' => null,	
							'project_nopj' => $projectNo,
							'project_itemcode' => $item_code,
							'project_itemdesc' => $deskripsi_barang,
							'project_itemdimension' => $dimensi_barang,
							'project_hargabarang' => null,
							'project_status' => 'pending',
							'project_tglinput' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin')
						];
						$aksiProject = $this->tm_project->insert_data($dataProject);
					}

					/** Insert to DB Caterlindo PPIC */
					$aksiPPIC = $this->db->insert('tm_barang', $data);
					

					$fg_kd = $this->tm_finishgood->buat_kode();
					$dataFG = array(
						'fg_kd' => $fg_kd,
						'kd_gudang' => 'MGD071218001',
						'barang_kd' => $kd_barang,
						'item_code' => $item_code,
						'item_barcode' => $item_group_kd == '2' ? '899000' : $item_barcode, /** Jika barang custom atau PJ barcodenya '899000' */
						'fg_qty' => 0,
						'fg_tglinput' => date('Y-m-d H:i:s'),
						'fg_tgledit' => null,
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					$actInsertFG = $this->tm_finishgood->insert_data ($dataFG);
				
					// Insert into db_caterlindo
					$d_old = array(
						'fc_kdstock' => $item_code,
						'fc_barcode' => $item_barcode,
						'fc_kdgroup' => $group_barang_kd,
						'fn_qty' => '0',
						'fn_qtymin' => '0',
						'fn_qtymax' => '0',
						'fc_status' => 'N',
						'fv_namastock' => $deskripsi_barang.' '.$dimensi_barang,
						'fd_tglinput' => date('Y-m-d H:i:s'),
						'fc_userinput' => $this->session->userdata('username'),
					);
					$aksi = $db_caterlindo->insert('tm_stock', $d_old);
				elseif ($act == 'edit') :
					$kd_barang = $this->input->post('txtKdBarang');

					// Update db_caterlindo_ppic
					$data = array(
						'item_barcode' => $item_barcode,
						'item_code' => $item_code,
						'hs_code' => $hs_code,
						'group_barang_kd' => $group_barang_kd,
						'kat_barang_kd' => $kat_barang_kd,
						'item_group_kd' => $item_group_kd,
						'deskripsi_barang' => $deskripsi_barang,
						'dimensi_barang' => $dimensi_barang,
						'netweight' => $netweight,
						'grossweight' => $grossweight,
						'boxweight' => $boxweight,
						'length_cm' => $length_cm,
						'width_cm' => $width_cm,
						'height_cm' => $height_cm,
						'item_cbm' => $item_cbm,
						'barang_stock_max' => $barang_stock_max,
						'barang_stock_min' => $barang_stock_min,
						'barang_stock_safety' => $barang_stock_safety,
						'inventory_item' => $inventory_item,
						'tgl_edit' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					$where = array('kd_barang' => $kd_barang);
					/** Update to Caterlindo PPIC */
					$aksiPPIC = $this->db->update('tm_barang', $data, $where);
					// Update db_caterlindo, check the data first, if exist update, if not insert it!
					$db_caterlindo->where(array('fc_barcode' => $item_barcode));
					$db_caterlindo->from('tm_stock');
					$q_stock = $db_caterlindo->get();
					$j_stock = $q_stock->num_rows();
					if ($j_stock > 0) :
						$d_old = array(
							'fc_kdstock' => $item_code,
							'fc_barcode' => $item_barcode,
							'fc_kdgroup' => $group_barang_kd,
							'fv_namastock' => $deskripsi_barang.' '.$dimensi_barang,
							'fd_update' => date('Y-m-d H:i:s'),
							'fc_userinput' => $this->session->userdata('username'),
						);
						$w_old = array('fc_kdstock' => $old_item_code);
						$aksi = $db_caterlindo->update('tm_stock', $d_old, $w_old);
					else :
						// Insert into db_caterlindo
						$d_old = array(
							'fc_kdstock' => $item_code,
							'fc_barcode' => $item_barcode,
							'fc_kdgroup' => $group_barang_kd,
							'fn_qty' => '0',
							'fn_qtymin' => '0',
							'fn_qtymax' => '0',
							'fc_status' => 'N',
							'fv_namastock' => $deskripsi_barang.' '.$dimensi_barang,
							'fd_tglinput' => date('Y-m-d H:i:s'),
							'fc_userinput' => $this->session->userdata('username'),
						);
						$aksi = $db_caterlindo->insert('tm_stock', $d_old);
					endif;
				endif;
				if ($this->db->trans_status() === FALSE) : 
					$this->db->trans_rollback();
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' data product dengan kd_barang \''.$kd_barang.'\' item_barcode \''.$item_barcode.'\' item_code \''.$item_code.'\' group_barang_kd \''.$group_barang_kd.'\' kat_barang_kd \''.$kat_barang_kd.'\' deskripsi_barang \''.$deskripsi_barang.'\' dimensi_barang \''.$dimensi_barang.'\'');
					$str['confirm'] = 'errValidation';
					$str['idErrForm'] = buildLabel('danger', 'Gagal error transaction data product, Kesalahan sistem!');
					$str['alert'] = "Error transaction";	
				else :
					if($act == 'input'):
						/** Insert to API SAP */
						$api = $this->push_to_sap($data['kd_barang'], "add");
						$label_err = 'API ['.$api[0]->Message.'] menambahkan';
					else:
						/** Update to API SAP */
						$api = $this->push_to_sap($kd_barang, "edit");
						$label_err = 'API ['.$api[0]->Message.'] mengubah';
					endif;
					if($api[0]->ErrorCode == 0):
						$this->db->trans_commit();
						$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' data product dengan kd_barang \''.$kd_barang.'\' item_barcode \''.$item_barcode.'\' item_code \''.$item_code.'\' group_barang_kd \''.$group_barang_kd.'\' kat_barang_kd \''.$kat_barang_kd.'\' deskripsi_barang \''.$deskripsi_barang.'\' dimensi_barang \''.$dimensi_barang.'\'');
						$str['confirm'] = 'success';
						$str['kd_barang'] = $kd_barang;
						$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' data product!');
					else:
						$this->db->trans_rollback();
						$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' data product dengan kd_barang \''.$kd_barang.'\' item_barcode \''.$item_barcode.'\' item_code \''.$item_code.'\' group_barang_kd \''.$group_barang_kd.'\' kat_barang_kd \''.$kat_barang_kd.'\' deskripsi_barang \''.$deskripsi_barang.'\' dimensi_barang \''.$dimensi_barang.'\'');
						$str['confirm'] = 'errValidation';
						$str['idErrForm'] = buildLabel('danger', 'Gagal '.$label_err.' data product, Kesalahan sistem!');
						$str['alert'] = $label_err;	
					endif;
					
				endif;
			endif;
			
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function hapus_data() {
		if ($this->input->is_ajax_request()) :
			$kd_barang = $this->input->get('id');
			$label_err = 'menghapus';
			$aksi['master'] = $this->db->delete('tm_barang', array('kd_barang' => $kd_barang));
			$aksi['harga'] = $this->db->delete('td_barang_harga', array('barang_kd' => $kd_barang));
			$aksi['media'] = $this->db->delete('td_barang_media', array('barang_kd' => $kd_barang));

			if ($aksi) :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' data product dengan kd_barang \''.$kd_barang.'\', data yang mempunyai kd_barang \''.$kd_barang.'\' juga dihapus dari td_barang_harga, td_barang_media');
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' data product!');
			else :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' data product dengan kd_barang \''.$kd_barang.'\'');
				$str['alert'] = buildLabel('danger', 'Gagal '.$label_err.' data product, Kesalahan sistem!');
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function show_form_select_grid() {
		$data['class_link'] = 'manage_items/manage_product/data_product';
		$data['form_errs'] = array('idErrSelGridTipe', 'idErrSelGroup', 'idErrSelCategory');
		$this->load->view('page/'.$data['class_link'].'/form_select_grid_box', $data);
	}

	public function get_form_select_grid() {
		$this->load->model(array('tm_group_barang', 'tm_kat_barang'));
		$data['class_link'] = 'manage_items/manage_product/data_product';
		$data['dropdown_group'] = $this->tm_group_barang->get_dropdown();
		$data['dropdown_kat'] = $this->tm_kat_barang->get_dropdown();
		$this->load->view('page/'.$data['class_link'].'/form_select_grid_main', $data);
	}

	public function send_selected() {
		$this->load->library(array('form_validation'));
		$this->form_validation->set_rules('selGridTipe', 'Grid Tipe', 'required');
		$this->form_validation->set_rules('selGroup', 'Product Group', 'required');
		$this->form_validation->set_rules('selCategory', 'Product Category', 'required');
		if ($this->form_validation->run() == FALSE) :
			$str['confirm'] = 'error';
			$str['idErrSelGridTipe'] = (!empty(form_error('selGridTipe')))?buildLabel('warning', form_error('selGridTipe', '"', '"')):'';
			$str['idErrSelGroup'] = (!empty(form_error('selGroup')))?buildLabel('warning', form_error('selGroup', '"', '"')):'';
			$str['idErrSelCategory'] = (!empty(form_error('selCategory')))?buildLabel('warning', form_error('selCategory', '"', '"')):'';
		else :
			$str['confirm'] = 'success';
			$str['selected'] = $this->input->post('selGridTipe');
			$str['group'] = $this->input->post('selGroup');
			$str['kat'] = $this->input->post('selCategory');
		endif;
		$str['csrf'] = $this->security->get_csrf_hash();

		header('Content-Type: application/json');
		echo json_encode($str);
	}

	public function show_form_grid($type = '', $group = '', $kat = '') {
		$this->load->model(array('tm_group_barang', 'tm_kat_barang'));
		$data['class_link'] = 'manage_items/manage_product/data_product';
		$data['form_errs'] = array('idErr');
		$data['type_title'] = $this->define_type($type);
		$data['group_name'] = $this->tm_group_barang->get_name($group);
		$data['kat_name'] = $this->tm_kat_barang->get_name($kat);
		$data['type'] = $type;
		$data['group'] = $group;
		$data['kat'] = $kat;
		$this->load->view('page/'.$data['class_link'].'/form_grid_box', $data);
	}

	public function get_form_grid() {
		$type = $this->input->get('type');
		$group = $this->input->get('group');
		$kat = $this->input->get('kat');
		$data['class_link'] = 'manage_items/manage_product/data_product';
		$data['table_head'] = $this->table_grid_head($type);
		$data['table_body'] = $this->model_product->editable_grid($type, $group, $kat);
		$this->load->view('page/'.$data['class_link'].'/form_grid_main', $data);
	}

	private function define_type($type = '') {
		if (!empty($type)) :
			$var = str_replace('_', ' ', $type);
			return ucwords($var);
		endif;
	}

	private function table_grid_head($type = '') {
		$this->db->where(array('id' => $this->session->kd_manage_items));
		$this->db->from('tb_set_dropdown');
		$q_set = $this->db->get();
		$r_set = $q_set->row();
		$manage_items = $r_set->nm_select;
		$table_head = '<th>Item Code</th>';
		if ($type == 'product_desc') :
			$table_head .= '<th>Barcode</th>';
			$table_head .= '<th>HS Code</th>';
			$table_head .= '<th>Description</th>';
			$table_head .= '<th>Dimension</th>';
		elseif ($type == 'product_dimension') :
			$table_head .= '<th>Grossweight</th>';
			$table_head .= '<th>Boxweight</th>';
			$table_head .= '<th>Netweight</th>';
			$table_head .= '<th>Length (cm)</th>';
			$table_head .= '<th>Width (cm)</th>';
			$table_head .= '<th>Height (cm)</th>';
			$table_head .= '<th>Item CBM</th>';
		elseif ($type == 'product_price') :
			if ($manage_items == 'Lokal' || $manage_items == 'Semua') :
				$table_head .= '<th>Harga Retail</th>';
				$table_head .= '<th>Harga Reseller</th>';
				$table_head .= '<th>Harga Distributor</th>';
			elseif ($manage_items == 'Tidak Ada') :
				$table_head .= '';
			endif;
			if ($manage_items == 'Ekspor' || $manage_items == 'Semua') :
				$table_head .= '<th>Harga Ekspor</th>';
			elseif ($manage_items == 'Tidak Ada') :
				$table_head .= '';
			endif;
		endif;
		return $table_head;
	}

	public function submit_product_grid() {
		if ($this->input->is_ajax_request()) :
			$data['form_type'] = $this->input->post('txtFormType');
			$data['kd_barang'] = $this->input->post('txtKd');
			if ($data['form_type'] == 'product_desc') :
				$data['item_barcode'] = $this->input->post('txtBarcode');
				$data['hs_code'] = $this->input->post('txtHsCode');
				$data['deskripsi_barang'] = $this->input->post('txtDesc');
				$data['dimensi_barang'] = $this->input->post('txtDimension');
			elseif ($data['form_type'] == 'product_dimension') :
				$data['grossweight'] = $this->input->post('txtGrossWeight');
				$data['boxweight'] = $this->input->post('txtBoxWeight');
				$data['netweight'] = $this->input->post('txtNetWeight');
				$data['length_cm'] = $this->input->post('txtLength');
				$data['width_cm'] = $this->input->post('txtWidth');
				$data['height_cm'] = $this->input->post('txtHeight');
				$data['item_cbm'] = $this->input->post('txtItemCbm');
			elseif ($data['form_type'] == 'product_price') :
				$data['harga_lokal_retail'] = $this->input->post('txtHargaRetail');
				$data['harga_lokal_reseller'] = $this->input->post('txtHargaReseller');
				$data['harga_lokal_distributor'] = $this->input->post('txtHargaDistributor');
				$data['harga_ekspor'] = $this->input->post('txtHargaEkspor');
			endif;
			$str = $this->tm_barang->submit_batch($data);

			$str['csrf'] = $this->security->get_csrf_hash();
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function push_to_sap($kd, $act){
		$data = $this->tm_barang->get_item(['kd_barang' => $kd]);
		/** API FILL VARIABLE */
		$dataAPI = [
			'ItemCode' => $data->item_code,
			'ItemName' => $data->deskripsi_barang,
			'PurchaseItem' => "N",
			'SalesItem' => "Y",
			'InventoryItem' => $data->inventory_item,
			'KdItemGroup' => $data->item_group_kd, /** kodenya diambil dari modul item group */
			'Dimensi' => $data->dimensi_barang,
			'NetWeight' => $data->netweight,
			'GrossWeight' => $data->grossweight,
			'BoxWeight' => $data->boxweight,
			'Length' => $data->length_cm,
			'Width' => $data->width_cm,
			'Height' => $data->height_cm,
			'Volume' => $data->item_cbm,
			'PrchseItem' => "N",
			'SellItem' => "Y",
			'InvntItem' => $data->inventory_item,
			'StockSafety' => $data->barang_stock_safety,
			'U_IDU_WEBID' => $data->kd_barang,
		];
		if ($act == "add"):
			$api = parent::api_sap_post('AddItem', $dataAPI);
		else:
			$api = parent::api_sap_post('EditItem', $dataAPI);
		endif;
		return $api;
	}

	public function duplicate(){
		$kd = $this->input->get('kd');
		$xxdata = $this->tm_barang->get_item(['kd_barang' => $kd]);
		$this->db->trans_begin();
		$db_caterlindo = $this->load->database('db_caterlindo', TRUE);
		$act = 'input';
		/** API FILL VARIABLE */
		$item_barcode = $xxdata->item_barcode;
		$item_code = $xxdata->item_code;
		$hs_code = $xxdata->hs_code;
		$old_item_code =  $xxdata->item_code;
		$group_barang_kd =$xxdata->group_barang_kd;
		$kat_barang_kd = $xxdata->kat_barang_kd;
		$item_group_kd = $xxdata->item_group_kd;
		$deskripsi_barang = $xxdata->deskripsi_barang;
		$dimensi_barang = $xxdata->dimensi_barang;
		$netweight = $xxdata->netweight;
		$grossweight = $xxdata->grossweight;
		$boxweight =$xxdata->boxweight;
		$length_cm = $xxdata->length_cm;
		$width_cm = $xxdata->width_cm;
		$height_cm = $xxdata->height_cm;
		$item_cbm = $xxdata->item_cbm;
		$barang_stock_max = 0;
		$barang_stock_min = 0;
		$barang_stock_safety = $xxdata->barang_stock_safety;
		$inventory_item = $xxdata->inventory_item;
		
		$conds = array(
			'select' => 'tgl_input, kd_barang',
			'order_by' => 'tgl_input DESC',
			'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
		);
		$kd_barang = $this->m_builder->buat_kode('tm_barang', $conds, 6, 'PRD');

		// Insert into db_caterlindo_ppic
		$data = array(
			'kd_barang' => $kd_barang,
			'item_barcode' => $item_barcode,
			'item_code' => $item_code,
			'hs_code' => $hs_code,
			'group_barang_kd' => $group_barang_kd,
			'kat_barang_kd' => $kat_barang_kd,
			'item_group_kd' => $item_group_kd,
			'deskripsi_barang' => $deskripsi_barang,
			'dimensi_barang' => $dimensi_barang,
			'netweight' => $netweight,
			'grossweight' => $grossweight,
			'boxweight' => $boxweight,
			'length_cm' => $length_cm,
			'width_cm' => $width_cm,
			'height_cm' => $height_cm,
			'item_cbm' => $item_cbm,
			'barang_stock_max' => $barang_stock_max, 
			'barang_stock_min' => $barang_stock_min,
			'barang_stock_safety' => $barang_stock_safety,
			'inventory_item' => $inventory_item,
			'tgl_input' => date('Y-m-d H:i:s'),
			'admin_kd' => $this->session->userdata('kd_admin'),
		);
		
		/** Jika barang yang di input custom maka insert ke tm_project */
		if($item_group_kd == '2'){
			$projectKd = $this->tm_project->create_code ('', '');
			$projectNo = $this->tm_project->generate_nopj('', '');
			$data['item_code'] = $projectNo." ".$item_code;
			$data['item_barcode'] = $projectNo;
			$dataProject = [
				'project_kd' => $projectKd,
				'kd_msalesorder' => null,
				'kd_ditem_so' => null,	
				'kd_citem_so' => null,	
				'project_nopj' => $projectNo,
				'project_itemcode' => $item_code,
				'project_itemdesc' => $deskripsi_barang,
				'project_itemdimension' => $dimensi_barang,
				'project_hargabarang' => null,
				'project_status' => 'pending',
				'project_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin')
			];
			$aksiProject = $this->tm_project->insert_data($dataProject);
		}

		/** Insert to DB Caterlindo PPIC */
		$aksiPPIC = $this->db->insert('tm_barang', $data);
		

		$fg_kd = $this->tm_finishgood->buat_kode();
		$dataFG = array(
			'fg_kd' => $fg_kd,
			'kd_gudang' => 'MGD071218001',
			'barang_kd' => $kd_barang,
			'item_code' => $item_code,
			'item_barcode' => $item_group_kd == '2' ? '899000' : $item_barcode, /** Jika barang custom atau PJ barcodenya '899000' */
			'fg_qty' => 0,
			'fg_tglinput' => date('Y-m-d H:i:s'),
			'fg_tgledit' => null,
			'admin_kd' => $this->session->userdata('kd_admin'),
		);
		$actInsertFG = $this->tm_finishgood->insert_data ($dataFG);
	
		// Insert into db_caterlindo
		$d_old = array(
			'fc_kdstock' => $kd_barang,
			'fc_barcode' => $item_barcode,
			'fc_kdgroup' => $group_barang_kd,
			'fn_qty' => '0',
			'fn_qtymin' => '0',
			'fn_qtymax' => '0',
			'fc_status' => 'N',
			'fv_namastock' => $deskripsi_barang.' '.$dimensi_barang,
			'fd_tglinput' => date('Y-m-d H:i:s'),
			'fc_userinput' => $this->session->userdata('username'),
		);
		$aksi = $db_caterlindo->insert('tm_stock', $d_old);

		if ($this->db->trans_status() === FALSE) : 
			$this->db->trans_rollback();
			$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' data product dengan kd_barang \''.$kd_barang.'\' item_barcode \''.$item_barcode.'\' item_code \''.$item_code.'\' group_barang_kd \''.$group_barang_kd.'\' kat_barang_kd \''.$kat_barang_kd.'\' deskripsi_barang \''.$deskripsi_barang.'\' dimensi_barang \''.$dimensi_barang.'\'');
			$str['confirm'] = 'errValidation';
			$str['idErrForm'] = buildLabel('danger', 'Gagal error transaction data product, Kesalahan sistem!');
			$str['alert'] = "Error transaction";	
		else :
			if($act == 'input'):
				/** Insert to API SAP */
				$api = $this->push_to_sap($data['kd_barang'], "add");
				$label_err = 'API ['.$api[0]->Message.'] menambahkan';
			else:
				/** Update to API SAP */
				$api = $this->push_to_sap($kd_barang, "edit");
				$label_err = 'API ['.$api[0]->Message.'] mengubah';
			endif;
			if($api[0]->ErrorCode == 0):
				$this->db->trans_commit();
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' data product dengan kd_barang \''.$kd_barang.'\' item_barcode \''.$item_barcode.'\' item_code \''.$item_code.'\' group_barang_kd \''.$group_barang_kd.'\' kat_barang_kd \''.$kat_barang_kd.'\' deskripsi_barang \''.$deskripsi_barang.'\' dimensi_barang \''.$dimensi_barang.'\'');
				$str['confirm'] = 'success';
				$str['kd_barang'] = $kd_barang;
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' data product!');
			else:
				$this->db->trans_rollback();
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' data product dengan kd_barang \''.$kd_barang.'\' item_barcode \''.$item_barcode.'\' item_code \''.$item_code.'\' group_barang_kd \''.$group_barang_kd.'\' kat_barang_kd \''.$kat_barang_kd.'\' deskripsi_barang \''.$deskripsi_barang.'\' dimensi_barang \''.$dimensi_barang.'\'');
				$str['confirm'] = 'errValidation';
				$str['idErrForm'] = buildLabel('danger', 'Gagal '.$label_err.' data product, Kesalahan sistem!');
				$str['alert'] = $label_err;	
			endif;
			
		endif;
	
		header('Content-Type: application/json');
		echo json_encode($str);
	
	}

}