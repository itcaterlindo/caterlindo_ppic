<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_groups extends MY_Controller {

	function __construct() {
		parent::__construct();
		
		$this->load->model('m_builder');
		$this->load->helper(array('form', 'html', 'my_helper'));
	}

	function index() {
		parent::administrator();

		$data['t_box_class'] = 'box-primary';
		$title = $this->uri->segment_array();
		$title = end($title);
		if (strpos($title, '_') !== FALSE ) :
			$title = str_replace('_', ' ', $title);
		endif;
		$title = ucwords($title);
		$data['table_title'] = $title;
		$data['table_alert'] = 'idAlertTable';
		$data['table_loader'] = 'idLoaderTable';
		$data['table_name'] = 'idTable';
		$data['table_overlay'] = 'idTableOverlay';
		$data['btn_add'] = cek_permission('PRODUCTGROUPS_CREATE');
		
		$this->load->section('scriptJS', 'script/manage_items/manage_product/product_groups/scriptJS');
		// $this->data_admin_form();
		$this->load->view('page/admin/v_table', $data);
	}

	function data_lihat() {
		if ($this->input->is_ajax_request()) :
			/* tabel property */
			$t_data['btn'] = TRUE;
			$t_data['t_header'] = array(
				'<th style="width:1%;">Kode Group</th>',
				'<th style="width:10%;">Nama Group</th>',
			);
			$t_data['t_uri'] = base_url().'manage_items/manage_product/product_groups/data_table';
			$t_data['t_order'] = '[2, "asc"]';

			$this->load->view('page/admin/table_ssp', $t_data);
		endif;
	}

	function data_table() {
		if (!$this->input->is_ajax_request()) :
            exit('No direct script access allowed');
        else :

            $this->load->library('ssp');

			$table = 'tm_group_barang';

			$primaryKey = 'kd_group_barang';

			$columns = array(
			    array( 'db' => 'kd_group_barang',
						'dt' => 1, 'field' => 'kd_group_barang',
						'formatter' => function($d){
							$btn_edit = '';
							$btn_delete = '';
							$divider = '';
							if (cek_permission('PRODUCTGROUPS_UPDATE')) :
								$btn_edit = '<a id="edit" title="Edit Data" href="javascript:void(0);" onclick="editData(\''.$d.'\')"><i class="fa fa-pencil"></i> Edit Data</a>';
								$divider = '<li class="divider"></li>';
							endif;
							if (cek_permission('PRODUCTGROUPS_DELETE')) :
								$btn_delete = '<a id="delete" title="Hapus Data" href="javascript:void(0);" onclick="return confirm(\'Apakah anda yakin?\')?hapusData(\''.$d.'\'):false;"><i class="fa fa-trash"></i> Hapus Data</a>';
							else :
								$btn_delete = '';
							endif;

							$btn = '<div align="center"><div class="btn-group"><button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">Opsi <span class="caret"></span></button><ul class="dropdown-menu"><li>'.$btn_edit.'</li>'.$divider.'<li>'.$btn_delete.'</li></ul></div></div>';
							
							return $btn;
						} ),
			    array( 'db' => 'kd_group_barang',
			    		'dt' => 2, 'field' => 'kd_group_barang',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'nm_group',
			    		'dt' => 3, 'field' => 'nm_group',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			);

			$sql_details = sql_connect();

			$joinQuery = "";
			$where = "";

			echo json_encode(
			    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $where )
			);
		endif;
	}

	function data_form(){
		if ($this->input->is_ajax_request()) :
			// Form data
			$id = $this->input->get('id');
			if (!empty($id)) :
				$det = $this->m_builder->getRow('tm_group_barang', $data = array('kd_group_barang' => $id), 'row');
				$id_form = 'idFormEdit';
				$data['f_box_class'] = 'box-warning';
				$data['form_title'] = 'Edit Data Group Barang';
				$btn_form = '<i class="fa fa-edit"></i> Edit Data';
				$kd_group_barang = $this->security->xss_clean($det->kd_group_barang);
				$nm_group = $this->security->xss_clean($det->nm_group);
			else :
				$id_form = 'idFormInput';
				$data['f_box_class'] = 'box-info';
				$data['form_title'] = 'Input Data Group Barang';
				$btn_form = '<i class="fa fa-save"></i> Input Data';
				$kd_group_barang = '';
				$nm_group = '';
			endif;

			$data['f_attr'] = 'id="idBoxForm"';
			$data['form_url'] = '';
			$data['form_att'] = array('id' => $id_form, 'class' => 'form-horizontal');
			$data['form_hidden'] = '';
			$data['form_close_att'] = '';
			$data['form_field'] = array(
				'nm_tipe_admin' => array(
					'<div class="form-group">',
					'<label for="idTxtNmGroup" class="col-md-2 control-label">Nama Group Barang</label>',
					'<div class="col-md-4">',
					'<div id="idErrNmGroup"></div>',
					form_input(array('type' => 'hidden', 'name' => 'txtKdGroup', 'id' => 'idTxtKdGroup', 'value' => $kd_group_barang)),
					form_input(array('name' => 'txtNmGroup', 'id' => 'idTxtNmGroup', 'class' => 'form-control', 'placeholder' => 'Nama Group Barang', 'value' => $nm_group)),
					'</div>',
					'</div>',
				),
			);

			$data['form_btn'] = array(
				'btn_submit' => array(
					'<button type="submit" name="btnSubmit" class="btn btn-primary pull-right">',
					$btn_form,
					'</button>',
				),
			);
			$data['form_box'] = 'idBoxFormBody';
			$data['form_alert'] = 'idAlertForm';
			$data['form_overlay'] = 'idOverlayForm';

			$this->load->view('page/admin/v_form', $data);
		endif;
	}

	function submit_form($act){
		if ($this->input->is_ajax_request()) :
			$this->load->library(array('form_validation'));
			$str['csrf'] = $this->security->get_csrf_hash();

			$this->form_validation->set_rules('txtNmGroup', 'Nama Group Barang', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'errValidation';
				$str['idErrNmGroup'] = (!empty(form_error('txtNmGroup')))?buildLabel('warning', form_error('txtNmGroup', '"', '"')):'';
			else :
				/*
				** Jika validasi berhasil dan tidak ada error
				** Data dari form akan diinputkan kedalam tabel
				** Jika query input return true
				** alert success akan ditampilakan, selain itu error (kesalahan sistem!)
				*/
				$nm_group = $this->input->post('txtNmGroup');
				if ($act == 'input') :
					$conds = array(
						'select' => 'tgl_input, kd_group_barang',
						'order_by' => 'tgl_input DESC',
					);
					$kd_group_barang = $this->m_builder->buat_kode('tm_group_barang', $conds, 2, '');

					$data = array(
						'kd_group_barang' => $kd_group_barang,
						'nm_group' => $nm_group,
						'tgl_input' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					$aksi = $this->db->insert('tm_group_barang', $data);
					$label_err = 'menambahkan';
				elseif ($act == 'edit') :
					$kd_group_barang = $this->input->post('txtKdGroup');

					$data = array(
						'nm_group' => $nm_group,
						'tgl_edit' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					$where = array('kd_group_barang' => $kd_group_barang);
					$aksi = $this->db->update('tm_group_barang', $data, $where);
					$label_err = 'mengubah';
				endif;

				if ($aksi) :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' product group dengan kd_group_barang \''.$kd_group_barang.'\' nm_group \''.$nm_group.'\'');
					$str['confirm'] = 'success';
					$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' data group barang!');
				else :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' product group dengan kd_group_barang \''.$kd_group_barang.'\' nm_group \''.$nm_group.'\'');
					$str['confirm'] = 'errValidation';
					$str['idErrForm'] = buildLabel('danger', 'Gagal '.$label_err.' data group barang, Kesalahan sistem!');
				endif;
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function hapus_data() {
		if ($this->input->is_ajax_request()) :
			$kd_group_barang = $this->input->get('id');
			$label_err = 'menghapus';
			$aksi = $this->db->delete('tm_group_barang', array('kd_group_barang' => $kd_group_barang));

			$data = array(
				'group_barang_kd' => '',
			);
			$where = array('group_barang_kd' => $kd_group_barang);
			$aksi = $this->db->update('tm_barang', $data, $where);

			if ($aksi) :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' product group dengan kd_group_barang \''.$kd_group_barang.'\', kd_group_barang \''.$kd_group_barang.'\' yang digunakan di tm_barang juga dihapus!');
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' data group barang!');
			else :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' product group dengan kd_group_barang \''.$kd_group_barang.'\'');
				$str['alert'] = buildLabel('danger', 'Gagal '.$label_err.' data group barang, Kesalahan sistem!');
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}