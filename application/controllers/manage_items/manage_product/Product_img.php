<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_img extends MY_Controller {
	private $my_db = 'db_caterlindo_ppic';
	private $my_tbl = 'td_barang_media';
	private $p_key = 'kd_barang_media';

	function __construct() {
		parent::__construct();
		
		$this->load->model('m_builder');
		$this->load->helper(array('form', 'html', 'my_helper'));
	}

	function index() {
		if ($this->input->is_ajax_request()) :
			// parent::administrator();
			$id = $this->input->get('id');
			$this->db->where(array('kd_barang' => $id));
			$this->db->from('tm_barang');
			$q_barang = $this->db->get();
			$r_barang = $q_barang->row();
			$item_code = $r_barang->item_code;

			$data['t_box_class'] = 'box-success';
			$data['t_attr'] = 'id="idBoxTableImg"';
			$data['btn_close'] = TRUE;
			$data['id_btn_close'] = 'idBtnCloseImg';
			$data['id_btn_tambah'] = 'idBtnTambahImg';

			$data['table_title'] = 'Product Images : '.$item_code;
			$data['table_alert'] = 'idAlertTableImg';
			$data['table_loader'] = 'idLoaderTableImg';
			$data['table_name'] = 'idTableImg';
			$data['table_overlay'] = 'idTableImgOverlay';
			$item['kd_barang'] = $id;

			$this->load->section('table_js', 'script/manage_items/manage_product/data_product/img_scriptJS', $item);
			$this->load->view('page/admin/v_table', $data);
		endif;
	}

	function data_lihat() {
		if ($this->input->is_ajax_request()) :
			/* tabel property */
			$id = $this->input->get('id');
			$this->db->where(array('id' => $this->session->kd_access_img));
			$this->db->from('tb_set_dropdown');
			$q_set = $this->db->get();
			$r_set = $q_set->row();
			$access_img = $r_set->nm_select;
			$t_data['btn'] = TRUE;
			if ($access_img == 'Product') :
				$product_img = '';
				$sketch_img = 'class="never"';
			elseif ($access_img == 'Sketch') :
				$product_img = 'class="never"';
				$sketch_img = '';
			elseif ($access_img == 'Semua') :
				$product_img = '';
				$sketch_img = '';
			endif;
			$t_data['table_id'] = 'dataTablePrices';
			$t_data['t_header'] = array(
				'<th style="width:1%;" class="never">KdBarang</th>',
				'<th style="width:5%;" '.$product_img.'>Product Img</th>',
				'<th style="width:5%;" '.$sketch_img.'>Welding Sketch</th>',
				'<th style="width:5%;" '.$sketch_img.'>Revet Sketch</th>',
				'<th style="width:1%;">Status</th>',
			);
			$t_data['t_uri'] = base_url().'manage_items/manage_product/product_img/data_table/'.$id;
			$t_data['t_order'] = '[2, "DESC"]';
			$t_data['t_col_def'] = '';

			$this->load->view('page/admin/table_ssp', $t_data);
		endif;
	}

	function data_table($id) {
		if (!$this->input->is_ajax_request()) :
            exit('No direct script access allowed');
        else :
            $this->load->library('ssp');

			$columns = array(
			    array( 'db' => 'a.'.$this->p_key,
						'dt' => 1, 'field' => $this->p_key,
						'formatter' => function($d, $row){
							if ($this->session->update_access == '1') :
								$btn_edit = '<a id="edit" title="Edit Data" href="javascript:void(0);" onclick="editDataImg(\''.$d.'\', \''.$row[1].'\')"><i class="fa fa-pencil"></i> Edit Data</a>';
								$divider = '<li class="divider"></li>';
							else :
								$btn_edit = '';
								$divider = '';
							endif;
							if ($this->session->delete_access == '1') :
								$btn_delete = '<a id="delete" title="Hapus Data" href="javascript:void(0);" onclick="return confirm(\'Apakah anda yakin?\')?hapusDataImg(\''.$d.'\'):false;"><i class="fa fa-trash"></i> Hapus Data</a>';
							else :
								$btn_delete = '';
							endif;

							$btn = '<div align="center"><div class="btn-group"><button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">Opsi <span class="caret"></span></button><ul class="dropdown-menu"><li>'.$btn_edit.'</li>'.$divider.'<li>'.$btn_delete.'</li></ul></div></div>';
							
							return $btn;
						} ),
			    array( 'db' => 'a.barang_kd',
			    		'dt' => 2, 'field' => 'barang_kd',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'a.media_product',
			    		'dt' => 3, 'field' => 'media_product',
			    		'formatter' => function($d, $row){
			    			$d = $this->security->xss_clean($d);
			    			if (!empty($d)) :
			    				$d = '<strong>'.anchor_popup('assets/admin_assets/dist/img/product_media/product_image/'.$d.'', 'Produk Image', array('title' => 'Produk Image '.$d)).'</strong>';
			    			else :
			    				$d = '-';
			    			endif;

			    			return $d;
			    		} ),
			    array( 'db' => 'a.media_sketch_welding',
			    		'dt' => 4, 'field' => 'media_sketch_welding',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);
			    			if (!empty($d)) :
			    				$d = '<strong>'.anchor_popup('assets/admin_assets/dist/img/product_media/product_image/'.$d.'', 'Welding Sketch', array('title' => 'Welding Sketch '.$d)).'</strong>';
			    			else :
			    				$d = '-';
			    			endif;

			    			return $d;
			    		} ),
			    array( 'db' => 'a.media_sketch_revet',
			    		'dt' => 5, 'field' => 'media_sketch_revet',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);
			    			if (!empty($d)) :
			    				$d = '<strong>'.anchor_popup('assets/admin_assets/dist/img/product_media/product_image/'.$d.'', 'Revet Sketch', array('title' => 'Revet Sketch '.$d)).'</strong>';
			    			else :
			    				$d = '-';
			    			endif;

			    			return $d;
			    		} ),
			    array( 'db' => 'IF(a.barang_kd IN (SELECT c.kd_barang FROM tm_barang c WHERE c.kd_barang = "'.$id.'" AND c.barang_media_kd = a.kd_barang_media),"Aktif","Tidak Aktif") AS stts_pakai',
			    		'dt' => 6, 'field' => 'stts_pakai',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			);

			$sql_details = sql_connect();

			$joinQuery = "
				FROM
					td_barang_media a
				LEFT JOIN tb_admin b ON a.admin_kd = b.kd_admin
			";
			$where = "a.barang_kd = '".$id."'";

			echo json_encode(
			    SSP::simple( $_GET, $sql_details, $this->my_tbl, $this->p_key, $columns, $joinQuery, $where )
			);
		endif;
	}

	function check_img($img, $path) {	
		if (!empty($img) && is_file($path)
			&& getimagesize($path)
			&& ($this->security->xss_clean($img, TRUE) == TRUE)
		) :
			return TRUE;
		else :
			return FALSE;
		endif;
	}

	function data_form(){
		if ($this->input->is_ajax_request()) :
			// Form data
			$act = $this->input->get('act');
			$id = $this->input->get('id');
			$kd = $this->input->get('kd');
			if (!empty($kd) && $act == 'edit') :
				$det = $this->m_builder->getRow($this->my_tbl, $data = array($this->p_key => $id), 'row');
				$id_form = 'idFormEdit';
				$data['f_box_class'] = 'box-warning';
				$data['form_title'] = 'Edit Image Barang';
				$btn_form = '<i class="fa fa-edit"></i> Edit Data';
				$kd_barang_media = $this->security->xss_clean($det->kd_barang_media);
				$barang_kd = $this->security->xss_clean($det->barang_kd);
				// Form for product image
				$media_product = $det->media_product;
				$product_path = 'assets/admin_assets/dist/img/product_media/product_image/'.$media_product;
				$product_preview = array('');
				if ($this->check_img($media_product, $product_path)) :
					$product_preview = array(
						'<div class="form-group">',
						'<label for="idFileImage" class="col-md-2 control-label">Image Preview</label>',
						'<div class="col-md-4">',
						'<img class="img-responsive" src="'.base_url($product_path).'">',
						'</div>',
						'</div>',
					);
				else :
					$product_preview = array('');
				endif;
				// Form for product sketch welding
				$media_sketch = $det->media_sketch_welding;
				$sketch_path = 'assets/admin_assets/dist/img/product_media/sketch_welding/'.$media_sketch;
				if (!empty($media_sketch)) :
					$sketch_preview = array(
						'<div class="form-group">',
						'<label for="idFilePreviewWelding" class="col-md-2 control-label">Welding Link Preview</label>',
						'<div class="col-md-4">',
						'<a href="'.base_url($sketch_path).'" target="_blank">Welding Preview</a>',
						'</div>',
						'</div>',
					);
				else :
					$sketch_preview = array('');
				endif;
				// Form for product sketch revet
				$media_revet = $det->media_sketch_revet;
				$revet_path = 'assets/admin_assets/dist/img/product_media/sketch_revet/'.$media_revet;
				if (!empty($media_revet)) :
					$revet_preview = array(
						'<div class="form-group">',
						'<label for="idFilePreviewRevet" class="col-md-2 control-label">Rivet Link Preview</label>',
						'<div class="col-md-4">',
						'<a href="'.base_url($revet_path).'" target="_blank">Revet Preview</a>',
						'</div>',
						'</div>',
					);
				else :
					$revet_preview = array('');
				endif;
				$check = $this->m_builder->getRow('tm_barang', $data = array('barang_media_kd' => $kd_barang_media), 'num_rows');
				$status = ($check > 0)?TRUE:FALSE;
			elseif (!empty($id) && $act == 'input') :
				$id_form = 'idFormInput';
				$data['f_box_class'] = 'box-success';
				$data['form_title'] = 'Input Image Barang';
				$btn_form = '<i class="fa fa-save"></i> Input Data';
				$kd_barang_media = '';
				$barang_kd = $id;
				$product_preview = array('');
				$sketch_preview = array('');
				$revet_preview = array('');
				$status = FALSE;
			endif;

			// Dapatkan media yang dipakai oleh product
			$m_barang = $this->m_builder->getRow('tm_barang', $data = array('kd_barang' => $kd), 'row');
			if (!empty($kd) && $m_barang->barang_media_kd == $id) :
				$det = $this->m_builder->getRow($this->my_tbl, $data = array($this->p_key => $id), 'row');
				$media_product = $det->media_product;
				$media_sketch = $det->media_sketch_welding;
				$revet_sketch = $det->media_sketch_revet;
			else :
				$this->db->order_by('tgl_input DESC');
				$this->db->from('td_barang_media');
				$q_media = $this->db->get();
				$j_media = $q_media->num_rows();
				if ($j_media > 0) :
					$det = $q_media->row();
					$media_product = $det->media_product;
					$media_sketch = $det->media_sketch_welding;
					$revet_sketch = $det->media_sketch_revet;
				else :
					$media_product = '';
					$media_sketch = '';
					$revet_sketch = '';
				endif;
			endif;
			// Tentukan hak akses upload media			
			if ($this->session->upload_product == '1') :
				$product_img = array(
					'<div class="form-group">',
					'<label for="idFileProduct" class="col-md-2 control-label">Product Image</label>',
					'<div class="col-md-4">',
					'<div id="idErrFileProduct"></div>',
					form_upload(array('name' => 'fileProduct', 'id' => 'idFileProduct', 'class' => 'form-control')),
					form_input(array('type' => 'hidden', 'name' => 'txtProduct', 'id' => 'idFileProduct', 'value' => $media_product)),
					'</div>',
					'</div>',
				);
			else :
				$product_img = array(form_input(array('type' => 'hidden', 'name' => 'txtProduct', 'id' => 'idFileProduct', 'value' => $media_product)));
			endif;
			if ($this->session->upload_sketch == '1') :
				$sketch_img = array(
					'<div class="form-group">',
					'<label for="idFileSketch" class="col-md-2 control-label">Welding File</label>',
					'<div class="col-md-4">',
					'<div id="idErrFileSketch"></div>',
					form_upload(array('name' => 'fileSketch', 'id' => 'idFileSketch', 'class' => 'form-control')),
					form_input(array('type' => 'hidden', 'name' => 'txtSketch', 'id' => 'idFileSketch', 'value' => $media_sketch)),
					'</div>',
					'</div>',
				);
				$revet_img = array(
					'<div class="form-group">',
					'<label for="idFileRevet" class="col-md-2 control-label">Rivet File</label>',
					'<div class="col-md-4">',
					'<div id="idErrFileRevet"></div>',
					form_upload(array('name' => 'fileSketch', 'id' => 'idFileSketch', 'class' => 'form-control')),
					form_input(array('type' => 'hidden', 'name' => 'txtRevet', 'id' => 'idFileRevet', 'value' => $revet_sketch)),
					'</div>',
					'</div>',
				);
			else :
				$sketch_img = array(form_input(array('type' => 'hidden', 'name' => 'txtSketch', 'id' => 'idFileSketch', 'value' => $media_sketch)));
				$revet_img = array(form_input(array('type' => 'hidden', 'name' => 'txtRevet', 'id' => 'idFileRevet', 'value' => $revet_sketch)));
			endif;

			$this->db->where(array('id' => $this->session->kd_access_img));
			$this->db->from('tb_set_dropdown');
			$q_set = $this->db->get();
			$r_set = $q_set->row();
			$access_img = $r_set->nm_select;
			$t_data['btn'] = TRUE;
			if ($access_img == 'Produk') :
				$product_preview = $product_preview;
				$sketch_preview = array('');
				$revet_preview = array('');
			elseif ($access_img == 'Skect') :
				$product_preview = array('');
				$sketch_preview = $sketch_preview;
				$revet_preview = $revet_preview;
			elseif ($access_img == 'Semua') :
				$product_preview = $product_preview;
				$sketch_preview = $sketch_preview;
				$revet_preview = $revet_preview;
			endif;

			$data['f_attr'] = 'id="idBoxImgForm"';
			$data['form_url'] = '';
			$data['form_att'] = array('id' => $id_form, 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data');
			$data['form_hidden'] = '';
			$data['form_close_att'] = '';
			$data['form_field'] = array(
				$product_preview,
				$product_img,
				$sketch_preview,
				$sketch_img,
				$revet_preview,
				$revet_img,
				'check_status' => array(
					'<div class="form-group">',
					'<div class="col-sm-offset-2 col-sm-10"><div class="checkbox">',
					'<label>',
					form_input(array('type' => 'hidden', 'name' => 'txtKdMedia', 'id' => 'idTxtKdImg', 'value' => $kd_barang_media)),
					form_input(array('type' => 'hidden', 'name' => 'txtKdBarang', 'id' => 'idTxtKdBarang', 'value' => $barang_kd)),
					form_checkbox(array('name' => 'chkStatus', 'id' => 'idChkStatus', 'value' => '1', 'checked' => $status,)),
					'Aktifkan Image',
					'</label></div></div></div>',
				),
			);

			$data['form_btn'] = array(
				'btn_submit' => array(
					'<button type="submit" name="btnSubmit" id="idBtnSubmit" class="btn btn-warning pull-right">',
					$btn_form,
					'</button>',
				),
			);
			$data['form_box'] = 'idBoxFormBody';
			$data['form_alert'] = 'idAlertForm';
			$data['form_overlay'] = 'idOverlayForm';
			$data['id_btn_close'] = 'idBtnTutup';

			$this->load->section('form_js', 'script/manage_items/manage_product/data_product/img_formJS');
			$this->load->view('page/admin/v_form', $data);
		endif;
	}

	function upload_img($folder, $path, $files, $allowed, $file_name){
		create_dir($folder, $path, '0777');
		$file['upload_path']   = $folder.$path;
		$file['allowed_types'] = $allowed;
		$file['max_size']      = 20000;
		$file['remove_spaces'] = TRUE;
		$file['file_name'] 	  = $file_name;

		$this->load->library('upload', $file);

		if ($this->upload->do_upload($files)) :
			return $this->upload->data();
		else :
			return FALSE;
		endif;
	}

	function compress_img($img_name) {
		$config['image_library'] = 'gd2';
		$config['source_image'] = './assets/admin_assets/dist/img/product_media/'.$img_name;
		$config['maintain_ratio'] = TRUE;
		$config['width'] = 600;

		$this->load->library('image_lib', $config);

		$this->image_lib->resize();
	}

	function submit_form($act){
		if ($this->input->is_ajax_request()) :
			$this->load->library(array('form_validation'));
			$str['csrf'] = $this->security->get_csrf_hash();
			$kd_barang = $this->input->post('txtKdBarang');

			if (($act == 'input') || ($act == 'edit' && !empty($_FILES['fileProduct']['name']))) :
				$temp = explode(".", $_FILES['fileProduct']['name']);
				$extension = end($temp);
				$product_folder = './assets/admin_assets/dist/img/product_media/';
				$product_path = 'product_image/';
				$product_ext = $this->m_builder->getRow('tb_set_dropdown', array('jenis_select' => 'upload_product'), 'row');
				$product_name = $kd_barang.'_'.date('Ymd_His').'.'.$extension;
				$product_upload = $this->upload_img($product_folder, $product_path, 'fileProduct', $product_ext->nm_select, $product_name);
				$this->compress_img($product_name);
			else :
				$product_upload = TRUE;
				$product_name = $this->input->post('txtProduct');
			endif;

			if (($act == 'input') || ($act == 'edit' && !empty($_FILES['fileSketch']['name']))) :
				$temp = explode(".", $_FILES['fileSketch']['name']);
				$extension = end($temp);
				$sketch_folder = './assets/admin_assets/dist/img/product_media/';
				$sketch_path = 'sketch_welding/';
				$sketch_ext = $this->m_builder->getRow('tb_set_dropdown', array('jenis_select' => 'upload_sketch'), 'row');
				$sketch_name = $kd_barang.'_'.date('Ymd_His').'.'.$extension;
				if ($extension == $sketch_ext->nm_select) :
					if (move_uploaded_file($_FILES['fileSketch']['tmp_name'], $sketch_folder.$sketch_path.$sketch_name)) :
						$sketch_upload = TRUE;
						$this->compress_img($sketch_name);
					else :
						$sketch_upload = FALSE;
					endif;
				endif;
			else :
				$sketch_upload = TRUE;
				$sketch_name = $this->input->post('txtSketch');
			endif;

			if (($act == 'input') || ($act == 'edit' && !empty($_FILES['fileRevet']['name']))) :
				$temp = explode(".", $_FILES['fileRevet']['name']);
				$extension = end($temp);
				$revet_folder = './assets/admin_assets/dist/img/product_media/';
				$revet_path = 'sketch_revet/';
				$revet_ext = $this->m_builder->getRow('tb_set_dropdown', array('jenis_select' => 'upload_sketch'), 'row');
				$revet_name = $kd_barang.'_'.date('Ymd_His').'.'.$extension;
				if ($extension == $revet_ext->nm_select) :
					if (move_uploaded_file($_FILES['fileRevet']['tmp_name'], $revet_folder.$revet_path.$revet_name)) :
						$revet_upload = TRUE;
						$this->compress_img($revet_name);
					else :
						$revet_upload = FALSE;
					endif;
				endif;
			else :
				$revet_upload = TRUE;
				$revet_name = $this->input->post('txtRevet');
			endif;

			if ($product_upload == FALSE || $sketch_upload == FALSE || $revet_upload == FALSE) :
				$str['confirm'] = 'errValidation';
				if ($product_upload == FALSE) :
					$str['idErrFileProduct'] = buildLabel('danger', 'Gagal upload file product');
				endif;
				if ($sketch_upload == FALSE) :
					$str['idErrFileSketch'] = buildLabel('danger', 'Gagal upload file sketch wielding');
				endif;
				if ($revet_upload == FALSE) :
					$str['idErrFileRevet'] = buildLabel('danger', 'Gagal upload file sketch revet');
				endif;
			else :
				/*
				** Jika validasi berhasil dan tidak ada error
				** Data dari form akan diinputkan kedalam tabel
				** Jika query input return true
				** alert success akan ditampilakan, selain itu error (kesalahan sistem!)
				*/
				$kd_barang = $this->input->post('txtKdBarang');
				$media_product = $product_name;
				$media_sketch = $sketch_name;
				$revet_sketch = $revet_name;
				$check = !empty($this->input->post('chkStatus'))?'1':'0';
				if ($act == 'input') :
					$conds = array(
						'select' => 'tgl_input, '.$this->p_key,
						'order_by' => 'tgl_input DESC',
						'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
					);
					$kd_barang_media = $this->m_builder->buat_kode($this->my_tbl, $conds, 3, 'MPR');

					$data_media = array(
						'kd_barang_media' => $kd_barang_media,
						'barang_kd' => $kd_barang,
						'media_product' => $media_product,
						'media_sketch_welding' => $media_sketch,
						'media_sketch_revet' => $revet_sketch,
						'tgl_input' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					$aksi = $this->db->insert($this->my_tbl, $data_media);

					if ($check == '1') :
						$data_master = array(
							'barang_media_kd' => $kd_barang_media,
							'tgl_edit' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin'),
						);
						$where_master = array('kd_barang' => $kd_barang);
						$aksi = $this->db->update('tm_barang', $data_master, $where_master);
					endif;
					$label_err = 'menambahkan';
				elseif ($act == 'edit') :
					$kd_barang_media = $this->input->post('txtKdMedia');

					$data_media = array(
						'barang_kd' => $kd_barang,
						'media_product' => $media_product,
						'media_sketch_welding' => $media_sketch,
						'media_sketch_revet' => $revet_sketch,
						'tgl_edit' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					$where_media = array('kd_barang_media' => $kd_barang_media);
					$aksi = $this->db->update($this->my_tbl, $data_media, $where_media);

					if ($check == '1') :
						$data_master = array(
							'barang_media_kd' => $kd_barang_media,
							'tgl_edit' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin'),
						);
						$where_master = array('kd_barang' => $kd_barang);
						$aksi = $this->db->update('tm_barang', $data_master, $where_master);
					elseif ($check == '0') :
						$this->db->where(array('barang_media_kd' => $kd_barang_media));
						$this->db->from('tm_barang');
						$q_barang = $this->db->get();
						$j_barang = $q_barang->num_rows();

						if ($j_barang > 0) :
							$data_master = array(
								'barang_media_kd' => '',
								'tgl_edit' => date('Y-m-d H:i:s'),
								'admin_kd' => $this->session->userdata('kd_admin'),
							);
							$where_master = array('kd_barang' => $kd_barang);
							$aksi = $this->db->update('tm_barang', $data_master, $where_master);
						endif;
					endif;
					$label_err = 'mengubah';
				endif;

				if ($aksi) :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' product media dengan kd_barang_media \''.$kd_barang_media.'\' media_product \''.$media_product.'\' media_sketch \''.$media_sketch.'\', untuk kd_barang \''.$kd_barang.'\'');
					$str['confirm'] = 'success';
					$str['kd_barang'] = $kd_barang;
					$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' product price!');
				else :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' product media dengan kd_barang_media \''.$kd_barang_media.'\' media_product \''.$media_product.'\' media_sketch \''.$media_sketch.'\', untuk kd_barang \''.$kd_barang.'\'');
					$str['confirm'] = 'errValidation';
					$str['idErrForm'] = buildLabel('danger', 'Gagal '.$label_err.' product price, Kesalahan sistem!');
				endif;
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function hapus_data() {
		if ($this->input->is_ajax_request()) :
			$kd_barang_media = $this->input->get('id');
			$label_err = 'menghapus';
			$det = $this->m_builder->getRow($this->my_tbl, $data = array($this->p_key => $kd_barang_media), 'row');
			$kd_barang = $det->barang_kd;
			$aksi = $this->db->delete($this->my_tbl, array($this->p_key => $kd_barang_media));

			$data = array(
				'barang_media_kd' => '',
			);
			$where = array('barang_media_kd' => $kd_barang_media);
			$aksi = $this->db->update('tm_barang', $data, $where);

			if ($aksi) :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' product media dengan kd_barang_media \''.$kd_barang_media.'\', untuk kd_barang \''.$kd_barang.'\'');
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' data product!');
			else :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' product media dengan kd_barang_media \''.$kd_barang_media.'\', untuk kd_barang \''.$kd_barang.'\'');
				$str['alert'] = buildLabel('danger', 'Gagal '.$label_err.' data product, Kesalahan sistem!');
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}