<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_grouping extends MY_Controller {
	private $db_name = 'db_caterlindo_ppic';
	private $tbl_name = 'tm_product_grouping';
	private $p_key = 'kd_mgrouping';

	function __construct() {
		parent::__construct();
		
		$this->load->model('m_builder');
		$this->load->helper(array('form', 'html', 'my_helper'));
	}

	function index() {
		parent::administrator();

		$data['t_box_class'] = 'box-primary';
		$title = $this->uri->segment_array();
		$title = end($title);
		if (strpos($title, '_') !== FALSE ) :
			$title = str_replace('_', ' ', $title);
		endif;
		$title = ucwords($title);
		$data['table_title'] = $title;
		$data['table_alert'] = 'idAlertTable';
		$data['table_loader'] = 'idLoaderTable';
		$data['table_name'] = 'idTable';
		$data['table_overlay'] = 'idTableOverlay';
		$script['class_name'] = 'product_grouping';
		$data['btn_add'] = cek_permission('PRODUCTGROUPING_CREATE');

		$this->load->css('assets/admin_assets/plugins/select2/select2.min.css');
		$this->load->css('assets/admin_assets/plugins/select2/select2-bootstrap.min.css');
		$this->load->js('assets/admin_assets/plugins/select2/select2.full.min.js');		
		$this->load->section('scriptJS', 'script/manage_items/manage_product/product_grouping/scriptJS', $script);
		// $this->data_admin_form();
		$this->load->view('page/admin/v_table', $data);
	}

	function data_lihat() {
		if ($this->input->is_ajax_request()) :
			/* tabel property */
			$t_data['btn'] = TRUE;
			$t_data['t_header'] = array(
				'<th style="width:1%;">Code</th>',
				'<th style="width:5%;">Desc</th>',
			);
			$t_data['t_uri'] = base_url().'manage_items/manage_product/product_grouping/data_table';
			$t_data['t_order'] = '[2, "asc"]';

			$this->load->view('page/admin/table_ssp', $t_data);
		endif;
	}

	function data_table() {
		if (!$this->input->is_ajax_request()) :
            exit('No direct script access allowed');
        else :

            $this->load->library('ssp');

			$table = $this->tbl_name;

			$primaryKey = $this->p_key;

			$columns = array(
			    array( 'db' => $this->p_key,
						'dt' => 1, 'field' => $this->p_key,
						'formatter' => function($d){
							$btn_edit = '';
							$divider = '';
							$btn_delete = '';
							if (cek_permission('PRODUCTGROUPING_UPDATE')) :
								$btn_edit = '<a id="edit" title="Edit Data" href="javascript:void(0);" onclick="editData(\''.$d.'\')"><i class="fa fa-pencil"></i> Edit Data</a>';
								$divider = '<li class="divider"></li>';
							endif;
							if (cek_permission('PRODUCTGROUPING_DELETE')) :
								$btn_delete = '<a id="delete" title="Hapus Data" href="javascript:void(0);" onclick="return confirm(\'Apakah anda yakin?\')?hapusData(\''.$d.'\'):false;"><i class="fa fa-trash"></i> Hapus Data</a>';
							endif;

							$btn = '<div align="center"><div class="btn-group"><button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">Opsi <span class="caret"></span></button><ul class="dropdown-menu"><li>'.$btn_edit.'</li>'.$divider.'<li>'.$btn_delete.'</li></ul></div></div>';
							
							return $btn;
						} ),
			    array( 'db' => 'code_grouping',
			    		'dt' => 2, 'field' => 'code_grouping',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'desc_group',
			    		'dt' => 3, 'field' => 'desc_group',
			    		'formatter' => function($d){
			    			$d = word_break($this->security->xss_clean($d), 5);

			    			return $d;
			    		} ),
			);

			$sql_details = sql_connect();

			$joinQuery = "";
			$where = "";

			echo json_encode(
			    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $where )
			);
		endif;
	}

	function data_form(){
		if ($this->input->is_ajax_request()) :
			// Form data
			$id = $this->input->get('id');
			if (!empty($id)) :
				$det = $this->m_builder->getRow($this->tbl_name, array($this->p_key => $id), 'row');
				$id_form = 'idFormEdit';
				$data['f_box_class'] = 'box-warning';
				$data['form_title'] = 'Edit Product Grouping';
				$btn_form = '<i class="fa fa-edit"></i> Edit Data';

				// Data Master Group
				$kd_mgrouping = $this->security->xss_clean($det->kd_mgrouping);
				$code_grouping = $this->security->xss_clean($det->code_grouping);
				$desc_group = $this->security->xss_clean($det->desc_group);

				// Data Detail Group
				$this->db->from('td_product_grouping')
					->where(array('mgrouping_kd' => $kd_mgrouping))
					->order_by('kd_dgrouping ASC');
				$query = $this->db->get();
				$d_det = $query->result();
				foreach ($d_det as $data_det) :
					$kd_dgrouping = $this->security->xss_clean($data_det->kd_dgrouping);
					$barang_kd = $this->security->xss_clean($data_det->barang_kd);
					$status = $this->security->xss_clean($data_det->status);

					$detail[$kd_dgrouping] = $barang_kd.'-'.$status;
				endforeach;
			else :
				$id_form = 'idFormInput';
				$data['f_box_class'] = 'box-info';
				$data['form_title'] = 'Input Product Grouping';
				$btn_form = '<i class="fa fa-save"></i> Input Data';

				// Data Master Group
				$kd_mgrouping = '';
				$code_grouping = '';
				$desc_group = '';
				$detail['kosong'] = ' - ';
			endif;

			// Buat form untuk detail grouping
			$this->db->from('tm_barang');
			$q_barang = $this->db->get();
			$r_barang = $q_barang->result();
			$pil_barang = array('' => '-- Pilih Product --');
			foreach ($r_barang as $d_barang) :
				$kd_barang = $this->security->xss_clean($d_barang->kd_barang);
				$item_code = $this->security->xss_clean($d_barang->item_code);
				$deskripsi_barang = $this->security->xss_clean($d_barang->deskripsi_barang);
				$pil_barang[$kd_barang] = $item_code.' - '.$deskripsi_barang;
			endforeach;

			$no_child = 0;
			$form_child = '';
			$d_det = $this->m_builder->getRow('td_product_grouping', array('mgrouping_kd' => $kd_mgrouping), 'result');
			foreach ($detail as $det_group => $value) :
				$no_child++;
				$pecah_detail = explode('-', $value);
				$barang_kd = $pecah_detail[0];
				$status = $pecah_detail[1];

				$sel_barang = $barang_kd;
				$attr_barang = array('id' => 'idSelChild'.$no_child, 'class' => 'form-control select2');

				$status = $no_child == '1'?'head':'body';

				// js button untuk add child
				if ($no_child <= 1) :
					$btn_js = '<a name="btnAdd" id="idBtnAdd" title="Tambah Child" class="btn btn-sm btn-primary btnAdd"><i class="fa fa-plus"></i></a>';
				else :
					$btn_js = '<a name="btnSub" id="idBtnSub" title="Hapus Child" class="btn btn-sm btn-danger btnSub"><i class="fa fa-minus"></i></a>';
				endif;

				// label
				$label_child = $no_child <= 1?'Pilih Product':'';

				$form_child .= '<div class="form-group">
					<label for="idSelChild'.$no_child.'" class="col-md-2 control-label">'.$label_child.'</label>
					<div class="col-md-3">';
				$form_child .= form_dropdown('selChild[]', $pil_barang, $sel_barang, $attr_barang);
				$form_child .= form_input(array('type' => 'hidden', 'name' => 'selStatus[]', 'value' => $status));
				$form_child .= '</div><div class="col-md-1"><b>'.ucfirst($status).'</b></div>';


				$form_child .= '<div class="col-md-1">'.$btn_js.'</div></div>';
			endforeach;

			$data['f_attr'] = 'id="idBoxForm"';
			$data['form_url'] = '';
			$data['form_att'] = array('id' => $id_form, 'class' => 'form-horizontal');
			$data['form_hidden'] = '';
			$data['form_close_att'] = '';
			$data['form_field'] = array(
				'code_grouping' => array(
					'<div class="form-group">',
					'<label for="idTxtCode" class="col-md-2 control-label">Code Group</label>',
					'<div class="col-md-4">',
					'<div id="idErrCode"></div>',
					form_input(array('type' => 'hidden', 'name' => 'txtKdMGroup', 'id' => 'idKdMGroup', 'value' => $kd_mgrouping)),
					form_input(array('name' => 'txtCode', 'id' => 'idTxtCode', 'class' => 'form-control', 'placeholder' => 'Code Group', 'value' => $code_grouping)),
					'</div>',
					'</div>',
				),
				'desc_group' => array(
					'<div class="form-group">',
					'<label for="idTxtDesc" class="col-md-2 control-label">Group Description</label>',
					'<div class="col-md-5">',
					'<div id="idErrDesc"></div>',
					form_textarea(array('name' => 'txtDesc', 'id' => 'idTxtDesc', 'class' => 'form-control', 'placeholder' => 'Group Description', 'value' => $desc_group, 'rows' => '5')),
					'</div>',
					'</div>',
				),
				'form_child' => array(
					$form_child,
				),
			);

			$data['form_btn'] = array(
				'btn_submit' => array(
					'<button type="submit" name="btnSubmit" id="idBtnSubmit" class="btn btn-primary pull-right">',
					$btn_form,
					'</button>',
				),
			);
			$data['form_box'] = 'idBoxFormBody';
			$data['form_alert'] = 'idAlertForm';
			$data['form_overlay'] = 'idOverlayForm';
			$script['class_name'] = 'product_grouping';

			$this->load->section('form_js', 'script/manage_items/manage_product/product_grouping/form_js', $script);
			$this->load->view('page/admin/v_form', $data);
		endif;
	}

	function verify_code() {
		if ($this->input->is_ajax_request()) :
			$code = $this->input->get('id');
			$kd_master = $this->input->get('kd_master');
			if (empty($code)) :
				$str['confirm'] = 'errValidation';
				$str['idErrCode'] = buildLabel('danger', 'Code tidak boleh kosong!');
			else :
				$check_code = $this->m_builder->check_code($code, $this->tbl_name, array('code_grouping' => $code, 'kd_mgrouping !=' => $kd_master));
				if ($check_code) :
					$str['confirm'] = 'success';
					$str['idErrCode'] = buildLabel('success', 'Code bisa dipakai!');
				else :
					$str['confirm'] = 'errValidation';
					$str['idErrCode'] = buildLabel('warning', 'Code sudah dipakai!');
				endif;
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function form_child() {
		if ($this->input->is_ajax_request()) :
			$this->db->from('tm_barang');
			$q_barang = $this->db->get();
			$r_barang = $q_barang->result();
			$pil_barang = array('' => '-- Pilih Product --');
			foreach ($r_barang as $d_barang) :
				$kd_barang = $this->security->xss_clean($d_barang->kd_barang);
				$item_code = $this->security->xss_clean($d_barang->item_code);
				$deskripsi_barang = $this->security->xss_clean($d_barang->deskripsi_barang);
				$pil_barang[$kd_barang] = $item_code.' - '.$deskripsi_barang;
			endforeach;

			$sel_barang = '';
			$attr_barang = array('id' => 'idSelChild', 'class' => 'form-control select2');
			$status = 'body';

			// js button untuk add child			
			$btn_js = '<a name="btnSub" id="idBtnSub" title="Hapus Child" class="btn btn-sm btn-danger btnSub"><i class="fa fa-minus"></i></a>';

			$data['form_child'] = '';
			$data['form_child'] .= '<div class="form-group">
				<label for="idSelChild" class="col-md-2 control-label"></label>
				<div class="col-md-3">';
			$data['form_child'] .= form_dropdown('selChild[]', $pil_barang, $sel_barang, $attr_barang);
			$data['form_child'] .= form_input(array('type' => 'hidden', 'name' => 'selStatus[]', 'value' => $status));
			$data['form_child'] .= '</div><div class="col-md-1"><b>Body</b></div>';
			$data['form_child'] .= '<div class="col-md-1">'.$btn_js.'</div></div>';

			header('Content-Type: application/json');
			echo json_encode($data);
		endif;
	}

	function submit_form($act){
		if ($this->input->is_ajax_request()) :
			$this->load->library(array('form_validation'));
			$str['csrf'] = $this->security->get_csrf_hash();

			$this->form_validation->set_rules('txtCode', 'Code', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);
			$this->form_validation->set_rules('txtDesc', 'Description', 'required',
				array(
					'required' => '{field} tidak boleh kosong!',
				)
			);

			if (!empty($_POST['selChild'])) :
				$this->form_validation->set_rules('selChild[]', 'Product', 'required',
					array(
						'required' => 'Pilihan {field} tidak boleh kosong!',
					)
				);
			endif;

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'errValidation';
				$str['idErrCode'] = (!empty(form_error('txtCode')))?buildLabel('warning', form_error('txtCode', '"', '"')):'';
				$str['idErrDesc'] = (!empty(form_error('txtDesc')))?buildLabel('warning', form_error('txtDesc', '"', '"')):'';
				$str['idErrDimension'] = (!empty(form_error('txtDimension')))?buildLabel('warning', form_error('txtDimension', '"', '"')):'';
				$str['idErrForm'] = (!empty(form_error('selChild[]')))?buildAlert('warning', 'Peringatan!', form_error('selChild[]', '"', '"')):'';
			else :
				/*
				** Jika validasi berhasil dan tidak ada error
				** Data dari form akan diinputkan kedalam tabel
				** Jika query input return true
				** alert success akan ditampilakan, selain itu error (kesalahan sistem!)
				*/
				$code_grouping = $this->input->post('txtCode');
				$desc_group = $this->input->post('txtDesc');
				$kd_child = $this->input->post('selChild');
				$status = $this->input->post('selStatus');
				$j_detail = count($kd_child);
				if ($act == 'input') :
					$conds = array(
						'select' => 'tgl_input, kd_mgrouping',
						'order_by' => 'tgl_input DESC',
						'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
					);
					$kd_mgrouping = $this->m_builder->buat_kode($this->tbl_name, $conds, 3, 'MGR');

					$this->db->trans_start();
					// Input data tabel master grouping
					$data = array(
						'kd_mgrouping' => $kd_mgrouping,
						'barang_kd' => $kd_child[0],
						'code_grouping' => $code_grouping,
						'desc_group' => $desc_group,
						'tgl_input' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					$aksi = $this->db->insert($this->tbl_name, $data);

					// Input data tabel detail grouping
					for ($i=0; $i < $j_detail; $i++) {
						$conds_detail = array(
							'select' => 'tgl_input, kd_dgrouping',
							'order_by' => 'tgl_input DESC, kd_dgrouping DESC',
							'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
						);
						$kd_dgrouping = $this->m_builder->buat_kode('td_product_grouping', $conds_detail, 3, 'DGR');

						$data_detail = array(
							'kd_dgrouping' => $kd_dgrouping,
							'mgrouping_kd' => $kd_mgrouping,
							'barang_kd' => $kd_child[$i],
							'status' => $status[$i],
							'tgl_input' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin'),
						);
						$aksi = $this->db->insert('td_product_grouping', $data_detail);
					}
					$this->db->trans_complete();

					$label_err = 'menambahkan';
				elseif ($act == 'edit') :
					$kd_mgrouping = $this->input->post('txtKdMGroup');

					// Update data tabel master group
					$this->db->trans_start();
					$data = array(
						'barang_kd' => $kd_child[0],
						'code_grouping' => $code_grouping,
						'desc_group' => $desc_group,
						'tgl_edit' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					$where = array('kd_mgrouping' => $kd_mgrouping);
					$aksi = $this->db->update($this->tbl_name, $data, $where);
					// Delete data tabel detail group
					$aksi = $this->db->delete('td_product_grouping', array('mgrouping_kd' => $kd_mgrouping));

					// Input data tabel detail grouping
					for ($i=0; $i < $j_detail; $i++) {
						$conds_detail = array(
							'select' => 'tgl_input, kd_dgrouping',
							'order_by' => 'tgl_input DESC, kd_dgrouping DESC',
							'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
						);
						$kd_dgrouping = $this->m_builder->buat_kode('td_product_grouping', $conds_detail, 3, 'DGR');

						$data_detail = array(
							'kd_dgrouping' => $kd_dgrouping,
							'mgrouping_kd' => $kd_mgrouping,
							'barang_kd' => $kd_child[$i],
							'status' => $status[$i],
							'tgl_input' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin'),
						);
						$aksi = $this->db->insert('td_product_grouping', $data_detail);
					}
					$this->db->trans_complete();

					$label_err = 'mengubah';
				endif;

				if ($this->db->trans_status() === FALSE) :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' grouping product dengan kd_mgrouping \''.$kd_mgrouping.'\' code_grouping \''.$code_grouping.'\' desc_group \''.$desc_group.'\'');
					$str['confirm'] = 'errValidation';
					$str['idErrForm'] = buildAlert('danger', 'Gagal '.$label_err.' grouping product, Kesalahan sistem!');
				else :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' grouping product dengan kd_mgrouping \''.$kd_mgrouping.'\' code_grouping \''.$code_grouping.'\' desc_group \''.$desc_group.'\'');
					$str['confirm'] = 'success';
					$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' grouping product!');
				endif;
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function hapus_data() {
		if ($this->input->is_ajax_request()) :
			$kd_mgrouping = $this->input->get('id');
			$label_err = 'menghapus';
			$aksi['master'] = $this->db->delete($this->tbl_name, array('kd_mgrouping' => $kd_mgrouping));
			$aksi['detail'] = $this->db->delete('td_product_grouping', array('mgrouping_kd' => $kd_mgrouping));

			if ($aksi) :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' berhasil '.$label_err.' grouping product dengan kd_mgrouping \''.$kd_mgrouping.'\', data di\'td_product_grouping\' juga dihapus!');
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' grouping product!');
			else :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->username.' gagal '.$label_err.' grouping product dengan kd_mgrouping \''.$kd_mgrouping.'\', data di\'td_product_grouping\' juga dihapus');
				$str['alert'] = buildAlert('danger', 'Gagal '.$label_err.' grouping product, Kesalahan sistem!');
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}