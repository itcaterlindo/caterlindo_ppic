<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_price extends MY_Controller {

	function __construct() {
		parent::__construct();
		
		$this->load->model('m_builder');
		$this->load->helper(array('form', 'html', 'my_helper'));
	}

	function index() {
		if ($this->input->is_ajax_request()) :
			// parent::administrator();
			$id = $this->input->get('id');
			$this->db->where(array('kd_barang' => $id));
			$this->db->from('tm_barang');
			$q_barang = $this->db->get();
			$r_barang = $q_barang->row();
			$item_code = $r_barang->item_code;

			$data['t_box_class'] = 'box-success';
			$data['t_attr'] = 'id="idBoxTablePrice"';
			$data['btn_close'] = TRUE;
			$data['id_btn_close'] = 'idBtnClosePrice';
			$data['id_btn_tambah'] = 'idBtnTambahPrice';

			$data['table_title'] = 'Harga Product : '.$item_code;
			$data['table_alert'] = 'idAlertTablePrice';
			$data['table_loader'] = 'idLoaderTablePrice';
			$data['table_name'] = 'idTablePrice';
			$data['table_overlay'] = 'idTablePriceOverlay';
			$item['kd_barang'] = $id;

			$this->load->section('table_js', 'script/manage_items/manage_product/data_product/price_scriptJS', $item);
			$this->load->view('page/admin/v_table', $data);
		endif;
	}

	function data_lihat() {
		if ($this->input->is_ajax_request()) :
			/* tabel property */
			$id = $this->input->get('id');
			$this->db->where(array('id' => $this->session->kd_manage_items));
			$this->db->from('tb_set_dropdown');
			$q_set = $this->db->get();
			$r_set = $q_set->row();
			$manage_items = $r_set->nm_select;
			$t_data['btn'] = TRUE;
			if ($manage_items == 'Ekspor') :
				$harga_lokal = 'class="never"';
				$harga_ekspor = '';
			elseif ($manage_items == 'Lokal') :
				$harga_lokal = '';
				$harga_ekspor = 'class="never"';
			elseif ($manage_items == 'Semua') :
				$harga_lokal = '';
				$harga_ekspor = '';
			elseif ($manage_items == 'Tidak Ada') :
				$harga_lokal = 'class="never"';
				$harga_ekspor = 'class="never"';
			endif;
			$t_data['table_id'] = 'dataTablePrices';
			$t_data['t_header'] = array(
				'<th style="width:10%;" '.$harga_lokal.'>Harga Retail</th>',
				'<th style="width:10%;" '.$harga_lokal.'>Harga Reseller</th>',
				'<th style="width:10%;" '.$harga_lokal.'>Harga Distributor</th>',
				'<th style="width:10%;" '.$harga_ekspor.'>Harga Ekspor</th>',
				'<th style="width:5%;">Tgl Input</th>',
				'<th style="width:5%;">Tgl Edit</th>',
				'<th style="width:5%;">Admin</th>',
				'<th style="width:1%;">Status</th>',
				'<th style="width:1%;" class="never">Format Retail</th>',
				'<th style="width:1%;" class="never">Format Reseller</th>',
				'<th style="width:1%;" class="never">Format Distributor</th>',
				'<th style="width:1%;" class="never">Format Ekspor</th>',
			);
			$t_data['t_uri'] = base_url().'manage_items/manage_product/product_price/data_table/'.$id;
			$t_data['t_order'] = '[6, "DESC"]';
			$t_data['t_col_def'] = '{"targets":7, "searchable":false, "orderable":false}';

			$this->load->view('page/admin/table_ssp', $t_data);
		endif;
	}

	function data_table($id) {
		if (!$this->input->is_ajax_request()) :
            exit('No direct script access allowed');
        else :

            $this->load->library('ssp');

			$table = 'td_barang_harga';

			$primaryKey = 'kd_harga_barang';

			$columns = array(
			    array( 'db' => 'a.kd_harga_barang',
						'dt' => 1, 'field' => 'kd_harga_barang',
						'formatter' => function($d){
							if ($this->session->update_access == '1') :
								$btn_edit = '<a id="edit" title="Edit Data" href="javascript:void(0);" onclick="editDataHarga(\''.$d.'\')"><i class="fa fa-pencil"></i> Edit Data</a>';
								$divider = '<li class="divider"></li>';
							else :
								$btn_edit = '';
								$divider = '';
							endif;
							if ($this->session->delete_access == '1') :
								$btn_delete = '<a id="delete" title="Hapus Data" href="javascript:void(0);" onclick="return confirm(\'Apakah anda yakin?\')?hapusDataHarga(\''.$d.'\'):false;"><i class="fa fa-trash"></i> Hapus Data</a>';
							else :
								$btn_delete = '';
							endif;

							$btn = '<div align="center"><div class="btn-group"><button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">Opsi <span class="caret"></span></button><ul class="dropdown-menu"><li>'.$btn_edit.'</li>'.$divider.'<li>'.$btn_delete.'</li></ul></div></div>';
							
							return $btn;
						} ),
			    array( 'db' => 'a.harga_lokal_retail',
			    		'dt' => 2, 'field' => 'harga_lokal_retail',
			    		'formatter' => function($d, $row){
			    			$d = !empty($d)?$d:'0';
			    			$formats = format_currency($d, $row[9]);

			    			return $formats;
			    		} ),
			    array( 'db' => 'a.harga_lokal_reseller',
			    		'dt' => 3, 'field' => 'harga_lokal_reseller',
			    		'formatter' => function($d, $row){
			    			$d = !empty($d)?$d:'0';
			    			$formats = format_currency($d, $row[10]);

			    			return $formats;
			    		} ),
			    array( 'db' => 'a.harga_lokal_distributor',
			    		'dt' => 4, 'field' => 'harga_lokal_distributor',
			    		'formatter' => function($d, $row){
			    			$d = !empty($d)?$d:'0';
			    			$formats = format_currency($d, $row[11]);

			    			return $formats;
			    		} ),
			    array( 'db' => 'a.harga_ekspor',
			    		'dt' => 5, 'field' => 'harga_ekspor',
			    		'formatter' => function($d, $row){
			    			$d = !empty($d)?$d:'0';
			    			$formats = format_currency($d, $row[12]);

			    			return $formats;
			    		} ),
			    array( 'db' => 'a.tgl_input',
			    		'dt' => 6, 'field' => 'tgl_input',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'a.tgl_edit',
			    		'dt' => 7, 'field' => 'tgl_edit',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'b.nm_admin',
			    		'dt' => 8, 'field' => 'nm_admin',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => 'IF(a.barang_kd IN (SELECT c.kd_barang FROM tm_barang c WHERE c.kd_barang = "'.$id.'" AND c.harga_barang_kd = a.kd_harga_barang),"Aktif","Tidak Aktif") AS stts_pakai',
			    		'dt' => 9, 'field' => 'stts_pakai',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => '(SELECT CONCAT(b.icon_type, \' \', b.currency_icon, \' \', b.pemisah_angka, \' \', b.format_akhir) FROM tb_tipe_harga a LEFT JOIN tm_currency b ON b.kd_currency = a.currency_kd WHERE a.nm_harga = \'Harga Retail\') as format_harga_retail',
			    		'dt' => 10, 'field' => 'format_harga_retail',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => '(SELECT CONCAT(b.icon_type, \' \', b.currency_icon, \' \', b.pemisah_angka, \' \', b.format_akhir) FROM tb_tipe_harga a LEFT JOIN tm_currency b ON b.kd_currency = a.currency_kd WHERE a.nm_harga = \'Harga Reseller\') as format_harga_reseller',
			    		'dt' => 11, 'field' => 'format_harga_reseller',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => '(SELECT CONCAT(b.icon_type, \' \', b.currency_icon, \' \', b.pemisah_angka, \' \', b.format_akhir) FROM tb_tipe_harga a LEFT JOIN tm_currency b ON b.kd_currency = a.currency_kd WHERE a.nm_harga = \'Harga Distributor\') as format_harga_distributor',
			    		'dt' => 12, 'field' => 'format_harga_distributor',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			    array( 'db' => '(SELECT CONCAT(b.icon_type, \' \', b.currency_icon, \' \', b.pemisah_angka, \' \', b.format_akhir) FROM tb_tipe_harga a LEFT JOIN tm_currency b ON b.kd_currency = a.currency_kd WHERE a.nm_harga = \'Harga Ekspor\') as format_harga_ekspor',
			    		'dt' => 13, 'field' => 'format_harga_ekspor',
			    		'formatter' => function($d){
			    			$d = $this->security->xss_clean($d);

			    			return $d;
			    		} ),
			);

			$sql_details = sql_connect();

			$joinQuery = "
				FROM
					td_barang_harga a
				LEFT JOIN tb_admin b ON a.admin_kd = b.kd_admin
			";
			$where = "a.barang_kd = '".$id."'";

			echo json_encode(
			    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $where )
			);
		endif;
	}

	function data_form(){
		if ($this->input->is_ajax_request()) :
			// Form data
			$act = $this->input->get('act');
			$id = $this->input->get('id');
			$det = $this->m_builder->getRow('td_barang_harga', $data = array('kd_harga_barang' => $id), 'row');
			if (!empty($id) && $act == 'edit') :
				$id_form = 'idFormEdit';
				$data['f_box_class'] = 'box-warning';
				$data['form_title'] = 'Edit Harga Barang';
				$btn_form = '<i class="fa fa-edit"></i> Edit Data';
				$kd_harga_barang = $this->security->xss_clean($det->kd_harga_barang);
				$barang_kd = $this->security->xss_clean($det->barang_kd);
				// cari harga aktif di tm_barang
				$barang = $this->m_builder->getRow('tm_barang', $data = array('kd_barang' => $barang_kd), 'row');
				$kd_harga_aktif = $barang->harga_barang_kd;
				$cek_jml = $this->m_builder->getRow('td_barang_harga', $data = array('kd_harga_barang' => $kd_harga_aktif), 'num_rows');
				if ($cek_jml > 0) :
					$harga_aktif = $this->m_builder->getRow('td_barang_harga', $data = array('kd_harga_barang' => $kd_harga_aktif), 'row');
				endif;

				$harga_lokal_distributor = $this->security->xss_clean($det->harga_lokal_distributor);
				$harga_lokal_retail = $this->security->xss_clean($det->harga_lokal_retail);
				$harga_lokal_reseller = $this->security->xss_clean($det->harga_lokal_reseller);
				$harga_ekspor = $this->security->xss_clean($det->harga_ekspor);
				$check = $this->m_builder->getRow('tm_barang', $data = array('harga_barang_kd' => $kd_harga_barang), 'num_rows');
				$status = ($check > 0)?TRUE:FALSE;
			elseif (!empty($id) && $act == 'input') :
				$id_form = 'idFormInput';
				$data['f_box_class'] = 'box-success';
				$data['form_title'] = 'Input Harga Barang';
				$btn_form = '<i class="fa fa-save"></i> Input Data';
				$kd_harga_barang = '';
				$barang_kd = $id;
				// cari harga aktif di tm_barang
				$barang = $this->m_builder->getRow('tm_barang', $data = array('kd_barang' => $barang_kd), 'row');
				$kd_harga_aktif = $barang->harga_barang_kd;
				$cek_jml = $this->m_builder->getRow('td_barang_harga', $data = array('kd_harga_barang' => $kd_harga_aktif), 'num_rows');
				if ($cek_jml > 0) :
					$harga_aktif = $this->m_builder->getRow('td_barang_harga', $data = array('kd_harga_barang' => $kd_harga_aktif), 'row');
					$harga_lokal_distributor = $this->security->xss_clean($harga_aktif->harga_lokal_distributor);
					$harga_lokal_retail = $this->security->xss_clean($harga_aktif->harga_lokal_retail);
					$harga_lokal_reseller = $this->security->xss_clean($harga_aktif->harga_lokal_reseller);
					$harga_ekspor = $this->security->xss_clean($harga_aktif->harga_ekspor);
				else :
					$harga_lokal_distributor = '';
					$harga_lokal_retail = '';
					$harga_lokal_reseller = '';
					$harga_ekspor = '';
				endif;
				$status = FALSE;
			endif;

			// Cari kode manage item dari admin
			$this->db->where(array('id' => $this->session->kd_manage_items));
			$this->db->from('tb_set_dropdown');
			$q_manage = $this->db->get();
			$r_manage = $q_manage->row();
			$manage_items = $r_manage->nm_select;
			$lokal = $manage_items == 'Semua' || $manage_items == 'Lokal'?'text':'hidden';
			$hidden_lokal = $manage_items == 'Semua' || $manage_items == 'Lokal'?'visible':'none';
			$ekspor = $manage_items == 'Semua' || $manage_items == 'Ekspor'?'text':'hidden';
			$hidden_ekspor = $manage_items == 'Semua' || $manage_items == 'Ekspor'?'visible':'none';
			$val_distributor = empty($harga_lokal_distributor) && $manage_items == 'Ekspor'?'0':$harga_lokal_distributor;
			$val_retail = empty($harga_lokal_retail) && $manage_items == 'Ekspor'?'0':$harga_lokal_retail;
			$val_reseller = empty($harga_lokal_reseller) && $manage_items == 'Ekspor'?'0':$harga_lokal_reseller;
			$val_ekspor = empty($harga_ekspor) && $manage_items == 'Lokal'?'0':$harga_ekspor;


			$data['f_attr'] = 'id="idBoxPriceForm"';
			$data['form_url'] = '';
			$data['form_att'] = array('id' => $id_form, 'class' => 'form-horizontal');
			$data['form_hidden'] = '';
			$data['form_close_att'] = '';
			$data['form_field'] = array(
				'form_harga_distri' => array(
					'<div class="form-group" style="display:'.$hidden_lokal.';">',
					'<label for="idTxtHargaDistributor" class="col-md-2 control-label">Harga Distributor</label>',
					'<div class="col-md-4">',
					'<div id="idErrHargaDistributor"></div>',
					form_input(array('type' => $lokal, 'name' => 'txtHargaDistributor', 'id' => 'idTxtHargaDistributor', 'class' => 'form-control', 'placeholder' => 'Harga Distributor', 'value' => $val_distributor)),
					'</div>',
					'</div>',
				),
				'form_harga_retail' => array(
					'<div class="form-group" style="display:'.$hidden_lokal.';">',
					'<label for="idTxtHargaRetail" class="col-md-2 control-label">Harga Retail</label>',
					'<div class="col-md-4">',
					'<div id="idErrHargaRetail"></div>',
					form_input(array('type' => $lokal, 'name' => 'txtHargaRetail', 'id' => 'idTxtHargaRetail', 'class' => 'form-control', 'placeholder' => 'Harga Retail', 'value' => $val_retail)),
					'</div>',
					'</div>',
				),
				'form_harga_resell' => array(
					'<div class="form-group" style="display:'.$hidden_lokal.';">',
					'<label for="idTxtHargaResell" class="col-md-2 control-label">Harga Reseller</label>',
					'<div class="col-md-4">',
					'<div id="idErrHargaResell"></div>',
					form_input(array('type' => $lokal, 'name' => 'txtHargaResell', 'id' => 'idTxtHargaResell', 'class' => 'form-control', 'placeholder' => 'Harga Reseller', 'value' => $val_reseller)),
					'</div>',
					'</div>',
				),
				'form_harga_ekspor' => array(
					'<div class="form-group" style="display:'.$hidden_ekspor.';">',
					'<label for="idTxtHargaEkspor" class="col-md-2 control-label">Harga Ekspor</label>',
					'<div class="col-md-4">',
					'<div id="idErrHargaEkspor"></div>',
					form_input(array('type' => $ekspor, 'name' => 'txtHargaEkspor', 'id' => 'idTxtHargaEkspor', 'class' => 'form-control', 'placeholder' => 'Harga Ekspor', 'value' => $val_ekspor)),
					'</div>',
					'</div>',
				),
				'check_status' => array(
					'<div class="form-group">',
					'<div class="col-sm-offset-2 col-sm-10"><div class="checkbox">',
					'<label>',
					form_input(array('type' => 'hidden', 'name' => 'txtKdHarga', 'id' => 'idTxtKdHarga', 'value' => $kd_harga_barang)),
					form_input(array('type' => 'hidden', 'name' => 'txtKdBarang', 'id' => 'idTxtKdBarang', 'value' => $barang_kd)),
					form_checkbox(array('name' => 'chkStatus', 'id' => 'idChkStatus', 'value' => '1', 'checked' => $status,)),
					'Aktifkan Harga',
					'</label></div></div></div>',
				),
			);

			$data['form_btn'] = array(
				'btn_submit' => array(
					'<button type="submit" name="btnSubmit" id="idBtnSubmit" class="btn btn-warning pull-right">',
					$btn_form,
					'</button>',
				),
			);
			$data['form_box'] = 'idBoxFormBody';
			$data['form_alert'] = 'idAlertPriceForm';
			$data['form_overlay'] = 'idOverlayForm';
			$data['id_btn_close'] = 'idBtnTutup';

			$this->load->section('form_js', 'script/manage_items/manage_product/data_product/price_js');
			$this->load->view('page/admin/v_form', $data);
		endif;
	}

	function numeric($str) {
		if (preg_match('/^[0-9]+$/', $str)) :
			return TRUE;
		else :
			$this->form_validation->set_message('numeric', '{field} harus diisi angka!');	
			return FALSE;
		endif;
	}

	function submit_form($act){
		if ($this->input->is_ajax_request()) :
			$this->load->library(array('form_validation'));
			$str['csrf'] = $this->security->get_csrf_hash();

			$this->form_validation->set_rules('txtHargaDistributor', 'Harga Distributor', 'required|numeric');
			$this->form_validation->set_rules('txtHargaRetail', 'Harga Retail', 'required|numeric');
			$this->form_validation->set_rules('txtHargaResell', 'Harga Reseller', 'required|numeric');
			$this->form_validation->set_rules('txtHargaEkspor', 'Harga Ekspor', 'required|numeric');

			if ($this->form_validation->run() == FALSE) :
				$str['confirm'] = 'errValidation';
				$str['idErrHargaDistributor'] = (!empty(form_error('txtHargaDistributor')))?buildLabel('warning', form_error('txtHargaDistributor', '"', '"')):'';
				$str['idErrHargaRetail'] = (!empty(form_error('txtHargaRetail')))?buildLabel('warning', form_error('txtHargaRetail', '"', '"')):'';
				$str['idErrHargaResell'] = (!empty(form_error('txtHargaResell')))?buildLabel('warning', form_error('txtHargaResell', '"', '"')):'';
				$str['idErrHargaEkspor'] = (!empty(form_error('txtHargaEkspor')))?buildLabel('warning', form_error('txtHargaEkspor', '"', '"')):'';
			else :
				/*
				** Jika validasi berhasil dan tidak ada error
				** Data dari form akan diinputkan kedalam tabel
				** Jika query input return true
				** alert success akan ditampilakan, selain itu error (kesalahan sistem!)
				*/
				$kd_barang = $this->input->post('txtKdBarang');
				$harga_lokal_distributor = $this->input->post('txtHargaDistributor');
				$harga_lokal_retail = $this->input->post('txtHargaRetail');
				$harga_lokal_reseller = $this->input->post('txtHargaResell');
				$harga_ekspor = $this->input->post('txtHargaEkspor');
				$check = !empty($this->input->post('chkStatus'))?'1':'0';
				if ($act == 'input') :
					$conds = array(
						'select' => 'tgl_input, kd_harga_barang',
						'order_by' => 'tgl_input DESC',
						'id_tgl' => array('hari' => array(3,2), 'bulan' => array(5,2), 'tahun' => array(7,2)),
					);
					$kd_harga_barang = $this->m_builder->buat_kode('td_barang_harga', $conds, 3, 'PPR');

					$data_harga = array(
						'kd_harga_barang' => $kd_harga_barang,
						'barang_kd' => $kd_barang,
						'harga_lokal_distributor' => $harga_lokal_distributor,
						'harga_lokal_retail' => $harga_lokal_retail,
						'harga_lokal_reseller' => $harga_lokal_reseller,
						'harga_ekspor' => $harga_ekspor,
						'tgl_input' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					$aksi = $this->db->insert('td_barang_harga', $data_harga);

					if ($check == '1') :
						$data_master = array(
							'harga_barang_kd' => $kd_harga_barang,
							'tgl_edit' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin'),
						);
						$where_master = array('kd_barang' => $kd_barang);
						$aksi = $this->db->update('tm_barang', $data_master, $where_master);
					endif;
					$label_err = 'menambahkan';
				elseif ($act == 'edit') :
					$kd_harga_barang = $this->input->post('txtKdHarga');

					$data_harga = array(
						'barang_kd' => $kd_barang,
						'harga_lokal_distributor' => $harga_lokal_distributor,
						'harga_lokal_retail' => $harga_lokal_retail,
						'harga_lokal_reseller' => $harga_lokal_reseller,
						'harga_ekspor' => $harga_ekspor,
						'tgl_edit' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					$where_harga = array('kd_harga_barang' => $kd_harga_barang);
					$aksi = $this->db->update('td_barang_harga', $data_harga, $where_harga);

					if ($check == '1') :
						$data_master = array(
							'harga_barang_kd' => $kd_harga_barang,
							'tgl_edit' => date('Y-m-d H:i:s'),
							'admin_kd' => $this->session->userdata('kd_admin'),
						);
						$where_master = array('kd_barang' => $kd_barang);
						$aksi = $this->db->update('tm_barang', $data_master, $where_master);
					elseif ($check == '0') :
						$this->db->where(array('harga_barang_kd' => $kd_harga_barang));
						$this->db->from('tm_barang');
						$q_barang = $this->db->get();
						$j_barang = $q_barang->num_rows();

						if ($j_barang > 0) :
							$data_master = array(
								'harga_barang_kd' => '',
								'tgl_edit' => date('Y-m-d H:i:s'),
								'admin_kd' => $this->session->userdata('kd_admin'),
							);
							$where_master = array('kd_barang' => $kd_barang);
							$aksi = $this->db->update('tm_barang', $data_master, $where_master);
						endif;
					endif;
					$label_err = 'mengubah';
				endif;

				if ($aksi) :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->kd_admin.' berhasil '.$label_err.' product price dengan kd_harga_barang \''.$kd_harga_barang.'\' harga_lokal_distributor \''.$harga_lokal_distributor.'\' harga_lokal_retail \''.$harga_lokal_retail.'\' harga_lokal_reseller \''.$harga_lokal_reseller.'\' harga_ekspor \''.$harga_ekspor.'\', untuk kd_barang \''.$kd_barang.'\'');
					$str['confirm'] = 'success';
					$str['kd_barang'] = $kd_barang;
					$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' product price!');
				else :
					$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->kd_admin.' gagal '.$label_err.' product price dengan kd_harga_barang \''.$kd_harga_barang.'\' harga_lokal_distributor \''.$harga_lokal_distributor.'\' harga_lokal_retail \''.$harga_lokal_retail.'\' harga_lokal_reseller \''.$harga_lokal_reseller.'\' harga_ekspor \''.$harga_ekspor.'\', untuk kd_barang \''.$kd_barang.'\'');
					$str['confirm'] = 'errValidation';
					$str['idErrForm'] = buildLabel('danger', 'Gagal '.$label_err.' product price, Kesalahan sistem!');
				endif;
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	function hapus_data() {
		if ($this->input->is_ajax_request()) :
			$kd_harga_barang = $this->input->get('id');
			$label_err = 'menghapus';
			$det = $this->m_builder->getRow('td_barang_harga', $data = array('kd_harga_barang' => $kd_harga_barang), 'row');
			$kd_barang = $det->barang_kd;
			$aksi = $this->db->delete('td_barang_harga', array('kd_harga_barang' => $kd_harga_barang));

			$data = array(
				'harga_barang_kd' => '',
			);
			$where = array('harga_barang_kd' => $kd_harga_barang);
			$aksi = $this->db->update('tm_barang', $data, $where);

			if ($aksi) :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->kd_admin.' berhasil '.$label_err.' product price dengan kd_harga_barang \''.$kd_harga_barang.'\', untuk kd_barang \''.$kd_barang.'\'');
				$str['alert'] = buildAlert('success', 'Berhasil!', 'Berhasil '.$label_err.' data product!');
			else :
				$input_log = $this->m_builder->insert_log($this->session->kd_admin, $this->session->kd_admin.' gagal '.$label_err.' product price dengan kd_harga_barang \''.$kd_harga_barang.'\', untuk kd_barang \''.$kd_barang.'\'');
				$str['alert'] = buildLabel('danger', 'Gagal '.$label_err.' data product, Kesalahan sistem!');
			endif;

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}
}