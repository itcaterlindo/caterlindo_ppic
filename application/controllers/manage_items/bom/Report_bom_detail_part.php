<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Report_bom_detail_part extends MY_Controller {
    private $class_link = 'manage_items/bom/report_bom_detail_part';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_part_jenis', 'td_part']);
	}

	public function index() {
		parent::administrator();
		parent::pnotify_assets();
		$this->table_box();
	}

	public function form_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		if ($sts == 'edit'){
			$actFGuser = $this->td_part_jenis->get_by_param (array('partjenis_kd'=> $id))->row_array();			
			$data['id'] = $actFGuser['partjenis_kd'];
			$data['partjenis_nama'] = $actFGuser['partjenis_nama'];
			$data['partjenis_generatewo'] = $actFGuser['partjenis_generatewo'];
		}

		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->td_part_jenis->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpartjenis_nama', 'Nama Jenis Part', 'required', ['required' => '{field} tidak boleh kosong!']);	

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpartjenis_nama' => (!empty(form_error('txtpartjenis_nama')))?buildLabel('warning', form_error('txtpartjenis_nama', '"', '"')):'',
			);
			
		}else {
			$partjenis_nama = $this->input->post('txtpartjenis_nama');
			$partjenis_generatewo = $this->input->post('txtpartjenis_generatewo');
			
			$data = array(
				'partjenis_kd' => $this->td_part_jenis->create_code(),
				'partjenis_nama' => strtoupper($partjenis_nama),
				'partjenis_generatewo' => $partjenis_generatewo,
				'partjenis_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->td_part_jenis->insert_data($data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
    }
 
	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpartjenis_nama', 'Nama Jenis Part', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpartjenis_nama' => (!empty(form_error('txtpartjenis_nama')))?buildLabel('warning', form_error('txtpartjenis_nama', '"', '"')):'',
			);
			
		}else {
			$partjenis_kd = $this->input->post('txtpartjenis_kd');
			$partjenis_nama = $this->input->post('txtpartjenis_nama');
			$partjenis_generatewo = $this->input->post('txtpartjenis_generatewo');
			
			$data = array(
				'partjenis_nama' => strtoupper($partjenis_nama),
				'partjenis_generatewo' => $partjenis_generatewo,
				'partjenis_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->td_part_jenis->update_data (array('partjenis_kd' => $partjenis_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$partjenis_kd = $this->input->get('id', TRUE);
		
		$actDel = $this->td_part_jenis->delete_data($partjenis_kd);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}

	public function ready_data() {
		$bom_kd = $this->input->get('bom_kd', true);

		
			$sql="SELECT 
			tmb1.bom_kd,
			tmb2.item_code,
			(SELECT GROUP_CONCAT(tm_part_main.partmain_nama, ' || ') FROM tm_bom
			INNER JOIN tm_barang ON tm_barang.kd_barang = tm_bom.kd_barang
			INNER JOIN td_bom_detail ON td_bom_detail.bom_kd = tm_bom.bom_kd
			INNER JOIN tm_part_main ON tm_part_main.partmain_kd = td_bom_detail.partmain_kd
			INNER JOIN td_part_jenis ON td_part_jenis.partjenis_kd = tm_part_main.partjenis_kd
			WHERE tm_bom.bom_kd = tmb1.bom_kd AND td_part_jenis.partjenis_kd = 'JNP0001') AS Project,
			(SELECT GROUP_CONCAT(tm_part_main.partmain_nama, ' || ') FROM tm_bom
			INNER JOIN tm_barang ON tm_barang.kd_barang = tm_bom.kd_barang
			INNER JOIN td_bom_detail ON td_bom_detail.bom_kd = tm_bom.bom_kd
			INNER JOIN tm_part_main ON tm_part_main.partmain_kd = td_bom_detail.partmain_kd
			INNER JOIN td_part_jenis ON td_part_jenis.partjenis_kd = tm_part_main.partjenis_kd
			WHERE tm_bom.bom_kd = tmb1.bom_kd AND td_part_jenis.partjenis_kd = 'JNP0002') AS Top,
			(SELECT GROUP_CONCAT(tm_part_main.partmain_nama, ' || ') FROM tm_bom
			INNER JOIN tm_barang ON tm_barang.kd_barang = tm_bom.kd_barang
			INNER JOIN td_bom_detail ON td_bom_detail.bom_kd = tm_bom.bom_kd
			INNER JOIN tm_part_main ON tm_part_main.partmain_kd = td_bom_detail.partmain_kd
			INNER JOIN td_part_jenis ON td_part_jenis.partjenis_kd = tm_part_main.partjenis_kd
			WHERE tm_bom.bom_kd = tmb1.bom_kd AND td_part_jenis.partjenis_kd = 'JNP0003') AS SolidCB,
			(SELECT GROUP_CONCAT(tm_part_main.partmain_nama, ' || ') FROM tm_bom
			INNER JOIN tm_barang ON tm_barang.kd_barang = tm_bom.kd_barang
			INNER JOIN td_bom_detail ON td_bom_detail.bom_kd = tm_bom.bom_kd
			INNER JOIN tm_part_main ON tm_part_main.partmain_kd = td_bom_detail.partmain_kd
			INNER JOIN td_part_jenis ON td_part_jenis.partjenis_kd = tm_part_main.partjenis_kd
			WHERE tm_bom.bom_kd = tmb1.bom_kd AND td_part_jenis.partjenis_kd = 'JNP0004') AS Sink_Bowl,
			(SELECT GROUP_CONCAT(tm_part_main.partmain_nama, ' || ') FROM tm_bom
			INNER JOIN tm_barang ON tm_barang.kd_barang = tm_bom.kd_barang
			INNER JOIN td_bom_detail ON td_bom_detail.bom_kd = tm_bom.bom_kd
			INNER JOIN tm_part_main ON tm_part_main.partmain_kd = td_bom_detail.partmain_kd
			INNER JOIN td_part_jenis ON td_part_jenis.partjenis_kd = tm_part_main.partjenis_kd
			WHERE tm_bom.bom_kd = tmb1.bom_kd AND td_part_jenis.partjenis_kd = 'JNP0005') AS Pipa,
			(SELECT GROUP_CONCAT(tm_part_main.partmain_nama, ' || ') FROM tm_bom
			INNER JOIN tm_barang ON tm_barang.kd_barang = tm_bom.kd_barang
			INNER JOIN td_bom_detail ON td_bom_detail.bom_kd = tm_bom.bom_kd
			INNER JOIN tm_part_main ON tm_part_main.partmain_kd = td_bom_detail.partmain_kd
			INNER JOIN td_part_jenis ON td_part_jenis.partjenis_kd = tm_part_main.partjenis_kd
			WHERE tm_bom.bom_kd = tmb1.bom_kd AND td_part_jenis.partjenis_kd = 'JNP0006') AS Lainlain,
			(SELECT GROUP_CONCAT(tm_part_main.partmain_nama, ' || ') FROM tm_bom
			INNER JOIN tm_barang ON tm_barang.kd_barang = tm_bom.kd_barang
			INNER JOIN td_bom_detail ON td_bom_detail.bom_kd = tm_bom.bom_kd
			INNER JOIN tm_part_main ON tm_part_main.partmain_kd = td_bom_detail.partmain_kd
			INNER JOIN td_part_jenis ON td_part_jenis.partjenis_kd = tm_part_main.partjenis_kd
			WHERE tm_bom.bom_kd = tmb1.bom_kd AND td_part_jenis.partjenis_kd = 'JNP0007') AS Packing
		
		FROM tm_bom as tmb1
		LEFT JOIN tm_barang as tmb2 ON tmb1.kd_barang = tmb2.kd_barang";


		$query = $this->db->query($sql);
		$res = $query->result_array();

		//get coasting
		$bomstds = $this->db->select('tm_bom.bom_kd, tm_barang.item_code, tb_part_lastversion.part_kd, td_part_jenis.partjenis_nama, tm_part_main.partmain_note')
			->from('tm_bom')
			->join('tm_barang', 'tm_bom.kd_barang=tm_barang.kd_barang', 'left')
			->join('td_bom_detail', 'tm_bom.bom_kd=td_bom_detail.bom_kd', 'left')
			->join('tb_part_lastversion', 'tb_part_lastversion.partmain_kd=td_bom_detail.partmain_kd', 'left')
			->join('tm_part_main', 'tm_part_main.partmain_kd=td_bom_detail.partmain_kd', 'left')
			->join('td_part_jenis', 'td_part_jenis.partjenis_kd=tm_part_main.partjenis_kd', 'left')
			->where('tm_bom.bom_jenis =', 'std')
			->get()->result_array();
		$results = [];
		$part_costing = 0;
		foreach ($bomstds as $bomstd) {
			$part_costing = $this->td_part->get_totalharga_costing_cust($bomstd['part_kd']);
			$results[] = array_merge($bomstd, ['part_costing' => $part_costing]);
		}
		

		$groupBy = [];
		$tester  = [];

		$no=1;
		foreach($results as $v=>$val){
			$no++;
			if(in_array($val['bom_kd'], array_column($groupBy, 'bom_kd'))) {
				// $tester["partjenis_nama"] = $val['partjenis_nama'];
				// $tester["angka"] = 'Ammen';
				$ind = array_search($val['bom_kd'], array_column($groupBy, 'bom_kd'));

					// if(array_key_exists($val['partjenis_nama'], $groupBy[$ind])){

						if(!empty($groupBy[$ind][$val['partjenis_nama']])){
							$groupBy[$ind][$val['partjenis_nama']] = $groupBy[$ind][$val['partjenis_nama']] .  '; ' .  round($val['part_costing']);
						}else {
							$groupBy[$ind][$val['partjenis_nama']] = $groupBy[$ind][$val['partjenis_nama']] .  '' .  round($val['part_costing']);
						}
						$groupBy[$ind]['total_all'] +=  round($val['part_costing']);
						

					// }
			}else{
				//$groupBy = $val;
				$data_push = array("bom_kd" => $val['bom_kd'], "PACKING"=>'',"LAIN-LAIN"=>'',"PIPA"=>'',"SOLID/CB"=>'',"SINK BOWL"=>'',"TOP"=>'', 'total_all'=>$val['part_costing'], "item_code" =>  $val['item_code'], $val['partjenis_nama'] =>  round($val['part_costing']));
				array_push($groupBy, $data_push);
			}
		}


		// $result = array_reduce($results, function($carry, $item) { 
		// 	if(!isset($carry[$item['bom_kd']])) {
		// 		$carry[$item['bom_kd']] = $item;
		// 	} else {
		// 		$carry[$item['bom_kd']][$item['partjenis_nama']] = $item['part_costing'] ? $item['part_costing'] : 0;
		// 	}
		// 	return $carry;
		// });
		
		$data['coasting'] = array_values($groupBy);
		$data['coasting_part'] = array_values($results);
		$data['item'] = $res;
		$data['no'] = $tester;
		
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	function findObjectById($id){
    	$array = array( /* your array of objects */ );

		foreach ( $array as $element ) {
			if ( $id == $element->id ) {
				return $element;
			}
		}

		return false;
	}
	
}
