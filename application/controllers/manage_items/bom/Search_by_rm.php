<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Search_by_rm extends MY_Controller {
	private $class_link = 'manage_items/bom/search_by_rm';

    public function __construct()
	{
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_part', 'tm_barang', 'td_bom_detail', 'tm_salesorder', 'tm_project', 'td_part_labourcost', 'td_part_overhead', 'tb_part_lastversion', 'tm_bom', 'tm_part_main', 'tb_files', 'tm_rawmaterial']);
	}

	public function index()
	{
		parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
		$this->get_data();
	}

    public function get_data()
	{
        $data['class_link'] = $this->class_link;

		$this->load->view('page/' . $this->class_link . '/table_box', $data);
	}
	

	
	public function table_main()
	{

		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/table_main', $data);
	}

    
	public function get_data_rm()
	{
		$data = $this->tm_rawmaterial->get_all()->result_array();

		header('Content-Type: application/json');
		echo json_encode($data);
	}


	public function get_data_bwp()
	{
			$bom_kd = $this->input->get('bom_kd', true);

		
			$sql="SELECT tmb.* FROM tm_barang AS tmb 
                INNER JOIN tm_bom AS tmbo ON tmbo.kd_barang = tmb.kd_barang
                INNER JOIN td_bom_detail AS tdbo ON tdbo.bom_kd = tmbo.bom_kd
                LEFT JOIN tm_part_main ON tdbo.partmain_kd = tm_part_main.partmain_kd
								LEFT JOIN tb_part_lastversion ON tm_part_main.partmain_kd = tb_part_lastversion.partmain_kd
								LEFT JOIN td_part ON td_part.part_kd = tb_part_lastversion.part_kd
                INNER JOIN td_part_detail AS tdpd ON td_part.part_kd = tdpd.part_kd
                INNER JOIN tm_rawmaterial AS tmr ON tmr.rm_kd = tdpd.rm_kd
                WHERE tmr.rm_kd ='$bom_kd'";    


					$query = $this->db->query($sql);
					$res = $query->result_array();


		
			$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terupdate', 'data' => $res);
		
		header('Content-Type: application/json');
		echo json_encode($resp);
	}

	public function table_data()
	{
		$bom_kd = $this->input->get('id', true);

		
			$sql="SELECT tmr.rm_kode,  CONCAT(tmr.rm_nama, '-', tmr.rm_spesifikasi) AS rm_nama, tdpd.partdetail_qty, tmp.partmain_nama, tmb.item_code, tmb.deskripsi_barang, tmb.dimensi_barang FROM tm_barang AS tmb 
                INNER JOIN tm_bom AS tmbo ON tmbo.kd_barang = tmb.kd_barang
                INNER JOIN td_bom_detail AS tdbo ON tdbo.bom_kd = tmbo.bom_kd
				LEFT JOIN tm_part_main AS tmp ON tdbo.partmain_kd = tmp.partmain_kd
								LEFT JOIN tb_part_lastversion AS tplv ON tmp.partmain_kd = tplv.partmain_kd
								LEFT JOIN td_part AS tdp ON tdp.part_kd = tplv.part_kd
                INNER JOIN td_part_detail AS tdpd ON tdp.part_kd = tdpd.part_kd
                INNER JOIN tm_rawmaterial AS tmr ON tmr.rm_kd = tdpd.rm_kd
                WHERE tmr.rm_kd ='$bom_kd'";    


					$query = $this->db->query($sql);
					$res = $query->result_array();


		
			$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terupdate', 'data' => $res);
		
		header('Content-Type: application/json');
		echo json_encode($resp);
	}


	
}
