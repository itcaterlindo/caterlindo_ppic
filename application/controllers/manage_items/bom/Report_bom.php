<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Report_bom extends MY_Controller {
	private $class_link = 'manage_items/bom/report_bom';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_bom', 'td_part', 'td_part_state_log']);
	}
    	
	public function bom_std_pdf () {
		$this->load->library('Pdf');
		$bom_kd = $this->input->get('bom_kd', true);
		$bomdetails = $this->bom_details($bom_kd);
		$data['bomMain'] = $this->tm_bom->get_by_param_detail(['tm_bom.bom_kd' => $bom_kd])->row_array();
		$data['bom'] = $bomdetails;
		foreach ($bomdetails as $rBomdetail) {
			if (!empty($rBomdetail['part_kd'])){
				$dataPart = $this->td_part->partcosting_data ($rBomdetail['part_kd']);
				$data['konten'][$rBomdetail['part_kd']] = $this->load->view('page/manage_items/manage_part/Part_data/view_detailtable_main', $dataPart, true);
				$data['states'][$rBomdetail['part_kd']] = $this->td_part_state_log->get_last_log($rBomdetail['part_kd'])->result_array();
			}
		}
		
		$this->load->view('page/'.$this->class_link.'/report_bom_pdf', $data);
	}

	public function bom_std_costing_pdf () {
		$this->load->library('Pdf');
		$bom_kd = $this->input->get('bom_kd', true);
		$bomdetails = $this->bom_details($bom_kd);
		$data['bomMain'] = $this->tm_bom->get_by_param_detail(['tm_bom.bom_kd' => $bom_kd])->row_array();
		$data['bom'] = $bomdetails;
		foreach ($bomdetails as $rBomdetail) {
			if (!empty($rBomdetail['part_kd'])){
				$dataPart = $this->td_part->partcosting_data ($rBomdetail['part_kd']);
				$data['konten'][$rBomdetail['part_kd']] = $this->load->view('page/manage_items/manage_part/part_data/view_detailtable_costing_main', $dataPart, true);
				$data['states'][$rBomdetail['part_kd']] = $this->td_part_state_log->get_last_log($rBomdetail['part_kd'])->result_array();
			}
		}
		$this->load->view('page/'.$this->class_link.'/report_bom_costing_pdf', $data);
	}

	private function bom_details($bom_kd)
	{
		$bomdetails = $this->db->select()
				->from('td_bom_detail')
				->join('tm_bom', 'td_bom_detail.bom_kd=tm_bom.bom_kd', 'left')
				->join('tm_barang', 'tm_bom.kd_barang=tm_barang.kd_barang', 'left')
				->join('tm_group_barang', 'tm_barang.group_barang_kd=tm_group_barang.kd_group_barang', 'left')
				->join('tm_project', 'tm_bom.project_kd=tm_project.project_kd', 'left')
				->join('tm_part_main', 'td_bom_detail.partmain_kd=tm_part_main.partmain_kd', 'left')
				->join('tb_part_lastversion', 'tm_part_main.partmain_kd=tb_part_lastversion.partmain_kd', 'left')
				->join('td_part', 'tb_part_lastversion.part_kd=td_part.part_kd', 'left')
				->join('td_part_jenis', 'tm_part_main.partjenis_kd=td_part_jenis.partjenis_kd', 'left')
				->where('td_bom_detail.bom_kd', $bom_kd)
				->get()->result_array();
		return $bomdetails;
	}
	
}
