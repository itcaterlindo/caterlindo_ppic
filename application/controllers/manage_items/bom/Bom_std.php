<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Bom_std extends MY_Controller
{
	private $class_link = 'manage_items/bom/bom_std';

	public function __construct()
	{
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_part', 'tm_barang', 'td_bom_detail', 'tm_salesorder', 'tm_project', 'td_part_labourcost', 'td_part_overhead', 'tb_part_lastversion', 'tm_bom', 'tm_part_main', 'tb_files']);
	}

	public function index()
	{
		parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
		$this->table_box();
	}

	public function form_box()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/form_box', $data);
	}

	public function form_detail_box()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$bom_kd = $this->input->get('id');

		$data['url'] = base_url() . '/manage_items/bom/report_bom/	?bom_kd=' . $bom_kd;

		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/form_detail_box', $data);
	}

	public function form_main()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');
		$bomdetail_kd = $this->input->get('bomdetail_kd');

		if (!empty($bomdetail_kd)) {
			$data['bomdetail'] = $this->db->join('tm_part_main', 'tm_part_main.partmain_kd=td_bom_detail.partmain_kd')
				->where('td_bom_detail.bomdetail_kd', $bomdetail_kd)
				->get('td_bom_detail')->row_array();
			$id = $data['bomdetail']['bom_kd'];
		}

		$data['dataMaster'] = $this->tm_bom->get_by_param_detail(['tm_bom.bom_kd' => $id])->row_array();
		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/form_main', $data);
	}

	public function get_partmain_std()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		define('EXCLUDE_PART_STD', 'JNP0001');
		$paramSearch = $this->input->get('paramSearch');

		$actopsiPart = $this->db->join('td_part_jenis', 'td_part_jenis.partjenis_kd=tm_part_main.partjenis_kd', 'left')
			->where('tm_part_main.partmain_status', 1)
			->where('tm_part_main.partjenis_kd !=', EXCLUDE_PART_STD);
		if (!empty($paramSearch)) {
			$actopsiPart = $actopsiPart->like('tm_part_main.partmain_nama', $paramSearch, 'match');
		}
		$actopsiPart = $actopsiPart->get('tm_part_main')
			->result_array();

		$opsiPart = [];
		foreach ($actopsiPart as $eachPart) :
			$eachPart['id'] = $eachPart['partmain_kd'];
			$eachPart['text'] = $eachPart['partmain_nama'];
			$opsiPart[] = $eachPart;
		endforeach;

		echo json_encode($opsiPart);
	}

	public function form_master_main()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		if ($sts == 'edit' || $sts == 'duplicate') {
			$act = $this->tm_bom->get_by_param_detail(array('tm_bom.bom_kd' => $id))->row_array();
			$opsiBarang[$act['kd_barang']] = $act['item_code'];
			$data['rowData'] = $act;
		}

		/** Opsi Barang */
		$queryBarang = $this->db->select('tm_barang.*')
			->from('tm_barang')
			->join('tm_bom', 'tm_barang.kd_barang=tm_bom.kd_barang', 'left')
			->join('tm_product_grouping', 'tm_barang.kd_barang=tm_product_grouping.barang_kd', 'left')
			->where('tm_bom.kd_barang IS NULL')
			->where('tm_product_grouping.barang_kd IS NULL')
			->get_compiled_select();
		/** Select RM yang dijadikan WIP */
		$queryRMWIP = $this->db->select('tm_rawmaterial.rm_kd, "NULL", "NULL", "NULL", tm_rawmaterial.rm_kode, "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL"')
			->from('tm_rawmaterial')
			->where('tm_rawmaterial.itemgroup_kd', '14')
			->get_compiled_select();
		$actopsiBarang = $this->db->query($queryBarang . ' UNION ' . $queryRMWIP)->result_array();

		$opsiBarang[null] = '-- Pilih Opsi --';
		foreach ($actopsiBarang as $eachBarang) :
			$opsiBarang[$eachBarang['kd_barang']] = $eachBarang['item_code'];
		endforeach;

		/** Opsi Jenis BOM */
		$opsiJenisBOM = array(
			'std' => 'Standart',
			'custom' => 'Custom'
		);

		/** Opsi SO */
		$actOpsiSO = $this->tm_salesorder->get_by_status(['pending', 'process_lpo', 'process_wo'])->result_array();
		$opsiSO[null] = '-- PIlih Opsi --';
		foreach ($actOpsiSO as $eachSO) :
			$noSO = $eachSO['no_salesorder'];
			if ($eachSO['tipe_customer'] == 'Ekspor') :
				$noSO = $eachSO['no_po'];
			endif;
			$opsiSO[$eachSO['kd_msalesorder']] = $noSO . ' | ' . $eachSO['nm_customer'];
		endforeach;

		$data['opsiBarang'] = $opsiBarang;
		$data['opsiJenisBOM'] = $opsiJenisBOM;
		$data['opsiSO'] = $opsiSO;
		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/form_master_main', $data);
	}

	public function table_box()
	{
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/table_box', $data);
	}

	public function table_main()
	{
		// if (!$this->input->is_ajax_request()) {
		// 	exit('No direct script access allowed');
		// }

		$jnsBom = $this->input->get('jnsBom', true);

		$data['class_link'] = $this->class_link;
		$data['jnsBom'] = $jnsBom;
		$this->load->view('page/' . $this->class_link . '/table_main', $data);
	}

	public function table_data()
	{
		// if (!$this->input->is_ajax_request()) {
		// 	exit('No direct script access allowed');
		// }
		$this->load->library(['ssp']);


		$data = $this->tm_bom->ssp_table_std();
		echo json_encode(
			SSP::simple($_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'])
		);
	}

	public function table_detail_main()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/table_detail_main', $data);
	}

	public function table_detail_data()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data = $this->td_bom_detail->ssp_table($id);
		echo json_encode(
			SSP::simple($_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'])
		);
	}

	public function bomstdcosting_box()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$bom_kd = $this->input->get('id');

		$bomstds = $this->db->select('tm_bom.*, tb_part_lastversion.part_kd, tm_part_main.partmain_nama, tm_part_main.partmain_note')
			->from('tm_bom')
			->join('td_bom_detail', 'tm_bom.bom_kd=td_bom_detail.bom_kd', 'left')
			->join('tb_part_lastversion', 'tb_part_lastversion.partmain_kd=td_bom_detail.partmain_kd', 'left')
			->join('tm_part_main', 'tm_part_main.partmain_kd=td_bom_detail.partmain_kd', 'left')
			->where('tm_bom.bom_kd', $bom_kd)
			->get()->result_array();
		$results = [];
		$part_costing = 0;
		foreach ($bomstds as $bomstd) {
			$part_costing = $this->td_part->get_totalharga_costing($bomstd['part_kd']);
			$results[] = array_merge($bomstd, ['part_costing' => $part_costing]);
		}

		$data['bomstd'] = $this->tm_bom->get_by_param_detail(['tm_bom.bom_kd' => $bom_kd])->row_array();
		$data['class_link'] = $this->class_link;
		$data['results'] = $results;
		$this->load->view('page/' . $this->class_link . '/bomstdcosting_box', $data);
	}

	public function push_sap()
	{
			$bom_kd = $this->input->get('id', true);

			$dataMain = array(
				'status_sap' => '1',
				'bom_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);
			$act = $this->tm_bom->update_data(array('bom_kd' => $bom_kd), $dataMain);

		if ($act) {
			$sql="SELECT 

					tm_bom.bom_kd,
					td_bom_detail.bomdetail_generatewo,
					tm_part_main.partmain_kd,
					tm_part_main.partmain_nama,
					tm_barang.item_code,
					tm_barang.deskripsi_barang,
					td_part_detail.rm_kd,
					td_part_detail.partdetail_nama,
					td_part_detail.partdetail_kd,
					td_part_detail.partdetail_qty,
					td_part_detail.bagian_kd,
					tmrw.rm_kode AS rm_kode_ori,
					'' AS rm_kode,
					'' AS rm_platepanjang,
					COUNT(*) as jml_rm,
						td_part.part_kd,
					'' AS partdetailplate_kd,
					'' AS partdetailplate_jenis,
					'' AS P, 
					'' AS L, 
					'' AS T, 
					'' AS MJ,
					'' AS qty,
					'' AS CUT_SIZE


					FROM tm_bom
						LEFT JOIN tm_rawmaterial as tmrw ON tmrw.rm_kd = tm_bom.kd_barang
						LEFT JOIN td_bom_detail ON tm_bom.bom_kd = td_bom_detail.bom_kd
						LEFT JOIN tm_barang ON tm_bom.kd_barang = tm_barang.kd_barang
						LEFT JOIN tm_part_main ON td_bom_detail.partmain_kd = tm_part_main.partmain_kd
						LEFT JOIN tb_part_lastversion ON tm_part_main.partmain_kd = tb_part_lastversion.partmain_kd
						LEFT JOIN td_part ON td_part.part_kd = tb_part_lastversion.part_kd
						LEFT JOIN td_part_detail ON td_part.part_kd = td_part_detail.part_kd
						
						WHERE tm_bom.bom_kd = '$bom_kd' AND  td_bom_detail.bomdetail_generatewo = 'T'
					GROUP BY partmain_kd
					UNION
					SELECT 

						tm_bom.bom_kd,
						td_bom_detail.bomdetail_generatewo,
						tm_part_main.partmain_kd,
						tm_part_main.partmain_nama,
						tm_barang.item_code,
						tm_barang.deskripsi_barang,
						td_part_detail.rm_kd,
						td_part_detail.partdetail_nama,
						td_part_detail.partdetail_kd,
						td_part_detail.partdetail_qty,
						td_part_detail.bagian_kd,
						tmrw.rm_kode AS rm_kode_ori,
						tm_rawmaterial.rm_kode,
						tm_rawmaterial.rm_platepanjang,
						td_part_detail.partdetail_qty as jml_rm,
							td_part.part_kd,
							tdpdp.partdetailplate_kd,
							tdpdp.partdetailplate_jenis,
							tdpdp.partdetailplate_panjang AS P, 
							tdpdp.partdetailplate_lebar AS L, 
							tdpdp.partdetailplate_tebal AS T, 
							tdpdp.partdetailplate_massajenis AS MJ,
							tdpdp.partdetailplate_qty AS qty,
							ROUND(SUM((((tdpdp.partdetailplate_panjang * tdpdp.partdetailplate_lebar * tdpdp.partdetailplate_tebal) / 1000000 * tdpdp.partdetailplate_massajenis) * tdpdp.partdetailplate_qty)), 4) AS CUT_SIZE


					FROM tm_bom
						LEFT JOIN tm_rawmaterial as tmrw ON tmrw.rm_kd = tm_bom.kd_barang
						LEFT JOIN td_bom_detail ON tm_bom.bom_kd = td_bom_detail.bom_kd
						LEFT JOIN tm_barang ON tm_bom.kd_barang = tm_barang.kd_barang
						LEFT JOIN tm_part_main ON td_bom_detail.partmain_kd = tm_part_main.partmain_kd
						LEFT JOIN tb_part_lastversion ON tm_part_main.partmain_kd = tb_part_lastversion.partmain_kd
						LEFT JOIN td_part ON td_part.part_kd = tb_part_lastversion.part_kd
						LEFT JOIN td_part_detail ON td_part.part_kd = td_part_detail.part_kd
						LEFT JOIN tm_rawmaterial ON tm_rawmaterial.rm_kd = td_part_detail.rm_kd
						LEFT JOIN td_part_detailplate AS tdpdp ON td_part_detail.partdetail_kd = tdpdp.partdetail_kd
					WHERE tm_bom.bom_kd = '$bom_kd' AND  td_bom_detail.bomdetail_generatewo = 'F'  AND tdpdp.partdetailplate_jenis = 'main'
					GROUP BY tdpdp.partdetailplate_kd
					UNION
					SELECT 

						tm_bom.bom_kd,
						td_bom_detail.bomdetail_generatewo,
						tm_part_main.partmain_kd,
						tm_part_main.partmain_nama,
						tm_barang.item_code,
						tm_barang.deskripsi_barang,
						td_part_detail.rm_kd,
						td_part_detail.partdetail_nama,
						td_part_detail.partdetail_kd,
						td_part_detail.partdetail_qty,
						td_part_detail.bagian_kd,
						tmrw.rm_kode AS rm_kode_ori,
						tm_rawmaterial.rm_kode,
						tm_rawmaterial.rm_platepanjang,
						td_part_detail.partdetail_qty as jml_rm,
							td_part.part_kd,
							tdpdp.partdetailplate_kd,
							tdpdp.partdetailplate_jenis,
							tdpdp.partdetailplate_panjang AS P, 
							tdpdp.partdetailplate_lebar AS L, 
							tdpdp.partdetailplate_tebal AS T, 
							tdpdp.partdetailplate_massajenis AS MJ,
							tdpdp.partdetailplate_qty AS qty,
							ROUND(SUM(((tm_rawmaterial.rm_platepanjang * tdpdp.partdetailplate_lebar * tdpdp.partdetailplate_tebal) / 1000000 * tdpdp.partdetailplate_massajenis)), 4) AS CUT_SIZE


					FROM tm_bom
						LEFT JOIN tm_rawmaterial as tmrw ON tmrw.rm_kd = tm_bom.kd_barang
						LEFT JOIN td_bom_detail ON tm_bom.bom_kd = td_bom_detail.bom_kd
						LEFT JOIN tm_barang ON tm_bom.kd_barang = tm_barang.kd_barang
						LEFT JOIN tm_part_main ON td_bom_detail.partmain_kd = tm_part_main.partmain_kd
						LEFT JOIN tb_part_lastversion ON tm_part_main.partmain_kd = tb_part_lastversion.partmain_kd
						LEFT JOIN td_part ON td_part.part_kd = tb_part_lastversion.part_kd
						LEFT JOIN td_part_detail ON td_part.part_kd = td_part_detail.part_kd
						LEFT JOIN tm_rawmaterial ON tm_rawmaterial.rm_kd = td_part_detail.rm_kd
						LEFT JOIN td_part_detailplate AS tdpdp ON td_part_detail.partdetail_kd = tdpdp.partdetail_kd
					WHERE tm_bom.bom_kd = '$bom_kd' AND  td_bom_detail.bomdetail_generatewo = 'F' AND tdpdp.partdetailplate_jenis = 'cut_size'
					GROUP BY tdpdp.partdetailplate_kd

					UNION

					SELECT 

					tm_bom.bom_kd,
						td_bom_detail.bomdetail_generatewo,
						tm_part_main.partmain_kd,
						tm_part_main.partmain_nama,
						tm_barang.item_code,
						tm_barang.deskripsi_barang,
						td_part_detail.rm_kd,
						td_part_detail.partdetail_nama,
						td_part_detail.partdetail_kd,
						td_part_detail.partdetail_qty,
						td_part_detail.bagian_kd,
						tmrw.rm_kode AS rm_kode_ori,
						tm_rawmaterial.rm_kode,
						tm_rawmaterial.rm_platepanjang,
						td_part_detail.partdetail_qty as jml_rm,
							td_part.part_kd,
						tdpdp.partdetailplate_kd AS partdetailplate_kd,
							'' AS partdetailplate_jenis,
							'' AS P, 
							'' AS L, 
							'' AS T, 
							'' AS MJ,
							'' AS qty,
							'' AS CUT_SIZE
					FROM tm_bom
						LEFT JOIN tm_rawmaterial as tmrw ON tmrw.rm_kd = tm_bom.kd_barang
						LEFT JOIN td_bom_detail ON tm_bom.bom_kd = td_bom_detail.bom_kd
						LEFT JOIN tm_barang ON tm_bom.kd_barang = tm_barang.kd_barang
						LEFT JOIN tm_part_main ON td_bom_detail.partmain_kd = tm_part_main.partmain_kd
						LEFT JOIN tb_part_lastversion ON tm_part_main.partmain_kd = tb_part_lastversion.partmain_kd
						LEFT JOIN td_part ON td_part.part_kd = tb_part_lastversion.part_kd
						LEFT JOIN td_part_detail ON td_part.part_kd = td_part_detail.part_kd
						LEFT JOIN td_part_detailplate AS tdpdp ON td_part_detail.partdetail_kd = tdpdp.partdetail_kd
						LEFT JOIN tm_rawmaterial ON tm_rawmaterial.rm_kd = td_part_detail.rm_kd
					WHERE tm_bom.bom_kd = '$bom_kd' AND  td_bom_detail.bomdetail_generatewo = 'F'"
										;    
					$query = $this->db->query($sql);
			$res = $query->result_array();

			$key = array_filter($res, function ($var) {
						return $var['bomdetail_generatewo'] == 'F';
					});

			if(!empty($key)){
				$ids = array_column($key, 'partmain_kd');
				$ids = array_unique($ids);

				$qLabourcost = $this->td_part_labourcost->get_lc_by_partmain('tm_part_main.partmain_kd', $ids);

				$qOverhead = $this->td_part_overhead->get_ovh_by_partmain_in($ids);
				$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terupdate', 'items' => $key, 'data' => $res, 'data_ov' => $qOverhead->result_array(), 'data_lb' => $qLabourcost->result_array());
			}else{
				$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terupdate', 'items' => $key, 'data' => $res, 'data_ov' => [], 'data_lb' => []);
			}

			

			//$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terupdate', 'items' => $ids, 'data' => $res);
		} else {
			$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		}
		header('Content-Type: application/json');
		echo json_encode($resp);
	}
	

	public function ok_kawand()
	{
			$bom_kd = $this->input->get('id', true);

			$dataMain = array(
				'status_sap' => '1',
				'bom_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);
			$act = true;

		if ($act) {
			$sql="SELECT 

				tm_bom.bom_kd,
				td_bom_detail.bomdetail_generatewo,
				tm_part_main.partmain_kd,
				tm_part_main.partmain_nama,
				tm_barang.item_code,
				tm_barang.deskripsi_barang,
				td_part_detail.rm_kd,
				td_part_detail.partdetail_nama,
				td_part_detail.partdetail_kd,
				td_part_detail.bagian_kd,
				'' AS rm_kode


				FROM tm_bom
					LEFT JOIN td_bom_detail ON tm_bom.bom_kd = td_bom_detail.bom_kd
					LEFT JOIN tm_barang ON tm_bom.kd_barang = tm_barang.kd_barang
					LEFT JOIN tm_part_main ON td_bom_detail.partmain_kd = tm_part_main.partmain_kd
					LEFT JOIN td_part ON td_part.partmain_kd = tm_part_main.partmain_kd
					LEFT JOIN td_part_detail ON td_part.part_kd = td_part_detail.part_kd
					WHERE tm_bom.bom_kd = '$bom_kd' AND  td_bom_detail.bomdetail_generatewo = 'T'
				GROUP BY partmain_kd
				UNION
				SELECT 

					tm_bom.bom_kd,
					td_bom_detail.bomdetail_generatewo,
					tm_part_main.partmain_kd,
					tm_part_main.partmain_nama,
					tm_barang.item_code,
					tm_barang.deskripsi_barang,
					td_part_detail.rm_kd,
					td_part_detail.partdetail_nama,
					td_part_detail.partdetail_kd,
					td_part_detail.bagian_kd,
					tm_rawmaterial.rm_kode


				FROM tm_bom
					LEFT JOIN td_bom_detail ON tm_bom.bom_kd = td_bom_detail.bom_kd
					LEFT JOIN tm_barang ON tm_bom.kd_barang = tm_barang.kd_barang
					LEFT JOIN tm_part_main ON td_bom_detail.partmain_kd = tm_part_main.partmain_kd
					LEFT JOIN td_part ON td_part.partmain_kd = tm_part_main.partmain_kd
					LEFT JOIN td_part_detail ON td_part.part_kd = td_part_detail.part_kd
					LEFT JOIN tm_rawmaterial ON tm_rawmaterial.rm_kd = td_part_detail.rm_kd
				WHERE tm_bom.bom_kd = '$bom_kd' AND  td_bom_detail.bomdetail_generatewo = 'F'
				GROUP BY partdetail_kd"
					;    
			$query = $this->db->query($sql);
			$res = $query->result_array();

			$key = array_filter($res, function ($var) {
						return $var['bomdetail_generatewo'] == 'F';
					});

			$ids = array_column($key, 'partmain_kd');
			$ids = array_unique($ids);

			$qLabourcost = $this->td_part_labourcost->get_lc_by_partmain('tm_part_main.partmain_kd', $ids);

			$qOverhead = $this->td_part_overhead->get_ovh_by_partmain_in($ids);

			//$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terupdate', 'items' => $ids, 'data' => $res);
			 $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terupdate', 'items' => $key, 'data' => $res, 'data_ov' => $qOverhead->result_array(), 'data_lb' => $qLabourcost->result_array());
		} else {
			$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		}
		header('Content-Type: application/json');
		echo json_encode($resp);
	}

	public function rollback_sap()
	{
			$bom_kd = $this->input->get('id', true);

			$dataMain = array(
				'status_sap' => '0',
				'bom_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);
			$act = $this->tm_bom->update_data(array('bom_kd' => $bom_kd), $dataMain);

		if ($act) {
			$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Gagal Push, Rollback Success!');
		} else {
			$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Rollback, Hub Admin!');
		}
		header('Content-Type: application/json');
		echo json_encode($resp);
	}

	public function viewfiles_box()
	{
		parent::administrator();
		parent::pnotify_assets();


		$id = $this->input->get('id');
		$id = url_decrypt($id);
		$sts = $this->input->get('sts');

		$data['id'] = $id;
		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$data['master'] = $this->db->join('tm_barang as b', 'b.kd_barang=a.kd_barang', 'left')
			->where('a.bom_kd', $id)
			->get('tm_bom as a')
			->row_array();
		$this->load->view('page/' . $this->class_link . '/viewfiles_box', $data);
	}

	public function viewfiles_main()
	{
		$id = $this->input->get('id');
		$sts = $this->input->get('sts');

		$data['path'] = 'assets/admin_assets/dist/files/';
		$data['id'] = $id;
		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$data['files'] = $this->db->where('bom_kd', $id)->where('file_tgldelete IS NULL')->get('tb_files')->result_array();

		$this->load->view('page/' . $this->class_link . '/viewfiles_main', $data);
	}

	public function viewfiles_form()
	{
		$bom_kd = $this->input->get('bom_kd');
		$id = $this->input->get('id');
		$sts = $this->input->get('sts');

		$data['bom_kd'] = $bom_kd;
		$data['id'] = $id;
		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;

		if ($sts == 'edit') {
			$data['row'] = $this->tb_files->get_by_param(['file_kd' => $id])->row_array();
		}

		$this->load->view('page/' . $this->class_link . '/viewfiles_form', $data);
	}

	public function get_item_so()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$kd_msalesorder = $this->input->get('kd_msalesorder', true);

		$projects = $this->tm_project->get_by_param(['kd_msalesorder' => $kd_msalesorder]);

		$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tampilkan', 'data' => $projects->result());

		header('Content-Type: application/json');
		echo json_encode($resp);
	}

	public function action_insert_files()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtfile_nama', 'File', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrfile_nama' => (!empty(form_error('txtfile_nama'))) ? buildLabel('warning', form_error('txtfile_nama', '"', '"')) : '',
			);
		} else {
			$bom_kd = $this->input->post('txtbom_kd', true);
			$file_kd = $this->input->post('txtfile_kd', true);
			$file_nama = $this->input->post('txtfile_nama', true);
			$file_note = $this->input->post('txtfile_note', true);
			$fileExtension = $this->input->post('txtfileExtension', true);

			if (empty($file_kd)) {
				/** ADD */
				//process upload
				$fileNameUUID = uniqid();
				$config['file_name'] = $fileNameUUID;
				$config['upload_path'] = FCPATH . 'assets/admin_assets/dist/files/';
				$config['allowed_types'] = 'docx|xlsx|doc|xls|pdf|jpg|png|jpeg|gif|txt';
				$config['max_filesize'] = 20480;
				$config['detect_mime'] = TRUE;

				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('txtfile')) {
					$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
				} else {
					$eFilenameOri = explode('.', $_FILES['txtfile']['name']);
					$data = array(
						'file_kd' => $this->tb_files->create_code(),
						'bom_kd' => $bom_kd,
						'file_uuid' => $fileNameUUID,
						'file_namafile' => "$file_nama.{$eFilenameOri[1]}",
						'file_note' => $file_note,
						'file_tglinput' => date('Y-m-d H:i:s'),
						'file_tgledit' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					$this->tb_files->insert_data($data);
					$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
				}
			} else {
				/** EDIT */
				$data = array(
					'file_namafile' => "$file_nama.$fileExtension",
					'file_note' => $file_note,
					'file_tgledit' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				);
				$this->tb_files->update_data(['file_kd' => $file_kd], $data);
				$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete_file()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$file_kd = $this->input->get('id', TRUE);

		$actUpdateFile = $this->tb_files->update_data(['file_kd' => $file_kd], ['file_tgldelete' => date('Y-m-d H:i:s')]);

		if ($actUpdateFile) {
			$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		} else {
			$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}

	public function action_master_insert()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtbarang_kd', 'Jenis', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrbarang_kd' => (!empty(form_error('txtbarang_kd'))) ? buildLabel('warning', form_error('txtbarang_kd', '"', '"')) : '',
			);
		} else {
			$kd_barang = $this->input->post('txtbarang_kd', true);
			$bom_ket = $this->input->post('txtbom_ket', true);

			/** Cek kd_barang bef insert */
			$cekBarang = $this->tm_bom->get_by_param(['bom_jenis' => 'std', 'kd_barang' => $kd_barang]);
			if ($cekBarang->num_rows() == 0) {
				$bom_kd = $this->tm_bom->create_code();
				$data = array(
					'bom_kd' => $bom_kd,
					'kd_barang' => $kd_barang,
					'project_kd' => null,
					'bom_jenis' => 'std',
					'bom_ket' => !empty($bom_ket) ? $bom_ket : null,
					'bom_tglinput' => date('Y-m-d H:i:s'),
					'bom_tgledit' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				);

				$act = $this->tm_bom->insert_data($data);
				if ($act) {
					$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => $data);
				} else {
					$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
				}
			} else {
				$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal arang sudah ada');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_master_duplicate()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtbarang_kd', 'Jenis', 'required', ['required' => '{field} tidak boleh kosong!']);

		$resp['csrf'] = $this->security->get_csrf_hash();
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrbarang_kd' => (!empty(form_error('txtbarang_kd'))) ? buildLabel('warning', form_error('txtbarang_kd', '"', '"')) : '',
			);
		} else {
			$bom_kd = $this->input->post('txtbom_kd', true);
			$kd_barang = $this->input->post('txtbarang_kd', true);
			$bom_ket = $this->input->post('txtbom_ket', true);

			/** Cek barang sudah memiliki BOM apa belum ? */
			$cekBarang = $this->tm_bom->get_by_param(array('bom_jenis' => 'std', 'kd_barang' => $kd_barang));
			if ($cekBarang->num_rows() == 0) {
				$duplicateBom_kd = $this->tm_bom->create_code();
				$dataMaster = array(
					'bom_kd' => $duplicateBom_kd,
					'bom_jenis' => 'std',
					'kd_barang' => !empty($kd_barang) ? $kd_barang : null,
					'bom_ket' => !empty($bom_ket) ? $bom_ket : null,
					'bom_tglinput' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				);
			} else {
				$resp['code'] = 400;
				$resp['status'] = 'Gagal';
				$resp['pesan'] = 'Barang Sudah Ada';
				echo json_encode($resp);
				die();
			}

			/** Detail bom */
			$dataDetailBom = $this->td_bom_detail->get_by_param(array('bom_kd' => $bom_kd))->result_array();
			$bomdetail_kd = $this->td_bom_detail->create_code();

			foreach ($dataDetailBom as $each) :
				$batchdataDetail[] = array(
					'bomdetail_kd' => $bomdetail_kd,
					'bom_kd' => $duplicateBom_kd,
					'partmain_kd' => $each['partmain_kd'],
					'bomdetail_tglinput' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				);
				$urutanBomdetail = (int) substr($bomdetail_kd, -10);
				$angkaBomdetail = $urutanBomdetail + 1;
				$bomdetail_kd = 'DBOM' . str_pad($angkaBomdetail, 10, '0', STR_PAD_LEFT);
			endforeach;

			$actMaster = $this->tm_bom->insert_data($dataMaster);
			$actDetail = $this->td_bom_detail->insert_batch_data($batchdataDetail);
			if ($actMaster && $actDetail) {
				$resp['code'] = 201;
				$resp['status'] = 'Sukses';
				$resp['pesan'] = 'Terduplicate';
				echo json_encode($resp);
				die();
			} else {
				$resp['code'] = 400;
				$resp['status'] = 'Gagal';
				$resp['pesan'] = 'Gagal Simpan';
				echo json_encode($resp);
				die();
			}
		}
	}

	public function action_master_update()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtbarang_kd', 'Jenis', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrbarang_kd' => (!empty(form_error('txtbarang_kd'))) ? buildLabel('warning', form_error('txtbarang_kd', '"', '"')) : '',
			);
		} else {
			$bom_kd =  $this->input->post('txtbom_kd', true);
			$kd_barang = $this->input->post('txtbarang_kd', true);
			$bom_ket = $this->input->post('txtbom_ket', true);

			$dataMain = array(
				'kd_barang' => $kd_barang,
				'project_kd' => null,
				'bom_ket' => !empty($bom_ket) ? $bom_ket : null,
				'bom_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);
			$act = $this->tm_bom->update_data(array('bom_kd' => $bom_kd), $dataMain);

			if ($act) {
				$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terupdate', 'data' => array('bom_kd' => $bom_kd));
			} else {
				$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_insert()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpartmain_kd', 'Part', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpartmain_kd' => (!empty(form_error('txtpartmain_kd'))) ? buildLabel('warning', form_error('txtpartmain_kd', '"', '"')) : '',
			);
		} else {
			$bom_kd = $this->input->post('txtbom_kd');
			$bomdetail_kd = $this->input->post('txtbomdetail_kd');
			$partmain_kd = $this->input->post('txtpartmain_kd');
			$bomdetail_generatewo = $this->input->post('txtbomdetail_generatewo');
			$bomdetail_wogrouping = $this->input->post('txtbomdetail_wogrouping');

			$bomdetail_generatewo = isset($bomdetail_generatewo) ? 'T' : 'F';
			$bomdetail_wogrouping = isset($bomdetail_wogrouping) ? 'T' : 'F';

			$data = array(
				'bom_kd' => $bom_kd,
				'partmain_kd' => $partmain_kd,
				'bomdetail_generatewo' => $bomdetail_generatewo,
				'bomdetail_wogrouping' => $bomdetail_wogrouping,
				'bomdetail_tglinput' => date('Y-m-d H:i:s'),
				'bomdetail_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);
			if (empty($bomdetail_kd)) {
				// Add
				$bomdetail_kd =  $this->td_bom_detail->create_code();
				$data = array_merge($data, ['bomdetail_kd' => $bomdetail_kd]);

				$act = $this->td_bom_detail->insert_data($data);
			} else {
				// Update
				$act = $this->td_bom_detail->update_data(['bomdetail_kd' => $bomdetail_kd], $data);
			}

			if ($act) {
				$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			} else {
				$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$bom_kd = $this->input->get('id', TRUE);

		$actDelDetail = $this->td_bom_detail->delete_by_param(array('bom_kd' => $bom_kd));
		$actDel = $this->tm_bom->delete_data($bom_kd);
		if ($actDelDetail == true && $actDel == true) {
			$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		} else {
			$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}

	public function action_delete_detail()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);

		$actDel = $this->td_bom_detail->delete_data($id);
		if ($actDel == true) {
			$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		} else {
			$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
}
