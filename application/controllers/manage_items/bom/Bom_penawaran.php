<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Bom_penawaran extends MY_Controller {
	private $class_link = 'manage_items/bom/bom_penawaran';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
        $this->load->model(['tm_bom', 'td_part', 'tm_bom_penawaran', 'tm_customer', 'td_bom_penawaran_detail', 'td_bom_state_log', 'tb_jenis_customer']);
	}

	public function index() {
		parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();
		$this->table_box();
	}

	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->tm_bom_penawaran->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
    }

	public function form_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	 public function form_master_main(){
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        $sts = $this->input->get('sts');
        $id = $this->input->get('id');

        if ($sts == 'edit'){
			$act = $this->tm_bom_penawaran->get_by_param_detail(array('tm_bom_penawaran.bompenawaran_kd'=> $id))->row_array();			
            $data['rowData'] = $act;
        }

        /** Opsi Customer */
		$actopsiCustomer = $this->tm_customer->get_all()->result_array();
		$opsiCustomer[null] = '-- Pilih Opsi --';
		foreach ($actopsiCustomer as $eachCustomer):
			$opsiCustomer[$eachCustomer['kd_customer']] = $eachCustomer['code_customer']." - ".$eachCustomer['nm_customer'];
		endforeach;

		$actopsiJenisCustomer = $this->tb_jenis_customer->get_all()->result_array();
		foreach ($actopsiJenisCustomer as $eachJenisCustomer):
			$opsiJenisCustomer[$eachJenisCustomer['kd_jenis_customer']] = $eachJenisCustomer['nm_jenis_customer'];
		endforeach;

		$data['opsiCustomer'] = $opsiCustomer;
		$data['opsiJenisCustomer'] = $opsiJenisCustomer;
        $data['sts'] = $sts;
        $data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_master_main', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');
       
        /** Opsi Part 
		 * cari part dengan status Approved
		 * dan belum ditarik ke bom penawaran yang statusnya masih editing
		*/
		$actopsiPart = $this->td_part->get_by_param_detail(['td_part.partstate_kd' => 5])->result_array();
		$bompenawaranEditing = $this->td_bom_penawaran_detail->get_by_param_detail (['tm_bom_penawaran.bomstate_kd' => 1])->result_array();
		$arrayPartFilter = [];
		$arrayPartExisting = [];
		foreach ($bompenawaranEditing as $rPenawaranEditing) {
			$arrayPartExisting[] = $rPenawaranEditing['part_kd'];
		}
		foreach ($actopsiPart as $rPart) {
			if (in_array($rPart['part_kd'], $arrayPartExisting)){
				continue;
			}
			$arrayPartFilter[] = [
				'part_kd' => $rPart['part_kd'],
				'partmain_nama' => $rPart['partmain_nama'],
				'part_versi' => $rPart['part_versi'],
			];		
		}

		$opsiPart[null] = '-- Pilih Opsi --';
		foreach ($arrayPartFilter as $eachPart):
			$opsiPart[$eachPart['part_kd']] = $eachPart['partmain_nama'].' / versi'.$eachPart['part_versi'];
		endforeach;

		$data['dataMaster'] = $this->tm_bom_penawaran->get_by_param_detail (['tm_bom_penawaran.bompenawaran_kd' => $id])->row_array();
        $data['opsiPart'] = $opsiPart;
		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}
	
	public function table_detail_main(){
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        $sts = $this->input->get('sts');
		$id = $this->input->get('id');

        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_detail_main', $data);
    }

    public function table_detail_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $this->load->library(['ssp']);

        $sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data = $this->td_bom_penawaran_detail->ssp_table($id);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function view_bom() {
		parent::administrator();
        parent::pnotify_assets();
		parent::select2_assets();

		$id = $this->input->get('id', true);
		$id = url_decrypt($id);
		if ($id == false) {
			show_error('ID tidak ditemukan...!!! <br> <a href="'.base_url().'"> Home </a>', '202', 'Error ID');
		}

		$data['class_link'] = $this->class_link;
		$data['id'] = $id;
		$data['header'] = $this->tm_bom_penawaran->get_by_param_detail (['tm_bom_penawaran.bompenawaran_kd' => $id])->row_array();
		$data['buttonState'] = $this->tm_bom_penawaran->generateButtonState ($id);
		$this->load->view('page/'.$this->class_link.'/viewbom_box', $data);
	}

	public function viewbom_tablemain () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$id = $this->input->get('id', true);

		$bompenawarandetail = $this->td_bom_penawaran_detail->get_by_param_detail_part(['td_bom_penawaran_detail.bompenawaran_kd' => $id]);
		
		$data['id'] = $id;
		$data['rowBom'] = $this->tm_bom_penawaran->get_by_param_detail(['tm_bom_penawaran.bompenawaran_kd' => $id])->row_array();
		$data['result'] = $bompenawarandetail->result_array();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/viewbom_tablemain', $data);
	}

	public function formupdateharga_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$bompenawaran_kd = $this->input->get('bompenawaran_kd', true);
		$part_kd = $this->input->get('part_kd', true);

		$part = $this->td_part->get_by_param(['part_kd' => $part_kd])->row_array();

		$bom_penawaran = $this->tm_bom_penawaran->get_by_param_detail(['tm_bom_penawaran.bompenawaran_kd' => $bompenawaran_kd])->row_array();
		
		$data['part_kd'] = $part_kd;
		$data['header'] = $this->td_part->get_by_param_detail(['part_kd' => $part_kd])->row_array();
		$data['part_hargacosting'] = $this->td_part->get_totalharga_costing($part_kd);
		$data['part_hargaapproved'] = $part['part_hargaapproved'];
		$data['jenis_customer'] = $this->tb_jenis_customer->get_row($bom_penawaran['kd_jenis_customer']);
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/formupdateharga_main', $data);
	}

	public function formubahstate_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$bompenawaran_kd = $this->input->get('bompenawaran_kd', true);
		$bomstate_kd = $this->input->get('bomstate_kd', true);

		$data['class_link'] = $this->class_link;
		$data['bompenawaran_kd'] = $bompenawaran_kd;
		$data['bomstate_kd'] = $bomstate_kd;
		$this->load->view('page/'.$this->class_link.'/formubahstate_main', $data);
	}

	public function viewbom_detaillog_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$bompenawaran_kd = $this->input->get('bompenawaran_kd', true);

		$resultBomstateLog = $this->td_bom_state_log->get_byparam_detail(['td_bom_state_log.bompenawaran_kd' => $bompenawaran_kd])->result_array();
		$data['resultBomstateLog'] = $resultBomstateLog;
		$this->load->view('page/'.$this->class_link.'/viewbom_detaillog_main', $data);		
	}

	public function viewcosting_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$part_kd = $this->input->get('part_kd');

		$data['header'] = $this->td_part->get_by_param_detail (['td_part.part_kd' => $part_kd])->row_array();
		$data['part_kd'] = $part_kd;
		$data['class_link'] = $this->class_link;
		$data['class_link_partcosting'] = 'manage_items/manage_part/part_costing';
		$this->load->view('page/'.$this->class_link.'/viewcosting_box', $data);
	}

	public function get_jeniscustomer () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$kd_customer = $this->input->get('kd_customer', true);
		
        if (!empty($kd_customer)){
            $act = $this->tm_customer->get_by_param (['kd_customer' => $kd_customer]);
            if ($act->num_rows() > 0 ){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => '', 'data' => $act->row_array());
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Tidak Ditemukan');
            }
        }else{
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Kosong');
        }

        echo json_encode($resp);
	}
	
	public function action_master_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtbompenawaran_note', 'Keterangan', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtkd_jenis_customer', 'Keterangan', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrbompenawaran_note' => (!empty(form_error('txtbompenawaran_note')))?buildLabel('warning', form_error('txtbompenawaran_note', '"', '"')):'',
				'idErrkd_jenis_customer' => (!empty(form_error('txtkd_jenis_customer')))?buildLabel('warning', form_error('txtkd_jenis_customer', '"', '"')):'',
			);

		}else { 
			$kd_customer = $this->input->post('txtkd_customer', true);
            $bompenawaran_note = $this->input->post('txtbompenawaran_note', true);
			$kd_jenis_customer = $this->input->post('txtkd_jenis_customer', true);

			$data = array(
				'bompenawaran_kd' => $this->tm_bom_penawaran->create_code(),
				'kd_customer' => $kd_customer,
				'kd_jenis_customer' => $kd_jenis_customer,
				'bomstate_kd' => 1,
				'bompenawaran_note' => $bompenawaran_note,
				'bompenawaran_tglinput' => date('Y-m-d H:i:s'),
				'bompenawaran_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);
			
			$act = $this->tm_bom_penawaran->insert_data($data);
			if ($act) {
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => $data);
			} else {
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_master_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtbompenawaran_note', 'Keterangan', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrbompenawaran_note' => (!empty(form_error('txtbompenawaran_note')))?buildLabel('warning', form_error('txtbompenawaran_note', '"', '"')):'',
			);

		}else { 
			$kd_customer = $this->input->post('txtkd_customer', true);
            $bompenawaran_note = $this->input->post('txtbompenawaran_note', true);
            $bompenawaran_kd = $this->input->post('txtbompenawaran_kd', true);
			$kd_jenis_customer = $this->input->post('txtkd_jenis_customer', true);

			$data = array(
				'kd_customer' => $kd_customer,
				'kd_jenis_customer' => $kd_jenis_customer,
				'bomstate_kd' => 1,
				'bompenawaran_note' => $bompenawaran_note,
				'bompenawaran_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);
			
			$act = $this->tm_bom_penawaran->update_data(['bompenawaran_kd' => $bompenawaran_kd], $data);
			if ($act) {
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate', 'data' => ['bompenawaran_kd' => $bompenawaran_kd]);
			} else {
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Update');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpart_kd', 'Part', 'required', ['required' => '{field} tidak boleh kosong!']);	

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpart_kd' => (!empty(form_error('txtpart_kd')))?buildLabel('warning', form_error('txtpart_kd', '"', '"')):'',
			);
			
		}else {
			$bompenawaran_kd = $this->input->post('txtbompenawaran_kd');
			$part_kd = $this->input->post('txtpart_kd');
			
			$data = array(
                'bompenawarandetail_kd' => $this->td_bom_penawaran_detail->create_code(),
				'bompenawaran_kd' => $bompenawaran_kd,
				'part_kd' => $part_kd,
				'bompenawarandetail_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			/** Cek kode */
			$cekPart = $this->td_bom_penawaran_detail->get_by_param(['part_kd' => $part_kd]);
			if ($cekPart->num_rows() == 0) {
				$act = $this->td_bom_penawaran_detail->insert_data($data);
				if ($act) {
					$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
				} else {
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
				}
			}else {
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Part Sudah Ada');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_ubahstate () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$bompenawaran_kd = $this->input->post('txtbompenawaran_kd', true);
		$bomstate_kd = $this->input->post('txtbomstate_kd', true);
		$bomstate_note = $this->input->post('txtbomstate_note', true);
				
		$actUbah = $this->tm_bom_penawaran->ubah_state ($bompenawaran_kd, $bomstate_kd, $bomstate_note);
		if ($actUbah['status']) {
			$resp = array('code'=> 200, 'status' => 'Sukses', 'data_message'=>$actUbah);
		}else {
			$resp = array('code'=> 400, 'status' => 'Gagal', 'data_message'=>$actUbah, 'pesan' => 'Data tdk ditemukan');
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}
	
	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$actDelDetail = $this->td_bom_penawaran_detail->delete_by_param(['bompenawaran_kd' => $id]);
		$actDel = $this->tm_bom_penawaran->delete_data($id);
		if ($actDel){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}

	public function action_delete_detail() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$actDel = $this->td_bom_penawaran_detail->delete_data($id);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}

	function action_update_hargapart () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpart_kd', 'Part', 'required', ['required' => '{field} tidak boleh kosong!']);	
		$this->form_validation->set_rules('txtpart_hargaapproved', 'Harga', 'required', ['required' => '{field} tidak boleh kosong!']);	

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpart_hargacosting' => (!empty(form_error('txtpart_kd')))?buildLabel('warning', form_error('txtpart_kd', '"', '"')):'',
				'idErrpart_hargaapproved' => (!empty(form_error('txtpart_hargaapproved')))?buildLabel('warning', form_error('txtpart_hargaapproved', '"', '"')):'',
			);
			
		}else {
			$part_kd = $this->input->post('txtpart_kd');
			$part_hargacosting = $this->input->post('txtpart_hargacosting');
			$part_hargaapproved = $this->input->post('txtpart_hargaapproved');
			
			$data = array(
                'part_hargacosting' => $part_hargacosting,
				'part_hargaapproved' => $part_hargaapproved,
				'part_hargacosting_update' => date('Y-m-d H:i:s'),
			);

			$act = $this->td_part->update_data (['part_kd' => $part_kd], $data);
			if ($act) {
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
			} else {
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Update');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

}
