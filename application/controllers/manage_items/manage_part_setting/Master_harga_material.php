<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Master_harga_material extends MY_Controller {
	private $class_link = 'manage_items/manage_part_setting/master_harga_material';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
        $this->load->model(['td_rawmaterial_harga', 'tm_rawmaterial']);
	}

	public function index() {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();
		$this->table_box();
	}
		
	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data (){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->td_rawmaterial_harga->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function form_box(){
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');
		
		$data['sts'] = $sts;
        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

        if ($sts == 'edit'){
            $rowData = $this->td_rawmaterial_harga->get_by_param_detail(array('rmharga_kd'=>$id))->row_array();
			$data['rowData'] = $rowData;
        }
		/** Opsi RM */
        $actopsiRm = $this->tm_rawmaterial->get_all()->result_array();
		foreach ($actopsiRm as $eachRm):
			$opsiRm[$eachRm['rm_kd']] = $eachRm['rm_kode'].'|'.$eachRm['rm_deskripsi'].'/'.$eachRm['rm_spesifikasi'];
		endforeach;

        $data['sts'] = $sts;
        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$data['opsiRm'] = $opsiRm;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrm_kd', 'User', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtrmharga_hargainput', 'Email', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrrm_kd' => (!empty(form_error('txtrm_kd')))?buildLabel('warning', form_error('txtrm_kd', '"', '"')):'',
				'idErrrmharga_hargainput' => (!empty(form_error('txtrmharga_hargainput')))?buildLabel('warning', form_error('txtrmharga_hargainput', '"', '"')):'',
			);
			
		}else {
            $rm_kd = $this->input->post('txtrm_kd', true);
            $rmharga_hargainput = $this->input->post('txtrmharga_hargainput', true);
			
			$cek = $this->td_rawmaterial_harga->get_by_param(['rm_kd' => $rm_kd]);
			if ($cek->num_rows() == 0) {
				$data = array(
					'rmharga_kd' => $this->td_rawmaterial_harga->create_code(),
					'rm_kd' => $rm_kd,
					'rmharga_hargagr' => $rmharga_hargainput,
					'rmharga_hargainput' => $rmharga_hargainput,
					'rmharga_tgledit_hargagr' => date('Y-m-d H:i:s'),
					'rmharga_tgledit_hargainput' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				);
	
				$act = $this->td_rawmaterial_harga->insert_data($data);
				if ($act){
					$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
				}else{
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
				}
			}else {
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'RM sudah ada');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrm_kd', 'User', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtrmharga_hargainput', 'Email', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrrm_kd' => (!empty(form_error('txtrm_kd')))?buildLabel('warning', form_error('txtrm_kd', '"', '"')):'',
				'idErrrmharga_hargainput' => (!empty(form_error('txtrmharga_hargainput')))?buildLabel('warning', form_error('txtrmharga_hargainput', '"', '"')):'',
			);
			
		}else {
			$rm_kd = $this->input->post('txtrm_kd', true);
            $rmharga_hargainput = $this->input->post('txtrmharga_hargainput', true);
            $rmharga_kd = $this->input->post('txtrmharga_kd', true);
			
			$data = array(
				'rmharga_hargainput' => $rmharga_hargainput,
				'rmharga_tgledit_hargainput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->td_rawmaterial_harga->update_data(array('rmharga_kd' => $rmharga_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$actDel = $this->td_rawmaterial_harga->delete_data($id);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
