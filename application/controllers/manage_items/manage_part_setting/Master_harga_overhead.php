<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Master_harga_overhead extends MY_Controller {
	private $class_link = 'manage_items/manage_part_setting/master_harga_overhead';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
        $this->load->model(['tb_part_overhead', 'td_rawmaterial_satuan']);
	}

	public function index() {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();
		$this->table_box();
	}
		
	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data (){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->tb_part_overhead->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function form_box(){
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');
		
		$data['sts'] = $sts;
        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

        if ($sts == 'edit'){
            $rowData = $this->tb_part_overhead->get_by_param_detail(array('overhead_kd'=>$id))->row_array();
			$data['rowData'] = $rowData;
        }
		/** Opsi Bagian */
        $actopsiSatuan = $this->td_rawmaterial_satuan->get_all()->result_array();
		foreach ($actopsiSatuan as $eachSatuan):
			$opsiSatuan[$eachSatuan['rmsatuan_kd']] = $eachSatuan['rmsatuan_nama'];
		endforeach;

        $data['sts'] = $sts;
        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$data['opsiSatuan'] = $opsiSatuan;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtoverhead_nama', 'User', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtoverhead_satuankd', 'Satuan', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtoverhead_harga', 'Harga', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErroverhead_nama' => (!empty(form_error('txtoverhead_nama')))?buildLabel('warning', form_error('txtoverhead_nama', '"', '"')):'',
				'idErroverhead_satuankd' => (!empty(form_error('txtoverhead_satuankd')))?buildLabel('warning', form_error('txtoverhead_satuankd', '"', '"')):'',
				'idErroverhead_harga' => (!empty(form_error('txtoverhead_harga')))?buildLabel('warning', form_error('txtoverhead_harga', '"', '"')):'',
			);
			
		}else {
            $overhead_nama = $this->input->post('txtoverhead_nama', true);
            $overhead_satuankd = $this->input->post('txtoverhead_satuankd', true);
            $overhead_harga = $this->input->post('txtoverhead_harga', true);
			
            $data = array(
                'overhead_kd' => $this->tb_part_overhead->create_code(),
                'overhead_nama' => $overhead_nama,
                'overhead_satuankd' => $overhead_satuankd,
                'overhead_harga' => $overhead_harga,
                'overhead_tglinput' => date('Y-m-d H:i:s'),
                'overhead_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            );

            $act = $this->tb_part_overhead->insert_data($data);
            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtoverhead_nama', 'User', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtoverhead_satuankd', 'Satuan', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtoverhead_harga', 'Harga', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErroverhead_nama' => (!empty(form_error('txtoverhead_nama')))?buildLabel('warning', form_error('txtoverhead_nama', '"', '"')):'',
				'idErroverhead_satuankd' => (!empty(form_error('txtoverhead_satuankd')))?buildLabel('warning', form_error('txtoverhead_satuankd', '"', '"')):'',
				'idErroverhead_harga' => (!empty(form_error('txtoverhead_harga')))?buildLabel('warning', form_error('txtoverhead_harga', '"', '"')):'',
			);
			
		}else {
            $overhead_nama = $this->input->post('txtoverhead_nama', true);
            $overhead_satuankd = $this->input->post('txtoverhead_satuankd', true);
            $overhead_harga = $this->input->post('txtoverhead_harga', true);
            $overhead_kd = $this->input->post('txtoverhead_kd', true);
			
				
            $data = array(
                'overhead_nama' => $overhead_nama,
                'overhead_satuankd' => $overhead_satuankd,
                'overhead_harga' => $overhead_harga,
                'overhead_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            );

			$act = $this->tb_part_overhead->update_data(array('overhead_kd' => $overhead_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$actDel = $this->tb_part_overhead->delete_data($id);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
