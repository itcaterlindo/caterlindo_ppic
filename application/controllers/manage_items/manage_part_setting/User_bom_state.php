<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class User_bom_state extends MY_Controller {
	private $class_link = 'manage_items/manage_part_setting/user_bom_state';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
        $this->load->model(['td_bom_state_user', 'tb_admin', 'tb_bom_state']);
	}

	public function index() {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();
		$this->table_box();
	}
		
	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data (){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->td_bom_state_user->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function form_box(){
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');
		
		$data['sts'] = $sts;
        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

        if ($sts == 'edit'){
            $rowData = $this->td_bom_state_user->get_by_param_detail(array('bomstateuser_kd'=>$id))->row_array();
			$data['rowData'] = $rowData;
        }

		/** Opsi admin */
        $actopsiAdmin = $this->tb_admin->get_all()->result_array();
		foreach ($actopsiAdmin as $eachAdmin):
			$opsiAdmin[$eachAdmin['kd_admin']] = $eachAdmin['nm_admin'];
        endforeach;
        
		/** Opsi state */
        $actopsiState = $this->tb_bom_state->get_all()->result_array();
		foreach ($actopsiState as $eachState):
			$opsiState[$eachState['bomstate_kd']] = $eachState['bomstate_nama'];
		endforeach;

        $data['sts'] = $sts;
        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$data['opsiAdmin'] = $opsiAdmin;
		$data['opsiState'] = $opsiState;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtbomstateuser_admin', 'Admin', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtbomstate_kd', 'State', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtbomstateuser_email', 'Email', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrbomstateuser_admin' => (!empty(form_error('txtbomstateuser_admin')))?buildLabel('warning', form_error('txtbomstateuser_admin', '"', '"')):'',
				'idErrbomstate_kd' => (!empty(form_error('txtbomstate_kd')))?buildLabel('warning', form_error('txtbomstate_kd', '"', '"')):'',
				'idErrbomstateuser_email' => (!empty(form_error('txtbomstateuser_email')))?buildLabel('warning', form_error('txtbomstateuser_email', '"', '"')):'',
			);
			
		}else {
            $bomstateuser_admin = $this->input->post('txtbomstateuser_admin', true);
            $bomstate_kd = $this->input->post('txtbomstate_kd', true);
            $bomstateuser_email = $this->input->post('txtbomstateuser_email', true);
            $bomstateuser_emailcc = $this->input->post('txtbomstateuser_emailcc', true);
			
            $data = array(
                'bomstateuser_kd' => $this->td_bom_state_user->create_code(),
                'bomstateuser_admin' => $bomstateuser_admin,
                'bomstate_kd' => $bomstate_kd,
                'bomstateuser_email' => $bomstateuser_email,
                'bomstateuser_emailcc' => $bomstateuser_emailcc,
                'bomstateuser_tglinput' => date('Y-m-d H:i:s'),
                'bomstateuser_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            );

            $act = $this->td_bom_state_user->insert_data($data);
            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtbomstateuser_admin', 'Admin', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtbomstate_kd', 'State', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtbomstateuser_email', 'Email', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrbomstateuser_admin' => (!empty(form_error('txtbomstateuser_admin')))?buildLabel('warning', form_error('txtbomstateuser_admin', '"', '"')):'',
				'idErrbomstate_kd' => (!empty(form_error('txtbomstate_kd')))?buildLabel('warning', form_error('txtbomstate_kd', '"', '"')):'',
				'idErrbomstateuser_email' => (!empty(form_error('txtbomstateuser_email')))?buildLabel('warning', form_error('txtbomstateuser_email', '"', '"')):'',
			);
			
		}else {
            $bomstateuser_admin = $this->input->post('txtbomstateuser_admin', true);
            $bomstate_kd = $this->input->post('txtbomstate_kd', true);
            $bomstateuser_email = $this->input->post('txtbomstateuser_email', true);
            $bomstateuser_kd = $this->input->post('txtbomstateuser_kd', true);
            $bomstateuser_emailcc = $this->input->post('txtbomstateuser_emailcc', true);
			
            $data = array(
                'bomstateuser_admin' => $bomstateuser_admin,
                'bomstate_kd' => $bomstate_kd,
				'bomstateuser_email' => $bomstateuser_email,
                'bomstateuser_emailcc' => $bomstateuser_emailcc,
                'bomstateuser_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            );
			$act = $this->td_bom_state_user->update_data(array('bomstateuser_kd' => $bomstateuser_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$actDel = $this->td_bom_state_user->delete_data($id);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
