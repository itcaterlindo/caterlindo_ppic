<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class User_part_costing extends MY_Controller {
    private $class_link = 'manage_items/manage_part_setting/user_part_costing';
    private $menu_kd = 89;

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
        $this->load->model(['td_admin_permission', 'tb_admin', 'tb_permission']);
	}

	public function index() {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();
		$this->table_box();
	}
		
	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data (){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->td_admin_permission->ssp_table($this->menu_kd);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function form_box(){
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');
		
		$data['sts'] = $sts;
        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

        if ($sts == 'edit'){
            $rowData = $this->td_admin_permission->get_by_param_detail(array('adminpermission_kd'=>$id))->row_array();
			$data['rowData'] = $rowData;
        }

		/** Opsi admin */
        $actopsiAdmin = $this->tb_admin->get_all()->result_array();
		foreach ($actopsiAdmin as $eachAdmin):
			$opsiAdmin[$eachAdmin['kd_admin']] = $eachAdmin['nm_admin'];
        endforeach;
        
		/** Opsi permission */
        $actopsiPermission = $this->tb_permission->get_by_param(['menu_kd' => $this->menu_kd])->result_array();
		foreach ($actopsiPermission as $eachPermission):
			$opsiPermission[$eachPermission['permission_kd']] = $eachPermission['permission_nama'];
		endforeach;

        $data['sts'] = $sts;
        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$data['opsiAdmin'] = $opsiAdmin;
		$data['opsiPermission'] = $opsiPermission;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtkd_admin', 'Admin', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpermission_kd', 'State', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrkd_admin' => (!empty(form_error('txtkd_admin')))?buildLabel('warning', form_error('txtkd_admin', '"', '"')):'',
				'idErrpermission_kd' => (!empty(form_error('txtpermission_kd')))?buildLabel('warning', form_error('txtpermission_kd', '"', '"')):'',
			);
			
		}else {
            $kd_admin = $this->input->post('txtkd_admin', true);
            $permission_kd = $this->input->post('txtpermission_kd', true);
			
            $data = array(
                'adminpermission_kd' => $this->td_admin_permission->create_code(),
                'kd_admin' => $kd_admin,
                'permission_kd' => $permission_kd,
                'adminpermission_tglinput' => date('Y-m-d H:i:s'),
                'adminpermission_tgledit' => date('Y-m-d H:i:s'),
            );

            $act = $this->td_admin_permission->insert_data($data);
            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtkd_admin', 'Admin', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpermission_kd', 'State', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrkd_admin' => (!empty(form_error('txtkd_admin')))?buildLabel('warning', form_error('txtkd_admin', '"', '"')):'',
				'idErrpermission_kd' => (!empty(form_error('txtpermission_kd')))?buildLabel('warning', form_error('txtpermission_kd', '"', '"')):'',
			);
			
		}else {
            $kd_admin = $this->input->post('txtkd_admin', true);
            $permission_kd = $this->input->post('txtpermission_kd', true);
            $adminpermission_kd = $this->input->post('txtadminpermission_kd', true);
			
            $data = array(
                'kd_admin' => $kd_admin,
                'permission_kd' => $permission_kd,
                'adminpermission_tgledit' => date('Y-m-d H:i:s'),
            );
			$act = $this->td_admin_permission->update_data(array('adminpermission_kd' => $adminpermission_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$actDel = $this->td_admin_permission->delete_data($id);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
