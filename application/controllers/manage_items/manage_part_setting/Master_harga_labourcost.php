<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Master_harga_labourcost extends MY_Controller {
	private $class_link = 'manage_items/manage_part_setting/master_harga_labourcost';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
        $this->load->model(['tb_part_labourcost_harga', 'tb_bagian']);
	}

	public function index() {
        parent::administrator();
        parent::pnotify_assets();
		$this->table_box();
	}
		
	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data (){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->tb_part_labourcost_harga->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function form_box(){
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');
		
		$data['sts'] = $sts;
        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

        if ($sts == 'edit'){
            $rowData = $this->tb_part_labourcost_harga->get_by_param_detail(array('labourcostharga_kd'=>$id))->row_array();
			$data['rowData'] = $rowData;
        }
		/** Opsi Bagian */
        $actopsiBagian = $this->tb_bagian->get_all()->result_array();
		foreach ($actopsiBagian as $eachBagian):
			$opsiBagian[$eachBagian['bagian_kd']] = $eachBagian['bagian_nama'];
		endforeach;

		$opsiSatuan = ['Persons' => 'Persons', 'Team' => 'Team'];

        $data['sts'] = $sts;
        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$data['opsiBagian'] = $opsiBagian;
		$data['opsiSatuan'] = $opsiSatuan;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtbagian_kd', 'User', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtlabourcostharga_satuan', 'Email', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtlabourcostharga_harga', 'Email', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrbagian_kd' => (!empty(form_error('txtbagian_kd')))?buildLabel('warning', form_error('txtbagian_kd', '"', '"')):'',
				'idErrrmharga_hargainput' => (!empty(form_error('txtlabourcostharga_satuan')))?buildLabel('warning', form_error('txtlabourcostharga_satuan', '"', '"')):'',
				'idErrlabourcostharga_harga' => (!empty(form_error('txtlabourcostharga_harga')))?buildLabel('warning', form_error('txtlabourcostharga_harga', '"', '"')):'',
			);
			
		}else {
            $bagian_kd = $this->input->post('txtbagian_kd', true);
            $labourcostharga_satuan = $this->input->post('txtlabourcostharga_satuan', true);
            $labourcostharga_harga = $this->input->post('txtlabourcostharga_harga', true);
			
			$cek = $this->tb_part_labourcost_harga->get_by_param(['bagian_kd' => $bagian_kd, 'labourcostharga_satuan' => $labourcostharga_satuan]);
			if ($cek->num_rows() == 0) {
				$data = array(
					'labourcostharga_kd' => $this->tb_part_labourcost_harga->create_code(),
					'bagian_kd' => $bagian_kd,
					'labourcostharga_satuan' => $labourcostharga_satuan,
					'labourcostharga_harga' => $labourcostharga_harga,
					'labourcostharga_tglinput' => date('Y-m-d H:i:s'),
					'labourcostharga_tgledit' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				);
	
				$act = $this->tb_part_labourcost_harga->insert_data($data);
				if ($act){
					$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
				}else{
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
				}
			}else {
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Labourcost sudah ada');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtbagian_kd', 'User', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtlabourcostharga_satuan', 'Email', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtlabourcostharga_harga', 'Email', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrbagian_kd' => (!empty(form_error('txtbagian_kd')))?buildLabel('warning', form_error('txtbagian_kd', '"', '"')):'',
				'idErrrmharga_hargainput' => (!empty(form_error('txtlabourcostharga_satuan')))?buildLabel('warning', form_error('txtlabourcostharga_satuan', '"', '"')):'',
				'idErrlabourcostharga_harga' => (!empty(form_error('txtlabourcostharga_harga')))?buildLabel('warning', form_error('txtlabourcostharga_harga', '"', '"')):'',
			);
			
		}else {
			$bagian_kd = $this->input->post('txtbagian_kd', true);
            $labourcostharga_satuan = $this->input->post('txtlabourcostharga_satuan', true);
            $labourcostharga_harga = $this->input->post('txtlabourcostharga_harga', true);
            $labourcostharga_kd = $this->input->post('txtlabourcostharga_kd', true);
			
			$data = array(
				'labourcostharga_harga' => $labourcostharga_harga,
				'labourcostharga_satuan' => $labourcostharga_satuan,
				'labourcostharga_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->tb_part_labourcost_harga->update_data(array('labourcostharga_kd' => $labourcostharga_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$actDel = $this->tb_part_labourcost_harga->delete_data($id);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
