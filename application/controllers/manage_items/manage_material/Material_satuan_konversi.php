<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Material_satuan_konversi extends MY_Controller
{
    private $class_link = 'manage_items/manage_material/material_satuan_konversi';

    public function __construct()
    {
        parent::__construct();

        $this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
        $this->load->model(['td_rawmaterial_satuan_konversi', 'td_rawmaterial_satuan_konversi']);
    }

    public function index()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();
        $this->table_box();
    }

    public function form_box()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $sts = $this->input->get('sts');
        $id = $this->input->get('id');

        $data['sts'] = $sts;
        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_box', $data);
    }

    public function form_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $sts = $this->input->get('sts');
        $id = $this->input->get('id');

        if ($sts == 'edit') {
            $act = $this->db->select('td_rawmaterial_satuan_konversi.*, fr_satuan.rmsatuan_nama as fr_satuan, to_satuan.rmsatuan_nama as to_satuan')
                ->where('td_rawmaterial_satuan_konversi.rmsatuankonversi_kd', $id)
                ->join('td_rawmaterial_satuan as fr_satuan', 'fr_satuan.rmsatuan_kd=td_rawmaterial_satuan_konversi.rmsatuankonversi_from', 'left')
                ->join('td_rawmaterial_satuan as to_satuan', 'to_satuan.rmsatuan_kd=td_rawmaterial_satuan_konversi.rmsatuankonversi_to', 'left')
                ->get('td_rawmaterial_satuan_konversi')->row_array();
            $data['id'] = $id;
            $data['rowData'] = $act;
        }

        $data['sts'] = $sts;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_main', $data);
    }

    public function table_box()
    {
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function table_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }

    public function table_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->load->library(['ssp']);

        $data = $this->td_rawmaterial_satuan_konversi->ssp_table2();
        
        echo json_encode($data);
    }

    public function action_insert()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtrmsatuankonversi_from', 'Dari', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtrmsatuankonversi_to', 'Ke', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtrmsatuankonversi_konversi', 'Konversi', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrrmsatuankonversi_from' => (!empty(form_error('txtrmsatuankonversi_from'))) ? buildLabel('warning', form_error('txtrmsatuankonversi_from', '"', '"')) : '',
                'idErrrmsatuankonversi_to' => (!empty(form_error('txtrmsatuankonversi_to'))) ? buildLabel('warning', form_error('txtrmsatuankonversi_to', '"', '"')) : '',
                'idErrrmsatuankonversi_konversi' => (!empty(form_error('txtrmsatuankonversi_konversi'))) ? buildLabel('warning', form_error('txtrmsatuankonversi_konversi', '"', '"')) : '',
            );
        } else {
            $rmsatuankonversi_kd = $this->input->post('txtrmsatuankonversi_kd');
            $rmsatuankonversi_from = $this->input->post('txtrmsatuankonversi_from');
            $rmsatuankonversi_to = $this->input->post('txtrmsatuankonversi_to');
            $rmsatuankonversi_konversi = $this->input->post('txtrmsatuankonversi_konversi');

            $data = array(
                'rmsatuankonversi_from' => $rmsatuankonversi_from,
                'rmsatuankonversi_to' => $rmsatuankonversi_to,
                'rmsatuankonversi_konversi' => $rmsatuankonversi_konversi,
                'rmsatuankonversi_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            );

            if (empty($rmsatuankonversi_kd)) {
                /** Add */
                $rmsatuankonversi_kd = $this->td_rawmaterial_satuan_konversi->create_code();
                $data = array_merge($data, [
                    'rmsatuankonversi_kd' => $rmsatuankonversi_kd,
                ]);
                $dataBatch[] = $data;
                /** Jika material from tidak samadengan to
                 * ex : kg to kg
                 */
                if ($rmsatuankonversi_from != $rmsatuankonversi_to){
                    /** Insert kebalikan dari konversi ini */
                    $dataReverse = array(
                        'rmsatuankonversi_kd' => $rmsatuankonversi_kd + 1,
                        'rmsatuankonversi_from' => $rmsatuankonversi_to,
                        'rmsatuankonversi_to' => $rmsatuankonversi_from,
                        'rmsatuankonversi_konversi' => 1 / $rmsatuankonversi_konversi,
                        'rmsatuankonversi_tgledit' => date('Y-m-d H:i:s'),
                        'admin_kd' => $this->session->userdata('kd_admin'),
                    );
                    $dataBatch[] = $dataReverse;
                }
                $act = $this->td_rawmaterial_satuan_konversi->insert_batch($dataBatch);
            } else {
                /** Edit */
                $act = $this->td_rawmaterial_satuan_konversi->update_data(['rmsatuankonversi_kd' => $rmsatuankonversi_kd], $data);
            }
            if ($act) {
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            } else {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_delete()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $rmsatuan_kd = $this->input->get('id', TRUE);

        $actDel = $this->td_rawmaterial_satuan_konversi->delete_data($rmsatuan_kd);
        if ($actDel == true) {
            $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
        } else {
            $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
        }

        echo json_encode($resp);
    }
}
