<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Material_group_suplier extends MY_Controller {
	private $class_link = 'manage_items/manage_material/material_group_suplier';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_rawmaterial_group', 'td_rawmaterial_group_suplier']);
	}

	public function index() {
		parent::administrator();
		parent::pnotify_assets();
		$this->table_box();
	}

	public function form_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		if ($sts == 'edit'){
			$actFGuser = $this->td_rawmaterial_group_suplier->get_by_param (array('rmgroupsup_kd'=> $id))->row_array();			
			$data['rmgroupsup_kode'] = $actFGuser['rmgroupsup_kode'];
			$data['rmgroupsup_name'] = $actFGuser['rmgroupsup_name'];
		}

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->td_rawmaterial_group_suplier->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function action_insert() {
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrmgroup_nama', 'Tipe Admin', 'required', ['required' => '{field} tidak boleh kosong!']);	

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrRmgroup_nama' => (!empty(form_error('txtrmgroup_nama')))?buildLabel('warning', form_error('txtrmgroup_nama', '"', '"')):'',
			);
			
		}else {
			$rmgroup_nama = $this->input->post('txtrmgroup_nama');
			$rmgroup_kode = $this->input->post('txtrmgroup_kode');
			
			$data = array(
				'rmgroupsup_kode' => strtoupper($rmgroup_kode),
				'rmgroupsup_name' => strtoupper($rmgroup_nama)
			);

			$act = $this->td_rawmaterial_group_suplier->insert_data($data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_update() {
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrmgroup_nama', 'Tipe Admin', 'required', ['required' => '{field} tidak boleh kosong!']);	

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrRmgroup_nama' => (!empty(form_error('txtrmgroup_nama')))?buildLabel('warning', form_error('txtrmgroup_nama', '"', '"')):'',
			);
			
		}else {
			$rmgroup_nama = $this->input->post('txtrmgroup_nama');
			$rmgroup_kode = $this->input->post('txtrmgroup_kode');
			$id = $this->input->post('txtrmgroup_kd');

			$data = array(
				'rmgroupsup_kode' => strtoupper($rmgroup_kode),
				'rmgroupsup_name' => strtoupper($rmgroup_nama)
			);

			$act = $this->td_rawmaterial_group_suplier->update_data (array('rmgroupsup_kd' => $id), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$rmgroup_kd = $this->input->get('id', TRUE);
		
		$actDel = $this->td_rawmaterial_group_suplier->delete_data($rmgroup_kd);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
