<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Material_data extends MY_Controller {
	private $class_link = 'manage_items/manage_material/material_data';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_rawmaterial', 'tm_item_group', 'td_rawmaterial_group', 'td_rawmaterial_group_suplier', 'td_rawmaterial_kategori', 'td_rawmaterial_satuan',
			'tb_permission', 'td_rawmaterial_stok_master']);
	}

	public function index() {
		parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
		$this->table_box();
	}

	public function form_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$groupSelected = '';
		$kategoriSelected = '';
		$satuanSelected = '';
		$rmsSelected = '';

		if ($sts == 'edit' || $sts == 'view'){
			$act = $this->tm_rawmaterial->get_by_param (array('rm_kd'=> $id))->row_array();			
			$data['id'] = $act['rm_kd'];
			$data['rowData'] = $act;
		}
		/** GroupMaterial */
		$actopsiGroup = $this->td_rawmaterial_group->get_all()->result_array();
		$opsiGroup[''] = '-- Pilih Group Material --';
		foreach ($actopsiGroup as $eachGroup):
			$opsiGroup[$eachGroup['rmgroup_kd']] = $eachGroup['rmgroup_nama'];
		endforeach;

		/** KategoriMaterial */
		$actopsiKategori = $this->td_rawmaterial_kategori->get_all()->result_array();
		$opsiKategori[''] = '-- Pilih Kategori Material --';
		foreach ($actopsiKategori as $eachKategori):
			$opsiKategori[$eachKategori['rmkategori_kd']] = $eachKategori['rmkategori_nama'];
		endforeach;

		/** SatuanMaterial */
		$actopsiSatuan = $this->td_rawmaterial_satuan->get_all()->result_array();
		$opsiSatuan[''] = '-- Pilih Satuan Material --';
		foreach ($actopsiSatuan as $eachSatuan):
			$opsiSatuan[$eachSatuan['rmsatuan_kd']] = $eachSatuan['rmsatuan_nama'];
		endforeach;

		/** Material Group Suplier */
		$actopsirms = $this->td_rawmaterial_group_suplier->get_all()->result_array();
		$opsirms[''] = '-- Pilih Group Suplier --';
		foreach ($actopsirms as $eachrms):
			$opsirms[$eachrms['rmgroupsup_kd']] = $eachrms['rmgroupsup_name'];
		endforeach;

		/** Item Group */
		$actopsiitg = $this->tm_item_group->get_all()->result_array();
		$opsiitg[''] = '-- Pilih Item Group --';
		foreach ($actopsiitg as $eachrms):
			$opsiitg[$eachrms['item_group_kd']] = $eachrms['item_group_name'];
		endforeach;

		/** Negara */
		$actopsiNegara = $this->db->order_by('nm_negara')->get('tb_negara')->result_array();
		$opsiNegara[''] = '-- Pilih Negara --';
		foreach ($actopsiNegara as $eachNegara):
			$opsiNegara[$eachNegara['id']] = $eachNegara['nm_negara'];
		endforeach;

		$data['opsiGroup'] = $opsiGroup;
		$data['groupSelected'] = $groupSelected;
		$data['opsiKategori'] = $opsiKategori;
		$data['kategoriSelected'] = $kategoriSelected;
		$data['opsiSatuan'] = $opsiSatuan;
		$data['opsiRms'] = $opsirms;
		$data['opsiItg'] = $opsiitg;
		$data['rmsSelected'] = $rmsSelected;
		$data['opsiNegara'] = $opsiNegara;
		$data['satuanSelected'] = $satuanSelected;

		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->tm_rawmaterial->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->db->trans_begin();
		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrm_nama', 'Nama Material', 'required|min_length[4]', ['required' => '{field} tidak boleh kosong!', 'min_length' => '{field} minimum 4 karakter']);	
		// $this->form_validation->set_rules('txtrm_nama', 'Nama Material', 'required|min_length[4]', ['required' => '{field} tidak boleh kosong!', 'min_length' => '{field} minimum 4 karakter']);	
		$this->form_validation->set_rules('txtrm_group', 'Group Material', 'required', ['required' => '{field} tidak boleh kosong!']);	
		$this->form_validation->set_rules('txtrm_kategori', 'Kategori Material', 'required', ['required' => '{field} tidak boleh kosong!']);	
		$this->form_validation->set_rules('txtrm_satuan', 'Satuan Material', 'required', ['required' => '{field} tidak boleh kosong!']);	
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrRm_nama' => (!empty(form_error('txtrm_nama')))?buildLabel('warning', form_error('txtrm_nama', '"', '"')):'',
				'idErrRm_group' => (!empty(form_error('txtrm_group')))?buildLabel('warning', form_error('txtrm_group', '"', '"')):'',
				'idErrRm_kategori' => (!empty(form_error('txtrm_kategori')))?buildLabel('warning', form_error('txtrm_kategori', '"', '"')):'',
				'idErrRm_satuan' => (!empty(form_error('txtrm_satuan')))?buildLabel('warning', form_error('txtrm_satuan', '"', '"')):'',
			);
			
		}else {
			// $rm_kd = $this->input->post('txtrm_kd');
			$rm_nama = $this->input->post('txtrm_nama');
			$rm_deskripsi = $this->input->post('txtrm_deskripsi'); 
			$rm_spesifikasi = $this->input->post('txtrm_spesifikasi');
			$rmsatuan_kd = $this->input->post('txtrm_satuan');
			$rmgroup_kd = $this->input->post('txtrm_group');
			$rmkategori_kd = $this->input->post('txtrm_kategori');
			$rm_stock_min = $this->input->post('txtrm_stock_min');
			$rm_stock_max = $this->input->post('txtrm_stock_max');
			$rm_stock_safety = $this->input->post('txtrm_stock_safety');
			$rm_rms = $this->input->post('txtrm_rms');
			$rm_merk = $this->input->post('txtrm_merk');
			$rm_alias = $this->input->post('txtrm_alias');
			$rm_platepanjang = $this->input->post('txtrm_platepanjang');
			$rm_platelebar = $this->input->post('txtrm_platelebar');
			$rm_platetebal = $this->input->post('txtrm_platetebal');
			$rm_platemassajenis = $this->input->post('txtrm_platemassajenis');
			$rm_oldkd = $this->input->post('txtrm_oldkd');
			$rm_filterbom = $this->input->post('txtrm_filterbom');
			$inventory_item = $this->input->post('txtrm_inventoryitem');
			$flag_active = $this->input->post('txt_flag_active');
			$rm_jenisperolehan = $this->input->post('txtrm_jenisperolehan');
			$negara_id = $this->input->post('txtnegara_id');
			$rm_hs_code = $this->input->post('txtrm_hs_code');
			$rmsatuansecondary_kd = $this->input->post('txtrmsatuansecondary_kd');
			$rm_konversiqty = $this->input->post('txtrm_konversiqty');
			$item_group = $this->input->post('txtrm_itg');

			$rm_kd = $this->tm_rawmaterial->create_rm_kd(null);
			$rm_kode = '';
			$rmgroup_custom = $this->td_rawmaterial_group->customCode();
			if ($rmgroup_kd == $rmgroup_custom){
				$rm_kode = $this->tm_rawmaterial->create_code_custom($rm_nama);
			}else{
				$rm_kode = $this->tm_rawmaterial->create_code_new($rm_nama);
			}
			
			$data = array(
				'rm_kd' => $rm_kd,
				'rm_kode' => $rm_kode,
				'rm_nama' => !empty($rm_nama) ? $rm_nama :null,
				'rm_deskripsi' => !empty($rm_deskripsi) ? $rm_deskripsi :null,
				'rm_spesifikasi' => !empty($rm_spesifikasi) ? $rm_spesifikasi :null,
				'rmsatuan_kd' => !empty($rmsatuan_kd) ? $rmsatuan_kd :null,
				'rmsatuansecondary_kd' => !empty($rmsatuansecondary_kd) ? $rmsatuansecondary_kd :null,
				'rm_konversiqty' => !empty($rm_konversiqty) ? $rm_konversiqty : 1,
				'rmgroup_kd' => !empty($rmgroup_kd) ? $rmgroup_kd :null,
				'rmgroupsup_kd' => !empty($rm_rms) ? $rm_rms :null,
				'rmkategori_kd' => !empty($rmkategori_kd) ? $rmkategori_kd :null,
				'rm_stock_min' => !empty($rm_stock_min) ? $rm_stock_min :null,
				'rm_stock_max' => !empty($rm_stock_max) ? $rm_stock_max :null,
				'rm_stock_safety' => !empty($rm_stock_safety) ? $rm_stock_safety :null,
				'rm_platepanjang' => !empty($rm_platepanjang) ? $rm_platepanjang :null,
				'rm_platelebar' => !empty($rm_platelebar) ? $rm_platelebar :null,
				'rm_platetebal' => !empty($rm_platetebal) ? $rm_platetebal :null,
				'rm_platemassajenis' => !empty($rm_platemassajenis) ? $rm_platemassajenis :null,
				'rm_merk' => !empty($rm_merk) ? $rm_merk :null,
				'rm_alias' => !empty($rm_alias) ? $rm_alias :null,
				'rm_filterbom' => $rm_filterbom,
				'rm_jenisperolehan' => $rm_jenisperolehan,
				'negara_id' => !empty($negara_id) ? $negara_id : null,
				'rm_hs_code' => !empty($rm_hs_code) ? $rm_hs_code : null,
				'rm_tglinput' => date('Y-m-d H:i:s'),
				'rm_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
				'rm_oldkd' => !empty($rm_oldkd) ? $rm_oldkd :null,
				'itemgroup_kd' => !empty($item_group) ? $item_group :null,
				'inventory_item' => !empty($inventory_item) ? $inventory_item :null,
				'flag_active' => $flag_active
			);
			$act = $this->tm_rawmaterial->insert_data($data);
			/** Insert RM baru ke tabel RM stock */
			$rowRMstok = $this->td_rawmaterial_stok_master->get_by_param('rm_kd', $rm_kd)->row_array();
			/** Cek rm_kd null */
			if( empty($rowRMstok['rm_kd']) ){
				$code = $this->td_rawmaterial_stok_master->create_code(null);
				$dataStokMaster = [
					'rmstokmaster_kd' => $code,
					'rm_kd' => $rm_kd,
					'kd_gudang' => 'MGD260719003', /** kode gudang RM catelindo */
					'rmstokmaster_qty' => 0,
					'rmstokmaster_tglinput' => now_with_ms(),
					'admin_kd' => $this->session->userdata('kd_admin'),
				];
				$actInserStokMaster = $this->td_rawmaterial_stok_master->insert_data($dataStokMaster);
			}
			/** End insert RM baru ke tabel RM stock */
			if ($this->db->trans_status() !== FALSE){
				$api = $this->push_to_sap($rm_kd, "add");
				if($api[0]->ErrorCode == 0){
					$this->db->trans_commit();
					$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan, API '.$api[0]->Message);
				}else{
					$this->db->trans_rollback();
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan, API '.$api[0]->Message);
				}
			}else{
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		
		$this->db->trans_begin();
		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrm_nama', 'Nama Material', 'required|min_length[4]', ['required' => '{field} tidak boleh kosong!', 'min_length' => '{field} minimum 4 karakter']);	
		$this->form_validation->set_rules('txtrm_group', 'Group Material', 'required', ['required' => '{field} tidak boleh kosong!']);	
		$this->form_validation->set_rules('txtrm_kategori', 'Kategori Material', 'required', ['required' => '{field} tidak boleh kosong!']);	
		$this->form_validation->set_rules('txtrm_satuan', 'Satuan Material', 'required', ['required' => '{field} tidak boleh kosong!']);	
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrRm_nama' => (!empty(form_error('txtrm_nama')))?buildLabel('warning', form_error('txtrm_nama', '"', '"')):'',
				'idErrRm_group' => (!empty(form_error('txtrm_group')))?buildLabel('warning', form_error('txtrm_group', '"', '"')):'',
				'idErrRm_kategori' => (!empty(form_error('txtrm_kategori')))?buildLabel('warning', form_error('txtrm_kategori', '"', '"')):'',
				'idErrRm_satuan' => (!empty(form_error('txtrm_satuan')))?buildLabel('warning', form_error('txtrm_satuan', '"', '"')):'',
			);
			
		}else {
			$rm_kd = $this->input->post('txtrm_kd');
			$rm_nama = $this->input->post('txtrm_nama');
			$rm_deskripsi = $this->input->post('txtrm_deskripsi'); 
			$rm_spesifikasi = $this->input->post('txtrm_spesifikasi');
			$rmsatuan_kd = $this->input->post('txtrm_satuan');
			$rmgroup_kd = $this->input->post('txtrm_group');
			$rmkategori_kd = $this->input->post('txtrm_kategori');
			$rm_stock_min = $this->input->post('txtrm_stock_min');
			$rm_stock_max = $this->input->post('txtrm_stock_max');
			$rm_stock_safety = $this->input->post('txtrm_stock_safety');
			$rm_merk = $this->input->post('txtrm_merk');
			$rm_alias = $this->input->post('txtrm_alias');
			$rm_platepanjang = $this->input->post('txtrm_platepanjang');
			$rm_platelebar = $this->input->post('txtrm_platelebar');
			$rm_platetebal = $this->input->post('txtrm_platetebal');
			$rm_platemassajenis = $this->input->post('txtrm_platemassajenis');
			$rm_oldkd = $this->input->post('txtrm_oldkd');
			$rm_kode = $this->input->post('txtrm_kode');
			$rm_filterbom = $this->input->post('txtrm_filterbom');
			$inventory_item = $this->input->post('txtrm_inventoryitem');
			$flag_active = $this->input->post('txt_flag_active');
			$rm_jenisperolehan = $this->input->post('txtrm_jenisperolehan');
			$negara_id = $this->input->post('txtnegara_id');
			$rm_hs_code = $this->input->post('txtrm_hs_code');
			$rmsatuansecondary_kd = $this->input->post('txtrmsatuansecondary_kd');
			$rm_konversiqty = $this->input->post('txtrm_konversiqty');
			$rm_rms = $this->input->post('txtrm_rms');
			$item_group = $this->input->post('txtrm_itg');

			$data = array(
				'rm_kode' => !empty($rm_kode) ? $rm_kode :null,
				'rm_nama' => !empty($rm_nama) ? $rm_nama :null,
				'rm_deskripsi' => !empty($rm_deskripsi) ? $rm_deskripsi :null,
				'rm_spesifikasi' => !empty($rm_spesifikasi) ? $rm_spesifikasi :null,
				'rmsatuan_kd' => !empty($rmsatuan_kd) ? $rmsatuan_kd :null,
				'rmsatuansecondary_kd' => !empty($rmsatuansecondary_kd) ? $rmsatuansecondary_kd :null,
				'rm_konversiqty' => !empty($rm_konversiqty) ? $rm_konversiqty : 1,
				'rmgroup_kd' => !empty($rmgroup_kd) ? $rmgroup_kd :null,
				'rmkategori_kd' => !empty($rmkategori_kd) ? $rmkategori_kd :null,
				'rm_stock_min' => !empty($rm_stock_min) ? $rm_stock_min :null,
				'rm_stock_max' => !empty($rm_stock_max) ? $rm_stock_max :null,
				'rm_stock_safety' => !empty($rm_stock_safety) ? $rm_stock_safety :null,
				'rm_platepanjang' => !empty($rm_platepanjang) ? $rm_platepanjang :null,
				'rm_platelebar' => !empty($rm_platelebar) ? $rm_platelebar :null,
				'rm_platetebal' => !empty($rm_platetebal) ? $rm_platetebal :null,
				'rm_platemassajenis' => !empty($rm_platemassajenis) ? $rm_platemassajenis :null,
				'rm_merk' => !empty($rm_merk) ? $rm_merk :null,
				'rm_alias' => !empty($rm_alias) ? $rm_alias :null,
				'rm_filterbom' => $rm_filterbom,
				'rm_jenisperolehan' => $rm_jenisperolehan,
				'negara_id' => !empty($negara_id) ? $negara_id : null,
				'rm_hs_code' => !empty($rm_hs_code) ? $rm_hs_code : null,
				'rm_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
				'rm_oldkd' => !empty($rm_oldkd) ? $rm_oldkd :null,
				'rmgroupsup_kd' => !empty($rm_rms) ? $rm_rms :null,
				'itemgroup_kd' => !empty($item_group) ? $item_group :null,
				'inventory_item' => !empty($inventory_item) ? $inventory_item :null,
				'flag_active' => $flag_active
			);
			
			$act = $this->tm_rawmaterial->update_data(array('rm_kd' => $rm_kd), $data);

			if ($this->db->trans_status() !== FALSE){
				$api = $this->push_to_sap($rm_kd, "edit");
				if($api[0]->ErrorCode == 0){
					$this->db->trans_commit();
					$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate, API '.$api[0]->Message);
				}else{
					$this->db->trans_rollback();
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Update, API '.$api[0]->Message);
				}
			}else{
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Update');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$rmsatuan_kd = $this->input->get('id', TRUE);
		
		$actDel = $this->tm_rawmaterial->delete_data($rmsatuan_kd);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}

	/** Modul edit rawmaterial by item_group
	 * 
	 */
	public function form_edit_minstock_box()
	{
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['item_group'] = $this->tm_item_group->get_all()->result_array();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_minstock_byitemgroup_box', $data);
	}

	public function form_edit_minstock_main()
	{
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$kd_item_group = $this->input->get('kd_item_group'); 
		$data['material_data'] = $this->tm_rawmaterial->get_by_param(['itemgroup_kd' => $kd_item_group])->result_array();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_minstock_byitemgroup_main', $data);
	}

	public function action_edit_minstock()
	{
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data = $this->input->post('data');
		$this->db->trans_begin();
		foreach($data as $key => $value){
			$act = $this->tm_rawmaterial->update_data(['rm_kd' => $key], ['rm_stock_min' => $value]);
		}
		if ($this->db->trans_status() !== FALSE){
			$this->db->trans_commit();
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data min. stok berhasil diubah');
		}else{
			$this->db->trans_rollback();
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}


	public function push_to_sap($kd, $act)
	{
		$data = $this->tm_rawmaterial->get_row($kd);
		$dataAPI = [
			'ItemCode' => $data->rm_kode,
			'ItemName' => $data->rm_deskripsi." ".$data->rm_spesifikasi,
			'PurchaseItem' => "Y",
			'SalesItem' => "N",
			'InventoryItem' => $data->inventory_item,
			'KdItemGroup' => $data->itemgroup_kd,
			'Dimensi' => 0,
			'NetWeight' => 0,
			'GrossWeight' => 0,
			'BoxWeight' => 0,
			'Length' => 0,
			'Width' => 0,
			'Height' => 0,
			'Volume' => 0,
			'PrchseItem' => "Y",
			'SellItem' => "N",
			'InvntItem' => $data->inventory_item,
			'StockSafety' => $data->rm_stock_safety != null || $data->rm_stock_safety != "" ? $data->rm_stock_safety : 0,
			'U_IDU_WEBID' => $data->rm_kd
		];
		if($act == "add"){
			$api = parent::api_sap_post('AddItem', $dataAPI);
		}else{
			$api = parent::api_sap_post('EditItem', $dataAPI);
		}
		return $api;
	}
	
}
