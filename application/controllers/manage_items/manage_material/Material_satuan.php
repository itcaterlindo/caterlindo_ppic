<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Material_satuan extends MY_Controller {
	private $class_link = 'manage_items/manage_material/material_satuan';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_rawmaterial_satuan']);
	}

	public function index() {
		parent::administrator();
		parent::pnotify_assets();
		$this->table_box();
	}

	public function form_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		if ($sts == 'edit'){
			$act = $this->td_rawmaterial_satuan->get_by_param (array('rmsatuan_kd'=> $id))->row_array();			
			$data['id'] = $act['rmsatuan_kd'];
			$data['rmsatuan_nama'] = $act['rmsatuan_nama'];
			$data['rmsatuan_ket'] = $act['rmsatuan_ket'];
		}

		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->td_rawmaterial_satuan->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrmsatuan_nama', 'Nama', 'required', ['required' => '{field} tidak boleh kosong!']);	
		$this->form_validation->set_rules('txtrmsatuan_ket', 'Keterangan', 'required', ['required' => '{field} tidak boleh kosong!']);	

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrRmsatuan_nama' => (!empty(form_error('txtrmsatuan_nama')))?buildLabel('warning', form_error('txtrmsatuan_nama', '"', '"')):'',
				'idErrRmsatuan_ket' => (!empty(form_error('txtrmsatuan_ket')))?buildLabel('warning', form_error('txtrmsatuan_ket', '"', '"')):'',
			);
			
		}else {
			$rmsatuan_nama = $this->input->post('txtrmsatuan_nama');
			$rmsatuan_ket = $this->input->post('txtrmsatuan_ket');
			
			$data = array(
				'rmsatuan_kd' => $this->td_rawmaterial_satuan->create_code(),
				'rmsatuan_nama' => strtoupper($rmsatuan_nama),
				'rmsatuan_ket' => $rmsatuan_ket,
				'rmsatuan_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->td_rawmaterial_satuan->insert_data($data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrmsatuan_nama', 'Tipe Admin', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtrmsatuan_ket', 'Keterangan', 'required', ['required' => '{field} tidak boleh kosong!']);	

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrRmsatuan_nama' => (!empty(form_error('txtrmsatuan_nama')))?buildLabel('warning', form_error('txtrmsatuan_nama', '"', '"')):'',
				'idErrRmsatuan_ket' => (!empty(form_error('txtrmsatuan_ket')))?buildLabel('warning', form_error('txtrmsatuan_ket', '"', '"')):'',
			);
			
		}else {
			$rmsatuan_kd = $this->input->post('txtrmsatuan_kd');
			$rmsatuan_nama = $this->input->post('txtrmsatuan_nama');
			$rmsatuan_ket = $this->input->post('txtrmsatuan_ket');
			
			$data = array(
				'rmsatuan_nama' => strtoupper($rmsatuan_nama),
				'rmsatuan_ket' => $rmsatuan_ket,
				'rmsatuan_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->td_rawmaterial_satuan->update_data (array('rmsatuan_kd' => $rmsatuan_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$rmsatuan_kd = $this->input->get('id', TRUE);
		
		$actDel = $this->td_rawmaterial_satuan->delete_data($rmsatuan_kd);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
