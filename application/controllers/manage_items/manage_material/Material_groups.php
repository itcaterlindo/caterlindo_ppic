<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Material_groups extends MY_Controller {
	private $class_link = 'manage_items/manage_material/material_groups';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_rawmaterial_group']);
	}

	public function index() {
		parent::administrator();
		parent::pnotify_assets();
		$this->table_box();
	}

	public function form_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		if ($sts == 'edit'){
			$actFGuser = $this->td_rawmaterial_group->get_by_param (array('rmgroup_kd'=> $id))->row_array();			
			$data['id'] = $actFGuser['rmgroup_kd'];
			$data['rmgroup_nama'] = $actFGuser['rmgroup_nama'];
		}

		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->td_rawmaterial_group->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrmgroup_nama', 'Tipe Admin', 'required', ['required' => '{field} tidak boleh kosong!']);	

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrRmgroup_nama' => (!empty(form_error('txtrmgroup_nama')))?buildLabel('warning', form_error('txtrmgroup_nama', '"', '"')):'',
			);
			
		}else {
			$rmgroup_nama = $this->input->post('txtrmgroup_nama');
			
			$data = array(
				'rmgroup_kd' => $this->td_rawmaterial_group->create_code(),
				'rmgroup_nama' => strtoupper($rmgroup_nama),
				'rmgroup_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->td_rawmaterial_group->insert_data($data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrmgroup_nama', 'Tipe Admin', 'required', ['required' => '{field} tidak boleh kosong!']);	

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrRmgroup_nama' => (!empty(form_error('txtrmgroup_nama')))?buildLabel('warning', form_error('txtrmgroup_nama', '"', '"')):'',
			);
			
		}else {
			$rmgroup_kd = $this->input->post('txtrmgroup_kd');
			$rmgroup_nama = $this->input->post('txtrmgroup_nama');
			
			$data = array(
				'rmgroup_nama' => strtoupper($rmgroup_nama),
				'rmgroup_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->td_rawmaterial_group->update_data (array('rmgroup_kd' => $rmgroup_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$rmgroup_kd = $this->input->get('id', TRUE);
		
		$actDel = $this->td_rawmaterial_group->delete_data($rmgroup_kd);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
