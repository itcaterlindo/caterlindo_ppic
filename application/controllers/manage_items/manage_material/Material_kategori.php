<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Material_kategori extends MY_Controller {
	private $class_link = 'manage_items/manage_material/material_kategori';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_rawmaterial_kategori']);
	}

	public function index() {
		parent::administrator();
		parent::pnotify_assets();
		$this->table_box();
	}

	public function form_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		if ($sts == 'edit'){
			$actFGuser = $this->td_rawmaterial_kategori->get_by_param (array('rmkategori_kd'=> $id))->row_array();			
			$data['id'] = $actFGuser['rmkategori_kd'];
			$data['rmkategori_nama'] = $actFGuser['rmkategori_nama'];
		}

		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->td_rawmaterial_kategori->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrmkategori_kd', 'Kode Admin', 'required', ['required' => '{field} tidak boleh kosong!']);			
		$this->form_validation->set_rules('txtrmkategori_nama', 'Tipe Admin', 'required', ['required' => '{field} tidak boleh kosong!']);	

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrRmkategori_kd' => (!empty(form_error('txtrmkategori_kd')))?buildLabel('warning', form_error('txtrmkategori_kd', '"', '"')):'',
				'idErrRmkategori_nama' => (!empty(form_error('txtrmkategori_nama')))?buildLabel('warning', form_error('txtrmkategori_nama', '"', '"')):'',
			);
			
		}else {
			$rmkategori_kd = $this->input->post('txtrmkategori_kd');
			$rmkategori_nama = $this->input->post('txtrmkategori_nama');
			
			$data = array(
				'rmkategori_kd' => $rmkategori_kd,
				'rmkategori_nama' => strtoupper($rmkategori_nama),
				'rmkategori_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			/** Cek Id */
			$actId = $this->td_rawmaterial_kategori->get_by_param(array('rmkategori_kd' => $rmkategori_kd));
			if ($actId->num_rows() == 0){
				$act = $this->td_rawmaterial_kategori->insert_data($data);
				if ($act){
					$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
				}else{
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
				}
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Kode Sudah Digunakan');
			}

		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrmkategori_nama', 'Tipe Admin', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrRmkategori_nama' => (!empty(form_error('txtrmkategori_nama')))?buildLabel('warning', form_error('txtrmkategori_nama', '"', '"')):'',
			);
			
		}else {
			$rmkategori_kd = $this->input->post('txtrmkategori_kd');
			$rmkategori_nama = $this->input->post('txtrmkategori_nama');
			
			$data = array(
				'rmkategori_nama' => strtoupper($rmkategori_nama),
				'rmkategori_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->td_rawmaterial_kategori->update_data (array('rmkategori_kd' => $rmkategori_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$rmkategori_kd = $this->input->get('id', TRUE);
		
		$actDel = $this->td_rawmaterial_kategori->delete_data($rmkategori_kd);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
