<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/third_party/codeigniter-restserver/src/RestController.php';
require APPPATH . '/third_party/codeigniter-restserver/src/Format.php';
use chriskacerguis\RestServer\RestController;

class Rawmaterial extends RestController {

    function __construct() {
        parent::__construct();
        $this->load->model(['tm_rawmaterial']);
    }

    public function index_post() {
        try{
            $param = $this->input->get('param');
            if( !empty($param) ){
                $master = $this->tm_rawmaterial->get_row($param);
            }else{
                $master = $this->tm_rawmaterial->get_all()->result_array();
            }
            $data = [
                'status' => 200,
                'message' => 'OK',
                'data' => $master
            ];
            $this->response($data, $data['status']);
        }catch(\Throwable $e){
            $data = [
                'status' =>  $e->getCode(),
                'message' => $e->getMessage(),
            ];
            $this->response($data, $data['status']);
        }
     
    }

    public function search_post() {
        try{
            $param = $this->input->get('param');
            if( $param == null ){
                $master = $this->db->from('tm_rawmaterial')
                        ->get()->result_array();
            }else{
                $master = $this->db->from('tm_rawmaterial')
                        ->like('rm_kode', $param)
                        ->or_like('rm_deskripsi', $param)
                        ->get()->result_array();
            }
            $data = [
                'status' => 200,
                'message' => $param,
                'data' => $master
            ];
            $this->response($data, $data['status']);
        }catch(\Throwable $e){
            $data = [
                'status' =>  $e->getCode(),
                'message' => $e->getMessage(),
            ];
            $this->response($data, $data['status']);
        }
     
    }

    public function select2_get() {
        try{
            // /** Get raw body json */
            // $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
            // $request = json_decode($stream_clean);
            // $param = $request->searchTerm;
            $param = $this->input->get('searchTerm', TRUE);

            if( $param == null ){
                $master = $this->db->from('tm_rawmaterial')
                        ->get()->result_array();
            }else{
                $master = $this->db->from('tm_rawmaterial')
                        ->like('rm_kode', $param)
                        ->or_like('rm_deskripsi', $param)
                        ->get()->result_array();
            }
            $masterData = [];
            foreach($master as $key => $val){
                $masterData[] = [
                    'id' => $val['rm_kd'],
                    'text' => $val['rm_kode']." - ".$val['rm_deskripsi']
                ];
            }
            $data = [
                'status' => 200,
                'message' => 'OK',
                'data' => $masterData
            ];
            $this->response($data, $data['status']);
        }catch(\Throwable $e){
            $data = [
                'status' =>  $e->getCode(),
                'message' => $e->getMessage(),
            ];
            $this->response($data, $data['status']);
        }
     
    }

}

?>