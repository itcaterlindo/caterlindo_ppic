<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Inspection_rawmaterial extends MY_Controller {
	private $class_link = 'quality_control/inspection_rawmaterial';

	public function __construct() {
		try{
			parent::__construct();
			$this->load->library(array('ssp'));
			$this->load->model(['tm_inspection_rawmaterial', 'tb_bagian', 'tb_inspectionrawmaterial_log', 'td_delivery_material', 'td_relasi_deliverymaterialinspectionrawmaterial', 'tb_inspectionrawmaterial_state']);
			$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		}catch(\Exception $e){
			echo $e->getMessage();
		}
    }

    public function index() {	
		parent::administrator();
		parent::select2_assets();
		parent::datetimepicker_assets();
		parent::pnotify_assets();
		$this->table_box();
    }

	public function form_box() {
		$data['id'] = $this->input->get('id');
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		try{
			$id = $this->input->get('id');
			$data['inspection'] = $this->tm_inspection_rawmaterial->get_by_param(['kd_inspectionrawmaterial' => $id])->row_array();
			$data['bagian'] = $this->tb_bagian->get_by_param(['bagian_lokasi' => 'WIP'])->result_array();
			$data['class_link'] = $this->class_link;
			$this->load->view('page/'.$this->class_link.'/form_main', $data);
		}catch(\Throwable $e){
			echo $e->getMessage();
		}
	}

	public function table_box(){
		$data['state'] = $this->tb_inspectionrawmaterial_state->get_all()->result_array();
		$data['class_link'] = $this->class_link;
		$data['js_file'] = 'table_js'; 
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		try{
			$data = $this->tm_inspection_rawmaterial->ssp_table2();
			echo json_encode($data);
		}catch(\Throwable $e){
			echo $e->getMessage();
		}
	}

	public function table_data_bagian() {
		try{
			$data = $this->tm_inspection_rawmaterial->ssp_bagian_table2();
			echo json_encode($data);
		}catch(\Throwable $e){
			echo $e->getMessage();
		}
	}

	public function table_data_penyebab() {

		$data = $this->tm_inspection_rawmaterial->ssp_penyebab_table2();
		echo json_encode($data);
	}

	public function action_submit() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		try{
			$id = $this->input->post('txtKd');
			$this->load->library(['form_validation']); 
			$this->form_validation->set_rules('txtTglInspection', 'Tanggal Inspection', 'required', ['required' => '{field} tidak boleh kosong!']);
			$this->form_validation->set_rules('txtNoDrm', 'No. Delivery Material', 'required', ['required' => '{field} tidak boleh kosong!']);
			$this->form_validation->set_rules('txtRmKode', 'Rm Kode', 'required', ['required' => '{field} tidak boleh kosong!']);
			$this->form_validation->set_rules('txtQty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
			$this->form_validation->set_rules('txtQtyReject', 'Qty Reject', 'required', ['required' => '{field} tidak boleh kosong!']);
			$this->form_validation->set_rules('check_qty', 'Check qty', 'callback_check_qty');
			$this->form_validation->set_rules('check_qty_reject', 'Check qty reject', 'callback_check_qty_reject');

			if ($this->form_validation->run() == FALSE) {
				$resp['code'] = 401;
				$resp['status'] = 'Required';
				$resp['pesan'] = array(
					'idErrTglInspection' => (!empty(form_error('txtTglInspection')))?buildLabel('warning', form_error('txtTglInspection', '"', '"')):'',
					'idErrNoDrm' => (!empty(form_error('txtNoDrm')))?buildLabel('warning', form_error('txtNoDrm', '"', '"')):'',
					'idErrRmKode' => (!empty(form_error('txtRmKode')))?buildLabel('warning', form_error('txtRmKode', '"', '"')):'',
					'idErrQty' => (!empty(form_error('txtQty')))?buildLabel('warning', form_error('txtQty', '"', '"')):'',
					'idErrQtyReject' => (!empty(form_error('txtQtyReject')))?buildLabel('warning', form_error('txtQtyReject', '"', '"')):'',
					'idErrCheckQty' => (!empty(form_error('check_qty')))?buildLabel('warning', form_error('check_qty', '"', '"')):'',
					'idErrCheckQtyReject' => (!empty(form_error('check_qty_reject')))?buildLabel('warning', form_error('check_qty_reject', '"', '"')):'',

				);
			}else {
					$this->db->trans_begin();
					if( empty($id) ){
						/** Action insert */
						$tgl = $this->input->post('txtTglInspection');
						$data['kd_inspectionrawmaterial'] = $this->tm_inspection_rawmaterial->create_code();
						$data['inspectionrawmaterial_no'] = $this->tm_inspection_rawmaterial->create_no($tgl);
						$data['deliverymaterial_kd'] = $this->input->post('txtNoDrm');
						$data['inspectionrawmaterial_rm_kode'] = $this->input->post('txtRmKode');
						$data['inspectionrawmaterial_bagian_kd'] = empty($this->input->post('txtBagianKd')) ? NULL : $this->input->post('txtBagianKd');
						$data['inspectionrawmaterial_tanggal'] = format_date($tgl, 'Y-m-d');
						$data['inspectionrawmaterial_tindakan'] = empty($this->input->post('txtTindakan')) ? NULL : $this->input->post('txtTindakan');
						$data['inspectionrawmaterial_qty'] = $this->input->post('txtQty');
						$data['inspectionrawmaterial_qty_reject'] = $this->input->post('txtQtyReject');
						$data['inspectionrawmaterial_penyebab_bagian_kd'] = empty($this->input->post('txtPenyebabBagian')) ? NULL : $this->input->post('txtPenyebabBagian');
						$data['inspectionrawmaterial_penyebab'] = $this->input->post('txtPenyebab');
						$data['inspectionrawmaterial_keterangan'] = $this->input->post('txtKeterangan');
						$data['inspectionrawmaterialstate_kd'] = 1;
						$data['inspectionrawmaterial_tglinput'] = date('Y-m-d H:i:s');
						$data['inspectionrawmaterial_tgledit'] = date('Y-m-d H:i:s');
						$data['admin_kd'] = $this->session->userdata('kd_admin');
						$act = $this->tm_inspection_rawmaterial->create($data);

						/** Insert LOG state */
						$dataLog = [
							'inspectionrawmateriallog_kd' => $this->tb_inspectionrawmaterial_log->create_code(),
							'inspectionrawmaterial_kd' => $data['kd_inspectionrawmaterial'],
							'inspectionrawmateriallogstate_kd' => $data['inspectionrawmaterialstate_kd'],
							'inspectionrawmateriallog_keterangan' => null,
							'inspectionrawmateriallog_tglinput' => date('Y-m-d H:i:s'),
							'admin_kd' => $data['admin_kd']
						]; 
						$insertLog = $this->tb_inspectionrawmaterial_log->insert_data($dataLog);

						if ($this->db->trans_status() === FALSE) {
							$this->db->trans_rollback();
							$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
						}else{
							$this->db->trans_commit();
							$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
						};
					}else{
						/** Action edit */
						$data['deliverymaterial_kd'] = $this->input->post('txtNoDrm');
						$data['inspectionrawmaterial_rm_kode'] = $this->input->post('txtRmKode');
						$data['inspectionrawmaterial_bagian_kd'] = empty($this->input->post('txtBagianKd')) ? NULL : $this->input->post('txtBagianKd');
						$data['inspectionrawmaterial_tanggal'] = format_date($this->input->post('txtTglInspection'), 'Y-m-d');
						$data['inspectionrawmaterial_tindakan'] = empty($this->input->post('txtTindakan')) ? NULL : $this->input->post('txtTindakan');
						$data['inspectionrawmaterial_qty'] = $this->input->post('txtQty');
						$data['inspectionrawmaterial_qty_reject'] = $this->input->post('txtQtyReject');
						$data['inspectionrawmaterial_penyebab_bagian_kd'] = empty($this->input->post('txtPenyebabBagian')) ? NULL : $this->input->post('txtPenyebabBagian');
						$data['inspectionrawmaterial_penyebab'] = $this->input->post('txtPenyebab');
						$data['inspectionrawmaterial_keterangan'] = $this->input->post('txtKeterangan');
						$data['inspectionrawmaterial_tgledit'] = date('Y-m-d H:i:s');
						$data['admin_kd'] = $this->session->userdata('kd_admin');
						$act = $this->tm_inspection_rawmaterial->update($data, $id);
						if ($this->db->trans_status() === FALSE) {
							$this->db->trans_rollback();
							$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
						}else{
							$this->db->trans_commit();
							$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
						};
					}
				$resp['csrf'] = $this->security->get_csrf_hash();
			}
			echo json_encode($resp);
		}catch(\Throwable $e){
			echo $e->getMessage();
		}
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->db->trans_begin();
		$id = $this->input->get('id', TRUE);
		if (!empty($id)){
			$actDel = $this->tm_inspection_rawmaterial->delete_data($id);
			$delete_log = $this->tb_inspectionrawmaterial_log->delete_data_by_param(['inspectionrawmaterial_kd' => $id]);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
			}else{
				$this->db->trans_commit();
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
			}
		}
		else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Tidak Sesuai');
		}
		echo json_encode($resp);
	}

	public function action_state()
	{
		$this->db->trans_begin();
		$id = $this->input->get('id', TRUE);
		$state = $this->input->get('state', TRUE);
		if (!empty($id)){
			/** 4 adalah state close */
			if($state == 4){
				$act = $this->tm_inspection_rawmaterial->update(['flag_close' => '1'], $id);
			}
			$act = $this->tm_inspection_rawmaterial->update(['inspectionrawmaterialstate_kd' => $state], $id);
			/** Insert LOG state */
			$dataLog = [
				'inspectionrawmateriallog_kd' => $this->tb_inspectionrawmaterial_log->create_code(),
				'inspectionrawmaterial_kd' => $id,
				'inspectionrawmateriallogstate_kd' => $state,
				'inspectionrawmateriallog_keterangan' => null,
				'inspectionrawmateriallog_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin')
			]; 
			$insertLog = $this->tb_inspectionrawmaterial_log->insert_data($dataLog);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal proses');
			}else{			
					$this->db->trans_commit();
					$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data berhasil di proses');
			}
		}
		else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Tidak Sesuai');
		}
		echo json_encode($resp);
	}

	public function action_approved_bagian()
	{
		$this->db->trans_begin();
		$id = $this->input->get('id', TRUE);
		if (!empty($id)){
				$dataPenyebab = [
					'flag_approved_bagian' => '1', 
					'approved_bagian_admin_kd' => $this->session->userdata('kd_admin'),
					'approved_bagian_tgl' => date('Y-m-d H:i:s')
				];
				$act = $this->tm_inspection_rawmaterial->update($dataPenyebab, $id);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal approved bagian');
			}else{
				$this->db->trans_commit();
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data berhasil di approved');
			}
		}
		else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Tidak Sesuai');
		}
		echo json_encode($resp);
	}

	public function action_approved_penyebab()
	{
		$this->db->trans_begin();
		$id = $this->input->get('id', TRUE);
		if (!empty($id)){
				$dataPenyebab = [
					'flag_approved_penyebab' => '1', 
					'approved_penyebab_admin_kd' => $this->session->userdata('kd_admin'),
					'approved_penyebab_tgl' => date('Y-m-d H:i:s')
				];
				$act = $this->tm_inspection_rawmaterial->update($dataPenyebab, $id);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal approved penyebab');
			}else{
				$this->db->trans_commit();
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data berhasil di approved');
			}
		}
		else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Tidak Sesuai');
		}
		echo json_encode($resp);
	}

	public function get_drm() {
		$paramDRM = $this->input->get('paramDRM');
		$resultData = [];
		/** GROUP BY berdasarkan tm_inspectionrawmaterial deliverymaterial_kd untuk mengambil summary dari qty inspection untuk parsial inspection */
		$query = $this->db->select('td_delivery_material.*, tm_rawmaterial.rm_kode, tm_rawmaterial.rm_kd, tm_rawmaterial.rm_deskripsi, tm_rawmaterial.rm_spesifikasi, COALESCE(SUM(tm_inspection_rawmaterial.inspectionrawmaterial_qty),0) AS sum_inspectionrawmaterial_qty')
			->from('td_delivery_material')
			->join('tm_inspection_rawmaterial', 'tm_inspection_rawmaterial.deliverymaterial_kd = td_delivery_material.deliverymaterial_kd', 'left')
			->join('tm_rawmaterial', 'tm_rawmaterial.rm_kd = td_delivery_material.rm_kd', 'left')
			->join('td_rawmaterial_satuan', 'td_rawmaterial_satuan.rmsatuan_kd = td_delivery_material.deliverymaterial_satuan_kd', 'left')
			->where('td_delivery_material.ke_gudang', 'MGD061023001')
			->where('td_delivery_material.status', 'diterima')
			->like('td_delivery_material.deliverymaterial_no', $paramDRM, 'match')
			->like('tm_rawmaterial.rm_deskripsi', $paramDRM, 'match')
			->group_by('td_delivery_material.deliverymaterial_kd')
			->order_by('td_delivery_material.deliverymaterial_no', 'DESC')
			->get();
		if (!empty($query->num_rows())) {
			$result = $query->result_array();
			foreach ($result as $r) {
				/** Delivery material qty - total inspection rawmaterial qty */
				$qty = $r['deliverymaterial_qty'] - $r['sum_inspectionrawmaterial_qty'];
				if($qty > 0){
					$resultData[] = [
						'id' => $r['deliverymaterial_kd'], 
						'text' => $r['deliverymaterial_no']. " | ". $r['rm_deskripsi'],
						'rm_kd' => $r['rm_kd'],
						'rm_kode' => $r['rm_kode'],
						'rm_deskripsi' => $r['rm_deskripsi'],
						'rm_spesifikasi' => $r['rm_spesifikasi'],
						'deliverymaterial_qty' => $qty
					];
				}
			}
		}
	
		echo json_encode($resultData);
	}

	public function report_pdf()
	{
		$this->load->library('Pdf');
		try{
		$id = $this->input->get('id', true);
		$konten['master'] = $this->tm_inspection_rawmaterial->get_by_param(['kd_inspectionrawmaterial' => $id])->row_array();
		$konten['master_bagian'] = $this->tb_bagian->get_by_param(['bagian_lokasi' => 'WIP'])->result_array();
		/** Get name log untuk TTD di footer */
		$konten['footer_data'] = $this->tb_inspectionrawmaterial_log->get_byparam_detail(['inspectionrawmaterial_kd' => $id])->result_array();
		$data['konten'] = $this->load->view('page/'.$this->class_link.'/pdf_datamain', $konten, true);
		$this->load->view('page/'.$this->class_link.'/pdf_main', $data);
		}catch(\Throwable $t){
			echo $t->getMessage();
		}
	}

	public function pdf()
	{
		$this->load->view('page/'.$this->class_link.'/pdf_datamain');

	}

	/** MODUL KIRIM KE DELIVERY MATERIAL */
	public function kirim_deliverymaterial()
	{
		parent::administrator();
		parent::select2_assets();
		parent::datetimepicker_assets();
		parent::pnotify_assets();
		$this->tablekirim_box();
	}

	public function formkirim_box() {
		$data['id'] = $this->input->get('id');
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/formkirim_box', $data);
	}

	public function formkirim_main() {
		try{
			$id = $this->input->get('id');
			$data['inspection'] = $this->tm_inspection_rawmaterial->get_by_param(['kd_inspectionrawmaterial' => $id])->row_array();
			$data['bagian'] = $this->tb_bagian->get_by_param(['bagian_lokasi' => 'WIP'])->result_array();
			$data['class_link'] = $this->class_link;
			$this->load->view('page/'.$this->class_link.'/formkirim_main', $data);
		}catch(\Throwable $e){
			echo $e->getMessage();
		}
	}

	public function tablekirim_box(){
		$data['class_link'] = $this->class_link;
		$data['js_file'] = 'table_js'; 
		$this->load->view('page/'.$this->class_link.'/tablekirim_box', $data);
	}

	public function tablekirim_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/tablekirim_main', $data);
	}

	public function tablekirim_data() {
		try{
			$data = $this->tm_inspection_rawmaterial->ssp_kirim_table2();
			echo json_encode($data);
		}catch(\Throwable $e){
			echo $e->getMessage();
		}
	}

	public function action_kirim_delivery()
	{
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$delivery_kd = $this->input->get('delivery_kd', true);
		$inspectionrawmaterial_kd = $this->input->get('inspectionrawmaterial_kd', true);
		$param = $this->input->get('param', true);
		/** Get data from master */
		$drm = $this->td_delivery_material->get_by_param(['deliverymaterial_kd' => $delivery_kd])->row_array();
		$inspectionRawMaterial = $this->tm_inspection_rawmaterial->get_by_param(['kd_inspectionrawmaterial' => $inspectionrawmaterial_kd])->row_array();
		
		/** Cek jika yang dikirim barang baik atau reject */
		if($param == 'baik'){
			/** Baik */
			$qty = $inspectionRawMaterial['inspectionrawmaterial_qty'] - $inspectionRawMaterial['inspectionrawmaterial_qty_reject'];
			$param = 'baik';
		}else{
			/** Reject */
			$qty = $inspectionRawMaterial['inspectionrawmaterial_qty_reject'];
			$param = 'reject';
		}
		
		/** Kirim delivery material berdasarkan delivery material asal (dari gudang QC ke gudang RM) */
		$dataInsertDRM = [   
			'deliverymaterial_kd' => $this->td_delivery_material->create_code(),
			'deliverymaterial_no' => $this->td_delivery_material->create_no(date('Y-m-d')),
			'rm_kd' => $drm['rm_kd'],
			'deliverymaterial_satuan_kd' => $drm['deliverymaterial_satuan_kd'],
			'dari_gudang' => $drm['ke_gudang'],
			'ke_gudang' => $drm['dari_gudang'],
			'status' => 'terkirim',
			'inventory_item' => $drm['inventory_item'],
			'rmgr_code_srj' => $drm['rmgr_code_srj'],
			'deliverymaterial_qty' => $qty,
			'deliverymaterial_remark' => $inspectionRawMaterial['inspectionrawmaterial_no']." | ".ucwords($param),
			'deliverymaterial_tanggal' => date('Y-m-d'),
			'deliverymaterial_tglinput' => date('Y-m-d H:i:s'),
			'deliverymaterial_tgledit' => date('Y-m-d H:i:s'),
			'admin_kd' => $this->session->userdata('kd_admin'),
		];

		$dataInsertRelasDrmInspectionRM = [
			'relasideliverymaterialinspectionrawmaterial_kd' => $this->td_relasi_deliverymaterialinspectionrawmaterial->create_code(),
			'inspectionrawmaterial_kd' => $inspectionrawmaterial_kd,
			'deliverymaterial_kd' => $dataInsertDRM['deliverymaterial_kd'],
			'qty' => $qty,
			'keterangan' => $param,
			'relasideliverymaterialinspectionrawmaterial_tglinput' => date('Y-m-d H:i:s'),
			'admin_kd' => $this->session->userdata('kd_admin'),
		];
		
		$this->db->trans_begin();
		$this->td_delivery_material->insert_data($dataInsertDRM);
		$this->td_relasi_deliverymaterialinspectionrawmaterial->insert_data($dataInsertRelasDrmInspectionRM);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		} else {
			$this->db->trans_commit();
			$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function history_item()
	{
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$inspectionrawmaterial_kd = $this->input->get('inspectionrawmaterial_kd', true);
		$query = "SELECT inspection.inspectionrawmaterial_tanggal, inspection.inspectionrawmaterial_no, delivery.deliverymaterial_tanggal, delivery.deliverymaterial_no, inspection.inspectionrawmaterial_rm_kode, material.rm_deskripsi, material.rm_spesifikasi, relasi.qty, relasi.keterangan
			FROM td_relasi_deliverymaterialinspectionrawmaterial AS relasi
			LEFT JOIN td_delivery_material AS delivery ON delivery.deliverymaterial_kd = relasi.deliverymaterial_kd
			LEFT JOIN tm_inspection_rawmaterial AS inspection ON inspection.kd_inspectionrawmaterial = relasi.inspectionrawmaterial_kd
			LEFT JOIN tm_rawmaterial AS material ON material.rm_kd = delivery.rm_kd
			WHERE inspection.kd_inspectionrawmaterial = '".$inspectionrawmaterial_kd."'";
		$data['history'] = $this->db->query($query)->result_array();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/tablekirimhistory_main', $data);
	}


	/** CALLBACK FUNCTION FORM VALIDATION */
	public function check_qty()
	{
		/** QTY INPUT TIDAK BOLEH LEBIH DARI (QTY DELIVERYMATERIAL - QTY INSPECTION YANG SUDAH DI INPUT) */
		$noDRM = $this->input->post('txtNoDrm');
		$qty = $this->input->post('txtQty');
		/** Get qty from delivery material */
		$drm = $this->td_delivery_material->get_by_param(['deliverymaterial_kd' => $noDRM])->row_array();
		/** Get qty from inspection raw material  */
		$qtyInspectionRM = $this->db->query("SELECT COALESCE(SUM(inspectionrawmaterial_qty),0) AS sum_inspectionrawmaterial_qty FROM tm_inspection_rawmaterial WHERE deliverymaterial_kd = '".$noDRM."' AND inspectionrawmaterialstate_kd NOT IN ('0', '1') ")->row_array(); // Ketika statusnya 0 = cancel, dan 1 = editing qty tidak di hitung
		/** Qty input - (qty deliverymaterial - total qty inspection yg sudah di input) */
		if($qty > (doubleval($drm['deliverymaterial_qty']) - doubleval($qtyInspectionRM['sum_inspectionrawmaterial_qty']))){
			$this->form_validation->set_message('check_qty', 'Qty input tidak boleh melebihi qty delivery material');
			return false;
		}else{
			return true;
		}
	}

	public function check_qty_reject()
	{
		/** QTY REJECT TIDAK BOLEH LEBIH DARI QTY INPUT */
		$qtyInput = $this->input->post('txtQty');
		$qtyReject = $this->input->post('txtQtyReject');
		if($qtyReject > $qtyInput){
			$this->form_validation->set_message('check_qty_reject', 'Qty input tidak boleh melebihi qty reject');
			return false;
		}else{
			return true;
		}
	}

}