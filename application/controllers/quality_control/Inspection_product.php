<?php

use PhpOffice\PhpSpreadsheet\Calculation\Category;

defined('BASEPATH') or exit('No direct script access allowed!');

class Inspection_product extends MY_Controller {
	private $class_link = 'quality_control/inspection_product';

	public function __construct() {
		parent::__construct();
		$this->load->library(array('ssp'));
		$this->load->model(['tm_inspection_product', 'td_workorder_item', 'tm_deliverynote', 'td_deliverynote_detail', 'tb_bagian', 'td_inspectionproduct_repair', 'tb_inspectionproduct_log', 'Tb_inspectionproduct_state']);
		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
    }

    public function index() {
        parent::administrator();
		parent::select2_assets();
        parent::datetimepicker_assets();
		parent::pnotify_assets();
        $this->table_box();
    }

	public function form_box() {
		$data['id'] = $this->input->get('id');
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		$id = $this->input->get('id');
		$data['inspection'] = $this->tm_inspection_product->get_by_param(['kd_inspectionproduct' => $id])->row_array();
		$data['bagian'] = $this->tb_bagian->get_by_param(['bagian_lokasi' => 'WIP'])->result_array();
		$data['repairBagian'] = $this->td_inspectionproduct_repair->get_by_param(['inspectionproduct_kd' => $id])->result_array();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		$data['class_link'] = $this->class_link;
		$data['js_file'] = 'table_js'; 
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		try{
			$data = $this->tm_inspection_product->ssp_table2();
			echo json_encode($data);
		}catch(\Throwable $e){
			echo $e->getMessage();
		}
	}

	public function table_data_bagian() {
		
		$data = $this->tm_inspection_product->ssp_bagian_table2();
		echo json_encode($data);
	}

	public function table_data_penyebab() {

		$data = $this->tm_inspection_product->ssp_penyebab_table2();
		echo json_encode($data);
	}

	public function table_data_repair() {

		$data = $this->tm_inspection_product->ssp_repair_table2();
		echo json_encode($data);
	}

	public function action_submit() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$id = $this->input->post('txtKd');
		$this->load->library(['form_validation']); 
		$this->form_validation->set_rules('txtTglInspection', 'Tanggal Inspection', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtNoWo', 'No. WO', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtItemCode', 'Item Code', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtBagianKd', 'Posisi Bagian', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtTindakan', 'Tindakan', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtQty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtPenyebab', 'Penyebab', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('check_qty', 'Check Qty', 'callback_check_qty');

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrTglInspection' => (!empty(form_error('txtTglInspection')))?buildLabel('warning', form_error('txtTglInspection', '"', '"')):'',
				'idErrNoWo' => (!empty(form_error('txtNoWo')))?buildLabel('warning', form_error('txtNoWo', '"', '"')):'',
				'idErrItemCode' => (!empty(form_error('txtItemCode')))?buildLabel('warning', form_error('txtItemCode', '"', '"')):'',
				'idErrBagianKd' => (!empty(form_error('txtBagianKd')))?buildLabel('warning', form_error('txtBagianKd', '"', '"')):'',
				'idErrTindakan' => (!empty(form_error('txtTindakan')))?buildLabel('warning', form_error('txtTindakan', '"', '"')):'',		
				'idErrQty' => (!empty(form_error('txtQty')))?buildLabel('warning', form_error('txtQty', '"', '"')):'',
				'idErrCheckQty' => (!empty(form_error('check_qty')))?buildLabel('warning', form_error('check_qty', '"', '"')):'',
				'idErrPenyebab' => (!empty(form_error('txtPenyebab')))?buildLabel('warning', form_error('txtPenyebab', '"', '"')):'',
			);
		}else {
				$this->db->trans_begin();
				if( empty($id) ){
					/** Action insert */
					$tgl = $this->input->post('txtTglInspection');
					$data['kd_inspectionproduct'] = $this->tm_inspection_product->create_code();
					$data['inspectionproduct_no'] = $this->tm_inspection_product->create_no($tgl);
					$data['inspectionproduct_woitem_kd'] = $this->input->post('txtNoWo');
					$data['inspectionproduct_bagian_kd'] = $this->input->post('txtBagianKd');
					$data['inspectionproduct_tanggal'] = format_date($tgl, 'Y-m-d');
					$data['inspectionproduct_item_code'] = $this->input->post('txtItemCode');
					$data['inspectionproduct_tindakan'] = $this->input->post('txtTindakan');
					$data['inspectionproduct_qty'] = $this->input->post('txtQty');
					$data['inspectionproduct_penyebab_bagian_kd'] = $this->input->post('txtPenyebabBagian');
					$data['inspectionproduct_penyebab'] = $this->input->post('txtPenyebab');
					$data['inspectionproduct_keterangan'] = $this->input->post('txtKeterangan');
					$data['inspectionproductstate_kd'] = 1;
					$data['inspectionproduct_tglinput'] = date('Y-m-d H:i:s');
					$data['inspectionproduct_tgledit'] = date('Y-m-d H:i:s');
					$data['admin_kd'] = $this->session->userdata('kd_admin');
					$act = $this->tm_inspection_product->create($data);

					/** Insert repair jika ada repair */
					$arrRepairBagian = $this->input->post('txtRepairBagian');
					if(!empty($arrRepairBagian) && $data['inspectionproduct_tindakan'] == 'repair'){
						foreach($arrRepairBagian as $key => $val){
							$data_repair[$key]['inspectionproduct_kd'] = $data['kd_inspectionproduct'];
							$data_repair[$key]['bagian_kd'] = $val;
							$data_repair[$key]['inspectionproductrepair_tglinput'] = date('Y-m-d H:i:s');
							$data_repair[$key]['admin_kd'] = $this->session->userdata('kd_admin');
						} 
						$actRepair = $this->td_inspectionproduct_repair->insert_batch_data($data_repair);
					}

					/** Insert LOG state */
					$dataLog = [
						'inspectionproductlog_kd' => $this->tb_inspectionproduct_log->create_code(),
						'inspectionproduct_kd' => $data['kd_inspectionproduct'],
						'inspectionproductlogstate_kd' => $data['inspectionproductstate_kd'],
						'inspectionproductlog_keterangan' => null,
						'inspectionproductlog_tglinput' => date('Y-m-d H:i:s'),
						'admin_kd' => $data['admin_kd']
					]; 
					$insertLog = $this->tb_inspectionproduct_log->insert_data($dataLog);

					if ($this->db->trans_status() === FALSE) {
						$this->db->trans_rollback();
						$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
					}else{
						$this->db->trans_commit();
						$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
					};
				}else{
					/** Action edit */
					$data['inspectionproduct_woitem_kd'] = $this->input->post('txtNoWo');
					$data['inspectionproduct_bagian_kd'] = $this->input->post('txtBagianKd');
					$data['inspectionproduct_tanggal'] = format_date($this->input->post('txtTglInspection'), 'Y-m-d');
					$data['inspectionproduct_item_code'] = $this->input->post('txtItemCode');
					$data['inspectionproduct_tindakan'] = $this->input->post('txtTindakan');
					$data['inspectionproduct_qty'] = $this->input->post('txtQty');
					$data['inspectionproduct_penyebab_bagian_kd'] = $this->input->post('txtPenyebabBagian');
					$data['inspectionproduct_penyebab'] = $this->input->post('txtPenyebab');
					$data['inspectionproduct_keterangan'] = $this->input->post('txtKeterangan');
					$data['inspectionproduct_tgledit'] = date('Y-m-d H:i:s');
					$data['admin_kd'] = $this->session->userdata('kd_admin');
					$act = $this->tm_inspection_product->update($data, $id);

					/** Insert repair jika ada repair */
					$arrRepairBagian = $this->input->post('txtRepairBagian');
					if($data['inspectionproduct_tindakan'] == 'repair'){
						if(!empty($arrRepairBagian)){
							$delete_repair = $this->td_inspectionproduct_repair->delete_by_param(['inspectionproduct_kd' => $id]);
							foreach($arrRepairBagian as $key => $val){
								$data_repair[$key]['inspectionproduct_kd'] = $id;
								$data_repair[$key]['bagian_kd'] = $val;
								$data_repair[$key]['inspectionproductrepair_tglinput'] = date('Y-m-d H:i:s');
								$data_repair[$key]['admin_kd'] = $this->session->userdata('kd_admin');
							} 
							$actRepair = $this->td_inspectionproduct_repair->insert_batch_data($data_repair);
						}
					}else{
						$delete_repair = $this->td_inspectionproduct_repair->delete_by_param(['inspectionproduct_kd' => $id]);
					}

					if ($this->db->trans_status() === FALSE) {
						$this->db->trans_rollback();
						$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
					}else{
						$this->db->trans_commit();
						$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
					};
				}
			$resp['csrf'] = $this->security->get_csrf_hash();
		}
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->db->trans_begin();
		$id = $this->input->get('id', TRUE);
		if (!empty($id)){
			$actDel = $this->tm_inspection_product->delete_data($id);
			$delete_repair = $this->td_inspectionproduct_repair->delete_by_param(['inspectionproduct_kd' => $id]);
			$delete_log = $this->tb_inspectionproduct_log->delete_data_by_param(['inspectionproduct_kd' => $id]);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
			}else{
				$this->db->trans_commit();
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
			}
		}
		else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Tidak Sesuai');
		}
		echo json_encode($resp);
	}

	public function action_state()
	{
		$this->db->trans_begin();
		$id = $this->input->get('id', TRUE);
		$state = $this->input->get('state', TRUE);
		if (!empty($id)){
			/** 4 adalah state close */
			if($state == 4){
				$act = $this->tm_inspection_product->update(['flag_close' => '1'], $id);
			}

			$act = $this->tm_inspection_product->update(['inspectionproductstate_kd' => $state], $id);
			/** Insert LOG state */
			$dataLog = [
				'inspectionproductlog_kd' => $this->tb_inspectionproduct_log->create_code(),
				'inspectionproduct_kd' => $id,
				'inspectionproductlogstate_kd' => $state,
				'inspectionproductlog_keterangan' => null,
				'inspectionproductlog_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin')
			]; 
			$insertLog = $this->tb_inspectionproduct_log->insert_data($dataLog);

			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal proses');
			}else{
				/** Cek apakah state kirim ke email */
				$sendMail = $this->tm_inspection_product->sendMail($id, $state);
				if($sendMail == true){
					$this->db->trans_commit();
					$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data berhasil di proses');
				}else{
					$this->db->trans_rollback();
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal proses, notif tidak terkirim. mohon ulangi proses !!');
				}
			}
		}
		else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Tidak Sesuai');
		}
		echo json_encode($resp);
	}

	public function action_approved_bagian()
	{
		$this->db->trans_begin();
		$id = $this->input->get('id', TRUE);
		if (!empty($id)){
				$dataPenyebab = [
					'flag_approved_bagian' => '1', 
					'approved_bagian_admin_kd' => $this->session->userdata('kd_admin'),
					'approved_bagian_tgl' => date('Y-m-d H:i:s')
				];
				$act = $this->tm_inspection_product->update($dataPenyebab, $id);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal approved bagian');
			}else{
				$this->db->trans_commit();
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data berhasil di approved');
			}
		}
		else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Tidak Sesuai');
		}
		echo json_encode($resp);
	}

	public function action_approved_penyebab()
	{
		$this->db->trans_begin();
		$id = $this->input->get('id', TRUE);
		if (!empty($id)){
				$dataPenyebab = [
					'flag_approved_penyebab' => '1', 
					'approved_penyebab_admin_kd' => $this->session->userdata('kd_admin'),
					'approved_penyebab_tgl' => date('Y-m-d H:i:s')
				];
				$act = $this->tm_inspection_product->update($dataPenyebab, $id);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal approved penyebab');
			}else{
				$this->db->trans_commit();
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data berhasil di approved');
			}
		}
		else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Tidak Sesuai');
		}
		echo json_encode($resp);
	}

	public function action_approved_repair()
	{
		$this->db->trans_begin();
		$id = $this->input->get('id', TRUE);
		$bagian_kd = $this->input->get('bagian_kd', TRUE);

		if (!empty($id)){
				$whereRepair = [
					'inspectionproduct_kd' => $id,
					'bagian_kd' => $bagian_kd
				];
				$dataRepair = [
					'flag_approved' => '1',
					'tgl_approved' => date('Y-m-d H:i:s'),
					'approved_admin_kd' => $this->session->userdata('kd_admin')
				];
				$act = $this->td_inspectionproduct_repair->update_data($whereRepair, $dataRepair);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal approved repair');
			}else{
				$this->db->trans_commit();
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data berhasil di approved');
			}
		}
		else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Tidak Sesuai');
		}
		echo json_encode($resp);
	}

	public function get_wo() {
		$paramWO = $this->input->get('paramWO');
		$resultData = [];
		if (!empty($paramWO)) {
			$query = $this->db->select('td_workorder_item.*, tm_inspection_product.inspectionproduct_woitem_kd')
				->from('td_workorder_item')
				->join('tm_inspection_product', 'tm_inspection_product.inspectionproduct_woitem_kd = td_workorder_item.woitem_kd', 'left')
				->where('tm_inspection_product.inspectionproduct_woitem_kd', null)
				->like('td_workorder_item.woitem_no_wo', $paramWO, 'match')
				->order_by('td_workorder_item.woitem_no_wo', 'DESC')
				->get();
			if (!empty($query->num_rows())) {
				$result = $query->result_array();
				foreach ($result as $r) {
					$resultData[] = [
						'id' => $r['woitem_kd'], 
						'text' => $r['woitem_no_wo'],
						'item_code' => $r['woitem_itemcode'],
						'woitem_qty' => $r['woitem_qty']
					];
				}
			}
		}
		echo json_encode($resultData);
	}

	public function get_bagian_by_wo_dn_tujuan() {
		/** Diambil nama bagian saja */
		$paramWO = $this->input->get('paramWO');
		$paramDN = $this->input->get('paramDN');
		$resultData = [];
		if (!empty($paramWO)) {
			$query = $this->db->select('td_deliverynote_detail.*, tm_deliverynote.dn_no, tb_bagian.bagian_nama, tb_bagian.bagian_kd')
				->from('td_deliverynote_detail')
				->join('tm_deliverynote', 'tm_deliverynote.dn_kd = td_deliverynote_detail.dn_kd')
				->join('tb_bagian', 'tb_bagian.bagian_kd=tm_deliverynote.dn_tujuan', 'left')
				->where('td_deliverynote_detail.woitem_kd', $paramWO)
				->like('tm_deliverynote.dn_no', $paramDN, 'match')
				->group_by('tb_bagian.bagian_kd')
				->order_by('td_deliverynote_detail.dn_kd', 'DESC')
				->get();
			if (!empty($query->num_rows())) {
				$result = $query->result_array();
				foreach ($result as $r) {
					$resultData[] = [
						'id' => $r['bagian_kd'], 
						'text' => $r['bagian_nama'],
					];
				}
			}
		}
		echo json_encode($resultData);
	}

	public function get_bagian_by_wo_dn_asal() {
		/** Diambil nama bagian saja */
		$paramWO = $this->input->get('paramWO');
		$paramDN = $this->input->get('paramDN');
		$resultData = [];
		if (!empty($paramWO)) {
			$query = $this->db->select('td_deliverynote_detail.*, tm_deliverynote.dn_no, tb_bagian.bagian_nama, tb_bagian.bagian_kd')
				->from('td_deliverynote_detail')
				->join('tm_deliverynote', 'tm_deliverynote.dn_kd = td_deliverynote_detail.dn_kd')
				->join('tb_bagian', 'tb_bagian.bagian_kd=tm_deliverynote.dn_asal', 'left')
				->where('td_deliverynote_detail.woitem_kd', $paramWO)
				->like('tm_deliverynote.dn_no', $paramDN, 'match')
				->group_by('tb_bagian.bagian_kd')
				->order_by('td_deliverynote_detail.dn_kd', 'DESC')
				->get();
			if (!empty($query->num_rows())) {
				$result = $query->result_array();
				foreach ($result as $r) {
					$resultData[] = [
						'id' => $r['bagian_kd'], 
						'text' => $r['bagian_nama'],
					];
				}
			}
		}
		echo json_encode($resultData);
	}

	public function get_all_bagian_bydn() {
		/** Diambil nama bagian saja */
		$paramWO = $this->input->get('paramWO');
		$paramDN = $this->input->get('paramDN');
		//$resultData = [];
		$resultDatax = [];
		$resultDatay = [];
		if (!empty($paramWO)) {
			$query = $this->db->select('td_deliverynote_detail.*, tm_deliverynote.dn_no, tb_bagian.bagian_nama, tb_bagian.bagian_kd')
				->from('td_deliverynote_detail')
				->join('tm_deliverynote', 'tm_deliverynote.dn_kd = td_deliverynote_detail.dn_kd')
				->join('tb_bagian', 'tb_bagian.bagian_kd=tm_deliverynote.dn_asal', 'left')
				->join('tb_bagian as fum', 'fum.bagian_kd=tm_deliverynote.dn_tujuan', 'left')
				->where('td_deliverynote_detail.woitem_kd', $paramWO)
				->like('tm_deliverynote.dn_no', $paramDN, 'match')
				->group_by('tb_bagian.bagian_kd')
				->order_by('td_deliverynote_detail.dn_kd', 'DESC')
				->get();
			if (!empty($query->num_rows())) {
				$result = $query->result_array();
				foreach ($result as $r) {
					$resultDatax[] = [
						'id' => $r['bagian_kd'], 
						'text' => $r['bagian_nama'],
					];
				}
			}

			$query = $this->db->select('td_deliverynote_detail.*, tm_deliverynote.dn_no, tb_bagian.bagian_nama, tb_bagian.bagian_kd')
			->from('td_deliverynote_detail')
			->join('tm_deliverynote', 'tm_deliverynote.dn_kd = td_deliverynote_detail.dn_kd')
			->join('tb_bagian', 'tb_bagian.bagian_kd=tm_deliverynote.dn_tujuan', 'left')
			->where('td_deliverynote_detail.woitem_kd', $paramWO)
			->like('tm_deliverynote.dn_no', $paramDN, 'match')
			->group_by('tb_bagian.bagian_kd')
			->order_by('td_deliverynote_detail.dn_kd', 'DESC')
			->get();
		if (!empty($query->num_rows())) {
			$result = $query->result_array();
			foreach ($result as $r) {
				$resultDatay[] = [
					'id' => $r['bagian_kd'], 
					'text' => $r['bagian_nama'],
				];
			}
		}
		}
		// $resultDatax = json_decode($resultDatax, true);  
		// $resultDatay = json_decode($resultDatay, true);  

		// Menggabungkan kedua array  
		$mergedArray = array_merge($resultDatax, $resultDatay); 

		$uniqueArray = [];  
		foreach ($mergedArray as $item) {  
			$uniqueArray[$item['id']] = $item; // Menggunakan 'id' sebagai kunci  
		}  
		$uniqueArray = array_values($uniqueArray);  


		echo json_encode($uniqueArray);
	}

	public function report_pdf()
	{
		$this->load->library('Pdf');
		try{
		$id = $this->input->get('id', true);
		$konten['master'] = $this->tm_inspection_product->get_by_param(['kd_inspectionproduct' => $id])->row_array();
		$konten['master_bagian'] = $this->tb_bagian->get_by_param(['bagian_lokasi' => 'WIP'])->result_array();
		$konten['repair_bagian'] = $this->td_inspectionproduct_repair->get_by_param(['inspectionproduct_kd' => $id])->result_array();
		/** Get name log untuk TTD di footer */
		$konten['footer_data'] = $this->tb_inspectionproduct_log->get_byparam_detail(['inspectionproduct_kd' => $id])->result_array();
		$data['konten'] = $this->load->view('page/'.$this->class_link.'/pdf_datamain', $konten, true);
		$this->load->view('page/'.$this->class_link.'/pdf_main', $data);
		}catch(\Throwable $t){
			echo $t->getMessage();
		}

	}

	public function pdf()
	{
		$this->load->view('page/'.$this->class_link.'/pdf_datamain');

	}

	public function check_qty()
	{
		$woKd = $this->input->post('txtNoWo');
		$qty = $this->input->post('txtQty');
		$wo = $this->td_workorder_item->get_by_param(['woitem_kd' => $woKd])->row_array();
		if($qty > $wo['woitem_qty']){
			$this->form_validation->set_message('check_qty', 'Qty input tidak boleh melebihi qty WO');
			return false;
		}else{
			return true;
		}	
	}

}