<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Inspectionrawmaterial_user extends MY_Controller
{
    private $class_link = 'quality_control/qc_manage/inspectionrawmaterial_user';

    public function __construct()
    {
        try{
            parent::__construct();

            $this->load->library(array('ssp', 'form_validation'));
            $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
            $this->load->model(array('tb_inspectionrawmaterial_user', 'td_inspectionrawmaterial_userstate', 'tb_inspectionrawmaterial_state', 'tb_bagian', 'tb_admin', 'td_admin_tipe'));
        }catch(\Throwable $e)
        {
            echo $e->getMessage();
        }
    }

    public function index()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function table_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }

    public function table_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data = $this->tb_inspectionrawmaterial_user->ssp_table2();
        echo json_encode($data);
    }

    public function table_data_state()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data = $this->td_inspectionrawmaterial_userstate->ssp_table2();
        echo json_encode($data);
    }

    public function form_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $user_kd = $this->input->get('id', true);

        /** Opsi Admin */
        $admins = $this->tb_admin->get_all()->result_array();
        $opsiUser[null] = '-- Pilih Opsi --';
        foreach ($admins as $admin) {
            $opsiUser[$admin['kd_admin']] = $admin['nm_admin'];
        }

        /** Opsi tipe admin */
        $adminTipes = $this->td_admin_tipe->get_all()->result_array();
        $opsiTipeUser[null] = '-- Pilih Opsi --';
        foreach ($adminTipes as $adminTipe) {
            $opsiTipeUser[$adminTipe['kd_tipe_admin']] = $adminTipe['nm_tipe_admin'];
        }

        /** Opsi Bagian */
        $bagians = $this->tb_bagian->get_all()->result_array();
        $opsiBagian[null] = '-- Pilih Opsi --';
        foreach ($bagians as $bagian) {
            $opsiBagian[$bagian['bagian_kd']] = $bagian['bagian_nama'];
        }

        /** Opsi Approved */
        $opsiApproved[null] = '-- Pilih Opsi --';
        $opsiApproved['0'] = 'Tidak';
        $opsiApproved['1'] = 'Ya';

        $data['opsiUser'] = $opsiUser;
        $data['opsiTipeUser'] = $opsiTipeUser;
        $data['opsiBagian'] = $opsiBagian;
        $data['opsiApproved'] = $opsiApproved;
        $data['class_link'] = $this->class_link;
        $data['id'] = $user_kd;
        $this->load->view('page/' . $this->class_link . '/form_main', $data);
    }

    public function form_main_state(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $user_kd = $this->input->get('id', true);

        /** Opsi Admin */
        $admins = $this->tb_admin->get_all()->result_array();
        $opsiUser[null] = '-- Pilih Opsi --';
        foreach ($admins as $admin) {
            $opsiUser[$admin['kd_admin']] = $admin['nm_admin'];
        }

        $data['opsiUser'] = $opsiUser;
        $data['state'] = $this->tb_inspectionrawmaterial_state->get_all()->result_array();
        $data['admin_state'] = $this->td_inspectionrawmaterial_userstate->get_by_param(['admin_kd' => $user_kd])->result_array();
        $data['admin'] = $this->tb_admin->get_by_param(['kd_admin' => $user_kd])->row_array();
        $data['id'] = $user_kd;
        $this->load->view('page/' . $this->class_link . '/form_main_state', $data);
    }

    public function action_insert()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtadmin_kd', 'User', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtkd_bagian', 'Bagian', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtapproved_bagian', 'Approved Bagian', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtapproved_penyebab', 'Approved Penyebab', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErridtxtadmin_kd' => (!empty(form_error('txtadmin_kd'))) ? buildLabel('warning', form_error('txtadmin_kd', '"', '"')) : '',
                'idErridtxtkd_bagian' => (!empty(form_error('txtkd_bagian'))) ? buildLabel('warning', form_error('txtkd_bagian', '"', '"')) : '',
                'idErridtxtapproved_bagian' => (!empty(form_error('txtapproved_bagian'))) ? buildLabel('warning', form_error('txtapproved_bagian', '"', '"')) : '',
                'idErridtxtapproved_penyebab' => (!empty(form_error('txtapproved_penyebab'))) ? buildLabel('warning', form_error('txtapproved_penyebab', '"', '"')) : '',
            );
        } else {
            $user_kd = $this->input->post('txtuser_kd', true);
            $admin_kd = $this->input->post('txtadmin_kd', true);
            $kd_bagian = $this->input->post('txtkd_bagian', true);
            $approved_bagian = $this->input->post('txtapproved_bagian', true);
            $approved_penyebab = $this->input->post('txtapproved_penyebab', true);

            $data = array(
                'admin_kd' => !empty($admin_kd) ? $admin_kd : null,
                'kd_bagian' => $kd_bagian,
                'approved_bagian' => $approved_bagian,
                'approved_penyebab' => $approved_penyebab,
                'inspectionrawmaterialuser_tgledit' => date('Y-m-d H:i:s'),
            );
            if (empty($user_kd)) {
                /** Add */
                $data = array_merge($data, [
                    'inspectionrawmaterialuser_kd' => $this->tb_inspectionrawmaterial_user->create_code(),
                    'inspectionrawmaterialuser_tglinput' => date('Y-m-d H:i:s'),
                ]);
                $act = $this->tb_inspectionrawmaterial_user->insert_data($data);
            } else {
                /** Edit */
                $act = $this->tb_inspectionrawmaterial_user->update_data(['inspectionrawmaterialuser_kd' => $user_kd], $data);
            }

            if ($act) :
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            else :
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            endif;
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_insert_state()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $state = $this->input->post('txtstate_kd', TRUE);
        $admin_kd = $this->input->post('txtadmin_kd', TRUE);
        
        if (!empty($admin_kd)) {
            $this->db->trans_start();
            $acDel = $this->td_inspectionrawmaterial_userstate->delete_by_param(['admin_kd' => $admin_kd]);
            foreach($state as $stateVal){
                $data = array();
                $data = array(
                    'admin_kd' => $admin_kd,
                    'inspectionrawmaterialstate_kd' => $stateVal,
                    'inspectionrawmaterialuserstate_tglinput' => date('Y-m-d H:i:s'),
                
                );
                $actinsert = $this->td_inspectionrawmaterial_userstate->insert_data($data);
            }
            
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Delete');
            } else {
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Sukses');
            }

            $resp['csrf'] = $this->security->get_csrf_hash();
        }else{
            $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Id Tidak Ditemukan');
        }
        echo json_encode($resp);
    }

    public function action_delete()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $id = $this->input->get('id');
        $admin_kd = $this->input->get('admin_kd');
        if (!empty($id)) {
            $this->db->trans_start();
            $this->tb_inspectionrawmaterial_user->delete_data(['inspectionrawmaterialuser_kd' => $id]);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Delete');
            } else {
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Sukses');
            }
        } else {
            $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Id Tidak Ditemukan');
        }
        echo json_encode($resp);
    }

    public function action_delete_user_state()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $admin_kd = $this->input->get('admin_kd');
        if (!empty($id)) {
            $this->db->trans_start();
            $this->td_inspectionrawmaterial_userstate->delete_data(['admin_kd' => $admin_kd]);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Delete');
            } else {
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Sukses');
            }
        } else {
            $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Id Tidak Ditemukan');
        }
        echo json_encode($resp);
    }
}
