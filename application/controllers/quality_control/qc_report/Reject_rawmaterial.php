<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Reject_rawmaterial extends MY_Controller {
	private $class_link = 'quality_control/qc_report/reject_rawmaterial';

	public function __construct() {
		parent::__construct();
		$this->load->model(['tm_inspection_rawmaterial']);
		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
	}

    public function index() {
        parent::administrator();
        $this->form_box();
    }
	 
	public function form_box(){
		parent::typeahead_assets();
		parent::datetimepicker_assets();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main(){

		$interval = 10;
		$year = date('Y');
		$arrYear = []; 
		for($i = 0; $i < $interval; $i++){
			array_push($arrYear, $year);
			$year = $year - 1;
		}
		$arrMonth = [];
		for($i = 1; $i <= 12; $i++){
			array_push($arrMonth, $i);
		}

		$data['optionYear'] = $arrYear;
		$data['optionMonth'] = $arrMonth; 
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);

	}

	public function table_box(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtYear', 'Year', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtMonth', 'Month', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
                'idErrYear' => (!empty(form_error('txtYear')))?buildLabel('warning', form_error('txtYear', '"', '"')):'',
                'idErrMonth' => (!empty(form_error('txtMonth')))?buildLabel('warning', form_error('txtMonth', '"', '"')):'',
			);
			
		}else {
			$year = $this->input->post('txtYear', true);
			$month = $this->input->post('txtMonth', true);
			
			$url = base_url().$this->class_link.'/table_data?year='.$year.'&month='.$month;
	
			$resp['code'] = 200;
			$resp['status'] = 'Sukses';
			$resp['data'] = $url;
		}

		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	
	public function table_data()
	{
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$year = $this->input->get('year', true);
		$month = $this->input->get('month', true);

		$reportData = $this->tm_inspection_rawmaterial->get_reject_material_by_month($year, $month);
		// echo json_encode($reportData);
		// die();
		$data['reportData'] = $reportData;
		$data['class_link'] = $this->class_link;
		$resp['data'] = $this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

}