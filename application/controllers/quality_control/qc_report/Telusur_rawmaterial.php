<?php
defined('BASEPATH') or exit('No direct script access allowed!');

require_once APPPATH."third_party/phpspreadsheets/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Telusur_rawmaterial extends MY_Controller {
	private $class_link = 'quality_control/qc_report/telusur_rawmaterial';

	public function __construct() {
		parent::__construct();
		$this->load->model(['tm_gudang', 'tm_barang', 'td_finishgood_in', 'td_finishgood_out', 'td_finishgood_repacking']);
		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		/** Cek Lokasi User */
		$userfg = $this->tb_finishgood_user->get_by_param(array('kd_tipe_admin' => $this->session->userdata('tipe_admin_kd')))->row_array();
		$this->set_gudang = $userfg['kd_gudang'];
    }

    public function index() {
        parent::administrator();
        $this->form_box();
    }
	 
	public function form_box(){
		parent::typeahead_assets();
		parent::datetimepicker_assets();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main(){

		/** Dropdown Warehouse */
		// $actWarehouse = $this->tm_gudang->get_all()->result_array();
		if ($this->session->userdata('tipe_admin_kd') == 'TPA190617001'){
			/** admin */
			$actWarehouse = $this->tm_gudang->get_all()->result_array();
		}else{
			$actWarehouse = $this->tm_gudang->get_by_param (array('kd_gudang'=>$this->set_gudang))->result_array();
		}
		// $opsiWarehouse[''] = '--Pilih Warehouse--';
		foreach($actWarehouse as $eachWarehouse):
			$opsiWarehouse[$eachWarehouse['kd_gudang']] = $eachWarehouse['nm_gudang']; 
		endforeach;
		
		/** Dropdown Jenis Transaksi */
		$opsiTransaksi = array(
			'' => '--Pilih Transaksi--',
			'ALL' => 'Semua',
			'IN' => 'Masuk',
			'OUT' => 'Keluar',
			// 'ADJ' => 'Adjustment',
			'RPK' => 'Repacking'
		);

		$data['opsiTransaksi'] = $opsiTransaksi;
		$data['opsiWarehouse'] = $opsiWarehouse;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);

	}

	public function table_box(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtStartdate', 'StartDate', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtEnddate', 'EndDate', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtWarehouse', 'Warehouse', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtJnsTransaksi', 'Jenis Transaksi', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtbarang_kd', 'Barang', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
                'idErrStartdate' => (!empty(form_error('txtStartdate')))?buildLabel('warning', form_error('txtStartdate', '"', '"')):'',
                'idErrEnddate' => (!empty(form_error('txtEnddate')))?buildLabel('warning', form_error('txtEnddate', '"', '"')):'',
                'idErrWarehouse' => (!empty(form_error('txtWarehouse')))?buildLabel('warning', form_error('txtWarehouse', '"', '"')):'',
                'idErrJnsTransaksi' => (!empty(form_error('txtJnsTransaksi')))?buildLabel('warning', form_error('txtJnsTransaksi', '"', '"')):'',  
                'idErrItemCode' => (!empty(form_error('txtbarang_kd')))?buildLabel('warning', form_error('txtbarang_kd', '"', '"')):'',  
			);
			
		}else {
			$startdate = $this->input->post('txtStartdate', true);
			$enddate = $this->input->post('txtEnddate', true);
			$warehouse = $this->input->post('txtWarehouse', true);
			$jns_transaksi = $this->input->post('txtJnsTransaksi', true);
			$barang_kd = $this->input->post('txtbarang_kd', true);
			
			$url = base_url().$this->class_link.'/table_data?startdate='.$startdate.'&enddate='.$enddate.'&jns_transaksi='.$jns_transaksi.'&warehouse='.$warehouse.'&barang_kd='.$barang_kd;
	
			$resp['code'] = 200;
			$resp['status'] = 'Sukses';
			$resp['data'] = $url;
		}

		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	
	public function table_data()
	{
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$startdate = $this->input->get('startdate', true);
		$data['startdate'] = $startdate;
		$startdate = format_date($startdate, 'Y-m-d');
		$enddate = $this->input->get('enddate', true);
		$data['enddate'] = $enddate;
		$enddate = format_date($enddate, 'Y-m-d');
		$warehouse = $this->input->get('warehouse', true);
		$jns_transaksi = $this->input->get('jns_transaksi', true);
		$barang_kd = $this->input->get('barang_kd', true);
		$actTransaksi = "";
		switch ($jns_transaksi) {
			case 'ALL' :
				$actTransaksi = $this->telusur_rm_all($startdate, $enddate, $barang_kd, $warehouse);
				break;
			case 'IN' :
				$actTransaksi = $this->telusur_rm_in($startdate, $enddate, $barang_kd, $warehouse);
				break;
			case 'OUT' :
				$actTransaksi = $this->telusur_rm_out($startdate, $enddate, $barang_kd, $warehouse);
				break;
			// case 'ADJ' :
			// 	$actTransaksi = $this->telusur_rm_adj($startdate, $enddate, $barang_kd, $warehouse);
			// 	break;
			case 'RPK' :
				$actTransaksi = $this->telusur_rm_repacking($startdate, $enddate, $barang_kd, $warehouse);
				break;
			default :
				$actTransaksi = array('PILIH JNS TRANSAKSI');
		}
		/** get identitas barang */
		$actBarang = $this->tm_barang->get_item(array('kd_barang'=> $barang_kd));
		$data['barang'] = $actBarang;
		
		/** get identitas judul */
		$actGudang = $this->tm_gudang->get_by_param (array('kd_gudang'=>$warehouse))->row_array();
		$data['nm_gudang'] = $actGudang['nm_gudang'];

		$data['class_link'] = $this->class_link;
		$data['rowData'] = $actTransaksi;
		$resp['data'] = $this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	/** Fungsi untuk convert fg kartu stok ke rm telusur */
	/** Parameter ambil dari fg barcode kd yang di generate dari kartu stock */
	public function data_telusur_rm_by_fgbarcode_kd($fgbarcode_kd = [])
	{

		// SELECT t0.fgbarcode_tglinput, t4.item_code, t5.woitem_no_wo, t6.rm_kode, t3.materialreceiptdetailrm_nama, t3.materialreceiptdetailrm_deskripsi, t3.rmgr_code_srj, t3.batch, t9.suplier_kode, t9.suplier_nama 
		// FROM td_finishgood_barcode t0 
		// INNER JOIN td_deliverynote_received t1 ON t0.dndetailreceived_kd = t1.dndetailreceived_kd 
		// INNER JOIN td_materialreceipt_detail t2 ON t1.woitem_kd = t2.woitem_kd 
		// INNER JOIN td_materialreceipt_detail_rawmaterial t3 ON t2.materialreceiptdetail_kd = t3.materialreceiptdetail_kd 
		// LEFT JOIN tm_barang t4 ON t0.barang_kd = t4.kd_barang 
		// LEFT JOIN td_workorder_item t5 ON t1.woitem_kd = t5.woitem_kd 
		// LEFT JOIN tm_rawmaterial t6 ON t6.rm_kd = t3.rm_kd 
		// LEFT JOIN (SELECT * FROM td_rawmaterial_goodsreceive GROUP BY rmgr_code_srj) t7 ON t3.rmgr_code_srj = t7.rmgr_code_srj 
		// LEFT JOIN tm_purchaseorder t8 ON t7.po_kd = t8.po_kd 
		// LEFT JOIN tm_suplier t9 ON t8.suplier_kd = t9.suplier_kd WHERE t3.rmgr_code_srj IS NOT NULL AND t3.rmgr_code_srj != '' AND t0.fgbarcode_kd IN('FBC230718000032', 'FBC230718000031', 'FBC230718000030', 'FBC230718000029') GROUP BY t0.dndetailreceived_kd

		// SELECT td_finishgood_barcode.fgbarcode_tglinput, td_workorder_item.woitem_no_wo, td_workorder_item.woitem_kd
		// FROM td_finishgood_barcode
		// INNER JOIN td_deliverynote_received ON td_deliverynote_received.dndetailreceived_kd = td_finishgood_barcode.dndetailreceived_kd 
		// INNER JOIN td_materialreceipt_detail ON td_deliverynote_received.woitem_kd = td_materialreceipt_detail.woitem_kd 
		// INNER JOIN td_materialreceipt_detail_rawmaterial ON td_materialreceipt_detail.materialreceiptdetail_kd = td_materialreceipt_detail_rawmaterial.materialreceiptdetail_kd
		// LEFT JOIN td_workorder_item ON td_workorder_item.woitem_kd = td_deliverynote_received.woitem_kd
		// LEFT JOIN td_workorder_item_detail ON td_workorder_item_detail.wo_kd = td_workorder_item.wo_kd AND td_workorder_item_detail.kd_barang = td_finishgood_barcode.barang_kd 
		// WHERE td_finishgood_barcode.fgbarcode_kd IN('FBC230718000032', 'FBC230718000031', 'FBC230718000030', 'FBC230718000029')

		// V1 ambil dari workorder item  tanpa detail
		// if( !empty($fgbarcode_kd) ){
		// 	$this->db->select('t0.fgbarcode_tglinput, t4.item_code, t5.woitem_no_wo, t6.rm_kode, t3.materialreceiptdetailrm_nama, t3.materialreceiptdetailrm_deskripsi, t3.rmgr_code_srj, t3.batch, t9.suplier_kode, t9.suplier_nama');
		// 	$this->db->from('td_finishgood_barcode t0');
		// 	$this->db->join('td_deliverynote_received t1', 't0.dndetailreceived_kd = t1.dndetailreceived_kd', 'inner');
		// 	$this->db->join('td_materialreceipt_detail t2', 't1.woitem_kd = t2.woitem_kd', 'inner');
		// 	$this->db->join('td_materialreceipt_detail_rawmaterial t3', 't2.materialreceiptdetail_kd = t3.materialreceiptdetail_kd', 'inner');
		// 	$this->db->join('tm_barang t4', 't0.barang_kd = t4.kd_barang', 'left');
		// 	$this->db->join('td_workorder_item t5', 't1.woitem_kd = t5.woitem_kd', 'left');
		// 	$this->db->join('tm_rawmaterial t6', 't6.rm_kd = t3.rm_kd', 'left');
		// 	$this->db->join('(SELECT * FROM td_rawmaterial_goodsreceive GROUP BY rmgr_code_srj) t7', 't3.rmgr_code_srj = t7.rmgr_code_srj', 'left');
		// 	$this->db->join('tm_purchaseorder t8', 't7.po_kd = t8.po_kd', 'left');
		// 	$this->db->join('tm_suplier t9', 't8.suplier_kd = t9.suplier_kd', 'left');
		// 	$this->db->where('t3.rmgr_code_srj !=', null);
		// 	$this->db->where('t3.rmgr_code_srj !=', '');
		// 	$this->db->where_in('t0.fgbarcode_kd', $fgbarcode_kd);
		// 	$this->db->group_by('t0.dndetailreceived_kd');
		// 	$data = $this->db->get();
		// 	echo $this->db->last_query();
		// 	return $data;
		// }else{
		// 	return false;
		// }

		if( !empty($fgbarcode_kd) ){
			/** Convert array to format IN di MySQL */
			$arrImplode = "'".implode("','" , $fgbarcode_kd)."'";
			$rawQuery = "SELECT t0.woitem_kd, td_workorder_item.woitem_itemcode, td_workorder_item.woitem_no_wo, td_materialreceipt_detail_rawmaterial.rm_kd, td_workorder_item.woitem_deskripsi, td_materialreceipt_detail_rawmaterial.materialreceiptdetailrm_nama, td_materialreceipt_detail_rawmaterial.materialreceiptdetailrm_spesifikasi, td_materialreceipt_detail_rawmaterial.materialreceiptdetailrm_deskripsi, td_materialreceipt_detail_rawmaterial.rmgr_code_srj, td_materialreceipt_detail_rawmaterial.batch, tm_suplier.suplier_kode, tm_suplier.suplier_nama, tm_rawmaterial.rm_kode, t0.dn_no, tm_materialreceipt.materialreceipt_no, tm_barang.item_code
						FROM (
							/* AMBIL BARANG PACKING */
							SELECT td_finishgood_barcode.fgbarcode_kd, td_finishgood_barcode.barang_kd, td_deliverynote_received.woitem_kd, td_finishgood_barcode.fgbarcode_tglinput, tm_deliverynote.dn_no
							FROM td_finishgood_barcode
							LEFT JOIN td_deliverynote_received ON td_deliverynote_received.dndetailreceived_kd = td_finishgood_barcode.dndetailreceived_kd
							LEFT JOIN td_deliverynote_detail ON td_deliverynote_detail.dndetail_kd = td_deliverynote_received.dndetail_kd 
							LEFT JOIN tm_deliverynote ON tm_deliverynote.dn_kd = td_deliverynote_detail.dn_kd 
							WHERE td_finishgood_barcode.fgbarcode_kd IN(".$arrImplode.")
							GROUP BY td_deliverynote_received.woitem_kd
							UNION ALL
							/* AMBIL BARANG PART */
							SELECT td_finishgood_barcode.fgbarcode_kd, td_finishgood_barcode.barang_kd, td_deliverynote_detail.woitem_kd, td_finishgood_barcode.fgbarcode_tglinput, tm_deliverynote.dn_no
							FROM td_finishgood_barcode
							LEFT JOIN td_deliverynote_received ON td_deliverynote_received.dndetailreceived_kd = td_finishgood_barcode.dndetailreceived_kd
							LEFT JOIN td_deliverynote_detail ON td_deliverynote_detail.dndetail_parent = td_deliverynote_received.dndetail_kd 
							LEFT JOIN tm_deliverynote ON tm_deliverynote.dn_kd = td_deliverynote_detail.dn_kd  
							WHERE td_finishgood_barcode.fgbarcode_kd IN(".$arrImplode.") 
							GROUP BY td_deliverynote_detail.woitem_kd ) t0
						LEFT JOIN td_workorder_item ON td_workorder_item.woitem_kd = t0.woitem_kd
						LEFT JOIN td_materialreceipt_detail ON td_materialreceipt_detail.woitem_kd = t0.woitem_kd 
						RIGHT JOIN (SELECT * FROM td_materialreceipt_detail_rawmaterial WHERE rmgr_code_srj IS NOT NULL AND rmgr_code_srj != '' ) td_materialreceipt_detail_rawmaterial ON td_materialreceipt_detail_rawmaterial.materialreceiptdetail_kd = td_materialreceipt_detail.materialreceiptdetail_kd
						LEFT JOIN tm_barang ON tm_barang.kd_barang = t0.barang_kd
						LEFT JOIN tm_rawmaterial ON tm_rawmaterial.rm_kd = td_materialreceipt_detail_rawmaterial.rm_kd
						LEFT JOIN (SELECT * FROM td_rawmaterial_goodsreceive GROUP BY rmgr_code_srj) td_rawmaterial_goodsreceive ON td_rawmaterial_goodsreceive.rmgr_code_srj = td_materialreceipt_detail_rawmaterial.rmgr_code_srj
						LEFT JOIN tm_purchaseorder ON tm_purchaseorder.po_kd = td_rawmaterial_goodsreceive.po_kd
						LEFT JOIN tm_suplier ON tm_suplier.suplier_kd = tm_purchaseorder.suplier_kd
						LEFT JOIN tm_materialreceipt ON tm_materialreceipt.materialreceipt_kd = td_materialreceipt_detail_rawmaterial.materialreceipt_kd
						WHERE t0.fgbarcode_kd IS NOT NULL
						GROUP BY t0.woitem_kd, td_materialreceipt_detail_rawmaterial.rmgr_code_srj, td_materialreceipt_detail_rawmaterial.batch";
			$query = $this->db->query($rawQuery);
			$data = $query;
		}else{
			$data = [];
		}
		return $data;

	}

	private function telusur_rm_all($startdate, $enddate, $item_code, $kd_gudang){
		$act_in = $this->telusur_rm_in($startdate, $enddate, $item_code, $kd_gudang);
		$act_out = $this->telusur_rm_out($startdate, $enddate, $item_code, $kd_gudang);
		$act_repack = $this->telusur_rm_repacking($startdate, $enddate, $item_code, $kd_gudang);

		$merge[] = $act_in;
		$merge[] = $act_out;
		$merge[] = $act_repack;

		$mergeAll = array();
		foreach($merge as $each_merge) {
			if(is_array($each_merge)) {
				$mergeAll = array_merge($mergeAll, $each_merge);
			}
		}
		asort($mergeAll);
		return $mergeAll;
	}

	private function telusur_rm_in($startdate, $enddate, $barang_kd, $kd_gudang){
		$actMaster = $this->td_finishgood_in->get_by_date_item($startdate, $enddate, $barang_kd, $kd_gudang)->result_array();
		$act = $this->data_telusur_rm_by_fgbarcode_kd( array_column($actMaster, 'fgbarcode_kd'));
		if($act != false) {
			$act = $act->result_array();
			foreach ($act as $each_act){
				/** Get data packing dari DN detail (key dndetail parent) */
				$dnDetail = $this->db->query("SELECT * FROM td_deliverynote_detail WHERE woitem_kd = '".$each_act['woitem_kd']."' AND dndetail_parent IS NOT NULL")->row_array();
				$wopacking = $this->db->query("SELECT td_workorder_item.* FROM td_deliverynote_detail INNER JOIN td_workorder_item ON td_workorder_item.woitem_kd = td_deliverynote_detail.woitem_kd WHERE dndetail_kd = '".$dnDetail['dndetail_parent']."'")->row_array();

				$aAct[] = array(
					'no_wo_packing' => $wopacking['woitem_no_wo'],
					'no_wo' => $each_act['woitem_no_wo'],
					'dn_no' => $each_act['dn_no'],
					'woitem_itemcode' => $each_act['woitem_itemcode'],
					'item_code' => $each_act['item_code'],
					'rm_kode' => $each_act['rm_kode'],
					'rm_nama' => $each_act['materialreceiptdetailrm_nama'],
					'rm_deskripsi' => $each_act['materialreceiptdetailrm_deskripsi'],
					'rm_spesifikasi' => $each_act['materialreceiptdetailrm_spesifikasi'],
					'rmgr_code_srj' => $each_act['rmgr_code_srj'],
					'batch' => $each_act['batch'],
					'suplier_kode' => $each_act['suplier_kode'],
					'suplier_nama' => $each_act['suplier_nama'],
					'materialreceipt_no' => $each_act['materialreceipt_no'],
					'uraian' => 'Produksi IN',
				);
			}
		}else{
			$aAct=array();
		}
		krsort($aAct);
		return $aAct;
	}

	private function telusur_rm_out($startdate, $enddate, $barang_kd, $kd_gudang){
		$actMaster = $this->td_finishgood_out->get_by_date_item($startdate, $enddate, $barang_kd, $kd_gudang);
		$act = $this->data_telusur_rm_by_fgbarcode_kd( array_column($actMaster, 'fgbarcode_kd'));
		if($act != false) {
			$act = $act->result_array();
			foreach ($act as $each_act){
				/** Get data packing dari DN detail (key dndetail parent) */
				$dnDetail = $this->db->query("SELECT * FROM td_deliverynote_detail WHERE woitem_kd = '".$each_act['woitem_kd']."' AND dndetail_parent IS NOT NULL")->row_array();
				$wopacking = $this->db->query("SELECT td_workorder_item.* FROM td_deliverynote_detail INNER JOIN td_workorder_item ON td_workorder_item.woitem_kd = td_deliverynote_detail.woitem_kd WHERE dndetail_kd = '".$dnDetail['dndetail_parent']."'")->row_array();

				$aAct[] = array(
					'no_wo_packing' => $wopacking['woitem_no_wo'],
					'no_wo' => $each_act['woitem_no_wo'],
					'dn_no' => $each_act['dn_no'],
					'woitem_itemcode' => $each_act['woitem_itemcode'],
					'item_code' => $each_act['item_code'],
					'rm_kode' => $each_act['rm_kode'],
					'rm_nama' => $each_act['materialreceiptdetailrm_nama'],
					'rm_deskripsi' => $each_act['materialreceiptdetailrm_deskripsi'],
					'rm_spesifikasi' => $each_act['materialreceiptdetailrm_spesifikasi'],
					'rmgr_code_srj' => $each_act['rmgr_code_srj'],
					'batch' => $each_act['batch'],
					'suplier_kode' => $each_act['suplier_kode'],
					'suplier_nama' => $each_act['suplier_nama'],
					'materialreceipt_no' => $each_act['materialreceipt_no'],
					'uraian' => 'Produksi OUT',
				);
			}
		}else{
			$aAct = array();
		}
		krsort($aAct);
		return $aAct;
	}

	private function telusur_rm_repacking($startdate, $enddate, $barang_kd, $kd_gudang){
		$actMaster = $this->td_finishgood_repacking->get_by_date_item($startdate, $enddate, $barang_kd, $kd_gudang);
		$act = $this->data_telusur_rm_by_fgbarcode_kd( array_column($actMaster, 'fgbarcode_kd'));

		if($act != false) {
			$act = $act->result_array();
			foreach($act as $each_act){
				/** Get data packing dari DN detail (key dndetail parent) */
				$dnDetail = $this->db->query("SELECT * FROM td_deliverynote_detail WHERE woitem_kd = '".$each_act['woitem_kd']."' AND dndetail_parent IS NOT NULL")->row_array();
				$wopacking = $this->db->query("SELECT td_workorder_item.* FROM td_deliverynote_detail INNER JOIN td_workorder_item ON td_workorder_item.woitem_kd = td_deliverynote_detail.woitem_kd WHERE dndetail_kd = '".$dnDetail['dndetail_parent']."'")->row_array();

				$aAct[] = array(
					'no_wo_packing' => $wopacking['woitem_no_wo'],
					'no_wo' => $each_act['woitem_no_wo'],
					'dn_no' => $each_act['dn_no'],
					'woitem_itemcode' => $each_act['woitem_itemcode'],
					'item_code' => $each_act['item_code'],
					'rm_kode' => $each_act['rm_kode'],
					'rm_nama' => $each_act['materialreceiptdetailrm_nama'],
					'rm_deskripsi' => $each_act['materialreceiptdetailrm_deskripsi'],
					'rm_spesifikasi' => $each_act['materialreceiptdetailrm_spesifikasi'],
					'rmgr_code_srj' => $each_act['rmgr_code_srj'],
					'batch' => $each_act['batch'],
					'suplier_kode' => $each_act['suplier_kode'],
					'suplier_nama' => $each_act['suplier_nama'],
					'materialreceipt_no' => $each_act['materialreceipt_no'],
					'uraian' => 'Repacking',
				);
			}
		}else{
			$aData = array();
		}
		krsort($aData);
		return $aData;
	}

	/** OUTPUT REPORT FUNCTION */

	public function cetak_telusur_rm()
	{
		parent::admin_print();
		$startdate = $this->input->get('startdate', true);
		$startdate = format_date($startdate, 'Y-m-d');
		$enddate = $this->input->get('enddate', true);
		$enddate = format_date($enddate, 'Y-m-d');
		$warehouse = $this->input->get('warehouse', true);
		$jns_transaksi = $this->input->get('jns_transaksi', true);
		$barang_kd = $this->input->get('barang_kd', true);

		$url = base_url().$this->class_link.'/table_data?startdate='.$startdate.'&enddate='.$enddate.'&jns_transaksi='.$jns_transaksi.'&warehouse='.$warehouse.'&barang_kd='.$barang_kd;
		
		$data['class_link'] = $this->class_link;
		$data['url'] = $url;
		$this->load->view('page/'.$this->class_link.'/print_detail', $data);
	}


	public function xls_telusur_rm(){
		$startdate = $this->input->get('startdate', true);
		$startdate = format_date($startdate, 'Y-m-d');
		$enddate = $this->input->get('enddate', true);
		$enddate = format_date($enddate, 'Y-m-d');
		$warehouse = $this->input->get('warehouse', true);
		$jns_transaksi = $this->input->get('jns_transaksi', true);
		$barang_kd = $this->input->get('barang_kd', true);
		
		switch ($jns_transaksi) {
			case 'ALL' :
				$actTransaksi = $this->telusur_rm_all($startdate, $enddate, $barang_kd, $warehouse);
				break;
			case 'IN' :
				$actTransaksi = $this->telusur_rm_in($startdate, $enddate, $barang_kd, $warehouse);
				break;
			case 'OUT' :
				$actTransaksi = $this->telusur_rm_out($startdate, $enddate, $barang_kd, $warehouse);
				break;
			// case 'ADJ' :
			// 	$actTransaksi = $this->telusur_rm_adjustment($startdate, $enddate, $barang_kd);
			// 	break;
			case 'RPK' :
				$actTransaksi = $this->telusur_rm_repacking($startdate, $enddate, $barang_kd, $warehouse);
				break;
		}

		$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()
			->setCreator($_SESSION['nm_admin'])
			->setLastModifiedBy($_SESSION['nm_admin'])
			->setTitle('Report Telusur RM '.date('d-M-Y'))
			->setCategory('Report');
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		$sheet = $spreadsheet->getActiveSheet();
		$header_cell = 1;
		$sheet->setCellValue('A'.$header_cell, 'No')->getStyle('A'.$header_cell);
		$sheet->setCellValue('B'.$header_cell, 'No WO Packing')->getStyle('B'.$header_cell);
		$sheet->setCellValue('C'.$header_cell, 'WO Item Code')->getStyle('C'.$header_cell);
		$sheet->setCellValue('D'.$header_cell, 'No WO')->getStyle('D'.$header_cell);
		$sheet->setCellValue('E'.$header_cell, 'WO Part')->getStyle('E'.$header_cell);
		$sheet->setCellValue('F'.$header_cell, 'No DN')->getStyle('F'.$header_cell);
		$sheet->setCellValue('G'.$header_cell, 'No MRC')->getStyle('G'.$header_cell);
		$sheet->setCellValue('H'.$header_cell, 'Code SRJ')->getStyle('H'.$header_cell);
		$sheet->setCellValue('I'.$header_cell, 'Batch')->getStyle('I'.$header_cell);
		$sheet->setCellValue('J'.$header_cell, 'RM Kode')->getStyle('J'.$header_cell);
		$sheet->setCellValue('K'.$header_cell, 'RM Deskripsi')->getStyle('K'.$header_cell);
		$sheet->setCellValue('L'.$header_cell, 'Suplier')->getStyle('L'.$header_cell);
		$sheet->setCellValue('M'.$header_cell, 'Uraian')->getStyle('M'.$header_cell);

		$dataCell = 2;
		$no = 1;

		foreach ($actTransaksi as $each):
			$sheet->setCellValue('A'.$dataCell, $no)->getStyle('A'.$dataCell);
			$sheet->setCellValue('B'.$dataCell, $each['no_wo_packing']);
			$sheet->setCellValue('C'.$dataCell, $each['item_code']);
			$sheet->setCellValue('D'.$dataCell, $each['no_wo']);
			$sheet->setCellValue('E'.$dataCell, $each['woitem_itemcode'])->getStyle('C'.$dataCell);
			$sheet->setCellValue('F'.$dataCell, $each['dn_no'])->getStyle('D'.$dataCell);
			$sheet->setCellValue('G'.$dataCell, $each['materialreceipt_no'])->getStyle('E'.$dataCell);
			$sheet->setCellValue('H'.$dataCell, $each['rmgr_code_srj'])->getStyle('F'.$dataCell);
			$sheet->setCellValue('I'.$dataCell, $each['batch'])->getStyle('G'.$dataCell);
			$sheet->setCellValue('J'.$dataCell, $each['rm_kode'])->getStyle('H'.$dataCell);
			$sheet->setCellValue('K'.$dataCell, $each['rm_deskripsi']." | ".$each['rm_spesifikasi'])->getStyle('I'.$dataCell);
			$sheet->setCellValue('L'.$dataCell, $each['suplier_kode']." | ".$each['suplier_nama'])->getStyle('J'.$dataCell);
			$sheet->setCellValue('M'.$dataCell, $each['uraian'])->getStyle('K'.$dataCell);
			$dataCell++;
			$no++;
		endforeach;


		$filename = 'Laporan_telusur_rm.xlsx';
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='. $filename .'');
		header('Cache-Control: max-age=0');
		$writer->save('php://output');
	}

}