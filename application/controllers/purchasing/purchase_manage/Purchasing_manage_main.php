<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Purchasing_manage_main extends MY_Controller {
	private $class_link = 'purchasing/purchase_manage/purchasing_manage_main';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
	}

	public function index() {
        parent::administrator();
        parent::pnotify_assets();
		$this->home_box();
	}
	
	public function home_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/home_box', $data);
	}

	public function home_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/home_main', $data);
	}
	
}
