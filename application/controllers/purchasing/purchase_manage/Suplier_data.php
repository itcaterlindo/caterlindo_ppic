<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Suplier_data extends MY_Controller {
	private $class_link = 'purchasing/purchase_manage/suplier_data';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_suplier', 'tb_set_dropdown', 'tm_currency', 'tb_bank']);
	}

	public function index() {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();
        $this->load->js('assets/admin_assets/plugins/input-mask/jquery.inputmask.js');
		$this->table_box();
	}

	public function form_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		if ($sts == 'edit'){
			$act = $this->tm_suplier->get_by_param (array('suplier_kd'=> $id))->row_array();			
			$data['id'] = $act['suplier_kd'];
			$data['rowData'] = $act;
        }
        /** Badan Usaha */
        $act = $this->tb_set_dropdown->get_by_param (array('jenis_select' => 'customer_property'))->result_array();
        $opsiBadanusaha[''] = '-- Pilih Jenis Usaha --';
        foreach ($act as $each):
			$opsiBadanusaha[$each['id']] = $each['nm_select'];
		endforeach;
		
		/** Currency */
        $act = $this->tm_currency->get_all();
        foreach ($act as $each):
			$opsiCurrency[$each->kd_currency] = $each->currency_nm;
		endforeach;

		 /** Bank */
		 $actBank = $this->tb_bank->get_all()->result_array();
		 $opsiBank[''] = '-- Pilih Opsi --';
		 foreach ($actBank as $each):
			 $opsiBank[$each['bank_kd']] = $each['bank_nama'];
		 endforeach;

		 /** Suplier */
		 $actPayto = $this->tm_suplier->get_all()->result_array();
		 $opsiPayto[''] = '-- Pilih Opsi --';
		 foreach ($actPayto as $each):
			 $opsiPayto[$each['suplier_kd']] = $each['suplier_kode'].' | '.$each['suplier_nama'];
		 endforeach;

		 $param = array(
				"CustomQuery" => "GetInputTax"
		
		 );
		//  $datappn = parent::api_sap_post('search', $param);

		//  $opsiPpn[''] = '-- Pilih Opsi --';
		//  foreach ($datappn[0] as $each):
		// 	 $opsiPpn[$each->Code] = $each->Code.' | '.$each->Name;
		//  endforeach;

		$opsiPpn = [
			'-1' => '-- Pilih Opsi --',
			'P2' => 'P2 | PPN Purchase 0%',
			'P1' => 'P1 | PPN Purchase 11%',
			'P3' => 'P3 | PPN Purchase 1%',
			];

		 $opsitop = [
			'-1' => '- Cash Basis -',
			'1' => 'CBD',
			'2' => '3 Hari',
			'3' => '7 Hari',
			'4' => '14 Hari',
			'5' => '21 Hari',
			'6' => '30 Hari',
			'7' => '45 Hari',
			'8' => '60 Hari',
			'9' => '90 Hari',
			'10' => '120 Hari',
			'11' => '180 Hari',
			'12' => '*'
			];

		$data['data_ppn'] = $opsiPpn;
		$data['data_top'] = $opsitop;

        $data['opsiBadanusaha'] = $opsiBadanusaha;
        $data['opsiCurrency'] = $opsiCurrency;
        $data['opsiBank'] = $opsiBank;
        $data['opsiPayto'] = $opsiPayto;
		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->tm_suplier->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function action_insert() {
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }
		$opsitop = [
			'-1' => '- Cash Basis -',
			'1' => 'CBD',
			'2' => '3',
			'3' => '7',
			'4' => '14',
			'5' => '21',
			'6' => '30',
			'7' => '45',
			'8' => '60',
			'9' => '90',
			'10' => '120',
			'11' => '180',
			'12' => '*'
			];

            $suplier_nama = $this->input->post('txtsuplier_nama', true);
            $suplier_cp = $this->input->post('txtsuplier_cp', true);
            $badanusaha_kd = $this->input->post('txtsuplier_badanusaha', true); 
            $suplier_alamat = $this->input->post('txtsuplier_alamat', true);
            $suplier_cp = $this->input->post('txtsuplier_cp', true, true);
            $suplier_telpon1 = $this->input->post('txtsuplier_telpon1', true);
            $suplier_telpon2 = $this->input->post('txtsuplier_telpon2', true);
            $suplier_fax = $this->input->post('txtsuplier_fax', true);
			$suplier_debpayacc = $this->input->post('txtsuplier_debpayacc', true);
			$suplier_email = $this->input->post('txtsuplier_email', true);
			$suplier_termpayment = $this->input->post('txtsuplier_termpayment', true);
			$suplier_leadtime = $this->input->post('txtsuplier_leadtime', true);
			$suplier_includeppn = $this->input->post('txtsuplier_includeppn', true);
			$kd_currency = $this->input->post('txtkd_currency', true);
			$suplier_npwp = $this->input->post('txtsuplier_npwp', true);
			$bank_kd = $this->input->post('txtbank_kd', true);
			$suplier_banknama = $this->input->post('txtsuplier_banknama', true);
			$suplier_banknorek = $this->input->post('txtsuplier_banknorek', true);
			$suplier_payto = $this->input->post('txtsuplier_payto', true);

			$sip = $suplier_includeppn;
				
			$data = array(
                'suplier_kd' => $this->tm_suplier->generate_kd (''),
                'suplier_kode' => $this->tm_suplier->create_code($suplier_nama),
                'badanusaha_kd' => !empty($badanusaha_kd) ? $badanusaha_kd :null, 
				'suplier_nama' => $suplier_nama,
				'suplier_alamat' => !empty($suplier_alamat) ? $suplier_alamat:null,
				'suplier_cp' => !empty($suplier_cp) ? $suplier_cp:null,
				'suplier_telpon1' => !empty($suplier_telpon1) ? $suplier_telpon1:null,
				'suplier_telpon2' => !empty($suplier_telpon2) ? $suplier_telpon2:null,
				'suplier_fax' => !empty($suplier_fax) ? $suplier_fax:null,
				'suplier_email' => !empty($suplier_email) ? $suplier_email:null,
				'suplier_npwp' => !empty($suplier_npwp) ? $suplier_npwp:null,
				'top_sap' => !empty($suplier_termpayment) ? $suplier_termpayment:null,
				'debpayacc_sap' => !empty($suplier_debpayacc) ? $suplier_debpayacc:null,
				'suplier_includeppn' => ($sip != 'P2') ? 'T':'F',
				'ppn_sap' => $sip,
				'suplier_termpayment' => $opsitop[$suplier_termpayment],
				'kd_currency' => !empty($kd_currency) ? $kd_currency:null,
				'bank_kd' => !empty($bank_kd) ? $bank_kd:null,
				'suplier_banknama' => !empty($suplier_banknama) ? $suplier_banknama:null,
				'suplier_banknorek' => !empty($suplier_banknorek) ? $suplier_banknorek:null,
				'suplier_payto' => !empty($suplier_payto) ? $suplier_payto:null,
				'suplier_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
				'suplier_leadtime' => !empty($suplier_leadtime) ? $suplier_leadtime:null,
			);

			$act = $this->tm_suplier->insert_data($data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => $data);
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan', 'data' => $data);
			}
			
		$resp['csrf'] = $this->security->get_csrf_hash();

		header('Content-Type: application/json');
		echo json_encode($resp);
	}

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$opsitop = [
			'-1' => '- Cash Basis -',
			'1' => 'CBD',
			'2' => '3',
			'3' => '7',
			'4' => '14',
			'5' => '21',
			'6' => '30',
			'7' => '45',
			'8' => '60',
			'9' => '90',
			'10' => '120',
			'11' => '180',
			'12' => '*'
			];

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtsuplier_nama', 'Nama Suplier', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtsuplier_kode', 'Kode Suplier', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idtxtsuplier_nama' => (!empty(form_error('txtsuplier_nama')))?buildLabel('warning', form_error('txtsuplier_nama', '"', '"')):'',
				'idErrsuplier_kode' => (!empty(form_error('txtsuplier_kode')))?buildLabel('warning', form_error('txtsuplier_kode', '"', '"')):'',
			);
			
		}else {
			$data_rollback = $this->tm_suplier->get_by_param (array('suplier_kd'=> $this->input->post('txtsuplier_kd')))->row_array();	


			

			$suplier_kd = $this->input->post('txtsuplier_kd');
			$suplier_nama = $this->input->post('txtsuplier_nama', true);
            $suplier_cp = $this->input->post('txtsuplier_cp', true);
            $badanusaha_kd = $this->input->post('txtsuplier_badanusaha', true); 
            $suplier_alamat = $this->input->post('txtsuplier_alamat', true);
            $suplier_cp = $this->input->post('txtsuplier_cp', true, true);
            $suplier_telpon1 = $this->input->post('txtsuplier_telpon1', true);
            $suplier_telpon2 = $this->input->post('txtsuplier_telpon2', true);
            $suplier_fax = $this->input->post('txtsuplier_fax', true);
			$suplier_email = $this->input->post('txtsuplier_email', true);
			$suplier_termpayment = $this->input->post('txtsuplier_termpayment', true);
			$suplier_leadtime = $this->input->post('txtsuplier_leadtime', true);
			$suplier_kode = $this->input->post('txtsuplier_kode', true);
			$suplier_includeppn = $this->input->post('txtsuplier_includeppn', true);
			$kd_currency = $this->input->post('txtkd_currency', true);
			$suplier_npwp = $this->input->post('txtsuplier_npwp', true);
			$bank_kd = $this->input->post('txtbank_kd', true);
			$suplier_banknama = $this->input->post('txtsuplier_banknama', true);
			$suplier_banknorek = $this->input->post('txtsuplier_banknorek', true);
			$suplier_payto = $this->input->post('txtsuplier_payto', true);	
			$sip = empty($suplier_includeppn) ? 'P2' : $suplier_includeppn;		
			$suplier_debpayacc = $this->input->post('txtsuplier_debpayacc', true);

			$data = array(
				'suplier_kd' => $suplier_kd,
                'suplier_kode' => !empty($suplier_kode) ? $suplier_kode :null, 
                'badanusaha_kd' => !empty($badanusaha_kd) ? $badanusaha_kd :null, 
				'suplier_nama' => $suplier_nama,
				'suplier_alamat' => !empty($suplier_alamat) ? $suplier_alamat:null,
				'suplier_cp' => !empty($suplier_cp) ? $suplier_cp:null,
				'suplier_telpon1' => !empty($suplier_telpon1) ? $suplier_telpon1:null,
				'suplier_telpon2' => !empty($suplier_telpon2) ? $suplier_telpon2:null,
				'suplier_fax' => !empty($suplier_fax) ? $suplier_fax:null,
				'suplier_email' => !empty($suplier_email) ? $suplier_email:null,
				'suplier_npwp' => !empty($suplier_npwp) ? $suplier_npwp:null,
				'suplier_leadtime' => !empty($suplier_leadtime) ? $suplier_leadtime:null,
				'top_sap' => !empty($suplier_termpayment) ? $suplier_termpayment:null,
				'suplier_includeppn' => ($sip == 'P2') ? 'F':'T',
				'debpayacc_sap' => !empty($suplier_debpayacc) ? $suplier_debpayacc:null,
				'ppn_sap' => $sip,
				'suplier_termpayment' => $opsitop[$suplier_termpayment],
				'kd_currency' => !empty($kd_currency) ? $kd_currency:null,
				'bank_kd' => !empty($bank_kd) ? $bank_kd:null,
				'suplier_banknama' => !empty($suplier_banknama) ? $suplier_banknama:null,
				'suplier_banknorek' => !empty($suplier_banknorek) ? $suplier_banknorek:null,
				'suplier_payto' => !empty($suplier_payto) ? $suplier_payto:null,
				'suplier_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->tm_suplier->update_data(array('suplier_kd' => $suplier_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate', 'data' => $data, 'data_rollback' => $data_rollback);
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan', 'data' => $data);
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		header('Content-Type: application/json');
		echo json_encode($resp);
	}
	public function action_update_rollback() {
		// if (!$this->input->is_ajax_request()){
		// // 	exit('No direct script access allowed');
		// // }
		$opsitop = [
			'-1' => '- Cash Basis -',
			'1' => 'CBD',
			'2' => '3',
			'3' => '7',
			'4' => '14',
			'5' => '21',
			'6' => '30',
			'7' => '45',
			'8' => '60',
			'9' => '90',
			'10' => '120',
			'11' => '180',
			'12' => '*'
			];

			$suplier_kd = $this->input->post('suplier_kd');
			$suplier_nama = $this->input->post('suplier_nama', true);
            $suplier_cp = $this->input->post('suplier_cp', true);
            $badanusaha_kd = $this->input->post('badanusaha_kd', true); 
            $suplier_alamat = $this->input->post('suplier_alamat', true);
            $suplier_cp = $this->input->post('suplier_cp', true, true);
            $suplier_telpon1 = $this->input->post('suplier_telpon1', true);
            $suplier_telpon2 = $this->input->post('suplier_telpon2', true);
            $suplier_fax = $this->input->post('suplier_fax', true);
			$suplier_email = $this->input->post('suplier_email', true);
			$suplier_termpayment = $this->input->post('suplier_termpayment', true);
			$suplier_leadtime = $this->input->post('suplier_leadtime', true);
			$suplier_kode = $this->input->post('suplier_kode', true);
			$suplier_includeppn = $this->input->post('suplier_includeppn', true);
			$kd_currency = $this->input->post('kd_currency', true);
			$suplier_npwp = $this->input->post('suplier_npwp', true);
			$bank_kd = $this->input->post('bank_kd', true);
			$suplier_banknama = $this->input->post('suplier_banknama', true);
			$suplier_banknorek = $this->input->post('suplier_banknorek', true);
			$suplier_payto = $this->input->post('suplier_payto', true);		
			$suplier_debpayacc = $this->input->post('suplier_debpayacc', true);
			
			$sip = empty($suplier_includeppn) ? 'P2' : $suplier_includeppn;	

			$data = array(
				'suplier_kd' => $suplier_kd,
                'suplier_kode' => !empty($suplier_kode) ? $suplier_kode :null, 
                'badanusaha_kd' => !empty($badanusaha_kd) ? $badanusaha_kd :null, 
				'suplier_nama' => $suplier_nama,
				'suplier_alamat' => !empty($suplier_alamat) ? $suplier_alamat:null,
				'suplier_cp' => !empty($suplier_cp) ? $suplier_cp:null,
				'suplier_telpon1' => !empty($suplier_telpon1) ? $suplier_telpon1:null,
				'suplier_telpon2' => !empty($suplier_telpon2) ? $suplier_telpon2:null,
				'suplier_fax' => !empty($suplier_fax) ? $suplier_fax:null,
				'suplier_email' => !empty($suplier_email) ? $suplier_email:null,
				'suplier_npwp' => !empty($suplier_npwp) ? $suplier_npwp:null,
				'suplier_leadtime' => !empty($suplier_leadtime) ? $suplier_leadtime:null,
				'top_sap' => !empty($suplier_termpayment) ? $suplier_termpayment:null,
				'suplier_includeppn' => ($sip == 'P2') ? 'F':'T',
				'debpayacc_sap' => !empty($suplier_debpayacc) ? $suplier_debpayacc:null,
				'ppn_sap' => $sip,
				'suplier_termpayment' => $opsitop[$suplier_termpayment],
				'kd_currency' => !empty($kd_currency) ? $kd_currency:null,
				'bank_kd' => !empty($bank_kd) ? $bank_kd:null,
				'suplier_banknama' => !empty($suplier_banknama) ? $suplier_banknama:null,
				'suplier_banknorek' => !empty($suplier_banknorek) ? $suplier_banknorek:null,
				'suplier_payto' => !empty($suplier_payto) ? $suplier_payto:null,
				'suplier_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->tm_suplier->update_data(array('suplier_kd' => $suplier_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate', 'data' => $data);
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan', 'data' => $data);
			}
			
			
		$resp['csrf'] = $this->security->get_csrf_hash();
		header('Content-Type: application/json');
		echo json_encode($resp);
	}


	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$actDel = $this->tm_suplier->delete_data($id);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}
		header('Content-Type: application/json');

		echo json_encode($resp);
	}
	
}
