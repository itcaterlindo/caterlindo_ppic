<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Purchaserequisition_users extends MY_Controller {
	private $class_link = 'purchasing/purchase_manage/purchaserequisition_users';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
        $this->load->model(['tb_purchaserequisition_user', 'tb_admin', 'tb_purchaserequisition_status']);
        $this->load->library('encryption');
	}

	public function index() {
        parent::administrator();
        parent::pnotify_assets();
		$this->table_box();
	}
		
	public function table_box(){
		parent::administrator();
        parent::pnotify_assets();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data (){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->tb_purchaserequisition_user->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function form_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');
        
        if ($sts == 'edit'){
            $rowData = $this->tb_purchaserequisition_user->get_by_param_detail(array('pruser_kd'=>$id))->row_array();
            $data['rowData'] = $rowData;
        }

		/** Opsi User */
        $actopsiAdmin = $this->tb_admin->get_all()->result_array();
		$opsiAdmin[''] = '-- Pilih Admin --';
		foreach ($actopsiAdmin as $eachAdmin):
			$opsiAdmin[$eachAdmin['kd_admin']] = $eachAdmin['nm_admin'];
		endforeach;
		
		/** Opsi PR Status */
        $actopsiPRstatus = $this->tb_purchaserequisition_status->get_all()->result_array();
		$opsiPRstatus[''] = '-- Pilih PR Status --';
		foreach ($actopsiPRstatus as $eachPRstatus):
			$opsiPRstatus[$eachPRstatus['prstatus_kd']] = $eachPRstatus['prstatus_nama'];
        endforeach;

        $data['sts'] = $sts;
        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$data['opsiAdmin'] = $opsiAdmin;
		$data['opsiPRstatus'] = $opsiPRstatus;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpruser_user', 'User', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtpruser_email', 'Email', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtprstatus_kd', 'Group', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpruser_user' => (!empty(form_error('txtpruser_user')))?buildLabel('warning', form_error('txtpruser_user', '"', '"')):'',
				'idErrpruser_email' => (!empty(form_error('txtpruser_email')))?buildLabel('warning', form_error('txtpruser_email', '"', '"')):'',
				'idErrprstatus_kd' => (!empty(form_error('txtprstatus_kd')))?buildLabel('warning', form_error('txtprstatus_kd', '"', '"')):'',
			);
			
		}else {
            $pruser_user = $this->input->post('txtpruser_user', true);
            $pruser_email = $this->input->post('txtpruser_email', true);
            $prstatus_kd = $this->input->post('txtprstatus_kd', true);
				
			$data = array(
				'pruser_user' => $pruser_user,
				'pruser_email' => $pruser_email,
				'prstatus_kd' => $prstatus_kd,
				'pruser_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->tb_purchaserequisition_user->insert_data($data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpruser_user', 'User', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtpruser_email', 'Email', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtprstatus_kd', 'Group', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpruser_user' => (!empty(form_error('txtpruser_user')))?buildLabel('warning', form_error('txtpruser_user', '"', '"')):'',
				'idErrpruser_email' => (!empty(form_error('txtpruser_email')))?buildLabel('warning', form_error('txtpruser_email', '"', '"')):'',
				'idErrprstatus_kd' => (!empty(form_error('txtprstatus_kd')))?buildLabel('warning', form_error('txtprstatus_kd', '"', '"')):'',
			);
			
		}else {
			$pruser_kd = $this->input->post('txtpruser_kd', true);
			$pruser_user = $this->input->post('txtpruser_user', true);
            $pruser_email = $this->input->post('txtpruser_email', true);
            $prstatus_kd = $this->input->post('txtprstatus_kd', true);

			$data = array(
				'pruser_user' => $pruser_user,
				'pruser_email' => $pruser_email,
				'prstatus_kd' => $prstatus_kd,
				'pruser_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->tb_purchaserequisition_user->update_data(array('pruser_kd' => $pruser_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$actDel = $this->tb_purchaserequisition_user->delete_data($id);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
