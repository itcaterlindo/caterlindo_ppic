<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Suplier_katalog extends MY_Controller {
	private $class_link = 'purchasing/purchase_manage/suplier_katalog';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_suplier', 'tb_set_dropdown', 'tm_rawmaterial', 'td_suplier_katalog', 'td_rawmaterial_satuan', 'tm_currency']);
	}

	public function index() {
        parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
		$this->table_box();
    }
    
	public function form_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		if ($sts == 'edit'){
			$act = $this->td_suplier_katalog->get_by_param (array('katalog_kd'=> $id))->row_array();			
			$data['id'] = $act['katalog_kd'];
			$data['rowData'] = $act;
        }
        /** Opsi Suplier */
		$actopsiSuplier = $this->tm_suplier->get_all()->result_array();
		$opsiSuplier[''] = '-- Pilih Suplier --';
		foreach ($actopsiSuplier as $eachSuplier):
			$opsiSuplier[$eachSuplier['suplier_kd']] = $eachSuplier['suplier_kode'].' | '.$eachSuplier['suplier_nama'];
		endforeach;

		/** Opsi Material */
		$actopsiMaterial = $this->tm_rawmaterial->get_all()->result_array();
		$opsiMaterial[''] = '-- Pilih Material --';
		foreach ($actopsiMaterial as $eachMaterial):
			$opsiMaterial[$eachMaterial['rm_kd']] = $eachMaterial['rm_kode'].' | '.$eachMaterial['rm_deskripsi'].'/'.$eachMaterial['rm_spesifikasi'];
		endforeach;

		/** Opsi Satuan */
		$actopsiSatuan = $this->td_rawmaterial_satuan->get_all()->result_array();
		$opsiSatuan[''] = '-- Pilih Satuan --';
		foreach ($actopsiSatuan as $eachSatuan):
			$opsiSatuan[$eachSatuan['rmsatuan_kd']] = $eachSatuan['rmsatuan_nama'];
		endforeach;

		/** Opsi Currency */
		$actopsiCurrency = $this->tm_currency->get_all();
		$opsiSatuan[''] = '-- Pilih Currency --';
		foreach ($actopsiCurrency as $eachCurrency):
			$opsiCurrency[$eachCurrency->kd_currency] = $eachCurrency->currency_nm;
		endforeach;

        $data['opsiSuplier'] = $opsiSuplier;
		$data['opsiMaterial'] = $opsiMaterial;
		$data['opsiSatuan'] = $opsiSatuan;
		$data['opsiCurrency'] = $opsiCurrency;
		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->td_suplier_katalog->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtsuplier_kd', 'Suplier', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtmaterial_kd', 'Material', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtkatalog_harga', 'Harga', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtkatalog_satuankd', 'Satuan', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrSuplier_kd' => (!empty(form_error('txtsuplier_kd')))?buildLabel('warning', form_error('txtsuplier_kd', '"', '"')):'',
				'idErrMaterial_kd' => (!empty(form_error('txtmaterial_kd')))?buildLabel('warning', form_error('txtmaterial_kd', '"', '"')):'',
				'idErrKatalog_harga' => (!empty(form_error('txtkatalog_harga')))?buildLabel('warning', form_error('txtkatalog_harga', '"', '"')):'',
				'idErrkatalog_satuankd' => (!empty(form_error('txtkatalog_satuankd')))?buildLabel('warning', form_error('txtkatalog_satuankd', '"', '"')):'',
			);
			
		}else {
            $suplier_kd = $this->input->post('txtsuplier_kd', true);
            $rm_kd = $this->input->post('txtmaterial_kd', true);
            $katalog_harga = $this->input->post('txtkatalog_harga', true);
            $katalog_konversi = $this->input->post('txtkatalog_konversi', true);
			$katalog_duedate = $this->input->post('txtkatalog_duedate', true);
			$katalog_satuankd = $this->input->post('txtkatalog_satuankd', true);
			$katalog_aktif = $this->input->post('txtkatalog_aktif', true);
			$curDate = date('Y-m-d H:i:s');
			
			$data = array(
				'katalog_kd' => $this->td_suplier_katalog->create_code(),
                'suplier_kd' => $suplier_kd,
                'rm_kd' => $rm_kd, 
				'katalog_harga' => !empty($katalog_harga) ? $katalog_harga:null,
				'katalog_konversi' => !empty($katalog_konversi) ? $katalog_konversi:null,
				'katalog_duedate' => !empty($katalog_duedate) ? $katalog_duedate:null,
				'katalog_satuankd' => !empty($katalog_satuankd) ? $katalog_satuankd:null,
				'katalog_aktif' => $katalog_aktif,
				'katalog_tglinput' => $curDate,
				'katalog_tgledit' => $curDate,
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			/** Cek item pada suplier */
			$actCek = $this->td_suplier_katalog->get_by_param (array('suplier_kd' => $suplier_kd, 'rm_kd' => $rm_kd));
			if ($actCek->num_rows() == 0){
				$act = $this->td_suplier_katalog->insert_data($data);
				if ($act){
					$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
				}else{
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
				}
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Item Sudah Ada');
			}
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtsuplier_kd', 'Suplier', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtmaterial_kd', 'Material', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtkatalog_harga', 'Harga', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtkatalog_satuankd', 'Satuan', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrSuplier_kd' => (!empty(form_error('txtsuplier_kd')))?buildLabel('warning', form_error('txtsuplier_kd', '"', '"')):'',
				'idErrMaterial_kd' => (!empty(form_error('txtmaterial_kd')))?buildLabel('warning', form_error('txtmaterial_kd', '"', '"')):'',
				'idErrKatalog_harga' => (!empty(form_error('txtkatalog_harga')))?buildLabel('warning', form_error('txtkatalog_harga', '"', '"')):'',
				'idErrkatalog_satuankd' => (!empty(form_error('txtkatalog_satuankd')))?buildLabel('warning', form_error('txtkatalog_satuankd', '"', '"')):'',
			);
			
		}else {
			$katalog_kd = $this->input->post('txtkatalog_kd');
            $katalog_harga = $this->input->post('txtkatalog_harga', true);
            $katalog_konversi = $this->input->post('txtkatalog_konversi', true);
            $katalog_duedate = $this->input->post('txtkatalog_duedate', true);           
            $katalog_satuankd = $this->input->post('txtkatalog_satuankd', true);
			$katalog_aktif = $this->input->post('txtkatalog_aktif', true);
			$curDate = date('Y-m-d H:i:s');

			$data = array(
				'katalog_harga' => !empty($katalog_harga) ? $katalog_harga:null,
				'katalog_konversi' => !empty($katalog_konversi) ? $katalog_konversi:null,
				'katalog_duedate' => !empty($katalog_duedate) ? $katalog_duedate:null,
				'katalog_satuankd' => !empty($katalog_satuankd) ? $katalog_satuankd:null,
				'katalog_aktif' => !empty($katalog_aktif) ? $katalog_aktif:null,
				'katalog_tgledit' => $curDate,
				'admin_kd' => $this->session->userdata('kd_admin'),
			);
	
			$act = $this->td_suplier_katalog->update_data(array('katalog_kd' => $katalog_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$actDel = $this->td_suplier_katalog->delete_data($id);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
