<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Purchaserequisition_status extends MY_Controller {
	private $class_link = 'purchasing/purchase_manage/purchaserequisition_status';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
        $this->load->model(['tb_purchaserequisition_status']);
        $this->load->library('encryption');
	}

	public function index() {
        parent::administrator();
        parent::pnotify_assets();
		$this->table_box();
	}
	
	public function table_box(){
		parent::administrator();
        parent::pnotify_assets();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data (){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->tb_purchaserequisition_status->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function form_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');
        
        if ($sts == 'edit'){
            $rowData = $this->tb_purchaserequisition_status->get_by_param(array('prstatus_kd'=>$id))->row_array();
            $data['rowData'] = $rowData;
		}
		
		/** Opsi send mail */
		$opsiSendemail = array('false' => 'Tidak', 'true'=> 'Ya');

        $data['sts'] = $sts;
        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$data['opsiSendemail'] = $opsiSendemail;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtprstatus_nama', 'Nama', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtprstatus_level', 'Level', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrprstatus_nama' => (!empty(form_error('txtprstatus_nama')))?buildLabel('warning', form_error('txtprstatus_nama', '"', '"')):'',
				'idErrprstatus_level' => (!empty(form_error('txtprstatus_level')))?buildLabel('warning', form_error('txtprstatus_level', '"', '"')):'',
			);
			
		}else {
            $prstatus_nama = $this->input->post('txtprstatus_nama', true);
            $prstatus_level = $this->input->post('txtprstatus_level', true);
            $prstatus_label = $this->input->post('txtprstatus_label', true);
            $prstatus_icon = $this->input->post('txtprstatus_icon', true);
            $prstatus_sendemail = $this->input->post('txtprstatus_sendemail', true);
				
			$data = array(
				'prstatus_nama' => !empty($prstatus_nama)? $prstatus_nama:null,
				'prstatus_level' => !empty($prstatus_level)? $prstatus_level:null,
				'prstatus_label' => !empty($prstatus_label)? $prstatus_label:null,
				'prstatus_icon' => !empty($prstatus_icon)? $prstatus_icon:null,
				'prstatus_sendemail' => !empty($prstatus_sendemail)? $prstatus_sendemail:'false',
				'prstatus_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->tb_purchaserequisition_status->insert_data($data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtprstatus_nama', 'Nama', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtprstatus_level', 'Level', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrprstatus_nama' => (!empty(form_error('txtprstatus_nama')))?buildLabel('warning', form_error('txtprstatus_nama', '"', '"')):'',
				'idErrprstatus_level' => (!empty(form_error('txtprstatus_level')))?buildLabel('warning', form_error('txtprstatus_level', '"', '"')):'',
			);
			
		}else {
			$prstatus_kd = $this->input->post('txtprstatus_kd', true);
			$prstatus_nama = $this->input->post('txtprstatus_nama', true);
            $prstatus_level = $this->input->post('txtprstatus_level', true);
            $prstatus_label = $this->input->post('txtprstatus_label', true);
            $prstatus_icon = $this->input->post('txtprstatus_icon', true);
            $prstatus_sendemail = $this->input->post('txtprstatus_sendemail', true);
				
			$data = array(
				'prstatus_nama' => !empty($prstatus_nama)? $prstatus_nama:null,
				'prstatus_level' => !empty($prstatus_level)? $prstatus_level:null,
				'prstatus_label' => !empty($prstatus_label)? $prstatus_label:null,
				'prstatus_icon' => !empty($prstatus_icon)? $prstatus_icon:null,
				'prstatus_sendemail' => !empty($prstatus_sendemail)? $prstatus_sendemail:'false',
				'prstatus_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->tb_purchaserequisition_status->update_data(array('prstatus_kd' => $prstatus_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$actDel = $this->tb_purchaserequisition_status->delete_data($id);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
