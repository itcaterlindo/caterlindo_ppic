<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Purchaseorder_status extends MY_Controller {
	private $class_link = 'purchasing/purchase_manage/purchaseorder_status';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
        $this->load->model(['tb_purchaseorder_status']);
        $this->load->library('encryption');
	}

	public function index() {
        parent::administrator();
        parent::pnotify_assets();
		$this->table_box();
	}
	
	public function table_box(){
		parent::administrator();
        parent::pnotify_assets();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data (){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->tb_purchaseorder_status->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function form_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');
        
        if ($sts == 'edit'){
            $rowData = $this->tb_purchaseorder_status->get_by_param(array('postatus_kd'=>$id))->row_array();
            $data['rowData'] = $rowData;
		}
		
		/** Opsi send mail */
		$opsiSendemail = array('false' => 'Tidak', 'true'=> 'Ya');

        $data['sts'] = $sts;
        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$data['opsiSendemail'] = $opsiSendemail;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpostatus_nama', 'Nama', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtpostatus_level', 'Level', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpostatus_nama' => (!empty(form_error('txtpostatus_nama')))?buildLabel('warning', form_error('txtpostatus_nama', '"', '"')):'',
				'idErrpostatus_level' => (!empty(form_error('txtpostatus_level')))?buildLabel('warning', form_error('txtpostatus_level', '"', '"')):'',
			);
			
		}else {
            $postatus_nama = $this->input->post('txtpostatus_nama', true);
            $postatus_level = $this->input->post('txtpostatus_level', true);
            $postatus_label = $this->input->post('txtpostatus_label', true);
            $postatus_icon = $this->input->post('txtpostatus_icon', true);
            $postatus_sendemail = $this->input->post('txtpostatus_sendemail', true);
				
			$data = array(
				'postatus_nama' => !empty($postatus_nama)? $postatus_nama:null,
				'postatus_level' => !empty($postatus_level)? $postatus_level:null,
				'postatus_label' => !empty($postatus_label)? $postatus_label:null,
				'postatus_icon' => !empty($postatus_icon)? $postatus_icon:null,
				'postatus_sendemail' => !empty($postatus_sendemail)? $postatus_sendemail:'false',
				'postatus_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->tb_purchaseorder_status->insert_data($data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpostatus_nama', 'Nama', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtpostatus_level', 'Level', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpostatus_nama' => (!empty(form_error('txtpostatus_nama')))?buildLabel('warning', form_error('txtpostatus_nama', '"', '"')):'',
				'idErrpostatus_level' => (!empty(form_error('txtpostatus_level')))?buildLabel('warning', form_error('txtpostatus_level', '"', '"')):'',
			);
			
		}else {
			$postatus_kd = $this->input->post('txtpostatus_kd', true);
			$postatus_nama = $this->input->post('txtpostatus_nama', true);
            $postatus_level = $this->input->post('txtpostatus_level', true);
            $postatus_label = $this->input->post('txtpostatus_label', true);
            $postatus_icon = $this->input->post('txtpostatus_icon', true);
            $postatus_sendemail = $this->input->post('txtpostatus_sendemail', true);
				
			$data = array(
				'postatus_nama' => !empty($postatus_nama)? $postatus_nama:null,
				'postatus_level' => !empty($postatus_level)? $postatus_level:null,
				'postatus_label' => !empty($postatus_label)? $postatus_label:null,
				'postatus_icon' => !empty($postatus_icon)? $postatus_icon:null,
				'postatus_sendemail' => !empty($postatus_sendemail)? $postatus_sendemail:'false',
				'postatus_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->tb_purchaseorder_status->update_data(array('postatus_kd' => $postatus_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$actDel = $this->tb_purchaseorder_status->delete_data($id);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
