<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Purchaseorder_users extends MY_Controller {
	private $class_link = 'purchasing/purchase_manage/purchaseorder_users';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
        $this->load->model(['tb_purchaseorder_user', 'tb_admin', 'tb_purchaseorder_status']);
        $this->load->library('encryption');
	}

	public function index() {
        parent::administrator();
        parent::pnotify_assets();
		$this->table_box();
	}
		
	public function table_box(){
		parent::administrator();
        parent::pnotify_assets();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data (){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->tb_purchaseorder_user->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function form_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');
        
        if ($sts == 'edit'){
            $rowData = $this->tb_purchaseorder_user->get_by_param_detail(array('pouser_kd'=>$id))->row_array();
            $data['rowData'] = $rowData;
        }

		/** Opsi User */
        $actopsiAdmin = $this->tb_admin->get_all()->result_array();
		$opsiAdmin[''] = '-- Pilih Admin --';
		foreach ($actopsiAdmin as $eachAdmin):
			$opsiAdmin[$eachAdmin['kd_admin']] = $eachAdmin['nm_admin'];
		endforeach;
		
		/** Opsi PO Status */
        $actopsiPOstatus = $this->tb_purchaseorder_status->get_all()->result_array();
		$opsiPOstatus[''] = '-- Pilih PO Status --';
		foreach ($actopsiPOstatus as $eachPOstatus):
			$opsiPOstatus[$eachPOstatus['postatus_kd']] = $eachPOstatus['postatus_nama'];
        endforeach;

        $data['sts'] = $sts;
        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$data['opsiAdmin'] = $opsiAdmin;
		$data['opsiPOstatus'] = $opsiPOstatus;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpouser_user', 'User', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtpouser_email', 'Email', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtpostatus_kd', 'Group', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpouser_user' => (!empty(form_error('txtpouser_user')))?buildLabel('warning', form_error('txtpouser_user', '"', '"')):'',
				'idErrpouser_email' => (!empty(form_error('txtpouser_email')))?buildLabel('warning', form_error('txtpouser_email', '"', '"')):'',
				'idErrpostatus_kd' => (!empty(form_error('txtpostatus_kd')))?buildLabel('warning', form_error('txtpostatus_kd', '"', '"')):'',
			);
			
		}else {
            $pouser_user = $this->input->post('txtpouser_user', true);
            $pouser_email = $this->input->post('txtpouser_email', true);
            $postatus_kd = $this->input->post('txtpostatus_kd', true);
				
			$data = array(
				'pouser_user' => $pouser_user,
				'pouser_email' => $pouser_email,
				'postatus_kd' => $postatus_kd,
				'pouser_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->tb_purchaseorder_user->insert_data($data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpouser_user', 'User', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtpouser_email', 'Email', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtpostatus_kd', 'Group', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpouser_user' => (!empty(form_error('txtpouser_user')))?buildLabel('warning', form_error('txtpouser_user', '"', '"')):'',
				'idErrpouser_email' => (!empty(form_error('txtpouser_email')))?buildLabel('warning', form_error('txtpouser_email', '"', '"')):'',
				'idErrpostatus_kd' => (!empty(form_error('txtpostatus_kd')))?buildLabel('warning', form_error('txtpostatus_kd', '"', '"')):'',
			);
			
		}else {
			$pouser_kd = $this->input->post('txtpouser_kd', true);
			$pouser_user = $this->input->post('txtpouser_user', true);
            $pouser_email = $this->input->post('txtpouser_email', true);
            $postatus_kd = $this->input->post('txtpostatus_kd', true);

			$data = array(
				'pouser_user' => $pouser_user,
				'pouser_email' => $pouser_email,
				'postatus_kd' => $postatus_kd,
				'pouser_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->tb_purchaseorder_user->update_data(array('pouser_kd' => $pouser_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$actDel = $this->tb_purchaseorder_user->delete_data($id);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
