<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Purchase_order_report extends MY_Controller {
	private $class_link = 'purchasing/purchase_order/purchase_order_report';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_purchaseorder', 'tm_salesorder', 'td_purchaseorder_detail', 'tb_admin', 'db_hrm/tb_karyawan', 'tb_purchaseorder_logstatus', 'tm_currency']);
    }

    public function index(){
        $this->load->library('Pdf');

        $id = $this->input->get('id');

        if (!empty($id)){
            $id = url_decrypt($id);
            $rowPO = $this->tm_purchaseorder->get_by_id_detail ($id)->row_array();
            $resultPOdetail = $this->td_purchaseorder_detail->get_by_param_detail(array('po_kd'=>$id))->result_array();

            /** Originator */
            $originator = '';
            $rowAdmin = $this->tb_admin->get_by_param(array('kd_admin' => $rowPO['po_originator']))->row_array();
            $originator = $rowAdmin['nm_admin'];
            if (!empty($originator['kd_karyawan'])){
                $rowKaryawan = $this->tb_karyawan->get_by_param(['kd_karyawan' => $originator['kd_karyawan']])->row_array();
                $originator = $rowKaryawan['nm_karyawan'];
            }

            /** Aproved by and time aproved */
            $resultLogStatus = array();
            $logStatus = $this->tb_purchaseorder_logstatus->get_max_log($id);
            if (!empty($logStatus)){
                $logStatus = $logStatus->result_array();
                $arrAdminkd = array();   
                foreach ($logStatus as $each){
                    if (!empty($each['kd_karyawan'])){
                        if (in_array($each['kd_karyawan'], $arrAdminkd)){
                            continue;
                        } else {
                            $arrAdminkd[] = $each['kd_karyawan'];
                        }
                    }
                }
                /** Get karyawan kd */
                if (!empty($arrAdminkd)){
                    $arrKaryawan = $this->tb_karyawan->get_where_in ('kd_karyawan', $arrAdminkd)->result_array();
                }
                foreach ($logStatus as $eachStatus){
                    $nm_karyawan = '-';
                    if (isset($arrKaryawan)){
                        foreach ($arrKaryawan as $eachKary){
                            if ($eachKary['kd_karyawan'] == $eachStatus['kd_karyawan'] ){
                                $nm_karyawan = $eachKary['nm_karyawan'];
                            }
                        }
                    }
                    $resultLogStatus [] = array(
                        'postatus_kd' => $eachStatus['postatus_kd'],
                        'postatus_nama' => $eachStatus['postatus_nama'],
                        'tgl_maxtrans' => $eachStatus['pologstatus_tglinput'],
                        'nm_karyawan' => $nm_karyawan,
                    );
                }
            }
            
            $data['id'] = $id;
            $data['class_link'] = $this->class_link;
            $data['rowPO'] = $rowPO;
            $data['originator'] = $originator;
            $data['resultPOdetail'] = $resultPOdetail;
            $data['resultLogStatus'] = $resultLogStatus;
            $data['logs'] = $this->tb_purchaseorder_logstatus->get_log_printout($id)->result_array();
            $data['currency_helper'] = $this->tm_currency->format_helper_currency_all();
            
            $this->load->view('page/'.$this->class_link.'/purchase_order_pdf', $data);
        }
        
    }

    	
}
