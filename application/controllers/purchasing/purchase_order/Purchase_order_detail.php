<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Purchase_order_detail extends MY_Controller {
    private $head_class_link = 'purchasing/purchase_order/purchase_order_main';
    private $class_link = 'purchasing/purchase_order/purchase_order_detail';
    private $class_linkReport = 'purchase/purchase_order/purchase_order_report';
    private $wf_kd = 3;
    private $tbl_name = 'tm_purchaseorder';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_purchaseorder', 'td_purchaseorder_detail', 'tb_admin', 'tb_purchaseorder_logstatus', 'tb_relasi_popr', 'td_purchaserequisition_detail', 
            'tm_purchaserequisition', 'tm_currency', 'td_rawmaterial_goodsreceive', 'tm_workflow', 'td_workflow_transition', 'td_workflow_transition_notification']);
    }

    public function index() {
        $id = $this->input->get('id', true);
        parent::administrator();
        parent::pnotify_assets();
        if (!empty($id)){
            $id = url_decrypt($id);
            $this->table_box($id);
        }
    }

    public function table_box($id){
		$data['class_link'] = $this->class_link;
        $data['head_class_link'] = $this->head_class_link;
        $data['id'] = $id;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }

        $id = $this->input->get('id', true);

        if (!empty($id)){
            $rowPO =  $this->db->select('a.*, b.*, c.nm_admin, d.suplier_nama, d. suplier_kode')
                ->from('tm_purchaseorder as a')
                ->join('td_workflow_state as b', 'a.wfstate_kd=b.wfstate_kd', 'left')
                ->join('tb_admin as c', 'c.kd_admin=a.po_originator', 'left')
                ->join('tm_suplier as d', 'a.suplier_kd=d.suplier_kd', 'left')
                ->where('a.po_kd', $id)
                ->get()->row_array();
            $resultPOdetail = $this->td_purchaseorder_detail->get_by_param_detail(array('po_kd' => $id))->result_array();
            $data['rowPO'] = $rowPO;
            $data['resultPOdetail'] = $resultPOdetail;
            $data['id'] = $id;
            $data['class_link'] = $this->class_link;
            $data['generateButtonAction'] = $this->tm_workflow->generate_button($id, $this->wf_kd, $rowPO['wfstate_kd']);
            $data['result_relasipopr'] = $this->tb_relasi_popr->get_by_param_detail (array('tb_relasi_popr.po_kd' => $id))->result_array();
            $data['currency_helper'] = $this->tm_currency->format_helper_currency_all ();
            $data['result_goodsreceive'] = $this->td_rawmaterial_goodsreceive->get_by_param(['po_kd' => $id])->result_array();

            $this->load->view('page/'.$this->class_link.'/table_main', $data);
        }
    }

    public function table_pr_box(){
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }

        $id = $this->input->get('id', true);
        $batch_pr = $this->input->get('batch_pr', true);

        $data['id'] = $id;
        $data['batch_pr'] = $batch_pr;
        $data['class_link'] = $this->class_link;
        
		$this->load->view('page/'.$this->class_link.'/table_pr_box', $data);
    }

    public function table_pr_main() {
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }

        $id = $this->input->get('id', true);
        $batch_pr = $this->input->get('batch_pr', true);

        $in = explode('_', $batch_pr);
        
        $rowPO = $this->tm_purchaseorder->get_by_id_detail($id)->row_array();
        $data['rowPO'] = $rowPO;
        $data['po_statusLabel'] = $this->tm_purchaseorder->labelStatus($rowPO['postatus_nama'], $rowPO['postatus_label']);
        $data['class_link'] = $this->class_link;
        $data['resultPRdetail'] = $this->td_purchaserequisition_detail->get_where_in_detail ('td_purchaserequisition_detail.pr_kd', $in)->result_array();
        $data['result_relasipopr'] = $this->tb_relasi_popr->get_by_param_detail (array('tb_relasi_popr.po_kd' => $id))->result_array();

		$this->load->view('page/'.$this->class_link.'/table_pr_main', $data);
    }

    public function view_log() {
		$id = $this->input->get('id', true);

		$data['result'] = $this->db->select('tb_purchaseorder_logstatus.*, td_workflow_transition.*, tb_admin.nm_admin, state_source.wfstate_nama as state_source, state_dst.wfstate_nama as state_dst')
                ->from('tb_purchaseorder_logstatus')
				->join('td_workflow_transition', 'tb_purchaseorder_logstatus.wftransition_kd=td_workflow_transition.wftransition_kd', 'left')
				->join('td_workflow_state as state_source', 'state_source.wfstate_kd=td_workflow_transition.wftransition_source', 'left')
				->join('td_workflow_state as state_dst', 'state_dst.wfstate_kd=td_workflow_transition.wftransition_destination', 'left')
				->join('tb_admin', 'tb_purchaseorder_logstatus.admin_kd=tb_admin.kd_admin', 'left')
				->where('tb_purchaseorder_logstatus.po_kd', $id)
				->order_by('tb_purchaseorder_logstatus.pologstatus_tglinput', 'desc')
				->get()->result_array();

		$this->load->view('page/'.$this->class_link.'/view_log', $data);
	}

    public function ubah_status_main(){
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }

        $akses = $this->input->get('akses', true);
        $po_kd = $this->input->get('po_kd', true);
        $postatus_level = $this->input->get('level', true);

        /** Opsi PO Status */
        $actopsiPOstatus = $this->tb_purchaseorder_status->get_all()->result_array();
		$opsiPOstatus[''] = '-- Pilih PO Status --';
		foreach ($actopsiPOstatus as $eachPOstatus):
			$opsiPOstatus[$eachPOstatus['postatus_kd']] = $eachPOstatus['postatus_nama'];
        endforeach;

        $data['class_link'] = $this->class_link;
        $data['po_kd'] = $po_kd;
        $data['postatus_level'] = $postatus_level;
        $data['opsiPOstatus'] = $opsiPOstatus;
        $data['akses'] = $akses;
        
        $this->load->view('page/'.$this->class_link.'/ubah_status_main', $data);
    }

    public function formubahstate_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id');
		$wftransition_kd = $this->input->get('wftransition_kd');
		
		$data['id'] = $id;
		$data['wftransition_kd'] = $wftransition_kd;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/formubahstate_main', $data);
	}

    public function log_status_main(){
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }

        $id = $this->input->get('id', true);

        if (!empty($id)){
            $resultData = $this->tb_purchaseorder_logstatus->get_by_param_detail (['tb_purchaseorder_logstatus.po_kd' => $id]);
            
            $data['id'] = $id;
            $data['resultData'] = $resultData->result_array();
    
            $this->load->view('page/'.$this->class_link.'/log_status_main', $data);
        }
    }
    public function rollback_status_main(){
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }

        $id = $this->input->get('id', true);

        if (!empty($id)){
            $resultData = $this->tb_purchaseorder_logstatus->get_by_param_detail (['tb_purchaseorder_logstatus.po_kd' => $id]);
            
            $data['id'] = $id;
            $data['resultData'] = $resultData->result_array();
    
            $this->load->view('page/'.$this->class_link.'/log_status_main', $data);
        }
    }

    private function action_special ($action = null, $aParam = []) {
        $act = true;
        $admin_kd = $this->session->userdata('kd_admin');
        switch ($action) {
            case 'PO_UPDATE_TGL' :
                $act = $this->tm_purchaseorder->update_data(['po_kd' => $aParam['po_kd']], ['po_tanggal' => date('Y-m-d'), 'admin_kd' => $admin_kd]);
                break;
            case 'PO_UPDATE_CANCEL_1' :
                $act = $this->tm_purchaseorder->update_data(['po_kd' => $aParam['po_kd']], ['po_cancelorder' => '1', 'po_closeorder' => '1', 'po_tgledit' => date('Y-m-d H:i:s'), 'admin_kd' => $admin_kd]);
                break;
            case 'PO_UPDATE_CANCEL_0' :
                $act = $this->tm_purchaseorder->update_data(['po_kd' => $aParam['po_kd']], ['po_cancelorder' => '0', 'po_closeorder' => '1', 'po_tgledit' => date('Y-m-d H:i:s'), 'admin_kd' => $admin_kd]);
                break;
            case 'PO_ROLLBACK_SAP' :
                $act = $this->tm_purchaseorder->update_data(['po_kd' => $aParam['po_kd']], ['postatus_kd' => '4', 'wfstate_kd' => '14', 'po_tgledit' => date('Y-m-d H:i:s'), 'admin_kd' => $admin_kd]);
                break;
            case 'PO_UPDATE_CLOSEORDER_0':
                $act = $this->tm_purchaseorder->update_data(['po_kd' => $aParam['po_kd']], ['po_closeorder' => '0', 'po_tgledit' => date('Y-m-d H:i:s'), 'admin_kd' => $admin_kd]);
                break;
            case 'PO_UPDATE_CLOSEORDER_1':
                $act = $this->tm_purchaseorder->update_data(['po_kd' => $aParam['po_kd']], ['po_closeorder' => '1', 'po_tgledit' => date('Y-m-d H:i:s'), 'admin_kd' => $admin_kd]);
                break;
            case 'PO_UPDATE_PR_CLOSEORDER':
                $poprs = $this->tb_relasi_popr->get_by_param (['po_kd' => $aParam['po_kd']])->result_array();
                $pr_kds = array_column($poprs, 'pr_kd');
                if (!empty($pr_kds)) {
                    foreach($pr_kds as $pr_kd) {
                        $act = $this->tm_purchaserequisition->update_close_order($pr_kd);
                    }
                }
                break;
            // Untuk update pr yg kembali ke editing
            case 'PO_UPDATE_PR_CLOSEORDER_0' :
                $poprs = $this->tb_relasi_popr->get_by_param (['po_kd' => $aParam['po_kd']])->result_array();
                $dataBatch = [];
                if (!empty($poprs)) {
                    foreach ($poprs as $popr) {
                        $dataBatch[] = [
                            'pr_kd' => $popr['pr_kd'],
                            'pr_closeorder' => '0',
                        ];
                    }
                }
                if (!empty($dataBatch)) {
                    $act = $this->tm_purchaserequisition->update_batch($dataBatch, 'pr_kd');
                }

                break;
            default :
                $act = true;
        }
        
        return $act;
    }

    public function action_change_state() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtid', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtwftransition_kd', 'Transition', 'required', ['required' => '{field} tidak boleh kosong!']);		

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrrm_kd' => (!empty(form_error('txtid')))?buildLabel('warning', form_error('txtid', '"', '"')):'',
				'idErrrm_kd' => (!empty(form_error('txtwftransition_kd')))?buildLabel('warning', form_error('txtwftransition_kd', '"', '"')):'',
			);
			
		}else {
			$po_kd = $this->input->post('txtid', true);
			$wftransition_kd = $this->input->post('txtwftransition_kd', true);
			$pologstatus_keterangan = $this->input->post('txtlog_note', true);


             $qData = $this->db->select('td_rawmaterial_goodsreceive.*')
                    ->from('td_rawmaterial_goodsreceive')
                    ->join('tm_purchaseorder as b', 'td_rawmaterial_goodsreceive.po_kd=b.po_kd', 'left')
                    ->where('b.po_kd', $po_kd)
                    ->get()->row_array();

            if(count((array)$qData) == 0){
                
                    $data = [
                        'po_kd' => $po_kd,
                        'wftransition_kd' => $wftransition_kd,
                        'pologstatus_user' => $this->session->userdata('kd_admin'),
                        'pologstatus_keterangan' => $pologstatus_keterangan,
                        'pologstatus_tglinput' => date('Y-m-d H:i:s'),
                        'admin_kd' => $this->session->userdata('kd_admin'),
                    ]; 
                    $this->db->trans_begin();
                    $po = $this->tm_purchaseorder->get_by_param(['po_kd' => $po_kd])->row_array();
                    $wftransition = $this->td_workflow_transition->get_by_param(['wftransition_kd' => $wftransition_kd])->row_array();
                    $dataUpdate = [
                        'wfstate_kd' => $wftransition['wftransition_destination'],
                        'po_tgledit' => date('Y-m-d H:i:s'),
                    ];
                    #email message notification
                   // $subject = 'Purchase Order - '.$po['po_no'];
                    $dtMessage['subjcet'] = $po['po_no'];
                    $dtMessage['text'] = 'Terdapat Purchase Order yang perlu approval Anda :';
                    $dtMessage['url'] = 'https://'.$_SERVER['HTTP_HOST'].base_url().$this->class_link.'?id='.url_encrypt($po_kd);
                    $dtMessage['urltext'] = 'Detail Purchase Order';
                    $dtMessage['keterangan'] = !empty($pologstatus_keterangan) ? $pologstatus_keterangan : null;
                   // $message = $this->load->view('templates/email/email_notification', $dtMessage, true);

                    #special action
                    if (!empty($wftransition['wftransition_action'])) {
                        $expAction = explode(';', $wftransition['wftransition_action']);
                        for ($i=0; $i<count($expAction); $i++){
                            $actSpecial = $this->action_special($expAction[$i], $data);
                        }
                    }

                    $act = $this->tb_purchaseorder_logstatus->insert_data($data);
                    $actUpdate = $this->tm_purchaseorder->update_data (['po_kd' => $po_kd], $dataUpdate);
                    //$actNotif = $this->td_workflow_transition_notification->generate_notification($wftransition_kd, $subject, $message);

                    $SttsNotif =  $this->td_workflow_transition_notification->getStatus_generate_notification($wftransition_kd);


                    $items = [];
                    if($wftransition_kd == '18'){
                        if (!empty($po_kd)){
                            $rowPO =  $this->db->select('a.*, b.*, c.nm_admin, d.suplier_nama, d.suplier_kode')
                                ->from('tm_purchaseorder as a')
                                ->join('td_workflow_state as b', 'a.wfstate_kd=b.wfstate_kd', 'left')
                                ->join('tb_admin as c', 'c.kd_admin=a.po_originator', 'left')
                                ->join('tm_suplier as d', 'a.suplier_kd=d.suplier_kd', 'left')
                                ->where('a.po_kd', $po_kd)
                                ->get()->row_array();
                            $resultPOdetail = $this->td_purchaseorder_detail->get_by_param_detail(array('po_kd' => $po_kd))->result_array();
                            $items['rowPO'] = $rowPO;
                            $items['resultPOdetail'] = $resultPOdetail;
                            $items['id'] = $po_kd;
                            $items['class_link'] = $this->class_link;
                            $result_relasipopr = $this->tb_relasi_popr->get_by_param_detail (array('tb_relasi_popr.po_kd' => $po_kd))->result_array();

                            if (!empty($result_relasipopr)) {
                                foreach ($result_relasipopr as $each_relasipopr){
                                    $arraypr_no[] = $each_relasipopr['pr_no'];
                                    $arraypr_kd[] = $each_relasipopr['pr_kd'];
                                }
                                $items['imppr_no'] = implode(',', $arraypr_no);
                                $items['imppr_kd'] = implode('_', $arraypr_kd);
                            }

                
                            // $this->load->view('page/'.$this->class_link.'/table_main', $data);
                        }
                    }
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
                }else{
                    $this->db->trans_commit();
                    $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'stts' =>  $SttsNotif, 'msg' => $dtMessage,  'data' => $items);
                }
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan, barang sudah di GR!');
            }
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

    public function action_closeorder () {
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $po_kd = $this->input->get('po_kd', true);

        if (!empty($po_kd)) {
            $act = $this->action_special ('PO_UPDATE_CLOSEORDER_1', ['po_kd' => $po_kd]);
            if ($act) {
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
        }else{
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'ID tidak ditemukan');
        }
        echo json_encode($resp);
    }

    public function action_rollbackcloseorder () {
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $po_kd = $this->input->get('po_kd', true);

        if (!empty($po_kd)) {
            $act = $this->action_special ('PO_UPDATE_CLOSEORDER_0', ['po_kd' => $po_kd]);
            if ($act) {
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
        }else{
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'ID tidak ditemukan');
        }
        echo json_encode($resp);
    }

    public function action_rollback_po () {
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $po_kd = $this->input->get('po_kd', true);

        if (!empty($po_kd)) {
            $act = $this->action_special ('PO_ROLLBACK_SAP', ['po_kd' => $po_kd]);
            if ($act) {
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
        }else{
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'ID tidak ditemukan');
        }
        echo json_encode($resp);
    }
    	
}
