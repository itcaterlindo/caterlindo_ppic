<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Purchase_order_main extends MY_Controller {
    private $class_link = 'purchasing/purchase_order/purchase_order_main';
    private $detail_class_link = 'purchasing/purchase_order/purchase_order_detail';
    private $wf_kd = 3;

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_purchaseorder', 'tm_suplier','td_suplier_katalog', 'td_purchaseorder_detail', 'tm_rawmaterial', '_mail_configuration', 'tm_purchaserequisition', 'tb_relasi_popr', 
            'td_purchaserequisition_detail', 'tb_purchaserequisition_status', 'td_rawmaterial_satuan', '_kursfiskal_configuration', 'td_workflow_state']);
    }

    public function index() {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();
        parent::datetimepicker_assets();
        $this->table_box();
    }

	public function form_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');
        $is_upn = $this->input->get('is_upn');

		$data['sts'] = $sts;
		$data['id'] = $id;
        $data['is_upn'] = $is_upn;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
    }
    
    public function form_master_main(){
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        $sts = $this->input->get('sts');
        $id = $this->input->get('id');
        $is_upn = $this->input->get('is_upn');

        $opsiPR = [];
        if ($sts == 'edit'){
            $act = $this->tm_purchaseorder->get_by_param (array('po_kd'=> $id))->row_array();			
            $data['rowData'] = $act;
            $resultpopr = $this->tb_relasi_popr->get_by_param_detail (array('tb_relasi_popr.po_kd' => $id))->result_array();
            foreach ($resultpopr as $eachpopr){
                $resultrelasipopr_kd [] = $eachpopr['pr_kd'];
                /** Untuk array PR selected dgn status finish */
                $opsiPR[$eachpopr['pr_kd']] = $eachpopr['pr_no'];
            }
            $data['PRSelected'] = !empty($resultrelasipopr_kd) ? $resultrelasipopr_kd : array();
            $po_nomor = $act['po_no'];
        }else{
            $po_nomor = $this->tm_purchaseorder->create_no();
        }

        /** Opsi Suplier */
        $opsiSuplier[null] = '-- Pilih Opsi --';
        $actopsiSuplier = $this->tm_suplier->get_all()->result_array();
		foreach ($actopsiSuplier as $eachSuplier):
			$opsiSuplier[$eachSuplier['suplier_kd']] = $eachSuplier['suplier_kode'].' | '.$eachSuplier['suplier_nama'];
        endforeach;

        /** Opsi PR */
        $arrOpsiSort = [];
        $actopsiPR = $this->tm_purchaserequisition->get_by_param (array('pr_closeorder <>' => '1'))->result_array();
        foreach ($actopsiPR as $each):
            $arrOpsiSort[] = array(
                'pr_no' => $each['pr_no'],
                'pr_kd' => $each['pr_kd'],
            );
        endforeach;
        krsort($arrOpsiSort);
		foreach ($arrOpsiSort as $eachPR):
			$opsiPR[$eachPR['pr_kd']] = $eachPR['pr_no'];
        endforeach;
        
        $data['po_nomor'] = $po_nomor;
        $data['opsiSuplier'] = $opsiSuplier;
        $data['opsiPR'] = $opsiPR;
        $data['po_kd'] = $id;
        $data['sts'] = $sts;
        $data['is_upn'] = $is_upn;
        $data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_master_main', $data);
    }
    
    public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
        $id = $this->input->get('id');
        
		if ($sts == 'edit'){
			$act = $this->tm_purchaseorder->get_by_param (array('po_kd'=> $id))->row_array();			
			$data['rowData'] = $act;
        }

        /** Master PO */
        $rowDataMasterPO = $this->db->select('tm_purchaseorder.*, tm_suplier.suplier_nama, tm_suplier.suplier_termpayment, tm_suplier.suplier_kode, tm_suplier.suplier_includeppn, tm_suplier.suplier_alamat, tm_suplier.suplier_telpon1, tm_suplier.kd_currency, tb_set_dropdown.nm_select, 
                    td_workflow_state.wfstate_nama, td_workflow_state.wfstate_badgecolor, tm_currency.currency_nm, tm_currency.currency_type')
                ->from('tm_purchaseorder')
                ->join('tm_suplier', 'tm_purchaseorder.suplier_kd=tm_suplier.suplier_kd', 'left')
                ->join('tb_set_dropdown', 'tm_suplier.badanusaha_kd = tb_set_dropdown.id', 'left')
                ->join('td_workflow_state', 'tm_purchaseorder.wfstate_kd=td_workflow_state.wfstate_kd', 'left')
                ->join('tb_admin', 'tm_purchaseorder.po_originator=tb_admin.kd_admin', 'left')
                ->join('tm_currency', 'tm_suplier.kd_currency=tm_currency.kd_currency', 'left')
                ->where('tm_purchaseorder.po_kd', $id)
                ->get()->row_array();
        $result_relasipopr = $this->tb_relasi_popr->get_by_param_detail (array('tb_relasi_popr.po_kd' => $id))->result_array();

        /** Opsi Jenis Material */
        $opsiJenisMaterial = array(
            'PR' => 'Purchase Requisition',
            'KTG' => 'Katalog',
            'STD' => 'Standart Material',
        );

        /** Opsi Jenis Referensi */
        $opsiReferensiSpecial = array(
            'STD' => 'Standart Material',
            'SPECIAL' => 'Special PO Sebelumnya',
        );

        /** Opsi Jenis PO */
        $opsiJenisPO = array(
            'material' => 'Material',
            'jasa' => 'Jasa',
        );

        /** Opsi Satuan */
		$actopsiSatuan = $this->td_rawmaterial_satuan->get_all()->result_array();
		foreach ($actopsiSatuan as $eachSatuan):
			$opsiSatuan[$eachSatuan['rmsatuan_kd']] = $eachSatuan['rmsatuan_nama'];
        endforeach;

        $data['opsiSatuan'] = $opsiSatuan;
        $data['opsiJenisMaterial'] = $opsiJenisMaterial;
        $data['opsiReferensiSpecial'] = $opsiReferensiSpecial;
        $data['opsiJenisPO'] = $opsiJenisPO;
        $data['rowDataMasterPO'] = $rowDataMasterPO;
        $data['result_relasipopr'] = $result_relasipopr;
		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
    }
    
	public function table_box(){
        $tahun = $this->input->get('Y');
        $bulan = $this->input->get('m');
        $wfstate_kd = $this->input->get('wfstate_kd');
        $pr_no = $this->input->get('pr_no');
        $suplier_kd = $this->input->get('suplier_kd');

        #opsi bulan
        $months = bulanIndo();
        $months = array_flip($months);
        $months['ALL'] = 'ALL';
        $data['months'] = $months;

		#opsi states
		$states = $this->td_workflow_state->get_by_param(['wf_kd' => $this->wf_kd])->result_array();
		$aState['ALL'] = 'ALL';
		foreach($states as $state) {
			$aState[$state['wfstate_kd']] = $state['wfstate_nama']; 
		}
		$data['states'] = $aState;

        #opsi suplier
        $supliers = $this->tm_suplier->get_all()->result_array();
        $aSuplier['ALL'] = 'ALL';
        foreach ($supliers as $suplier) {
            $aSuplier[$suplier['suplier_kd']] = $suplier['suplier_kode'].' | '.$suplier['suplier_nama'];
        }
        $data['supliers'] = $aSuplier;
        #params
        $tahun = !empty($tahun) ? $tahun : date('Y');
        $bulan = !empty($bulan) ? $bulan : date('m');
        $wfstate_kd = !empty($wfstate_kd) ? $wfstate_kd : 'ALL';
        $pr_no = !empty($pr_no) ? $pr_no : null;
        $suplier_kd = !empty($suplier_kd) ? $suplier_kd : 'ALL';
        $data['tahun'] = $tahun;
        $data['bulan'] = $bulan;
        $data['wfstate_kd'] = $wfstate_kd;
        $data['pr_no'] = $pr_no;
        $data['suplier_kd'] = $suplier_kd;
		$data['params'] = "?Y=$tahun&m=$bulan&wfstate_kd=$wfstate_kd&pr_no=$pr_no&suplier_kd=$suplier_kd";

		$data['class_link'] = $this->class_link;
		$data['detail_class_link'] = $this->detail_class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        
		$data['class_link'] = $this->class_link;
		$data['params'] = $this->input->get('params');
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }

    function table_data () {
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $tahun = $this->input->get('Y', true);
		$bulan = $this->input->get('m', true);
		$wfstate_kd = $this->input->get('wfstate_kd', true);
		$pr_no = $this->input->get('pr_no', true);
		$suplier_kd = $this->input->get('suplier_kd', true);

		$params = ['tahun' => $tahun, 'bulan' => $bulan, 'wfstate_kd' => $wfstate_kd, 'pr_no' => $pr_no, 'suplier_kd' => $suplier_kd];
        $data = $this->tm_purchaseorder->ssp_table2($params);

        echo json_encode($data);
    }

	public function table_data2() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $this->load->library(['ssp']);
        
		$data = $this->tm_purchaseorder->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
    }

    public function view_detail_box(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$po_kd = $this->input->get('po_kd');

		$data['url'] = base_url().'/purchasing/purchase_order/purchase_order_report?id='.$po_kd;
		
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/view_detail_box', $data);
	}
    
    public function form_currency () {
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
		}
		$kd_currency = $this->input->get('kd_currency');
        
        $data['kd_currency'] = $kd_currency;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/'.$this->class_link.'/form_currency', $data);
    }
    
    public function table_detail_main(){
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        $sts = $this->input->get('sts');
		$id = $this->input->get('id');

        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_detail_main', $data);
    }

    public function table_detail_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $this->load->library(['ssp']);

        $sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data = $this->td_purchaseorder_detail->ssp_table($id);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
    }

    public function table_partial_katalog(){
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
        
        $id = $this->input->get('id', true);

        if (!empty($id)){
            $rowPO = $this->tm_purchaseorder->get_by_param (array('po_kd' => $id))->row_array();
            $resultData = $this->td_suplier_katalog->get_detail_by_param(array('tm_suplier.suplier_kd' => $rowPO['suplier_kd']))->result_array();

            $data['id'] = $id;
            $data['resultData'] = $resultData;
            $this->load->view('page/'.$this->class_link.'/table_partial_katalog', $data);
        }
    }

    private function groupingDetailPO($data){
		$groups = array();
		foreach ($data as $item) {
			$key = $item['prdetail_kd'];
			/** jika tidak ada key */
			if (!array_key_exists($key, $groups)) {
				$groups[$key] = array(
					'rm_kd' => $item['rm_kd'],
					'prdetail_kd' => $key,
					'podetail_qty' => $item['podetail_qty'],
				);
			} else {
				$groups[$key]['podetail_qty'] = $groups[$key]['podetail_qty'] + $item['podetail_qty'];
			}
		}
		
		foreach ($groups as $each){
			$groupResult[] = $each;
		}
		return $groupResult;
	}

    public function table_partial_pr(){
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
        
        $id = $this->input->get('id', true);
        $pr_kd = $this->input->get('pr_kd', true);

        if (!empty($id) && !empty($pr_kd)){
            $exp_pr_kd = explode('_', $pr_kd);
            $resultPRdetails = $this->td_purchaserequisition_detail->get_where_in_detail ('td_purchaserequisition_detail.pr_kd', $exp_pr_kd)->result_array();
            /** get PO detail dari yang sudah ambil dari PR ini  
             * cek apakah status PO tidak "cancel"
            */
            $resultpo_kd = $this->tb_relasi_popr->get_where_in('pr_kd', $exp_pr_kd)->result_array();
            foreach($resultpo_kd as $eachpo_kd){
                $arraypo_kd [] = $eachpo_kd['po_kd'];
            }
            $resultPOdetails = $this->td_purchaseorder_detail->get_where_in ('td_purchaseorder_detail.po_kd', $arraypo_kd)->result_array();
            foreach ($resultPOdetails as $eachPO){
                /** jika status PO yang sudah ambil memliki status tidak "cancel" */
                if ($eachPO['postatus_kd'] != 0){ 
                    $POaktifResult[] = $eachPO;
                }
            }
            $groupPOdetails = array();
            if (!empty($POaktifResult)){
                $groupPOdetails = $this->groupingDetailPO($POaktifResult);
            }
            

            $data['id'] = $id;
            $data['resultPRdetails'] = $resultPRdetails;
            $data['resultPOdetails'] = !empty($groupPOdetails) ? $groupPOdetails:array();
            $this->load->view('page/'.$this->class_link.'/table_partial_pr', $data);
        }
    }

    public function table_partial_material(){
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }

        $id = $this->input->get('id', true);

        if (!empty($id)){
            $resultData = $this->tm_rawmaterial->get_all_detail_material()->result_array();

            $data['id'] = $id;
            $data['resultData'] = $resultData;
            $this->load->view('page/'.$this->class_link.'/table_partial_material', $data);
        }
    }

    /** untuk menampilkn partial item dari seluruh list PO sebelumnya */
    public function table_partial_special(){
        $id = $this->input->get('id', true);

        if (!empty($id)){
            $resultData = $this->td_purchaseorder_detail->get_by_param(array('rm_kd' => 'SPECIAL'))->result_array();

            $data['id'] = $id;
            $data['resultData'] = $resultData;
            $this->load->view('page/'.$this->class_link.'/table_partial_special', $data);
        }
    }
    
    public function getDetailPO(){
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
        $id = $this->input->get('id');
        if (!empty($id)){
            $act = $this->td_purchaseorder_detail->get_by_param_detail(array('td_purchaseorder_detail.podetail_kd'=> $id));
            if ($act->num_rows() > 0 ){
                $act = $act->row_array();
                $podetail_tgldeliveryFormat = $act['podetail_tgldelivery'];
                if (!empty($podetail_tgldeliveryFormat)){
                    $podetail_tgldeliveryFormat = format_date($podetail_tgldeliveryFormat, 'd-m-Y');
                }
                $rmsatuan_namadef = $this->tm_rawmaterial->get_detail_material(['tm_rawmaterial.rm_kd' => $act['rm_kd']])->row_array();
                $actMerge = array_merge($act, array('podetail_tgldeliveryFormat'=>$podetail_tgldeliveryFormat), ['rmsatuan_namadefault' => $rmsatuan_namadef['rmsatuan_nama']]);
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => '', 'data' => $actMerge);
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Tidak Ditemukan');
            }
        }else{
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Kosong');
        }

        echo json_encode($resp);
    }

    public function getMaterial(){
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
        $id = $this->input->get('id');
        $po_kd = $this->input->get('po_kd');

        if (!empty($id)){
            $act = $this->tm_rawmaterial->get_detail_material (array('tm_rawmaterial.rm_kd' => $id));
            if ($act->num_rows() > 0 ){
                $act = $act->row_array();
                /** Cek Katalog */
                $rowPO = $this->tm_purchaseorder->get_by_param(array('po_kd' => $po_kd))->row_array();
                $actKatalog = $this->db->select('td_suplier_katalog.*, td_rawmaterial_satuan.rmsatuan_kd')
                        ->where(array('suplier_kd' => $rowPO['suplier_kd'], 'rm_kd' => $id))
                        ->from('td_suplier_katalog')
                        ->join('td_rawmaterial_satuan', 'td_suplier_katalog.katalog_satuankd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
                        ->get();
                if ($actKatalog->num_rows() != 0){
                    $actKatalog = $actKatalog->row_array();
                    $act = array_merge($act, $actKatalog);
                }
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => '', 'data' => $act);
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Tidak Ditemukan');
            }
        }else{
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Kosong');
        }

        echo json_encode($resp);
    }

    public function getDetailPR(){
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
        $id = $this->input->get('id');
        $po_kd = $this->input->get('po_kd');

        if (!empty($id)){
            $act = $this->td_purchaserequisition_detail->get_by_id_detail ($id);
            $actPO = $this->tm_purchaseorder->get_by_param (array('po_kd' => $po_kd))->row_array();
            if ($act->num_rows() > 0 ){
                $act = $act->row_array();

                /** Cek satuan secondary and konversi */
                $rm = $this->tm_rawmaterial->get_detail_material (['tm_rawmaterial.rm_kd' => $act['rm_kd']])->row_array();
                $rm_konversiqty = 1;
                if ($act['rmsatuan_kd'] == $rm['rmsatuansecondary_kd']) {
                    $rm_konversiqty = $rm['rm_konversiqty'];
                }
                $act = array_merge($act, ['rm_konversiqty' => $rm_konversiqty]);

                /** Cek tgl duedate PR */
                $formatDuedate = array('formatDuedate' =>  empty($act['prdetail_duedate']) ? $act['prdetail_duedate'] : format_date($act['prdetail_duedate'], 'd-m-Y'));
                $act = array_merge($act, $formatDuedate);

                /** Cek Katalog */
                $actKatalog = $this->db->select('td_suplier_katalog.*, td_rawmaterial_satuan.rmsatuan_kd')
                        ->where(array('suplier_kd' => $actPO['suplier_kd'], 'rm_kd' => $act['rm_kd']))
                        ->from('td_suplier_katalog')
                        ->join('td_rawmaterial_satuan', 'td_suplier_katalog.katalog_satuankd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
                        ->get();
                if ($actKatalog->num_rows() != 0){
                    $actKatalog = $actKatalog->row_array();
                    $act = array_merge($act, $actKatalog);
                }
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => '', 'data' => $act);
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Tidak Ditemukan');
            }
        }else{
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Kosong');
        }

        echo json_encode($resp);
    }

    public function get_currency () {
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $kd_currency = $this->input->get('kd_currency');
        $tgl_currency = $this->input->get('tgl_currency');

        $row = $this->_kursfiskal_configuration->getby_kdcurrency($kd_currency, $tgl_currency);
        echo json_encode($row);
    }
   
	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpodetail_nama', 'Nama Material', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpodetail_harga', 'Harga Material', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpodetail_qty', 'Qty Material', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtrmsatuan_kd', 'Satuan', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
                'idErrpodetail_nama' => (!empty(form_error('txtpodetail_nama')))?buildLabel('warning', form_error('txtpodetail_nama', '"', '"')):'',
                'idErrpodetail_harga' => (!empty(form_error('txtpodetail_harga')))?buildLabel('warning', form_error('txtpodetail_harga', '"', '"')):'',
                'idErrpodetail_qty' => (!empty(form_error('txtpodetail_qty')))?buildLabel('warning', form_error('txtpodetail_qty', '"', '"')):'',
                'idErrmsatuan_kd' => (!empty(form_error('txtrmsatuan_kd')))?buildLabel('warning', form_error('txtrmsatuan_kd', '"', '"')):'',
            );
			
		}else {
            $po_kd = $this->input->post('txtpo_kd', true);
            $rm_kd = $this->input->post('txtrm_kd', true);
            $podetail_nama = $this->input->post('txtpodetail_nama', true);
            $podetail_deskripsi = $this->input->post('txtpodetail_deskripsi', true);
            $podetail_spesifikasi = $this->input->post('txtpodetail_spesifikasi', true);
            $podetail_jenis = $this->input->post('txtpodetail_jenis', true);
            $podetail_harga = $this->input->post('txtpodetail_harga', true);
            $podetail_qty = $this->input->post('txtpodetail_qty', true);
            $podetail_konversi = $this->input->post('txtpodetail_konversi', true);
            $podetail_tgldelivery = $this->input->post('txtpodetail_tgldelivery', true);
            $prdetail_kd = $this->input->post('txtprdetail_kd', true);
            $rmsatuan_kd = $this->input->post('txtrmsatuan_kd', true);
            $podetail_remark = $this->input->post('txtpodetail_remark', true);
            $podetail_konversicurrency = $this->input->post('txtpodetail_konversicurrency', true);
            $kd_currency = $this->input->post('txtkd_currency', true);

            #podetail tgl delivery
            $podetail_tgldelivery = empty($podetail_tgldelivery) ? $podetail_tgldelivery : format_date($podetail_tgldelivery, 'Y-m-d');
            /** Konversi PO jika lebih dari 0 makan rubah menjadi 1 */
            if ($podetail_konversi <=0 ) {
                $podetail_konversi = 1;
            }

            /** Buat Katalog untuk material yang belum ada di katalog suplier
             * apabila sudah ada di katalog dengan kode rm tersebut maka update harga saja
             * untuk material dengan non SPECIAL kode
             */
            $katalog_kd = null;
            if ($rm_kd != 'SPECIAL'){
                $suplier_kd = $this->tm_purchaseorder->get_by_param (array('po_kd'=>$po_kd))->row();
                $suplier_kd = $suplier_kd->suplier_kd;
                $katalog_kd = $this->td_suplier_katalog->create_katalog($suplier_kd, $rm_kd, $podetail_harga, $podetail_konversi, $rmsatuan_kd, $kd_currency);
            }
			
			$data = array(
                'podetail_kd' => $this->td_purchaseorder_detail->create_code(),
                'po_kd' => $po_kd,
                'katalog_kd' => $katalog_kd,
                'rm_kd' => !empty($rm_kd) ? $rm_kd:null,
                'prdetail_kd' => !empty($prdetail_kd) ? $prdetail_kd:null,
                'kd_currency' => !empty($kd_currency) ? $kd_currency:null,
                'podetail_nama' => !empty($podetail_nama) ? $podetail_nama:null,
                'podetail_deskripsi' => !empty($podetail_deskripsi) ? $podetail_deskripsi:null,
                'podetail_spesifikasi' => !empty($podetail_spesifikasi) ? $podetail_spesifikasi:null,
                'podetail_jenis' => $podetail_jenis,
                'podetail_harga' => !empty($podetail_harga) ? $podetail_harga:0,
                'podetail_qty' => !empty($podetail_qty) ? $podetail_qty:1,
                'rmsatuan_kd' => !empty($rmsatuan_kd) ? $rmsatuan_kd:null,
                'podetail_konversicurrency' => !empty($podetail_konversicurrency) ? $podetail_konversicurrency : 1,
                'podetail_jmlharga' => $podetail_harga * $podetail_qty,
                'podetail_konversi' => $podetail_konversi,
                'podetail_tgldelivery' => $podetail_tgldelivery,
                'podetail_remark' => !empty($podetail_remark) ? $podetail_remark : null,
				'podetail_tglinput' => date('Y-m-d H:i:s'),
				'podetail_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);
            $this->db->trans_start();

            $act = $this->td_purchaseorder_detail->insert_data($data);

            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE){
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }else{
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
    }

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpodetail_nama', 'Nama Material', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpodetail_harga', 'Harga Material', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpodetail_qty', 'Qty Material', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtrmsatuan_kd', 'Satuan', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrrm_nama' => (!empty(form_error('txtpodetail_nama')))?buildLabel('warning', form_error('txtpodetail_nama', '"', '"')):'',
				'idErrpodetail_harga' => (!empty(form_error('txtpodetail_harga')))?buildLabel('warning', form_error('txtpodetail_harga', '"', '"')):'',
				'idErrpodetail_qty' => (!empty(form_error('txtpodetail_qty')))?buildLabel('warning', form_error('txtpodetail_qty', '"', '"')):'',
                'idErrmsatuan_kd' => (!empty(form_error('txtrmsatuan_kd')))?buildLabel('warning', form_error('txtrmsatuan_kd', '"', '"')):'',
			);
			
		}else {
            $podetail_kd = $this->input->post('txtpodetail_kd', true);
            $podetail_nama = $this->input->post('txtpodetail_nama', true);
            $podetail_deskripsi = $this->input->post('txtpodetail_deskripsi', true);
            $podetail_spesifikasi = $this->input->post('txtpodetail_spesifikasi', true);
            $podetail_jenis = $this->input->post('txtpodetail_jenis', true);
            $podetail_harga = $this->input->post('txtpodetail_harga', true);
            $podetail_qty = $this->input->post('txtpodetail_qty', true);
            $podetail_konversi = $this->input->post('txtpodetail_konversi', true);
            $katalog_kd = $this->input->post('txtkatalog_kd', true);
            $podetail_tgldelivery = $this->input->post('txtpodetail_tgldelivery', true);
            $rmsatuan_kd = $this->input->post('txtrmsatuan_kd', true);
            $podetail_remark = $this->input->post('txtpodetail_remark', true);
            $podetail_konversicurrency = $this->input->post('txtpodetail_konversicurrency', true);
            $kd_currency = $this->input->post('txtkd_currency', true);

            if (!empty($podetail_tgldelivery)){
                $podetail_tgldelivery = format_date($podetail_tgldelivery, 'Y-m-d');
            }else{
                $podetail_tgldelivery = null;
            }

            /** Konversi PO jika lebih dari 0 makan rubah menjadi 1 */
            if ($podetail_konversi <=0 ) {
                $podetail_konversi = 1;
            }

			$podetail_tgledit = date('Y-m-d H:i:s');
			$data = array(
                'podetail_nama' => !empty($podetail_nama) ? $podetail_nama:null,
                'podetail_deskripsi' => !empty($podetail_deskripsi) ? $podetail_deskripsi:null,
                'podetail_spesifikasi' => !empty($podetail_spesifikasi) ? $podetail_spesifikasi:null,
                'podetail_jenis' => $podetail_jenis,
                'podetail_harga' => !empty($podetail_harga) ? $podetail_harga:0,
                'podetail_qty' => !empty($podetail_qty) ? $podetail_qty:0,
                'rmsatuan_kd' => !empty($rmsatuan_kd) ? $rmsatuan_kd:null,
                'podetail_konversi' => $podetail_konversi,
                'podetail_tgldelivery' => $podetail_tgldelivery,
                'podetail_jmlharga' => $podetail_harga * $podetail_qty,
                'podetail_remark' => !empty($podetail_remark) ? $podetail_remark : null,
                'kd_currency' => !empty($kd_currency) ? $kd_currency:null,
                'podetail_konversicurrency' => !empty($podetail_konversicurrency) ? $podetail_konversicurrency : 1,
				'podetail_tgledit' => $podetail_tgledit,
				'admin_kd' => $this->session->userdata('kd_admin'),
            );
            
            $this->db->trans_start();

            $act = $this->td_purchaseorder_detail->update_data(array('podetail_kd' => $podetail_kd), $data);
            
            /** Update katalog */
            if (!empty($katalog_kd)){
                $dataKatalog = array('katalog_kd'=> $katalog_kd, 'katalog_harga' => $podetail_harga, 'katalog_konversi' => $podetail_konversi, 'kd_currency' => $kd_currency, 'katalog_satuankd' => $rmsatuan_kd, 'katalog_tgledit' => $podetail_tgledit);
                $actKatalog = $this->td_suplier_katalog->update_harga($dataKatalog);
            }
           
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE){
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }else{
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
    }
    
    public function action_master_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtsuplier_kd', 'Suplier', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtpo_no', 'No', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrsuplier_kd' => (!empty(form_error('txtsuplier_kd')))?buildLabel('warning', form_error('txtsuplier_kd', '"', '"')):'',
				'idErrpo_no' => (!empty(form_error('txtpo_no')))?buildLabel('warning', form_error('txtpo_no', '"', '"')):'',
			);
			
		}else {
            $po_kd = $this->input->post('txtpo_kd', true);
            $suplier_kd = $this->input->post('txtsuplier_kd', true);
            $pr_kd = $this->input->post('txtpr_kd', true);
            $po_note = $this->input->post('txtpo_note', true);
            $po_note = $this->security->xss_clean($po_note);
            $po_no = $this->input->post('txtpo_no', true);

            
            $data = array(
                'po_no' => !empty($po_no) ? $po_no:null,
                'suplier_kd' => !empty($suplier_kd) ? $suplier_kd:null,
                'postatus_kd' => 1,
                'po_note' => !empty($po_note) ? $po_note : null,
                'admin_kd' => $this->session->userdata('kd_admin'),
            );

            if (empty($po_kd)) {
                #add
                $wfstate = $this->td_workflow_state->get_by_param(['wf_kd' => $this->wf_kd, 'wfstate_initial' => '1'])->row_array();
                $po_kd = $this->tm_purchaseorder->create_code();
                $arrAdd = [
                    'po_kd' => $po_kd,
                    'wf_kd' => $this->wf_kd,
                    'wfstate_kd' => $wfstate['wfstate_kd'],
                    'po_closeorder' => '1',
                    'po_cancelorder' => '0',
                    'po_tanggal' => date('Y-m-d'),
                    'po_originator' => $this->session->userdata('kd_admin'),
                    'po_tglinput' => date('Y-m-d H:i:s'),
                ];
                $data = array_merge($data, $arrAdd);

                $act = $this->tm_purchaseorder->insert_data($data);
            }else{
                #update
                $act = $this->tm_purchaseorder->update_data(array('po_kd' => $po_kd),$data);
                $data = array_merge(['po_kd' => $po_kd], $data);
            }
            
            #Insert relasi antara po dan pr
            if (!empty($pr_kd)){
                foreach ($pr_kd as $each){
                    $dataRelasi[] = array(
                        'po_kd' => $po_kd,
                        'pr_kd' => $each,
                        'relasipopr_tglinput' => date('Y-m-d H:i:s'),
                        'admin_kd' => $this->session->userdata('kd_admin'),
                    );
                }
                $actRelasi = $this->tb_relasi_popr->clear_insert_batch_data($po_kd, $dataRelasi);
            }

            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => $data);
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }    
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }
    
    public function action_katalog_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }

        $po_kd = $this->input->post('txtpo_kdKatalog');
        $katalog_kd = $this->input->post('txtkatalog_kd');
        $podetail_harga = $this->input->post('txtHarga');
        $podetail_konversi = $this->input->post('txtKonversi');
        $podetail_qty = $this->input->post('txtQty');
        $podetail_tgldelivery = $this->input->post('txtDelivery');
        $rm_kd = $this->input->post('txtRm_kd');
        $rm_nama = $this->input->post('txtRm_nama');
        $rm_deskripsi = $this->input->post('txtRm_deskripsi');
        
        if (!empty($katalog_kd)){
            $podetail_kd = $this->td_purchaseorder_detail->create_code();
            foreach ($katalog_kd as $eachKatalog){
                if (!empty($podetail_tgldelivery[$eachKatalog])){
                    $podetail_tgldeliveryFormat = format_date($podetail_tgldelivery[$eachKatalog], 'Y-m-d');
                }else{
                    $podetail_tgldeliveryFormat = null;
                }

                $podetail_qtyCek = $podetail_qty[$eachKatalog];
                $podetail_qtyCek = !empty($podetail_qtyCek) ? $podetail_qtyCek : 0;

                $data[] = array(
                    'podetail_kd' => $podetail_kd,
                    'po_kd' => $po_kd,
                    'katalog_kd' => $eachKatalog,
                    'rm_kd' => $rm_kd[$eachKatalog],
                    'rm_kd' => $rm_kd[$eachKatalog],
                    'podetail_nama' => $rm_nama[$eachKatalog],
                    'podetail_deskripsi' => $rm_deskripsi[$eachKatalog],
                    'podetail_qty' => $podetail_qtyCek,
                    'podetail_konversi' => $podetail_konversi[$eachKatalog],
                    'podetail_harga' => $podetail_harga[$eachKatalog],
                    'podetail_jmlharga' => $podetail_qtyCek * $podetail_harga[$eachKatalog],
                    'podetail_tgldelivery' => $podetail_tgldeliveryFormat,
                    'podetail_tglinput' => now_with_ms(),
                    'admin_kd' => $this->session->userdata('kd_admin'),
                );
                $podetail_kd ++;
            }

            $act = $this->td_purchaseorder_detail->insert_batch($data);            
            if ($act){
                /** Update harga terbaru di katalog suplier */
                $actUpdate = $this->td_suplier_katalog->update_harga_batch($data);

                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => $data);
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }

        }else{
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Tidak ada yang dipilih');
        }	
		
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
    }
    
    public function action_detail_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $id = $this->input->get('id', TRUE);

        if (!empty($id)) {
            $this->db-> trans_start();
            
            $rowDetailPO = $this->db->select()
                            ->from('td_purchaseorder_detail')
                            ->join('td_purchaserequisition_detail', 'td_purchaseorder_detail.prdetail_kd=td_purchaserequisition_detail.prdetail_kd' ,'left')
                            ->where('td_purchaseorder_detail.podetail_kd', $id)
                            ->get()->row_array();
            $actDel = $this->td_purchaseorder_detail->delete_data($id);
            $actCekPR = $this->tm_purchaserequisition->autoUpdate_status_pr($rowDetailPO['pr_kd']);
            
            $this->db->trans_complete();
            
            if ($this->db->trans_status() === FALSE) {
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
            }else {
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
            }
        }else {
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal ID kosong');
        }

		echo json_encode($resp);
    }

    public function push_to_sap(Type $var = null)
    {
        $po_kd = $this->input->get('id', TRUE);
        $data = array(
            'status_sap' => "1",
        );
        $act = $this->tm_purchaseorder->update_data(array('po_kd' => $po_kd),$data);

        $items = [];
            
                if (!empty($po_kd)){
                    $rowPO =  $this->db->select('a.*, b.*, c.nm_admin, d.suplier_nama, d.suplier_kode')
                        ->from('tm_purchaseorder as a')
                        ->join('td_workflow_state as b', 'a.wfstate_kd=b.wfstate_kd', 'left')
                        ->join('tb_admin as c', 'c.kd_admin=a.po_originator', 'left')
                        ->join('tm_suplier as d', 'a.suplier_kd=d.suplier_kd', 'left')
                        ->where('a.po_kd', $po_kd)
                        ->get()->row_array();
                    $resultPOdetail = $this->td_purchaseorder_detail->get_by_param_detail(array('po_kd' => $po_kd))->result_array();
                    $items['rowPO'] = $rowPO;
                    $items['resultPOdetail'] = $resultPOdetail;
                    $items['id'] = $po_kd;
                    $items['class_link'] = $this->class_link;
                    $result_relasipopr = $this->tb_relasi_popr->get_by_param_detail (array('tb_relasi_popr.po_kd' => $po_kd))->result_array();

                    if (!empty($result_relasipopr)) {
                        foreach ($result_relasipopr as $each_relasipopr){
                            $arraypr_no[] = $each_relasipopr['pr_no'];
                            $arraypr_kd[] = $each_relasipopr['pr_kd'];
                        }
                        $items['imppr_no'] = implode(',', $arraypr_no);
                        $items['imppr_kd'] = implode('_', $arraypr_kd);
                    }

        
                    // $this->load->view('page/'.$this->class_link.'/table_main', $data);
                }

                if ($act){
                    $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => $items);
                }else{
                    $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
                }

                echo json_encode($resp);
    }

    public function action_rollback_po () {
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $po_kd = $this->input->get('po_kd', true);

        if (!empty($po_kd)) {
            $data = array(
                'status_sap' => "0",
            );
            $act = $this->tm_purchaseorder->update_data(array('po_kd' => $po_kd),$data);
            if ($act) {
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
        }else{
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'ID tidak ditemukan');
        }
        echo json_encode($resp);
    }
}
