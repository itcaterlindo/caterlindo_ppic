<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Purchase_perbandingan_harga extends MY_Controller {
	private $class_link = 'purchasing/Purchase_perbandingan_harga';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_purchaserequisition', 'td_purchaserequisition_detail', 'td_perbandinganharga_detail', 'tm_perbandinganharga', 
			'td_perbandinganharga_detail_katalog', 'td_suplier_katalog', 'tm_suplier', 'td_rawmaterial_satuan', 'td_perbandinganharga_pr']);
	}

	public function index() {
        parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
		parent::icheck_assets();
		$this->table_box();
	}

	public function form_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['header'] = $this->db->select('tm_perbandinganharga.perbandinganharga_kd, tm_perbandinganharga.perbandinganharga_no, tm_perbandinganharga.perbandinganharga_tglinput,
				GROUP_CONCAT(tm_purchaserequisition.pr_no) as pr_no')
			->from('tm_perbandinganharga')
			->join('td_perbandinganharga_pr', 'tm_perbandinganharga.perbandinganharga_kd=td_perbandinganharga_pr.perbandinganharga_kd', 'left')
			->join('tm_purchaserequisition', 'td_perbandinganharga_pr.pr_kd=tm_purchaserequisition.pr_kd', 'left')
			->where(['tm_perbandinganharga.perbandinganharga_kd' => $id])
			->get()
			->row_array();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		if ($sts == 'edit'){
			$row = $this->tm_perbandinganharga->get_by_param (array('perbandinganharga_kd'=> $id))->row_array();			
			$data['rowData'] = $row;
			$perbandinganharga_prs = $this->td_perbandinganharga_pr->get_by_param(['perbandinganharga_kd' => $id])->result_array();
			$data['pr_kds'] = array_column($perbandinganharga_prs, 'pr_kd');
        }
        /** PR */
        $actPR = $this->db->select()
			->from('tm_purchaserequisition as a')
			->join('td_workflow_state as b', 'b.wfstate_kd=a.wfstate_kd', 'left')
			->where('b.wfstate_nama', 'Acknowledged')
			->get()->result_array();
        foreach ($actPR as $each):
			$opsiPR[$each['pr_kd']] = $each['pr_no'];
		endforeach;

        $data['opsiPR'] = $opsiPR;
		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function view_perbandinganhargaordersuplier_pdf () {
		$this->load->library('Pdf');

		$perbandinganharga_kd = $this->input->get('perbandinganharga_kd', true);
		$perbandinganharga_kd = url_decrypt($perbandinganharga_kd);

		$r_perbandinganhargadetail = $this->db->select('td_perbandinganharga_detail_katalog.*, td_perbandinganharga_detail.*, 
						tm_suplier.suplier_kd, tm_suplier.suplier_kode, tm_suplier.suplier_nama, tm_suplier.suplier_includeppn, tm_suplier.suplier_termpayment,
						td_purchaserequisition_detail.prdetail_deskripsi, td_purchaserequisition_detail.prdetail_spesifikasi, td_purchaserequisition_detail.prdetail_qty,
						satuan_katalog.rmsatuan_nama as satuan_katalog, satuan_pr.rmsatuan_nama as satuan_pr,
						tm_rawmaterial.rm_kode, tm_rawmaterial.rm_oldkd, tm_purchaserequisition.pr_no')
					->from('td_perbandinganharga_detail_katalog')
					->join('td_perbandinganharga_detail', 'td_perbandinganharga_detail_katalog.perbandinganhargadetail_kd=td_perbandinganharga_detail.perbandinganhargadetail_kd', 'right')
					->join('tm_suplier', 'td_perbandinganharga_detail_katalog.suplier_kd=tm_suplier.suplier_kd', 'left')
					->join('td_purchaserequisition_detail', 'td_perbandinganharga_detail.prdetail_kd=td_purchaserequisition_detail.prdetail_kd', 'left')
					->join('tm_rawmaterial', 'td_purchaserequisition_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
					->join('tm_purchaserequisition', 'td_purchaserequisition_detail.pr_kd=tm_purchaserequisition.pr_kd', 'left')
					->join('td_rawmaterial_satuan as satuan_katalog', 'td_perbandinganharga_detail_katalog.rmsatuan_kd=satuan_katalog.rmsatuan_kd', 'left')
					->join('td_rawmaterial_satuan as satuan_pr', 'td_purchaserequisition_detail.rmsatuan_kd=satuan_pr.rmsatuan_kd', 'left')
					->where('td_perbandinganharga_detail.perbandinganharga_kd', $perbandinganharga_kd)
					->order_by('tm_suplier.suplier_kode, tm_rawmaterial.rm_kode, td_perbandinganharga_detail_katalog.perbandinganhargadetailkatalog_selected')
					->get()
					->result_array();

		$dataPerbandinganKatalogSuplier['r_perbandinganhargadetail'] = $r_perbandinganhargadetail;
		$dataPerbandinganKatalogSuplier['perbandinganharga_kd'] = $perbandinganharga_kd;
		$data['konten'] = $this->load->view('page/'.$this->class_link.'/view_perbandinganhargaordersuplier_tablemain', $dataPerbandinganKatalogSuplier, true);
		$data['header'] = $this->db->select('tm_perbandinganharga.perbandinganharga_kd, tm_perbandinganharga.perbandinganharga_no, tm_perbandinganharga.perbandinganharga_tglinput, tm_perbandinganharga.perbandinganharga_tgledit,
					GROUP_CONCAT(tm_purchaserequisition.pr_no, " (",  DATE(tm_purchaserequisition.pr_tglinput), ") " SEPARATOR ", ") as pr_no')
				->from('tm_perbandinganharga')
				->join('td_perbandinganharga_pr', 'tm_perbandinganharga.perbandinganharga_kd=td_perbandinganharga_pr.perbandinganharga_kd', 'left')
				->join('tm_purchaserequisition', 'td_perbandinganharga_pr.pr_kd=tm_purchaserequisition.pr_kd', 'left')
				->where(['tm_perbandinganharga.perbandinganharga_kd' => $perbandinganharga_kd])
				->get()
				->row_array();
		
		$this->load->view('page/'.$this->class_link.'/view_perbandinganhargaordersuplier_pdf', $data);
	}

	public function view_perbandinganharga_pdf () {
		$this->load->library('Pdf');

		$perbandinganharga_kd = $this->input->get('perbandinganharga_kd', true);
		$perbandinganharga_kd = url_decrypt($perbandinganharga_kd);

		$r_perbandinganhargadetail = $this->db->select('td_perbandinganharga_detail.*, tm_perbandinganharga.*, tm_purchaserequisition.pr_no, 
						tm_rawmaterial.rm_kode, tm_rawmaterial.rm_oldkd, satuan_pr.rmsatuan_nama as satuan_pr, satuan_po.rmsatuan_nama as satuan_po,td_purchaserequisition_detail.*, tm_suplier.suplier_nama, tm_suplier.suplier_kode, 
						tm_purchaseorder.po_no, td_purchaseorder_detail.podetail_harga, td_purchaseorder_detail.podetail_tglinput')
					->from('td_perbandinganharga_detail')
					->join('tm_perbandinganharga', 'td_perbandinganharga_detail.perbandinganharga_kd=tm_perbandinganharga.perbandinganharga_kd', 'left')
					->join('td_purchaserequisition_detail', 'td_perbandinganharga_detail.prdetail_kd=td_purchaserequisition_detail.prdetail_kd', 'left')
					->join('tm_purchaserequisition', 'td_purchaserequisition_detail.pr_kd=tm_purchaserequisition.pr_kd', 'left')
					
					->join('td_purchaseorder_detail', 'td_perbandinganharga_detail.perbandinganhargadetail_podetail_kd=td_purchaseorder_detail.podetail_kd' , 'left')
					->join('tm_purchaseorder', 'td_purchaseorder_detail.po_kd=tm_purchaseorder.po_kd' , 'left')
					->join('td_rawmaterial_satuan as satuan_po', 'td_purchaseorder_detail.rmsatuan_kd=satuan_po.rmsatuan_kd', 'left')
					->join('tm_suplier', 'tm_suplier.suplier_kd=tm_purchaseorder.suplier_kd' , 'left')
					
					->join('tm_rawmaterial', 'td_purchaserequisition_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
					->join('td_rawmaterial_satuan as satuan_pr', 'td_purchaserequisition_detail.rmsatuan_kd=satuan_pr.rmsatuan_kd', 'left')
					->where('td_perbandinganharga_detail.perbandinganharga_kd', $perbandinganharga_kd)
					->get()
					->result_array();

		$r_perbandinganhargadetailKatalog = $this->db->select('td_perbandinganharga_detail_katalog.*, td_perbandinganharga_detail.*, td_purchaserequisition_detail.*, 
					td_suplier_katalog.*, satuan_katalog.rmsatuan_nama as satuan_katalog, tm_suplier.suplier_nama, tm_suplier.suplier_kode, tm_rawmaterial.rm_kode, tm_suplier.suplier_includeppn, tm_suplier.suplier_termpayment')
				->from('td_perbandinganharga_detail_katalog')
				->join('td_perbandinganharga_detail', 'td_perbandinganharga_detail_katalog.perbandinganhargadetail_kd=td_perbandinganharga_detail.perbandinganhargadetail_kd', 'left')
				->join('td_purchaserequisition_detail', 'td_perbandinganharga_detail.prdetail_kd=td_purchaserequisition_detail.prdetail_kd', 'left')
				->join('td_suplier_katalog', 'td_suplier_katalog.katalog_kd=td_perbandinganharga_detail_katalog.katalog_kd', 'left')
				->join('tm_rawmaterial', 'td_perbandinganharga_detail_katalog.rm_kd=tm_rawmaterial.rm_kd', 'left')
				->join('td_rawmaterial_satuan as satuan_katalog', 'satuan_katalog.rmsatuan_kd=td_perbandinganharga_detail_katalog.rmsatuan_kd', 'left')
				->join('tm_suplier', 'td_perbandinganharga_detail_katalog.suplier_kd=tm_suplier.suplier_kd', 'left')
				->where(['td_perbandinganharga_detail_katalog.perbandinganharga_kd' => $perbandinganharga_kd])
				// ->where('td_perbandinganharga_detail_katalog.perbandinganhargadetailkatalog_tglkatalog >=', date('Y-m-d H:i:s', strtotime('-1 month')))
				// ->where('td_perbandinganharga_detail_katalog.perbandinganhargadetailkatalog_tglkatalog <=', date('Y-m-d H:i:s'))
				->order_by('td_perbandinganharga_detail_katalog.perbandinganhargadetailkatalog_tglkatalog', 'DESC')
				->get()
				->result_array();

		$dataPerbandinganKatalog['r_perbandinganhargadetail'] = $r_perbandinganhargadetail;
		$dataPerbandinganKatalog['r_perbandinganhargadetailKatalog'] = $r_perbandinganhargadetailKatalog;
		$dataPerbandinganKatalog['class_link'] = $this->class_link;
		$dataPerbandinganKatalog['perbandinganharga_kd'] = $perbandinganharga_kd;
		
		$data['header'] = $this->db->select('tm_perbandinganharga.perbandinganharga_kd, tm_perbandinganharga.perbandinganharga_no, tm_perbandinganharga.perbandinganharga_tglinput, tm_perbandinganharga.perbandinganharga_tgledit,
		GROUP_CONCAT(tm_purchaserequisition.pr_no, " (",  DATE(tm_purchaserequisition.pr_tglinput), ") " SEPARATOR ", ") as pr_no')
				->from('tm_perbandinganharga')
				->join('td_perbandinganharga_pr', 'tm_perbandinganharga.perbandinganharga_kd=td_perbandinganharga_pr.perbandinganharga_kd', 'left')
				->join('tm_purchaserequisition', 'td_perbandinganharga_pr.pr_kd=tm_purchaserequisition.pr_kd', 'left')
				->where(['tm_perbandinganharga.perbandinganharga_kd' => $perbandinganharga_kd])
				->get()
				->row_array();
		$data['konten'] = $this->load->view('page/'.$this->class_link.'/view_perbandinganharga_tablemain', $dataPerbandinganKatalog, true);
		
		$this->load->view('page/'.$this->class_link.'/view_perbandinganharga_pdf', $data);
	}


	public function table_box(){
		$tahun = $this->input->get('Y');
        $bulan = $this->input->get('m');
        $pr_no = $this->input->get('pr_no');

		#opsi bulan
        $months = bulanIndo();
        $months = array_flip($months);
        $months['ALL'] = 'ALL';
        $data['months'] = $months;

		#params
		$tahun = !empty($tahun) ? $tahun : date('Y');
		$bulan = !empty($bulan) ? $bulan : date('m');
		$pr_no = !empty($pr_no) ? $pr_no : null;
		$data['tahun'] = $tahun;
		$data['bulan'] = $bulan;
		$data['pr_no'] = $pr_no;
		$data['params'] = "?Y=$tahun&m=$bulan&pr_no=$pr_no";

		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$data['params'] = $this->input->get('params');
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$tahun = $this->input->get('Y', true);
		$bulan = $this->input->get('m', true);
		$pr_no = $this->input->get('pr_no', true);

		$params = ['tahun' => $tahun, 'bulan' => $bulan, 'pr_no' => $pr_no];		
        $data = $this->tm_perbandinganharga->ssp_table2($params);

        echo json_encode($data);
	}

	private function generate_lastpo_price($arrRmkd) {
		$result = [];
		if (!empty($arrRmkd)) {
			$qMaxrm = $this->db->select('a.rm_kd, MAX(b.po_no) AS max_po, MAX(a.podetail_tglinput) AS max_tglpodetail')
						->from('td_purchaseorder_detail as a')
						->join('tm_purchaseorder as b', 'a.po_kd=b.po_kd','left')
						->where_in('a.rm_kd', $arrRmkd)
						->group_by('a.rm_kd')
						->get_compiled_select();
			$r_LastPO = $this->db->select('aa.podetail_kd, aa.rm_kd, bb.po_no, aa.podetail_harga, cc.rmsatuan_nama, dd.suplier_nama')
						->from('td_purchaseorder_detail as aa')
						->join('tm_purchaseorder as bb', 'aa.po_kd=bb.po_kd','left')
						->join('td_rawmaterial_satuan as cc', 'aa.rmsatuan_kd=cc.rmsatuan_kd','left')
						->join('tm_suplier as dd', 'bb.suplier_kd=dd.suplier_kd','left')
						->join('('.$qMaxrm.') as ee', 'aa.rm_kd=ee.rm_kd AND bb.po_no=ee.max_po AND aa.podetail_tglinput=ee.max_tglpodetail', 'inner')
						->group_by('aa.rm_kd, bb.po_no')
						->get()
						->result_array();
			$result = $r_LastPO;
		}
		return $result;
	}

	public function table_detailpr_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$sts = $this->input->get('sts', true);
		$pr_kd = $this->input->get('pr_kd', true);
		$perbandinganharga_kd = $this->input->get('perbandinganharga_kd', true);

		$r_prdetail =  $this->db->select('td_purchaserequisition_detail.*, td_rawmaterial_satuan.rmsatuan_nama, tm_rawmaterial.rm_kode, tm_purchaserequisition.pr_no')
					->from('td_purchaserequisition_detail')
					->join('tm_purchaserequisition', 'td_purchaserequisition_detail.pr_kd=tm_purchaserequisition.pr_kd', 'left')
					->join('tm_rawmaterial', 'td_purchaserequisition_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
					->join('td_rawmaterial_satuan', 'td_purchaserequisition_detail.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
					->join('td_perbandinganharga_detail', 'td_purchaserequisition_detail.prdetail_kd=td_perbandinganharga_detail.prdetail_kd', 'left')
					->where_in('td_purchaserequisition_detail.pr_kd', $pr_kd)
					->where('td_perbandinganharga_detail.prdetail_kd IS NULL')
					->order_by('tm_rawmaterial.rm_kode')
					->get()->result_array();
		$aRmkd = array_column($r_prdetail, 'rm_kd');
		$rLastPO = $this->generate_lastpo_price($aRmkd);
		$aResult = [];
		foreach ($r_prdetail as $e_prdetail) {
			$podetail_kd = null;
			foreach ($rLastPO as $eLastPO) {
				if ($eLastPO['rm_kd'] == $e_prdetail['rm_kd'] && $e_prdetail['rm_kd'] != 'SPECIAL' ) {
					$podetail_kd = $eLastPO['podetail_kd'];
				}
			}
			$aResult[] = array_merge($e_prdetail, ['podetail_kd' => $podetail_kd]);
		}

		$data['sts'] = $sts;
		$data['pr_kd'] = implode(',', $pr_kd);
		$data['perbandinganharga_kd'] = $perbandinganharga_kd;
		$data['r_prdetail'] = $aResult;
		$data['class_link'] = $this->class_link;

	
		$this->load->view('page/'.$this->class_link.'/table_detailpr_main', $data);
	}

	public function table_perbandinganhargadetail_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$perbandinganharga_kd = $this->input->get('perbandinganharga_kd', true);

		$r_perbandinganhargadetail = $this->db->select('td_perbandinganharga_detail.*, tm_perbandinganharga.*, tm_purchaserequisition.pr_no, tm_rawmaterial.rm_kode, td_rawmaterial_satuan.rmsatuan_nama, td_purchaserequisition_detail.*')
					->from('td_perbandinganharga_detail')
					->join('tm_perbandinganharga', 'td_perbandinganharga_detail.perbandinganharga_kd=tm_perbandinganharga.perbandinganharga_kd', 'left')
					->join('td_purchaserequisition_detail', 'td_perbandinganharga_detail.prdetail_kd=td_purchaserequisition_detail.prdetail_kd', 'left')
					->join('tm_purchaserequisition', 'td_purchaserequisition_detail.pr_kd=tm_purchaserequisition.pr_kd', 'left')
					->join('tm_rawmaterial', 'td_purchaserequisition_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
					->join('td_rawmaterial_satuan', 'td_purchaserequisition_detail.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
					->where('td_perbandinganharga_detail.perbandinganharga_kd', $perbandinganharga_kd)
					->get()
					->result_array();

		$data['perbandinganharga_kd'] = $perbandinganharga_kd;
		$data['r_perbandinganharga_detail'] = $r_perbandinganhargadetail;
		$data['class_link'] = $this->class_link;

		// header('Content-Type: application/json');
		// echo json_encode($data);
		// $this->load->view('page/'.$this->class_link.'/table_detailpr_main', $data);
		$this->load->view('page/'.$this->class_link.'/table_perbandinganhargadetail_main', $data);
	}

	public function table_perbandinganhargadetailkatalog_main () {
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }

		$perbandinganharga_kd = $this->input->get('perbandinganharga_kd', true);

		$r_perbandinganhargadetail = $this->db->select('td_perbandinganharga_detail.*, tm_perbandinganharga.*, tm_purchaserequisition.pr_no, 
						tm_rawmaterial.rm_kode, satuan_pr.rmsatuan_nama as satuan_pr, satuan_po.rmsatuan_nama as satuan_po,td_purchaserequisition_detail.*, tm_suplier.suplier_nama, 
						tm_purchaseorder.po_no, td_purchaseorder_detail.podetail_harga, td_purchaseorder_detail.podetail_tglinput')
					->from('td_perbandinganharga_detail')
					->join('tm_perbandinganharga', 'td_perbandinganharga_detail.perbandinganharga_kd=tm_perbandinganharga.perbandinganharga_kd', 'left')
					->join('td_purchaserequisition_detail', 'td_perbandinganharga_detail.prdetail_kd=td_purchaserequisition_detail.prdetail_kd', 'left')
					->join('tm_purchaserequisition', 'td_purchaserequisition_detail.pr_kd=tm_purchaserequisition.pr_kd', 'left')
					
					->join('td_purchaseorder_detail', 'td_perbandinganharga_detail.perbandinganhargadetail_podetail_kd=td_purchaseorder_detail.podetail_kd' , 'left')
					->join('tm_purchaseorder', 'td_purchaseorder_detail.po_kd=tm_purchaseorder.po_kd' , 'left')
					->join('td_rawmaterial_satuan as satuan_po', 'td_purchaseorder_detail.rmsatuan_kd=satuan_po.rmsatuan_kd', 'left')
					->join('tm_suplier', 'tm_suplier.suplier_kd=tm_purchaseorder.suplier_kd' , 'left')
					
					->join('tm_rawmaterial', 'td_purchaserequisition_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
					->join('td_rawmaterial_satuan as satuan_pr', 'td_purchaserequisition_detail.rmsatuan_kd=satuan_pr.rmsatuan_kd', 'left')
					->where('td_perbandinganharga_detail.perbandinganharga_kd', $perbandinganharga_kd)
					->get()
					->result_array();

		$r_perbandinganhargadetailKatalog = $this->db->select('td_perbandinganharga_detail_katalog.*, td_perbandinganharga_detail.*, td_purchaserequisition_detail.*, 
					td_suplier_katalog.*, satuan_katalog.rmsatuan_nama as satuan_katalog, tm_suplier.suplier_nama, tm_suplier.suplier_kode, tm_rawmaterial.rm_kode')
				->from('td_perbandinganharga_detail_katalog')
				->join('td_perbandinganharga_detail', 'td_perbandinganharga_detail_katalog.perbandinganhargadetail_kd=td_perbandinganharga_detail.perbandinganhargadetail_kd', 'left')
				->join('td_purchaserequisition_detail', 'td_perbandinganharga_detail.prdetail_kd=td_purchaserequisition_detail.prdetail_kd', 'left')
				->join('td_suplier_katalog', 'td_suplier_katalog.katalog_kd=td_perbandinganharga_detail_katalog.katalog_kd', 'left')
				->join('tm_rawmaterial', 'td_perbandinganharga_detail_katalog.rm_kd=tm_rawmaterial.rm_kd', 'left')
				->join('td_rawmaterial_satuan as satuan_katalog', 'satuan_katalog.rmsatuan_kd=td_perbandinganharga_detail_katalog.rmsatuan_kd', 'left')
				->join('tm_suplier', 'td_perbandinganharga_detail_katalog.suplier_kd=tm_suplier.suplier_kd', 'left')
				->where(['td_perbandinganharga_detail_katalog.perbandinganharga_kd' => $perbandinganharga_kd])
				// ->where('td_perbandinganharga_detail_katalog.perbandinganhargadetailkatalog_tglkatalog >=', date('Y-m-d H:i:s', strtotime('-1 month')))
				// ->where('td_perbandinganharga_detail_katalog.perbandinganhargadetailkatalog_tglkatalog <=', date('Y-m-d H:i:s'))
				->order_by('td_perbandinganharga_detail_katalog.perbandinganhargadetailkatalog_tglkatalog', 'DESC')
				->get()
				->result_array();

		$data['r_perbandinganhargadetail'] = $r_perbandinganhargadetail;
		$data['r_perbandinganhargadetailKatalog'] = $r_perbandinganhargadetailKatalog;
		$data['class_link'] = $this->class_link;
		$data['perbandinganharga_kd'] = $perbandinganharga_kd;

		//header('Content-Type: application/json');
		//echo json_encode($data);
		$this->load->view('page/'.$this->class_link.'/table_perbandinganhargadetailkatalog_main', $data);
	}

	public function form_perbandinganhargadetailkatalog_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts', true);
		$perbandinganhargadetail_kd = $this->input->get('perbandinganhargadetail_kd', true);
		$perbandinganhargadetailkatalog_kd = $this->input->get('perbandinganhargadetailkatalog_kd', true);

		/** Suplier */
        $actSuplier = $this->tm_suplier->get_all()->result_array();
        $opsiSuplier[''] = '-- Pilih Suplier --';
        foreach ($actSuplier as $each):
			$opsiSuplier[$each['suplier_kd']] = $each['suplier_kode'].' | '.$each['suplier_nama'];
		endforeach;
		/** Satuan */
        $actSatuan = $this->td_rawmaterial_satuan->get_all()->result_array();
        $opsiSatuan[''] = '-- Pilih Satuan --';
        foreach ($actSatuan as $each):
			$opsiSatuan[$each['rmsatuan_kd']] = $each['rmsatuan_nama'];
		endforeach;

		if ($sts == 'edit') {
			$data['rowData'] = $this->db->select('td_perbandinganharga_detail_katalog.*, td_perbandinganharga_detail.*, td_purchaserequisition_detail.*,
						tm_suplier.*, tm_rawmaterial.rm_kode, satuan_perbandingankatalog.rmsatuan_kd, satuan_perbandingankatalog.rmsatuan_nama as satuan_perbandingankatalog')
					->from('td_perbandinganharga_detail_katalog')
					->join('td_perbandinganharga_detail', 'td_perbandinganharga_detail_katalog.perbandinganhargadetail_kd=td_perbandinganharga_detail.perbandinganhargadetail_kd', 'left')
					->join('td_purchaserequisition_detail', 'td_perbandinganharga_detail.prdetail_kd=td_purchaserequisition_detail.prdetail_kd', 'left')
					->join('tm_suplier', 'td_perbandinganharga_detail_katalog.suplier_kd=tm_suplier.suplier_kd', 'left')
					->join('tm_rawmaterial', 'td_purchaserequisition_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
					->join('td_rawmaterial_satuan as satuan_perbandingankatalog', 'td_perbandinganharga_detail_katalog.rmsatuan_kd=satuan_perbandingankatalog.rmsatuan_kd', 'left')
					->where(['td_perbandinganharga_detail_katalog.perbandinganhargadetailkatalog_kd' => $perbandinganhargadetailkatalog_kd])
					->get()
					->row_array();
		}else{
			$data['rowData'] = $this->db->select('td_perbandinganharga_detail.*, td_purchaserequisition_detail.*,
						tm_rawmaterial.rm_kode, td_rawmaterial_satuan.rmsatuan_nama as satuan_perbandingankatalog')
					->from('td_perbandinganharga_detail')
					->join('td_purchaserequisition_detail', 'td_perbandinganharga_detail.prdetail_kd=td_purchaserequisition_detail.prdetail_kd', 'left')
					->join('tm_rawmaterial', 'td_purchaserequisition_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
					->join('td_rawmaterial_satuan', 'td_purchaserequisition_detail.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
					->where(['td_perbandinganharga_detail.perbandinganhargadetail_kd' => $perbandinganhargadetail_kd])
					->get()
					->row_array();
		}

		$data['class_link'] = $this->class_link;
		$data['perbandinganhargadetail_kd'] = $perbandinganhargadetail_kd;
		$data['perbandinganhargadetailkatalog_kd'] = $perbandinganhargadetailkatalog_kd;
		$data['sts'] = $sts;
		$data['opsiSuplier'] = $opsiSuplier;
		$data['opsiSatuan'] = $opsiSatuan;
		$this->load->view('page/'.$this->class_link.'/form_perbandinganhargadetailkatalog_main', $data);

	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtTabelpr_kd', 'PR', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpr_kd' => (!empty(form_error('txtTabelpr_kd')))?buildLabel('warning', form_error('txtTabelpr_kd', '"', '"')):'',
			);
			
		}else {	
			$sts = $this->input->post('txtTabelpr_kd', true);
			$pr_kd = $this->input->post('txtTabelpr_kd', true);
			$prdetail_kds = $this->input->post('chkprdetail_kd', true);
			$perbandinganharga_kd = $this->input->post('txtTabelperbandinganharga_kd', true);
			$podetail_kds = $this->input->post('podetail_kds', true);
			
			$dataPerbandinganDetail = [];
			$dataPerbandingan = [];
			$dataRmkd = [];
			if (!empty($prdetail_kds)) {
				/** Jika insert new */
				if (empty($perbandinganharga_kd)) {
					/** Perbandingan Harga */
					$perbandinganharga_kd = $this->tm_perbandinganharga->create_code();
					$perbandinganharga_no = $this->tm_perbandinganharga->create_no();
					$dataPerbandingan = [
						'perbandinganharga_kd' => $perbandinganharga_kd,
						'perbandinganharga_no' => $perbandinganharga_no,
						'perbandinganharga_tglinput' => date('Y-m-d H:i:s'),
						'perbandinganharga_tgledit' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					];
					$actPerbandingan = $this->tm_perbandinganharga->insert_data($dataPerbandingan);
					$exPrkds = explode(',', $pr_kd);
					$dataDetailPR = [];
					foreach ($exPrkds as $e_exPrkd) {
						$dataDetailPR[] = [
							'perbandinganharga_kd' => $perbandinganharga_kd,
							'pr_kd' => $e_exPrkd,
							'perbandinganhargapr_tglinput' => date('Y-m-d H:i:s'),
						];
					}
					$actPerbandinganPR = $this->td_perbandinganharga_pr->insert_batch($dataDetailPR);
				}else{
					
					$this->td_perbandinganharga_pr->delete_by_param('perbandinganharga_kd', $perbandinganharga_kd);

					$exPrkds = explode(',', $pr_kd);
					$dataDetailPR = [];
					foreach ($exPrkds as $e_exPrkd) {
						$dataDetailPR[] = [
							'perbandinganharga_kd' => $perbandinganharga_kd,
							'pr_kd' => $e_exPrkd,
							'perbandinganhargapr_tglinput' => date('Y-m-d H:i:s'),
						];
					}
					$actPerbandinganPR = $this->td_perbandinganharga_pr->insert_batch($dataDetailPR);
				}
				
				/** Detail Perbandingan */
				$perbandinganhargadetail_kd = $this->td_perbandinganharga_detail->create_code();
				foreach ($prdetail_kds as $prdetail_kd) {
					$dataPerbandinganDetail[] =[
						'perbandinganhargadetail_kd' => $perbandinganhargadetail_kd,
						'perbandinganharga_kd' => $perbandinganharga_kd,
						'prdetail_kd' => $prdetail_kd,
						'perbandinganhargadetail_podetail_kd' => !empty($podetail_kds[$prdetail_kd]) ? $podetail_kds[$prdetail_kd] : null,
						'perbandinganhargadetail_tglinput' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					];
					$perbandinganhargadetail_kd++;
				}
				$this->db->trans_start();
				$actPerbandingan = $this->td_perbandinganharga_detail->insert_batch($dataPerbandinganDetail);
				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE) {
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan');
				}else{
					$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => $perbandinganharga_kd);
				}
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Kosong');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_generate_katalog () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$actPerbandinaghargadtlKatalog = true;
		$perbandinganharga_kd = $this->input->get('perbandinganharga_kd', true);

		#Update tgledit tm_perbandinganharga
		$actPerbandinganHarga = $this->tm_perbandinganharga->update_data(['perbandinganharga_kd' => $perbandinganharga_kd], ['perbandinganharga_tgledit' => date('Y-m-d H:i:s')]);
		
		$rPerbandinganhargadetail = $this->db->select()
								->from('td_perbandinganharga_detail')
								->join('td_purchaserequisition_detail', 'td_perbandinganharga_detail.prdetail_kd=td_purchaserequisition_detail.prdetail_kd', 'left')
								->join('td_suplier_katalog', 'td_purchaserequisition_detail.rm_kd=td_suplier_katalog.rm_kd', 'inner')
								->where('perbandinganharga_kd', $perbandinganharga_kd)
								->where('td_suplier_katalog.katalog_aktif', '1')
								->get()->result_array();
		$rPerbandinganhargadetailKatalog = $this->td_perbandinganharga_detail_katalog->get_by_param(['perbandinganharga_kd' => $perbandinganharga_kd])->result_array();

		$aPerbandinganhargadetailKatalog = [];
		$arrRmkd=[];
		$perbandinganhargadetailkatalog_kd = $this->td_perbandinganharga_detail_katalog->create_code();
		foreach ($rPerbandinganhargadetail as $ePerbandinganhargadetail1) {
			$aPerbandinganhargadetailKatalog[] = [
				'perbandinganhargadetailkatalog_kd' => $perbandinganhargadetailkatalog_kd,
				'perbandinganharga_kd' => $perbandinganharga_kd,
				'perbandinganhargadetail_kd' => $ePerbandinganhargadetail1['perbandinganhargadetail_kd'],
				'perbandinganhargadetailkatalog_selected' => null,
				'katalog_kd' => $ePerbandinganhargadetail1['katalog_kd'],
				'suplier_kd' => $ePerbandinganhargadetail1['suplier_kd'],
				'rm_kd' => $ePerbandinganhargadetail1['rm_kd'],
				'perbandinganhargadetailkatalog_katalogharga' => $ePerbandinganhargadetail1['katalog_harga'],
				'rmsatuan_kd' => $ePerbandinganhargadetail1['katalog_satuankd'],
				'perbandinganhargadetailkatalog_tglinput' => date('Y-m-d H:i:s'),
				'perbandinganhargadetailkatalog_tglkatalog' => $ePerbandinganhargadetail1['katalog_tgledit'],
				'admin_kd' => $this->session->userdata('kd_admin'),
			];
			$perbandinganhargadetailkatalog_kd++;
		}
		
		if (empty($rPerbandinganhargadetailKatalog)) {
			/** Belum ada detail katalog sebelumnya */
			if (!empty($aPerbandinganhargadetailKatalog)) {
				$actPerbandinaghargadtlKatalog = $this->td_perbandinganharga_detail_katalog->insert_batch($aPerbandinganhargadetailKatalog);
			}else{
				// input kosong
				$actPerbandinaghargadtlKatalog = true;
			}
		}else {
			/** Sudah ada detail katalog sebelumnya */
			$aPerbandianganhargaDetail  = [];
			$aInsertPerbandinganhargaadetailKatalog = [];
			$dataInsertPerbandinganhargaadetailKatalog = [];
			foreach ($rPerbandinganhargadetailKatalog as $ePerbandinganhargadetailKatalog) {
				if (!in_array($ePerbandinganhargadetailKatalog['perbandinganhargadetail_kd'], $aPerbandianganhargaDetail)) {
					$aPerbandianganhargaDetail [] = $ePerbandinganhargadetailKatalog['perbandinganhargadetail_kd'];
				}
			}
			foreach ($aPerbandinganhargadetailKatalog as $ePerbandinganhargadetailKatalog2) {
				if (in_array($ePerbandinganhargadetailKatalog2['perbandinganhargadetail_kd'], $aPerbandianganhargaDetail)){
					continue;
				}
				$aInsertPerbandinganhargaadetailKatalog [] = $ePerbandinganhargadetailKatalog2;
			}
			if (!empty($aInsertPerbandinganhargaadetailKatalog)) {
				/** Rebuild perbandinagnhargadetail ID */
				$perbandinganhargadetailkatalog_kd2 = $this->td_perbandinganharga_detail_katalog->create_code();
				foreach ($aInsertPerbandinganhargaadetailKatalog as $eInsertPerbandinganhargaadetailKatalog) {
					$dataInsertPerbandinganhargaadetailKatalog [] = [
						'perbandinganhargadetailkatalog_kd' => $perbandinganhargadetailkatalog_kd2,
						'perbandinganharga_kd' => $eInsertPerbandinganhargaadetailKatalog['perbandinganharga_kd'],
						'perbandinganhargadetail_kd' => $eInsertPerbandinganhargaadetailKatalog['perbandinganhargadetail_kd'],
						'perbandinganhargadetailkatalog_selected' => $eInsertPerbandinganhargaadetailKatalog['perbandinganhargadetailkatalog_selected'],
						'katalog_kd' => $eInsertPerbandinganhargaadetailKatalog['katalog_kd'],
						'suplier_kd' => $eInsertPerbandinganhargaadetailKatalog['suplier_kd'],
						'rm_kd' => $eInsertPerbandinganhargaadetailKatalog['rm_kd'],
						'perbandinganhargadetailkatalog_katalogharga' => $eInsertPerbandinganhargaadetailKatalog['perbandinganhargadetailkatalog_katalogharga'],
						'rmsatuan_kd' => $eInsertPerbandinganhargaadetailKatalog['satuan_kd'],
						'perbandinganhargadetailkatalog_tglinput' => date('Y-m-d H:i:s'),
						'perbandinganhargadetailkatalog_tglkatalog' => $eInsertPerbandinganhargaadetailKatalog['katalog_tgledit'],
						'admin_kd' => $this->session->userdata('kd_admin'),
					];
					$perbandinganhargadetailkatalog_kd2++;
				}
				$actPerbandinaghargadtlKatalog = $this->td_perbandinganharga_detail_katalog->insert_batch($dataInsertPerbandinganhargaadetailKatalog);
			}
		}
		
		echo json_encode($actPerbandinaghargadtlKatalog);
	}

	public function action_selected () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$sts = $this->input->post('txtStsFormDetailKatalog', true);
		$perbandinganharga_kd = $this->input->post('txtperbandinganharga_kd', true);
		$perbandinganhargadetail_kd = $this->input->post('txtperbandinganhargadetail_kd', true);
		$perbandinganhargadetailkatalog_kd = $this->input->post('txtperbandinganhargadetailkatalog_kd', true);
		$perbandinganhargadetailkatalog_selected = $this->input->post('txtperbandinganhargadetailkatalog_selected', true);
		$rm_kd = $this->input->post('txtrm_kd', true);
		$suplier_kd = $this->input->post('txtsuplier_kd', true);
		$perbandinganhargadetailkatalog_updateharga = $this->input->post('txtperbandinganhargadetailkatalog_updateharga', true);
		$perbandinganhargadetailkatalog_qty = $this->input->post('txtperbandinganhargadetailkatalog_qty', true);
		$perbandinganhargadetailkatalog_keterangan = $this->input->post('txtperbandinganhargadetailkatalog_keterangan', true);
		$rmsatuan_kd = $this->input->post('txtrmsatuan_kdSelected', true);

		$dataUpdate = [];
		if ($sts == 'edit') {
			$dataUpdate = [
				'perbandinganhargadetailkatalog_selected' => !empty($perbandinganhargadetailkatalog_selected) ? $perbandinganhargadetailkatalog_selected : null,
				'perbandinganhargadetailkatalog_updateharga' => !empty($perbandinganhargadetailkatalog_updateharga) ? $perbandinganhargadetailkatalog_updateharga : null,
				'perbandinganhargadetailkatalog_qty' => !empty($perbandinganhargadetailkatalog_qty) ? $perbandinganhargadetailkatalog_qty : null,
				'perbandinganhargadetailkatalog_keterangan' => !empty($perbandinganhargadetailkatalog_keterangan) ? $perbandinganhargadetailkatalog_keterangan : null,
				'perbandinganhargadetailkatalog_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			];
		}elseif ($sts == 'add') {
			$dataUpdate = [
				'perbandinganhargadetailkatalog_kd' => $this->td_perbandinganharga_detail_katalog->create_code(),
				'perbandinganharga_kd' => $perbandinganharga_kd,
				'perbandinganhargadetail_kd' => $perbandinganhargadetail_kd,
				'suplier_kd' => $suplier_kd,
				'rm_kd' => $rm_kd,
				'perbandinganhargadetailkatalog_qty' => !empty($perbandinganhargadetailkatalog_qty) ? $perbandinganhargadetailkatalog_qty : null,
				'perbandinganhargadetailkatalog_updateharga' => !empty($perbandinganhargadetailkatalog_updateharga) ? $perbandinganhargadetailkatalog_updateharga : null,
				'perbandinganhargadetailkatalog_status' => 'additional',
				'rmsatuan_kd' => $rmsatuan_kd,
				'perbandinganhargadetailkatalog_keterangan' => !empty($perbandinganhargadetailkatalog_keterangan) ? $perbandinganhargadetailkatalog_keterangan : null,
				'perbandinganhargadetailkatalog_selected' => !empty($perbandinganhargadetailkatalog_selected) ? $perbandinganhargadetailkatalog_selected : null,
				'perbandinganhargadetailkatalog_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			];
		}

		if (!empty($dataUpdate)) {
			if ($sts == 'edit') {
				$act = $this->td_perbandinganharga_detail_katalog->update_data(['perbandinganhargadetailkatalog_kd' => $perbandinganhargadetailkatalog_kd], $dataUpdate);
			}else {
				$act = $this->td_perbandinganharga_detail_katalog->insert_data($dataUpdate);
			}

			if ($act) {
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else {
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}else {
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Kosong');
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	
	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$actDel = $this->tm_perbandinganharga->delete_data($id);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}

	public function action_deletedetail() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$actDel = $this->td_perbandinganharga_detail->delete_data($id);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}

	public function action_deletedetailkatalog() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$actDel = $this->td_perbandinganharga_detail_katalog->delete_data($id);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
