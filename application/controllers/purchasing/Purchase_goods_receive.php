<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Purchase_goods_receive extends MY_Controller {
	private $class_link = 'purchasing/purchase_goods_receive';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_rawmaterial_harga_history', 'td_rawmaterial_goodsreceive', 'tm_rawmaterial',
			'td_rawmaterial_harga', 'tm_currency']);
	}

	public function index() {
        parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
		parent::datetimepicker_assets();
		$this->form_box();
	}
		
	public function form_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function table_box(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$date = $this->input->get('date', true);

		$data['class_link'] = $this->class_link;
		$data['date'] = $date;

		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main(){
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }
		$date = $this->input->get('date', true);

		$data['class_link'] = $this->class_link;
		$data['date'] = $date;

		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data (){
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }
		$this->load->library(['ssp']);

		
		$data = $this->td_rawmaterial_goodsreceive->ssp_table_grpurchase();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function getGR (){
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }
		// $this->load->library(['ssp']);

		$rmgr_code_srj = $this->input->get('rmgr_code_srj', true);
		$no_srj = $this->input->get('no_srj', true);

		$update = $this->db->update('td_rawmaterial_goodsreceive', array('status_sap' => '1'), array('rmgr_code_srj' => $rmgr_code_srj,'rmgr_nosrj' => $no_srj ));
		
		$result = $this->db->select('*')
		->from('td_rawmaterial_goodsreceive')
		->join('td_purchaseorder_detail', 'td_purchaseorder_detail.podetail_kd=td_rawmaterial_goodsreceive.podetail_kd','inner')
		->join('tb_admin', 'tb_admin.kd_admin=td_rawmaterial_goodsreceive.admin_kd','inner')
		->where(array('rmgr_code_srj' => $rmgr_code_srj,'rmgr_nosrj' => $no_srj ))
		->get()->result_array();

		header('Content-Type: application/json');
		echo json_encode($result);
		
	}

	public function action_rollback()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$rmgr_code_srj = $this->input->get('rmgr_code_srj', true);
		$no_srj = $this->input->get('no_srj', true);

		$update = $this->db->update('td_rawmaterial_goodsreceive', array('status_sap' => '0'), array('rmgr_code_srj' => $rmgr_code_srj,'rmgr_nosrj' => $no_srj ));
		if ($update){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		}
		echo json_encode($resp);

	}
    
	public function get_grby_suratjalan() {
		// if (!$this->input->is_ajax_request()) {
		// 	exit('No direct script access allowed');
		// }

		$paramSRJ = $this->input->get('paramSRJ', true);
		
		$result = [];
		$this->db->select('td_rawmaterial_goodsreceive.rmgr_nosrj, td_rawmaterial_goodsreceive.rmgr_code_srj, tm_purchaseorder.po_no, tm_suplier.suplier_nama')
			->from('td_rawmaterial_goodsreceive')
			->join('td_purchaseorder_detail', 'td_rawmaterial_goodsreceive.podetail_kd=td_purchaseorder_detail.podetail_kd', 'left')
			->join('tm_purchaseorder', 'td_purchaseorder_detail.po_kd=tm_purchaseorder.po_kd', 'left')
			->join('tm_suplier', 'tm_purchaseorder.suplier_kd=tm_suplier.suplier_kd', 'left')
			->where(array('rmgr_tglgrpurch' => null, 'td_rawmaterial_goodsreceive.status_sap' => 0));
		if (!empty($paramSRJ)) {
			$this->db->group_start()
			->like('td_rawmaterial_goodsreceive.rmgr_nosrj', $paramSRJ)
			->or_like('tm_purchaseorder.po_no', $paramSRJ)
			->or_like('tm_suplier.suplier_nama', $paramSRJ)
			->or_like('td_rawmaterial_goodsreceive.rmgr_code_srj', $paramSRJ)
			->group_end();
		}
		$this->db->group_by(array("td_rawmaterial_goodsreceive.rmgr_code_srj"))
		->order_by('tm_purchaseorder.po_no', 'asc');
		$query = $this->db->get();
		if (!empty($query->num_rows())) {
			$query = $query->result_array();
			foreach ($query as $r) {
				$result[] = array(
					'id' => $r['rmgr_code_srj'],
					'text' => $r['rmgr_code_srj'].' || '.$r['rmgr_nosrj'],
				); 
			}
		}
		echo json_encode($result);
	}

	public function form_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function form_editharga_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$rmgr_kd = $this->input->get('id', true);
		
		$rowData = [];
		if (!empty($rmgr_kd)) {
			$rowData = $this->db->select('td_rawmaterial_goodsreceive.*, td_purchaseorder_detail.*, tm_purchaseorder.po_no, tm_suplier.suplier_nama, tm_rawmaterial.rm_kode')
						->from('td_rawmaterial_goodsreceive')
						->join('td_purchaseorder_detail', 'td_rawmaterial_goodsreceive.podetail_kd=td_purchaseorder_detail.podetail_kd', 'left')
						->join('tm_purchaseorder', 'td_purchaseorder_detail.po_kd=tm_purchaseorder.po_kd', 'left')
						->join('tm_suplier', 'tm_purchaseorder.suplier_kd=tm_suplier.suplier_kd', 'left')
						->join('tm_rawmaterial', 'td_rawmaterial_goodsreceive.rm_kd=tm_rawmaterial.rm_kd', 'left')
						->where('td_rawmaterial_goodsreceive.rmgr_kd', $rmgr_kd)
						->get()
						->row_array();
		}

		$data['class_link'] = $this->class_link;
		$data['rmgr_kd'] = $rmgr_kd;
		$data['rowData'] = $rowData;
		$this->load->view('page/'.$this->class_link.'/form_editharga_main', $data);
	}

	public function table_rmgrdetail () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$rmgr_nosrj = $this->input->get('rmgr_nosrj', true);
		
		$goodsreceive = [];
		if (!empty($rmgr_nosrj)) {	
			$goodsreceive = $this->db->select('td_rawmaterial_goodsreceive.rmgr_nosrj, td_rawmaterial_goodsreceive.podetail_kd, rmgr_qty, rmgr_qtykonversi, tm_purchaseorder.po_no, tm_suplier.kd_currency, tm_suplier.suplier_nama, tb_set_dropdown.nm_select,
						td_purchaseorder_detail.rm_kd, td_purchaseorder_detail.podetail_nama, td_purchaseorder_detail.podetail_deskripsi, td_purchaseorder_detail.podetail_spesifikasi, td_purchaseorder_detail.podetail_harga, td_purchaseorder_detail.podetail_konversi, td_purchaseorder_detail.podetail_qty,
						td_rawmaterial_satuan.rmsatuan_nama, tm_rawmaterial.rm_kode, td_rawmaterial_goodsreceive.rmgr_tgldatang, td_rawmaterial_goodsreceive.rmgr_kd,
						tm_currency.icon_type, tm_currency.currency_icon, tm_currency.pemisah_angka, tm_currency.format_akhir')
					->from('td_rawmaterial_goodsreceive')
					->join('td_purchaseorder_detail', 'td_rawmaterial_goodsreceive.podetail_kd=td_purchaseorder_detail.podetail_kd', 'left')
					->join('tm_rawmaterial', 'td_purchaseorder_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
					->join('td_rawmaterial_satuan', 'td_purchaseorder_detail.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
					->join('tm_purchaseorder', 'td_rawmaterial_goodsreceive.po_kd=tm_purchaseorder.po_kd', 'left')
					->join('tm_suplier', 'tm_purchaseorder.suplier_kd=tm_suplier.suplier_kd', 'left')
					->join('tb_set_dropdown', 'tm_suplier.badanusaha_kd=tb_set_dropdown.id', 'left')
					->join('tm_currency', 'tm_suplier.kd_currency=tm_currency.kd_currency')
					->where ('td_rawmaterial_goodsreceive.rmgr_code_srj', $rmgr_nosrj)
					->where('td_rawmaterial_goodsreceive.rmgr_tglgrpurch IS NULL')
					->order_by('td_rawmaterial_goodsreceive.podetail_kd', 'asc')
					->get()
					->result_array();
		}
		$data['rmgr_nosrj'] = $rmgr_nosrj;
		$data['goodsreceive'] = $goodsreceive;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_rmgrdetail', $data);
	}

	public function action_insert () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrmgr_hargaunit', 'Purchase Order', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrrmgr_hargaunit' => (!empty(form_error('txtrmgr_hargaunit')))?buildLabel('warning', form_error('txtrmgr_hargaunit', '"', '"')):'',
			);
			
		}else {
			$podetail_kd = $this->input->post('txtpodetail_kd', true);
			$podetail_konversi = $this->input->post('txtpodetail_konversi', true);
			$rmgr_hargaunit = $this->input->post('txtrmgr_hargaunit', true);
			$rmgr_nosrj = $this->input->post('txtrmgr_nosrj', true);
			$rm_kd = $this->input->post('txtrm_kd', true);

			$now = date('Y-m-d H:i:s');
			if ($podetail_konversi < 1) {
				$podetail_konversi = 1;
			}
			$hargakonversi = (float) $rmgr_hargaunit / $podetail_konversi;
			/** Update GR */
			$dataGRpurch = [
				'rmgr_hargaunit' => $rmgr_hargaunit,
				'rmgr_hargakonversi' => $hargakonversi,
				'rmgr_tglgrpurch' => $now,
			];
			/** Update harga RM */
			$dataRM = [
				'rm_kd' => $rm_kd,
				'rmharga_hargagr' => $hargakonversi,
				'rmharga_tgledit_hargagr' => $now,
			];
			/** Insert Hist Harga */
			$dataHistHarga = [
				'rmhrghist_kd' => $this->td_rawmaterial_harga_history->create_code(),
				'podetail_kd' => $podetail_kd,
				'rm_kd' => $rm_kd,
				'rmhrghist_harga' => $hargakonversi,
				'admin_kd' => $this->session->userdata('kd_admin'),
			];

			$this->db->trans_begin();

			$actGR = $this->td_rawmaterial_goodsreceive->update_data(['podetail_kd' => $podetail_kd, 'rmgr_nosrj' => $rmgr_nosrj], $dataGRpurch);
			if ($rm_kd != 'SPECIAL') {
				$actRM = $this->td_rawmaterial_harga->update_harga_gr($dataRM);
			}
			$actHistHarga = $this->td_rawmaterial_harga_history->insert_data($dataHistHarga);

			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}else{
				$this->db->trans_commit();
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['rmgr_nosrj' => $rmgr_nosrj]);
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_insertbatch () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$rmgr_kd = $this->input->post('txtrmgr_kd', true);
		$podetail_kd = $this->input->post('txtpodetail_kd', true);
		$podetail_konversi = $this->input->post('txtpodetail_konversi', true);
		$rmgr_hargaunit = $this->input->post('txtrmgr_hargaunit', true);
		$rm_kd = $this->input->post('txtrm_kd', true);

		$dataGRpurch = []; $dataRM = []; $dataHistHarga = [];
		$now = date('Y-m-d H:i:s');
		$admin_kd = $this->session->userdata('kd_admin');
		$rmhrghist_kd = $this->td_rawmaterial_harga_history->create_code();
		foreach ($rmgr_kd as $each) {
			// Update GR
			$hargakonversi = (float) $rmgr_hargaunit[$each] / $podetail_konversi[$each];
			$dataGRpurch[] = [
				'rmgr_kd' => $each,
				'rmgr_hargaunit' => $rmgr_hargaunit[$each],
				'rmgr_hargakonversi' => $hargakonversi,
				'rmgr_tglgrpurch' => $now,
			];
			/** Update harga RM */
			if ($rm_kd[$each] != 'SPECIAL') {
				$dataRM[] = [
					'rm_kd' => $rm_kd[$each],
					'rmharga_hargagr' => $hargakonversi,
					'rmharga_tgledit_hargagr' => $now,
				];
			}
			/** Insert Hist Harga */
			$dataHistHarga[] = [
				'rmhrghist_kd' => $rmhrghist_kd,
				'podetail_kd' => $podetail_kd[$each],
				'rm_kd' => $rm_kd[$each],
				'rmhrghist_harga' => $hargakonversi,
				'admin_kd' => $admin_kd,
			];
			$rmhrghist_kd ++;
		}

		$this->db->trans_begin();

		$actGR = $this->td_rawmaterial_goodsreceive->update_batch('rmgr_kd', $dataGRpurch);
		$actRM = $this->td_rawmaterial_harga->update_harga_gr_batch($dataRM);
		$actHistHarga = $this->td_rawmaterial_harga_history->insert_batch($dataHistHarga);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		}else{
			$this->db->trans_commit();
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_editharga () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrmgr_hargaunit', 'Harga', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrrmgr_hargaunit' => (!empty(form_error('txtrmgr_hargaunit')))?buildLabel('warning', form_error('txtrmgr_hargaunit', '"', '"')):'',
			);
			
		}else {
			$rmgr_kd = $this->input->post('txtrmgr_kd', true);
			$rmgr_qty = $this->input->post('txtrmgr_qty', true);
			$rmgr_qtykonversi = $this->input->post('txtrmgr_qtykonversi', true);
			$rmgr_hargaunit = $this->input->post('txtrmgr_hargaunit', true);
			$podetail_kd = $this->input->post('txtpodetail_kd', true);
			$rm_kd = $this->input->post('txtrm_kd', true);

			$now = date('Y-m-d H:i:s');
			$podetail_konversi = $rmgr_qtykonversi / $rmgr_qty;
			
			$hargakonversi = (float) $rmgr_hargaunit / $podetail_konversi;
			/** Update GR */
			$dataGRpurch = [
				'rmgr_hargaunit' => $rmgr_hargaunit,
				'rmgr_hargakonversi' => $hargakonversi,
				'rmgr_tgledit' => $now,
				'rmgr_tglgrpurch' => $now,
			];
			/** Update harga RM */
			$dataRM = [
				'rm_kd' => $rm_kd,
				'rmharga_hargagr' => $hargakonversi,
				'rmharga_tgledit_hargagr' => $now,
			];
			/** Insert Hist Harga */
			$dataHistHarga = [
				'rmhrghist_kd' => $this->td_rawmaterial_harga_history->create_code(),
				'podetail_kd' => $podetail_kd,
				'rm_kd' => $rm_kd,
				'rmhrghist_harga' => $hargakonversi,
				'admin_kd' => $this->session->userdata('kd_admin'),
			];

			$this->db->trans_begin();

			$actGR = $this->td_rawmaterial_goodsreceive->update_data(['rmgr_kd' => $rmgr_kd], $dataGRpurch);
			if ($rm_kd != 'SPECIAL') {
				$actRM = $this->td_rawmaterial_harga->update_harga_gr($dataRM);
			}
			$actHistHarga = $this->td_rawmaterial_harga_history->insert_data($dataHistHarga);

			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Update');
			}else{
				$this->db->trans_commit();
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	
}
