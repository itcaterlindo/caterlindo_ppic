<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Purchase_requisition_report extends MY_Controller {
	private $class_link = 'purchasing/purchase_requisition/purchase_requisition_report';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_purchaserequisition', 'tm_salesorder', 'td_purchaserequisition_detail', 'tb_admin', 'db_hrm/tb_karyawan', 'tb_purchaserequisition_logstatus', 'tb_purchaserequisition_status']);
    }
    

    public function index(){
        $this->load->library('Pdf');

        $id = $this->input->get('id');
        $id = url_decrypt($id);
        $data = array();
        
        if (!empty($id)){
            $rowPR = $this->tm_purchaserequisition->get_by_param (array('pr_kd'=>$id))->row_array();
            /** Ambil nama user pembuat */
            $requestBy = '';
            $tglrequestBy = $rowPR['pr_tglinput'];
            $rowAdmin = $this->tb_admin->get_by_param(array('kd_admin' => $rowPR['pr_user']))->row_array();
            if (!empty($rowAdmin['kd_karyawan'])){
                $rowKaryawan = $this->tb_karyawan->get_by_param(array('kd_karyawan' => $rowAdmin['kd_karyawan']))->row_array();
                $requestBy = $rowKaryawan['nm_karyawan'];
            }

            $resultPR = $this->td_purchaserequisition_detail->get_by_param_detail(array('pr_kd'=>$id))->result_array();
            $data['id'] = $id;
            $data['class_link'] = $this->class_link;
            $data['rowPR'] = $rowPR;
            $data['resultPR'] = $resultPR;
            $data['logs'] = $this->tb_purchaserequisition_logstatus->get_log_printout($id)->result_array();
        }
        
        $this->load->view('page/'.$this->class_link.'/purchase_requisition_pdf', $data);
    }
    	
}
