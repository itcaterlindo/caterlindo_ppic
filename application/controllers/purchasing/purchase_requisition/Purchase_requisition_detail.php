<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Purchase_requisition_detail extends MY_Controller {
    private $head_class_link = 'purchasing/purchase_requisition/purchase_requisition_main';
    private $class_link = 'purchasing/purchase_requisition/purchase_requisition_detail';
    private $class_linkReport = 'purchase/purchase_requisition/purchase_requisition_reprrt';
    private $wf_kd = 2;

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_purchaserequisition', 'td_purchaserequisition_detail', 'tb_admin', 'tb_purchaserequisition_logstatus', 'tb_relasi_popr', 'tb_purchaserequisition_status', 'tb_purchaserequisition_user', 
            'td_purchaseorder_detail', 'td_rawmaterial_goodsreceive', 'tm_workflow', 'td_workflow_transition', 'td_workflow_transition_notification']);
    }

    public function index() {
        $id = $this->input->get('id', true);
        parent::administrator();
        parent::pnotify_assets();
        if (!empty($id)){
            $id = url_decrypt($id);
            $this->table_box($id);
        }
    }

    public function table_box($id){
		$data['class_link'] = $this->class_link;
        $data['head_class_link'] = $this->head_class_link;
        $data['id'] = $id;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }

        $id = $this->input->get('id', true);

        if (!empty($id)){
            $rowPR = $this->db->select('a.*, b.*, c.nm_admin')
                    ->from('tm_purchaserequisition as a')
                    ->join('td_workflow_state as b', 'a.wfstate_kd=b.wfstate_kd', 'left')
                    ->join('tb_admin as c', 'c.kd_admin=a.pr_user', 'left')
                    ->where('a.pr_kd', $id)
                    ->get()->row_array();
            $data['rowPR'] = $rowPR;
            $data['resultPRdetail'] = $this->td_purchaserequisition_detail->get_by_param_detail(array('pr_kd' => $id))->result_array();
            $data['class_link'] = $this->class_link;
            $data['generateButtonAction'] = $this->tm_workflow->generate_button($id, $this->wf_kd, $rowPR['wfstate_kd']);

            /** Find PR yang sudah di PO kan */
            $result_relasipopr = $this->tb_relasi_popr->get_by_param_detail (array('tb_relasi_popr.pr_kd' => $id));
            if ($result_relasipopr->num_rows() != 0){
                $result_relasipopr = $result_relasipopr->result_array();
                $array_po = array();
                foreach ($result_relasipopr as $r){
                    $array_po [] = $r['po_kd'];
                }
                $data['result_detailPO'] = $this->td_purchaseorder_detail->get_where_in ('td_purchaseorder_detail.po_kd', $array_po)->result_array();
                $data['result_detailGR'] = $this->td_rawmaterial_goodsreceive->get_where_in ('po_kd', $array_po)->result_array(); 
            }           

            $this->load->view('page/'.$this->class_link.'/table_main', $data);
        }
        
    }

    public function view_log() {
		$id = $this->input->get('id', true);

		$data['result'] = $this->db->select('tb_purchaserequisition_logstatus.*, td_workflow_transition.*, tb_admin.nm_admin, state_source.wfstate_nama as state_source, state_dst.wfstate_nama as state_dst')
                ->from('tb_purchaserequisition_logstatus')
				->join('td_workflow_transition', 'tb_purchaserequisition_logstatus.wftransition_kd=td_workflow_transition.wftransition_kd', 'left')
				->join('td_workflow_state as state_source', 'state_source.wfstate_kd=td_workflow_transition.wftransition_source', 'left')
				->join('td_workflow_state as state_dst', 'state_dst.wfstate_kd=td_workflow_transition.wftransition_destination', 'left')
				->join('tb_admin', 'tb_purchaserequisition_logstatus.admin_kd=tb_admin.kd_admin', 'left')
				->where('tb_purchaserequisition_logstatus.pr_kd', $id)
				->order_by('tb_purchaserequisition_logstatus.prlogstatus_tglinput', 'desc')
				->get()->result_array();

		$this->load->view('page/'.$this->class_link.'/view_log', $data);
	}

    public function ubah_status_main(){
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }

        $akses = $this->input->get('akses', true);
        $pr_kd = $this->input->get('pr_kd', true);
        $prstatus_level = $this->input->get('level', true);

        /** Opsi PR Status */
        $actopsiPRstatus = $this->tb_purchaserequisition_status->get_all()->result_array();
		$opsiPRstatus = [];
		foreach ($actopsiPRstatus as $eachPRstatus):
			$opsiPRstatus[$eachPRstatus['prstatus_kd']] = $eachPRstatus['prstatus_nama'];
        endforeach;

        $data['class_link'] = $this->class_link;
        $data['pr_kd'] = $pr_kd;
        $data['prstatus_level'] = $prstatus_level;
        $data['opsiPRstatus'] = $opsiPRstatus;
        $data['akses'] = $akses;
        
        $this->load->view('page/'.$this->class_link.'/ubah_status_main', $data);
    }

    public function log_status_main(){
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }

        $id = $this->input->get('id', true);

        if (!empty($id)){
            $resultData = $this->tb_purchaserequisition_logstatus->get_by_param_detail (['tb_purchaserequisition_logstatus.pr_kd' => $id]);
            
            $data['id'] = $id;
            $data['resultData'] = $resultData->result_array();
    
            $this->load->view('page/'.$this->class_link.'/log_status_main', $data);
        }
    }

    public function formubahstate_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id');
		$wftransition_kd = $this->input->get('wftransition_kd');
		
		$data['id'] = $id;
		$data['wftransition_kd'] = $wftransition_kd;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/formubahstate_main', $data);
	}

    private function action_special ($action = null, $aParam = []) {
        $act = true;
        $admin_kd = $this->session->userdata('kd_admin');
        switch ($action) {
            case 'PR_UPDATE_TGL' :
                $act = $this->tm_purchaserequisition->update_data(['pr_kd' => $aParam['pr_kd']], ['pr_tanggal' => date('Y-m-d'), 'admin_kd' => $admin_kd]);
                break;
            case 'PR_UPDATE_CANCEL_1' :
                $act = $this->tm_purchaserequisition->update_data(['pr_kd' => $aParam['pr_kd']], ['pr_cancelorder' => '1', 'pr_closeorder' => '1', 'pr_tgledit' => date('Y-m-d H:i:s'), 'admin_kd' => $admin_kd]);
                break;
            case 'PR_UPDATE_CANCEL_0' :
                $act = $this->tm_purchaserequisition->update_data(['pr_kd' => $aParam['pr_kd']], ['pr_cancelorder' => '0', 'pr_closeorder' => '1', 'pr_tgledit' => date('Y-m-d H:i:s'), 'admin_kd' => $admin_kd]);
                break;
            case 'PR_UPDATE_CLOSEORDER_0':
                $act = $this->tm_purchaserequisition->update_data(['pr_kd' => $aParam['pr_kd']], ['pr_closeorder' => '0', 'pr_tgledit' => date('Y-m-d H:i:s'), 'admin_kd' => $admin_kd]);
                break;
            case 'PR_UPDATE_CLOSEORDER_1':
                $act = $this->tm_purchaserequisition->update_data(['pr_kd' => $aParam['pr_kd']], ['pr_closeorder' => '1', 'pr_tgledit' => date('Y-m-d H:i:s'), 'admin_kd' => $admin_kd]);
                break;
            default :
                $act = true;
        }
        
        return $act;
    }

    public function action_change_state() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtid', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtwftransition_kd', 'Transition', 'required', ['required' => '{field} tidak boleh kosong!']);		

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrrm_kd' => (!empty(form_error('txtid')))?buildLabel('warning', form_error('txtid', '"', '"')):'',
				'idErrrm_kd' => (!empty(form_error('txtwftransition_kd')))?buildLabel('warning', form_error('txtwftransition_kd', '"', '"')):'',
			);
			
		}else {
			$pr_kd = $this->input->post('txtid', true);
			$wftransition_kd = $this->input->post('txtwftransition_kd', true);
			$prlogstatus_keterangan = $this->input->post('txtlog_note', true);

			$data = [
                'pr_kd' => $pr_kd,
                'wftransition_kd' => $wftransition_kd,
                'prlogstatus_user' => $this->session->userdata('kd_admin'),
                'prlogstatus_keterangan' => $prlogstatus_keterangan,
                'prlogstatus_tglinput' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            ]; 
			$this->db->trans_begin();
			$pr = $this->tm_purchaserequisition->get_by_param(['pr_kd' => $pr_kd])->row_array();
			$wftransition = $this->td_workflow_transition->get_by_param(['wftransition_kd' => $wftransition_kd])->row_array();
			$dataUpdate = [
				'wfstate_kd' => $wftransition['wftransition_destination'],
				'pr_tgledit' => date('Y-m-d H:i:s'),
			];
			#email message notification
			//$subject = 'Purchase Requisition - '.$pr['pr_no'];
            $dtMessage['subjcet'] = $pr['pr_no'];
			$dtMessage['text'] = 'Terdapat Purchase Requisition yang perlu approval Anda :';
			$dtMessage['url'] = 'https://'.$_SERVER['HTTP_HOST'].base_url().$this->class_link.'?id='.url_encrypt($pr_kd);
			$dtMessage['urltext'] = 'Detail Purchase Requisition';
			$dtMessage['keterangan'] = !empty($prlogstatus_keterangan) ? $prlogstatus_keterangan : null;
			$message = $this->load->view('templates/email/email_notification', $dtMessage, true);

            #special action
            if (!empty($wftransition['wftransition_action'])) {
                $expAction = explode(';', $wftransition['wftransition_action']);
                for ($i=0; $i<count($expAction); $i++){
                    $actSpecial = $this->action_special($expAction[$i], $data);
                }
            }

			$act = $this->tb_purchaserequisition_logstatus->insert_data($data);
			$actUpdate = $this->tm_purchaserequisition->update_data (['pr_kd' => $pr_kd], $dataUpdate);


			//$actNotif = $this->td_workflow_transition_notification->generate_notification($wftransition_kd, $subject, $message);

            $SttsNotif =  $this->td_workflow_transition_notification->getStatus_generate_notification($wftransition_kd);
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}else{
				$this->db->trans_commit();
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'stts' =>  $SttsNotif, 'msg' => $dtMessage);
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

    public function action_closeorder () {
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $pr_kd = $this->input->get('pr_kd', true);

        if (!empty($pr_kd)) {
            $act = $this->action_special ('PR_UPDATE_CLOSEORDER_1', ['pr_kd' => $pr_kd]);
            if ($act) {
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
        }else{
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'ID tidak ditemukan');
        }
        echo json_encode($resp);
    }
   	
}
