<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Purchase_requisition_main extends MY_Controller {
	private $class_link = 'purchasing/purchase_requisition/purchase_requisition_main';
	private $class_linkRekomendasi = 'purchasing/purchase_requisition/purchase_requisition_rekomendasi';
	private $class_linkReport = 'purchasing/purchase_requisition/purchase_requisition_report';
	private $detail_class_link = 'purchasing/purchase_requisition/purchase_requisition_detail';
	private $wf_kd = 2;

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_rawmaterial','tm_purchaserequisition', 'td_purchaserequisition_detail', 'db_hrm/tb_bagian', 'tb_admin', 'td_rawmaterial_satuan', 'db_hrm/tb_karyawan', 'pr_has_ticket', 'td_workflow_state']);
	}

	public function index() {
        parent::administrator();
		parent::pnotify_assets();
        parent::select2_assets();
        parent::datetimepicker_assets();
		$this->table_box();
    }
    
	public function form_box() {
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');
		
		
        if ($sts == 'edit'){
			$act = $this->tm_purchaserequisition->get_by_param (array('pr_kd'=> $id))->row_array();
			$data['ticket_kd'] = $act['ticket_kd'];
        }

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$data['class_linkRekomendasi'] = $this->class_linkRekomendasi;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
    }
    
    public function form_master_main(){
        // if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
        // }
        $sts = $this->input->get('sts');
		$id = $this->input->get('id');
		$BagianSelected = "";
		
        if ($sts == 'edit'){
			$act = $this->tm_purchaserequisition->get_by_param (array('pr_kd'=> $id))->row_array();			
            $data['rowData'] = $act;
			$data['ticket_kd'] = '';
			$BagianSelected = $act['pr_bagian'];

			$pr_tk = $this->pr_has_ticket->get_by_param(array('pr_kd'=> $id))->result_array();	
			$data['data_prtk'] = $pr_tk;
        }

        /** Opsi Bagian */
        $actopsiBagian = $this->tb_bagian->get_all ()->result_array();
		$opsiBagian[''] = '-- Pilih Bagian --';
		foreach ($actopsiBagian as $eachBagian):
			$opsiBagian[$eachBagian['kd_bagian']] = $eachBagian['nm_bagian'];
        endforeach;
        
        /** PR User */
        $admin_kd = $this->session->userdata('kd_admin');
		$actAdmin = (array) $this->tb_admin->get_row($admin_kd);
		/** User bagian */
		$kd_karyawan = $actAdmin['kd_karyawan'];
		if (!empty($kd_karyawan)){
			$rowKaryawan = $this->tb_karyawan->get_by_param(array('kd_karyawan' => $kd_karyawan))->row_array();
			// $BagianSelected = $rowKaryawan['kd_bagian'];
		}
        
        $data['rowDataUser'] = $actAdmin;
        $data['opsiBagian'] = $opsiBagian;
        $data['BagianSelected'] = $BagianSelected;
        $data['pr_kd'] = $id;
        $data['sts'] = $sts;
        $data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_master_main', $data);
		// header('Content-Type: application/json');
		// echo json_encode($data);
    }

	public function formmain_box () {
		parent::administrator();
		parent::pnotify_assets();
        parent::select2_assets();
        parent::datetimepicker_assets();
		
		$id = $this->input->get('id', true);
		$id = url_decrypt($id);
		if ($id == false) {
			show_error('ID tidak ditemukan...!!! <br> <a href="'.base_url().'"> Home </a>', '202', 'Error ID');
		}

		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$data['class_linkRekomendasi'] = $this->class_linkRekomendasi;

		$this->load->view('page/'.$this->class_link.'/formmain_box', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$id = $this->input->get('id', true);
		$sts = $this->input->get('sts', true);
        
		if ($sts == 'edit'){
			$act = $this->tm_purchaserequisition->get_by_param (array('pr_kd'=> $id))->row_array();			
			$data['rowData'] = $act;
			$data['ticket_kd'] = $act['ticket_kd'];
        }

        /** Master PR */
        $actPR = $this->db->select()->from('tm_purchaserequisition as a')
			->join('td_workflow_state as b','a.wfstate_kd=b.wfstate_kd','left')
			->join('tb_admin as c','a.admin_kd=c.kd_admin','left')
			->where('a.pr_kd', $id)
			->get()->row_array();
        $actBagian = $this->tb_bagian->get_by_param (array('kd_bagian'=> $actPR['pr_bagian']))->row_array();

        /** Opsi Jenis Material */
        $opsiJenisMaterial = array(
            'STD' => 'Standart',
            'NEW' => 'Special',
        );
        
        /** Opsi Satuan */
		$actopsiSatuan = $this->td_rawmaterial_satuan->get_all()->result_array();
		$opsiSatuan[null] = '-- Pilih Opsi --';
		foreach ($actopsiSatuan as $eachSatuan):
			$opsiSatuan[$eachSatuan['rmsatuan_kd']] = $eachSatuan['rmsatuan_nama'];
        endforeach;

        $data['opsiJenisMaterial'] = $opsiJenisMaterial;
        $data['opsiSatuan'] = $opsiSatuan;
        $data['rowDataMasterPR'] = $actPR;
        $data['bagian_nama'] = $actBagian['nm_bagian'];
		$data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
		// header('Content-Type: application/json');
		// echo json_encode($data);
    }
    
	public function table_box() {
		$tahun = $this->input->get('Y');
		$bulan = $this->input->get('m');
		$wfstate_kd = $this->input->get('wfstate_kd');

		$data['class_link'] = $this->class_link;
		$data['class_linkReport'] = $this->class_linkReport;
		$data['detail_class_link'] = $this->detail_class_link;
		
		#opsi bulan
        $months = bulanIndo();
        $months = array_flip($months);
        $months['ALL'] = 'ALL';
        $data['months'] = $months;

		#opsi states
		$states = $this->td_workflow_state->get_by_param(['wf_kd' => $this->wf_kd])->result_array();
		$aState['ALL'] = 'ALL';
		foreach($states as $state) {
			$aState[$state['wfstate_kd']] = $state['wfstate_nama']; 
		}
		$data['states'] = $aState;

		#params
		$tahun = !empty($tahun) ? $tahun : date('Y');
		$bulan = !empty($bulan) ? $bulan : date('m');
		$wfstate_kd = !empty($wfstate_kd) ? $wfstate_kd : 'ALL';
		$data['tahun'] = $tahun;
		$data['bulan'] = $bulan;
		$data['wfstate_kd'] = $wfstate_kd;
		$data['params'] = "?Y=$tahun&m=$bulan&wfstate_kd=$wfstate_kd";

		
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
        // }
        
		$data['class_link'] = $this->class_link;
		$data['params'] = $this->input->get('params');
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }
	
	public function table_data() {
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }
        $this->load->library(['ssp']);
        
		$tahun = $this->input->get('Y', true);
		$bulan = $this->input->get('m', true);
		$wfstate_kd = $this->input->get('wfstate_kd', true);

		$params = ['tahun' => $tahun, 'bulan' => $bulan, 'wfstate_kd' => $wfstate_kd];
		$data = $this->tm_purchaserequisition->ssp_table($params);
		header('Content-Type: application/json');
		//echo json_encode($data);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'], $data['groupBy'])
		);
    }
    
    public function table_detail_main(){
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        $sts = $this->input->get('sts');
		$id = $this->input->get('id');

        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_detail_main', $data);
    }

    public function table_detail_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $this->load->library(['ssp']);

        $sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data = $this->td_purchaserequisition_detail->ssp_table($id);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}
	
	public function table_partial_material(){
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }

        $id = $this->input->get('id', true);

        if (!empty($id)){
            $resultData = $this->tm_rawmaterial->get_all_detail_material()->result_array();

            $data['id'] = $id;
            $data['resultData'] = $resultData;
            $this->load->view('page/'.$this->class_link.'/table_partial_material', $data);
        }
    }

    function getMaterialData(){
        if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $id = $this->input->get('id');
        if (!empty($id)){
            $act = $this->tm_rawmaterial->get_detail_material(array('tm_rawmaterial.rm_kd'=>$id))->row_array();
            $resp = array('code'=> 200, 'status' => 'Sukses', 'data' => $act);
        }else{
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Data tdk ditemukan');
        }
        
        echo json_encode($resp);
    }
    
    function getDetailPR(){
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
        $id = $this->input->get('id');
        if (!empty($id)){
			$act = $this->db->select('td_purchaserequisition_detail.*, tm_rawmaterial.rm_kode')
					->from('td_purchaserequisition_detail')
					->join('tm_rawmaterial', 'td_purchaserequisition_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
					->where('td_purchaserequisition_detail.prdetail_kd', $id)
					->get();
            if ($act->num_rows() > 0 ){
				$row = $act->row_array();
				$prdetailduedate_format = ['prdetailduedate_format' => format_date($row['prdetail_duedate'], 'Y-m-d')];
				$result = array_merge($row, $prdetailduedate_format);
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => '', 'data' => $result);
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Tidak Ditemukan');
            }
        }else{
            $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Kosong');
        }

        echo json_encode($resp);
    }

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtRm_kd', 'Kode Material', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtprdetail_nama', 'Nama Material', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtprdetail_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtprdetail_duedate', 'DueDate', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtrmsatuan_kd', 'Satuan', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrRm_kd' => (!empty(form_error('txtRm_kd')))?buildLabel('warning', form_error('txtRm_kd', '"', '"')):'',
				'idErrprdetail_nama' => (!empty(form_error('txtprdetail_nama')))?buildLabel('warning', form_error('txtprdetail_nama', '"', '"')):'',
				'idErrprdetail_qty' => (!empty(form_error('txtprdetail_qty')))?buildLabel('warning', form_error('txtprdetail_qty', '"', '"')):'',
				'idErrprdetail_duedate' => (!empty(form_error('txtprdetail_duedate')))?buildLabel('warning', form_error('txtprdetail_duedate', '"', '"')):'',
				'idErrrmsatuan_kd' => (!empty(form_error('txtrmsatuan_kd')))?buildLabel('warning', form_error('txtrmsatuan_kd', '"', '"')):'',
			);
			
		}else {
            $pr_kd = $this->input->post('txtpr_kd', true);
            $rm_kd = $this->input->post('txtRm_kd', true);
            $prdetail_qty = $this->input->post('txtprdetail_qty', true);
            $prdetail_duedate = $this->input->post('txtprdetail_duedate', true);
			$prdetail_remarks = $this->input->post('txtprdetail_remarks', true);
			$prdetail_nama = $this->input->post('txtprdetail_nama', true);
			$prdetail_deskripsi = $this->input->post('txtprdetail_deskripsi', true);
			$prdetail_spesifikasi = $this->input->post('txtprdetail_spesifikasi', true);
			$rmsatuan_kd = $this->input->post('txtrmsatuan_kd', true);
			$prdetail_konversi = $this->input->post('txtprdetail_konversi', true);
			$prdetail_qtykonversi = $this->input->post('txtprdetail_qtykonversi', true);
			$budgeting_detail_kd = $this->input->post('txtbudgeting_detail_kd', true);

			if (!empty($prdetail_duedate)){
                $prdetail_duedate = format_date($prdetail_duedate, 'Y-m-d');
            }else{
                $prdetail_duedate = null;
            }
			
			$prdqty = !empty($prdetail_qty) ? $prdetail_qty:1;
			$prdkon = !empty($prdetail_konversi) ? $prdetail_konversi:1;
			$prdqtykonreal = $prdqty * $prdkon;
			//$prdqtykon = !empty($prdetail_qtykonversi) ? $prdetail_qtykonversi:null;
			$data = array(
                'prdetail_kd' => $this->td_purchaserequisition_detail->create_code(),
                'pr_kd' => $pr_kd,
				'rm_kd' => !empty($rm_kd) ? $rm_kd:null,
				'prdetail_nama' => !empty($prdetail_nama) ? $prdetail_nama:null,
				'prdetail_deskripsi' => !empty($prdetail_deskripsi) ? $prdetail_deskripsi:null,
				'prdetail_spesifikasi' => !empty($prdetail_spesifikasi) ? $prdetail_spesifikasi:null,
                'prdetail_qty' => $prdqty,
                'prdetail_konversi' => $prdkon,
                'prdetail_qtykonversi' => !empty($prdetail_qtykonversi) ? $prdetail_qtykonversi:$prdqtykonreal,
                'rmsatuan_kd' => !empty($rmsatuan_kd) ? $rmsatuan_kd:'-',
                'prdetail_duedate' => $prdetail_duedate,
                'prdetail_remarks' => !empty($prdetail_remarks) ? $prdetail_remarks:null,
				'prdetail_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
				'budgeting_detail_kd' => !empty($budgeting_detail_kd) ? $budgeting_detail_kd:null,
			);

            $act = $this->td_purchaserequisition_detail->insert_data($data);
            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
    }

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtprdetail_nama', 'Nama Material', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtprdetail_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtrmsatuan_kd', 'Satuan', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrprdetail_nama' => (!empty(form_error('txtprdetail_nama')))?buildLabel('warning', form_error('txtprdetail_nama', '"', '"')):'',
				'idErrprdetail_qty' => (!empty(form_error('txtprdetail_qty')))?buildLabel('warning', form_error('txtprdetail_qty', '"', '"')):'',
				'idErrrmsatuan_kd' => (!empty(form_error('txtrmsatuan_kd')))?buildLabel('warning', form_error('txtrmsatuan_kd', '"', '"')):'',
			);
			
		}else {
            $prdetail_kd = $this->input->post('txtprdetail_kd', true);
            $rm_kd = $this->input->post('txtRm_kd', true);
            $prdetail_qty = $this->input->post('txtprdetail_qty', true);
            $prdetail_duedate = $this->input->post('txtprdetail_duedate', true);
            $prdetail_duedate = empty($prdetail_duedate) ? $prdetail_duedate : format_date($prdetail_duedate, 'Y-m-d');
			$prdetail_remarks = $this->input->post('txtprdetail_remarks', true);
			$prdetail_nama = $this->input->post('txtprdetail_nama', true);
			$prdetail_deskripsi = $this->input->post('txtprdetail_deskripsi', true);
			$prdetail_spesifikasi = $this->input->post('txtprdetail_spesifikasi', true);
			$rmsatuan_kd = $this->input->post('txtrmsatuan_kd', true);
			$prdetail_konversi = $this->input->post('txtprdetail_konversi', true);
			$prdetail_qtykonversi = $this->input->post('txtprdetail_qtykonversi', true);
			$budgeting_detail_kd = $this->input->post('txtbudgeting_detail_kd', true);
			
			
			$data = array(
				'rm_kd' => !empty($rm_kd) ? $rm_kd:null,
				'prdetail_nama' => !empty($prdetail_nama) ? $prdetail_nama:null,
				'prdetail_deskripsi' => !empty($prdetail_deskripsi) ? $prdetail_deskripsi:null,
				'prdetail_spesifikasi' => !empty($prdetail_spesifikasi) ? $prdetail_spesifikasi:null,
				'prdetail_qty' => !empty($prdetail_qty) ? $prdetail_qty:null,
				'prdetail_konversi' => !empty($prdetail_konversi) ? $prdetail_konversi:1,
                'prdetail_qtykonversi' => !empty($prdetail_qtykonversi) ? $prdetail_qtykonversi:null,
				'rmsatuan_kd' => !empty($rmsatuan_kd) ? $rmsatuan_kd:'-',
                'prdetail_duedate' => !empty($prdetail_duedate) ? $prdetail_duedate:null,
                'prdetail_remarks' => !empty($prdetail_remarks) ? $prdetail_remarks:null,
				'prdetail_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
				'budgeting_detail_kd' => !empty($budgeting_detail_kd) ? $budgeting_detail_kd:null,
			);

            $act = $this->td_purchaserequisition_detail->update_data(array('prdetail_kd' => $prdetail_kd), $data);
            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
    }
    
    public function action_master_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpr_bagian', 'Bagian', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpr_bagian' => (!empty(form_error('txtpr_bagian')))?buildLabel('warning', form_error('txtpr_bagian', '"', '"')):'',
			);
			
		}else {
			$pr_kd = $this->input->post('txtpr_kd', true);
            $pr_user = $this->input->post('txtpr_user', true);
            $pr_bagian = $this->input->post('txtpr_bagian', true);
            $pr_note = $this->input->post('txtpr_note', true);
			$ticket_kd = $this->input->post('txtticket_kd', true);

			
			$data = array(
                'pr_note' => !empty($pr_note) ? $pr_note:null,
                'pr_bagian' => !empty($pr_bagian) ? $pr_bagian:null,
				'admin_kd' => $this->session->userdata('kd_admin'),
				'ticket_kd' => !empty($ticket_kd) ? $ticket_kd:null
			);

			if (empty($pr_kd)) {
				#add
				$wfstate = $this->td_workflow_state->get_by_param(['wf_kd' => $this->wf_kd, 'wfstate_initial' => '1'])->row_array();
				$pr_kd = $this->tm_purchaserequisition->create_code();
				$arrAdd = [
					'pr_kd' => $pr_kd,
					'pr_user' => !empty($pr_user) ? $pr_user:null,
					'pr_tanggal' => date('Y-m-d'),
					'pr_no' => $this->tm_purchaserequisition->create_no(),
					'wf_kd' => $this->wf_kd,
					'wfstate_kd' => $wfstate['wfstate_kd'],
					'pr_closeorder' => '1',
					'pr_cancelorder' => '0',
					'pr_tglinput' => date('Y-m-d H:i:s'),
				];
				$data = array_merge($data, $arrAdd);

				$act = $this->tm_purchaserequisition->insert_data($data);
			}else{
				#update
				$act = $this->tm_purchaserequisition->update_data(['pr_kd' => $pr_kd], $data);
			}

            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => array_merge($data, ['id' => url_encrypt($pr_kd)]), 'id' => $pr_kd);
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
				
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}
	
	public function action_rekomendasi_insert(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		
		$pr_kd = $this->input->post('txtpr_kdRekomendasi', true);
		$rm_kd = $this->input->post('txtrm_kd', true);
		

		foreach ($rm_kd as $each){
			$data[] = array(
				'prdetail_kd' => '1',
				'pr_kd' => $pr_kd,
				'rm_kd' => !empty($rm_kd) ? $rm_kd:null,
				'prdetail_qty' => !empty($prdetail_qty) ? $prdetail_qty:null,
			);
		}

        $resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($data);
	}
    
	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);

		if (!empty($id)){
			$actDelDetail = $this->td_purchaserequisition_detail->delete_by_param(['pr_kd' => $id]);
			$actDel = $this->tm_purchaserequisition->delete_data($id);
			if ( $actDelDetail && $actDel){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
			}
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Kosong');
		}
		
		echo json_encode($resp);
    }
    
    public function action_detail_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$actDel = $this->td_purchaserequisition_detail->delete_data($id);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}

	public function action_insert_ticket(){
		$data = $this->input->post('data', TRUE);
		
		$act = $this->pr_has_ticket->insert_batch_data($data);
            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terupdate');
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }

			echo json_encode($resp);

	}

	public function action_delete_detail_ticket() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$actDel = $this->pr_has_ticket->delete_data($id);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
