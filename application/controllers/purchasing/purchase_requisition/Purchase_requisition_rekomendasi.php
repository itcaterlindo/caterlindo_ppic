<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Purchase_requisition_rekomendasi extends MY_Controller {
	private $class_link = 'purchase/purchase_requisition/purchase_requisition_rekomendasi';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_rawmaterial', 'tm_salesorder']);
    }
    

    public function table_rekomendasi_material(){
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
        $sts = $this->input->get('sts');
        $id = $this->input->get('id');
        
        $act = $this->tm_rawmaterial->get_rekomendasi_pr()->result_array();

        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        $data['resultData'] = $act;
		$this->load->view('page/'.$this->class_link.'/table_rekomendasi_material', $data);
    }

    public function form_rekomendasi_po(){
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
        $sts = $this->input->get('sts');
        $id = $this->input->get('id');
        
        $actopsiSO = $this->tm_salesorder->get_by_status(array('process_lpo', 'process_wo'))->result_array();
        foreach ($actopsiSO as $eachSO):
            $text = $eachSO['no_po'];
            if ($eachSO['tipe_customer'] == 'Lokal'){
                $text = $eachSO['no_salesorder'];
            }
			$opsiSO[$eachSO['kd_msalesorder']] = $text;
        endforeach;

        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        $data['opsiSO'] = $opsiSO;
		$this->load->view('page/'.$this->class_link.'/form_rekomendasi_po', $data);
    }
    
	
}
