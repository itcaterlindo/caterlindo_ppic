<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class purchasereport_leadtime_suplier extends MY_Controller {
	private $class_link = 'purchasing/purchase_report/purchasereport_leadtime_suplier';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_purchaserequisition', 'tm_purchaseorder', 'tm_suplier', 'db_hrm/tb_hari_efektif']);
    }

    public function index() {
        parent::administrator();
        parent::handsonetable();
        parent::pnotify_assets();
        parent::select2_assets();
        $this->table_box();
    }

	 
	public function table_box(){
        $supliers = $this->tm_suplier->get_all()->result_array();
        $po = $this->tm_purchaseorder->get_all()->result_array();


        $aSuplier['ALL'] = '-- Pilih Suplier --';
        foreach ($supliers as $suplier) {
            $aSuplier[$suplier['suplier_kd']] = $suplier['suplier_kode'].' | '.$suplier['suplier_nama'];
        }
        $data['supliers'] = $aSuplier;
        $suplier_kd = !empty($suplier_kd) ? $suplier_kd : '';

        $aPo['ALL'] = '-- Pilih PO --';
        foreach ($po as $no_po) {
            $aPo[$no_po['po_no']] = $no_po['po_no'];
        }
        $data['po'] = $aPo;
        $po_no = !empty($po_no) ? $po_no : '';



        $data['suplier_kd'] = $suplier_kd;
        $data['po_no'] = $po_no;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

    public function table_main () {
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
        // }
		$data['class_link'] = $this->class_link;

		$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }  

    public function testgetData()
    {
        $sup = $this->input->get('sup');
        $new_sup = array();
        $bulan = '09';
        $tahun = '2022';
        for($i=0; $i<count($sup); $i++){
            array_push($new_sup, '"'.$sup[$i].'"');
        }
        $new_sup = implode(", ", $new_sup);

        $data['suplier_kd'] = $sup;
        $data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
        $data['maszeh'] = $new_sup;

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function getData()
    {
        $sup = $this->input->get('sup');
        $filter = $this->input->get('filter');
        $bulan = $this->input->get('bulan');
        $tahun = $this->input->get('tahun');
        $po_no = $this->input->get('po_no');

        $getLt = $this->tm_purchaseorder->getLeadtime($sup, $filter, $bulan, $tahun, $po_no);
        
        header('Content-Type: application/json');
        echo json_encode($getLt);

    }

    private function group_by($key, $data) {
		$result = array();
		foreach($data as $val) {
			if(array_key_exists($key, $val)){
				$result[$val[$key]][] = $val;
			}else{
				$result[""][] = $val;
			}
		}
		return $result;
	}

        
          
}
