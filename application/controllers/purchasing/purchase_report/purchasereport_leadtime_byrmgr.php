<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class purchasereport_leadtime_byrmgr extends MY_Controller {
	private $class_link = 'purchasing/purchase_report/purchasereport_leadtime_byrmgr';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_purchaserequisition', 'tm_purchaseorder', 'tm_suplier', 'db_hrm/tb_hari_efektif']);
    }

    public function index() {
        parent::administrator();
        parent::handsonetable();
        parent::pnotify_assets();
        parent::select2_assets();
        $this->table_box();
    }

	 
	public function table_box(){
        $supliers = $this->tm_suplier->get_all()->result_array();
        $po = $this->tm_purchaseorder->get_all()->result_array();


        $aSuplier['ALL'] = '-- Pilih Suplier --';
        foreach ($supliers as $suplier) {
            $aSuplier[$suplier['suplier_kd']] = $suplier['suplier_kode'].' | '.$suplier['suplier_nama'];
        }
        $data['supliers'] = $aSuplier;
        $suplier_kd = !empty($suplier_kd) ? $suplier_kd : '';

        $data['suplier_kd'] = $suplier_kd;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

    public function table_main () {
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
        // }
		$data['class_link'] = $this->class_link;

		$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }  

    public function testgetData()
    {
        $sup = $this->input->get('sup');
        $new_sup = array();
        $bulan = '06';
        $tahun = '2021';
        for($i=0; $i<count($sup); $i++){
            array_push($new_sup, '"'.$sup[$i].'"');
        }
        $new_sup = implode(", ", $new_sup);

        $data['suplier_kd'] = $sup;
        $data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
        $data['maszeh'] = $new_sup;

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function getData()
    {
        // echo 'ok';
        $sup = $this->input->get('sup');
        $new_sup = array();
        $bulan = $this->input->get('bulan');
        $tahun = $this->input->get('tahun');
        $smt = $this->input->get('smt');

        for($i=0; $i<count($sup); $i++){
            array_push($new_sup, '"'.$sup[$i].'"');
        }
        
        $new_sup = implode(", ", $new_sup);

            
            
            if($smt == '0'){
                $query  =   "SET @bulan := '". $bulan ."', @tahun := '". $tahun ."'";
                $this->db->query($query);

                $newquery = $this->db->query("SELECT
                suplier_kd as sup_kd_real,
                suplier_nama,
                rmgroupsup_name,
                (COUNT(po_count)) as po_count_real,
                (SELECT 
                            COUNT(*)
                            FROM (SELECT
                                    rmgr.*,
                                    rm.rmgroupsup_kd,
                                    sup.suplier_kd
                                FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL' AND rmgr_type != 'RETURN') AS rmgr
                                INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                                INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                                INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                                INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                                INNER JOIN td_purchaseorder_detail pod on rmgr.podetail_kd = pod.podetail_kd GROUP BY rmgr_nosrj) AS rmgr
                                WHERE suplier_kd = sup_kd_real AND rmgroupsup_kd IS NOT NULL AND YEAR(rmgr_tgldatang) = @tahun AND MONTH(rmgr_tgldatang) = @bulan) AS gr_count_real,
                (SELECT
                            COUNT(*)
                            FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL') AS rmgr
                            INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                            INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                            INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                            INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                    WHERE sup.suplier_kd = sup_kd_real AND rm.rmgroupsup_kd IS NOT NULL AND MONTH(rmgr.rmgr_tgldatang) = @bulan AND YEAR(rmgr.rmgr_tgldatang) = @tahun AND rmgr.rmgr_type = 'RETURN'
                    GROUP BY sup.suplier_kd) as return_count_real,
    
                    (SELECT FLOOR(SUM(po_qty_real)) FROM 
                                        (
                            SELECT 
                                                        pod.podetail_qty AS po_qty_real,
                                                        sup.suplier_kd,
                                                        rm.rmgroupsup_kd,
                                                        rmgr.rmgr_tgldatang,
                                                        pod.podetail_kd,
                                                        po.po_kd
                                                    FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL' AND rmgr_type != 'RETURN') AS rmgr
                                                    INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                                                    INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                                                    INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                                                    INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                                                    INNER JOIN td_purchaseorder_detail pod on rmgr.podetail_kd = pod.podetail_kd) As Z
                    WHERE suplier_kd = sup_kd_real AND rmgroupsup_kd IS NOT NULL AND MONTH(rmgr_tgldatang) = @bulan AND YEAR(rmgr_tgldatang) = @tahun) as po_qty_real,
    
                (SELECT
                        FLOOR(SUM(rmgr.rmgr_qty)) AS gr_qty_real
                    FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL' AND rmgr_type != 'RETURN') AS rmgr
                    INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                    INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                    INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                    INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                    INNER JOIN td_purchaseorder_detail pod on rmgr.podetail_kd = pod.podetail_kd
                    WHERE sup.suplier_kd = sup_kd_real AND rm.rmgroupsup_kd IS NOT NULL AND MONTH(rmgr.rmgr_tgldatang) = @bulan AND YEAR(rmgr.rmgr_tgldatang) = @tahun) as gr_qty_real,
    
                (SELECT
                                     FLOOR(SUM(rmgr_qty))
                                        FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL') AS rmgr
                                        INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                                        INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                                        INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                                        INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                                WHERE sup.suplier_kd = sup_kd_real AND rm.rmgroupsup_kd IS NOT NULL AND MONTH(rmgr.rmgr_tgldatang) = @bulan AND YEAR(rmgr.rmgr_tgldatang) = @tahun AND rmgr.rmgr_type = 'RETURN') as return_qty_real,
    
    
                rmgr_tgldatang
                FROM(SELECT
                        rmgr.rmgr_kd,
                        po.po_no,
                        sup.suplier_kd,
                        sup.suplier_nama,
                        rmk.rmkategori_nama,
                        rmgr.rmgr_nosrj,
                        (COUNT(rmgr.rmgr_nosrj)) as gr_count,
                        (COUNT(po_no)) as po_count,
                        rmgr.rmgr_qty,
                        rmgr.rmgr_tgldatang,
                        rmgs.rmgroupsup_name
                    FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL') AS rmgr
                    INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                    INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                    INNER JOIN td_rawmaterial_group_suplier rmgs ON rm.rmgroupsup_kd = rmgs.rmgroupsup_kd
                    INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                    INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                WHERE sup.suplier_kd IN ($new_sup) AND MONTH(rmgr.rmgr_tgldatang) = @bulan AND YEAR(rmgr.rmgr_tgldatang) = @tahun AND rmgr.rmgr_type != 'RETURN'
                    GROUP BY po_no) get_leadtime
                GROUP BY rmgroupsup_name");
            }else{
                if($smt == '01'){
                    $query  =   "SET @bulan1 := '01', @tahun := '". $tahun ."', @bulan2 := '06'";
                    $this->db->query($query);

                    $newquery = $this->db->query("SELECT
                                suplier_kd as sup_kd_real,
                                suplier_nama,
                                rmgroupsup_name,
                                (COUNT(po_count)) as po_count_real,
                                (SELECT 
                                            COUNT(*)
                                            FROM (SELECT
                                                    rmgr.*,
                                                    rm.rmgroupsup_kd,
                                                    sup.suplier_kd
                                                FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL' AND rmgr_type != 'RETURN') AS rmgr
                                                INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                                                INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                                                INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                                                INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                                                INNER JOIN td_purchaseorder_detail pod on rmgr.podetail_kd = pod.podetail_kd GROUP BY rmgr_nosrj) AS rmgr
                                                WHERE suplier_kd = sup_kd_real AND rmgroupsup_kd IS NOT NULL AND YEAR(rmgr_tgldatang) = @tahun AND MONTH(rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2) AS gr_count_real,
                                (SELECT
                                            COUNT(*)
                                            FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL') AS rmgr
                                            INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                                            INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                                            INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                                            INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                                    WHERE sup.suplier_kd = sup_kd_real AND rm.rmgroupsup_kd IS NOT NULL AND MONTH(rmgr.rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2 AND YEAR(rmgr.rmgr_tgldatang) = @tahun AND rmgr.rmgr_type = 'RETURN'
                                    GROUP BY sup.suplier_kd) as return_count_real,
                    
                                    (SELECT FLOOR(SUM(po_qty_real)) FROM 
                                                        (
                                            SELECT 
                                                                        pod.podetail_qty AS po_qty_real,
                                                                        sup.suplier_kd,
                                                                        rm.rmgroupsup_kd,
                                                                        rmgr.rmgr_tgldatang,
                                                                        pod.podetail_kd,
                                                                        po.po_kd
                                                                    FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL' AND rmgr_type != 'RETURN') AS rmgr
                                                                    INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                                                                    INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                                                                    INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                                                                    INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                                                                    INNER JOIN td_purchaseorder_detail pod on rmgr.podetail_kd = pod.podetail_kd) As Z
                                    WHERE suplier_kd = sup_kd_real AND rmgroupsup_kd IS NOT NULL AND MONTH(rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2 AND YEAR(rmgr_tgldatang) = @tahun) as po_qty_real,
                    
                                (SELECT
                                        FLOOR(SUM(rmgr.rmgr_qty)) AS gr_qty_real
                                    FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL' AND rmgr_type != 'RETURN') AS rmgr
                                    INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                                    INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                                    INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                                    INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                                    INNER JOIN td_purchaseorder_detail pod on rmgr.podetail_kd = pod.podetail_kd
                                    WHERE sup.suplier_kd = sup_kd_real AND rm.rmgroupsup_kd IS NOT NULL AND MONTH(rmgr.rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2 AND YEAR(rmgr.rmgr_tgldatang) = @tahun) as gr_qty_real,
                    
                                (SELECT
                                        FLOOR(SUM(rmgr_qty))
                                                        FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL') AS rmgr
                                                        INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                                                        INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                                                        INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                                                        INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                                                WHERE sup.suplier_kd = sup_kd_real AND rm.rmgroupsup_kd IS NOT NULL AND MONTH(rmgr.rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2 AND YEAR(rmgr.rmgr_tgldatang) = @tahun AND rmgr.rmgr_type = 'RETURN') as return_qty_real,
                    
                    
                                rmgr_tgldatang
                    FROM(SELECT
                            rmgr.rmgr_kd,
                            po.po_no,
                            sup.suplier_kd,
                            sup.suplier_nama,
                            rmk.rmkategori_nama,
                            rmgr.rmgr_nosrj,
                            (COUNT(rmgr.rmgr_nosrj)) as gr_count,
                            (COUNT(po_no)) as po_count,
                            rmgr.rmgr_qty,
                            rmgr.rmgr_tgldatang,
                            rmgs.rmgroupsup_name
                        FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL' AND rmgr_type != 'RETURN') AS rmgr
                        INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                        INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                        INNER JOIN td_rawmaterial_group_suplier rmgs ON rm.rmgroupsup_kd = rmgs.rmgroupsup_kd
                        INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                        INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                    WHERE sup.suplier_kd IN ($new_sup) AND MONTH(rmgr.rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2 AND YEAR(rmgr.rmgr_tgldatang) = @tahun AND rmgr.rmgr_type != 'RETURN'
                        GROUP BY po_no) get_leadtime
                    GROUP BY rmgroupsup_name");
                }else{
                    $query  =   "SET @bulan1 := '07', @tahun := '". $tahun ."',@bulan2 := '12'";
                    $this->db->query($query);

                    $newquery = $this->db->query("SELECT
                    suplier_kd as sup_kd_real,
                    suplier_nama,
                    rmgroupsup_name,
                    (COUNT(po_count)) as po_count_real,
                    (SELECT 
                                COUNT(*)
                                FROM (SELECT
                                        rmgr.*,
                                        rm.rmgroupsup_kd,
                                        sup.suplier_kd
                                    FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL' AND rmgr_type != 'RETURN') AS rmgr
                                    INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                                    INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                                    INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                                    INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                                    INNER JOIN td_purchaseorder_detail pod on rmgr.podetail_kd = pod.podetail_kd GROUP BY rmgr_nosrj) AS rmgr
                                    WHERE suplier_kd = sup_kd_real AND rmgroupsup_kd IS NOT NULL AND YEAR(rmgr_tgldatang) = @tahun AND MONTH(rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2) AS gr_count_real,
                    (SELECT
                                COUNT(*)
                                FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL') AS rmgr
                                INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                                INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                                INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                                INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                        WHERE sup.suplier_kd = sup_kd_real AND rm.rmgroupsup_kd IS NOT NULL AND MONTH(rmgr.rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2 AND YEAR(rmgr.rmgr_tgldatang) = @tahun AND rmgr.rmgr_type = 'RETURN'
                        GROUP BY sup.suplier_kd) as return_count_real,
        
                        (SELECT FLOOR(SUM(po_qty_real)) FROM 
                                            (
                                SELECT 
                                                            pod.podetail_qty AS po_qty_real,
                                                            sup.suplier_kd,
                                                            rm.rmgroupsup_kd,
                                                            rmgr.rmgr_tgldatang,
                                                            pod.podetail_kd,
                                                            po.po_kd
                                                        FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL' AND rmgr_type != 'RETURN') AS rmgr
                                                        INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                                                        INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                                                        INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                                                        INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                                                        INNER JOIN td_purchaseorder_detail pod on rmgr.podetail_kd = pod.podetail_kd) As Z
                        WHERE suplier_kd = sup_kd_real AND rmgroupsup_kd IS NOT NULL AND MONTH(rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2 AND YEAR(rmgr_tgldatang) = @tahun) as po_qty_real,
        
                    (SELECT
                            FLOOR(SUM(rmgr.rmgr_qty)) AS gr_qty_real
                        FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL' AND rmgr_type != 'RETURN') AS rmgr
                        INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                        INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                        INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                        INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                        INNER JOIN td_purchaseorder_detail pod on rmgr.podetail_kd = pod.podetail_kd
                        WHERE sup.suplier_kd = sup_kd_real AND rm.rmgroupsup_kd IS NOT NULL AND MONTH(rmgr.rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2 AND YEAR(rmgr.rmgr_tgldatang) = @tahun) as gr_qty_real,
        
                    (SELECT
                                            SUM(rmgr_qty)
                                            FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL') AS rmgr
                                            INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                                            INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                                            INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                                            INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                                    WHERE sup.suplier_kd = sup_kd_real AND rm.rmgroupsup_kd IS NOT NULL AND MONTH(rmgr.rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2 AND YEAR(rmgr.rmgr_tgldatang) = @tahun AND rmgr.rmgr_type = 'RETURN') as return_qty_real,
        
        
                    rmgr_tgldatang
                    FROM(SELECT
                            rmgr.rmgr_kd,
                            po.po_no,
                            sup.suplier_kd,
                            sup.suplier_nama,
                            rmk.rmkategori_nama,
                            rmgr.rmgr_nosrj,
                            (COUNT(rmgr.rmgr_nosrj)) as gr_count,
                            (COUNT(po_no)) as po_count,
                            rmgr.rmgr_qty,
                            rmgr.rmgr_tgldatang,
                            rmgs.rmgroupsup_name
                        FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL') AS rmgr
                        INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                        INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                        INNER JOIN td_rawmaterial_group_suplier rmgs ON rm.rmgroupsup_kd = rmgs.rmgroupsup_kd
                        INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                        INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                    WHERE sup.suplier_kd IN ($new_sup) AND MONTH(rmgr.rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2 AND YEAR(rmgr.rmgr_tgldatang) = @tahun AND rmgr.rmgr_type != 'RETURN'
                        GROUP BY po_no) get_leadtime
                    GROUP BY rmgroupsup_name");
                }
            }
            // print_r($newquery);



                            $data = $newquery->result();
                    
                            header('Content-Type: application/json');
                            echo json_encode($data);

    }

    public function lossData()
    {
        // echo 'ok';
        $sup = $this->input->get('sup');
        $new_sup = array();
        $bulan = $this->input->get('bulan');
        $tahun = $this->input->get('tahun');
        $smt = $this->input->get('smt');

        $data_final = array();

        for($i=0; $i<count($sup); $i++){
            array_push($new_sup, '"'.$sup[$i].'"');
        }
        
        $new_sup = implode(", ", $new_sup);

            
            
        if($smt == '0'){
            $query  =   "SET @bulan1 := '". $bulan ."', @tahun := '". $tahun ."', @bulan2 := '". $bulan ."'";
            $this->db->query($query);
        }else{
            if($smt == '01'){
                $query  =   "SET @bulan1 := '01', @tahun := '". $tahun ."', @bulan2 := '06'";
                $this->db->query($query);
            }else{
                $query  =   "SET @bulan1 := '07', @tahun := '". $tahun ."',@bulan2 := '12'";
                $this->db->query($query);
            }
        }
        
        $newquery = $this->db->query("SELECT
                                        rmgroupsup_kd as rmgroupsup_kd_real,
                                        suplier_kd as sup_kd_real,
                                        suplier_nama,
                                        rmgroupsup_name,
                                        (COUNT(po_count)) as po_count_real,
                                        (SELECT 
                                            COUNT(*)
                                            FROM (SELECT
                                                    rmgr.*,
                                                    rm.rmgroupsup_kd,
                                                    sup.suplier_kd
                                                FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL' AND rmgr_type != 'RETURN') AS rmgr
                                                INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                                                INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                                                INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                                                INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                                                INNER JOIN td_purchaseorder_detail pod on rmgr.podetail_kd = pod.podetail_kd GROUP BY rmgr_nosrj) AS rmgr
                                                WHERE suplier_kd = sup_kd_real AND rmgroupsup_kd = rmgroupsup_kd_real AND rmgroupsup_kd IS NOT NULL AND YEAR(rmgr_tgldatang) = @tahun AND MONTH(rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2) AS gr_count_real,
                                        (SELECT
                                        IFNULL(COUNT(rmgr.rmgr_kd), 0)
                                            FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL') AS rmgr
                                            INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                                            INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                                            INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                                            INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                                        WHERE sup.suplier_kd = sup_kd_real AND rm.rmgroupsup_kd = rmgroupsup_kd_real AND rmgroupsup_kd IS NOT NULL AND MONTH(rmgr.rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2 AND YEAR(rmgr.rmgr_tgldatang) = @tahun AND rmgr.rmgr_type = 'RETURN'
                                        GROUP BY sup.suplier_kd) as return_count_real,

                                        (SELECT FLOOR(SUM(po_qty_real)) FROM 
                                            (
                                                        SELECT 
                                                                    pod.podetail_qty AS po_qty_real,
                                                                    sup.suplier_kd,
                                                                    rm.rmgroupsup_kd as rmgs_kd,
                                                                    pod.podetail_kd,
                                                                    po.po_kd,
                                                                    po_no,
                                                                    rmgr_tgldatang
                                                            FROM tm_purchaseorder AS po
                                                            LEFT JOIN td_purchaseorder_detail pod on po.po_kd = pod.po_kd
                                                            LEFT JOIN tm_rawmaterial rm ON pod.rm_kd = rm.rm_kd
                                                            LEFT JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                                                            LEFT JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                                                            LEFT JOIN td_rawmaterial_goodsreceive rmgr ON rmgr.po_kd = po.po_kd
                                                            WHERE rmgroupsup_kd IS NOT NULL AND MONTH(rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2 AND YEAR(rmgr_tgldatang) = @tahun GROUP BY podetail_kd ) As Z
                                            WHERE suplier_kd = sup_kd_real AND rmgs_kd = rmgroupsup_kd_real ) as po_qty_real,

                                        (SELECT
                                        FLOOR(SUM(rmgr.rmgr_qty)) AS gr_qty_real
                                        FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL' AND rmgr_type != 'RETURN') AS rmgr
                                        INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                                        INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                                        INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                                        INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                                        INNER JOIN td_purchaseorder_detail pod on rmgr.podetail_kd = pod.podetail_kd
                                        WHERE sup.suplier_kd = sup_kd_real AND rm.rmgroupsup_kd = rmgroupsup_kd_real AND rm.rmgroupsup_kd IS NOT NULL AND MONTH(rmgr.rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2 AND YEAR(rmgr.rmgr_tgldatang) = @tahun) as gr_qty_real,

                                        (SELECT
                                        ABS(FLOOR(IFNULL(SUM(rmgr_qty), 0)))
                                                        FROM (SELECT * FROM td_rawmaterial_goodsreceive WHERE rm_kd != 'SPECIAL') AS rmgr
                                                        INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                                                        INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                                                        INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                                                        INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                                                WHERE sup.suplier_kd = sup_kd_real AND rm.rmgroupsup_kd = rmgroupsup_kd_real AND rm.rmgroupsup_kd IS NOT NULL AND MONTH(rmgr.rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2 AND YEAR(rmgr.rmgr_tgldatang) = @tahun AND rmgr.rmgr_type = 'RETURN') as return_qty_real
                                        FROM(SELECT
                                        rmgr.rmgr_kd,
                                        po.po_no,
                                        sup.suplier_kd,
                                        sup.suplier_nama,
                                        rmk.rmkategori_nama,
                                        rmgr.rmgr_nosrj,
                                        (COUNT(rmgr.rmgr_nosrj)) as gr_count,
                                        (COUNT(po_no)) as po_count,
                                        rmgr.rmgr_qty,
                                        rmgr.rmgr_tgldatang,
                                        rmgs.rmgroupsup_name,
                                                                rmgs.rmgroupsup_kd
                                        FROM (SELECT rmgr_sive.* FROM td_rawmaterial_goodsreceive as rmgr_sive
                                                        LEFT JOIN tm_purchaseorder po ON po.po_kd = rmgr_sive.po_kd
                                                        LEFT JOIN tm_suplier sup ON sup.suplier_kd = po.suplier_kd
                                                        GROUP BY po_kd) rmgr
                                        INNER JOIN tm_rawmaterial rm ON rmgr.rm_kd = rm.rm_kd
                                        INNER JOIN td_rawmaterial_kategori rmk ON rmk.rmkategori_kd = rm.rmkategori_kd
                                        INNER JOIN td_rawmaterial_group_suplier rmgs ON rm.rmgroupsup_kd = rmgs.rmgroupsup_kd
                                        INNER JOIN tm_purchaseorder po ON rmgr.po_kd = po.po_kd
                                        INNER JOIN tm_suplier sup ON po.suplier_kd = sup.suplier_kd
                                        WHERE sup.suplier_kd IN ($new_sup) AND MONTH(rmgr.rmgr_tgldatang) BEtWEEN @bulan1 AND @bulan2 AND YEAR(rmgr.rmgr_tgldatang) = @tahun AND rmgr.rmgr_type != 'RETURN'
                                        GROUP BY po_no) get_leadtime
                                        GROUP BY rmgroupsup_name, suplier_nama");




        $data = $newquery->result();

        $getLt = $this->tm_suplier->getLeadtime($sup, $filter = "po", $bulan, $tahun, $po_no = "0", $smt);
            
        $count_late_on = $this->myCustomMerge($data, $getLt['count_late']);
        // unset($count_late_on['suplier_kd']);
        $count_long_short = $this->myCustomMerge($count_late_on, $getLt['count_long_shrt']);
        // // unset($count_long_short['sup_kd']);
        $count_avg = $this->myCustomMerge($count_long_short, $getLt['count_avg']);

        $lt_pr = $this->lt_perc($count_avg);
                                            
       



        header('Content-Type: application/json');
        echo json_encode($lt_pr);

    }

    public function lt_perc($count_avg)
    {
        $result = array();
        for($i = 0; $i < count($count_avg); $i++){



            //Return Count\
            if($count_avg[$i]->return_count_real == null){
                $count_avg[$i]->return_count_real = 0;
            }

            //return_percent
            if(!$count_avg[$i]->return_qty_real == 0){
                $rt_pr =  $count_avg[$i]->return_qty_real / $count_avg[$i]->po_qty_real;
                $rt_pr = $rt_pr * 100;
                $count_avg[$i]->return_percent = round($rt_pr, 2) . '%';
                if( $rt_pr > 0 &&  $rt_pr < 26){
                    $count_avg[$i]->Return = '4';
                }else if( $rt_pr > 25 &&  $rt_pr < 51){
                    $count_avg[$i]->ReturnL = '3';
                }else if( $rt_pr > 50 &&  $rt_pr < 76){
                    $count_avg[$i]->Return = '2';
                }else if( $rt_pr > 75 &&  $rt_pr < 101){
                    $count_avg[$i]->Return = '1';
                }
            }else{
                $count_avg[$i]->return_percent = '0,00%';
                $count_avg[$i]->Return = '5';
            }
            //Leadtime_percent
            $lt_pr =  $count_avg[$i]->late_lt / $count_avg[$i]->po_count_real;
            $lt_pr = $lt_pr * 100;
            $count_avg[$i]->Lt = round($lt_pr) . '%';

            if(!$lt_pr){
                $count_avg[$i]->Lt_score = '5';
            }
            else if( $lt_pr > 0 &&  $lt_pr < 26){
                $count_avg[$i]->Lt_score = '4';
            }else if( $lt_pr > 25 &&  $lt_pr < 51){
                $count_avg[$i]->Lt_score = '3';
            }else if( $lt_pr > 50 &&  $lt_pr < 76){
                $count_avg[$i]->Lt_score = '2';
            }else if( $lt_pr > 75 &&  $lt_pr < 101){
                $count_avg[$i]->Lt_score = '1';
            }

            $result[] = $count_avg[$i];
        }
        return $result;
    }

    function myCustomMerge($array1, $array2) {
        $result = array();
        foreach ($array1 as $o1) {
            foreach ($array2 as $o2) {
                if ($o1->sup_kd_real === $o2['suplier_kd'] && $o1->rmgroupsup_kd_real === $o2['rmgroupsup_kd']) {
                    $result[] = (object) array_merge((array) $o1, (array) $o2);
                }
            }
        }
        return $result;
     }

     function myCustomMerge1($array1, $array2) {
        $result = array();
        foreach ($array1 as $o1) {
            foreach ($array2 as $o2) {
                if ($o1->sup_kd_real === $o2->suplier_kd) {
                    $result[] = (object) array_merge((array) $o1, (array) $o2);
                }
            }
        }
        return $result;
     }

    private function group_by($key, $data) {
		$result = array();
		foreach($data as $val) {
			if(array_key_exists($key, $val)){
				$result[$val[$key]][] = $val;
			}else{
				$result[""][] = $val;
			}
		}
		return $result;
	}

        
          
}
