<?php

use PhpOffice\PhpSpreadsheet\Writer\Ods\Thumbnails;

defined('BASEPATH') or exit('No direct script access allowed!');

class Purchasereport_po_approved_itemgroup extends MY_Controller {
	private $class_link = 'purchasing/purchase_report/purchasereport_po_approved_itemgroup';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_purchaserequisition', 'tm_purchaseorder', 'tm_item_group', 'td_itemgroup_admin']);
    }

    public function index() {
        parent::administrator();
        $this->table_box();
    }

	 
	public function table_box(){
		/** Get item group admin */
		$kd_tipe_admin = $this->session->tipe_admin_kd;
		$itemGroupAdmin = $this->td_itemgroup_admin->get_by_param(['kd_tipe_admin' => $kd_tipe_admin])->result_array();
		$data['item_group'] = $this->tm_item_group->get_by_param_in('item_group_kd', array_column($itemGroupAdmin, 'item_group_kd'))->result_array();
		/** Generate Tahun */
		$initTahun = $this->db->query("SELECT MIN(YEAR(po_tanggal)) AS tahun_awal FROM tm_purchaseorder")->row_array();
		$tahunAkhir = date('Y');
		$selisih = $tahunAkhir - $initTahun['tahun_awal'];
		$arrTahun = [];
		for($i = 0; $i <= $selisih; $i++ ){
			$arrTahun[] = (int) $initTahun['tahun_awal']++;
		}
		$data['tahun'] = $arrTahun;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
		$kd_item_group = $this->input->get('kd_item_group');
		$tahun = $this->input->get('tahun');
        $data['resultPO'] = $this->tm_purchaseorder->po_approved_by_itemgroup($kd_item_group, $tahun);
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }

}
