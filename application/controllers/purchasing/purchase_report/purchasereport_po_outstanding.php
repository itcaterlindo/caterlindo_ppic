<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class purchasereport_po_outstanding extends MY_Controller {
	private $class_link = 'purchasing/purchase_report/purchasereport_po_outstanding';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_purchaserequisition', 'tm_purchaseorder']);
    }

    public function index() {
        parent::administrator();
        $this->table_box();
    }

	 
	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        $data = $this->tm_purchaseorder->outstanding_po();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }    	
}
