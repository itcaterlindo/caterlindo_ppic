<?php

use PhpOffice\PhpSpreadsheet\Writer\Ods\Thumbnails;

defined('BASEPATH') or exit('No direct script access allowed!');

class Purchasereport_goods_receive_push_sap extends MY_Controller {
	private $class_link = 'purchasing/purchase_report/purchasereport_goods_receive_push_sap';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_purchaseorder', 'td_rawmaterial_goodsreceive']);
    }

    public function index() {
        parent::administrator();
        $this->table_box();
    }
	 
	public function table_box(){
		/** Generate Tahun */
		$initTahun = $this->db->query("SELECT MIN(YEAR(po_tanggal)) AS tahun_awal FROM tm_purchaseorder")->row_array();
		$tahunAkhir = date('Y');
		$selisih = $tahunAkhir - $initTahun['tahun_awal'];
		$arrTahun = [];
		for($i = 0; $i <= $selisih; $i++ ){
			$arrTahun[] = (int) $initTahun['tahun_awal']++;
		}
		$data['tahun'] = $arrTahun;
		$data['status'] = [0 => 'Belum', 1 => 'Sudah'];
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
		$tahun = $this->input->get('tahun');
		$status = $this->input->get('status');
        $data['resultGR'] = $this->td_rawmaterial_goodsreceive->get_rmgr_status_push_sap($tahun, $status);
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }

}
