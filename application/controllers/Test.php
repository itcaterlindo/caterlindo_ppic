<?php

use PhpOffice\PhpSpreadsheet\Writer\Ods\Thumbnails;

defined('BASEPATH') or exit('No direct script access allowed!');

class Test extends MY_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->model(array('model_do', 'tb_default_disc'));
		$this->load->helper(array('my_helper', 'my_prices_helper', 'url'));
	}

	public function index() {
		for ($i = 1; $i < 2; $i++) :
			echo $i.'<br>';
		endfor;
	}

	function gettxt()
	{
		$this->load->helper('file');
		$string = read_file('./text10.txt');
		$lines_arr = preg_split('/\n|\r/',$string);
		$num_newlines = count($lines_arr); 
		$pos = 0;
		$arrString = [];
		foreach(preg_split("/((\r?\n)|(\r\n?))/", $string) as $line){
			$parts = preg_split('/\s+/', $line);
			$arrString[] = $parts;
		}
		$arrString = array_slice($arrString, 2);
		// echo json_encode($arrString);die();

		$dataBatch = array();
		$id = 1;
		foreach ($arrString as $r){
			$countDt = count($r);
			$dataBatch[] = array(
				'id' => $id,
				'code1' => isset($r[0]) ? $r[0] : null,
				'code2' => isset($r[1]) ? $r[1] : null,
				'desc' => isset($r[2]) ? "{$r[2]} {$r[3]}" : null,
				'qty' => isset($r[$countDt-4]) ? $r[$countDt-4] : null,
				'satuan' => isset($r[$countDt-3]) ? $r[$countDt-3] : null,
				'price' => isset($r[$countDt-2]) ? $r[$countDt-2] : null,

			);
			$id++;
		}
		// echo json_encode($dataBatch);die();

		// $arrString = array([
		// 	"id" => 1,
		// 	"code1" => "ABRA001",
		// 	"code2" => "0001",
		// 	"desc" => "Abrasive Paper-0",
		// 	"qty" => "400",
		// 	"satuan" => "PCS",
		// 	"price" => "2500"
		// ],
		//   [
		// 	"id" => 2,
		// 	"code1" => "ABRA002",
		// 	"code2" => "0003",
		// 	"desc" => "Abrasive Paper",
		// 	"qty" => "Mat",
		// 	"satuan" => "350",
		// 	"price" => "PCS"
		//   ]);

		//   echo json_encode($arrString);die();
		$this->db->insert_batch('rmstokopame_set', $dataBatch);
	}

	function generate_rmstokmaster(){
		$this->load->model(['td_rawmaterial_stok_master']);
		// $rmstokmaster_kd = $this->td_rawmaterial_stok_master->create_code(null);

		$rr = $this->db->query(
			"select tr.rm_kd, tr.rm_nama, trsm.rmstokmaster_kd  FROM  tm_rawmaterial tr 
			left join td_rawmaterial_stok_master trsm on trsm.rm_kd = tr.rm_kd 
			where trsm.rm_kd is null"
		)->result_array();

		$dataBatch = [];
		$rmstokmaster_kd = null;
		foreach ($rr as $r){
			$rmstokmaster_kd = $this->td_rawmaterial_stok_master->create_code($rmstokmaster_kd);
			$dataBatch[] = [
				'rmstokmaster_kd' => $rmstokmaster_kd,
				'rm_kd' => $r['rm_kd'],
				'rmstokmaster_qty' => 0,
				'rmstokmaster_stokopname_tgl' => '2022-01-01 00:00:00',
				'rmstokmaster_stokopname_qty' => 0,
				'rmstokmaster_tglinput' => date('Y-m-d H:i:s'),
				'rmstokmaster_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin')
			];
		}

		$act = $this->td_rawmaterial_stok_master->insert_batch($dataBatch);
		echo $act;
		// echo json_encode($dataBatch);
	}

	function generate_rmcurstok(){
		$this->load->model(['td_rawmaterial_stok_master']);
		$periode = $this->input->get('periode');
		// $periode = '2022-05-09 00:00:00';
		if (empty($periode)){
			echo 'dateperiode null';die();
		}
		// $periode = '2022-01-01 00:00:00';
		$qStokmasters = $this->db->where('rmstokmaster_stokopname_tgl', $periode)
			->get('td_rawmaterial_stok_master')->result_array();
		$rm_kds = array_column($qStokmasters, 'rm_kd');
		// $rm_kds = array('RM000008');
		$endTime = date('Y-m-d H:i:s');
		$rr = $this->td_rawmaterial_stok_master->generateMutasiStokResult($rm_kds, $endTime);
		
		$dataBatch = array(); 
		foreach ($rr as $rm_kd => $el){
			$dataBatch[] = [
				'rm_kd' => $rm_kd,
				'rmstokmaster_qty' => $el['qty_aft_mutasi'],
				'rmstokmaster_tgledit' => date('Y-m-d H:i:s')
			];
		}

		$act = $this->db->update_batch('td_rawmaterial_stok_master', $dataBatch, 'rm_kd');


		echo json_encode($act);
	}

	public function po_outstanding()
	{
		$this->load->model('tm_purchaseorder');
		$data = $this->tm_purchaseorder->outstanding_po_purchasesuggest(['RM000508']);
		echo json_encode($data);
	}

}