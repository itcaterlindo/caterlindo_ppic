<?php
defined('BASEPATH') or exit('No direct script access allowed!');

require_once APPPATH."third_party/phpspreadsheets/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Rawmaterial_kartu_stock extends MY_Controller
{
    private $class_link = 'inventory/raw_material/rawmaterial_kartu_stock';

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('ssp', 'form_validation'));
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
        $this->load->model(array('tm_rawmaterial', 'td_rawmaterial_stok_master', 'td_rawmaterial_in', 'td_rawmaterial_out', 'td_rawmaterial_stokopname_detail'));
    }

    public function index()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();
        parent::datetimepicker_assets();

        $data['opsiTransaksi'] = array(
            'ALL' => 'ALL',
            'IN' => 'IN',
            'OUT' => 'OUT'
        );
        $data['opsiSatuan'] = array(
            'PRIMARY' => 'Primary',
            'SECONDARY' => 'Secondary'
        );
        $data['class_link'] = $this->class_link;

        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function table_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $jnstransaksi = $this->input->get('jnstransaksi');
        $jnssatuan = $this->input->get('jnssatuan');
        $rm_kd = $this->input->get('rm_kd');
        $startdate = $this->input->get('startdate');
        $enddate = $this->input->get('enddate');

        $data['kartu_stock'] = $this->getDataKartuStock($jnstransaksi, $jnssatuan, $rm_kd, $startdate, $enddate);
        $data['startdate'] = $startdate;
        $data['enddate'] = $enddate;
        $data['rm_kd'] = $rm_kd;
        $data['jnstransaksi'] = $jnstransaksi;
        $data['jnssatuan'] = $jnssatuan;
        $data['class_link'] = $this->class_link;
        $data['material'] = $this->tm_rawmaterial->get_by_param_detail_satuan(['tm_rawmaterial.rm_kd' => $rm_kd])->row_array();
        $data['rmstokmaster'] = $this->td_rawmaterial_stok_master->get_by_param(['rm_kd' => $rm_kd])->row_array();
        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }

    private function generateSecondarySatuan($jnstransaksi, $arrData){
        $resultData = array();
        switch ($jnstransaksi){
            case 'IN':
                foreach ($arrData as $eachData){
                    $resultData[] = [
                        'trans_tgl'=> $eachData['trans_tgl'],
                        'trans_jenis'=> $eachData['trans_jenis'],
                        'rm_kd'=> $eachData['rm_kd'],
                        'rm_kode'=> $eachData['rm_kode'],
                        'rm_deskripsi'=> $eachData['rm_deskripsi'],
                        'rm_spesifikasi'=> $eachData['rm_spesifikasi'],
                        'rm_konversiqty'=> $eachData['rm_konversiqty'],
                        'trans_bukti'=> $eachData['trans_bukti'],
                        'trans_bukti_remark'=> $eachData['trans_bukti_remark'],
                        'trans_bukti_uraian'=> $eachData['trans_bukti_uraian'],
                        'trans_qty_in'=> $eachData['trans_qty_secondary'],
                        'trans_qty_out'=> 0,
                        'trans_satuan'=> $eachData['trans_satuan_secondary'],
                    ];
                }
                break;
            case 'OUT':
                foreach ($arrData as $eachData){
                    $resultData[] = [
                        'trans_tgl'=> $eachData['trans_tgl'],
                        'trans_jenis'=> $eachData['trans_jenis'],
                        'rm_kd'=> $eachData['rm_kd'],
                        'rm_kode'=> $eachData['rm_kode'],
                        'rm_deskripsi'=> $eachData['rm_deskripsi'],
                        'rm_spesifikasi'=> $eachData['rm_spesifikasi'],
                        'rm_konversiqty'=> $eachData['rm_konversiqty'],
                        'trans_bukti'=> $eachData['trans_bukti'],
                        'trans_bukti_remark'=> $eachData['trans_bukti_remark'],
                        'trans_bukti_uraian'=> $eachData['trans_bukti_uraian'],
                        'trans_qty_in'=> 0,
                        'trans_qty_out'=> $eachData['trans_qty_secondary'],
                        'trans_satuan'=> $eachData['trans_satuan_secondary'],
                    ];
                }
                break;
            case 'OPNAME':
                foreach ($arrData as $eachData){
                    $resultData[] = [
                        'trans_tgl'=> $eachData['trans_tgl'],
                        'trans_jenis'=> $eachData['trans_jenis'],
                        'rm_kd'=> $eachData['rm_kd'],
                        'rm_kode'=> $eachData['rm_kode'],
                        'rm_deskripsi'=> $eachData['rm_deskripsi'],
                        'rm_spesifikasi'=> $eachData['rm_spesifikasi'],
                        'rm_konversiqty'=> $eachData['rm_konversiqty'],
                        'trans_bukti'=> $eachData['trans_bukti'],
                        'trans_bukti_remark'=> $eachData['trans_bukti_remark'],
                        'trans_bukti_uraian'=> $eachData['trans_bukti_uraian'],
                        'trans_qty_in'=> $eachData['trans_qty_secondary'],
                        'trans_qty_out'=> $eachData['trans_qty_secondary'],
                        'trans_satuan'=> $eachData['trans_satuan_secondary'],
                    ];
                }
                break;
            default:
                $resultData = $arrData;
        }
        return $resultData;
    }

    public function getDataKartuStock($param_jnstransaksi, $param_jnssatuan, $param_rm_kd, $param_startdate, $param_enddate)
    {
        $jnstransaksi = $param_jnstransaksi;
        $jnssatuan = $param_jnssatuan;
        $rm_kd = $param_rm_kd;
        $startdate = $param_startdate;
        $enddate = $param_enddate;      

        $resultIn =  $this->td_rawmaterial_in->get_kartustock($rm_kd, $startdate, $enddate);
        $resultOut = $this->td_rawmaterial_out->get_kartustock($rm_kd, $startdate, $enddate);

        $resultOpname = $this->td_rawmaterial_stokopname_detail->get_priodeopname($rm_kd, $startdate, $enddate);

        $arrData = [];
        switch ($jnstransaksi) {
            case 'IN':
                /** Check jnssatuan PRIMARY or SECONDARY */
                if ($jnssatuan == 'SECONDARY'){
                    $arrData = $this->generateSecondarySatuan('IN', $resultIn);
                }else{
                    $arrData = $resultIn;
                }
                break;
            case 'OUT':
                /** Check jnssatuan PRIMARY or SECONDARY */
                if ($jnssatuan == 'SECONDARY'){
                    $arrData = $this->generateSecondarySatuan('OUT', $resultOut);

                }else{
                    $arrData = $resultOut;
                }
                break;
            case 'ALL':
                /** Check jnssatuan PRIMARY or SECONDARY */
                if ($jnssatuan == 'SECONDARY'){
                    $arrData = array_merge($this->generateSecondarySatuan('IN', $resultIn), $this->generateSecondarySatuan('OUT', $resultOut), $this->generateSecondarySatuan('OPNAME', $resultOpname));
                }else{
                    $arrData = array_merge($resultIn, $resultOut, $resultOpname);
                }
                sort($arrData);
                break;
        }

        /** Adjustment data saldo balance */
        $qtyBalance = 0; 
		$adjQty = 0;
        foreach($arrData as $key => $value){
				/** Hitung saldo balance */
				if($value['trans_jenis'] == 'IN'){
					$qtyBalance += $value['trans_qty_in']; 
				}else if($value['trans_jenis'] == 'OUT'){
					$qtyBalance -= $value['trans_qty_out'];
				}else{
					$now_balance = $value['rm_konversiqty'] * $value['trans_qty_secondary']; /** qty in atau out ketika stock opname outputnya sama */
                    $before_belence = 0;
                    if($key != 0){
                        $before_belence = $arrData[$key - 1]['trans_qty_balance'];
                    }


                    $ap_balance = $now_balance - $before_belence;

                    if($ap_balance != 0){
                        $arrData[$key]['trans_qty_in'] = $ap_balance;
                    }
                    
                    $qtyBalance = sprintf("%.2f", $now_balance);
					// if($qtyBalance - $value['trans_qty_in'] >= 0){
					// 	$adjQty = $qtyBalance - $value['trans_qty_in'];
					// 	$value['trans_qty_in'] = 0;
					// 	$value['trans_qty_out'] = $adjQty;
 					// }else{
					// 	$adjQty = $qtyBalance - $value['trans_qty_in'];
					// 	if ($adjQty < 0){
					// 		$adjQty = $adjQty * -1;
					// 	}
					// 	$value['trans_qty_in'] = $adjQty;
					// 	$value['trans_qty_out'] = 0;
					// }
				}
            $arrData[$key]['trans_qty_balance'] = $qtyBalance;
        }
        return $arrData;
    }

    public function cetak_kartu_stok() {
        
		parent::admin_print();
        $jnstransaksi = $this->input->get('jnstransaksi');
        $jnssatuan = $this->input->get('jnssatuan');
        $rm_kd = $this->input->get('rm_kd');
        $startdate = $this->input->get('startdate');
        $enddate = $this->input->get('enddate');

		$url = base_url().$this->class_link.'/table_main?startdate='.$startdate.'&enddate='.$enddate.'&jnstransaksi='.$jnstransaksi.'&jnssatuan='.$jnssatuan.'&rm_kd='.$rm_kd;
		$data['class_link'] = $this->class_link;
		$data['url'] = $url;
		$this->load->view('page/'.$this->class_link.'/print_detail', $data);
		
	}

    public function xls_kartu_stok(){

        $jnstransaksi = $this->input->get('jnstransaksi');
        $jnssatuan = $this->input->get('jnssatuan');
        $rm_kd = $this->input->get('rm_kd');
        $startdate = $this->input->get('startdate');
        $enddate = $this->input->get('enddate');
        $kartu_stock = $this->getDataKartuStock($jnstransaksi, $jnssatuan, $rm_kd, $startdate, $enddate);

		$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()
			->setCreator($_SESSION['nm_admin'])
			->setLastModifiedBy($_SESSION['nm_admin'])
			->setTitle('Report RM Stock '.date('d-M-Y'))
			->setCategory('Report');
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		$sheet = $spreadsheet->getActiveSheet();
		$header_cell = 1;
		$sheet->setCellValue('A'.$header_cell, 'No')->getStyle('A'.$header_cell);
		$sheet->setCellValue('B'.$header_cell, 'Tgl Transaksi')->getStyle('B'.$header_cell);
		$sheet->setCellValue('C'.$header_cell, 'Uraian')->getStyle('C'.$header_cell);
		$sheet->setCellValue('D'.$header_cell, 'Satuan')->getStyle('D'.$header_cell);
		$sheet->setCellValue('E'.$header_cell, 'Qty IN')->getStyle('E'.$header_cell);
		$sheet->setCellValue('F'.$header_cell, 'Qty OUT')->getStyle('F'.$header_cell);
		$sheet->setCellValue('G'.$header_cell, 'Balance')->getStyle('G'.$header_cell);

		$dataCell = 2;
		$no = 1;

		foreach ($kartu_stock as $each):
			$sheet->setCellValue('A'.$dataCell, $no)->getStyle('A'.$dataCell);
			$sheet->setCellValue('B'.$dataCell, format_date($each['tgl_trans'], 'd-m-Y'))->getStyle('B'.$dataCell);
			$sheet->setCellValue('C'.$dataCell, $each['trans_bukti_uraian'])->getStyle('C'.$dataCell);
			$sheet->setCellValue('D'.$dataCell, $each['trans_satuan'])->getStyle('D'.$dataCell);
			$sheet->setCellValue('E'.$dataCell, number_format($each['trans_qty_in'],2,'.','') )->getStyle('E'.$dataCell);
			$sheet->setCellValue('F'.$dataCell, number_format($each['trans_qty_out'],2,'.','') )->getStyle('F'.$dataCell);
			$sheet->setCellValue('G'.$dataCell, number_format($each['trans_qty_balance'],2,'.','') )->getStyle('G'.$dataCell);

			$dataCell++;
			$no++;
		endforeach;


		$filename = 'kartu_stock_rawmaterial_'.$jnstransaksi.'_'.$rm_kd.'_'.$startdate.'_sd_'.$enddate.'.xlsx';
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='. $filename .'');
		header('Cache-Control: max-age=0');
		$writer->save('php://output');
    }

}
