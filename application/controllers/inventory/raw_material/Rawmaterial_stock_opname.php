<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Rawmaterial_stock_opname extends MY_Controller
{
    private $class_link = 'inventory/raw_material/rawmaterial_stock_opname';

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('ssp', 'form_validation'));
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
        $this->load->model(array('td_rawmaterial_stokopname_jenis', 'tm_rawmaterial', 'td_rawmaterial_stokopname_jenis_detail',
            'tm_rawmaterial_stokopname', 'td_rawmaterial_stokopname_detail', 'td_rawmaterial_stok_master'));
    }

    public function index()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();
        parent::datetimepicker_assets();

        $stokopnamActive = $this->tm_rawmaterial_stokopname->get_by_param(['rmstokopname_status' => 'active'])->row_array();

        $data['class_link'] = $this->class_link;
        $data['row'] = $this->tm_rawmaterial_stokopname->get_by_param_detail(['rmstokopname_kd' => $stokopnamActive['rmstokopname_kd']])->row_array();
        $data['row']['rmstokopname_status_badge'] = $this->tm_rawmaterial_stokopname->generateSpanStatus($stokopnamActive['rmstokopname_status']);
        $data['rmstokopname_kd'] = $stokopnamActive['rmstokopname_kd'];

        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function table_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $rmstokopname_kd = $this->input->get('rmstokopname_kd');

        $data['class_link'] = $this->class_link;
        $opnameMaster = $this->tm_rawmaterial_stokopname->get_by_param(['rmstokopname_kd' => $rmstokopname_kd])->row_array();
        $opnameDetails = $this->td_rawmaterial_stokopname_detail->stokOpnameDetailRM($rmstokopname_kd)->result_array();
        $rm_kds = array_column($opnameDetails, 'rm_kd');
        $mutasiStok = $this->td_rawmaterial_stok_master->generateMutasiStokResult($rm_kds, $opnameMaster['rmstokopname_periode']);
        $arrDetails = array();
        foreach ($opnameDetails as $opnameDetail) {
            $arrDetails[] = [
                'rmstokopnamedetail_kd' => $opnameDetail['rmstokopnamedetail_kd'],
                'rm_kd' => $opnameDetail['rm_kd'],
                'rm_oldkd' => $opnameDetail['rm_oldkd'],
                'rm_kode' => $opnameDetail['rm_kode'],
                'rm_deskripsi' => $opnameDetail['rm_deskripsi'],
                'rm_spesifikasi' => $opnameDetail['rm_spesifikasi'],
                'rmsatuan_nama' => $opnameDetail['rmsatuan_nama'],
                'satuan_secondary_nama' => $opnameDetail['satuan_secondary_nama'],
                'rmstokopnamedetail_qty' => $opnameDetail['rmstokopnamedetail_qty'],
                'satuan_opname_nama' => $opnameDetail['satuan_opname_nama'],
                'rmstokopnamedetail_konversi' => $opnameDetail['rmstokopnamedetail_konversi'],
                'rmstokopnamedetail_qtykonversi' => $opnameDetail['rmstokopnamedetail_qtykonversi'],
                'rmstokopnamedetail_remark' => $opnameDetail['rmstokopnamedetail_remark'],
                'rmstokopnamedetail_tgledit' => $opnameDetail['rmstokopnamedetail_tgledit'],
                'qty_aft_mutasi' => isset($mutasiStok[$opnameDetail['rm_kd']]['qty_aft_mutasi']) ? $mutasiStok[$opnameDetail['rm_kd']]['qty_aft_mutasi'] : 0
            ];
        }
        $data['detailRMopnames'] = $arrDetails;

        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }

    public function table_formmain()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $rmstokopnamedetail_kd = $this->input->get('rmstokopnamedetail_kd');
        $next_rmstokopnamedetail_kd = $this->input->get('next_rmstokopnamedetail_kd');
        $back_rmstokopnamedetail_kd = $this->input->get('back_rmstokopnamedetail_kd');
        

        $data['class_link'] = $this->class_link;
        $data['row'] = $this->td_rawmaterial_stokopname_detail->get_by_param_detail(['rmstokopnamedetail_kd' => $rmstokopnamedetail_kd])->row_array();
        $data['next_rmstokopnamedetail_kd'] = $next_rmstokopnamedetail_kd;
        $data['back_rmstokopnamedetail_kd'] = $back_rmstokopnamedetail_kd;

        $this->load->view('page/' . $this->class_link . '/table_formmain', $data);
    }

    public function jenis_stockopname()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();

        $data['class_link'] = $this->class_link;

        $this->load->view('page/' . $this->class_link . '/jenisstockopname_index', $data);
    }

    public function jenisstockopname_tablemain()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['class_link'] = $this->class_link;

        $this->load->view('page/' . $this->class_link . '/jenisstockopname_tablemain', $data);
    }

    public function jenisstockopname_formbatch()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $rmstokopnamejenis_kd = $this->input->get('rmstokopnamejenis_kd');

        if (!empty($rmstokopnamejenis_kd)) {
            $rmstokopnamejenis = $this->td_rawmaterial_stokopname_jenis->get_by_param(['rmstokopnamejenis_kd' => $rmstokopnamejenis_kd])->row_array();
            $data['rmstokopnamejenis_kd'] = $rmstokopnamejenis['rmstokopnamejenis_kd'];
            $data['rmstokopnamejenis_nama'] = $rmstokopnamejenis['rmstokopnamejenis_nama'];
        }

        $data['slug'] = $this->input->get('slug');
        $data['class_link'] = $this->class_link;

        $this->load->view('page/' . $this->class_link . '/jenisstockopname_formbatch', $data);
    }

    public function jenisstockopname_formbatch_datamaterial()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $rmstokopnamejenis_kd = $this->input->get('rmstokopnamejenis_kd');
        $result = $this->tm_rawmaterial->get_all()->result_array();

        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $stokJenisDetails = $this->td_rawmaterial_stokopname_jenis_detail->get_by_param(['rmstokopnamejenis_kd' => $rmstokopnamejenis_kd])->result_array();
        $stokJenisDetailRms = array_column($stokJenisDetails, 'rm_kd');

        $data = [];
        foreach ($result as $r) {
            $htmlCheck = 'unchecked';
            if (in_array($r['rm_kd'], $stokJenisDetailRms)) {
                $htmlCheck = 'checked';
            }
            $data[] = [
                '0' => $r['rm_kd'],
                '1' => $r['rm_kode'] . ' / ' . $r['rm_oldkd'],
                '2' => $r['rm_deskripsi'] . ' ' . $r['rm_spesifikasi'],
                '3' => $htmlCheck
            ];
        }

        $output = array(
            'draw' => $draw,
            'recordsTotal' => count($data),
            'recordsFiltered' => count($data),
            'data' => $data
        );

        echo json_encode($output);
    }

    public function jenisstockopname_tabledata()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->load->library(['ssp']);

        $data = $this->td_rawmaterial_stokopname_jenis->ssp_table();
        echo json_encode(
            SSP::simple($_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'])
        );
    }

    public function view_main()
    {
        parent::administrator();
        parent::pnotify_assets();

        $rmstokopname_kd = $this->input->get('rmstokopname_kd');

        $data['class_link'] = $this->class_link;
        $data['rmstokopname_kd'] = $rmstokopname_kd;
        $data['row'] = $this->tm_rawmaterial_stokopname->get_by_param_detail(['rmstokopname_kd' => $rmstokopname_kd])->row_array();
        $data['row']['rmstokopname_status_badge'] = $this->tm_rawmaterial_stokopname->generateSpanStatus($data['row']['rmstokopname_status']);

        $this->load->view('page/' . $this->class_link . '/view_box', $data);
    }


    public function view_table()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $rmstokopname_kd = $this->input->get('rmstokopname_kd');

        $data['class_link'] = $this->class_link;
        $data['detailRMopnames'] = $this->td_rawmaterial_stokopname_detail->getOpnameDetailByStokopnameID($rmstokopname_kd);

        $this->load->view('page/' . $this->class_link . '/view_table', $data);
    }

    public function view_pdf()
    {
        $this->load->library('Pdf');
        $rmstokopname_kd = $this->input->get('rmstokopname_kd', true);

        $dataDetail['detailRMopnames'] = $this->td_rawmaterial_stokopname_detail->getOpnameDetailByStokopnameID($rmstokopname_kd);
        $data['konten'] = $this->load->view('page/' . $this->class_link . '/view_table', $dataDetail, true);
        $data['dataHeader'] = $this->tm_rawmaterial_stokopname->get_by_param_detail(['tm_rawmaterial_stokopname.rmstokopname_kd' => $rmstokopname_kd])->row_array();

        $this->load->view('page/' . $this->class_link . '/view_pdf', $data);
    }

    public function get_active_rmdetail()
    {
        $results = $this->db->select('td_rawmaterial_stokopname_detail.rmstokopnamedetail_kd, td_rawmaterial_stokopname_detail.rm_kd')
            ->join('tm_rawmaterial_stokopname', 'tm_rawmaterial_stokopname.rmstokopname_kd=td_rawmaterial_stokopname_detail.rmstokopname_kd','left')
            ->join('tm_rawmaterial', 'tm_rawmaterial.rm_kd=td_rawmaterial_stokopname_detail.rm_kd','left')
            ->where('tm_rawmaterial_stokopname.rmstokopname_status', 'active')
            ->order_by('tm_rawmaterial.rm_kode', 'asc')
            ->get('td_rawmaterial_stokopname_detail')
            ->result_array();

        $arrResult = array();
        foreach ($results as $r){
            $arrResult[] = [
                $r['rmstokopnamedetail_kd'] => $r['rm_kd']
            ];
        }

        header('Content-Type: application/json; charset=utf-8');        
        echo json_encode($results);
    }

    public function action_jenisstockopnamedetail_batch()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $rmstokopnamejenis_kd = $this->input->post('txtrmstokopnamejenis_kd');
        $rm_kds = $this->input->post('txtrm_kds');

        if (!empty($rmstokopnamejenis_kd)) {
            /** Edit */
            $dataMaster = [
                'rmstokopnamejenis_nama' => $this->input->post('txtrmstokopnamejenis_nama'),
                'rmstokopnamejenis_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin')
            ];
            $actMaster = $this->td_rawmaterial_stokopname_jenis->update_data(['rmstokopnamejenis_kd' => $rmstokopnamejenis_kd],$dataMaster);
            if (!empty($rm_kds)) {
                $arrRm = array();
                foreach ($rm_kds as $rm_kd) {
                    $arrRm[] = [
                        'rmstokopnamejenis_kd' => $rmstokopnamejenis_kd,
                        'rm_kd' => $rm_kd,
                        'rmstokopnamejenisdetail_tglinput' => date('Y-m-d H:i:s'),
                        'rmstokopnamejenisdetail_tgledit' => date('Y-m-d H:i:s'),
                    ];
                }
                try {
                    $actDel = $this->td_rawmaterial_stokopname_jenis_detail->delete_by_param(['rmstokopnamejenis_kd' => $rmstokopnamejenis_kd]);
                    if ($actDel) {
                        $this->td_rawmaterial_stokopname_jenis_detail->insert_batch($arrRm);
                    }
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
                } catch (Exception $e) {
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan');
                }
            } else {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'RM Kosong');
            }
        } else {
            /** Add */
            $rmstokopnamejenis_kd = $this->td_rawmaterial_stokopname_jenis->create_code();
            $dataMaster = [
                'rmstokopnamejenis_kd' => $rmstokopnamejenis_kd,
                'rmstokopnamejenis_nama' => $this->input->post('txtrmstokopnamejenis_nama'),
                'rmstokopnamejenis_tglinput' => date('Y-m-d H:i:s'),
                'rmstokopnamejenis_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin')
            ];
            $actMaster = $this->td_rawmaterial_stokopname_jenis->insert_data($dataMaster);
            if ($actMaster) {
                $arrRm = array();
                foreach ($rm_kds as $rm_kd) {
                    $arrRm[] = [
                        'rmstokopnamejenis_kd' => $rmstokopnamejenis_kd,
                        'rm_kd' => $rm_kd,
                        'rmstokopnamejenisdetail_tglinput' => date('Y-m-d H:i:s'),
                        'rmstokopnamejenisdetail_tgledit' => date('Y-m-d H:i:s'),
                    ];
                }
                try {
                    $this->td_rawmaterial_stokopname_jenis_detail->insert_batch($arrRm);
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
                } catch (Exception $e) {
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan');
                }
            }
        }

        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_jenisstockopnamedetail_delete ()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $id = $this->input->get('id');
        if (!empty($id)) {
            try {
                $this->td_rawmaterial_stokopname_jenis->delete_data($id);
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
            } catch (Exception $e) {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal');
            }
        } else {
            $resp = array('code' => 400, 'pesan' => 'Id tidak ditemukan');
        }
        echo json_encode($resp);
    }

    public function periode_stockopname()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();
        parent::datetimepicker_assets();

        $data['class_link'] = $this->class_link;

        $this->load->view('page/' . $this->class_link . '/periodestockopname_index', $data);
    }

    public function periodestockopname_tablemain ()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['class_link'] = $this->class_link;

        $this->load->view('page/' . $this->class_link . '/periodestockopname_tablemain', $data);
    }

    public function periodestockopname_tabledata()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->load->library(['ssp']);

        $data = $this->tm_rawmaterial_stokopname->ssp_table();
        echo json_encode(
            SSP::simple($_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'])
        );
    }

    public function periodestockopname_formmain()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $rmstokopname_kd = $this->input->get('rmstokopname_kd');

        $opsiRmstokopnamejenis = array();
        $stokOpnameJenis = $this->td_rawmaterial_stokopname_jenis->get_all()->result_array();
        foreach ($stokOpnameJenis as $eJenis) {
            $opsiRmstokopnamejenis[$eJenis['rmstokopnamejenis_kd']] = $eJenis['rmstokopnamejenis_nama'];
        }

        if (!empty($rmstokopname_kd)) {
            $rmstokopname = $this->tm_rawmaterial_stokopname->get_by_param(['rmstokopname_kd' => $rmstokopname_kd])->row_array();
            $data['row'] = $rmstokopname;
        }
        
        $data['rmstokopname_kd'] = $rmstokopname_kd;
        $data['class_link'] = $this->class_link;
        $data['opsiRmstokopnamejenis'] = $opsiRmstokopnamejenis;
        $this->load->view('page/' . $this->class_link . '/periodestockopname_formmain', $data);
    }

    public function periodestockopname_formrmtabledetail()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $rmstokopname_kd = $this->input->get('rmstokopname_kd');

        $data['class_link'] = $this->class_link;
        $data['rmstokopname_kd'] = $rmstokopname_kd;
        
        $this->load->view('page/' . $this->class_link . '/periodestockopname_formrmtabledetail', $data);
    }

    public function periodestockopname_formrmtabledetail_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $rmstokopname_kd = $this->input->get('rmstokopname_kd');
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $result = $this->tm_rawmaterial->get_all()->result_array();
        $stokOpnameDetails = $this->td_rawmaterial_stokopname_detail->get_by_param(['rmstokopname_kd' => $rmstokopname_kd])->result_array();
        $stokOpnameDetailRms = array_column($stokOpnameDetails, 'rm_kd');

        $data = [];
        foreach ($result as $r) {
            $htmlCheck = 'unchecked';
            if (in_array($r['rm_kd'], $stokOpnameDetailRms)) {
                $htmlCheck = 'checked';
            }
            $data[] = [
                '0' => $r['rm_kd'],
                '1' => $r['rm_kode'] . ' / ' . $r['rm_oldkd'],
                '2' => $r['rm_deskripsi'] . ' ' . $r['rm_spesifikasi'],
                '3' => $htmlCheck
            ];
        }

        $output = array(
            'draw' => $draw,
            'recordsTotal' => count($data),
            'recordsFiltered' => count($data),
            'data' => $data
        );

        echo json_encode($output);
    }

    public function action_periodestockopname()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrmstokopname_periode', 'Periode', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtrmstokopnamejenis_kd', 'Jenis', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrrmstokopname_periode' => (!empty(form_error('txtrmstokopname_periode')))?buildLabel('warning', form_error('txtrmstokopname_periode', '"', '"')):'',
				'idErrrmstokopnamejenis_kd' => (!empty(form_error('txtrmstokopnamejenis_kd')))?buildLabel('warning', form_error('txtrmstokopnamejenis_kd', '"', '"')):'',
			);
			
		}else {
            $rmstokopname_kd = $this->input->post('txtrmstokopname_kd');
            $rmstokopname_periode = $this->input->post('txtrmstokopname_periode');
            $rmstokopnamejenis_kd = $this->input->post('txtrmstokopnamejenis_kd');
            $rmstokopname_note = $this->input->post('txtrmstokopname_note');
    
            $data = array(
                'rmstokopnamejenis_kd' => $rmstokopnamejenis_kd,
                'rmstokopname_periode' => $rmstokopname_periode,
                'rmstokopname_note' => $rmstokopname_note,
                'rmstokopname_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin')
            );
            
            if (empty($rmstokopname_kd)) {
                /** Add */
                $rmstokopname_kd = $this->tm_rawmaterial_stokopname->create_code();
                $rmstokopname_kode = $this->tm_rawmaterial_stokopname->create_no($rmstokopname_periode);
                $data = array_merge($data, [
                    'rmstokopname_kd' => $rmstokopname_kd,
                    'rmstokopname_kode' => $rmstokopname_kode,
                    'rmstokopname_status' => 'editing',
                    'rmstokopname_tglinput' => date('Y-m-d H:i:s')
                ]);
                try {
                    $this->tm_rawmaterial_stokopname->insert_data($data);

                    /** Insert detail material */
                    $stokopnameJenisDetails = $this->td_rawmaterial_stokopname_jenis_detail->get_by_param(['rmstokopnamejenis_kd' => $rmstokopnamejenis_kd])->result_array();
                    if (!empty($stokopnameJenisDetails)) {
                        $arrStokopnameDetails = array();
                        $rmstokopnamedetail_kd= $this->td_rawmaterial_stokopname_detail->create_code();
                        foreach ($stokopnameJenisDetails as $stokopnameJenisDetail) {
                            $arrStokopnameDetails[] = [
                                'rmstokopnamedetail_kd' => $rmstokopnamedetail_kd,
                                'rmstokopname_kd' => $rmstokopname_kd,
                                'rm_kd' => $stokopnameJenisDetail['rm_kd'],
                                'rmstokopnamedetail_tglinput' => date('Y-m-d H:i:s'),
                                'rmstokopnamedetail_tgledit' => date('Y-m-d H:i:s'),
                                'admin_kd' => $this->session->userdata('kd_admin')
                            ];
                            $rmstokopnamedetail_kd++;
                        }
                        if (!empty($stokopnameJenisDetails)) {
                            $this->td_rawmaterial_stokopname_detail->insert_batch($arrStokopnameDetails);
                        }
                    }

                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['rmstokopname_kd' => $rmstokopname_kd]);
                } catch (Exception $e) {
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan');
                }
            } else {
                /** Edit */
                try {
                    $this->tm_rawmaterial_stokopname->update_data (['rmstokopname_kd' => $rmstokopname_kd], $data);
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['rmstokopname_kd' => $rmstokopname_kd]);
                } catch (Exception $e) {
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan');
                }
            }
        }

        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_periodestockopname_detailbatch()
    {
        $this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrmstokopname_kd', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);		

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrtxtrmstokopname_kd' => (!empty(form_error('txtrmstokopname_kd')))?buildLabel('warning', form_error('txtrmstokopname_kd', '"', '"')):'',
			);
			
		}else {
            $rmstokopname_kd = $this->input->post('txtrmstokopname_kd');
            $rm_kds = $this->input->post('txtrm_kds');

            if (!empty($rm_kds)) {
                $stokopnameDetailExists = $this->td_rawmaterial_stokopname_detail->get_by_param(['rmstokopname_kd' => $rmstokopname_kd])->result_array();
                $arrStokopnameDetailExists = array();
                foreach ($stokopnameDetailExists as $stokopnameDetailExist) {
                    $arrStokopnameDetailExists[$stokopnameDetailExist['rm_kd']] = $stokopnameDetailExist;
                }
                $rmstokopnamedetail_kd = $this->td_rawmaterial_stokopname_detail->create_code();
                $arrRm = array();
                foreach ($rm_kds as $rm_kd) {
                    $arrRm[] = [
                        'rmstokopnamedetail_kd' => $rmstokopnamedetail_kd,
                        'rmstokopname_kd' => $rmstokopname_kd,
                        'rm_kd' => $rm_kd,
                        'rmstokopnamedetail_qty' => isset($arrStokopnameDetailExists[$rm_kd]['rmstokopnamedetail_qty']) ? $arrStokopnameDetailExists[$rm_kd]['rmstokopnamedetail_qty'] : null,
                        'rmstokopnamedetail_remark' => isset($arrStokopnameDetailExists[$rm_kd]['rmstokopnamedetail_remark']) ? $arrStokopnameDetailExists[$rm_kd]['rmstokopnamedetail_remark'] : null,
                        'rmstokopnamedetail_tglinput' => date('Y-m-d H:i:s'),
                        'rmstokopnamedetail_tgledit' => date('Y-m-d H:i:s'),
                        'admin_kd' => $this->session->userdata('kd_admin')
                    ];
                    $rmstokopnamedetail_kd++;
                }
                try {
                    $actDel = $this->td_rawmaterial_stokopname_detail->delete_by_param(['rmstokopname_kd' => $rmstokopname_kd]);
                    if ($actDel) {
                        $this->td_rawmaterial_stokopname_detail->insert_batch($arrRm);
                    }
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
                } catch (Exception $e) {
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan');
                }
            } else {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'RM Kosong');
            }
        }

        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_stokopnamdedetail_update ()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrmstokopnamedetail_kd', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);		

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrrm_kode' => (!empty(form_error('txtrmstokopnamedetail_kd')))?buildLabel('warning', form_error('txtrmstokopname_kd', '"', '"')):'',
			);
			
		}else {
            $rmstokopnamedetail_kd = $this->input->post('txtrmstokopnamedetail_kd');
            $rmstokopnamedetail_qty = $this->input->post('txtrmstokopnamedetail_qty');
            $rmstokopnamedetail_satuan = $this->input->post('txtrmstokopnamedetail_satuan');
            $rmstokopnamedetail_konversi = $this->input->post('txtrmstokopnamedetail_konversi');
            $rmstokopnamedetail_qtykonversi = $this->input->post('txtrmstokopnamedetail_qtykonversi');
            $rmstokopnamedetail_remark = $this->input->post('txtrmstokopnamedetail_remark');
            
            $data = [
                'rmstokopnamedetail_qty' => $rmstokopnamedetail_qty,
                'rmstokopnamedetail_satuan' => $rmstokopnamedetail_satuan,
                'rmstokopnamedetail_konversi' => $rmstokopnamedetail_konversi,
                'rmstokopnamedetail_qtykonversi' => $rmstokopnamedetail_qtykonversi,
                'rmstokopnamedetail_remark' => $rmstokopnamedetail_remark,
                'rmstokopnamedetail_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin')
            ];

            $this->td_rawmaterial_stokopname_detail->update_data(['rmstokopnamedetail_kd' => $rmstokopnamedetail_kd], $data);
            $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_periodestockopnamedetail_delete ()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $id = $this->input->get('id');
        if (!empty($id)) {
            try {
                $actDetail = $this->td_rawmaterial_stokopname_detail->delete_by_param(['rmstokopname_kd' => $id]);
                if ($actDetail){
                    $this->tm_rawmaterial_stokopname->delete_data($id);
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
                }else{
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal');
                }
            } catch (Exception $e) {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal');
            }
        } else {
            $resp = array('code' => 400, 'pesan' => 'Id tidak ditemukan');
        }
        echo json_encode($resp);
    }

    public function action_periodestockopname_setstatus()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $id = $this->input->get('id');
        $rmstokopname_status = $this->input->get('rmstokopname_status');

        if (!empty($id) && !empty($rmstokopname_status)) {
            /** Cek active; allow only single periode on active */
            if ($rmstokopname_status == 'active') {
                $arrActive = $this->tm_rawmaterial_stokopname->get_by_param(['rmstokopname_status' => $rmstokopname_status])->result_array();
                if (!empty($arrActive)){
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Sudah ada yg active');
                    echo json_encode($resp);
                    die();    
                }else{
                    /** Action set default to current stock */
                    $this->td_rawmaterial_stokopname_detail->generateDetailStokOpname($id); 
                }
            }elseif($rmstokopname_status == 'finish'){
                /** Action copy opname result to stok master */
                $this->td_rawmaterial_stokopname_detail->generateFinishOpnameResult($id); 
            }
            $dataUpdate = [
                'rmstokopname_status' => $rmstokopname_status,
                'rmstokopname_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin')
            ];
            $this->tm_rawmaterial_stokopname->update_data(['rmstokopname_kd' => $id], $dataUpdate);
            
            $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
        }else{
            $resp = array('code' => 400, 'pesan' => 'Id tidak ditemukan');
        }
        echo json_encode($resp);
    }

}
