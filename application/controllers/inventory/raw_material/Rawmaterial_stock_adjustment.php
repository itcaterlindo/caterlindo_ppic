<?php

use PhpOffice\PhpSpreadsheet\Writer\Ods\Thumbnails;

defined('BASEPATH') or exit('No direct script access allowed!');

class Rawmaterial_stock_adjustment extends MY_Controller
{
    private $class_link = 'inventory/raw_material/rawmaterial_stock_adjustment';

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('ssp', 'form_validation'));
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
        $this->load->model(array('td_rawmaterial_stok_adjustment', 'td_rawmaterial_in', 'td_rawmaterial_out', 'td_rawmaterial_stok_master', 'tb_finishgood_user', 'tm_gudang'));
    }

    public function index()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();
        parent::datetimepicker_assets();

        $data['class_link'] = $this->class_link;

        $this->load->view('page/' . $this->class_link . '/form_box', $data);
    }

    public function form_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_main', $data);
    }

    public function form_main_input()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['jenis_transaksi'] = $this->input->get('jenis_transaksi');
        $data['form_index'] = $this->input->get('form_index');
        $data['gudang'] = $this->tm_gudang->get_all()->result_array();
        /** Set default gudang */
        $data['gudang_user'] = $this->tb_finishgood_user->get_by_param(['kd_tipe_admin' => $this->session->userdata('tipe_admin_kd')])->row_array();
        $this->load->view('page/' . $this->class_link . '/form_main_input', $data);
    }

    public function table_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $date = $this->input->get('date');
    
        $data['date'] = $date;
        $data['class_link'] = $this->class_link;

        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }

    public function table_data (){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$date = $this->input->get('date', true);

		$date = format_date($date, 'Y-m-d');
		$data = $this->td_rawmaterial_stok_adjustment->ssp_table($date);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

    public function action_insert()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        
        $rmstokadj_waktu = $this->input->post('txtrmstokadj_waktu', true)." ".date('H:i:s');;
        $rmstokadj_type = $this->input->post('txtrmstokadj_type', true);

        /** ARRAY PARAMETER */
        $rm_kd = $this->input->post('txtrm_kd', true);
        $rmstokadj_qty = $this->input->post('txtrmstokadj_qty', true);
        $gudang_kd = $this->input->post('txtgudang_kd', true);
        $rmstokadj_remark = $this->input->post('txtrmstokadj_remark', true);
        $jns_transaksi = $this->input->post('txtJenistransaksi', true);

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtrmstokadj_type', 'Type', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtrm_kd[]', 'RM', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtgudang_kd[]', 'Gudang', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtrmstokadj_qty[]', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrrmstokadj_type' => (!empty(form_error('txtrmstokadj_type'))) ? buildLabel('warning', form_error('txtrmstokadj_type', '"', '"')) : '',
                'idErrrm_kd' => (!empty(form_error('txtrm_kd[]'))) ? buildLabel('warning', form_error('txtrm_kd[]', '"', '"')) : '',
                'idErrGudang' => (!empty(form_error('txtgudang_kd[]'))) ? buildLabel('warning', form_error('txtgudang_kd[]', '"', '"')) : '',
                'idErrrmstokadj_qty' => (!empty(form_error('txtrmstokadj_qty[]'))) ? buildLabel('warning', form_error('txtrmstokadj_qty[]', '"', '"')) : '',
            );
        } else {
            $this->db->trans_begin();
            if(!empty($rm_kd)){
                for($i = 0; $i < count($rm_kd); $i++){
                    $rmstokadj_kd = $this->td_rawmaterial_stok_adjustment->create_code();
                    $stokmaster =$this->td_rawmaterial_stok_master->get_by_param(['rm_kd' => $rm_kd[$i]])->row_array();
                    /** ADD MASTER ADJUSTMENT DATA */
                    $data = [
                        'rmstokadj_kd' => $rmstokadj_kd,
                        'rm_kd' => $rm_kd[$i],
                        'rmstokadj_type' => $jns_transaksi[$i],
                        'rmstokadj_qty' => !empty($rmstokadj_qty[$i]) ? $rmstokadj_qty[$i] : null,
                        'rmstokadj_waktu' => $rmstokadj_waktu,
                        'rmstokadj_remark' => $rmstokadj_remark[$i],
                        'gudang_kd' => $gudang_kd[$i],
                        'rmstokadj_tglinput' => date('Y-m-d H:i:s'),
                        'rmstokadj_tgledit' => date('Y-m-d H:i:s'),
                        'admin_kd' => $this->session->userdata('kd_admin'),
                    ];
                    $act = $this->td_rawmaterial_stok_adjustment->insert_data($data);

                    /** ADD RAW MATERIAL IN ATAU OUT */
                    if($jns_transaksi[$i] == "IN"){
                        /** ADJ TYPE IN */
                        $dataIn = [
                            'rmin_kd' => $this->td_rawmaterial_in->create_code(),
                            'rmstokmaster_kd' => $stokmaster['rmstokmaster_kd'],
                            'rm_kd' => $rm_kd[$i],
                            'rmstokadj_kd' => $rmstokadj_kd,
                            'rmin_qty' => $rmstokadj_qty[$i],
                            'rmin_tglinput' => $rmstokadj_waktu,
                            'admin_kd' => $this->session->userdata('kd_admin'),
                        ];
                        $actDetail = $this->td_rawmaterial_in->insert_data($dataIn);
                        $actUpdateStok = $this->td_rawmaterial_stok_master->update_stok($stokmaster['rmstokmaster_kd'], 'IN', $rmstokadj_qty[$i]);
                    }else if($jns_transaksi[$i] == "OUT"){
                        /** ADJ TYPE OUT */
                        $dataOut = [
                            'rmout_kd' => $this->td_rawmaterial_out->create_code(),
                            'rmstokmaster_kd' => $stokmaster['rmstokmaster_kd'],
                            'rm_kd' => $rm_kd[$i],
                            'rmstokadj_kd' => $rmstokadj_kd,
                            'rmout_qty' => $rmstokadj_qty[$i],
                            'rmout_tglinput' => $rmstokadj_waktu,
                            'admin_kd' => $this->session->userdata('kd_admin'),
                        ];
                        $actDetail = $this->td_rawmaterial_out->insert_data($dataOut);
                        $actUpdateStok = $this->td_rawmaterial_stok_master->update_stok($stokmaster['rmstokmaster_kd'], 'OUT', $rmstokadj_qty[$i]);
                    }
                }
                
                if ($this->db->trans_status() !== FALSE){
                    $this->db->trans_commit();
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
                }else{
                    $this->db->trans_rollback();
                    $resp = array('code' => 400, 'status' => 'Gagal;', 'pesan' => 'Gagal Simpan');
                }
            }else{
                $resp = array('code' => 400, 'status' => 'Gagal;', 'pesan' => 'Gagal simpan Raw Material kosong');
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);

    }

    public function action_delete ()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $id = $this->input->get('id');
        if (!empty($id)) {
            try {
                $this->db->trans_begin();
                $row = $this->td_rawmaterial_stok_adjustment->get_by_param(['rmstokadj_kd' => $id])->row_array();
                if ($row['rmstokadj_type'] == 'IN') {
                    $in = $this->td_rawmaterial_in->get_by_param(['rmstokadj_kd' => $id])->row_array();
                    $this->td_rawmaterial_in->delete_by_param(['rmstokadj_kd' => $id]);
                    /** IN jika di hapus kurangi stok */
                    $actUpdateStok = $this->td_rawmaterial_stok_master->update_stok($in['rmstokmaster_kd'], 'OUT', (float)$row['rmstokadj_qty']);
                }else {
                    $out = $this->td_rawmaterial_out->get_by_param(['rmstokadj_kd' => $id])->row_array();
                    $this->td_rawmaterial_out->delete_by_param('rmstokadj_kd', $id);
                    /** OUT jika di hapus nambah stok */
                    $actUpdateStok = $this->td_rawmaterial_stok_master->update_stok($out['rmstokmaster_kd'], 'IN', (float)$row['rmstokadj_qty']);
                }
                $this->td_rawmaterial_stok_adjustment->delete_data($id);
                if($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal');
                }else{
                    $this->db->trans_commit();
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
                }
            } catch (Throwable $e) {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal '.$e->getMessage());
            }
        } else {
            $resp = array('code' => 400, 'pesan' => 'Id tidak ditemukan');
        }
        echo json_encode($resp);
    }
}
