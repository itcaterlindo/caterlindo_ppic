<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Delivery_material extends MY_Controller
{
	private $class_link = 'inventory/raw_material/delivery_material';

	public function __construct()
	{
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_materialrequisition_general', 'td_rawmaterial_out', 'td_rawmaterial_in', 'td_rawmaterial_stok_master', 'td_delivery_material', 'td_rawmaterial_goodsreceive', 'tm_inspection_rawmaterial', 'tb_finishgood_user']);
	}

	public function index()
	{
		parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
		parent::datetimepicker_assets();
		parent::input_mask();
		$this->form_box();
	}

	public function form_box()
	{
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/form_box', $data);
	}

	public function table_box()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$date = $this->input->get('date', true);
		/** Start year 5 tahun kebelakang */
		$interval = 5;
		$startYear = date('Y') - $interval;
		$arrYear = [];
		for($i = 0; $i <= $interval; $i++){
			$arrYear[$i] = $startYear;
			$startYear++;
		}
		
		$arrMonth = [
			1 => 'Januari',
			2 => 'Februari',
			3 => 'Maret',
			4 => 'April',
			5 => 'Mei',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Agustus',
			9 => 'September',
			10 => 'Oktober',
			11 => 'November',
			12 => 'Desember',
		];

		$data['class_link'] = $this->class_link;
		$data['date'] = $date;
		$data['arrYear'] = $arrYear;
		$data['arrMonth'] = $arrMonth;

		$this->load->view('page/' . $this->class_link . '/table_box', $data);
	}

	public function table_main()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$year = $this->input->get('year', true);
		$month = $this->input->get('month', true);

		$data['class_link'] = $this->class_link;
		$data['year'] = $year;
		$data['month'] = $month;

		$this->load->view('page/' . $this->class_link . '/table_main', $data);
	}

	public function table_data()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		try{
			$year = $this->input->post('year', TRUE);
			$month = $this->input->post('month', TRUE);
			$data = $this->td_delivery_material->ssp_table2($year, $month);
			echo json_encode($data);
		}catch(\Throwable $e){
			echo $e->getMessage();
		}

	}

	public function table_history_inspection()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', true);
		$data['inspection_rawmaterial'] = $this->tm_inspection_rawmaterial->get_by_param(['tm_inspection_rawmaterial.deliverymaterial_kd' => $id])->result_array();
		if(empty($data['inspection_rawmaterial'])){
			exit('Data kosong, tidak ada data yang ditampilkan');
		}
		$this->load->view('page/'.$this->class_link.'/table_history_inspection', $data);
	}
	
	public function get_raw_material_stock() {
		$paramRM = $this->input->get('paramRM');
		$paramSRJ = $this->input->get('paramSRJ');
		$goodsReceive = $this->td_rawmaterial_goodsreceive->get_by_param(['rmgr_code_srj' => $paramSRJ])->result_array();
		$resultData = [];
		if (!empty($paramRM)) {
			$this->db->from('tm_rawmaterial')
				->join('td_rawmaterial_satuan', 'tm_rawmaterial.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd','left')
				->join('td_rawmaterial_satuan as satuan_secondary', 'tm_rawmaterial.rmsatuansecondary_kd=satuan_secondary.rmsatuan_kd','left')
				->join('td_rawmaterial_stok_master as stock', 'tm_rawmaterial.rm_kd=stock.rm_kd','left')
				->join('(SELECT rm_kd, SUM(rmgr_qty) AS sum_rmgr_qty FROM td_rawmaterial_goodsreceive WHERE rmgr_code_srj = "'.$paramSRJ.'" GROUP BY rmgr_code_srj, rm_kd) as goodsreceive', 'goodsreceive.rm_kd=tm_rawmaterial.rm_kd', 'left');
			/** Jika code SRJ terisi maka tampilkan rawmaterial material berdasarkan code SRJ */
			if(!empty($paramSRJ)){
				/** Filter material by code srj */
				$this->db->where_in('tm_rawmaterial.rm_kd', array_column($goodsReceive, 'rm_kd'))
					->like('rm_kode', $paramRM, 'match');
			}else{
				/** Filter all material */
				$this->db->like('rm_kode', $paramRM, 'match')
					->or_like('rm_nama', $paramRM, 'match')
					->or_like('rm_deskripsi', $paramRM, 'match');
			}
			$this->db->order_by('tm_rawmaterial.rm_nama', 'ASC');
			$query = $this->db
					->select('tm_rawmaterial.rm_kd, tm_rawmaterial.rm_kode, tm_rawmaterial.rm_nama, tm_rawmaterial.rm_deskripsi, tm_rawmaterial.rm_spesifikasi, tm_rawmaterial.rmsatuan_kd, td_rawmaterial_satuan.rmsatuan_nama, satuan_secondary.rmsatuan_kd as rmsatuan_secodary_kd, satuan_secondary.rmsatuan_nama as rmsatuan_secodary_nama, tm_rawmaterial.rm_konversiqty, stock.rmstokmaster_qty, COALESCE(goodsreceive.sum_rmgr_qty,0) as sum_rmgr_qty')
					->get();
			
			if (!empty($query->num_rows())) {
				$result = $query->result_array();
				foreach ($result as $r) {
					$resultData[] = [
						'id' => $r['rm_kd'], 
						'text' => $r['rm_kode'].' | '.$r['rm_nama'].'/'.$r['rm_deskripsi'].'/'.$r['rm_spesifikasi'],
						'rm_kode' => $r['rm_kode'],
						'rm_nama' => $r['rm_nama'],
						'rm_deskripsi' => $r['rm_deskripsi'],
						'rm_spesifikasi' => $r['rm_spesifikasi'],
						'rmsatuan_kd' => $r['rmsatuan_kd'],
						'rmsatuan_nama' => $r['rmsatuan_nama'],
						'rm_kd' => $r['rm_kd'],
						'stock_qty' => $r['rmstokmaster_qty'],
						'stock_by_codesrj' => $r['sum_rmgr_qty']
					];
				}
			}
		}
		echo json_encode($resultData);
	}	

	public function get_podetail_special()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$paramSpecial = $this->input->get('paramSpecial');
		$paramSRJ = $this->input->get('paramSRJ');

		/** Jika parameter code srj maka tampilkan material yang ada di GR sesuai code SRJ */
		if(!empty($paramSRJ)){
			$qGoodsReceived = $this->db->select('td_purchaseorder_detail.*, td_rawmaterial_goodsreceive.rmgr_kd, td_rawmaterial_goodsreceive.rmgr_qty, tm_purchaseorder.po_no, td_rawmaterial_satuan.rmsatuan_nama')
				->from('td_rawmaterial_goodsreceive')
				->join('td_purchaseorder_detail', 'td_purchaseorder_detail.podetail_kd=td_rawmaterial_goodsreceive.podetail_kd')
				->join('tm_purchaseorder', 'tm_purchaseorder.po_kd=td_purchaseorder_detail.po_kd')
				->join('td_rawmaterial_satuan', 'td_rawmaterial_satuan.rmsatuan_kd=td_purchaseorder_detail.rmsatuan_kd')
				->where('td_rawmaterial_goodsreceive.rmgr_code_srj', $paramSRJ)
				->where('td_rawmaterial_goodsreceive.rm_kd', 'SPECIAL')
				->order_by('tm_purchaseorder.po_no', 'asc')->get()->result_array();
			$qdeliverymaterial = $this->db->select('td_materialrequisition_general.rmgr_kd, SUM(td_materialrequisition_general.deliverymaterial_qty) as sum_deliverymaterial_qty')
				->from('td_materialrequisition_general')
				->join('td_rawmaterial_goodsreceive', 'td_rawmaterial_goodsreceive.rmgr_kd=td_materialrequisition_general.rmgr_kd', 'left')
				->where('td_rawmaterial_goodsreceive.rmgr_code_srj', $paramSRJ)
				->where('td_materialrequisition_general.rm_kd', 'SPECIAL')
				->group_by('rmgr_kd')
				->get()->result_array();
		}else{
			$qGoodsReceived = $this->db->select('td_purchaseorder_detail.*, td_rawmaterial_goodsreceive.rmgr_kd, td_rawmaterial_goodsreceive.rmgr_qty, tm_purchaseorder.po_no, td_rawmaterial_satuan.rmsatuan_nama')
				->from('td_rawmaterial_goodsreceive')
				->join('td_purchaseorder_detail', 'td_purchaseorder_detail.podetail_kd=td_rawmaterial_goodsreceive.podetail_kd')
				->join('tm_purchaseorder', 'tm_purchaseorder.po_kd=td_purchaseorder_detail.po_kd')
				->join('td_rawmaterial_satuan', 'td_rawmaterial_satuan.rmsatuan_kd=td_purchaseorder_detail.rmsatuan_kd')
				->where('td_rawmaterial_goodsreceive.rm_kd', 'SPECIAL')
				->order_by('tm_purchaseorder.po_no', 'asc')->get()->result_array();
			$qdeliverymaterial = $this->db->select('td_materialrequisition_general.rmgr_kd, SUM(td_materialrequisition_general.deliverymaterial_qty) as sum_deliverymaterial_qty')
				->from('td_materialrequisition_general')
				->where('td_materialrequisition_general.rm_kd', 'SPECIAL')
				->group_by('rmgr_kd')
				->get()->result_array();
		}
		
		$arrMaterialReg = [];
		foreach ($qdeliverymaterial as $eachMatereialReqGeneral) {
			$arrMaterialReg[$eachMatereialReqGeneral['rmgr_kd']] = $eachMatereialReqGeneral['sum_deliverymaterial_qty'];
		}

		$arrResult = [];
		foreach ($qGoodsReceived as $eachGoodsReceived) {
			$qty = $eachGoodsReceived['rmgr_qty'];
			if (isset($arrMaterialReg[$eachGoodsReceived['rmgr_kd']])) {
				$qty = $qty - $arrMaterialReg[$eachGoodsReceived['rmgr_kd']];
			}
			if (empty($qty)) {
				continue;
			}
			$arrResult[] = [
				'id' => $eachGoodsReceived['rmgr_kd'],
				'rmgr_kd' => $eachGoodsReceived['rmgr_kd'],
				'podetail_kd' => $eachGoodsReceived['podetail_kd'],
				'podetail_deskripsi' => $eachGoodsReceived['podetail_deskripsi'],
				'podetail_spesifikasi' => $eachGoodsReceived['podetail_spesifikasi'],
				'rmsatuan_kd' => $eachGoodsReceived['rmsatuan_kd'],
				'rmsatuan_nama' => $eachGoodsReceived['rmsatuan_nama'],
				'rm_kd' => $eachGoodsReceived['rm_kd'],
				'qty' => $qty,
				'text' => "{$eachGoodsReceived['po_no']} | {$eachGoodsReceived['podetail_deskripsi']} {$eachGoodsReceived['podetail_spesifikasi']}",
			];
		}

		$arrResultFilter = [];
		if (!empty($paramSpecial)) {
			foreach ($arrResult as $eachResult) {
				if (!empty(strpos($eachResult['text'], $paramSpecial))) {
					$arrResultFilter[] = $eachResult;
				}
			}
		} else {
			$arrResultFilter = $arrResult;
		}
		echo json_encode($arrResultFilter);
	}

	public function form_main()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		if(!cek_permission("DELIVERYMATERIAL_CREATE")){
			exit();
		}

		/** Tampilkan gudang berdasarkan tabel user FG */
		$kd_tipe_admin = $this->session->userdata('tipe_admin_kd');
		$data['gudang'] = $this->tb_finishgood_user->get_detail_by_param(['tb_finishgood_user.kd_tipe_admin' => $kd_tipe_admin])->result_array();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/form_main', $data);
	}

	public function action_insert_drm()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$deliverymaterial_tanggal = $this->input->post('txtdeliverymaterial_tanggal', true);
		$rm_kd = $this->input->post('txtrm_kd', true);
		$rmgr_kd = $this->input->post('txtrmgr_kd', true);
		$podetail_kd = $this->input->post('txtpodetail_kd', true);
		$code_srj = $this->input->post('txtCodeSrj', true);
		$deliverymaterial_satuan_kd = $this->input->post('txtdeliverymaterial_satuan_kd', true);
		$deliverymaterial_qty = $this->input->post('txtdeliverymaterial_qty', true);
		$bagian_kd = $this->input->post('txtbagian_kd', true);
		$dari_gudang = $this->input->post('txtname_from_gudang', true);
		$deliverymaterial_remark = $this->input->post('txtdeliverymaterial_remark', true);
		$inventory_item = $this->input->post('chkInventoryItem', true) == '1' ? '1' : '0';

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtdeliverymaterial_tanggal', 'Tanggal', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtrm_kd', 'Material', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtdeliverymaterial_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtbagian_kd', 'Bagian', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrdeliverymaterial_tanggal' => (!empty(form_error('txtdeliverymaterial_tanggal'))) ? buildLabel('warning', form_error('txtdeliverymaterial_tanggal', '"', '"')) : '',
				'idErrrm_kd' => (!empty(form_error('txtrm_kd'))) ? buildLabel('warning', form_error('txtrm_kd', '"', '"')) : '',
				'idErrdeliverymaterial_qty' => (!empty(form_error('txtdeliverymaterial_qty'))) ? buildLabel('warning', form_error('txtdeliverymaterial_qty', '"', '"')) : '',
				'idErrbagian_kd' => (!empty(form_error('txtbagian_kd'))) ? buildLabel('warning', form_error('txtbagian_kd', '"', '"')) : '',
			);
		} else {
			
			$deliverymaterial_kd = $this->td_delivery_material->create_code();

			$data_insert = [   
				'deliverymaterial_kd' => $deliverymaterial_kd,
				'deliverymaterial_no' => $this->td_delivery_material->create_no($deliverymaterial_tanggal),
				'rm_kd' => $rm_kd,
				'deliverymaterial_satuan_kd' => $deliverymaterial_satuan_kd,
				'dari_gudang' => $dari_gudang,
				'ke_gudang' => $bagian_kd,
				'status' => 'terkirim',
				'inventory_item' => $inventory_item,
				'rmgr_code_srj' => $code_srj,
				'deliverymaterial_qty' => $deliverymaterial_qty,
				'deliverymaterial_remark' => !empty($deliverymaterial_remark) ? $deliverymaterial_remark : null,
				'deliverymaterial_tanggal' => $deliverymaterial_tanggal,
				'deliverymaterial_tglinput' => date('Y-m-d H:i:s'),
				'deliverymaterial_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			];
			
			$this->db->trans_begin();

			$this->td_delivery_material->insert_data($data_insert);
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			} else {
				$this->db->trans_commit();
				$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}
		
		}	

		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}


	public function action_delete()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		$act = $this->td_delivery_material->delete_data($id);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		} else {
			$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
		}

		echo json_encode($resp);
	}


	public function get_stock()
	{
		$rm_kd = $this->input->get('rm_kd', TRUE);
		$kd_gudang = $this->input->get('kd_gudang', TRUE);

		//$rowGeneral = $this->td_rawmaterial_stok_master->get_by_param(['rm_kd' => $id])->row_array();

		$sql = "SELECT COALESCE(td_rawmaterial_stok_master.rmstokmaster_qty,0) AS rmstokmaster_qty
		 		FROM td_rawmaterial_stok_master 
				WHERE td_rawmaterial_stok_master.rm_kd = '".$rm_kd."' AND td_rawmaterial_stok_master.kd_gudang = '".$kd_gudang."'";
		$rowGeneral = $this->db->query($sql)->row_array();

		$response = [
			'rmstokmaster_qty' => !empty($rowGeneral) ? $rowGeneral['rmstokmaster_qty'] : 0
		];

		echo json_encode($response);
	}

	public function terima(){
		$this->db->trans_begin();
		$code = null;
		$rm_kd = $this->input->get('id', TRUE);

		$dlmt_id = $this->td_delivery_material->get_by_param (['deliverymaterial_kd' => $rm_kd])->row_array();

		/** Process jika statusnya inventory item (tambah kurang stock) */
		if($dlmt_id['inventory_item'] == '1'){
			$gudang_tujuan = $this->td_rawmaterial_stok_master->get_by_param(['kd_gudang' => $dlmt_id['ke_gudang'], 'rm_kd' => $dlmt_id['rm_kd']])->result_array();
			$gudang_asal = $this->td_rawmaterial_stok_master->get_by_param(['kd_gudang' => $dlmt_id['dari_gudang'], 'rm_kd' => $dlmt_id['rm_kd']])->result_array();

			$rmstokmaster_kds = $this->td_rawmaterial_stok_master->get_by_param(['rm_kd' => $dlmt_id['rm_kd']])->result_array();
			$rmstokmaster_kd = $rmstokmaster_kds[0]['rmstokmaster_kd'];

			$rmin_kd = $this->td_rawmaterial_in->create_code();
			//rm in
			$arrRMin[] = [
				'rmin_kd' => $rmin_kd,
				'rmstokmaster_kd' => $rmstokmaster_kd,
				'rm_kd' => $dlmt_id['rm_kd'],
				'deliverymaterial_kd' => $dlmt_id['deliverymaterial_kd'],
				'rmin_qty' => $dlmt_id['deliverymaterial_qty'],
				'rmin_tglinput' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			];
			$rmin_kd = $rmin_kd + 1;
			$this->td_rawmaterial_in->insert_batch($arrRMin);
			/** RM Out */
			$dataOut = [
				'rmout_kd' => $this->td_rawmaterial_out->create_code(),
				'rmstokmaster_kd' => $rmstokmaster_kd,
				'rm_kd' => $dlmt_id['rm_kd'],
				'deliverymaterial_kd' => $dlmt_id['deliverymaterial_kd'],
				'rmout_qty' => $dlmt_id['deliverymaterial_qty'],
				'rmout_tglinput' => date('Y-m-d H:i:s'),
				'rmout_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			];
			$this->td_rawmaterial_out->insert_data($dataOut);
			if (!empty($gudang_tujuan)) {
				$this->td_rawmaterial_stok_master->update_stock_batch('OUT', [['rmstokmaster_kd' => $gudang_asal[0]['rmstokmaster_kd'], 'qty' => $dlmt_id['deliverymaterial_qty']]]);
				$this->td_rawmaterial_stok_master->update_stock_batch('IN', [['rmstokmaster_kd' => $gudang_tujuan[0]['rmstokmaster_kd'], 'qty' => $dlmt_id['deliverymaterial_qty']]]);
			}else{
				$this->td_rawmaterial_stok_master->update_stock_batch('OUT', [['rmstokmaster_kd' => $gudang_asal[0]['rmstokmaster_kd'], 'qty' => $dlmt_id['deliverymaterial_qty']]]);
				$code = $this->td_rawmaterial_stok_master->create_code($code);
				$arrayRespNull[] = [
					'rmstokmaster_kd' => $code,
					'rm_kd' => $dlmt_id['rm_kd'],
					'kd_gudang' => $dlmt_id['ke_gudang'],
					'rmstokmaster_qty' =>  $dlmt_id['deliverymaterial_qty'],
					'rmstokmaster_tglinput' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				];

				$actInsertMater = $this->td_rawmaterial_stok_master->insert_batch($arrayRespNull);
			}
		}
		/** Process jika statusnya inventory item (tambah kurang stock) */

		$act = $this->td_delivery_material->update_data(['deliverymaterial_kd' => $rm_kd], ['status' => "diterima"]);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		} else {
			$this->db->trans_commit();
			$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
		}

		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	/** Validasi stock delivery material tidak boleh lebih dari qty stock yang ada di gudang */
	// public function check_stock()
	// {
	// 	$rmkd = $this->input->post('txtrm_kd', true);
	// 	$qty = $this->input->post('txtdeliverymaterial_qty', true);
	// 	$stockSrj = $this->input->post('txtstock_srj', true);
	// 	$codeSrj = $this->input->post('txtCodeSrj', true);
	// 	$gudang = $this->input->post('txtname_from_gudang', true);
	// 	$stokMaster = $this->td_rawmaterial_stok_master->get_by_param(['kd_gudang' => $gudang, 'rm_kd' => $rmkd])->row_array();
	// 	if( doubleval($qty) > doubleval($stokMaster['rmstokmaster_qty']) ){
	// 		/** Check jika inputan qty melebihi current stock */
	// 		$this->form_validation->set_message('check_stock', 'QTY input tidak boleh melebihi QTY CURRENT STOCK');
	// 		return false;
	// 	}else if( doubleval($qty) > doubleval($stockSrj) && !empty($codeSrj) ){
	// 		/** Check jika code srj tidak kosong dan inputan melebihi stock srj */
	// 		$this->form_validation->set_message('check_stock', 'Qty input tidak boleh melebihi STOCK QTY SRJ');
	// 		return false;
	// 	}else{
	// 		return true;
	// 	}
	// }

}
