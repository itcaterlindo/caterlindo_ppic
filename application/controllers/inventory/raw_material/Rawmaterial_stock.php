<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Rawmaterial_stock extends MY_Controller
{
    private $class_link = 'inventory/raw_material/rawmaterial_stock';

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('ssp', 'form_validation'));
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
        $this->load->model(array('td_rawmaterial_stok_master', 'db_hrm/tb_hari_efektif'));
    }

    public function index()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();
        parent::datetimepicker_assets();

        $data['class_link'] = $this->class_link;

        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function table_main()
    {
        // if (!$this->input->is_ajax_request()) {
        //     exit('No direct script access allowed');
        // }

        $future_timestamp = strtotime("-3 month");
        $mounthago = date('Y-m-d', $future_timestamp);
      

        $now = date('Y-m-d');

        $suggest = $this->tb_hari_efektif->get_lead_time($mounthago, $now) + 1;
                
        // $qResult = $this->db->select('td_rawmaterial_stok_master.*, tm_rawmaterial.*, td_rawmaterial_satuan.rmsatuan_nama')
        //     ->join('tm_rawmaterial', 'tm_rawmaterial.rm_kd=td_rawmaterial_stok_master.rm_kd', 'left')
        //     ->join('td_rawmaterial_satuan', 'td_rawmaterial_satuan.rmsatuan_kd=tm_rawmaterial.rmsatuan_kd', 'left')
        //     ->order_by('tm_rawmaterial.rm_kode')
        //     ->get('td_rawmaterial_stok_master')
        //     ->result_array();
        // $data['results'] = $qResult;
        // $dataku = $this->db->query("SELECT SUM(rmout_qty) as qty FROM td_rawmaterial_out WHERE materialreqgeneral_kd IS NOT NULL OR materialreceiptdetailrm_kd IS NOT NULL");
        // $dataku = $dataku->result();
        // $calculate = $dataku[0]->qty / $suggest;

        $results = $this->db->query("SELECT td_rawmaterial_stok_master.*,
                                                    td_rawmaterial_stok_master.rm_kd as rwm_kd,
                                                    (((SELECT SUM(rmout_qty) as qty FROM (SELECT * FROM td_rawmaterial_out WHERE materialreqgeneral_kd IS NOT NULL OR materialreceiptdetailrm_kd IS NOT NULL) as q 
									
                                                    WHERE rm_kd = rwm_kd AND DATE(rmout_tglinput) BETWEEN '$mounthago' AND '$now') / $suggest) * td_suplier_katalog.real_leadtime) as suggest,
                                                    tm_rawmaterial.*,
                                                    td_rawmaterial_satuan.rmsatuan_nama
                                            FROM td_rawmaterial_stok_master 
                                            LEFT JOIN tm_rawmaterial ON tm_rawmaterial.rm_kd=td_rawmaterial_stok_master.rm_kd 
                                            LEFT JOIN 
                                                    (SELECT AVG(suplier_leadtime) as real_leadtime, td_suplier_katalog.*, tm_suplier.suplier_nama, tm_suplier.suplier_leadtime 
                                                    FROM td_suplier_katalog 
                                                    LEFT JOIN tm_suplier ON tm_suplier.suplier_kd = td_suplier_katalog.suplier_kd GROUP BY rm_kd) AS td_suplier_katalog ON tm_rawmaterial.rm_kd = td_suplier_katalog.rm_kd
                                            LEFT JOIN td_rawmaterial_satuan ON td_rawmaterial_satuan.rmsatuan_kd=tm_rawmaterial.rmsatuan_kd 
                                            WHERE tm_rawmaterial.inventory_item = 'Y' AND tm_rawmaterial.flag_active = '1'
                                            ORDER BY tm_rawmaterial.rm_kode")->result_array();
        
        /** Initate Recap Data */
        $kartuStok = $this->td_rawmaterial_stok_master->getLastStokResult( array_column($results, 'rm_kd'), date('Y-m-d H:i:s'));
        $kartuStokFinal = [];
        /** Switch values to key */
        foreach($kartuStok as $key => $value){
            $kartuStokFinal[$value['rm_kd']] = $value['qty_aft_mutasi'];
        }
        $data = [];
        $i = 0;
        /** Recap Data */
        foreach($results as $key => $value){
            $data['results'][$i] = $value;
            $data['results'][$i]['last_stock'] = ROUND($kartuStokFinal[$value['rm_kd']], 2);
            /** Jika qty current dan last stock tidak sesuai maka recalculate */
            if( ROUND($value['rmstokmaster_qty'], 2) !== ROUND($data['results'][$i]['last_stock'], 2) ){
                $act = $this->recalculate($value['rmstokmaster_kd']);
                $data['results'][$i]['rmstokmaster_qty'] = ROUND($data['results'][$i]['last_stock'], 2);
                $data['results'][$i]['last_stock'] = ROUND($data['results'][$i]['last_stock'], 2);
                $data['results'][$i]['recalculate'] = true;
            }
            $i++;
        }
        // header('Content-Type: application/json');
        // echo json_encode($data);
        $this->load->view('page/' . $this->class_link . '/table_main', $data);

        // rmstokmaster_qty di bandingkan dengan last_stock
        // Jika rmstokmaster_qty tidak sesuai dengan last stock maka recalculate

    }

    public function get_rmstokmaster()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $rmstokmaster_kd = $this->input->get('rmstokmaster_kd');
        $resp = $this->td_rawmaterial_stok_master->get_by_param(['rmstokmaster_kd' => $rmstokmaster_kd])->row_array();

        echo json_encode($resp);
    }

    public function action_recalculate_stok ()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $rmstokmaster_kd = $this->input->get('rmstokmaster_kd');
        if (!empty($rmstokmaster_kd)){
            $act = $this->recalculate($rmstokmaster_kd);
            if($act){
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }else{
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal tersimpan');
            }
        }else{
            $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Id tidak ditemukan');
        }
        echo json_encode($resp);
    }

    /** Recal stock */
    private function recalculate($rmstokmaster_kd = "")
    {
        $rmstokmaster = $this->td_rawmaterial_stok_master->get_by_param(['rmstokmaster_kd' => $rmstokmaster_kd])->row_array();
        $mutasiStok = $this->td_rawmaterial_stok_master->generateMutasiStokResult([$rmstokmaster['rm_kd']], date('Y-m-d H:i:s'));
        /** Update cur stok */
        $act = $this->td_rawmaterial_stok_master->update_data (['rmstokmaster_kd' => $rmstokmaster_kd], ['rmstokmaster_qty' => $mutasiStok[$rmstokmaster['rm_kd']]['qty_aft_mutasi']]);
        return $act;
    }



}
