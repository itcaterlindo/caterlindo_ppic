<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Material_receipt extends MY_Controller
{
    private $class_link = 'inventory/raw_material/material_receipt';
    private $wf_kd = 6;

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('ssp', 'form_validation'));
        $this->load->helper(array('form', 'html', 'my_helper', 'my_btn_access_helper'));
        $this->load->model(array(
            'tm_materialreceipt', 'tm_materialrequisition', 'td_workflow_state',
            'td_workflow_transition_notification', 'td_workflow_transition', 'tb_materialreceipt_detail_requisition', 'td_materialrequisition_detail', 'td_materialreceipt_detail',
            'td_materialreceipt_detail_rawmaterial', 'td_rawmaterial_stok_master', 'td_rawmaterial_out', 'tm_materialrequisition', 'td_materialreceipt_log', 'tb_bagian', 'td_rawmaterial_goodsreceive'
        ));
    }

    public function index()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();
        parent::datetimepicker_assets();

        $data['class_link'] = $this->class_link;

        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    public function table_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }

    public function table_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data = $this->tm_materialreceipt->ssp_table2();

        echo json_encode($data);
    }

    public function table_detail_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $data['class_link'] = $this->class_link;
        $data['results'] = $this->td_materialreceipt_detail->getReceiptDetailWoitem(['materialreceipt_kd' => $data['id']])->result_array();
        $this->load->view('page/' . $this->class_link . '/table_detail_main', $data);
    }

    public function table_detail_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $data = $this->td_materialreceipt_detail->ssp_table($data['id']);

        echo json_encode(
            SSP::simple($_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'])
        );
    }

    public function form_box()
    {
        parent::administrator();
        parent::pnotify_assets();

        // if (!cek_permission('WEEKLYPLANNING_UPDATE')) {
        // show_error('Anda tidak memiliki akses ! <br> <a href="'.base_url().$this->class_link.'"> Weekly Planning </a>', '202', 'Error Unauthorized');
        // }

        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $data['class_link'] = $this->class_link;
        $data['header'] = $this->tm_materialreceipt->getRowmaterialreceiptBagianState($data['id']);
        $receiptReqs = $this->tb_materialreceipt_detail_requisition->get_by_param_detail(['tm_materialreceipt.materialreceipt_kd' => $data['id']])->result_array();
        $data['header']['materialreq_nos'] =  implode(',', array_column($receiptReqs, 'materialreq_no'));

        $this->load->view('page/' . $this->class_link . '/form_box', $data);
    }

    public function form_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_main', $data);
    }

    public function formout_box()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();

        // if (!cek_permission('WEEKLYPLANNING_UPDATE')) {
        // show_error('Anda tidak memiliki akses ! <br> <a href="'.base_url().$this->class_link.'"> Weekly Planning </a>', '202', 'Error Unauthorized');
        // }

        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $data['class_link'] = $this->class_link;
        $data['header'] = $this->tm_materialreceipt->getRowmaterialreceiptBagianState($data['id']);
        $receiptReqs = $this->tb_materialreceipt_detail_requisition->get_by_param_detail(['tm_materialreceipt.materialreceipt_kd' => $data['id']])->result_array();
        $data['header']['materialreq_nos'] =  implode(',', array_column($receiptReqs, 'materialreq_no'));

        $this->load->view('page/' . $this->class_link . '/formout_box', $data);
    }

    public function formout_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);
        $data['masterMRC'] = $this->tm_materialreceipt->get_by_param(['materialreceipt_kd' => $data['id']])->row_array();

        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/formout_main', $data);
    }

    public function formout_tabledetail()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/formout_tabledetail', $data);
    }

    public function formout_tabledetail_data()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $data = $this->td_materialreceipt_detail_rawmaterial->ssp_table2($data['id']);

        echo json_encode($data);
    }

    public function formout_tabledetailmaterial()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $results = $this->td_materialreceipt_detail->getMaterialPartReceipt($data['id']);
        foreach($results as $key => $val){
            $data['results'][$key] = $val;
            $data['results'][$key]['current_stock'] = $this->td_rawmaterial_stok_master->get_by_param( ['rm_kd' => $val['rm_kd'], 'kd_gudang' => 'MGD260719003'] )->row_array()['rmstokmaster_qty'];
        }

        $resultRMOuts = $this->td_materialreceipt_detail_rawmaterial->getMaterialOutGroupByRM($data['id']);
        foreach($resultRMOuts as $key => $val){
            $data['resultRMOuts'][$key] = $val;
            $data['resultRMOuts'][$key]['current_stock'] = $this->td_rawmaterial_stok_master->get_by_param( ['rm_kd' => $val['rm_kd'], 'kd_gudang' => 'MGD260719003'] )->row_array()['rmstokmaster_qty'];
        }

        $this->load->view('page/' . $this->class_link . '/formout_tabledetailmaterial', $data);
    }

    public function formout_tabledetailrmbom()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $data['results'] = $this->td_materialreceipt_detail->getMaterialPartReceipt($data['id']);
        $resultOuts = [];
        $rOuts = $this->td_materialreceipt_detail_rawmaterial->getMaterialOutGroupByRM($data['id']);
        foreach ($rOuts as $rOut) {
            $resultOuts[$rOut['rm_kd']] = $rOut['sum_materialreceiptdetailrm_qtykonversi'];
        }
        $data['resultOuts'] = $resultOuts;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/formout_tabledetailrmbom', $data);
    }

    public function formout_tabledetailrmbomv2()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $data['materialDetailByReceipts'] = $this->td_materialreceipt_detail->generateDetailMaterialByMaterailReceiptDetailKd($data['id']);
        $resultOuts = [];
        $rOuts = $this->td_materialreceipt_detail_rawmaterial->getMaterialOutGroupByMaterialReceiptDetailRM($data['id']);
        foreach ($rOuts as $rOut) {
            $resultOuts[$rOut['materialreceiptdetail_kd'].'-'.$rOut['rm_kd']] = $rOut['sum_materialreceiptdetailrm_qtykonversi'];
        }
        $data['resultOuts'] = $resultOuts;

        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/formout_tabledetailrmbomv2', $data);
    }

    public function form_tablematerialrequisition()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $qMaster = $this->tm_materialreceipt->get_by_param(['materialreceipt_kd' => $data['id']])->row_array();
        $qMasterDetail = $this->tb_materialreceipt_detail_requisition->get_by_param(['materialreceipt_kd' => $data['id']])->result_array();
        $materialreqs = $this->td_materialrequisition_detail->get_where_in_detail('tm_materialrequisition.materialreq_kd', array_column($qMasterDetail, 'materialreq_kd'))->result_array();

        $arrMasterDetail = [];
        foreach ($qMasterDetail as $eachMasterDetail) {
            $arrMasterDetail[$eachMasterDetail['materialreq_kd']] = $eachMasterDetail['materialreceiptreq_kd'];
        }

        /** Search yang sudah input di planning weekly detail */
        $materialReceiptDetails = $this->td_materialreceipt_detail->getItemInsertedGroupByReqdetail(array_column($qMasterDetail, 'materialreq_kd'));

        $results = [];
        foreach ($materialreqs as $materialreq) {
            $qty = $materialreq['materialreqdetail_qty'];
            foreach ($materialReceiptDetails as $materialReceiptDetail) {
                if ($materialReceiptDetail['materialreqdetail_kd'] == $materialreq['materialreqdetail_kd']) {
                    $qty = $qty - $materialReceiptDetail['sum_materialreceiptdetail_qty'];
                }
            }
            if (empty($qty)) {
                continue;
            }
            $results[] = [
                'materialreqdetail_kd' => $materialreq['materialreqdetail_kd'],
                'woitem_kd' => $materialreq['woitem_kd'],
                'materialreceiptreq_kd' => $arrMasterDetail[$materialreq['materialreq_kd']],
                'materialreq_kd' => $materialreq['materialreq_kd'],
                'materialreq_no' => $materialreq['materialreq_no'],
                'woitem_itemcode' => $materialreq['woitem_itemcode'],
                'woitem_no_wo' => $materialreq['woitem_no_wo'],
                'woitem_deskripsi' => $materialreq['woitem_deskripsi'],
                'woitem_dimensi' => $materialreq['woitem_dimensi'],
                'woitem_jenis' => $materialreq['woitem_jenis'],
                'materialreqdetail_qty' => $qty,
                'rmgr_code_srj' => $qMaster['rmgr_code_srj'],
                'batch' => $qMaster['batch'],
            ];
        }

        $data['class_link'] = $this->class_link;
        $data['results'] = $results;
        $this->load->view('page/' . $this->class_link . '/form_tablematerialrequisition', $data);
    }

    public function form_master_box()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_master_box', $data);
    }

    public function form_master_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $materialReqs = $this->tm_materialrequisition->get_by_param_detail(['materialreq_receiptclosed' => 0])->result_array();
        foreach ($materialReqs as $materialReq) {
            $opsiMaterialReqs[$materialReq['materialreq_kd']] = "{$materialReq['materialreq_no']} | {$materialReq['bagian_nama']}";
        }

        $bagians = $this->tb_bagian->get_by_param(['bagian_lokasi' => 'WIP'])->result_array();
        $opsiBagians[null] = '-- Pilih Opsi --';
        foreach ($bagians as $bagian) {
            $opsiBagians[$bagian['bagian_kd']] = $bagian['bagian_nama'];
        }

        $goodsReceives = $this->td_rawmaterial_goodsreceive->get_all()->result_array();
        $opsiGoodsReceives[null] = '-- Pilih Opsi --';
        foreach($goodsReceives as $goodsreceive){
            if( !in_array($goodsreceive['rmgr_code_srj'], $opsiGoodsReceives) ){
                $opsiGoodsReceives[$goodsreceive['rmgr_code_srj']] = $goodsreceive['rmgr_code_srj'];
            }
        }

        if ($data['slug'] == 'edit') {
            $data['rowData'] = $this->tm_materialreceipt->get_by_param(['materialreceipt_kd' => $data['id']])->row_array();
            $data['receiptReqs'] = $this->tb_materialreceipt_detail_requisition->get_by_param_detail(['tb_materialreceipt_detail_requisition.materialreceipt_kd' => $data['id']])->result_array();
        }
        $data['opsiMaterialReqs'] = $opsiMaterialReqs;
        $data['opsiBagians'] = $opsiBagians;
        $data['opsiGoodsReceives'] = $opsiGoodsReceives;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_master_main', $data);
    }

    public function get_materialreqs()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $paramMaterialreq = $this->input->get('paramMaterialreq', true);
        $bagian_kd = $this->input->get('bagian_kd', true);

        $materialReqs = $this->db->select('tm_materialrequisition.*, tb_bagian.*')
            ->from('tm_materialrequisition')
            ->join('tb_bagian', 'tm_materialrequisition.bagian_kd=tb_bagian.bagian_kd', 'left')
            ->where('tm_materialrequisition.materialreq_receiptclosed', 0)
            ->where('tb_bagian.bagian_kd', $bagian_kd);
        if (!empty($paramMaterialreq)) {
            $materialReqs = $materialReqs->like('tm_materialrequisition.materialreq_no', $paramMaterialreq);
        }
        $materialReqs = $materialReqs->get()->result_array();

        /** Get material request yang sudah ada dimaterial receipt */
        $dataMrcJoinMreq = $this->db->select('DISTINCT(t2.materialreq_no) materialreq_no')
            ->from('tm_materialreceipt t0 ')
            ->join('tb_materialreceipt_detail_requisition t1', 't0.materialreceipt_kd = t1.materialreceipt_kd', 'inner')
            ->join('tm_materialrequisition t2', 't1.materialreq_kd = t2.materialreq_kd', 'inner')
            ->where('t2.bagian_kd', $bagian_kd)
            ->where('t2.materialreq_receiptclosed', 1);
        $dataMrcJoinMreq = $dataMrcJoinMreq->get()->result_array();

        $arrayMaterialReqs = [];
        foreach ($materialReqs as $materialReq) {
            /** Cek apakah ada materialreq sudah di material receipt, jika ada maka jangan ditampilkan ke select box */
            if( !in_array($materialReq['materialreq_no'], array_column($dataMrcJoinMreq, 'materialreq_no')) ){
                $arrayMaterialReqs[] = [
                    'id' => $materialReq['materialreq_kd'],
                    'text' => "{$materialReq['materialreq_no']} | {$materialReq['bagian_nama']}",
                ];
            }
        }

        echo json_encode($arrayMaterialReqs);
    }

    public function get_woitemreceipt()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $materialreceipt_kd = $this->input->get('materialreceipt_kd', true);
        $paramWoitemreceipt = $this->input->get('paramWoitemreceipt', true);

        $materialRecpts = $this->db->select('td_materialreceipt_detail.*, td_workorder_item.woitem_no_wo, td_workorder_item.woitem_itemcode')
            ->from('td_materialreceipt_detail')
            ->join('td_workorder_item', 'td_workorder_item.woitem_kd=td_materialreceipt_detail.woitem_kd', 'left')
            ->where('td_materialreceipt_detail.materialreceipt_kd', $materialreceipt_kd)
            ->like('td_workorder_item.woitem_no_wo', $paramWoitemreceipt)
            ->get()->result_array();

        $arrayMaterialRecpts = [];
        foreach ($materialRecpts as $materialRecpt) {
            $arrayMaterialRecpts[] = [
                'id' => $materialRecpt['materialreceiptdetail_kd'],
                'text' => "{$materialRecpt['woitem_no_wo']} | {$materialRecpt['woitem_itemcode']}",
            ];
        }

        echo json_encode($arrayMaterialRecpts);
    }

    public function form_master_tablemain()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $data['slug'] = $this->input->get('slug', true);
        $data['id'] = $this->input->get('id', true);

        $data['results'] = $this->tb_materialreceipt_detail_requisition->get_by_param_detail(['tb_materialreceipt_detail_requisition.materialreceipt_kd' => $data['id']])->result_array();

        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/form_master_tablemain', $data);
    }

    public function view_tablemain()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $materialreceipt_kd = $this->input->get('id', true);
        $data['results'] = $this->td_materialreceipt_detail->getDetailPlanningWoitem($materialreceipt_kd);
        $materialreceiptdetail_kds = array_column($data['results'], 'materialreceiptdetail_kd');
        $data['materialreqdetails'] = $this->td_materialrequisition_detail->get_where_in_detail('materialreceiptdetail_kd', $materialreceiptdetail_kds)->result_array();

        $this->load->view('page/' . $this->class_link . '/view_tablemain', $data);
    }

    public function pdf_tablemain()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $materialreceipt_kd = $this->input->get('id', true);
        $data['resultDetails'] = $this->td_materialreceipt_detail->getReceiptDetailWoitem(['td_materialreceipt_detail.materialreceipt_kd' => $materialreceipt_kd])->result_array();
        $data['resultMaterialDetails'] = $this->td_materialreceipt_detail_rawmaterial->getMaterialDetail(['td_materialreceipt_detail_rawmaterial.materialreceipt_kd' => $materialreceipt_kd])->result_array();

        $this->load->view('page/' . $this->class_link . '/pdf_tablemain', $data);
    }

    public function pdf_main()
    {
        $this->load->library('Pdf');
        $materialreceipt_kd = $this->input->get('id', true);

        $data['master'] = $this->tm_materialreceipt->getRowmaterialreceiptBagianState($materialreceipt_kd);
        $receiptReqs = $this->tb_materialreceipt_detail_requisition->get_by_param_detail(['tm_materialreceipt.materialreceipt_kd' => $materialreceipt_kd])->result_array();
        $data['master']['materialreq_nos'] =  implode(', ', array_column($receiptReqs, 'materialreq_no'));

        $dataDetail['resultDetails'] = $this->td_materialreceipt_detail->getReceiptDetailWoitem(['td_materialreceipt_detail.materialreceipt_kd' => $materialreceipt_kd])->result_array();
        $dataDetail['resultMaterialDetails'] = $this->td_materialreceipt_detail_rawmaterial->getMaterialDetailGroupByRM(['td_materialreceipt_detail_rawmaterial.materialreceipt_kd' => $materialreceipt_kd])->result_array();
        $data['konten'] = $this->load->view('page/' . $this->class_link . '/pdf_tablemain', $dataDetail, true);
        $data['logs'] = $this->td_materialreceipt_log->get_log_printout($materialreceipt_kd)->result_array();

        $this->load->view('page/' . $this->class_link . '/pdf_main', $data);
    }

    public function pdf_cetak_item_detail_by_wo()
    {
        $this->load->library('Pdf');
        $materialreceipt_kd = $this->input->get('id', true);

        $data['master'] = $this->tm_materialreceipt->getRowmaterialreceiptBagianState($materialreceipt_kd);
        $receiptReqs = $this->tb_materialreceipt_detail_requisition->get_by_param_detail(['tm_materialreceipt.materialreceipt_kd' => $materialreceipt_kd])->result_array();
        $data['master']['materialreq_nos'] =  implode(', ', array_column($receiptReqs, 'materialreq_no'));

        $dataDetail['materialDetailByReceipts'] = $this->td_materialreceipt_detail->generateDetailMaterialByMaterailReceiptDetailKd($materialreceipt_kd);
        $materialOutByWOs = $this->td_materialreceipt_detail_rawmaterial->getMaterialDetailGroupByRMWoitem(['td_materialreceipt_detail_rawmaterial.materialreceipt_kd' => $materialreceipt_kd])->result_array();
        $arrMasterialOutByWOs = [];
        foreach ($materialOutByWOs as $materialOutByWO){
            $arrMasterialOutByWOs[$materialOutByWO['woitem_kd']][] = $materialOutByWO;
        }
        $dataDetail['materialOutByWOs'] = $arrMasterialOutByWOs;

        $data['konten'] = $this->load->view('page/' . $this->class_link . '/pdf_tabledetailwomaterial', $dataDetail, true);

        $this->load->view('page/' . $this->class_link . '/pdfdetail_main', $data);
    }

    public function view_box()
    {
        parent::administrator();
        parent::pnotify_assets();
        $id = $this->input->get('id', true);

        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        $data['header'] = $this->tm_materialreceipt->getRowmaterialreceiptBagianState($data['id']);
        $receiptReqs = $this->tb_materialreceipt_detail_requisition->get_by_param_detail(['tm_materialreceipt.materialreceipt_kd' => $data['id']])->result_array();
        $data['header']['materialreq_nos'] =  implode(',', array_column($receiptReqs, 'materialreq_no'));
        $data['generate_button'] = $this->tm_materialreceipt->generate_button($id, $this->wf_kd, $data['header']['wfstate_kd']);

        $this->load->view('page/' . $this->class_link . '/view_box', $data);
    }

    public function view_log()
    {
        $id = $this->input->get('id', true);

        $data['result'] = $this->db->select('td_materialreceipt_log.*, td_workflow_transition.*, tb_admin.nm_admin, state_source.wfstate_nama as state_source, state_dst.wfstate_nama as state_dst')
            ->from('td_materialreceipt_log')
            ->join('td_workflow_transition', 'td_materialreceipt_log.wftransition_kd=td_workflow_transition.wftransition_kd', 'left')
            ->join('td_workflow_state as state_source', 'state_source.wfstate_kd=td_workflow_transition.wftransition_source', 'left')
            ->join('td_workflow_state as state_dst', 'state_dst.wfstate_kd=td_workflow_transition.wftransition_destination', 'left')
            ->join('tb_admin', 'td_materialreceipt_log.admin_kd=tb_admin.kd_admin', 'left')
            ->where('td_materialreceipt_log.materialreceipt_kd', $id)
            ->order_by('td_materialreceipt_log.materialreceiptlog_tglinput', 'desc')
            ->get()->result_array();

        $this->load->view('page/' . $this->class_link . '/view_log', $data);
    }

    public function formubahstate_main()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $id = $this->input->get('id');
        $wftransition_kd = $this->input->get('wftransition_kd');

        $data['id'] = $id;
        $data['wftransition_kd'] = $wftransition_kd;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/' . $this->class_link . '/formubahstate_main', $data);
    }

    private function action_special($action = null, $aParam = [])
    {
        $act = true;
        $admin_kd = $this->session->userdata('kd_admin');
        switch ($action) {
            case 'MATERIALREQ_CHECK_CLOSED':
                $receiptReqs = $this->tb_materialreceipt_detail_requisition->get_by_param(['materialreceipt_kd' => $aParam['materialreceipt_kd']])->result_array();
                $materialreq_kds = array_column($receiptReqs, 'materialreq_kd');
                $act = $this->tm_materialrequisition->checkMaterialReqReceipt($materialreq_kds);
                break;
            default:
                $act = true;
        }

        return $act;
    }

    public function action_change_state()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtid', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtwftransition_kd', 'Transition', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrrm_kd' => (!empty(form_error('txtid'))) ? buildLabel('warning', form_error('txtid', '"', '"')) : '',
                'idErrrm_kd' => (!empty(form_error('txtwftransition_kd'))) ? buildLabel('warning', form_error('txtwftransition_kd', '"', '"')) : '',
            );
        } else {
           
            $materialreceipt_kd = $this->input->post('txtid', true);
            $wftransition_kd = $this->input->post('txtwftransition_kd', true);
            $materialreceiptlog_note = $this->input->post('txtmaterialreceiptlog_note', true);

            $data = [
                'materialreceiptlog_kd' => $this->td_materialreceipt_log->create_code(),
                'materialreceipt_kd' => $materialreceipt_kd,
                'wftransition_kd' => $wftransition_kd,
                'admin_kd' => $this->session->userdata('kd_admin'),
                'materialreceiptlog_note' => $materialreceiptlog_note,
                'materialreceiptlog_tglinput' => date('Y-m-d H:i:s'),
            ];
            //$resp['jalan'] = $data;
            //$this->db->trans_begin();
            $materialreceipt = $this->tm_materialreceipt->get_by_param(['materialreceipt_kd' => $materialreceipt_kd])->row_array();
            $wftransition = $this->td_workflow_transition->get_by_param(['wftransition_kd' => $wftransition_kd])->row_array();
            $dataUpdate = [
                'wfstate_kd' => $wftransition['wftransition_destination'],
                'materialreceipt_tgledit' => date('Y-m-d H:i:s'),
            ];
            // #email message notification
            // $subject = 'Material Receipt - ' . $materialreceipt['materialreceipt_no'];
            //  $resp['jalanx'] = $materialreceipt;
            //   $resp['jalan'] = $wftransition;
            // $dtMessage['text'] = 'Terdapat Material Receipt yang perlu approval Anda :';
            // $dtMessage['url'] = 'http://' . $_SERVER['HTTP_HOST'] . base_url() . $this->class_link . '/view_box?id=' . $materialreceipt_kd;
            // $dtMessage['urltext'] = 'Detail Material Receipt';
            // $dtMessage['keterangan'] = !empty($materialreceiptlog_note) ? $materialreceiptlog_note : null;
            // $message = $this->load->view('templates/email/email_notification', $dtMessage, true);

            #special action
            // if (!empty($wftransition['wftransition_action'])) {
            //     $expAction = explode(';', $wftransition['wftransition_action']);
            //     for ($i = 0; $i < count($expAction); $i++) {
            //         $actSpecial = $this->action_special($expAction[$i], $data);
            //     }
            // }

            $act = $this->td_materialreceipt_log->insert_data($data);
            $actUpdate = $this->tm_materialreceipt->update_data(['materialreceipt_kd' => $materialreceipt_kd], $dataUpdate);
            //$actNotif = $this->td_workflow_transition_notification->generate_notification($wftransition_kd, $subject, '');

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            } else {
                $this->db->trans_commit();
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_insert_master()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtmaterialreq_kds[]', 'Material Req', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtmaterialreq_tgl', 'Tanggal', 'required', ['required' => '{field} tidak boleh kosong!']);


        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrmaterialreq_kds' => (!empty(form_error('txtmaterialreq_kds[]'))) ? buildLabel('warning', form_error('txtmaterialreq_kds[]', '"', '"')) : '',
                'idErrmaterialreq_tgl' => (!empty(form_error('txtmaterialreq_tgl'))) ? buildLabel('warning', form_error('txtmaterialreq_tgl', '"', '"')) : '',
            );
        } else {
            $slug = $this->input->post('slug', true);
            $materialreceipt_kd = $this->input->post('txtmaterialreceipt_kd', true);
            $materialreq_kds = $this->input->post('txtmaterialreq_kds', true);
            $bagian_kd = $this->input->post('txtbagian_kd', true);
            $tgl = $this->input->post('txtmaterialreq_tgl', true);
            $kode_srj = $this->input->post('txtsrj_kd', true);
            $batch = $this->input->post('txtmaterialreq_batch', true);

            $dataMaster = [
                'bagian_kd' => $bagian_kd,
                'materialreceipt_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin')
            ];
            if (empty($materialreceipt_kd)) {
                /** Add */
                $materialreceipt_kd = $this->tm_materialreceipt->create_code();
                $dataMaster = array_merge($dataMaster, [
                    'materialreceipt_kd' => $materialreceipt_kd,
                    'materialreceipt_no' => $this->tm_materialreceipt->generate_no(),
                    'materialreceipt_tanggal' => $tgl,
                    'rmgr_code_srj' => $kode_srj,
                    'batch' => $batch,
                    'wfstate_kd' => $this->td_workflow_state->getInitialState($this->wf_kd),
                    'materialreceipt_originator' => $this->session->userdata('kd_admin'),
                    'materialreceipt_tglinput' => date('Y-m-d H:i:s', strtotime( $tgl." ".date('H:i:s') )),
                ]);
                try {
                    $this->tm_materialreceipt->insert_data($dataMaster);
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['id' => $materialreceipt_kd]);
                } catch (Exception $e) {
                    $resp = array('code' => 400, 'status' => 'Gagal;', 'pesan' => $e->getMessage());
                }
            } else {
                /** Edit */
                $dataMaster = array_merge($dataMaster, [
                    'rmgr_code_srj' => $kode_srj,
                    'batch' => $batch,
                    'materialreceipt_tanggal' => $tgl,
                    'materialreceipt_tglinput' => date('Y-m-d H:i:s', strtotime( $tgl." ".date('H:i:s') ))
                ]);

                $datarmo = array(
                    'rmout_tglinput' => date('Y-m-d H:i:s', strtotime( $tgl." ".date('H:i:s') ))
                );

                
                try {
                    $this->tm_materialreceipt->update_data(['materialreceipt_kd' => $materialreceipt_kd], $dataMaster);

                    $materialReceiptDetail = $this->td_materialreceipt_detail_rawmaterial->get_by_param(['materialreceipt_kd' => $materialreceipt_kd])->row_array();
                    
                    if(!empty($materialReceiptDetail)){
                        $this->td_rawmaterial_out->update_data(['materialreceiptdetailrm_kd' => $materialReceiptDetail['materialreceiptdetailrm_kd']], $datarmo);
                    }

                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terupdate', 'data' => ['id' => $materialreceipt_kd]);
                } catch (Exception $e) {
                    $resp = array('code' => 400, 'status' => 'Gagal;', 'pesan' => $e->getMessage());
                }
            }

            /** Detail Material Requisition */
            if (!empty($materialreq_kds)) {
                $materialReceiptReq = $this->tb_materialreceipt_detail_requisition->get_by_param(['materialreceipt_kd' => $materialreceipt_kd])->result_array();
                $materialreq_kdExist = array_column($materialReceiptReq, 'materialreq_kd');

                $arrInsertDetails = array_values(array_diff($materialreq_kds, $materialreq_kdExist));
                $arrDeleteDetails = array_values(array_diff($materialreq_kdExist, $materialreq_kds));
                if (!empty($arrInsertDetails)) {
                    for ($i = 0; $i < count($arrInsertDetails); $i++) {
                        $dataDetailInsert[] = [
                            'materialreceipt_kd' => $materialreceipt_kd,
                            'materialreq_kd' => $arrInsertDetails[$i],
                            'materialreceiptreq_tglinput' => date('Y-m-d H:i:s', strtotime( $tgl." ".date('H:i:s') )),
                            'admin_kd' => $this->session->userdata('kd_admin')
                        ];
                    }
                    try {
                        $this->tb_materialreceipt_detail_requisition->insert_batch($dataDetailInsert);
                    } catch (Exception $e) {
                        $resp = array('code' => 400, 'status' => 'Gagal;', 'pesan' => $e->getMessage());
                    }
                }
                if (!empty($arrDeleteDetails)) {
                    try {
                        $this->db->where_in('materialreq_kd', $arrDeleteDetails)
                            ->delete('tb_materialreceipt_detail_requisition');
                    } catch (Exception $e) {
                        $resp = array('code' => 400, 'status' => 'Gagal;', 'pesan' => $e->getMessage());
                    }
                }
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_insert_detail_batch()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtmaterialreceipt_kd', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrmaterialreceipt_kd' => (!empty(form_error('txtmaterialreceipt_kd'))) ? buildLabel('warning', form_error('txtmaterialreceipt_kd', '"', '"')) : '',
            );
        } else {
            $materialreceipt_kd = $this->input->post('txtmaterialreceipt_kd', true);
            $materialreqdetail_kds = $this->input->post('txtmaterialreqdetail_kds', true);
            $materialreceiptreq_kds = $this->input->post('txtmaterialreceiptreq_kds', true);
            $woitem_kds = $this->input->post('txtwoitem_kds', true);
            $txtsrj_kd = $this->input->post('txtsrj_kd', true);
            $batch = $this->input->post('txtbatch', true);
            $materialreceiptdetail_qty = $this->input->post('txtmaterialreceiptdetail_qty', true);
            $materialreceiptdetail_remarks = $this->input->post('txtmaterialreceiptdetail_remarks', true);
            $materialreq_kds = $this->input->post('txtmaterialreq_kds', true);
            
            /** Get tanggal MRC untuk di input ke field tglinput yang berelasi dengan master MRC */
            $masterMRC = $this->tm_materialreceipt->get_by_param(['materialreceipt_kd' => $materialreceipt_kd])->row_array();

            if (!empty($materialreqdetail_kds)) {
                $arrayMaterialreq_kd = [];
                $materialreceiptdetail_kd = $this->td_materialreceipt_detail->create_code();
                for ($i = 0; $i < count($materialreqdetail_kds); $i++) {
                    $arrayMaterialreq_kd[] = $materialreq_kds[$materialreqdetail_kds[$i]];
                    $data[] = [
                        'materialreceiptdetail_kd' => $materialreceiptdetail_kd,
                        'materialreceipt_kd' => $materialreceipt_kd,
                        'materialreceiptreq_kd' => $materialreceiptreq_kds[$materialreqdetail_kds[$i]],
                        'materialreqdetail_kd' => $materialreqdetail_kds[$i],
                        'woitem_kd' => $woitem_kds[$materialreqdetail_kds[$i]],
                        'rmgr_code_srj' => $txtsrj_kd[$materialreqdetail_kds[$i]],
                        'batch' => $batch[$materialreqdetail_kds[$i]],
                        'materialreceiptdetail_qty' => $materialreceiptdetail_qty[$materialreqdetail_kds[$i]],
                        'materialreceiptdetail_remark' => !empty($materialreceiptdetail_remarks[$materialreqdetail_kds[$i]]) ? $materialreceiptdetail_remarks[$materialreqdetail_kds[$i]] : null,
                        'materialreceiptdetail_tglinput' => $masterMRC['materialreceipt_tglinput'],
                        'materialreceiptdetail_tgledit' => date('Y-m-d H:i:s'),
                        'admin_kd' => $this->session->userdata('kd_admin')
                    ];
                    $materialreceiptdetail_kd++;
                }
                try {
                    $this->td_materialreceipt_detail->insert_batch_data($data);
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
                } catch (Exception $e) {
                    $resp = array('code' => 400, 'status' => 'Gagal;', 'pesan' => $e->getMessage());
                }
            } else {
                $resp = array('code' => 400, 'status' => 'Gagal;', 'pesan' => 'Data Kosong');
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_insert_detail_rawmaterial()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtmaterialreceiptdetail_kd', 'WO', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtrm_kd', 'Raw Mtaerial', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtmaterialreceiptdetailrm_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtmaterialreceiptdetailrm_qtykonversi', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrmaterialreceiptdetail_kd' => (!empty(form_error('txtmaterialreceiptdetail_kd'))) ? buildLabel('warning', form_error('txtmaterialreceiptdetail_kd', '"', '"')) : '',
                'idErrrm_kd' => (!empty(form_error('txtrm_kd'))) ? buildLabel('warning', form_error('txtrm_kd', '"', '"')) : '',
                'idErrmaterialreceiptdetailrm_qty' => (!empty(form_error('txtmaterialreceiptdetailrm_qty'))) ? buildLabel('warning', form_error('txtmaterialreceiptdetailrm_qty', '"', '"')) : '',
                'idErrmaterialreceiptdetailrm_qtykonversi' => (!empty(form_error('txtmaterialreceiptdetailrm_qtykonversi'))) ? buildLabel('warning', form_error('txtmaterialreceiptdetailrm_qtykonversi', '"', '"')) : '',
            );
        } else {
            $materialreceipt_kd = $this->input->post('txtmaterialreceipt_kd');
            $materialreceiptdetail_kd = $this->input->post('txtmaterialreceiptdetail_kd');
            $rm_kd = $this->input->post('txtrm_kd');
            $materialreceiptdetailrm_qty = $this->input->post('txtmaterialreceiptdetailrm_qty');
            $materialreceiptdetailrm_satuan = $this->input->post('txtmaterialreceiptdetailrm_satuan');
            $materialreceiptdetailrm_qtykonversi = $this->input->post('txtmaterialreceiptdetailrm_qtykonversi');
            $materialreceiptdetailrm_satuankonversi = $this->input->post('txtmaterialreceiptdetailrm_satuankonversi');
            $materialreceiptdetailrm_konversi = $this->input->post('txtmaterialreceiptdetailrm_konversi');
            $materialreceiptdetailrm_nama = $this->input->post('txtmaterialreceiptdetailrm_nama');
            $materialreceiptdetailrm_deskripsi = $this->input->post('txtmaterialreceiptdetailrm_deskripsi');
            $materialreceiptdetailrm_spesifikasi = $this->input->post('txtmaterialreceiptdetailrm_spesifikasi');
            $materialreceiptdetailrm_kodesrj = $this->input->post('txtmaterialreceiptdetailrm_kodesrj');
            $materialreceiptdetailrm_batch = $this->input->post('txtmaterialreceiptdetailrm_batch');


            /** Get tanggal MRC untuk di input ke field tglinput yang berelasi dengan master MRC */
            $masterMRC = $this->tm_materialreceipt->get_by_param(['materialreceipt_kd' => $materialreceipt_kd])->row_array();

            $materialreceiptdetailrm_kd = $this->td_materialreceipt_detail_rawmaterial->create_code();
            $data = [
                'materialreceiptdetailrm_kd' => $materialreceiptdetailrm_kd,
                'materialreceipt_kd' => $materialreceipt_kd,
                'materialreceiptdetail_kd' => $materialreceiptdetail_kd,
                'rm_kd' => $rm_kd,
                'materialreceiptdetailrm_nama' => !empty($materialreceiptdetailrm_nama) ? $materialreceiptdetailrm_nama : null,
                'materialreceiptdetailrm_deskripsi' => !empty($materialreceiptdetailrm_deskripsi) ? $materialreceiptdetailrm_deskripsi : null,
                'materialreceiptdetailrm_spesifikasi' => !empty($materialreceiptdetailrm_spesifikasi) ? $materialreceiptdetailrm_spesifikasi : null,
                'materialreceiptdetailrm_tanggal' => date('Y-m-d'),
                'materialreceiptdetailrm_qty' => $materialreceiptdetailrm_qty,
                'materialreceiptdetailrm_satuan' => $materialreceiptdetailrm_satuan,
                'materialreceiptdetailrm_qtykonversi' => $materialreceiptdetailrm_qtykonversi,
                'materialreceiptdetailrm_satuankonversi' => $materialreceiptdetailrm_satuankonversi,
                'materialreceiptdetailrm_konversi' => $materialreceiptdetailrm_konversi,
                'rmgr_code_srj' => $materialreceiptdetailrm_kodesrj,
                'batch' => $materialreceiptdetailrm_batch,
                'materialreceiptdetailrm_tglinput' => $masterMRC['materialreceipt_tglinput'],
                'materialreceiptdetailrm_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin')
            ];
            try {
                $this->db->trans_begin();
                $this->td_materialreceipt_detail_rawmaterial->insert_data($data);
                /** RM Out */
                $rmstokmaster_kds = $this->td_rawmaterial_stok_master->get_rmstokmasterkd_batch([['rm_kd' => $rm_kd]]);
                $rmstokmaster_kd = $rmstokmaster_kds[0]['rmstokmaster_kd'];
                $dataOut = [
                    'rmout_kd' => $this->td_rawmaterial_out->create_code(),
                    'rmstokmaster_kd' => $rmstokmaster_kd,
                    'rm_kd' => $rm_kd,
                    'materialreceiptdetailrm_kd' => $materialreceiptdetailrm_kd,
                    'rmout_qty' => $materialreceiptdetailrm_qtykonversi,
                    'rmout_tglinput' => $masterMRC['materialreceipt_tglinput'],
                    'rmout_tgledit' => date('Y-m-d H:i:s'),
                    'admin_kd' => $this->session->userdata('kd_admin'),
                ];
                $this->td_rawmaterial_out->insert_data($dataOut);
                if ($rm_kd != 'SPECIAL') {
                    /** Update Stock On Hand */
                    $this->td_rawmaterial_stok_master->update_stock_batch('OUT', [['rmstokmaster_kd' => $rmstokmaster_kd, 'qty' => $materialreceiptdetailrm_qtykonversi]]);
                }

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
                } else {
                    $this->db->trans_commit();
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
                }
            } catch (Exception $e) {
                $resp = array('code' => 400, 'status' => 'Gagal;', 'pesan' => $e->getMessage());
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    public function action_insert_detail_rawmaterial_batch()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtmaterialreceipt_kd', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrmaterialreceipt_kd' => (!empty(form_error('txtmaterialreceipt_kd'))) ? buildLabel('warning', form_error('txtmaterialreceipt_kd', '"', '"')) : '',
            );
        } else {
            $materialreceipt_kd = $this->input->post('txtmaterialreceipt_kd', true);
            $rm_kds = $this->input->post('txtchkrmkds', true);
            $materialreceiptdetailrm_namas = $this->input->post('txtmaterialreceiptdetailrm_namaBatch', true);
            $materialreceiptdetailrm_deskripsis = $this->input->post('txtmaterialreceiptdetailrm_deskripsiBatch', true);
            $materialreceiptdetailrm_spesifikasis = $this->input->post('txtmaterialreceiptdetailrm_spesifikasiBatch', true);
            $materialreceiptdetailrm_satuans = $this->input->post('txtmaterialreceiptdetailrm_satuanBatch', true);
            $materialreceiptdetailrm_satuankonversis = $this->input->post('txtmaterialreceiptdetailrm_satuankonversiBatch', true);
            $materialreceiptdetailrm_qtykonversis = $this->input->post('txtmaterialreceiptdetailrm_qtykonversiBatch', true);
            $materialreceiptdetailrm_qtys = $this->input->post('txtmaterialreceiptdetailrm_qtyBatch', true);
            $materialreceiptdetailrm_konversis = $this->input->post('txtmaterialreceiptdetailrm_konversiBatch', true);
            $materialreceiptdetailrm_kodesrjs = $this->input->post('txtmaterialreceiptdetailrm_kodesrj', true);
            $materialreceiptdetailrm_batchs = $this->input->post('txtmaterialreceiptdetailrm_batch', true);

            /** Get tanggal MRC untuk di input ke field tglinput yang berelasi dengan master MRC */
            $masterMRC = $this->tm_materialreceipt->get_by_param(['materialreceipt_kd' => $materialreceipt_kd])->row_array();

            $stokmasterrm_kds = [];
            foreach ($rm_kds as $rm_kd0) {
                $stokmasterrm_kds[] = [
                    'rm_kd' => $rm_kd0
                ];
            }

            if (!empty($rm_kds)) {
                $rmstokMasters = $this->td_rawmaterial_stok_master->get_rmstokmasterkd_batch($stokmasterrm_kds);
                $materialreceiptdetailrm_kd = $this->td_materialreceipt_detail_rawmaterial->create_code();
                $rmout_kd = $this->td_rawmaterial_out->create_code();
                for ($i = 0; $i < count($rm_kds); $i++) {
                    if ($materialreceiptdetailrm_qtykonversis[$rm_kds[$i]] == 0) {
                        continue;
                    }
                    $data[] = [
                        'materialreceiptdetailrm_kd' => $materialreceiptdetailrm_kd,
                        'materialreceipt_kd' => $materialreceipt_kd,
                        'rm_kd' => $rm_kds[$i],
                        'materialreceiptdetailrm_nama' => !empty($materialreceiptdetailrm_namas[$rm_kds[$i]]) ? $materialreceiptdetailrm_namas[$rm_kds[$i]] : null,
                        'materialreceiptdetailrm_deskripsi' => !empty($materialreceiptdetailrm_deskripsis[$rm_kds[$i]]) ? urldecode($materialreceiptdetailrm_deskripsis[$rm_kds[$i]]) : null,
                        'materialreceiptdetailrm_spesifikasi' => !empty($materialreceiptdetailrm_spesifikasis[$rm_kds[$i]]) ? urldecode($materialreceiptdetailrm_spesifikasis[$rm_kds[$i]]) : null,
                        'materialreceiptdetailrm_tanggal' => date('Y-m-d'),
                        'materialreceiptdetailrm_qty' => $materialreceiptdetailrm_qtys[$rm_kds[$i]],
                        'materialreceiptdetailrm_satuan' => $materialreceiptdetailrm_satuans[$rm_kds[$i]],
                        'materialreceiptdetailrm_qtykonversi' => $materialreceiptdetailrm_qtykonversis[$rm_kds[$i]],
                        'materialreceiptdetailrm_satuankonversi' => $materialreceiptdetailrm_satuankonversis[$rm_kds[$i]],
                        'rmgr_code_srj' => $materialreceiptdetailrm_kodesrjs[$rm_kds[$i]],
                        'batch' => $materialreceiptdetailrm_batchs[$rm_kds[$i]],
                        'materialreceiptdetailrm_konversi' => $materialreceiptdetailrm_konversis[$rm_kds[$i]],
                        'materialreceiptdetailrm_tglinput' => $masterMRC['materialreceipt_tglinput'],
                        'materialreceiptdetailrm_tgledit' => date('Y-m-d H:i:s'),
                        'admin_kd' => $this->session->userdata('kd_admin')
                    ];

                    /** Out log rm */
                    foreach ($rmstokMasters as $rmstokMaster0) {
                        if ($rmstokMaster0['rm_kd'] == $rm_kds[$i]) {
                            $rmstokmaster_kd = $rmstokMaster0['rmstokmaster_kd'];
                        }
                    }
                    $dataOuts[] = [
                        'rmout_kd' => $rmout_kd,
                        'rmstokmaster_kd' => $rmstokmaster_kd,
                        'rm_kd' => $rm_kds[$i],
                        'materialreceiptdetailrm_kd' => $materialreceiptdetailrm_kd,
                        'rmout_qty' => $materialreceiptdetailrm_qtykonversis[$rm_kds[$i]],
                        'rmout_tglinput' => $masterMRC['materialreceipt_tglinput'],
                        'rmout_tgledit' => date('Y-m-d H:i:s'),
                        'admin_kd' => $this->session->userdata('kd_admin'),
                    ];

                    $materialreceiptdetailrm_kd++;
                    $rmout_kd++;
                }
                try {
                    if (!empty($data)) {
                        $this->td_materialreceipt_detail_rawmaterial->insert_batch_data($data);
                        foreach ($rmstokMasters as $rmstokMaster) {
                            $rmstokmaster_kds[] = [
                                'rmstokmaster_kd' => $rmstokMaster['rmstokmaster_kd'],
                                'qty' => $rmstokMaster['rmstokmaster_qty']
                            ];
                        }
                        // Update Out batch
                        $this->td_rawmaterial_stok_master->update_stock_batch('OUT', $rmstokmaster_kds);
                        $this->td_rawmaterial_out->insert_batch($dataOuts);
                        $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
                    } else {
                        $resp = array('code' => 400, 'status' => 'Gagal;', 'pesan' => 'Data Kosong');
                    }
                } catch (Exception $e) {
                    $resp = array('code' => 400, 'status' => 'Gagal;', 'pesan' => $e->getMessage());
                }
            } else {
                $resp = array('code' => 400, 'status' => 'Gagal;', 'pesan' => 'Data Kosong');
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    /** spreat By WO */
    public function action_insert_detail_rawmaterial_batch_v2()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->load->library(['form_validation']);
        $this->form_validation->set_rules('txtmaterialreceipt_kd', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);

        if ($this->form_validation->run() == FALSE) {
            $resp['code'] = 401;
            $resp['status'] = 'Required';
            $resp['pesan'] = array(
                'idErrmaterialreceipt_kd' => (!empty(form_error('txtmaterialreceipt_kd'))) ? buildLabel('warning', form_error('txtmaterialreceipt_kd', '"', '"')) : '',
            );
        } else {
            $materialreceipt_kd = $this->input->post('txtmaterialreceipt_kd', true);
            $rm_kds = $this->input->post('txtchkrmkds', true);
            $materialreceiptdetailrm_namas = $this->input->post('txtmaterialreceiptdetailrm_namaBatch', true);
            $materialreceiptdetailrm_deskripsis = $this->input->post('txtmaterialreceiptdetailrm_deskripsiBatch', true);
            $materialreceiptdetailrm_spesifikasis = $this->input->post('txtmaterialreceiptdetailrm_spesifikasiBatch', true);
            $materialreceiptdetailrm_satuans = $this->input->post('txtmaterialreceiptdetailrm_satuanBatch', true);
            $materialreceiptdetailrm_satuankonversis = $this->input->post('txtmaterialreceiptdetailrm_satuankonversiBatch', true);
            $materialreceiptdetailrm_qtykonversis = $this->input->post('txtmaterialreceiptdetailrm_qtykonversiBatch', true);
            $materialreceiptdetailrm_qtys = $this->input->post('txtmaterialreceiptdetailrm_qtyBatch', true);
            $materialreceiptdetailrm_konversis = $this->input->post('txtmaterialreceiptdetailrm_konversiBatch', true);
            $materialreceiptdetailrm_kodesrjs = $this->input->post('txtmaterialreceiptdetailrm_kodesrj', true);
            $materialreceiptdetailrm_batchs = $this->input->post('txtmaterialreceiptdetailrm_batch', true);

            /** Get tanggal MRC untuk di input ke field tglinput yang berelasi dengan master MRC */
            $masterMRC = $this->tm_materialreceipt->get_by_param(['materialreceipt_kd' => $materialreceipt_kd])->row_array();

            $stokmasterrm_kds = [];
            $arrCekRmDupp = [];
            foreach ($rm_kds as $rm_kd0) {
                $expRmkd = explode('-', $rm_kd0);
                if (!in_array($expRmkd[1], $arrCekRmDupp)) {
                    $stokmasterrm_kds[] = [
                        'rm_kd' => $expRmkd[1]
                    ];
                    $arrCekRmDupp[] = $expRmkd[1];
                }
            }

            if (!empty($rm_kds)) {
                $rmstokMasters = $this->td_rawmaterial_stok_master->get_rmstokmasterkd_batch($stokmasterrm_kds);
                $materialreceiptdetailrm_kd = $this->td_materialreceipt_detail_rawmaterial->create_code();
                $rmout_kd = $this->td_rawmaterial_out->create_code();
                $arrStokmaster = [];
                for ($i = 0; $i < count($rm_kds); $i++) {
                    /** Explode [materialreceiptdetail_kd][rm_kd]  */
                    $expRmkd = explode('-', $rm_kds[$i]);

                    if ($materialreceiptdetailrm_qtykonversis[$rm_kds[$i]] == 0) {
                        continue;
                    }
                    $data[] = [
                        'materialreceiptdetailrm_kd' => $materialreceiptdetailrm_kd,
                        'materialreceipt_kd' => $materialreceipt_kd,
                        'materialreceiptdetail_kd' => $expRmkd[0],
                        'rm_kd' => $expRmkd[1],
                        'materialreceiptdetailrm_nama' => !empty($materialreceiptdetailrm_namas[$rm_kds[$i]]) ? $materialreceiptdetailrm_namas[$rm_kds[$i]] : null,
                        'materialreceiptdetailrm_deskripsi' => !empty($materialreceiptdetailrm_deskripsis[$rm_kds[$i]]) ? urldecode($materialreceiptdetailrm_deskripsis[$rm_kds[$i]]) : null,
                        'materialreceiptdetailrm_spesifikasi' => !empty($materialreceiptdetailrm_spesifikasis[$rm_kds[$i]]) ? urldecode($materialreceiptdetailrm_spesifikasis[$rm_kds[$i]]) : null,
                        'materialreceiptdetailrm_tanggal' => date('Y-m-d'),
                        'materialreceiptdetailrm_qty' => $materialreceiptdetailrm_qtys[$rm_kds[$i]],
                        'materialreceiptdetailrm_satuan' => $materialreceiptdetailrm_satuans[$rm_kds[$i]],
                        'materialreceiptdetailrm_qtykonversi' => $materialreceiptdetailrm_qtykonversis[$rm_kds[$i]],
                        'materialreceiptdetailrm_satuankonversi' => $materialreceiptdetailrm_satuankonversis[$rm_kds[$i]],
                        'materialreceiptdetailrm_konversi' => $materialreceiptdetailrm_konversis[$rm_kds[$i]],
                        'rmgr_code_srj' => $materialreceiptdetailrm_kodesrjs[$rm_kds[$i]],
                        'batch' => $materialreceiptdetailrm_batchs[$rm_kds[$i]],
                        'materialreceiptdetailrm_tglinput' => $masterMRC['materialreceipt_tglinput'],
                        'materialreceiptdetailrm_tgledit' => date('Y-m-d H:i:s'),
                        'admin_kd' => $this->session->userdata('kd_admin')
                    ];

                    /** Out log rm */
                    foreach ($rmstokMasters as $rmstokMaster0) {
                        if ($rmstokMaster0['rm_kd'] == $expRmkd[1]) {
                            $rmstokmaster_kd = $rmstokMaster0['rmstokmaster_kd'];
                        }
                    }
                    /** For updta stok master */
                    $arrStokmaster[] = [
                        'rmstokmaster_kd' => $rmstokmaster_kd,
                        'qty' => $materialreceiptdetailrm_qtykonversis[$rm_kds[$i]]
                    ];
                        
                    $dataOuts[] = [
                        'rmout_kd' => $rmout_kd,
                        'rmstokmaster_kd' => $rmstokmaster_kd,
                        'rm_kd' => $expRmkd[1],
                        'materialreceiptdetailrm_kd' => $materialreceiptdetailrm_kd,
                        'rmout_qty' => $materialreceiptdetailrm_qtykonversis[$rm_kds[$i]],
                        'rmout_tglinput' => $masterMRC['materialreceipt_tglinput'],
                        'rmout_tgledit' => date('Y-m-d H:i:s'),
                        'admin_kd' => $this->session->userdata('kd_admin'),
                    ];

                    $materialreceiptdetailrm_kd++;
                    $rmout_kd++;
                }

                try {
                    if (!empty($data)) {
                        // echo json_encode($arrStokmaster);die();
                        // foreach ($rmstokMasters as $rmstokMaster) {
                        //     $rmstokmaster_kds[] = [
                        //         'rmstokmaster_kd' => $rmstokMaster['rmstokmaster_kd'],
                        //         'qty' => $rmstokMaster['rmstokmaster_qty']
                        //     ];
                        // }
                        $this->td_materialreceipt_detail_rawmaterial->insert_batch_data($data);
                        // Update Out batch
                        $this->td_rawmaterial_stok_master->update_stock_batch('OUT', $arrStokmaster);
                        $this->td_rawmaterial_out->insert_batch($dataOuts);
                        $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
                    } else {
                        $resp = array('code' => 400, 'status' => 'Gagal;', 'pesan' => 'Data Kosong');
                    }
                } catch (Exception $e) {
                    $resp = array('code' => 400, 'status' => 'Gagal;', 'pesan' => $e->getMessage());
                }
            } else {
                $resp = array('code' => 400, 'status' => 'Gagal;', 'pesan' => 'Data Kosong');
            }
        }
        $resp['csrf'] = $this->security->get_csrf_hash();
        echo json_encode($resp);
    }

    function action_delete_detail()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $materialreceiptdetail_kd = $this->input->get('id');
        if (!empty($materialreceiptdetail_kd)) {
            $materialReceiptDtl = $this->db->select('tb_materialreceipt_detail_requisition.materialreq_kd')
                ->where('td_materialreceipt_detail.materialreceiptdetail_kd', $materialreceiptdetail_kd)
                ->join('tb_materialreceipt_detail_requisition', 'tb_materialreceipt_detail_requisition.materialreceiptreq_kd=td_materialreceipt_detail.materialreceiptreq_kd')
                ->get('td_materialreceipt_detail')->row_array();
            try {
                $actDel = $this->td_materialreceipt_detail->delete_data($materialreceiptdetail_kd);
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
            } catch (Exception $e) {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal');
            }
        } else {
            $resp = array('code' => 400, 'pesan' => 'Id tidak ditemukan');
        }
        echo json_encode($resp);
    }

    function action_delete_detail_batch()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $materialreceiptdetail_kds = $this->input->get('txtmaterialreceiptdetail_kds');
        if (!empty($materialreceiptdetail_kds)) {
            try {
                $this->db->where_in('materialreceiptdetail_kd', $materialreceiptdetail_kds)
                    ->delete('td_materialreceipt_detail');
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
            } catch (Exception $e) {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal');
            }
        } else {
            $resp = array('code' => 400, 'pesan' => 'Id tidak ditemukan');
        }
        echo json_encode($resp);
    }

    public function action_delete_detail_rawmaterial()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $materialreceiptdetailrm_kd = $this->input->get('id');
        if (!empty($materialreceiptdetailrm_kd)) {
            try {
                /** u/ update stok */
                $rowOUT = $this->td_rawmaterial_out->get_by_param(['materialreceiptdetailrm_kd' => $materialreceiptdetailrm_kd])->row_array();

                $this->db->trans_start();

                $this->td_rawmaterial_stok_master->update_stok($rowOUT['rmstokmaster_kd'], 'IN', $rowOUT['rmout_qty']);
                $this->td_rawmaterial_out->delete_data($rowOUT['rmout_kd']);
                $this->td_materialreceipt_detail_rawmaterial->delete_data($materialreceiptdetailrm_kd);

                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE) {
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
                } else {
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
                }
            } catch (Exception $e) {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal');
            }
        } else {
            $resp = array('code' => 400, 'pesan' => 'Id tidak ditemukan');
        }
        echo json_encode($resp);
    }

    public function action_recalculate_detail_rawmaterial()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $materialreceipt_kd = $this->input->get('id');
        if (!empty($materialreceipt_kd)) {
            try {
                $detailrms = $this->td_materialreceipt_detail_rawmaterial->get_by_param(['materialreceipt_kd' => $materialreceipt_kd])->result_array();

                $this->db->trans_start();
                foreach ($detailrms as $detailrm) {
                    /** u/ update stok */
                    $rowOUT = $this->td_rawmaterial_out->get_by_param(['materialreceiptdetailrm_kd' => $detailrm['materialreceiptdetailrm_kd']])->row_array();


                    $this->td_rawmaterial_stok_master->update_stok($rowOUT['rmstokmaster_kd'], 'IN', $rowOUT['rmout_qty']);
                    $this->td_rawmaterial_out->delete_data($rowOUT['rmout_kd']);
                    $this->td_materialreceipt_detail_rawmaterial->delete_data($detailrm['materialreceiptdetailrm_kd']);
                }

                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE) {
                    $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
                } else {
                    $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
                }
            } catch (Exception $e) {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal');
            }
        } else {
            $resp = array('code' => 400, 'pesan' => 'Id tidak ditemukan');
        }
        echo json_encode($resp);
    }

    function action_delete_master()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $materialreceipt_kd = $this->input->get('id');
        if (!empty($materialreceipt_kd)) {
            try {
                $actDetail = $this->td_materialreceipt_detail->delete_by_param(['materialreceipt_kd' => $materialreceipt_kd]);
                if ($actDetail) {
                    $this->tm_materialreceipt->delete_data($materialreceipt_kd);
                }
                $resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
            } catch (Exception $e) {
                $resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal');
            }
        } else {
            $resp = array('code' => 400, 'pesan' => 'Id tidak ditemukan');
        }
        echo json_encode($resp);
    }
}
