<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Delivery_order extends MY_Controller {
	private $class_link = 'inventory/raw_material/delivery_order';
	private $wf_kd = 1;

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
        $this->load->model(['tm_whdeliveryorder', 'td_whdeliveryorder_detail', 'td_rawmaterial_stok_master', 'td_rawmaterial_out', 'td_rawmaterial_goodsreceive', 
			'td_workflow_state', 'tm_workflow', 'td_workflow_transition', 'td_whdeliveryorder_transition_log', 'td_workflow_transition_notification', 'db_hrm/Tb_karyawan']);
	}

	public function index() {
        parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
		parent::datetimepicker_assets();
		$this->table_box();
	}
	
	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$date = $this->input->get('date', true);

		$data['class_link'] = $this->class_link;
		$data['date'] = $date;

		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data (){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->tm_whdeliveryorder->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

    public function formmaster_box(){
        $sts = $this->input->get('sts', true);
        $id = $this->input->get('id', true);

        $data['sts'] = $sts;
        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/formmaster_box', $data);
	}

    public function formmaster_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts', true);
		$id = $this->input->get('id', true);

		if (!empty($id)){
			$data['row'] = $this->db->where('tm_whdeliveryorder.whdeliveryorder_kd', $id)
					->join('tm_suplier', 'tm_suplier.suplier_kd=tm_whdeliveryorder.suplier_kd', 'left')
					->get('tm_whdeliveryorder')->row_array();
			$data['suplierSelected'] = ['id' => $data['row']['suplier_kd'], 'text' => $data['row']['suplier_nama']];
			$karyawan = $this->Tb_karyawan->get_by_param(['kd_karyawan' => $data['row']['kd_karyawan']])->row_array();
			$data['karyawanSelected'] = ['id' => $karyawan['kd_karyawan'], 'text' => $karyawan['nik_karyawan'].' | '.$karyawan['nm_karyawan']];
		}
		
		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/formmaster_main', $data);
	}

    public function form_box(){
        parent::administrator();
		parent::pnotify_assets();
        parent::select2_assets();
        parent::icheck_assets();
        $sts = $this->input->get('sts', true);
        $id = $this->input->get('id', true);

        $data['master'] = $this->db->select()->from('tm_whdeliveryorder')
					->join('td_workflow_state', 'tm_whdeliveryorder.wfstate_kd=td_workflow_state.wfstate_kd', 'left')
					->join('tm_suplier', 'tm_suplier.suplier_kd=tm_whdeliveryorder.suplier_kd', 'left')
					->where(['tm_whdeliveryorder.whdeliveryorder_kd' => $id])
					->get()->row_array();
		$karyawan = $this->Tb_karyawan->get_by_param(['kd_karyawan' => $data['master']['kd_karyawan']])->row_array();
		$data['master']['whdeliveryorder_to'] = $data['master']['suplier_nama'].$karyawan['nm_karyawan'];

        $data['sts'] = $sts;
        $data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

    public function form_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $sts = $this->input->get('sts', true);
        $id = $this->input->get('id', true);
        $whdeliveryorderdetial_kd = $this->input->get('whdeliveryorderdetial_kd', true);

        if (!empty($whdeliveryorderdetial_kd)) {
            $data['row'] = $this->db->select('td_whdeliveryorder_detail.*, td_rawmaterial_satuan.rmsatuan_nama, tm_rawmaterial.rm_kode')
                    ->from('td_whdeliveryorder_detail')
                    ->join('td_rawmaterial_satuan', 'td_whdeliveryorder_detail.whdeliveryorderdetail_satuankd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
                    ->join('tm_rawmaterial', 'tm_rawmaterial.rm_kd=td_whdeliveryorder_detail.rm_kd', 'left')
                    ->where('td_whdeliveryorder_detail.whdeliveryorderdetail_kd', $whdeliveryorderdetial_kd)
                    ->get()->row_array();
        }
		
		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

    public function formgr_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $sts = $this->input->get('sts', true);
        $id = $this->input->get('id', true);
		
		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/formgr_main', $data);
	}

    public function form_table(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $sts = $this->input->get('sts', true);
        $id = $this->input->get('id', true);

        $results = $this->db->select('td_whdeliveryorder_detail.*, td_rawmaterial_satuan.rmsatuan_nama, tm_rawmaterial.rm_kode, tm_rawmaterial.rm_oldkd')
                ->from('td_whdeliveryorder_detail')
                ->join('td_rawmaterial_satuan', 'td_rawmaterial_satuan.rmsatuan_kd=td_whdeliveryorder_detail.whdeliveryorderdetail_satuankd', 'left')
                ->join('tm_rawmaterial', 'tm_rawmaterial.rm_kd=td_whdeliveryorder_detail.rm_kd', 'left')
                ->where('td_whdeliveryorder_detail.whdeliveryorder_kd', $id)
                ->order_by('td_whdeliveryorder_detail.whdeliveryorderdetail_tgledit', 'desc')
                ->get()->result_array();
		
        $data['results'] = $results;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_table', $data);
	}

    public function formgr_table(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
        $po_kd = $this->input->get('id', true);

		$results = $this->db->select('td_rawmaterial_goodsreceive.*, td_purchaseorder_detail.*, tm_purchaseorder.po_no, tm_rawmaterial.rm_kode, tm_rawmaterial.rm_oldkd, td_rawmaterial_satuan.rmsatuan_nama')
				->from('td_rawmaterial_goodsreceive')
				->join('td_purchaseorder_detail', 'td_rawmaterial_goodsreceive.podetail_kd=td_purchaseorder_detail.podetail_kd', 'left')
				->join('tm_purchaseorder', 'tm_purchaseorder.po_kd=td_purchaseorder_detail.po_kd', 'left')
				->join('tm_rawmaterial', 'td_rawmaterial_goodsreceive.rm_kd=tm_rawmaterial.rm_kd', 'left')
				->join('td_rawmaterial_satuan', 'td_rawmaterial_satuan.rmsatuan_kd=td_purchaseorder_detail.rmsatuan_kd', 'left')
				->where('td_rawmaterial_goodsreceive.rmgr_type', 'RETURN')
				->where('td_rawmaterial_goodsreceive.rmgr_nosrj IS NULL')
				->where('td_rawmaterial_goodsreceive.po_kd', $po_kd)
				->get()->result_array();
		
        $data['results'] = $results;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/formgr_table', $data);
	}
	
	public function formubahstate_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id');
		$wftransition_kd = $this->input->get('wftransition_kd');
		
		$data['id'] = $id;
		$data['wftransition_kd'] = $wftransition_kd;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/formubahstate_main', $data);
	}
	
	public function view_box() {
		parent::administrator();
		parent::pnotify_assets();
		$id = $this->input->get('id', true);

		$data['master'] = $this->db->select()->from('tm_whdeliveryorder')
					->join('td_workflow_state', 'tm_whdeliveryorder.wfstate_kd=td_workflow_state.wfstate_kd', 'left')
					->join('tm_suplier', 'tm_suplier.suplier_kd=tm_whdeliveryorder.suplier_kd', 'left')
					->where(['tm_whdeliveryorder.whdeliveryorder_kd' => $id])
					->get()->row_array();
		$karyawan = $this->Tb_karyawan->get_by_param(['kd_karyawan' => $data['master']['kd_karyawan']])->row_array();
		$data['master']['whdeliveryorder_to'] = $data['master']['suplier_nama'].$karyawan['nm_karyawan'];
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$data['generate_button'] = $this->tm_workflow ->generate_button ($id, $this->wf_kd, $data['master']['wfstate_kd']);
		$this->load->view('page/'.$this->class_link.'/view_box', $data);
	}

	public function view_table() {
		$whdeliveryorder_kd = $this->input->get('id', true);
		
		$data['results'] = $this->db->select()	
				->from('td_whdeliveryorder_detail')
				->join('td_rawmaterial_satuan', 'td_whdeliveryorder_detail.whdeliveryorderdetail_satuankd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
				->where('td_whdeliveryorder_detail.whdeliveryorder_kd', $whdeliveryorder_kd)
				->get()->result_array();

		$this->load->view('page/'.$this->class_link.'/view_table', $data);
	}
	
	public function view_pdf() {
		$this->load->library('Pdf');
		$id = $this->input->get('id', true);

		$dataKonten['results'] = $this->db->select()	
				->from('td_whdeliveryorder_detail')
				->join('td_rawmaterial_satuan', 'td_whdeliveryorder_detail.whdeliveryorderdetail_satuankd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
				->where('td_whdeliveryorder_detail.whdeliveryorder_kd', $id)
				->get()->result_array();
		$data['konten'] = $this->load->view('page/'.$this->class_link.'/view_table', $dataKonten, true);
		$data['master'] = $this->db->select()->from('tm_whdeliveryorder')
					->join('td_workflow_state', 'tm_whdeliveryorder.wfstate_kd=td_workflow_state.wfstate_kd', 'left')
					->join('tm_suplier', 'tm_suplier.suplier_kd=tm_whdeliveryorder.suplier_kd', 'left')
					->where(['tm_whdeliveryorder.whdeliveryorder_kd' => $id])
					->get()->row_array();
		$karyawan = $this->Tb_karyawan->get_by_param(['kd_karyawan' => $data['master']['kd_karyawan']])->row_array();
		if(empty($data['master']['suplier_kd']) && empty($data['master']['kd_karyawan'])){
			$data['master']['whdeliveryorder_to'] = $data['master']['set_name'].'<br><medium>'.$data['master']['whdeliveryorder_note'].'</medium>';
		}else{
			$data['master']['whdeliveryorder_to'] = $data['master']['suplier_nama'].$karyawan['nm_karyawan'].'<br><medium>'.$data['master']['suplier_alamat'].$karyawan['alamat_jalan_1'].'</medium>';
		}
		//$data['master']['whdeliveryorder_to'] = $data['master']['suplier_nama'].$karyawan['nm_karyawan'].$data['master']['set_name'].'<br><small>'.$data['master']['suplier_alamat'].$karyawan['alamat_jalan_1'].$data['master']['whdeliveryorder_note'].'</small>';
		$data['logs'] = $this->td_whdeliveryorder_transition_log->get_log_printout($id)->result_array();
		
		$this->load->view('page/'.$this->class_link.'/view_pdf', $data);
	}
	
	public function view_log() {
		$id = $this->input->get('id', true);

		$data['result'] = $this->db->select('td_whdeliveryorder_transition_log.*, td_workflow_transition.*, tb_admin.nm_admin, state_source.wfstate_nama as state_source, state_dst.wfstate_nama as state_dst')->from('td_whdeliveryorder_transition_log')
				->join('td_workflow_transition', 'td_whdeliveryorder_transition_log.wftransition_kd=td_workflow_transition.wftransition_kd', 'left')
				->join('td_workflow_state as state_source', 'state_source.wfstate_kd=td_workflow_transition.wftransition_source', 'left')
				->join('td_workflow_state as state_dst', 'state_dst.wfstate_kd=td_workflow_transition.wftransition_destination', 'left')
				->join('tb_admin', 'td_whdeliveryorder_transition_log.admin_kd=tb_admin.kd_admin', 'left')
				->where('td_whdeliveryorder_transition_log.whdeliveryorder_kd', $id)
				->order_by('td_whdeliveryorder_transition_log.whdeliveryordertransitionlog_tglinput', 'desc')
				->get()->result_array();

		$this->load->view('page/'.$this->class_link.'/view_log', $data);
	}

	public function get_pogreturn() {
		$paramPO = $this->input->get('paramPO', true);

		$results = $this->db->select('td_rawmaterial_goodsreceive.po_kd, tm_purchaseorder.po_no, tm_suplier.suplier_nama')
				->from('td_rawmaterial_goodsreceive')
				->join('tm_purchaseorder', 'td_rawmaterial_goodsreceive.po_kd=tm_purchaseorder.po_kd', 'left')
				->join('tm_suplier', 'tm_purchaseorder.suplier_kd=tm_suplier.suplier_kd', 'left')
				->where('td_rawmaterial_goodsreceive.rmgr_type', 'RETURN')
				->where('td_rawmaterial_goodsreceive.rmgr_nosrj IS NULL')
				->group_start()
				->like('tm_purchaseorder.po_no', $paramPO, 'both')
				->or_like('tm_suplier.suplier_nama', $paramPO, 'both')
				->group_end()
				->group_by('td_rawmaterial_goodsreceive.po_kd')
				->get()->result_array();
		$arrayResult = [];
		foreach ($results as $result) {
			$arrayResult[] = [
				'id' => $result['po_kd'],
				'text' => $result['po_no'].' | '.$result['suplier_nama'],
			];

		}
		echo json_encode($arrayResult);
	}

	public function action_insert_master() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtwhdeliveryorder_tanggal', 'Tanggal Order', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtwhdeliveryorder_vehiclenumber', 'Vehicle Number', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrwhdeliveryorder_tanggal' => (!empty(form_error('txtwhdeliveryorder_tanggal')))?buildLabel('warning', form_error('txtwhdeliveryorder_tanggal', '"', '"')):'',
				'idErrwhdeliveryorder_vehiclenumber' => (!empty(form_error('txtwhdeliveryorder_vehiclenumber')))?buildLabel('warning', form_error('txtwhdeliveryorder_vehiclenumber', '"', '"')):'',
			);
			
		}else {
			$whdeliveryorder_kd = $this->input->post('txtid', true);
			$whdeliveryorder_tanggal = $this->input->post('txtwhdeliveryorder_tanggal', true);
			$kd_karyawan = $this->input->post('txtkd_karyawan', true);
			$suplier_kd = $this->input->post('txtsuplier_kd', true);
			$whdeliveryorder_vehiclenumber = $this->input->post('txtwhdeliveryorder_vehiclenumber', true);
			$set_name = $this->input->post('set_name', true);
			$whdeliveryorder_note = $this->input->post('txtwhdeliveryorder_note', true);
			$whdeliveryorder_sendback = $this->input->post('txtwhdeliveryorder_sendback', true);

			$data = [
                'whdeliveryorder_tanggal' => $whdeliveryorder_tanggal,
                'whdeliveryorder_vehiclenumber' => $whdeliveryorder_vehiclenumber,
                'kd_karyawan' => $kd_karyawan,
                'suplier_kd' => $suplier_kd,
                'wf_kd' => $this->wf_kd,
				'set_name' => $set_name,
                'whdeliveryorder_sendback' => $whdeliveryorder_sendback,
                'whdeliveryorder_note' => $whdeliveryorder_note,
                'whdeliveryorder_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            ];
            $act = false;
            if (empty($whdeliveryorder_kd)){
                #add
				#initial state
				$wfstate = $this->td_workflow_state->get_by_param(['wf_kd' => $this->wf_kd])->row_array();
                $data = array_merge($data,
                    [
                        'whdeliveryorder_kd' => $this->tm_whdeliveryorder->create_code(),
                        'whdeliveryorder_no' => $this->tm_whdeliveryorder->create_no(),
						'wfstate_kd' => $wfstate['wfstate_kd'],
                        'whdeliveryorder_tglinput' => date('Y-m-d H:i:s'),
                    ]);
                $act = $this->tm_whdeliveryorder->insert_data($data);
            }else{
                #update
                $act = $this->tm_whdeliveryorder->update_data (['whdeliveryorder_kd' => $whdeliveryorder_kd], $data);
				$data = array_merge($data, ['whdeliveryorder_kd' => $whdeliveryorder_kd]);
            }

            if ($act){
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => $data);
            }else{
                $resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
            }
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrm_kd', 'Tanggal Order', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtwhdeliveryorderdetail_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtwhdeliveryorderdetail_satuankd', 'Vehicle Number', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrrm_kd' => (!empty(form_error('txtrm_kd')))?buildLabel('warning', form_error('txtrm_kd', '"', '"')):'',
				'idErrwhdeliveryorderdetail_qty' => (!empty(form_error('txtwhdeliveryorderdetail_qty')))?buildLabel('warning', form_error('txtwhdeliveryorderdetail_qty', '"', '"')):'',
				'idErrwhdeliveryorderdetail_satuankd' => (!empty(form_error('txtwhdeliveryorderdetail_satuankd')))?buildLabel('warning', form_error('txtwhdeliveryorderdetail_satuankd', '"', '"')):'',
			);
			
		}else {
			$whdeliveryorderdetail_kd = $this->input->post('txtwhdeliveryorderdetail_kd', true);
			$whdeliveryorder_kd = $this->input->post('txtwhdeliveryorder_kd', true);
			$rm_kd = $this->input->post('txtrm_kd', true);
			$whdeliveryorderdetail_nama = $this->input->post('txtwhdeliveryorderdetail_nama', true);
			$whdeliveryorderdetail_deskripsi = $this->input->post('txtwhdeliveryorderdetail_deskripsi', true);
			$whdeliveryorderdetail_spesifikasi = $this->input->post('txtwhdeliveryorderdetail_spesifikasi', true);
			$whdeliveryorderdetail_qty = $this->input->post('txtwhdeliveryorderdetail_qty', true);
			$whdeliveryorderdetail_satuankd = $this->input->post('txtwhdeliveryorderdetail_satuankd', true);
			$whdeliveryorderdetail_remark = $this->input->post('txtwhdeliveryorderdetail_remark', true);
			$whdeliveryorderdetail_reducestok = $this->input->post('txtwhdeliveryorderdetail_reducestok', true);
			$whdeliveryorderdetail_konversi = $this->input->post('txtwhdeliveryorderdetail_konversi', true);

			if (empty($whdeliveryorderdetail_konversi)){
				$whdeliveryorderdetail_konversi = 1;
			}
			$whdeliveryorderdetail_qtykonversi = $whdeliveryorderdetail_qty * $whdeliveryorderdetail_konversi;
			$data = [
                'whdeliveryorder_kd' => $whdeliveryorder_kd,
                'rm_kd' => $rm_kd,
                'whdeliveryorderdetail_nama' => !empty($whdeliveryorderdetail_nama) ? $whdeliveryorderdetail_nama : null,
                'whdeliveryorderdetail_deskripsi' => !empty($whdeliveryorderdetail_deskripsi) ? $whdeliveryorderdetail_deskripsi : null,
                'whdeliveryorderdetail_spesifikasi' => !empty($whdeliveryorderdetail_spesifikasi) ? $whdeliveryorderdetail_spesifikasi : null,
                'whdeliveryorderdetail_satuankd' => $whdeliveryorderdetail_satuankd,
                'whdeliveryorderdetail_qty' => $whdeliveryorderdetail_qty,
                'whdeliveryorderdetail_qtykonversi' => $whdeliveryorderdetail_qtykonversi,
                'whdeliveryorderdetail_remark' => !empty($whdeliveryorderdetail_remark) ? $whdeliveryorderdetail_remark: null,
                'whdeliveryorderdetail_reducestok' => $whdeliveryorderdetail_reducestok,
                'whdeliveryorderdetail_tgledit' => date('Y-m-d H:i:s'),
                'admin_kd' => $this->session->userdata('kd_admin'),
            ];
            $act = false;
            if (empty($whdeliveryorderdetail_kd)){
                #add
				$whdeliveryorderdetail_kd = $this->td_whdeliveryorder_detail->create_code();
				
                $data = array_merge($data,
                    [
                        'whdeliveryorderdetail_kd' => $whdeliveryorderdetail_kd,
                        'whdeliveryorderdetail_tglinput' => date('Y-m-d H:i:s'),
                    ]);

				$this->db->trans_begin();
                $act = $this->td_whdeliveryorder_detail->insert_data($data);
				# jika pengurangan stok rm
				if (!empty($whdeliveryorderdetail_reducestok)) {
					$rmstokmasters = $this->td_rawmaterial_stok_master->get_rmstokmasterkd_batch([['rm_kd' => $rm_kd]]);
					foreach ($rmstokmasters as $rmstokmaster) {
						if ($rmstokmaster['rm_kd'] == $rm_kd){
							$rmstokmaster_kd = 	$rmstokmaster['rmstokmaster_kd'];
						}
					}
					$arrayOut = [
						'rmout_kd' => $this->td_rawmaterial_out->create_code(),
						'rmstokmaster_kd' => $rmstokmaster_kd,
						'rm_kd' => $rm_kd,
						'whdeliveryorderdetail_kd' => $whdeliveryorderdetail_kd,
						'rmout_qty' => $whdeliveryorderdetail_qtykonversi,
						'rmout_tglinput' => now_with_ms(),
						'admin_kd' => $this->session->userdata('kd_admin'),
					];
	
					$dataUpdateStokMaster[] = [
						'rmstokmaster_kd' => $rmstokmaster_kd,
						'qty' => $whdeliveryorderdetail_qtykonversi,
					];

					$actOut = $this->td_rawmaterial_out->insert_data($arrayOut);
					$actUpdateStokMaster = $this->td_rawmaterial_stok_master->update_stock_batch ('OUT', $dataUpdateStokMaster);
				}
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
				}else{
					$this->db->trans_commit();
					$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
				}
            }else{
                #update
                $act = $this->td_whdeliveryorder_detail->update_data (['whdeliveryorderdetail_kd' => $whdeliveryorderdetail_kd], $data);
            }
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_insertGR_batch () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$rmgr_kds = $this->input->post('rmgr_kds', true);
		$rmgr_kets = $this->input->post('txtrmgr_kets', true);
		$whdeliveryorder_kd = $this->input->post('txtwhdeliveryorder_kd', true);

		if (!empty($rmgr_kds)){
			$grs = $this->db->select()
					->from('td_rawmaterial_goodsreceive')
					->join('td_purchaseorder_detail', 'td_rawmaterial_goodsreceive.podetail_kd=td_purchaseorder_detail.podetail_kd', 'left')
					->where_in('td_rawmaterial_goodsreceive.rmgr_kd', $rmgr_kds)
					->get()->result_array();
			$whdeliveryorder = $this->tm_whdeliveryorder->get_by_param(['whdeliveryorder_kd' => $whdeliveryorder_kd])->row_array();
			
			$whdeliveryorderdetail_kd = $this->td_whdeliveryorder_detail->create_code();
			foreach ($grs as $gr) {
				$data[] = [
					'whdeliveryorderdetail_kd' => $whdeliveryorderdetail_kd,
					'rmgr_kd' => $gr['rmgr_kd'],
					'whdeliveryorder_kd' => $whdeliveryorder_kd,
					'rm_kd' => $gr['rm_kd'],
					'whdeliveryorderdetail_nama' => $gr['podetail_nama'],
					'whdeliveryorderdetail_deskripsi' => $gr['podetail_deskripsi'],
					'whdeliveryorderdetail_spesifikasi' => $gr['podetail_spesifikasi'],
					'whdeliveryorderdetail_satuankd' => $gr['rmsatuan_kd'],
					'whdeliveryorderdetail_qty' => abs($gr['rmgr_qty']),
					'whdeliveryorderdetail_qtykonversi' => abs($gr['rmgr_qtykonversi']),
					'whdeliveryorderdetail_remark' => !empty($rmgr_kets[$gr['rmgr_kd']]) ? $rmgr_kets[$gr['rmgr_kd']] : null,
					'whdeliveryorderdetail_reducestok' => 0,
					'whdeliveryorderdetail_tgledit' => date('Y-m-d H:i:s'),
					'whdeliveryorderdetail_tgledit' => date('Y-m-d H:i:s'),
					'admin_kd' => $this->session->userdata('kd_admin'),
				];

				$dataUpdate[] = [
					'rmgr_kd' => $gr['rmgr_kd'],
					'rmgr_nosrj' => $whdeliveryorder['whdeliveryorder_no'],
				];

				$whdeliveryorderdetail_kd++;
			}
			$this->db->trans_begin();
			$act = $this->td_whdeliveryorder_detail->insert_batch($data);
			$actUpdate = $this->db->update_batch('td_rawmaterial_goodsreceive', $dataUpdate, 'rmgr_kd');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}else{
				$this->db->trans_commit();
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Tidak ada yang dipilih');
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_change_state() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtid', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtwftransition_kd', 'Transition', 'required', ['required' => '{field} tidak boleh kosong!']);		

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrrm_kd' => (!empty(form_error('txtid')))?buildLabel('warning', form_error('txtid', '"', '"')):'',
				'idErrrm_kd' => (!empty(form_error('txtwftransition_kd')))?buildLabel('warning', form_error('txtwftransition_kd', '"', '"')):'',
			);
			
		}else {
			$whdeliveryorder_kd = $this->input->post('txtid', true);
			$wftransition_kd = $this->input->post('txtwftransition_kd', true);
			$whdeliveryordertransitionlog_note = $this->input->post('txtwhdeliveryordertransitionlog_note', true);

			$data = [
                'whdeliveryordertransitionlog_kd' => $this->td_whdeliveryorder_transition_log->create_code(),
                'whdeliveryorder_kd' => $whdeliveryorder_kd,
                'wftransition_kd' => $wftransition_kd,
                'admin_kd' => $this->session->userdata('kd_admin'),
                'whdeliveryordertransitionlog_note' => $whdeliveryordertransitionlog_note,
                'whdeliveryordertransitionlog_tglinput' => date('Y-m-d H:i:s'),
            ]; 
			$this->db->trans_begin();
			$whdeliveryorder = $this->tm_whdeliveryorder->get_by_param(['whdeliveryorder_kd' => $whdeliveryorder_kd])->row_array();
			$wftransition = $this->td_workflow_transition->get_by_param(['wftransition_kd' => $wftransition_kd])->row_array();
			$dataUpdate = [
				'wfstate_kd' => $wftransition['wftransition_destination'],
				'whdeliveryorder_tgledit' => date('Y-m-d H:i:s'),
			];
			#email message notification
			$subject = 'WH Delivery Order - '.$whdeliveryorder['whdeliveryorder_no'];

			$dtMessage['text'] = 'Terdapat WH Delivery Order yang perlu approval Anda :';
			$dtMessage['url'] = 'http://'.$_SERVER['HTTP_HOST'].base_url().$this->class_link.'/view_box?id='.$whdeliveryorder_kd;
			$dtMessage['urltext'] = 'Detail WH Delivery Order';
			$dtMessage['keterangan'] = !empty($whdeliveryordertransitionlog_note) ? $whdeliveryordertransitionlog_note : null;
			$message = $this->load->view('templates/email/email_notification', $dtMessage, true);

			$act = $this->td_whdeliveryorder_transition_log->insert_data($data);
			$actUpdate = $this->tm_whdeliveryorder->update_data (['whdeliveryorder_kd' => $whdeliveryorder_kd], $dataUpdate);
			$actNotif = $this->td_workflow_transition_notification->generate_notification($wftransition_kd, $subject, $message);
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}else{
				$this->db->trans_commit();
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete_detail() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$whdeliveryorderdetail_kd = $this->input->get('id', TRUE);
		
		if (!empty($whdeliveryorderdetail_kd)){
			$whdeliveryorderdetail = $this->td_whdeliveryorder_detail->get_by_param(['whdeliveryorderdetail_kd' => $whdeliveryorderdetail_kd])->row_array();

			# dari goodsreturn
			if (!empty($whdeliveryorderdetail['rmgr_kd'])) {
				$actUpdSrj = $this->td_rawmaterial_goodsreceive->update_data(['rmgr_kd' => $whdeliveryorderdetail['rmgr_kd']], ['rmgr_nosrj' => null]);
			}
			#penambahan kembali stok 
			if (!empty($whdeliveryorderdetail['whdeliveryorderdetail_reducestok'])){
				$rmstokmaster = $this->td_rawmaterial_stok_master->get_by_param(['rm_kd' => $whdeliveryorderdetail['rm_kd']])->row_array();
				$dataUpdateStokMaster[] = [
					'rmstokmaster_kd' => $rmstokmaster['rmstokmaster_kd'],
					'qty' => $whdeliveryorderdetail['whdeliveryorderdetail_qty'],
				];
				$actOut = $this->td_rawmaterial_out->delete_by_param('whdeliveryorderdetail_kd', $whdeliveryorderdetail_kd);
				$actUpdateStokMaster = $this->td_rawmaterial_stok_master->update_stock_batch ('IN', $dataUpdateStokMaster);
			}
			$act = $this->td_whdeliveryorder_detail->delete_data($whdeliveryorderdetail_kd);


			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
			}
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}


		echo json_encode($resp);
	}
	
}
