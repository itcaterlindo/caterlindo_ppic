<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Goods_receive extends MY_Controller
{
	private $class_link = 'inventory/raw_material/goods_receive';

	public function __construct()
	{
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model([
			'td_rawmaterial_in', 'td_rawmaterial_goodsreceive', 'tm_purchaseorder', 'tm_rawmaterial', 'td_purchaseorder_detail',
			'td_rawmaterial_stok', 'td_rawmaterial_stok_master', 'td_rawmaterial_harga', 'td_rawmaterial_harga_history', 'db_hrm/Tb_karyawan', 'td_whdeliveryorder_detail', 'tm_whdeliveryorder',
			'tm_deliverynote'
		]);
	}

	public function index()
	{
		parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
		parent::datetimepicker_assets();
		parent::input_mask();
		$this->form_box();
	}

	public function form_box()
	{
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/form_box', $data);
	}

	public function table_box()
	{
		// if (!$this->input->is_ajax_request()) {
		// 	exit('No direct script access allowed');
		// }

		$date = $this->input->get('date', true);

		$data['class_link'] = $this->class_link;
		$data['date'] = $date;

		$this->load->view('page/' . $this->class_link . '/table_box', $data);
	}

	public function table_main()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$date = $this->input->get('date', true);

		$data['class_link'] = $this->class_link;
		$data['date'] = $date;

		$this->load->view('page/' . $this->class_link . '/table_main', $data);
	}

	public function table_data()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$date = $this->input->get('date', true);

		$date = format_date($date, 'Y-m-d');
		$data = $this->td_rawmaterial_goodsreceive->ssp_table($date, ['RECEIVE', 'BACK', 'DN']);
		echo json_encode(
			SSP::simple($_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'])
		);
	}

	public function get_code_srj()
	{
		$xxz = $this->input->get('xxz', true);
		$data =  $this->td_rawmaterial_goodsreceive->get_code_srj_valid($xxz);
		echo json_encode($data);
	}



	public function get_po_param()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$paramPO = $this->input->get('paramPO');

		$result = [];
		$query = $this->db->select('tm_purchaseorder.*, tm_suplier.suplier_nama, tb_set_dropdown.nm_select')
			->from('tm_purchaseorder')
			->join('tm_suplier', 'tm_purchaseorder.suplier_kd=tm_suplier.suplier_kd')
			->join('tb_set_dropdown', 'tm_suplier.badanusaha_kd = tb_set_dropdown.id', 'left')
			->where('tm_purchaseorder.po_closeorder', '0');
		if (!empty($paramPO)) {
			$query = $query->group_start()
				->like('tm_purchaseorder.po_no', $paramPO, 'match')
				->or_like('tm_suplier.suplier_nama', $paramPO, 'match')
				->group_end();
		}
		$query = $query->order_by('tm_purchaseorder.po_no', 'asc')->get();

		if (!empty($query->num_rows())) {
			$query = $query->result_array();
			foreach ($query as $r) {
				$result[] = array(
					'id' => $r['po_kd'],
					'text' => $r['po_no'] . ' | ' . $r['nm_select'] . ' ' . $r['suplier_nama'],
				);
			}
		}
		echo json_encode($result);
	}

	public function get_whdo()
	{
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }
		$paramWhdo = $this->input->get('paramWhdo');
		$statusWhdo = 'Approved';

		$qWhdos = $this->db->where('td_workflow_state.wfstate_nama', $statusWhdo)
			->where('tm_whdeliveryorder.whdeliveryorder_sendback', 1)
			->group_start()
			->like('tm_whdeliveryorder.whdeliveryorder_no', $paramWhdo, 'match')
			->or_like('tm_suplier.suplier_nama', $paramWhdo, 'match')
			->group_end()
			->join('td_workflow_state', 'td_workflow_state.wfstate_kd=tm_whdeliveryorder.wfstate_kd', 'left')
			->join('tm_suplier', 'tm_whdeliveryorder.suplier_kd=tm_suplier.suplier_kd', 'left')
			->get('tm_whdeliveryorder')->result_array();
		$result = [];
		$kd_karyawans = array_unique(array_column($qWhdos, 'kd_karyawan'));
		$karyawans = $this->Tb_karyawan->get_where_in('kd_karyawan', $kd_karyawans)->result_array();

		foreach ($qWhdos as $whdo) {
			$nm_karyawan = '';
			foreach ($karyawans as $karyawan) {
				if ($karyawan['kd_karyawan'] == $whdo['kd_karyawan']) {
					$nm_karyawan = $karyawan['nm_karyawan'];
				}
			}
			if ($this->cek_qty($whdo['whdeliveryorder_no']) > 0) {
				$result[] = [
					'id' => $whdo['whdeliveryorder_kd'],
					'text' => $whdo['whdeliveryorder_no'] . ' | ' . $whdo['suplier_nama'] . $nm_karyawan,
				];
			}
		}
		echo json_encode($result);
	}

	private function cek_qty($tm_whdeliveryorder)
	{
		$result1 = $this->db->query("SELECT tm_whdeliveryorder.whdeliveryorder_no,sum(whdeliveryorderdetail_qty) as qty_asli 
		FROM tm_whdeliveryorder inner join td_whdeliveryorder_detail 
		on tm_whdeliveryorder.whdeliveryorder_kd = td_whdeliveryorder_detail.whdeliveryorder_kd 
		WHERE tm_whdeliveryorder.whdeliveryorder_no = '" . $tm_whdeliveryorder . "'")->first_row();

		$result2 = $this->db->query("SELECT tm_whdeliveryorder.whdeliveryorder_no,ifnull(sum(td_rawmaterial_goodsreceive.rmgr_qty),0) as qty_back 
		FROM tm_whdeliveryorder inner join td_whdeliveryorder_detail on tm_whdeliveryorder.whdeliveryorder_kd = td_whdeliveryorder_detail.whdeliveryorder_kd 
		left join td_rawmaterial_goodsreceive on td_whdeliveryorder_detail.whdeliveryorderdetail_kd = td_rawmaterial_goodsreceive.whdeliveryorderdetail_kd  
		WHERE tm_whdeliveryorder.whdeliveryorder_no = '" . $tm_whdeliveryorder . "'")->first_row();

		$result3 = $result1->qty_asli - $result2->qty_back;
		return $result3;
	}

	public function form_main()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/form_main', $data);
	}

	public function formsendback_main()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/formsendback_main', $data);
	}

	public function formgrdn_main()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/formgrdn_main', $data);
	}

	public function formgrdn_tablebatch()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$dn_kd = $this->input->get('dn_kd');

		$dnheader = $this->tm_deliverynote->get_by_param(['dn_kd' => $dn_kd])->row_array();

		$dndetails = $this->db->select('td_deliverynote_detail.*, td_workorder_item.woitem_no_wo, td_workorder_item.woitem_itemcode, SUM(td_deliverynote_received.dndetailreceived_qty) AS sum_dndetailreceived_qty')
			->where('td_deliverynote_detail.dn_kd', $dn_kd)
			->join('td_workorder_item', 'td_workorder_item.woitem_kd=td_deliverynote_detail.woitem_kd', 'left')
			->join('td_deliverynote_received', 'td_deliverynote_received.dndetail_kd=td_deliverynote_detail.dndetail_kd', 'left')
			->group_by('td_deliverynote_detail.dndetail_kd')
			->get('td_deliverynote_detail')->result_array();

		$dngrdetails = $this->db->select()
			->join('tm_rawmaterial', 'tm_rawmaterial.rm_kd=td_rawmaterial_goodsreceive.rm_kd', 'left')
			->join('td_rawmaterial_satuan', 'td_rawmaterial_satuan.rmsatuan_kd=tm_rawmaterial.rmsatuan_kd', 'left')
			->where('td_rawmaterial_goodsreceive.dn_kd', $dn_kd)
			->get('td_rawmaterial_goodsreceive')->result_array();

		$data['dnheader'] = $dnheader;
		$data['dndetails'] = $dndetails;
		$data['dngrdetails'] = $dngrdetails;
		$data['class_link'] = $this->class_link;

		$this->load->view('page/' . $this->class_link . '/formgrdn_tablebatch', $data);
	}

	private function groupby_podetail($goodsreceives = [])
	{
		$groups = array();
		foreach ($goodsreceives as $item) {
			$key = $item['podetail_kd'];
			if (!array_key_exists($key, $groups)) {
				$groups[$key] = array(
					'podetail_kd' => $key,
					'sumrmgr_qty' => $item['rmgr_qty'],
				);
			} else {
				$groups[$key]['sumrmgr_qty'] = (float) $groups[$key]['sumrmgr_qty'] + $item['rmgr_qty'];
			}
		}

		foreach ($groups as $g) {
			$arrGroup[] = $g;
		}

		return $arrGroup;
	}

	public function table_podetail()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$po_kd = $this->input->get('po_kd', true);
		$groupGoodsreceive = array();
		$resultDetails = array();

		$podetails = array();
		if (!empty($po_kd)) {
			$podetails = $this->td_purchaseorder_detail->get_by_param_detail(['po_kd' => $po_kd])->result_array();
			$goodsreceive = $this->td_rawmaterial_goodsreceive->get_by_param(['po_kd' => $po_kd]);

			if ($goodsreceive->num_rows() != 0) {
				$goodsreceive = $goodsreceive->result_array();
				$groupGoodsreceive = $this->groupby_podetail($goodsreceive);
			}
			foreach ($podetails as $item) {
				$kumQty = (float) $item['podetail_qty'];
				foreach ($groupGoodsreceive as $itemGR) {
					if ($itemGR['podetail_kd'] == $item['podetail_kd']) {
						$kumQty = (float) $kumQty - $itemGR['sumrmgr_qty'];
					}
				}

				$resultDetails[] = array(
					'podetail_kd' => $item['podetail_kd'],
					'podetail_tgldelivery' => $item['podetail_tgldelivery'],
					'rm_kd' => $item['rm_kd'],
					'rm_kode' => $item['rm_kode'],
					'podetail_deskripsi' => $item['podetail_deskripsi'],
					'podetail_spesifikasi' => $item['podetail_spesifikasi'],
					'podetail_konversi' => $item['podetail_konversi'],
					'podetail_konversicurrency' => $item['podetail_konversicurrency'],
					'kd_currency' => $item['kd_currency'],
					'podetail_harga' => $item['podetail_harga'],
					'satuan' => $item['rmsatuan_nama'],
					'podetail_qty' => $kumQty,
				);
			}
		}

		$data['po_kd'] = $po_kd;
		$data['podetails'] = $resultDetails;
		$this->load->view('page/' . $this->class_link . '/table_podetail', $data);
	}

	public function table_whdodetail()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$whdeliveryorder_kd = $this->input->get('whdeliveryorder_kd', true);
		$resultDetails = array();

		if (!empty($whdeliveryorder_kd)) {
			$whdeliveryorderdetails = $this->db->where('td_whdeliveryorder_detail.whdeliveryorder_kd', $whdeliveryorder_kd)
				->join('tm_rawmaterial', 'td_whdeliveryorder_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
				->join('tm_whdeliveryorder', 'tm_whdeliveryorder.whdeliveryorder_kd=td_whdeliveryorder_detail.whdeliveryorder_kd', 'left')
				->select('td_whdeliveryorder_detail.*, tm_rawmaterial.rm_kode, tm_rawmaterial.rm_oldkd, tm_whdeliveryorder.*')
				->get('td_whdeliveryorder_detail')->result_array();

			$goodsreceives = $this->db->where('td_rawmaterial_goodsreceive.rmgr_type', 'BACK')
				->where('td_whdeliveryorder_detail.whdeliveryorder_kd', $whdeliveryorder_kd)
				->join('td_whdeliveryorder_detail', 'td_whdeliveryorder_detail.whdeliveryorderdetail_kd=td_rawmaterial_goodsreceive.whdeliveryorderdetail_kd', 'left')
				->select('td_rawmaterial_goodsreceive.whdeliveryorderdetail_kd, SUM(td_rawmaterial_goodsreceive.rmgr_qty) as sumrmgr_qty, SUM(td_rawmaterial_goodsreceive.rmgr_qtykonversi) as sumrmgr_qtykonversi')
				->group_by('td_rawmaterial_goodsreceive.whdeliveryorderdetail_kd')
				->get('td_rawmaterial_goodsreceive')->result_array();


			foreach ($whdeliveryorderdetails as $item) {
				$kumQty = (float) $item['whdeliveryorderdetail_qty'];
				$kumQtykonversi = (float) $item['whdeliveryorderdetail_qtykonversi'];
				foreach ($goodsreceives as $itemGR) {
					if ($itemGR['whdeliveryorderdetail_kd'] == $item['whdeliveryorderdetail_kd']) {
						$kumQty = (float) $kumQty - $itemGR['sumrmgr_qty'];
						$kumQtykonversi = (float) $kumQtykonversi - $itemGR['sumrmgr_qtykonversi'];
					}
				}
				if (empty($kumQty)) {
					continue;
				}

				$resultDetails[] = array_merge($item, ['qty' => $kumQty, 'qty_konversi' => $kumQtykonversi]);
			}
		}

		$data['whdeliveryorder_kd'] = $whdeliveryorder_kd;
		$data['whdeliveryorderdetails'] = $resultDetails;
		$this->load->view('page/' . $this->class_link . '/table_whdodetail', $data);
	}

	public function get_deliverywhrm()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$paramDnWhrm = $this->input->get('paramDnWhrm');

		$dnwhrms = $this->db->where('dn_tujuan', '80');
		if (!empty($paramDnWhrm)) {
			$dnwhrms = $dnwhrms->like('dn_no', $paramDnWhrm, 'match');
		}
		$dnwhrms = $dnwhrms->get('tm_deliverynote')->result_array();

		$arrDnwhrms = array();
		foreach ($dnwhrms as $dnwhrm) {
			$arrDnwhrms[] = [
				'id' => $dnwhrm['dn_kd'],
				'text' => $dnwhrm['dn_no']
			];
		}
		echo json_encode($arrDnwhrms);
	}

	public function action_insert_batch()
	{
		// if (!$this->input->is_ajax_request()) {
		// 	exit('No direct script access allowed');
		// }

		// $this->load->library(['form_validation']);
		// $this->form_validation->set_rules('txtpo_kd', 'Purchase Order', 'required', ['required' => '{field} tidak boleh kosong!']);
		// $this->form_validation->set_rules('txtrmgr_nosrj', 'No Srj', 'required', ['required' => '{field} tidak boleh kosong!']);
		// $this->form_validation->set_rules('txtrmgr_tglsrj', 'Tgl Srj', 'required', ['required' => '{field} tidak boleh kosong!']);
		// $this->form_validation->set_rules('txtrmgr_tgldatang', 'Tgl Transaksi', 'required', ['required' => '{field} tidak boleh kosong!']);

		// if ($this->form_validation->run() == FALSE) {
		// 	$resp['code'] = 401;
		// 	$resp['status'] = 'Required';
		// 	$resp['pesan'] = array(
		// 		'idErrpo_kd' => (!empty(form_error('txtpo_kd'))) ? buildLabel('warning', form_error('txtpo_kd', '"', '"')) : '',
		// 		'idErrrmgr_nosrj' => (!empty(form_error('txtrmgr_nosrj'))) ? buildLabel('warning', form_error('txtrmgr_nosrj', '"', '"')) : '',
		// 		'idErrrmgr_tglsrj' => (!empty(form_error('txtrmgr_tglsrj'))) ? buildLabel('warning', form_error('txtrmgr_tglsrj', '"', '"')) : '',
		// 		'idErrrmgr_tgldatang' => (!empty(form_error('txtrmgr_tgldatang'))) ? buildLabel('warning', form_error('txtrmgr_tgldatang', '"', '"')) : '',
		// 	);
		// } else {


		$po_kd = $this->input->post('txtpo_kd', true);
		$podetail_kd = $this->input->post('txtpodetail_kd', true);
		$rmgr_qty = $this->input->post('txtrmgr_qty', true);
		$podetail_konversi = $this->input->post('txtpodetail_konversi', true);
		$rm_kd = $this->input->post('txtrm_kd', true);
		$rmgr_nosrj = $this->input->post('txtrmgr_nosrj', true);
		$rmgr_code_srj = $this->input->post('txtrmgr_code_srj', true);
		$kd_warehouse = $this->input->post('txtkd_warehouse', true);

		$rmgr_tglsrj = $this->input->post('txtrmgr_tglsrj', true);
		$rmgr_batch = $this->input->post('txtrmgr_batch', true);
		$rmgr_ket = $this->input->post('txtrmgr_ket', true);
		$rmgr_tgldatang = $this->input->post('txtrmgr_tgldatang', true);
		$podetail_harga = $this->input->post('txtpodetail_harga', true);
		$podetail_konversicurrency = $this->input->post('txtpodetail_konversicurrency', true);
		$kd_currency = $this->input->post('txtkd_currency', true);

		$rmin_kd = $this->td_rawmaterial_in->create_code();
		$rmgr_kd = $this->td_rawmaterial_goodsreceive->create_code();
		$rmstok_kd = $this->td_rawmaterial_stok->create_code();
		$rmhrghist_kd = $this->td_rawmaterial_harga_history->create_code();


		// $where = array('rmgr_nosrj !=' => $rmgr_nosrj , 'rmgr_code_srj' => $rmgr_code_srj);
		// $filter = $this->td_rawmaterial_goodsreceive->get_by_param($where)->row_array();
		// if(!empty($filter)){
		// 	$resp = array('code' => '999', 'status' => 'Gagal', 'pesan' => 'Data Duplicate! Antara "No Srj" / "Code Srj"');
		// 	echo json_encode($resp);
		// 	die();
		// }


		/** Tgl datang */
		$rmgr_tgldatang = format_date($rmgr_tgldatang, 'Y-m-d H:i:s');

		/** Get rmstokmaster_kd */
		$arrayRmkd = [];
		foreach ($podetail_kd as $each) :
			if ((trim($rmgr_qty[$each]) > 0)) {
				$arrayRmkd[] = [
					'rm_kd' => $rm_kd[$each],
					'rmgr_qtykonversi' => (float) $rmgr_qty[$each] * $podetail_konversi[$each],
					'kd_gudang' => $kd_warehouse
				];
			}
		endforeach;

		if (!empty($arrayRmkd)) {
			$rmstokmasters = $this->td_rawmaterial_stok_master->get_rmstokmasterkd_batch($arrayRmkd);

			/** Get suplier_kd */
			$rowPO = $this->tm_purchaseorder->get_by_param(['po_kd' => $po_kd])->row_array();
			$suplier_kd = $rowPO['suplier_kd'];

			foreach ($podetail_kd as $each) :
				if (!empty(trim($rmgr_qty[$each]))) {

					$rmgr_qtykonversi = (float) $rmgr_qty[$each] * $podetail_konversi[$each];
					$rmgr_hargakonversi = (float) $podetail_harga[$each] / $podetail_konversi[$each];
					$rmgr_hargaunitcurrency = $podetail_harga[$each] * $podetail_konversicurrency[$each];
					$rmgr_hargakonversicurrency = $rmgr_hargaunitcurrency / $podetail_konversi[$each];
					$rmgr_tglinput = now_with_ms();

					$arrRMgr[] = [
						'rmgr_kd' => $rmgr_kd,
						'po_kd' => $po_kd,
						'podetail_kd' => $each,
						'rm_kd' => $rm_kd[$each],
						'rmgr_tgldatang' => $rmgr_tgldatang,
						'rmgr_code_srj' => $this->td_rawmaterial_goodsreceive->get_code_srj_valid($rmgr_nosrj),
						'rmgr_nosrj' => $rmgr_nosrj,
						'rmgr_tglsrj' => $rmgr_tglsrj,
						'kd_warehouse' => $kd_warehouse,
						'rmgr_tglsrj' => format_date($rmgr_tglsrj, 'Y-m-d'),
						'batch' => $rmgr_batch[$each],
						'rmgr_ket' => !empty($rmgr_ket[$each]) ? $rmgr_ket[$each] : null,
						'rmgr_qty' => $rmgr_qty[$each],
						'rmgr_qtykonversi' => $rmgr_qtykonversi,
						'rmgr_hargaunit' =>  $podetail_harga[$each],
						'rmgr_hargakonversi' => $rmgr_hargakonversi,
						'rmgr_hargaunitcurrency' =>  $rmgr_hargaunitcurrency,
						'rmgr_hargakonversicurrency' =>  $rmgr_hargakonversicurrency,
						'kd_currency' =>  $kd_currency[$each],
						'rmgr_tglinput' => $rmgr_tglinput,
						'admin_kd' => $this->session->userdata('kd_admin'),
					];

					foreach ($rmstokmasters as $rmstokmaster) {
						if ($rmstokmaster['rm_kd'] == $rm_kd[$each]) {
							$rmstokmaster_kd = 	$rmstokmaster['rmstokmaster_kd'];
						}
					}
					$arrRMin[] = [
						'rmin_kd' => $rmin_kd,
						'rmstokmaster_kd' => $rmstokmaster_kd,
						'rm_kd' => $rm_kd[$each],
						'rmgr_kd' => $rmgr_kd,
						'rmin_qty' => $rmgr_qtykonversi,
						'rmin_tglinput' => $rmgr_tgldatang,
						'admin_kd' => $this->session->userdata('kd_admin'),
					];

					$arrMaterialStok[] = [
						'rmstok_kd' => $rmstok_kd,
						'rm_kd' => $rm_kd[$each],
						'rmin_kd' => $rmin_kd,
						'suplier_kd' => $suplier_kd,
						'rmstok_batch' => $rmgr_tgldatang,
						'rmstokjenis_kd' => '01',
						'rmstok_qty' => $rmgr_qtykonversi,
						'rmstok_tglinput' => $rmgr_tglinput,
						'admin_kd' => $this->session->userdata('kd_admin'),
					];

					$dataUpdateStokMaster[] = [
						'rmstokmaster_kd' => $rmstokmaster_kd,
						'qty' => $rmgr_qtykonversi,
						'kd_gudang' => $kd_warehouse,
					];

					$dataHargaGR[] = [
						'rm_kd' => $rm_kd[$each],
						'rmharga_hargagr' => $rmgr_hargakonversicurrency,
						'rmharga_tgledit_hargagr' => date('Y-m-d H:i:s')
					];

					$dataHistHarga[] = [
						'rmhrghist_kd' => $rmhrghist_kd,
						'podetail_kd' => $each,
						'rm_kd' => $rm_kd[$each],
						'rmhrghist_harga' => $rmgr_hargakonversicurrency,
						'admin_kd' => $this->session->userdata('kd_admin'),
					];

					/** Urutan rmin_kd */
					$rmin_kd = $rmin_kd + 1;
					/** Urutan rmgr_kd */
					$urutanGR = substr($rmgr_kd, -6);
					$angkaGR = $urutanGR + 1;
					$rmgr_kd = 'MGR' . date('ymd') . str_pad($angkaGR, 6, '0', STR_PAD_LEFT);
					/** Urutan rmstok_kd */
					$rmstok_kd = $rmstok_kd + 1;
					/** History harga */
					$rmhrghist_kd = $rmhrghist_kd + 1;
				}
			endforeach;

			$this->db->trans_begin();

			$actGR = $this->td_rawmaterial_goodsreceive->insert_batch($arrRMgr);
			$actIN = $this->td_rawmaterial_in->insert_batch($arrRMin);
			$actUpdateStokMaster = $this->td_rawmaterial_stok_master->update_stock_batch('IN', $dataUpdateStokMaster);
			$actStok = $this->td_rawmaterial_stok->insert_batch($arrMaterialStok);
			$actUpdateStatusPO = $this->tm_purchaseorder->update_close_order($po_kd);
			$actUpdateHargaGR = $this->td_rawmaterial_harga->update_harga_gr_batch($dataHargaGR);
			$actInsertBatchHargaGR = $this->td_rawmaterial_harga_history->insert_batch($dataHistHarga);

			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			} else {
				$this->db->trans_commit();
				$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'datanya' => $arrRMgr);
			}
		} else {
			$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Input Kosong');
		}
		// }
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_insert_batch_sendback()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtwhdeliveryorder_kd', 'Purchase Order', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtrmgr_nosrjSendback', 'No Srj', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtrmgr_tglsrjSendback', 'Tgl Srj', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtrmgr_tgldatangSendback', 'Tgl Transaksi', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrwhdeliveryorder_kd' => (!empty(form_error('txtwhdeliveryorder_kd'))) ? buildLabel('warning', form_error('txtwhdeliveryorder_kd', '"', '"')) : '',
				'idErrrmgr_nosrjSendback' => (!empty(form_error('txtrmgr_nosrjSendback'))) ? buildLabel('warning', form_error('txtrmgr_nosrjSendback', '"', '"')) : '',
				'idErrrmgr_tglsrjSendback' => (!empty(form_error('txtrmgr_tglsrjSendback'))) ? buildLabel('warning', form_error('txtrmgr_tglsrjSendback', '"', '"')) : '',
				'idErrrmgr_tgldatangSendback' => (!empty(form_error('txtrmgr_tgldatangSendback'))) ? buildLabel('warning', form_error('txtrmgr_tgldatangSendback', '"', '"')) : '',
			);
		} else {
			$whdeliveryorder_kd = $this->input->post('txtwhdeliveryorder_kd', true);
			$rmgr_tgldatang = $this->input->post('txtrmgr_tgldatangSendback', true);
			$rmgr_nosrj = $this->input->post('txtrmgr_nosrjSendback', true);
			$rmgr_tglsrj = $this->input->post('txtrmgr_tglsrjSendback', true);

			$whdeliveryorderdetail_kds = $this->input->post('txtwhdeliveryorderdetail_kd', true);
			$rmgr_qtys = $this->input->post('txtrmgr_qty', true);
			$whdeliveryorderdetail_qtys = $this->input->post('txtwhdeliveryorderdetail_qty', true);
			$whdeliveryorderdetail_qtykonversis = $this->input->post('txtwhdeliveryorderdetail_qtykonversi', true);
			$rm_kds = $this->input->post('txtrm_kd', true);
			$rmgr_kets = $this->input->post('txtrmgr_ket', true);

			$rmin_kd = $this->td_rawmaterial_in->create_code();
			$rmgr_kd = $this->td_rawmaterial_goodsreceive->create_code();
			$rmstok_kd = $this->td_rawmaterial_stok->create_code();

			/** Tgl datang */
			$rmgr_tgldatang = format_date($rmgr_tgldatang, 'Y-m-d H:i:s');

			/** Get rmstokmaster_kd */
			$arrayRmkd = [];
			foreach ($whdeliveryorderdetail_kds as $whdeliveryorderdetail_kd_0) :
				if ((trim($rmgr_qtys[$whdeliveryorderdetail_kd_0]) > 0)) {
					$arrayRmkd[] = [
						'rm_kd' => $rm_kds[$whdeliveryorderdetail_kd_0],
						'rmgr_qtykonversi' => (float) $rmgr_qtys[$whdeliveryorderdetail_kd_0] / $whdeliveryorderdetail_qtys[$whdeliveryorderdetail_kd_0] * $whdeliveryorderdetail_qtykonversis[$whdeliveryorderdetail_kd_0],
					];
				}
			endforeach;

			if (!empty($arrayRmkd)) {
				$rmstokmasters = $this->td_rawmaterial_stok_master->get_rmstokmasterkd_batch($arrayRmkd);

				/** Get suplier_kd */
				$rowWhdo = $this->tm_whdeliveryorder->get_by_param(['whdeliveryorder_kd' => $whdeliveryorder_kd])->row_array();
				$suplier_kd = $rowWhdo['suplier_kd'];

				foreach ($whdeliveryorderdetail_kds as $whdeliveryorderdetail_kd) :
					if (!empty(trim($rmgr_qtys[$whdeliveryorderdetail_kd]))) {

						$rmgr_qtykonversi = (float) $rmgr_qtys[$whdeliveryorderdetail_kd] / $whdeliveryorderdetail_qtys[$whdeliveryorderdetail_kd] * $whdeliveryorderdetail_qtykonversis[$whdeliveryorderdetail_kd];
						$rmgr_tglinput = now_with_ms();

						$arrRMgr[] = [
							'rmgr_kd' => $rmgr_kd,
							'po_kd' => null,
							'podetail_kd' => null,
							'whdeliveryorderdetail_kd' => $whdeliveryorderdetail_kd,
							'rm_kd' => $rm_kds[$whdeliveryorderdetail_kd],
							'rmgr_tgldatang' => $rmgr_tgldatang,
							'rmgr_code_srj' => $this->td_rawmaterial_goodsreceive->get_code_srj_valid($rmgr_nosrj),
							'rmgr_nosrj' => $rmgr_nosrj,
							'rmgr_tglsrj' => format_date($rmgr_tglsrj, 'Y-m-d'),
							'rmgr_ket' => !empty($rmgr_kets[$whdeliveryorderdetail_kd]) ? $rmgr_kets[$whdeliveryorderdetail_kd] : null,
							'rmgr_qty' => $rmgr_qtys[$whdeliveryorderdetail_kd],
							'rmgr_qtykonversi' => $rmgr_qtykonversi,
							'rmgr_hargaunit' =>  null,
							'rmgr_hargakonversi' => null,
							'rmgr_hargaunitcurrency' =>  null,
							'rmgr_hargakonversicurrency' =>  null,
							'rmgr_type' =>  'BACK',
							'rmgr_invoice' =>  1,
							'kd_currency' =>  null,
							'rmgr_tglinput' => $rmgr_tglinput,
							'admin_kd' => $this->session->userdata('kd_admin'),
						];

						foreach ($rmstokmasters as $rmstokmaster) {
							if ($rmstokmaster['rm_kd'] == $rm_kds[$whdeliveryorderdetail_kd]) {
								$rmstokmaster_kd = 	$rmstokmaster['rmstokmaster_kd'];
							}
						}
						$arrRMin[] = [
							'rmin_kd' => $rmin_kd,
							'rmstokmaster_kd' => $rmstokmaster_kd,
							'rm_kd' => $rm_kds[$whdeliveryorderdetail_kd],
							'rmgr_kd' => $rmgr_kd,
							'rmin_qty' => $rmgr_qtykonversi,
							'rmin_tglinput' => $rmgr_tgldatang,
							'admin_kd' => $this->session->userdata('kd_admin'),
						];

						$arrMaterialStok[] = [
							'rmstok_kd' => $rmstok_kd,
							'rm_kd' => $rm_kds[$whdeliveryorderdetail_kd],
							'rmin_kd' => $rmin_kd,
							'suplier_kd' => $suplier_kd,
							'rmstok_batch' => $rmgr_tgldatang,
							'rmstokjenis_kd' => '01',
							'rmstok_qty' => $rmgr_qtykonversi,
							'rmstok_tglinput' => $rmgr_tglinput,
							'admin_kd' => $this->session->userdata('kd_admin'),
						];

						$dataUpdateStokMaster[] = [
							'rmstokmaster_kd' => $rmstokmaster_kd,
							'qty' => $rmgr_qtykonversi,
						];

						/** Urutan rmin_kd */
						$rmin_kd = $rmin_kd + 1;
						/** Urutan rmgr_kd */
						$urutanGR = substr($rmgr_kd, -6);
						$angkaGR = $urutanGR + 1;
						$rmgr_kd = 'MGR' . date('ymd') . str_pad($angkaGR, 6, '0', STR_PAD_LEFT);
						/** Urutan rmstok_kd */
						$rmstok_kd = $rmstok_kd + 1;
					}
				endforeach;

				$this->db->trans_begin();

				$actGR = $this->td_rawmaterial_goodsreceive->insert_batch($arrRMgr);
				$actIN = $this->td_rawmaterial_in->insert_batch($arrRMin);
				$actUpdateStokMaster = $this->td_rawmaterial_stok_master->update_stock_batch('IN', $dataUpdateStokMaster);
				$actStok = $this->td_rawmaterial_stok->insert_batch($arrMaterialStok);

				if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
				} else {
					$this->db->trans_commit();
					$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
				}
			} else {
				$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Input Kosong');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_insert_batch_dn()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtdn_kd', 'ID', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrdn_kd' => (!empty(form_error('txtdn_kd'))) ? buildLabel('warning', form_error('txtdn_kd', '"', '"')) : '',
			);
		} else {
			$dn_kd = $this->input->post('txtdn_kd');
			$seqs = $this->input->post('txtseqs');
			$rm_kds = $this->input->post('txtrm_kds');
			$rmgr_qtys = $this->input->post('txtrmgr_qtys');
			$rmgr_kets = $this->input->post('txtrmgr_kets');
			$rmgr_tgldatang = $this->input->post('txtrmgr_tgldatangDnwhrm');

			if (!empty($seqs)) {
				/** Generate for get rmstokmaster */
				$arrRm_kds = [];
				$groups = [];
				foreach ($seqs as $seq0) {
					$rm_kd = $rm_kds[$seq0];
					$rmgr_qty = isset($rmgr_qtys[$seq0][$rm_kd]) ? (float)$rmgr_qtys[$seq0][$rm_kd] : 0;
					if (!array_key_exists($rm_kd, $groups)) {
						$groups[$rm_kd] = array(
							'sum_rmgr_qty' => $rmgr_qty,
						);
						$arrRm_kds[] = array('rm_kd' => $rm_kd);
					} else {
						$groups[$rm_kd]['sum_rmgr_qty'] = $groups[$rm_kd]['sum_rmgr_qty'] + $rmgr_qty;
					}
				}
				$dn = $this->tm_deliverynote->get_by_param(['dn_kd' => $dn_kd])->row_array();
				$rmstokmasters = $this->td_rawmaterial_stok_master->get_rmstokmasterkd_batch($arrRm_kds);

				$rmgr_kd = $this->td_rawmaterial_goodsreceive->create_code();
				$rmin_kd = $this->td_rawmaterial_in->create_code();
				$rmstok_kd = $this->td_rawmaterial_stok->create_code();
				foreach ($seqs as $seq) {
					$rm_kd = $rm_kds[$seq];
					$rmgr_qty = isset($rmgr_qtys[$seq][$rm_kd]) ? (float)$rmgr_qtys[$seq][$rm_kd] : 0;
					if ($rmgr_qty <= 0) {
						continue;
					}
					$arrRMgr[] = [
						'rmgr_kd' => $rmgr_kd,
						'po_kd' => null,
						'podetail_kd' => null,
						'whdeliveryorderdetail_kd' => null,
						'dn_kd' => $dn_kd,
						'rm_kd' => $rm_kd,
						'rmgr_tgldatang' => format_date($rmgr_tgldatang, 'Y-m-d H:i:s'),
						'rmgr_nosrj' => $dn['dn_no'],
						'rmgr_code_srj' => $this->td_rawmaterial_goodsreceive->get_code_srj_valid($dn['dn_no']),
						'rmgr_tglsrj' => format_date($rmgr_tgldatang, 'Y-m-d'),
						'rmgr_ket' => isset($rmgr_kets[$seq][$rm_kd]) ? $rmgr_kets[$seq][$rm_kd] : null,
						'rmgr_qty' => $rmgr_qty,
						'rmgr_qtykonversi' => $rmgr_qty,
						'rmgr_hargaunit' =>  null,
						'rmgr_hargakonversi' => null,
						'rmgr_hargaunitcurrency' =>  null,
						'rmgr_hargakonversicurrency' =>  null,
						'rmgr_type' =>  'DN',
						'rmgr_invoice' =>  1,
						'kd_currency' =>  null,
						'rmgr_tglinput' => now_with_ms(),
						'admin_kd' => $this->session->userdata('kd_admin'),
					];
					foreach ($rmstokmasters as $rmstokmaster) {
						if ($rmstokmaster['rm_kd'] == $rm_kd) {
							$rmstokmaster_kd = 	$rmstokmaster['rmstokmaster_kd'];
						} else {
							$rmstokmaster_kd = null;
						}
					}
					$arrRMin[] = [
						'rmin_kd' => $rmin_kd,
						'rmstokmaster_kd' => $rmstokmaster_kd,
						'rm_kd' => $rm_kd,
						'rmgr_kd' => $rmgr_kd,
						'rmin_qty' => $rmgr_qty,
						'rmin_tglinput' => format_date($rmgr_tgldatang, 'Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					];

					$arrMaterialStok[] = [
						'rmstok_kd' => $rmstok_kd,
						'rm_kd' => $rm_kd,
						'rmin_kd' => $rmin_kd,
						'suplier_kd' => null,
						'rmstok_batch' => format_date($rmgr_tgldatang, 'Y-m-d H:i:s'),
						'rmstokjenis_kd' => '01',
						'rmstok_qty' => $rmgr_qty,
						'rmstok_tglinput' => now_with_ms(),
						'admin_kd' => $this->session->userdata('kd_admin'),
					];

					/** Urutan rmin_kd */
					$rmin_kd = $rmin_kd + 1;
					/** Urutan rmgr_kd */
					$urutanGR = substr($rmgr_kd, -6);
					$angkaGR = $urutanGR + 1;
					$rmgr_kd = 'MGR' . date('ymd') . str_pad($angkaGR, 6, '0', STR_PAD_LEFT);
					/** Urutan rmstok_kd */
					$rmstok_kd = $rmstok_kd + 1;
				}
				/** Update stok master */
				foreach ($groups as $rm_kd => $el) {
					foreach ($rmstokmasters as $rmstokmaster1) {
						if ($rmstokmaster1['rm_kd'] == $rm_kd) {
							$rmstokmaster_kd = 	$rmstokmaster1['rmstokmaster_kd'];
						}
					}
					$dataUpdateStokMaster[] = [
						'rmstokmaster_kd' => $rmstokmaster_kd,
						'qty' => $el['sum_rmgr_qty'],
					];
				}
				$this->db->trans_begin();

				$this->td_rawmaterial_goodsreceive->insert_batch($arrRMgr);
				$this->td_rawmaterial_in->insert_batch($arrRMin);
				$this->td_rawmaterial_stok->insert_batch($arrMaterialStok);
				if (!empty($dataUpdateStokMaster)) {
					$this->td_rawmaterial_stok_master->update_stock_batch('IN', $dataUpdateStokMaster);
				}

				if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
				} else {
					$this->db->trans_commit();
					$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
				}
			} else {
				$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'RM Kosong');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);

		/** u/ update stok */
		$rowIN = $this->td_rawmaterial_in->get_by_param(['rmgr_kd' => $id])->row_array();
		$rowGR = $this->td_rawmaterial_goodsreceive->get_by_param(['rmgr_kd' => $id])->row_array();

		$this->db->trans_start();

		$actUpdateStok = $this->td_rawmaterial_stok_master->update_stok($rowIN['rmstokmaster_kd'], 'OUT', $rowIN['rmin_qty']);
		$actDelIN = $this->td_rawmaterial_in->delete_data($rowIN['rmin_kd']);
		$actDelGR = $this->td_rawmaterial_goodsreceive->delete_data($id);
		if (!empty($rowGR['po_kd'])) {
			$actUpdateStatusPO = $this->tm_purchaseorder->update_close_order($rowGR['po_kd']);
		}

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		} else {
			$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
		}

		echo json_encode($resp);
	}
	public function action_rollback()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$id = $this->input->get('id', TRUE);

		$act = $this->td_rawmaterial_goodsreceive->update_data(['rmgr_kd' => $id], ['status_sap' => "0"]);
		if ($act) {
			$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
		} else {
			$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		}
		echo json_encode($resp);
	}
	public function set_to_sap()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$rmgr = $this->input->get('id', TRUE);
		$act = $this->td_rawmaterial_goodsreceive->update_data(['rmgr_kd' => $rmgr], ['status_sap' => "1"]);
		$rowGR = $this->td_rawmaterial_goodsreceive->get_by_param_join(['rmgr_kd' => $rmgr])->row_array();
		if ($act) {
			$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => $rowGR);
		} else {
			$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		}
		echo json_encode($resp);
	}
}
