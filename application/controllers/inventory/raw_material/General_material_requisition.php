<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class General_material_requisition extends MY_Controller
{
	private $class_link = 'inventory/raw_material/general_material_requisition';

	public function __construct()
	{
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_materialrequisition_general', 'td_rawmaterial_out', 'td_rawmaterial_stok_master', 'tm_gudang', 'tb_finishgood_user']);
	}

	public function index()
	{
		parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
		parent::datetimepicker_assets();
		parent::input_mask();
		$this->form_box();
	}

	public function form_box()
	{
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/form_box', $data);
	}

	public function table_box()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$date = $this->input->get('date', true);

		$data['class_link'] = $this->class_link;
		$data['date'] = $date;

		$this->load->view('page/' . $this->class_link . '/table_box', $data);
	}

	public function table_main()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$date = $this->input->get('date', true);

		$data['class_link'] = $this->class_link;
		$data['date'] = $date;

		$this->load->view('page/' . $this->class_link . '/table_main', $data);
	}

	public function table_data()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$date = $this->input->get('date', true);

		$date = format_date($date, 'Y-m-d');
		$data = $this->td_materialrequisition_general->ssp_table($date);
		echo json_encode(
			SSP::simple($_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'])
		);
	}
	
	public function cetak_data()
	{
		$this->load->library('Pdf');

        $date = $this->input->get('date');
        $data = array();
        
        if (!empty($date)){
            $data['date'] = $date;
            $data['class_link'] = $this->class_link;


			if($this->session->tipe_admin == 'Admin'){
           			 $data['data_general'] =  $this->db->query("SELECT * FROM td_materialrequisition_general as a 
									LEFT JOIN td_rawmaterial_satuan as b ON a.materialreqgeneral_satuan_kd=b.rmsatuan_kd
									LEFT JOIN tm_rawmaterial as c ON c.rm_kd=a.rm_kd
									LEFT JOIN td_purchaseorder_detail as d ON d.podetail_kd=a.podetail_kd
									LEFT JOIN tb_bagian as e on e.bagian_kd=a.bagian_kd WHERE a.materialreqgeneral_tanggal ='" . $date ."'" )->result();
			}else{
				$param = $this->session->kd_admin;
				$data['data_general'] = $this->db->query("SELECT * FROM td_materialrequisition_general as a 
									LEFT JOIN td_rawmaterial_satuan as b ON a.materialreqgeneral_satuan_kd=b.rmsatuan_kd
									LEFT JOIN tm_rawmaterial as c ON c.rm_kd=a.rm_kd
									LEFT JOIN td_purchaseorder_detail as d ON d.podetail_kd=a.podetail_kd
									LEFT JOIN tb_bagian as e on e.bagian_kd=a.bagian_kd WHERE a.materialreqgeneral_tanggal ='$date' AND a.admin_kd = '$param' ")->result();
			}
        }
        echo json_encode($this->session->all_userdata());
        $this->load->view('page/'.$this->class_link.'/cetak_data', $data);
	}

	

	public function get_podetail_special()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$paramSpecial = $this->input->get('paramSpecial');

		$qGoodsReceived = $this->db->select('td_purchaseorder_detail.*, td_rawmaterial_goodsreceive.rmgr_kd, td_rawmaterial_goodsreceive.rmgr_qty, tm_purchaseorder.po_no, td_rawmaterial_satuan.rmsatuan_nama')
			->from('td_rawmaterial_goodsreceive')
			->join('td_purchaseorder_detail', 'td_purchaseorder_detail.podetail_kd=td_rawmaterial_goodsreceive.podetail_kd')
			->join('tm_purchaseorder', 'tm_purchaseorder.po_kd=td_purchaseorder_detail.po_kd')
			->join('td_rawmaterial_satuan', 'td_rawmaterial_satuan.rmsatuan_kd=td_purchaseorder_detail.rmsatuan_kd')
			->where('td_rawmaterial_goodsreceive.rm_kd', 'SPECIAL')
			->order_by('tm_purchaseorder.po_no', 'asc')->get()->result_array();
		$qMaterialReqGeneral = $this->db->select('td_materialrequisition_general.rmgr_kd, SUM(td_materialrequisition_general.materialreqgeneral_qty) as sum_materialreqgeneral_qty')
			->from('td_materialrequisition_general')
			->where('td_materialrequisition_general.rm_kd', 'SPECIAL')
			->group_by('rmgr_kd')
			->get()->result_array();

		$arrMaterialReg = [];
		foreach ($qMaterialReqGeneral as $eachMatereialReqGeneral) {
			$arrMaterialReg[$eachMatereialReqGeneral['rmgr_kd']] = $eachMatereialReqGeneral['sum_materialreqgeneral_qty'];
		}

		$arrResult = [];
		foreach ($qGoodsReceived as $eachGoodsReceived) {
			$qty = $eachGoodsReceived['rmgr_qty'];
			if (isset($arrMaterialReg[$eachGoodsReceived['rmgr_kd']])) {
				$qty = $qty - $arrMaterialReg[$eachGoodsReceived['rmgr_kd']];
			}
			if (empty($qty)) {
				continue;
			}
			$arrResult[] = [
				'id' => $eachGoodsReceived['rmgr_kd'],
				'rmgr_kd' => $eachGoodsReceived['rmgr_kd'],
				'podetail_kd' => $eachGoodsReceived['podetail_kd'],
				'podetail_deskripsi' => $eachGoodsReceived['podetail_deskripsi'],
				'podetail_spesifikasi' => $eachGoodsReceived['podetail_spesifikasi'],
				'rmsatuan_kd' => $eachGoodsReceived['rmsatuan_kd'],
				'rmsatuan_nama' => $eachGoodsReceived['rmsatuan_nama'],
				'rm_kd' => $eachGoodsReceived['rm_kd'],
				'qty' => $qty,
				'text' => "{$eachGoodsReceived['po_no']} | {$eachGoodsReceived['podetail_deskripsi']} {$eachGoodsReceived['podetail_spesifikasi']}",
			];
		}

		$arrResultFilter = [];
		if (!empty($paramSpecial)) {
			foreach ($arrResult as $eachResult) {
				if (!empty(strpos($eachResult['text'], $paramSpecial))) {
					$arrResultFilter[] = $eachResult;
				}
			}
		} else {
			$arrResultFilter = $arrResult;
		}
		echo json_encode($arrResultFilter);
	}

	public function get_whdo()
	{
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }
		$paramWhdo = $this->input->get('paramWhdo');
		$statusWhdo = 'Approved';

		$qWhdos = $this->db->where('td_workflow_state.wfstate_nama', $statusWhdo)
			->where('tm_whdeliveryorder.whdeliveryorder_sendback', 1)
			->group_start()
			->like('tm_whdeliveryorder.whdeliveryorder_no', $paramWhdo, 'match')
			->or_like('tm_suplier.suplier_nama', $paramWhdo, 'match')
			->group_end()
			->join('td_workflow_state', 'td_workflow_state.wfstate_kd=tm_whdeliveryorder.wfstate_kd', 'left')
			->join('tm_suplier', 'tm_whdeliveryorder.suplier_kd=tm_suplier.suplier_kd', 'left')
			->get('tm_whdeliveryorder')->result_array();
		$result = [];
		$kd_karyawans = array_unique(array_column($qWhdos, 'kd_karyawan'));
		$karyawans = $this->Tb_karyawan->get_where_in('kd_karyawan', $kd_karyawans)->result_array();

		foreach ($qWhdos as $whdo) {
			$nm_karyawan = '';
			foreach ($karyawans as $karyawan) {
				if ($karyawan['kd_karyawan'] == $whdo['kd_karyawan']) {
					$nm_karyawan = $karyawan['nm_karyawan'];
				}
			}
			$result[] = [
				'id' => $whdo['whdeliveryorder_kd'],
				'text' => $whdo['whdeliveryorder_no'] . ' | ' . $whdo['suplier_nama'] . $nm_karyawan,
			];
		}
		echo json_encode($result);
	}

	public function form_main()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		/** Set default gudang */
		$data['gudang'] = $this->tm_gudang->get_all()->result_array();
		$data['gudang_user'] = $this->tb_finishgood_user->get_by_param(['kd_tipe_admin' => $this->session->userdata('tipe_admin_kd')])->row_array();
		$data['gudang_nm'] = $this->tm_gudang->get_row($data['gudang_user']['kd_gudang'])->nm_gudang;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/form_main', $data);
	}

	public function action_insert()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtmaterialreqgeneral_tanggal', 'Tanggal', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtrm_kd', 'Material', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtmaterialreqgeneral_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtbagian_kd', 'Bagian', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrmaterialreqgeneral_tanggal' => (!empty(form_error('txtmaterialreqgeneral_tanggal'))) ? buildLabel('warning', form_error('txtmaterialreqgeneral_tanggal', '"', '"')) : '',
				'idErrrm_kd' => (!empty(form_error('txtrm_kd'))) ? buildLabel('warning', form_error('txtrm_kd', '"', '"')) : '',
				'idErrmaterialreqgeneral_qty' => (!empty(form_error('txtmaterialreqgeneral_qty'))) ? buildLabel('warning', form_error('txtmaterialreqgeneral_qty', '"', '"')) : '',
				'idErrbagian_kd' => (!empty(form_error('txtbagian_kd'))) ? buildLabel('warning', form_error('txtbagian_kd', '"', '"')) : '',
			);
		} else {
			$materialreqgeneral_tanggal = $this->input->post('txtmaterialreqgeneral_tanggal', true);
			$rm_kd = $this->input->post('txtrm_kd', true);
			$rmgr_kd = $this->input->post('txtrmgr_kd', true);
			$podetail_kd = $this->input->post('txtpodetail_kd', true);
			$materialreqgeneral_satuan_kd = $this->input->post('txtmaterialreqgeneral_satuan_kd', true);
			$materialreqgeneral_qty = $this->input->post('txtmaterialreqgeneral_qty', true);
			$bagian_kd = $this->input->post('txtbagian_kd', true);
			$gudang_kd = $this->input->post('txtgudang_kd', true);
			$materialreqgeneral_remark = $this->input->post('txtmaterialreqgeneral_remark', true);

			$materialreqgeneral_kd = $this->td_materialrequisition_general->create_code();
			$data = [
				'materialreqgeneral_kd' => $materialreqgeneral_kd,
				'rmgr_kd' => !empty($rmgr_kd) ? $rmgr_kd : null,
				'podetail_kd' => !empty($podetail_kd) ? $podetail_kd : null,
				'rm_kd' => $rm_kd,
				'materialreqgeneral_satuan_kd' => $materialreqgeneral_satuan_kd,
				'bagian_kd' => $bagian_kd,
				'gudang_kd' => $gudang_kd,
				'materialreqgeneral_qty' => $materialreqgeneral_qty,
				'materialreqgeneral_remark' => !empty($materialreqgeneral_remark) ? $materialreqgeneral_remark : null,
				'materialreqgeneral_tanggal' => $materialreqgeneral_tanggal,
				'materialreqgeneral_tglinput' => date('Y-m-d H:i:s', strtotime( $materialreqgeneral_tanggal." ".date('H:i:s') )),
				'materialreqgeneral_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			];

			
			$this->db->trans_begin();

			$this->td_materialrequisition_general->insert_data($data);

			/** RM Out */
			$rmstokmaster_kds = $this->td_rawmaterial_stok_master->get_rmstokmasterkd_batch ([['rm_kd' => $rm_kd]]);
			$rmstokmaster_kd = $rmstokmaster_kds[0]['rmstokmaster_kd'];
			$dataOut = [
				'rmout_kd' => $this->td_rawmaterial_out->create_code(),
				'rmstokmaster_kd' => $rmstokmaster_kd,
				'rm_kd' => $rm_kd,
				'materialreqgeneral_kd' => $materialreqgeneral_kd,
				'rmout_qty' => $materialreqgeneral_qty,
				'rmout_tglinput' => date('Y-m-d H:i:s', strtotime( $materialreqgeneral_tanggal." ".date('H:i:s') )),
				'rmout_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			];
			$this->td_rawmaterial_out->insert_data($dataOut);
			if ($rm_kd != 'SPECIAL') {
				/** Update Stock On Hand */
				$this->td_rawmaterial_stok_master->update_stock_batch ('OUT', [['rmstokmaster_kd' => $rmstokmaster_kd, 'qty' => $materialreqgeneral_qty]]);
			}

			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			} else {
				$this->db->trans_commit();
				$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);

		/** u/ update stok */
		$rowOUT = $this->td_rawmaterial_out->get_by_param(['materialreqgeneral_kd' => $id])->row_array();
		$rowGeneral = $this->td_materialrequisition_general->get_by_param(['materialreqgeneral_kd' => $id])->row_array();

		$this->db->trans_start();

		$this->td_rawmaterial_stok_master->update_stok($rowOUT['rmstokmaster_kd'], 'IN', $rowOUT['rmout_qty']);
		$this->td_rawmaterial_out->delete_data($rowOUT['rmout_kd']);
		$this->td_materialrequisition_general->delete_data($id);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		} else {
			$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
		}

		echo json_encode($resp);
	}
}
