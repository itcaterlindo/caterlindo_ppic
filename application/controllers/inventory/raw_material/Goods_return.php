<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Goods_return extends MY_Controller {
	private $class_link = 'inventory/raw_material/goods_return';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
        $this->load->model(['td_rawmaterial_in', 'td_rawmaterial_goodsreceive', 'td_rawmaterial_stok', 'td_rawmaterial_stok_master', 'tm_purchaseorder', 'td_purchaseorder_detail']);
	}

	public function index() {
        parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
		parent::datetimepicker_assets();
        parent::input_mask();
		$this->form_box();
	}
		
	public function form_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function table_box(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$date = $this->input->get('date', true);

		$data['class_link'] = $this->class_link;
		$data['date'] = $date;

		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$date = $this->input->get('date', true);

		$data['class_link'] = $this->class_link;
		$data['date'] = $date;

		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data (){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$date = $this->input->get('date', true);

		$date = format_date($date, 'Y-m-d');
		$data = $this->td_rawmaterial_goodsreceive->ssp_table($date, ['RETURN']);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function get_poreceived() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$paramPO = $this->input->get('paramPO');
		
		$result = [];
		$query = $this->db->select('tm_purchaseorder.*, tm_suplier.suplier_nama, tb_set_dropdown.nm_select')
					->from('td_rawmaterial_goodsreceive')
					->join('tm_purchaseorder', 'tm_purchaseorder.po_kd=td_rawmaterial_goodsreceive.po_kd', 'left')
					->join('tm_suplier', 'tm_purchaseorder.suplier_kd=tm_suplier.suplier_kd')
					->join('tb_set_dropdown', 'tm_suplier.badanusaha_kd = tb_set_dropdown.id', 'left');
		if (!empty($paramPO)) {
		$query = $query->group_start()
			->like('tm_purchaseorder.po_no', $paramPO, 'match')
			->or_like('tm_suplier.suplier_nama', $paramPO, 'match')
			->group_end();
		}
		$query = $query->order_by('tm_purchaseorder.po_no', 'asc')->group_by('td_rawmaterial_goodsreceive.po_kd')->get();
		
		if (!empty($query->num_rows())) {
			$query = $query->result_array();
			foreach ($query as $r) {
				$result[] = array(
					'id' => $r['po_kd'],
					'text' => $r['po_no'].' | '.$r['nm_select'].' '.$r['suplier_nama'],
				); 
			}
		}
		echo json_encode($result);
	}

	public function form_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function grdetail_table () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$po_kd = $this->input->get('po_kd', true);

        $goodsreceives = $this->db->select('td_rawmaterial_goodsreceive.*, td_purchaseorder_detail.*, tm_rawmaterial.rm_kode, tm_rawmaterial.rm_oldkd')
                    ->from('td_rawmaterial_goodsreceive')
                    ->join('td_purchaseorder_detail', 'td_purchaseorder_detail.podetail_kd = td_rawmaterial_goodsreceive.podetail_kd', 'left')
                    ->join('tm_rawmaterial', 'td_purchaseorder_detail.rm_kd = tm_rawmaterial.rm_kd', 'left')
                    ->where('td_rawmaterial_goodsreceive.po_kd', $po_kd)
					->where('td_rawmaterial_goodsreceive.rmgr_type', 'RECEIVE')
                    ->get()->result_array();
		$goodsreturns = $this->td_rawmaterial_goodsreceive->get_by_param(['po_kd' => $po_kd, 'rmgr_type' => 'RETURN'])->result_array();
		
		$arrayReceived = [];
		foreach ($goodsreceives as $goodsreceive) {
			$qty_received = $goodsreceive['rmgr_qty'];
			foreach ($goodsreturns as $goodsreturn){
				if ($goodsreturn['podetail_kd'] == $goodsreceive['podetail_kd']){
					$qty_received += $goodsreturn['rmgr_qty'];
				}
			}
			$arrayReceived[] = array_merge($goodsreceive, ['qty_received' => $qty_received]);
		}

		$data['po_kd'] = $po_kd;
		$data['results'] = $arrayReceived;
		$this->load->view('page/'.$this->class_link.'/grdetail_table', $data);
	}

	public function action_insert_batch() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpo_kd', 'Purchase Order', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtrmgr_tgldatang', 'Tgl Return', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpo_kd' => (!empty(form_error('txtpo_kd')))?buildLabel('warning', form_error('txtpo_kd', '"', '"')):'',
				'idErrrmgr_tgldatang' => (!empty(form_error('txtrmgr_tgldatang')))?buildLabel('warning', form_error('txtrmgr_tgldatang', '"', '"')):'',
			);
			
		}else {
			$po_kd = $this->input->post('txtpo_kd', true);

			$rmgr_tgldatang = $this->input->post('txtrmgr_tgldatang', true);
			$rmgr_kds = $this->input->post('txtrmgr_kds', true);
			$rm_kds = $this->input->post('txtrm_kds', true);
			$rmgr_qtys = $this->input->post('txtrmgr_qtys', true);
			$rmgr_qtykonversis = $this->input->post('txtrmgr_qtykonversis', true);

			$rmin_kd = $this->td_rawmaterial_in->create_code();
			$rmgr_kd = $this->td_rawmaterial_goodsreceive->create_code();
			$rmstok_kd = $this->td_rawmaterial_stok->create_code();
			
			/** Tgl datang */
			$rmgr_tgldatang = format_date($rmgr_tgldatang, 'Y-m-d H:i:s');

			/** Get rmstokmaster_kd */
			$arrayRmkd = []; $rmgrkd_process = [];
			foreach ($rmgr_kds as $each):
				if ((trim($rmgr_qtys[$each]) > 0 )){ 
					$arrayRmkd[] = [
						'rm_kd' => $rm_kds[$each],
						'rmgr_qtykonversi' => (float) $rmgr_qtykonversis[$each],
					];
                    $rmgrkd_process[] = $each;
				}
			endforeach;

			if (!empty($arrayRmkd)){
				$rmstokmasters = $this->td_rawmaterial_stok_master->get_rmstokmasterkd_batch($arrayRmkd);
                $goodsreceives = $this->td_rawmaterial_goodsreceive->get_where_in ('rmgr_kd', $rmgrkd_process)->result_array();
				$po = $this->tm_purchaseorder->get_by_param(['po_kd' => $po_kd])->row_array();

				foreach ($goodsreceives as $each):
                        $rmgr_qty_return = $rmgr_qtys[$each['rmgr_kd']] * -1;
                        $rmgr_qtykonversi_return = $each['rmgr_qtykonversi'] / $each['rmgr_qty'] * $rmgr_qty_return;
						$rmgr_hargaunit_return = $each['rmgr_hargaunit'] * -1;
						$rmgr_hargakonversi_return = $each['rmgr_hargakonversi'] * -1;
						$rmgr_hargaunitcurrency_return = $each['rmgr_hargaunitcurrency'] * -1;
						$rmgr_hargakonversicurrency_return = $each['rmgr_hargakonversicurrency'] * -1;
						$rmgr_tglinput = now_with_ms();

						$arrRMgr[] = [
							'rmgr_kd' => $rmgr_kd,
							'po_kd' => $each['po_kd'],
							'podetail_kd' => $each['podetail_kd'],
							'rm_kd' => $each['rm_kd'],
							'rmgr_tgldatang' => $rmgr_tgldatang,
							'rmgr_code_srj' => $this->td_rawmaterial_goodsreceive->get_code_srj_valid(''),
							'rmgr_nosrj' => null,
							'rmgr_tglsrj' => format_date($rmgr_tgldatang, 'Y-m-d'),
							'rmgr_ket' => $each['rmgr_ket'],
							'rmgr_qty' => $rmgr_qty_return,
							'rmgr_qtykonversi' => $rmgr_qtykonversi_return,
							'rmgr_hargaunit' =>  $rmgr_hargaunit_return,
							'rmgr_hargakonversi' => $rmgr_hargakonversi_return,
							'rmgr_hargaunitcurrency' =>  $rmgr_hargaunitcurrency_return,
							'rmgr_hargakonversicurrency' =>  $rmgr_hargakonversicurrency_return,
							'kd_currency' =>  $each['kd_currency'],
							'rmgr_type' => 'RETURN',
							'rmgr_tglinput' => $rmgr_tglinput,
							'admin_kd' => $this->session->userdata('kd_admin'),
						];

						foreach ($rmstokmasters as $rmstokmaster) {
							if ($rmstokmaster['rm_kd'] == $each['rm_kd']){
								$rmstokmaster_kd = 	$rmstokmaster['rmstokmaster_kd'];
							}
						}
						$arrRMin[] = [
							'rmin_kd' => $rmin_kd,
							'rmstokmaster_kd' => $rmstokmaster_kd,
							'rm_kd' => $each['rm_kd'],
							'rmgr_kd' => $rmgr_kd,
							'rmin_qty' => $rmgr_qtykonversi_return,
							'rmin_tglinput' => $rmgr_tgldatang,
							'admin_kd' => $this->session->userdata('kd_admin'),
						];
						
						$arrMaterialStok [] = [
							'rmstok_kd' => $rmstok_kd,
							'rm_kd' => $each['rm_kd'],
							'rmin_kd' => $rmin_kd,
							'suplier_kd' => $po['suplier_kd'],
							'rmstok_batch' => $rmgr_tgldatang,
							'rmstokjenis_kd' => '01',
							'rmstok_qty' => $rmgr_qtykonversi_return,
							'rmstok_tglinput' => $rmgr_tglinput,
							'admin_kd' => $this->session->userdata('kd_admin'),
						];

						$dataUpdateStokMaster[] = [
							'rmstokmaster_kd' => $rmstokmaster_kd,
							'qty' => $rmgr_qtykonversi_return,
						];

						/** Urutan rmin_kd */
						$rmin_kd = $rmin_kd + 1;
						/** Urutan rmgr_kd */
						$urutanGR = substr($rmgr_kd, -6);
						$angkaGR = $urutanGR + 1;
						$rmgr_kd = 'MGR'.date('ymd').str_pad($angkaGR, 6, '0', STR_PAD_LEFT);
						/** Urutan rmstok_kd */
						$rmstok_kd = $rmstok_kd + 1;
				endforeach;

				$this->db->trans_begin();

					$actGR = $this->td_rawmaterial_goodsreceive->insert_batch($arrRMgr);
					$actIN = $this->td_rawmaterial_in->insert_batch($arrRMin);
					$actUpdateStokMaster = $this->td_rawmaterial_stok_master->update_stock_batch ('IN', $dataUpdateStokMaster);
					$actUpdateStatusPO = $this->tm_purchaseorder->autoUpdate_status_po($po_kd);
					$actStok = $this->td_rawmaterial_stok->insert_batch($arrMaterialStok);

				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
				}else{
					$this->db->trans_commit();
					$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
				}
			}else {
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Kosong');
				$resp['csrf'] = $this->security->get_csrf_hash();
				echo json_encode($resp);
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		/** u/ update stok */
		$rowIN = $this->td_rawmaterial_in->get_by_param(['rmgr_kd' => $id])->row_array();
		$rowGR = $this->td_rawmaterial_goodsreceive->get_by_param(['rmgr_kd' => $id])->row_array();

		$this->db->trans_start();

		$actUpdateStok = $this->td_rawmaterial_stok_master->update_stok($rowIN['rmstokmaster_kd'], 'OUT', $rowIN['rmin_qty']);
		$actDelIN = $this->td_rawmaterial_in->delete_data($rowIN['rmin_kd']);
		$actDelGR = $this->td_rawmaterial_goodsreceive->delete_data($id);
		
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}else{
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
		}

		echo json_encode($resp);
	}
	
}
