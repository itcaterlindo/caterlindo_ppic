<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Report_opname extends MY_Controller {
	private $class_link = 'inventory/report/report_opname';
	private $form_errs = array('idErrTglOpname');

	public function __construct() {
		parent::__construct();

		$this->load->helper(array('my_helper', 'html', 'form'));
		$this->load->model(array('db_caterlindo/tm_stock', 'db_caterlindo/td_opname'));
		$this->load->library(array('form_validation'));
	}

	public function index() {
		parent::administrator();
		parent::datetimepicker_assets();
		$this->get_form();
	}

	public function get_form() {
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = $this->form_errs;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function open_form() {
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = $this->form_errs;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function send_data() {
		if ($this->input->is_ajax_request()) :
			$this->form_validation->set_rules($this->td_opname->form_rules());
			if ($this->form_validation->run() == FALSE) :
				$str = $this->td_opname->build_warning($this->form_errs);
				$str['confirm'] = 'error';
			else :
				$tgl_opname = str_replace('-', '', $this->input->post('txtTglOpname'));
				$str['kd_opname'] = $tgl_opname;
				$str['tgl_opname'] = $this->input->post('txtTglOpname');
				$str['confirm'] = 'success';
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function get_detail() {
		$data['class_link'] = $this->class_link;
		$data['kd_opname'] = $this->input->get('kd_opname');
		$data['tgl_opname'] = $this->input->get('tgl_opname');
		$this->load->view('page/'.$this->class_link.'/detail_box', $data);
	}

	public function open_detail() {
		$data['report'] = $this->tm_stock->get_report_opname($this->input->get('kd_opname'));
		$data['class_link'] = $this->class_link;
		$data['kd_opname'] = $this->input->get('kd_opname');
		$data['tgl_opname'] = $this->input->get('tgl_opname');
		$this->load->view('page/'.$this->class_link.'/detail_main', $data);
	}
}