<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Report_po extends MY_Controller
{
	private $class_link = 'inventory/report/report_po';
	private $form_errs = array('idErrTglOpname');

	public function __construct()
	{
		parent::__construct();

		$this->load->helper(array('my_helper', 'html', 'form'));
		$this->load->model(array('db_caterlindo/tm_stock', 'tm_salesorder', 'td_salesorder_item', 'td_salesorder_item_detail', 'tm_finishgood', 'tb_finishgood_user', 'tm_gudang', 'td_product_grouping', 'td_do_item'));
		/** set gudang */
		$userfg = $this->tb_finishgood_user->get_by_param(array('kd_tipe_admin' => $this->session->userdata('tipe_admin_kd')))->row_array();
		$this->set_gudang = $userfg['kd_gudang'];
	}

	public function index()
	{
		parent::administrator();
		parent::icheck_assets();
		$this->get_form();
	}

	public function get_form()
	{
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = array('idErrForm');
		$this->load->view('page/' . $this->class_link . '/form_box', $data);
	}

	public function open_form()
	{
		$data['data_mso'] = $this->tm_salesorder->get_by_status(array('pending', 'process_lpo', 'process_wo'))->result();
		$this->load->view('page/' . $this->class_link . '/form_main', $data);
	}

	public function send_data()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$msalesorder_kd = $this->input->post('chkMenu');
		if (!empty($msalesorder_kd)) {
			$msalesorder_kd = implode('_', $msalesorder_kd);
		}

		$data['csrf'] = $this->security->get_csrf_hash();
		$data['msalesorder_kd'] = $msalesorder_kd;

		echo json_encode($data);
	}

	public function get_detail()
	{
		$msalesorder_kd = $this->input->get('kd_mso', true);

		$data['msalesorder_kd'] = $msalesorder_kd;
		$data['class_link'] = $this->class_link;

		$this->load->view('page/' . $this->class_link . '/detail_box', $data);
	}

	public function open_detail()
	{
		$msalesorder_kd = $this->input->get('msalesorder_kd', true);

		if (!empty($msalesorder_kd)) {
			$msalesorder_kd = explode('_', $msalesorder_kd);
			/** ambil rekap sales order */
			$data_mso = array();
			foreach ($msalesorder_kd as $eachMso) {
				$data_mso[$eachMso] = $this->prosesDetailBySO($eachMso);
			}

			$master_mso = $this->tm_salesorder->get_wherein($msalesorder_kd)->result_array();
			$reportFG = $this->db->join('tm_finishgood', 'tm_finishgood.barang_kd=tm_barang.kd_barang AND tm_finishgood.kd_gudang = "'.$this->set_gudang.'"', 'left')
				->select('tm_barang.kd_barang as barang_kd, tm_barang.deskripsi_barang, tm_barang.dimensi_barang, tm_barang.item_code,
				tm_finishgood.fg_qty, tm_barang.item_barcode')
				->get('tm_barang')->result_array();

			$data['master_mso'] = $master_mso;
			$data['data_mso'] = $data_mso;
			$data['report'] = $reportFG;
		}

		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/detail_main', $data);
	}

	private function prosesDetailBySO($kd_msalesorder)
	{
		/** item */
		$resultItemSO = $this->td_salesorder_item->get_items_rekap(array($kd_msalesorder))->result_array();
		/** detail item */
		$resultItemDetailSO = $this->td_salesorder_item_detail->get_items_rekap(array($kd_msalesorder))->result_array();
		/**merge all item SO */
		$marge = array_merge($resultItemSO, $resultItemDetailSO);

		/** get DO */
		$queryDO = $this->td_do_item->get_from_so($kd_msalesorder);

		/** kalkulasi antara gabungan dari SO detail item
		 * dikurangi dengan item yang sudah di DO kan
		 */
		$arrParent = array();
		$arrChild = array();
		foreach ($marge as $eachMargeSO) {
			foreach ($queryDO as $ee) {
				/** Parent */
				if ($eachMargeSO['child_kd'] == '-' && ($ee->parent_kd ==  $eachMargeSO['parent_kd']) && $ee->child_kd == null) {
					$arrParent[] = $eachMargeSO['parent_kd'];
					$arrAfterDO[] = array(
						'msalesorder_kd' => $eachMargeSO['msalesorder_kd'],
						'barang_kd' => $eachMargeSO['barang_kd'],
						'item_code' => $eachMargeSO['item_code'],
						'item_barcode' => $eachMargeSO['item_barcode'],
						'item_status' => $eachMargeSO['item_status'],
						'item_desc' => $eachMargeSO['item_desc'],
						'item_dimension' => $eachMargeSO['item_dimension'],
						'item_note' => $eachMargeSO['item_note'],
						'item_qty' => $eachMargeSO['item_qty'] - $ee->stuffing_item_qty,
						'kd_mgrouping' => $eachMargeSO['kd_mgrouping'],
						'parent_kd' => $eachMargeSO['parent_kd'],
					);
				}
				/** Child */
				elseif ($eachMargeSO['parent_kd'] == '-' && ($ee->child_kd ==  $eachMargeSO['child_kd'])) {
					$arrChild[] = $eachMargeSO['child_kd'];
					$arrAfterDO[] = array(
						'msalesorder_kd' => $eachMargeSO['msalesorder_kd'],
						'barang_kd' => $eachMargeSO['barang_kd'],
						'item_code' => $eachMargeSO['item_code'],
						'item_barcode' => $eachMargeSO['item_barcode'],
						'item_status' => $eachMargeSO['item_status'],
						'item_desc' => $eachMargeSO['item_desc'],
						'item_dimension' => $eachMargeSO['item_dimension'],
						'item_note' => $eachMargeSO['item_note'],
						'item_qty' => $eachMargeSO['item_qty'] - $ee->stuffing_item_qty,
						'kd_mgrouping' => $eachMargeSO['kd_mgrouping'],
						'parent_kd' => $eachMargeSO['parent_kd'],
					);
				}
			}
		}

		foreach ($marge as $eachMargeSO2) {
			if (in_array($eachMargeSO2['parent_kd'], $arrParent)) {
				continue;
			} elseif (in_array($eachMargeSO2['child_kd'], $arrChild)) {
				continue;
			}
			$arrAfterDO[] = array(
				'msalesorder_kd' => $eachMargeSO2['msalesorder_kd'],
				'barang_kd' => $eachMargeSO2['barang_kd'],
				'item_code' => $eachMargeSO2['item_code'],
				'item_barcode' => $eachMargeSO2['item_barcode'],
				'item_status' => $eachMargeSO2['item_status'],
				'item_desc' => $eachMargeSO2['item_desc'],
				'item_dimension' => $eachMargeSO2['item_dimension'],
				'item_note' => $eachMargeSO2['item_note'],
				'item_qty' => $eachMargeSO2['item_qty'],
				'kd_mgrouping' => $eachMargeSO2['kd_mgrouping'],
				'parent_kd' => $eachMargeSO2['parent_kd'],
			);
		}

		/** End perhitungan antaran SO dikurangi dengan 
		 * jumlah qty yang sudah di DO kan 
		 */

		/** find grouping */
		$groupingKd = array();
		foreach ($arrAfterDO as $eGrouping) {
			$key = $eGrouping['kd_mgrouping'];
			if (!empty($key)) {
				if (!in_array($key, $groupingKd)) {
					$groupingKd[] = $eGrouping['kd_mgrouping'];
				}
			}
		}

		/** Jika ada grouping */
		if (!empty($groupingKd)) {
			/** get grouping kd */
			$arrGrouping = $this->td_product_grouping->get_item($groupingKd);
			/** Result after grouping */
			foreach ($arrAfterDO as $e) {
				/** tidak ada grouping */
				if (empty($e['kd_mgrouping'])) {
					$afterGrouping[] = array(
						'msalesorder_kd' => $e['msalesorder_kd'],
						'barang_kd' => $e['barang_kd'],
						'item_code' => $e['item_code'],
						'item_barcode' => $e['item_barcode'],
						'item_status' => $e['item_status'],
						'item_desc' => $e['item_desc'],
						'item_dimension' => $e['item_dimension'],
						'item_note' => $e['item_note'],
						'item_qty' => $e['item_qty'],
						'kd_mgrouping' => $e['kd_mgrouping'],
					);
				} else {
					/** ada grouping */
					foreach ($arrGrouping as $eachGrouping) {
						if ($e['kd_mgrouping'] == $eachGrouping->mgrouping_kd) {
							$afterGrouping[] = array(
								'msalesorder_kd' => $e['msalesorder_kd'],
								'barang_kd' => $eachGrouping->barang_kd,
								'item_code' => $eachGrouping->item_code,
								'item_barcode' => $eachGrouping->item_barcode,
								'item_status' => $e['item_status'],
								'item_desc' => $eachGrouping->deskripsi_barang,
								'item_dimension' => $eachGrouping->dimensi_barang,
								'item_note' => $e['item_note'],
								'item_qty' => $e['item_qty'],
								'kd_mgrouping' => $e['kd_mgrouping'],
							);
						}
					}
				}
			}
		} else {
			/** tidak ada grouping sama sekali */
			$afterGrouping = $arrAfterDO;
		}



		/** group by item product after grouping */
		$groupByAfter = array();
		foreach ($afterGrouping as $eachAfter) {
			$key = $eachAfter['barang_kd'];
			/** jika tidak ada key */
			if (!array_key_exists($key, $groupByAfter)) {
				$groupByAfter[$key] = array(
					'msalesorder_kd' => $eachAfter['msalesorder_kd'],
					'barang_kd' => $eachAfter['barang_kd'],
					'item_code' => $eachAfter['item_code'],
					'item_barcode' => $eachAfter['item_barcode'],
					'item_status' => $eachAfter['item_status'],
					'item_desc' => $eachAfter['item_desc'],
					'item_dimension' => $eachAfter['item_dimension'],
					'item_note' => $eachAfter['item_note'],
					'item_qty' => $eachAfter['item_qty'],
					// 'kd_mgrouping' => $eachAfter['kd_mgrouping'],
				);
			} else {
				$groupByAfter[$key]['item_qty'] = $groupByAfter[$key]['item_qty'] + $eachAfter['item_qty'];
			}
		}

		foreach ($groupByAfter as $each_groupByAfter) {
			/** detail so item after grouping groupby kdbarang */
			$groupResult[] = $each_groupByAfter;
		}

		return $groupResult;
	}


	/** Open Form Lama */
	// public function open_form() {
	// 	$data['data_mso'] = $this->tm_salesorder->get_unfinish_mso();
	// 	$this->load->view('page/'.$this->class_link.'/form_main', $data);
	// }

	/** send data lama */
	// public function send_data() {
	// 	if ($this->input->is_ajax_request()) :
	// 		$_SESSION['modul_report_po']['kd_mso'] = $this->input->post('chkMenu');
	// 		$str['csrf'] = $this->security->get_csrf_hash();

	// 		header('Content-Type: application/json');
	// 		echo json_encode($str);
	// 	endif;
	// }

	/** Open detail lama */
	// public function open_detail() {
	// 	// $data['report'] = $this->tm_stock->get_report_stock();
	// 	$data['report'] = $this->tm_finishgood->get_where_kd_gudang($this->set_gudang)->result();
	// 	$data['data_mso'] = $this->tm_salesorder->get_unfinish_mso($_SESSION['modul_report_po']['kd_mso']);
	// 	$arr_kdmso = extract_column($data['data_mso'], 'kd_msalesorder');
	// 	$data['item_parent'] = $this->td_salesorder_item->get_items($arr_kdmso);
	// 	$data['item_child'] = $this->td_salesorder_item_detail->get_items($arr_kdmso);
	// 	$data['class_link'] = $this->class_link;
	// 	$this->load->view('page/'.$this->class_link.'/detail_main', $data);
	// }

	// function temp(){
	// 	/** item */
	// 	$resultItemSO = $this->td_salesorder_item->get_items_rekap(array('MSO120919000001'))->result_array();
	// 	/** detail item */
	// 	$resultItemDetailSO = $this->td_salesorder_item_detail->get_items_rekap(array('MSO120919000001'))->result_array();
	// 	/**merge all item SO */
	// 	$marge = array_merge($resultItemSO, $resultItemDetailSO);

	// 	/** get DO */
	// 	$resultDO = array();
	// 	$queryDO = $this->td_do_item->get_from_so('MSO120919000001');

	// 	/** kalkulasi antara gabungan dari SO detail item
	// 	 * dikurangi dengan item yang sudah di DO kan
	// 	 */
	// 	foreach ($marge as $eachMargeSO){
	// 		foreach ($queryDO as $ee){
	// 			/** Parent */
	// 			if ($eachMargeSO['child_kd'] == '-' && ($ee->parent_kd ==  $eachMargeSO['parent_kd'] ) && $ee->child_kd == null ){
	// 				$arrParent [] = $eachMargeSO['parent_kd'];
	// 				$arrAfterDO[] = array(
	// 					'msalesorder_kd' => $eachMargeSO['msalesorder_kd'],
	// 					'barang_kd' => $eachMargeSO['barang_kd'],
	// 					'item_code' => $eachMargeSO['item_code'],
	// 					'item_barcode' => $eachMargeSO['item_barcode'],
	// 					'item_status' => $eachMargeSO['item_status'],
	// 					'item_desc' => $eachMargeSO['item_desc'],
	// 					'item_dimension' => $eachMargeSO['item_dimension'],
	// 					'item_note' => $eachMargeSO['item_note'],
	// 					'item_qty' => $eachMargeSO['item_qty'] - $ee->stuffing_item_qty,
	// 					'kd_mgrouping' => $eachMargeSO['kd_mgrouping'],
	// 					'parent_kd' => $eachMargeSO['parent_kd'],
	// 				);
	// 			}
	// 			/** Child */
	// 			elseif($eachMargeSO['parent_kd'] == '-' && ($ee->child_kd ==  $eachMargeSO['child_kd'] )){
	// 				$arrChild [] = $eachMargeSO['child_kd'];					
	// 				$arrAfterDO[] = array(
	// 					'msalesorder_kd' => $eachMargeSO['msalesorder_kd'],
	// 					'barang_kd' => $eachMargeSO['barang_kd'],
	// 					'item_code' => $eachMargeSO['item_code'],
	// 					'item_barcode' => $eachMargeSO['item_barcode'],
	// 					'item_status' => $eachMargeSO['item_status'],
	// 					'item_desc' => $eachMargeSO['item_desc'],
	// 					'item_dimension' => $eachMargeSO['item_dimension'],
	// 					'item_note' => $eachMargeSO['item_note'],
	// 					'item_qty' => $eachMargeSO['item_qty'] - $ee->stuffing_item_qty,
	// 					'kd_mgrouping' => $eachMargeSO['kd_mgrouping'],
	// 					'parent_kd' => $eachMargeSO['parent_kd'],
	// 				);
	// 			}
	// 		}
	// 	}

	// 	foreach ($marge as $eachMargeSO2){
	// 		if ( in_array($eachMargeSO2['parent_kd'], $arrParent) ){
	// 			continue;
	// 		}elseif( in_array($eachMargeSO2['child_kd'], $arrChild) ){
	// 			continue;
	// 		} 
	// 		$arrAfterDO[] = array(
	// 			'msalesorder_kd' => $eachMargeSO2['msalesorder_kd'],
	// 			'barang_kd' => $eachMargeSO2['barang_kd'],
	// 			'item_code' => $eachMargeSO2['item_code'],
	// 			'item_barcode' => $eachMargeSO2['item_barcode'],
	// 			'item_status' => $eachMargeSO2['item_status'],
	// 			'item_desc' => $eachMargeSO2['item_desc'],
	// 			'item_dimension' => $eachMargeSO2['item_dimension'],
	// 			'item_note' => $eachMargeSO2['item_note'],
	// 			'item_qty' => $eachMargeSO2['item_qty'],
	// 			'kd_mgrouping' => $eachMargeSO2['kd_mgrouping'],
	// 			'parent_kd' => $eachMargeSO2['parent_kd'],
	// 		);
	// 	}

	// 	/** End perhitungan antaran SO dikurangi dengan 
	// 	 * jumlah qty yang sudah di DO kan 
	// 	 */

	// 	/** find grouping */
	// 	$groupingKd = array();
	// 	foreach ($arrAfterDO as $eGrouping){
	// 		$key = $eGrouping['kd_mgrouping'];
	// 		if (!empty($key)){
	// 			if (!in_array($key, $groupingKd)) {
	// 				$groupingKd[] = $eGrouping['kd_mgrouping'];
	// 			}
	// 		}
	// 	}

	// 	/** get grouping kd */
	// 	$arrGrouping = $this->td_product_grouping->get_item($groupingKd);
	// 	/** Result after grouping */
	// 	foreach ($arrAfterDO as $e){
	// 		/** tidak ada grouping */
	// 		if (empty($e['kd_mgrouping'])){
	// 			$afterGrouping[] = array(
	// 				'msalesorder_kd' => $e['msalesorder_kd'], 
	// 				'barang_kd' => $e['barang_kd'], 
	// 				'item_code' => $e['item_code'], 
	// 				'item_barcode' => $e['item_barcode'], 
	// 				'item_status' => $e['item_status'], 
	// 				'item_desc' => $e['item_desc'], 
	// 				'item_dimension' => $e['item_dimension'], 
	// 				'item_note' => $e['item_note'], 
	// 				'item_qty' => $e['item_qty'], 
	// 				'kd_mgrouping' => $e['kd_mgrouping'], 
	// 			);
	// 		}else{
	// 			/** ada grouping */
	// 			foreach($arrGrouping as $eachGrouping){
	// 				if ($e['kd_mgrouping'] == $eachGrouping->mgrouping_kd){
	// 					$afterGrouping[] = array(
	// 						'msalesorder_kd' => $e['msalesorder_kd'], 
	// 						'barang_kd' => $eachGrouping->barang_kd, 
	// 						'item_code' => $eachGrouping->item_code, 
	// 						'item_barcode' => $eachGrouping->item_barcode, 
	// 						'item_status' => $e['item_status'], 
	// 						'item_desc' => $eachGrouping->deskripsi_barang, 
	// 						'item_dimension' => $eachGrouping->dimensi_barang, 
	// 						'item_note' => $e['item_note'], 
	// 						'item_qty' => $e['item_qty'], 
	// 						'kd_mgrouping' => $e['kd_mgrouping'], 
	// 					);
	// 				}
	// 			}
	// 		}
	// 	}
	// 	/** group by item product after grouping */
	// 	$groupByAfter = array();
	// 	foreach ($afterGrouping as $eachAfter ){
	// 		$key = $eachAfter['barang_kd'];
	// 		/** jika tidak ada key */
	// 		if (!array_key_exists($key, $groupByAfter)) {
	// 			$groupByAfter[$key] = array(
	// 				'msalesorder_kd' => $eachAfter['msalesorder_kd'], 
	// 				'barang_kd' => $eachAfter['barang_kd'], 
	// 				'item_code' => $eachAfter['item_code'], 
	// 				'item_barcode' => $eachAfter['item_barcode'], 
	// 				'item_status' => $eachAfter['item_status'], 
	// 				'item_desc' => $eachAfter['item_desc'], 
	// 				'item_dimension' => $eachAfter['item_dimension'],
	// 				'item_note' => $eachAfter['item_note'],
	// 				'item_qty' => $eachAfter['item_qty'],
	// 				// 'kd_mgrouping' => $eachAfter['kd_mgrouping'],
	// 			);
	// 		} else {
	// 			$groupByAfter[$key]['item_qty'] = $groupByAfter[$key]['item_qty'] + $eachAfter['item_qty'];
	// 		}
	// 	}

	// 	foreach ($groupByAfter as $each_groupByAfter){
	// 		/** detail so item after grouping groupby kdbarang */
	// 		$groupResult[] = $each_groupByAfter;
	// 	}

	// 	echo json_encode($groupResult);
	// }

}
