<?php
defined('BASEPATH') or exit('No direct script access allowed!');

require_once APPPATH."third_party/phpspreadsheets/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Report_stock extends MY_Controller {
	private $class_link = 'inventory/report/report_stock';
	private $form_errs = array('idErrTglOpname');

	public function __construct() {
		parent::__construct();

		$this->load->helper(array('my_helper', 'html', 'form'));
		$this->load->model(array('db_caterlindo/tm_stock', 'db_caterlindo/td_opname', 'tm_finishgood', 'td_finishgood_in', 'tb_finishgood_user', 'tm_gudang'));
		$this->load->library(array('form_validation'));
		/** set gudang */
		$userfg = $this->tb_finishgood_user->get_by_param (array('kd_tipe_admin' => $this->session->userdata('tipe_admin_kd')))->row_array();
		$this->set_gudang = $userfg['kd_gudang'];
	}

	public function index() {
		parent::administrator();
		parent::datetimepicker_assets();
		$this->get_detail();
	}

	public function get_detail() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/detail_box', $data);
	}

	private function rekap_stok_fg_today(){
		$actFG = $this->tm_finishgood->get_where_kd_gudang($this->set_gudang)->result_array();
		$actIN = $this->td_finishgood_in->get_rekap_fgin_blm_out($this->set_gudang)->result_array();

		foreach ($actFG as $eachFG){
			$qtyStock = 0;
			$selisih = 0;
			foreach ($actIN as $eachIN){
				if ($eachIN['fg_kd'] == $eachFG['fg_kd'] ){
					$qtyStock = $eachIN['qtyStock'];
				}
			}
			$result [] = array(
				'item_code' => $eachFG['item_code'],
				'nama_stock' => $eachFG['deskripsi_barang'].' '.$eachFG['dimensi_barang'],
				'qtyData' => $eachFG['fg_qty'],
				'qtyStock' => $qtyStock,				
			);
		}

		return $result;
	}

	public function open_detail() {
		$actGudang = $this->tm_gudang->get_row($this->set_gudang);
		$data['nm_gudang'] = $actGudang->nm_gudang;
		$data['report'] = $this->rekap_stok_fg_today();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/detail_main', $data);
	}

	public function report_today_baru () {
		$act = $this->rekap_stok_fg_today();
		
		$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()
			->setCreator($_SESSION['nm_admin'])
			->setLastModifiedBy($_SESSION['nm_admin'])
			->setTitle('Report Stock '.date('d-M-Y'))
			->setSubject('Report Stock')
			->setCategory('Report');
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		$sheet = $spreadsheet->getActiveSheet();
		$sheet = $spreadsheet->getActiveSheet();
		$header_cell = 1;
		$sheet->setCellValue('A'.$header_cell, 'No')->getStyle('A'.$header_cell);
		$sheet->setCellValue('B'.$header_cell, 'Product Code')->getStyle('B'.$header_cell);
		$sheet->setCellValue('C'.$header_cell, 'Nama Barang')->getStyle('C'.$header_cell);
		$sheet->setCellValue('D'.$header_cell, 'QtyData')->getStyle('D'.$header_cell);
		$sheet->setCellValue('E'.$header_cell, 'QtyStock')->getStyle('E'.$header_cell);

		$dataCell = 2;
		$no = 1;

		foreach ($act as $each):
			$sheet->setCellValue('A'.$dataCell, $no)->getStyle('A'.$dataCell);
			$sheet->setCellValue('B'.$dataCell, $each['item_code'])->getStyle('B'.$dataCell);
			$sheet->setCellValue('C'.$dataCell, $each['nama_stock'])->getStyle('C'.$dataCell);
			$sheet->setCellValue('D'.$dataCell, $each['qtyData'])->getStyle('D'.$dataCell);
			$sheet->setCellValue('E'.$dataCell, $each['qtyStock'])->getStyle('E'.$dataCell);

			$dataCell++;
			$no++;
		endforeach;


		$filename = 'report_stock_baru_'.date('d-M-Y').'.xlsx';
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='. $filename .'');
		header('Cache-Control: max-age=0');
		$writer->save('php://output');

	}

	// public function open_detail() {
	// 	$data['report'] = $this->tm_stock->get_report_stock();
	// 	$data['class_link'] = $this->class_link;
	// 	$data['kd_opname'] = $this->input->get('kd_opname');
	// 	$data['tgl_opname'] = $this->input->get('tgl_opname');
	// 	$this->load->view('page/'.$this->class_link.'/detail_main', $data);
	// }
	
}