<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Report_barangkeluar_fg extends MY_Controller {
	private $class_link = 'inventory/report/report_barangkeluar_fg';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_finishgood_barcode', 'td_finishgood_out', 'tm_finishgood', 
		'tm_salesorder', 'td_salesorder_item', 'td_salesorder_item_detail','tm_gudang', 
		'td_product_grouping', 'td_finishgood_in', 'td_finishgood_adjustment', 'td_finishgood_repacking', 'tm_finishgood_stokopname']);
		/** Cek Lokasi User */
		$userfg = $this->tb_finishgood_user->get_by_param (array('kd_tipe_admin' => $this->session->userdata('tipe_admin_kd')))->row_array();
		$this->set_gudang = $userfg['kd_gudang'];
	}

	public function index() {
		parent::administrator();
		$this->form_box();
	}

	public function form_box() {
		parent::pnotify_assets();
		parent::ionicons_assets();
		parent::select2_assets();

		$actGudang = $this->tm_gudang->get_row($this->set_gudang);
		$data['nm_gudang'] = $actGudang->nm_gudang;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		/** SO finish */
		$actSO = $this->tm_salesorder->get_by_status(array('finish'))->result_array();
		foreach ($actSO as $eachSO):
			if ($eachSO['tipe_customer'] == 'Lokal'){
				$namaSO = $eachSO['no_salesorder'].' | '.$eachSO['nm_customer'];
			}else{
				$namaSO = $eachSO['no_po'].' | '.$eachSO['nm_customer'];
			}
			$opsiSO[$eachSO['kd_msalesorder']] = $namaSO;
		endforeach;

		$data['opsiSO'] = $opsiSO;

		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$kd_msalesorder = $this->input->get('kd_msalesorder', true);

		$actGudang = $this->tm_gudang->get_row($this->set_gudang);
		$data['nm_gudang'] = $actGudang->nm_gudang;
		$data['class_link'] = $this->class_link;
		$data['js_file'] = 'table_js'; 
		$data['kd_msalesorder'] = $kd_msalesorder;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$kd_msalesorder = $this->input->get('kd_msalesorder', true);

		if (!empty($kd_msalesorder)){
			$actIdentitasSO = $this->tm_salesorder->get_by_id($kd_msalesorder)->row_array();
			$resultData = $this->td_finishgood_out->get_by_mso_gdg($kd_msalesorder, $this->set_gudang)->result_array();

			$data['class_link'] = $this->class_link;
			$data['kd_msalesorder'] = $kd_msalesorder;
			$data['identitasSO'] = $actIdentitasSO;
			$data['resultData'] = $resultData;
		}
		
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function detail_salesorder_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$kd_msalesorder = $this->input->get('kd_msalesorder', true);

		$data['class_link'] = $this->class_link;
		$data['kd_msalesorder'] = $kd_msalesorder;

		$this->load->view('page/'.$this->class_link.'/detail_salesorder_box', $data);
	}

	private function groupingDetailSO($data){
		$groups = array();
		foreach ($data as $item) {
			$key = $item['barang_kd'];
			/** jika tidak ada key */
			if (!array_key_exists($key, $groups)) {
				$groups[$key] = array(
					'kd_msalesorder' => $item['kd_msalesorder'],
					'no_salesorder' => $item['no_salesorder'],
					'barang_kd' => $key,
					'item_barcode' => $item['item_barcode'],
					'item_code' => $item['item_code'],
					'item_status' => $item['item_status'],
					'qty' => $item['qty'],
					'deskripsi_barang' => $item['deskripsi_barang'],
					'dimensi_barang' => $item['dimensi_barang'],
					'kd_mgrouping' => $item['kd_mgrouping'],
				);
			} else {
				$groups[$key]['qty'] = $groups[$key]['qty'] + $item['qty'];
			}
		}
		
		foreach ($groups as $each){
			$groupResult[] = $each;
		}
		return $groupResult;
	}

	public function detail_salesorder_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$kd_msalesorder = $this->input->get('kd_msalesorder', true);

		/** Untuk rekap SO dan grouping */
		$actItemSO_rekap = $this->tm_salesorder->get_rekap_itemstatus_by_nosalesorder($kd_msalesorder);
		$itemSO_rekap_grouping = array();
		foreach($actItemSO_rekap as $eachSO):
			if($eachSO['kd_mgrouping']==null){
				$itemSO_rekap_grouping [] = $eachSO;
			}else{
				$no_salesorder = $eachSO['no_salesorder'];
				$actGrouping = (array) $this->td_product_grouping->get_item($eachSO['kd_mgrouping']);
				foreach ($actGrouping as $eachGrouping):
					$aDetilGrouping[] = array(
						'kd_msalesorder' => $kd_msalesorder,
						'no_salesorder' => $no_salesorder,
						'barang_kd' => $eachGrouping->barang_kd,
						'item_barcode' => $eachGrouping->item_barcode,
						'item_code' => $eachGrouping->item_code,
						'item_status' => 'std',
						'qty' => $eachSO['qty'],
						'deskripsi_barang' => $eachGrouping->deskripsi_barang,
						'dimensi_barang' => $eachGrouping->dimensi_barang,
						'kd_mgrouping' => null,
					);
				endforeach;
			}
		endforeach;
		/** Rekap SO sesuai dengan sales order kode*/
		/** Cek apakah ada grouping  */
		if (!empty($aDetilGrouping)){
			$RekapSO_grouping = array_merge($itemSO_rekap_grouping, $aDetilGrouping);
			$RekapSO_grouping = $this->groupingDetailSO($RekapSO_grouping);
		}else{
			$RekapSO_grouping = $itemSO_rekap_grouping;
		}

		$actFGOut = $this->td_finishgood_out->get_rekap_by_kd_msalesorder ($kd_msalesorder)->result_array();
		$actIdentitasSO = $this->tm_salesorder->get_by_id($kd_msalesorder)->row_array();	

		$data['class_link'] = $this->class_link;
		$data['kd_msalesorder'] = $kd_msalesorder;
		$data['item_so_grouping'] = $RekapSO_grouping;		
		$data['fg_out'] = $actFGOut;
		$data['identitasSO'] = $actIdentitasSO;

		$this->load->view('page/'.$this->class_link.'/detail_salesorder_main', $data);
	}	
}
