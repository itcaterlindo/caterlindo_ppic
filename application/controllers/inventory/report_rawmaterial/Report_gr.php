<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Report_gr extends MY_Controller {
	private $class_link = 'inventory/report_rawmaterial/report_gr';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_helper']);
		// $this->load->model(['']);
    }

    public function index() {
        parent::administrator();
        parent::datetimepicker_assets();
        $this->table_box();
    }
	 
	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
        // }
        $dari = $this->input->get('dari', true);
        $sampai = $this->input->get('sampai', true);
        $kurs = $this->input->get('kurs', true);

        $dari = format_date($dari, 'Y-m-d');
        $sampai = format_date($sampai, 'Y-m-d');

        ///hrg konversi = hrg qty knvrs x qty knvrs lalu di jumlaAH

        // $rmgrdetails = $this->db->select('SUM(a.rmgr_qtykonversi) as total_qty_conf, SUM(a.rmgr_hargakonversi) as total_hrg_conf,a.*, b.*, c.po_no, d.suplier_nama, e.rm_kode, e.rm_oldkd, f.nm_select, g.rmsatuan_nama, 
        //     h.whdeliveryorderdetail_deskripsi, h.whdeliveryorderdetail_spesifikasi, i.rmsatuan_nama as rmsatuan_whdodetail, a.rmgr_code_srj')
        //             ->from('td_rawmaterial_goodsreceive as a')
        //             ->join('td_purchaseorder_detail as b', 'a.podetail_kd=b.podetail_kd', 'left')
        //             ->join('tm_purchaseorder as c', 'b.po_kd=c.po_kd', 'left')
        //             ->join('tm_suplier as d', 'c.suplier_kd=d.suplier_kd', 'left')
        //             ->join('tm_rawmaterial as e', 'a.rm_kd=e.rm_kd', 'left')
        //             ->join('tb_set_dropdown as f', 'd.badanusaha_kd=f.id', 'left')
        //             ->join('td_rawmaterial_satuan as g', 'b.rmsatuan_kd=g.rmsatuan_kd', 'left')
        //             ->join('td_whdeliveryorder_detail as h', 'a.whdeliveryorderdetail_kd=h.whdeliveryorderdetail_kd', 'left')
        //             ->join('td_rawmaterial_satuan as i', 'h.whdeliveryorderdetail_satuankd=i.rmsatuan_kd', 'left')
        //             ->where('DATE(a.rmgr_tgldatang) >=', $dari)
        //             ->where('DATE(a.rmgr_tgldatang) <=', $sampai)
		// 	        ->group_by('a.rm_kd, a.kd_currency')
        //             ->order_by('a.rmgr_tgldatang', 'asc')
        //             ->get()->result_array();

        // Subquery  
        $sql = "  
            SELECT   
                SUM(qty_conf) as total_qty_conf,  
                SUM(total_hrg_conf_perGR) as total_hrg_conf,  
                rm_kode,  
                kd_currency,  
                rm_spesifikasi,  
                rm_deskripsi  
            FROM  
            (  
                SELECT   
                    a.rmgr_qtykonversi as qty_conf,  
                    a.rmgr_hargakonversi * a.rmgr_qtykonversi as total_hrg_conf_perGR,  
                    e.rm_kode,  
                    e.rm_spesifikasi,  
                    e.rm_deskripsi,  
                    a.kd_currency 
                FROM   
                    td_rawmaterial_goodsreceive AS a  
                LEFT JOIN td_purchaseorder_detail AS b ON a.podetail_kd = b.podetail_kd  
                LEFT JOIN tm_purchaseorder AS c ON b.po_kd = c.po_kd  
                LEFT JOIN tm_suplier AS d ON c.suplier_kd = d.suplier_kd    
                LEFT JOIN tm_rawmaterial AS e ON a.rm_kd = e.rm_kd  
                LEFT JOIN tb_set_dropdown AS f ON d.badanusaha_kd = f.id  
                LEFT JOIN td_rawmaterial_satuan AS g ON b.rmsatuan_kd = g.rmsatuan_kd  
                LEFT JOIN td_whdeliveryorder_detail AS h ON a.whdeliveryorderdetail_kd = h.whdeliveryorderdetail_kd  
                LEFT JOIN td_rawmaterial_satuan AS i ON h.whdeliveryorderdetail_satuankd = i.rmsatuan_kd  
                WHERE   
                    DATE(a.rmgr_tgldatang) >= '$dari' AND DATE(a.rmgr_tgldatang) <= '$sampai' AND e.inventory_item = 'Y'
            ) AS XXI   
            GROUP BY   
                rm_kode  
        ";  

        $query = $this->db->query($sql);  
        $rmgrdetails = $query->result_array();   


        

		$data['class_link'] = $this->class_link;
        $data['result_grdetail'] = $rmgrdetails;
        $data['kurs'] = $kurs;

        
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }    	
}
