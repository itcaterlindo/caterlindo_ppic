<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Reportrm_outgoingmaterial extends MY_Controller {
	private $class_link = 'inventory/report_rawmaterial/reportrm_outgoingmaterial';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_helper']);
		$this->load->model(['db_hrm/tb_karyawan']);
    }

    public function index() {
        parent::administrator();
        parent::datetimepicker_assets();
        $this->table_box();
    }
	 
	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        $dari = $this->input->get('dari', true);
        $sampai = $this->input->get('sampai', true);

        $dari = format_date($dari, 'Y-m-d');
        $sampai = format_date($sampai, 'Y-m-d');

        $whdodetails = $this->db->select('a.*, b.*, c.suplier_nama, d.rm_oldkd, d.rm_kode, e.rmsatuan_nama')
                ->where('b.whdeliveryorder_tanggal >=', $dari)
                ->where('b.whdeliveryorder_tanggal <=', $sampai)
                ->where('f.wfstate_nama !=', 'Cancel')
                ->join('tm_whdeliveryorder as b', 'a.whdeliveryorder_kd=b.whdeliveryorder_kd', 'left')
                ->join('tm_suplier as c', 'b.suplier_kd=c.suplier_kd', 'left')
                ->join('tm_rawmaterial as d', 'a.rm_kd=d.rm_kd', 'left')
                ->join('td_rawmaterial_satuan as e', 'a.whdeliveryorderdetail_satuankd=e.rmsatuan_kd', 'left')
                ->join('td_workflow_state as f', 'b.wfstate_kd=f.wfstate_kd', 'left')
                ->order_by('b.whdeliveryorder_no', 'desc')
                ->get('td_whdeliveryorder_detail as a')->result_array();
        
        $kd_karyawans = array_values(array_filter(array_unique(array_column($whdodetails, 'kd_karyawan'))));
        if (!empty($kd_karyawans)) {
            $karyawans = $this->tb_karyawan->get_where_in('kd_karyawan', $kd_karyawans)->result_array();
        }
        
        $whresults = [];
        foreach ($whdodetails as $whdodetail) {
            $nm_karyawan = '';
            if (!empty($kd_karyawans)) {
                foreach($karyawans as $karyawan){
                    if ($karyawan['kd_karyawan'] == $whdodetail['kd_karyawan']){
                        $nm_karyawan = $karyawan['nm_karyawan'];
                    }
                }
            }
            $whresults[] = array_merge($whdodetail, ['nm_karyawan' => $nm_karyawan]);
        }
        /** Return */
        $returns = [];
        if (!empty($whresults)){
            $whdeliveryorderdetail_kds = array_values(array_filter(array_unique(array_column($whresults, 'whdeliveryorderdetail_kd'))));
            $returns = $this->db->select()
                ->where('a.rmgr_type', 'BACK')
                ->where_in('a.whdeliveryorderdetail_kd', $whdeliveryorderdetail_kds)
                ->get('td_rawmaterial_goodsreceive as a')->result_array();
        }

        $data['whresults'] = $whresults;
        $data['returns'] = $returns;
        $data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }   
    
}
