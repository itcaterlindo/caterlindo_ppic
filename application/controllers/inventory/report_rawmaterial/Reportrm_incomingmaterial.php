<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Reportrm_incomingmaterial extends MY_Controller {
	private $class_link = 'inventory/report_rawmaterial/reportrm_incomingmaterial';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_helper']);
		// $this->load->model(['']);
    }

    public function index() {
        parent::administrator();
        parent::datetimepicker_assets();
        $this->table_box();
    }
	 
	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        $dari = $this->input->get('dari', true);
        $sampai = $this->input->get('sampai', true);

        $dari = format_date($dari, 'Y-m-d');
        $sampai = format_date($sampai, 'Y-m-d');

        $rmgrdetails = $this->db->select('a.*, b.*, c.po_no, d.suplier_nama, e.rm_kode, e.rm_oldkd, f.nm_select, g.rmsatuan_nama, 
            h.whdeliveryorderdetail_deskripsi, h.whdeliveryorderdetail_spesifikasi, i.rmsatuan_nama as rmsatuan_whdodetail, a.rmgr_code_srj')
                    ->from('td_rawmaterial_goodsreceive as a')
                    ->join('td_purchaseorder_detail as b', 'a.podetail_kd=b.podetail_kd', 'left')
                    ->join('tm_purchaseorder as c', 'b.po_kd=c.po_kd', 'left')
                    ->join('tm_suplier as d', 'c.suplier_kd=d.suplier_kd', 'left')
                    ->join('tm_rawmaterial as e', 'a.rm_kd=e.rm_kd', 'left')
                    ->join('tb_set_dropdown as f', 'd.badanusaha_kd=f.id', 'left')
                    ->join('td_rawmaterial_satuan as g', 'b.rmsatuan_kd=g.rmsatuan_kd', 'left')
                    ->join('td_whdeliveryorder_detail as h', 'a.whdeliveryorderdetail_kd=h.whdeliveryorderdetail_kd', 'left')
                    ->join('td_rawmaterial_satuan as i', 'h.whdeliveryorderdetail_satuankd=i.rmsatuan_kd', 'left')
                    ->where('DATE(a.rmgr_tgldatang) >=', $dari)
                    ->where('DATE(a.rmgr_tgldatang) <=', $sampai)
                    ->order_by('a.rmgr_tgldatang', 'asc')
                    ->get()->result_array();

		$data['class_link'] = $this->class_link;
        $data['result_grdetail'] = $rmgrdetails;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }    	
}
