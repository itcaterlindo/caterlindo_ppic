<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class User_deliverymaterial extends MY_Controller {
	private $class_link = 'inventory/manage_inventory/user_deliverymaterial';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tb_deliverymaterial_user', 'td_admin_tipe', 'tm_gudang']);
	}

	public function index() {
		parent::administrator();
		parent::pnotify_assets();
		$this->table_box();
	}

	public function form_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		if ($sts == 'edit'){
			$actdeliverymaterialuser = $this->tb_deliverymaterial_user->get_detail_by_param(array('deliverymaterialuser_kd' => $id))->row_array();
			$data['id'] = $actdeliverymaterialuser['deliverymaterialuser_kd'];
			$data['selectedTipeAdmin'] = $actdeliverymaterialuser['kd_tipe_admin'];
			$data['selectedGudang'] = $actdeliverymaterialuser['kd_gudang'];
			$data['flag_terima'] = $actdeliverymaterialuser['flag_terima'];
		}

		/** tipe admin */
		$actTipeAdmin = $this->td_admin_tipe->get_all()->result_array();
		$arrOpsiTipeAdmin[''] = '--Pilih Tipe Admin--';
		foreach ($actTipeAdmin as $each){
			$arrOpsiTipeAdmin[$each['kd_tipe_admin']] = $each['nm_tipe_admin'];
		}

		/** mst gudang */
		$actGudang = $this->tm_gudang->get_all()->result_array();
		$arrGudang[''] = '--Pilih Gudang--';
		foreach ($actGudang as $each){
			$arrGudang[$each['kd_gudang']] = $each['nm_gudang'];
		}

		$data['sts'] = $sts;
		$data['opsiTipeAdmin'] = $arrOpsiTipeAdmin;
		$data['opsiGudang'] = $arrGudang;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->tb_deliverymaterial_user->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		
		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txttipeAdmin', 'Tipe Admin', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtnmGudang', 'Gudang', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrTipeAdmin' => (!empty(form_error('txttipeAdmin')))?buildLabel('warning', form_error('txttipeAdmin', '"', '"')):'',
				'idErrNmGudang' => (!empty(form_error('txtnmGudang')))?buildLabel('warning', form_error('txtnmGudang', '"', '"')):'',
			);
		}else {
			$kd_tipe_admin = $this->input->post('txttipeAdmin');
			$kd_gudang = $this->input->post('txtnmGudang');
			$flag_terima = $this->input->post('chkTerima');
			
			$data = array(
				'kd_tipe_admin' => $kd_tipe_admin,
				'kd_gudang' => $kd_gudang,
				'flag_terima' => $flag_terima,
				'deliverymaterialuser_tglinput' => date('Y-m-d'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->tb_deliverymaterial_user->insert_data($data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txttipeAdmin', 'Tipe Admin', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtnmGudang', 'Gudang', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrTipeAdmin' => (!empty(form_error('txttipeAdmin')))?buildLabel('warning', form_error('txttipeAdmin', '"', '"')):'',
				'idErrNmGudang' => (!empty(form_error('txtnmGudang')))?buildLabel('warning', form_error('txtnmGudang', '"', '"')):'',
			);
			
		}else {
			$deliverymaterialuser_id = $this->input->post('txtdeliverymaterialuser_kd');
			$kd_tipe_admin = $this->input->post('txttipeAdmin');
			$kd_gudang = $this->input->post('txtnmGudang');
			$flag_terima = $this->input->post('chkTerima');
			
			$data = array(
				'kd_tipe_admin' => $kd_tipe_admin,
				'kd_gudang' => $kd_gudang,
				'flag_terima' => $flag_terima,
				'deliverymaterialuser_tgledit' => date('Y-m-d'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->tb_deliverymaterial_user->update_data (array('deliverymaterialuser_kd' => $deliverymaterialuser_id), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
			$resp['csrf'] = $this->security->get_csrf_hash();
			echo json_encode($resp);
		}
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$deliverymaterialuser_kd = $this->input->get('id', TRUE);
		
		$actDel = $this->tb_deliverymaterial_user->delete_data($deliverymaterialuser_kd);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
