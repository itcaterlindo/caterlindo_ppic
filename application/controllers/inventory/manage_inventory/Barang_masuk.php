<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Barang_masuk extends MY_Controller {
	private $class_link = 'inventory/manage_inventory/barang_masuk';
	private $form_errs = ['idErrNmGudang'];

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
	}

	public function index() {
		parent::administrator();
		$this->get_form_select();
	}

	public function get_form_select() {
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = ['idErrTipeBarang'];
		$this->load->view('page/'.$this->class_link.'/form_select_box', $data);
	}

	public function open_form_select() {
		$data['master_var'] = $this->input->post('master_var');
		$str['csrf_hash'] = $this->security->get_csrf_hash();
		$str['view'] = $this->load->view('page/'.$this->class_link.'/form_select_main', $data, TRUE);

		header('Content-Type: application/json');
		echo json_encode($str);
	}

	public function send_form_select_data() {
		if ($this->input->is_ajax_request()) :
			$this->load->library(['form_validation']);
			$this->load->model(['modul_inventory/m_barang_masuk']);

			$this->form_validation->set_rules($this->m_barang_masuk->form_select_rules());
			if ($this->form_validation->run() == FALSE) :
				$str = $this->m_barang_masuk->form_select_warning(['idErrTipeBarang']);
				$str['confirm'] = 'error';
			else :
				$tipe_barang = $this->input->post('selTipeBarang');

				$data['class_link'] = $this->class_link;

				$str['view'] = $this->load->view('page/'.$this->class_link.'/table_box', $data, TRUE);
				$str['confirm'] = 'success';
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function open_table() {
		$data['class_link'] = $this->class_link;
		$str['csrf_hash'] = $this->security->get_csrf_hash();
		$str['view'] = $this->load->view('page/'.$this->class_link.'/table_main', $data, TRUE);

		header('Content-Type: application/json');
		echo json_encode($str);
	}

	public function table_data() {
		$this->load->library(['ssp']);
		$this->load->model(['modul_inventory/m_barang_masuk']);

		$data = $this->m_barang_masuk->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function get_form() {
		$data['id'] = $this->input->get('id');
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = $this->form_errs;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function open_form() {
		$id = $this->input->get('id');
		$data = $this->tm_gudang->get_data($id);
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function send_data() {
		if ($this->input->is_ajax_request()) :
			$this->form_validation->set_rules($this->tm_gudang->form_rules());
			if ($this->form_validation->run() == FALSE) :
				$str = $this->tm_gudang->form_warning($this->form_errs);
				$str['confirm'] = 'error';
			else :
				$data['kd_gudang'] = $this->input->post('txtKd');
				$data['nm_gudang'] = $this->input->post('txtNmGudang');
				$str = $this->tm_gudang->submit_data($data);
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function delete_data() {
		if ($this->input->is_ajax_request()) :
			$id = $this->input->get('id');
			$str = $this->tm_gudang->delete_data($id);
			
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function data_rak() {
		$id = $this->input->get('id');
		$data = $this->tm_gudang->get_data($id);
		$_SESSION['modul_inventory']['kd_gudang'] = $data['kd_gudang'];
		$_SESSION['modul_inventory']['nm_gudang'] = $data['nm_gudang'];
	}
}
