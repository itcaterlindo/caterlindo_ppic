<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class User_fg extends MY_Controller {
	private $class_link = 'inventory/manage_inventory/user_fg';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_finishgood', 'tb_finishgood_user', 'td_admin_tipe', 'tm_gudang']);
	}

	public function index() {
		parent::administrator();
		parent::pnotify_assets();
		$this->table_box();
	}

	public function form_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		$data['sts'] = $sts;
		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$sts = $this->input->get('sts');
		$id = $this->input->get('id');

		if ($sts == 'edit'){
			$actFGuser = $this->tb_finishgood_user->get_detail_by_param(array('fguser_kd' => $id))->row_array();
			$data['id'] = $actFGuser['fguser_kd'];
			$data['selectedTipeAdmin'] = $actFGuser['kd_tipe_admin'];
			$data['selectedGudang'] = $actFGuser['kd_gudang'];
		}

		/** tipe admin */
		$actTipeAdmin = $this->td_admin_tipe->get_all()->result_array();
		$arrOpsiTipeAdmin[''] = '--Pilih Tipe Admin--';
		foreach ($actTipeAdmin as $each){
			$arrOpsiTipeAdmin[$each['kd_tipe_admin']] = $each['nm_tipe_admin'];
		}

		/** mst gudang */
		$actGudang = $this->tm_gudang->get_all()->result_array();
		$arrGudang[''] = '--Pilih Gudang--';
		foreach ($actGudang as $each){
			$arrGudang[$each['kd_gudang']] = $each['nm_gudang'];
		}

		$data['sts'] = $sts;
		$data['opsiTipeAdmin'] = $arrOpsiTipeAdmin;
		$data['opsiGudang'] = $arrGudang;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$data = $this->tb_finishgood_user->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txttipeAdmin', 'Tipe Admin', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtnmGudang', 'Gudang', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrTipeAdmin' => (!empty(form_error('txttipeAdmin')))?buildLabel('warning', form_error('txttipeAdmin', '"', '"')):'',
				'idErrNmGudang' => (!empty(form_error('txtnmGudang')))?buildLabel('warning', form_error('txtnmGudang', '"', '"')):'',
			);
			
		}else {
			$kd_tipe_admin = $this->input->post('txttipeAdmin');
			$kd_gudang = $this->input->post('txtnmGudang');
			
			$data = array(
				'kd_tipe_admin' => $kd_tipe_admin,
				'kd_gudang' => $kd_gudang,
				'fguser_tglinput' => date('Y-m-d'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->tb_finishgood_user->insert_data($data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
			$resp['csrf'] = $this->security->get_csrf_hash();
			echo json_encode($resp);
		}
	}

	public function action_update() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txttipeAdmin', 'Tipe Admin', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtnmGudang', 'Gudang', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrTipeAdmin' => (!empty(form_error('txttipeAdmin')))?buildLabel('warning', form_error('txttipeAdmin', '"', '"')):'',
				'idErrNmGudang' => (!empty(form_error('txtnmGudang')))?buildLabel('warning', form_error('txtnmGudang', '"', '"')):'',
			);
			
		}else {
			$fguser_id = $this->input->post('txtfguser_kd');
			$kd_tipe_admin = $this->input->post('txttipeAdmin');
			$kd_gudang = $this->input->post('txtnmGudang');
			
			$data = array(
				'kd_tipe_admin' => $kd_tipe_admin,
				'kd_gudang' => $kd_gudang,
				'fguser_tgledit' => date('Y-m-d'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->tb_finishgood_user->update_data (array('fguser_kd' => $fguser_id), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
			$resp['csrf'] = $this->security->get_csrf_hash();
			echo json_encode($resp);
		}
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$fguser_kd = $this->input->get('id', TRUE);
		
		$actDel = $this->tb_finishgood_user->delete_data($fguser_kd);
		if ($actDel == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}
	
}
