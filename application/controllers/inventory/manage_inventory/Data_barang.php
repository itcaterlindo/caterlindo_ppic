<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Data_barang extends MY_Controller {
	private $class_link = 'inventory/manage_inventory/data_barang';
	private $form_errs = array('idErrInputOption', 'idErrTglTransaksi', 'idErrItemBarcode', 'idErrItemCode', 'idErrJmlItem');

	public function __construct() {
		parent::__construct();

		$this->load->library(array('ssp', 'form_validation'));
		$this->load->helper(array('form', 'my_btn_access_helper', 'my_helper'));
		$this->load->model(array('m_builder', 'modul_inventory/td_rak_histori'));
	}

	public function index() {
		parent::administrator();
		parent::typeahead_assets();
		parent::datetimepicker_assets();
		$this->get_table();
	}

	public function get_table($id = '') {
		$data['nm_gudang'] = $_SESSION['modul_inventory']['nm_gudang'];
		$data['nm_rak'] = $_SESSION['modul_inventory']['nm_rak'];
		$data['nm_rak_ruang'] = $_SESSION['modul_inventory']['nm_rak_ruang'];
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function open_table() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		$data = $this->td_rak_histori->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'], $data['groupBy'] )
		);
	}

	public function get_form() {
		$data['id'] = $this->input->get('id');
		$data['class_link'] = $this->class_link;
		$data['form_errs'] = $this->form_errs;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function open_form() {
		$id = $this->input->get('id');
		$data = $this->td_rak_histori->get_data($id);
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function send_data() {
		$this->load->model(['tm_barang']);
		if ($this->input->is_ajax_request()) :
			$input_option = $this->input->post('selInputOption');
			$this->form_validation->set_rules($this->td_rak_histori->form_rules());
			if ($input_option == 'item_code') :
				$data['barang_kd'] = $this->input->post('txtKdBarang');
			elseif ($input_option == 'barcode') :
				$row = $this->tm_barang->get_item(['item_barcode' => $this->input->post('txtItemBarcode')]);
				if (!empty($row)) :
					$data['barang_kd'] = $row->kd_barang;
				else :
					$str['idErrItemBarcode'] = buildLabel('warning', 'Kode Barcode tidak ditemukan pada Data Barang!');
					$str['confirm'] = 'error';
					$str['csrf'] = $this->security->get_csrf_hash();

					header('Content-Type: application/json');
					echo json_encode($str);
					exit();
				endif;
			endif;
			if ($this->form_validation->run() == FALSE) :
				if ($input_option == 'item_code') :
					$this->form_errs = ['idErrItemCode', 'idErrJmlItem'];
				elseif ($input_option == 'barcode') :
					$this->form_errs = ['idErrItemBarcode'];
				endif;
				$str = $this->td_rak_histori->form_warning($this->form_errs);
				$str['confirm'] = 'error';
			else :
				$data['kd_rak_histori'] = $this->input->post('txtKd');
				$data['rak_ruang_kd'] = $this->input->post('txtKdRakRuang');
				$data['rak_kd'] = $this->input->post('txtKdRak');
				$data['gudang_kd'] = $this->input->post('txtKdGudang');
				$data['tgl_transaksi'] = format_date($this->input->post('txtTglTransaksi').' '.date('H:i:s'), 'Y-m-d H:i:s');
				if ($input_option == 'item_code') :
					$data['qty'] = $this->input->post('txtJmlItem');
				elseif ($input_option == 'barcode') :
					$data['qty'] = '1';
				endif;
				$data['mutasi'] = '0';
				$data['status'] = 'in';
				$str = $this->td_rak_histori->submit_data($data);
				$str['input_option'] = $input_option;
			endif;
			$str['csrf'] = $this->security->get_csrf_hash();

			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function delete_data() {
		if ($this->input->is_ajax_request()) :
			$id = $this->input->get('id');
			$str = $this->td_rak_histori->delete_data($id);
			
			header('Content-Type: application/json');
			echo json_encode($str);
		endif;
	}

	public function get_item_data() {
		$this->load->model('tm_barang');
		$str['item_barcode'] = $this->input->get('item_barcode');
		$data['item_row'] = $this->tm_barang->get_item(['item_barcode' => $str['item_barcode']]);
		$data['input_barcode'] = $str['item_barcode'];
		$str['kd_barang'] = empty($data['item_row']->kd_barang)?'':$data['item_row']->kd_barang;
		$str['item_code'] = empty($data['item_row']->item_code)?'':$data['item_row']->item_code;
		$str['view'] = $this->load->view('page/'.$this->class_link.'/form_detail_item', $data, TRUE);

		header('Content-Type: application/json');
		echo json_encode($str);
	}

	public function get_histori() {
		$kd_barang = $this->input->get('kd_barang');
		$kd_gudang = $this->input->get('kd_gudang');
		$kd_rak = $this->input->get('kd_rak');
		$kd_rak_ruang = $this->input->get('kd_rak_ruang');
		$_SESSION['modul_inventory']['items_history'] = $this->td_rak_histori->get_all(['barang_kd' => $kd_barang, 'gudang_kd' => $kd_gudang, 'rak_kd' => $kd_rak, 'rak_ruang_kd' => $kd_rak_ruang]);
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/histori_box', $data);
	}

	public function open_histori() {
		$this->load->view('page/'.$this->class_link.'/histori_main');
	}
}