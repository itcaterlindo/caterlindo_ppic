<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Adjustment_fg extends MY_Controller {
	private $class_link = 'inventory/finish_good/adjustment_fg';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_finishgood_barcode', 'td_finishgood_in', 'tm_finishgood', 'td_finishgood_adjustment', 'td_finishgood_out', 'tb_finishgood_user', 'tm_gudang']);
		/** Cek Lokasi User */
		$userfg = $this->tb_finishgood_user->get_by_param (array('kd_tipe_admin' => $this->session->userdata('tipe_admin_kd')))->row_array();
		$this->set_gudang = $userfg['kd_gudang'];
	}

	public function index() {
		parent::administrator();
		$this->form_box();
	}

	public function form_box() {
		parent::pnotify_assets();
		parent::typeahead_assets();
		parent::datetimepicker_assets();
		parent::ionicons_assets();

		$actGudang = $this->tm_gudang->get_row($this->set_gudang);
		$data['nm_gudang'] = $actGudang->nm_gudang;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		$actGudang = $this->tm_gudang->get_row($this->set_gudang);
		$data['nm_gudang'] = $actGudang->nm_gudang;
		$data['class_link'] = $this->class_link;
		$data['js_file'] = 'table_js'; 
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$fgadj_tglinput = $this->input->get('fgadj_tglinput', true);
		$fgadj_tglinput = format_date($fgadj_tglinput, 'Y-m-d');
		$act = $this->td_finishgood_adjustment->get_adj_by_date($fgadj_tglinput);
		
		$data['class_link'] = $this->class_link;
		$data['fgadj_tglinput'] = $fgadj_tglinput;
		$data['resultData'] = $act->result_array();

		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function get_barcode_fg() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$fgbarcode_barcode = $this->input->get('id');

		if(!empty($fgbarcode_barcode)){
			$row = $this->db->select('a.*, b.fg_kd, b.fgin_kd, b.rakruangkolom_kd, b.fgin_qty')
				->from('td_finishgood_barcode AS a')
				->join('td_finishgood_in as b', 'b.fgbarcode_kd=a.fgbarcode_kd')
				->where('a.fgbarcode_barcode', $fgbarcode_barcode)
				->order_by('b.fgin_tglinput', 'desc')
				->get()->row_array();
					
			if(!empty($row)){
				$resp['code'] = 200;
				$resp['status'] = 'Sukses';
				$resp['data'] = $row;
			}else{
				$resp['code'] = 400;
				$resp['status'] = 'Gagal';
				$resp['pesan'] = 'Data Tidak Ditemukan';
			}
			echo json_encode($resp);
		}
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtBarcode', 'Barcode', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtJnsAdj', 'Jns Adj', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtQty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtbarang_kd', 'Deskripsi', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrBarcode' => (!empty(form_error('txtBarcode')))?buildLabel('warning', form_error('txtBarcode', '"', '"')):'',
				'idErrJnsAdj' => (!empty(form_error('txtJnsAdj')))?buildLabel('warning', form_error('txtJnsAdj', '"', '"')):'',
				'idErrQty' => (!empty(form_error('txtQty')))?buildLabel('warning', form_error('txtQty', '"', '"')):'',
				'idErrDescription' => (!empty(form_error('txtbarang_kd')))?buildLabel('warning', form_error('txtbarang_kd', '"', '"')):'',
			);
			
		}else {
			$fgadj_status =  $this->input->post('txtJnsAdj');
			$fg_kd = $this->input->post('txtfgKd');
			$fgadj_qty = $this->input->post('txtQty');
			$fgbarcode_kd = $this->input->post('txtFgbarcodekd');
			$fgadj_keterangan = $this->input->post('txtKet');
			$rakruangkolom_kd = $this->input->post('txtkd_rak_ruangKolom');
			$fgin_kd = $this->input->post('txtfgin_kd');
			$barang_kd = $this->input->post('txtbarang_kd');

			/** Stock akhir dari td_finishgood_stock*/
			if (empty($fg_kd)){
				$fg_kd = $this->tm_finishgood->getrow_by_barang_gdg($barang_kd, $this->set_gudang)->row_array();
				$fg_kd = $fg_kd['fg_kd'];
			}
			$fgadj_qty_awal = $this->tm_finishgood->get_byId($fg_kd)->row_array();
			$fgadj_qty_awal = $fgadj_qty_awal['fg_qty'];

			$fgadj_tglinput = now_with_ms();
			
			$fgadj_kd = $this->td_finishgood_adjustment->buat_kode();
			$data = array(
				'fgadj_kd' => $fgadj_kd,
				'fg_kd' => $fg_kd,
				'fgadj_qty' => $fgadj_qty,
				'fgadj_status' => $fgadj_status,
				'fgadj_keterangan' => $fgadj_keterangan,
				'fgadj_tglinput' => $fgadj_tglinput,
				'fgadj_tgledit' => null,
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			if ($fgadj_status == 'IN')
			{
				/** Insert td_finishgood_adjustment dan IN*/
				$act = $this->td_finishgood_adjustment->insert_data($data);
				$actIN = $this->insertFG_in($fg_kd, $fgbarcode_kd, $fgadj_kd, $fgadj_qty, $fgadj_tglinput);
				if( $act == true && $actIN == true){
					$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
				}else{
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
				}
			}
			else if ($fgadj_status == 'OUT')
			{
				$actCekOut = $this->td_finishgood_in->get_by_param (array('fgbarcode_kd' => $fgbarcode_kd));
				if ($actCekOut->num_rows() != 0){
					/** Cek Rak Ruang  */
					if (!empty($rakruangkolom_kd)){
						/** Insert td_finishgood_adjustment dan detail*/
						$act = $this->td_finishgood_adjustment->insert_data($data);
						$actOUT = $this->insertFG_out($fg_kd, $fgin_kd, $fgbarcode_kd, $fgadj_kd, $fgadj_qty, $fgadj_tglinput);
						if( $act == true && $actOUT == true){
							$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
						}else{
							$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
						}
					}else{
						$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Lokasi Penyimpanan Kosong');
					}
				}else{
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Barang Belum Masuk');
				}
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	private function insertFG_in($fg_kd, $fgbarcode_kd, $fgadj_kd, $fgadj_qty, $fgin_tglinput){
		/** Stock akhir dari td_finishgood_stock*/
		$actFG = $this->tm_finishgood->get_byId($fg_kd)->row_array();
		$fgin_qty_awal = $actFG['fg_qty'];

		$fgin_kd = $this->td_finishgood_in->buat_kode();
		$data = array(
			'fgin_kd' => $fgin_kd,
			'fg_kd' => $fg_kd,
			'fgadj_kd' => $fgadj_kd,
			'fgbarcode_kd' => $fgbarcode_kd,
			'fgin_qty_awal' => $fgin_qty_awal,
			'fgin_qty' => $fgadj_qty,
			'fgin_tglinput' => $fgin_tglinput,
			'fgin_tgledit' => null,
			'admin_kd' => $this->session->userdata('kd_admin'),
		);
		$act = $this->td_finishgood_in->insert_data($data);
		/** Update Tm stock */
		$actStock = $this->tm_finishgood->update_stock($fg_kd, $fgadj_qty, 'IN');

		if ($act == true && $actStock == true){
			$resp = true;
		}else{
			$resp = false;
		}
		return $resp;
	}

	private function insertFG_out($fg_kd, $fgin_kd, $fgbarcode_kd, $fgadj_kd, $fgadj_qty, $fgout_tglinput){
		/** Stock akhir dari td_finishgood_stock*/
		$actFG = $this->tm_finishgood->get_byId($fg_kd)->row_array();
		$fgout_qty_awal = $actFG['fg_qty'];

		$fgout_kd = $this->td_finishgood_out->buat_kode();
		$data = array(
			'fgout_kd' => $fgout_kd,
			'fgin_kd' => $fgin_kd,
			'fgadj_kd' => $fgadj_kd,
			'kd_msalesorder' => null,
			'no_salesorder' => null,
			'fgout_qty_awal' => $fgout_qty_awal,
			'fgout_qty' => $fgadj_qty,
			'fgout_tglinput' => $fgout_tglinput,
			'fgout_tgledit' => null,
			'admin_kd' => $this->session->userdata('kd_admin'),
		);
		$act = $this->td_finishgood_out->insert_data($data);
		/** Update Tm stock */
		$actStock = $this->tm_finishgood->update_stock($fg_kd, $fgadj_qty, 'OUT');

		/** Flag FG_OUT di FG in ubah ke 1 (tandanya barang sudah keluar) */
		$flagOut = $this->td_finishgood_in->update_data(['fgin_kd'=>$fgin_kd], ['flag_out'=>'1']);

		if ($act == true && $actStock == true && $flagOut == true){
			$resp = true;
		}else{
			$resp = false;
		}
		return $resp;
	}

	
}
