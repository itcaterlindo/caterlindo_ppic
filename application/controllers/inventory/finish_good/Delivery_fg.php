<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Delivery_fg extends MY_Controller {
	private $class_link = 'inventory/finish_good/delivery_fg';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_finishgood_barcode', 'td_finishgood_in', 'tm_finishgood', 'td_finishgood_delivery', 'td_finishgood_out', 'tb_finishgood_user', 'tm_gudang', 'tm_barang', 'model_kartustokfg']);
		/** Cek Lokasi User */
		$userfg = $this->tb_finishgood_user->get_by_param (array('kd_tipe_admin' => $this->session->userdata('tipe_admin_kd')))->row_array();
		$this->set_gudang = $userfg['kd_gudang'];
	}

	public function index() {
		parent::administrator();
		$this->form_box();
	}

	public function form_box() {
		parent::pnotify_assets();
		parent::typeahead_assets();
		parent::datetimepicker_assets();
		parent::ionicons_assets();

		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		$data['opsiGudang'] = $this->tm_gudang->get_all()->result_array();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		$actGudang = $this->tm_gudang->get_row($this->set_gudang);
		$data['nm_gudang'] = $actGudang->nm_gudang;
		$data['class_link'] = $this->class_link;
		$data['js_file'] = 'table_js'; 
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$fgdlv_tglinput = $this->input->get('fgdlv_tglinput', true);
		$fgdlv_tglinput = format_date($fgdlv_tglinput, 'Y-m-d');
		$act = $this->td_finishgood_delivery->get_dlv_by_date($fgdlv_tglinput);
		
		$data['class_link'] = $this->class_link;
		$data['fgdlv_tglinput'] = $fgdlv_tglinput;
		$data['resultData'] = $act->result_array();

		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function get_barcode_fg() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$fgbarcode_barcode = $this->input->get('id');

		if(!empty($fgbarcode_barcode)){
			$row = $this->db->select('a.*, b.fg_kd, b.fgin_kd, b.rakruangkolom_kd, b.fgin_qty, c.fg_qty, c.kd_gudang, d.nm_gudang, c.item_code')
				->from('td_finishgood_barcode AS a')
				->join('td_finishgood_in as b', 'b.fgbarcode_kd=a.fgbarcode_kd')
				->join('tm_finishgood as c', 'c.fg_kd=b.fg_kd', 'left')
				->join('tm_gudang as d', 'd.kd_gudang=c.kd_gudang')
				->where('a.fgbarcode_barcode', $fgbarcode_barcode)
				->order_by('b.fgin_tglinput', 'desc')
				->get()->row_array();
					
			if(!empty($row)){
				$resp['code'] = 200;
				$resp['status'] = 'Sukses';
				$resp['data'] = $row;
			}else{
				$resp['code'] = 400;
				$resp['status'] = 'Gagal';
				$resp['pesan'] = 'Data Tidak Ditemukan';
			}
			echo json_encode($resp);
		}
	}

	public function action_insert() {
		try{
			if (!$this->input->is_ajax_request()){
				exit('No direct script access allowed');
			}

			
			
			$this->load->library(['form_validation']);
			$this->form_validation->set_rules('txtBarcode', 'Barcode', 'required', ['required' => '{field} tidak boleh kosong!']);
			$this->form_validation->set_rules('txtQty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
			$this->form_validation->set_rules('txtQtyFg', 'Qty FG', 'required', ['required' => '{field} tidak boleh kosong!']);
			$this->form_validation->set_rules('txtbarang_kd', 'Deskripsi', 'required', ['required' => '{field} tidak boleh kosong!']);
			$this->form_validation->set_rules('txtDariGudang', 'Dari Gudang', 'required', ['required' => '{field} tidak boleh kosong!']);
			$this->form_validation->set_rules('txtKeGudang', 'Ke Gudang', 'required', ['required' => '{field} tidak boleh kosong!']);
			
			if ($this->form_validation->run() == FALSE) {
				$resp['code'] = 401;
				$resp['status'] = 'Required';
				$resp['pesan'] = array(
					'idErrBarcode' => (!empty(form_error('txtBarcode')))?buildLabel('warning', form_error('txtBarcode', '"', '"')):'',
					'idErrQty' => (!empty(form_error('txtQty')))?buildLabel('warning', form_error('txtQty', '"', '"')):'',
					'idErrQtyFg' => (!empty(form_error('txtQtyFg')))?buildLabel('warning', form_error('txtQtyFg', '"', '"')):'',
					'idErrDescription' => (!empty(form_error('txtbarang_kd')))?buildLabel('warning', form_error('txtbarang_kd', '"', '"')):'',
					'idErrDariGudang' => (!empty(form_error('txtDariGudang')))?buildLabel('warning', form_error('txtDariGudang', '"', '"')):'',
					'idErrKeGudang' => (!empty(form_error('txtKeGudang')))?buildLabel('warning', form_error('txtKeGudang', '"', '"')):'',
				);
				
			}else {
				$this->db->trans_start();
				$fg_kd = $this->input->post('txtfgKd');
				$fgdlv_qty = $this->input->post('txtQty');
				$fgbarcode_kd = $this->input->post('txtFgbarcodekd');
				$fgdlv_keterangan = $this->input->post('txtKet');
				$rakruangkolom_kd = $this->input->post('txtkd_rak_ruangKolom');
				$fgin_kd = $this->input->post('txtfgin_kd');
				$barang_kd = $this->input->post('txtbarang_kd');
				$dari_gudang = $this->input->post('txtDariGudangKd');
				$ke_gudang = $this->input->post('txtKeGudang');


				// /** Stock akhir dari td_finishgood_stock*/
				// if (empty($fg_kd)){
				// 	$fg_kd = $this->tm_finishgood->getrow_by_barang_gdg($barang_kd, $dari_gudang)->row_array();
				// 	$fg_kd = $fg_kd['fg_kd'];
				// }
				// $fgdlv_qty_awal = $this->tm_finishgood->get_byId($fg_kd)->row_array();
				// $fgdlv_qty_awal = $fgdlv_qty_awal['fg_qty'];

				/** PROSES BARANG OUT */
				$dataOUT = array(
					'fgdlv_kd' => $this->td_finishgood_delivery->buat_kode(),
					'fg_kd' => $fg_kd,
					'fgdlv_qty' => $fgdlv_qty,
					'fgdlv_status' => "OUT",
					'fgdlv_keterangan' => $fgdlv_keterangan,
					'fgdlv_tglinput' => now_with_ms(),
					'fgdlv_tgledit' => null,
					'admin_kd' => $this->session->userdata('kd_admin'),
				);
				$actCekOut = $this->td_finishgood_in->get_by_param (array('fgbarcode_kd' => $fgbarcode_kd));
				
				/** Cek apakah barang masuk sudah keluar, jika barang sudah keluar maka tidak bisa di delivery */
				/** Jika fg in ada di tabel fg out maka barang sudah keluar dan tidak bisa di delivery  */
				$checkFgOut = $this->td_finishgood_out->get_by_param(['fgin_kd'=>$fgin_kd]);
				if( $checkFgOut->num_rows() != 0 ){
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Barang FG sudah transaksi keluar, tidak dapat di delivery !');
					$resp['csrf'] = $this->security->get_csrf_hash();
					echo json_encode($resp);
					die();
				}

				if ($actCekOut->num_rows() != 0){
					/** Cek Rak Ruang  */
					if (!empty($rakruangkolom_kd)){
						/** Insert td_finishgood_delivery dan detail*/
						$act = $this->td_finishgood_delivery->insert_data($dataOUT);
						$actOUT = $this->insertFG_out($dataOUT['fg_kd'], $fgin_kd, $fgbarcode_kd, $dataOUT['fgdlv_kd'], $fgdlv_qty, $dataOUT['fgdlv_tglinput']);
					}else{
						$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Lokasi Penyimpanan Kosong');
						$resp['csrf'] = $this->security->get_csrf_hash();
						echo json_encode($resp);
						die();
					}
				}else{
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Barang Belum Masuk');
					$resp['csrf'] = $this->security->get_csrf_hash();
					echo json_encode($resp);
					die();
				}

				/** PROSES BARANG IN */
				/** Cek apakah barang yang IN ada di tm_finishgood */
				$checkFGMaster = $this->tm_finishgood->get_byParam(['barang_kd' => $barang_kd, 'kd_gudang' => $ke_gudang])->row_array();
				if(empty($checkFGMaster)){
					/** ADD barang ke tm_finishgood */
					$insertFG = $this->insertFGMaster($barang_kd, $ke_gudang);
				}
				$fgMaster = $this->tm_finishgood->get_byParam(['barang_kd' => $barang_kd, 'kd_gudang' => $ke_gudang])->row_array();
				
				$dataIN = array(
					'fgdlv_kd' => $this->td_finishgood_delivery->buat_kode(),
					'fg_kd' => $fgMaster['fg_kd'],
					'fgdlv_qty' => $fgdlv_qty,
					'fgdlv_status' => "IN",
					'fgdlv_keterangan' => $fgdlv_keterangan,
					'fgdlv_tglinput' => now_with_ms(),
					'fgdlv_tgledit' => null,
					'admin_kd' => $this->session->userdata('kd_admin'),
				);
				/** Insert td_finishgood_delivery dan IN*/
				$act = $this->td_finishgood_delivery->insert_data($dataIN);
				$actIN = $this->insertFG_in($dataIN['fg_kd'], $fgbarcode_kd, $dataIN['fgdlv_kd'], $dataIN['fgdlv_qty'], $dataIN['fgdlv_tglinput']);
			
				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE){
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan');
				}else{
					$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
				}

			}

			$resp['csrf'] = $this->security->get_csrf_hash();
			echo json_encode($resp);
		}catch(\Throwable $e){
			echo $e->getMessage();
		}
	}

	private function insertFGMaster($barang_kd, $kd_gudang)
	{
		$this->load->model(['tm_barang']);

		$actBarang = $this->tm_barang->get_item(array('kd_barang'=> $barang_kd));
		$fg_kd = $this->tm_finishgood->buat_kode();
		$dataFG = array(
			'fg_kd' => $fg_kd,
			'kd_gudang' => $kd_gudang,
			'barang_kd' => $barang_kd,
			'item_code' => $actBarang->item_code,
			'item_barcode' => $actBarang->item_group_kd == '2' ? '899000' : $actBarang->item_barcode, /** Jika barang custom atau PJ barcodenya '899000' */
			'fg_qty' => 0,
			'fg_tglinput' => date('Y-m-d H:i:s'),
			'fg_tgledit' => null,
			'admin_kd' => $this->session->userdata('kd_admin'),
		);
		$actInsertFG = $this->tm_finishgood->insert_data ($dataFG);
		if($actInsertFG == true){
			$resp = true;
		}else{
			$resp = false;
		}
		return $resp;
	}

	private function insertFG_in($fg_kd, $fgbarcode_kd, $fgdlv_kd, $fgdlv_qty, $fgin_tglinput){
		/** Stock akhir dari td_finishgood_stock*/
		$actFG = $this->tm_finishgood->get_byId($fg_kd)->row_array();

		/** Jika barang custom ambil qty awal dari kartu stok */
		if( $this->tm_barang->get_item(['kd_barang' => $actFG['barang_kd']])->item_group_kd == '2' ){
			/** Custom */
			$fgin_qty_awal = $this->get_fg_qty_awal_custom($actFG['barang_kd'], $actFG['kd_gudang']);
		}else{
			/** Std */
			$fgin_qty_awal = $actFG['fg_qty'];
		}

		$fgin_kd = $this->td_finishgood_in->buat_kode();
		$data = array(
			'fgin_kd' => $fgin_kd,
			'fg_kd' => $fg_kd,
			'fgdlv_kd' => $fgdlv_kd,
			'fgbarcode_kd' => $fgbarcode_kd,
			'fgin_qty_awal' => $fgin_qty_awal,
			'fgin_qty' => $fgdlv_qty,
			'fgin_tglinput' => $fgin_tglinput,
			'fgin_tgledit' => null,
			'admin_kd' => $this->session->userdata('kd_admin'),
		);
		$act = $this->td_finishgood_in->insert_data($data);
		/** Update Tm stock */
		$actStock = $this->tm_finishgood->update_stock($fg_kd, $fgdlv_qty, 'IN');

		if ($act == true && $actStock == true){
			$resp = true;
		}else{
			$resp = false;
		}
		return $resp;
	}

	private function insertFG_out($fg_kd, $fgin_kd, $fgbarcode_kd, $fgdlv_kd, $fgdlv_qty, $fgout_tglinput){
		/** Stock akhir dari td_finishgood_stock*/
		$actFG = $this->tm_finishgood->get_byId($fg_kd)->row_array();

		/** Jika barang custom ambil qty awal dari kartu stok */
		if( $this->tm_barang->get_item(['kd_barang' => $actFG['barang_kd']])->item_group_kd == '2' ){
			/** Custom */
			$fgout_qty_awal = $this->get_fg_qty_awal_custom($actFG['barang_kd'], $actFG['kd_gudang']);
		}else{
			/** Std */
			$fgout_qty_awal = $actFG['fg_qty'];
		}

		$fgout_kd = $this->td_finishgood_out->buat_kode();
		$data = array(
			'fgout_kd' => $fgout_kd,
			'fgin_kd' => $fgin_kd,
			'fgdlv_kd' => $fgdlv_kd,
			'kd_msalesorder' => null,
			'no_salesorder' => null,
			'fgout_qty_awal' => $fgout_qty_awal,
			'fgout_qty' => $fgdlv_qty,
			'fgout_tglinput' => $fgout_tglinput,
			'fgout_tgledit' => null,
			'admin_kd' => $this->session->userdata('kd_admin'),
		);
		$act = $this->td_finishgood_out->insert_data($data);
		/** Update Tm stock */
		$actStock = $this->tm_finishgood->update_stock($fg_kd, $fgdlv_qty, 'OUT');

		/** Flag FG_OUT di FG in ubah ke 1 (tandanya barang sudah keluar) */
		$flagOut = $this->td_finishgood_in->update_data(['fgin_kd'=>$fgin_kd], ['flag_out'=>'1']);

		if ($act == true && $actStock == true && $flagOut == true){
			$resp = true;
		}else{
			$resp = false;
		}
		return $resp;
	}

	private function get_fg_qty_awal_custom($barang_kd, $kd_gudang)
	{
		$kartustok = [];
		/** Get last data untuk ambil startdate kartu stok custom berdasarkan gudang */
		$fgCustom = $this->db->select('fg_kd, kd_gudang, barang_kd, item_code, item_barcode, fg_tglinput')
			->where('item_barcode', '899000')
			->where('barang_kd', $barang_kd)
			->order_by('fg_tglinput', 'DESC')
			->limit(1)
			->get('tm_finishgood')
			->row_array();
		/** Cek jika barang custom by gudang tidak kosong maka ambil qty awal dari kartu stock */
		if(!empty($fgCustom)){
			/** Jika barang custom / PJ setting qty awal ambil dari qty (sisa) kartu stock (field sisa) */
			$startdate = format_date( $fgCustom['fg_tglinput'] , 'Y-m-d');
			$startdate = date('Y-m-d', strtotime($fgCustom['fg_tglinput'].' - 1 day'));
			$enddate = date('Y-m-d');
			$item_code = $fgCustom['barang_kd'];
			//$kd_gudang = $fgCustom['kd_gudang'];
			$kartustok = $this->model_kartustokfg->kartu_stok_all($startdate, $enddate, $item_code, $kd_gudang);
			/** Ambil sisa data terakhir */
			if(!empty($kartustok)){
				$fg_qty_awal = $kartustok[count($kartustok)-1]['sisa'];
			}else{
				$fg_qty_awal = 0;
			}
		}else{
			/** Jika tidak ada di kartu stock  */
			$fg_qty_awal = 0;
		}
		return $fg_qty_awal;
	}

	
}
