<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Repacking_fg extends MY_Controller {
	private $class_link = 'inventory/finish_good/repacking_fg';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_finishgood_barcode', 'td_finishgood_in', 'td_finishgood_out', 'tm_finishgood', 'td_finishgood_adjustment', 'td_finishgood_repacking']);
	}

	public function index() {
		parent::administrator();
		$this->form_box();
	}

	public function form_box() {
		parent::pnotify_assets();
		parent::typeahead_assets();
		parent::ionicons_assets();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		$data['class_link'] = $this->class_link;
		$data['js_file'] = 'table_js'; 
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		
		$fgrepack_tglinput = date('Y-m-d');
		$act = $this->td_finishgood_repacking->get_repack_by_date($fgrepack_tglinput);
		$data['class_link'] = $this->class_link;
		$data['resultData'] = $act->result_array();

		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$fgrepack_tglinput = $this->input->get('fgrepack_tglinput', TRUE);
		$data = $this->td_finishgood_repacking->ssp_table($fgrepack_tglinput);

		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function get_barcode_fg() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$fgbarcode_barcode = $this->input->get('id');

		if(!empty($fgbarcode_barcode)){
			$act = $this->td_finishgood_barcode->select_by_barcode($fgbarcode_barcode);
			if($act->num_rows() != 0){
				$act = $act->row_array();
				$resp['code']=200;
				$resp['status']='Sukses';
				$resp['data']=$act;
			}else{
				$resp['code']=400;
				$resp['status']='Gagal';
				$resp['pesan']='Data Tidak Ditemukan';
			}
			echo json_encode($resp);
		}
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtBarcode', 'Barcode', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtProdcode', 'Prod Code', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtQty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrBarcode' => (!empty(form_error('txtBarcode')))?buildLabel('warning', form_error('txtBarcode', '"', '"')):'',
				'idErrProdcode' => (!empty(form_error('txtProdcode')))?buildLabel('warning', form_error('txtProdcode', '"', '"')):'',
				'idErrQty' => (!empty(form_error('txtQty')))?buildLabel('warning', form_error('txtQty', '"', '"')):'',
			);
			
		}else {
			$fg_kd = $this->input->post('txtfgKd');
			$fgbarcode_kd = $this->input->post('txtFgbarcodekd');
			$qtyItem = $this->input->post('txtQty'); /** item yang di repack */
			$QtyRepack = $this->input->post('txtQtyRepack'); /** item baru */
			$barang_kd = $this->input->post('txtbarangKd');
			$item_code = $this->input->post('txtProdcode');
			$fgbarcode_desc = $this->input->post('txtDescription');
			$fgbarcode_dimensi = $this->input->post('txtDimension');
			$batchShift = $this->input->post('txtBatchShift');
			$item_barcode = $this->input->post('txtitemBarcode');
			$fgin_kd = $this->input->post('txtfgin_kd');
			$rakruangkolom_kd = $this->input->post('txtkd_rak_ruangKolom');
			$note = $this->input->post('txtNote');
			$dn_kd = $this->input->post('txtkd_dn');

			$sumRepack = array_sum($QtyRepack);
			if ($sumRepack == $qtyItem){
				/** Cek apakah barang sudah keluar */
				$cekOut = $this->td_finishgood_out->get_by_param(array('fgin_kd'=>$fgin_kd));
				if ($cekOut->num_rows() == 0 ){
					/** cek rak ruang */
					if (!empty($rakruangkolom_kd)){
						/** Insert repacking dan detail barang yg direpack menjadi out*/
						$this->db->trans_start();
						$actRepackOut = $this->insertFG_repacking($fg_kd, $fgin_kd, $fgbarcode_kd, $qtyItem, 'OUT', $rakruangkolom_kd);
						/** create barcode dan insert ke detail untuk item baru in*/
						$batchShift = explode('-', $batchShift);
						$fgbarcode_batch = $batchShift[0];
						$fgbarcode_shift = $batchShift[1];
						foreach ($QtyRepack as $eachQtyItemBaru){
							/**Generate barcode */
							$urutBarcode = $this->td_finishgood_barcode->urut_barcode($barang_kd, $fgbarcode_batch);
							$fgbarcode_barcode = $item_barcode.'-'.$urutBarcode.'-'.$fgbarcode_batch.'-'.$fgbarcode_shift;
							/** insert barcode sekaligus insert detail dan repacking */
							$actBcdDetail = $this->insertFG_barcode($fg_kd, $barang_kd, $fgbarcode_barcode, $fgbarcode_desc, $fgbarcode_dimensi, $eachQtyItemBaru, $note, $dn_kd);
						}
						$this->db->trans_complete();
						if ($this->db->trans_status() === FALSE){
							$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan');
						}else{
							$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
						}
					}else{
						$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Lokasi penyimpanan kosong');
					}	
				}else{
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Barang Sudah Keluar');
				}
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Qty Tidak Sama');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	/** Menu insert untuk repacking replace */
	public function action_insert_replace() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtBarcodeReplace', 'Barcode', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtProdcodeReplace', 'Prod Code', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtQtyReplace', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtbarangKdBaruReplace', 'Prod Code', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrBarcodeReplace' => (!empty(form_error('txtBarcodeReplace')))?buildLabel('warning', form_error('txtBarcodeReplace', '"', '"')):'',
				'idErrProdcodeReplace' => (!empty(form_error('txtProdcodeReplace')))?buildLabel('warning', form_error('txtProdcodeReplace', '"', '"')):'',
				'idErrQtyReplace' => (!empty(form_error('txtQtyReplace')))?buildLabel('warning', form_error('txtQtyReplace', '"', '"')):'',
				'idErrItemCodeBaruReplace' => (!empty(form_error('txtbarangKdBaruReplace')))?buildLabel('warning', form_error('txtbarangKdBaruReplace', '"', '"')):'',
			);
			
		}else {
			$fg_kd = $this->input->post('txtfgKdReplace');
			$fgbarcode_kd = $this->input->post('txtFgbarcodekdReplace');
			$qtyItem = $this->input->post('txtQtyReplace');
			$fgin_kd = $this->input->post('txtfgin_kdReplace');
			$rakruangkolom_kd = $this->input->post('txtkd_rak_ruangKolomReplace');
			$note = $this->input->post('txtNoteReplace');
			$dn_kd = $this->input->post('txtkd_dnReplace');
			$fgbarcode = $this->input->post('txtBarcodeReplace');
			/** Barang Baru */ 
			$barang_kd_baru = $this->input->post('txtbarangKdBaruReplace');
			$fgbarcode_desc_baru = $this->input->post('txtDescriptionBaruReplace');
			$fgbarcode_dimensi_baru = $this->input->post('txtDimensionBaruReplace');
			/** Get data finishgood barang baru dengan kd gudang FG Caterlindo */
			$fgBarangBaru = $this->tm_finishgood->get_byParam(['kd_gudang' => 'MGD071218001', 'barang_kd' => $barang_kd_baru])->row_array();
			if(empty($fgBarangBaru)){
				$resp['csrf'] = $this->security->get_csrf_hash();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan, Barang baru tidak ada di finishgood');
				echo json_encode($resp);
				die();
			}

			/** Cek apakah barang sudah keluar */
			$cekOut = $this->td_finishgood_out->get_by_param(array('fgin_kd'=>$fgin_kd));
			if ($cekOut->num_rows() != 0 ){
				$resp['csrf'] = $this->security->get_csrf_hash();
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Barang Sudah Keluar');
				echo json_encode($resp);
				die();
			}

			/** INSERT REPACKING OUT */
			$this->db->trans_start();
			$actRepackOut = $this->insertFG_repacking($fg_kd, $fgin_kd, $fgbarcode_kd, $qtyItem, 'OUT', $rakruangkolom_kd);
			/** INSERT BARCODE BARANG BARU DAN BARCODE REFERENCE BY BARANG LAMA YANG REPACKING */
			/** create barcode barang baru */
			/** Contoh output 890212-010-240319-1 */
			$fgbarcode = explode('-', $fgbarcode);
			$urutBarcode = $fgbarcode[1];
			$fgbarcode_batch = $fgbarcode[2];
			$fgbarcode_shift = $fgbarcode[3];
			/**Generate barcode reference by barang lama kecuali item_barcode ambil dari barang baru */
			$fgbarcode_barcode_baru = $fgBarangBaru['item_barcode'].'-'.$urutBarcode.'-'.$fgbarcode_batch.'-'.$fgbarcode_shift;
			/** insert barcode sekaligus insert detail dan repacking */
			$actBcdDetail = $this->insertFG_barcode($fgBarangBaru['fg_kd'], $fgBarangBaru['barang_kd'], $fgbarcode_barcode_baru, $fgbarcode_desc_baru, $fgbarcode_dimensi_baru, $qtyItem, $note, $dn_kd);
			/** Update stock di di finishgood, ada di dalam fungsi Insert FG OUT dan Insert FG IN (Otomatis dieksekusi) */

			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE){
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan');
			}else{
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	/** untuk create barcode dari repacking barang baru */
	private function insertFG_barcode($fg_kd, $barang_kd, $fgbarcode_barcode, $fgbarcode_desc, $fgbarcode_dimensi, $fgrepack_qty, $note, $kd_dn){
		/** ambil detail dati barcode */
		$expBarcode = explode('-', $fgbarcode_barcode);
		$fgbarcode_batch = $expBarcode[2];
		$fgbarcode_tglbatch = '20'.substr($fgbarcode_batch, 0,2).'-'.substr($fgbarcode_batch, 2,2).'-'.substr($fgbarcode_batch, 4,2);
		$fgbarcode_shift = $expBarcode[3];

		$fgbarcode_kd = $this->td_finishgood_barcode->buat_kode();
		$dataBarcode = array(
			'fgbarcode_kd' => $this->td_finishgood_barcode->buat_kode(),
			'barang_kd' => $barang_kd,
			'fgbarcode_barcode' => $fgbarcode_barcode,
			'fgbarcode_batch' => $fgbarcode_batch,
			'fgbarcode_tglbatch' => $fgbarcode_tglbatch,
			'fgbarcode_shift' => $fgbarcode_shift, 
			'fgbarcode_desc' => $fgbarcode_desc,
			'fgbarcode_dimensi' => $fgbarcode_dimensi,
			'fgbarcode_note' => $note,
			'kd_dn' => $kd_dn,
			'fgbarcode_tglinput' => now_with_ms(),
			'fgbarcode_tgledit' => null,
			'admin_kd' => $this->session->userdata('kd_admin')
		);
		$actBarcode = $this->td_finishgood_barcode->insert_data($dataBarcode);
		$actRepackingIn = $this->insertFG_repacking($fg_kd, null, $fgbarcode_kd, $fgrepack_qty, 'IN', null);

		if ($actBarcode == true && $actRepackingIn == true){
			$resp = true;
		}else{
			$resp = false;
		}
		return $resp;
	}

	private function insertFG_repacking($fg_kd, $fgin_kd, $fgbarcode_kd, $fgrepack_qty, $fgrepack_status, $rakruangkolom_kd){
		$fgrepack_kd = $this->td_finishgood_repacking->buat_kode();
		$fgrepack_tglinput = now_with_ms();
		$data = array(
			'fgrepack_kd' => $fgrepack_kd,
			'fg_kd' => $fg_kd,
			'fgrepack_qty' => $fgrepack_qty,
			'fgrepack_status' => $fgrepack_status,
			'fgrepack_tglinput' => $fgrepack_tglinput,
			'fgrepack_tgledit' => null,
			'admin_kd' => $this->session->userdata('kd_admin'),
		);
		$act = $this->td_finishgood_repacking->insert_data($data);
		/** insert ke fg in atau out */
		if ($fgrepack_status == 'IN'){
			$actFG = $this->insertFG_in($fg_kd, $fgbarcode_kd, $fgrepack_kd, $fgrepack_qty, $fgrepack_tglinput);
		}else{
			// OUT
			$actFG = $this->insertFG_out($fg_kd, $fgin_kd, $fgbarcode_kd, $fgrepack_kd, $fgrepack_qty, $fgrepack_tglinput);
		}
		
		if ($act = true && $actFG == true ){
			$resp = true;
		}else{
			$resp = false;
		}

		return $resp;
	}

	private function insertFG_out($fg_kd, $fgin_kd, $fgbarcode_kd, $fgrepack_kd, $fgout_qty, $fgout_tglinput){
		/** Stock akhir dari td_finishgood_stock*/
		$actFG = $this->tm_finishgood->get_byId($fg_kd)->row_array();
		$fgout_qty_awal = $actFG['fg_qty'];

		$fgout_kd = $this->td_finishgood_out->buat_kode();
		$data = array(
			'fgout_kd' => $fgout_kd,
			'fgin_kd' => $fgin_kd,
			'fgrepack_kd' => $fgrepack_kd,
			'kd_msalesorder' => null,
			'no_salesorder' => null,
			'fgout_qty_awal' => $fgout_qty_awal,
			'fgout_qty' => $fgout_qty,
			'fgout_tglinput' => $fgout_tglinput,
			'fgout_tgledit' => null,
			'admin_kd' => $this->session->userdata('kd_admin'),
		);
		$act = $this->td_finishgood_out->insert_data($data);
		/** Update Tm stock */
		$actStock = $this->tm_finishgood->update_stock($fg_kd, $fgout_qty, 'OUT');
		/** Flag FG_OUT di FG in ubah ke 1 (tandanya barang sudah keluar) */
		$flagOut = $this->td_finishgood_in->update_data(['fgin_kd'=>$fgin_kd], ['flag_out'=>'1']);

		if ($act == true && $actStock == true && $flagOut == true){
			$resp = true;
		}else{
			$resp = false;
		}
		return $resp;
	}

	private function insertFG_in($fg_kd, $fgbarcode_kd, $fgrepack_kd, $fgin_qty, $fgin_tglinput){
		/** Stock akhir dari td_finishgood_stock*/
		$actFG = $this->tm_finishgood->get_byId($fg_kd)->row_array();
		$fgin_qty_awal = $actFG['fg_qty'];

		$fgin_kd = $this->td_finishgood_in->buat_kode();
		$data = array(
			'fgin_kd' => $fgin_kd,
			'fg_kd' => $fg_kd,
			'fgrepack_kd' => $fgrepack_kd,
			'fgbarcode_kd' => $fgbarcode_kd,
			'fgin_qty_awal' => $fgin_qty_awal,
			'fgin_qty' => $fgin_qty,
			'fgin_tglinput' => $fgin_tglinput,
			'fgin_tgledit' => null,
			'admin_kd' => $this->session->userdata('kd_admin'),
		);
		$act = $this->td_finishgood_in->insert_data($data);
		/** Update Tm stock */
		$actStock = $this->tm_finishgood->update_stock($fg_kd, $fgin_qty, 'IN');

		if ($act == true && $actStock == true){
			$resp = true;
		}else{
			$resp = false;
		}
		return $resp;
	}
	
}
