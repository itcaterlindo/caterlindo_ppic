<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Cetakbarcode_fg extends MY_Controller
{
	private $class_link = 'inventory/finish_good/cetakbarcode_fg';

	public function __construct()
	{
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_finishgood_barcode', 'tm_finishgood', 'td_finishgood_in', 'tm_deliverynote', 'td_deliverynote_received', 'tm_barang']);
	}

	public function index()
	{
		parent::administrator();
		parent::select2_assets();
		parent::icheck_assets();
		$this->form_box();
	}

	public function form_box()
	{
		parent::datetimepicker_assets();
		parent::typeahead_assets();
		parent::pnotify_assets();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/' . $this->class_link . '/form_box', $data);
	}

	public function form_main()
	{
		$rDn = $this->db->select()
			->from('tm_deliverynote')
			->where('dn_tujuan', '70')
			->where_in('dnstate_kd', [2, 3])
			->order_by('dn_kd', 'desc')
			->get()->result();
		$opsiDN[null] = '--Pilih Opsi--';
		foreach ($rDn as $r) :
			$opsiDN[$r->dn_kd] = $r->dn_no;
		endforeach;
		$data['opsiDN'] = $opsiDN;
		$data['class_link'] = $this->class_link;

		$this->load->view('page/' . $this->class_link . '/form_main', $data);
	}

	public function tabledn_main()
	{
		$dn_kd = $this->input->get('id', true);
		$data['result'] = $this->db->select('td_deliverynote_received.*, td_deliverynote_detail.kd_barang, td_workorder_item.woitem_no_wo,
				td_workorder_item.woitem_status, td_workorder_item_so.woitemso_itemcode, td_workorder_item_so.woitemso_itemdeskripsi, td_workorder_item_so.woitemso_itemdimensi')
			->from('td_deliverynote_received')
			->join('td_deliverynote_detail', 'td_deliverynote_received.dndetail_kd=td_deliverynote_detail.dndetail_kd', 'left')
			->join('td_workorder_item', 'td_deliverynote_detail.woitem_kd=td_workorder_item.woitem_kd', 'left')
			->join('td_workorder_item_detail', 'td_workorder_item.woitem_kd=td_workorder_item_detail.woitem_kd', 'left')
			->join('td_workorder_item_so', 'td_workorder_item_detail.woitemso_kd=td_workorder_item_so.woitemso_kd', 'left')
			->where('td_deliverynote_received.dn_kd', $dn_kd)
			->where_in('td_workorder_item.woitem_jenis', ['packing', 'project'])
			->group_by('td_deliverynote_received.dndetailreceived_kd')
			->get()->result_array();
		$data['id'] = $dn_kd;
		$this->load->view('page/' . $this->class_link . '/tabledn_main', $data);
	}

	public function table_box()
	{
		$data['class_link'] = $this->class_link;
		$data['js_file'] = 'table_js';
		$this->load->view('page/' . $this->class_link . '/table_box', $data);
	}

	public function table_main()
	{
		$tglprod = $this->input->get('tglprod', TRUE);
		$tglprod = format_date($tglprod, 'Y-m-d');
		$data['class_link'] = $this->class_link;
		$data['tglprod'] = $tglprod;
		$this->load->view('page/' . $this->class_link . '/table_main', $data);
	}

	public function table_data()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$tglprod = $this->input->get('tglprod', TRUE);
		$data = $this->td_finishgood_barcode->ssp_table($tglprod);
		echo json_encode(
			SSP::simple($_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'])
		);
	}

	public function get_barang()
	{
		/** Get barang berdasarkan delivery note */
		$item_code = $this->input->get('item_code');
		$dn_kd = $this->input->get('dn_kd');

		$this->db->from('tm_barang')
			->join('(SELECT * FROM td_deliverynote_detail WHERE dn_kd = "'.$dn_kd.'" GROUP BY kd_barang) td_deliverynote_detail', 'td_deliverynote_detail.kd_barang=tm_barang.kd_barang')
			->where('td_deliverynote_detail.dn_kd', $dn_kd)
			->like('tm_barang.item_code', $item_code, 'match')
			->order_by('length(tm_barang.item_code) ASC, tm_barang.item_code ASC');
		$query = $this->db->get();
		$result = $query->result();
		if (!empty($result)) :
			foreach ($result as $row) :
				$dataset[] = array (
					'item_code' => $row->item_code,
					'kd_barang' => $row->kd_barang, 
					'item_barcode' => $row->item_barcode,
					'deskripsi_barang' => $row->deskripsi_barang,
					'dimensi_barang' => $row->dimensi_barang,
					);
			endforeach;
			echo json_encode($dataset);
		endif;
	}

	public function action_insert()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtProductCode', 'Product Code', 'required', array('required' => '{field} tidak boleh kosong!'));
		$this->form_validation->set_rules('txtBarcode', 'Barcode', 'required', array('required' => '{field} tidak boleh kosong!'));
		$this->form_validation->set_rules('txtQtyItem', 'Qty Item', 'required', array('required' => '{field} tidak boleh kosong!'));
		$this->form_validation->set_rules('txtBatch', 'Batch', 'required', array('required' => '{field} tidak boleh kosong!'));

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrProductCode' => (!empty(form_error('txtProductCode'))) ? buildLabel('warning', form_error('txtProductCode', '"', '"')) : '',
				'idErrBarcode' => (!empty(form_error('txtBarcode'))) ? buildLabel('warning', form_error('txtBarcode', '"', '"')) : '',
				'idErrQtyItem' => (!empty(form_error('txtQtyItem'))) ? buildLabel('warning', form_error('txtQtyItem', '"', '"')) : '',
				'idErrNoBatch' => (!empty(form_error('txtBatch'))) ? buildLabel('warning', form_error('txtBatch', '"', '"')) : '',
			);
		} else {
			$barang_kd = $this->input->post('txtKdBarang');
			$item_code = $this->input->post('txtProductCode');
			$item_barcode = $this->input->post('txtBarcode');
			$fgbarcode_batch = $this->input->post('txtBatch');
			$fgbarcode_tglbatch = $this->input->post('txtTglProduksi');
			$fgbarcode_shift = $this->input->post('txtShift');
			$fgbarcode_desc = $this->input->post('txtDescription');
			$fgbarcode_dimensi = $this->input->post('txtDimension');
			$txtQtyItem = $this->input->post('txtQtyItem');
			$txtKdDN = $this->input->post('txtKdDN');
			$barang = $this->tm_barang->get_item(['kd_barang' => $barang_kd]);
			/** Ketika barang yang di input statusnya custom (PJ) */
			if($barang->item_group_kd == '2'){
				$barangStatus = 'custom';
				$item_barcode = '899000';
			}else{
				$barangStatus = 'std';
			}
			$this->db->trans_start();
			/** Untuk insert multiple */
			for ($i = 1; $i <= $txtQtyItem; $i++) {
				/**Generate barcode */
				$urutBarcode = $this->td_finishgood_barcode->urut_barcode($barang_kd, $fgbarcode_batch, $barangStatus);
				$fgbarcode_barcode = $item_barcode . '-' . $urutBarcode . '-' . $fgbarcode_batch . '-' . $fgbarcode_shift;

				$data = array(
					'fgbarcode_kd' => $this->td_finishgood_barcode->buat_kode(),
					'barang_kd' => $barang_kd,
					'fgbarcode_barcode' => $fgbarcode_barcode,
					'fgbarcode_batch' => !empty($fgbarcode_batch) ? $fgbarcode_batch : null,
					'fgbarcode_tglbatch' => format_date($fgbarcode_tglbatch, 'Y-m-d'),
					'fgbarcode_shift' => !empty($fgbarcode_shift) ? $fgbarcode_shift : null,
					'fgbarcode_desc' => !empty($fgbarcode_desc) ? $fgbarcode_desc : null,
					'fgbarcode_dimensi' => !empty($fgbarcode_dimensi) ? $fgbarcode_dimensi : null,
					'fgbarcode_tglinput' => now_with_ms(),
					'fgbarcode_tgledit' => null,
					'admin_kd' => $this->session->userdata('kd_admin'),
					'kd_dn' => $txtKdDN
				);
				$this->td_finishgood_barcode->insert_data($data);
			}
			$this->db->trans_complete();

			if ($this->db->trans_status() != FALSE) {
				$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Data Tersimpan');
			} else {
				$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}
	
	public function action_generatebarcode_dn()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtdn_kd', 'Product Code', 'required', array('required' => '{field} tidak boleh kosong!'));

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrProductCode' => (!empty(form_error('txtdn_kd'))) ? buildLabel('warning', form_error('txtdn_kd', '"', '"')) : '',
			);
		} else {
			$this->db->trans_begin();
			$arrDndetailreceived_kd = $this->input->post('txtdndetailreceived_kd');
			if (empty($arrDndetailreceived_kd)) {
				$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Input Kosong');
				$resp['csrf'] = $this->security->get_csrf_hash();
				echo json_encode($resp);
				die();
			}

			$itembarcode_custom = '899000';
			$qRcv = $this->db->select('td_deliverynote_received.*, td_deliverynote_detail.kd_barang, td_workorder_item.woitem_no_wo,
						td_workorder_item.woitem_status, td_workorder_item_so.woitemso_itemcode, td_workorder_item_so.woitemso_itemdeskripsi, td_workorder_item_so.woitemso_itemdimensi, tm_barang.item_barcode')
				->from('td_deliverynote_received')
				->join('td_deliverynote_detail', 'td_deliverynote_received.dndetail_kd=td_deliverynote_detail.dndetail_kd', 'left')
				->join('td_workorder_item', 'td_deliverynote_detail.woitem_kd=td_workorder_item.woitem_kd', 'left')
				->join('td_workorder_item_detail', 'td_workorder_item.woitem_kd=td_workorder_item_detail.woitem_kd', 'left')
				->join('td_workorder_item_so', 'td_workorder_item_detail.woitemso_kd=td_workorder_item_so.woitemso_kd', 'left')
				->join('tm_barang', 'tm_barang.kd_barang=td_workorder_item_so.kd_barang', 'left')
				->where_in('td_deliverynote_received.dndetailreceived_kd', $arrDndetailreceived_kd)
				->order_by('td_deliverynote_detail.kd_barang', 'asc')
				->get()->result_array();

			$fgbarcode_kd = $this->td_finishgood_barcode->buat_kode_urut(null);
			$arrUrutBarang = [];
			$urutBarcodeCustom = 1;
			$incrementBarcodeCustom = false;
			foreach ($qRcv as $eRcv) {
				$fgbarcode_batch = format_date($eRcv['dndetailreceived_tanggal'], 'ymd');
				$item_barcode = $eRcv['item_barcode'];
				$kd_barang = $eRcv['kd_barang'];
				if (isset($arrUrutBarang[$fgbarcode_batch])){
					if (!array_key_exists($kd_barang, $arrUrutBarang[$fgbarcode_batch])) {
						$urutBarcode = $this->td_finishgood_barcode->urut_barcode($kd_barang, $fgbarcode_batch, $eRcv['woitem_status']);
						$arrUrutBarang[$fgbarcode_batch][$kd_barang] = $urutBarcode;
					}
				}else{
					$urutBarcode = $this->td_finishgood_barcode->urut_barcode($kd_barang, $fgbarcode_batch, $eRcv['woitem_status']);
					$arrUrutBarang[$fgbarcode_batch][$kd_barang] = $urutBarcode;
				}
				$fgbarcode_shift = 1;
				for ($i = 1; $i <= $eRcv['dndetailreceived_qty']; $i++) {
					/** Counting urutan barang custom looping berdasarkan qty detail */
					if ($eRcv['woitem_status'] == 'custom') {
						$item_barcode = $itembarcode_custom;
						// $kd_barang = 'PRD020817000573'; // Kode barang mengikuti kd_barang di master termasuk barang custom PJ
						if($incrementBarcodeCustom == true){
							$urutBarcodeCustom++;
						}else{
							$urutBarcodeCustom = $this->td_finishgood_barcode->urut_barcode($kd_barang, $fgbarcode_batch, $eRcv['woitem_status']);
							$incrementBarcodeCustom = true;
						}
						$urutBarcodeCustom = str_pad($urutBarcodeCustom, 3, "0", STR_PAD_LEFT);
						$arrUrutBarang[$fgbarcode_batch][$kd_barang] = $urutBarcodeCustom;	
					}
					/** END Counting urutan barang custom */
					$fgbarcode_barcode = $item_barcode . '-' . $arrUrutBarang[$fgbarcode_batch][$kd_barang] . '-' . $fgbarcode_batch . '-' . $fgbarcode_shift;
					$data[] = [
						'fgbarcode_kd' => $fgbarcode_kd,
						'barang_kd' => $kd_barang,
						'dndetailreceived_kd' => $eRcv['dndetailreceived_kd'],
						'fgbarcode_barcode' => $fgbarcode_barcode,
						'fgbarcode_batch' => $fgbarcode_batch,
						'fgbarcode_tglbatch' => $eRcv['dndetailreceived_tanggal'],
						'fgbarcode_shift' => $fgbarcode_shift,
						'fgbarcode_desc' => $eRcv['woitemso_itemdeskripsi'],
						'fgbarcode_dimensi' => $eRcv['woitemso_itemdimensi'],
						'fgbarcode_tglinput' => now_with_ms(),
						'admin_kd' => $this->session->userdata('kd_admin'),
					];
					$fgbarcode_kd = $this->td_finishgood_barcode->buat_kode_urut($fgbarcode_kd);
					$urutBarcode++;
					$urutBarcode = str_pad($urutBarcode, 3, "0", STR_PAD_LEFT);
					$arrUrutBarang[$fgbarcode_batch][$kd_barang] = $urutBarcode;
				}
				/** untuk update dnreceived */
				$dataUpdate[] = [
					'dndetailreceived_kd' => $eRcv['dndetailreceived_kd'],
					'dndetailreceived_tglbarcodefg' => date('Y-m-d H:i:s'),
				];
			}
			$act = $this->td_finishgood_barcode->insert_batch($data);
			$actUpdate = $this->td_deliverynote_received->update_batch($dataUpdate);
			if($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan');
			}
			else{
				$this->db->trans_commit();
				$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Data Tersimpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$fgbarcode_kd = $this->input->get('id', TRUE);
		if (!empty($fgbarcode_kd)) {
			/** Cek di barang masuk */
			$actIN = $this->td_finishgood_in->get_by_param(array('fgbarcode_kd' => $fgbarcode_kd));
			if ($actIN->num_rows() == 0) {
				$act = $this->td_finishgood_barcode->delete_data($fgbarcode_kd);
				if ($act) {
					$resp = array('code' => 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
				} else {
					$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Data Tidak Terhapus');
				}
			} else {
				$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Barang Sudah Masuk');
			}
		} else {
			$resp = array('code' => 400, 'status' => 'Gagal', 'pesan' => 'Data Tidak Ditemukan');
		}
		echo json_encode($resp);
	}

	public function cetak_barcode()
	{
		$this->load->library('Pdf');
		$barcodefg_kd = $this->uri->segment(5);
		if (!empty($barcodefg_kd)) {
			$rowData = $this->td_finishgood_barcode->get_by_id($barcodefg_kd)->row();
			$data['source'] = 'single';
			$data['rowData'] = $rowData;
			$data['qrcode'] = $rowData->fgbarcode_barcode;
			$data['class_link'] = $this->class_link;
			$this->load->view('page/' . $this->class_link . '/detail_pdf', $data);
		}
	}

	public function cetak_barcode_date()
	{
		$this->load->library('Pdf');
		$date = $this->uri->segment(5);
		// $date = $this->uri->segment(5);
		if (!empty($date)) {
			$date = format_date($date, 'Y-m-d');
			$rowData = $this->td_finishgood_barcode->get_by_dateBatch($date)->result();
			$data['source'] = 'date';
			$data['rowData'] = $rowData;
			$data['class_link'] = $this->class_link;
			$data['date'] = $date;
			$this->load->view('page/' . $this->class_link . '/detail_date_pdf', $data);
		}
	}

	// public function generate_qrcode(){
	// 	$this->load->library('ciqrcode');
	// 	$qrcode = $this->input->get('qrcode', true);
	// 	if(isset($qrcode)){
	// 		header("Content-Type: image/png");
	// 		$params['data'] = $qrcode;
	// 		$this->ciqrcode->generate($params);
	// 	}
	// 	else{
	// 		echo 'No Text Entered';
	// 	}	
	// }

	// public function generate_barcode () {
	// 	$barcode = $this->input->get('barcode', true);
	// 	//load library
	// 	$this->load->library('Zend');
	// 	//load in folder Zend
	// 	$this->zend->load('Zend/Barcode');
	// 	//generate barcode
	// 	$option = array (
	// 		'text'=>$barcode, 
	// 		'barHeight'=>100, 
	// 		'fontSize'=> 18,
	// 		'drawText' => false,
	// 	);
	// 	Zend_Barcode::render('code39', 'image', $option, array());
	// }

	// public function cetak_pdf(){
	// 	$this->load->library('Pdf');
	// 	$barcodefg_kd = $this->uri->segment(5);
	// 	if(!empty($barcodefg_kd)){
	// 		$rowData = $this->td_finishgood_barcode->get_by_id($barcodefg_kd)->row();
	// 		$data['source'] = 'single';
	// 		$data['rowData'] = $rowData;
	// 		$data['qrcode'] = $rowData->fgbarcode_barcode;
	// 		$data['class_link'] = $this->class_link;
	// 		$this->load->view('page/'.$this->class_link.'/detail_pdf', $data);
	// 	}

	// }

	/** Untuk bantuan cetak barcode */
	public function cetak_barcode_blm_out()
	{
		// SELECT a.*, b.fgbarcode_barcode, b.fgbarcode_desc, b.fgbarcode_dimensi, bb.item_code FROM td_finishgood_in AS a 
		// LEFT JOIN td_finishgood_barcode AS b ON a.fgbarcode_kd=b.fgbarcode_kd
		// LEFT JOIN tm_finishgood AS bb ON b.fg_kd=bb.fg_kd
		// LEFT JOIN td_finishgood_out AS c ON a.fgin_kd=c.fgin_kd
		// WHERE c.fgin_kd IS NULL AND DATE(a.fgin_tglinput) < '2019-07-23';
		$this->load->library('Pdf');

		$result = $this->db->select('a.*, b.fgbarcode_barcode, b.fgbarcode_desc, b.fgbarcode_dimensi, bb.item_code')
			->from('td_finishgood_in as a')
			->join('td_finishgood_barcode AS b', 'a.fgbarcode_kd=b.fgbarcode_kd', 'left')
			->join('tm_finishgood AS bb', 'b.fg_kd=bb.fg_kd', 'left')
			->join('td_finishgood_out AS c', 'a.fgin_kd=c.fgin_kd', 'left')
			->where('c.fgin_kd IS NULL')
			->where('DATE(a.fgin_tglinput) < ', '2019-07-23')
			->order_by('bb.item_barcode')
			->get()->result();
		$data['source'] = 'date';
		$data['rowData'] = $result;
		$data['date'] = 'blmout';
		$this->load->view('page/' . $this->class_link . '/detail_date_pdf', $data);
	}
}
