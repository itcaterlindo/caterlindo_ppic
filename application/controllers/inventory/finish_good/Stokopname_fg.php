<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Stokopname_fg extends MY_Controller {
	private $class_link = 'inventory/finish_good/stokopname_fg';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_finishgood_barcode', 'tm_finishgood_stokopname', 'tm_finishgood', 'td_finishgood_stokopname_detail', 'td_rak_ruang', 'td_finishgood_in', 'td_finishgood_out', 'tb_finishgood_user', 'tm_gudang']);
		/** Cek Lokasi User */
		$userfg = $this->tb_finishgood_user->get_by_param (array('kd_tipe_admin' => $this->session->userdata('tipe_admin_kd')))->row_array();
		$this->set_gudang = $userfg['kd_gudang'];
	}

	public function index() {
		parent::administrator();
		$this->form_box();
	}

	public function form_box() {
		parent::pnotify_assets();
		parent::ionicons_assets();
		parent::datetimepicker_assets();

		$actGudang = $this->tm_gudang->get_row($this->set_gudang);
		$data['nm_gudang'] = $actGudang->nm_gudang;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		$actAktif = $this->tm_finishgood_stokopname->get_byParam (array('fgstopname_status' => 'aktif', 'kd_gudang' => $this->set_gudang));
		$act = $actAktif;
		if ($actAktif->num_rows() == 0){
			$actNonAktif = $this->tm_finishgood_stokopname->get_byParam (array('fgstopname_status' => 'non_aktif', 'kd_gudang' => $this->set_gudang));
			$act = $actNonAktif;
		}

		$data['kd_gudang'] = $this->set_gudang;
		$data['rowStopname'] = $act->row_array();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$fgstopname_kd = $this->input->get('fgstopname_kd', true);

		$actGudang = $this->tm_gudang->get_row($this->set_gudang);
		$data['nm_gudang'] = $actGudang->nm_gudang;
		$data['class_link'] = $this->class_link;
		$data['js_file'] = 'table_js'; 
		$data['fgstopname_kd'] = $fgstopname_kd;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$fgstopname_kd = $this->input->get('fgstopname_kd', true);

		$data['class_link'] = $this->class_link;
		$data['fgstopname_kd'] = $fgstopname_kd;
		
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$fgstopname_kd = $this->input->get('fgstopname_kd', true);

		$data = $this->td_finishgood_stokopname_detail->ssp_table($fgstopname_kd, $this->set_gudang);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function setperiode () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$actGudang = $this->tm_gudang->get_row($this->set_gudang);
		$data['nm_gudang'] = $actGudang->nm_gudang;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/setperiode_box', $data);
	}

	public function masteropname_table(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;
		$data['resultOpname'] = $this->tm_finishgood_stokopname->get_all_detail()->result_array();
		$this->load->view('page/'.$this->class_link.'/masteropname_table', $data);
	}
	
	public function masteropname_form () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		/** Opsi Gudang */
		$actopsiGudang = $this->tm_gudang->get_all()->result_array();
		foreach ($actopsiGudang as $eachGudang):
			$opsiGudang[$eachGudang['kd_gudang']] = $eachGudang['nm_gudang'];
		endforeach;

		$data['opsiGudang'] = $opsiGudang;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/masteropname_form', $data);
	}

	public function action_master_opname(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id');
		
		$id = explode('/', $id);
		$kd = $id[0];
		$sts = $id[1];
		if ($sts != 'delete'){
			$act = $this->tm_finishgood_stokopname->update_data(array('fgstopname_kd'=> $kd), array('fgstopname_status'=> $sts, 'fgstopname_tgledit'=>date('Y-m-d H:i:s'), 'admin_kd' => $this->session->userdata('kd_admin')));
			if ($sts == 'aktif' || $sts = 'non_aktif'){
				$actTmOpname = $this->tm_finishgood_stokopname->get_byId($kd)->row_array();
				$resp['data'] = $actTmOpname;
			}
		}else{
			//delete
			$act = $this->tm_finishgood_stokopname->delete_data($kd);
		}
		
		if ($act){
			$resp['code'] = 200;
			$resp['status'] = 'Sukses';
			$resp['pesan'] = 'Data Tersimpan';
		}else{
			$resp['code'] = 400;
			$resp['status'] = 'Gagal';
			$resp['pesan'] = 'Gagal Tersimpan';
		}
		
		echo json_encode($resp);
	}

	public function rekap_opname_box(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$jnsRekap = $this->input->get('jnsRekap', true);
		$fgstopname_kd = $this->input->get('fgstopname_kd', true);

		if ($jnsRekap == 'penyimpanan'){
			$url = base_url().$this->class_link.'/rekap_opname_penyimpanan?fgstopname_kd='.$fgstopname_kd;
		}elseif ($jnsRekap == 'stok'){
			$url = base_url().$this->class_link.'/rekap_opname_stok?fgstopname_kd='.$fgstopname_kd;
		}elseif($jnsRekap == 'abnormal'){
			$url = base_url().$this->class_link.'/abnormalstokopname_table?fgstopname_kd='.$fgstopname_kd;
		}

		$data['url'] = $url;
		$data['class_link'] = $this->class_link;

		$this->load->view('page/'.$this->class_link.'/rekap_opname_box', $data);
	}

	public function rekap_opname_penyimpanan () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$fgstopname_kd = $this->input->get('fgstopname_kd', true);
		$actOpname = $this->db->select('td_finishgood_stokopname_detail.*, td_finishgood_in.rakruangkolom_kd AS rakruangkolom_stok, td_finishgood_in.fgin_qty, td_finishgood_barcode.*, tm_barang.item_code')
				->from('td_finishgood_stokopname_detail')
				->join('td_finishgood_in', 'td_finishgood_stokopname_detail.fgin_kd = td_finishgood_in.fgin_kd', 'left')
				->join('td_finishgood_barcode', 'td_finishgood_stokopname_detail.fgbarcode_kd = td_finishgood_barcode.fgbarcode_kd', 'left')
				->join('tm_barang', 'td_finishgood_barcode.barang_kd=tm_barang.kd_barang', 'left')
				->where(array('td_finishgood_stokopname_detail.fgstopname_kd' => $fgstopname_kd))
				->get();
		$actRuangKolom = $this->td_rak_ruang_kolom->get_all();
		
		if ($actOpname->num_rows() != 0){
			$actOpname = $actOpname->result_array();
			$actRuangKolom = $actRuangKolom->result_array();
			/** Foreach untuk item opname */
			foreach($actOpname as $eachOpname){
				$rakRuang_data = 'kosong';
				$rakRuang_opname = 'kosong';
				/** FOreach untuk ruang untuk tampilan agar tidak selalu ambil di database*/
				foreach ($actRuangKolom as $eachRuangKolom){	
					/** nama rak ruang di opname */
					if($eachOpname['rakruangkolom_kd'] == $eachRuangKolom['rakruangkolom_kd']){
						$rakRuang_opname = $eachRuangKolom['nm_gudang'].'/'.$eachRuangKolom['nm_rak'].'/'.$eachRuangKolom['nm_rak_ruang'].'/'.$eachRuangKolom['rakruangkolom_nama'];
					}				
					/** nama rak ruang di stok */
					if($eachOpname['rakruangkolom_stok'] == $eachRuangKolom['rakruangkolom_kd']){
						$rakRuang_data = $eachRuangKolom['nm_gudang'].'/'.$eachRuangKolom['nm_rak'].'/'.$eachRuangKolom['nm_rak_ruang'].'/'.$eachRuangKolom['rakruangkolom_nama'];
					}
				}
				/** cek status lokasi penyimpanan rak */
				if($eachOpname['rakruangkolom_kd'] == $eachOpname['rakruangkolom_stok']){
					$statusRuang = 'sama';
				}else{
					$statusRuang = 'beda';
				}

				$aResult[] = array(
					'statusRuang' => $statusRuang,
					'fgbarcode_barcode' => $eachOpname['fgbarcode_barcode'],
					'item_code' => $eachOpname['item_code'],
					'fgbarcode_desc' => $eachOpname['fgbarcode_desc'],
					'fgbarcode_dimensi' => $eachOpname['fgbarcode_dimensi'],
					'rakRuang_data' => $rakRuang_data,
					'rakRuang_opname' => $rakRuang_opname,
					'opnameKeterangan' => $eachOpname['fgstopnamedetail_ket'],
				);
			}
		}
		else{
			$aResult = array();
		}
		asort($aResult);

		/** nm_gudang */
		$actGudang = $this->tm_gudang->get_row($this->set_gudang);
		$data['nm_gudang'] = $actGudang->nm_gudang;
		$data['item_opname'] = $aResult;
		
		$this->load->view('page/'.$this->class_link.'/rekap_opname_penyimpanan', $data);
	}

	public function rekap_opname_stok () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$fgstopname_kd = $this->input->get('fgstopname_kd', true);

		$actfg = $this->tm_finishgood->get_where_kd_gudang($this->set_gudang)->result_array();
		$actOpname = $this->td_finishgood_stokopname_detail->get_by_groupby_fg ($fgstopname_kd);
				
		if ($actOpname->num_rows() != 0){
			$actOpname = $actOpname->result_array();
			foreach ($actfg as $eachfg){
				$qty_opname = 0;
				foreach ($actOpname as $eachOpname){
					if($eachOpname['fg_kd'] == $eachfg['fg_kd']){
						$qty_opname = $eachOpname['sum_opname'];
					}
					if ($qty_opname == $eachfg['fg_qty'] ){
						$statusQty = 'sama';
					}else{
						$statusQty = 'beda';
					}	
				}

				$aResult[] = array(
					'statusQty' => $statusQty,
					'item_code' => $eachfg['item_code'],
					'item_barcode' => $eachfg['item_barcode'],
					'item_desc' => $eachfg['deskripsi_barang'],
					'item_dimensi' => $eachfg['dimensi_barang'],
					'qty_data' => $eachfg['fg_qty'] ,
					'qty_opname' => $qty_opname,
				);
			}
		}else{
			$aResult = array();
		}

		asort($aResult);

		/** nm_gudang */
		$actGudang = $this->tm_gudang->get_row($this->set_gudang);
		$data['nm_gudang'] = $actGudang->nm_gudang;
		$data['resultOpnameQty'] = $aResult;
		
		$this->load->view('page/'.$this->class_link.'/rekap_opname_stok', $data);
	}
	
	public function abnormalstokopname_table() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$fgstopname_kd = $this->input->get('fgstopname_kd', true);

		$qFgstokopnamedt = $this->db->select('a.*, d.*, e.item_code, c.fgout_kd')
				->from('td_finishgood_stokopname_detail as a')
				->join('td_finishgood_in as b', 'b.fgin_kd=a.fgin_kd', 'left')
				->join('td_finishgood_out as c', 'c.fgin_kd=b.fgin_kd', 'left')
				->join('td_finishgood_barcode as d', 'd.fgbarcode_kd=a.fgbarcode_kd', 'left')
				->join('tm_barang as e', 'e.kd_barang=d.barang_kd', 'left')
				->where('a.fgstopname_kd', $fgstopname_kd)
				->get()
				->result_array();

		$actRuangKolom = $this->td_rak_ruang_kolom->get_all()->result_array();
		$arrayFGdt = [];
		foreach ($qFgstokopnamedt as $dt) {
			$dt['status'] =  '';
			$dt['rakRuang_opname'] =  '';
			if (!empty($dt['fgout_kd'])) {
				$dt['status'] =  'Sudah Keluar';
			}
			if (empty($dt['fgin_kd'])) {
				$dt['status'] =  'Belum Masuk';
			}
			foreach ($actRuangKolom as $eachRuangKolom){	
				/** nama rak ruang di opname */
				if($dt['rakruangkolom_kd'] == $eachRuangKolom['rakruangkolom_kd']){
					$dt['rakRuang_opname'] =  $eachRuangKolom['nm_gudang'].'/'.$eachRuangKolom['nm_rak'].'/'.$eachRuangKolom['nm_rak_ruang'].'/'.$eachRuangKolom['rakruangkolom_nama'];
				}				
			}
			
			if (empty($dt['status'])){
				continue;
			}
			$arrayFGdt[] = $dt;
		}
		$actGudang = $this->tm_gudang->get_row($this->set_gudang);
		$data['result'] = $arrayFGdt;
		$data['nm_gudang'] = $actGudang->nm_gudang;
		$data['class_link'] = $this->class_link;
		
		$this->load->view('page/'.$this->class_link.'/abnormalstokopname_table', $data);
	}

	public function action_submit_periode() {
		$stopname_periode = $this->input->get('stopname_periode', true);
		$kd_gudang = $this->input->get('kd_gudang', true);

		$periode = str_replace("-","",$stopname_periode);

		if (!empty($stopname_periode)){
			/**cek periode */
			$actCekPeriode = $this->tm_finishgood_stokopname->get_byParam (array('fgstopname_periode' => $periode));
			if ($actCekPeriode->num_rows() == 0){
				$data = array(
					'fgstopname_kd' => $this->tm_finishgood_stokopname->buat_kode(),
					'kd_gudang' => $kd_gudang,
					'fgstopname_periode' => $periode,
					'fgstopname_tglperiode' => format_date($stopname_periode, 'Y-m-d'),
					'fgstopname_status' => 'non_aktif',
					'fgstopname_tglinput' => date('Y-m-d H:i:s'),
					'fgstopname_tgledit' => null,
					'admin_kd' => $this->session->userdata('kd_admin'),
				);
				$act = $this->tm_finishgood_stokopname->insert_data ($data);
				if ($act){
					$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Tersimpan', 'data' => $data);
				}else{
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan');
				}
			}else{
				$resp = array('code'=> 401, 'status' => 'Sukses', 'pesan' => 'Periode Sudah ada', 'data' => $actCekPeriode);
			}
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Kosong');
		}

		echo json_encode($resp);
	}

	public function get_barcode_from_stock() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$barcodefg_barcode = $this->input->get('id');
		if(!empty($barcodefg_barcode)){
			/**  Cari barcode*/
			$actCekBarcode = $this->td_finishgood_in->get_id_by_barcode($barcodefg_barcode);
			if ($actCekBarcode->num_rows() != 0){
				$actCekBarcode = $actCekBarcode->row_array();
				$resp['code']=200;
				$resp['status']='Sukses';
				$resp['data']=$actCekBarcode;
			}else{
				$resp['code']=400;
				$resp['status']='Gagal';
				$resp['pesan']='Data tidak ditemukan';
			}
			
			echo json_encode($resp);
		}
	}

	private function lokasi_penyimpanan ($kd_rak_ruang) {
		if (!empty($kd_rak_ruang)){
			$actRuang = $this->td_rak_ruang->get_byParam(array('kd_rak_ruang'=>$kd_rak_ruang))->row_array();
		}else{
			$actRuang = array();
		}

		return $actRuang;
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtBarcode', 'Barcode', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtPeriode', 'Periode', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtRuangKolom', 'Ruang Kolom', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrBarcode' => (!empty(form_error('txtBarcode')))?buildLabel('warning', form_error('txtBarcode', '"', '"')):'',
				'idErrPeriode' => (!empty(form_error('txtPeriode')))?buildLabel('warning', form_error('txtPeriode', '"', '"')):'',
				'idErrRuangKolom' => (!empty(form_error('txtRuangKolom')))?buildLabel('warning', form_error('txtRuangKolom', '"', '"')):'',
			);
			
		}else {
			$fg_kd = $this->input->post('txtfg_kd', true);
			$fgin_kd = $this->input->post('txtfgin_kd', true);
			$fgstopname_kd = $this->input->post('txtfgstopname_kd', true);
			$fgstopnamedetail_qty =  $this->input->post('txtQty', true);
			$fgstopnamedetail_ket =  $this->input->post('txtKet', true);
			$rakruangkolom_kd = $this->input->post('txtRuangKolom', true);
			$barcode = $this->input->post('txtBarcode', true);
			$kd_gudang = $this->input->post('txtGudang', true);
			
			/** Get FGIN dan FG_KD */
			$qBarcode = $this->db->select('a.fgbarcode_kd, a.barang_kd, b.fg_kd')
				->from('td_finishgood_barcode as a')
				->join('tm_finishgood as b', 'b.barang_kd=a.barang_kd', 'left')
				->where('a.fgbarcode_barcode', $barcode)
				->where('b.kd_gudang', $kd_gudang)
				->get()->row_array();
			
			$fgins = $this->db->select('a.fgin_kd, a.fgin_qty, b.fgout_kd')
				->from('td_finishgood_in as a')
				->join('td_finishgood_out as b', 'b.fgin_kd=b.fgin_kd', 'left')
				->where('a.fgbarcode_kd', $qBarcode['fgbarcode_kd'])
				->order_by('a.fgin_tglinput', 'desc')
				->get()->result_array();
				
			$fgin_kd = isset($fgins[0]['fgin_kd']) ? $fgins[0]['fgin_kd'] : null;
			$fgin_qty = isset($fgins[0]['fgin_qty']) ? $fgins[0]['fgin_qty'] : 1;
			foreach ($fgins as $fgin) {
				if (empty($fgin['fgout_kd'])) {
					$fgin_kd = $fgin['fgin_kd'];
					$fgin_qty = $fgin['fgin_qty'];
				}
			}

			$data = array(
				'fgstopnamedetail_kd' => $this->td_finishgood_stokopname_detail->buat_kode(),
				'fgstopname_kd' => $fgstopname_kd,
				'fg_kd' => isset($qBarcode['fg_kd']) ? $qBarcode['fg_kd'] : null,
				'fgin_kd' => isset($fgin_kd) ? $fgin_kd : null,
				'fgbarcode_kd' => $qBarcode['fgbarcode_kd'],
				'rakruangkolom_kd' => $rakruangkolom_kd,
				'fgstopnamedetail_qty' => !empty($fgin_qty) ? $fgin_qty : 1,
				'fgstopnamedetail_ket' => $fgstopnamedetail_ket,
				'fgstopnamedetail_tglinput' => now_with_ms(),
				'fgstopnamedetail_tgledit' => null,
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			/** cek apakah sudah ada fgdetail_kd nya */
			$cekFgbarcode = $this->db->where('fgbarcode_kd', $qBarcode['fgbarcode_kd'])
				->where('fgstopname_kd', $fgstopname_kd)
				->get('td_finishgood_stokopname_detail');
			if ($cekFgbarcode->num_rows() == 0){
				$act = $this->td_finishgood_stokopname_detail->insert_data($data);
				if ($act){
					$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Tersimpan');
				}else{
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan');
				}
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Barcode Sudah Masuk');
			}
			
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$fgstopnamedetail_kd = $this->input->get('id', TRUE);
		if (!empty($fgstopnamedetail_kd)){
			$act = $this->td_finishgood_stokopname_detail->delete_data($fgstopnamedetail_kd);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Terhapus');
			}
		}
		else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Tidak Sesuai');
		}
		echo json_encode($resp);
	}

}
