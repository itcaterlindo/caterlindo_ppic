<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Barangmasuk_fg extends MY_Controller {
	private $class_link = 'inventory/finish_good/barangmasuk_fg';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_finishgood_barcode', 'td_finishgood_in', 'tm_finishgood', 'td_finishgood_out', 'tm_finishgood_stokopname', 'tb_finishgood_user', 'tm_gudang', 
				'tm_barang', 'td_rak_ruang', 'model_kartustokfg']);
		/** Cek Lokasi User */
		$userfg = $this->tb_finishgood_user->get_by_param (array('kd_tipe_admin' => $this->session->userdata('tipe_admin_kd')))->row_array();
		$this->set_gudang = $userfg['kd_gudang'];
		/** Cek stok opname */
		$cekOpname = $this->tm_finishgood_stokopname->get_byParam (array('fgstopname_status' => 'aktif', 'kd_gudang' => $this->set_gudang));
		if ($cekOpname->num_rows() != 0){
		?>
			<script>
				alert('Maaf Masih Dilakukan StokOpname!');
				window.location.replace('<?php echo base_url(); ?>');
			</script>
		<?php
		}
	}

	public function index() {
		parent::administrator();
		$this->form_box();
	}

	public function form_box() {
		parent::pnotify_assets();
		parent::ionicons_assets();
		parent::datetimepicker_assets();

		$actGudang = $this->tm_gudang->get_row($this->set_gudang);
		$data['nm_gudang'] = $actGudang->nm_gudang;
		$data['kd_gudang'] = $actGudang->kd_gudang;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		$kd_gudang = $this->input->get('kd_gudang', true);
		
		$data['class_link'] = $this->class_link;
		$data['gudang'] = $this->tm_gudang->get_row($kd_gudang);
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		$actGudang = $this->tm_gudang->get_row($this->set_gudang);
		$data['nm_gudang'] = $actGudang->nm_gudang;
		$data['class_link'] = $this->class_link;
		$data['js_file'] = 'table_js'; 
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$fgin_tglinput = $this->input->get('fgin_tglinput', true);
		$data['class_link'] = $this->class_link;
		$data['fgin_tglinput'] = $fgin_tglinput;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$fgin_tglinput = $this->input->get('fgin_tglinput', TRUE);
		$fgin_tglinput = format_date($fgin_tglinput, 'Y-m-d');
		$data = $this->td_finishgood_in->ssp_table($fgin_tglinput);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function get_rak(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$gudang_kd = $this->input->get('gudang_kd', true);
		if(!empty($gudang_kd)){
			$act = $this->tm_rak->get_byParam(array('gudang_kd'=>$gudang_kd));
			if($act->num_rows() != 0){
				$resp['code'] = 200;
				$resp['status'] = 'Sukses';
				$resp['pesan'] = '';
				$resp['data'] = $act->result_array();
			}else{
				$resp['code'] = 400;
				$resp['status'] = 'Gagal';
				$resp['pesan'] = 'Data Tidak ditemukan';
			}
		}else{
			$resp['code'] = 400;
			$resp['status'] = 'Gagal';
			$resp['pesan'] = 'Input tidak sesuai';
		}

		echo json_encode($resp);
	}

	public function get_ruang(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$rak_kd = $this->input->get('rak_kd', true);
		if(!empty($rak_kd)){
			$act = $this->td_rak_ruang->get_byParam(array('rak_kd'=>$rak_kd));
			if($act->num_rows() != 0){
				$resp['code'] = 200;
				$resp['status'] = 'Sukses';
				$resp['pesan'] = '';
				$resp['data'] = $act->result_array();
			}else{
				$resp['code'] = 400;
				$resp['status'] = 'Gagal';
				$resp['pesan'] = 'Data Tidak ditemukan';
			}
		}else{
			$resp['code'] = 400;
			$resp['status'] = 'Gagal';
			$resp['pesan'] = 'Input tidak sesuai';
		}

		echo json_encode($resp);
	}

	public function get_ruang_kolom(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$kd_rak_ruang = $this->input->get('kd_rak_ruang', true);
		if(!empty($kd_rak_ruang)){
			$act = $this->td_rak_ruang_kolom->get_byParam(array('kd_rak_ruang'=>$kd_rak_ruang));
			if($act->num_rows() != 0){
				$resp['code'] = 200;
				$resp['status'] = 'Sukses';
				$resp['pesan'] = '';
				$resp['data'] = $act->result_array();
			}else{
				$resp['code'] = 400;
				$resp['status'] = 'Gagal';
				$resp['pesan'] = 'Data Tidak ditemukan';
			}
		}else{
			$resp['code'] = 400;
			$resp['status'] = 'Gagal';
			$resp['pesan'] = 'Input tidak sesuai';
		}

		echo json_encode($resp);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtBarcode', 'Barcode', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtQtyin', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrBarcode' => (!empty(form_error('txtBarcode')))?buildLabel('warning', form_error('txtBarcode', '"', '"')):'',
				'idErrQtyin' => (!empty(form_error('txtQtyin')))?buildLabel('warning', form_error('txtQtyin', '"', '"')):'',
			);
			
		}else {
			$kd_gudang = $this->input->post('txtkd_gudang');
			$barcodefg_barcode = $this->input->post('txtBarcode');
			$fgin_qty =  $this->input->post('txtQtyin');
			$rakruangkolom_kd =  $this->input->post('txtrakruangkolom_kd');
			$barangCustom = false;
			$kartustok = [];

			/** Get last data for custom/PJ */
			$barcodeFG = $this->db->select('fgbarcode_kd, barang_kd')
				->where('fgbarcode_barcode', $barcodefg_barcode)
				->get('td_finishgood_barcode')->row_array();
			/** Untuk check apakah barang custom atau std berdasarkan item group */
			if(!empty($barcodeFG)){
				$barang = $this->tm_barang->get_item(['kd_barang' => $barcodeFG['barang_kd']]);
				if($barang->item_group_kd == '2'){
				/** Get last data untuk ambil startdate kartu stok custom */
				$fgCustom = $this->db->select('tm_finishgood.fg_kd, tm_finishgood.kd_gudang, tm_finishgood.barang_kd, tm_finishgood.item_code, tm_finishgood.item_barcode, tm_finishgood.fg_tglinput')
						->from('tm_finishgood')
						->join('td_finishgood_in', 'td_finishgood_in.fg_kd=tm_finishgood.fg_kd', 'inner')
						->where('tm_finishgood.item_barcode', '899000')
						->order_by('tm_finishgood.fg_tglinput', 'DESC')
						->limit(1)
						->get()
						->row_array();
				$barangCustom = true;
				}
			}
			/** End Get last data for custom/PJ */
			
			/** Get FGKD last stock */
			$qFG = $this->db->select('td_finishgood_barcode.fgbarcode_kd, td_finishgood_barcode.barang_kd, 
						tm_finishgood.fg_kd, tm_finishgood.fg_qty, tm_finishgood.kd_gudang, tm_finishgood.item_barcode, tm_finishgood.fg_tglinput')
					->from('td_finishgood_barcode')
					->join('tm_finishgood', 'td_finishgood_barcode.barang_kd=tm_finishgood.barang_kd', 'left')
					->where(['td_finishgood_barcode.fgbarcode_barcode' => $barcodefg_barcode, 'tm_finishgood.kd_gudang' => $kd_gudang])
					->get()->row_array();
			$fgbarcode_kd = ''; $fg_kd = '';
			if (!empty($qFG)){
				$fg_kd = $qFG['fg_kd'];
				$fgin_qty_awal = $qFG['fg_qty'];
				$fgbarcode_kd = $qFG['fgbarcode_kd'];
			}else{
				$qBarcode = $this->db->select('fgbarcode_kd, barang_kd')->where('fgbarcode_barcode', $barcodefg_barcode)->get('td_finishgood_barcode')->row_array();
				$fg_kd = $this->tm_finishgood->create_fg_kd($qBarcode['barang_kd']);
				$fgin_qty_awal = 0;
				$fgbarcode_kd = $qBarcode['fgbarcode_kd'];
			}
			if (empty($fg_kd) || empty($fgbarcode_kd)){
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan a');
				echo json_encode($resp);die();
			}

			/** Jika barang custom */
			if($barangCustom == true){
				/** Jika barang custom / PJ setting qty awal ambil dari qty (sisa) kartu stock (field sisa) */
				$startdate = format_date($fgCustom['fg_tglinput'], 'Y-m-d');
				$enddate = date('Y-m-d');
				$item_code = $fgCustom['barang_kd'];
				//$kd_gudang = $fgCustom['kd_gudang'];
				
				$kartustok = $this->model_kartustokfg->kartu_stok_all($startdate, $enddate, $item_code, $kd_gudang);

				if (!empty($kartustok) && is_array($kartustok)) {  
					usort($kartustok, function ($a, $b) {  
						// Pastikan tanggal kedua elemen adalah objek DateTime  
						if (!$a['tgl_trans'] instanceof DateTime || !$b['tgl_trans'] instanceof DateTime) {  
							return 0; // Jika salah satu bukan DateTime, anggap sama untuk menghindari error  
						}  
						return $a['tgl_trans']->getTimestamp() - $b['tgl_trans']->getTimestamp();  
					}); 
				}
				/** Ambil sisa data terakhir */
				$fgin_qty_awal = $kartustok[count($kartustok)-1]['sisa'];
				//$fgin_qty_awal =0;
			}

			$data = array(
				'fgin_kd' => $this->td_finishgood_in->buat_kode(),
				'fg_kd' => $fg_kd,
				'fgbarcode_kd' => $fgbarcode_kd,
				'fgin_qty_awal' => $fgin_qty_awal,
				'fgin_qty' => $fgin_qty,
				'rakruangkolom_kd' => !empty($rakruangkolom_kd) ? $rakruangkolom_kd : null,
				'fgin_tglinput' => now_with_ms(),
				'fgin_tgledit' => null,
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			/** Cek apa sudah di input di td_finishgood_in */
			$actCek = $this->td_finishgood_in->get_by_param (array( 'fg_kd' => $fg_kd, 'fgbarcode_kd' => $fgbarcode_kd ))->num_rows();
			if ($actCek == 0){
				/** Insert td_finishgood_in */
				$this->db->trans_begin();
				$act = $this->td_finishgood_in->insert_data($data);
				$actUpdate = $this->tm_finishgood->update_stock($fg_kd, $fgin_qty, 'IN');
				if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan', 'data' => $data);
				}else {
					$this->db->trans_commit();
					$resp = 
					array(
						'code'=> 200,
					 	'status' => 'Sukses', 
						'pesan' => 'Data Tersimpan', 
						// 'data' => $data, 
						// 'brg_cst' => $barangCustom,
						// 'st_date' => $startdate, 
						// 'enddate' => $enddate, 
						// 'item_code' => $item_code,
						// 'fg_custom' => $fgCustom
					);
				}
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Barang sudah masuk', 'data' => $data);
			}
		}

		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$fgin_kd = $this->input->get('id', TRUE);
		if (!empty($fgin_kd)){
			/** Cek dulu apakah ini input terakhir */
			$cekMaxTrans = false;
			$actBarcodeFG = $this->td_finishgood_in->get_barcode_by_param(array('td_finishgood_in.fgin_kd'=>$fgin_kd))->row_array();
			$maxKD = $this->td_finishgood_in->get_kd_max_trans($actBarcodeFG['fg_kd']);
			if ($fgin_kd == $maxKD){
				$cekMaxTrans = true;
			}

			if ($cekMaxTrans){
				/** Update Tm stock sebelum dihapus */
				$actStock = $this->tm_finishgood->update_stock($actBarcodeFG['fg_kd'], $actBarcodeFG['fgin_qty'], 'OUT');

				if ($actStock){ 
					/** Jika sukses update stock baru dihapus */
					$actDel = $this->td_finishgood_in->delete_data($fgin_kd);
					if ($actDel == true){
						$resp = array('code'=> 201, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
					}else{
						$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
					}
				}else{
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Update Stock');
				}	
			} else {
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Data Bukan Input Terakhir');
			}
		}
		else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Tidak Sesuai');
		}
		echo json_encode($resp);
	}
	
}
