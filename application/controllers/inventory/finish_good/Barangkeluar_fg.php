<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Barangkeluar_fg extends MY_Controller {
	private $class_link = 'inventory/finish_good/barangkeluar_fg';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_finishgood_barcode', 'td_finishgood_out', 'tm_finishgood', 
		'tm_salesorder', 'td_salesorder_item', 'td_salesorder_item_detail','tm_gudang', 
		'td_product_grouping', 'td_finishgood_in', 'td_finishgood_adjustment', 'td_finishgood_repacking', 'tm_finishgood_stokopname', 'model_kartustokfg']);
		/** Cek Lokasi User */
		$userfg = $this->tb_finishgood_user->get_by_param (array('kd_tipe_admin' => $this->session->userdata('tipe_admin_kd')))->row_array();
		$this->set_gudang = $userfg['kd_gudang'];
		/** Cek stok opname */
		$cekOpname = $this->tm_finishgood_stokopname->get_byParam (array('fgstopname_status' => 'aktif', 'kd_gudang' => $this->set_gudang));
		if ($cekOpname->num_rows() != 0){
		?>
			<script>
				alert('Maaf Masih Dilakukan StokOpname!');
				window.location.replace('<?php echo base_url(); ?>');
			</script>
		<?php
		}
	}

	public function index() {
		parent::administrator();
		$this->form_box();
	}

	public function form_box() {
		parent::pnotify_assets();
		parent::ionicons_assets();
		parent::select2_assets();

		$data['gudang'] = $this->tm_gudang->get_row($this->set_gudang);
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		$kd_gudang = $this->input->get('kd_gudang', true);
		$actSO = $this->tm_salesorder->get_where_param(array('a.status_so'=> 'process_lpo'), array('a.status_so'=> 'process_wo'), '')->result_array();

		/** Opsi SO */
		$opsiSO[null] = '-- Pilih Opsi --';
		foreach ($actSO as $eachSO):
			if (empty($eachSO['tgl_kirim']) || $eachSO['tgl_kirim'] == '0000-00-00') {
				continue;
			}
			if ($eachSO['tipe_customer'] == 'Lokal'){
				$namaSO = $eachSO['no_salesorder'].' | '.$eachSO['nm_customer'];
			}else{
				$namaSO = $eachSO['no_po'].' | '.$eachSO['nm_customer'];
			}
			$now = strtotime('today');
			$newtWeek = strtotime('+7 day', $now);
			if (strtotime($eachSO['tgl_kirim']) <= $newtWeek) {
				$opsiSO[$eachSO['kd_msalesorder']] = $namaSO;
			}
		endforeach;
		
		$data['kd_gudang'] = $kd_gudang;
		$data['opsiSO'] = $opsiSO;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$kd_msalesorder = $this->input->get('kd_msalesorder', true);

		$actGudang = $this->tm_gudang->get_row($this->set_gudang);
		$data['nm_gudang'] = $actGudang->nm_gudang;
		$data['class_link'] = $this->class_link;
		$data['js_file'] = 'table_js'; 
		$data['kd_msalesorder'] = $kd_msalesorder;
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$kd_msalesorder = $this->input->get('kd_msalesorder', true);

		$data['class_link'] = $this->class_link;
		$data['kd_msalesorder'] = $kd_msalesorder;
		
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$kd_msalesorder = $this->input->get('kd_msalesorder', true);

		$data['kd_msalesorder'] = $kd_msalesorder;
		$data = $this->td_finishgood_out->ssp_table2($kd_msalesorder, $this->set_gudang);
		// $data = $this->td_finishgood_out->ssp_table($kd_msalesorder, $this->set_gudang);
		// echo json_encode(
			// 	SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
			// );
		echo json_encode($data);
	}

	public function detail_salesorder_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$kd_msalesorder = $this->input->get('kd_msalesorder', true);

		$data['class_link'] = $this->class_link;
		$data['kd_msalesorder'] = $kd_msalesorder;

		$this->load->view('page/'.$this->class_link.'/detail_salesorder_box', $data);
	}

	private function groupingDetailSO($data){
		$groups = array();
		foreach ($data as $item) {
			$key = $item['barang_kd'];
			/** jika tidak ada key */
			if (!array_key_exists($key, $groups)) {
				$groups[$key] = array(
					'kd_msalesorder' => $item['kd_msalesorder'],
					'no_salesorder' => $item['no_salesorder'],
					'barang_kd' => $key,
					'item_barcode' => $item['item_barcode'],
					'item_code' => $item['item_code'],
					'item_status' => $item['item_status'],
					'qty' => $item['qty'],
					'deskripsi_barang' => $item['deskripsi_barang'],
					'dimensi_barang' => $item['dimensi_barang'],
					'nm_group' => $item['nm_group'],
					'kd_mgrouping' => $item['kd_mgrouping'],
				);
			} else {
				$groups[$key]['qty'] = $groups[$key]['qty'] + $item['qty'];
			}
		}
		
		foreach ($groups as $each){
			$groupResult[] = $each;
		}
		return $groupResult;
	}

	public function detail_salesorder_main() {
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }
		$kd_msalesorder = $this->input->get('kd_msalesorder', true);
		$aDetilGrouping = [];
		/** Untuk rekap SO dan grouping */
		$actItemSO_rekap = $this->tm_salesorder->get_rekap_itemstatus_by_nosalesorder($kd_msalesorder);
		$itemSO_rekap_grouping = array();
		foreach($actItemSO_rekap as $eachSO):
			if($eachSO['kd_mgrouping']==null){
				$itemSO_rekap_grouping [] = $eachSO;
			}else{
				$no_salesorder = $eachSO['no_salesorder'];
				$actGrouping = (array) $this->td_product_grouping->get_item($eachSO['kd_mgrouping']);
				foreach ($actGrouping as $eachGrouping):
					$aDetilGrouping[] = array(
						'kd_msalesorder' => $kd_msalesorder,
						'no_salesorder' => $no_salesorder,
						'barang_kd' => $eachGrouping->barang_kd,
						'item_barcode' => $eachGrouping->item_barcode,
						'item_code' => $eachGrouping->item_code,
						'item_status' => 'std',
						'qty' => $eachSO['qty'],
						'deskripsi_barang' => $eachGrouping->deskripsi_barang,
						'dimensi_barang' => $eachGrouping->dimensi_barang,
						'nm_group' => $eachGrouping->nm_group,
						'kd_mgrouping' => null,
					);
				endforeach;
			}
		endforeach;
		/** Rekap SO sesuai dengan sales order kode*/
		/** Cek apakah ada grouping  */
		if (!empty($aDetilGrouping)){
			$RekapSO_grouping = array_merge($itemSO_rekap_grouping, $aDetilGrouping);
			$RekapSO_grouping = $this->groupingDetailSO($RekapSO_grouping);
		}else{
			$RekapSO_grouping = $itemSO_rekap_grouping;
		}

		$actFGOut = $this->td_finishgood_out->get_rekap_by_kd_msalesorder ($kd_msalesorder)->result_array();
		$actIdentitasSO = $this->tm_salesorder->get_by_id($kd_msalesorder)->row_array();	

		$data['class_link'] = $this->class_link;
		$data['kd_msalesorder'] = $kd_msalesorder;
		$data['item_so_grouping'] = $RekapSO_grouping;		
		$data['cek'] = $aDetilGrouping;	
		$data['fg_out'] = $actFGOut;
		$data['identitasSO'] = $actIdentitasSO;

		// header('Content-Type: application/json');
		// echo json_encode($data);

		$this->load->view('page/'.$this->class_link.'/detail_salesorder_main', $data);
	}

	public function form_history_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		/** SO finish */
		$actSO = $this->tm_salesorder->get_by_status(array('finish'))->result_array();
		foreach ($actSO as $eachSO):
			if ($eachSO['tipe_customer'] == 'Lokal'){
				$namaSO = $eachSO['no_salesorder'].' | '.$eachSO['nm_customer'];
			}else{
				$namaSO = $eachSO['no_po'].' | '.$eachSO['nm_customer'];
			}
			$opsiSO[$eachSO['kd_msalesorder']] = $namaSO;
		endforeach;

		$data['opsiSO'] = $opsiSO;

		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_history_main', $data);
	}

	public function action_insert() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtBarcode', 'Barcode', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtNo_salesorder', 'No salesorder', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrBarcode' => (!empty(form_error('txtBarcode')))?buildLabel('warning', form_error('txtBarcode', '"', '"')):'',
				'idErrNo_salesorder' => (!empty(form_error('txtNo_salesorder')))?buildLabel('warning', form_error('txtNo_salesorder', '"', '"')):'',
			);
			
		}else {
			$kd_msalesorder = $this->input->post('txtNo_salesorder', true);
			$fgbarcode_barcode = $this->input->post('txtBarcode', true);
			$kd_gudang = $this->input->post('txtkd_gudang', true);
			
			/** Get FGKD last stock */
			$qFG = $this->db->select('td_finishgood_barcode.fgbarcode_kd, td_finishgood_barcode.barang_kd, 
						tm_finishgood.fg_kd, tm_finishgood.fg_qty, td_finishgood_in.fgin_kd, td_finishgood_in.rakruangkolom_kd, td_finishgood_in.fgin_qty, tm_barang.item_group_kd')
					->from('td_finishgood_barcode')
					->join('tm_finishgood', 'td_finishgood_barcode.barang_kd=tm_finishgood.barang_kd', 'left')
					->join('td_finishgood_in', 'td_finishgood_barcode.fgbarcode_kd=td_finishgood_in.fgbarcode_kd', 'left')
					->join('tm_barang', 'tm_barang.kd_barang=td_finishgood_barcode.barang_kd', 'left')
					// ->join('td_finishgood_out', 'td_finishgood_in.fgin_kd=td_finishgood_out.fgin_kd', 'left')
					->where(['td_finishgood_barcode.fgbarcode_barcode' => $fgbarcode_barcode, 'tm_finishgood.kd_gudang' => $kd_gudang])
					// ->where('td_finishgood_out.fgin_kd IS NULL')
					->where('td_finishgood_in.flag_out', '0')
					->get();

			if (!empty($qFG->num_rows())) {
				$qFG = $qFG->row_array();
				if (!empty($qFG['rakruangkolom_kd'])){
					$barangCustom = false;
					/** CUSTOM  */
					if($qFG['item_group_kd'] == '2'){
						/** Get last data untuk ambil startdate kartu stok custom */
						$fgCustom = $this->db->select('fg_kd, kd_gudang, barang_kd, item_code, item_barcode, fg_tglinput')
								->where('item_barcode', '899000')
								->order_by('fg_tglinput', 'DESC')
								->limit(1)
								->get('tm_finishgood')
								->row_array();
						/** Jika barang custom / PJ setting qty awal ambil dari qty (sisa) kartu stock (field sisa) */
						$startdate = format_date($fgCustom['fg_tglinput'], 'Y-m-d');
						$enddate = date('Y-m-d');
						$item_code = $fgCustom['barang_kd'];
						//$kd_gudang = $fgCustom['kd_gudang'];
						$kartustok = $this->model_kartustokfg->kartu_stok_all($startdate, $enddate, $item_code, $kd_gudang);

						if (!empty($kartustok) && is_array($kartustok)) {  
							usort($kartustok, function ($a, $b) {  
								// Pastikan tanggal kedua elemen adalah objek DateTime  
								if (!$a['tgl_trans'] instanceof DateTime || !$b['tgl_trans'] instanceof DateTime) {  
									return 0; // Jika salah satu bukan DateTime, anggap sama untuk menghindari error  
								}  
								return $a['tgl_trans']->getTimestamp() - $b['tgl_trans']->getTimestamp();  
							}); 
						}
						/** Ambil sisa data terakhir */
						$fgout_qty_awal = $kartustok[count($kartustok)-1]['sisa'];
						$barangCustom = true;
					}
					/** END CUSTOM */

					$data = array(
						'fgout_kd' => $this->td_finishgood_out->buat_kode(),
						'fgin_kd' => $qFG['fgin_kd'],
						'kd_msalesorder' => $kd_msalesorder,
						'fgout_qty_awal' => $barangCustom == true ? $fgout_qty_awal : $qFG['fg_qty'],
						'fgout_qty' => $qFG['fgin_qty'],
						'fgout_tglinput' => now_with_ms(),
						'fgout_tgledit' => null,
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
					$this->db->trans_begin();
	
					$act = $this->td_finishgood_out->insert_data($data);
					/** Update tm_fg */
					$actUpdate = $this->tm_finishgood->update_stock($qFG['fg_kd'], $qFG['fgin_qty'], 'OUT');

					/** Update penanda finish good sudah out */
					$actUpdateFGIN = $this->td_finishgood_in->update_data(['fgin_kd' => $qFG['fgin_kd']], ['flag_out' => '1']);
	
					if ($this->db->trans_status() === FALSE) {
						$this->db->trans_rollback();
						$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan');
					}else {
						$this->db->trans_commit();
						$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Tersimpan');
					}
				}else{
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Lokasi Rak Kosong');
				}
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Data tidak ditemukan');
			}			
		}
		echo json_encode($resp);
	}

	public function action_delete() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$fgout_kd = $this->input->get('id', TRUE);
		if (!empty($fgout_kd)){
			/** Cek dulu apakah ini input terakhir */
			$cekMaxTrans = false;
			$actBarcodeFG = $this->td_finishgood_out->get_barcode_by_param(array('td_finishgood_out.fgout_kd'=>$fgout_kd))->row_array();
			$maxKD = $this->td_finishgood_in->get_kd_max_trans($actBarcodeFG['fg_kd']);
			if ($fgout_kd == $maxKD){
				$cekMaxTrans = true;
			}

			if ($cekMaxTrans){
				/** Update Tm stock sebelum dihapus */
				$fgout_qty = $actBarcodeFG['fgout_qty'];
				$actStock = $this->tm_finishgood->update_stock($actBarcodeFG['fg_kd'], $fgout_qty , 'IN');
				/** Update penanda finish good sudah out */
				$actUpdateFGIN = $this->td_finishgood_in->update_data(['fgin_kd' => $actBarcodeFG['fgin_kd']], ['flag_out' => '0']);
				if ($actStock){
					/** Jika sukses del pada fg_out_kd */
					$actDel = $this->td_finishgood_out->delete_data($fgout_kd);
					if ($actDel){
						$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terhapus');
					}else{
						$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
					}
				}else{
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Update Stock');
				}	
			} else {
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Data Bukan Input Terakhir');
			}
		}
		else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Tidak Sesuai');
		}
		echo json_encode($resp);
	}
	
}
