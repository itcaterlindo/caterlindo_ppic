<?php
defined('BASEPATH') or exit('No direct script access allowed!');

require_once APPPATH."third_party/phpspreadsheets/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Kartustok_fg extends MY_Controller {
	private $class_link = 'inventory/finish_good/kartustok_fg';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_finishgood_barcode', 'td_finishgood_out', 'tm_finishgood', 'tm_gudang', 'td_finishgood_in', 'td_finishgood_adjustment', 'td_finishgood_repacking' ,'tm_barang', 'td_rak_ruang', 'tb_finishgood_user', 'model_kartustokfg']);
		/** Cek Lokasi User */
		$userfg = $this->tb_finishgood_user->get_by_param(array('kd_tipe_admin' => $this->session->userdata('tipe_admin_kd')))->row_array();
		$this->set_gudang = $userfg['kd_gudang'];
	}

	public function index() {
		parent::administrator();
		$this->form_box();
	}

	public function form_box() {
		parent::pnotify_assets();
		parent::typeahead_assets();
		parent::datetimepicker_assets();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		/** Dropdown Warehouse */
		// $actWarehouse = $this->tm_gudang->get_all()->result_array();
		if ($this->session->userdata('tipe_admin_kd') == 'TPA190617001'){
			/** admin */
			$actWarehouse = $this->tm_gudang->get_all()->result_array();
		}else{
			$actWarehouse = $this->tm_gudang->get_by_param (array('kd_gudang'=>$this->set_gudang))->result_array();
		}

		// $opsiWarehouse[''] = '--Pilih Warehouse--';
		foreach($actWarehouse as $eachWarehouse):
			$opsiWarehouse[$eachWarehouse['kd_gudang']] = $eachWarehouse['nm_gudang']; 
		endforeach;

		/** Dropdown Jenis Transaksi */
		$opsiTransaksi = array(
			//'' => '--Pilih Transaksi--',
			'ALL' => 'Semua',
			'IN' => 'Masuk',
			'OUT' => 'Keluar',
			'ADJ' => 'Adjustment',
			'RPK' => 'Repacking',
			'DLV' => 'Delivery'
		);
		
		$data['class_link'] = $this->class_link;
		$data['opsiWarehouse'] = $opsiWarehouse;
		$data['opsiTransaksi'] = $opsiTransaksi;

		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$data['class_link'] = $this->class_link;
		$data['js_file'] = 'table_js'; 
		
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	private function kartu_stok_in($startdate, $enddate, $barang_kd, $kd_gudang){
		$data = $this->model_kartustokfg->kartu_stok_in($startdate, $enddate, $barang_kd, $kd_gudang);
		return $data;
	}

	private function kartu_stok_out($startdate, $enddate, $barang_kd, $kd_gudang){
		$data = $this->model_kartustokfg->kartu_stok_out($startdate, $enddate, $barang_kd, $kd_gudang);
		return $data;
	}

	private function kartu_stok_adjustment($startdate, $enddate, $barang_kd, $kd_gudang){
		$data = $this->model_kartustokfg->kartu_stok_adjustment($startdate, $enddate, $barang_kd, $kd_gudang);
		return $data;
	}

	private function kartu_stok_repacking($startdate, $enddate, $barang_kd, $kd_gudang){
		$data = $this->model_kartustokfg->kartu_stok_repacking($startdate, $enddate, $barang_kd, $kd_gudang);
		return $data;
	}

	private function kartu_stok_delivery($startdate, $enddate, $barang_kd, $kd_gudang){
		$data = $this->model_kartustokfg->kartu_stok_delivery($startdate, $enddate, $barang_kd, $kd_gudang);
		return $data;
	}

	private function kartu_stok_all($startdate, $enddate, $item_code, $kd_gudang){
		$data = $this->model_kartustokfg->kartu_stok_all($startdate, $enddate, $item_code, $kd_gudang);
		return $data;
	}

	public function table_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtStartdate', 'StartDate', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtEnddate', 'EndDate', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtWarehouse', 'Warehouse', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtJnsTransaksi', 'Jenis Transaksi', 'required', ['required' => '{field} tidak boleh kosong!']);
		$this->form_validation->set_rules('txtbarang_kd', 'Barang', 'required', ['required' => '{field} tidak boleh kosong!']);
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
                'idErrStartdate' => (!empty(form_error('txtStartdate')))?buildLabel('warning', form_error('txtStartdate', '"', '"')):'',
                'idErrEnddate' => (!empty(form_error('txtEnddate')))?buildLabel('warning', form_error('txtEnddate', '"', '"')):'',
                'idErrWarehouse' => (!empty(form_error('txtWarehouse')))?buildLabel('warning', form_error('txtWarehouse', '"', '"')):'',
                'idErrJnsTransaksi' => (!empty(form_error('txtJnsTransaksi')))?buildLabel('warning', form_error('txtJnsTransaksi', '"', '"')):'',  
                'idErrItemCode' => (!empty(form_error('txtbarang_kd')))?buildLabel('warning', form_error('txtbarang_kd', '"', '"')):'',  
			);
			
		}else {
			$startdate = $this->input->post('txtStartdate', true);
			$enddate = $this->input->post('txtEnddate', true);
			$warehouse = $this->input->post('txtWarehouse', true);
			$jns_transaksi = $this->input->post('txtJnsTransaksi', true);
			$barang_kd = $this->input->post('txtbarang_kd', true);
			
			$url = base_url().$this->class_link.'/table_data?startdate='.$startdate.'&enddate='.$enddate.'&jns_transaksi='.$jns_transaksi.'&warehouse='.$warehouse.'&barang_kd='.$barang_kd;
	
			$resp['code'] = 200;
			$resp['status'] = 'Sukses';
			$resp['data'] = $url;
		}

		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}


	public function table_data () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		try{

		$startdate = $this->input->get('startdate', true);
		$data['startdate'] = $startdate;
		$startdate = format_date($startdate, 'Y-m-d');
		$enddate = $this->input->get('enddate', true);
		$data['enddate'] = $enddate;
		$enddate = format_date($enddate, 'Y-m-d');
		$warehouse = $this->input->get('warehouse', true);
		$jns_transaksi = $this->input->get('jns_transaksi', true);
		$barang_kd = $this->input->get('barang_kd', true);
		
		switch ($jns_transaksi) {
			case 'ALL' :
				$actTransaksi = $this->kartu_stok_all($startdate, $enddate, $barang_kd, $warehouse);
				break;
			case 'IN' :
				$actTransaksi = $this->kartu_stok_in($startdate, $enddate, $barang_kd, $warehouse);
				break;
			case 'OUT' :
				$actTransaksi = $this->kartu_stok_out($startdate, $enddate, $barang_kd, $warehouse);
				break;
			case 'ADJ' :
				$actTransaksi = $this->kartu_stok_adjustment($startdate, $enddate, $barang_kd, $warehouse);
				break;
			case 'RPK' :
				$actTransaksi = $this->kartu_stok_repacking($startdate, $enddate, $barang_kd, $warehouse);
				break;
			case 'DLV' :
				$actTransaksi = $this->kartu_stok_delivery($startdate, $enddate, $barang_kd, $warehouse);
				break;
			default :
				$actTransaksi = array('PILIH JNS TRANSAKSI');
		}
		/** get identitas barang */
		$actBarang = $this->tm_barang->get_item(array('kd_barang'=> $barang_kd));
		$data['barang'] = $actBarang;
		
		/** get identitas judul */
		$actGudang = $this->tm_gudang->get_by_param (array('kd_gudang'=>$warehouse))->row_array();
		$data['nm_gudang'] = $actGudang['nm_gudang'];

		$data['class_link'] = $this->class_link;
		$data['rowData'] = $actTransaksi;
		$resp['data'] = $this->load->view('page/'.$this->class_link.'/table_main', $data);
		}catch(\Throwable $e){
			echo $e->getMessage();
		}
	}

	public function cetak_kartu_stok () {
		parent::admin_print();
		$startdate = $this->input->get('startdate', true);
		$startdate = format_date($startdate, 'Y-m-d');
		$enddate = $this->input->get('enddate', true);
		$enddate = format_date($enddate, 'Y-m-d');
		$warehouse = $this->input->get('warehouse', true);
		$jns_transaksi = $this->input->get('jns_transaksi', true);
		$barang_kd = $this->input->get('barang_kd', true);

		$url = base_url().$this->class_link.'/table_data?startdate='.$startdate.'&enddate='.$enddate.'&jns_transaksi='.$jns_transaksi.'&warehouse='.$warehouse.'&barang_kd='.$barang_kd;
		
		$data['class_link'] = $this->class_link;
		$data['url'] = $url;
		$this->load->view('page/'.$this->class_link.'/print_detail', $data);
		
	}

	public function xls_kartu_stok(){
		$startdate = $this->input->get('startdate', true);
		$startdate = format_date($startdate, 'Y-m-d');
		$enddate = $this->input->get('enddate', true);
		$enddate = format_date($enddate, 'Y-m-d');
		$warehouse = $this->input->get('warehouse', true);
		$jns_transaksi = $this->input->get('jns_transaksi', true);
		$barang_kd = $this->input->get('barang_kd', true);
		
		switch ($jns_transaksi) {
			case 'ALL' :
				$actTransaksi = $this->kartu_stok_all($startdate, $enddate, $barang_kd, $warehouse);
				break;
			case 'IN' :
				$actTransaksi = $this->kartu_stok_in($startdate, $enddate, $barang_kd, $warehouse);
				break;
			case 'OUT' :
				$actTransaksi = $this->kartu_stok_out($startdate, $enddate, $barang_kd, $warehouse);
				break;
			case 'ADJ' :
				$actTransaksi = $this->kartu_stok_adjustment($startdate, $enddate, $barang_kd, $warehouse);
				break;
			case 'RPK' :
				$actTransaksi = $this->kartu_stok_repacking($startdate, $enddate, $barang_kd, $warehouse);
				break;
		}

		$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()
			->setCreator($_SESSION['nm_admin'])
			->setLastModifiedBy($_SESSION['nm_admin'])
			->setTitle('Report Stock '.date('d-M-Y'))
			->setCategory('Report');
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
		$spreadsheet->getActiveSheet()->getPageSetup()
			->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		$sheet = $spreadsheet->getActiveSheet();
		$header_cell = 1;
		$sheet->setCellValue('A'.$header_cell, 'No')->getStyle('A'.$header_cell);
		$sheet->setCellValue('B'.$header_cell, 'Tgl Transaksi')->getStyle('B'.$header_cell);
		$sheet->setCellValue('C'.$header_cell, 'Product Code')->getStyle('C'.$header_cell);
		$sheet->setCellValue('D'.$header_cell, 'Uraian')->getStyle('D'.$header_cell);
		$sheet->setCellValue('E'.$header_cell, 'Barcode')->getStyle('E'.$header_cell);
		$sheet->setCellValue('F'.$header_cell, 'Awal')->getStyle('F'.$header_cell);
		$sheet->setCellValue('G'.$header_cell, 'Masuk')->getStyle('G'.$header_cell);
		$sheet->setCellValue('H'.$header_cell, 'Keluar')->getStyle('H'.$header_cell);
		$sheet->setCellValue('I'.$header_cell, 'Sisa')->getStyle('H'.$header_cell);

		$dataCell = 2;
		$no = 1;

		foreach ($actTransaksi as $each):
			$sheet->setCellValue('A'.$dataCell, $no)->getStyle('A'.$dataCell);
			$sheet->setCellValue('B'.$dataCell, format_date($each['tgl_trans'], 'd-m-Y'))->getStyle('B'.$dataCell);
			$sheet->setCellValue('C'.$dataCell, $each['item_code'])->getStyle('C'.$dataCell);
			$sheet->setCellValue('D'.$dataCell, $each['uraian'])->getStyle('D'.$dataCell);
			$sheet->setCellValue('E'.$dataCell, $each['barcode'])->getStyle('E'.$dataCell);
			$sheet->setCellValue('F'.$dataCell, $each['awal'])->getStyle('F'.$dataCell);
			$sheet->setCellValue('G'.$dataCell, $each['masuk'])->getStyle('G'.$dataCell);
			$sheet->setCellValue('H'.$dataCell, $each['keluar'])->getStyle('H'.$dataCell);
			$sheet->setCellValue('I'.$dataCell, $each['sisa'])->getStyle('H'.$dataCell);

			$dataCell++;
			$no++;
		endforeach;


		$filename = 'kartu_stock_'.$jns_transaksi.'_'.$barang_kd.'_'.$startdate.'_sd_'.$enddate.'.xlsx';
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='. $filename .'');
		header('Cache-Control: max-age=0');
		$writer->save('php://output');
	}
}
