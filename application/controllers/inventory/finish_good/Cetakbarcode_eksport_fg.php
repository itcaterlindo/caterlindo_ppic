<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Cetakbarcode_eksport_fg extends MY_Controller {
	private $class_link = 'inventory/finish_good/cetakbarcode_eksport_fg';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['td_salesorder_item', 'td_salesorder_item_detail', 'tm_salesorder']);
	}

	public function index() {
		parent::administrator();
		parent::select2_assets();
		parent::pnotify_assets();
		$this->form_box();
	}

	public function form_box() {
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$salesorders = $this->tm_salesorder->get_by_param_master (['tm_salesorder.tipe_customer' => 'Ekspor'])->result();
		$opsiSO[''] = '-- Pilih Opsi --';
		foreach ($salesorders as $salesorder) {
			if ($salesorder->status_so == 'finish' || $salesorder->status_so == 'cancel') {
				continue;
			}
			$opsiSO[$salesorder->kd_msalesorder] = $salesorder->no_po.' | '.$salesorder->nm_customer; 
		}

		$data['opsiSO'] = $opsiSO;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function detail_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$kd_msalesorder = $this->input->get('kd_msalesorder', true);
		$data['rowMaster'] = $this->tm_salesorder->get_by_param_master(['tm_salesorder.kd_msalesorder' => $kd_msalesorder])->row();
		$data['url'] = base_url().$this->class_link.'/cetak_barcode_exp/'.$kd_msalesorder;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/detail_box', $data);
	}

	private function group_by_item($arrItems) {
		$groups = array();
		foreach ($arrItems as $item) {
			$key = $item['kd_barang'];
			if (!array_key_exists($key, $groups)) {
				$groups[$key] = array(
					'kd_barang' => $item->kd_barang,
					'item_code' => $item->item_code,
					'deskripsi_barang' => $item->deskripsi_barang,
					'dimensi_barang' => $item->dimensi_barang,
					'bcdexp_barcode' => $item->bcdexp_barcode,
					'item_qty' => $item->item_qty,
				);
			} else {
				$groups[$key]['item_qty'] = $groups[$key]['item_qty'] + $item['item_qty'];
			}
		}
		
		foreach ($groups as $each){
			$groupResult[] = $each;
		}
		return $groupResult;
	}
	
	public function cetak_barcode_exp() {
        $this->load->library('Pdf');
		$kd_msalesorder = $this->uri->segment(5);
		if(!empty($kd_msalesorder)) {
			$items = $this->td_salesorder_item->get_by_param_barcode_eksport(['td_salesorder_item.msalesorder_kd' => $kd_msalesorder])->result();
			$detailItems = $this->td_salesorder_item_detail->get_by_param_barcode_eksport(['td_salesorder_item_detail.msalesorder_kd' => $kd_msalesorder]);

			$arrItem = [];
			foreach ($items as $item) {
				if ($item->item_status == 'std') {
					$arrItem[] = [
						'kd_barang' => $item->kd_barang,
						'item_code' => $item->item_code,
						'deskripsi_barang' => $item->deskripsi_barang,
						'dimensi_barang' => $item->dimensi_barang,
						'bcdexp_barcode' => $item->bcdexp_barcode,
						'item_qty' => $item->item_qty,
					];
				}
			}
			if ($detailItems->num_rows() != 0) {
				foreach ($detailItems as $detailItem) {
					if ($detailItem->item_status == 'std') {
						$arrItem[] = [
							'kd_barang' => $detailItem->kd_barang,
							'item_code' => $detailItem->item_code,
							'deskripsi_barang' => $detailItem->deskripsi_barang,
							'dimensi_barang' => $detailItem->dimensi_barang,
							'bcdexp_barcode' => $item->bcdexp_barcode,
							'item_qty' => $detailItem->item_qty,
						];
					}
				}
				$arrItem = $this->group_by_item($arrItem);
			}
			
			$data['resultItem'] = $arrItem;
            $data['no_po'] = 'EAN';
            $data['class_link'] = $this->class_link;
			$this->load->view('page/'.$this->class_link.'/barcode_exp_pdf', $data);
		}
	}

}
