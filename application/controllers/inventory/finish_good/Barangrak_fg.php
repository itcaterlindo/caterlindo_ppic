<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Barangrak_fg extends MY_Controller {
	private $class_link = 'inventory/finish_good/barangrak_fg';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_gudang', 'tm_rak', 'td_rak_ruang', 'td_finishgood_in', 'tb_finishgood_user', 'td_rak_ruang_kolom']);
		/** Cek Lokasi User */
		$userfg = $this->tb_finishgood_user->get_by_param (array('kd_tipe_admin' => $this->session->userdata('tipe_admin_kd')))->row_array();
		$this->set_gudang = $userfg['kd_gudang'];
	}

	public function index() {
		parent::administrator();
		$this->form_box();
	}

	public function form_box() {
		parent::pnotify_assets();
		parent::ionicons_assets();
		parent::typeahead_assets();

		$actGudang = $this->tm_gudang->get_row($this->set_gudang);
		$data['nm_gudang'] = $actGudang->nm_gudang;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
	}

	public function form_main() {
		$data['class_link'] = $this->class_link;
		$data['opsi'] = array(
			'0' => '--Pilih Jenis Filter--',
			'1' => 'Semua Barang',
			'2' => 'Blm Input Penyimpanan',
			'3' => 'Barcode',
			'4' => 'Product Code',
			'5' => 'Lokasi Penyimpanan',	
		);
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}

	public function table_box(){
		$data['class_link'] = $this->class_link;
		$data['js_file'] = 'table_js'; 
		$this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

	public function form_lokasi_box() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['id'] = $this->input->get('id');
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_lokasi_box', $data);
	}

	public function form_lokasi_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', TRUE);
		
		$row = $this->td_finishgood_in->get_lokasi_by_id($id)->row_array();
		$data['rowData'] = $row;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_lokasi_main', $data);
	}

	public function table_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$jnsFilter =  $this->input->get('jnsFilter', TRUE);
		$gudang = $this->input->get('gudang', TRUE);
		$rak = $this->input->get('rak', TRUE);
		$ruangKolom = $this->input->get('ruangKolom', TRUE);
		$barcode = $this->input->get('barcode', TRUE);
		$barang_kd = $this->input->get('barang_kd', TRUE);

		$data['class_link'] = $this->class_link;
		$data['url'] = base_url().$this->class_link.'/table_data?'.'jnsFilter='.$jnsFilter.'&gudang='.$gudang.'&rak='.$rak.'&ruangKolom='.$ruangKolom.'&barcode='.$barcode.'&barang_kd='.$barang_kd;
		$this->load->view('page/'.$this->class_link.'/table_main', $data);
	}

	public function table_data() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$this->load->library(['ssp']);

		$jnsFilter =  $this->input->get('jnsFilter', TRUE);
		$gudang = $this->input->get('gudang', TRUE);
		$rak = $this->input->get('rak', TRUE);
		$ruangKolom = $this->input->get('ruangKolom', TRUE);
		$barcode = $this->input->get('barcode', TRUE);
		$barang_kd = $this->input->get('barang_kd', TRUE);

		$detilVal = array(
			'gudang' => !empty($gudang) ? $gudang: null,
			'rak' => !empty($rak) ? $rak: null,
			'ruangKolom' => !empty($ruangKolom) ? $ruangKolom: null,
			'barcode' => !empty($barcode) ? $barcode: null,
			'barang_kd' => !empty($barang_kd) ? $barang_kd: null,
		);
		
		$data = $this->td_finishgood_in->ssp_table_barang_rak($jnsFilter, $detilVal);
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
	}

	public function grid_lokasi_box(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/grid_lokasi_box', $data);
	}

	public function grid_lokasi_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/grid_lokasi_main', $data);
	}

	public function grid_lokasi_table(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$actFGin = $this->td_finishgood_in->get_fgin_blm_out($this->set_gudang)->result_array();

		$data['resultData'] = $actFGin;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/grid_lokasi_table', $data);
	}

	public function get_gudang(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		
		if ($this->session->userdata('tipe_admin_kd') == 'TPA190617001'){
			/** admin */
			$act = $this->tm_gudang->get_all();
		}else{
			$act = $this->tm_gudang->get_by_param (array('kd_gudang'=>$this->set_gudang));
		}
		
		if($act->num_rows() != 0){
			$resp['code'] = 200;
			$resp['status'] = 'Sukses';
			$resp['pesan'] = '';
			$resp['data'] = $act->result_array();
		}else{
			$resp['code'] = 400;
			$resp['status'] = 'Gagal';
			$resp['pesan'] = 'Data Tidak ditemukan';
		}
	
		echo json_encode($resp);
	}

	public function get_rak(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$gudang_kd = $this->input->get('gudang_kd', true);
		if(!empty($gudang_kd)){
			$act = $this->tm_rak->get_byParam(array('gudang_kd'=>$gudang_kd));
			if($act->num_rows() != 0){
				$resp['code'] = 200;
				$resp['status'] = 'Sukses';
				$resp['pesan'] = '';
				$resp['data'] = $act->result_array();
			}else{
				$resp['code'] = 400;
				$resp['status'] = 'Gagal';
				$resp['pesan'] = 'Data Tidak ditemukan';
			}
		}else{
			$resp['code'] = 400;
			$resp['status'] = 'Gagal';
			$resp['pesan'] = 'Input tidak sesuai';
		}

		echo json_encode($resp);
	}

	public function get_ruang(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$rak_kd = $this->input->get('rak_kd', true);
		if(!empty($rak_kd)){
			$act = $this->td_rak_ruang->get_byParam(array('rak_kd'=>$rak_kd));
			if($act->num_rows() != 0){
				$resp['code'] = 200;
				$resp['status'] = 'Sukses';
				$resp['pesan'] = '';
				$resp['data'] = $act->result_array();
			}else{
				$resp['code'] = 400;
				$resp['status'] = 'Gagal';
				$resp['pesan'] = 'Data Tidak ditemukan';
			}
		}else{
			$resp['code'] = 400;
			$resp['status'] = 'Gagal';
			$resp['pesan'] = 'Input tidak sesuai';
		}

		echo json_encode($resp);
	}

	public function get_ruang_kolom(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$kd_rak_ruang = $this->input->get('kd_rak_ruang', true);
		if(!empty($kd_rak_ruang)){
			$act = $this->td_rak_ruang_kolom->get_byParam(array('kd_rak_ruang'=>$kd_rak_ruang));
			if($act->num_rows() != 0){
				$resp['code'] = 200;
				$resp['status'] = 'Sukses';
				$resp['pesan'] = '';
				$resp['data'] = $act->result_array();
			}else{
				$resp['code'] = 400;
				$resp['status'] = 'Gagal';
				$resp['pesan'] = 'Data Tidak ditemukan';
			}
		}else{
			$resp['code'] = 400;
			$resp['status'] = 'Gagal';
			$resp['pesan'] = 'Input tidak sesuai';
		}

		echo json_encode($resp);
	}

	public function action_update_lokasirak() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtRuangKolom', 'Ruang Kolom', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrRuangKolom' => (!empty(form_error('txtRuangKolom')))?buildLabel('warning', form_error('txtRuangKolom', '"', '"')):''
			);
			
		}else {

			$fgin_kd = $this->input->post('txtfgin_kd', true);
			$rakruangkolom_kd = $this->input->post('txtRuangKolom');
			
			$data = array(
				'rakruangkolom_kd' => !empty($rakruangkolom_kd) ? $rakruangkolom_kd:null,
				'fgin_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			);

			$act = $this->td_finishgood_in->update_data (array('fgin_kd'=> $fgin_kd), $data);
			if ($act){
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terupdate');
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_update_lokasirak_batch(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtRuangKolomGrid', 'Ruang Kolom', 'required', ['required' => '{field} tidak boleh kosong!']);

		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrRuangKolomGrid' => (!empty(form_error('txtRuangKolomGrid')))?buildLabel('warning', form_error('txtRuangKolomGrid', '"', '"')):''
			);
			
		}else {

			$fgin_kd = $this->input->post('txtFginkd', true);
			$rakruangkolom_kd = $this->input->post('txtRuangKolomGrid');
			
			if (!empty($fgin_kd)){
				foreach ($fgin_kd as $eachFgin){
					$data[] = array(
						'fgin_kd' => $eachFgin,
						'rakruangkolom_kd' => !empty($rakruangkolom_kd) ? $rakruangkolom_kd:null,
						'fgin_tgledit' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					);
				}
				$act = $this->td_finishgood_in->update_data_batch('fgin_kd', $data);
				if ($act){
					$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Data Terupdate');
				}else{
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Tersimpan');
				}
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Pilih Data yg diupdate');
			}

		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	function temp(){
		$a = array(1,2,3,4,5);
		$b = count($a);
		// foreach ($a as $b){
		// 	echo $b;
		// }
		echo $b;
	}

	public function cetak_barcode() {
		$fgin_kd = $this->input->get('fgin_kd');
		$fgbarcode_kd = $this->td_finishgood_in->get_barcode_by_param(array('fgin_kd'=> $fgin_kd))->row_array();
		$url = base_url().'inventory/finish_good/cetakbarcode_fg/cetak_barcode/'.$fgbarcode_kd['fgbarcode_kd'];
		$resp['code'] = 200;
		$resp['url'] = $url;

		echo json_encode($resp);
	}
		
}
