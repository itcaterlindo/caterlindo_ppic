<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class master_payment extends MY_Controller {
	private $class_link = 'finance/finance_manage/master_payment';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tb_pph', 'tm_m_payment']);
	}

	public function index() {
        parent::administrator();
		parent::pnotify_assets();
        $data['class_link'] = $this->class_link;

        $this->load->view('page/'.$this->class_link.'/table_box', $data);
	}
    
    public function table_main(){
    	if (!$this->input->is_ajax_request()){
    		exit('No direct script access allowed');
    	}
    	$data['class_link'] = $this->class_link;

    	$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }

    public function table_data (){
    	// if (!$this->input->is_ajax_request()){
    	// 	exit('No direct script access allowed');
    	// }
    	$this->load->library(['ssp']);

		$data = $this->tm_m_payment->ssp_table();
		echo json_encode(
			SSP::simple( $_GET, $data['sql_details'], $data['table'], $data['primaryKey'], $data['columns'], $data['joinQuery'], $data['where'] )
		);
    }

	
	function form_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$pph_kd = $this->input->get('id', true);
		$sts = $this->input->get('sts', true);

		if (!empty($pph_kd)) {
			$data['rowData'] = $this->tb_pph->get_by_param (['pph_kd' => $pph_kd])->row_array();
		}

		$data['sts'] = $sts;
		$data['id'] = $pph_kd;
		$this->load->view('page/'.$this->class_link.'/form_main', $data);
	}
    
    public function action_insert () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpph_nama', 'PPh', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtpph_persen', 'Persen', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpph_nama' => (!empty(form_error('txtpph_nama')))?buildLabel('warning', form_error('txtpph_nama', '"', '"')):'',
				'idErrpph_persen' => (!empty(form_error('txtpph_persen')))?buildLabel('warning', form_error('txtpph_persen', '"', '"')):'',
			);
			
		}else {
			$pph_kd = $this->input->post('txtpph_kd', true);
			$pph_nama = $this->input->post('txtpph_nama', true);
			$pph_persen = $this->input->post('txtpph_persen', true);			

            $data = [
                'nm_bank' => $pph_nama,
                'no_rek' => $pph_persen,
			];

            $act = false;
			if (empty($pph_kd)) {
                $act = $this->tm_m_payment->insert_data($data);
            }else {
                $data = array_merge($data, ['pph_tgledit' => date('Y-m-d H:i:s')]);
                $act = $this->tm_m_payment->update_data (['pph_kd' => $pph_kd], $data);
            }

			if ($act) {
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['pph_kd' => $pph_kd]);
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_hapus () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$pph_kd = $this->input->get('id', TRUE);

		$act = $this->tm_m_payment->delete_data ($pph_kd);
		if ($act){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}

	
}
