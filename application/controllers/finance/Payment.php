<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Payment extends MY_Controller {
	private $class_link = 'finance/payment';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_purchaseinvoice', 'tm_suplier', 'td_purchaseinvoice_detail', 'td_rawmaterial_goodsreceive', 'tb_pph', 'tm_payment', 'td_payment', 'tm_m_payment']);
	}

	public function index() {
        parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
        parent::datetimepicker_assets();
        $this->load->js('assets/admin_assets/plugins/input-mask/jquery.inputmask.js');
        $data['class_link'] = $this->class_link;
        $this->load->view('page/'.$this->class_link.'/table_box', $data);
	}
    
    public function table_main(){
    	// if (!$this->input->is_ajax_request()){
    	// 	exit('No direct script access allowed');
    	// }
		$bulan = $this->input->get('bulan', true);
        $tahun = $this->input->get('tahun', true);

    	$data['class_link'] = $this->class_link;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;

		// echo json_encode($data);

    	$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }

	public function aku_nyobak()
	{
		$items = '';
		$data = $this->tm_payment->get_invoice_no();

		foreach ($data as $r) {
			$items .= $r['purchaseinvoice_no'] . '; ';
			
		}


		header('Content-Type: application/json');
		echo json_encode($items);

		
	}

    public function table_data (){
    	// if (!$this->input->is_ajax_request()){
    	// 	exit('No direct script access allowed');
    	// }
			
		$bulan = $this->input->get('bulan', true);
        $tahun = $this->input->get('tahun', true);

    	$this->load->library(['ssp']);

		$data = $this->tm_payment->ssp_table2($bulan, $tahun);
		
		echo json_encode($data);
    }

	public function form_box () {
        if (!$this->input->is_ajax_request()){
    		exit('No direct script access allowed');
        }
        $id = $this->input->get('id', true);
        $sts = $this->input->get('sts', true);

        $data['class_link'] = $this->class_link;
        $data['id'] = $id;
        $data['sts'] = $sts;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
    }
    
    public function form_master_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        $id = $this->input->get('id', true);
        $sts = $this->input->get('sts', true);
        
        if ($sts == 'edit') {
			$data['rowData'] = $this->db->select('tm_payment.*, tm_m_payment.*, tm_suplier.suplier_kode, tm_suplier.suplier_nama')
					->from('tm_payment')
					->join('tm_suplier', 'tm_payment.suplier_kd=tm_suplier.suplier_kd', 'left')
					->join('tm_m_payment', 'tm_payment.m_payment_kd=tm_m_payment.m_payment_kd', 'left')
					->where('tm_payment.payment_kd', $id)
					->get()->row_array();
		}     
        
        $data['id'] = $id;
        $data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_master_main', $data);
    }

    public function formdetail_box() {
        parent::administrator();
		parent::pnotify_assets();
        parent::select2_assets();
        parent::icheck_assets();
        $id = $this->input->get('id', true);

		$dataku = $this->db->query('SELECT * FROM tm_payment tp 
								  INNER JOIN tm_suplier tms ON tp.suplier_kd = tms.suplier_kd 
								  INNER JOIN tm_m_payment tmp ON tp.m_payment_kd = tmp.m_payment_kd WHERE tp.no_check = "'. $id .'"');
		$data['my_items'] = $dataku->result();
        $data['id'] = $id;
        $data['class_link'] = $this->class_link;

		// header('Content-Type: application/json');

		// echo json_encode($data['my_items'][0]->created_payment );
        $this->load->view('page/'.$this->class_link.'/formdetail_box', $data);
    }
    
    function formdetail_main () {
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
        $id = $this->input->get('id', true);
		$sts = $this->input->get('sts', true);
		$sup_kd = $this->input->get('sup_kd', true);
		
		 /** PPh */
		 $actPph = $this->tb_pph->get_all()->result_array();
		 $opsiPph[''] = '-- Pilih Opsi --';
		 foreach ($actPph as $each):
			 $opsiPph[$each['pph_kd']] = $each['pph_nama'];
		 endforeach;

		$data['id'] = $id;
		$data['suplier_kd'] = $sup_kd;
		$data['sts'] = $sts;
		$data['opsiPph'] = $opsiPph;
        $this->load->view('page/'.$this->class_link.'/formdetail_main', $data);
	}
	
    function formdetail_edit () {
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
		}
		
        $purchaseinvoicedetail_kd = $this->input->get('id', true);
		$sts = $this->input->get('sts', true);

		$data['row'] = $this->db->select('td_purchaseinvoice_detail.*, tm_purchaseorder.po_no, td_rawmaterial_goodsreceive.rmgr_nosrj, tm_rawmaterial.rm_kode, td_rawmaterial_satuan.rmsatuan_nama')
				->from('td_purchaseinvoice_detail')
				->join('td_purchaseorder_detail', 'td_purchaseinvoice_detail.podetail_kd=td_purchaseorder_detail.podetail_kd', 'left')
				->join('tm_purchaseorder', 'td_purchaseorder_detail.po_kd=tm_purchaseorder.po_kd', 'left')
				->join('td_rawmaterial_goodsreceive', 'td_purchaseinvoice_detail.rmgr_kd=td_rawmaterial_goodsreceive.rmgr_kd', 'left')
				->join('tm_rawmaterial', 'td_purchaseinvoice_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
				->join('td_rawmaterial_satuan', 'td_purchaseinvoice_detail.purchaseinvoicedetail_satuankd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
				->where('td_purchaseinvoice_detail.purchaseinvoicedetail_kd', $purchaseinvoicedetail_kd)
				->get()->row_array();
		
		 /** PPh */
		 $actPph = $this->tb_pph->get_all()->result_array();
		 $opsiPph[''] = '-- Pilih Opsi --';
		 foreach ($actPph as $each):
			 $opsiPph[$each['pph_kd']] = $each['pph_nama'];
		 endforeach;

		$data['id'] = $purchaseinvoicedetail_kd;
		$data['sts'] = $sts;
		$data['opsiPph'] = $opsiPph;
        $this->load->view('page/'.$this->class_link.'/formdetail_edit', $data);
	}
	
	function ppnform_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$purchaseinvoice_kd = $this->input->get('id', true);
		$sts = $this->input->get('sts', true);

		$data['master'] = $this->tm_purchaseinvoice->get_by_param (['purchaseinvoice_kd' => $purchaseinvoice_kd])->row_array();
		$data['id'] = $purchaseinvoice_kd;
		$this->load->view('page/'.$this->class_link.'/ppnform_main', $data);
	}

    public function table_rmgrdetail () {
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }
		$total = 0;
		$total_min_pph = 0;

		$invoice = $this->input->get('invoices', true);
		$in_detail_pm = $this->db->select('td_payment.*, SUM(td_payment.grand_total) AS grd_total_payment_detail')
				->from('td_payment')
				->where('invoice_kd', $invoice)
				->group_by('invoice_kd')
				->get()->result_array();
		

		$qData = $this->db->select('tm_purchaseinvoice.*,tm_suplier.suplier_nama,tm_suplier.suplier_kode')
		->from('tm_purchaseinvoice')
		->join('tm_suplier', 'tm_purchaseinvoice.suplier_kd = tm_suplier.suplier_kd')
		->where(array('tm_purchaseinvoice.purchaseinvoice_kd' => $invoice))
		->get()->result_array();

		$pphNominal = $this->db->select('purchaseinvoicedetail_pphnominal')
					->from('td_purchaseinvoice_detail')
					->where(array('purchaseinvoice_kd' => $invoice))
					->get()->result_array();

		

		for ($i=0; $i < count($pphNominal); $i++) { 
			if($pphNominal[$i]['purchaseinvoicedetail_pphnominal'] < 0){
				$total += $pphNominal[$i]['purchaseinvoicedetail_pphnominal'];
			}
		}

		if($total < 0){
			$total_min_pph = $qData[0]['purchaseinvoice_grandtotal'] + $total;
		}else{
			$total_min_pph = $qData[0]['purchaseinvoice_grandtotal'];
		}	


		$data['total_min_ph'] = $total_min_pph ;
		$data['nominal_pph'] = $total;
		$data['param_invoice_kd'] = $invoice;
		$data['qData'] = $qData;
		$data['in_detail_pm'] = $in_detail_pm;
		$data['class_link'] = $this->class_link;

		if($in_detail_pm){
			$data['totalnya'] = $total_min_pph - $in_detail_pm[0]['grd_total_payment_detail'];
		}else{
			$data['totalnya'] = 0;
		}
		// header('Content-Type: application/json');

		// echo json_encode($data);
		// // echo json_encode($in_detail_pm[0]['grd_total_payment_detail']);
		// // echo json_encode($qData[0]['purchaseinvoice_grandtotal']);
		$this->load->view('page/'.$this->class_link.'/table_rmgrdetail', $data);
	}
	
	public function tabledetail_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$purchaseinvoice_kd = $this->input->get('id', true);

		$data['result'] = $this->db->select('tmp.*,tpi.purchaseinvoice_tgledit,tpi.purchaseinvoice_no,tpi.purchaseinvoice_note,tpi.purchaseinvoice_termpayment,tdp.grand_total AS detail_grand_total,tdp.detpayment_kd,tpi.purchaseinvoice_tanggal,tpi.purchaseinvoice_tanggaltermpayment,tpi.purchaseinvoice_faktur,tpi.purchaseinvoice_grandtotal AS gt_invoice,tms.suplier_nama,tms.suplier_kode')
								->from('tm_payment tmp')
								->join('td_payment tdp', 'tdp.payment_kd = tmp.payment_kd')
								->join('tm_purchaseinvoice tpi', 'tpi.purchaseinvoice_kd = tdp.invoice_kd')
								->join('tm_suplier tms', 'tpi.suplier_kd = tms.suplier_kd')
						->where(array('tmp.no_check' => $purchaseinvoice_kd))
						->get()->result_array();

		$data['grand_totalnya'] = $this->db->select_sum('`tdp`.`grand_total`', 'Totalnya')
			->from('`tm_payment` `tmp`')
			->join('`td_payment` `tdp`', '`tdp`.`payment_kd` = `tmp`.`payment_kd`')
			->join('`tm_purchaseinvoice` `tpi`', '`tpi`.`purchaseinvoice_kd` = `tdp`.`invoice_kd`')
			->join('`tm_suplier` `tms`', '`tpi`.`suplier_kd` = `tms`.`suplier_kd`')
			->where(array('`tmp`.`no_check`' => $purchaseinvoice_kd))
			->group_by('tmp.payment_kd')
		->get()->result_array();

		$data['class_link'] = $this->class_link;
		$data['id'] = $purchaseinvoice_kd;
		
		$this->load->view('page/'.$this->class_link.'/tabledetail_main', $data);
	}

	public function view_box() {
        parent::administrator();
		parent::pnotify_assets();
        $id = $this->input->get('id', true);
        $id = $this->input->get('id', true);
		
		$data['master'] = $this->db->select('tmp.*, tmmp.nm_bank, tpi.purchaseinvoice_no,tpi.purchaseinvoice_note,tpi.purchaseinvoice_termpayment,tdp.grand_total AS detail_grand_total,tdp.detpayment_kd,tpi.purchaseinvoice_tanggal,tpi.purchaseinvoice_tanggaltermpayment,tpi.purchaseinvoice_faktur,tpi.purchaseinvoice_grandtotal AS gt_invoice,tms.suplier_nama,tms.suplier_kode')
										->from('tm_payment tmp')
										->join('td_payment tdp', 'tdp.payment_kd = tmp.payment_kd')
										->join('tm_purchaseinvoice tpi', 'tpi.purchaseinvoice_kd = tdp.invoice_kd')
										->join('tm_suplier tms', 'tpi.suplier_kd = tms.suplier_kd')
										->join('tm_m_payment tmmp', 'tmmp.m_payment_kd = tmp.m_payment_kd')
								->where(array('tmp.payment_kd' => $id))
								->get()->row_array();
        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/'.$this->class_link.'/view_box', $data);
	}
	
	public function view_table () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', true);
		
		$data['master'] = $this->tm_purchaseinvoice->get_by_param (['purchaseinvoice_kd' => $id])->row_array();
		$data['result'] = $this->db->select('tmp.*,tpi.purchaseinvoice_no,tpi.purchaseinvoice_note,tpi.purchaseinvoice_termpayment,tdp.grand_total AS detail_grand_total,tdp.detpayment_kd,tpi.purchaseinvoice_tanggal,tpi.purchaseinvoice_tanggaltermpayment,tpi.purchaseinvoice_faktur,tpi.purchaseinvoice_grandtotal AS gt_invoice,tms.suplier_nama,tms.suplier_kode')
								->from('tm_payment tmp')
								->join('td_payment tdp', 'tdp.payment_kd = tmp.payment_kd')
								->join('tm_purchaseinvoice tpi', 'tpi.purchaseinvoice_kd = tdp.invoice_kd')
								->join('tm_suplier tms', 'tpi.suplier_kd = tms.suplier_kd')
						->where(array('tmp.payment_kd' => $id))
						->get()->result_array();

		$data['grand_totalnya'] = $this->db->select_sum('`tdp`.`grand_total`', 'Totalnya')
								->from('`tm_payment` `tmp`')
								->join('`td_payment` `tdp`', '`tdp`.`payment_kd` = `tmp`.`payment_kd`')
								->join('`tm_purchaseinvoice` `tpi`', '`tpi`.`purchaseinvoice_kd` = `tdp`.`invoice_kd`')
								->join('`tm_suplier` `tms`', '`tpi`.`suplier_kd` = `tms`.`suplier_kd`')
								->where(array('`tmp`.`payment_kd`' => $id))
								->group_by('tmp.payment_kd')
								->get()->result_array();

		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		
		// header('Content-Type: application/json');

		// echo json_encode($data);
		$this->load->view('page/'.$this->class_link.'/view_table', $data);
	}
	
	public function view_pph () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$purchaseinvoicedetail_kd = $this->input->get('purchaseinvoicedetail_kd', true);
		
		$data['row'] = $this->db->select()
					->from('td_purchaseinvoice_detail')
					->join('tb_pph', 'td_purchaseinvoice_detail.pph_kd=tb_pph.pph_kd', 'left')
					->where(['purchaseinvoicedetail_kd' => $purchaseinvoicedetail_kd])
					->get()->row_array();
		$data['purchaseinvoicedetail_kd'] = $purchaseinvoicedetail_kd;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/view_pph', $data);
	}
	
	public function formdetail_diskon () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$purchaseinvoice_kd = $this->input->get('id', true);

		$data['id'] = $purchaseinvoice_kd;
		$data['row'] = $this->tm_purchaseinvoice->get_by_param (['purchaseinvoice_kd' => $purchaseinvoice_kd])->row_array();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/formdetail_diskon', $data);
	}
	
	public function dpform_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$purchaseinvoice_kd = $this->input->get('id', true);

		$data['id'] = $purchaseinvoice_kd;
		$data['sts'] = 'add';
		$data['class_link'] = $this->class_link;
		
		$this->load->view('page/'.$this->class_link.'/dpform_main', $data);
	}

	public function dptable_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$purchaseinvoice_kd = $this->input->get('id', true);

		$resultInv = $this->tm_purchaseinvoice->get_by_param(['purchaseinvoice_parent' => $purchaseinvoice_kd])->result_array();
		$purchaseinvoice_kds = array_column($resultInv, 'purchaseinvoice_kd');

		$data['result'] = $this->tm_purchaseinvoice->calc_grandtotal_dp_batch($purchaseinvoice_kds);		
		$data['class_link'] = $this->class_link;
		$data['id'] = $purchaseinvoice_kd;
		$this->load->view('page/'.$this->class_link.'/dptable_main', $data);
	}

	public function getInvoice()
	{
		$result= [];
		$paramSRJ = $this->input->get('paramSRJ', true);
		$suplier_kd = $this->input->get('suplier_kd', true);

		
		$this->db->select('tm_purchaseinvoice.*, tm_suplier.suplier_nama, tm_suplier.suplier_kode')
				->from('tm_purchaseinvoice')
				->join('tm_suplier','tm_purchaseinvoice'.'.suplier_kd=tm_suplier.suplier_kd', 'left')
				->where('tm_purchaseinvoice.suplier_kd', $suplier_kd);
		if (!empty($paramSRJ)) {
			$this->db->group_start()
			->like('tm_purchaseinvoice.purchaseinvoice_no', $paramSRJ)
			->or_like('tm_purchaseinvoice.purchaseinvoice_faktur', $paramSRJ)
			->or_like('tm_purchaseinvoice.purchaseinvoice_tanggal', $paramSRJ)
			->group_end();
		}

		$query = $this->db->get();
		if (!empty($query->num_rows())) {
			$query = $query->result_array();
			foreach ($query as $r) {
				$result[] = array(
					'id' => $r['purchaseinvoice_kd'],
					'text' => $r['purchaseinvoice_no'].'|'.$r['purchaseinvoice_faktur'].' / '.$r['purchaseinvoice_tanggal']
				); 
			}
		}
		echo json_encode($result);
	}

	public function Nyoba_nggeh()
	{
		$result= [];
		$paramSRJ = $this->input->get('paramSRJ', true);
		$suplier_kd = $this->input->get('suplier_kd', true);
		$payment_kd = $this->input->get('payment_kd', true);

		// $in_detail_pm = $this->db->select('td_payment.*')
		// 		->from('td_payment')
		// 		->where('payment_kd', $payment_kd)->get()->result_array();


		
		$this->db->select('tm_purchaseinvoice.*,SUM(td_payment.grand_total) AS grd_total_payment_detail,td_payment.payment_kd,td_payment.invoice_kd,tm_suplier.suplier_nama,tm_suplier.suplier_kode')
		->from('tm_purchaseinvoice')
		->join('tm_suplier', 'tm_purchaseinvoice.suplier_kd = tm_suplier.suplier_kd', 'LEFT')
		->join('td_payment', 'td_payment.invoice_kd = tm_purchaseinvoice.purchaseinvoice_kd', 'LEFT')
		->where(array('tm_suplier.suplier_payto' => $suplier_kd))
		->group_by('tm_purchaseinvoice.purchaseinvoice_kd');

		if (!empty($paramSRJ)) {
			$this->db->group_start()
			->like('tm_purchaseinvoice.purchaseinvoice_no', $paramSRJ)
			->or_like('tm_purchaseinvoice.purchaseinvoice_faktur', $paramSRJ)
			->or_like('tm_purchaseinvoice.purchaseinvoice_tanggal', $paramSRJ)
			->group_end();
		}

		$query = $this->db->get()->result_array();

			foreach ($query as $r) {
				$pph = $this->is_pph($r['purchaseinvoice_kd']);
				if($r['grd_total_payment_detail'] === null || ($r['purchaseinvoice_grandtotal'] + $pph) > $r['grd_total_payment_detail']){
					$result[] = array(
						'id' => $r['purchaseinvoice_kd'],
						'note' => $r['grd_total_payment_detail'],
						'text' => $r['purchaseinvoice_no'].'|'.$r['purchaseinvoice_faktur'].' / '.$r['suplier_nama'].' / '.$r['purchaseinvoice_tanggal']
					); 
				}
			}

		// if (!empty($query->num_rows())) {
		// 	$query = $query->result_array();
		// 	foreach ($query as $r) {
		// 		$result[] = array(
		// 			'id' => $r['purchaseinvoice_kd'],
		// 			'text' => $r['purchaseinvoice_no'].'|'.$r['purchaseinvoice_faktur'].' / '.$r['suplier_nama'].' / '.$r['purchaseinvoice_tanggal']
		// 		); 
		// 	}
		// }
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function is_pph($invoice_kd)
	{
		$total = 0;
		$pphNominal = $this->db->select('purchaseinvoicedetail_pphnominal')
					->from('td_purchaseinvoice_detail')
					->where(array('purchaseinvoice_kd' => $invoice_kd))
					->get()->result_array();

		

		for ($i=0; $i < count($pphNominal); $i++) { 
			if($pphNominal[$i]['purchaseinvoicedetail_pphnominal'] < 0){
				$total += $pphNominal[$i]['purchaseinvoicedetail_pphnominal'];
			}
		}

		if($total < 0){
			$total_min_pph = $total;
		}else{
			$total_min_pph = 0;
		}	

		return $total_min_pph;
	}

    public function get_grby_suratjalan() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$paramSRJ = $this->input->get('paramSRJ', true);
		$suplier_kd = $this->input->get('suplier_kd', true);
		
		$result = [];
		$this->db->select('td_rawmaterial_goodsreceive.rmgr_nosrj, tm_purchaseorder.po_no, tm_suplier.suplier_nama, td_rawmaterial_goodsreceive.po_kd')
			->from('td_rawmaterial_goodsreceive')
			->join('td_purchaseorder_detail', 'td_rawmaterial_goodsreceive.podetail_kd=td_purchaseorder_detail.podetail_kd', 'left')
			->join('tm_purchaseorder', 'td_purchaseorder_detail.po_kd=tm_purchaseorder.po_kd', 'left')
			->join('tm_suplier', 'tm_purchaseorder.suplier_kd=tm_suplier.suplier_kd', 'left')
			->where('rmgr_invoice IS NULL')
			->where('tm_purchaseorder.suplier_kd', $suplier_kd);
		if (!empty($paramSRJ)) {
			$this->db->group_start()
			->like('td_rawmaterial_goodsreceive.rmgr_nosrj', $paramSRJ)
			->or_like('tm_purchaseorder.po_no', $paramSRJ)
			->or_like('tm_suplier.suplier_nama', $paramSRJ)
			->group_end();
		}
		$this->db->group_by('td_rawmaterial_goodsreceive.rmgr_nosrj, tm_purchaseorder.po_kd')
		->order_by('tm_purchaseorder.po_no', 'asc');
		$query = $this->db->get();
		if (!empty($query->num_rows())) {
			$query = $query->result_array();
			foreach ($query as $r) {
				$result[] = array(
					'id' => $r['rmgr_nosrj'],
					'text' => $r['po_no'].'|'.$r['suplier_nama'].' / '.$r['rmgr_nosrj'],
					'po_kd' => $r['po_kd'],
				); 
			}
		}
		echo json_encode($result);
	}

	public function get_invoice_dp() {
		$purchaseinvoice_kd = $this->input->get('purchaseinvoice_kd', true);
		$paramInv = $this->input->get('paramInv', true);

		$rInv = $this->tm_purchaseinvoice->get_by_param(['purchaseinvoice_kd' => $purchaseinvoice_kd])->row_array();
		$resultInvDp = $this->db->select()
					->from('tm_purchaseinvoice')
					->join('tm_suplier', 'tm_purchaseinvoice.suplier_kd=tm_suplier.suplier_kd', 'left')
					->where('tm_purchaseinvoice.suplier_kd', $rInv['suplier_kd'])
					->where('tm_purchaseinvoice.purchaseinvoice_jenis', 'dp')
					->where('tm_purchaseinvoice.purchaseinvoice_parent IS NULL', null)
					->like('tm_purchaseinvoice.purchaseinvoice_no', $paramInv, 'match')
					->get()->result_array();
		$result = [];
		foreach ($resultInvDp as $eInvDp) {
			$result [] = [
				'id' => $eInvDp['purchaseinvoice_kd'],
				'text' => $eInvDp['purchaseinvoice_no']
			];
		}

		echo json_encode($result);
	}
	
	public function get_date_termpayment () {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$purchaseinvoice_tanggal = $this->input->get('purchaseinvoice_tanggal', true);
		$purchaseinvoice_termpayment = $this->input->get('purchaseinvoice_termpayment', true);

		$termpayment_date = date('Y-m-d');
		if (!empty($purchaseinvoice_termpayment)) {
			$time_purchaseinvoice_tanggal = strtotime($purchaseinvoice_tanggal);
			$time_termpayment_date = strtotime('+'.$purchaseinvoice_termpayment.' day', $time_purchaseinvoice_tanggal);
			$termpayment_date = date('Y-m-d', $time_termpayment_date);
		}
		$dt = [
			'purchaseinvoice_tanggal' => $purchaseinvoice_tanggal,
			'purchaseinvoice_termpayment' => $purchaseinvoice_termpayment,
			'termpayment_date' => $termpayment_date,
		];
		echo json_encode($dt);
	}
    
    public function action_insert_master () {
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtsuplier_kd', 'Suplier', 'required', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtmpayment', 'Payment', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtno_check', 'No', 'required', ['required' => '{field} tidak boleh kosong!']);				
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrsuplier_kd' => (!empty(form_error('txtsuplier_kd')))?buildLabel('warning', form_error('txtsuplier_kd', '"', '"')):'',
                'idErrmpayment' => (!empty(form_error('txtmpayment')))?buildLabel('warning', form_error('txtmpayment', '"', '"')):'',
			);
			
		}else {
			$sts = $this->input->post('txtSts', true);
			$payment_kd = $this->input->post('id_payment_kd', true);
			$suplier_kd = $this->input->post('txtsuplier_kd', true);
            $mpayment_kd = $this->input->post('txtmpayment', true);
			$no_check = $this->input->post('txtno_check', true);
			$created_payment = $this->input->post('txtcreated_payment', true);
            $data = [
                'suplier_kd' => $suplier_kd,
                'm_payment_kd' => $mpayment_kd,
                'no_check' => $no_check,
				'tanggal_input' => $created_payment
            ];

            $act = false;
			if ($sts == 'add') {
                // $purchaseinvoice_kd = $this->tm_purchaseinvoice->create_code();
                // $data = array_merge($data, ['purchaseinvoice_kd' => $purchaseinvoice_kd, 'purchaseinvoice_tglinput' => date('Y-m-d H:i:s')]);
                $act = $this->tm_payment->insert_data($data);
            }else {
					$resp = array('code'=> 400, 'status' => 'AkuTester', 'pesan' => $payment_kd);
                // $data = array_merge($data, ['purchaseinvoice_tgledit' => date('Y-m-d H:i:s')]);
                $act = $this->tm_payment->update_data(['payment_kd' => $payment_kd], $data);
            }

			if ($act) {
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['no_check' => $no_check]);
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

    public function action_insert_batch () {
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }

		// $this->load->library(['form_validation']);
		// $this->form_validation->set_rules('txtpurchaseinvoice_kdGR', 'Master', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		// if ($this->form_validation->run() == FALSE) {
		// 	$resp['code'] = 401;
		// 	$resp['status'] = 'Required';
		// 	$resp['pesan'] = array(
		// 		'idErrpurchaseinvoice_kdGR' => (!empty(form_error('txtpurchaseinvoice_kdGR')))?buildLabel('warning', form_error('txtpurchaseinvoice_kdGR', '"', '"')):'',
		// 	);
			
		// }else {
		// 	$rmgr_kds = $this->input->post('txtrmgr_kds', true);
		// 	$podetail_kds = $this->input->post('txtpodetail_kds', true);
		// 	$rm_kds = $this->input->post('txtrm_kds', true);
		// 	$podetail_namas = $this->input->post('txtpodetail_namas', true);
		// 	$podetail_deskripsis = $this->input->post('txtpodetail_deskripsis', true);
		// 	$podetail_spesifikasis = $this->input->post('txtpodetail_spesifikasis', true);
		// 	$podetail_qtys = $this->input->post('txtpodetail_qtys', true);
		// 	$purchaseinvoicedetail_hargaunits = $this->input->post('txtpurchaseinvoicedetail_hargaunits', true);
		// 	$purchaseinvoice_kd = $this->input->post('txtpurchaseinvoice_kdGR', true);
		// 	$rmsatuan_kds = $this->input->post('txtrmsatuan_kds', true);
		// 	$rmgr_qtys = $this->input->post('txtrmgr_qtys', true);
			
		// 	$purchaseinvoicedetail_kd = $this->td_purchaseinvoice_detail->create_code();
		// 	if (!empty($rmgr_kds)) {
		// 		foreach ($rmgr_kds as $rmgr_kd) {
		// 			$data[] = [
		// 				'purchaseinvoicedetail_kd' => $purchaseinvoicedetail_kd,
		// 				'purchaseinvoice_kd' => $purchaseinvoice_kd,
		// 				'rmgr_kd' => $rmgr_kd,
		// 				'podetail_kd' => $podetail_kds[$rmgr_kd],
		// 				'rm_kd' => $rm_kds[$rmgr_kd],
		// 				'purchaseinvoicedetail_nama' => $podetail_namas[$rmgr_kd],
		// 				'purchaseinvoicedetail_deskripsi' => $podetail_deskripsis[$rmgr_kd],
		// 				'purchaseinvoicedetail_spesifikasi' => $podetail_spesifikasis[$rmgr_kd],
		// 				'purchaseinvoicedetail_hargaunit' => $purchaseinvoicedetail_hargaunits[$rmgr_kd],
		// 				'purchaseinvoicedetail_qty' => $rmgr_qtys[$rmgr_kd],
		// 				'purchaseinvoicedetail_satuankd' => $rmsatuan_kds[$rmgr_kd],
		// 				'purchaseinvoicedetail_tglinput' => date('Y-m-d H:i:s'),
		// 				'admin_kd' => $this->session->userdata('kd_admin'),
		// 			];
		// 			$purchaseinvoicedetail_kd++;
					
		// 			/** Data update received */
		// 			$dataGR[] = [
		// 				'rmgr_kd' => $rmgr_kd,
		// 				'rmgr_invoice' => 1,
		// 			];
		// 		}
		// 		$act = $this->td_purchaseinvoice_detail->insert_batch($data);
		// 		if ($act) {
		// 			/** Update rmgr flag invoiced */
		// 			$actInvoiced = $this->td_rawmaterial_goodsreceive->update_batch ('rmgr_kd', $dataGR);
        //         	$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['rmgr_nosrj']);
		// 		}else {
		// 			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		// 		}
		// 	}else {
		// 		$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Kosong');
		// 	}

		// }
		// $resp['csrf'] = $this->security->get_csrf_hash();
		$data = [
							'invoice_kd' => $this->input->post('txtpi_kd'),
							'payment_kd' => $this->input->post('payment_kd'),
							'grand_total' => $this->input->post('txtgrand_total'),
						];
		$act = $this->td_payment->insert_data($data);
		if ($act) {
			/** Update rmgr flag invoiced */
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => $this->input->post('txtpi_kd'));
		}else {
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		}
 

		echo json_encode($resp);
	}

    public function action_insert_detail () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrm_kd', 'Barang', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtpurchaseinvoicedetail_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtpurchaseinvoicedetail_hargaunit', 'Harga', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrrm_kd' => (!empty(form_error('txtrm_kd')))?buildLabel('warning', form_error('txtrm_kd', '"', '"')):'',
				'idErrpurchaseinvoicedetail_qty' => (!empty(form_error('txtpurchaseinvoicedetail_qty')))?buildLabel('warning', form_error('txtpurchaseinvoicedetail_qty', '"', '"')):'',
				'idErrpurchaseinvoicedetail_hargaunit' => (!empty(form_error('txtpurchaseinvoicedetail_hargaunit')))?buildLabel('warning', form_error('txtpurchaseinvoicedetail_hargaunit', '"', '"')):'',
			);
			
		}else {
			$purchaseinvoicedetail_kd = $this->input->post('txtpurchaseinvoicedetail_kd', true);
			$purchaseinvoice_kd = $this->input->post('txtpurchaseinvoice_kd', true);
			$rmgr_kd = $this->input->post('txtrmgr_kd', true);
			$podetail_kd = $this->input->post('txtpodetail_kd', true);
			$rm_kd = $this->input->post('txtrm_kd', true);
			$purchaseinvoicedetail_nama = $this->input->post('txtpurchaseinvoicedetail_nama', true);
			$purchaseinvoicedetail_deskripsi = $this->input->post('txtpurchaseinvoicedetail_deskripsi', true);
			$purchaseinvoicedetail_spesifikasi = $this->input->post('txtpurchaseinvoicedetail_spesifikasi', true);
			$purchaseinvoicedetail_hargaunit = $this->input->post('txtpurchaseinvoicedetail_hargaunit', true);
			$purchaseinvoicedetail_qty = $this->input->post('txtpurchaseinvoicedetail_qty', true);
			$purchaseinvoicedetail_satuankd = $this->input->post('txtpurchaseinvoicedetail_satuankd', true);
			$rmsatuan_kds = $this->input->post('txtrmsatuan_kds', true);
			$pph_kd = $this->input->post('txtpph_kd', true);
			$purchaseinvoicedetail_pphpotongpayment = $this->input->post('txtpurchaseinvoicedetail_pphpotongpayment', true);

			$purchaseinvoicedetail_pphnominal = null;
			$purchaseinvoicedetail_pphpersen = null;
			if (!empty($pph_kd)) {
				$pph = $this->tb_pph->get_by_param(['pph_kd' => $pph_kd])->row_array();
				if ($purchaseinvoicedetail_pphpotongpayment == '1'){
					$purchaseinvoicedetail_pphpersen = $pph['pph_persen'] * -1;
					$purchaseinvoicedetail_pphnominal = $pph['pph_persen'] / 100 * $purchaseinvoicedetail_hargaunit * $purchaseinvoicedetail_qty * -1;
				}else {
					$purchaseinvoicedetail_pphpersen = $pph['pph_persen'];
					$purchaseinvoicedetail_pphnominal = $pph['pph_persen'] / 100 * $purchaseinvoicedetail_hargaunit * $purchaseinvoicedetail_qty;
				}
			}else{
				$purchaseinvoicedetail_pphpotongpayment = null;
			}
			
			$data = [
				'purchaseinvoice_kd' => $purchaseinvoice_kd,
				'rmgr_kd' => !empty($rmgr_kd) ? $rmgr_kd : null,
				'podetail_kd' => !empty($podetail_kd) ? $podetail_kd : null,
				'rm_kd' => !empty($rm_kd) ? $rm_kd : null,
				'purchaseinvoicedetail_nama' => !empty($purchaseinvoicedetail_nama) ? $purchaseinvoicedetail_nama : null,
				'purchaseinvoicedetail_deskripsi' => !empty($purchaseinvoicedetail_deskripsi) ? $purchaseinvoicedetail_deskripsi : null,
				'purchaseinvoicedetail_spesifikasi' => !empty($purchaseinvoicedetail_spesifikasi) ? $purchaseinvoicedetail_spesifikasi : null,
				'purchaseinvoicedetail_hargaunit' => !empty($purchaseinvoicedetail_hargaunit) ? $purchaseinvoicedetail_hargaunit : null,
				'purchaseinvoicedetail_qty' => !empty($purchaseinvoicedetail_qty) ? $purchaseinvoicedetail_qty : null,
				'purchaseinvoicedetail_satuankd' => !empty($purchaseinvoicedetail_satuankd) ? $purchaseinvoicedetail_satuankd : null,
				'purchaseinvoicedetail_pphpersen' => $purchaseinvoicedetail_pphpersen,
				'purchaseinvoicedetail_pphnominal' => $purchaseinvoicedetail_pphnominal,
				'purchaseinvoicedetail_pphpotongpayment' => $purchaseinvoicedetail_pphpotongpayment,
				'pph_kd' => !empty($pph_kd) ? $pph_kd : null,
				'admin_kd' => $this->session->userdata('kd_admin'),
			];

			$act = false;
			if (empty($purchaseinvoicedetail_kd)) {
				$purchaseinvoicedetail_kd = $this->td_purchaseinvoice_detail->create_code();
				$data = array_merge($data, ['purchaseinvoicedetail_kd' => $purchaseinvoicedetail_kd, 'purchaseinvoicedetail_tglinput' => date('Y-m-d H:i:s')]);
				$act = $this->td_purchaseinvoice_detail->insert_data($data);
			}else {
				$data = array_merge($data, ['purchaseinvoicedetail_tgledit' => date('Y-m-d H:i:s')]);
				$act = $this->td_purchaseinvoice_detail->update_data(['purchaseinvoicedetail_kd' => $purchaseinvoicedetail_kd], $data);
			}

			if ($act) {
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan' );
			}else {
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}
	
    public function action_update_diskon () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpurchaseinvoice_kd', 'Diskon', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpurchaseinvoice_diskonpersen' => (!empty(form_error('txtpurchaseinvoice_kd')))?buildLabel('warning', form_error('txtpurchaseinvoice_kd', '"', '"')):'',
			);
			
		}else {
			$purchaseinvoice_kd = $this->input->post('txtpurchaseinvoice_kd', true);
			$purchaseinvoice_jenisdiskon = $this->input->post('txtpurchaseinvoice_jenisdiskon', true);
			$purchaseinvoice_diskonpersen = $this->input->post('txtpurchaseinvoice_diskonpersen', true);
			$purchaseinvoice_diskonnominal = $this->input->post('txtpurchaseinvoice_diskonnominal', true);		

			if ($purchaseinvoice_jenisdiskon == 'persentase'){
				$purchaseinvoice_diskonnominal = null;
			}else{
				$purchaseinvoice_diskonpersen = null;
			}
            $data = [
                'purchaseinvoice_jenisdiskon' => $purchaseinvoice_jenisdiskon,
                'purchaseinvoice_diskonpersen' => $purchaseinvoice_diskonpersen,
                'purchaseinvoice_diskonnominal' => $purchaseinvoice_diskonnominal,
			];

			$act = $this->tm_purchaseinvoice->update_data (['purchaseinvoice_kd' => $purchaseinvoice_kd], $data);
			if ($act) {
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['purchaseinvoice_kd' => $purchaseinvoice_kd]);
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

    public function action_update_ppn () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpurchaseinvoice_kd', 'PPN', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpurchaseinvoice_kd' => (!empty(form_error('txtpurchaseinvoice_kd')))?buildLabel('warning', form_error('txtpurchaseinvoice_kd', '"', '"')):'',
			);
			
		}else {
			$purchaseinvoice_kd = $this->input->post('txtpurchaseinvoice_kd', true);
			$purchaseinvoice_ppn = $this->input->post('txtpurchaseinvoice_ppn', true);	

            $data = [
                'purchaseinvoice_ppn' => !empty($purchaseinvoice_ppn) ? $purchaseinvoice_ppn : null,
			];

			$act = $this->tm_purchaseinvoice->update_data (['purchaseinvoice_kd' => $purchaseinvoice_kd], $data);
			if ($act) {
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['purchaseinvoice_kd' => $purchaseinvoice_kd]);
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

    public function action_update_um () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpurchaseinvoice_kd', 'PPN', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpurchaseinvoice_kd' => (!empty(form_error('txtpurchaseinvoice_kd')))?buildLabel('warning', form_error('txtpurchaseinvoice_kd', '"', '"')):'',
			);
			
		}else {
			$purchaseinvoice_kd = $this->input->post('txtpurchaseinvoice_kd', true);
			$purchaseinvoice_kdUm = $this->input->post('txtpurchaseinvoice_kdUm', true);	

            $data = ['purchaseinvoice_parent' => !empty($purchaseinvoice_kd) ? $purchaseinvoice_kd : null];
			$actUpdateInvDP = $this->tm_purchaseinvoice->update_data (['purchaseinvoice_kd' => $purchaseinvoice_kdUm], $data);
			if ($actUpdateInvDP) {
				$rInv = $this->tm_purchaseinvoice->get_by_param(['purchaseinvoice_kd' => $purchaseinvoice_kd])->row_array();
				$purchaseinvoice_dp = $rInv['purchaseinvoice_dp'];
				$calGrandTotal = $this->tm_purchaseinvoice->calc_grandtotal_dp_dpp_batch([$purchaseinvoice_kdUm]);
				$purchaseinvoice_dp += $calGrandTotal[0]['grandTotal'];

				$dataUm = ['purchaseinvoice_dp' =>  $purchaseinvoice_dp];
				$actUpdateInv = $this->tm_purchaseinvoice->update_data (['purchaseinvoice_kd' => $purchaseinvoice_kd], $dataUm);
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['purchaseinvoice_kd' => $purchaseinvoice_kd]);
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_hapus_diskon () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$purchaseinvoice_kd = $this->input->get('id', TRUE);
		
		$data = [
			'purchaseinvoice_jenisdiskon' => null,
			'purchaseinvoice_diskonpersen' => null,
			'purchaseinvoice_diskonnominal' => null,			
		];
		$act = $this->tm_purchaseinvoice->update_data (['purchaseinvoice_kd' => $purchaseinvoice_kd], $data);
		if ($act == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}

	public function action_hapus_detail () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$detpayment_kd = $this->input->get('id', TRUE);

		/** Cek terkait GR */

		$act = $this->td_payment->delete_data ($detpayment_kd);
		if ($act == true){
			
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}

	public function action_hapus_master () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$purchaseinvoice_kd = $this->input->get('id', TRUE);

		/** Cek terkait GR */
		$dataRmgr = [];
		$qPurchinvDetail = $this->td_purchaseinvoice_detail->get_by_param(['purchaseinvoice_kd' => $purchaseinvoice_kd])->result_array();
		foreach ($qPurchinvDetail as $e) {
			if (empty($e['rmgr_kd'])){
				continue;
			}
			$dataRmgr[] = [
				'rmgr_kd' => $e['rmgr_kd'],
				'rmgr_invoice' => null,
			];
		}

		$actDetail = $this->td_purchaseinvoice_detail->delete_by_param(['purchaseinvoice_kd' => $purchaseinvoice_kd]);
		if ($actDetail == true){
			$actMaster = $this->tm_purchaseinvoice->delete_data($purchaseinvoice_kd);
			if (!empty($dataRmgr)){
				$actUpd = $this->td_rawmaterial_goodsreceive->update_batch ('rmgr_kd', $dataRmgr);
			}
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}

	public function action_hapus_um() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$purchaseinvoice_kd = $this->input->get('id', TRUE);

		$rPurchinv = $this->tm_purchaseinvoice->get_by_param(['purchaseinvoice_kd' => $purchaseinvoice_kd])->row_array();
		$calGrandTotal = $this->tm_purchaseinvoice->calc_grandtotal_dp_batch([$purchaseinvoice_kd]);
		$purchaseinvoice_dp = $calGrandTotal[0]['grandTotal'];

		$rPurchinvParent = $this->tm_purchaseinvoice->get_by_param(['purchaseinvoice_kd' => $rPurchinv['purchaseinvoice_parent']])->row_array();
		$sisadp = $rPurchinvParent['purchaseinvoice_dp'] - $purchaseinvoice_dp;

		$act = $this->tm_purchaseinvoice->update_data(['purchaseinvoice_kd' => $purchaseinvoice_kd], ['purchaseinvoice_parent' => null]);
		if ($act == true){
			$actUpdUm = $this->tm_purchaseinvoice->update_data(['purchaseinvoice_kd' => $rPurchinv['purchaseinvoice_parent']], ['purchaseinvoice_dp' => $sisadp]);
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}

	public function action_update_grandtotal () {
		// if (!$this->input->is_ajax_request()){
		// 	exit('No direct script access allowed');
		// }
		$purchaseinvoice_kd = $this->input->get('id', TRUE);
		$purchaseinvoice_grandtotal = $this->input->get('grandtotal', TRUE);

		$act = $this->tm_payment->update_data(['no_check' => $purchaseinvoice_kd], ['grand_total' => !empty($purchaseinvoice_grandtotal) ? $purchaseinvoice_grandtotal : null]);
		if ($act){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		}

		echo json_encode($resp);
	}
	
}
