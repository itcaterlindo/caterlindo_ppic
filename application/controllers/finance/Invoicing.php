<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Invoicing extends MY_Controller {
	private $class_link = 'finance/invoicing';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_purchaseinvoice', 'tm_suplier', 'td_purchaseinvoice_detail', 'td_rawmaterial_goodsreceive', 'tb_pph']);
	}

	public function index() {
        parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
        parent::datetimepicker_assets();
        $this->load->js('assets/admin_assets/plugins/input-mask/jquery.inputmask.js');
        $data['class_link'] = $this->class_link;
        $this->load->view('page/'.$this->class_link.'/table_box', $data);
	}
    
    public function table_main(){
    	// if (!$this->input->is_ajax_request()){
    	// 	exit('No direct script access allowed');
    	// }
		$bulan = $this->input->get('bulan', true);
        $tahun = $this->input->get('tahun', true);

    	$data['class_link'] = $this->class_link;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;

		// echo json_encode($data);

    	$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }

    public function table_data (){
    	// if (!$this->input->is_ajax_request()){
    	// 	exit('No direct script access allowed');
    	// }
			
		$bulan = $this->input->get('bulan', true);
        $tahun = $this->input->get('tahun', true);

    	$this->load->library(['ssp']);

		$data = $this->tm_purchaseinvoice->ssp_table2($bulan, $tahun);
		
		echo json_encode($data);
    }

	public function form_box () {
        if (!$this->input->is_ajax_request()){
    		exit('No direct script access allowed');
        }
        $id = $this->input->get('id', true);
        $sts = $this->input->get('sts', true);

        $data['class_link'] = $this->class_link;
        $data['id'] = $id;
        $data['sts'] = $sts;
		$this->load->view('page/'.$this->class_link.'/form_box', $data);
    }
    
    public function form_master_main(){
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
        }
        $id = $this->input->get('id', true);
        $sts = $this->input->get('sts', true);
        
        if ($sts == 'edit') {
			$data['rowData'] = $this->db->select('tm_purchaseinvoice.*, tm_suplier.suplier_kode, tm_suplier.suplier_nama')
					->from('tm_purchaseinvoice')
					->join('tm_suplier', 'tm_purchaseinvoice.suplier_kd=tm_suplier.suplier_kd', 'left')
					->where('tm_purchaseinvoice.purchaseinvoice_kd', $id)
					->get()->row_array();
		}     
        
        $data['id'] = $id;
        $data['sts'] = $sts;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/form_master_main', $data);
    }

    public function formdetail_box() {
        parent::administrator();
		parent::pnotify_assets();
        parent::select2_assets();
        parent::icheck_assets();
        $id = $this->input->get('id', true);

		$data['master'] = $this->db->select('tm_purchaseinvoice.*, tm_suplier.suplier_kode, tm_suplier.suplier_nama')
				->from('tm_purchaseinvoice')
				->join('tm_suplier', 'tm_purchaseinvoice.suplier_kd=tm_suplier.suplier_kd', 'left')
				->where('tm_purchaseinvoice.purchaseinvoice_kd', $id)
				->get ()->row_array();
        $data['id'] = $id;
        $data['class_link'] = $this->class_link;

		// header('Content-Type: application/json');
		// echo json_encode($data);
        $this->load->view('page/'.$this->class_link.'/formdetail_box', $data);
    }
    
    function formdetail_main () {
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
        }
        $id = $this->input->get('id', true);
		$sts = $this->input->get('sts', true);
		
		 /** PPh */
		 $actPph = $this->tb_pph->get_all()->result_array();
		 $opsiPph[''] = '-- Pilih Opsi --';
		 foreach ($actPph as $each):
			 $opsiPph[$each['pph_kd']] = $each['pph_nama'];
		 endforeach;

		$data['id'] = $id;
		$data['sts'] = $sts;
		$data['opsiPph'] = $opsiPph;
        $this->load->view('page/'.$this->class_link.'/formdetail_main', $data);
	}
	
    function formdetail_edit () {
        if (!$this->input->is_ajax_request()){
            exit('No direct script access allowed');
		}
		
        $purchaseinvoicedetail_kd = $this->input->get('id', true);
		$sts = $this->input->get('sts', true);

		$data['row'] = $this->db->select('td_purchaseinvoice_detail.*, tm_purchaseorder.po_no, td_rawmaterial_goodsreceive.rmgr_nosrj, tm_rawmaterial.rm_kode, td_rawmaterial_satuan.rmsatuan_nama')
				->from('td_purchaseinvoice_detail')
				->join('td_purchaseorder_detail', 'td_purchaseinvoice_detail.podetail_kd=td_purchaseorder_detail.podetail_kd', 'left')
				->join('tm_purchaseorder', 'td_purchaseorder_detail.po_kd=tm_purchaseorder.po_kd', 'left')
				->join('td_rawmaterial_goodsreceive', 'td_purchaseinvoice_detail.rmgr_kd=td_rawmaterial_goodsreceive.rmgr_kd', 'left')
				->join('tm_rawmaterial', 'td_purchaseinvoice_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
				->join('td_rawmaterial_satuan', 'td_purchaseinvoice_detail.purchaseinvoicedetail_satuankd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
				->where('td_purchaseinvoice_detail.purchaseinvoicedetail_kd', $purchaseinvoicedetail_kd)
				->get()->row_array();
		
		 /** PPh */
		 $actPph = $this->tb_pph->get_all()->result_array();
		 $opsiPph[''] = '-- Pilih Opsi --';
		 foreach ($actPph as $each):
			 $opsiPph[$each['pph_kd']] = $each['pph_nama'];
		 endforeach;

		$data['id'] = $purchaseinvoicedetail_kd;
		$data['sts'] = $sts;
		$data['opsiPph'] = $opsiPph;
        $this->load->view('page/'.$this->class_link.'/formdetail_edit', $data);
	}
	
	function ppnform_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$purchaseinvoice_kd = $this->input->get('id', true);
		$sts = $this->input->get('sts', true);

		$data['master'] = $this->tm_purchaseinvoice->get_by_param (['purchaseinvoice_kd' => $purchaseinvoice_kd])->row_array();
		$data['id'] = $purchaseinvoice_kd;
		$this->load->view('page/'.$this->class_link.'/ppnform_main', $data);
	}

    public function table_rmgrdetail () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$rmgr_nosrj = $this->input->get('rmgr_nosrj', true);
		$po_kd = $this->input->get('po_kd', true);
		$id = $this->input->get('id', true);
		
		$goodsreceive = [];
		if (!empty($rmgr_nosrj)) {	
			$goodsreceive = $this->db->select('td_rawmaterial_goodsreceive.rmgr_nosrj, td_rawmaterial_goodsreceive.podetail_kd, rmgr_qty, rmgr_qtykonversi, tm_purchaseorder.po_no, tm_suplier.kd_currency, tm_suplier.suplier_nama, tb_set_dropdown.nm_select,
						td_purchaseorder_detail.rm_kd, td_purchaseorder_detail.podetail_nama, td_purchaseorder_detail.podetail_deskripsi, td_purchaseorder_detail.podetail_spesifikasi, td_purchaseorder_detail.podetail_harga, td_purchaseorder_detail.podetail_konversi, td_purchaseorder_detail.podetail_qty, td_purchaseorder_detail.rmsatuan_kd,
						td_rawmaterial_satuan.rmsatuan_nama, tm_rawmaterial.rm_kode, td_rawmaterial_goodsreceive.rmgr_tgldatang, td_rawmaterial_goodsreceive.rmgr_kd,
						tm_currency.icon_type, tm_currency.currency_icon, tm_currency.pemisah_angka, tm_currency.format_akhir')
					->from('td_rawmaterial_goodsreceive')
					->join('td_purchaseorder_detail', 'td_rawmaterial_goodsreceive.podetail_kd=td_purchaseorder_detail.podetail_kd', 'left')
					->join('tm_rawmaterial', 'td_purchaseorder_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
					->join('td_rawmaterial_satuan', 'td_purchaseorder_detail.rmsatuan_kd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
					->join('tm_purchaseorder', 'td_rawmaterial_goodsreceive.po_kd=tm_purchaseorder.po_kd', 'left')
					->join('tm_suplier', 'tm_purchaseorder.suplier_kd=tm_suplier.suplier_kd', 'left')
					->join('tb_set_dropdown', 'tm_suplier.badanusaha_kd=tb_set_dropdown.id', 'left')
					->join('tm_currency', 'tm_suplier.kd_currency=tm_currency.kd_currency')
					->where ('td_rawmaterial_goodsreceive.rmgr_nosrj', $rmgr_nosrj)
					->where ('td_rawmaterial_goodsreceive.po_kd', $po_kd)
					->where('td_rawmaterial_goodsreceive.rmgr_invoice IS NULL')
					->order_by('td_rawmaterial_goodsreceive.podetail_kd', 'asc')
					->get()
					->result_array();
		}
		$data['rmgr_nosrj'] = $rmgr_nosrj;
		$data['goodsreceive'] = $goodsreceive;
		$data['id'] = $id;
		$data['po_kd'] = $po_kd;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/table_rmgrdetail', $data);
	}
	
	public function tabledetail_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$purchaseinvoice_kd = $this->input->get('id', true);

		$data['result'] = $this->db->select('td_purchaseinvoice_detail.*, td_rawmaterial_satuan.rmsatuan_nama, td_purchaseorder_detail.podetail_qty, td_purchaseorder_detail.podetail_harga, tm_purchaseorder.po_no, tm_rawmaterial.rm_kode, tm_rawmaterial.rm_oldkd, td_rawmaterial_goodsreceive.rmgr_nosrj,
					tb_pph.pph_nama')
				->from('td_purchaseinvoice_detail')
				->join('td_rawmaterial_satuan', 'td_purchaseinvoice_detail.purchaseinvoicedetail_satuankd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
				->join('td_purchaseorder_detail', 'td_purchaseinvoice_detail.podetail_kd=td_purchaseorder_detail.podetail_kd', 'left')
				->join('tm_purchaseorder', 'tm_purchaseorder.po_kd=td_purchaseorder_detail.po_kd', 'left')
				->join('tm_rawmaterial', 'td_purchaseinvoice_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
				->join('td_rawmaterial_goodsreceive', 'td_purchaseinvoice_detail.rmgr_kd=td_rawmaterial_goodsreceive.rmgr_kd', 'left')
				->join('tb_pph', 'td_purchaseinvoice_detail.pph_kd=tb_pph.pph_kd', 'left')
				->where('td_purchaseinvoice_detail.purchaseinvoice_kd', $purchaseinvoice_kd)
				->get()->result_array();

		$data['master'] = $this->tm_purchaseinvoice->get_by_param (['purchaseinvoice_kd' => $purchaseinvoice_kd])->row_array();
		$data['class_link'] = $this->class_link;
		$data['id'] = $purchaseinvoice_kd;
		$this->load->view('page/'.$this->class_link.'/tabledetail_main', $data);
	}

	public function view_box() {
        parent::administrator();
		parent::pnotify_assets();
        $id = $this->input->get('id', true);
        $id = $this->input->get('id', true);
		
		$data['master'] = $this->db->select('tm_purchaseinvoice.*, tm_suplier.suplier_kode, tm_suplier.suplier_nama')
				->from('tm_purchaseinvoice')
				->join('tm_suplier', 'tm_purchaseinvoice.suplier_kd=tm_suplier.suplier_kd', 'left')
				->where('tm_purchaseinvoice.purchaseinvoice_kd', $id)
				->get ()->row_array();
        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        $this->load->view('page/'.$this->class_link.'/view_box', $data);
	}
	
	public function view_table () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$id = $this->input->get('id', true);
		
		$data['master'] = $this->tm_purchaseinvoice->get_by_param (['purchaseinvoice_kd' => $id])->row_array();
		$data['result'] = $this->db->select('td_purchaseinvoice_detail.*, td_purchaseorder_detail.podetail_harga, tm_purchaseorder.po_no, td_rawmaterial_goodsreceive.rmgr_nosrj,
						tm_rawmaterial.rm_kode, tm_rawmaterial.rm_oldkd, td_rawmaterial_satuan.rmsatuan_nama, tb_pph.pph_nama')
					->from('td_purchaseinvoice_detail')
					->join('td_purchaseorder_detail', 'td_purchaseinvoice_detail.podetail_kd=td_purchaseorder_detail.podetail_kd', 'left')
					->join('tm_purchaseorder', 'td_purchaseorder_detail.po_kd=tm_purchaseorder.po_kd', 'left')
					->join('td_rawmaterial_goodsreceive', 'td_purchaseinvoice_detail.rmgr_kd=td_rawmaterial_goodsreceive.rmgr_kd', 'left')
					->join('tm_rawmaterial', 'td_purchaseinvoice_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
					->join('td_rawmaterial_satuan', 'td_purchaseinvoice_detail.purchaseinvoicedetail_satuankd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
					->join('tb_pph', 'td_purchaseinvoice_detail.pph_kd=tb_pph.pph_kd', 'left')
					->where('td_purchaseinvoice_detail.purchaseinvoice_kd', $id)
					->get()->result_array();

		$data['id'] = $id;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/view_table', $data);
	}
	
	public function view_pph () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$purchaseinvoicedetail_kd = $this->input->get('purchaseinvoicedetail_kd', true);
		
		$data['row'] = $this->db->select()
					->from('td_purchaseinvoice_detail')
					->join('tb_pph', 'td_purchaseinvoice_detail.pph_kd=tb_pph.pph_kd', 'left')
					->where(['purchaseinvoicedetail_kd' => $purchaseinvoicedetail_kd])
					->get()->row_array();
		$data['purchaseinvoicedetail_kd'] = $purchaseinvoicedetail_kd;
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/view_pph', $data);
	}
	
	public function formdetail_diskon () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$purchaseinvoice_kd = $this->input->get('id', true);

		$data['id'] = $purchaseinvoice_kd;
		$data['row'] = $this->tm_purchaseinvoice->get_by_param (['purchaseinvoice_kd' => $purchaseinvoice_kd])->row_array();
		$data['class_link'] = $this->class_link;
		$this->load->view('page/'.$this->class_link.'/formdetail_diskon', $data);
	}
	
	public function dpform_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$purchaseinvoice_kd = $this->input->get('id', true);

		$data['id'] = $purchaseinvoice_kd;
		$data['sts'] = 'add';
		$data['class_link'] = $this->class_link;
		
		$this->load->view('page/'.$this->class_link.'/dpform_main', $data);
	}

	public function dptable_main () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$purchaseinvoice_kd = $this->input->get('id', true);

		$resultInv = $this->tm_purchaseinvoice->get_by_param(['purchaseinvoice_parent' => $purchaseinvoice_kd])->result_array();
		$purchaseinvoice_kds = array_column($resultInv, 'purchaseinvoice_kd');

		$data['result'] = $this->tm_purchaseinvoice->calc_grandtotal_dp_batch($purchaseinvoice_kds);		
		$data['class_link'] = $this->class_link;
		$data['id'] = $purchaseinvoice_kd;
		$this->load->view('page/'.$this->class_link.'/dptable_main', $data);
	}

    public function get_grby_suratjalan() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$paramSRJ = $this->input->get('paramSRJ', true);
		$suplier_kd = $this->input->get('suplier_kd', true);
		$pi_jenis = $this->input->get('pi_jenis', true);

		$result = [];

		if ($pi_jenis == 'dp') {
			$this->db->select('tm_purchaseorder.po_no, tm_suplier.suplier_nama, tm_purchaseorder.po_kd')
			->from('tm_purchaseorder')
			->join('tm_suplier', 'tm_purchaseorder.suplier_kd=tm_suplier.suplier_kd', 'left')
			->where('tm_purchaseorder.wfstate_kd', '15')
			->where('tm_purchaseorder.suplier_kd', $suplier_kd);
			if (!empty($paramSRJ)) {
				$this->db->group_start()
				->like('tm_purchaseorder.po_no', $paramSRJ)
				->or_like('tm_suplier.suplier_nama', $paramSRJ)
				->group_end();
			}
			$this->db->group_by('tm_purchaseorder.po_kd')
			->order_by('tm_purchaseorder.po_no', 'asc');
			$query = $this->db->get();
			if (!empty($query->num_rows())) {
				$query = $query->result_array();
				foreach ($query as $r) {
					$result[] = array(
						'id' => $r['po_kd'],
						'text' => $r['po_no'].'|'.$r['suplier_nama'],
						'po_kd' => $r['po_kd'],
					); 
				}
			}
		}else{
			$this->db->select('td_rawmaterial_goodsreceive.rmgr_nosrj, tm_purchaseorder.po_no, tm_suplier.suplier_nama, td_rawmaterial_goodsreceive.po_kd')
			->from('td_rawmaterial_goodsreceive')
			->join('td_purchaseorder_detail', 'td_rawmaterial_goodsreceive.podetail_kd=td_purchaseorder_detail.podetail_kd', 'left')
			->join('tm_purchaseorder', 'td_purchaseorder_detail.po_kd=tm_purchaseorder.po_kd', 'left')
			->join('tm_suplier', 'tm_purchaseorder.suplier_kd=tm_suplier.suplier_kd', 'left')
			->where('rmgr_invoice IS NULL')
			->where('tm_purchaseorder.suplier_kd', $suplier_kd);
			if (!empty($paramSRJ)) {
				$this->db->group_start()
				->like('td_rawmaterial_goodsreceive.rmgr_nosrj', $paramSRJ)
				->or_like('tm_purchaseorder.po_no', $paramSRJ)
				->or_like('tm_suplier.suplier_nama', $paramSRJ)
				->group_end();
			}
			$this->db->group_by('td_rawmaterial_goodsreceive.rmgr_nosrj, tm_purchaseorder.po_kd')
			->order_by('tm_purchaseorder.po_no', 'asc');
			$query = $this->db->get();
			if (!empty($query->num_rows())) {
				$query = $query->result_array();
				foreach ($query as $r) {
					$result[] = array(
						'id' => $r['rmgr_nosrj'],
						'text' => $r['po_no'].'|'.$r['suplier_nama'].' / '.$r['rmgr_nosrj'],
						'po_kd' => $r['po_kd'],
					); 
				}
			}
		}
		
		echo json_encode($result);
	}

	public function get_invoice_dp() {
		$purchaseinvoice_kd = $this->input->get('purchaseinvoice_kd', true);
		$paramInv = $this->input->get('paramInv', true);

		$rInv = $this->tm_purchaseinvoice->get_by_param(['purchaseinvoice_kd' => $purchaseinvoice_kd])->row_array();
		$resultInvDp = $this->db->select()
					->from('tm_purchaseinvoice')
					->join('tm_suplier', 'tm_purchaseinvoice.suplier_kd=tm_suplier.suplier_kd', 'left')
					->where('tm_purchaseinvoice.suplier_kd', $rInv['suplier_kd'])
					->where('tm_purchaseinvoice.purchaseinvoice_jenis', 'dp')
					->where('tm_purchaseinvoice.purchaseinvoice_parent IS NULL', null)
					->like('tm_purchaseinvoice.purchaseinvoice_no', $paramInv, 'match')
					->get()->result_array();
		$result = [];
		foreach ($resultInvDp as $eInvDp) {
			$result [] = [
				'id' => $eInvDp['purchaseinvoice_kd'],
				'text' => $eInvDp['purchaseinvoice_no']
			];
		}

		echo json_encode($result);
	}
	
	public function get_date_termpayment () {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$purchaseinvoice_tanggal = $this->input->get('purchaseinvoice_tanggal', true);
		$purchaseinvoice_termpayment = $this->input->get('purchaseinvoice_termpayment', true);

		$termpayment_date = date('Y-m-d');
		if (!empty($purchaseinvoice_termpayment)) {
			$time_purchaseinvoice_tanggal = strtotime($purchaseinvoice_tanggal);
			$time_termpayment_date = strtotime('+'.$purchaseinvoice_termpayment.' day', $time_purchaseinvoice_tanggal);
			$termpayment_date = date('Y-m-d', $time_termpayment_date);
		}
		$dt = [
			'purchaseinvoice_tanggal' => $purchaseinvoice_tanggal,
			'purchaseinvoice_termpayment' => $purchaseinvoice_termpayment,
			'termpayment_date' => $termpayment_date,
		];
		echo json_encode($dt);
	}
    
    public function action_insert_master () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtsuplier_kd', 'Suplier', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtpurchaseinvoice_no', 'No', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtpurchaseinvoice_tanggal', 'Tanggal', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtpurchaseinvoice_tanggaltermpayment', 'Tanggal', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrsuplier_kd' => (!empty(form_error('txtsuplier_kd')))?buildLabel('warning', form_error('txtsuplier_kd', '"', '"')):'',
				'idErrpurchaseinvoice_no' => (!empty(form_error('txtpurchaseinvoice_no')))?buildLabel('warning', form_error('txtpurchaseinvoice_no', '"', '"')):'',
				'idErrpurchaseinvoice_tanggal' => (!empty(form_error('txtpurchaseinvoice_tanggal')))?buildLabel('warning', form_error('txtpurchaseinvoice_tanggal', '"', '"')):'',
				'idErrpurchaseinvoice_tanggaltermpayment' => (!empty(form_error('txtpurchaseinvoice_tanggaltermpayment')))?buildLabel('warning', form_error('txtpurchaseinvoice_tanggaltermpayment', '"', '"')):'',
			);
			
		}else {
			$sts = $this->input->post('txtSts', true);
			$purchaseinvoice_kd = $this->input->post('txtpurchaseinvoice_kd', true);
			$suplier_kd = $this->input->post('txtsuplier_kd', true);
			$po_kd = $this->input->post('txtno_po_dp', true);
			$amount = $this->input->post('txtpurchaseinvoice_grand_total', true);
			$purchaseinvoice_no = $this->input->post('txtpurchaseinvoice_no', true);
			$purchaseinvoice_faktur = $this->input->post('txtpurchaseinvoice_faktur', true);
			$purchaseinvoice_tanggal = $this->input->post('txtpurchaseinvoice_tanggal', true);
			$purchaseinvoice_tanggalfaktur = $this->input->post('txtpurchaseinvoice_tanggalfaktur', true);
			$purchaseinvoice_tanggaltt = $this->input->post('txtpurchaseinvoice_tanggaltt', true);
			$purchaseinvoice_termpayment = $this->input->post('txtpurchaseinvoice_termpayment', true);
			$purchaseinvoice_tanggaltermpayment = $this->input->post('txtpurchaseinvoice_tanggaltermpayment', true);
			$purchaseinvoice_ppn = $this->input->post('txtpurchaseinvoice_ppn', true);
			$purchaseinvoice_jenis = $this->input->post('txtpurchaseinvoice_jenis', true);
			$purchaseinvoice_note = $this->input->post('txtpurchaseinvoice_note', true);
			$pph_sap = $this->input->post('txtpph_SAP', true);
            $data = [
                'suplier_kd' => $suplier_kd,
				'po_kd' => $po_kd ? $po_kd : '',
				'purchaseinvoice_grandtotal' => $amount ? $amount : '',
                'purchaseinvoice_jenis' => !empty($purchaseinvoice_jenis) ? $purchaseinvoice_jenis : null,
                'purchaseinvoice_no' => $purchaseinvoice_no,
                'purchaseinvoice_faktur' => $purchaseinvoice_faktur,
				'pph_sap' => $pph_sap ? $pph_sap : '',
                'purchaseinvoice_tanggal' => !empty($purchaseinvoice_tanggal) ? format_date($purchaseinvoice_tanggal, 'Y-m-d') :null ,
                'purchaseinvoice_tanggalfaktur' => !empty($purchaseinvoice_tanggalfaktur) ? format_date($purchaseinvoice_tanggalfaktur, 'Y-m-d') : null,
                'purchaseinvoice_tanggaltt' => !empty($purchaseinvoice_tanggaltt) ? format_date($purchaseinvoice_tanggaltt, 'Y-m-d') : null,
                'purchaseinvoice_termpayment' => !empty($purchaseinvoice_termpayment) ? $purchaseinvoice_termpayment : null,
                'purchaseinvoice_tanggaltermpayment' => format_date($purchaseinvoice_tanggaltermpayment, 'Y-m-d'),
				'purchaseinvoice_ppn' => !empty($purchaseinvoice_ppn) ? $purchaseinvoice_ppn : null,
				'purchaseinvoice_note' => !empty($purchaseinvoice_note) ? $purchaseinvoice_note : null,
				'purchaseinvoice_tgledit' => date('Y-m-d H:i:s'),
				'admin_kd' => $this->session->userdata('kd_admin'),
			];

            $act = false;
			if ($sts == 'add' || empty($purchaseinvoice_kd)) {
                $purchaseinvoice_kd = $this->tm_purchaseinvoice->create_code();
                $data = array_merge($data, ['purchaseinvoice_kd' => $purchaseinvoice_kd, 'purchaseinvoice_tglinput' => date('Y-m-d H:i:s')]);
                $act = $this->tm_purchaseinvoice->insert_data($data);
            }else {
                $data = array_merge($data, ['purchaseinvoice_tgledit' => date('Y-m-d H:i:s')]);
                $act = $this->tm_purchaseinvoice->update_data (['purchaseinvoice_kd' => $purchaseinvoice_kd], $data);
            }

			if ($act) {
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['purchaseinvoice_kd' => $purchaseinvoice_kd]);
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

    public function action_insert_batch () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpurchaseinvoice_kdGR', 'Master', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpurchaseinvoice_kdGR' => (!empty(form_error('txtpurchaseinvoice_kdGR')))?buildLabel('warning', form_error('txtpurchaseinvoice_kdGR', '"', '"')):'',
			);
			
		}else {
			$rmgr_kds = $this->input->post('txtrmgr_kds', true);
			$podetail_kds = $this->input->post('txtpodetail_kds', true);
			$rm_kds = $this->input->post('txtrm_kds', true);
			$podetail_namas = $this->input->post('txtpodetail_namas', true);
			$podetail_deskripsis = $this->input->post('txtpodetail_deskripsis', true);
			$podetail_spesifikasis = $this->input->post('txtpodetail_spesifikasis', true);
			$podetail_qtys = $this->input->post('txtpodetail_qtys', true);
			$purchaseinvoicedetail_hargaunits = $this->input->post('txtpurchaseinvoicedetail_hargaunits', true);
			$purchaseinvoice_kd = $this->input->post('txtpurchaseinvoice_kdGR', true);
			$rmsatuan_kds = $this->input->post('txtrmsatuan_kds', true);
			$rmgr_qtys = $this->input->post('txtrmgr_qtys', true);
			
			$purchaseinvoicedetail_kd = $this->td_purchaseinvoice_detail->create_code();
			if (!empty($rmgr_kds)) {
				foreach ($rmgr_kds as $rmgr_kd) {
					$data[] = [
						'purchaseinvoicedetail_kd' => $purchaseinvoicedetail_kd,
						'purchaseinvoice_kd' => $purchaseinvoice_kd,
						'rmgr_kd' => $rmgr_kd,
						'podetail_kd' => $podetail_kds[$rmgr_kd],
						'rm_kd' => $rm_kds[$rmgr_kd],
						'purchaseinvoicedetail_nama' => $podetail_namas[$rmgr_kd],
						'purchaseinvoicedetail_deskripsi' => $podetail_deskripsis[$rmgr_kd],
						'purchaseinvoicedetail_spesifikasi' => $podetail_spesifikasis[$rmgr_kd],
						'purchaseinvoicedetail_hargaunit' => $purchaseinvoicedetail_hargaunits[$rmgr_kd],
						'purchaseinvoicedetail_qty' => $rmgr_qtys[$rmgr_kd],
						'purchaseinvoicedetail_satuankd' => $rmsatuan_kds[$rmgr_kd],
						'purchaseinvoicedetail_tglinput' => date('Y-m-d H:i:s'),
						'admin_kd' => $this->session->userdata('kd_admin'),
					];
					$purchaseinvoicedetail_kd++;
					
					/** Data update received */
					$dataGR[] = [
						'rmgr_kd' => $rmgr_kd,
						'rmgr_invoice' => 1,
					];
				}
				$act = $this->td_purchaseinvoice_detail->insert_batch($data);
				if ($act) {
					/** Update rmgr flag invoiced */
					$actInvoiced = $this->td_rawmaterial_goodsreceive->update_batch ('rmgr_kd', $dataGR);
                	$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['rmgr_nosrj']);
				}else {
					$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
				}
			}else {
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Input Kosong');
			}

		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

    public function action_insert_detail () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtrm_kd', 'Barang', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtpurchaseinvoicedetail_qty', 'Qty', 'required', ['required' => '{field} tidak boleh kosong!']);		
		$this->form_validation->set_rules('txtpurchaseinvoicedetail_hargaunit', 'Harga', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrrm_kd' => (!empty(form_error('txtrm_kd')))?buildLabel('warning', form_error('txtrm_kd', '"', '"')):'',
				'idErrpurchaseinvoicedetail_qty' => (!empty(form_error('txtpurchaseinvoicedetail_qty')))?buildLabel('warning', form_error('txtpurchaseinvoicedetail_qty', '"', '"')):'',
				'idErrpurchaseinvoicedetail_hargaunit' => (!empty(form_error('txtpurchaseinvoicedetail_hargaunit')))?buildLabel('warning', form_error('txtpurchaseinvoicedetail_hargaunit', '"', '"')):'',
			);
			
		}else {
			$purchaseinvoicedetail_kd = $this->input->post('txtpurchaseinvoicedetail_kd', true);
			$purchaseinvoice_kd = $this->input->post('txtpurchaseinvoice_kd', true);
			$rmgr_kd = $this->input->post('txtrmgr_kd', true);
			$podetail_kd = $this->input->post('txtpodetail_kd', true);
			$rm_kd = $this->input->post('txtrm_kd', true);
			$purchaseinvoicedetail_nama = $this->input->post('txtpurchaseinvoicedetail_nama', true);
			$purchaseinvoicedetail_deskripsi = $this->input->post('txtpurchaseinvoicedetail_deskripsi', true);
			$purchaseinvoicedetail_spesifikasi = $this->input->post('txtpurchaseinvoicedetail_spesifikasi', true);
			$purchaseinvoicedetail_hargaunit = $this->input->post('txtpurchaseinvoicedetail_hargaunit', true);
			$purchaseinvoicedetail_qty = $this->input->post('txtpurchaseinvoicedetail_qty', true);
			$purchaseinvoicedetail_satuankd = $this->input->post('txtpurchaseinvoicedetail_satuankd', true);
			$rmsatuan_kds = $this->input->post('txtrmsatuan_kds', true);
			$pph_kd = $this->input->post('txtpph_kd', true);
			$purchaseinvoicedetail_pphpotongpayment = $this->input->post('txtpurchaseinvoicedetail_pphpotongpayment', true);

			$purchaseinvoicedetail_pphnominal = null;
			$purchaseinvoicedetail_pphpersen = null;
			if (!empty($pph_kd)) {
				$pph = $this->tb_pph->get_by_param(['pph_kd' => $pph_kd])->row_array();
				if ($purchaseinvoicedetail_pphpotongpayment == '1'){
					$purchaseinvoicedetail_pphpersen = $pph['pph_persen'] * -1;
					$purchaseinvoicedetail_pphnominal = $pph['pph_persen'] / 100 * $purchaseinvoicedetail_hargaunit * $purchaseinvoicedetail_qty * -1;
				}else {
					$purchaseinvoicedetail_pphpersen = $pph['pph_persen'];
					$purchaseinvoicedetail_pphnominal = $pph['pph_persen'] / 100 * $purchaseinvoicedetail_hargaunit * $purchaseinvoicedetail_qty;
				}
			}else{
				$purchaseinvoicedetail_pphpotongpayment = null;
			}
			
			$data = [
				'purchaseinvoice_kd' => $purchaseinvoice_kd,
				'rmgr_kd' => !empty($rmgr_kd) ? $rmgr_kd : null,
				'podetail_kd' => !empty($podetail_kd) ? $podetail_kd : null,
				'rm_kd' => !empty($rm_kd) ? $rm_kd : null,
				'purchaseinvoicedetail_nama' => !empty($purchaseinvoicedetail_nama) ? $purchaseinvoicedetail_nama : null,
				'purchaseinvoicedetail_deskripsi' => !empty($purchaseinvoicedetail_deskripsi) ? $purchaseinvoicedetail_deskripsi : null,
				'purchaseinvoicedetail_spesifikasi' => !empty($purchaseinvoicedetail_spesifikasi) ? $purchaseinvoicedetail_spesifikasi : null,
				'purchaseinvoicedetail_hargaunit' => !empty($purchaseinvoicedetail_hargaunit) ? $purchaseinvoicedetail_hargaunit : null,
				'purchaseinvoicedetail_qty' => !empty($purchaseinvoicedetail_qty) ? $purchaseinvoicedetail_qty : null,
				'purchaseinvoicedetail_satuankd' => !empty($purchaseinvoicedetail_satuankd) ? $purchaseinvoicedetail_satuankd : null,
				'purchaseinvoicedetail_pphpersen' => $purchaseinvoicedetail_pphpersen,
				'purchaseinvoicedetail_pphnominal' => $purchaseinvoicedetail_pphnominal,
				'purchaseinvoicedetail_pphpotongpayment' => $purchaseinvoicedetail_pphpotongpayment,
				'pph_kd' => !empty($pph_kd) ? $pph_kd : null,
				'admin_kd' => $this->session->userdata('kd_admin'),
			];

			$act = false;
			if (empty($purchaseinvoicedetail_kd)) {
				$purchaseinvoicedetail_kd = $this->td_purchaseinvoice_detail->create_code();
				$data = array_merge($data, ['purchaseinvoicedetail_kd' => $purchaseinvoicedetail_kd, 'purchaseinvoicedetail_tglinput' => date('Y-m-d H:i:s')]);
				$act = $this->td_purchaseinvoice_detail->insert_data($data);
			}else {
				$data = array_merge($data, ['purchaseinvoicedetail_tgledit' => date('Y-m-d H:i:s')]);
				$act = $this->td_purchaseinvoice_detail->update_data(['purchaseinvoicedetail_kd' => $purchaseinvoicedetail_kd], $data);
			}

			if ($act) {
				$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan' );
			}else {
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}
	
    public function action_update_diskon () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpurchaseinvoice_kd', 'Diskon', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpurchaseinvoice_diskonpersen' => (!empty(form_error('txtpurchaseinvoice_kd')))?buildLabel('warning', form_error('txtpurchaseinvoice_kd', '"', '"')):'',
			);
			
		}else {
			$purchaseinvoice_kd = $this->input->post('txtpurchaseinvoice_kd', true);
			$purchaseinvoice_jenisdiskon = $this->input->post('txtpurchaseinvoice_jenisdiskon', true);
			$purchaseinvoice_diskonpersen = $this->input->post('txtpurchaseinvoice_diskonpersen', true);
			$purchaseinvoice_diskonnominal = $this->input->post('txtpurchaseinvoice_diskonnominal', true);		

			if ($purchaseinvoice_jenisdiskon == 'persentase'){
				$purchaseinvoice_diskonnominal = null;
			}else{
				$purchaseinvoice_diskonpersen = null;
			}
            $data = [
                'purchaseinvoice_jenisdiskon' => $purchaseinvoice_jenisdiskon,
                'purchaseinvoice_diskonpersen' => $purchaseinvoice_diskonpersen,
                'purchaseinvoice_diskonnominal' => $purchaseinvoice_diskonnominal,
			];

			$act = $this->tm_purchaseinvoice->update_data (['purchaseinvoice_kd' => $purchaseinvoice_kd], $data);
			if ($act) {
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['purchaseinvoice_kd' => $purchaseinvoice_kd]);
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

    public function action_update_ppn () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpurchaseinvoice_kd', 'PPN', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpurchaseinvoice_kd' => (!empty(form_error('txtpurchaseinvoice_kd')))?buildLabel('warning', form_error('txtpurchaseinvoice_kd', '"', '"')):'',
			);
			
		}else {
			$purchaseinvoice_kd = $this->input->post('txtpurchaseinvoice_kd', true);
			$purchaseinvoice_ppn = $this->input->post('txtpurchaseinvoice_ppn', true);	

            $data = [
                'purchaseinvoice_ppn' => !empty($purchaseinvoice_ppn) ? $purchaseinvoice_ppn : null,
			];

			$act = $this->tm_purchaseinvoice->update_data (['purchaseinvoice_kd' => $purchaseinvoice_kd], $data);
			if ($act) {
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['purchaseinvoice_kd' => $purchaseinvoice_kd]);
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

    public function action_update_um () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}

		$this->load->library(['form_validation']);
		$this->form_validation->set_rules('txtpurchaseinvoice_kd', 'PPN', 'required', ['required' => '{field} tidak boleh kosong!']);		
		
		
		if ($this->form_validation->run() == FALSE) {
			$resp['code'] = 401;
			$resp['status'] = 'Required';
			$resp['pesan'] = array(
				'idErrpurchaseinvoice_kd' => (!empty(form_error('txtpurchaseinvoice_kd')))?buildLabel('warning', form_error('txtpurchaseinvoice_kd', '"', '"')):'',
			);
			
		}else {
			$purchaseinvoice_kd = $this->input->post('txtpurchaseinvoice_kd', true);
			$purchaseinvoice_kdUm = $this->input->post('txtpurchaseinvoice_kdUm', true);	

            $data = ['purchaseinvoice_parent' => !empty($purchaseinvoice_kd) ? $purchaseinvoice_kd : null];
			$actUpdateInvDP = $this->tm_purchaseinvoice->update_data (['purchaseinvoice_kd' => $purchaseinvoice_kdUm], $data);
			if ($actUpdateInvDP) {
				$rInv = $this->tm_purchaseinvoice->get_by_param(['purchaseinvoice_kd' => $purchaseinvoice_kd])->row_array();
				$purchaseinvoice_dp = $rInv['purchaseinvoice_dp'];
				$calGrandTotal = $this->tm_purchaseinvoice->calc_grandtotal_dp_batch([$purchaseinvoice_kdUm]);
				$purchaseinvoice_dp += $calGrandTotal[0]['grandTotal'];

				$dataUm = ['purchaseinvoice_dp' =>  $purchaseinvoice_dp];
				$actUpdateInv = $this->tm_purchaseinvoice->update_data (['purchaseinvoice_kd' => $purchaseinvoice_kd], $dataUm);
                $resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => ['purchaseinvoice_kd' => $purchaseinvoice_kd]);
			}else{
				$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
			}
		}
		$resp['csrf'] = $this->security->get_csrf_hash();
		echo json_encode($resp);
	}

	public function action_hapus_diskon () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$purchaseinvoice_kd = $this->input->get('id', TRUE);
		
		$data = [
			'purchaseinvoice_jenisdiskon' => null,
			'purchaseinvoice_diskonpersen' => null,
			'purchaseinvoice_diskonnominal' => null,			
		];
		$act = $this->tm_purchaseinvoice->update_data (['purchaseinvoice_kd' => $purchaseinvoice_kd], $data);
		if ($act == true){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}

	public function action_hapus_detail () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$purchaseinvoicedetail_kd = $this->input->get('id', TRUE);

		/** Cek terkait GR */
		$rmgr_kd = null;
		$qPurchinvDetail = $this->td_purchaseinvoice_detail->get_by_param(['purchaseinvoicedetail_kd' => $purchaseinvoicedetail_kd])->row_array();
		$rmgr_kd = $qPurchinvDetail['rmgr_kd'];

		$act = $this->td_purchaseinvoice_detail->delete_data ($purchaseinvoicedetail_kd);
		if ($act == true){
			if (!empty($rmgr_kd)){
				$actUpd = $this->td_rawmaterial_goodsreceive->update_data (['rmgr_kd' => $rmgr_kd ], ['rmgr_invoice' => null]);
			}
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}

	public function action_hapus_master () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$purchaseinvoice_kd = $this->input->get('id', TRUE);

		/** Cek terkait GR */
		$dataRmgr = [];
		$qPurchinvDetail = $this->td_purchaseinvoice_detail->get_by_param(['purchaseinvoice_kd' => $purchaseinvoice_kd])->result_array();
		foreach ($qPurchinvDetail as $e) {
			if (empty($e['rmgr_kd'])){
				continue;
			}
			$dataRmgr[] = [
				'rmgr_kd' => $e['rmgr_kd'],
				'rmgr_invoice' => null,
			];
		}

		$actDetail = $this->td_purchaseinvoice_detail->delete_by_param(['purchaseinvoice_kd' => $purchaseinvoice_kd]);
		if ($actDetail == true){
			$actMaster = $this->tm_purchaseinvoice->delete_data($purchaseinvoice_kd);
			if (!empty($dataRmgr)){
				$actUpd = $this->td_rawmaterial_goodsreceive->update_batch ('rmgr_kd', $dataRmgr);
			}
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}

	public function set_to_sap()
	{
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$purchaseinvoice_kd = $this->input->get('id', TRUE);
		$act = $this->tm_purchaseinvoice->update_data(['purchaseinvoice_kd' => $purchaseinvoice_kd], ['status_sap' => "1"]);
		$data['PI'] = $this->tm_purchaseinvoice->get_by_param1(['purchaseinvoice_kd' => $purchaseinvoice_kd])->result_array();
		$data['PID'] = $this->td_purchaseinvoice_detail->get_by_param1(['purchaseinvoice_kd' => $purchaseinvoice_kd])->result_array();
		
		if ($act){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan', 'data' => $data);
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		}
		echo json_encode($resp);
	}	

	public function rollback_to_sap()
	{
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$purchaseinvoice_kd = $this->input->get('id', TRUE);
		$act = $this->tm_purchaseinvoice->update_data(['purchaseinvoice_kd' => $purchaseinvoice_kd], ['status_sap' => "0"]);
		if ($act){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		}
		echo json_encode($resp);
	}	

	public function action_hapus_um() {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$purchaseinvoice_kd = $this->input->get('id', TRUE);

		$rPurchinv = $this->tm_purchaseinvoice->get_by_param(['purchaseinvoice_kd' => $purchaseinvoice_kd])->row_array();
		$calGrandTotal = $this->tm_purchaseinvoice->calc_grandtotal_dp_batch([$purchaseinvoice_kd]);
		$purchaseinvoice_dp = $calGrandTotal[0]['grandTotal'];

		$rPurchinvParent = $this->tm_purchaseinvoice->get_by_param(['purchaseinvoice_kd' => $rPurchinv['purchaseinvoice_parent']])->row_array();
		$sisadp = $rPurchinvParent['purchaseinvoice_dp'] - $purchaseinvoice_dp;

		$act = $this->tm_purchaseinvoice->update_data(['purchaseinvoice_kd' => $purchaseinvoice_kd], ['purchaseinvoice_parent' => null]);
		if ($act == true){
			$actUpdUm = $this->tm_purchaseinvoice->update_data(['purchaseinvoice_kd' => $rPurchinv['purchaseinvoice_parent']], ['purchaseinvoice_dp' => $sisadp]);
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Terhapus');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Hapus');
		}

		echo json_encode($resp);
	}

	public function action_update_grandtotal () {
		if (!$this->input->is_ajax_request()){
			exit('No direct script access allowed');
		}
		$purchaseinvoice_kd = $this->input->get('id', TRUE);
		$purchaseinvoice_grandtotal = $this->input->get('grandtotal', TRUE);


		

		$act = $this->tm_purchaseinvoice->update_data(['purchaseinvoice_kd' => $purchaseinvoice_kd], ['purchaseinvoice_grandtotal' => !empty($purchaseinvoice_grandtotal) ? $purchaseinvoice_grandtotal : null, 'purchaseinvoice_tgledit' => date('Y-m-d H:i:s')]);
		if ($act){
			$resp = array('code'=> 200, 'status' => 'Sukses', 'pesan' => 'Tersimpan');
		}else{
			$resp = array('code'=> 400, 'status' => 'Gagal', 'pesan' => 'Gagal Simpan');
		}

		echo json_encode($resp);
	}
	
}
