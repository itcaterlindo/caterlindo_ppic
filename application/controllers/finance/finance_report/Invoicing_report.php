<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Invoicing_report extends MY_Controller {
	private $class_link = 'finance/finance_report/invoicing_report';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_purchaseinvoice', 'tm_suplier', 'td_purchaseinvoice_detail', 'td_rawmaterial_goodsreceive', 'tb_pph']);
	}

    public function index() {
        parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
        parent::datetimepicker_assets();

        $start = $this->input->get('start');
        $end = $this->input->get('end');

        $data['class_link'] = $this->class_link;
        $data['start'] = !empty($start) ? $start : date('Y-m-d');
        $data['end'] = !empty($end) ? $end : date('Y-m-d');
        $this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

    private function splitInterval ($sStartdt, $sEnddt) {
        $startdt = new Datetime($sStartdt);
        $enddt = new Datetime($sEnddt);
        $intervaldt = $enddt->diff($startdt)->days + 1;
        $intervalPeriode = 7;
        $jmlPeriode = ceil($intervaldt / $intervalPeriode);

        $strtimedt = strtotime("-1 day", strtotime($sStartdt));
        $aPeriode = [];
        $countIntvl = 1;
        for ($i=1; $i<=$jmlPeriode; $i++){

            $aPerodedt = [];
            if (($intervaldt - $countIntvl) >= $intervalPeriode){
                $intervalPeriode = $intervalPeriode;
            }else{
                $intervalPeriode = $intervaldt - $countIntvl + 1;
            }
            for($j=1; $j <= $intervalPeriode; $j++){
                $aPerodedt[] = date('Y-m-d', strtotime("+$countIntvl day", $strtimedt));
                $countIntvl++;
            }

            /** Result periode ke */
            // $aPeriode[$i] = $aPerodedt;
            $aPeriode["$i/".$aPerodedt[0]."/".$aPerodedt[$intervalPeriode-1]] = $aPerodedt;
        }
        return $aPeriode;
    }

    function table_main() {
        $sStartdt = $this->input->get('start');
        $sEnddt = $this->input->get('end');
        
        if (empty($sStartdt)){
            $sStartdt = date('Y-m-d');
        }
        if (empty($sEnddt)){
            $sEnddt = date('Y-m-d');
        }

        $qInvoice = $this->db->select('tm_purchaseinvoice.*, tm_suplier.suplier_kode, tm_suplier.suplier_nama, GROUP_CONCAT(DISTINCT(tm_purchaseorder.po_no) SEPARATOR " ; ") AS po_no, 
                SUM(td_purchaseinvoice_detail.purchaseinvoicedetail_pphnominal) as purchaseinvoicedetail_pphnominal')
            ->from('td_purchaseinvoice_detail')
            ->join('tm_purchaseinvoice', 'td_purchaseinvoice_detail.purchaseinvoice_kd=tm_purchaseinvoice.purchaseinvoice_kd')
            ->join('td_rawmaterial_goodsreceive', 'td_purchaseinvoice_detail.rmgr_kd=td_rawmaterial_goodsreceive.rmgr_kd', 'left')
            ->join('tm_purchaseorder', 'td_rawmaterial_goodsreceive.po_kd=tm_purchaseorder.po_kd', 'left')
            ->join('tm_suplier', 'tm_purchaseinvoice.suplier_kd=tm_suplier.suplier_kd', 'left')
            ->where(['tm_purchaseinvoice.purchaseinvoice_tanggaltermpayment >=' => $sStartdt, 'tm_purchaseinvoice.purchaseinvoice_tanggaltermpayment <=' => $sEnddt])
            ->group_by('tm_purchaseinvoice.purchaseinvoice_kd')
            ->order_by('tm_purchaseinvoice.purchaseinvoice_tanggaltermpayment')->get();
            
        $aSplitint = $this->splitInterval($sStartdt, $sEnddt);
        $aResult = [];
        foreach ($aSplitint as $eSplitint => $element){
            $aFilterpriode = [];
            $expDt = explode('/', $eSplitint);
            $startTm = strtotime($expDt[1]);
            $endTm = strtotime($expDt[2]);
            foreach($qInvoice->result_array() as $eInvoice) {
                $termpaymentTm = strtotime($eInvoice['purchaseinvoice_tanggaltermpayment']);
                if ($termpaymentTm >= $startTm && $termpaymentTm <= $endTm){
                    $aFilterpriode[] = $eInvoice;
                }
            }
            $aResult[$eSplitint] = $aFilterpriode; # Group by periode per item invoice
        }
        // echo json_encode($aResult);die();
        # Group by suplier per periode
        foreach ($aResult as $eResult => $element) {
            $aGroupbySuplier = []; $aGroupbySuplierResult = [];
            foreach ($element as $eachElement) {
                $key = $eachElement['suplier_kd'];
                if (!array_key_exists($key, $aGroupbySuplier)) {
                    $aGroupbySuplier[$key] = array(
                        'suplier_kd' => $eachElement['suplier_kd'],
                        'suplier_kode' => $eachElement['suplier_kode'],
                        'suplier_nama' => $eachElement['suplier_nama'],
                        'purchaseinvoice_kd' => $eachElement['purchaseinvoice_kd'],
                        'po_no' => $eachElement['po_no'],
                        'purchaseinvoice_no' => $eachElement['purchaseinvoice_no'],
                        'sumGrandtotal' => $eachElement['purchaseinvoice_grandtotal'],
                        'sumPurchaseinvoicedetail_pphnominal' => $eachElement['purchaseinvoicedetail_pphnominal'],
                    );
                } else {
                    $aGroupbySuplier[$key]['sumGrandtotal'] = $aGroupbySuplier[$key]['sumGrandtotal'] + $eachElement['purchaseinvoice_grandtotal'];
                    $aGroupbySuplier[$key]['sumPurchaseinvoicedetail_pphnominal'] = $aGroupbySuplier[$key]['sumPurchaseinvoicedetail_pphnominal'] + $eachElement['purchaseinvoicedetail_pphnominal'];
                    $aGroupbySuplier[$key]['purchaseinvoice_no'] = $aGroupbySuplier[$key]['purchaseinvoice_no'] .' ; '. $eachElement['purchaseinvoice_no'];
                    $aGroupbySuplier[$key]['po_no'] = $aGroupbySuplier[$key]['po_no'] .' ; '. $eachElement['po_no'];
                    $aGroupbySuplier[$key]['purchaseinvoice_kd'] = $aGroupbySuplier[$key]['purchaseinvoice_kd'] .' ; '. $eachElement['purchaseinvoice_kd'];
                }
            }
            foreach ($aGroupbySuplier as $eGroupbySuplier){
                $aGroupbySuplierResult[] = $eGroupbySuplier;
            }
            $arrAfterGroupSuplier[$eResult] = $aGroupbySuplierResult;
        }
        #remove duplicate po_no
        $arrLastResult = []; 
        foreach ($arrAfterGroupSuplier as $periode => $element){
            $po_unique = '';$elemenUnique = [];
            foreach($element as $el){
                $arrPo = explode(' ; ', $el['po_no']);
                $arrPo = array_unique($arrPo);
                $po_unique = implode(' ; ', $arrPo);
                $elemenUnique[] = array_merge($el, ['po_unique' => $po_unique]);
            }
            $arrLastResult[$periode] = $elemenUnique;
        }
        
        $data['result'] = $arrLastResult;
        
        $this->load->view('page/'.$this->class_link.'/table_main', $data);
    }
	
}
