<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Outstanding_invoicing_report extends MY_Controller {
	private $class_link = 'finance/finance_report/outstanding_invoicing_report';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_purchaseinvoice', 'tm_suplier', 'td_purchaseinvoice_detail', 'td_rawmaterial_goodsreceive', 'tb_pph']);
	}

    public function index() {
        parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
        parent::datetimepicker_assets();

        $start = $this->input->get('start');
        $end = $this->input->get('end');

        $data['class_link'] = $this->class_link;
        $data['start'] = !empty($start) ? $start : date('Y-m-d');
        $data['end'] = !empty($end) ? $end : date('Y-m-d');
        $this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

    private function splitInterval ($sStartdt, $sEnddt) {
        $startdt = new Datetime($sStartdt);
        $enddt = new Datetime($sEnddt);
        $intervaldt = $enddt->diff($startdt)->days + 1;
        $intervalPeriode = 7;
        $jmlPeriode = ceil($intervaldt / $intervalPeriode);

        $strtimedt = strtotime("-1 day", strtotime($sStartdt));
        $aPeriode = [];
        $countIntvl = 1;
        for ($i=1; $i<=$jmlPeriode; $i++){

            $aPerodedt = [];
            if (($intervaldt - $countIntvl) >= $intervalPeriode){
                $intervalPeriode = $intervalPeriode;
            }else{
                $intervalPeriode = $intervaldt - $countIntvl + 1;
            }
            for($j=1; $j <= $intervalPeriode; $j++){
                $aPerodedt[] = date('Y-m-d', strtotime("+$countIntvl day", $strtimedt));
                $countIntvl++;
            }

            /** Result periode ke */
            // $aPeriode[$i] = $aPerodedt;
            $aPeriode["$i/".$aPerodedt[0]."/".$aPerodedt[$intervalPeriode-1]] = $aPerodedt;
        }
        return $aPeriode;
    }

    function table_main() {
        $sStartdt = $this->input->get('start');
        $sEnddt = $this->input->get('end');
        
        if (empty($sStartdt)){
            $sStartdt = date('Y-m-d');
        }
        if (empty($sEnddt)){
            $sEnddt = date('Y-m-d');
        }

        // $qInvoice = $this->db->select('tmpi.*,tm_suplier.suplier_kode,tm_suplier.suplier_nama,GROUP_CONCAT(DISTINCT(tm_purchaseorder.po_no) SEPARATOR " ; ") AS po_no, ,SUM(td_purchaseinvoice_detail.purchaseinvoicedetail_pphnominal) as purchaseinvoicedetail_pphnominal')
        //                     ->from('td_purchaseinvoice_detail')
        //                     ->join('(SELECT tmpin.* FROM tm_purchaseinvoice as tmpin 
        //                         LEFT JOIN td_payment ON tmpin.purchaseinvoice_kd = td_payment.invoice_kd 
        //                         WHERE invoice_kd IS NULL) as tmpi', 'td_purchaseinvoice_detail.purchaseinvoice_kd = tmpi.purchaseinvoice_kd')
        //                     ->join('td_rawmaterial_goodsreceive', 'td_purchaseinvoice_detail.rmgr_kd = td_rawmaterial_goodsreceive.rmgr_kd', 'LEFT')
        //                     ->join('tm_purchaseorder', 'td_rawmaterial_goodsreceive.po_kd = tm_purchaseorder.po_kd', 'LEFT')
        //                     ->join('tm_suplier', 'tmpi.suplier_kd = tm_suplier.suplier_kd', 'LEFT')
        //                     ->where('tmpi.purchaseinvoice_tanggaltermpayment >=', $sStartdt)
        //                     ->where('tmpi.purchaseinvoice_tanggaltermpayment <=', $sEnddt)
        //                     ->group_by('tmpi.purchaseinvoice_kd')
        //                     ->order_by('tmpi.purchaseinvoice_tanggaltermpayment ASC')
        //                     ->get();

        $qInvoice = $this->db->select('*')
                    ->from('(SELECT tmpi.*, tm_suplier.suplier_kode, tm_suplier.suplier_nama, 
                    IFNULL((tmpi.purchaseinvoice_grandtotal - td_payment.grand_total), tmpi.purchaseinvoice_grandtotal) as sisa_payment, 
                    IFNULL(td_payment.grand_total, 0) as total_di_bayar, GROUP_CONCAT(DISTINCT(tm_purchaseorder.po_no) SEPARATOR " ; ") AS po_no, SUM(td_purchaseinvoice_detail.purchaseinvoicedetail_pphnominal) as purchaseinvoicedetail_pphnominal 
                        FROM td_purchaseinvoice_detail 
                            JOIN tm_purchaseinvoice as tmpi ON td_purchaseinvoice_detail.purchaseinvoice_kd = tmpi.purchaseinvoice_kd 
                            LEFT JOIN td_rawmaterial_goodsreceive ON td_purchaseinvoice_detail.rmgr_kd = td_rawmaterial_goodsreceive.rmgr_kd 
                            LEFT JOIN tm_purchaseorder ON td_rawmaterial_goodsreceive.po_kd = tm_purchaseorder.po_kd 
                        LEFT JOIN td_payment ON td_payment.invoice_kd = tmpi.purchaseinvoice_kd 
                            LEFT JOIN tm_suplier ON tmpi.suplier_kd = tm_suplier.suplier_kd 
                    GROUP BY tmpi.purchaseinvoice_kd ORDER BY tmpi.purchaseinvoice_tanggaltermpayment ASC) as OUTSTANDING_PI')
                    ->where('sisa_payment >=', '500')
                    ->where('purchaseinvoice_tanggaltermpayment >=', $sStartdt)
                    ->where('purchaseinvoice_tanggaltermpayment <=', $sEnddt)
                    ->get();
                        
        $aSplitint = $this->splitInterval($sStartdt, $sEnddt);
        $aResult = [];
        foreach ($aSplitint as $eSplitint => $element){
            $aFilterpriode = [];
            $expDt = explode('/', $eSplitint);
            $startTm = strtotime($expDt[1]);
            $endTm = strtotime($expDt[2]);
            foreach($qInvoice->result_array() as $eInvoice) {
                $termpaymentTm = strtotime($eInvoice['purchaseinvoice_tanggaltermpayment']);
                if ($termpaymentTm >= $startTm && $termpaymentTm <= $endTm){
                    $aFilterpriode[] = $eInvoice;
                }
            }
            $aResult[$eSplitint] = $aFilterpriode; # Group by periode per item invoice
        }
        // echo json_encode($aResult);die();
        # Group by suplier per periode
        foreach ($aResult as $eResult => $element) {
            $aGroupbySuplier = []; $aGroupbySuplierResult = [];
            foreach ($element as $eachElement) {
                $key = $eachElement['suplier_kd'];
                if (!array_key_exists($key, $aGroupbySuplier)) {
                    $aGroupbySuplier[$key] = array(
                        'suplier_kd' => $eachElement['suplier_kd'],
                        'suplier_kode' => $eachElement['suplier_kode'],
                        'suplier_nama' => $eachElement['suplier_nama'],
                        'purchaseinvoice_kd' => $eachElement['purchaseinvoice_kd'],
                        'po_no' => $eachElement['po_no'],
                        'purchaseinvoice_no' => $eachElement['purchaseinvoice_no'],
                        'sumGrandtotal' => $eachElement['sisa_payment'],
                        'sumPurchaseinvoicedetail_pphnominal' => $eachElement['purchaseinvoicedetail_pphnominal'],
                    );
                } else {
                    $aGroupbySuplier[$key]['sumGrandtotal'] = $aGroupbySuplier[$key]['sumGrandtotal'] + $eachElement['sisa_payment'];
                    $aGroupbySuplier[$key]['sumPurchaseinvoicedetail_pphnominal'] = $aGroupbySuplier[$key]['sumPurchaseinvoicedetail_pphnominal'] + $eachElement['purchaseinvoicedetail_pphnominal'];
                    $aGroupbySuplier[$key]['purchaseinvoice_no'] = $aGroupbySuplier[$key]['purchaseinvoice_no'] .' ; '. $eachElement['purchaseinvoice_no'];
                    $aGroupbySuplier[$key]['po_no'] = $aGroupbySuplier[$key]['po_no'] .' ; '. $eachElement['po_no'];
                    $aGroupbySuplier[$key]['purchaseinvoice_kd'] = $aGroupbySuplier[$key]['purchaseinvoice_kd'] .' ; '. $eachElement['purchaseinvoice_kd'];
                }
            }
            foreach ($aGroupbySuplier as $eGroupbySuplier){
                $aGroupbySuplierResult[] = $eGroupbySuplier;
            }
            $arrAfterGroupSuplier[$eResult] = $aGroupbySuplierResult;
        }
        #remove duplicate po_no
        $arrLastResult = []; 
        foreach ($arrAfterGroupSuplier as $periode => $element){
            $po_unique = '';$elemenUnique = [];
            foreach($element as $el){
                $arrPo = explode(' ; ', $el['po_no']);
                $arrPo = array_unique($arrPo);
                $po_unique = implode(' ; ', $arrPo);
                $elemenUnique[] = array_merge($el, ['po_unique' => $po_unique]);
            }
            $arrLastResult[$periode] = $elemenUnique;
        }
        
        $data['result'] = $arrLastResult;
        
        $this->load->view('page/'.$this->class_link.'/table_main', $data);
    }
	
}
