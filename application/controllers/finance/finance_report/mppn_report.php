<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class mppn_report extends MY_Controller {
	private $class_link = 'finance/finance_report/mppn_report';

	public function __construct() {
		parent::__construct();

		$this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
		$this->load->model(['tm_purchaseinvoice', 'tm_suplier', 'td_purchaseinvoice_detail', 'td_rawmaterial_goodsreceive', 'tb_pph', 'tm_payment', 'td_payment', 'tm_m_payment']);
	}

	public function index() {
        parent::administrator();
		parent::pnotify_assets();
		parent::select2_assets();
        parent::datetimepicker_assets();
        $this->load->js('assets/admin_assets/plugins/input-mask/jquery.inputmask.js');
        $data['class_link'] = $this->class_link;
        $this->load->view('page/'.$this->class_link.'/table_box', $data);
	}

    public function table_main(){
    	// if (!$this->input->is_ajax_request()){
    	// 	exit('No direct script access allowed');
    	// }
		$bulan = $this->input->get('bulan', true);
        $tahun = $this->input->get('tahun', true);

    	$data['class_link'] = $this->class_link;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;

		// echo json_encode($data);

    	$this->load->view('page/'.$this->class_link.'/table_main', $data);
    }

    public function table_data (){
    	// if (!$this->input->is_ajax_request()){
    	// 	exit('No direct script access allowed');
    	// }
			
		$bulan = $this->input->get('bulan', true);
        $tahun = $this->input->get('tahun', true);

    	$this->load->library(['ssp']);

		$data = $this->ssp_table2($bulan, $tahun);
		
		echo json_encode($data);
    }

    public function ssp_table2 ($bulan, $tahun) {
		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		
		$data = [];
		if($bulan != 'ALL'){
            $qData = $this->db->select('sp.suplier_kode,sp.suplier_nama,sp.suplier_banknama,sp.suplier_banknorek,sp.suplier_npwp,iv.purchaseinvoice_faktur,iv.purchaseinvoice_no,iv.purchaseinvoice_kd,iv.purchaseinvoice_tanggal,py.tanggal_input,mpay.no_rek,mpay.nm_bank')
                                    ->from('tm_purchaseinvoice AS iv')
                                    ->join('tm_suplier sp', 'sp.suplier_kd = iv.suplier_kd')
                                    ->join('td_payment', 'td_payment.invoice_kd = iv.purchaseinvoice_kd', 'LEFT')
                                    ->join('tm_payment py', 'py.payment_kd = td_payment.payment_kd', 'LEFT')
                                    ->join('tm_m_payment mpay', 'mpay.m_payment_kd = py.m_payment_kd', 'LEFT')
                                    ->where('MONTH(iv.purchaseinvoice_tanggal)', $bulan)
			                        ->where('YEAR(iv.purchaseinvoice_tanggal)', $tahun)
                                    ->get();
		}else{
			$qData = $this->db->select('sp.suplier_kode,sp.suplier_nama,sp.suplier_banknama,sp.suplier_banknorek,sp.suplier_npwp,iv.purchaseinvoice_faktur,iv.purchaseinvoice_no, iv.purchaseinvoice_kd,iv.purchaseinvoice_tanggal,py.tanggal_input,mpay.no_rek,mpay.nm_bank')
                                    ->from('tm_purchaseinvoice AS iv')
                                    ->join('tm_suplier sp', 'sp.suplier_kd = iv.suplier_kd')
                                    ->join('td_payment', 'td_payment.invoice_kd = iv.purchaseinvoice_kd', 'LEFT')
                                    ->join('tm_payment py', 'py.payment_kd = td_payment.payment_kd', 'LEFT')
                                    ->join('tm_m_payment mpay', 'mpay.m_payment_kd = py.m_payment_kd', 'LEFT')
			                        ->where('YEAR(iv.purchaseinvoice_tanggal)', $tahun)
                                    ->get();
		}

		foreach ($qData->result_array() as $r) {
			$getPo = $this->get_po($r['purchaseinvoice_kd']);
            $getNosrj = $this->get_nosrj($r['purchaseinvoice_kd']);
            $getDpp = $this->get_dpp($r['purchaseinvoice_kd']);
            $getPpn = $this->get_ppn($r['purchaseinvoice_kd']);
            $getNm = $this->get_nm_barang($r['purchaseinvoice_kd']);

			$data[] = [
				'1' => $r['suplier_kode'],
				'2' => $r['suplier_nama'],
				'3' => $r['suplier_banknama'],
				'4' => $r['suplier_banknorek'],
				'5' => $r['suplier_npwp'],
				'6' => $getPo,
                '7' => $r['purchaseinvoice_faktur'],
				'8' => $r['purchaseinvoice_no'],
				'9' => $getNosrj,
				'10' => $r['purchaseinvoice_tanggal'],
				'11' => $getDpp,
				'12' => $getPpn,
				'13' => $getNm,
                '14' => $r['tanggal_input'],
				'15' => $r['nm_bank'],
			];
		}
		
		$output = array(
			'draw' => $draw,
			'recordsTotal' => count($data),
			'recordsFiltered' => count($data),
			'data' => $data
		);
		return $output;
	}

    private function tbl_btn($id) {
		$btns = array();
		if (cek_permission('PURCHASEINVOICE_VIEW')) {
			$btns[] = get_btn(array('title' => 'Lihat Item', 'icon' => 'search', 'onclick' => 'view_box(\''.$id.'\')'));
		}
        if (cek_permission('PURCHASEINVOICE_UPDATE')) {
            $btns[] = get_btn(array('title' => 'Edit Item', 'icon' => 'pencil', 'onclick' => 'edit_data(\''.$id.'\')'));
        }
		$btn_group = group_btns($btns);

		return $btn_group;
	}

	public function get_po($pi_kd)
	{
		$items = '';
		$database = $this->db->select('po.po_no')
                    ->from('tm_purchaseorder po')
                    ->join('td_purchaseorder_detail pod', 'pod.po_kd = po.po_kd')
                    ->join('td_purchaseinvoice_detail pid', 'pid.podetail_kd = pod.podetail_kd')
                    ->where(array('pid.purchaseinvoice_kd' => $pi_kd))
                    ->group_by('po.po_no')
                    ->get()->result_array();

		foreach ($database as $r) {
			$items .= $r['po_no'] . '; ';
			
		}
		return $items;
	}

    public function get_nosrj($pi_kd)
	{
		$items = '';
		$database = $this->db->select('rmgr.rmgr_nosrj')
                    ->from('td_rawmaterial_goodsreceive rmgr')
                    ->join('td_purchaseinvoice_detail pid', 'pid.rmgr_kd = rmgr.rmgr_kd')
                    ->where(array('pid.purchaseinvoice_kd' => $pi_kd))
                    ->group_by('rmgr.rmgr_nosrj')
                    ->get()->result_array();

		foreach ($database as $r) {
			$items .= $r['rmgr_nosrj'] . '; ';
			
		}
		return $items;
	}

    public function get_dpp($pi_kd)
    {
        $data = $this->tm_purchaseinvoice->get_by_param (['purchaseinvoice_kd' => $pi_kd])->row_array();
        $data_ku = $this->db->select('td_purchaseinvoice_detail.*, td_purchaseorder_detail.podetail_harga, tm_purchaseorder.po_no, td_rawmaterial_goodsreceive.rmgr_nosrj,
						tm_rawmaterial.rm_kode, tm_rawmaterial.rm_oldkd, td_rawmaterial_satuan.rmsatuan_nama, tb_pph.pph_nama')
					->from('td_purchaseinvoice_detail')
					->join('td_purchaseorder_detail', 'td_purchaseinvoice_detail.podetail_kd=td_purchaseorder_detail.podetail_kd', 'left')
					->join('tm_purchaseorder', 'td_purchaseorder_detail.po_kd=tm_purchaseorder.po_kd', 'left')
					->join('td_rawmaterial_goodsreceive', 'td_purchaseinvoice_detail.rmgr_kd=td_rawmaterial_goodsreceive.rmgr_kd', 'left')
					->join('tm_rawmaterial', 'td_purchaseinvoice_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
					->join('td_rawmaterial_satuan', 'td_purchaseinvoice_detail.purchaseinvoicedetail_satuankd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
					->join('tb_pph', 'td_purchaseinvoice_detail.pph_kd=tb_pph.pph_kd', 'left')
					->where('td_purchaseinvoice_detail.purchaseinvoice_kd', $pi_kd)
					->get()->result_array();

        $dum = 0; $total = 0;
        foreach ($data_ku as $r){
            $dum = $r['purchaseinvoicedetail_hargaunit'] * $r['purchaseinvoicedetail_qty'];
            $total += $dum;
        }

        $diskonrp = 0;
        $diskontxt = '';
        if ($data['purchaseinvoice_jenisdiskon'] == 'nominal') {
            $diskonrp = $data['purchaseinvoice_diskonnominal'];
        }elseif ($data['purchaseinvoice_jenisdiskon'] == 'persentase') {
            $diskonrp = $data['purchaseinvoice_diskonpersen'] / 100 * $total;
            $diskontxt = '('.$data['purchaseinvoice_diskonpersen'].'%)'; 
        }
    
        $uangmuka = $data['purchaseinvoice_dp'];
        $dpp = $total - $diskonrp - $uangmuka;

        $hasil_rupiah_dpp = "Rp " . number_format($dpp,2,',','.');

	    return $hasil_rupiah_dpp;

    }

    public function get_ppn($pi_kd)
    {
        $data = $this->tm_purchaseinvoice->get_by_param (['purchaseinvoice_kd' => $pi_kd])->row_array();
        $data_ku = $this->db->select('td_purchaseinvoice_detail.*, td_purchaseorder_detail.podetail_harga, tm_purchaseorder.po_no, td_rawmaterial_goodsreceive.rmgr_nosrj,
						tm_rawmaterial.rm_kode, tm_rawmaterial.rm_oldkd, td_rawmaterial_satuan.rmsatuan_nama, tb_pph.pph_nama')
					->from('td_purchaseinvoice_detail')
					->join('td_purchaseorder_detail', 'td_purchaseinvoice_detail.podetail_kd=td_purchaseorder_detail.podetail_kd', 'left')
					->join('tm_purchaseorder', 'td_purchaseorder_detail.po_kd=tm_purchaseorder.po_kd', 'left')
					->join('td_rawmaterial_goodsreceive', 'td_purchaseinvoice_detail.rmgr_kd=td_rawmaterial_goodsreceive.rmgr_kd', 'left')
					->join('tm_rawmaterial', 'td_purchaseinvoice_detail.rm_kd=tm_rawmaterial.rm_kd', 'left')
					->join('td_rawmaterial_satuan', 'td_purchaseinvoice_detail.purchaseinvoicedetail_satuankd=td_rawmaterial_satuan.rmsatuan_kd', 'left')
					->join('tb_pph', 'td_purchaseinvoice_detail.pph_kd=tb_pph.pph_kd', 'left')
					->where('td_purchaseinvoice_detail.purchaseinvoice_kd', $pi_kd)
					->get()->result_array();

        $dum = 0; $total = 0;
        foreach ($data_ku as $r){
            $dum = $r['purchaseinvoicedetail_hargaunit'] * $r['purchaseinvoicedetail_qty'];
            $total += $dum;
        }

        $diskonrp = 0;
        $diskontxt = '';
        if ($data['purchaseinvoice_jenisdiskon'] == 'nominal') {
            $diskonrp = $data['purchaseinvoice_diskonnominal'];
        }elseif ($data['purchaseinvoice_jenisdiskon'] == 'persentase') {
            $diskonrp = $data['purchaseinvoice_diskonpersen'] / 100 * $total;
            $diskontxt = '('.$data['purchaseinvoice_diskonpersen'].'%)'; 
        }
    
        $uangmuka = $data['purchaseinvoice_dp'];
        $dpp = $total - $diskonrp - $uangmuka;

        $ppnRp=0; $ppntxt = '';
    
        if (!empty($data['purchaseinvoice_ppn'])) {
            
            $tgl_ppn_naik = '2022-03-31';
            $tgl_invoice = $data['purchaseinvoice_tanggal'];
            if(($tgl_ppn_naik <  $tgl_invoice) == true){
                if($data['purchaseinvoice_ppn'] == '10'){
                    $data['purchaseinvoice_ppn'] = '11';
                }
                $ppnRp = $data['purchaseinvoice_ppn'] / 100 * $dpp;
                $ppntxt = '('.$data['purchaseinvoice_ppn'].'%)';
            }else{
                $ppnRp = $data['purchaseinvoice_ppn'] / 100 * $dpp;
                $ppntxt = '('.$data['purchaseinvoice_ppn'].'%)';
            }
            
        }

        $hasil_rupiah_dpp = "Rp " . number_format($ppnRp,2,',','.');

	    return $hasil_rupiah_dpp;
    }

    public function get_nm_barang($pi_kd)
    {
        $items = '';
        $database = $this->db->select('CONCAT(pid.purchaseinvoicedetail_deskripsi,\'/\',pid.purchaseinvoicedetail_spesifikasi) as nm_brg')
                    ->from('td_purchaseinvoice_detail pid')
                    ->where(array('pid.purchaseinvoice_kd' => $pi_kd))
                    ->get()->result_array();

                    foreach ($database as $r) {
                        $items .= $r['nm_brg'] . '; <br>';
                        
                    }
                    return $items;
    }
}