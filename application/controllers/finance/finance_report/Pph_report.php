<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Pph_report extends MY_Controller
{
    private $class_link = 'finance/finance_report/pph_report';

    public function __construct()
    {
        parent::__construct();

        $this->load->helper(['form', 'my_btn_access_helper', 'my_helper']);
        $this->load->model(['tm_purchaseinvoice', 'tm_suplier', 'td_purchaseinvoice_detail', 'td_rawmaterial_goodsreceive', 'tb_pph']);
    }

    public function index()
    {
        parent::administrator();
        parent::pnotify_assets();
        parent::select2_assets();
        parent::datetimepicker_assets();

        $jenisfilter = $this->input->get('jenisfilter');
        $start = $this->input->get('start');
        $end = $this->input->get('end');

        $data['class_link'] = $this->class_link;
        $data['jenisfilter'] = !empty($jenisfilter) ? $jenisfilter : 'INVOICE';
        $data['start'] = !empty($start) ? $start : date('Y-m-d');
        $data['end'] = !empty($end) ? $end : date('Y-m-d');
        $this->load->view('page/' . $this->class_link . '/table_box', $data);
    }

    function table_main()
    {
        $jenisfilter = $this->input->get('jenisfilter');
        $sStartdt = $this->input->get('start');
        $sEnddt = $this->input->get('end');

        if (empty($sStartdt)) {
            $sStartdt = date('Y-m-d');
        }
        if (empty($sEnddt)) {
            $sEnddt = date('Y-m-d');
        }

        $qResult = $this->db->select('tm_purchaseinvoice.*, tm_suplier.suplier_kode, tm_suplier.suplier_nama, GROUP_CONCAT(DISTINCT(tm_purchaseorder.po_no) SEPARATOR " ; ") AS po_no, 
                SUM(td_purchaseinvoice_detail.purchaseinvoicedetail_pphnominal) as purchaseinvoicedetail_pphnominal,
                tb_pph.pph_nama')
            ->join('tm_purchaseinvoice', 'td_purchaseinvoice_detail.purchaseinvoice_kd=tm_purchaseinvoice.purchaseinvoice_kd')
            ->join('tb_pph', 'td_purchaseinvoice_detail.pph_kd=tb_pph.pph_kd', 'left')
            ->join('td_rawmaterial_goodsreceive', 'td_purchaseinvoice_detail.rmgr_kd=td_rawmaterial_goodsreceive.rmgr_kd', 'left')
            ->join('tm_purchaseorder', 'tm_purchaseorder.po_kd=td_rawmaterial_goodsreceive.po_kd', 'left')
            ->join('tm_suplier', 'tm_purchaseinvoice.suplier_kd=tm_suplier.suplier_kd', 'left');
        if ($jenisfilter == 'INVOICE') {
            $qResult = $qResult->where(['tm_purchaseinvoice.purchaseinvoice_tanggal >=' => $sStartdt, 'tm_purchaseinvoice.purchaseinvoice_tanggal <=' => $sEnddt]);
        } elseif ($jenisfilter == 'PAYMENT') {
            $qResult = $qResult->where(['tm_purchaseinvoice.purchaseinvoice_tanggaltermpayment >=' => $sStartdt, 'tm_purchaseinvoice.purchaseinvoice_tanggaltermpayment <=' => $sEnddt]);
        }
        $qResult = $qResult->where('td_purchaseinvoice_detail.pph_kd IS NOT NULL')
            ->group_by('tm_purchaseinvoice.purchaseinvoice_kd, td_purchaseinvoice_detail.pph_kd')
            ->order_by('tm_purchaseinvoice.purchaseinvoice_tanggal, tm_purchaseinvoice.purchaseinvoice_no')
            ->get('td_purchaseinvoice_detail');
        $data['result'] =  $qResult->result_array();
        $this->load->view('page/' . $this->class_link . '/table_main', $data);
    }
}
