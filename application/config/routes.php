<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'auth/Login';
$route['404_override'] = 'notfound';
$route['translate_uri_dashes'] = FALSE;

/*
| contoh rounting controllers
| $route['product/(:num)'] = 'catalog/product_lookup/$1';
| $route['url/:any || :num'] = 'folder/controller/method || controller/method';
*/

$route['login'] = 'auth/Login';
$route['dashboard'] = 'dashboard/home';
$route['activity_log'] = 'administrator/log/activity_log';
$route['administrator/manage_admin/'] = 'administrator/manage_admin/';
$route['manage_items/manage_product/'] = 'manage_items/manage_product/';
$route['app_setting/(:any)/code_format'] = 'app_setting/code_format';
$route['app_setting/(:any)/format_laporan'] = 'app_setting/format_laporan';
$route['sales/sales_orders'] = 'sales/sales_orders/sales_orders_tbl';
$route['sales/data_do'] = 'sales/delivery_orders/data_do';
$route['sales/data_do/(:any)'] = 'sales/delivery_orders/data_do/$1';
$route['sales/items_do/'] = 'sales/delivery_orders/items_do';
$route['sales/items_do/(:any)'] = 'sales/delivery_orders/items_do/$1';

// Cli Routing
$route['cli_request'] = 'cli/cli_request';
$route['cli_request/(:any)'] = 'cli/cli_request/$1';
$route['cli_request/(:any)/(:any)'] = 'cli/cli_request/$1/$2';