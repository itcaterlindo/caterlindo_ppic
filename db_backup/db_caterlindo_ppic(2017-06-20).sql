/*
Navicat MySQL Data Transfer

Source Server         : MySQL to Navicat
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : db_caterlindo_ppic

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2017-06-20 14:51:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tb_admin`
-- ----------------------------
DROP TABLE IF EXISTS `tb_admin`;
CREATE TABLE `tb_admin` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kd_admin` varchar(13) COLLATE latin1_general_ci NOT NULL,
  `tipe_admin_kd` varchar(13) COLLATE latin1_general_ci DEFAULT NULL,
  `kd_karyawan` char(3) COLLATE latin1_general_ci DEFAULT NULL,
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `nm_admin` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `session_id` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tb_admin
-- ----------------------------
INSERT INTO `tb_admin` VALUES ('1', 'ADM190617001', 'TPA190617001', '-', 'admin_galih', '$2y$12$R16pLcdjv3pNJRuNKz6g9ePJxzsVt3VHhjH/5wVHvvyOobkUtLnEq', 'Admin Galih', null);

-- ----------------------------
-- Table structure for `tb_hakakses_menu`
-- ----------------------------
DROP TABLE IF EXISTS `tb_hakakses_menu`;
CREATE TABLE `tb_hakakses_menu` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `admin_kd` varchar(13) COLLATE latin1_general_ci DEFAULT NULL,
  `menu_kd` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tb_hakakses_menu
-- ----------------------------
INSERT INTO `tb_hakakses_menu` VALUES ('1', 'ADM190617001', '1');

-- ----------------------------
-- Table structure for `tb_menu`
-- ----------------------------
DROP TABLE IF EXISTS `tb_menu`;
CREATE TABLE `tb_menu` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nm_menu` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `link_menu` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `icon_menu` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `tipe_menu` enum('single','tree') COLLATE latin1_general_ci DEFAULT NULL,
  `level_menu` int(1) DEFAULT NULL,
  `parent_menu` int(10) DEFAULT NULL,
  `urutan` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tb_menu
-- ----------------------------
INSERT INTO `tb_menu` VALUES ('1', 'dashboard', 'administrator', 'fa fa-home', 'single', '0', '1', '1');

-- ----------------------------
-- Table structure for `tb_setting`
-- ----------------------------
DROP TABLE IF EXISTS `tb_setting`;
CREATE TABLE `tb_setting` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nm_setting` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `val_setting` text COLLATE latin1_general_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tb_setting
-- ----------------------------
INSERT INTO `tb_setting` VALUES ('1', 'header_title', 'Caterlindo PPIC Application');
INSERT INTO `tb_setting` VALUES ('2', 'main_title', 'Production Planning & Inventory Control');
INSERT INTO `tb_setting` VALUES ('3', 'home_title', 'Caterlindo PPIC');
INSERT INTO `tb_setting` VALUES ('4', 'favicon', 'favicon.png');
INSERT INTO `tb_setting` VALUES ('5', 'login_title', 'PPIC Login | Halaman Login');
INSERT INTO `tb_setting` VALUES ('6', 'thn_mulai', '2017');
INSERT INTO `tb_setting` VALUES ('7', 'footer_title', 'Caterlindo PPIC Application');
INSERT INTO `tb_setting` VALUES ('8', 'login_bg_img', 'wallpaper.jpg');
INSERT INTO `tb_setting` VALUES ('9', 'main_bg_img', null);
INSERT INTO `tb_setting` VALUES ('10', 'no_profile_img', 'no-profile-img.jpg');
INSERT INTO `tb_setting` VALUES ('11', 'menu_title', 'Main Menu');

-- ----------------------------
-- Table structure for `td_admin_tipe`
-- ----------------------------
DROP TABLE IF EXISTS `td_admin_tipe`;
CREATE TABLE `td_admin_tipe` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kd_tipe_admin` varchar(13) COLLATE latin1_general_ci NOT NULL,
  `nm_tipe_admin` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of td_admin_tipe
-- ----------------------------
INSERT INTO `td_admin_tipe` VALUES ('1', 'TPA190617001', 'Admin');
